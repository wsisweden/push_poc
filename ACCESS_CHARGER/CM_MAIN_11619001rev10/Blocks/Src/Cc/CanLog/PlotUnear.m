% PlotUnear(data, NodeId)
%   data        Read by readCanTrace() function
%   NodeId      Node id of node
function PlotUnear(data, NodeId)
	idx = find([data.CobId] == 0x280 + NodeId);
	t = [data(idx).t]*1e-3;
	Unear = ([data(idx).data](5,:));
	Unear = 1*bitget(Unear', 1)';

	figure
	plot(t, Unear)
	title(sprintf("U near Node ID %u", NodeId))
	xlabel("t in (s)")
	ylabel("Zero no, otherwise")
endfunction
