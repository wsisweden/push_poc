function data = readCanTrace(filename)
	if(!exist("filename"))
		filename = uigetfile("*.trc");
	endif
	if(filename == 0)
		return
	else
		[fid msg] = fopen(filename, "r");
	endif
	
	if(fid == -1)
		error([msg "\n"]);
	endif

	pos = 1;
	row = skipHeader(fid);
	data(1e6).data(8) = NaN;
	while(row != -1)
		[data(pos).nr data(pos).t data(pos).Mtype data(pos).CobId data(pos).len count errormsg] = sscanf(row, "%u) %f %s %x %u", "C");
		
		data(pos).data(1:8,1) = NaN;
		if(!strcmp(data(pos).Mtype, "Warng"))
			data(pos).data(1:data(pos).len) = sscanf(row(42:end), "%x", data(pos).len);
			pos++;
		endif
		
		row = fgets(fid);
	endwhile
	data = data(1:pos - 1);
	
	fclose(fid);
endfunction

function row = skipHeader(fid)
	row = fgets(fid);
	while(row(1) == ";")
		row = fgets(fid);
	endwhile
endfunction
