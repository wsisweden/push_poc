% PlotP(data, NodeId)
%   data        Read by readCanTrace() function
%   NodeId      Node id of node
function PlotP(data, NodeId)
	set.idx = find([data.CobId] == 0x300 + NodeId);
	set.t = [data(set.idx).t]*1e-3;
	set.P = CanVect2single([data(set.idx).data](1:4,:))*1e-3;

	meas.idx = find([data.CobId] == 0x280 + NodeId);
	meas.t = [data(meas.idx).t]*1e-3;
	meas.P = CanVect2single([data(meas.idx).data](1:4,:))*1e-3;

	figure
	plot(set.t, set.P, meas.t, meas.P)
	title(sprintf("Power Node ID %u", NodeId))
	xlabel("t in (s)")
	ylabel("Charger power in (kW)")
	legend("Set reference", "Measured")
endfunction
