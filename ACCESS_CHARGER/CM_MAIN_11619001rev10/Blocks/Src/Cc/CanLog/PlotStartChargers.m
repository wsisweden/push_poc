% PlotStartChargers(data)
%   data        Read by readCanTrace() function
function PlotStartChargers(data)
	idx = find([data.CobId] == 0x27F);
	t = [data(idx).t]*1e-3;
	StartChargers = [1 256]*[data(idx).data](5:6,:);
	
	figure
	plot(t, StartChargers)
	title(sprintf("Start chargers"))
	xlabel("t in (s)")
	ylabel("Number of chargers to start")
endfunction
