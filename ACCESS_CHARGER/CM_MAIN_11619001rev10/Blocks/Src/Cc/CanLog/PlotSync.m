% PlotSync(data)
%   data        Read by readCanTrace() function
function PlotSync(data)
	idx = find([data.CobId] == 0x80);
	t = [data(idx).t]'*1e-3;
	y = zeros(size(t));

	figure
	plot(t, y, "x")
	title("Sync")
	xlabel("t in (s)")
	ylabel("")
endfunction
