function value = CanVect2single(data)
	value = typecast(uint32(2.^[0 8 16 24]*data), "single");
endfunction
