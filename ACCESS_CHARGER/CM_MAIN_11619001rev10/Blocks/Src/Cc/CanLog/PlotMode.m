% PlotMode(data, NodeId)
%   data        Read by readCanTrace() function
%   NodeId      Node id of node
function PlotMode(data, NodeId)
	idx = find([data.CobId] == 0x300 + NodeId);
	t = [data(idx).t]*1e-3;
	Mode = [data(idx).data](5,:);

	figure
	plot(t, Mode)
	title(sprintf("Mode Node ID %u", NodeId))
	xlabel("t in (s)")
	ylabel("Regulator mode")
endfunction
