function PlotNeedI(data, NodeIdMaster)
	COB_TPDO1 = 0x180;
	COB_TPDO2 = 0x180;
	COB_RPDO1 = 0x200;
	COB_RPDO2 = 0x300;

	idx2 = find([data.CobId] == COB_RPDO2 + NodeIdMaster);
	if(length(idx2) > 0)
		tSet = [data(idx2).t]/1e3;
		Pset = CanVect2single([data(idx2).data](1:4,:));
		
		figure
		plot(tSet, Pset)
		title("Needed current")
		xlabel("t in (s)")
		ylabel("Need current in (A)")
	endif
endfunction
