% PlotDerate(data, NodeId)
%   data        Read by readCanTrace() function
%   NodeId      Node id of node
function PlotDerate(data, NodeId)
	idx = find([data.CobId] == 0x280 + NodeId);
	t = [data(idx).t]*1e-3;
	Derate = [data(idx).data](6,:);

	figure
	plot(t, Derate)
	title(sprintf("Derate Node ID %u", NodeId))
	xlabel("t in (s)")
	ylabel("Zero no derate, otherwise de-rate")
endfunction
