% output = getArray(data, CobId)
%   output      Array with data for Cob ID
%   data        Read by readCanTrace() function
%   CobId       Cob id to extract
function output = getArray(data, CobId)
	idx = find([data.CobId] == CobId);
	output = [data(idx)];
endfunction
