/*
 * ClusterControl.c
 *
 *  Created on: May 17, 2010
 *      Author: nicka
 */
#include "tools.h"
#include "ClusterControl.h"
#include "Cc.h"
#include <math.h>

/* Set values used by chargers */
#define CHARGER_SET_DEFAULT {{0.0f, NAN, 0.0f}, ReguMode_Off, {{BatterydetectCond_NotUsed, BatterydetectCond_NotUsed}}, {{BatterydetectCond_NotUsed, BatterydetectCond_NotUsed}}}

static int countSelectedChargers(void);				// Get number of selected chargers
static int find(uint32_t SerialNumber);				// Find position of charger
static void iterateReset(void);						// Reset iterator
static int iterate(void);							// Return true until all chargers iterated
static int iteratePos(void);						// Return current position
static int countStartedChargers(void);				// Count number of started chargers
static void TurnOffCharger(int n);
static int ISlackAllowed(int charger);
static int notISetLimited(int charger);
static void setTurnOffIndex(void);

int turnOffIndex = 0;
static float Usuppress = 0.0f;
float Isuppress = 0.0f;
static float Iunsuppressed = 0.0f;

struct CcMaster
{
	/* These values are sent to the chargers */
	volatile ChargerSet_type Set[CC_CHARGERS_LEN];	// Set values for all chargers, accessed from two different processes which should be separated in time by sync and TPDO set

	/* These values are received from the chargers */
	volatile ChargerMeas_type Meas[CC_CHARGERS_LEN];// Measurements from all chargers, accessed from two different processes which should be separated in time by sync and TPDO set

	/* Information about chargers */
	float PsetMax;									// Maximum output power
	float IsetMax;									// Maximum output current

	/* Internal values */
	int On[CC_CHARGERS_LEN];						// Indicate if charger turned on
	uint32_t SerialNr[CC_CHARGERS_LEN];				// All serial numbers are unique according to Tomas Sandtstedt 2011-10-21
													// Numbering and addresses may change at any time but this will always address the same charger
													// This is especially important if parameters are read from chargers since they could be assumed to never change
	uint32_t SerialNrThis;							// Serial number of this charger. The serial number is used to identify the charger.
	unsigned int LastStarted;						// Index of last started charger
};
#define CC_MASTER_DEFAULT {{[0 ... CC_CHARGERS_LEN - 1] = CHARGER_SET_DEFAULT},				/* Set				*/ \
						   {[0 ... CC_CHARGERS_LEN - 1] = CHARGER_MEAS_DEFAULT},			/* Meas				*/ \
						   8000.0,															/* PsetMax			*/ \
						   80.0,															/* IsetMax			*/ \
						   {[0 ... CC_CHARGERS_LEN - 1] = 0},								/* SerialNr			*/ \
						   {[0 ... CC_CHARGERS_LEN - 1] = 0},								/* SerialNr			*/ \
						   0,																/* SerialNrThis		*/ \
						   0}																/* LastStarted		*/ \

/* The variable below is accessed from two different processes:
 *   1. Sum of measurements and calculation of new values is run half a period after sync have been set.
 *   2. Measurements are supposed to be received during the first half period until (1.) above and updated by a separate process.
 *   3. Set values are supposed to be sent after (1.) above until the next sync is received and are read by a separate process before sent.
 */
static struct CcMaster Cluster = CC_MASTER_DEFAULT;
static int derateDelayed[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = 0};
static float derateI[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = INFINITY};

/* Watch dog */
#define WATCHDOG_TIME 3
static volatile int WatchDogUI[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = 0};
static volatile int WatchDogPStatus[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = 0};

/* Turn off chargers that for some reason have stopped to work */
static void TurnOffFaulty();

/* Adjust running chargers towards optimal */
static void TurnOffOn(const ChargerSet_type* inSet, float optimal, const ChargerMeas_type* measSum);

static ReguValue_type NeededOutput(const ChargerSet_type* inSet, const ChargerMeas_type* measSum); // Calculate needed output, if a limit is reached set current may for example not be needed

static void CcChargerAdd(uint32_t serialNr);											// add a charger to list of available chargers, if already added do nothing (idempotent function)
static void CcChargerRemove(uint32_t serialNr);											// remove a charger with serial number, if server not added do nothing (idempotent function)

/* Balance load among chargers indicated to be balanced. Under some conditions special handling is required as for example during
 * turn off/on and de-rate and only chargers indicated for balancing should be balanced by this function. */
static void BalanceLoad(ReguValue_type inSet, const ChargerMeas_type* measSum);

static float OptimalChargersToRun(const ReguValue_type* neededOutput);					// Calculate optimal number of chargers to run

ChargerMeas_type CcMasterSum(){
	ChargerMeas_type sum = {{0.0f, 0.0f, 0.0f}, {.Detail.Derate.Detail.NoLoad=1}};				// #JJ fix for battery connected when not supposed to
	{																					// Sum measurements
		float Uconnected = 0.0f;

		{																				// Sum I, P and find largest U
			int n;
			for(n = 0; n < CC_CHARGERS_LEN; n++){										// For all charger positions
				Cluster.Meas[n].Status.Detail.Derate.Detail.LowVoltage = 0;					// Work around stupid flag
				if(Cluster.SerialNr[n] &&												// Charger at position ?
				   !Cluster.Meas[n].Status.Detail.Error.Sum){									// Charger is OK ?
					sum.Meas.I += Cluster.Meas[n].Meas.I;								// Sum current
					sum.Meas.P += Cluster.Meas[n].Meas.P;								// Sum power

					if(!Cluster.Meas[n].Status.Detail.Derate.Detail.NoLoad &&					// Charger detect does not detect no load ?
					   Cluster.Meas[n].Meas.U > Uconnected){							// Higher voltage than measured before ?
						Uconnected = Cluster.Meas[n].Meas.U;							// Store maximum measured voltage at charger with load
					}
				}
			}
		}
		Uconnected -= 6.0f;																// Withdraw to minimum voltage for a charger considered connected

		{																				// Find chargers not connected to the others
			int n;
			for(n = 0; n < CC_CHARGERS_LEN; n++){										// For all charger positions
				if(Cluster.SerialNr[n] &&												// Charger at position ?
				   !Cluster.Meas[n].Status.Detail.Error.Sum &&									// Charger is OK ?
				   Cluster.Meas[n].Meas.U < Uconnected){								// Charger voltage to low to be considered connected ?
					Cluster.Meas[n].Status.Detail.Error.Detail.Watchdog = 1;					// Set error
				}
			}
		}

		{
			int chargers = 0;
			int charger;

			for(charger = 0; charger < CC_CHARGERS_LEN; charger++){						// For all charger positions
				if(Cluster.SerialNr[charger] &&											// Charger at position ?
				   !Cluster.Meas[charger].Status.Detail.Error.Sum &&							// Charger is OK ?
				   !Cluster.Meas[charger].Status.Detail.Derate.Detail.NoLoad){					// Charger does not detect no load ?
					chargers++;
					sum.Meas.U += Cluster.Meas[charger].Meas.U;							// Sum voltage, should be averaged later
				}
			}
			if(chargers){																// Charger which is OK and detect load exist ?
				sum.Meas.U = sum.Meas.U/chargers;										// Calculate average from charger that do not detect no load
			}
			else{
				for(charger = 0; charger < CC_CHARGERS_LEN; charger++){					// For all charger positions
					if(Cluster.SerialNr[charger] &&										// Charger at position ?
						!Cluster.Meas[charger].Status.Detail.Error.Sum &&						// Charger is OK ?
						!Cluster.Meas[charger].Status.Detail.Derate.Detail.NoLoad){			// Charger does not detect no load ?
						chargers++;
						sum.Meas.U += Cluster.Meas[charger].Meas.U;						// Sum voltage, should be averaged later
					}
				}
				if(chargers){															// Charger which is OK and detect load exist ?
					sum.Meas.U = sum.Meas.U/chargers;									// Calculate average from charger that do not detect no load
				}
				else{																	// No charger which is OK and detect load available
					for(charger = 0; charger < CC_CHARGERS_LEN; charger++){				// For all charger positions
						if(Cluster.SerialNr[charger] &&									// Charger at position ?
						   !Cluster.Meas[charger].Status.Detail.Error.Sum){					// Charger is OK ?
							chargers++;
							sum.Meas.U += Cluster.Meas[charger].Meas.U;					// Sum voltage, should be averaged later
						}
					}
					if(chargers){														// Charger which is is OK exist ?
						sum.Meas.U = sum.Meas.U/chargers;								// Calculate average
					}
					else{
						sum.Meas.U = NAN;												// No measured voltage, set to not a number
					}
				}
			}
		}
	}

	{																					// Sum status
		int n;
		int ErrorAll = 1;																// In case none is available consider it as an error
		int DerateAll = 1;																// True if all chargers de-rated
		int Uerr = 0;
		int Ierr = 1;
		int Perr = 1;

		for(n = 0; n < CC_CHARGERS_LEN; n++){											// For all turned on chargers
			if(Cluster.SerialNr[n]){													// Connected charger at position ?
				if(cc_GetAddedConnected(Cluster.SerialNr[n])){										// #JJ fix for battery connected when not supposed to
					if(!Cluster.Meas[n].Status.Detail.Derate.Detail.NoLoad){						// Detect load ?
						Uerr |= Cluster.Meas[n].Status.Detail.Info.Detail.VoltageIsNearSetValue;	// If one charger is near the set voltage the cluster is near the set voltage
						Ierr &= Cluster.Meas[n].Status.Detail.Info.Detail.CurrentIsNearSetValue;	// All chargers must be near the set current
						Perr &= Cluster.Meas[n].Status.Detail.Info.Detail.PowerIsNearSetValue;		// All chargers must be near the set power
						sum.Status.Detail.Derate.Detail.NoLoad = 0;									// #JJ fix for battery connected when not supposed to
					}
				}
				sum.Status.Detail.Derate.Sum |= Cluster.Meas[n].Status.Detail.Derate.Sum;			// Sum de-rate
				if(n != 0)
					sum.Status.Detail.Error.Sum |= Cluster.Meas[n].Status.Detail.Error.Sum;			// Sum errors except own
				ErrorAll = ErrorAll && Cluster.Meas[n].Status.Detail.Error.Sum;						// Logically and together errors, note logic and important since error bits may be different
				DerateAll = DerateAll && Cluster.Meas[n].Status.Detail.Derate.Sum;					// Logically and together errors, note logic and important since error bits may be different
			}
		}

		sum.Status.Detail.Info.Detail.VoltageIsNearSetValue = Uerr;
		sum.Status.Detail.Info.Detail.CurrentIsNearSetValue= Ierr;
		sum.Status.Detail.Info.Detail.PowerIsNearSetValue = Perr;

		if(DerateAll){																	// All chargers de-rated ?
		}
		else{																			// Cluster in total not de-rated
			sum.Status.Detail.Derate.Sum = 0;											// Clear flags and run chargers that are ok
		}

		sum.Status.Detail.Warning.Sum |= sum.Status.Detail.Error.Sum;					// Errors from slaves are warnings in master

		if(ErrorAll){																	// No chargers without error ?
		}
		else{																			// There is at least one available charger without an error
			uint8_t watchdog = Cluster.Meas[0].Status.Detail.Error.Detail.Watchdog;
			sum.Status.Detail.Error.Sum = 0;											// Clear flags and run chargers that are ok
			sum.Status.Detail.Error.Detail.Watchdog = watchdog;							// Show at least can timeout error
		}
	}

	return sum;
}

void CcMasterSet(const ChargerSet_type* inSet, const ChargerMeas_type* measSum, float IsetMax, float PsetMax){
	Cluster.IsetMax = IsetMax;
	Cluster.PsetMax = PsetMax;
	TurnOffFaulty();															// Turn off chargers that for some reason have stopped working
	{
		const ReguValue_type need = NeededOutput(inSet, measSum);				// Calculate how much output is needed, note input may be limited by actual use
		const float optimal = OptimalChargersToRun(&need);						// Calculate optimal number of chargers or optimal combination of chargers to run
		TurnOffOn(inSet, optimal, measSum);										// Adjust the number of running chargers towards optimal
	}

	{																			// Check how many chargers are running
		int charger;

		for(charger = 0; charger < CC_CHARGERS_LEN; charger++){					// For all charger positions
			if(Cluster.On[charger] && !Cluster.Meas[charger].Status.Detail.Derate.Detail.NoLoad){														// Started ?
				if((Cluster.Meas[charger].Status.Detail.Derate.Sum && derateDelayed[charger]) && !isnan(Cluster.Meas[charger].Meas.I) && Cluster.Meas[charger].Meas.I > 0.0f){
					derateI[charger] = Cluster.Meas[charger].Meas.I;
				}
				else if(Cluster.Meas[charger].Meas.I > derateI[charger]){
					derateI[charger] = Cluster.Meas[charger].Meas.I;
				}
			}
		}
	}

	BalanceLoad(inSet->Set, measSum);											// Balance load between different chargers

	{																			// Check how many chargers are running
		int charger;

		for(charger = 0; charger < CC_CHARGERS_LEN; charger++)					// For all charger positions
		{
			if(Cluster.SerialNr[charger]){										// Charger at position ?
				Cluster.Set[charger].Uact = inSet->Uact;						// Set battery detection for sensed voltage
				Cluster.Set[charger].RemoteIn = inSet->RemoteIn;				// Set battery detection for remote in
				if(Cluster.On[charger]){										// Started ?
					Cluster.Set[charger].Mode = inSet->Mode;					// Set the same mode as input
				}
			}

			if(Cluster.On[charger]){											// Started ?
				derateDelayed[charger] = Cluster.Meas[charger].Status.Detail.Derate.Sum;
			}
		}
	}
}

static void CcChargerAdd(uint32_t serialNr){
	int k = -1;																	// Position to add charger at, initialize to value indicating no suitable position found
	if(serialNr == Cluster.SerialNrThis){										// Serial number is for this charger ?
		if(Cluster.SerialNr[0] != serialNr){									// Serial number changed ?
			k = 0;																// Set position for this charger
		}
	}
	else{																		// Serial number is not for this charger
		int n;

		for(n = 1; n < CC_CHARGERS_LEN; n++){									// For all charger positions
			if(Cluster.SerialNr[n]){											// Position used ?
				if(Cluster.SerialNr[n] == serialNr){							// Already added ?
					return;														// Do nothing, idempotent function
				}
			}
			else{																// Position unused
				k = n;															// Set position to add charger to this, do NOT break in already somewhere in table, idempotent function
			}
		}
	}

	if(k >= 0){																	// Suitable position found, observe should not reach here if already in list
		Cluster.SerialNr[k] = serialNr;											// Add to list
		WatchDogUI[k] = WATCHDOG_TIME;											// Turn up watch dog
		WatchDogPStatus[k] = WATCHDOG_TIME;										// Turn up watch dog
		TurnOffCharger(k);
		Cluster.Meas[k].Meas.U = NAN;											// Initialize measured voltage to not a number
		Cluster.Meas[k].Meas.I = NAN;											// Initialize measured current to not a number
		Cluster.Meas[k].Meas.P = NAN;											// Initialize measured power to not a number
		Cluster.Meas[k].Status.Sum = 0;											// Initialize status to no status
		Cluster.Meas[k].Status.Detail.Derate.Detail.NoLoad = 1;						// #JJ fix for battery connected when not supposed to
	}
}

static void CcChargerRemove(uint32_t serialNr){
	int n;
	for(n = 0; n < CC_CHARGERS_LEN; n++){										// For all charger indexes
		if(Cluster.SerialNr[n] == serialNr){									// Serial number at this index
			Cluster.SerialNr[n] = 0;											// Remove charger from list
			TurnOffCharger(n);													// Turn off charger
		}
	}
}

static ReguValue_type NeededOutput(const ChargerSet_type* inSet, const ChargerMeas_type* measSum){
	ReguValue_type need = measSum->Meas;										// Assume needed output is measured output

	if(isnan(inSet->Set.I) || isnan(measSum->Meas.I)){							// No set reference value for current or measured current ?
		need.I = 0.0f;															// Assume zero current is needed
	}
	else if(inSet->Set.I < measSum->Meas.I){									// Set reference value for current and set reference value for current below measured current ?
		need.I = inSet->Set.I;													// Assume needed current equal to set reference value for current
	}
	if(inSet->Set.I > 0.0f && need.I < 0.0f){									// Set reference value for current above zero and need below small value
		need.I = 0.0f;															// Set reference value for current to small value
	}

	if(isnan(inSet->Set.P) || isnan(measSum->Meas.P)){							// No set reference value for power or measured power ?
		need.P = 0.0f;															// Assume zero power is needed
	}
	else if(inSet->Set.P < measSum->Meas.P){									// Set reference value for power and set reference value for power below measured power ?
		need.P = inSet->Set.P;													// Assume needed power equal to set reference value for power
	}
	if(inSet->Set.P > 0.0f && need.P < 0.0f){									// Set reference value for current above zero and need below small value
		need.P = 0.0f;															// Set reference value for current to small value
	}

	return need;																// Return assumed needed output
}

static float OptimalChargersToRun(const ReguValue_type* neededOutput){
	int deRated = 0;															// Counter for number of de-rated chargers
	float deRatedI = 0.0f;														// Sum de-rated current here
	float deRatedP = 0.0f;														// Sum de-rated power here

	{																			// Count de-rated chargers and sum de-rated outpput
		unsigned charger;														// Charger index

		for(charger = 0; charger < CC_CHARGERS_LEN; charger++){					// For all charger indexes
			if(Cluster.On[charger]){											// Charger not turned off ?
				if(Cluster.Meas[charger].Status.Detail.Derate.Sum){					// Charger de-rated ?
					deRated++;													// Count charger
					deRatedI += Cluster.Meas[charger].Meas.I;					// Sum de-rated current
					deRatedP += Cluster.Meas[charger].Meas.P;					// Sum de-rated power
				}
			}
		}
	}
	{
		float n;
		float N_Iset;															// Number of chargers in case current is used
		float N_Pset;															// Number of chargers in case power is used

		N_Iset = 1.25f*(neededOutput->I - deRatedI)/Cluster.IsetMax;			// Calculate optimal number of chargers in case current is used
		N_Pset = 1.25f*(neededOutput->P - deRatedP)/Cluster.PsetMax;			// Calculate optimal number of chargers in case power is used

		/* Max(N_Pset, N_Iset) */
		if(N_Pset > N_Iset)														// More chargers needed for power ?
			n = N_Pset;															// Use number of chargers for power
		else																	// More chargers need for current
			n = N_Iset;															// Use number of chargers for current

		return (float)deRated + n;												// Return optimal number of chargers to use
	}
}

static void TurnOffFaulty(){
	unsigned int k;
	for(k = 0; k < CC_CHARGERS_LEN; k++)										// For all chargers
	{
		if(!Cluster.SerialNr[k] || Cluster.Meas[k].Status.Detail.Error.Sum || !WatchDogUI[k] || !WatchDogPStatus[k]) // Charger not useful ? (It should not be necessary to check watch dog since the error flag should be set bet it is done anyway just in case)
		{
			TurnOffCharger(k);													// Turn off charger
		}
	}
}

uint16_t startChargers;
static void TurnOffOn(const ChargerSet_type* inSet, float optimal, const ChargerMeas_type* measSum){				/* Adjust on value between zero and 100 percent */
	/* Start optimal number of chargers. */
	/* In case of a step turn up as much as needed but not down. */

	/* If more than optimal chargers are started:
	 *   If in current control decrease current on non optimal chargers within one minute.
	 *   If in voltage control decrease voltage on optimal chargers within one one minute.
	 * If during turn off number of optimal chargers increase to at least number of running
	 * chargers withdraw and wait for a while, next time try another charger. */

	/* Leftover current should be distributed evenly between the chargers except if there is a charger
	 * decreasing current during turn off.
	 *
	 * Arithmetic average of set voltages should agree with set reference voltage. Observe if current is
	 * zero and voltage set below the limit there it start to deliver output this means set reference value
	 * for another voltage will increase and to high voltage may be delivered. If this is fixed by adjusting
	 * the measurements instead of the outputs the measurements are adjusted to show everything is correct
	 * instead of solving the problem.
	 */

	static int test = 0;
	static int Udelay = 0;
	static int Idelay = 0;
	{
		static int noLoadOld = 1;
		const int noLoad = measSum->Status.Detail.Derate.Detail.NoLoad;

		if(!noLoad && noLoadOld){												// Load connected
			optimal = (float)CC_CHARGERS_LEN;									// Start at full speed
			test = 5*60;														// Wait at least this time before turning off any charger
			Udelay = 0;
			Idelay = 0;
		}

		noLoadOld = noLoad;
	}
	const int start = (int)(optimal + 1.05f);									// Round upwards to closest integer
	startChargers = start;
	int NumberOfStartedChargers = countStartedChargers();						// Count number of started chargers
	int limited = 0;

	{
		static float Iold = 0.0f;
		static float Pold = 0.0f;

		if((inSet->Set.I > Iold || inSet->Set.P > Pold) && test < 30){			// Set reference value for current or power increase ?
			test = 30;															// Wait at least this time before turning off any charger
		}

		Iold = inSet->Set.I;													// Remember current until next time
		Pold = inSet->Set.P;													// Remember power until next time
	}

	{																			// Check output is limited by something else than current but not de-rated
		int charger;

		for(charger = 0; charger < CC_CHARGERS_LEN; charger++)					// For all charger positions
		{
			if(Cluster.On[charger]){											// Started ?
				if((Cluster.Meas[charger].Status.Detail.Info.Detail.VoltageIsNearSetValue || Cluster.Meas[charger].Status.Detail.Info.Detail.PowerIsNearSetValue)){
					limited = 1;												// Indicate charger is limited
				}
			}
		}
	}

	for(iterateReset(); iterate() && NumberOfStartedChargers < start;){			// For all chargers until optimal chargers are started
		const int charger = iteratePos();

		if(Cluster.SerialNr[charger] &&											// Charger at position and ?
		   !Cluster.Meas[charger].Status.Detail.Error.Sum &&							// useful and ?
		   !Cluster.On[charger]){												// not already used ?
			NumberOfStartedChargers++;											// Increase number of started chargers
			Cluster.On[charger] = 1;
			Cluster.LastStarted = charger;										// Remember which one started last
			test = 5*60;
			Udelay = 0;
			Idelay = 0;
		}
	}

	{
		if(NumberOfStartedChargers <= start || (test > 0 && test < 5*60 + 2)){	// Number of started chargers less than or equal to optimal
			if(test > 0){
				test--;
			}

			if(!Cluster.On[turnOffIndex]){
				setTurnOffIndex();
			}

			if(Udelay > 0){
				if(Idelay == 0 && Udelay == 1){
					setTurnOffIndex();
				}
				Udelay--;
				Usuppress = (float)Udelay*(1.0f/60.0f)*1.0f;
			}
			else{
				Usuppress = 0.0f;
			}

			if(Idelay > 0){
				if(Udelay == 1){
					setTurnOffIndex();
				}
				Idelay--;
				Isuppress = (float)Idelay*(1.0f/60.0f)*Cluster.IsetMax;
			}
			else{
				Isuppress = 0.0f;
				Iunsuppressed = Cluster.Meas[turnOffIndex].Meas.I;
			}
		}
		else if(Idelay < 60 && Cluster.Meas[turnOffIndex].Meas.I > 0.01f*Cluster.IsetMax){// More than optimal chargers started decrease current on non optimal chargers within one minute
			test = 5*60 + 2;
			if(limited && Udelay < 60){
				Udelay++;
			}
			else{
				Idelay++;																// Decrease delay
			}
			Isuppress = (float)Idelay*(1.0f/60.0f)*Cluster.IsetMax;
			Usuppress = (float)Udelay*(1.0f/60.0f)*1.0f;
		}
		else{																			// Number of started chargers larger than optimal and time out
			Usuppress = 0.0f;
			Isuppress = 0.0f;
			Udelay = 0;
			Idelay = 0;																	// wait 60 seconds before number of chargers is decreased again
			test = 0;
			TurnOffCharger(turnOffIndex);												// Stop charger
			setTurnOffIndex();
		}
	}
}

float compensate;
static void BalanceLoad(ReguValue_type inSet, const ChargerMeas_type* measSum){
	/* All chargers except one is run with a little to high voltage value so that voltage regulation is done at only one charger.
	 * Basic case is divide actual current between chargers and put the left over current at charger doing voltage regulation.
	 */
	float Uset = inSet.U;
	int balanceChargers = countStartedChargers();										// Check on how many chargers load should be balanced
	if(Isuppress > 0.0f){
		balanceChargers--;
	}

	{
		static float tune[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = 0.0f};		// Should be zero in average for balanced chargers
		static float Isum[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = 0.0f};		// Integrator
		static float KpOld[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = 0.0f};		// Integrator
		const float deltaMax = 0.05f*measSum->Meas.U;
		int charger;

		if(!isnan(deltaMax)){
			const float Kp = 4e-4*Cluster.IsetMax;
			const float Ki = 4e-5*Cluster.IsetMax;
			float ImeasAverage = 0.0f;

			for(charger = 0; charger < CC_CHARGERS_LEN; charger++){							// For all chargers
				if((measSum->Status.Detail.Info.Detail.VoltageIsNearSetValue ||				// Limited by voltage ? or
				   measSum->Status.Detail.Info.Detail.PowerIsNearSetValue) &&
				   Cluster.On[charger] &&
				   !isnan(Cluster.Meas[charger].Meas.I) &&
				   ISlackAllowed(charger)){													// Limited by power ? Only meaningful then limited by something else than set current
					ImeasAverage += Cluster.Meas[charger].Meas.I;							// Add measured currents
				}
			}
			ImeasAverage = ImeasAverage/balanceChargers;									// Average measured currents

			for(charger = 0; charger < CC_CHARGERS_LEN; charger++){							// For all chargers
				if((measSum->Status.Detail.Info.Detail.VoltageIsNearSetValue ||				// Limited by voltage ? or
				   measSum->Status.Detail.Info.Detail.PowerIsNearSetValue) &&
				   Cluster.On[charger] &&
				   !isnan(Cluster.Meas[charger].Meas.I) &&
				   ISlackAllowed(charger)){													// Limited by power ? Only meaningful then limited by something else than set current
					if((charger != turnOffIndex) && balanceChargers > 1){					// Balance ?
						float u;															// PI-regulator output
						const float e = ImeasAverage - Cluster.Meas[charger].Meas.I;
						const float u_prim = Kp*e + KpOld[charger] + Isum[charger];

						/* Saturate output */
						if(u_prim  < -deltaMax){
							u = -deltaMax;
						}
						else if(u_prim > deltaMax){
							u = deltaMax;
						}
						else{
							u = u_prim;
						}

						tune[charger] = u;													// Tune
						KpOld[charger] = u_prim - Kp*e;
						Isum[charger] = Ki*e + Ki*(u - u_prim);								// anti wind up
					}
				}
				tune[charger] = 0.0f;
			}

			if((measSum->Status.Detail.Info.Detail.VoltageIsNearSetValue ||						// Limited by voltage ? or
			   measSum->Status.Detail.Info.Detail.PowerIsNearSetValue) &&
			   countStartedChargers() > 0){
				const float UKp = 2e-3;
				const float UKi = 2e-4;
				static float Usum = 0.0f;													// Integrator
				static float KpOldU = 0.0f;													// Integrator

				float u;																	// PI-regulator output
				const float e = Uset - measSum->Meas.U;
				const float u_prim = UKp*e + KpOldU + Usum;

				if(0&&!isnan(e)){
					/* Saturate output */
					if(u_prim  < -4*deltaMax){
						u = -4*deltaMax;
					}
					else if(u_prim > 4*deltaMax){
						u = 4*deltaMax;
					}
					else{
						u = u_prim;
					}

					KpOldU = u_prim - UKp*e;
					Usum = UKi*e + UKi*(u - u_prim);										// anti wind up

					Uset += u;																// Note u is small which increase resolution for control loop
					compensate = u;
				}
				else{
					compensate = NAN;
				}
			}
		}

		float deltaURest = 0.0f;
		/* It is important the deltas for turn off do not move outside the area there it can change the output
		 * signal because otherwise the output may or actually will be to low or to high.
		 */

		float deltaUthis;
		if(balanceChargers > 0){
			deltaUthis = Usuppress/balanceChargers;
		}
		else{
			deltaUthis = 0.0f;
		}

		float Is[CC_CHARGERS_LEN] = {[0 ... CC_CHARGERS_LEN - 1] = 0.0f};

		for(charger = 0; charger < CC_CHARGERS_LEN; charger++){								// For all chargers
			if(Cluster.On[charger]){														// Started ?
				float Imax = Cluster.IsetMax;												// May be set per charger

				if(derateI[charger] < Imax){
					Imax = derateI[charger];												// Currently known limit
				}

				if(charger == turnOffIndex){
					Cluster.Set[charger].Set.U = Uset - Usuppress + tune[charger];

					if(Isuppress > 0.0f){													// I slack not allowed ?
						float deltaI = Iunsuppressed - Isuppress;
						if(deltaI < Imax){
						}
						else{
							deltaI = Imax;
						}

						if(deltaI < 0.0f){
							Is[charger] = 0.0f;
						}
						else if(inSet.I < deltaI){
							Is[charger] = inSet.I;
						}
						else{
							Is[charger] = deltaI;
						}
					}
					else if(Usuppress > 0.0f && Cluster.Meas[charger].Meas.I < Imax){
						Is[charger] = Cluster.Meas[charger].Meas.I;
					}
					else{
						Is[charger] = Imax;
					}
				}
				else{
					Is[charger] = Imax;

					if(notISetLimited(charger)){											// Not limited by set current
						const float deltaU = Uset + deltaUthis - (Cluster.Meas[charger].Meas.U + tune[charger]);
						if(deltaU < deltaUthis){
							deltaURest += deltaU;
							Cluster.Set[charger].Set.U = Uset + deltaUthis - deltaU + tune[charger];
						}
						else{
							deltaURest += deltaUthis;
							Cluster.Set[charger].Set.U = Uset + deltaUthis + tune[charger];
						}
					}
					else{
						Cluster.Set[charger].Set.U = Uset + deltaUthis + tune[charger];
					}
				}
			}
			else{
				TurnOffCharger(charger);													// Stop charger
			}
		}
		{
			float Idistribute;
			int cnt = 0;

			if(Isuppress > 0.0f){															// I slack not allowed ?
				Idistribute = inSet.I - Cluster.Set[turnOffIndex].Set.I;
			}
			else{
				Idistribute = inSet.I;
			}

			if(Idistribute < 0.0f){
				Idistribute = 0.0f;
			}

			float Iavailable = 0.0f;
			float IsumAll = 0.0f;
			for(charger = 0; charger < CC_CHARGERS_LEN; charger++){							// For all chargers
				if(Cluster.On[charger]){													// Started ?
					if(ISlackAllowed(charger)){
						cnt++;
						Iavailable += Is[charger];
					}
				}
			}

			if(Idistribute < Iavailable){
				const float percent = Idistribute/Iavailable;								// Percent of load on each charger

				for(charger = 0; charger < CC_CHARGERS_LEN; charger++){						// For all chargers
					if(Cluster.On[charger]){												// Started ?
						if(ISlackAllowed(charger)){
							Cluster.Set[charger].Set.I = percent*Is[charger];
						}
						else{
							Cluster.Set[charger].Set.I = Is[charger];
						}
						IsumAll += Cluster.Set[charger].Set.I;
					}
				}
			}
			else{
				float percent = 0.0f;

				if(cnt > 0){
					percent = (Idistribute - Iavailable)/(Cluster.IsetMax*cnt - Iavailable);	// Percent of load on each charger
				}

				if(percent < 0.0f){															// Below zero percent ?
					percent = 0.0f;															// Truncate to zero percent
				}
				else if(percent > 1.0f){													// Above 100 percent ?
					percent = 1.0f;															// Truncate to 100 percent
				}
				else{
					percent = 1.0f;															// Set to 100 percent
				}

				for(charger = 0; charger < CC_CHARGERS_LEN; charger++){						// For all chargers
					if(Cluster.On[charger]){												// Started ?
						if(ISlackAllowed(charger)){											// Slack allowed on this charger ?
							Cluster.Set[charger].Set.I = Is[charger] + percent*(Cluster.IsetMax - Is[charger]);
						}
						else{
							Cluster.Set[charger].Set.I = Is[charger];
						}
						IsumAll += Cluster.Set[charger].Set.I;
					}
				}
			}
			for(charger = 0; charger < CC_CHARGERS_LEN; charger++){						// For all chargers
				Cluster.Set[charger].Set.P = Cluster.Set[charger].Set.I/IsumAll*inSet.P;
			}
		}
		Cluster.Set[turnOffIndex].Set.U += deltaURest;
	}
}

uint16_t CcChargersUpdate(uint32_t* DisplaySerialNrs, int len){
	uint16_t status = 0;
	int n;

	/* Check list of enabled chargers in display against chargers in cluster control:
	 *   All enabled chargers should be in cluster control
	 *   All chargers in cluster control should be enabled */
	for(n = 0; n < len; n++){											// For all chargers in display
		CcChargerAdd(DisplaySerialNrs[n]);								// Add charger, will done only once
	}

	for (n = 0; n < CC_CHARGERS_LEN; n++){								// For all chargers in cluster control
		{
			int timeout = 0;

			if(WatchDogUI[n]){											// Watch dog turned up ?
				WatchDogUI[n]--;										// Decrease watch dog
			}
			else{
				timeout = 1;
			}

			if(WatchDogPStatus[n]){										// Watch dog turned up ?
				WatchDogPStatus[n]--;									// Decrease watch dog
			}
			else{
				timeout = 1;
			}

			if(timeout){												// Any of the watch dogs timed out ?
				Cluster.Meas[n].Status.Detail.Error.Detail.Watchdog = 1;		// Set error timed out to one
			}
			else{														// None of the watch dogs timed out
				Cluster.Meas[n].Status.Detail.Error.Detail.Watchdog = 0;		// Set error timed out to zero
			}
		}

		uint32_t ClusterSerialNr = Cluster.SerialNr[n];
		int found = 0;
		int k;
		for(k = 0; k < len; k++){										// For all serial number selected to be used in display
			if(ClusterSerialNr == DisplaySerialNrs[k]){					// Serial number in display ?
				if(!Cluster.Meas[n].Status.Detail.Error.Detail.Watchdog){
					status |= (1 << k);
				}
				found = 1;
			}
		}
		if(!found){														// Did not find serial number in display ?
			CcChargerRemove(ClusterSerialNr);							// Remove charger
		}
	}

	return status;
}

void CcMeasUI(uint32_t SerialNumber, float U, float I){
	const int pos = find(SerialNumber);									// Search for position of charger with serial number

	if(pos >= 0){														// Value indicate position have been found ?
		WatchDogUI[pos] = WATCHDOG_TIME;								// Turn up watch dog
		Cluster.Meas[pos].Meas.U = U;									// Update measured voltage
		Cluster.Meas[pos].Meas.I = I;									// Update measured current
	}
}

void CcMeasPStatus(uint32_t SerialNumber, float P, ChargerStatusFields_type Status){
	const int pos = find(SerialNumber);									// Search for position of charger with serial number

	if(pos >= 0){														// Value indicate position have been found ?
		WatchDogPStatus[pos] = WATCHDOG_TIME;							// Turn up watch dog
		Cluster.Meas[pos].Meas.P = P;									// Update measured power
		Cluster.Meas[pos].Status = Status;								// Update measured status
	}
}

int CcConnectedOk(uint32_t SerialNumber){
	const int pos = find(SerialNumber);									// Search for position of charger with serial number
	int OK = 0;

	if(pos >= 0){														// Value indicate position have been found ?
		if(!Cluster.Meas[pos].Status.Detail.Error.Sum){						// Charger OK ?
			OK = 1;
		}
	}

	return OK;
}

int CcGetEnginesOn(){
	int startedChargres = countStartedChargers();						// Get chargers with engine on

	return startedChargres;
}

uint32_t CcSet(int pos, volatile const ChargerSet_type** Set){
	*Set = &Cluster.Set[pos];											// Write pointer to set reference values to pointed to pointer
	return Cluster.SerialNr[pos];										// Return serial number for charger at position
}

volatile ChargerSet_type* CcSetThis(){
	return &Cluster.Set[0];												// Return set reference values for this charger
}

void CcSetSerialNumberThis(uint32_t SerialNr){
	Cluster.SerialNrThis = SerialNr;									// Set serial for this charger
}

uint32_t CcGetSerialNumberThis(){
	return Cluster.SerialNrThis;										// Return serial for this charger
}

ChargerMeas_type CcMeasGetThis(){
	return Cluster.Meas[0];												// Return measurements for this charger
}

void CcMeasSetThis(ChargerMeas_type* meas){
	WatchDogPStatus[0] = WATCHDOG_TIME;									// Turn up watch dog timer
	WatchDogUI[0] = WATCHDOG_TIME;										// Turn up watch dog timer
	Cluster.Meas[0] = *meas;											// Store values for local regulator at position zero
}

int CcGetSelectedChargers(void){
	int selectedChargers = countSelectedChargers();

	return selectedChargers;
}

static int countSelectedChargers(void){
	int chargers = 0;
	int n;

	for(n = 0; n < CC_CHARGERS_LEN; n++){								// For all charger positions
		if(Cluster.SerialNr[n] != 0){									// Charger at this position ?
			chargers++;													// count up number of chargers
		}
	}

	return chargers;													// Return number of chargers found (and selected)
}

static int find(uint32_t SerialNumber){
	int n;

	for(n = 0; n < CC_CHARGERS_LEN; n++){								// For all charger positions
		if(SerialNumber &&Cluster.SerialNr[n] == SerialNumber){			// Charger at this position ?
			return n;													// Return position
		}
	}

	return -1;															// Return value indicating not found
}

static unsigned iterateStart = 0;										// Iterate until this position
static unsigned iterateCnt = 0;											// Iterated to this position

static void iterateReset(void){
	iterateStart = Cluster.LastStarted;									// Until last started
	iterateCnt = 0;														// First iterated will be the one after this
}

static int iterate(void){
	int more;

	if(iterateCnt < CC_CHARGERS_LEN){									// One turn ?
		iterateCnt++;													// Just one turn
		more = 1;														// More positions
	}
	else{																// Not one turn
		more = 0;														// No more positions
	}

	return more;
}

static int iteratePos(void){
	return iterateCnt % CC_CHARGERS_LEN;								// Go around at overflow, reminder is position
}

static int countStartedChargers(void){
	int NumberOfStartedChargers = 0;									// Count chargers
	unsigned charger;

	for(charger = 0; charger < CC_CHARGERS_LEN; charger++)				// Check how many chargers are running
	{
		if(Cluster.On[charger]){										// Started ?
			NumberOfStartedChargers++;									// Count charger
		}
	}

	return NumberOfStartedChargers;										// Return counted number of started chargers
}

static void TurnOffCharger(int n){
	Cluster.On[n] = 0;													// Turn off
	Cluster.Set[n].Mode = ReguMode_Off;									// Turn off
	Cluster.Set[n].Set.U = NAN;											// Set reference value for voltage to not a number
	Cluster.Set[n].Set.I = NAN;											// Set reference value for current to not a number
	Cluster.Set[n].Set.P = NAN;											// Set reference value for power to not a number
}

static int ISlackAllowed(int charger){
	int slackAllowed = 0;

	if(charger == turnOffIndex && Isuppress > 0.0f) {

	} else {
		slackAllowed = 1;
	}

	return slackAllowed;
}

static int notISetLimited(int charger){
	return Cluster.Meas[charger].Status.Detail.Info.Detail.VoltageIsNearSetValue || Cluster.Meas[charger].Status.Detail.Info.Detail.PowerIsNearSetValue || Cluster.Meas[charger].Status.Detail.Derate.Sum;
}

static void setTurnOffIndex(void){
	for(iterateReset(); iterate();){									// For all chargers
		if(Cluster.On[iteratePos()]){									// Not turned off ?
			turnOffIndex = iteratePos();
			Iunsuppressed = Cluster.Meas[turnOffIndex].Meas.I;
			break;
		}
	}
}
