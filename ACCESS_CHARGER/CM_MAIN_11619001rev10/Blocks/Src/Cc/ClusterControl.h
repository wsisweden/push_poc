/*
 * ClusterControl.h
 *
 *  Created on: May 17, 2010
 *      Author: nicka
 */

#ifndef CLUSTERCONTROL_H_
#define CLUSTERCONTROL_H_

#include "cai_cc.h"

/* The purpose of cluster control is to control several chargers connected in parallel so that they behave as one single charger.
 * Note this software module only do the calculation of the values and do not send them anywhere.
 */

/* The internal functions OptimalChargersToRun(...) and SetValues(...) may be changed to run an optimal combination of chargers.
 * There should be no need to change the other functions for optimization purposes. Note however that the optimal will not be
 * used immediately because there would be transients and chargers are not allowed to switched off and on to often.
 */

#define CC_CHARGERS_LEN 6								// number of chargers
#define CHARGER_MEAS_DEFAULT {{0.0f, 0.0f, 0.0f}, {{.Derate.Detail.NoLoad=1}}}

/* Sum measurements and store result in structure out */
ChargerMeas_type CcMasterSum();

/* Calculate output values used by chargers. */
void CcMasterSet(const ChargerSet_type* inSet, const ChargerMeas_type* measSum, float IsetMax, float PsetMax);

/* Functions used to connect chargers to cluster. Note all chargers selected to be used should be in the cluster
 * regardless of their current state or if they could used or not for the proper status to be calculated correct.
 *
 * This list may be different from the list of nodes on CAN network and especially that a node may be here even
 * though it is not currently on the CAN network. This list should in other words only be changed then chargers
 * are selected or to be used or not be used. */
uint16_t CcChargersUpdate(uint32_t* DisplaySerialNrs, int len);				// Make list of chargers that may be used agree with internal list and calculate watch dog
void CcMeasUI(uint32_t SerialNumber, float U, float I);						// Write measurements from charger with serial number used as identification
void CcMeasPStatus(uint32_t SerialNumber, float P, ChargerStatusFields_type Status);		// Write measurements from charger with serial number used as identification
ChargerMeas_type CcMeasGetThis();											// Read measurement values for this charger
void CcMeasSetThis(ChargerMeas_type* meas);									// Write measurements for this charger
uint32_t CcSet(int pos, volatile const ChargerSet_type** Set);				// Read set values for charger at position
volatile ChargerSet_type* CcSetThis();										// Read set values for this charger
int CcConnectedOk(uint32_t SerialNumber);									// Charger may be used and is OK ?
int CcGetEnginesOn();														// Returns number of engines started
void CcSetSerialNumberThis(uint32_t SerialNr);
uint32_t CcGetSerialNumberThis();
int CcGetSelectedChargers(void);											// Return number of selected chargers
#endif /* CLUSTERCONTROL_H_ */
