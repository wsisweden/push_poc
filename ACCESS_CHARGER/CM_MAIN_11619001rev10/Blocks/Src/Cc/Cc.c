/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Cc.c
*
*	\ingroup	CC
*
*	\brief		Cluster control FB main source file.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	CC		CC
*
*	\brief		Cluster control
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "cai_cc.h"
#include "ClusterControl.h"
#include "engines_types.h"
#include "math.h"
#include "cc.h"
#include "can.h"
#include "regu.h"
#include "sup.h"
#include "dpl.h"
#include "init.h"
#include "../Can/Master/Master.h"
/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Instance type of the CC FB.
 */
 
typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	osa_TaskType			task;		/**< Task							*/
	cc_Init const_P * 		pInit;		/**< Initialization data			*/
	Boolean					test;		/**< Internal test flag				*/
	sys_FBInstId			instId;		/**< Instance ID of the FB			*/
} cc__Inst;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

static const char TaskCc_Name[] = "Cc";
cc__Inst*	Cc_pInst;
static osa_SemaType			taskSema;	/**< Task triggering semaphore.		*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/
PRIVATE OSA_TASK void Cc__Task(void); // charger control loop
static void UpdateChargersStatus();

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/
/* Allocation of masters in ChargersMeas.
 *   index 0 this charger
 *   index 1..5 chargers connected to CAN network
 */
volatile int ChargerSelected = TRUE;
volatile int TplateOperational = 0;
volatile ChargerSet_type CanToCc = {{0.0, NAN, 0.0}, 0, {{0}}, {{0}},0};
volatile ChargerMeas_type CcToCan = CHARGER_MEAS_DEFAULT;
volatile ChargerSet_type CanSet = {{0.0, NAN, 0.0}, 0, {{0}}, {{0}},0};
volatile ChargerMeas_type CanMeas = CHARGER_MEAS_DEFAULT;
/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid		An index (0..) that is accepted by various functions
*						to identify the instance. Should be kept for later use,
*						or may be discarded if not required by the FB.
*	\param		pArgs	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * cc_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	cc__Inst *				pInst;

	pInst = (cc__Inst *) mem_reserve(&mem_normal, sizeof(cc__Inst));

	osa_newTask(&pInst->task, TaskCc_Name, OSA_PRIORITY_LOW, Cc__Task, 2048);

	pInst->pInit = (cc_Init const_P *) pArgs;
	pInst->task.pUtil = pInst;
	pInst->test = TRUE;
	pInst->instId = iid;

	osa_newSemaTaken(&taskSema);

	return((void *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		instance	Pointer to instance.
* 	\param		pArgs		Pointer to the initialization arguments.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void cc_reset(
	void *					instance,
	void const_P *			pArgs
) {	
	DUMMY_VAR(pArgs);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		instance	Pointer to instance.
*	\param		nType		Down type.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void cc_down(
	void *					instance, 
	sys_DownType			nType
) {
	DUMMY_VAR(nType);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read
*				the value of a parameter owned by this FB.
*
*	\param		instance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue		Pointer to the storage for the value.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC sys_IndStatus cc_read(
	void *					instance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex,
	void *					pValue
) {	
	sys_IndStatus			retVal;

	DUMMY_VAR(aIndex);

	switch(rIndex)
	{
		case reg_indic_UsetP:
			*((Uint32 *)pValue) = CanSet.Set.U * 1000;
			retVal = REG_OK;
			break;

		case reg_indic_IsetP:
			*((Uint32 *)pValue) = CanSet.Set.I * 1000;
			retVal = REG_OK;
			break;

		case reg_indic_PsetP:
			*((Uint32 *)pValue) = CanSet.Set.P;
			retVal = REG_OK;
			break;

		case reg_indic_SocP:
			*((Uint8 *)pValue) = CanSet.soc;
			retVal = REG_OK;
			break;

		case reg_indic_UmeasP:
			*((Uint32 *)pValue) = CanMeas.Meas.U * 1000;
			retVal = REG_OK;
			break;

		case reg_indic_ImeasP:
			*((Uint32 *)pValue) = CanMeas.Meas.I * 1000;
			retVal = REG_OK;
			break;

		case reg_indic_PmeasP:
			*((Uint32 *)pValue) = CanMeas.Meas.P;
			retVal = REG_OK;
			break;

		case reg_indic_DerateP:
			*((Uint8 *)pValue) = CanMeas.Status.Detail.Derate.Sum;
			retVal = REG_OK;
			break;

		case reg_indic_WarningP:
			*((Uint8 *)pValue) = CanMeas.Status.Detail.Warning.Sum;
			retVal = REG_OK;
			break;

		case reg_indic_ErrorP:
			*((Uint8 *)pValue) = CanMeas.Status.Detail.Error.Sum;
			retVal = REG_OK;
			break;

		case reg_indic_DerateDisp:
			*((Uint8 *)pValue) = CanMeas.Status.Detail.Derate.Sum == 0 ? FALSE:TRUE;
			retVal = REG_OK;
			break;

		case reg_indic_WarningDisp:
			*((Uint8 *)pValue) = CanMeas.Status.Detail.Warning.Sum == 0 ? FALSE:TRUE;
			retVal = REG_OK;
			break;

		case reg_indic_ErrorDisp:
			*((Uint8 *)pValue) = CanMeas.Status.Detail.Error.Sum == 0 ? FALSE:TRUE;
			retVal = REG_OK;
			break;

		case reg_indic_RemoteP:
		{
			extern int gRemoteRestricted;

			*((Uint8 *)pValue) = (Uint8)gRemoteRestricted;
			retVal = REG_OK;
		}
			break;

		default:
			retVal = REG_UNAVAIL;
			break;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		instance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC sys_IndStatus cc_write(
	void *					instance, 
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex
) {
	
	DUMMY_VAR(aIndex);

	switch(rIndex)
	{
	case reg_indic_Sync :
		osa_semaSet(&taskSema);
		break;
	}

	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_ctrl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		instance	Pointer to the data of the instance.
*	\param		pCtrl		Pointer to the control indication.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void cc_ctrl(
	void *					instance,
	sys_CtrlType *			pCtrl
) {
	DUMMY_VAR(pCtrl);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_test
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Self test
*
*	\param		instance	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 cc_test(
	void *					instance
) {
	cc__Inst *			pInst;

	pInst = (cc__Inst *) instance;
	Cc_pInst = pInst;
	
	if (osa_syncPeek(project_stateMachSync))
	{
		/*
		 *	Only do test check if SUP's sync flag is set.
		 */

		if(pInst->test == FALSE)
		{
	//		project__errorHandler(PROJECT_ERROR_TEST);
		}

		pInst->test = FALSE;
	}
	
	return sys_msToTestTicks(5000);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cc_activate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\param		pInstance	Pointer to the FB instance.
*
*	\return		
*
*	\details
*
*	\note		Called each second by SUP when in cluster control mode.
*
*******************************************************************************/

PRIVATE OSA_TASK void Cc__Task(
	void
) {
	ChargerMeas_type measSum;					// #JJ

	/*
	 * Wait for REG to be ready. This includes waiting for FLMEM to be
	 * ready.
	 */
	deb_log("CC: Waiting for REG to be operational");
	osa_syncGet(OSA__SYNC_REG);
	TplateOperational = 1;

	{
		Uint32 SerialNr;
		reg_get(&SerialNr, Cc__SerialNo);
		CcSetSerialNumberThis(SerialNr);
	}

	measSum.Status.Sum = 0;						// #JJ fix for battery connected view at startup after power on
	measSum.Status.Detail.Derate.Detail.NoLoad = 1;	// #JJ
	reg_put(&measSum.Status.Sum, Cc_Status);	// #JJ

	FOREVER
	{
		Uint8 ParallelControl_Mode;

		{
			osa_Status timeout;

			osa_semaWait(&taskSema, 100, &timeout);				// Wait for synchronization which may be generated locally or from CAN
			if(timeout){
//				measSum.Status.Sum = 0;
//				measSum.Status.Derate.Detail.NoLoad = 1;	// #JJ fix for battery connected view displayed again after battery disconnected
//				reg_put(&measSum.Status.Sum, Cc_Status);
				continue;
			}
		}
		osa_taskWait(50);										// Measurement are done at synchronization wait for all measurements to be received.
																// It would be possible to abort the wait then all expected messages have been received
																// and/or update the sum each time a new messages arrive.

		reg_get(&ParallelControl_Mode, Cc_ParallelControl);

		switch(ParallelControl_Mode)
		{
		case 0 :												// Stand alone charger
			measSum	= CcMeasGetThis();							// Use measurements from this charger only
			ChargerSelected = TRUE;								// This charger should be used
			break;
		case 1 :												// Parallel control
			UpdateChargersStatus();								// Check list of chargers that should be used
			measSum	= CcMasterSum();							// Sum measurements from all useful chargers
			break;
		}
		reg_put(&measSum.Status.Sum, Cc_Status);
		CcToCan = measSum;

		ChargerSet_type Input;
		{
			Uint8 CAN_CommProfile;
			reg_get(&CAN_CommProfile, Cc_CAN_CommProfile);
			if(CAN_CommProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){							// Communication profile master external IO ?
				Input = CanToCc;
				reg_put(&Input.soc, Cc_SOC);					// write to SOC register
			}
			else{												// Use charging algorithm
				Interval_type UactOffInterval;
				Interval_type UactOnInterval;

				chalg_Chalg(measSum, &Input, &UactOffInterval, &UactOnInterval);	// run charging algorithm, calculate new reference values
				CanToCc = Input;

				regu_setUactDisconnect(&UactOffInterval);
				regu_setUactConnect(&UactOnInterval);

				{
					extern volatile int BatteryConnected;
					ChargerStatusFields_type status;
					status.Detail.Derate.Detail.NoLoad = !BatteryConnected;	// Trigger local sending of BMU communication
					if(cc_GetAddedConnected(CcGetSerialNumberThis())){
						battConnect(0, status);								// Call battConnect function to reset state when battery disconnected
					}
				}
			}
			CanSet = CanToCc;
		}

		switch(ParallelControl_Mode)
		{
		case 0 :																// Stand alone charger
			regu_setInput(&Input);
			break;
		case 1 :																// Parallel control
			{
				float IsetMax;
				float PsetMax;
				uint32_t IdcLimit;

				reg_get(&IdcLimit, Cc_IdcLimit);
				IsetMax = engineNominalCurrent();								// get maximum allowed current per engine for the currently measured voltage
				if((float)IdcLimit < IsetMax){
					IsetMax = (float)IdcLimit;
				}
				PsetMax = enginePowerMax();										// Get maximum allowed power per engine

				{																// Limit Pset value if parallel control and DPL function is enabled
					Uint8 powerGroupFunc = dplGetPowerGroupFunc();

					if(powerGroupFunc != DplDisabled){
						Uint32 PsetDpl = dplGetPset();
						if(PsetDpl < Input.Set.P){
							Input.Set.P = PsetDpl;
						}
					}
				}

				{
					Uint16 supStatusFlags;
					reg_get(&supStatusFlags, Cc__statusFlags);

					if(supStatusFlags & SUP_STATUS_TIMERESTRICT){
						Input.Set.I = 0;
					}
				}

				CcMasterSet(&Input, &measSum, IsetMax, PsetMax);				// Route signal from charging algorithm to all available chargers
			}
			Input = *CcSetThis();
			regu_setInput(&Input);
			break;
		}

		{
			Uint8 dummy;
			reg_put(&dummy, Cc_can_tpdoSet);
		}
	}
}

PUBLIC void cc_activate(

		void *					pInstance
) {
	cc__Inst *				pInst;

	pInst = Cc_pInst;

	pInst->test = TRUE; /* Reset internal test flag */
}

static void UpdateChargersStatus(){
	/* Check list of enabled chargers in display against chargers in cluster control:
	 *   All enabled chargers should be in cluster control but no others */
	Uint32 Chargers[16];
	Cc_GetEnabledChargers(Chargers, 16);
	uint16_t watchdogs = CcChargersUpdate(Chargers, 16);				// Tell cluster control which chargers should be enabled and check watch dog
	int masterSelected = FALSE;
	int n;
	for (n = 0; n < 16; n++){											// For all positions in list
		Uint8 status;

		if(Chargers[n]){												// Charger should be used if available ?
			if(watchdogs & (1 << n)){									// Connected ?
				status = ParallelStatus_Enum_AddedConnected;
				if(Chargers[n] == CcGetSerialNumberThis()){
					masterSelected = TRUE;
				}
			}
			else{														// Not connected
				status = ParallelStatus_Enum_AddedNotConnected;
			}
		}
		else{															// Charge not be used
			status = ParallelStatus_Enum_NotAddedConnected;
		}
		reg_aPut(&status, Cc_ParallelStatus, n);						// Write status
	}
	ChargerSelected = masterSelected;
}

void Cc_GetEnabledChargers(Uint32* Chargers, int len)
{
	int n;
	for (n = 0; n < len; n++){											// For all positions
		reg_aGet(&Chargers[n], Cc_ParallelAvail, n);					// Read serial number
	}
}

int CC_ChargerEnabled(Uint32 SerialNr){
	int n;
	for (n = 0; n < 16; n++){											// For all positions
		Uint32 Charger;
		reg_aGet(&Charger, Cc_ParallelAvail, n);						// Read serial number
		if(SerialNr == Charger){
			return 1;
		}
	}

	return 0;
}

int cc_GetAddedConnected(Uint32 SerialNr){								// Check if charger is enabled and connected
	Uint32 Chargers[16];
	Cc_GetEnabledChargers(Chargers, 16);
	int result = FALSE;

	int n;
	for (n = 0; n < 16; n++){											// For all positions in list
		Uint8 status;
		if(Chargers[n] == SerialNr){
			reg_aGet(&status, Cc_ParallelStatus, n);					// Get status
			if(status == ParallelStatus_Enum_AddedConnected){
				result = TRUE;
				break;
			}
		}
	}
	return result;
}
