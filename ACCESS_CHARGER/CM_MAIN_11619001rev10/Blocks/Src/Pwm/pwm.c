/* 30-10-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		pwm.c
*
*	\ingroup	PWM
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version	30-10-2009 / Antero Rintam�ki
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "_hw.h"

#include "pwm.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of PWM.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void pwm_init(void)
{
	SC->PCONP |= (1<<6); 						/* Enable power for PWM		*/
	SC->PCLKSEL0 &= ~(1<<13);		/* Use main clock 1/1		*/
	SC->PCLKSEL0 |= (1<<12);		/* Use main clock 1/1		*/
	PWM1PR = 0;
	PWM1MCR = (1<<1);

	atomic(PINCON->PINMODE3 |= (1<<9) | (0<<8););	/* Disable pull-up and pull-down resistors */
	atomic(PINCON->PINMODE7 |= (1<<19) | (0<<18););	/* Disable pull-up and pull-down resistors */
	atomic(PINCON->PINSEL3 |=
			(1<<5) |				/* P1.18							*/
			(1<<9) |				/* P1.20							*/
		   (1<<11) |				/* P1.21							*/
		   (1<<15) |				/* P1.23							*/
		   (1<<17) |				/* P1.24							*/
		   (1<<21););				/* P1.26							*/
	
	PWM1TCR = TCR_CNT_RESET | 1;					/* Reset Timer control register */
	PWM1CTCR = 0;
	
	PWM1MR0 = PWM_CYCLE_FULL;
	PWM1MR1	= 0;
	PWM1MR2 = 0; // TODO Regu (i ref, Charge Control)
	PWM1MR3 = 0;
	PWM1MR4 = 0;
	PWM1MR5 = 0;
	PWM1MR6 = 0;
	
	PWM1LER = LER0_EN | LER1_EN | LER2_EN | LER3_EN | LER4_EN | LER5_EN | LER6_EN; /* enable latch registers */
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__start
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts PWM.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void pwm__start(void)
{
	PWM1PCR = PWMENA1 | PWMENA2 | PWMENA3 | PWMENA4 | PWMENA5 | PWMENA6;
	PWM1TCR = TCR_CNT_EN | TCR_PWM_EN;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__stop
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Stops PWM.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void pwm__stop(void)
{
	PWM1PCR = 0;
	PWM1TCR = 0x00;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__getOutputCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns PWM-cycle for PWM-output.
*
*	\param		output		Number of specific PWM-output
*
*	\return		PWM-cyclevalue of requested PWM-output.
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC Uint32 pwm__getOutputCycle(Uint32 output)
{
	switch(output)
	{
		case PWM_OUTPUT_FAN:
			return PWM1MR1;

		case PWM_OUTPUT_CHARGE_CONTROL:
			return PWM1MR2;
			
		case PWM_OUTPUT_ALARM:
			return PWM1MR3;
			
		case PWM_OUTPUT_CHARG:
			return PWM1MR4;		
			
		case PWM_OUTPUT_CHARGRDY:
			return PWM1MR5;
			
		case PWM_OUTPUT_STANDBY:
			return PWM1MR6;
	}
	
	return 0;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__setOutputCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets PWM-cycle for PWM-output.
*
*	\param		output		Number of specific PWM-output.
*	\param		cycle		New PWM-cyclevalue
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void pwm__setOutputCycle(
	Uint32 				output, 
	Uint32 				cycle
) {	
	switch(output)
	{
		case PWM_OUTPUT_FAN:
			PWM1MR1 = cycle;
			PWM1LER |= LER1_EN;
			break;

		case PWM_OUTPUT_CHARGE_CONTROL:
			PWM1MR2 = cycle;
			PWM1LER |= LER2_EN;
			break;
			
		case PWM_OUTPUT_ALARM:
			PWM1MR3 = cycle;
			PWM1LER |= LER3_EN;
			break;
			
		case PWM_OUTPUT_CHARG:
			PWM1MR4 = cycle;
			PWM1LER |= LER4_EN;
			break;
			
		case PWM_OUTPUT_CHARGRDY:
			PWM1MR5 = cycle;
			PWM1LER |= LER5_EN;
			break;
			
		case PWM_OUTPUT_STANDBY:
			PWM1MR6 = cycle;
			PWM1LER |= LER6_EN;
			break;
	}
	
}
