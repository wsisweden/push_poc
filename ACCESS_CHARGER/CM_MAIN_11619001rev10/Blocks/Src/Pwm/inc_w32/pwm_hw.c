/* 30-10-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_w32/pwm_hw.c
*
*	\ingroup	PWM
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version	30-10-2009 / Antero Rintam�ki
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "_hw.h"

#include "pwm.h"
#include "HwSimu.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint32	PWM1MR2;
PRIVATE Uint32	PWM1MR3;
PRIVATE Uint32	PWM1MR4;
PRIVATE Uint32	PWM1MR5;
PRIVATE Uint32	PWM1MR6;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of PWM.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void pwm_init(void)
{
	PWM1MR2 = 0;
	PWM1MR3 = 0;
	PWM1MR4 = 0;
	PWM1MR5 = 0;
	PWM1MR6 = 0;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__start
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts PWM.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void pwm_start(void)
{

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__stop
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Stops PWM.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void pwm_stop(void)
{

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__getOutputCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns PWM-cycle for PWM-output.
*
*	\param		output		Number of specific PWM-output
*
*	\return		PWM-cyclevalue of requested PWM-output.
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC Uint32 pwm_getOutputCycle(Uint32 output)
{
	switch(output)
	{
		case PWM_OUTPUT_CHARGE_CONTROL:
			return PWM1MR2;
			
		case PWM_OUTPUT_ALARM:
			return PWM1MR3;
			
		case PWM_OUTPUT_CHARG:
			return PWM1MR4;		
			
		case PWM_OUTPUT_CHARGRDY:
			return PWM1MR5;
			
		case PWM_OUTPUT_STANDBY:
			return PWM1MR6;
	}
	
	return 0;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pwm__setOutputCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets PWM-cycle for PWM-output.
*
*	\param		output		Number of specific PWM-output.
*	\param		cycle		New PWM-cyclevalue
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void pwm_setOutputCycle(
	Uint32 				output, 
	Uint32 				cycle
) {	
	switch(output)
	{
		case PWM_OUTPUT_CHARGE_CONTROL:
			PWM1MR2 = cycle;
			break;
			
		case PWM_OUTPUT_ALARM:
			PWM1MR3 = cycle;
			hwsimu_setParameter(HWSIMU_P_ALARMLED, cycle);			
			break;
			
		case PWM_OUTPUT_CHARG:
			PWM1MR4 = cycle;
			hwsimu_setParameter(HWSIMU_P_CHARGINGLED, cycle);			
			break;
			
		case PWM_OUTPUT_CHARGRDY:
			PWM1MR5 = cycle;
			hwsimu_setParameter(HWSIMU_P_CHARGEREADYLED, cycle);			
			break;
			
		case PWM_OUTPUT_STANDBY:
			PWM1MR6 = cycle;
			hwsimu_setParameter(HWSIMU_P_STANDBYLED, cycle);			
			break;
	}
}
