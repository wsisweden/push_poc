/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		sup_log.c
*
*	\ingroup	SUP
*
*	\brief		Log handling
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>

#include "tools.h"
#include "sys.h"
#include "reg.h"
#include "deb.h"
#include "reg.h"

#include "rtc.h"
#include "Chalg.h"

#include "local.h"
//#include "Revision.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void	sup__logClear(sup__Inst *);
PRIVATE void	sup__logClearInst(sup__Inst *);

/* Historical log */
PRIVATE void	sup__logWriteHistorical(sup__Inst *,void *,Uint8);
PRIVATE void	sup__logHistStandard(sup__Inst *);

/* Event log */
PRIVATE void	sup__logWriteEvent(sup__Inst *,void *,Uint8,Uint8);
PRIVATE void	sup__logEventTimeSet(sup__Inst *);
//PRIVATE void	sup__logEventChargeStart(sup__Inst *);
//PRIVATE void	sup__logEventChargeEnd(sup__Inst *);
PRIVATE void	sup__logEventChalgError(sup__Inst *);
PRIVATE void	sup__logEventReguError(sup__Inst *);
PRIVATE void	sup__logEventCurveData(sup__Inst *);
//PRIVATE void	sup__logEventVcc(sup__Inst *);
PRIVATE void	sup__logEventVoltCal(sup__Inst *);
PRIVATE void	sup__logEventCurrCal(sup__Inst *);
//PRIVATE void	sup__logEventStartNet(sup__Inst *);
//PRIVATE void	sup__logEventJoinNet(sup__Inst *);
PRIVATE void	sup__logEventStartup(sup__Inst *);
//PRIVATE void	sup__logEventSettings(sup__Inst *);
PRIVATE void	sup__logEventAssert(sup__Inst *);
PRIVATE void	sup__logEventSupError(sup__Inst *);
PRIVATE void	sup__logEventTiltSense(sup__Inst *);
PRIVATE void	sup__logEventCableRiSlope(sup__Inst *);

/* Instant log */
PRIVATE void	sup__logInstantTStamp(sup__Inst *);
PRIVATE void	sup__logInstantStd(sup__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logProcessPending
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle pending log writes.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PUBLIC void sup__logProcessPending(
	sup__Inst *				pInst
) {

	DISABLE;
	if (!pInst->logData.logBuffLock && pInst->logData.logTodoFlags != 0)
	{
		pInst->logData.logBuffLock = TRUE;
		pInst->logData.timeout = 300;	// restart reset timer
		ENABLE;

		if (pInst->logData.logTodoFlags & SUP__LTODO_CLEARINST)
		{
			sup__logClearInst(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CLEARINST;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_TSTAMPINST)
		{
			sup__logInstantTStamp(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_TSTAMPINST;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_STDINST)
		{
			sup__logInstantStd(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_STDINST;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_TIMESET)
		{
			sup__logEventTimeSet(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_TIMESET;
		}
#if 0
		else if (pInst->logData.logTodoFlags & SUP__LTODO_CHARGESTART)
		{
			sup__logEventChargeStart(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CHARGESTART;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_CHARGEEND)
		{
			sup__logEventChargeEnd(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CHARGEEND;
		}
#endif
		else if (pInst->logData.logTodoFlags & SUP__LTODO_CHALGERROR)
		{
			sup__logEventChalgError(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CHALGERROR;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_REGUERROR)
		{
			sup__logEventReguError(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_REGUERROR;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_CURVEDATA)
		{
			sup__logEventCurveData(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CURVEDATA;
		}
#if 0
		else if (pInst->logData.logTodoFlags & SUP__LTODO_VCC)
		{
			sup__logEventVcc(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_VCC;
		}
#endif
		else if (pInst->logData.logTodoFlags & SUP__LTODO_VOLTCAL)
		{
			sup__logEventVoltCal(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_VOLTCAL;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_CURRCAL)
		{
			sup__logEventCurrCal(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CURRCAL;
		}
#if 0
		else if (pInst->logData.logTodoFlags & SUP__LTODO_STARTNET)
		{
			sup__logEventStartNet(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_STARTNET;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_JOINNET)
		{
			sup__logEventJoinNet(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_JOINNET;
		}
#endif
		else if (pInst->logData.logTodoFlags & SUP__LTODO_STARTUP)
		{
			sup__logEventStartup(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_STARTUP;
		}
#if 0
		else if (pInst->logData.logTodoFlags & SUP__LTODO_SETTINGS)
		{
			sup__logEventSettings(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_SETTINGS;
		}
#endif
		else if (pInst->logData.logTodoFlags & SUP__LTODO_EVTASSERT)
		{
			sup__logEventAssert(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_EVTASSERT;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_SUPERROR)
		{
			sup__logEventSupError(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_SUPERROR;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_TILTSENSE)
		{
			sup__logEventTiltSense(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_TILTSENSE;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_CABLERISLOPE)
		{
			sup__logEventCableRiSlope(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CABLERISLOPE;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_HISTORICAL)
		{
			sup__logHistStandard(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_HISTORICAL;
		}
		else if (pInst->logData.logTodoFlags & SUP__LTODO_CLEAR)
		{
			sup__logClear(pInst);
			pInst->logData.logTodoFlags &= ~SUP__LTODO_CLEAR;
		}
	}
	else
	{
		ENABLE;

		if(pInst->logData.logBuffLock){
			// count down reset counter
			if(pInst->logData.timeout){
				pInst->logData.timeout--;
			}
			else{
				// Reset logBuffLock flag
				atomic(
					pInst->logData.logBuffLock = FALSE;
				);
			}
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logReqDoneFn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate historical log write.
*
*	\param		pRequest	Pointer to the handled request.
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PRIVATE void sup__logReqDoneFn(
	cm_Request *			pRequest
) {
	sup__Inst *				pInst;

	pInst = (sup__Inst *) pRequest->pUtil;

	atomic(
		pInst->logData.logBuffLock = FALSE;
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logClear
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate all log data clearing.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logClear(
	sup__Inst *				pInst
) {

	/*
	 * Setup CM request and send the request to CM.
	 */

	pInst->logRequest.flags = CM_REQFLG_CLEAR;
	pInst->logRequest.logType = CM_LOGTYPE_HISTORICAL;
	pInst->logRequest.pDoneFn = &sup__logReqDoneFn;
	pInst->logRequest.pRecord = NULL;
	pInst->logRequest.pUtil = pInst;
	pInst->logRequest.size = 0;

	cm_processRequest(&pInst->logRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logClearInst
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate Instlog data clearing.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void sup__logClearInst(
	sup__Inst *				pInst
) {

	/*
	 * Setup CM request and send the request to CM.
	 */

	pInst->logRequest.flags = CM_REQFLG_CLEARINST;
	pInst->logRequest.logType = CM_LOGTYPE_INSTANT;
	pInst->logRequest.pDoneFn = &sup__logReqDoneFn;
	pInst->logRequest.pRecord = NULL;
	pInst->logRequest.pUtil = pInst;
	pInst->logRequest.size = 0;

	cm_processRequest(&pInst->logRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logInstantTStamp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate timestamp instant log record write.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PRIVATE void sup__logInstantTStamp(
	sup__Inst *				pInst
) {
	cm_InstLogTstamp *		pRecord;

	pRecord = (cm_InstLogTstamp *) pInst->logBuff;

	/*
	 * The record sync data is set so that CM identifies the write as a 
	 * timestamp write.
	 */

	pRecord->RecordSync = 0x80;	// binary 1000 0000
		
	/*
	 * Setup CM request and send the request to CM. Timestamp record contents
	 * is managed entirely by CM.
	 */

	pInst->logRequest.flags = CM_REQFLG_WRITE;
	pInst->logRequest.logType = CM_LOGTYPE_INSTANT;
	pInst->logRequest.pDoneFn = &sup__logReqDoneFn;
	pInst->logRequest.pRecord = pRecord;
	pInst->logRequest.pUtil = pInst;
	pInst->logRequest.size = sizeof(cm_InstLogTstamp);

	cm_processRequest(&pInst->logRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logInstantStd
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate standard instant log record write.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PRIVATE void sup__logInstantStd(
	sup__Inst *				pInst
) {
	cm_InstRecStd *			pRecord;

	pRecord = (cm_InstRecStd *) pInst->logBuff;

	pRecord->RecordSync = 0x7F;	// binary 0111 1111
	reg_get(&pRecord->ChalgError, sup__chalgError);
	{
		ChargerStatusFields_type Status;
		chargerAlarm_type ReguError;

		reg_get(&Status.Sum, sup__reguStatus);
		ReguError.Warning = Status.Detail.Warning;
		ReguError.Error = Status.Detail.Error;
		pRecord->ReguError = ReguError;
	}
	reg_get(&pRecord->Uchalg, sup__Uchalg);
	reg_get(&pRecord->Ichalg, sup__Ichalg);
	reg_get(&pRecord->Tboard, sup__Tboard);
	reg_get(&pRecord->Ths, sup__Ths);

	// Fill spare params with zero
	pRecord->u32spare1 = 0;

	/*
	 * Setup CM request and send the request to CM.
	 */

	pInst->logRequest.flags = CM_REQFLG_WRITE;
	pInst->logRequest.logType = CM_LOGTYPE_INSTANT;
	pInst->logRequest.pDoneFn = &sup__logReqDoneFn;
	pInst->logRequest.pRecord = pRecord;
	pInst->logRequest.pUtil = pInst;
	pInst->logRequest.size = sizeof(cm_InstRecStd);

	cm_processRequest(&pInst->logRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logHistStandard
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate standard historical log record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logHistStandard(
	sup__Inst *				pInst
) {
	cm_HistRecStd *			pRec;
	char					algNoName[CHALG_ALG_NAME_LEN_MAX + 1];
	Uint8					len;

	pRec = (cm_HistRecStd *) pInst->logBuff;

	/* store Start time of latest log for CmInfo */
	reg_putLocal(&pInst->startTime, reg_i_LatestHistLogTime, pInst->instId);

	/* Setup log record data */
	pRec->chargeStartTime = pInst->startTime;
	pRec->chargeEndTime = pInst->endTime;
	reg_get(&pRec->AlgNo,			sup__currAlgNo);
	reg_get(&pRec->Capacity,		sup__currCapacity);
	reg_get(&pRec->Cells,			sup__currCells);
	reg_get(&pRec->Ri,				sup__currCableRes);
	reg_get(&pRec->Baseload,		sup__currBaseLoad);
	reg_get(&pRec->ThsStart,		sup__startHSTemp);
	reg_get(&pRec->ThsEnd,			sup__endHSTemp);
	reg_get(&pRec->ThsMax,			sup__maxHSTemp);
	reg_get(&pRec->startVoltage,	sup__startVoltage);
	reg_get(&pRec->endVoltage,		sup__endVoltage);
	pRec->ChargeTime = pInst->chargeTime;
	reg_get(&pRec->chargedAh ,		sup__chargedAh);
	reg_get(&pRec->chargedWh,		sup__chargedWh);
	reg_get(&pRec->chargedAhP,		sup__chargedAhP);
	reg_get(&pRec->equTime,			sup__equTime);
	reg_get(&pRec->equAh,			sup__equAh);
	reg_get(&pRec->equWh,			sup__equWh);
	pRec->ChalgErrorSum = pInst->ChalgErrorSumLog;
	pRec->ReguErrorSum = pInst->ReguErrorSumLog;
	reg_getLocal(&pRec->EvtIdxStart, reg_i_EvtIdxStart, pInst->instId);
	reg_getLocal(&pRec->EvtIdxStop, reg_i_EvtIdxStop, pInst->instId);
	reg_get(&pRec->FID,				sup__FID);
	reg_get(&pRec->BID,				sup__BID);
	reg_get(&pRec->BSN,				sup__BSN);
	reg_get(&pRec->BMfgId,			sup__BMfgId);
	reg_get(&pRec->BatteryType,		sup__currBatteryType);
	reg_get(&pRec->ChargingMode,	sup__chargingMode);
	pRec->ChalgStatusSum = pInst->ChalgStatusSumLog;

	//reg_getLocal(&pRec->CMtype,	reg_i_EngineCode, pInst->instId);
	reg_getLocal(&pRec->CMtype,	reg_i_currEngineCode, pInst->instId);
	reg_getLocal(&pRec->SerialNo,	reg_i_SerialNo, pInst->instId);

	reg_get(algNoName,				sup__currAlgName);

	len = MIN(algNoName[0], 8); // Name is max 8 bytes.
	memcpy(pRec->AlgNoName, &algNoName[1], len);

	/*
	 * Insert null character at the end of the name if the length is less than
	 * 8 characters long.
	 */

	if (algNoName[0] < 8)
	{
		pRec->AlgNoName[(Uint8) algNoName[0]] = '\0';
	}

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u32spare2 = 0;
	pRec->u16spare1 = 0;
	pRec->u16spare2 = 0;
	pRec->u8spare1 = 0;
	pRec->u8spare2 = 0;

	sup__logWriteHistorical(pInst, pRec, CM_HISTREC_STANDARD);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logWriteHistorical
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate historical log record write.
*
*	\param		pInst		Pointer to SUP instance.
*	\param		pRecord		Pointer to the record filled with record data.
*	\param		type		Record type e.g. CM_HISTREC_STANDARD.
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PRIVATE void sup__logWriteHistorical(
	sup__Inst *				pInst,
	void *					pRecord,
	Uint8					type
) {
	cm_HistLogHdr *			pHeader;
	Uint32					logIndex;
	Uint32					logIndexReset;
	//Uint8					HeaderAndLogSizeDebug;
	//Uint8					HeaderSizeDebug;
	//Uint8					logSizeDebug;

	pHeader = (cm_HistLogHdr *) pRecord;

	reg_get(&logIndex, sup__HistLogIndex);
	if(logIndex != 0xFFFFFFFF){
		logIndex++;
	}

	reg_get(&logIndexReset, sup__HistLogIndexReset);
	if(logIndex != 0xFFFFFFFF){
		logIndexReset++;
	}

	/*
	 * Setup record header.
	 */

	deb_assert(type == CM_HISTREC_STANDARD);

	pHeader->ChargeIndex = logIndex;
	pHeader->ChargeIndexReset = logIndexReset;
	pHeader->RecordType = type;
	pHeader->RecordSize = (sizeof(cm_HistRecStd) - sizeof(cm_HistLogHdr));

	//HeaderAndLogSizeDebug = sizeof(cm_HistRecStd);
	//HeaderSizeDebug = sizeof(cm_HistLogHdr);
	//logSizeDebug = (HeaderAndLogSizeDebug - HeaderSizeDebug);
	/*
	 * Setup CM request and send the request to CM.
	 */

	pInst->logRequest.flags = CM_REQFLG_WRITE;
	pInst->logRequest.logType = CM_LOGTYPE_HISTORICAL;
	pInst->logRequest.pDoneFn = &sup__logReqDoneFn;
	pInst->logRequest.pRecord = pRecord;
	pInst->logRequest.pUtil = pInst;
	pInst->logRequest.size = sizeof(cm_HistRecStd);

	cm_processRequest(&pInst->logRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventTimeSet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate TimeSet event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note					
*
*******************************************************************************/

PRIVATE void sup__logEventTimeSet(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecTimeSet *			pRec;

	pRec = (cm_EvtRecTimeSet *) pInst->logBuff;

	pRec->OldTime = pInst->logData.oldTime;
	
	if (pInst->logData.flags & SUP__LFLAG_SMTIME)
	{
		pRec->Source = 1;
	}
	else
	{
		pRec->Source = 2;
	}

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecTimeSet) - sizeof(cm_EvtLogHdr);
	sup__logWriteEvent(pInst, pRec, CM_EVENT_TIMESET, DataSize);
}
#if 0
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventChargeStart
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate ChargeStarted event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note			
*
*******************************************************************************/

PRIVATE void sup__logEventChargeStart(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_HistLogHdr *			pRec;

	pRec = (cm_HistLogHdr *) pInst->logBuff;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecChargeStarted) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(
		pInst,
		pRec,
		CM_EVENT_CHARGESTARTED,
		//sizeof(cm_HistLogHdr)
		DataSize
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventChargeEnd
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate ChargeEnded event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventChargeEnd(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_HistLogHdr *			pRec;

	pRec = (cm_HistLogHdr *) pInst->logBuff;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecChargeEnded) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_CHARGEENDED, DataSize /*sizeof(cm_HistLogHdr)*/);
}
#endif
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventChalgError
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate ChalgError event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventChalgError(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecChalgError *	pRec;
	Uint16					ChalgErrorNew;
	static Uint16			ChalgErrorOld = 0;

	pRec = (cm_EvtRecChalgError *) pInst->logBuff;

	// Event log should only contain the latest error
	reg_get(&ChalgErrorNew, sup__chalgError);
	pRec->ChalgError = 0;
	pRec->Active = 0;

	for(int i = 0; i < 16; i++)
	{
		Uint16 ErrorNew = (ChalgErrorNew & (1 << i));
		Uint16 ErrorOld = (ChalgErrorOld & (1 << i));

		if(ErrorNew != ErrorOld)
		{
			pRec->ChalgError |= (ErrorNew | ErrorOld);
			pRec->Active |= ErrorNew;
		}
	}

	ChalgErrorOld = ChalgErrorNew;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecChalgError) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_CHALGERROR, DataSize);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventReguError
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate ReguError event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventReguError(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecReguError *	pRec;
	chargerAlarm_type		ReguErrorNew;

	static chargerAlarm_type	ReguErrorOld = {.Sum = 0};

	pRec = (cm_EvtRecReguError *) pInst->logBuff;

	// Event log should only contain the latest error
	{
		ChargerStatusFields_type Status;

		reg_get(&Status.Sum, sup__reguStatus);
		ReguErrorNew.Warning = Status.Detail.Warning;
		ReguErrorNew.Error = Status.Detail.Error;
	}
	pRec->ReguError = 0;
	pRec->Active = 0;

	for(int i = 0; i < 16; i++)
	{
		Uint16 ErrorNew = (ReguErrorNew.Sum & (1 << i));
		Uint16 ErrorOld = (ReguErrorOld.Sum & (1 << i));

		if(ErrorNew != ErrorOld)
		{
			pRec->ReguError |= (ErrorNew | ErrorOld);
			pRec->Active |= ErrorNew;
		}
	}


	{
		Uint8 reguErrorCnt;
		reg_get(&reguErrorCnt, sup__reguErrorCntP);
		if(reguErrorCnt){
			pRec->ReguError |= 1<<1;	// Regulator error
			pRec->Active |= 1<<1;	// Regulator error
		}
	}

	ReguErrorOld.Sum = ReguErrorNew.Sum;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecReguError) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_REGUERROR, DataSize);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventCurveData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate CurveData event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventCurveData(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecCurveData *	pRec;
	chalg_BattInfoType		battData;


	pRec = (cm_EvtRecCurveData *) pInst->logBuff;

	reg_get(&battData, sup__batteryInfo);

	pRec->AlgNo_new = battData.algNo;
	pRec->Capacity_new = battData.capacity;
	pRec->Cells_new = battData.cell;
	pRec->Ri_new = battData.cableRes;
	pRec->Baseload_new = battData.baseLoad;

	reg_get(&battData, sup__oldBatteryInfo);

	pRec->AlgNo_old = battData.algNo;
	pRec->Capacity_old = battData.capacity;
	pRec->Cells_old = battData.cell;
	pRec->Ri_old = battData.cableRes;
	pRec->Baseload_old = battData.baseLoad;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecCurveData) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(
		pInst,
		pRec,
		CM_EVENT_CURVEDATA,
		DataSize
	);
}
#if 0
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventVcc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate Vcc event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventVcc(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecVcc *			pRec;
	Uint8					mains;
	Uint16					chalgStatus;
	Uint8					VccStatusNew;
	static Uint8			VccStatusOld = 0;

	pRec = (cm_EvtRecVcc *) pInst->logBuff;

	// Get new Vcc status
	reg_get(&mains, sup__mains);
	reg_get(&chalgStatus, sup__chalgStatus);
	pRec->VccStatus = VccStatusOld;
	VccStatusNew = 0;

	if (mains)
	{
		VccStatusNew |= (1<<0);
	}

	if (chalgStatus & CHALG_STAT_BATTRY_CONN)
	{
		VccStatusNew |= (1<<1);
	}

	for(int i = 0; i < 8; i++)
	{
		Uint8 StatusNew = (VccStatusNew & (1 << i));
		Uint8 StatusOld = (VccStatusOld & (1 << i));

		if(StatusNew != StatusOld)
		{
			pRec->VccStatus = (StatusNew | StatusOld);
			pRec->Active = StatusNew;
		}
	}

	VccStatusOld = VccStatusNew;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecVcc) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_VCC, DataSize);
}
#endif
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventVoltCal
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate VoltageCalibration event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventVoltCal(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecVolCalib *		pRec;

	pRec = (cm_EvtRecVolCalib *) pInst->logBuff;

	reg_get(&pRec->UslopeNew, sup__UadSlope);
	reg_get(&pRec->UslopeOld, sup__logOldUadSlope);

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecVolCalib) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(
		pInst,
		pRec,
		CM_EVENT_VOLTAGECALIBRATION,
		DataSize
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventCurrCal
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate CurrentCalibration event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventCurrCal(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecCurrCalib *	pRec;

	pRec = (cm_EvtRecCurrCalib *) pInst->logBuff;

	reg_get(&pRec->IslopeNew, sup__IadSlope);
	reg_get(&pRec->IslopeOld, sup__logOldIadSlope);

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecCurrCalib) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(
		pInst,
		pRec,
		CM_EVENT_CURRENTCALIBRATION,
		DataSize
	);
}
#if 0
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventStartNet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate StartNetwork event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/
PRIVATE void sup__logEventStartNet(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecStartNet *		pRec;

	pRec = (cm_EvtRecStartNet *) pInst->logBuff;

	reg_get(&pRec->Channel, sup__radioChannel);
	reg_get(&pRec->NwkID, sup__radioNwkId);
	reg_get(&pRec->PanID, sup__radioPanId);

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecStartNet) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(
		pInst,
		pRec,
		CM_EVENT_STARTNETWORK,
		DataSize
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventJoinNet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate JoinNetwork event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventJoinNet(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecJoinNet *		pRec;

	pRec = (cm_EvtRecJoinNet *) pInst->logBuff;

	reg_get(&pRec->Channel, sup__radioChannel);
	reg_get(&pRec->NwkID, sup__radioNwkId);
	reg_get(&pRec->PanID, sup__radioPanId);

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecJoinNet) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(
		pInst,
		pRec,
		CM_EVENT_JOINNETWORK,
		DataSize
	);
}
#endif
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventStartup
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate StartUp event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logEventStartup(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecStartUp *			pRec;

	pRec = (cm_EvtRecStartUp *) pInst->logBuff;

	reg_getLocal(&pRec->MainFwType, reg_i_FirmwareTypeMain, pInst->instId);
	reg_getLocal(&pRec->MainFwVer, reg_i_FirmwareVerMain, pInst->instId);
//	pRec->MainFwVer = SVN_REVISION_NUMBER;
	reg_get(&pRec->RadioFwType, sup__FirmwareTypeRF);
	reg_get(&pRec->RadioFwVer, sup__FirmwareVerRF);

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecStartUp) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_STARTUP, DataSize);
}
#if 0
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventSettings
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate Settings event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/
PRIVATE void sup__logEventSettings(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	//cm_EvtRecSettings *	pRec;

	//pRec = (cm_EvtRecSettings *) pInst->logBuff;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecSettings) - sizeof(cm_EvtLogHdr);

	deb_assert(FALSE);

	/* todo:	Contents of settings event record?
	 */
}
#endif

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventAssert
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Assert has occured.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void sup__logEventAssert(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecAssert *			pRec;

	pRec = (cm_EvtRecAssert *) pInst->logBuff;

	// Get Id of assert
	reg_getLocal(&pRec->AssertId, reg_i_EvtAssert, pInst->instId);

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecAssert) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_ASSERT, DataSize);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventSupError
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate SupError event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void sup__logEventSupError(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	Uint16					statusFlags;
	Uint16					statusFlagsOld;
	cm_EvtRecSupError *	pRec;

	pRec = (cm_EvtRecSupError *) pInst->logBuff;

	statusFlags = pInst->statusFlags & SUP_EVENT_MASK;
	statusFlagsOld = pInst->statusFlagsOld & SUP_EVENT_MASK;

	// Event log should only contain the latest error
	pRec->SupError = 0;
	pRec->Active = 0;

	for(int i = 0; i < 16; i++)
	{
		Uint16 ErrorNew = (statusFlags & (1 << i));
		Uint16 ErrorOld = (statusFlagsOld & (1 << i));

		if(ErrorNew != ErrorOld)
		{
			pRec->SupError |= (ErrorNew | ErrorOld);
			pRec->Active |= ErrorNew;
		}
	}

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecSupError) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_SUPERROR, DataSize);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventTiltSense
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate ChalgError event with tilt sense error only.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void sup__logEventTiltSense(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecChalgError *	pRec;
	Uint16					TiltSense;
	Uint16					ChalgErrorNew = 0;
	static Uint16			ChalgErrorOld = 0;

	pRec = (cm_EvtRecChalgError *) pInst->logBuff;
	pRec->ChalgError = 0;
	pRec->Active = 0;

	// Event log should only contain the latest status
	reg_get(&TiltSense, sup__tiltSense);
	if(TiltSense){
		ChalgErrorNew = CHALG_ERR_INCORRECT_MOUNT;
	}

	{
		Uint16 ErrorNew = (ChalgErrorNew & CHALG_ERR_INCORRECT_MOUNT);
		Uint16 ErrorOld = (ChalgErrorOld & CHALG_ERR_INCORRECT_MOUNT);

		if(ErrorNew != ErrorOld)
		{
			pRec->ChalgError |= (ErrorNew | ErrorOld);
			pRec->Active |= ErrorNew;
		}
	}

	ChalgErrorOld = ChalgErrorNew;

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecChalgError) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(pInst, pRec, CM_EVENT_CHALGERROR, DataSize);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logEventCableRiSlope
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate CableRiSlope event record write.
*
*	\param		pInst		Ptr to SUP instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void sup__logEventCableRiSlope(
	sup__Inst *				pInst
) {
	Uint8					DataSize;
	cm_EvtRecCableRiSlope *		pRec;

	pRec = (cm_EvtRecCableRiSlope *) pInst->logBuff;

	reg_get(&pRec->RiSlopeNew, sup__RiSlope);
	reg_get(&pRec->RiSlopeOld, sup__RiSlopeOld);

	// Fill spare params with zero
	pRec->u32spare1 = 0;
	pRec->u16spare1 = 0;
	pRec->u8spare1 = 0;

	DataSize = sizeof(cm_EvtRecCableRiSlope) - sizeof(cm_EvtLogHdr);

	sup__logWriteEvent(
		pInst,
		pRec,
		CM_EVENT_CABLERISLOPE,
		DataSize
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__logWriteEvent
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initiate event log record write.
*
*	\param		pInst		Pointer to SUP instance.
*	\param		pRecord		Pointer to the record filled with record data.
*	\param		type		Record type.
*	\param		size		Record size.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE void sup__logWriteEvent(
	sup__Inst *				pInst,
	void *					pRecord,
	Uint8					type,
	Uint8					size
) {
	cm_EvtLogHdr *			pHeader;
	Uint32					logIndex;
	Uint32					logIndexReset;
	Posix_Time				logTime;

	pHeader = (cm_EvtLogHdr *) pRecord;

	reg_get(&logIndex, sup__EventLogIndex);
	if(logIndex != 0xFFFFFFFF){
		logIndex++;
	}

	reg_get(&logIndexReset, sup__EventLogIndexReset);
	if(logIndex != 0xFFFFFFFF){
		logIndexReset++;
	}

	/*
	 * Store a timestamp containing the time the log was written.
	 */

	rtc_hwGetAsPosix(&logTime);
	reg_putLocal(&logTime, reg_i_LatestEvtLogTime, pInst->instId);

	/*
	 * Setup record header.
	 */

	pHeader->Index = logIndex;
	pHeader->IndexReset = logIndexReset;
	rtc_hwGetAsPosix(&pHeader->Time);
	pHeader->EventId = type;
	pHeader->DataSize = size;

	/*
	 * Setup CM request and send the request to CM.
	 */

	pInst->logRequest.flags = CM_REQFLG_WRITE;
	pInst->logRequest.logType = CM_LOGTYPE_EVENT;
	pInst->logRequest.pDoneFn = &sup__logReqDoneFn;
	pInst->logRequest.pRecord = pRecord;
	pInst->logRequest.pUtil = pInst;
	pInst->logRequest.size = (sizeof(cm_EvtLogHdr) + size);

	cm_processRequest(&pInst->logRequest);

}
