/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		rtc.c
*
*	\ingroup	SUP_ARM7
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	09-12-2009 / Antero Rintam�ki
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "_hw.h"
#include "rtc.h"

#include "osa.h"
#include "deb.h"

SYS_DEFINE_FILE_NAME;

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define CCR_CLKEN			(1<<0)			/** Clock enable				*/
#define CCR_RST				(1<<1)			/** CTC reset					*/

#define RTC_CTR1_MASK		0x0FFF0F1F		/** Valid bits in CTR1			*/
#define RTC_GP_FEED			0x5BBDF7EA		/** Fill value for rtc state detect*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of RTC(Real Time Clock). 
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void rtc_hwInit(
	void
) {
	SC->PCONP |= (1<<9);

	rtc_hwDisableIrq();

	if (RTC->RTC_AUX & (1<<4))
	{
		//RTC_Time			time;

		RTC_AMR = 0;
		RTC_CIIR = 0;
		RTC_CCR = 0;
		RTC_CAL = 0;

		//time.tm_year = 100;
		//time.tm_mon = 0;
		//time.tm_mday = 1;
		//time.tm_hour = 0;
		//time.tm_min = 0;
		//time.tm_sec = 0;

		//time.tm_wday = 0;
		//time.tm_yday = 0;

		//rtc_hwSet(&time);

		RTC->RTC_AUX |= (1<<4);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwSetCalibration
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the calibration value of the RTC oscillator.
*
*	\param		rtcCalib	RTC calibration value including the direction bit.
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void rtc_hwSetCalibration(
	Uint32					rtcCalib
) {
	if (rtcCalib != RTC->CALIBRATION)
	{
		if (rtcCalib == 0)
		{
			RTC->CCR |= (1<<4);
		}
		else
		{
			RTC->CCR &= ~(1<<4);
		}

		RTC->CALIBRATION = rtcCalib;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwStart
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts RTC
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void rtc_hwStart(
	void
) {
	RTC_CCR |= CCR_CLKEN;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwStop
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts RTC
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void rtc_hwStop(
	void
) {
	RTC_CCR &= ~CCR_CLKEN;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwEnableIrq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enables rtc 1s interrupt.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void rtc_hwEnableIrq(
	void
) {
#if 1
	RTC_CIIR = (1<<0); /* Isr on 1 sec */
#else
	RTC_CIIR = 0;//(1<<0); 
	RTC_AMR = 0xFE;

	{
		Uint32 tmp;

		tmp = RTC_SEC + 1;

		if (tmp >= 60)
		{
			tmp -= 60;
		}

		RTC_ALSEC = tmp;
	}
#endif

	hw_enableIrq(RTC_IRQn);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwDisableIrq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Disables rtc 1s interrupt.
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void rtc_hwDisableIrq(
	void
) {
	hw_disableIrq(RTC_IRQn);
	RTC_CIIR = 0; 
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwSet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets the date and time to RTC-registers.
*
*	\param		pTime	pointer to RTC_Time-structure that includes
*						time-attributes.
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/

PUBLIC void rtc_hwSet(
	RTC_Time *				pTime
) {
	Uint32					running;

	running = RTC_CCR & CCR_CLKEN;
	RTC_CCR &= ~CCR_CLKEN;

	RTC_SEC = pTime->tm_sec;
    RTC->MIN = pTime->tm_min;
    RTC_HOUR = pTime->tm_hour;
    RTC_DOM = pTime->tm_mday;
    RTC_DOW = pTime->tm_wday;
    RTC_DOY = pTime->tm_yday;
    RTC_MONTH = pTime->tm_mon + 1;
	RTC_YEAR = pTime->tm_year + 1900;

	RTC_CCR |= running;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwSetTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets the time (no date) to RTC-registers.
*
*	\param		pTime	pointer to RTC_Time-structure that includes time-attributes
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/

PUBLIC void rtc_hwSetTime(
	RTC_Time *				pTime
) {
	Uint32					running;

	running = RTC_CCR & CCR_CLKEN;
	RTC_CCR &= ~CCR_CLKEN;

	RTC_SEC = pTime->tm_sec;
    RTC->MIN = pTime->tm_min;
    RTC_HOUR = pTime->tm_hour;

	RTC_CCR |= running;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwSetDate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets the date (no time) to RTC-registers.
*
*	\param		pTime	pointer to RTC_Time-structure that includes
*						time-attributes.
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/

PUBLIC void rtc_hwSetDate(
	RTC_Time *				pTime
) {
	Uint32					running;

	running = RTC_CCR & CCR_CLKEN;
	RTC_CCR &= ~CCR_CLKEN;

    RTC_DOM = pTime->tm_mday;
    RTC_DOW = pTime->tm_wday;
    RTC_DOY = pTime->tm_yday;
    RTC_MONTH = pTime->tm_mon + 1;
	RTC_YEAR = pTime->tm_year + 1900;

	RTC_CCR |= running;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwGet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the time.
*
*	\param		pTime		Pointer to RTC_Time -structure. Function will modify 
*							given parameter to match current time at RTC.
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/

PUBLIC void rtc_hwGet(
	RTC_Time *					pTime
) {
#if 0
	Uint32						ctr0;
	Uint32						ctr1;
	Uint32						ctr1_dbl;
	Uint32						ctr2;
	Uint8						cnt = 3;

	/*
	 *	Keep reading until day, month and year do not changed between two reads.
	 *	This is to prevent register value mismatch in case day changes between
	 *	reading ctr0 and ctr1.
	 */
	do 
	{
		ctr1 = RTC_CTIME1 & RTC_CTR1_MASK;
		ctr0 = RTC_CTIME0;
		ctr2 = RTC_CTIME2;
		ctr1_dbl = RTC_CTIME1 & RTC_CTR1_MASK;
	} 
	while ((ctr1 != ctr1_dbl) && cnt--);


	pTime->tm_sec = (Uint16)(ctr0 & 0x003F);
	pTime->tm_min = (Uint16)((ctr0 >> 8) & 0x003F);
	pTime->tm_hour = (Uint16)((ctr0 >> 16) & 0x001F);
	pTime->tm_wday = (Uint16)((ctr0 >> 24) & 0x0007);
	pTime->tm_mday = (Uint16)(ctr1 & 0x001F);
	pTime->tm_mon = (Uint16)((ctr1 >> 8) & 0x000F) - 1;
	pTime->tm_year = (Uint16)((ctr1 >> 16) & 0x0FFF) - 1900; 
	pTime->tm_yday = (Uint16)(ctr2 & 0x0FFF);
#else

	pTime->tm_sec = RTC_SEC;
	pTime->tm_min = RTC->MIN;
	pTime->tm_hour = RTC_HOUR;
	pTime->tm_wday = RTC_DOW;
	pTime->tm_mday = RTC_DOM;
	pTime->tm_mon = RTC_MONTH - 1;
	pTime->tm_year = RTC_YEAR - 1900; 
	pTime->tm_yday = RTC_DOY;

#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	RTC_IRQHandler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		
*
*	\return	
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PUBLIC osa_isrOSFn(NULL, RTC_IRQHandler)
{
	osa_isrEnter();

#if 0 //#ifndef NDEBUG
	{
		static Boolean foo = TRUE;

		if (foo)
		{
			FIO2SET = (1<<11);
		}
		else
		{
			FIO2CLR = (1<<11);
		}

		foo = !foo;
	}
#endif
	
	RTC_ILR = 3;
	
	osa_isrLeave();
}
osa_endOSIsr
