/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		rtc.c
*
*	\ingroup	SUP_ARM7
*
*	\brief		
*
*	\details	
*
*	\par		
*
*	\par		
*
*	\note		
*
*	\version	02-11-2009 / Antero Rintam�ki
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "lpc23xx.h"
#include "rtc.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__rtcInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of RTC(Real Time Clock). 
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__rtcInit(void)
{
	PCONP |= (1<<9);
	RTC_CCR = 0;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__rtcStart
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts RTC
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__rtcStart(void)
{
	RTC_CCR |= CCR_CLKEN;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__rtcStop
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Stops RTC. 
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__rtcStop(void)
{
	RTC_CCR &= ~CCR_CLKEN;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__rtcSetTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets the time to RTC-registers.
*
*	\param		t		pointer to tm-structure that includes time-attributes
*
*	\return		-
*
*	\details	-
*
*	\note		for tm-structure see time.h
*
*******************************************************************************/

PUBLIC void tui__rtcSetTime(
	struct tm *				t
) {
	RTC_SEC = t->tm_sec;
    RTC_MIN = t->tm_min;
    RTC_HOUR = t->tm_hour;
    RTC_DOM = t->tm_mday;
    RTC_DOW = t->tm_wday;
    RTC_DOY = t->tm_yday;
    RTC_MONTH = t->tm_mon;
    RTC_YEAR = t->tm_year;    
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__rtcSetGMT
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets the GMT. GMT value is stored at "RTC general purpose
*				register 0".
*
*	\param		timeShift	GMT-shift value to be stored at RTC. Timeshift-value
*							is used later on to evaluate local time for specific
*							timezone.
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/

PUBLIC void tui__rtcSetGMT(
	Uint32 timeShift
) {
	RTC_GPREG0 = timeShift;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__rtcGetRawTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the time without GMT-shift.
*
*	\param		tim		Pointer to time_t -structure. Function will modify given
*						parameter to match current time at RTC.
*
*	\return		-
*
*	\details	-
*
*	\note		for time_t -structure see time.h
*
*******************************************************************************/

PUBLIC void tui__rtcGetRawTime(
	struct tm *				tim
) {
	struct tm				timRaw;
	struct tm *				timRaw_p = &timRaw;
	
	timRaw_p->tm_sec = RTC_SEC;
	timRaw_p->tm_min = RTC_MIN;
	timRaw_p->tm_hour = RTC_HOUR;
	timRaw_p->tm_mday = RTC_DOM;
	timRaw_p->tm_wday = RTC_DOW;
	timRaw_p->tm_yday = RTC_DOY;
	timRaw_p->tm_mon = RTC_MONTH;
	timRaw_p->tm_year = RTC_YEAR;
	
	time_t t = mktime(timRaw_p);
	localtime_r(&t, tim);	
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__rtcGetLocalTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the time with GMT-shift.
*
*	\param		tim		Pointer to time_t -structure. Function will modify given parameter
*						to match current time at RTC.
*
*	\return		-
*
*	\details	-
*
*	\note		for time_t -structure see time.h
*
*******************************************************************************/

PUBLIC void tui__rtcGetLocalTime(
	struct tm *				tim
) {
	struct tm				timRaw;
	struct tm *				timRaw_p = &timRaw;
	
	// localtime
	timRaw_p->tm_sec = RTC_SEC;
	timRaw_p->tm_min = RTC_MIN;
	timRaw_p->tm_hour = RTC_HOUR;
	timRaw_p->tm_mday = RTC_DOM;
	timRaw_p->tm_wday = RTC_DOW;
	timRaw_p->tm_yday = RTC_DOY;
	timRaw_p->tm_mon = RTC_MONTH;
	timRaw_p->tm_year = RTC_YEAR;
	
	// add timeshift to localtime 
	time_t t = mktime(timRaw_p);
	t+= RTC_GPREG0*3600;
	localtime_r(&t, tim);
}
