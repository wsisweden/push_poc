/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SUP_ARM7
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	02-11-2009 / Antero Rintam�ki
*
*******************************************************************************/

#ifndef RTC_H_INCLUDED
#define RTC_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "time.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

// RTC general purpose registers
#define RTC_GPREG0        (*(volatile unsigned int *)(RTC_BASE_ADDR + 0x44))
// #define RTC_GPREG1        (*(volatile unsigned int *)(RTC_BASE_ADDR + 0x48))
// #define RTC_GPREG2        (*(volatile unsigned int *)(RTC_BASE_ADDR + 0x4C))
// #define RTC_GPREG3        (*(volatile unsigned int *)(RTC_BASE_ADDR + 0x50))
// #define RTC_GPREG4        (*(volatile unsigned int *)(RTC_BASE_ADDR + 0x54))

// RTC clock registers
#define CCR_CLKEN	0x01 // enable

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void tui__rtcInit(void);
extern void tui__rtcStart(void);
extern void tui__rtcStop(void);

extern void tui__rtcSetTime(struct tm *t);
extern void tui__rtcSetGMT(Uint32 timeShift);
extern void tui__rtcGetRawTime(struct tm *tim);
extern void tui__rtcGetLocalTime(struct tm *tim);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */
#endif
