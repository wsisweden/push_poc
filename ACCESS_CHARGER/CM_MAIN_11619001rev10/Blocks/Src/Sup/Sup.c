/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Sup.c
*
*	\ingroup	SUP
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	\$Rev: 1668 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	SUP		SUP
*
*	\brief		Main state machine
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* Standard library */
#include <string.h>

/* T-Plat.E */
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "deb.h"
#include "rtc.h"
#include "nxp.h"

#ifndef NDEBUG
#include "_HW.H"
#endif

#include "global.h"

/* Micropower */
#include "Meas.h"
#include "chksum.h"
#include "Sup.h"
#include "engines_types.h"
#include "gui.h"
#include "CAN.h"
#include "regu.h"

/* Local */
#include "init.h"
#include "local.h"
//#include "Revision.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Task execution triggers
 */

/*@{*/
#define SUP__TRG_MAINS		(1<<0)		/**< Mains input has changed.		*/
#define SUP__TRG_CLEARSTAT	(1<<1)		/**< Clear all statistics			*/
#define SUP__TRG_STATUSREP	(1<<2)		/**< Status reoported				*/
#define SUP__TRG_DATE		(1<<3)		/**< Date register written			*/
#define SUP__TRG_TIME		(1<<4)		/**< Time register written			*/
#define SUP__TRG_REMOTEIN	(1<<5)		/**< Remote in input has changed	*/
#define SUP__TRG_POSIXTIME	(1<<6)		/**< setRTC has been written		*/
#define SUP__TRG_CHALGERR	(1<<7)		/**< Chalg error reg has changed	*/
#define SUP__TRG_REGUERR	(1<<8)		/**< Regu error reg has changed		*/
#define SUP__TRG_UADSLOPE	(1<<9)		/**< UadSlope reg has changed		*/
#define SUP__TRG_IADSLOPE	(1<<10)		/**< IadSlope reg has changed		*/
#define SUP__TRG_RADIOFW	(1<<11)		/**< Radio FW ver reg has changed	*/
#define SUP__TRG_BATTINFO	(1<<12)		/**< Battery info has changed		*/
#define SUP__TRG_INSTLOGINT	(1<<13)		/**< Instant log interval has changed */
#define SUP__TRG_REMOTEOUT	(1<<14)		/**< Update remote out pending		*/
#define SUP__TRG_FACTORYDEF	(1<<15)		/**< Reset to factory defaults		*/
#define SUP__TRG_F1_FUNC	(1<<16)		/**< F1 button function has changed	*/
#define SUP__TRG_F2_FUNC	(1<<17)		/**< F2 button function has changed	*/
#define SUP__TRG_WATER_ACT	(1<<18)		/**< Water active REG changed		*/
#define SUP__TRG_AIRPUMP_ACT (1<<19)	/**< AirPump active REG changed		*/
#define SUP__TRG_EQUAL_ACT	(1<<20)		/**< Equalize active REG changed 	*/
#define SUP__TRG_ENGINEINDEX (1<<21)	/**< EngineIndex has changed		*/
#define SUP__TRG_FACTORYSET	(1<<22)		/**< Set factory defaults			*/
#define SUP__TRG_EQUAL_VAR (1<<23)		/**< Equalize_var has changed		*/
#define SUP__TRG_RTCCALIB	(1<<24)		/**< Set RTC calibration value		*/
#define SUP__TRG_REMOTEOUT_MAN	(1<<25)	/**< Manual remote out has changed	*/
#define SUP__TRG_EVTASSERT	(1<<26)		/**< Assert has occurred			*/
#define SUP__TRG_ECFUNC		(1<<27)		/**< Extra charge function has changed */
#define SUP__TRG_ECDAY		(1<<28)		/**< New day set for extra charge	*/
#define SUP__TRG_ECTIME		(1<<29)		/**< New time set for extra charge	*/
#define SUP__TRG_TIMER		(1<<30)		/**< Timer has expired				*/
#define SUP__TRG_EQUAL_FUNC (1<<31)		/**< Equalize function has changed	*/

/* trigger2 definition */
#define SUP__TRG2_CANMODE	(1<<0)		/**< CAN profile has changed		*/
#define SUP__TRG2_VSENSE	(1<<1)		/**< Trigger for volt sense delta measurement	*/
#define SUP__TRG2_LANGUAGE	(1<<2)		/**< Trigger for language change	*/
#define SUP__TRG2_TILTSENSE	(1<<3)		/**< Trigger for Tilt sensor		*/
#define SUP__TRG2_CRISLOPE	(1<<4)		/**< Cable Resistance slope raise	*/
#define SUP__TRG2_WATER_MAN	(1<<5)		/**< Manual Water REG changed		*/
#define SUP__TRG2_FW_ERROR	(1<<6)		/**< Trigger for firmware malfunction	*/
#define SUP__TRG2_CHARGEMODE	(1<<7)	/**< ChargingMode has changed		*/

/*@}*/

/**
 * Battery detection level
 */
/*@{*/
#define SUP_BATTERY_CONNECTED		6500	// Battery connected detection level (mV)
#define SUP_BATTERY_DISCONNECTED	6000	// Battery connected detection level (mV)
/*@}*/

/* Address conflict timeout (seconds) */
#define SUP__ADDRESS_CONFLICT (3*60)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#define sup__enterSleepState(pInst_)									\
{																		\
	if ((pInst_)->state == SUP_STATE_CHARGING)	{						\
																		\
	}																	\
	(pInst_)->oldState = oldState;										\
	sys_downAll(SYS_DOWN_POWER_SAVE);									\
	(pInst_)->pInit->pSleepFn((pInst_)->pInit->measInstId);				\
	sup__enterWakeupState((pInst_));									\
}

#define sup__enterWakeupState(pInst_)									\
{																		\
	sys_resetAll();														\
	if ((pInst_)->oldState == SUP_STATE_CHARGING) {					\
		sup__enterState((pInst_), SUP_STATE_CHARGING);					\
	} else {															\
		sup__enterState((pInst_), SUP_STATE_INIT_BM);					\
	}																	\
}

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE OSA_TASK void		sup_task(void);
PRIVATE void sup__timer(swtimer_Timer_st * pTimer);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG
Uint32 sup_counter = 0;
tstamp_Stamp sup_lastRun;
#endif
volatile Uint32 ResetTimer = 0;	/**< 1s counter from software reset	*/
volatile int SupBatteryConnected = 0;	/**< Battery connection flag for modes with no normal battery detection */

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/**
 * Pointer to the only instance of this function block.
 */

PRIVATE sup__Inst *			sup__pInst = NULL;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid		An index (0..) that is accepted by various functions
*						to identify the instance. Should be kept for later use,
*						or may be discarded if not required by the FB.
*	\param		pArgs	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * sup_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	sup__Inst *				pInst;

	pInst = (sup__Inst *) mem_reserve(&mem_normal, sizeof(sup__Inst));

	osa_newTask(
		&pInst->task,
		"sup_task",
		OSA_PRIORITY_NORMAL,
		sup_task,
		1536
	);

	pInst->syncFlag = osa_newSyncFlag();
	pInst->pInit = (sup_Init const_P *) pArgs;
	pInst->task.pUtil = pInst;
	pInst->state = SUP_STATE_STARTUP;
	pInst->instId = iid;
	pInst->triggers = SUP__TRG_RTCCALIB;
	pInst->triggers2 = SUP__TRG2_CANMODE;
	pInst->timeout = 0;
	pInst->statusFlags = 0;
	pInst->endRestrict = 0;
	pInst->delayCtr = 0;
	pInst->test = TRUE;
	pInst->startTime = 0;
	pInst->logData.logTodoFlags = 0;
	pInst->logData.logBuffLock = FALSE;	// jakob 100929
	pInst->instLogCnt = 0;
	pInst->InstLogSPOld = 0;
	pInst->manualStart = FALSE;
	pInst->CAN_Mode = 0;
	pInst->disabled = FALSE;
	pInst->flags = 0;
	osa_newSemaTaken(&pInst->factorySema);

	swtimer_new(&pInst->timer, sup__timer, 1000, 1000);
	pInst->timer.pUtil = pInst;

	chksum_setArea(&pInst->chkdata, pInst->pInit->pFlash);
	chksum_checkReset(&pInst->chkdata);

	sys_initHeader(&pInst->header);

	rtc_hwInit();
	rtc_hwStart();

	sys_mainInitCount(1);

	sup__pInst = pInst;

	return((void *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		pInstance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus sup_write(
	void *					pInstance, 
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex
) {
	sup__Inst *				pInst;
	Uint32					newTriggers;
	Uint32					newTriggers2;

	pInst = (sup__Inst *) pInstance;	

	DUMMY_VAR(aIndex);
	newTriggers = 0;
	newTriggers2 = 0;

	switch(rIndex)
	{
		case reg_indic_ChalgError:
			newTriggers = SUP__TRG_CHALGERR | SUP__TRG_REMOTEOUT;
			break;

		case reg_indic_batteryInfo:
			newTriggers = SUP__TRG_BATTINFO;
			break;

		case reg_indic_BBC:
			newTriggers = SUP__TRG_REMOTEOUT;
			break;

		case reg_indic_mains:
			newTriggers = SUP__TRG_MAINS;
			break;

		case reg_indic_ButtonF1_func:
			newTriggers = SUP__TRG_F1_FUNC;
			break;

		case reg_indic_ButtonF2_func:
			newTriggers = SUP__TRG_F2_FUNC;
			break;

		case reg_indic_RemoteIn_func:
		case reg_indic_RemoteIn:
		case reg_indic_ExtraIn_func:
		case reg_indic_ExtraIn:
		case reg_indic_CanInput:
		case reg_indic_CanInputMode:
			newTriggers = SUP__TRG_REMOTEIN;
			break;

		case reg_indic_UadSlope:
			newTriggers = SUP__TRG_UADSLOPE;
			break;

		case reg_indic_IadSlope:
			newTriggers = SUP__TRG_IADSLOPE;
			break;

		case reg_indic_FirmwareVerRF:
			newTriggers = SUP__TRG_RADIOFW;
			break;

		case reg_indic_ReguStatus:
			newTriggers = SUP__TRG_REGUERR | SUP__TRG_REMOTEOUT;
			break;

		case reg_indic_ReguErrorEvent:
			newTriggers = SUP__TRG_REGUERR;
			break;

		case reg_indic_setRTC:
			newTriggers = SUP__TRG_POSIXTIME;
			break;

		case reg_indic_date_write:
			newTriggers = SUP__TRG_DATE;
			break;

		case reg_indic_time_write:
			newTriggers = SUP__TRG_TIME;
			break;

		case reg_indic_RemoteOut_func:
		case reg_indic_RemoteOutAlarm_var:
		case reg_indic_RemoteOutPhase_var:
		case reg_indic_RemoteOutBBC_var:
		//case reg_indic_Ithreshold_var: 	// sorabITR - not now
		//case reg_indic_Ithreshold_cond:	// sorabITR - not now
			newTriggers = SUP__TRG_REMOTEOUT;
			break;

		case reg_indic_RemoteOut_man:
			newTriggers = SUP__TRG_REMOTEOUT_MAN | SUP__TRG_REMOTEOUT;
			break;

		case reg_indic_InstLogSamplePeriod:
			newTriggers = SUP__TRG_INSTLOGINT;
			break;

		case reg_indic_FactoryDefaults:
			newTriggers = SUP__TRG_FACTORYDEF;
			break;

		case reg_indic_FactorySet:
			newTriggers = SUP__TRG_FACTORYSET;
			break;

		case reg_indic_ClearStatistics:
			newTriggers = SUP__TRG_CLEARSTAT; 
			break;

		case reg_indic_Water_on:
			newTriggers = SUP__TRG_REMOTEOUT;
			break;

		case reg_indic_Water_active:
			newTriggers = SUP__TRG_WATER_ACT;
			break;

		case reg_indic_AirPump_on:
			newTriggers = SUP__TRG_REMOTEOUT;
			break;

		case reg_indic_AirPump_active:
			newTriggers = SUP__TRG_AIRPUMP_ACT;
			break;

		case reg_indic_Equalize_func:
			newTriggers = SUP__TRG_EQUAL_FUNC;
			break;

		case reg_indic_Equalize_var:
			newTriggers = SUP__TRG_EQUAL_VAR;
			break;

		case reg_indic_rtcCalib:
			newTriggers = SUP__TRG_RTCCALIB;
			break;

		case reg_indic_Equalize_active:
			newTriggers = SUP__TRG_EQUAL_ACT;
			break;

		case reg_indic_EvtAssert:
			newTriggers = SUP__TRG_EVTASSERT;
			break;

		case reg_indic_ExtraCharge_func:
			newTriggers = SUP__TRG_ECFUNC;
			break;

		case reg_indic_ExtraCharge_var:
			newTriggers = SUP__TRG_ECDAY;
			break;

		case reg_indic_ECTime_write:
			newTriggers = SUP__TRG_ECTIME;
			break;

		/* OBS!! Trigger2 below here */
		case reg_indic_CAN_CommProfile:
			newTriggers2 = SUP__TRG2_CANMODE;
			break;

		case reg_indic_Language:
			newTriggers2 = SUP__TRG2_LANGUAGE;
			break;

		case reg_indic_TiltSense:
			newTriggers2 = SUP__TRG2_TILTSENSE;
			break;

		case reg_indic_RiSlope:
			newTriggers2 = SUP__TRG2_CRISLOPE;
			break;

		case reg_indic_Water_man:
			newTriggers2 = SUP__TRG2_WATER_MAN;
			break;

		case reg_indic_currChargingMode:
			newTriggers2 = SUP__TRG2_CHARGEMODE;
			break;
	}

	if (newTriggers || newTriggers2)
	{
		pInst->triggers |= newTriggers;
		pInst->triggers2 |= newTriggers2;
		osa_signalSend(&pInst->task);
	}

	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read
*				the value of a parameter owned by this FB.
*
*	\param		pInstance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue		Pointer to the storage for the value.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus sup_read(
	void *					pInstance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex,
	void *					pValue
) {	
	sup__Inst *				pInst;
	sys_IndStatus			retVal = REG_OK;

	pInst = (sup__Inst *) pInstance;

	DUMMY_VAR(aIndex);

	switch(rIndex)
	{

		case reg_indic_FirmwareTypeMain:
			*(Uint32 *)pValue = *(Uint32*)pInst->pInit->pFirmType;
			break;

		case reg_indic_FirmwareVerMain:
			*(Uint32 *)pValue = *(Uint32*)pInst->pInit->pFirmVer;
			//*(Uint32 *)pValue = (Uint32)SVN_REVISION_NUMBER;
			break;

		case reg_indic_ChecksumMain:
		{
#if TARGET_SELECTED & TARGET_W32
			*(Uint32 *)pValue = 122436;
#else
			*(Uint32 *)pValue = *(Uint32 *)PROJECT_CHKSUM_ADDR;
#endif
			break;
		}

		case reg_indic_supervisionState:
			*(Uint8 *)pValue = pInst->state;
			break;

		case reg_indic_statusFlags:
			*(Uint16 *)pValue = pInst->statusFlags;
			break;

		case reg_indic_restrictEnd:
			*(Uint32 *)pValue = pInst->endRestrict;
			break;

		case reg_indic_chargeStartDate:
		case reg_indic_chargeStartTime:
			*(Uint32 *)pValue = pInst->startTime;
			break;

		case reg_indic_chargeEndTime:
			*(Uint32 *)pValue = pInst->endTime;
			break;

		case reg_indic_chargeTime:
			*(Uint32 *)pValue = pInst->chargeTime;
			break;

		case reg_indic_LocalTime:
		case reg_indic_date_read:
		case reg_indic_time_read:
		{
			rtc_hwGetAsPosix(pValue);
			break;
		}

		case reg_indic_ECTime_read:
		{
			// Convert hour and minutes to seconds (for posix comparison)
			*(Uint32 *) pValue = (Uint32)pInst->ExtraChargeHour * 3600UL + (Uint32)pInst->ExtraChargeMin * 60UL;
			break;
		}

		case reg_indic_ExtraCharge_active:
		{
			// Convert hour and minutes to seconds (for posix comparison)
			*(Uint8 *) pValue = (Uint8)(pInst->statusFlags & SUP_STATUS_EXTRACHARGE) ? TRUE:FALSE;
			break;
		}

		case reg_indic_restrictEndWday:
			*(Uint8 *)pValue = pInst->endRestrictWday;
			break;

		case reg_indic_ChargedWhTotalP:
		{
			// Convert Wh to kWh
			Uint32 ChargedWhTotal;
			reg_getLocal(&ChargedWhTotal, reg_i_ChargedWhTotal, pInst->instId);

			*(Uint32 *) pValue = (Uint32)(ChargedWhTotal/1000);
			break;
		}

		case reg_indic_batteryTemp:
		{
			Int16		temp;
			/*
			 *	Read battery temp sensor value if in User def. mode. Else read the
			 *	BM battery temperature (BM/Dual/Multi mode).
			 */
			if (pInst->chargingMode == CHALG_USER_DEF)
			{
				/* Use Robust external battery temperature. */
				reg_get(&temp, sup__TbattExt);
			}
			else
			{
				/* Use BM battery temperature. */
				reg_get(&temp, sup__Tbatt);
			}
			*((Int16 *) pValue) = temp;
			break;
		}

		default:
			retVal = REG_UNAVAIL;
			break;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_msg
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Called when an event has been received.
*
*	\param		pInstance		Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void sup_msg(
	void *	pInstance
) {
    sup__Inst * pInst;

	pInst = (sup__Inst *) pInstance;

	DISABLE;
	if (pInst->disabled == TRUE)
	{
		ENABLE;

		FOREVER
		{
			msg_Ptr messageHandle;

			messageHandle = msg_receive(pInst->instId);
			if (messageHandle == NULL)
			{
				break;
			}
			msg_unlock(messageHandle);
		}
	}
	else
	{
		pInst->triggers |= SUP__TRG_STATUSREP;
		ENABLE;
		osa_signalSend(&pInst->task);
	}


}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		pInstance	Pointer to instance.
* 	\param		pArgs		Pointer to the initialization arguments.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void sup_reset(
	void *					pInstance,
	void const_P *			pArgs
) {	
	sup__Inst *				pInst;

	pInst = (sup__Inst *) pInstance;

	DUMMY_VAR(pArgs);

	atomic(
		pInst->flags &= ~SUP_FLG_DOWN;
	);

	osa_signalSend(&pInst->task);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		pInstance	Pointer to instance.
*	\param		nType		Down type.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void Sup_down(void * pInstance, sys_DownType nType) {
	sup__Inst *				pInst;

	pInst = (sup__Inst *) pInstance;

	DUMMY_VAR(nType);

	atomic(
		pInst->flags |= SUP_FLG_DOWN;
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_test
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Self test
*
*	\param		instance	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 sup_test(
	void *					instance
) {
	sup__Inst *				pInst;

	pInst = (sup__Inst *) instance;

	if (
		osa_syncPeek(project_stateMachSync) &&
		not(pInst->statusFlags & SUP_STATUS_CORRUPTED) &&
		not(pInst->flags & SUP_FLG_DOWN) &&
		not(pInst->disabled == TRUE)
	) {
		/*
		 *	Only do test check if sup's sync flag is set.
		 */
		if(pInst->test == FALSE)
		{
			project__errorHandler(PROJECT_ERROR_TEST);
		}

		pInst->test = FALSE;
	}
	else
	{
		/* todo: some check sup is not stuck? */
	}

#if PROJECT_USE_WDOG == 1
	/*
	 *	Kick the watchdog.
	 */
	nxp_wdogKick();
#endif

	return sys_msToTestTicks(1000);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_ctrl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Control interface.
*
*	\param		pInstance	Pointer to GUI instance.
*	\param		nCtrl		Control request type.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void sup_ctrl(
	void *					pInstance,
	sys_CtrlType *			pCtrl
) {
	sup__Inst *				pInst;

	pInst = (sup__Inst *) pInstance;

	/*
	 *	0x80 = request to enter test mode.
	 */

	if (pCtrl->cmd == 0x80)
	{
		atomic(
			pInst->disabled = TRUE;
			pInst->state = SUP_STATE_IDLE;
		);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_task
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE OSA_TASK void sup_task(
	void
) {
	sup__Inst *				pInst;

	pInst = (sup__Inst *) osa_taskCurrent()->pUtil;

#if TARGET_SELECTED & TARGET_CM3
	if((FIO1PIN & PROJECT_PIN_ALL_DEFINED) == PROJECT_PIN_CLEAR_ALL_NVM )
	{
		memdrv_Alloc request;
		void * pDrvInst;

		/*
		 * F1 and F2 button pressed. Erase flash chip.
		 */
		
		request.op = MEMDRV_OP_ERASE_CHIP;
		request.fnDone = NULL;

		pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
		pInst->pInit->drvIf->doOp(pDrvInst, &request);

		sup__storeFactoryDefaults(pInst);	// Store default parameters to default..

		FOREVER
		{
			osa_yield();
			/* Just do nothing to indicate that the erase has been started. */
		}
	}
#endif

	/* 
	 * Wait for REG to be ready. This includes waiting for FLMEM to be
	 * ready.
	 */
	
	deb_log("SUP: Waiting for REG to be operational");
	osa_syncGet(OSA__SYNC_REG);

#if TARGET_SELECTED & TARGET_CM3
	if((FIO1PIN & PROJECT_PIN_ALL_DEFINED) == PROJECT_PIN_RESET_DEFAULTS)
	{
		sup__resetToFactoryDefaults(pInst);
	}
#endif

	sys_mainInitReady();				/* Tell SYS that init completed		*/
	
	{
		/* Handle Hardfault reset */
		Uint32 *pSoftwareReset = &SUP_RESET_SOFTWARE;
		Uint32 *pStatusReset = &SUP_RESET_STATUS;
		Uint32 *pTimerReset = &SUP_RESET_TIMER;

		if(*pSoftwareReset == SUP_MASK_HARDFAULT
		|| (*pSoftwareReset != SUP_MASK_SWRESET && project_wdogTimeout == 1)){
			if(*pStatusReset == 1 && *pTimerReset != 0){	// battery connected and timer has not timed out
				// Trigger event
				pInst->triggers2 |= SUP__TRG2_FW_ERROR;
			}
			else{
				// Set event
				Uint8 AssertId = 10;
				reg_putLocal(&AssertId, reg_i_EvtAssert, pInst->instId);
			}

			ResetTimer = SUP_RESET_TIMEOUT;
			*pTimerReset = ResetTimer;		// Stored in case of watchdog reset
		}

		*pSoftwareReset = 0;
	}

	/*
	 * Initialize function data
	 */

	sup__funcInit(pInst);

	/*
	 * Initialze static data
	 */
	reg_getLocal(&pInst->InstLogSPOld, reg_i_InstLogSamplePeriod, pInst->instId);

	reg_getLocal(&pInst->ExtraChargeFunc, reg_i_ExtraCharge_func, pInst->instId);
	reg_getLocal(&pInst->ExtraChargeDay, reg_i_ExtraCharge_var, pInst->instId);
	reg_getLocal(&pInst->ExtraChargeHour, reg_i_ExtraCharge_var2, pInst->instId);
	reg_getLocal(&pInst->ExtraChargeMin, reg_i_ExtraCharge_var3, pInst->instId);
	reg_get(&pInst->chargingMode, sup__chargingMode);

	/* Start timer */
	swtimer_start(&pInst->timer);

	FOREVER
	{
		Uint32			triggers;
		Uint32			triggers2;
		sup__StateAction action = 0;

		osa_signalGet();

		if(pInst->flags & SUP_FLG_DOWN){
			/* SUP is disabled in deep sleep mode */
			continue;
		}

		if (pInst->disabled == TRUE)
		{
			/* SUP is disabled in production test mode */
			continue;
		}

#ifndef NDEBUG
	tstamp_getStamp(&sup_lastRun);
	sup_counter++;
#endif

		pInst->test = TRUE; /* Reset internal test flag */

		atomic(
			triggers = pInst->triggers;
			triggers2 = pInst->triggers2;
			pInst->triggers = 0;
			pInst->triggers2 = 0;
		);

		/***********************************************************************
		 * Check mains state
		 */

		if (triggers & SUP__TRG_MAINS)
		{
			Uint8 mains;

			reg_get(&mains, sup__mains);

			if(mains == 0)
			{
				/*
				 * Mains has been disconnected.
				 */
				
				action |= SUP__ACT_MAINS_OFF;
			}
			else
			{
				action |= SUP__ACT_MAINS_ON;
			}

			//sup__logQueueWrite(pInst, SUP__LTODO_VCC); removed 110307
		}

		/***********************************************************************
		 * Initialize instant log second counter first time.
		 */

		if (triggers & SUP__TRG_INSTLOGINT)
		{
			Uint16 InstLogSP;

			reg_getLocal(
				&InstLogSP,
				reg_i_InstLogSamplePeriod,
				pInst->instId
			);

			if(InstLogSP != 0)
			{
				if(InstLogSP != pInst->InstLogSPOld)
				{
					sup__logQueueWrite(pInst, SUP__LTODO_CLEARINST);
					pInst->instLogCnt = InstLogSP;
				}
			}
			else
			{
				pInst->instLogCnt = 0xFFFF;
			}

			pInst->InstLogSPOld = InstLogSP;

		}

		/***********************************************************************
		 * Check if remote in state has changed.
		 */

		if (triggers & SUP__TRG_REMOTEIN)
		{
			action |= sup__handleRemoteIn(pInst);
		}

		/***********************************************************************
		 * Check if remote in state has changed.
		 */

		if (triggers & SUP__TRG_FACTORYDEF)
		{
			sup__resetToFactoryDefaults(pInst);

			// Set trigger for init of new charging params
			action |= SUP__ACT_FACT_DEF;
		}

		if (triggers & SUP__TRG_FACTORYSET)
		{
			sup__storeFactoryDefaults(pInst);
		}

		/***********************************************************************
		 * Check if new time or date has been set
		 */

		if (triggers & SUP__TRG_DATE)
		{
			RTC_Time		t;
			Int32			time;
			Posix_Time		pxTime;

			rtc_hwGetAsPosix(&pxTime);
			pInst->logData.oldTime = pxTime;
			pInst->logData.flags &= ~SUP__LFLAG_SMTIME;

			reg_getLocal(&time, reg_i_date_write, pInst->instId);
			rtc_convertToTime32(time, &t);
			rtc_hwSetDate(&t);
			
			sup__logQueueWrite(pInst, SUP__LTODO_TIMESET);
		}

		if (triggers & SUP__TRG_TIME)
		{
			RTC_Time		t;
			Int32			time;
			Posix_Time		pxTime;

			rtc_hwGetAsPosix(&pxTime);
			pInst->logData.oldTime = pxTime;
			pInst->logData.flags &= ~SUP__LFLAG_SMTIME;

			reg_getLocal(&time, reg_i_time_write, pInst->instId);
			rtc_convertToTime32(time, &t);
			rtc_hwSetTime(&t);

			sup__logQueueWrite(pInst, SUP__LTODO_TIMESET);
		}
 
		if (triggers & SUP__TRG_POSIXTIME)
		{
			Posix_Time		pxTime;

			rtc_hwGetAsPosix(&pxTime);
			pInst->logData.oldTime = pxTime;
			pInst->logData.flags |= SUP__LFLAG_SMTIME;

			reg_getLocal(&pxTime, reg_i_setRTC, pInst->instId);
			rtc_hwSetAsPosix(&pxTime);

			sup__logQueueWrite(pInst, SUP__LTODO_TIMESET);
		}

		/***********************************************************************
		 * Check if a function key function has changed.
		 */

		if (triggers & SUP__TRG_F1_FUNC)
		{
			Uint8 buttFunc;
			Uint8 language;
			Uint8 canComProfile;

			reg_get(&canComProfile, sup__CAN_CommProfile);
			if(canComProfile == CAN_COMM_PROFILE_SLAVE || canComProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
				buttFunc = SUP_FFUNC_NO_FUNC;
			}
			else{
				reg_get(&language, sup__Language);
				if(language == gui_lang_english_us) //if FW_MODE == USA
				{
					buttFunc = SUP_FFUNC_EQUALIZE;
				}
				else
				{
					reg_getLocal(&buttFunc, reg_i_ButtonF1_func, pInst->instId);
				}
			}

			if (buttFunc != pInst->oldF1Func)
			{
				sup__funcDisable(pInst, pInst->oldF1Func);
				pInst->oldF1Func = buttFunc;
				sup__funcUpdateLeds(pInst);
			}
		}

		if (triggers & SUP__TRG_F2_FUNC)
		{
			Uint8 buttFunc;
			Uint8 canComProfile;

			reg_get(&canComProfile, sup__CAN_CommProfile);
			if(canComProfile == CAN_COMM_PROFILE_SLAVE || canComProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
				buttFunc = SUP_FFUNC_NO_FUNC;
			}
			else{
				reg_getLocal(&buttFunc, reg_i_ButtonF2_func, pInst->instId);
			}

			if (buttFunc != pInst->oldF2Func)
			{
				sup__funcDisable(pInst, pInst->oldF2Func);
				pInst->oldF2Func = buttFunc;
				sup__funcUpdateLeds(pInst);
			}
		}

		/***********************************************************************
		 * Check if water, airpump or equalize function has changed.
		 */

		if (triggers & SUP__TRG_WATER_ACT)
		{
			sup__funcChangeWaterActive(pInst);
		}

		if (triggers & SUP__TRG_AIRPUMP_ACT)
		{
			sup__funcChangeAirPumpActive(pInst);
		}

		if (triggers & SUP__TRG_EQUAL_ACT)
		{
			sup__funcChangeEqualActive(pInst);
		}

		if (triggers & SUP__TRG_EQUAL_FUNC)
		{
			sup__funcChangeEqualFunc(pInst);
		}

		if (triggers & SUP__TRG_EQUAL_VAR)
		{
/*
			Uint8 EquCycleCount;
			Uint8 EquCycleInterval;

			reg_getLocal(&EquCycleInterval, reg_i_Equalize_var, pInst->instId);
			if(EquCycleInterval != 0)
			{
				EquCycleCount = EquCycleInterval;
				reg_putLocal(&EquCycleCount, reg_i_EquCycleCount, pInst->instId);
			}
*/
			Uint8 EquCycleCount;

			EquCycleCount = 0;
			reg_putLocal(&EquCycleCount, reg_i_EquCycleCount, pInst->instId);
		}

		if (triggers & SUP__TRG_REMOTEOUT_MAN)
		{
			sup__funcChangeRemoteOut(pInst);
		}

		/***********************************************************************
		 * Check if a status change was reported or if key states have changed.
		 */

		if (triggers & SUP__TRG_STATUSREP)
		{
			FOREVER
			{
				msg_Ptr messageHandle;

				messageHandle = msg_receive(pInst->instId);

				if (messageHandle == NULL)
				{
					break;
				}

				if (messageHandle->msgType == PROJECT_MSGT_STATUSREP)
				{
					switch(messageHandle->msgParam)
					{					
						case SUP_STAT_BDATA_RECEIVED:
							action |= SUP__ACT_BM_DATA;
							break;

						case SUP_STAT_CHALG_RECEIVED:
							action |= SUP__ACT_BM_CAPACK;
							break;

						case SUP_STAT_REGU_VALUES:
						{
							void * pReguInst;
							pReguInst = 
								sys_instIdToTHIS(pInst->pInit->reguInstId);
							pInst->pInit->pReguFn(pReguInst);
							action |= SUP__ACT_REGUVAL;
							pInst->timeout++;
							break;
						}

						case SUP_STAT_CHALG_ACTIVATE:
						{
							void * pChalgInst;
							pChalgInst = 
								sys_instIdToTHIS(pInst->pInit->chalgInstId);
							pInst->pInit->pChalgFn(pChalgInst);
							break;
						}

						case SUP_STAT_RADIO_ERROR:
						{
							Uint8 GwError;
							reg_get(&GwError, sup__GwError);

							if(GwError)
							{
								pInst->statusFlags |= SUP_STATUS_RADIO_ERROR;
							}
							else
							{
								pInst->statusFlags &= ~SUP_STATUS_RADIO_ERROR;
							}
							break;
						}

						case SUP_STAT_START_CHARGING:
							action |= SUP__ACT_START;
							break;
							
						case SUP_STAT_STOP_CHARGING:
							action |= SUP__ACT_STOP;
							break;

						case SUP_STAT_BM_INIT2:
							action |= SUP__ACT_BM_INIT2;
							break;

						case SUP_STAT_BM_CALOK:
							action |= SUP__ACT_BM_CALOK;
							break;

						case SUP_STAT_VSENSE:
							action |= SUP__ACT_VSENSE;
							break;

						case SUP_STAT_BM_INIT2_FAIL:
							action |= SUP__ACT_BM_INIT2_FAIL;
							break;

						case SUP_STAT_CHALG_VALUES:
						{
							action |= SUP__ACT_CHALGVAL;
							pInst->delayCtr++;
							pInst->instLogCnt--;
							pInst->remoteOffCtr++;
							break;
						}

						case SUP_STAT_ADDRESS_CONFLICT:
						{
								pInst->statusFlags |= SUP_STATUS_ADDR_CONFLICT;
							break;
						}

						case SUP_STAT_BM_ADDRESS_CONFLICT:
						{
							pInst->statusFlags |= SUP_STATUS_BM_ADDR_CONFLICT;
							break;
						}

						case SUP_STAT_DPL_NO_MASTER:
						{
							pInst->statusFlags |= SUP_STATUS_DPL_NO_MASTER;
							break;
						}

						case SUP_STAT_DPL_MASTER:
						{
							pInst->statusFlags &= ~SUP_STATUS_DPL_NO_MASTER;
							break;
						}

						case SUP_STAT_DPL_MASTER_CONFLICT_SET:
						{
							pInst->statusFlags |= SUP_STATUS_DPL_MASTER_CONFLICT;
							break;
						}

						case SUP_STAT_DPL_MASTER_CONFLICT_CLEAR:
						{
							pInst->statusFlags &= ~SUP_STATUS_DPL_MASTER_CONFLICT;
							break;
						}

						case SUP_STAT_ADDRESS_CONFLICT_CLEAR:
						{
							pInst->statusFlags &= ~SUP_STATUS_ADDR_CONFLICT;
							pInst->statusFlags &= ~SUP_STATUS_BM_ADDR_CONFLICT;
							break;
						}
					}
				}
				else if (messageHandle->msgType == MSG_T_KEYBCHANGED)
				{
					Uint16 changedKeys;
					Uint8 canComProfile;

					changedKeys = pInst->oldKeyStates ^ messageHandle->msgParam;
					
					reg_get(&canComProfile, sup__CAN_CommProfile);
					if(canComProfile == CAN_COMM_PROFILE_SLAVE || canComProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
						; // do not handle F1 and F2 buttons
					}
					else{
						if (
							(changedKeys & MEAS_KEY_F1) &&
							(messageHandle->msgParam & MEAS_KEY_F1)
						) {
							Uint8 func;
							Uint8 language;

							reg_get(&language, sup__LanguageP);
							if(language == gui_lang_english_us) //if FW_MODE == USA
							{
								func = SUP_FFUNC_EQUALIZE;
							}
							else
							{
								reg_getLocal(&func, reg_i_ButtonF1_func, pInst->instId);
							}

							action |= sup__funcHandleButton(pInst, func);
						}

						if (
							(changedKeys & MEAS_KEY_F2) &&
							(messageHandle->msgParam & MEAS_KEY_F2)
						) {
							Uint8 func;
							reg_getLocal(&func, reg_i_ButtonF2_func, pInst->instId);
							action |= sup__funcHandleButton(pInst, func);
						}
					}

					if ((changedKeys & MEAS_KEY_STOP) &&
						(messageHandle->msgParam & MEAS_KEY_STOP)
					) {
						action |= sup__handleSTOPButton(pInst);
					}

					pInst->oldKeyStates = messageHandle->msgParam;
				}

				msg_unlock(messageHandle);
			}
		}

		/***********************************************************************
		 * Check CHALG status and error
		 */

		if(action & SUP__ACT_CHALGVAL)
		{
			Uint16 chalgStatus;

			reg_get(&chalgStatus, sup__chalgStatus);

			if (chalgStatus != pInst->oldChalgStat)
			{
				if (
					(chalgStatus & CHALG_STAT_BATTRY_CONN) &&
					not(pInst->oldChalgStat & CHALG_STAT_BATTRY_CONN) 
				) {
					action |= SUP__ACT_BATT_CONN;
				}
				else if (
					(not(chalgStatus & CHALG_STAT_BATTRY_CONN) &&
					(pInst->oldChalgStat & CHALG_STAT_BATTRY_CONN)) 
				) {
					action |= SUP__ACT_BATT_DCONN;

					atomic(
						pInst->statusFlags &= ~SUP_STATUS_EXTRACHARGE;
					);
				}

				else if ((chalgStatus & CHALG_STAT_CHARG_COMPLETED) &&
						not(pInst->oldChalgStat & CHALG_STAT_CHARG_COMPLETED))
				{
					action |= SUP__ACT_CHARG_COMP;

					atomic(
						pInst->statusFlags &= ~SUP_STATUS_EXTRACHARGE;
					);
				}

			}

			pInst->oldChalgStat = chalgStatus;
			triggers |= SUP__TRG_REMOTEOUT;
		}

		if (triggers & SUP__TRG_CHALGERR)
		{
/*	change tag 1
			Uint16 chalgErr;

			reg_get(&chalgErr, sup__chalgError);

			if(chalgErr & CHALG_ERR_HIGH_BATTERY_LIMIT){
				action |= SUP__ACT_CHARG_ERR;
			}
*/

			// Allways write when change has occurred
			sup__logQueueWrite(pInst, SUP__LTODO_CHALGERROR);
		}

		/***********************************************************************
		 * Check if remote out should be updated.
		 */

		if (triggers & SUP__TRG_REMOTEOUT)
		{
			sup__updateRemoteOut(pInst);
		}

		/***********************************************************************
		 * Check if statistics should be cleared.
		 */

		if (triggers & SUP__TRG_CLEARSTAT)
		{
			Uint16 u16Temp;
			Uint32 u32Temp;

			u16Temp = 0;
			u32Temp = 0;

			reg_putLocal(&u16Temp, reg_i_CyclesSumBelow25, pInst->instId);
			reg_putLocal(&u16Temp, reg_i_CyclesSum26to50, pInst->instId);
			reg_putLocal(&u16Temp, reg_i_CyclesSum51to80, pInst->instId);
			reg_putLocal(&u16Temp, reg_i_CyclesSum81to90, pInst->instId);
			reg_putLocal(&u16Temp, reg_i_CyclesSumAbove90, pInst->instId);
			reg_putLocal(&u32Temp, reg_i_CyclesSumTotal, pInst->instId);

			reg_putLocal(&u32Temp, reg_i_ChargeTimeTotal, pInst->instId);
			reg_putLocal(&u32Temp, reg_i_ChargedAhTotal, pInst->instId);
			reg_putLocal(&u32Temp, reg_i_ChargedWhTotal, pInst->instId);
			reg_putLocal(&u32Temp, reg_i_AcWhTotal, pInst->instId);

			reg_put(&u16Temp, sup__OverVoltCnt);

			sup__logQueueWrite(pInst, SUP__LTODO_CLEAR);
		}

		/***********************************************************************
		 * Check REGU error
		 */

		if (triggers & SUP__TRG_REGUERR)
		{
/*
			Uint16 reguError;

			reg_get(&reguError, sup__reguError);

			if(reguError)
			{
				;
			}
*/
			// Allways write when change has occurred
			sup__logQueueWrite(pInst, SUP__LTODO_REGUERROR);
		}

		/***********************************************************************
		 * Check if ADC calibration values have changed.
		 */

		if (triggers & SUP__TRG_UADSLOPE)
		{
			sup__logQueueWrite(pInst, SUP__LTODO_VOLTCAL);
		}

		if (triggers & SUP__TRG_IADSLOPE)
		{
			sup__logQueueWrite(pInst, SUP__LTODO_CURRCAL);
		}

		/***********************************************************************
		 * Check if radio FW version has changed (should only happen during
		 * startup).
		 */

		if (triggers & SUP__TRG_RADIOFW)
		{
			sup__logQueueWrite(pInst, SUP__LTODO_STARTUP);
		}

		/***********************************************************************
		 * Check if battery info has changed.
		 */

		if (triggers & SUP__TRG_BATTINFO)
		{
			sup__logQueueWrite(pInst, SUP__LTODO_CURVEDATA);
		}

		/***********************************************************************
		 * Check if RTC calibration value should be set.
		 */
		if (triggers & SUP__TRG_RTCCALIB)
		{
			Uint32 calibValue;

			reg_getLocal(&calibValue, reg_i_rtcCalib, pInst->instId);
			rtc_hwSetCalibration(calibValue);
		}

		/***********************************************************************
		 * Write event if assert has occurred.
		 */
		if (triggers & SUP__TRG_EVTASSERT)
		{
			sup__logQueueWrite(pInst, SUP__LTODO_EVTASSERT);
		}

		/***********************************************************************
		 * New func set for extra charge.
		 */
		if (triggers & SUP__TRG_ECFUNC)
		{
			Uint8			func;

			reg_getLocal(&func, reg_i_ExtraCharge_func, pInst->instId);

			atomic(
				pInst->ExtraChargeFunc = func;
			//	pInst->statusFlags &= ~SUP_STATUS_EXTRACHARGE;
			);
		}

		/***********************************************************************
		 * New day set for extra charge.
		 */
		if (triggers & SUP__TRG_ECDAY)
		{
			Uint8			day;

			reg_getLocal(&day, reg_i_ExtraCharge_var, pInst->instId);

			atomic(
				pInst->ExtraChargeDay = day;
			//	pInst->statusFlags &= ~SUP_STATUS_EXTRACHARGE;
			);
		}

		/***********************************************************************
		 * New time set for extra charge.
		 */
		if (triggers & SUP__TRG_ECTIME)
		{
			RTC_Time		t;
			Posix_Time		pxTime;

			reg_getLocal(&pxTime, reg_i_ECTime_write, pInst->instId);

			rtc_convertToTime32(pxTime, &t);

			atomic(
				pInst->ExtraChargeHour = (Uint8)t.tm_hour;
				pInst->ExtraChargeMin = (Uint8)t.tm_min;
			//	pInst->statusFlags &= ~SUP_STATUS_EXTRACHARGE;
			);

			reg_putLocal(&pInst->ExtraChargeHour, reg_i_ExtraCharge_var2, pInst->instId);
			reg_putLocal(&pInst->ExtraChargeMin, reg_i_ExtraCharge_var3, pInst->instId);

		}

		/***********************************************************************
		 * Timer has expired.
		 */
		if (triggers & SUP__TRG_TIMER)
		{
			action |= SUP__ACT_TIMER;
		}

		/***********************************************************************
		 * CAN mode has changed.
		 */
		if (triggers2 & SUP__TRG2_CANMODE)
		{
			Uint8				u8;
			reg_get(&u8, sup__CAN_CommProfile);

			switch(u8)
			{
				case CAN_COMM_PROFILE_DISABLED:
				case CAN_COMM_PROFILE_MASTER:
				case CAN_COMM_PROFILE_STATUS:
					/* CHALG controlled */
					action |= SUP__ACT_CHALG_CTRL;
					pInst->CAN_Mode = SUP_CHALG_CONTROLLED;
					break;

				case CAN_COMM_PROFILE_SLAVE:
					/* CAN controlled */
					action |= SUP__ACT_CAN_CTRL;
					pInst->CAN_Mode = SUP_CAN_CONTROLLED;
					break;

				case CAN_COMM_PROFILE_MASTER_CAN_INPUT:
					/* CAN external controlled */
					action |= SUP__ACT_CAN_EXT_CTRL;
					pInst->CAN_Mode = SUP_CAN_EXT_CONTROLLED;
					break;

				default:
					pInst->CAN_Mode = SUP_CHALG_CONTROLLED;
					break;

			}
			pInst->triggers |= (SUP__TRG_F1_FUNC|SUP__TRG_F2_FUNC);
		}

		/***********************************************************************
		 * Timer for voltage sense has expired.
		 */
		if (triggers2 & SUP__TRG2_VSENSE)
		{
			action |= SUP__ACT_VSENSE;
		}

		/***********************************************************************
		 * Language has changed.
		 */
		if (triggers2 & SUP__TRG2_LANGUAGE)
		{
			pInst->triggers |= SUP__TRG_F1_FUNC;
		}

		/***********************************************************************
		 * Tilt sensor value has changed.
		 */
		if (triggers2 & SUP__TRG2_TILTSENSE)
		{
			sup__logQueueWrite(pInst, SUP__LTODO_TILTSENSE);
		}

		/***********************************************************************
		 * Check if Cable Resistance slope raised.
		 */

		if (triggers2 & SUP__TRG2_CRISLOPE)
		{
			sup__logQueueWrite(pInst, SUP__LTODO_CABLERISLOPE);
		}

		if (triggers2 & SUP__TRG2_WATER_MAN)
		{
			sup__funcManualWater(pInst);
		}

		/***********************************************************************
		 * Check if firmware malfunction
		 */
		if (triggers2 & SUP__TRG2_FW_ERROR)
		{
			// Stop charging and display error
			pInst->statusFlags |= SUP_STATUS_CORRUPTED;
		}

		/***********************************************************************
		 * ChargingMode has changed.
		 */
		if (triggers2 & SUP__TRG2_CHARGEMODE)
		{
			reg_get(&pInst->chargingMode, sup__chargingMode);						// Update ChargingMode process variable
		}


		/***********************************************************************
		 * Check if instant log should be written.
		 */

		if (pInst->instLogCnt == 0)
		{
			Uint16 InstLogSP;

			reg_getLocal(
				&InstLogSP,
				reg_i_InstLogSamplePeriod,
				pInst->instId
			);

			if(InstLogSP != 0)
			{
				sup__logQueueWrite(pInst, SUP__LTODO_STDINST);
				pInst->instLogCnt = InstLogSP;
			}
			else
			{
				pInst->instLogCnt = 0xFFFF;
			}
		}

		/***********************************************************************
		 * Check SUP status flags
		 */
		if(pInst->statusFlags != pInst->statusFlagsOld)
		{
			if((pInst->statusFlags & SUP_EVENT_MASK) != (pInst->statusFlagsOld & SUP_EVENT_MASK))
			{
				sup__logQueueWrite(pInst, SUP__LTODO_SUPERROR);
			}
			
			if((pInst->statusFlags & SUP_STATUS_REMOTERESTR2) && !(pInst->statusFlagsOld & SUP_STATUS_REMOTERESTR2))
			{
				pInst->remoteOffCtr = 0;
			}
			
			pInst->statusFlagsOld = pInst->statusFlags;
		}

		/***********************************************************************
		 * Check if there are pending log writes. Also continue with flash
		 * checksum calculation.
		 */

		if (action & SUP__ACT_REGUVAL)
		{
			Boolean done;

			/*
			 * This code is run each 10ms. It is assumed that new REGU values
			 * will be stored to REG at that interval.
			 */
			sup__logProcessPending(pInst);

			/*
			 * It should take about 50 seconds to calculate the checksum when
			 * 50 bytes is calculated each 10ms.
			 */

			done = chksum_checkStep(&pInst->chkdata, 128);

			if (done)
			{
				Uint32 checksum;

				/*
				 * Checksum has been calculated for the whole flash.
				 */

				reg_getLocal(&checksum, reg_i_ChecksumMain, pInst->instId);

				/*
				 * For some reason the "correct" checksum contains 4 bytes
				 * less padding. For this reason the bytes are now added to the
				 * checksum
				 */

				checksum += 4 * 0xFF;

				if (0)//not(checksum == chksum_getChecksum(&pInst->chkdata))
				{
				/*
					 * Flash memory has been corrupted. Set the status bit so
					 * that GUI will display the error.
					 *
					 * Drive down all function blocks except GUI as fast as
					 * possible. SYS_DOWN_POWER_LOSS is used because that should
					 * give the fastest shutdown and the sys_downAllEx should
					 * return directly without waiting for function blocks to
					 * go down.
					 *
					 * Finally the project error handler is called. That 
					 * function should then do the rest.
					 */
					
					pInst->statusFlags |= SUP_STATUS_CORRUPTED;

					sys_downAllEx(SYS_DOWN_POWER_LOSS, pInst->pInit->guiInstId);

					project__errorHandler(PROJECT_ERROR_CHKSUM);
				}

				chksum_setArea(&pInst->chkdata, pInst->pInit->pFlash);
				chksum_checkReset(&pInst->chkdata);
			}
		}

		sup__stateMachine(pInst, action);

#ifndef NDEBUG
		sup_counter++;
#endif
		}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup_enpackEnumCode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get engine code from engine struct position number.
*
*	\param		idx		Engine number.
*	\param		pBuf 	Pointer to buffer where the name will be copied.
*	\param		pCnt	Number of engines available.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC Uint8 sup_enpackEnumCode(
	Uint8					idx,
	BYTE *					pBuf,
	Uint8 *					pCnt
) {
#include "engines_types.h"
	deb_assert(pCnt);

	*pCnt = engineGetLen();

	if (pBuf != NULL)
	{
		if (idx < *pCnt)
		{
			Uint8 len = 0;
			Uint16 MaxLen = 11;
			Uint32 Code = engineGetCode(idx);

			extern Uint8 lib_Uint32ToAscii(Uint32,char *,Uint16);
			len = lib_Uint32ToAscii(Code,(char *)pBuf,MaxLen);

			return len;
		}
	}

	return 0;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		SW timer callback.
*
* \param		pTimer		ptr to the expired timer
*
* \details
*
* \note
*
* \sa
*
******************************************************************************/
PRIVATE void sup__timer(
	swtimer_Timer_st *		pTimer
) {
	sup__Inst *				pInst = (sup__Inst*)pTimer->pUtil;
	static int vSenseDelayTime = SUP_VSENSE_DELAYTIME;

	if(pInst->state == SUP_STATE_CHARGING)
		pInst->triggers |= SUP__TRG_TIMER;

	if(pInst->state == SUP_STATE_VOLTAGE_SENSE){
		if(vSenseDelayTime){
			vSenseDelayTime--;
			if(vSenseDelayTime == 0){
				pInst->triggers2 |= SUP__TRG2_VSENSE;							// Trigger new measurement
				vSenseDelayTime = SUP_VSENSE_DELAYTIME;							// Reset time for trigger
			}
		}
	}

	if(ResetTimer){
		ResetTimer--;
	}
	SUP_RESET_TIMER = ResetTimer;	// Stored in case of watchdog reset

	{
		Uint32 batteryVoltage;
		Interval_type UactOff;
		Interval_type UactOn;
		Uint32 batteryOnLimit;
		Uint32 batteryOffLimit;
		Uint32 batteryDcon;

		reg_get(&batteryVoltage, sup__Umeas1s);									// Get voltage(mV)

		regu_getUactOnOffLimits(&UactOff, &UactOn);								// Get limits for battery detection
		reg_get(&batteryDcon, sup__BatteryDcon);								// Get flag for battery disconnection due to delta voltage

		batteryOnLimit = (Uint32)(UactOn.Low * 1000);							// Convert to mV
		batteryOffLimit = (Uint32)(UactOff.Low * 1000);							// Convert to mV

		if(batteryVoltage >= batteryOnLimit)
		{
			if(batteryDcon)
			{
				SupBatteryConnected = FALSE;
			}
			else
			{
				SupBatteryConnected = TRUE;
			}
		}
		else if(batteryVoltage < batteryOffLimit)
		{
			SupBatteryConnected = FALSE;
		}
	}

	osa_signalSend(&pInst->task);
}

PUBLIC void sup_testClearStat(
) {
	Uint16 u16Temp;
	Uint32 u32Temp;

	u16Temp = 0;
	u32Temp = 0;

	reg_putLocal(&u16Temp, reg_i_CyclesSumBelow25, sup__pInst->instId);
	reg_putLocal(&u16Temp, reg_i_CyclesSum26to50, sup__pInst->instId);
	reg_putLocal(&u16Temp, reg_i_CyclesSum51to80, sup__pInst->instId);
	reg_putLocal(&u16Temp, reg_i_CyclesSum81to90, sup__pInst->instId);
	reg_putLocal(&u16Temp, reg_i_CyclesSumAbove90, sup__pInst->instId);
	reg_putLocal(&u32Temp, reg_i_CyclesSumTotal, sup__pInst->instId);

	reg_putLocal(&u32Temp, reg_i_ChargeTimeTotal, sup__pInst->instId);
	reg_putLocal(&u32Temp, reg_i_ChargedAhTotal, sup__pInst->instId);
	reg_putLocal(&u32Temp, reg_i_ChargedWhTotal, sup__pInst->instId);
	reg_putLocal(&u32Temp, reg_i_AcWhTotal, sup__pInst->instId);

	reg_put(&u16Temp, sup__OverVoltCnt);

	sup__logQueueWrite(sup__pInst, SUP__LTODO_CLEAR);
}
