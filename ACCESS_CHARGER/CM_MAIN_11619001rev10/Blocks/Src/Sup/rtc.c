/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		rtc.c
*
*	\ingroup	SUP_ARM7
*
*	\brief		
*
*	\details	
*
*	\par		
*
*	\par		
*
*	\note		
*
*	\version	02-11-2009 / Antero Rintam�ki
*
*******************************************************************************/


/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/
#include "tools.h"
#include "_hw.h"
#include "rtc.h"

#include "date.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_hwSetAsPosix
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets the time to RTC-registers. 
*
*	\param		pTimeRaw			Seconds as Posix Time.
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/
PUBLIC void rtc_hwSetAsPosix(
	Posix_Time *			pTimeRaw
) {
	RTC_Time				t;

	rtc_convertToTime32(*pTimeRaw, &t);
	rtc_hwSet(&t);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_get
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the time as Posix Time(seconds from 1970).
*
*	\param		pTimeRaw		Pointer to Raw Posix Time variable. Function will  
*								modify given parameter to match current time at 
*								RTC.
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/
PUBLIC void rtc_hwGetAsPosix(
	Posix_Time *				pTimeRaw
) {
	RTC_Time					t;
	
	rtc_hwGet(&t);
	rtc_convertToPosix32(&t, pTimeRaw);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_convertToPosix32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts RTC_Time-structure to Posix time as single integer(32bit)
*
*	\param		pTime		Pointer to RTC_Time -structure
*	\param		val			Pointer to single integer that will have the posix time
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC Boolean rtc_convertToPosix32(
	RTC_Time *					pTime,
	Posix_Time *				val
) {
	date_Date 					d;
	date_SerNr 					ser;
	
	// calculate days from 1970
	d.year = pTime->tm_year + 1900;
	d.month = (Uint8)pTime->tm_mon + 1;
	d.day = (Uint8)pTime->tm_mday;
	ser = date_toSerNr(&d);
	
	// calculate seconds from "1970 Jan 1"
	*val = (Posix_Time)((Uint32)ser * 24UL * 3600UL + (Uint32)pTime->tm_hour * 3600UL + (Uint32)pTime->tm_min * 60UL + pTime->tm_sec);
	
	return TRUE;
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_convertToTime32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts Posix time integer(32bit) to RTC_Time-structure.
*
*	\param		pTime		Pointer to RTC_Time -structure
*	\param		val			Pointer to single integer that will have the posix time
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC Boolean rtc_convertToTime32(
	Posix_Time					val,
	RTC_Time *					pTime
) {// 1
	date_Date 					d;
	date_SerNr					nSerNr;
	Int32						val2;
	
	// get years, months and days 14555
	nSerNr = (date_SerNr)(val / (24*3600)); // modulus to get the day-count from epoch
	date_fromSerNr(&d, nSerNr);
	pTime->tm_year = d.year - 1900;
	pTime->tm_mon = d.month - 1;
	pTime->tm_mday = d.day;

	// value for hours, mins, seconds
	val2 = val - (nSerNr*24*3600);
	pTime->tm_hour = (Uint16)(val2 / 3600);
	pTime->tm_min = (Uint16)(val2 - pTime->tm_hour*3600) / 60;
	pTime->tm_sec = (Uint16)(val2 - pTime->tm_hour*3600 - pTime->tm_min*60);
	
	return TRUE;
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts posix time to weekday.
*
*	\param		val		posix time
*
*	\return		Weekday index from 0 (monday) to 6 (sunday).
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Uint8 rtc_posixToWeekday(
	Posix_Time					val
) {
	val /= RTC_SECS_PER_DAY;

	/* 1.1.1970 was thursday */
	return (Uint8)((val + 3) % 7);
}