/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Sup_func.c
*
*	\ingroup	SUP
*
*	\brief		F1, F2, STOP button and remote in function handling.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "reg.h"
#include "deb.h"

#include "Meas.h"
#include "Radio.h"
#include "cai_cc.h"
#include "gui.h"
#include "can.h"
#include "sup.h"
#include "tui.h"

#include "../Cc/ClusterControl.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Remote out phase variable bits
 *
 * \brief	Variable bits for phase function.
 */

/*@{*/
#define SUP__ROVAR_PHASE_PRE		(1<<0)
#define SUP__ROVAR_PHASE_MAIN		(1<<1)
#define SUP__ROVAR_PHASE_ADDITIONAL	(1<<2)
#define SUP__ROVAR_PHASE_READY		(1<<3)
#define SUP__ROVAR_PHASE_EQUALIZE	(1<<4)
#define SUP__ROVAR_PHASE_IDLE		(1<<5)
/*@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef struct {
	sys_RegHandle			fActiveReg;		/**< Function active reg handle	*/
} sup__FuncRegs;

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/
int gStartStopFunc = 0;
int gRemoteRestricted = 0;
int gLowCurrentMode = 0;
int gRemoteOutToggle = 0;
/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Function button function to active register handle mapping table.
 */

PRIVATE sup__FuncRegs sup__funcRegs[] = {
	{SYS_BAD_REGISTER},						/* SUP_FFUNC_NO_FUNC */
	{SYS_BAD_REGISTER},						/* SUP_FFUNC_EQUALIZE */
	{SYS_BAD_REGISTER},						/* SUP_FFUNC_REMOTEOUT */
	{SYS_BAD_REGISTER},						/* SUP_FFUNC_AIRPUMP */
	{SYS_BAD_REGISTER}						/* SUP_FFUNC_WATER */
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize function handling.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PUBLIC void sup__funcInit(
	sup__Inst *				pInst
) {
	reg_getLocal(&pInst->oldF1Func, reg_i_ButtonF1_func, pInst->instId);
	reg_getLocal(&pInst->oldF2Func, reg_i_ButtonF2_func, pInst->instId);

	sup__funcRegs[SUP_FFUNC_EQUALIZE].fActiveReg =
		reg_indexToHandle(pInst->instId, reg_i_Equalize_active);
	sup__funcRegs[SUP_FFUNC_REMOTEOUT].fActiveReg =
		reg_indexToHandle(pInst->instId, reg_i_RemoteOut_man);
	sup__funcRegs[SUP_FFUNC_AIRPUMP].fActiveReg =
		reg_indexToHandle(pInst->instId, reg_i_AirPump_active);
	sup__funcRegs[SUP_FFUNC_WATER].fActiveReg =
		reg_indexToHandle(pInst->instId, reg_i_Water_active);

	sup__funcUpdateLeds(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcUpdateLeds
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Updates function button led states.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PUBLIC void sup__funcUpdateLeds(
	sup__Inst *				pInst

) {
	Uint8					funcActive;
	Uint8					buttFunc;
	Uint8 					language;
	Uint8 					canComProfile;
	/*
	 *	Update F1 led state.
	 */
	reg_get(&canComProfile, sup__CAN_CommProfile);

	if(canComProfile == CAN_COMM_PROFILE_SLAVE || canComProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
		buttFunc = SUP_FFUNC_NO_FUNC;
	}
	else{
		reg_get(&language, sup__LanguageP);
		if(language == gui_lang_english_us) //if FW_MODE == USA
		{
			buttFunc = SUP_FFUNC_EQUALIZE;
		}
		else
		{
			reg_getLocal(&buttFunc, reg_i_ButtonF1_func, pInst->instId);
		}
	}

	funcActive = 0;
	if (buttFunc != SUP_FFUNC_NO_FUNC)
	{
		reg_get(&funcActive, sup__funcRegs[buttFunc].fActiveReg);
	}
	reg_putLocal(&funcActive, reg_i_ButtonF1_active, pInst->instId);

	/*
	 *	Update F2 led state.
	 */

	if(canComProfile == CAN_COMM_PROFILE_SLAVE || canComProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
		buttFunc = SUP_FFUNC_NO_FUNC;
	}
	else{
		reg_getLocal(&buttFunc, reg_i_ButtonF2_func, pInst->instId);
	}

	funcActive = 0;
	if (buttFunc != SUP_FFUNC_NO_FUNC)
	{
		reg_get(&funcActive, sup__funcRegs[buttFunc].fActiveReg);
	}
	reg_putLocal(&funcActive, reg_i_ButtonF2_active, pInst->instId);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcHandleButton
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles a function button pressed event.
*
*	\param		pInst		Pointer to SUP instance.
*	\param		buttFunc	Button function.
*
*	\return		State machine action flags.
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PUBLIC sup__StateAction sup__funcHandleButton(
	sup__Inst *				pInst,
	Uint8					buttFunc
) {
	Uint8					funcActive;
	Uint8					funcOverride;

	if (buttFunc != SUP_FFUNC_NO_FUNC)
	{
		reg_get(&funcActive, sup__funcRegs[buttFunc].fActiveReg);
		funcActive = funcActive ? 0 : 1;
		reg_put(&funcActive, sup__funcRegs[buttFunc].fActiveReg);

		if(buttFunc == SUP_FFUNC_EQUALIZE)
		{
			funcOverride = TRUE;
			reg_putLocal(&funcOverride, reg_i_Equalize_override, pInst->instId);
		}
	}
	
	return(0);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcChange
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle function button function change by disabling previous
*				function.
*
*	\param		pInst			Pointer to SUP instance.
*	\param		funcChanged		The button function has changed.
*
*	\return		-
*
*	\details	Should be called when the F1 button function is changed.
*				This function will disable the function that was configured on
*				the button before the change.
*
*	\note				
*
*******************************************************************************/

PUBLIC void sup__funcDisable(
	sup__Inst *				pInst,
	Uint8					oldFunc
) {
	Uint8					funcActive;

	if (oldFunc != SUP_FFUNC_NO_FUNC)
	{
		reg_get(&funcActive, sup__funcRegs[oldFunc].fActiveReg);
		if (funcActive)
		{
			funcActive = 0;
			reg_put(&funcActive, sup__funcRegs[oldFunc].fActiveReg);
		}
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcChangeRemoteOut
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles manual remote out function change.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details	Should be called when the RemoteOut_man function register has been
*				written to.
*
*	\note
*
*******************************************************************************/

PUBLIC void sup__funcChangeRemoteOut(
	sup__Inst *				pInst
) {
	Uint8					funcActive;
	Uint8					status;

	/*
	 *	Report function change to GUI so that a status message will be shown.
	 *	Also update the function button led status.
	 */

	reg_getLocal(&funcActive, reg_i_RemoteOut_man, pInst->instId);
	status = funcActive ? SUP_REP_REMOTEOUT_ENABLED : SUP_REP_REMOTEOUT_DISABLED;
	reg_putLocal(&status, reg_i_statusRep, pInst->instId);

	sup__funcUpdateLeds(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcChangeEqualActive
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles equalize function change.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details	Should be called when the equalize function register has been
*				written to.
*
*	\note				
*
*******************************************************************************/

PUBLIC void sup__funcChangeEqualActive(
	sup__Inst *				pInst
) {
	static Uint8			funcActiveOld = 0;
	Uint8					funcActive;
	Uint8					status;

	/*
	 *	Report function change to GUI so that a status message will be shown.
	 *	Also update the function button led status.
	 */

	reg_getLocal(&funcActive, reg_i_Equalize_active, pInst->instId);
	if(funcActive != funcActiveOld)
	{
		status = funcActive ? SUP_REP_EQUALIZE_ENABLED : SUP_REP_EQUALIZE_DISABLED;
		reg_putLocal(&status, reg_i_statusRep, pInst->instId);
		funcActiveOld = funcActive;
	}

	sup__funcUpdateLeds(pInst);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcChangeEqualFunc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles equalize function change.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details	Should be called when the equalize function register has been
*				written to.
*
*	\note
*
*******************************************************************************/
PUBLIC void sup__funcChangeEqualFunc(
	sup__Inst *				pInst
) {
	Uint8					funcEnabled;
	Uint8					funcActive;
	//Uint8					status;

	/*
	 *	Report function change to GUI so that a status message will be shown.
	 *	Also update the function button led status.
	 */

	reg_getLocal(&funcEnabled, reg_i_Equalize_func, pInst->instId);

	if(funcEnabled == 0)
	{
		funcActive = 0;
		reg_putLocal(&funcActive, reg_i_Equalize_active, pInst->instId);
	}

	sup__funcUpdateLeds(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcChangeWaterActive
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles water function change.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details	Should be called when the equalize function register has been
*				written to.
*
*	\note
*
*******************************************************************************/

PUBLIC void sup__funcChangeWaterActive(
	sup__Inst *				pInst
) {
	Uint8					funcActive;
	Uint8					status;

	/*
	 *	Report function change to GUI so that a status message will be shown.
	 *	Also update the function button led status.
	 */

	reg_getLocal(&funcActive, reg_i_Water_active, pInst->instId);
	status = funcActive ? SUP_REP_WATER_ENABLED : SUP_REP_WATER_DISABLED;
	reg_putLocal(&status, reg_i_statusRep, pInst->instId);

	sup__funcUpdateLeds(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcManualWater
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles manual water indication.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details	Should be called when the manualWatering  register has been
*				written to.
*
*	\note
*
*******************************************************************************/

PUBLIC void sup__funcManualWater(
	sup__Inst *				pInst
) {
	Uint8					funcActive;
	Uint8					status;

	/*
	 *	Report function change to GUI so that a status message will be shown.
	 */

	reg_getLocal(&funcActive, reg_i_Water_man, pInst->instId);
	status = funcActive ? SUP_REP_WATER_MANUAL_ENABLED : SUP_REP_WATER_MANUAL_DISABLED;
	reg_putLocal(&status, reg_i_statusRep, pInst->instId);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__funcChangeAirPumpActive
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles air pump function change.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details	Should be called when the equalize function register has been
*				written to.
*
*	\note
*
*******************************************************************************/

PUBLIC void sup__funcChangeAirPumpActive(
	sup__Inst *				pInst
) {
	Uint8					funcActive;
	Uint8					status;

	/*
	 *	Report function change to GUI so that a status message will be shown.
	 *	Also update the function button led status.
	 */

	reg_getLocal(&funcActive, reg_i_AirPump_active, pInst->instId);
	status = funcActive ? SUP_REP_AIR_ENABLED : SUP_REP_AIR_DISABLED;
	reg_putLocal(&status, reg_i_statusRep, pInst->instId);

	sup__funcUpdateLeds(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__handleSTOPButton
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns a state machine action according to the configured
*				STOP button function
*
*	\param		pInst		ptr to SUP instance.
*
*	\return		State machine action flags.
*
*	\details		
*
*	\note					
*
*******************************************************************************/

PUBLIC sup__StateAction sup__handleSTOPButton(
	sup__Inst *				pInst
) {
	/* todo:	Is this the correct function?
	 */

	return(SUP__ACT_STOP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__handleRemoteIn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns a state machine action according to the configured
*				remote in function
*
*	\param		pInst			ptr to SUP instance.
*
*	\return		State machine action flags.
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PUBLIC sup__StateAction sup__handleRemoteIn(
	sup__Inst *				pInst
) {
	sup__StateAction		retVal = 0;
	Uint8 function;
	Uint8 newState;
	Uint8 remoteIn;
	Uint8 remoteInFunc;
	Uint8 extraIn;
	Uint8 extraInFunc;
	Uint8 canIn;
	Uint8 canInMode;

	reg_get(&remoteIn, sup__remoteIn);
	reg_getLocal(&remoteInFunc, reg_i_RemoteIn_func, pInst->instId);
	reg_get(&extraIn, sup__ExtraIn);
	reg_get(&extraInFunc, sup__ExtraIn_func);
	reg_get(&canIn, sup__CanInput);
	reg_get(&canInMode, sup__CanInputMode);

	// OR together remote in and extra in functions
	function = extraInFunc;
	function |= 1 << remoteInFunc;
	// AND together remote in and extra in logic levels
	newState = extraIn;
	// OR/AND remote in and canIn logic level depending on mode
	if(canInMode == 0){
		if(!remoteIn || canIn){
			newState &= ~(1 << remoteInFunc);									// Input is zero (active) if one input is zero (active)
		}
	}
	else{
		if(!remoteIn && canIn){
			newState &= ~(1 << remoteInFunc);									// Input is zero (active) if both inputs are zero (active)
		}
	}

	// Function stop
	if(function & (1 << SUP_RIFUNC_STOP)){
		if(newState & (1 << SUP_RIFUNC_STOP)){
			// input high
		}
		else{
			//input low
			retVal = SUP__ACT_STOP;
		}
	}
	// Start/Stop function active
	if(function & (1 << SUP_RIFUNC_START_STOP)){
		gStartStopFunc = TRUE;
		if(newState & (1 << SUP_RIFUNC_START_STOP)){
			// input high
			pInst->statusFlags |= SUP_STATUS_REMOTERESTR2;
			gRemoteRestricted = TRUE;
			SUP_CHARGING_ON_DO_OFF();
		}
		else{
			//input low
			pInst->statusFlags &= ~SUP_STATUS_REMOTERESTR2;
			gRemoteRestricted = FALSE;
		}
	}
	else{
		pInst->statusFlags &= ~SUP_STATUS_REMOTERESTR2;
		gStartStopFunc = FALSE;
		gRemoteRestricted = FALSE;
	}

	// Function High/low
	if(function & (1 << SUP_RIFUNC_HIGHLOW)){
		if(newState & (1 << SUP_RIFUNC_HIGHLOW)){
			// input high
			gLowCurrentMode = TRUE;
		}
		else{
			//input low
			gLowCurrentMode = FALSE;
		}
	}
	else{
		gLowCurrentMode = FALSE;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sup__updateRemoteOut
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Updates the remote out register value for TUI.
*
*	\param		pInst		Pointer to SUP instance.
*
*	\return		-
*
*	\details		
*
*	\note					
*
*******************************************************************************/

PUBLIC void sup__updateRemoteOut(
	sup__Inst *				pInst
) {
	Uint8					remoteOutFunc;
	Uint8					oldState;
	Uint8					newState;
	Uint8					manState;
	Uint8					F1_func;
	Uint8					F2_func;
	int						remoteOutToggle;

	newState = 0;
	remoteOutToggle = FALSE;

	reg_getLocal(&remoteOutFunc, reg_i_RemoteOut_func, pInst->instId);
	
	switch(remoteOutFunc)
	{
		case SUP_ROFUNC_NO_FUNC:
			break;

		case SUP_ROFUNC_ERROR:
		{
			Uint16 chalgError;
			Uint16 supError;
			ChargerStatusFields_type reguStatus;

			reg_get(&chalgError, sup__chalgError);
			reg_get(&reguStatus.Sum, sup__reguStatus);
			supError = pInst->statusFlags & SUP_ERROR_MASK;

			if (chalgError || reguStatus.Detail.Error.Sum || supError)
			{
				newState = 1;
			}
			break;
		}

		case SUP_ROFUNC_PHASE:
		{
			Uint8 remoteOutVar;
			Uint16 chalgStatus;

			reg_getLocal(&remoteOutVar, reg_i_RemoteOutPhase_var, pInst->instId);
			reg_get(&chalgStatus, sup__chalgStatus);

			if ((chalgStatus & CHALG_STAT_RESTRICTED) ||
				(chalgStatus & CHALG_STAT_PAUSE)
				) {
					newState = 0;
			}
			else
			{
				if (
					(chalgStatus & CHALG_STAT_PRE_CHARG) &&
					(remoteOutVar & SUP__ROVAR_PHASE_PRE)
				) {
					newState = 1;
				}

				if (
					(chalgStatus & CHALG_STAT_MAIN_CHARG) &&
					(remoteOutVar & SUP__ROVAR_PHASE_MAIN)
				) {
					newState = 1;
				}

				if (
					(chalgStatus & CHALG_STAT_ADDITIONAL_CHARG) &&
					(remoteOutVar & SUP__ROVAR_PHASE_ADDITIONAL)
				) {
					newState = 1;
				}

				if (
					(chalgStatus & CHALG_STAT_CHARG_COMPLETED) &&
					(remoteOutVar & SUP__ROVAR_PHASE_READY)
				) {
					newState = 1;
				}

				if (
					(chalgStatus & CHALG_STAT_EQUALIZE_CHARG) &&
					(remoteOutVar & SUP__ROVAR_PHASE_EQUALIZE)
				) {
					newState = 1;
				}

				if (!(chalgStatus & CHALG_STAT_BATTRY_CONN) &&
					(remoteOutVar & SUP__ROVAR_PHASE_IDLE)
				) {
					newState = 1;
				}
			}
			break;
		}

		case SUP_ROFUNC_BBC:
		{
			Uint16 chalgStatus;
			Uint8 remoteOutVar;
			Uint8 BBC;

			reg_get(&chalgStatus, sup__chalgStatus);
			reg_getLocal(&remoteOutVar, reg_i_RemoteOutBBC_var, pInst->instId);
			reg_get(&BBC, sup__BBC);

			if(remoteOutVar == SUP_ROFUNC_BBC_BBC_READY)
			{
				if(BBC && (chalgStatus & CHALG_STAT_CHARG_COMPLETED))
				{
					newState = 1;
				}
			}
			else
			{
				if(BBC)
				{
					newState = 1;
				}
			}
			break;
		}

		case SUP_ROFUNC_WATER:
		{
			Uint8 waterOn;

			reg_getLocal(&waterOn, reg_i_Water_on, pInst->instId);

			if(waterOn)
			{
				newState = 1;
			}
			break;
		}

		case SUP_ROFUNC_AIR:
		{
			Uint8 airPumpOn;

			reg_getLocal(&airPumpOn, reg_i_AirPump_on, pInst->instId);

			if(airPumpOn)
			{
				newState = 1;
			}
			break;
		}

		case SUP_ROFUNC_NO_ERROR:
		{
			Uint16 chalgError;
			ChargerStatusFields_type reguStatus;

			reg_get(&chalgError, sup__chalgError);
			reg_get(&reguStatus.Sum, sup__reguStatus);

			if (chalgError || reguStatus.Detail.Error.Sum)
			{
				newState = 0;
			}
			else{
				// No errors, set remote out
				newState = 1;
			}
			break;
		}

		case SUP_ROFUNC_CHARGING:
		{
			// On if engine is on or (SOC < 100 % and internal state = CHARGING)
			extern int chargingState;
			int stateCharging;
			int engineOn;

			// engine on (even slaves if parallel control)
			engineOn = (CcGetEnginesOn() >= 1) ? TRUE:FALSE;
			engineOn |= SUP_ENGINE_ON;

			// State == SUP_STATE_CHARGING && SOC < 99 (chargingState from TUI)
			stateCharging = (pInst->state == SUP_STATE_CHARGING) ? TRUE:FALSE;
			stateCharging &= (chargingState == TUI__CHARGING) ? TRUE:FALSE;

			if (engineOn || stateCharging)
			{
				newState = 1;
			}
			break;
		}

		case SUP_ROFUNC_RELAY_TOGGLE:
			remoteOutToggle = TRUE;
			break;

		/*case SUP_ROFUNC_ITHRESHOLD: // sorabITR - not now
		{
			Uint16 Ithreshold;

			Uint8 ITRcond;
			Uint32 Imeas = 0;

			Uint16 chalgStatus;
			reg_get(&chalgStatus, sup__chalgStatus);

			reg_getLocal(&Ithreshold, reg_i_Ithreshold_var, pInst->instId);
			reg_getLocal(&ITRcond, reg_i_Ithreshold_cond, pInst->instId);
			reg_get(&Imeas, sup__Ichalg);

			Uint32 Limit = (Uint32)(Ithreshold);

			if ((chalgStatus & CHALG_STAT_BATTRY_CONN) && Limit)
			{
				if ((chalgStatus & CHALG_STAT_RESTRICTED) ||
					(chalgStatus & CHALG_STAT_PAUSE)
				) {
					newState = 0;
				}
				else
				{
					if ((ITRcond == SUP_ROFUNC_ITHRESHOLD_CURRENTH ) &&
							(Imeas >= Limit))
					{
						newState = 1;
					}
					else if ((ITRcond == SUP_ROFUNC_ITHRESHOLD_CURRENTL) &&
							(Imeas < Limit))
					{
						newState = 1;
					}
				}
			}
			else
			{
				newState = 0;
			}

			break;
		}*/

		default:
			deb_assert(FALSE);
			break;
	}

	//Check remote out is manually set (from F1, F2 button)
	reg_getLocal(&F1_func, reg_i_ButtonF1_func, pInst->instId);
	reg_getLocal(&F2_func, reg_i_ButtonF2_func, pInst->instId);
	if((F1_func == SUP_FFUNC_REMOTEOUT) || (F2_func == SUP_FFUNC_REMOTEOUT))
	{
		reg_getLocal(&manState, reg_i_RemoteOut_man, pInst->instId);
		//Manual state overrides newState if set
		newState = manState;
	}

	reg_get(&oldState, sup__remoteOut);

	if (oldState != newState)
	{
		reg_put(&newState, sup__remoteOut);
	}

	// Update remote out toggle global variable
	gRemoteOutToggle = remoteOutToggle;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Equalize handling.
*
* \param		none
*
* \details
*
* \note
*
* \sa
*
******************************************************************************/
PUBLIC void sup__handleEqualize(
		sup__Inst *				pInst
) {
	/*
	 * Check if equalize should be active at end
	 * of this charging cycle
	 */
	Uint16 chalgStatus;
	Uint16 bmStatus;
	Uint8 EquOverride;
	Uint8 EquFunc;
	Uint8 EquActive;


	reg_getLocal(&EquOverride, reg_i_Equalize_override, pInst->instId);
	reg_getLocal(&EquFunc, reg_i_Equalize_func, pInst->instId);
	reg_getLocal(&EquActive, reg_i_Equalize_active, pInst->instId);
	reg_get(&chalgStatus, sup__chalgStatus);
	reg_get(&bmStatus, sup__BmStatus);


	if(!EquOverride)
	{

		if(chalgStatus & CHALG_STAT_BM_CONNTECTED)
		{
			if((bmStatus != 0xFFFF) && (bmStatus & RADIO_BMSTATUS_FUNC_EQUALIZE))
			{
				if(bmStatus & RADIO_BMSTATUS_NEED_EQUALIZE)
				{
					// Set Equalize active
					if(!EquActive)
					{
						EquActive = TRUE;
						EquOverride = TRUE;
						reg_putLocal(&EquOverride, reg_i_Equalize_override, pInst->instId);
						reg_putLocal(&EquActive, reg_i_Equalize_active, pInst->instId);
					}
				}
			}
		}
		else
		{

			switch(EquFunc)
			{
				case CHALG_EQU_FUNC_DISABLED:
				{
					break;
				}

				case CHALG_EQU_FUNC_CYCLIC:
				{
					Uint8 EquCycleInterval;
					Uint8 EquCycleCount;

					reg_getLocal(&EquCycleCount, reg_i_EquCycleCount, pInst->instId);
					reg_getLocal(&EquCycleInterval, reg_i_Equalize_var, pInst->instId);

					if(EquCycleCount == 0)
					{
						// Set Equalize active
						if(!EquActive)
						{
							EquActive = TRUE;
							EquOverride = TRUE;
							reg_putLocal(&EquOverride, reg_i_Equalize_override, pInst->instId);
							reg_putLocal(&EquActive, reg_i_Equalize_active, pInst->instId);
						}
					}

					break;
				}

				case CHALG_EQU_FUNC_WEEKDAY:
				{
					Posix_Time now;
					Uint8 Today;
					Uint8 WeekdaySet;

					// Get Weekday set to trigger equalize
					reg_getLocal(&WeekdaySet, reg_i_Equalize_var2, pInst->instId);

					// Get present day
					rtc_hwGetAsPosix(&now);
					Today = rtc_posixToWeekday(now);

					// Compare present day with day set to trigger
					if(Today == WeekdaySet)
					{
						// Set Equalize active
						if(!EquActive)
						{
							EquActive = TRUE;
							EquOverride = TRUE;
							reg_putLocal(&EquOverride, reg_i_Equalize_override, pInst->instId);
							reg_putLocal(&EquActive, reg_i_Equalize_active, pInst->instId);
						}
					}
					break;
				}

				default:
					break;

			}
		}
	}
}
