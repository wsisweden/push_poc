/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		rtc.c
*
*	\ingroup	SUP_ARM7
*
*	\brief		
*
*	\details	
*
*	\par		
*
*	\par		
*
*	\note		
*
*	\version	09-12-2009 / Antero Rintam�ki
*
*******************************************************************************/


/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/
#include "tools.h"
#include <time.h>
#include "rtc.h"


/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/
/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of RTC(Real Time Clock). 
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void rtc_hwInit(void)
{

}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_start
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts RTC
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void rtc_hwStart(void)
{
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_stop
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts RTC
*
*	\param		-
*
*	\return		-
*
*	\details	-
*
*	\note
*
*******************************************************************************/
PUBLIC void rtc_hwStop(void)
{
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_set
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets the time to RTC-registers.
*
*	\param		pTime	pointer to RTC_Time-structure that includes time-attributes
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/
PUBLIC void rtc_hwSet(
	RTC_Time *					pTime
) {
	// this should be empty!
}

PUBLIC void rtc_hwSetTime(
	RTC_Time *				pTime
) {
}

PUBLIC void rtc_hwSetDate(
	RTC_Time *				pTime
) {
}

PUBLIC void rtc_hwEnableIrq(void)
{
}

PUBLIC void rtc_hwDisableIrq(void)
{
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	rtc_get
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the time.
*
*	\param		pTime		Pointer to RTC_Time -structure. Function will modify 
*							given parameter to match current time at RTC.
*
*	\return		-
*
*	\details	-
*
*	\note		
*
*******************************************************************************/
PUBLIC void rtc_hwGet(
	RTC_Time *			pTime
) {
	time_t					time;
	struct tm				s_tm;
	
	_time64(&time);
	gmtime_s(&s_tm, &time);
	
	pTime->tm_year = s_tm.tm_year;
	pTime->tm_mon = s_tm.tm_mon;
	pTime->tm_mday = s_tm.tm_mday;
	pTime->tm_hour = s_tm.tm_hour;
	pTime->tm_min = s_tm.tm_min;
	pTime->tm_sec = s_tm.tm_sec;
}

