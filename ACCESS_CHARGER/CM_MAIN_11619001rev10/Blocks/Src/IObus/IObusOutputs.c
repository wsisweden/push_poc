#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "global.h"

#include "IObus.h"
#include "chalg.h"
#include "sup.h"
#include "tui.h"
#include "../Cc/ClusterControl.h"

#include "IObusOutputs.h"

static int outputStage(IoBusOutRegister_type select);
static int outputError(void);
static int outputBBC(IoBusOutRegister_type select);
static int outputWater(IoBusOutRegister_type select, int pin);
static int outputAir(IoBusOutRegister_type select, int pin);
static int outputCharging(void);

// Pin should be bounded to interval [0,6]
int getOutput(int pin, uint8_t* outputFunc) {
	// Default value here because default value in switch should be avoided
	int output = 0;
	const IoBusOutRegister_type Select = IoBusReg_aGet(pin);

	// Cast to correct type to get warning in case a value not handled
	switch (Select.signal) {
	case IObusOutput_Disabled:
		break;
	case IObusOutput_Alarm:
		output = outputError();
		*outputFunc |= IO_OUTFUNC_ALARM;
		break;
	case IObusOutput_Phase:
		output = outputStage(Select);
		*outputFunc |= IO_OUTFUNC_PHASE;
		break;
	case IObusOutput_BBC:
		output = outputBBC(Select);
		*outputFunc |= IO_OUTFUNC_BBC;
		break;
	case IObusOutput_ChalgOutWaterFill:
		output = outputWater(Select, pin);
		*outputFunc |= IO_OUTFUNC_WATER;
		break;
	case IObusOutput_ChalgOutAirpump:
		output = outputAir(Select, pin);
		*outputFunc |= IO_OUTFUNC_AIRPUMP;
		break;
	case IObusOutput_NoAlarm :
		output = !outputError();
		*outputFunc |= IO_OUTFUNC_NO_ALARM;
		break;
	case IObusOutput_Charging :
		output = outputCharging();
		*outputFunc |= IO_OUTFUNC_CHARGING;
		break;
	// Should not be set but needed for static calculation of length or last element
	case IObusOutput_Length:
		break;
	}

	return output;
}

IoBusOutRegister_type IoBusReg_aGet(int index) {
	IoBusOutRegister_type out = {0};
	uint32_t reg;

	reg_aGet(&reg, IObus_PinOutSelect, index);

	out.signal = reg >> 24;
	out.ChargingStages = reg & 0xFFFFFFUL;

	return out;
}

void IoBusReg_aPut(int index, IoBusOutRegister_type value) {
	uint32_t reg = 0;;

	reg |= value.signal << 24;
	reg |= value.ChargingStages & 0xFFFFFFUL;
	reg_aPut(&reg, IObus_PinOutSelect, index);
}

static int outputStage(IoBusOutRegister_type select) {
	int output = 0;
	Uint16 chalgStatus;

	reg_get(&chalgStatus, io__chalgStatus);

	if ((chalgStatus & CHALG_STAT_RESTRICTED) ||
		(chalgStatus & CHALG_STAT_PAUSE)) {
			output = 0;
	} else {
		if ((chalgStatus & CHALG_STAT_PRE_CHARG) &&
			(select.ChargingStages & (1 << IO__PHASE_PRE))) {
			output = 1;
		}

		if ((chalgStatus & CHALG_STAT_MAIN_CHARG) &&
			(select.ChargingStages & (1 << IO__PHASE_MAIN))) {
			output = 1;
		}

		if ((chalgStatus & CHALG_STAT_ADDITIONAL_CHARG) &&
			(select.ChargingStages & (1 << IO__PHASE_ADDITIONAL))) {
			output = 1;
		}

		if ((chalgStatus & CHALG_STAT_CHARG_COMPLETED) &&
			(select.ChargingStages & (1 << IO__PHASE_READY))) {
			output = 1;
		}

		if ((chalgStatus & CHALG_STAT_EQUALIZE_CHARG) &&
			(select.ChargingStages & (1 << IO__PHASE_EQUALIZE))) {
			output = 1;
		}

		if (!(chalgStatus & CHALG_STAT_BATTRY_CONN) &&
			(select.ChargingStages & (1 << IO__PHASE_IDLE))) {
			output = 1;
		}
	}

	return output;
}

static int outputError(void) {
	Uint16 chalgError;
	Uint16 supStatus;
	Uint16 supError;
	ChargerStatusFields_type reguStatus;

	reg_get(&chalgError, io__chalgError);
	reg_get(&reguStatus.Sum, io__reguStatus);

	/* Get supervision status from SUP */
	reg_get(&supStatus, io__statusFlags);
	supError = supStatus & SUP_ERROR_MASK;

	return (chalgError || reguStatus.Detail.Error.Sum || supError);
}

static int outputBBC(IoBusOutRegister_type select) {
	int output = 0;
	Uint16 chalgStatus;
	Uint8 BBC;

	reg_get(&chalgStatus, io__chalgStatus);
	reg_get(&BBC, io__BBC);

	if (select.BBCSettings == SUP_ROFUNC_BBC_BBC_READY) {
		if (BBC && (chalgStatus & CHALG_STAT_CHARG_COMPLETED)) {
			output = 1;
		}
	} else {
		if (BBC) {
			output = 1;
		}
	}

	return output;
}

static int outputWater(IoBusOutRegister_type select, int pin) {
	int output = 0;
	Uint8 waterActive;
	Boolean chalgWaterOn;
	Uint32 timer;

	reg_get(&waterActive, io__WaterPump_active);
	chalgWaterOn = chalg_GetWaterPumpOutput(); 						// Get Water function from Chalg - SORAB! Better to not use WaterPump_on register from sup1

	timer = IObusGetTimer(pin);

	/*
	 * select.Detail.var1 = WaterDuration
	 */

	if (waterActive && chalgWaterOn) {
		if (select.Detail.var1 == 0) {								// Water off (invalid values)
			output = 0;
		} else if (timer < (Uint32)(select.Detail.var1 * 60)) {		// Water on time
			output = 1;
		} else {													// Water off time
			output = 0;
		}
	}

	return output;
}

static int outputAir(IoBusOutRegister_type select, int pin) {
	int output = 0;
	Uint8 airActive;
	Boolean chalgAirOn;
	Boolean airPumpFailed;
	Uint32 timer;

	reg_get(&airActive, io__AirPump_active);
	chalgAirOn = chalg_GetAirPumpOutput(); 		// Get Airpump function from Chalg - SORAB! Do not use AirPump_on register from sup1
	airPumpFailed = chalg_GetAirPumpFailed();	// Get the error flag

	timer = IObusGetTimer(pin);

	/*
	 * select.Detail.var1 = AirPumpPumpOn
	 * select.Detail.var2 = AirPumpPumpCycle
	 */

	if (airActive & chalgAirOn) {
		if (select.Detail.var1 == 0 || select.Detail.var2 == 0) {
			// Pump off (invalid values)
			output = 0;
		} else if ((select.Detail.var1 == select.Detail.var2) &&
				(!airPumpFailed)) {	// And if hasn't failed
			// Pump constantly on
			output = 1;
		} else if ((timer < (Uint32)(select.Detail.var1 * 60)) &&
				(!airPumpFailed)) {	// And if hasn't failed
			// Pump on time
			output = 1;
		} else {
			// Pump off time
			output = 0;
		}
	}

	return output;
}

static int outputCharging(void) {
	// On if engine is on or (SOC < 100 % and internal state = CHARGING)
	extern int chargingState;
	int stateCharging;
	int engineOn;
	Uint8 supState;

	reg_get(&supState, io__supervisionState);

	// engine on (even slaves if parallel control)
	engineOn = (CcGetEnginesOn() >= 1) ? TRUE:FALSE;
	engineOn |= IO_ENGINE_ON;

	// State == SUP_STATE_CHARGING && SOC < 99 (chargingState from TUI)
	stateCharging = (supState == SUP_STATE_CHARGING) ? TRUE:FALSE;
	stateCharging &= (chargingState == TUI__CHARGING) ? TRUE:FALSE;

	return (engineOn || stateCharging);
}
