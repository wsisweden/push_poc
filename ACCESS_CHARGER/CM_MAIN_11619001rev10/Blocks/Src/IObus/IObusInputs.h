#ifndef IO_BUS_INPUTS_H
#define IO_BUS_INPUTS_H

#include <stdint.h>

#define CHAIN_LEN_INPUT_MAX 1

#define IO_IN_DISABLED 			0
#define IO_IN_STARTSTOP 		1
#define IO_IN_STOP 				2
#define IO_IN_HIGHLOW			3

#define IO_INFUNC_DISABLED	 	(1 << IO_IN_DISABLED)
#define IO_INFUNC_STARTSTOP 	(1 << IO_IN_STARTSTOP)
#define IO_INFUNC_STOP 			(1 << IO_IN_STOP)
#define IO_INFUNC_HIGHLOW		(1 << IO_IN_HIGHLOW)

typedef enum {IObusInput_Disabled = IO_IN_DISABLED,
			  IObusInput_StartStop = IO_IN_STARTSTOP,
			  IObusInput_Stop = IO_IN_STOP,
			  IObusInput_HighLow = IO_IN_HIGHLOW,
			  IObusInput_Length
} IObusInputs_enum;										// Should fit with names in GUI init.h

typedef union{
		IObusInputs_enum signal:8;
		union{
			int dummy:24;
		};
} IoBusInRegister_type;

void setInput(int pin, int level, uint8_t *inputFunc, uint8_t *input);
IoBusInRegister_type IoBusRegIn_aGet(int index);
void IoBusRegIn_aPut(int index, IoBusInRegister_type value);

#endif
