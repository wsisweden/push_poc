#ifndef IO_BUS_OUTPUTS_H
#define IO_BUS_OUTPUTS_H

#include <stdint.h>

#define IO_ENGINE_ON !(FIO2PIN & (1<<13))

#define IO_OUT_DISABLED 	0
#define IO_OUT_ALARM 		1
#define IO_OUT_PHASE 		2
#define IO_OUT_BBC 			3
#define IO_OUT_WATER 		4
#define IO_OUT_AIRPUMP 		5
#define IO_OUT_NO_ALARM 	6
#define IO_OUT_CHARGING 	7

#define IO_OUTFUNC_ALARM 	(1 << IO_OUT_ALARM)
#define IO_OUTFUNC_PHASE 	(1 << IO_OUT_PHASE)
#define IO_OUTFUNC_BBC 		(1 << IO_OUT_BBC)
#define IO_OUTFUNC_WATER 	(1 << IO_OUT_WATER)
#define IO_OUTFUNC_AIRPUMP 	(1 << IO_OUT_AIRPUMP)
#define IO_OUTFUNC_NO_ALARM (1 << IO_OUT_NO_ALARM)
#define IO_OUTFUNC_CHARGING (1 << IO_OUT_CHARGING)

// Should fit with names in GUI init.h
typedef enum {
			 IObusOutput_Disabled = IO_OUT_DISABLED,
			 IObusOutput_Alarm = IO_OUT_ALARM,
			 IObusOutput_Phase = IO_OUT_PHASE,
			 IObusOutput_BBC = IO_OUT_BBC,
			 IObusOutput_ChalgOutWaterFill = IO_OUT_WATER,
			 IObusOutput_ChalgOutAirpump = IO_OUT_AIRPUMP,
			 IObusOutput_NoAlarm = IO_OUT_NO_ALARM,
			 IObusOutput_Charging = 254,	//IO_OUT_CHARGING
			 IObusOutput_Length	= 8
} IObusOutputs_enum;

/*
 * Pressure sensor reference (PSref5V) is divided from 5V to 3V as follows:
 * PSref5V = 5, MCUref3V = 3
 * PSref3V = PSref5V*68/120 = 5*68/120
 * PSrefAdc = (PSref3V/MCUref3V)*4095 = ((5*68/120)/3)*4095 = 3867.5
 */
#define	IO_PSREF_ADC	3868


/**
 * \name	Remote out phase variable bits
 *
 * \brief	Variable bits for phase function.
 */
#define IO__PHASE_PRE			0
#define IO__PHASE_MAIN			1
#define IO__PHASE_ADDITIONAL	2
#define IO__PHASE_READY			3
#define IO__PHASE_EQUALIZE		4
#define IO__PHASE_IDLE			5

typedef enum {OutputStage_Pre = IO__PHASE_PRE,
			  OutputStage_Main = IO__PHASE_MAIN,
			  OutputStage_Additional = IO__PHASE_ADDITIONAL,
			  OutputStage_Ready = IO__PHASE_READY,
			  OutputStage_EQU = IO__PHASE_EQUALIZE,
			  OutputStage_Idle = IO__PHASE_IDLE,
			  OutputStage_Length
} OutputStage_type;

typedef enum {OutputBBC_BBC,
			  OutputBBC_BBC_Ready,
			  OutputBBC_Length
} OutputBBC_type;

typedef struct{
		IObusOutputs_enum signal:8;
		union{
			int ChargingStages:24;
			int BBCSettings:24;
			int WaterSettings:24;
			int AirpumpSettings:24;
			struct{
				uint8_t	var1;
				uint8_t	var2;
				uint8_t	var3;
			} Detail;
		};
} IoBusOutRegister_type;

IoBusOutRegister_type IoBusReg_aGet(int index);
void IoBusReg_aPut(int index, IoBusOutRegister_type value);
int getOutput(int pin, uint8_t* outputFunc);

#endif
