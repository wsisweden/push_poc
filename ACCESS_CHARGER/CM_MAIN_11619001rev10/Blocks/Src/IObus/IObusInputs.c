#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "global.h"

#include "IObus.h"
#include "IObusInputs.h"

// Pin should be bounded to interval [0,6]
void setInput(int pin, int level, uint8_t *inputFunc, uint8_t *input) {
	uint8_t function = 0;
	const IoBusInRegister_type Select = IoBusRegIn_aGet(pin);

	function = (uint8_t)Select.signal;
	// Get input pin function configuration
	*inputFunc |= 1 << function;

	// Cast to correct type to get warning in case a value not handled
	switch (Select.signal) {
		case IObusInput_Disabled:
			if (!level) {
				// Input is zero if one input is zero
				*input &= ~IO_INFUNC_DISABLED;
			}
			break;
		case IObusInput_StartStop:
			if (!level) {
				// Input is zero if one input is zero
				*input &= ~IO_INFUNC_STARTSTOP;
			}
			break;
		case IObusInput_Stop:
			if (!level) {
				// Input is zero if one input is zero
				*input &= ~IO_INFUNC_STOP;
			}
			break;
		case IObusInput_HighLow:
			if (!level) {
				// Input is zero if one input is zero
				*input &= ~IO_INFUNC_HIGHLOW;
			}
			break;
		case IObusInput_Length:
			break;
		// Note, no default to get warning in case of missing switch
	}
}

IoBusInRegister_type IoBusRegIn_aGet(int index) {
	IoBusInRegister_type in = {0};
	uint32_t reg;

	reg_aGet(&reg, IObus_PinInSelect, index);

	in.signal = reg >> 24;
	in.dummy = reg & 0xFFFFFFUL;

	return in;
}

void IoBusRegIn_aPut(int index, IoBusInRegister_type value) {
	uint32_t reg = 0;

	reg |= value.signal << 24;
	reg |= value.dummy & 0xFFFFFFUL;
	reg_aPut(&reg, IObus_PinInSelect, index);
}
