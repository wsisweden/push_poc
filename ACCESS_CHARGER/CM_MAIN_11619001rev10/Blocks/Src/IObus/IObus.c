#include "IObus.h"
#include "IoBusInputs.h"
#include "IoBusOutputs.h"

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "lib.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "swtimer.h"
#include "chalg.h"
#include "sup.h"
#include "Meas.h"

#include <string.h>
#include <stdbool.h>

typedef struct {
	IObus_Init const_P *pInit;	// Initialization data
	protif_Request spiRequest;	// Protocol handler request
	sys_FBInstId instId;
	uint16_t airPumpPressure;	// Pressure sensor measurement
	uint8_t outputFunc;			// Active output functions
	swtimer_Timer_st timer;
} IObus_Inst;

PRIVATE void IObus__coTask(osa_CoTaskType *);
PRIVATE void done(void);
PRIVATE void spi_writeData(IObus_Inst* pInst);
PRIVATE void SPI_callback(Uint8 retCode, protif_Request* pRequest);
PRIVATE Uint16 getAirPumpPressure(Uint16 Vout, Uint16 Vsrc);
PRIVATE void IObus__timer(swtimer_Timer_st * pTimer);

IObus_Inst*	IObus_pInstance;														// T-plate stuff
osa_CoTaskType coTaskIObus;															// Run periodically once each millisecond and must exit each time
PRIVATE const char TaskIObus_Name[] = "IObus";										// The CoTask must have a name

PRIVATE volatile Uint32 outputTimer[IO_CARD_OUTPUTS];								// Timer counter, one for each output
PRIVATE uint8_t send;																// All data in this array is sent periodically.
PRIVATE uint8_t receive;															// This array is filled with received data periodically
PRIVATE uint8_t buffer;																// Send data in buffer and overwrite with received data
PRIVATE uint16_t AdcIn;																// Analog value is stored here

PUBLIC void * IObus_init(sys_FBInstId iid, void const_P * pArgs) {
	IObus_Inst * pInst;

	memset((void*)outputTimer, 0, sizeof(outputTimer));
	send = 0x80 | (3 << 5); // Set Select Next high
	receive = 0xff;
	buffer = 0;

	/* T-plate stuff */
	pInst = (IObus_Inst *) mem_reserve(&mem_normal, sizeof(IObus_Inst));
	IObus_pInstance = pInst;
	pInst->pInit = (IObus_Init *) pArgs;

	pInst->spiRequest.fnCallback = &SPI_callback;
	pInst->spiRequest.pTargetInfo = pInst->pInit->pTargetInfo;
	pInst->spiRequest.pUtil = pInst;
	pInst->instId = iid;

	osa_newCoTask(&coTaskIObus, TaskIObus_Name, OSA_PRIORITY_NORMAL, IObus__coTask, 1024);
	swtimer_new(&pInst->timer, IObus__timer, 1000, 1000);
	pInst->timer.pUtil = pInst;
	swtimer_start(&pInst->timer);

	return ((void *) pInst);
}

PUBLIC void IObus_reset(void * instance, void const_P * pArgs) {
	(void)instance;
	(void)pArgs;
}

PUBLIC void IObus_down(void * instance, sys_DownType nType) {
	(void)instance;
	(void)nType;
}

PUBLIC sys_IndStatus IObus_read(void * instance, sys_IndIndex rIndex,
		sys_ArrIndex aIndex, void * pValue) {
	IObus_Inst* pInst = (IObus_Inst*)instance;
	sys_IndStatus ret = REG_OK;

	switch (rIndex)
	{
	case reg_indic_AirPumpPressureP:
		*(Uint16 *)pValue = pInst->airPumpPressure;
		break;
	case reg_indic_ExtraOut_func:
		*(Uint8 *)pValue = pInst->outputFunc;
		break;
	default:
		ret = REG_UNAVAIL;
		break;
	}

	return ret;
}

PUBLIC sys_IndStatus IObus_write(void * instance, sys_IndIndex rIndex,
		sys_ArrIndex aIndex) {
	(void)instance;
	(void)rIndex;
	(void)aIndex;

	return REG_OK;
}

PUBLIC void IObus_ctrl(void * instance, sys_CtrlType * pCtrl) {
	(void)instance;
	(void)pCtrl;
}

PUBLIC Uint16 IObus_test(void * instance) {
	(void)instance;

	return 0xFFFF;
}

PUBLIC uint8_t IObusGetChainLen(void) {
	return 1;
}

PUBLIC Uint32 IObusGetTimer(uint8_t pin) {
	return outputTimer[pin];
}

PUBLIC void IObusResetTimer(uint8_t pin) {
	outputTimer[pin] = 0;
}

PRIVATE void IObus__coTask(osa_CoTaskType *pCoTask) {
	// Indicate if T-plate is operational
	extern volatile int TplateOperational;
	extern int gTestMode;
	IObus_Inst* pInst = IObus_pInstance;
	static uint8_t first = 2;

	if (first) {
		first--;
		spi_writeData(IObus_pInstance);
	}

	if (TplateOperational && !gTestMode) {
		// Clock counter
		static uint_fast8_t clock = 0;

		if (clock > 0) {
			clock--;
		} else {
			// Use fixed pressure sensor reference voltage (10 measurements)
			Uint16 Vsrc = IO_PSREF_ADC * 10;

			// Turn up clock 20 ms
			clock = 20;

			AdcIn = meas_getIoBoardSum();
			pInst->airPumpPressure = getAirPumpPressure(AdcIn, Vsrc);

			// Initiate write and receive of data
			spi_writeData(IObus_pInstance);
		}
	}
}

PRIVATE void done(void) {
	// Must set all to one since result is anded (&)
	uint8_t input = 0xFF;
	uint8_t outputFunc = 0;
	uint8_t inputFunc = 0;
	static uint8_t inputOld = 0;
	static uint8_t inputFuncOld = 0;
	static uint8_t inputDebounce = 2;
	static bool first = true;
	IObus_Inst*	pInst = IObus_pInstance;

	if (first) {
		first = false;
		reg_putLocal(&input, reg_i_ExtraIn, pInst->instId);
	}

	// Should be low for inputs, received sent data?
	if (!(receive & 0xe0)) {
		uint8_t bit;

		// For all data bits except length detection bit
		for (bit = 0; bit < IO_CARD_INPUTS; ++bit) {
			// Bit mask for this bit
			const uint8_t bitMask = 1 << bit;

			// Take appropriate action for this received bit
			setInput(bit, receive & bitMask, &inputFunc, &input);

			if (getOutput(bit, &outputFunc)) {
				// Set to high
				send |= bitMask;
			} else {
				// Set to low
				send &= ~bitMask;
			}
		}
	}

	pInst->outputFunc = outputFunc;

	// Trigger input handler if function or input has changed
	if (inputFunc != inputFuncOld) {
		inputFuncOld = inputFunc;
		reg_putLocal(&inputFunc, reg_i_ExtraIn_func, pInst->instId);
	} else if (input != inputOld) {
		if (inputDebounce) {
			inputDebounce--;
		} else {
			inputOld = input;
			reg_putLocal(&input, reg_i_ExtraIn, pInst->instId);
		}
	} else {
		inputDebounce = 2;
	}
}

PRIVATE void spi_writeData(IObus_Inst* pInst) {
	void* pHandler;

	buffer = send;
	pInst->spiRequest.data.flags = PROTIF_FLAG_READ | PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = &buffer;
	pInst->spiRequest.data.size = 1;
	pInst->spiRequest.data.pNext = NULL;
	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}

/**
 * Transfer done
 */
PRIVATE void SPI_callback(Uint8 retCode, protif_Request* pRequest) {
	receive = buffer;
	done();
}

/*
 * Get air pump pressure from sensor according to formula:
 * P(kPa) = (Vout - Voffs) / (Vsrc * k)
 * Voffs = 0,2V (approximately)
 * k = 0.018
 * Return pressure in Pa
 */
PRIVATE Uint16 getAirPumpPressure(Uint16 Vout, Uint16 Vsrc) {
	const float k = 0.018f;													// proportional constant
	const int Voffs = 1450;													// 0,2V = 1450 bits in Adc
	float pressure = 0;														// calculated pressure in kPa
	static float pressureAvg = 0;											// average of sampled pressure in kPa
	static float pressureSum = 0;											// sum of sampled pressure in kPa
	static int sumCounter = 0;												// sample counter

	pressure = ((int)Vout - Voffs) / ((int)Vsrc * k);						// calculate pressure form measured voltage
	pressureSum += pressure;												// sum calculated pressure
	sumCounter++;

	if (sumCounter == 16) {
		pressureAvg = pressureSum / sumCounter;								// calculate average pressure from sum
		sumCounter = 0;
		pressureSum = 0;
	}

	return (Uint16)(pressureAvg * 1000);									// Convert from kPa(float) to Pa(Uint16)
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		SW timer callback.
*
* \param		pTimer		ptr to the expired timer
*
* \details
*
* \note
*
* \sa
*
******************************************************************************/
PRIVATE void IObus__timer(swtimer_Timer_st*	pTimer) {
	int pin;
	Uint8 airActive;
	Uint8 waterActive;
	Boolean chalgAirOn;
	Boolean chalgWaterOn;


	reg_get(&airActive, io__AirPump_active);
	reg_get(&waterActive, io__WaterPump_active);

	chalgAirOn = chalg_GetAirPumpOutput(); 		// Get Airpump function from Chalg - SORAB! Do not use AirPump_on register from sup1
	chalgWaterOn = chalg_GetWaterPumpOutput(); 	// Get Water function from Chalg - SORAB! Better to not use WaterPump_on register from sup1

	for (pin = 0; pin < IO_CARD_OUTPUTS; pin++) {
		const IoBusOutRegister_type IoBusReg =
				IoBusReg_aGet(pin);

		if (airActive && chalgAirOn && (IoBusReg.signal == IObusOutput_ChalgOutAirpump)) {
			outputTimer[pin] = (outputTimer[pin] + 1) %
					(Uint32)(IoBusReg.Detail.var2 * 60);
		} else if (waterActive && chalgWaterOn && (IoBusReg.signal == IObusOutput_ChalgOutWaterFill)) {
			outputTimer[pin] = outputTimer[pin] + 1;
		} else {
			outputTimer[pin] = 0;
		}
	}
}
