#ifndef IOBUS_INIT_H
#define IOBUS_INIT_H

#include "global.h"
#include "protif.h"

typedef struct {
	void const_P *			pTargetInfo;/**< Protocol specific target info  */
	protif_Interface const_P *pProtIf;	/**< Ptr to Protocol handler interface*/
	sys_FBInstId 			handlerId;	/**< Inst id of the protocol handler*/
} IObus_Init;

#endif

#ifndef EXE_MODE
#define EXE_GEN_IOBUS
#elif defined(EXE_GEN_IOBUS)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_IOBUS
#endif

#define EXE_APPL(n)			n##IObus

/********************************************************************/EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN /* Non-volatile numeric	*/
//#define EXE_USE_NS /* Non-volatile string	*/
#define EXE_USE_VN /* Volatile numeric		*/
//#define EXE_USE_VS /* Volatile string		*/
//#define EXE_USE_PN /* Process-point numeric	*/
//#define EXE_USE_PS /* Process-point string	*/
//#define EXE_USE_BL /* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name				Flags				Dim	Type	Def	Min	Max
 ---------------				-------------------	---	-------	---	---	----------*/
EXE_REG_NN( PinOutSelect,		SYS_REG_DEFAULT,	8, Uint32,	0,	0,	0xFFFFFFFFUL )				// Select which signal to output
EXE_REG_NN( PinInSelect,		SYS_REG_DEFAULT,	8, Uint32,	0,	0,	0xFFFFFFFFUL )				// Select signal for input
EXE_REG_PN( AirPumpPressureP,	SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF)
EXE_REG_NN( AirPumpAlarmLow,	SYS_REG_DEFAULT,	1,	Uint16,	5000,	0,	0xFFFF) 				// Air pump pressure low limit 5000Pa = 50cmVp
EXE_REG_NN( AirPumpAlarmHigh,	SYS_REG_DEFAULT,	1,	Uint16,	14000,	0,	0xFFFF)					// Air pump pressure high limit 14000Pa = 140cmVp
EXE_REG_PN( ExtraOut_func,		SYS_REG_DEFAULT,	1,	Uint8,		0,	0xFF)
EXE_REG_VN( ExtraIn_func,		SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFF)						// Extra input functions selected
EXE_REG_VN( ExtraIn,			SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFF)						// Extra input logic levels
/*******************************************************************************
 * HMI attributes
 */
EXE_MMI_NONE(PinOutSelect)
EXE_MMI_NONE(PinInSelect)
EXE_MMI_INT( AirPumpPressureP,		SYS_MMI_U_NONE,	tAirPumpPressure,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_INT( AirPumpAlarmLow,		SYS_MMI_U_NONE,	tAirPumpAlarmLow,		SYS_MMI_RW, SYS_Q_NONE)
EXE_MMI_INT( AirPumpAlarmHigh,		SYS_MMI_U_NONE,	tAirPumpAlarmHigh,		SYS_MMI_RW, SYS_Q_NONE)
EXE_MMI_NONE(ExtraOut_func)
EXE_MMI_NONE(ExtraIn_func)
EXE_MMI_NONE(ExtraIn)

/* References to external registers */
EXE_REG_EX( io__chalgStatus,		ChalgStatus,			chalg1 )
EXE_REG_EX( io__chalgError,			ChalgError,				chalg1 )
EXE_REG_EX( io__BBC,				BBC,					chalg1 )
EXE_REG_EX( IObus_PinOutSelect,		PinOutSelect,			IObus1 )
EXE_REG_EX( IObus_PinInSelect,		PinInSelect,			IObus1 )
EXE_REG_EX( io__reguStatus,			Status,					regu1 )
EXE_REG_EX( io__supervisionState,	supervisionState,		sup1 )
EXE_REG_EX( io__AirPump_active,		AirPump_active,			sup1 )
//EXE_REG_EX( io__AirPump_on,			AirPump_on,				sup1 )
EXE_REG_EX( io__WaterPump_active,	Water_active,			sup1 )
//EXE_REG_EX( io__WaterPump_on,		Water_on,				sup1 )
EXE_REG_EX( io__statusFlags,		statusFlags,			sup1 )

/******************************************************************************* 
 * read/write indications
 * (sorted alphabetically by instance name and by declaration order)
 */
EXE_IND_ME( AirPumpPressureP,		AirPumpPressureP				)
EXE_IND_ME( ExtraOut_func,			ExtraOut_func					)
/******************************************************************************* 
 * Language-dependent texts 
 *  - tExampleTxtHandle1 is reserved handlename and it's discarded by 
 *		textparser tools.
 *  - you can add txthandle comment at the end of the texthandle line, and this
 *		is parsed by textparser tools.
 */

EXE_TXT( tAirPumpPressure,	EXE_T_EN("Pressure\050Pa\051") EXE_T_ES("Presi\363n\050Pa\051") EXE_T_EN_US("Pressure\050Pa\051") EXE_T_PT("press\343o\050Pa\051") EXE_T_DE("Pumpendruck\050Pa\051") EXE_T_FI("") EXE_T_SE("Pumptryck\050Pa\051") EXE_T_IT("Pressione\050Pa\051") EXE_T_JP("")	"") /*Menu:Service->I/O control->Remote output Air->Settings (16)*/
EXE_TXT( tAirPumpAlarmLow,	EXE_T_EN("Alarm low\050Pa\051") EXE_T_ES("Falla baja\050Pa\051") EXE_T_EN_US("Alarm low\050Pa\051") EXE_T_PT("Falha baixa\050Pa\051") EXE_T_DE("Alarm nieder\050Pa\051") EXE_T_FI("") EXE_T_SE("Alarm l\345g\050Pa\051") EXE_T_IT("Allarm.bassa\050Pa\051") EXE_T_JP("")	"") /*Menu:Service->I/O control->Remote output Air->Settings (16)*/
EXE_TXT( tAirPumpAlarmHigh,	EXE_T_EN("Alarm high\050Pa\051") EXE_T_ES("Falla alta\050Pa\051") EXE_T_EN_US("Alarm high\050Pa\051") EXE_T_PT("Falha alta\050Pa\051") EXE_T_DE("Alarm hoch\050Pa\051") EXE_T_FI("") EXE_T_SE("Alarm h\366g\050Pa\051") EXE_T_IT("Allarme alta\050Pa\051") EXE_T_JP("")	"") /*Menu:Service->I/O control->Remote output Air->Settings (16)*/
/**********************************************************************/EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
