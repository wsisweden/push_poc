#include "tools.h"
#include "osa.h"

#include "IObusIsr.h"

extern osa_CoTaskType coTaskIObus;

inline void IObus_isr(void)
{
	osa_coTaskRunIsr(&coTaskIObus);
}
