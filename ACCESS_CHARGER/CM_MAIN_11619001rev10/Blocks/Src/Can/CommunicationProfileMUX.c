/*
 * CommunicationProfileMUX.c
 *
 * Change function calls depending on which communication profile is chosen.
 *
 *  Created on: 28 nov 2011
 *      Author: nicka
 */

#include "CommunicationProfileMux.h"
#include "MicroCANopenPlus/MCO/mco.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/mcop.h"

#include "can.h"
#include "sup.h"

#include "sys.h"
#include "osa.h"
#include "reg.h"

#include "Master/Master.h"
#include "Master/IoModule.h"
#include "Slave/Slave.h"
#include "Status/Status.h"
#include "NodesStatusAndMapping.h"									// Map between Node ID and other information such as serial number of nodes
#include "mpdo.h"

#include "Master/ScanNetwork.h"
#include "../Cc/ClusterControl.h"
#include "regu.h"
#include "math.h"
#include "../Regu/ControlLoop/sendToBattery.h"
#include "ConfigParameters.h"

enum emergency_enum {Emergency_AddressCollision,
					 Emergency_Unused,
					 Emergency_PauseOn,
					 Emergency_PauseOff,
};

extern enum can_CommProfile_enum CAN_CommProfile;
static int pauseWatchdog = 0;

static void EmergencyReceived(
  COBID_TYPE ID,
  UNSIGNED8 Len,
  UNSIGNED8 *pDat
  );

static void Sync();													// Received from an appropriate source
static void CAN_SendSync();
static void pause();
static void checkBattery(void);
void batteryConnect(int connect, int NodeId);
static void emergencyFilterReceiveAll();

int MCOUSER_ReceiveRaw(CAN_MSG MEM_FAR *pRPDO){
	int processed = 0;

	switch(CAN_CommProfile){
	case CAN_COMM_PROFILE_DISABLED :
		break;
	case CAN_COMM_PROFILE_MASTER :
		if(IoModuleReceiveRaw(pRPDO)){								// Received message for IO module ?
			processed = 1;											// Return value indicate message was handled here
		}
		else if(MasterReceiveRaw(pRPDO)){							// Returned value indicate if message was for master
			processed = 1;											// Return value indicate message was handled here
		}
		else{
			processed = mpdoDamReceive(pRPDO);						// Receive MPDO
		}
		break;
	case CAN_COMM_PROFILE_SLAVE :
		processed = mpdoDamReceive(pRPDO);						// Receive MPDO
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :
		if(MasterReceiveRaw(pRPDO)){								// Returned value indicate if message was for master
			processed = 1;											// Receive MPDO
		}
		else{
			processed = mpdoDamReceive(pRPDO);						// Receive MPDO
		}
		break;
	case CAN_COMM_PROFILE_STATUS :
		processed = mpdoDamReceive(pRPDO);						// Receive MPDO
		break;
	}																// Note, no default will give a warning in case of missing enumeration value

	return processed;												// Return value indicate is not handled here, note slave is handled by ordinary PDO handling in MICROCANOpen
}

void MCOUSER_RPDOReceived (
  UNSIGNED16 RPDONr, // RPDO Number
  UNSIGNED16 pRPDO,  // Offset to RPDO data in Process Image
  UNSIGNED8  len     // Length of RPDO
  )
{
	switch(CAN_CommProfile){										// Run charging algorithm if needed
	case CAN_COMM_PROFILE_DISABLED :								// CAN disabled
		break;
	case CAN_COMM_PROFILE_MASTER :									// Communication profile master
		break;
	case CAN_COMM_PROFILE_SLAVE :									// Communication profile slave
		if(ReceiveSlave(RPDONr, pRPDO, len)){
			ChargerSet_type input;

			if(slaveGetInput(&input)){
				regu_setInput(&input);
				if(input.Uact.off != BatterydetectCond_NotUsed){
					Interval_type disconnect;

					slaveGetUactDisconnect(&disconnect);
					regu_setUactDisconnect(&disconnect);
				}
				if(input.Uact.on != BatterydetectCond_NotUsed){
					Interval_type connect;

					slaveGetUactConnect(&connect);
					regu_setUactConnect(&connect);
				}
			}
		}
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :						// Communication profile master external IO
		if(ReceiveSlave(RPDONr, pRPDO, len)){
			ChargerSet_type input;

			if(slaveGetInput(&input)){
				extern volatile ChargerSet_type CanToCc;

				CanToCc = input;
				if(input.Uact.off != BatterydetectCond_NotUsed){
					Interval_type disconnect;

					slaveGetUactDisconnect(&disconnect);
					regu_setUactDisconnect(&disconnect);
				}
				if(input.Uact.on != BatterydetectCond_NotUsed){
					Interval_type connect;

					slaveGetUactConnect(&connect);
					regu_setUactConnect(&connect);
				}
			}
		}
		break;
	case CAN_COMM_PROFILE_STATUS :									// Communication profile Status
		break;
	}																// Note, no default will give a warning in case of missing enumeration value
}

void MCOUSER_EmergencyReceived(
  COBID_TYPE ID,
  UNSIGNED8 Len,
  UNSIGNED8 *pDat
  ){
	switch(CAN_CommProfile){										// Run charging algorithm if needed
	case CAN_COMM_PROFILE_DISABLED :								// CAN disabled
		break;
	case CAN_COMM_PROFILE_MASTER :									// Communication profile master
		EmergencyReceived(ID, Len, pDat);
		break;
	case CAN_COMM_PROFILE_SLAVE :									// Communication profile slave
		EmergencyReceived(ID, Len, pDat);
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :						// Communication profile master external IO
		EmergencyReceived(ID, Len, pDat);
		break;
	case CAN_COMM_PROFILE_STATUS :									// Communication profile Status
		EmergencyReceived(ID, Len, pDat);
		break;
	}																// Note, no default will give a warning in case of missing enumeration value
}

void CAN_Operational(int oneSecond, int tpdoSet) {
	/* Check heart beat */
	{
		Uint8 ParallelControl_Mode;

		reg_get(&ParallelControl_Mode, can_ParallelControl);

		switch(CAN_CommProfile){
		case CAN_COMM_PROFILE_DISABLED :							// CAN disabled
			break;
		case CAN_COMM_PROFILE_MASTER :								// Communication profile master
			pause();
			checkBattery();
			if(oneSecond){
				CAN_SendSync();										// Send sync signal
				SendIoModule();										// IO module
				{
					extern volatile ChargerMeas_type CcToCan;
					const ChargerMeas_type tmp = CcToCan;

					SyncSlave(&tmp);								// Send measurements of sum from cluster control
				}
			}
			if(ParallelControl_Mode){
				if(tpdoSet){
					SendUactInterval();								// Send voltage detection interval
					SendMaster(1);									// Send messages to other chargers
				}
				CAN_ScanNetwork();									// Look for available nodes
			}
			break;
		case CAN_COMM_PROFILE_SLAVE :								// Communication profile slave
			pause();
			checkBattery();
			break;
		case CAN_COMM_PROFILE_MASTER_CAN_INPUT :					// Communication profile master
			pause();
			checkBattery();
			if(tpdoSet){
				extern volatile ChargerMeas_type CcToCan;
				const ChargerMeas_type tmp = CcToCan;

				SyncSlave(&tmp);									// Send measurements of sum from cluster control
			}
			if(ParallelControl_Mode){
				if(tpdoSet){
					SendMaster(0);									// Send messages to other chargers
				}
				CAN_ScanNetwork();									// Look for available nodes
			}
			break;
		case CAN_COMM_PROFILE_STATUS :								// CAN status
			//pause();
			//checkBattery();
			break;
		}
	}																// Note, no default will give a warning in case of missing enumeration value
}

void MCOUSER_ResetPDO (void)
{
    //Initialization of PDOs comes from EDS

	switch(CAN_CommProfile){
	case CAN_COMM_PROFILE_DISABLED :								// CAN disabled
		break;
	case CAN_COMM_PROFILE_MASTER :									// Communication profile master
		IoModuleInit();
		MasterInit();
		{
			extern MCO_CONFIG MEM_FAR gMCOConfig;
			MCO_InitTPDO(1, NODEID+0x40000180, 0, 0, 8, P201201_UMEAS);
			MCO_InitTPDO(2, NODEID+0x40000280, 0, 0, 8, P201203_PMEAS);
		}
		emergencyFilterReceiveAll();
		MCOHW_SetCANFilter(0x580);
		break;
	case CAN_COMM_PROFILE_SLAVE :									// Communication profile slave
		SlaveInit();
		emergencyFilterReceiveAll();
		MCOHW_SetCANFilter(0x580);
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :						// Communication profile master
		SlaveInit();
		MasterInit();
		emergencyFilterReceiveAll();
		MCOHW_SetCANFilter(0x580);
		break;
	case CAN_COMM_PROFILE_STATUS :									// Communication profile status
		StatusInit();
		emergencyFilterReceiveAll();
		MCOHW_SetCANFilter(0x580);
		break;
	}																// Note, no default will give a warning in case of missing enumeration value

	{
		const uint8_t tmp = CAN_CommProfile;
		PI_WRITE(PIACC_PDO, P201003_COMMUNICATION_PROFILE, &tmp, 1);
	}

#if USE_STORE_PARAMETERS
    MCOSP_GetStoredParameters();
#endif
}

void MCOUSER_NMTChange(UNSIGNED8 state){
	switch(state){
	case NMTSTATE_OP :													// Switched to state operational
		switch(CAN_CommProfile){
		case CAN_COMM_PROFILE_DISABLED :								// CAN disabled
			break;
		case CAN_COMM_PROFILE_MASTER :									// Communication profile master
			break;
		case CAN_COMM_PROFILE_SLAVE :									// Communication profile slave
			break;
		case CAN_COMM_PROFILE_MASTER_CAN_INPUT :						// Communication profile master
			break;
		case CAN_COMM_PROFILE_STATUS :									// Communication profile status
			break;
		}																// Note, no default will give a warning in case of missing enumeration value
		break;
	case NMTSTATE_PREOP :												// Switched to state pre-operational
		if(CAN_CommProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){		// Communication profile master external input ?
			Uint32 SerialNrThis;										// Serial number for this charger
			int n;

			reg_get(&SerialNrThis, can__SerialNo);						// Get serial number for this charger
			for(n = 0; n < CC_CHARGERS_LEN; n++){						// For all positions in cluster control
				volatile const ChargerSet_type* Set;
				const uint32_t SerialNr = CcSet(n, &Set);				// Serial number for this position

				if(SerialNr){											// Found a charger ?
					if(SerialNr == SerialNrThis){						// This charger ?
						continue;
					}
					else{
						const int NodeId = CAN_NodeStatusAndMapping_GetNodeId(SerialNr);
						if(NodeId){
							MasterSendPreOperational(NodeId);			// Send network management message to switch to pre-operational for this node
						}
					}
				}
			}
		}

		{
			/* Reset set values when no set values received */
			extern volatile ChargerSet_type CanToCc;
			extern volatile ChargerMeas_type CcToCan;
			extern volatile ChargerSet_type CanSet;
			extern volatile ChargerMeas_type CanMeas;

			CanToCc.Set.U = 0;
			CanToCc.Set.I = 0;
			CanToCc.Set.P = 0;
			CanToCc.Mode = 0;
			CanToCc.Uact.both = 0;
			CanToCc.RemoteIn.both = 0;
			CanToCc. soc = 0;
			CcToCan.Meas.U = 0;
			CcToCan.Meas.I = 0;
			CcToCan.Meas.P = 0;
			CcToCan.Status.Detail.Derate.Sum = 0;
			CcToCan.Status.Detail.Warning.Sum = 0;
			CcToCan.Status.Detail.Error.Sum = 0;

			CanSet = CanToCc;
			CanMeas = CcToCan;
		}
		break;
	}
}

void CAN_1s(){														// Internal in every operational state
	switch(CAN_CommProfile){										// Run charging algorithm if needed
	case CAN_COMM_PROFILE_DISABLED :								// CAN disabled
	case CAN_COMM_PROFILE_MASTER :									// Communication profile master
	case CAN_COMM_PROFILE_STATUS :									// Communication profile status
		Sync();														// Synchronize from internal source
		break;
	case CAN_COMM_PROFILE_SLAVE :									// Communication profile slave
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :						// Communication profile master CAN input
		if(!slaveWatchdog()){											// Run slave watch dog
			extern volatile ChargerSet_type CanToCc;
			extern volatile ChargerMeas_type CcToCan;
			extern volatile ChargerSet_type CanSet;
			extern volatile ChargerMeas_type CanMeas;

			CanToCc.Mode = ReguMode_Off;
			CanToCc.Set.U = NAN;
			CanToCc.Set.I = NAN;
			CanToCc.Set.P = NAN;
			CanToCc.soc = 0;
			CcToCan.Meas.U = 0;
			CcToCan.Meas.I = 0;
			CcToCan.Meas.P = 0;
			CcToCan.Status.Detail.Derate.Sum = 0;
			CcToCan.Status.Detail.Warning.Sum = 0;
			CcToCan.Status.Detail.Error.Sum = 0;

			CanSet = CanToCc;
			CanMeas = CcToCan;
		}
		break;
	}																// Note, no default will give a warning in case of missing enumeration value

	WatchdogIoModule();												// Run IO module watch dog
}

void MCOUSER_SYNCReceived(){										// Received from CAN interface
	switch(CAN_CommProfile){
	case CAN_COMM_PROFILE_DISABLED :								// CAN disabled
	case CAN_COMM_PROFILE_MASTER :									// Communication profile master
		break;
	case CAN_COMM_PROFILE_SLAVE :									// Communication profile slave
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :						// Communication profile master CAN input
		Sync();														// Synchronize from external source
		break;
	case CAN_COMM_PROFILE_STATUS :									// Communication profile status
		{
			ChargerMeas_type meas = CcMeasGetThis();				// Get measurements from latest internal sync
			SyncStatus(&meas);										// Send local measurements
		}
		break;
	}																// Note, no default will give a warning in case of missing enumeration value
}

static void Sync(){													// Received from an appropriate source
	ChargerMeas_type meas;
	int dummy;

	MeasGetAverageSinceLast(&meas);									// Measure average since last time this function was called
	reg_put(&dummy, canRegu_NewSet);								// Start to use new value, write dummy value to trigger event

	switch(CAN_CommProfile){
	case CAN_COMM_PROFILE_DISABLED :								// CAN disabled
		reg_put(&dummy, can__CcSync);								// Trigger execution of cluster control and charging algorithm, write dummy value to trigger event
		break;
	case CAN_COMM_PROFILE_MASTER :									// Communication profile master
		reg_put(&dummy, can__CcSync);								// Trigger execution of cluster control and charging algorithm, write dummy value to trigger event
		break;
	case CAN_COMM_PROFILE_SLAVE :									// Communication profile slave
		SyncSlave(&meas);											// Send local measurements
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :						// Communication profile master CAN input
		{
			extern MCO_CONFIG MEM_FAR gMCOConfig; 					// must be able to access this to access MY_NMT_STATE
			if(MY_NMT_STATE == NMTSTATE_OP){
				reg_put(&dummy, can__CcSync);						// Trigger execution of cluster control and charging algorithm, write dummy value to trigger event
			}
		}
		break;
	case CAN_COMM_PROFILE_STATUS :									// CAN profile status
		reg_put(&dummy, can__CcSync);								// Trigger execution of cluster control and charging algorithm, write dummy value to trigger event
		break;
	}																// Note, no default will give a warning in case of missing enumeration value
	CcMeasSetThis(&meas);
}

static void CAN_SendSync(){
	static CAN_MSG SyncMessage = { 0x80, { { 0, 0, 0, 0, 0, 0, 0, 0 } }, 0 };	// synchronization message
	MCOHW_PushMessage(&SyncMessage);								// Send synchronization message
}

static void EmergencyReceived(
  COBID_TYPE ID,
  UNSIGNED8 Len,
  UNSIGNED8 *pDat
  ){
	switch(pDat[1]){
	case 0x81 :															// Communication
		switch(pDat[0]){
		case 0x50 :														// Transmit COB-ID collision
			MCOUSER_ResetCommunication(0);								// Reset communication to currently selected communication profile
			break;
		}
		break;
	case 0xFF :
		{
			const enum emergency_enum Emergency = pDat[0];

			switch(Emergency){
			case Emergency_AddressCollision : 							// Address collision change address and remove other node from list
				{
					extern MCO_CONFIG MEM_FAR gMCOConfig;

					if(ID == (unsigned)0x80 + MY_NODE_ID){
						MCOUSER_ResetCommunication(0);					// Reset communication to currently selected communication profile
					}
				}
				break;
			case Emergency_Unused : 									// Just placed in middle to keep other numbers the same as before this was removed
				break;
			case Emergency_PauseOn :
				{
					pauseWatchdog = 500;
					Uint8 Blinkstate = 1;
					reg_put(&Blinkstate, can__Blinkstate);				// Sync blinking

					msg_sendTry(
						MSG_LIST(statusRep),
						PROJECT_MSGT_STATUSREP,
						SUP_STAT_STOP_CHARGING
					);
				}
				break;
			case Emergency_PauseOff :
				{
					msg_sendTry(
						MSG_LIST(statusRep),
						PROJECT_MSGT_STATUSREP,
						SUP_STAT_START_CHARGING
					);
				}
				break;
			}
			break;
		}
	}
}

static void pause(){
#include "global.h"
	extern volatile int SUP_pauseOn;
	extern volatile int SUP_pauseOff;								// Use this to avoid more than one message

	if(SUP_pauseOn){												// Stop button pressed ?
		SUP_pauseOn = 0;
		MCOP_PushEMCY(0xFF00 + Emergency_PauseOn, 0, 0 ,0 ,0, 0);	// Send pause message
	}
	else if(SUP_pauseOff){											// Esc button pressed ?
		SUP_pauseOff = 0;
		MCOP_PushEMCY(0xFF00 + Emergency_PauseOff, 0, 0 ,0 ,0, 0);	// Send pause off message
	}
}

static void checkBattery(void){
	int batt;
	{
		extern volatile int BatteryConnected;

		batt = BatteryConnected;													// Read once
	}
	static int BatteryConnectedOld = 0;

	if(batt != BatteryConnectedOld){												// Connect flank ?
		ChargerMeas_type meas;

		PI_READ(PIACC_PDO, P201204_STATUS, &meas.Status.Sum, 4);					// Read status field from process image
		if(batt){																	// Battery connected
			meas.Status.Detail.Derate.Detail.NoLoad = 0;
		}
		else{
			meas.Status.Detail.Derate.Detail.NoLoad = 1;
		}
		PI_WRITE(PIACC_PDO, P201204_STATUS, &meas.Status.Sum, 4);					// Write status field to process image
		mpdoSamSend(0x2012, 0x04, P201204_STATUS);									// Send status field

		if(!batt){
			ConfigParametersBattDisconnected();
		}
	}

	BatteryConnectedOld = batt;
}

static void emergencyFilterReceiveAll(){
	int n;

	for(n = 1; n < 127; n++){
		extern MCO_CONFIG MEM_FAR gMCOConfig;

		MCOHW_SetCANFilter(0x80 + n);
	}
}
