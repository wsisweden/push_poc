#include "mpdo.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/mcop.h"
#include "ConfigParameters.h"

void mpdoSamSend(int idx, int subIndex, int offset){
	extern MCO_CONFIG MEM_FAR gMCOConfig;
	CAN_MSG MPDO;

	MPDO.ID = (unsigned)0x580;
	MPDO.LEN = 8;
	MPDO.BUF.BUF[0] = MY_NODE_ID;
	MPDO.BUF.BUF[1] = idx;
	MPDO.BUF.BUF[2] = idx >> 8;
	MPDO.BUF.BUF[3] = subIndex;
	PI_READ(PIACC_PDO, offset, &MPDO.BUF.BUF[4], 4);			// Read data from process image

	MCOHW_PushMessage(&MPDO);
}

void mpdoDamSend(uint8_t NodeId, int idx, int subIndex, uint8_t data[4]){
	extern MCO_CONFIG MEM_FAR gMCOConfig;
	CAN_MSG MPDO;

	MPDO.ID = (unsigned)0x580;
	MPDO.LEN = 8;
	MPDO.BUF.BUF[0] = 0x80 | NodeId;
	MPDO.BUF.BUF[1] = idx;
	MPDO.BUF.BUF[2] = idx >> 8;
	MPDO.BUF.BUF[3] = subIndex;
	MPDO.BUF.BUF[4] = data[0];
	MPDO.BUF.BUF[5] = data[1];
	MPDO.BUF.BUF[6] = data[2];
	MPDO.BUF.BUF[7] = data[3];

	MCOHW_PushMessage(&MPDO);
}

int mpdoDamReceive(CAN_MSG MEM_FAR *pRPDO){
	int retval;

	if(((pRPDO->ID & ~(0x7F)) == 0x580) && (pRPDO->BUF.BUF[0] & 0x80)){					// MPDO and DAM? Master and slave
		const int NodeId  = pRPDO->BUF.BUF[0] & 0x7F;
		extern MCO_CONFIG MEM_FAR gMCOConfig;

		if(NodeId == 0 || NodeId == MY_NODE_ID){										// Addressed to this node
			if(pRPDO->LEN == 8){														// Correct length ?
				const uint16_t idx = (pRPDO->BUF.BUF[2] << 8) + pRPDO->BUF.BUF[1];
				const uint8_t subIndex = pRPDO->BUF.BUF[3];
			    const uint16_t found = MCO_SearchODProcTable(idx, subIndex);

			    if (found == 0xFFFF){													// Did not found index and sub index ?
					MCOP_PushEMCY(0x8210, 0, 0 ,0 ,0, 0);								// 0x82xx = Protocol error
			    }
			    else{
			    	extern OD_PROCESS_DATA_ENTRY MEM_CONST gODProcTable[];

			    	PI_WRITE(PIACC_PDO, gODProcTable[found].offset, &pRPDO->BUF.BUF[4], gODProcTable[found].len);
			    }

			    switch(idx){
			    case 0x2010 :															// Configuration parameters
					{
						ConfigParametersReceive(subIndex);
					}
			    	break;
			    }
			}
			else{
				MCOP_PushEMCY(0x8210, 0, 0 ,0 ,0, 0);									// 0x8210 = PDO not processed due to length error
			}
		}
	}
	else{
		switch(pRPDO->ID & ~(0x7F)){
			case 0x400 :
				{
					extern int nodeIdMaster;
					nodeIdMaster = pRPDO->ID & 0x7F;											// Store nodeId of master
				}
				break;
		}
		retval = 0;
	}

	return retval;
}
