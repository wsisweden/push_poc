/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		isr.C
*
*	\ingroup	MEAS
*
*	\brief		Contains the code that needs to be executed at 1ms intervalls.
*
*	\details
*
*	\note
*
*	\version	**-**-2009 / Ari Suomi
*
*******************************************************************************/
#include "tools.h"
#include "osa.h"

#include "canIsr.h"

extern osa_CoTaskType coTaskCAN;

/*	
 *  This code is to be included inside the 1ms ISR
 */
inline void can_isr(void)
{
	osa_coTaskRunIsr(&coTaskCAN);
}
