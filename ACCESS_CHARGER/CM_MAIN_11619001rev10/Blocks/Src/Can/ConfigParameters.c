#include "mpdo.h"
#include "MicroCANopenPlus/MCO_DS401_LPC1768/procimg.h"
#include "../Regu/ControlLoop/sendToBattery.h"
#include "global.h"
#include "sup.h"


static void ConfigInvalidate(void);
static void receivedData(void);

static int dataEventReceived[2] = {0 , 0};

extern UNSIGNED8 MEM_NEAR gProcImg[];

void ConfigParametersReceive(uint8_t subIndex){
	switch(subIndex){
	case 1 :																			// Configuration date
		break;
	case 2 :																			// Configuration time
		break;
	case 3 :																			// Communication profile
		ConfigInvalidate();
		break;
	case 4 :																			// Data to send to battery, first four bytes
		ConfigInvalidate();
		dataEventReceived[0] = 1;
		receivedData();
		break;
	case 5 :																			// Data to send to battery, second, last two bytes
		ConfigInvalidate();
		dataEventReceived[1] = 1;
		receivedData();
		break;
	}
}

static void ConfigInvalidate(void){
	const uint32_t zero = 0;

	PI_WRITE(PIACC_PDO, P201001_CONFIGURATION_DATE, &zero, 4);							// \ Invalidate configuration
	PI_WRITE(PIACC_PDO, P201002_CONFIGURATION_TIME, &zero, 4);							// /
}

void ConfigParametersBattDisconnected(void){
	extern volatile batteryCommunication_type dataToBattery;

	dataEventReceived[0] = 0;															// Not received via event
	dataEventReceived[1] = 0;															// Not received via event

	dataToBattery.Valid = 0;															// Invalidate data
}

static void receivedData(void){
	if(dataEventReceived[0] && dataEventReceived[1]){
		batteryCommunication_type data;

		PI_READ(PIACC_PDO, P201004_SEND_TO_BATTERY_1, &data.raw[0], 4);					// Read data to send to battery from process image
		PI_READ(PIACC_PDO, P201005_SEND_TO_BATTERY_2, &data.raw[4], 4);					// Read data to send to battery from process image

		switch(data.raw[0]){
			case 0x10:
			{
				extern volatile batteryCommunication_type dataToBattery;

				// Build data byte for byte
				dataToBattery.PanId = ((Uint16) data.raw[1]) << 8;
				dataToBattery.PanId |= ((Uint16) data.raw[2]);
				dataToBattery.Channel = data.raw[3];
				dataToBattery.NodeId = ((Uint16) data.raw[4]) << 8;
				dataToBattery.NodeId |= ((Uint16) data.raw[5]);
				dataToBattery.Valid = ((int)data.raw[6]);									// Data is now set to valid value
				break;
			}
			case 0x31:
			{
				BmCalibration_t sBmCalibrationReadFromSlave;

				sBmCalibrationReadFromSlave.CalibrationPoint1 = ((Int32)data.raw[4]) << 24;
				sBmCalibrationReadFromSlave.CalibrationPoint1 |= ((Int32)data.raw[5]) << 16;
				sBmCalibrationReadFromSlave.CalibrationPoint1 |= ((Int32)data.raw[6]) << 8;
				sBmCalibrationReadFromSlave.CalibrationPoint1 |= (Int32)data.raw[7];
				bmCalibration1SetFromSlave(&sBmCalibrationReadFromSlave);									// Set data to send in radio message
				break;
			}
			case 0x32:
			{
				BmCalibration_t sBmCalibrationReadFromSlave;

				sBmCalibrationReadFromSlave.CalibrationPoint2 = ((Int32)data.raw[4]) << 24;
				sBmCalibrationReadFromSlave.CalibrationPoint2 |= ((Int32)data.raw[5]) << 16;
				sBmCalibrationReadFromSlave.CalibrationPoint2 |= ((Int32)data.raw[6]) << 8;
				sBmCalibrationReadFromSlave.CalibrationPoint2 |= (Int32)data.raw[7];
				bmCalibration2SetFromSlave(&sBmCalibrationReadFromSlave);									// Set data to send in radio message
				break;
			}
		}
		dataEventReceived[0] = 0;														// Not received via event
		dataEventReceived[1] = 0;														// Not received via event
	}
	else if(dataEventReceived[1]){
		Uint8 data[4];

		PI_READ(PIACC_PDO, P201005_SEND_TO_BATTERY_2, &data[0], 4);					// Get data from CAN message

		switch(data[0]){
			case 0x20:
			{
				msg_sendTry(
					MSG_LIST(statusRep),
					PROJECT_MSGT_STATUSREP,
					SUP_STAT_BM_INIT2
				);
				break;
			}
			case 0x21:
			{
				CurrentCombination_t CurrentCombination;

				CurrentCombination.CurrentLevel1 = data[1];
				CurrentCombination.CurrentLevel2 = data[2];
				CurrentCombination.CurrentLevel3 = data[3];
				sendToBattery2Set(&CurrentCombination);										// Set data to send in radio message
				break;
			}
			case 0x22:
			{
				extern volatile batteryCommunication_type dataToBattery;
				dataToBattery.Valid = 0;													// Set REGU state to end battery communication
				break;
			}
			case 0x30:
			{
				extern volatile batteryCommunication_type dataToBattery;
				dataToBattery.Valid = 3;													// Set REGU state to calibration
				break;
			}
			case 0x33:
			{
				bmCalibrationEndFromSlave();															// Inform Master that calibration
				break;
			}
			case 0x3F:
			{
				bmCalibrationAckFromMaster();													// Inform slave that calibration values have been successfully sent
				break;
			}
			case 0x40:
			{
				msg_sendTry(
					MSG_LIST(statusRep),
					PROJECT_MSGT_STATUSREP,
					SUP_STAT_VSENSE
				);
				break;
			}
		}
		dataEventReceived[0] = 0;														// Not received via event
		dataEventReceived[1] = 0;														// Not received via event
	}
}
