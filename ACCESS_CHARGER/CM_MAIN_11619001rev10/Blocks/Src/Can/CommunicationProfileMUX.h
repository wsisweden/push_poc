/*
 * CommunicationProfileMUX.h
 *
 *  Created on: 28 nov 2011
 *      Author: nicka
 */

#ifndef COMMUNICATIONPROFILEMUX_H_
#define COMMUNICATIONPROFILEMUX_H_

#include "cai_cc.h"

/* Disabled:
 *   No communication occur.
 *
 * Master:
 *   Signals from the charging algorithm are connected to the CAN bus. All signals including local signals sent directly are also
 *   sent in parallel via the CAN bus. Signals to from the IO module are also sent.
 *
 * Slave:
 *   The charging algorithm is not run and the signals to the regulator is instead connected to the CAN bus.
 *
 * Master CAN IO:
 *   The same as master although the charging algorithm is not run and the signals are instead received via the CAN bus.
 *
 * Note master CAN IO and slave protocols are exactly the same.
 *
 */

/* Regulator may connected to:
 *   1. Charging algorithm.
 *   2. Cluster control.
 *   3. CAN.
 *
 * Cluster control input may be connected to:
 *   0. Bypassed.
 *   1. Charging algorithm.
 *   2. CAN.
 *   Output is always connected to CAN and to the local regulator.
 *
 *
 */

void CAN_Operational(int oneSecond, int tpdoSet);			// Called in operational

void CAN_1s();												// Called at internal sync

void MeasGetAverageSinceLast(ChargerMeas_type* meas);

#endif /* COMMUNICATIONPROFILEMUX_H_ */
