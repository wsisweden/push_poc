#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/lss.h"
#include "bootloaderDownload.h"
#include "can.h"

extern UNSIGNED8 MEM_NEAR gProcImg[];

static void bootloaderDownloadStoreIds(UNSIGNED8* This, UNSIGNED8 LocalNodeId);
static int bootloaderDownloadIdUsed(UNSIGNED8* This, UNSIGNED8 LocalNodeId);

void CAN_bootloaderUpload(COBID_TYPE ID, UNSIGNED8 Len, UNSIGNED8 *pDat)
{
	static CAN_MSG CAN_MSG_NMT_Reset = { 0, { { 129, 0, 0, 0, 0, 0, 0, 0 } }, 2 }; // COB-ID = 600 + NodeId
	static CAN_MSG CAN_MSG_DownloadSegment = { 0x67C, { { 0x00, 0x10, 0x80, 0x00, 0x00, 0xF0, 0x7F, 0x00 } }, 8 }; // COB-ID = 600 + NodeId

	static UNSIGNED16 timer;

	static enum{
		CanBootloaderInit,

		/* Detect nodes with older software version (one each time) */
		CanBootloaderScanNetwork, // Send download request for index 0x1000 and subindex 0x00 to all nodes and wait for answers
		CanBootloaderUploadVendorId, // Try with increasing Node Id until a Node-Id with our Vendor Id is found and remove other nodes from list
		CanBootloaderUploadProductCode, // If not correct product code try next detected Node-Id and remove other nodes from list
		CanBootloaderUploadRevisionNumber, // Find newest revision number and if we have lower revision request update regularly until successful
		CanBootloaderWaitRevisionNumber, // Find newest revision number and if we have lower revision request update regularly until successful

		/* Download software to one node. We should only reach here if no newer revision is found and this is the normal state to be in. */
		CanBootloaderSendReset, // Wait for download request. The boot loader is run first so node is reset to start the boot loader
		CanBootloaderSendDownloadRequest,
		CanBootloaderSendDownloadFirstSegment,
		CanBootloaderSendDownloadBlock,
		CanBootloaderSendDownloadSegment,
		CanBootloaderAbort, // There may still be more nodes left, try next node
		CanBootloaderDownloadEof,
		CanBootloaderDownloadSuccesful, // There may still be more nodes left

		/* Download done (This should be removed the normal state is to wait for download request). */
		CanBootloaderDone // all nodes should have been updated
	} CanBootloaderState = CanBootloaderInit;

	/* Keep track of position within segment and memory */
	static union{
		Uint8 *memPtr;
		Uint32 Addr;
		Uint16 AddrWords[2];
		Uint8 AddrBytes[4];
	} memoryAddress;
	static unsigned int segmentPos;
	static Uint32 dataPos;

    static Uint8 checksum; // segment checksum
	const unsigned int EOF[] = {0, 0, 0, 1, 0xFF};

	static UNSIGNED8 LocalNodeId = 1;
	static UNSIGNED8 nodeIds[16] = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};
	static UNSIGNED32 newestRevisionNumber;
	static UNSIGNED32 NodeIdOfNewestRevisionNumber;

	switch (CanBootloaderState) // Initialization should be done once
	{
	/* Scan for other nodes with newer software */
	case CanBootloaderInit :
		PI_READ(PIACC_SDO, P101803_REVISION_NUMBER__USE_THE_SVN_REVISION_NUMBER, &newestRevisionNumber, 4);
		timer = MCOHW_GetTime() + BOOTLOADER_DOWNLOAD_WAIT_START;
		CanBootloaderState = CanBootloaderScanNetwork;
		break;
	case CanBootloaderScanNetwork :
		if(MCOHW_IsTimeExpired(timer))
		{
			static CAN_MSG CAN_MSG_ScanNetwork = {0, {{0x40, 0x00, 0x10, 0x00, 0, 0, 0, 0}}, 8}; // COB-ID = 600 + NodeId

			if(ID > 0x580 && ID < 0x600) // answer from node => node exist
				bootloaderDownloadStoreIds(nodeIds, ID - 0x580);
			MCOHW_SetCANFilter(0x580 + LocalNodeId); // so that we will receive answer
			CAN_MSG_ScanNetwork.ID = 0x600 + LocalNodeId++;
			MCOHW_PushMessage(&CAN_MSG_ScanNetwork);
			if(LocalNodeId > 127)
			{
				LocalNodeId = 1;
				timer = MCOHW_GetTime() + LSS_TIMEOUT;
				CanBootloaderState = CanBootloaderUploadVendorId; // note that we must continue to check for answers until timeout
			}
		}
		break;
	case CanBootloaderUploadVendorId :
		if(MCOHW_IsTimeExpired(timer)) // wait for all answers from scan network
		{
			if(bootloaderDownloadIdUsed(nodeIds, LocalNodeId))
			{
				static CAN_MSG CAN_MSG_UploadVendorId = {0, {{0x40, 0x18, 0x10, 0x01, 0, 0, 0, 0}}, 8}; // COB-ID = 600 + NodeId

				CAN_MSG_UploadVendorId.ID = 0x600 + LocalNodeId;
				MCOHW_PushMessage(&CAN_MSG_UploadVendorId);
				timer = MCOHW_GetTime() + LSS_TIMEOUT;
				CanBootloaderState = CanBootloaderUploadProductCode;
			}
			else if(++LocalNodeId > 127)
			{
				CanBootloaderState = CanBootloaderSendReset;
			}
		}
		else if(ID > 0x580 && ID < 0x600) // answer from node => node exist
		{
			bootloaderDownloadStoreIds(nodeIds, ID - 0x580);
		}
		break;
	case CanBootloaderUploadProductCode :
		if(MCOHW_IsTimeExpired(timer))
		{
			if(++LocalNodeId > 127)
			{
				CanBootloaderState = CanBootloaderSendReset;
			}
			else
			{
				CanBootloaderState = CanBootloaderUploadVendorId; // timeout try next node id
			}
		}
		else if ((ID == (unsigned) 0x580 + LocalNodeId) && pDat[0] == 0x43 && pDat[1] == 0x18 && pDat[2] == 0x10 && pDat[3] == 0x01) // correct: COB-ID, answer code, index, subindex
		{
			UNSIGNED32 vendorId;

			PI_READ(PIACC_SDO, P101801_VENDOR_ID, &vendorId, 4);
			if(vendorId == LSS_GetDword(&pDat[4]))
			{
				static CAN_MSG CAN_MSG_UploadProductCode = {0, {{0x40, 0x18, 0x10, 0x02, 0, 0, 0, 0}}, 8}; // COB-ID = 600 + NodeId

				CAN_MSG_UploadProductCode.ID = 0x600 + LocalNodeId;
				MCOHW_PushMessage(&CAN_MSG_UploadProductCode);
				timer = MCOHW_GetTime() + LSS_TIMEOUT;
				CanBootloaderState = CanBootloaderUploadRevisionNumber;
			}
			else if(++LocalNodeId > 127)
			{
				CanBootloaderState = CanBootloaderSendReset; // no more nodes
			}
			else
			{
				CanBootloaderState = CanBootloaderUploadVendorId; // not our vendor id try next node id
			}
		}
		break;
	case CanBootloaderUploadRevisionNumber :
		if(MCOHW_IsTimeExpired(timer))
		{
			if(++LocalNodeId > 127)
			{
				CanBootloaderState = CanBootloaderSendReset;
			}
			else
			{
				CanBootloaderState = CanBootloaderUploadVendorId; // timeout try next node id
			}
		}
		else if ((ID == (unsigned) 0x580 + LocalNodeId) && pDat[0] == 0x43 && pDat[1] == 0x18 && pDat[2] == 0x10 && pDat[3] == 0x02) // correct: COB-ID, answer code, index, subindex
		{
			UNSIGNED32 productCode;

			PI_READ(PIACC_SDO, P101802_PRODUCT_CODE, &productCode, 4);
			if(productCode == LSS_GetDword(&pDat[4]))
			{
				static CAN_MSG CAN_MSG_UploadRevisionNumber = {0, {{0x40, 0x18, 0x10, 0x03, 0, 0, 0, 0}}, 8}; // COB-ID = 600 + NodeId

				CAN_MSG_UploadRevisionNumber.ID = 0x600 + LocalNodeId;
				MCOHW_PushMessage(&CAN_MSG_UploadRevisionNumber);
				timer = MCOHW_GetTime() + LSS_TIMEOUT;
				CanBootloaderState = CanBootloaderWaitRevisionNumber;
			}
			else if(++LocalNodeId > 127)
			{
				CanBootloaderState = CanBootloaderSendReset; // no more nodes
			}
			else
			{
				CanBootloaderState = CanBootloaderUploadVendorId; // not our vendor id try next node id
			}
		}
		break;
	case CanBootloaderWaitRevisionNumber :
		if(MCOHW_IsTimeExpired(timer))
		{
			if(++LocalNodeId > 127)
			{
				CanBootloaderState = CanBootloaderSendReset;
			}
			else
			{
				CanBootloaderState = CanBootloaderUploadVendorId; // timeout try next node id
			}
		}
		else if ((ID == (unsigned) 0x580 + LocalNodeId) && pDat[0] == 0x43 && pDat[1] == 0x18 && pDat[2] == 0x10 && pDat[3] == 0x03) // correct: COB-ID, answer code, index, subindex
		{
			UNSIGNED32 revisionNumber;

			revisionNumber = LSS_GetDword(&pDat[4]);
			if(revisionNumber > newestRevisionNumber)
			{
				newestRevisionNumber = revisionNumber;
				NodeIdOfNewestRevisionNumber = LocalNodeId;
			}

			if(++LocalNodeId > 127)
			{
				CanBootloaderState = CanBootloaderSendReset; // no more nodes
			}
			else
			{
				CanBootloaderState = CanBootloaderUploadVendorId; // not our vendor id try next node id
			}
		}
		break;

	/* Wait for request from other nodes and send to reset to the requesting node */
	case CanBootloaderSendReset :

		PI_READ(PIACC_SDO, P201001_DOWNLOAD_REQUEST_1, &LocalNodeId, 1);
		if(LocalNodeId)
		{
			timer = MCOHW_GetTime() + 300;
			CAN_MSG_NMT_Reset.BUF.BUF[1] = LocalNodeId;
			MCOHW_PushMessage(&CAN_MSG_NMT_Reset);
			LocalNodeId = 0;
			PI_WRITE(PIACC_SDO, P201001_DOWNLOAD_REQUEST_1, &LocalNodeId, 1);
			CanBootloaderState = CanBootloaderSendDownloadRequest;
		}
		else if(MCOHW_IsTimeExpired(timer))
		{
			UNSIGNED32 revisionNumber;

			PI_READ(PIACC_SDO, P101803_REVISION_NUMBER__USE_THE_SVN_REVISION_NUMBER, &revisionNumber, 4);
			if(revisionNumber < newestRevisionNumber) // try same node until we are reset
			{
				static CAN_MSG CAN_MSG_DownloadUpgradeRequest = {0, {{0x2F, 0x10, 0x20, 0x01, 0, 0, 0, 0}}, 8}; // COB-ID = 600 + NodeId

				extern MCO_CONFIG MEM_FAR gMCOConfig;
				CAN_MSG_DownloadUpgradeRequest.ID = 0x600 + MY_NODE_ID;
				CAN_MSG_DownloadUpgradeRequest.BUF.BUF[4] = NodeIdOfNewestRevisionNumber;
				MCOHW_PushMessage(&CAN_MSG_DownloadUpgradeRequest);
				timer = MCOHW_GetTime() + 5000;
			}
		}
		break;
	case CanBootloaderSendDownloadRequest :
		if(MCOHW_IsTimeExpired(timer))
		{
			static CAN_MSG CAN_MSG_DownloadRequest = { 0x67C, { { 0x21, 0x50, 0x1F, 0x01, 0x14, 0x7E, 0x04, 0x00 } }, 8 }; // COB-ID = 600 + NodeId

			timer = MCOHW_GetTime() + 300;
			MCOHW_SetCANFilter(0x5FC); // so that we will receive answer
			LSS_PutDword(BOOTLOADER_DOWNLOAD_APPLICATION_LEN, &CAN_MSG_DownloadRequest.BUF.BUF[4]);
			MCOHW_PushMessage(&CAN_MSG_DownloadRequest);
			CanBootloaderState = CanBootloaderSendDownloadFirstSegment;
		}
		break;
	case CanBootloaderSendDownloadFirstSegment :
		if (ID == (unsigned) 0x5FC && (pDat[0] & 0xE0) == 0x60 && pDat[1] == 0x50 && pDat[2] == 0x1F && pDat[3] == 0x01) // correct: COB-ID, answer code, index, subindex
		{
			dataPos = 0;
			memoryAddress.Addr = BOOTLOADER_DOWNLOAD_APPLICATION_START + dataPos;
			timer = MCOHW_GetTime() + 300;
			CAN_MSG_DownloadSegment.BUF.BUF[0] = 0;
			CAN_MSG_DownloadSegment.BUF.BUF[1] = BOOTLOADER_DOWNLOAD_SEGMENT_LEN; // length
            checksum = CAN_MSG_DownloadSegment.BUF.BUF[1];
			CAN_MSG_DownloadSegment.BUF.BUF[2] = memoryAddress.AddrBytes[1]; // address high
            checksum += CAN_MSG_DownloadSegment.BUF.BUF[2];
			CAN_MSG_DownloadSegment.BUF.BUF[3] = memoryAddress.AddrBytes[0]; // address low
            checksum += CAN_MSG_DownloadSegment.BUF.BUF[3];
			CAN_MSG_DownloadSegment.BUF.BUF[4] = 0; // record type
            checksum += CAN_MSG_DownloadSegment.BUF.BUF[4];
            memoryAddress.Addr = BOOTLOADER_DOWNLOAD_APPLICATION_START + dataPos++;
			CAN_MSG_DownloadSegment.BUF.BUF[5] = *memoryAddress.memPtr; // data
            checksum += CAN_MSG_DownloadSegment.BUF.BUF[5];
            memoryAddress.Addr = BOOTLOADER_DOWNLOAD_APPLICATION_START + dataPos++;
			CAN_MSG_DownloadSegment.BUF.BUF[6] = *memoryAddress.memPtr; // data
            checksum += CAN_MSG_DownloadSegment.BUF.BUF[6];
            memoryAddress.Addr = BOOTLOADER_DOWNLOAD_APPLICATION_START + dataPos++;
			CAN_MSG_DownloadSegment.BUF.BUF[7] = *memoryAddress.memPtr; // data
            checksum += CAN_MSG_DownloadSegment.BUF.BUF[7];
			segmentPos = 7;
			MCOHW_PushMessage(&CAN_MSG_DownloadSegment);
			CanBootloaderState = CanBootloaderSendDownloadSegment;
		}
		if(MCOHW_IsTimeExpired(timer))
		{
			CanBootloaderState = CanBootloaderAbort;
		}
		break;
	case CanBootloaderSendDownloadBlock :
		CAN_MSG_DownloadSegment.BUF.BUF[0] ^= 0x10;
		CAN_MSG_DownloadSegment.BUF.BUF[1] = 2;
		CAN_MSG_DownloadSegment.BUF.BUF[2] = 0;
		CAN_MSG_DownloadSegment.BUF.BUF[3] = 0;
		CAN_MSG_DownloadSegment.BUF.BUF[4] = 2;
		CAN_MSG_DownloadSegment.BUF.BUF[5] = (memoryAddress.Addr + BOOTLOADER_DOWNLOAD_SEGMENT_LEN) >> 12;
		CAN_MSG_DownloadSegment.BUF.BUF[6] = 0;
		{
			int k;
			checksum = 0;
			for(k = 1; k < 7; k++)
			{
				checksum += CAN_MSG_DownloadSegment.BUF.BUF[k];
			}
		}
		checksum = ~checksum + 1;
		CAN_MSG_DownloadSegment.BUF.BUF[7] = checksum;
		MCOHW_PushMessage(&CAN_MSG_DownloadSegment);

		CanBootloaderState = CanBootloaderSendDownloadSegment;
		break;
	case CanBootloaderSendDownloadSegment : // Note that SDO message size and Intel hex file format record size does not fit together
		if (ID == (unsigned) 0x5FC && (pDat[0] & 0xE0) == 0x20 && (pDat[0] & 0x10) == (CAN_MSG_DownloadSegment.BUF.BUF[0] & 0x10)) // correct: COB-ID, answer code, toggle
		{
			{
				timer = MCOHW_GetTime() + 300;
				CAN_MSG_DownloadSegment.BUF.BUF[0] ^= 0x10;
				int n;
				for(n = 1; n < 8; n++) // fill the SDO message with Intel hex file format at current position
				{
					/* There exist length, address high, address low, record type, data, checksum
					 * in total six different choices
					 */
					if(segmentPos < 4) // length, address high, address low, record type
					{
						if(segmentPos < 2) // length, address high
						{
							if(segmentPos < 1) // length
							{
								memoryAddress.Addr = BOOTLOADER_DOWNLOAD_APPLICATION_START + dataPos;
								if(BOOTLOADER_DOWNLOAD_APPLICATION_LEN - dataPos >= BOOTLOADER_DOWNLOAD_SEGMENT_LEN)
								{
									CAN_MSG_DownloadSegment.BUF.BUF[n] = BOOTLOADER_DOWNLOAD_SEGMENT_LEN; // length
								}
								else
								{
									CAN_MSG_DownloadSegment.BUF.BUF[n] = BOOTLOADER_DOWNLOAD_APPLICATION_LEN - dataPos; // length
								}
								checksum = CAN_MSG_DownloadSegment.BUF.BUF[n];
								segmentPos++;
							}
							else // address high
							{
								CAN_MSG_DownloadSegment.BUF.BUF[n] = memoryAddress.AddrBytes[1]; // address high
								checksum += CAN_MSG_DownloadSegment.BUF.BUF[n];
								segmentPos++;
							}
						}
						else // address low, record type
						{
							if(segmentPos < 3) // address low
							{
								CAN_MSG_DownloadSegment.BUF.BUF[n] = memoryAddress.AddrBytes[0]; // address low
								checksum += CAN_MSG_DownloadSegment.BUF.BUF[n];
								segmentPos++;
							}
							else // record type
							{
								CAN_MSG_DownloadSegment.BUF.BUF[n] = 0; // record type
								checksum += CAN_MSG_DownloadSegment.BUF.BUF[n];
								segmentPos++;
							}
						}
					}
					else // data, checksum
					{
						if(segmentPos < (BOOTLOADER_DOWNLOAD_SEGMENT_LEN + 4) && dataPos < BOOTLOADER_DOWNLOAD_APPLICATION_LEN) // data
						{
							memoryAddress.Addr = BOOTLOADER_DOWNLOAD_APPLICATION_START + dataPos++;
							CAN_MSG_DownloadSegment.BUF.BUF[n] = *memoryAddress.memPtr; // data
							checksum += CAN_MSG_DownloadSegment.BUF.BUF[n];
							segmentPos++;
						}
						else // checksum
						{
							checksum = ~checksum + 1;
							CAN_MSG_DownloadSegment.BUF.BUF[n] = checksum; // data
							checksum = 0;
							segmentPos = 0;
							if(dataPos >= BOOTLOADER_DOWNLOAD_APPLICATION_LEN) // last byte
							{

								/* Fill SDO message */
								segmentPos = 0;
								int k = n + 1;
								while(k < 8 && segmentPos < 5)
								{
									CAN_MSG_DownloadSegment.BUF.BUF[k] = EOF[segmentPos];
									segmentPos++;
									k++;
								}
								if(segmentPos < 5)
								{
									CAN_MSG_DownloadSegment.BUF.BUF[0] |= (8 - k) << 1; // set number of unused bytes and indicate last segment
									CanBootloaderState = CanBootloaderDownloadEof;
								}
								else
								{
									CAN_MSG_DownloadSegment.BUF.BUF[0] |= ((8 - k) << 1) + 1; // set number of unused bytes and indicate last segment
									CanBootloaderState = CanBootloaderDownloadSuccesful;
								}
							}
							else if(memoryAddress.AddrWords[0] == 0xFFFF)
							{
								CanBootloaderState = CanBootloaderSendDownloadBlock;
							}
							break;
						}
					}
				}

				MCOHW_PushMessage(&CAN_MSG_DownloadSegment);
			}
		}
		else if(MCOHW_IsTimeExpired(timer))
		{
			CanBootloaderState = CanBootloaderAbort;
		}
		break;
	case CanBootloaderAbort :
		CanBootloaderState = CanBootloaderSendReset;
		break;
	case CanBootloaderDownloadEof :
		if (ID == (unsigned) 0x5FC && (pDat[0] & 0xE0) == 0x20 && (pDat[0] & 0x10) == (CAN_MSG_DownloadSegment.BUF.BUF[0] & 0x10)) // correct: COB-ID, answer code, toggle
		{
			timer = MCOHW_GetTime() + 300;
			CAN_MSG_DownloadSegment.BUF.BUF[0] ^= 0x10;
			int n = 1;
			while(segmentPos < 5)
			{
				CAN_MSG_DownloadSegment.BUF.BUF[n] = EOF[segmentPos];
				segmentPos++;
				n++;
			}
			CAN_MSG_DownloadSegment.BUF.BUF[0] |= ((8 - n) << 1) + 1; // set number of unused bytes and indicate last segment
			MCOHW_PushMessage(&CAN_MSG_DownloadSegment);

			CanBootloaderState = CanBootloaderDownloadSuccesful;
		}
		else if(MCOHW_IsTimeExpired(timer))
		{
			CanBootloaderState = CanBootloaderAbort;
		}
		break;
	case CanBootloaderDownloadSuccesful :
		if (ID == (unsigned) 0x5FC && (pDat[0] & 0xE0) == 0x20 && (pDat[0] & 0x10) == (CAN_MSG_DownloadSegment.BUF.BUF[0] & 0x10)) // correct: COB-ID, answer code, toggle
		{
			CAN_MSG_NMT_Reset.BUF.BUF[1] = 124;
			MCOHW_PushMessage(&CAN_MSG_NMT_Reset);
			timer = MCOHW_GetTime() + 300;
			CanBootloaderState = CanBootloaderSendReset;
		}
		else if(MCOHW_IsTimeExpired(timer))
		{
			CanBootloaderState = CanBootloaderSendReset;
		}
		break;
	case CanBootloaderDone :
		break;
	}
}

static void bootloaderDownloadStoreIds(UNSIGNED8* This, UNSIGNED8 LocalNodeId)
{
	LocalNodeId &= 0x7F; // node id should be in interval [1 127]

	This[LocalNodeId >> 3] |= 1 << (LocalNodeId & 7); // set bit indicating node id
}

static int bootloaderDownloadIdUsed(UNSIGNED8* This, UNSIGNED8 LocalNodeId)
{
	LocalNodeId &= 0x7F; // node id should be in interval [1 127]

	return This[LocalNodeId >> 3] & (1 << (LocalNodeId & 7));
}
