#ifndef SET_NODE_ID_H
#define SET_NODE_ID_H

#include "MicroCANopenPlus/MCO_DS401_LPC1768/types.h"
#include "MicroCANopenPlus/MCO_DS401_LPC1768/nodecfg.h"

void SetNodeIdAuto();						// Should be called then MY_NMT_STATE in NMT_STATE_AUTOID to find free Node-ID

#define AUTO_NODE_ID_FIRST 1
#define AUTO_NODE_ID_LAST 0x7F

void SetNodeIdSdoResponse(COBID_TYPE ID, UNSIGNED8 Len, UNSIGNED8 *pDat);

#endif
