/* 07-10-08 TJK
 ********************************************************************* tabs:[5 9]
 *
 *	F I L E   D E S C R I P T I O N
 *
 *	[x]Lib FB main source file		[ ]Lib FB source file
 *	[ ]Lib FB target source file	[ ]Project source file
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\file		Can.c
 *
 *	\ingroup	CAN
 *
 *	\brief
 *
 *	\details
 *
 *	\note
 *
 *	\version	dd-mm-yyyy
 *
 *******************************************************************************/

/*******************************************************************************
 *
 *	G R O U P   D O C U M E N T A T I O N
 *
 ****************************************************************************//**
 *
 *	\defgroup	CAN		CAN
 *
 *	\brief		CanOpen implementation.
 *
 ********************************************************************************
 *
 *	\details
 *
 *******************************************************************************/

/*******************************************************************************
 ;
 ;	D E C L A R A T I O N   M O D U L E
 ;
 ;*******************************************************************************

 ;*******************************************************************************
 ;
 ;	HEADER / INCLUDE FILES
 ;
 ;-----------------------------------------------------------------------------*/

#include "can.h"
#include "global.h"
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "_hw.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "local.h"
#include "Random.h"

#include "NodesStatusAndMapping.h"				// Map between Node ID and other information such as serial number of nodes
#include "SetNodeId.h"							// Used to set Node ID

#include "CommunicationProfileMUX.h"			// Used to switch between different communication profiles

#include "Master/Master.h"
#include "../Cc/ClusterControl.h"
#include "Cc.h"

#include <stdlib.h>

/*******************************************************************************
 ;
 ;	CONSTANTS
 ;
 ;-----------------------------------------------------------------------------*/
static const char TaskCAN_Name[] = "CAN";		// The CoTask must have a name

/*******************************************************************************
 ;
 ;	INTERNAL MACROS
 ;
 ;-----------------------------------------------------------------------------*/

/*******************************************************************************
 ;
 ;	INTERNAL STRUCTURES AND DATA TYPES
 ;
 ;-----------------------------------------------------------------------------*/

/**
 * Template function block instance type
 */

typedef struct { /*''''''''''''''''''''''''''''' RAM	*/
	can_Init const_P * pInit; /**< Initialization data			*/
	Uint8	flags;				/**< State flags					*/
} can_Inst; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
 ;
 ;	STATIC FUNCTIONS
 ;
 ;-----------------------------------------------------------------------------*/
PRIVATE void CAN__coTask(osa_CoTaskType *);

/*******************************************************************************
 ;
 ;	PUBLIC DATA
 ;
 ;-----------------------------------------------------------------------------*/
PUBLIC can_Inst*		can_pInstance;
osa_CoTaskType coTaskCAN;							// Run periodically once each millisecond and must exit each time
enum can_CommProfile_enum CAN_CommProfile = 0;		// Note, value should only be read once from register each time this process is run
UNSIGNED8 NodeIdFixed = 0;							// Use this node id if fixed is selected to be used
UNSIGNED16 CanBps = 0;								// Used for configurable baudrate

/*******************************************************************************
 ;
 ;	STATIC DATA
 ;
 ;-----------------------------------------------------------------------------*/
static int tpdoSet = 0;								// Set to one by register call back then master TPDOs should be transmitted
static int NodeIdFixedUpdated = 1;					// Set to one by register call then NodeId fixed is updated
static int CAN_CommProfileUpdated = 1;				// Set to one by register call then CAN_CommProfile is updated
static int CanBpsUpdated = 1;						// Set to one by register call then CanBps is updated
static int CanInputModeUpdated = 0;					// Set to one by register call then CanInputMode is updated

/* End of declaration module **************************************************/
PRIVATE void CAN__coTask(osa_CoTaskType *pCoTask) {
	static int syncTimer = 0;

	DISABLE;
	if (can_pInstance->flags & CAN_FLG_TESTMODE)
	{
		ENABLE;
		/* Production test mode enabled. REGU should not do anything. */
		return;
	}
	ENABLE;

	{
		extern volatile int TplateOperational;
		if(TplateOperational){
			static int once = 0;
			if(!once){
				once = 1;
				reg_get(&NodeIdFixed, can_NodeIdFixed);
				reg_get(&CanBps, can_CanBps);
			}
			Uint32 SerialNr;

			reg_get(&SerialNr, can__SerialNo);
			if(SerialNr){
				;
			}
			else{
				//SerialNr = 0xFF000000 | rand();
				SerialNr = 0xFF000000 | mp_random();	// JJ fix to avoid hardfault
				reg_put(&SerialNr, can__SerialNo);
			}
		}
		else{
			syncTimer = MCOHW_GetTime();
			return;
		}
	}

	int oneSecond;
	{
		static int oldBatteryConnected = 0;
		extern volatile int BatteryConnected;
		int batteryFlank;
		if((BatteryConnected && !oldBatteryConnected) || (!BatteryConnected && oldBatteryConnected)){
			batteryFlank = 1;
			{
				ChargerStatusFields_type status;
				status.Detail.Derate.Detail.NoLoad = !BatteryConnected;	// Trigger local sending of BMU communication
				if(CAN_CommProfile == CAN_COMM_PROFILE_MASTER){
					if(cc_GetAddedConnected(CcGetSerialNumberThis())){
						battConnect(0, status);
					}
				}
			}
		}
		else
			batteryFlank = 0;

		if (MCOHW_IsTimeExpired(syncTimer) || batteryFlank){		// Sync at 1 second interval or battery connection flank
			if(batteryFlank){
				syncTimer = MCOHW_GetTime();
			}
			syncTimer += 1000;										// Time relative to last pulse not current time
			oneSecond = 1;
		}
		else{
			oneSecond = 0;
		}

		oldBatteryConnected = BatteryConnected;
	}

	{
		if(CAN_CommProfileUpdated)
		{
			Uint8 tmp;
			reg_get(&tmp, can_CommProfile);									// Important, read only once each time this task is run
			CAN_CommProfile = tmp;

			if ((CAN_CommProfile == CAN_COMM_PROFILE_DISABLED) ||
				(CAN_CommProfile == CAN_COMM_PROFILE_SLAVE) ||
				(CAN_CommProfile == CAN_COMM_PROFILE_STATUS)
			){
				/* Disable parallel control */
				tmp = 0;
				reg_put(&tmp, can_ParallelControl);
			}

			if (CAN_CommProfile == CAN_COMM_PROFILE_SLAVE)
			{
				/* Disable DPL function */
				tmp = 0;
				reg_put(&tmp, can__PowerGroup_func);
			}


			CAN_CommProfileUpdated = 0;
		}
	}
	static int CommProfile_Old = 0;										// default to disabled, will reset communication at startup if needed

	{																	// Reset communication if necessary
		int resetCommunication = 0;
		extern volatile int CAN_AddressConflict;

		if(CAN_CommProfile != CommProfile_Old){							// Communication profile changed
			resetCommunication = 1;
		}
		if(CAN_AddressConflict){										// Address conflict ?
			CAN_AddressConflict = 0;
			extern MCO_CONFIG MEM_FAR gMCOConfig; 						// must be able to access this to access MY_NMT_STATE
			if(CAN_CommProfile != CAN_COMM_PROFILE_MASTER_CAN_INPUT && MY_NMT_STATE != NMTSTATE_AUTO_ID){
				resetCommunication = 2;
			}
		}
		if(NodeIdFixedUpdated){											// Fixed node id updated ?
			NodeIdFixedUpdated = 0;
			reg_get(&NodeIdFixed, can_NodeIdFixed);
			resetCommunication = 1;
		}
		if(CanBpsUpdated){												// CAN baudrate updated ?
			Uint8 canBps;

			reg_get(&canBps, can_CanBps);

			switch(canBps){
			case CAN_20_BPS:
				CanBps = CAN_BPS_20;
				break;
			case CAN_50_BPS:
				CanBps = CAN_BPS_50;
				break;
			case CAN_125_BPS:
				CanBps = CAN_BPS_125;
				break;
			case CAN_250_BPS:
				CanBps = CAN_BPS_250;
				break;
			case CAN_500_BPS:
				CanBps = CAN_BPS_500;
				break;
			case CAN_800_BPS:
				CanBps = CAN_BPS_800;
				break;
			case CAN_1000_BPS:
				CanBps = CAN_BPS_1000;
				break;
			default:
				CanBps = CAN_BPS_250;
				break;
			}

			CanBpsUpdated = 0;
			resetCommunication = 1;
		}
		if(CanInputModeUpdated){											// CanInputMode updated ?
			uint8_t canInputMode;
			uint8_t canInputModeSet;

			reg_get(&canInputMode, can_InputMode);
			reg_get(&canInputModeSet, can_InputModeSet);

			if(canInputMode != canInputModeSet){
				reg_put(&canInputModeSet, can_InputMode);
			}
			CanInputModeUpdated = 0;
		}

		if(CAN_CommProfile && resetCommunication){
			if(resetCommunication == 2){
				MCOUSER_ResetCommunication(0);							// Reset communication but not node id counter
			}
			else{
				MCOUSER_ResetCommunication(1);							// Reset communication to currently selected communication profile
			}
		}
	}

	if(CAN_CommProfile)
	{
		extern MCO_CONFIG MEM_FAR gMCOConfig; 							// must be able to access this to access MY_NMT_STATE

		MCO_ProcessStack();

		uint8_t state;
		switch (MY_NMT_STATE) 											// CAN operational states could be find in mco.h
		{
		case NMTSTATE_BOOT:
			state = 0;
			break;
		case NMTSTATE_AUTO_ID:
			state = 1;
			SetNodeIdAuto();											// Automatically assign node id
			break;
		case NMTSTATE_STOP:
			state = 2;
			break;
		case NMTSTATE_OP:
			state = 3;
			CAN_Operational(oneSecond, tpdoSet);
			//CAN_bootloaderUpload(0, 0, 0);
			tpdoSet = 0;
			break;
		case NMTSTATE_PREOP:
			state = 4;
			break;
		default:
			state = 5;
			break;
		}
		reg_put(&state, can_State);
		reg_put(&MY_NODE_ID, can_NodeId);
		CAN_NodeStatusAndMapping_Heartbeat(MY_NODE_ID, MY_NMT_STATE);
	}
	else{
		//FIO1SET = (1 << 29); // CAN power disable active low
	}

	CommProfile_Old = CAN_CommProfile;

	if(oneSecond){
		CAN_1s();
	}
}

/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	can_init
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief		Create an instance of the FB.
 *
 *	\param		iid		An index (0..) that is accepted by various functions
 *						to identify the instance. Should be kept for later use,
 *						or may be discarded if not required by the FB.
 *	\param		pArgs	Pointer to the initialization arguments.
 *
 *	\return		Pointer to the instance i.e. to its private data. This pointer
 *				will be passed to all the other functions of the SYS interface
 *				as it identifies the instance in question.
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PUBLIC void * can_init(sys_FBInstId iid, void const_P * pArgs) {
	can_Inst * pInst;

	pInst = (can_Inst *) mem_reserve(&mem_normal, sizeof(can_Inst));
	can_pInstance = pInst;
	pInst->pInit = (can_Init const_P *) pArgs;
	pInst->flags = 0;

	/* Enable isolated voltage to the CAN bus driver */
	FIO1DIR |= (1 << 29);		// CAN power enable
	FIO1CLR = (1 << 29);		// CAN power enable active low

	LPC_TIM0->MR0 = 11999;		// 1mSec = 12.000-1 counts
	LPC_TIM0->MCR = 3;			// Interrupt and Reset on MR0
	LPC_TIM0->TCR = 1;			// Timer0 Enable
	hw_enableIrq(TIMER0_IRQn); 	// enable TIMER0 interrupt

	osa_newCoTask(&coTaskCAN, "CAN", OSA_PRIORITY_NORMAL, CAN__coTask, 2048);

	return ((void *) pInst);
}
/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	can_reset
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief		Reset the instance.
 *
 *	\param		instance	Pointer to instance.
 * 	\param		pArgs		Pointer to the initialization arguments.
 *
 *	\return
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PUBLIC void can_reset(void * instance, void const_P * pArgs) {
	DUMMY_VAR(pArgs);
}
/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	can_down
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief		Power down indication.
 *
 *	\param		instance	Pointer to instance.
 *	\param		nType		Down type.
 *
 *	\return
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PUBLIC void can_down(void * instance, sys_DownType nType) {
	DUMMY_VAR(nType);
}
/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	can_read
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief		Read indication function used by an external program to read
 *				the value of a parameter owned by this FB.
 *
 *	\param		instance	Pointer to the data of the instance.
 *	\param		rIndex		Register index (0..) according to the indication
 *							list defined for this FB.
 *	\param		aIndex		Array index. May be omitted unless the the register
 *							is an array. (For non-arrays the value is zero.)
 *	\param		pValue		Pointer to the storage for the value.
 *
 *	\return
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PUBLIC sys_IndStatus can_read(void * instance, sys_IndIndex rIndex,
		sys_ArrIndex aIndex, void * pValue) {
	DUMMY_VAR(aIndex);

	return(REG_OK);
}
/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	can_write
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief		Write indication function used by an external program to
 *				indicate and ask permission to write the value of a parameter
 *				(not necessarily owned by this FB).
 *
 *	\param		instance	Pointer to the data of the instance.
 *	\param		rIndex		Register index (0..).
 *	\param		aIndex		Array index. May be omitted unless the the register
 *							is an array. (For non-arrays the value is zero.)
 *
 *	\return
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PUBLIC sys_IndStatus can_write(void * instance, sys_IndIndex rIndex,
		sys_ArrIndex aIndex) {

	DUMMY_VAR(aIndex);

	switch (rIndex)
	{
	case reg_indic_tpdoSet :
		tpdoSet = 1;
		break;
	case reg_indic_NodeIdFixed :
		NodeIdFixedUpdated = 1;
		break;
	case reg_indic_CAN_CommProfile :
		CAN_CommProfileUpdated = 1;
		break;
	case reg_indic_CanBps :
		CanBpsUpdated = 1;
		break;
	case reg_indic_CanInputModeSet :
		CanInputModeUpdated = 1;
		break;
	}

	return REG_OK;
}
/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	can_ctrl
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief
 *
 *	\param		instance	Pointer to the data of the instance.
 *	\param		pCtrl		Pointer to the control indication.
 *
 *	\return
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PUBLIC void can_ctrl(void * instance, sys_CtrlType * pCtrl) {
	//DUMMY_VAR(pCtrl);
	/*
	 *	0x80 = request to enter test mode.
	 */

	if (pCtrl->cmd == 0x80)
	{
		atomic(
			((can_Inst *) instance)->flags |= CAN_FLG_TESTMODE;
		);
	}
}
/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	can_test
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief		Self test
 *
 *	\param		instance	Pointer to instance.
 *
 *	\return
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PUBLIC Uint16 can_test(void * instance) {

	return (0xFFFF);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	can_resetCanInput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset canInput to zero if can communication profile == STATUS
*
*	\param
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void can_resetCanInput() {

	Uint8 CanCommProfile;
	reg_get(&CanCommProfile, can_CommProfile);

	if (CanCommProfile == CAN_COMM_PROFILE_STATUS){
		/* Set canInput to zero */
		Uint8 canInput = 0;
		reg_put(&canInput, can_Input);
	}
}
