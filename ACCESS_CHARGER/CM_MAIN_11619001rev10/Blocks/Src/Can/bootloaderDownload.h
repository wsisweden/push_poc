/*
 * bootlooaderUpload.h
 *
 *  Created on: Jul 8, 2010
 *      Author: nicka
 */

#ifndef BOOTLOOADER_DOWNLOAD_H_
#define BOOTLOOADER_DOWNLOAD_H_

#define BOOTLOADER_DOWNLOAD_WAIT_START 1000
#define BOOTLOADER_DOWNLOAD_APPLICATION_START 0x2000
#define BOOTLOADER_DOWNLOAD_APPLICATION_LEN 4*65536 - 0x2000
#define BOOTLOADER_DOWNLOAD_SEGMENT_LEN 16 // segmentLen = 16 have been tested others may or may not work

/* Called periodically and then SDO response received */
void CAN_bootloaderUpload(COBID_TYPE ID, UNSIGNED8 Len, UNSIGNED8 *pDat);

#endif /* BOOTLOOADER_DOWNLOAD_H_ */
