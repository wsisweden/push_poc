/**************************************************************************
MODULE:    MCOHW_LPC1
CONTAINS:  Driver implementation for Philips LPC17xx derivatives with
           CAN interface. Compiled and Tested with Keil Tools www.keil.com
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS IS THE COMMERCIAL VERSION OF MICROCANOPEN
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2010-02-17 23:14:09 -0400 (Wed, 17 Feb 2010) $
           $LastChangedRevision: 1573 $
***************************************************************************/

#include "../MCO/mcohw.h"
#include "../MCO/canfifo.h"
#include <string.h>

// Common CAN bit rates BTR values for 12MHz CAN Clock
#define   CANBitrate20k_12MHz     0x003E401DUL
#define   CANBitrate50k_12MHz     0x002B400EUL
#define   CANBitrate125k_12MHz    0x002B4005UL
#define   CANBitrate250k_12MHz    0x002B4002UL
#define   CANBitrate500k_12MHz    0x00274001UL
#define   CANBitrate800k_12MHz    0x00394000UL
#define   CANBitrate1000k_12MHz   0x00364000UL

// Maximum number of FullCAN Filters
#define MAX_FILTERS 32

// On the LPC17xx several CAN ports are supported, this is the number
// of the CAN port used (1-2)
#define FIRST_CANPORT 1

// Declare, for usage in IRQ Vector

//osa_SemaType MCO_ProcessStackRxWait;

#if 0
#ifdef __ARMCC_VERSION
// ARM compiler version
void __irq MCOHW_CANISR (void);
void __irq MCOHW_TimerISR (void);

#else

// GNU compiler verison
#define __irq
void MCOHW_CANISR (void) __attribute__ ((interrupt));
void MCOHW_TimerISR (void) __attribute__ ((interrupt));
#endif
#else
PUBLIC osa_isrFastDecl(CAN_IRQHandler);
PUBLIC osa_isrFastDecl(TIMER0_IRQHandler);
#endif


/**************************************************************************
GLOBAL VARIABLES
***************************************************************************/ 

// This structure holds all node specific configuration
extern MCO_CONFIG gMCOConfig;

// Global timer/conter variable, incremented every millisecond
UNSIGNED16 volatile gTimCnt = 0;


/**************************************************************************
LOCAL VARIABLES
***************************************************************************/ 
#if (FIRST_CANPORT == 1)
static UNSIGNED32 CAN1RxCount = 0;
#elif (FIRST_CANPORT == 2)
static UNSIGNED32 CAN2RxCount = 0;
#endif

/**************************************************************************
LOCAL FUNCTIONS
***************************************************************************/ 
static void MCOHW_CAN1ISR_Rx (void);
static void MCOHW_CAN1ISR_Err (void);


/**************************************************************************
PUBLIC FUNCTIONS
***************************************************************************/ 

/**************************************************************************
DOES:    This function returns the global status variable.
CHANGES: The status can be changed anytime by this module, for example from 
         within an interrupt service routine or by any of the other 
         functions in this module.
BITS:    0: INIT - set to 1 after a completed initialization
                   left 0 if not yet inited or init failed
         1: CERR - set to 1 if a CAN bit or frame error occured
         2: ERPA - set to 1 if a CAN "error passive" occured
         3: RXOR - set to 1 if a receive queue overrun occured
         4: TXOR - set to 1 if a transmit queue overrun occured
         5: Reserved
         6: TXBSY - set to 1 if Transmit queue is not empty
         7: BOFF - set to 1 if a CAN "bus off" error occured
**************************************************************************/
UNSIGNED8 MCOHW_GetStatus (
  void
  )
{
UNSIGNED32 *pCSR;          // pointer into SFR space
  // Set SFR pointer
  pCSR = (UNSIGNED32 *) &LPC_CAN1->SR + (FIRST_CANPORT-1)*0x1000;

  // Transmit FIFO or buffer in use?
  if ((CANTXFIFO_GetOutPtr() != 0) || (!(*pCSR & 0x00000004L)))
  { // Busy transmitting
    gMCOConfig.HWStatus |= HW_TXBSY;
  }
  else
  { // all Tx buffers empty
    gMCOConfig.HWStatus &= ~HW_TXBSY;
  }

  return gMCOConfig.HWStatus;
}


/**************************************************************************
DOES:    This function implements a CAN receive queue. With each
         function call a message is pulled from the queue.
RETURNS: 1 Message was pulled from receive queue
         0 Queue empty, no message received
**************************************************************************/
UNSIGNED8 MCOHW_PullMessage (
  CAN_MSG MEM_FAR *pReceiveBuf
  )
{
CAN_MSG *pSrc;

  // Check if message is in Rx FIFO  
//  osa_semaGet(&MCO_ProcessStackRxWait);
  pSrc = CANRXFIFO_GetOutPtr();
  if (pSrc != 0)
  {
    // copy message
	  memcpy(pReceiveBuf,pSrc,sizeof(CAN_MSG));
	  // copying complete, update FIFO
	  CANRXFIFO_OutDone();
	  return TRUE; // msg received
  }
  return FALSE; // no msg rcvd 
}


#if MONITOR_ALL_NODES
/**************************************************************************
DOES:    This function is used by the manager to poll messages that are
         needed by the manager
RETURNS: TRUE or FALSE, if no message was received
**************************************************************************/
UNSIGNED8 MCOHWMGR_PullMessage (
  CAN_MSG *pReceiveBuf // buffer to witch a received message is copied
  )
{ 
CAN_MSG *pSrc;

  // Check if message is in Rx FIFO  
  pSrc = CANMGRFIFO_GetOutPtr();
  if (pSrc != 0)
  {
    // copy message
	  memcpy(pReceiveBuf,pSrc,sizeof(CAN_MSG));
	  // copying complete, update FIFO
	  CANMGRFIFO_OutDone();
	  return TRUE; // msg received
  }
  return FALSE; // no msg rcvd 
}
#endif // MONITOR_ALL_NODES


/**************************************************************************
DOES: If there is something in the transmit queue, and if the transmit
      buffer is available, copy next message from queue to transmit buffer
***************************************************************************/ 
void MCOHW_CheckTxQueue (
  void
  )
{
UNSIGNED32 *pCSR;          // pointer into SFR space
CAN_MSG *pMsg;
UNSIGNED32 *pAddr;
UNSIGNED32 *pCandata;

#if (FIRST_CANPORT == 1)
  if(!(LPC_SC->PCONP & (1 << 13))){					// Clock to peripheral enabled ?
	  return;
  }
#else
  if(!(LPC_SC->PCONP & (1 << 14))){					// Clock to peripheral enabled ?
	  return;
  }
#endif
  pCSR = (UNSIGNED32 *) &LPC_CAN1->SR + (FIRST_CANPORT-1)*0x1000;

  // Get next message from FIFO
  pMsg = CANTXFIFO_GetOutPtr();

  if ((*pCSR & 0x00000004L) && (pMsg != 0))
  { // Transmit Channel 1 is available and message is in queue
    
    pAddr = (UNSIGNED32 *) &LPC_CAN1->TFI1 + (FIRST_CANPORT-1)*0x1000;

    // Write DLC
    *pAddr = ((UNSIGNED32) (pMsg->LEN)) << 16;  
  
    // Write CAN ID
    pAddr++;
    *pAddr = pMsg->ID & 0x000007FFL;
 
    // Write first 4 data bytes 
    pCandata = &pMsg->BUF.BUF32[0];
    pAddr++;
    *pAddr = *pCandata;

    // Write second 4 data bytes 
    pCandata++;
    pAddr++;
    *pAddr = *pCandata;
  
    // Write transmission request
    pAddr = (UNSIGNED32 *) &LPC_CAN1->CMR + (FIRST_CANPORT-1)*0x1000;
#if USE_HARDWARE_LOOPBACK
    *pAddr = 0x30; // Transmission Request Buf 1
#else
    *pAddr = 0x21; // Transmission Request Buf 1
#endif

    // Update Out pointer
    CANTXFIFO_OutDone();

  }
}


/**************************************************************************
DOES:    Adding a CAN message to the transmit queue
RETURNS: TRUE or FALSE if queue overrun
***************************************************************************/ 
UNSIGNED8 MCOHW_PushMessage (
  CAN_MSG *pTransmitBuf // Data structure with message to be send
  )
{
CAN_MSG *pDst; // Destination pointer

  // Get next message space available in FIFO
  pDst = CANTXFIFO_GetInPtr();
  if (pDst != 0)
  {
    // Copy message to transmit queue
    memcpy(pDst,pTransmitBuf,sizeof(CAN_MSG));
    // Copy completed
  	CANTXFIFO_InDone();
    return TRUE;
  }
  // Overrun occured
  // Signal overrun to status variable
  gMCOConfig.HWStatus |= HW_TXOR;
  return FALSE;
}


/**************************************************************************
DOES:    CAN error interrupt handler
**************************************************************************/
void MCOHW_CAN1ISR_Err (
  void
  )
{

  // INSERT APPLICATION SPECIFIC CODE AS NEEDED

#if USE_LEDS
  gMCOConfig.LEDErr = LED_FLASH1;
#endif

  if (LPC_CAN1->GSR & 0x02)
  { // Data overrun
    gMCOConfig.HWStatus |= HW_RXOR;
  }

  if (LPC_CAN1->GSR & 0x40)
  { // Error warning/Error passive
    gMCOConfig.HWStatus |= HW_ERPA;
  }

  if (LPC_CAN1->GSR & 0x80)
  { // Bus off

    gMCOConfig.HWStatus |= HW_BOFF; // Global status variable

    // Clear reset bit set by bus-off condition
    LPC_CAN1->MOD = 0;

#if USE_LEDS
    gMCOConfig.LEDErr = LED_ON;
#endif

  }  

  gMCOConfig.HWStatus |= HW_CERR; // Global status variable

}


/**************************************************************************
DOES:    Timer interrupt handler (1ms)
**************************************************************************/
PUBLIC osa_isrFastFn(NULL, TIMER0_IRQHandler)
//void __irq MCOHW_TimerISR (
//  void
//  )
{
  osa_isrEnter();

  LPC_TIM0->IR = 1; // Clear interrupt flag
  gTimCnt++; // increment global timer counter
  {
	  extern volatile int CAN_CommProfile;
	  if(CAN_CommProfile){
		  MCOHW_CheckTxQueue(); // check if something is in the Tx queue
	  }
  }

  osa_isrLeave();
}


/**************************************************************************
DOES:    CAN1 receive handler
**************************************************************************/
volatile UNSIGNED8 Heartbeats[128] = {[0 ... 127] = 0};
volatile int CAN_AddressConflict = 0;
void MCOHW_CAN1ISR_Rx (
  void
  )
{
CAN_MSG *pDst;
UNSIGNED32 canid;
UNSIGNED32 *pDest;

  if (!(LPC_CAN1->RFS & 0xC0000400L))
  { // 11-bit ID, no RTR, matched a filter
    canid = LPC_CAN1->RID & 0x000007FFL;

    {
        if((canid & ~(COBID_TYPE)0x7F) == 0x700){					// Heart beat ?
        	Heartbeats[canid & 0x7F] = LPC_CAN1->RDA & 0xFF;		// Save heart beat in table

            if((canid & 0x7F) == MY_NODE_ID){						// Received heart beat for own node id ?
            	CAN_AddressConflict = 1;							// Address conflict
            }
        }

        int n;
        for(n = 0; n < NR_OF_TPDOS; n++){							// For all TPDOs this node is sending
        	extern TPDO_CONFIG MEM_FAR gTPDOConfig[NR_OF_TPDOS];
            if(canid && canid == gTPDOConfig[n].CAN.ID){			// Received TPDO sent by this node ?
            	CAN_AddressConflict = 1;							// Address conflict
            }
        }
    }
    #if MONITOR_ALL_NODES
    if (MY_NMT_STATE == 5)
    { // only work on manager when operational
#if USE_CiA447
      if ( ((canid >= 0x081) && (canid <= 0x090)) ||
           ((canid >= 0x600) && (canid <= 0x6FF)) ||
           ((canid >= 0x701) && (canid <= 0x710))
         )
#else                                                // Comments below from [Embedded Networking with CAN and CANopen page 493-494]
      if ( ((canid >= 0x081) && (canid <= 0x0FF)) || // Emergency
           ((canid >= 0x581) && (canid <= 0x5FF)) || // Transmit SDO
           ((canid >= 0x701) && (canid <= 0x77F))    // NMT Error Control (Heartbeat and Node Guarding)
         )
#endif
      { // This is a message for the CANopen Manager

        // initialize destination pointer into FIFO
        pDst = CANMGRFIFO_GetInPtr();

	      if (pDst != 0)
	      { // FIFO available
	        // copy ID
		      pDst->ID = CAN1->RID & 0x000007FFL;
		      // copy DLC
          pDst->LEN = (CAN1->RFS & 0x000F0000L) >> 16;
		      // copy data
		      pDest = (UNSIGNED32 *) &(pDst->BUF[0]);
		      *pDest++ = CAN1->RDA;
		      *pDest = C1RDB; 
		      // copying is all done
		      CANMGRFIFO_InDone();
	      }
		    else
		    { // overrun, message lost
		      gMCOConfig.HWStatus |= HW_RXOR;
		    }
      }
    }
#endif // MONITOR_ALL_NODES

    if (CANSWFILTER_Match(canid))
    { // Message needs to be received

      // initialize destination pointer into FIFO
      pDst = CANRXFIFO_GetInPtr();

      if (pDst != 0)
      { // FIFO available
        // copy ID
        pDst->ID = LPC_CAN1->RID & 0x000007FFL;
	      // copy DLC
        pDst->LEN = (LPC_CAN1->RFS & 0x000F0000L) >> 16;
		    // copy data
		    pDest = &pDst->BUF.BUF32[0];
		    *pDest++ = LPC_CAN1->RDA;
		    *pDest = LPC_CAN1->RDB;
		    // copying is all done
		    CANRXFIFO_InDone();
		  }
		  else
		  { // overrun, message lost
		    gMCOConfig.HWStatus |= HW_RXOR;
		  }
	    
    }
  }

  LPC_CAN1->CMR = 0x04; // release receive buffer

//  osa_semaSetIsr(&MCO_ProcessStackRxWait);

	return;
}


/**************************************************************************
DOES:    CAN interrupt handler
**************************************************************************/
PUBLIC osa_isrFastFn(NULL, CAN_IRQHandler)
//void __irq MCOHW_CANISR (
//  void
//  )
{
    osa_isrEnter();

    /* koden kommer hit */

#if (FIRST_CANPORT == 1)
  if ( LPC_CAN1->GSR & (1 << 0 ) )
	{
		CAN1RxCount++;
		MCOHW_CAN1ISR_Rx();
	}
	if ( LPC_CAN1->GSR & (3 << 6 ) )
	{
		/* The error count includes both TX and RX */
		MCOHW_CAN1ISR_Err();
	}
	// Clear interrupt
	LPC_CAN1->ICR;
#else
  if ( CAN2->GSR & (1 << 0 ) )
	{
		CAN2RxCount++;
		MCOHW_CAN2ISR_Rx();
	}
	if ( CAN2->GSR & (3 << 6 ) )
	{
		/* The error count includes both TX and RX */
		CAN2ErrCount = (CAN2->GSR >> 16 );
		MCOHW_CAN2ISR_Err();
	}
	// Clear interrupt
	temp = CAN2->ICR;
#endif  // (FIRST_CANPORT == 1)


    osa_isrLeave();

	return;
}


/**************************************************************************
DOES:    This function implements the initialization of the CAN interface.
RETURNS: 1 if init is completed 
         0 if init failed, bit INIT of MCOHW_GetStatus stays 0
**************************************************************************/
UNSIGNED8 MCOHW_Init (
  UNSIGNED16 baudrate
  )
{
UNSIGNED32 *pAddr;
UNSIGNED32 p;
UNSIGNED32 btr;

//	osa_newSemaSet(&MCO_ProcessStackRxWait);
//	osa_semaGet(&MCO_ProcessStackRxWait);

  switch (baudrate)
  {
    case 20:
      btr = CANBitrate20k_12MHz;
      break;
      
    case 50:
      btr = CANBitrate50k_12MHz;
      break;
      
    case 125:
      btr = CANBitrate125k_12MHz;
      break;
      
    case 250:
      btr = CANBitrate250k_12MHz;
      break;
      
    case 500:
      btr = CANBitrate500k_12MHz;
      break;
      
    case 800:
      btr = CANBitrate800k_12MHz;
      break;
      
    case 1000:
      btr = CANBitrate1000k_12MHz;
      break;
      
    default:
      return 0;  // Not supported
  }

  // Init HW status variable
  gMCOConfig.HWStatus = HW_INIT;

  // Init CAN receive SW filter
  CANSWFILTER_Init();

#if (TXFIFOSIZE > 0)
  // Init Tx FIFO
  CANTXFIFO_Flush();
#endif  

#if (RXFIFOSIZE > 0)
  // Init RxFIFO
  CANRXFIFO_Flush();
#endif  

#if (MGRFIFOSIZE > 0)
  // Init MGRFIFO
  CANMGRFIFO_Flush();
#endif  

#if (FIRST_CANPORT == 1)
  // Enable clock to the peripheral
  LPC_SC->PCONP |= (1 << 13);
  // Enable Pins for CAN port 1
  LPC_PINCON->PINSEL0 |=  (1 <<  0);               // Pin P0.0 used as RD1 (CAN1)
  LPC_PINCON->PINSEL0 |=  (1 <<  2);               // Pin P0.1 used as TD1 (CAN1)
#else
  // Enable clock to the peripheral
  SC->PCONP |= (1 << 14);
  // Enable Pins for CAN port 2
  PINCON->PINSEL0 |=  (1 <<  14);              // Pin P2.7 used as RD2 (CAN2)
  PINCON->PINSEL0 |=  (1 <<  16);              // Pin P2.8 used as TD2 (CAN2)
#endif

  // Reset and disable all message filters
  // Acceptance Filter Mode Register = off !
  LPC_CANAF->AFMR = 0x00000001L;

  LPC_CAN1->MOD  = 1;      // Go into Reset mode
  LPC_CAN1->IER  = 0;      // Disable All Interrupts
  LPC_CAN1->GSR  = 0;      // Clear Status register
  LPC_CAN1->CMR  = 0x0E;   // Clear receive buffer, data overrun, abort tx and set self reception so that this node could receive it's own messages
  LPC_CAN1->BTR  = btr;    // Set bit timing

  // Enter Normal Operating Mode
  LPC_CAN1->MOD  = 0;      // Operating Mode

  // Enable Receive Interrupt (bit 0), Error Warning (bit 2),
  // Data overrun (bit 3), Error passive (bit 5), Bus error (bit 7)
  LPC_CAN1->IER = 0x000000AD;

  // Now work on Acceptance Filter Configuration     
  // Acceptance Filter Mode Register = off !
  LPC_CANAF->AFMR = 0x00000001;
  
  // Initialize pointer to filter RAM
  pAddr = (UNSIGNED32 *) LPC_CANAF_RAM_BASE;
  p = 0;

  // Set pointer for Standard Frame Individual
  // Standard Frame Explicit
  LPC_CANAF->SFF_sa = p;

  // Set pointer for Standard Frame Groups
  // Standard Frame Group Start Address Register
  LPC_CANAF->SFF_GRP_sa = p;

  // Set pointer for Standard Frame Groups
  // Receive all 11bit CAN IDS
#if (FIRST_CANPORT == 1)
  *pAddr = (0x000 << 16) + 0x7FF;
#else
  // 0x20002000 indicates CAN interface 2
  *pAddr = 0x20002000 + (0x000 << 16) + 0x7FF;
#endif

  p += 4;

  // Set pointer for Extended Frame Individual
  // Extended Frame Start Address Register
  LPC_CANAF->EFF_sa = p;

  // Set pointer for Extended Frame Groups
  // Extended Frame Group Start Address Register
  LPC_CANAF->EFF_GRP_sa = p;

  // Set ENDofTable 
  // End of AF Tables Register
  LPC_CANAF->ENDofTable = p;

  // Acceptance Filter Mode Register, start using filter

  LPC_CANAF->AFMR = 0x00000000;

  // Initialize Timer Interrupt
  //LPC_TIM0->MR0 = 11999; // 1mSec = 12.000-1 counts
  //LPC_TIM0->MCR = 3;     // Interrupt and Reset on MR0
  //LPC_TIM0->TCR = 1;     // Timer0 Enable

  // Init Interrupts
  //hw_enableIrq(TIMER0_IRQn); 	// enable TIMER0 interrupt
  hw_enableIrq(CAN_IRQn); 		// enable CAN interrupt
//  NVIC_EnableIRQ(TIMER0_IRQn);   // enable TIMER0 interrupt
//  NVIC_EnableIRQ(CAN_IRQn);   // enable CAN interrupt



  return 1;
}


/**************************************************************************
DOES:    This function implements the initialization of a CAN ID hardware
         filter as supported by many CAN controllers.
RETURNS: 1 if filter was set 
         2 if this HW does not support filters 
           (in this case HW will receive EVERY CAN message)
         0 if no more filter is available
**************************************************************************/
UNSIGNED8 MCOHW_SetCANFilter (
  UNSIGNED16 CANID
  )
{
  return CANSWFILTER_Set(CANID);
}


/**************************************************************************
DOES:    This function implements the deletion of a previously set CAN ID 
         hardware filter as supported by many CAN controllers.
RETURNS: 1 if filter was deleted
         0 if filter could not be deleted
**************************************************************************/
UNSIGNED8 MCOHW_ClearCANFilter (
  UNSIGNED16 CANID
  )
{
  return CANSWFILTER_Clear(CANID);
}


#if MONITOR_ALL_NODES
/**************************************************************************
DOES:    This function implements an additional CAN receive filter
         used by the manager. Messages received using this ID are pulled
         by the manager using function MCOHWMGR_PullMessage
         Filter set receives messages from 0x81 to 0xFF and 0x581 to 0x5FF
RETURNS: TRUE or FALSE, if filter was not set
**************************************************************************/
UNSIGNED8 MCOHWMGR_SetCANFilter (
  void
  )
{ 
  // In this driver no HW filters are used, SW only
  return TRUE;
}
#endif // MONITOR_ALL_NODES


/**************************************************************************
DOES:    This function reads a 1 millisecond timer tick. The timer tick
         must be a UNSIGNED16 and must be incremented once per millisecond.
RETURNS: 1 millisecond timer tick
**************************************************************************/
UNSIGNED16 MCOHW_GetTime (void)
{
  return gTimCnt;
}


/**************************************************************************
DOES:    This function compares a UNSIGNED16 timestamp to the internal 
         timer tick and returns 1 if the timestamp expired/passed.
RETURNS: 1 if timestamp expired/passed
         0 if timestamp is not yet reached
NOTES:   The maximum timer runtime measurable is 0x8000 (about 32 seconds).
         For the usage in MicroCANopen that is sufficient. 
**************************************************************************/
UNSIGNED8 MCOHW_IsTimeExpired (
  UNSIGNED16 timestamp
  )
{
UNSIGNED16 time_now;

  time_now = gTimCnt;
  if (time_now >= timestamp)
  {
    if ((time_now - timestamp) < 0x8000)
      return 1;
    else
      return 0;
  }
  else
  {
    if ((timestamp - time_now) >= 0x8000)
      return 1;
    else
      return 0;
  }
}

/**************************************************************************
DOES:    CAN message copy routine for up to 8 bytes
**************************************************************************/
/* RealView memcopy is as efficent as this
void MCOHW_CANCPY (
  UNSIGNED8 *dst, 
  UNSIGNED8 *src, 
  UNSIGNED8 len
  )
{
  while (len > 0)
  {
    if ((len > 3) && (!((UNSIGNED32)dst & 3)) && (!((UNSIGNED32)src & 3)))
	{ // 32bit copy, if src and dst 32bit aligned
	  *((UNSIGNED32 *)dst) = *((UNSIGNED32 *)src);
	  dst += 4;
	  src += 4;
	  len -= 4;  
	}
    if ((len > 1) && (!((UNSIGNED32)dst & 1)) && (!((UNSIGNED32)src & 1)))
	{ // 16bit copy, if src and dst 32bit aligned
	  *((UNSIGNED16 *)dst) = *((UNSIGNED16 *)src);
	  dst += 2;
	  src += 2;
	  len -= 2;  
	}
	else
	{ // 8bit copy
      *dst = *src;
	  dst++;
	  src++;
	  len--;
	}  
  }
}
*/

/*----------------------- END OF FILE ----------------------------------*/
