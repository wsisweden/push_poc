/**************************************************************************
MODULE:    NODECFG.H
CONTAINS:  MicroCANopen node configuration
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS IS THE COMMERCIAL VERSION OF MICROCANOPEN PLUS
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
           See file license_commercial_plus.txt or
           www.microcanopen.com/license_commercial_plus.txt
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2010-02-17 03:36:11 -0400 (Wed, 17 Feb 2010) $
           $LastChangedRevision: 1572 $
***************************************************************************/ 

#ifndef _NODECFG_H
#define _NODECFG_H


// CANopen Data Types
#include "types.h"
#include "LPC17xx.h" // LPC17xx Peripheral Registers


/**************************************************************************
DEFINES: MEMORY TYPE OPTIMIZATION
**************************************************************************/

// CONST Object Dictionary Data
#define MEM_CONST const

// Process data
#define MEM_PROC

// Frequently used variables
#define MEM_NEAR

// Seldomly used variables
#define MEM_FAR

// data type for CAN IDs, normally 16bit sufficient,
// on 32bit architectures use 32bit for better alignment
typedef UNSIGNED32 COBID_TYPE;


/**************************************************************************
MACRO: RTOS SLEEP FUNCTION
**************************************************************************/
// If used in RTOS, allow other tasks to run in blocking function calls
// #define RTOS_SLEEP Sleep(3);
#define RTOS_SLEEP


/**************************************************************************
DEFINES: BASIC CAN COMMUNICATION
Modify these for your application
**************************************************************************/

// Default CAN bitrate in (kHz)
#define CAN_BITRATE  125

// Use CAN SW Filtering
#define USE_CANSWFILTER 1


/**************************************************************************
DEFINES: DEFAULT NODE ID
**************************************************************************/

// Define the default Node ID that is used to initialize the CANopen
// stack if no user-specific setting is implemented in
// MCOUSER_ResetCommunication(). If the Node ID from auto-generated code
// via CANopen Architect EDS is to be used, set it to NODE_ID.
#define NODEID MY_NODE_ID


/**************************************************************************
DEFINES: ENABLING/DISABLING CODE FUNCTIONALITY
**************************************************************************/

// If enabled, node starts up automatically (does not wait for NMT master)
//#define AUTOSTART 1 // Dependent on chosen communication profile

// If enabled, 1 or more TPDOs use the event timer
#define USE_EVENT_TIME 1

// If enabled, 1 or more TPDOs are change-of-state and use the inhibit timer
#define USE_INHIBIT_TIME 1

// If enabled, 1 or more TPDOs use the SYNC signal
#define USE_SYNC 1

// Heartbeat consumer is dynamic / configurable
#define DYNAMIC_HB_CONSUMER 1

// If enabled, all parameters passed to functions are checked for consistency. 
// On failures, the user function MCOUSER_FatalError is called.
#define CHECK_PARAMETERS 1

// If enabled, CANopen indicator lights are implemented
#define USE_LEDS 0


/**************************************************************************
DEFINES: CAN HARDWARE DRIVER DEFINITIONS
**************************************************************************/

// if defined a SW FIFO for CAN receive and transmit is used
#define USE_CANFIFO 1

// Tx FIFO depth (must be 0, 4, 8, 16 or 32)
#define TXFIFOSIZE 16

// Rx FIFO depth (must be 0, 4, 8, 16 or 32)
#define RXFIFOSIZE 16

// Manager Rx FIFO depth (must be 0, 4, 8, 16 or 32)
#define MGRFIFOSIZE 0


/**************************************************************************
DEFINES: ADDITIONAL ENABLING/DISABLING CODE FUNCTIONALITY OF PLUS PACKAGE
**************************************************************************/

// Using the "plus" functionality of MicroCANopen
#define USE_MCOP 1

// If enabled, Emergency Messages are used
#define USE_EMCY 1
// If EMCYs are used, size of error field history [1003h]
#define ERROR_FIELD_SIZE 8

// If enabled, extended SDO handling (segmented transfer) is used
#define USE_EXTENDED_SDO 0
// If enabled, use extended call backs MCOUSER_XSDOInitWrite and MCOUSER_XSDOWriteSegment
#define USE_XSDOCB_WRITE 0

// If enabled, blocked SDO transfers are allowed
#define USE_BLOCKED_SDO 0
// Max size of a block supported
#define BLK_MAX_SIZE 4
#define BLK_B2B_TIMEOUT 4

// If enabled, minimal Node Guarding is used to satisfy conformance test
#define USE_NODE_GUARDING 0

// If enabled, the function Store Parameters [1010h] is supported
// The driver must provide funtcions NVOL_ReadByte and NVOL_WriteByte
#define USE_STORE_PARAMETERS 0
// Specify offset and size of non-volatile memory,
#define NVOL_STORE_START 2
#define NVOL_STORE_SIZE 1024

// If enabled, Layer Setting Services (Slave) are implemented
#define USE_LSS_SLAVE 0

// If enabled, sent messages are received (added by Micropower)
#define USE_HARDWARE_LOOPBACK 0

// If enabled, LSS Fast Scan is implemented
#define USE_MICROLSS 0

// If enabled, cal back MCOSUSER_TimeOfDay() is called when time object is received
#define USE_TIME_OF_DAY_RX 1


/**************************************************************************
DEFINES: ENABLING CALL-BACK FUNCTIONS
**************************************************************************/

// If enabled, the call-back function MCOUSER_SDOResponse (added by Micropower)
#define USECB_SDOResponse 1

// If enabled, the call-back function MCOUSER_EmergencyReceived (added by Micropower)
#define USECB_Emergency 1

// If enabled, the call-back function MCOUSER_LSSMasterReceived (added by Micropower)
#define USECB_LSS_MASTER 0

// If enabled, the call-back function MCOUSER_NMTChange is called
// everytime the CANopen stack changes its NMT Slave State
#define USECB_NMTCHANGE 1

// If enabled, the call-back function MCOUSER_SYNCReceived is called every
// time the CANopen stack receives the SYNC message
#define USECB_SYNCRECEIVE 1

// If enabled, the call-back function MCOUSER_RPDOReceived is called every
// time the CANopen stack receives an RPDO
#define USECB_RPDORECEIVE 1

// If enabled, the call-back function MCOUSER_TPDOReady is called every
// time right before the CANopen stack sends a TPDO
#define USECB_TPDORDY 0

// If enabled, the call-back function MCOUSER_SDORequest is called every
// time the CANopen stack receives an SDO Request that it cannot handle
#define USECB_SDOREQ 0

// If enabled, the call back function MCOUSER_AppSDOReadInit is called
// before every segmented SDO read request - for implementation of application
// specific, custom SDO handling
#define USECB_APPSDO_READ 0

// If enabled, the call back function MCOUSER_AppSDOWriteInit is called
// before every segmented SDO write request - for implementation of application
// specific, custom SDO handling
#define USECB_APPSDO_WRITE 0

// If enabled, the call-back function MCOUSER_GetSerial is called every
// time the CANopen stack receives an SDO Request for the serial number
#define USECB_ODSERIAL 1


/**************************************************************************
DEFINES: ADDITIONAL ENABLING/DISABLING CODE FUNCTIONALITY OF MANAGER
**************************************************************************/

// Use the CANopen Manager
#define MONITOR_ALL_NODES 0

#if MONITOR_ALL_NODES

// Define this for full CANopen Manager node list
#define USE_FULL_NODELIST 1
// How many heartbeat timeouts are checked with each call to ProcessMgr
#define NR_OF_HBCHECKS_PERCYCLE 16
// If enabled, the [1018h] Object Dictionary entries are included in the node list
#define NODELIST_WITH_IDOBJECT 1
// If enabled, the [1000h] and [6000h] OD entries are included in the node list
#define NODELIST_447 1

// Maximum number of nodes
#define MAX_NR_OF_NODES 16

// Default settings for number of SDO clients
#define NR_OF_SDO_CLIENTS 16
// SDO client timeout in milliseconds
#define SDO_REQUEST_TIMEOUT 200
// SDO client back-3back transmit timeout in milliseconds
#define SDO_BACK2BACK_TIMEOUT 5
// SDO client supports block mode
#define USE_BLOCKED_SDO_CLIENT 1
// SDO block transfer max number of blocks (4 to 127)
#define SDO_BLK_MAX_SIZE 4
// The concised DCF option, when enabled, allows to process multiple
// SDO commands automatically from a table.
#define USE_CONCISEDCF 0

// If enabled, the call-back function MCOUSER_EMCYReceived is called every
// time the CANopen stack receives an EMCY message
#define USECB_EMCYRECEIVE 1

#endif // MONITOR_ALL_NODES

// If enabled, supports the sleep mode as defined in CiA447
#define USE_SLEEP 0

// Allows to enable the optional LSS Manager
#define USE_LSS_MANAGER 1

// if defined use MicroLSS/LSS FastScan manager
#define USE_MLSS_MANAGER 0

#if USE_MLSS_MANAGER

// define to 1 to enable 80-bit scan
#define MLSSM_EIGHTYBIT 0
// define to 1 to use byte optimized scan
#define MLSSM_BYTEOPTIMIZE 1
// node ID to assign to first node found. All other nodes will be assigned
// sequential node IDs
#define MLSSM_STARTNODEID 0x02
// scan timeout in milliseconds
#ifdef __SIMULATION__
#define MLSSM_TIMEOUT 50
#else
#define MLSSM_TIMEOUT 10
#endif
// timeout increase after fail
#define MLSSM_TIMEOUTINCREASE 10
// number of fails allowed
#define MLSSM_MAXFAIL 8
// timeout between scans for new devices
#define MLSSM_SCANTIME 1000

#endif // USE_MLSS_MANAGER


/**************************************************************************
DEFINES: Application Profile Specific Support
**************************************************************************/

// If enabled, support of car-add on devices is implemented
#define USE_CiA447 0

// If enabled, support of proprietary fully-meshed SDO communication is
// implemented
#define USE_SDOMESH 0

#if USE_CiA447
#define NR_OF_SDOSERVER 16
#else
#if USE_SDOMESH
#define NR_OF_SDOSERVER 16
#else
#define NR_OF_SDOSERVER 1
#endif
#endif


/**************************************************************************
DEFINES: BOOTLOADER SUPPORT
**************************************************************************/
// If enabled, supports the ESAcademy CANopen bootloader, copy values
// from bootloader configuration
#define REBOOT_FLAG_ADR     0x40007FFCUL
#define REBOOT_FLAG         *(volatile UNSIGNED32 *)REBOOT_FLAG_ADR
#define REBOOT_BOOTLOAD_VAL 0x21654387


#endif // _NODECFG_H

/*----------------------- END OF FILE ----------------------------------*/
