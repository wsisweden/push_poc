#ifndef _TYPESH_
#define _TYPESH_

/**************************************************************************
DEFINES: DATA TYPES
**************************************************************************/

// CANopen Data Types
#define UNSIGNED8 unsigned char
#define UNSIGNED16 unsigned short
#define UNSIGNED32 unsigned int
#define INTEGER8 char
#define INTEGER16 short
#define INTEGER32 int


#define TRUE 1
#define FALSE 0

#endif  // _TYPESH_
