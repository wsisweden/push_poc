/**************************************************************************
MODULE:    MCOHW_NVOL_SIM
CONTAINS:  Simulation of non-volatile memory, here in regular RAM
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS IS THE COMMERCIAL VERSION OF MICROCANOPEN PLUS
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
           See file license_commercial_plus.txt or
           www.microcanopen.com/license_commercial_plus.txt
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2009-09-24 21:22:11 -0300 (Thu, 24 Sep 2009) $
           $LastChangedRevision: 1447 $
***************************************************************************/ 

#include "../MCO/mcohw.h"

#if USE_STORE_PARAMETERS

// Simulated non-volatile memory, here: regular RAM
UNSIGNED8 mNVOL[NVOL_STORE_SIZE];
//static UNSIGNED8 *mNVOL; // DEBUG/TEST: here grabbing some free RAM

/**************************************************************************
DOES:    Initializes access to non-volatile memory.
**************************************************************************/
void NVOL_Init (
  void
  )
{
UNSIGNED8 *pRAM;
UNSIGNED8 lp;

return;

  // here: use reserved bytes in RAM
  pRAM = (UNSIGNED8 *) 0x40003800;
  for (lp = 0; lp < NVOL_STORE_SIZE; lp++)
  {
    mNVOL[lp] = *pRAM;
    pRAM++;
  }
}

/**************************************************************************
DOES:    Reads a data byte from non-volatile memory
NOTE:    The address is relative, an offset to NVOL_STORE_START
RETURNS: The data read from memory
**************************************************************************/
UNSIGNED8 NVOL_ReadByte (
  UNSIGNED16 address // location of byte in NVOL memory
  )
{
  // Protect from illegal address access
  if (address >= NVOL_STORE_SIZE) return 0;
  return mNVOL[address];
}


/**************************************************************************
DOES:    Writes a data byte to non-volatile memory
NOTE:    The address is relative, an offset to NVOL_STORE_START
RETURNS: nothing
**************************************************************************/
void NVOL_WriteByte (
  UNSIGNED16 address, // location of byte in NVOL memory
  UNSIGNED8 data
  )
{
  // Protect from illegal address access
  if (address >= NVOL_STORE_SIZE) return;
  mNVOL[address] = data;
}


/**************************************************************************
DOES:    Is called when a block of write cycles is complete. The driver
         may buffer the data from calls to NVOL_WriteByte in RAM and then
         write the entire buffer to non-volatile memory upon a call to
         this function.
**************************************************************************/
void NVOL_WriteComplete (
  void
  )
{
UNSIGNED8 *pRAM;
UNSIGNED8 lp;

return;

  // here: use reserved bytes in RAM
  pRAM = (UNSIGNED8 *) 0x40003800;
  for (lp = 0; lp < NVOL_STORE_SIZE; lp++)
  {
    *pRAM = mNVOL[lp];
    pRAM++;
  }
}
#endif // USE_STORE_PARAMETERS

/*----------------------- END OF FILE ----------------------------------*/

