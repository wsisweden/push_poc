/**************************************************************************
MODULE:    CANFIFO
CONTAINS:  MicroCANopen Plus implementation, CAN SW Filter and I/O FIFOs
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS FILE IS PART OF MICROCANOPEN PLUS
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2009-09-24 21:22:11 -0300 (Thu, 24 Sep 2009) $
           $LastChangedRevision: 1447 $
***************************************************************************/ 

#include "mco.h"
#include "canfifo.h"
#include <string.h>

#if USE_CANFIFO

#if (TXFIFOSIZE != 0) && (TXFIFOSIZE != 4) && (TXFIFOSIZE != 8) && (TXFIFOSIZE != 16) && (TXFIFOSIZE != 32)
#error TXFIFOSIZE must be 0 (deactivated), 4, 8, 16 or 32
#endif
#if (RXFIFOSIZE != 0) && (RXFIFOSIZE != 4) && (RXFIFOSIZE != 8) && (RXFIFOSIZE != 16) && (RXFIFOSIZE != 32)
#error RXFIFOSIZE must be 0 (deactivated), 4, 8, 16 or 32
#endif
#if (MGRFIFOSIZE != 0) && (MGRFIFOSIZE != 4) && (MGRFIFOSIZE != 8) && (MGRFIFOSIZE != 16) && (MGRFIFOSIZE != 32)
#error MGRFIFOSIZE must be 0 (deactivated), 4, 8, 16 or 32

#endif


/**************************************************************************
GLOBAL VARIABLES
***************************************************************************/ 

typedef struct
{
#if USE_CANSWFILTER
  UNSIGNED32 FilterList[64]; // 2048 bits, one for each 11bit CAN ID
#endif
#if (TXFIFOSIZE > 0)
  CAN_MSG TxFifo[TXFIFOSIZE];
#endif
#if (RXFIFOSIZE > 0)
  CAN_MSG RxFifo[RXFIFOSIZE];
#endif
#if (MGRFIFOSIZE > 0)
  CAN_MSG MGRFifo[MGRFIFOSIZE];
#endif
#if (TXFIFOSIZE > 0)
  UNSIGNED8 TxIn;
  UNSIGNED8 TxOut;
#endif
#if (RXFIFOSIZE > 0)
  UNSIGNED8 RxIn;
  UNSIGNED8 RxOut;
#endif
#if (MGRFIFOSIZE > 0)
  UNSIGNED8 MGRIn;
  UNSIGNED8 MGROut;
#endif
} CANFIFOINFO;

// Module variable with all FIFO information
CANFIFOINFO mCF;


/**************************************************************************
PUBLIC FUNCTIONS
***************************************************************************/ 

#if USE_CANSWFILTER
/**************************************************************************
DOES: Initializes the CAN SW receive filtering variables
      Default: No CAN message is received.
***************************************************************************/ 
void CANSWFILTER_Init (
  void
  )
{
UNSIGNED16 loop;

  for (loop = 0; loop < 64; loop++)
  {
    mCF.FilterList[loop] = 0;
  }
}


/**************************************************************************
DOES: Setting a single CAN receive filter
***************************************************************************/ 
UNSIGNED8 CANSWFILTER_Set (
  UNSIGNED16 CANID     // CAN-ID to be received by filter 
  )
{
  // In the array of 2048 bits, set the bit Nr CANID
  mCF.FilterList[(CANID & 0x07E0) >> 5] |= (1 << (CANID & 0x001F));

  if (CANID <= 0x7FF)
  {
    return TRUE;
  }
  return FALSE;
}


/**************************************************************************
DOES: Clearing a previously set CAN receive filter
***************************************************************************/ 
UNSIGNED8 CANSWFILTER_Clear (
  UNSIGNED16 CANID     // CAN-ID to be no longer received
  )
{
  // In the array of 2048 bits, set the bit Nr CANID
  mCF.FilterList[(CANID & 0x07E0) >> 5] &= ~(1 << (CANID & 0x001F));

  if (CANID <= 0x7FF)
  {
    return TRUE;
  }
  return FALSE;
}


/**************************************************************************
DOES: Checks if a CAN filter is set for an ID
***************************************************************************/ 
UNSIGNED8 CANSWFILTER_Match (
  UNSIGNED16 CANID     // CAN-ID requested
  )
{
  // In the array of 2048 bits, return the bit Nr CANID
  return (0x01 & ((mCF.FilterList[(CANID & 0x07E0) >> 5]) >> (CANID & 0x001F)));
}
#endif // USE_CANSWFILTER


#if (TXFIFOSIZE > 0)
/**************************************************************************
DOES: Flushes / clears the TXFIFO, all data stored in FIFO is lost
***************************************************************************/ 
void CANTXFIFO_Flush (
  void
  )
{
  mCF.TxIn = 0;
  mCF.TxOut = 0;
}


/**************************************************************************
DOES:    Returns a CAN message pointer to the next free location in FIFO.
         Application can then copy a CAN message to the location given by 
		 the pointer and MUST call CANTXFIFO_InDone() when done.
RETURNS: CAN message pointer into FIFO      
         NULL if FIFO is full
***************************************************************************/ 
CAN_MSG *CANTXFIFO_GetInPtr (
  void
  )
{
UNSIGNED8 ovr; // check if FIFO is full

  ovr = mCF.TxIn + 1;
  ovr &= (TXFIFOSIZE-1);

  if (ovr != mCF.TxOut)
  {// FIFO is not full
    return &(mCF.TxFifo[mCF.TxIn]);
  }
  return 0;
}


/**************************************************************************
DOES: 	 Must be called by application after data was copied into the FIFO,
         this increments the internal IN pointer to the next free location 
		 in the FIFO.
RETURNS: nothing
***************************************************************************/ 
void CANTXFIFO_InDone (
  void
  )
{
  // Increment IN pointer
  mCF.TxIn++;
  mCF.TxIn &= (TXFIFOSIZE-1);
}


/**************************************************************************
DOES:    Returns a CAN message pointer to the next OUT message in the FIFO.
         Application can then copy the CAN message from the location given 
		 by the pointer to the desired destination and MUST call 
		 CANTXFIFO_OutDone() when done.
RETURNS: CAN message pointer into FIFO      
         NULL if FIFO is empty
***************************************************************************/ 
CAN_MSG *CANTXFIFO_GetOutPtr (
  void
  )
{
  if (mCF.TxIn != mCF.TxOut)
  { // message available in FIFO
    return &(mCF.TxFifo[mCF.TxOut]);
  }
  return 0;
}


/**************************************************************************
DOES: 	 Must be called by application after data was copied from the FIFO,
         this increments the internal OUT pointer to the next location 
		 in the FIFO.
RETURNS: nothing
***************************************************************************/ 
void CANTXFIFO_OutDone (
  void
  )
{
  mCF.TxOut++;
  mCF.TxOut &= (TXFIFOSIZE-1);
}
#endif // (TXFIFOSIZE > 0)


#if (RXFIFOSIZE > 0)
/**************************************************************************
DOES: Flushes / clears the RXFIFO, all data stored in FIFO is lost
***************************************************************************/ 
void CANRXFIFO_Flush (
  void
  )
{
  mCF.RxIn = 0;
  mCF.RxOut = 0;
}


/**************************************************************************
DOES:    Returns a CAN message pointer to the next free location in FIFO.
         Application can then copy a CAN message to the location given by 
		 the pointer and MUST call CANRXFIFO_InDone() when done.
RETURNS: CAN message pointer into FIFO      
         NULL if FIFO is full
***************************************************************************/ 
CAN_MSG *CANRXFIFO_GetInPtr (
  void
  )
{
UNSIGNED8 ovr; // check if FIFO is full

  ovr = mCF.RxIn + 1;
  ovr &= (RXFIFOSIZE-1);

  if (ovr != mCF.RxOut)
  {// FIFO is not full
    return &(mCF.RxFifo[mCF.RxIn]);
  }
  return 0;
}


/**************************************************************************
DOES: 	 Must be called by application after data was copied into the FIFO,
         this increments the internal IN pointer to the next free location 
		 in the FIFO.
RETURNS: nothing
***************************************************************************/ 
void CANRXFIFO_InDone (
  void
  )
{
  // Increment IN pointer
  mCF.RxIn++;
  mCF.RxIn &= (RXFIFOSIZE-1);
}


/**************************************************************************
DOES:    Returns a CAN message pointer to the next OUT message in the FIFO.
         Application can then copy the CAN message from the location given 
		 by the pointer to the desired destination and MUST call 
		 CANRXFIFO_OutDone() when done.
RETURNS: CAN message pointer into FIFO      
         NULL if FIFO is empty
***************************************************************************/ 
CAN_MSG *CANRXFIFO_GetOutPtr (
  void
  )
{
  if (mCF.RxIn != mCF.RxOut)
  { // message available in FIFO
    return &(mCF.RxFifo[mCF.RxOut]);
  }
  return 0;
}


/**************************************************************************
DOES: 	 Must be called by application after data was copied from the FIFO,
         this increments the internal OUT pointer to the next location 
		 in the FIFO.
RETURNS: nothing
***************************************************************************/ 
void CANRXFIFO_OutDone (
  void
  )
{
  mCF.RxOut++;
  mCF.RxOut &= (RXFIFOSIZE-1);
}
#endif // (RXFIFOSIZE > 0)


#if (MGRFIFOSIZE > 0)
/**************************************************************************
DOES: Flushes / clears the MGRFIFO, all data stored in FIFO is lost
***************************************************************************/ 
void CANMGRFIFO_Flush (
  void
  )
{
  mCF.MGRIn = 0;
  mCF.MGROut = 0;
}


/**************************************************************************
DOES:    Returns a CAN message pointer to the next free location in FIFO.
         Application can then copy a CAN message to the location given by 
		 the pointer and MUST call CANMGRFIFO_InDone() when done.
RETURNS: CAN message pointer into FIFO      
         NULL if FIFO is full
***************************************************************************/ 
CAN_MSG *CANMGRFIFO_GetInPtr (
  void
  )
{
UNSIGNED8 ovr; // check if FIFO is full

  ovr = mCF.MGRIn + 1;
  ovr &= (MGRFIFOSIZE-1);

  if (ovr != mCF.MGROut)
  {// FIFO is not full
    return &(mCF.MGRFifo[mCF.MGRIn]);
  }
  return 0;
}


/**************************************************************************
DOES: 	 Must be called by application after data was copied into the FIFO,
         this increments the internal IN pointer to the next free location 
		 in the FIFO.
RETURNS: nothing
***************************************************************************/ 
void CANMGRFIFO_InDone (
  void
  )
{
  // Increment IN pointer
  mCF.MGRIn++;
  mCF.MGRIn &= (MGRFIFOSIZE-1);
}


/**************************************************************************
DOES:    Returns a CAN message pointer to the next OUT message in the FIFO.
         Application can then copy the CAN message from the location given 
		 by the pointer to the desired destination and MUST call 
		 CANMGRFIFO_OutDone() when done.
RETURNS: CAN message pointer into FIFO      
         NULL if FIFO is empty
***************************************************************************/ 
CAN_MSG *CANMGRFIFO_GetOutPtr (
  void
  )
{
  if (mCF.MGRIn != mCF.MGROut)
  { // message available in FIFO
    return &(mCF.MGRFifo[mCF.MGROut]);
  }
  return 0;
}


/**************************************************************************
DOES: 	 Must be called by application after data was copied from the FIFO,
         this increments the internal OUT pointer to the next location 
		 in the FIFO.
RETURNS: nothing
***************************************************************************/ 
void CANMGRFIFO_OutDone (
  void
  )
{
  mCF.MGROut++;
  mCF.MGROut &= (MGRFIFOSIZE-1);
}
#endif // (MGRFIFOSIZE > 0)

#endif // USE_CANFIFO

