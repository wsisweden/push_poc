/**************************************************************************
MODULE:    STORPARA
CONTAINS:  MicroCANopen Plus implementation, Store Parameter Function
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS IS THE COMMERCIAL PLUS VERSION OF MICROCANOPEN
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
           See file license_commercial_plus.txt
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2009-09-24 21:22:11 -0300 (Thu, 24 Sep 2009) $
           $LastChangedRevision: 1447 $
***************************************************************************/ 

#include "mco.h"
#include "mcop.h"
#include "mcohw.h"


#if USE_STORE_PARAMETERS

// this structure holds all node specific configuration
extern MCO_CONFIG MEM_FAR gMCOConfig;

// external declaration for the process image array
extern UNSIGNED8 MEM_PROC gProcImg[];

// external declaration for the OD table
extern OD_PROCESS_DATA_ENTRY MEM_CONST gODProcTable[];

#if NR_OF_TPDOS > 0
// this structure holds all the TPDO configuration data for up to 4 TPDOs
extern TPDO_CONFIG MEM_FAR gTPDOConfig[NR_OF_TPDOS];
#endif

#if NR_OF_RPDOS > 0
// this structure holds all the RPDO configuration data for up to 4 RPDOs
extern RPDO_CONFIG MEM_FAR gRPDOConfig[NR_OF_RPDOS];
#endif

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
extern HBCONS_CONFIG gHBCons[NR_OF_HB_CONSUMER];
#endif // (NR_OF_HB_CONSUMER > 0)
#endif // MONITOR_ALL_NODES

// Index definitions
#define STORE_PARAMETERS    0x1010
#define RESTORE_PARAMETERS  0x1011

enum {
    PARAMETERS_ALL = 1,
    PARAMETERS_COMMUNICATION,
    PARAMETERS_APPLICATION,
    PARAMETERS_MANUFACTURER
} store_parameters_config;

/**************************************************************************
PRIVATE FUNCTIONS
***************************************************************************/ 

/**************************************************************************
DOES:    Counts how many bytes of data are in the process image in the
         specified index range with OD entry type ODWR.
RETURNS: Number of bytes reuqired for storage.
**************************************************************************/
UNSIGNED16 MCOSP_ODCount (
  UNSIGNED16 start,
  UNSIGNED16 end
  )
{
UNSIGNED16 sum; // for counting bytes
OD_PROCESS_DATA_ENTRY MEM_CONST *pOD; // pointer into OD array

  sum = 0;
  pOD = &(gODProcTable[0]);
  while (pOD->idx != 0xFFFF)
  {
    if ((pOD->idx >= start) && (pOD->idx <= end))
    { // entry is in right index range
      if (pOD->len & ODWR)
      { // entry can be written to
        // add to total
        sum += (pOD->len & 0x0F);
      }
    }
    pOD++;
  }
  if (sum > 0)
  { // add space for checksum and ID
    sum += 4;  
  }
  return sum;
}


/**************************************************************************
DOES:    Determines offset values for the the different regions
         in non-volatile memory.
         1. LSS
         2. Save Parameters 1XXX
         3. Save Parameters 6XXX
         4. Save Parameters 2XXX
         5. First free space
RETURNS: Info on start offsets is vcpied into the string (5 values)
**************************************************************************/
void MCOSP_GetNVOLUsage (
  UNSIGNED16 pLoc[5] // info is written into this string
  )
{
UNSIGNED16 pos;

  // Init access to non-volatile memory
  NVOL_Init();

  pos = NVOL_STORE_START;
  *pLoc = pos; // start of LSS
  pLoc++; // pointer to next section: 1XXX
  pos += 4; // size of LSS record is 4 bytes
  // Now start of section 1XXXX
  *pLoc = pos;
  pLoc++; // pointer to next section: 2XXX
  pos += 4; // 2 byte ID, 2 byte checksum
  pos += 2; // 2 byte HB producer time
#if NR_OF_TPDOS > 0
  pos += (sizeof(TPDO_CONFIG) * NR_OF_TPDOS);
#endif
#if NR_OF_RPDOS > 0
  pos += (sizeof(RPDO_CONFIG) * NR_OF_RPDOS);
#endif
#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
  pos += (sizeof(HBCONS_CONFIG) * NR_OF_HB_CONSUMER);
#endif
#endif // MONITOR_ALL_NODES
#if USE_SYNC
  pos += 2;
#endif
  // Now reached end of section, next section 6XXX
  *pLoc = pos;
  pLoc++; // pointer to next section: 6XXX
  pos += MCOSP_ODCount(0x6000,0x9FFF);
  // Now reached end of section, next section 2XXX
  *pLoc = pos;
  pLoc++; // pointer to end
  pos += MCOSP_ODCount(0x2000,0x5FFF);
  // Now reached end of required NVOL
  *pLoc = pos;
  if (pos > (NVOL_STORE_START + NVOL_STORE_SIZE))
  { // NVOL storage size TOO small!!!
    MCOUSER_FatalError(0x1212);
  }
}


/**************************************************************************
DOES:    Checks or retrieves all communication parameters from NVOL
RETURNS: TRUE, if data is present in NVOL
         FALSE, if no data is present in NVOL
**************************************************************************/
UNSIGNED8 MCOSP_GetStoredComParameters (
  UNSIGNED16 start, // start offset in NVOL memory
  UNSIGNED8 restore // TRUE: restore data, FALSE: only perform chksum test
  )
{
// Note: first UNSIGNED16 used for ID "1X", second for checksum
UNSIGNED16 offset;
UNSIGNED16 loop;
UNSIGNED16 chk_sum;
UNSIGNED8 *pDest;

  if ( (NVOL_ReadByte(start) != '1') ||
       (NVOL_ReadByte(start+1) != 'X') 
     )
  { // ID does not match, no parameters stored
    return FALSE;
  }

  // Verify checksum
  chk_sum = 0;
  offset = start + 4;
  //HB time
  chk_sum += NVOL_ReadByte(offset);
  offset++;
  chk_sum += NVOL_ReadByte(offset);
  offset++;

#if NR_OF_TPDOS > 0
  // TPDO configuration data
  for (loop = 0; loop < (sizeof(TPDO_CONFIG)*NR_OF_TPDOS); loop++)
  {
    chk_sum += NVOL_ReadByte(offset);
    offset++;
  }
#endif

#if NR_OF_RPDOS > 0
  // RPDO configuration data
  for (loop = 0; loop < (sizeof(RPDO_CONFIG)*NR_OF_RPDOS); loop++)
  {
    chk_sum += NVOL_ReadByte(offset);
    offset++;
  }
#endif

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
  // HB consumer data
  for (loop = 0; loop < (sizeof(HBCONS_CONFIG)*NR_OF_HB_CONSUMER); loop++)
  {
    chk_sum += NVOL_ReadByte(offset);
    offset++;
  }
#endif
#endif //MONITOR_ALL_NODES

  if ( (NVOL_ReadByte(start+2) != (chk_sum & 0x00FF)) ||
       (NVOL_ReadByte(start+3) != ((chk_sum>>8) & 0x00FF)) 
     )
  { // Checksum does not match, no parameters stored
    return FALSE;
  }

  if (! restore)
  { // No restore requested, verify only
    return TRUE;
  }

  // ID matches and checksum matches -> retrieve data
  offset = start + 4; // skip ID and chksum
  // Get Heartbeat time
  gMCOConfig.heartbeat_time = NVOL_ReadByte(offset+1);
  gMCOConfig.heartbeat_time <<= 8;
  gMCOConfig.heartbeat_time += NVOL_ReadByte(offset);
  offset += 2;

#if NR_OF_TPDOS > 0
  // Save TPDO configuration data
  pDest = (UNSIGNED8 *) &(gTPDOConfig[0]);
  for (loop = 0; loop < (sizeof(TPDO_CONFIG)*NR_OF_TPDOS); loop++)
  { // Get all TPDO Config Parameter from NVOL
    *pDest = NVOL_ReadByte(offset);  
    offset++;
    pDest++;
  }
  for (loop = 0; loop < NR_OF_TPDOS; loop++)
  { // restore default COB-IDs
    if (gTPDOConfig[loop].CAN.ID == 0xFFFF)
    {
      gTPDOConfig[loop].CAN.ID = (0x180+(0x100*loop)+MY_NODE_ID);
    }
  }
#endif

#if NR_OF_RPDOS > 0
  // Save RPDO configuration data
  pDest = (UNSIGNED8 *) &(gRPDOConfig[0]);
  for (loop = 0; loop < (sizeof(RPDO_CONFIG)*NR_OF_RPDOS); loop++)
  { // Get all RPDO Config Parameter from NVOL
    *pDest = NVOL_ReadByte(offset);  
    offset++;
    pDest++;
  }
  for (loop = 0; loop < NR_OF_RPDOS; loop++)
  { // restore default COB-IDs
    if (gRPDOConfig[loop].CANID == 0xFFFF)
    {
      gRPDOConfig[loop].CANID = (0x180+(0x100*loop)+MY_NODE_ID);
    }
  }
#endif

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
  // Get HB consumer data
  pDest = (UNSIGNED8 *) &(gHBCons[0]);
  for (loop = 0; loop < (sizeof(HBCONS_CONFIG)*NR_OF_HB_CONSUMER); loop++)
  { // Write all HB Config Parameter to NVOL
    *pDest = NVOL_ReadByte(offset);  
    offset++;
    pDest++;
  }
#endif
#endif // MONITOR_ALL_NODES

#if USE_SYNC
  // get CAN ID of SYNC object
  gMCOConfig.SYNCid = NVOL_ReadByte(offset);
  offset++;
  gMCOConfig.SYNCid += (((UNSIGNED16) NVOL_ReadByte(offset)) << 8);
  offset++;
#endif // USE_SYNC

  return TRUE;
}


/**************************************************************************
DOES:    Stores all communication parameters to NVOL
RETURNS: TRUE, if parameters were stored
         FALSE, if a write/verify occured
**************************************************************************/
UNSIGNED8 MCOSP_StoreComParameters (
  UNSIGNED16 start // destination address in NVOL memory
  )
{
// Note: first UNSIGNED16 used for ID "1X", second for checksum
UNSIGNED16 offset;
UNSIGNED16 loop;
UNSIGNED16 chk_sum;
UNSIGNED8 *pSrc;

  chk_sum = 0;
  offset = start;
  // erase ID
  NVOL_WriteByte(offset,0xFF);
  NVOL_WriteByte(offset+1,0xFF);
  offset += 4; // skip ID and chk_sum

  // Save Heartbeat time
  NVOL_WriteByte(offset,gMCOConfig.heartbeat_time & 0x00FF);
  chk_sum += gMCOConfig.heartbeat_time & 0x00FF;
  offset++;
  NVOL_WriteByte(offset,(gMCOConfig.heartbeat_time>>8) & 0x00FF);
  chk_sum += (gMCOConfig.heartbeat_time>>8) & 0x00FF;
  offset++;

#if NR_OF_TPDOS > 0
  // Save TPDO configuration data
  // Check COB-IDs, if they are default, save as FFFFh
  for (loop = 0; loop < NR_OF_TPDOS; loop++)
  { 
    if (gTPDOConfig[loop].CAN.ID == ((unsigned)0x180+(0x100*loop)+MY_NODE_ID))
    {
      gTPDOConfig[loop].CAN.ID = 0xFFFF;
    }
  }
  pSrc = (UNSIGNED8 *) &(gTPDOConfig[0]);
  for (loop = 0; loop < (sizeof(TPDO_CONFIG)*NR_OF_TPDOS); loop++)
  { // Write all TPDO Config Parameter to NVOL
    NVOL_WriteByte(offset,*pSrc);  
    chk_sum += *pSrc;
    offset++;
    pSrc++;
  }
  for (loop = 0; loop < NR_OF_TPDOS; loop++)
  { // restore default COB-IDs
    if (gTPDOConfig[loop].CAN.ID == 0xFFFF)
    {
      gTPDOConfig[loop].CAN.ID = (0x180+(0x100*loop)+MY_NODE_ID);
    }
  }
#endif

#if NR_OF_RPDOS > 0
  // Check COB-IDs, if they are default, save as FFFFh
  for (loop = 0; loop < NR_OF_RPDOS; loop++)
  { 
    if (gRPDOConfig[loop].CANID == ((unsigned)0x200+(0x100*loop)+MY_NODE_ID))
    {
      gRPDOConfig[loop].CANID = 0xFFFF;
    }
  }
  // Save RPDO configuration data
  pSrc = (UNSIGNED8 *) &(gRPDOConfig[0]);
  for (loop = 0; loop < (sizeof(RPDO_CONFIG)*NR_OF_RPDOS); loop++)
  { // Write all RPDO Config Parameter to NVOL
    NVOL_WriteByte(offset,*pSrc);  
    chk_sum += *pSrc;
    offset++;
    pSrc++;
  }
  for (loop = 0; loop < NR_OF_RPDOS; loop++)
  { 
    if (gRPDOConfig[loop].CANID == 0xFFFF)
    {
      gRPDOConfig[loop].CANID = (0x200+(0x100*loop)+MY_NODE_ID);
    }
  }
#endif

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
  // Save HB consumer data
  pSrc = (UNSIGNED8 *) &(gHBCons[0]);
  for (loop = 0; loop < (sizeof(HBCONS_CONFIG)*NR_OF_HB_CONSUMER); loop++)
  { // Write all HB Config Parameter to NVOL
    NVOL_WriteByte(offset,*pSrc);  
    chk_sum += *pSrc;
    offset++;
    pSrc++;
  }
#endif
#endif // MONITOR_ALL_NODES

#if USE_SYNC
  NVOL_WriteByte(offset,gMCOConfig.SYNCid & 0x00FF);
  chk_sum += gMCOConfig.SYNCid & 0x00FF;
  offset++;
  NVOL_WriteByte(offset,(gMCOConfig.SYNCid>>8) & 0x00FF);
  chk_sum += (gMCOConfig.SYNCid>>8) & 0x00FF;
  offset++;
#endif // USE_SYNC

  // No further consecutive writes here
  NVOL_WriteComplete();

  // Now write chksum + ID
  NVOL_WriteByte(start,'1');  
  NVOL_WriteByte(start+1,'X'); 
  NVOL_WriteByte(start+2,chk_sum & 0x00FF);  
  NVOL_WriteByte(start+3,(chk_sum>>8) & 0x00FF);  

  // No further consecutive writes after this
  NVOL_WriteComplete();

  // Now verify if valid data was written 
  return MCOSP_GetStoredComParameters(start,FALSE);
}


/**************************************************************************
DOES:    Retrieves OD data from NVOL to process image
RETURNS: TRUE, if data is present in NVOL
         FALSE, if no data is present in NVOL
**************************************************************************/
UNSIGNED8 MCOSP_GetStoredODParameters (
  UNSIGNED16 start, // destination address in NVOL memory
  UNSIGNED16 idx_start, // index range to store
  UNSIGNED16 idx_end, // end of index range to store
  UNSIGNED8 restore // TRUE: restore data, FALSE: only perform chksum test
  )
{
// Note: first UNSIGNED16 used for ID "OD", second for checksum
UNSIGNED16 offset;
UNSIGNED16 loop;
UNSIGNED16 chk_sum;
UNSIGNED8 *pDest;
OD_PROCESS_DATA_ENTRY MEM_CONST *pOD; // pointer into OD array

  if ( (NVOL_ReadByte(start) != 'O') ||
       (NVOL_ReadByte(start+1) != 'D') 
     )
  { // ID does not match, no parameters stored
    return FALSE;
  }

  // Verify checksum
  chk_sum = 0;
  offset = start + 4; // skip ID and chk_sum
  pOD = &(gODProcTable[0]);
  while (pOD->idx != 0xFFFF)
  {
    if ((pOD->idx >= idx_start) && (pOD->idx <= idx_end))
    { // entry is in right index range
      if (pOD->len & ODWR)
      { // entry can be written to
        for (loop = 0; loop < (pOD->len & 0x0F); loop++)
        {
          chk_sum += NVOL_ReadByte(offset);
          offset++;
        }
      }
    }
    pOD++;
  }
  // Checksum calculated, now verify
  if ( (NVOL_ReadByte(start+2) != (chk_sum & 0x00FF)) ||
       (NVOL_ReadByte(start+3) != ((chk_sum>>8) & 0x00FF)) 
     )
  { // Checksum does not match, no parameters stored
    return FALSE;
  }

  if (! restore)
  { // No restore requested, verify only
    return TRUE;
  }

  // ID matches and checksum matches -> retrieve data
  offset = start + 4; // skip ID and chksum
  pOD = &(gODProcTable[0]);
  while (pOD->idx != 0xFFFF)
  {
    if ((pOD->idx >= idx_start) && (pOD->idx <= idx_end))
    { // entry is in right index range
      if (pOD->len & ODWR)
      { // entry can be written to
        pDest = &(gProcImg[pOD->offset]);
        for (loop = 0; loop < (pOD->len & 0x0F); loop++)
        {
          *pDest = NVOL_ReadByte(offset);
          offset++;
          pDest++;
        }
      }
    }
    pOD++;
  }

  return TRUE;
}


/**************************************************************************
DOES:    Stores data from OD to NVOL
RETURNS: TRUE, if data is present in NVOL
         FALSE, if no data is present in NVOL
**************************************************************************/
UNSIGNED8 MCOSP_StoreODParameters (
  UNSIGNED16 start, // destination address in NVOL memory
  UNSIGNED16 idx_start, // index range to store
  UNSIGNED16 idx_end // end of index range to store
  )
{
// Note: first UNSIGNED16 used for ID "OD", second for checksum
UNSIGNED16 offset;
UNSIGNED16 loop;
UNSIGNED16 chk_sum;
UNSIGNED8 *pSrc;
OD_PROCESS_DATA_ENTRY MEM_CONST *pOD; // pointer into OD array

  chk_sum = 0;
  offset = start;
  // erase ID
  NVOL_WriteByte(offset,0xFF);
  NVOL_WriteByte(offset+1,0xFF);
  offset += 4; // skip ID and chk_sum

  pOD = &(gODProcTable[0]);
  while (pOD->idx != 0xFFFF)
  {
    if ((pOD->idx >= idx_start) && (pOD->idx <= idx_end))
    { // entry is in right index range
      if (pOD->len & ODWR)
      { // entry can be written to
        // store entry
        pSrc = &(gProcImg[pOD->offset]);
        for (loop = 0; loop < (pOD->len & 0x0F); loop++)
        {
          NVOL_WriteByte(offset,*pSrc);
          chk_sum += *pSrc;
          offset++;
          pSrc++;
        }
      }
    }
    pOD++;
  }
  // No further consecutive writes after this
  NVOL_WriteComplete();

  // Now write chksum + ID
  NVOL_WriteByte(start,'O');  
  NVOL_WriteByte(start+1,'D');  
  NVOL_WriteByte(start+2,chk_sum & 0x00FF);  
  NVOL_WriteByte(start+3,(chk_sum>>8) & 0x00FF);  
                         
  // No further consecutive writes after this
  NVOL_WriteComplete();

  // Verify if valid data was stored
  return MCOSP_GetStoredODParameters(start,idx_start,idx_end,FALSE);
}


/**************************************************************************
DOES:    Implements the Store Parameters and Restore Parameters
         functionaility.
RETURNS: TRUE, if data was restored
         FALSE. if no valid data was found
**************************************************************************/
UNSIGNED8 MCOSP_StoreParameters (
  UNSIGNED16 idx, // set to 0x1010 for store, to 0x1011 for restore
  UNSIGNED8 sub // subindex
  )
{
UNSIGNED16 location[5]; // start offsets and sizes of areas in NVOL
UNSIGNED8 ret_value;

  // Get info about current NVOL usage
  MCOSP_GetNVOLUsage(&(location[0]));

  if (idx == STORE_PARAMETERS)
  { // Save Parameters
  	ret_value = TRUE;
    if ((sub == PARAMETERS_ALL) || (sub == PARAMETERS_COMMUNICATION))
    { // Save Configuration Parameters 0x1XXX
      ret_value &= MCOSP_StoreComParameters(location[1]);
    }
    if ((sub == PARAMETERS_ALL) || (sub == PARAMETERS_APPLICATION))
    { // Save Application Parameters 0x6XXX
      ret_value &= MCOSP_StoreODParameters(location[2],0x6000,0x9FFF);
    }
    if ((sub == PARAMETERS_ALL) || (sub == PARAMETERS_MANUFACTURER))
    { // Save Manufacturer Parameters 0x2XXX
      ret_value &= MCOSP_StoreODParameters(location[3],0x2000,0x5FFF);
    }
    return ret_value;
  }
  if (idx == RESTORE_PARAMETERS)
  { // Restore Parameters
    if ((sub == PARAMETERS_ALL) || (sub == PARAMETERS_COMMUNICATION))
    { // Restore Configuration Parameters 0x1XXX
      // Destroy ID and Chksum
      NVOL_WriteByte(location[1],0xFF);
      NVOL_WriteByte(location[1]+2,NVOL_ReadByte(location[1]+2)+1);
      // No further consecutive writes after this
      NVOL_WriteComplete();
    }
    if ((sub == PARAMETERS_ALL) || (sub == PARAMETERS_APPLICATION))
    { // Restore Application Parameters 0x6XXX
      NVOL_WriteByte(location[2],0xFF);
      NVOL_WriteByte(location[2]+2,NVOL_ReadByte(location[2]+2)+1);
      // No further consecutive writes after this
      NVOL_WriteComplete();
    }
    if ((sub == PARAMETERS_ALL) || (sub == PARAMETERS_MANUFACTURER))
    { // Restore Manufacturer Parameters 0x2XXX
      NVOL_WriteByte(location[3],0xFF);
      NVOL_WriteByte(location[3]+2,NVOL_ReadByte(location[3]+2)+1);
      // No further consecutive writes after this
      NVOL_WriteComplete();
    }
  }
  return TRUE;
}


/**************************************************************************
PUBLIC FUNCTIONS
***************************************************************************/ 

/**************************************************************************
DOES:    Checks the non-volatile memory for saved parameters and retrives
         them all
RETURNS: Nothing.
**************************************************************************/
void MCOSP_GetStoredParameters (
  void  
  )
{
UNSIGNED16 location[5]; // start offsets and sizes of areas in NVOL

  // Get info about current NVOL usage
  MCOSP_GetNVOLUsage(&(location[0]));

  // Try to retrieve communication parameters
  MCOSP_GetStoredComParameters(location[1],TRUE);

  // Try to retrieve device profile parameters
  MCOSP_GetStoredODParameters(location[2],0x6000,0x9FFF,TRUE);

  // Try to retrieve manufacturer parameters
  MCOSP_GetStoredODParameters(location[3],0x2000,0x5FFF,TRUE);
}
#endif // USE_STORE_PARAMETERS

/*----------------------- END OF FILE ----------------------------------*/
