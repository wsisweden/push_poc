/**************************************************************************
MODULE:    MCO
CONTAINS:  MicroCANopen implementation
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS IS THE COMMERCIAL PLUS VERSION OF MICROCANOPEN
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
           See file license_commercial_plus.txt
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2009-09-24 21:22:11 -0300 (Thu, 24 Sep 2009) $
           $LastChangedRevision: 1447 $
***************************************************************************/ 

#include "mco.h"
#include "mcohw.h"

#include <string.h>

#if USE_MCOP
#include "mcop.h"
#endif

#if USE_EXTENDED_SDO
#include "xsdo.h"
#endif

#if USE_LSS_SLAVE
#include "lss.h"
#endif

#if USE_MICROLSS
#include "mlss.h"
#endif

#if USE_LEDS
#include "mcohw_LEDs.h"
#endif

/**************************************************************************
GLOBAL VARIABLES
***************************************************************************/ 

// this structure holds all node specific configuration
MCO_CONFIG MEM_FAR gMCOConfig;

#if USE_EMCY
extern EMCY_CONFIG MEM_FAR mEF;
 #ifndef EMCY_INHIBIT_TIME
 // Set default emergency inhibit time
 #define EMCY_INHIBIT_TIME 0
 #endif
#endif

#if ! MONITOR_ALL_NODES
 #if (NR_OF_HB_CONSUMER > 0)
// from MCOP, HB consumer values
extern HBCONS_CONFIG gHBCons[NR_OF_HB_CONSUMER];
 #endif // (NR_OF_HB_CONSUMER > 0)
#endif

#if NR_OF_TPDOS > 0
// this structure holds all the TPDO configuration data for the TPDOs
TPDO_CONFIG MEM_FAR gTPDOConfig[NR_OF_TPDOS];
#endif

// this is the next TPDO to be checked in MCO_ProcessStack
UNSIGNED16 MEM_FAR gTPDONr = NR_OF_TPDOS;

#if NR_OF_RPDOS > 0
// this structure holds all the RPDO configuration data for the RPDOs
RPDO_CONFIG MEM_FAR gRPDOConfig[NR_OF_RPDOS];
#endif

// this structure holds the current receive message
CAN_MSG MEM_FAR gRxCAN;

// this structure holds the CAN message for SDO responses or aborts
CAN_MSG MEM_FAR gTxSDO;

// process image from user_xxxx.c
extern UNSIGNED8 MEM_PROC gProcImg[];

// table with SDO Responses for read requests to OD - defined in user_xxx.c
extern UNSIGNED8 MEM_CONST gSDOResponseTable[];

// table with Object Dictionary entries to process Data
extern OD_PROCESS_DATA_ENTRY MEM_CONST gODProcTable[];


/**************************************************************************
LOCAL FUNCTIONS
***************************************************************************/

#if USE_LEDS
/**************************************************************************
DOES:    This function switches the CANopen Err and Run LEDs
         as specified by DR-303-3
         It must be called every 200ms
GLOBALS: Uses global macros LED_xxx_ON and LED_xxx_OFF.
         Uses module variables mLEDtoggle and mLEDcnt for the blinking.
**************************************************************************/
void MCO_SwitchLEDs  (
  void
  )
{
  // For blinking or flickering
  gMCOConfig.LEDtoggle = ~gMCOConfig.LEDtoggle;

  switch(gMCOConfig.LEDRun) // Run LED
  {
    case LED_OFF:
      LED_RUN_OFF;
      break;
    case LED_ON:
      LED_RUN_ON;
      break;
    case LED_BLINK:
      if (gMCOConfig.LEDtoggle == 0)
      {
        LED_RUN_ON;
      }
      else
      {
        LED_RUN_OFF;
      }
      break;
    case LED_FLASH1:
      gMCOConfig.LEDcntR++;
      if (gMCOConfig.LEDcntR >= 6)
      {
        gMCOConfig.LEDcntR = 0;
        LED_RUN_ON;
      }
      else
      {
        LED_RUN_OFF;
      }
      break;
    case LED_FLASH2:
      gMCOConfig.LEDcntR++;
      if (gMCOConfig.LEDcntR >= 8)
      {
        gMCOConfig.LEDcntR = 0;
      }
      if ((gMCOConfig.LEDcntR == 0) || (gMCOConfig.LEDcntR == 2))
      {
        LED_RUN_ON;
      }
      else
      {
        LED_RUN_OFF;
      }
      break;
    case LED_FLASH3:
      gMCOConfig.LEDcntR++;
      if (gMCOConfig.LEDcntR >= 10)
      {
        gMCOConfig.LEDcntR = 0;
      }
      if ((gMCOConfig.LEDcntR == 0) || (gMCOConfig.LEDcntR == 2) || (gMCOConfig.LEDcntR == 4))
      {
        LED_RUN_ON;
      }
      else
      {
        LED_RUN_OFF;
      }
      break;
    case LED_FLASH4:
      gMCOConfig.LEDcntR++;
      if (gMCOConfig.LEDcntR >= 12)
      {
        gMCOConfig.LEDcntR = 0;
      }
      if ((gMCOConfig.LEDcntR == 0) || (gMCOConfig.LEDcntR == 2) || 
          (gMCOConfig.LEDcntR == 4) || (gMCOConfig.LEDcntR == 6)
         )
      {
        LED_RUN_ON;
      }
      else
      {
        LED_RUN_OFF;
      }
      break;
    default:
      break;
  }

  switch(gMCOConfig.LEDErr) // Error LED
  {
    case LED_OFF:
      LED_ERR_OFF;
      break;
    case LED_ON:
      LED_ERR_ON;
      break;
    case LED_BLINK: // flicker when called every 50ms
      if (gMCOConfig.LEDtoggle == 0)
      {
        LED_ERR_ON;
      }
      else
      {
        LED_ERR_OFF;
      }
      break;
    case LED_FLASH1:
      gMCOConfig.LEDcntE++;
      if (gMCOConfig.LEDcntE >= 6)
      {
        gMCOConfig.LEDcntE = 0;
        LED_ERR_ON;
      }
      else
      {
        LED_ERR_OFF;
      }
      break;
    case LED_FLASH2:
      gMCOConfig.LEDcntE++;
      if (gMCOConfig.LEDcntE >= 8)
      {
        gMCOConfig.LEDcntE = 0;
      }
      if ((gMCOConfig.LEDcntE == 0) || (gMCOConfig.LEDcntE == 2))
      {
        LED_ERR_ON;
      }
      else
      {
        LED_ERR_OFF;
      }
      break;
    case LED_FLASH3:
      gMCOConfig.LEDcntE++;
      if (gMCOConfig.LEDcntE >= 10)
      {
        gMCOConfig.LEDcntE = 0;
      }
      if ((gMCOConfig.LEDcntE == 0) || (gMCOConfig.LEDcntE == 2) || (gMCOConfig.LEDcntE == 4))
      {
        LED_ERR_ON;
      }
      else
      {
        LED_ERR_OFF;
      }
      break;
    case LED_FLASH4:
      gMCOConfig.LEDcntE++;
      if (gMCOConfig.LEDcntE >= 12)
      {
        gMCOConfig.LEDcntE = 0;
      }
      if ((gMCOConfig.LEDcntE == 0) || (gMCOConfig.LEDcntE == 2) || 
          (gMCOConfig.LEDcntE == 4) || (gMCOConfig.LEDcntE == 6)
         )
      {
        LED_ERR_ON;
      }
      else
      {
        LED_ERR_OFF;
      }
      break;
    default:
      break;
  }
}
#endif // USE_LEDS


/**************************************************************************
DOES:    Search the SDO Response table for a specifc searchODindex and subindex.
RETURNS: 0xFFFF if not found, otherwise the number of the record found
         (staring at zero)
**************************************************************************/
UNSIGNED16 MCO_SearchOD (
  UNSIGNED16 searchODindex,  // Index of OD entry searched
  UNSIGNED8 subindex // Subindex of OD entry searched 
  )
{
  UNSIGNED16 i;
  UNSIGNED8 i_hi, hi;
  UNSIGNED8 i_lo, lo;
  UNSIGNED8 MEM_CONST *p;
  UNSIGNED8 MEM_CONST *r;

  i = 0;
  i_hi = (UNSIGNED8) (searchODindex >> 8);
  i_lo = (UNSIGNED8) searchODindex;
  r = &(gSDOResponseTable[0]);
  while (i < 0xFFFF)
  {
    p = r;
    // set r to next record in table
    r += 8;
    // skip command byte
    p++;
    lo = *p;
    p++;
    hi = *p;
    // if index in table is 0xFFFF, then this is the end of the table
    if ((lo == 0xFF) && (hi == 0xFF))
    {
      return 0xFFFF;
    }
    if (lo == i_lo)
    { 
      if (hi == i_hi)
      { 
        p++;
        // entry found?
        if (*p == subindex)
        {
          return i;
        }
      }
    }
    i++;
  }
  // not found
  return 0xFFFF;
}


/**************************************************************************
DOES:    Search the gODProcTable from user_xxxx.c for a specifc index 
         and subindex.
RETURNS: 0xFFFF if not found, otherwise the number of the record found
         (staring at zero)
**************************************************************************/
UNSIGNED16 MCO_SearchODProcTable (
  UNSIGNED16 searchODProcTableIndex,   // Index of OD entry searched
  UNSIGNED8 subindex  // Subindex of OD entry searched 
  )
{
  UNSIGNED16 j = 0;
  UNSIGNED16 compare;
  // pointer to od records
  OD_PROCESS_DATA_ENTRY MEM_CONST *pOD;

  // initialize pointer
  pOD = &(gODProcTable[0]);
  // loop until maximum table size
  while (j < 0xFFFF)
  {
    compare = pOD->idx;
    // end of table reached? 
    if (compare == 0xFFFF)
    {
      return 0xFFFF;
    }
    // index found?
    if (compare == searchODProcTableIndex)
    {
      // subindex found?
      if (pOD->subidx == subindex)
      {
        return j;
      }
    }
    // increment by SIZEOF(OD_PROCESS_DATA_ENTRY)
    pOD++;
    j++;
  }
  // not found
  return 0xFFFF;
}


/**************************************************************************
DOES:    Common exit routine for SDO_Handler. 
         Send SDO response with variable length (1-4 bytes).
         Assumes that gTxCAN.ID, LEN and BUF[1-3] are already set
RETURNS: 1 if response transmitted 
**************************************************************************/
UNSIGNED8 MCO_ReplyWith (
  UNSIGNED8 *pDat,  // pointer to sdo data
  UNSIGNED8 len     // number of bytes of data in SDO
  )
{
  UNSIGNED8 k; // for loop counter

  // expedited, len of data
  gTxSDO.BUF.BUF[0] = 0x43 | ((4-len) << 2);
  // Needed to pass conformance test: clear unused bytes
  gTxSDO.BUF.BUF[4] = 0;
  gTxSDO.BUF.BUF[5] = 0;
  gTxSDO.BUF.BUF[6] = 0;
  gTxSDO.BUF.BUF[7] = 0;
  // copy data
  for (k = 4; k < len+4; k++)
  {
    gTxSDO.BUF.BUF[k] = *pDat;
    pDat++;
  }

  // transmit message
  if (!MCOHW_PushMessage(&gTxSDO))
  {
    // failed to transmit
    MCOUSER_FatalError(ERROFL_SDO);
  }

  // transmitted ok
  return 1;
}


/**************************************************************************
DOES:    Generates an SDO Abort Response
RETURNS: nothing
**************************************************************************/
void MCO_SendSDOAbort (
  UNSIGNED32 ErrorCode  // 4 byte SDO abort error code
  )
{
UNSIGNED8 i;

#if USE_EXTENDED_SDO
  // Inform extended SDO handling about the abort
  // XSDO_Abort();
  // commented as SDO server info is not available here
#endif // USE_EXTENDED_SDO

  // construct message data
  gTxSDO.BUF.BUF[0] = 0x80;
  for (i=0;i<4;i++)
  {
    gTxSDO.BUF.BUF[4+i] = (UNSIGNED8)ErrorCode;
    ErrorCode >>= 8;
  }

  // transmit message
  if (!MCOHW_PushMessage(&gTxSDO))
  {
    // failed to transmit
    MCOUSER_FatalError(ERROFL_SDO);
  }
}


/**************************************************************************
DOES:    Handle an incoimg SDO request.
RETURNS: returns 1 if SDO access success, returns 0 if SDO abort generated
**************************************************************************/
UNSIGNED8 MCO_HandleSDORequest (
  UNSIGNED8 *pData  // pointer to 8 data bytes with SDO data
  )
{
  // command byte of SDO request
  UNSIGNED8 cmd;
  // index of SDO request
  UNSIGNED16 handleSDORequestIndex;
  // subindex of SDO request
  UNSIGNED8 subindex;
  // search result of Search_OD
  UNSIGNED16 found;
  // buffer for read of error data
  UNSIGNED32 buf32;
  UNSIGNED8 len;
  // pointer to an entry in gODProcTable
  OD_PROCESS_DATA_ENTRY MEM_CONST *pOD;
#if USECB_ODSERIAL
  // buffer for serial number
  UNSIGNED32 serial;
#endif 
#if USE_EXTENDED_SDO
  UNSIGNED8 sdoserv;
#endif 

#if USECB_SDOREQ
    switch (MCOUSER_SDORequest(pData))
    {
    case 0:  // MCOUSER_SDORequest replied with an ABORT
      return 0;
    case 1:  // MCOUSER_SDORequest sent a response
      return 1;
    default: // MCOUSER_SDORequest did not do anything
      break;
    }
#endif // USECB_SDOREQ

#if USE_EXTENDED_SDO
  sdoserv = SDOSERVER(gTxSDO.ID);
  switch(XSDO_HandleExtended(pData,&(gTxSDO),sdoserv))
  {
    case 1:
	  // SDO response was sent
	  return 1;
	case 2:
	  // SDO abort was sent
	  return 0;
	default:
	  break;
  }
#endif // USE_EXTENDED_SDO

  // init variables
  // upper 3 bits are the command
  cmd = *pData & 0xE0;
  // get high byte of index
  handleSDORequestIndex = pData[2];
  // add low byte of index
  handleSDORequestIndex = (handleSDORequestIndex << 8) + pData[1];
  // subindex
  subindex = pData[3];

  // Copy Multiplexor into response
  // index low
  gTxSDO.BUF.BUF[1] = pData[1];
  // index high
  gTxSDO.BUF.BUF[2] = pData[2];
  // subindex
  gTxSDO.BUF.BUF[3] = pData[3];

#if USE_BLOCKED_SDO
  // Translate block read into regular read
  if ((cmd == 0xA0) && (pData[5] != 0))
  {
    cmd = 0x40;
  }
#endif

  // is it a read or write command?
  if ((cmd == 0x40) || (cmd == 0x20)) 
  {
#if USECB_ODSERIAL
    if ((handleSDORequestIndex == 0x1018) && (subindex == 4))
    {
      // read command?
      if (cmd == 0x40)
      {
        // Get serial number from call-back function
        serial = MCOUSER_GetSerial();
        return MCO_ReplyWith((UNSIGNED8 *)&(serial),4);
      }
      MCO_SendSDOAbort(SDO_ABORT_READONLY);
      return 0;
    }
#endif // USECB_ODSERIAL

#if (NR_OF_RPDOS > 0) || (NR_OF_TPDOS > 0)
  #if (NR_OF_RPDOS > 0)
    if ((handleSDORequestIndex >= 0x1400) && (handleSDORequestIndex <= 0x15FF))
    { // RPDO
      return SDO_HandlePDOComParam(1,handleSDORequestIndex,pData);
    }
  #endif
  #if (NR_OF_TPDOS > 0)
    if ((handleSDORequestIndex >= 0x1800) && (handleSDORequestIndex <= 0x19FF))
    { // TPDO
      return SDO_HandlePDOComParam(0,handleSDORequestIndex,pData);
    }
  #endif
#endif // (NR_OF_RPDOS > 0) || (NR_OF_TPDOS > 0)

    // access to [1017,00] - heartbeat time
    if ((handleSDORequestIndex == 0x1017) && (subindex == 0x00))
    {
      // read command
      if (cmd == 0x40)
      {
        // expedited, 2 bytes of data
        gTxSDO.BUF.BUF[0] = 0x4B;
        gTxSDO.BUF.BUF[4] = (UNSIGNED8) gMCOConfig.heartbeat_time;
        gTxSDO.BUF.BUF[5] = (UNSIGNED8) (gMCOConfig.heartbeat_time >> 8);
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // expedited write command with 2 bytes of data
      if (*pData == 0x2B)
      {
        gMCOConfig.heartbeat_time = pData[5];
        gMCOConfig.heartbeat_time = (gMCOConfig.heartbeat_time << 8) + pData[4];
        // reset heartbeat time for immediate transmission
        gMCOConfig.heartbeat_timestamp = MCOHW_GetTime();
        // write response
        gTxSDO.BUF.BUF[0] = 0x60;
        // Needed to pass conformance test: clear unused bytes
        gTxSDO.BUF.BUF[4] = 0;
        gTxSDO.BUF.BUF[5] = 0;
        gTxSDO.BUF.BUF[6] = 0;
        gTxSDO.BUF.BUF[7] = 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      if (*pData == 0x21)
      {
        // Needed to pass conformance test: Abort code different
        // for segmented write command with 2 bytes of data
        MCO_SendSDOAbort(SDO_ABORT_UNKNOWN_COMMAND);
      }
      else
      {
        MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      }
      return 0;
    }

#ifdef REBOOT_FLAG_ADR
    // Supporting the ESAcademy CANopen Bootloader
    if ((handleSDORequestIndex == 0x1F51) && (subindex == 0x01))
    {
      // read command
      if (cmd == 0x40)
      {
        // expedited, 1 byte of data
        gTxSDO.BUF.BUF[0] = 0x4F;
        gTxSDO.BUF.BUF[4] = 1;   // return "application running"
        gTxSDO.BUF.BUF[5] = (UNSIGNED8) 0;
        gTxSDO.BUF.BUF[6] = (UNSIGNED8) 0;
        gTxSDO.BUF.BUF[7] = (UNSIGNED8) 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }

      // expedited write command with 1 byte of data
      if (*pData == 0x2F)
      {
        // Write of 0 (stop application)?
        if (pData[4] == 0x00)
        { // Set signal to bootloader
          REBOOT_FLAG = REBOOT_BOOTLOAD_VAL;
          // write response
          gTxSDO.BUF.BUF[0] = 0x60;
          // Needed to pass conformance test: clear unused bytes
          gTxSDO.BUF.BUF[4] = 0;
          gTxSDO.BUF.BUF[5] = 0;
          gTxSDO.BUF.BUF[6] = 0;
          gTxSDO.BUF.BUF[7] = 0;
          if (!MCOHW_PushMessage(&gTxSDO))
          {
            MCOUSER_FatalError(ERROFL_SDO);
          }
          return 1;
        }
      }
      MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      return 0;
    }
#endif // REBOOT_FLAG_ADR


#if USE_SYNC
    // dynamic access to [1005]
    // access to [1005,00] - SYNC COB-ID
    if ((handleSDORequestIndex == 0x1005) && (subindex == 0x00))
    {
      // read command
      if (cmd == 0x40)
      {
        // expedited, 4 bytes of data
        gTxSDO.BUF.BUF[0] = 0x43;
        gTxSDO.BUF.BUF[4] = (UNSIGNED8) gMCOConfig.SYNCid & 0x00FF;
        gTxSDO.BUF.BUF[5] = (UNSIGNED8) (gMCOConfig.SYNCid>>8) & 0x00FF;
        gTxSDO.BUF.BUF[6] = (UNSIGNED8) 0;
        gTxSDO.BUF.BUF[7] = (UNSIGNED8) 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // write access
      if (*pData == (1<<5) + 3)
      { // expedited write command with 4 bytes of data
        // Retrieve new COB-ID
        gMCOConfig.SYNCid = pData[5];        
        gMCOConfig.SYNCid <<= 8;
        gMCOConfig.SYNCid += pData[4];        
        // Set new CAN receive filter
        MCOHW_SetCANFilter(gMCOConfig.SYNCid);
        // write response
        gTxSDO.BUF.BUF[0] = 0x60;
        // Needed to pass conformance test: clear unused bytes
        gTxSDO.BUF.BUF[4] = 0;
        gTxSDO.BUF.BUF[5] = 0;
        gTxSDO.BUF.BUF[6] = 0;
        gTxSDO.BUF.BUF[7] = 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // if we reach here, fail
      MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      return 0;
    }
#endif // USE_SYNC


#if USE_EMCY
    // dynamic access to [1014]
    // access to [1014,00] - EMCY COB-ID
    if ((handleSDORequestIndex == 0x1014) && (subindex == 0x00))
    {
      // read command
      if (cmd == 0x40)
      {
        // expedited, 4 bytes of data
        gTxSDO.BUF.BUF[0] = 0x43;
        gTxSDO.BUF.BUF[4] = (UNSIGNED8) 0x80 + MY_NODE_ID;
        gTxSDO.BUF.BUF[5] = (UNSIGNED8) 0;
        gTxSDO.BUF.BUF[6] = (UNSIGNED8) 0;
        gTxSDO.BUF.BUF[7] = (UNSIGNED8) 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // fail on write access
      MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      return 0;
    }
    // access to [1015,00] - EMCY Inhibit Time
    if ((handleSDORequestIndex == 0x1015) && (subindex == 0x00))
    {
      // read command
      if (cmd == 0x40)
      {
        // expedited, 2 bytes of data
        gTxSDO.BUF.BUF[0] = 0x4B;
        gTxSDO.BUF.BUF[4] = (UNSIGNED8) mEF.emcy_inhibit;
        gTxSDO.BUF.BUF[5] = (UNSIGNED8) (mEF.emcy_inhibit >> 8);
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // expedited write command with 2 bytes of data
      if (*pData == 0x2B)
      {
        mEF.emcy_inhibit = pData[5];
        mEF.emcy_inhibit = (mEF.emcy_inhibit << 8) + pData[4];
        // reset timestamp for immediate transmission
        mEF.emcy_timestamp = MCOHW_GetTime();
        // write response
        gTxSDO.BUF.BUF[0] = 0x60;
        // Needed to pass conformance test: clear unused bytes
        gTxSDO.BUF.BUF[4] = 0;
        gTxSDO.BUF.BUF[5] = 0;
        gTxSDO.BUF.BUF[6] = 0;
        gTxSDO.BUF.BUF[7] = 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      return 0;
    }
#if ERROR_FIELD_SIZE > 0
    // access to [1003,xx] - Error Field (History)
    if (handleSDORequestIndex == 0x1003)
    {
      // read command
      if (cmd == 0x40)
      {
        buf32 = MCOP_ErrField_Get(subindex);
        if (buf32 != 0xFFFFFFFF)
        {
          if (subindex == 0)
          { // expedited, 1 byte of data
            gTxSDO.BUF.BUF[0] = 0x4F;
            gTxSDO.BUF.BUF[4] = (UNSIGNED8)buf32;
            gTxSDO.BUF.BUF[5] = 0;
            gTxSDO.BUF.BUF[6] = 0;
            gTxSDO.BUF.BUF[7] = 0;
          }
          else
          { // expedited, 4 bytes of data
            gTxSDO.BUF.BUF[0] = 0x43;
            gTxSDO.BUF.BUF[4] = (UNSIGNED8) buf32;
            gTxSDO.BUF.BUF[5] = (UNSIGNED8)(buf32 >> 8);
            gTxSDO.BUF.BUF[6] = (UNSIGNED8)(buf32 >> 16);
            gTxSDO.BUF.BUF[7] = (UNSIGNED8)(buf32 >> 24);
          }
          if (!MCOHW_PushMessage(&gTxSDO))
          {
            MCOUSER_FatalError(ERROFL_SDO);
          }
          return 1;
        }
        else
        {
          MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
          return 0;
        }
      }
      else
      {
        if (subindex == 0)
        { // flush error history
          MCOP_ErrField_Flush();
          // write response
          gTxSDO.BUF.BUF[0] = 0x60;
          // Needed to pass conformance test: clear unused bytes
          gTxSDO.BUF.BUF[4] = 0;
          gTxSDO.BUF.BUF[5] = 0;
          gTxSDO.BUF.BUF[6] = 0;
          gTxSDO.BUF.BUF[7] = 0;
          if (!MCOHW_PushMessage(&gTxSDO))
          {
            MCOUSER_FatalError(ERROFL_SDO);
          }
          return 1;
        }
      }
      // if we reach here, fail
      MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      return 0;
    }
#endif // ERROR_FIELD_SIZE > 0
#endif // USE_EMCY

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
#if DYNAMIC_HB_CONSUMER
    // dynamic read/write accesses
    // access to [1016,xx] - heartbeat consumer time
    if ((handleSDORequestIndex == 0x1016) && (subindex > 0) && (subindex <= NR_OF_HB_CONSUMER))
    {
      // read command
      if (cmd == 0x40)
      {
        subindex--; // now can directly be used as array pointer
        // expedited, 4 bytes of data
        gTxSDO.BUF.BUF[0] = 0x43;
        gTxSDO.BUF.BUF[4] = (UNSIGNED8) gHBCons[subindex].time;
        gTxSDO.BUF.BUF[5] = (UNSIGNED8) (gHBCons[subindex].time >> 8);
        gTxSDO.BUF.BUF[6] = (UNSIGNED8) gHBCons[subindex].can_id;
        gTxSDO.BUF.BUF[7] = (UNSIGNED8) 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // expedited write command with 4 bytes of data
      if ( (*pData == 0x23) && 
           (pData[6] != MY_NODE_ID) && // not our own node ID
           (pData[6] > 0) && (pData[6] < 128) // && // not an illegal node ID
           // removed to work with PCOMPDS V1.23
           //(!MCOP_IsHBMonitored(subindex,pData[6]))
         )
      { // only if requested node ID is unequal to own node id and not yet used
        MCOP_InitHBConsumer(subindex,pData[6],((UNSIGNED16)(pData[5])<< 8) + pData[4]);
        // write response
        gTxSDO.BUF.BUF[0] = 0x60;
        // Needed to pass conformance test: clear unused bytes
        gTxSDO.BUF.BUF[4] = 0;
        gTxSDO.BUF.BUF[5] = 0;
        gTxSDO.BUF.BUF[6] = 0;
        gTxSDO.BUF.BUF[7] = 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      else
      {
        MCO_SendSDOAbort(SDO_ABORT_PARAMETER);
        return 0;
      }
    }
#endif // DYNAMIC_HB_CONSUMER
#endif // (NR_OF_HB_CONSUMER > 0)
#endif // MONITOR_ALL_NODES

#if USE_STORE_PARAMETERS
    // access to [1010,xx] or [1011,xx] - Store Parameters, Restore
    if ( ((handleSDORequestIndex == 0x1010) || (handleSDORequestIndex == 0x1011)) &&
         (subindex > 0) && (subindex <= 4)
       )
    {
      // read command
      if (cmd == 0x40)
      {
        if (subindex == 0)
        {
          // expedited, 1 byte of data
          gTxSDO.BUF.BUF[0] = 0x4F;
          gTxSDO.BUF.BUF[4] = 4;
        }
        else
        { // access supported
          // expedited, 4 bytes of data
          gTxSDO.BUF.BUF[0] = 0x43;
          gTxSDO.BUF.BUF[4] = 1;
        }
        gTxSDO.BUF.BUF[5] = 0;
        gTxSDO.BUF.BUF[6] = 0;
        gTxSDO.BUF.BUF[7] = 0;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // expedited write command with 4 bytes of data
      if (*pData == 0x23)
      {
        if(
// ===== BEGIN COMPATIBILITY SECTION ==================
// This section allows to use both "save" resp. "evas"
// as well as "load" resp. "daol". For strict CiA301
// behaviour, disable this section. There are different
// interpretations and implementations in common
// CANopen tools, so allowing both options is the safe
// choice.
		   ( (handleSDORequestIndex == 0x1010) &&
             (pData[4] == 'e') && (pData[5] == 'v') &&
             (pData[6] == 'a') && (pData[7] == 's')
           )
           ||
           ( (handleSDORequestIndex == 0x1011) &&
             (pData[4] == 'd') && (pData[5] == 'a') &&
             (pData[6] == 'o') && (pData[7] == 'l')
           )
           ||
// ===== END COMPATIBILITY SECTION ====================
           ( (handleSDORequestIndex == 0x1010) &&
             (pData[4] == 's') && (pData[5] == 'a') &&
             (pData[6] == 'v') && (pData[7] == 'e')
           )
           ||
           ( (handleSDORequestIndex == 0x1011) &&
             (pData[4] == 'l') && (pData[5] == 'o') &&
             (pData[6] == 'a') && (pData[7] == 'd')
           )
          )
        { // write of "save" to 0x1010 or "load" to 0x1011
          if (MCOSP_StoreParameters(handleSDORequestIndex,subindex))
          { // Parameters stored OK.
            // write response
            gTxSDO.BUF.BUF[0] = 0x60;
            // Needed to pass conformance test: clear unused bytes
            gTxSDO.BUF.BUF[4] = 0;
            gTxSDO.BUF.BUF[5] = 0;
            gTxSDO.BUF.BUF[6] = 0;
            gTxSDO.BUF.BUF[7] = 0;
            if (!MCOHW_PushMessage(&gTxSDO))
            {
              MCOUSER_FatalError(ERROFL_SDO);
            }
            return 1;
          }
          else
          {
            MCO_SendSDOAbort(SDO_ABORT_TRANSFER);
            return 0;
          }
        }
      }
      MCO_SendSDOAbort(SDO_ABORT_PARAMETER);
      return 0;
    }
#endif // USE_STORE_PARAMETERS

    // deal with access to process image area
    found = MCO_SearchODProcTable(handleSDORequestIndex,subindex);
    // entry found?
    if (found != 0xFFFF)
    {
      pOD = &(gODProcTable[found]);
      // read command?
      if (cmd == 0x40)
      {
        // read allowed?
        if ((pOD->len & ODRD) != 0) // Check if RD bit is set
        {
          PI_READ(PIACC_SDO,pOD->offset,&buf32,pOD->len & 0x0F);
          return MCO_ReplyWith((UNSIGNED8 *)&(buf32),(pOD->len & 0x0F));
        }
        // read not allowed
        else
        {
          MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
          return 0;
        }
      }
      // write command?
      else
      {
        // is WR bit set? - then write allowed
        if ((pOD->len & ODWR) != 0)
        {
          // for writes: Bits 2 and 3 of *pData are number of bytes without data
          len = 4 - ((*pData & 0x0C) >> 2); 
          // is length ok?
          if (len != (pOD->len & 0x0F))
          {
            MCO_SendSDOAbort(SDO_ABORT_TYPEMISMATCH);
            return 0;
          }
          // retrieve data from SDO write request and copy into process image
          PI_WRITE(PIACC_SDO,pOD->offset,&(gRxCAN.BUF.BUF[4]),len);

          // write response
          gTxSDO.BUF.BUF[0] = 0x60;
          // Needed to pass conformance test: clear unused bytes
          gTxSDO.BUF.BUF[4] = 0;
          gTxSDO.BUF.BUF[5] = 0;
          gTxSDO.BUF.BUF[6] = 0;
          gTxSDO.BUF.BUF[7] = 0;
          if (!MCOHW_PushMessage(&gTxSDO))
          {
            MCOUSER_FatalError(ERROFL_SDO);
          }
          return 1;
        }
        // write not allowed
        else
        {
          MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
          return 0;
        }
      }
    }

    // search table with constants
    found = MCO_SearchOD(handleSDORequestIndex,subindex);
    // entry found?
    if (found < 0xFFFF)
    {
      // read command?
      if (cmd == 0x40)
      {
        UNSIGNED16 i;
        UNSIGNED16 ind;
        for (i=0,ind=(found<<3); i<8; i++,ind++)
        {
          gTxSDO.BUF.BUF[i] = gSDOResponseTable[ind];
        }
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // write command
      MCO_SendSDOAbort(SDO_ABORT_READONLY);
      return 0;
    }
    if ((handleSDORequestIndex == 0x1001) && (subindex == 0x00))
    {
      // read command
      if (cmd == 0x40)
      {
        // expedited, 1 byte of data
        gTxSDO.BUF.BUF[0] = 0x4F;
        gTxSDO.BUF.BUF[4] = gMCOConfig.error_register;
        if (!MCOHW_PushMessage(&gTxSDO))
        {
          MCOUSER_FatalError(ERROFL_SDO);
        }
        return 1;
      }
      // write command
      MCO_SendSDOAbort(SDO_ABORT_READONLY);
      return 0;
    }

    // Requested OD entry not found
    if (subindex == 0)
    {
      MCO_SendSDOAbort(SDO_ABORT_NOT_EXISTS);
    }
    else
    {
      MCO_SendSDOAbort(SDO_ABORT_UNKNOWNSUB);
    }
    return 0;
  }
  // ignore abort received - all other produce an error
  if (cmd != 0x80)
  {
    MCO_SendSDOAbort(SDO_ABORT_UNKNOWN_COMMAND);
    return 0;
  }
#if USE_EXTENDED_SDO
  else
  { // Inform extended handling about the abort
    XSDO_Abort(sdoserv);
  }
#endif // USE_EXTENDED_SDO
  return 1;
}


#if NR_OF_RPDOS > 0
/**************************************************************************
DOES:    Called when going into the operational mode.
         Inits all RPDO Filters
RETURNS: nothing
**************************************************************************/
void MCO_PrepareRPDOs (
    void
  )
{
UNSIGNED8 i;

  i = 0;
  // prepare all RPDO filters for reception
  while (i < NR_OF_RPDOS)
  {
    if ((gMCOConfig.error_code & 0x80) == 0)
    { // RPDO filters not yet set
      if (!MCOHW_SetCANFilter(gRPDOConfig[i].CANID))
      {
        MCOUSER_FatalError(ERRFT_RXFLTP);
      }
    }
    i++;
  }
  gMCOConfig.error_code |= 0x80; // Signal that RPDO filters are now set
}
#endif // NR_OF_RPDOS > 0


#if NR_OF_TPDOS > 0
/**************************************************************************
DOES:    Called when going into the operational mode.
         Prepares all TPDOs for operational.
RETURNS: nothing
**************************************************************************/
void MCO_PrepareTPDOs (
    void
  )
{
UNSIGNED8 i;

  i = 0;
  // prepare all TPDOs for transmission
  while (i < NR_OF_TPDOS)
  {
    // Copy current process data
	PDO_TXCOPY(i,&(gTPDOConfig[i].CAN.BUF.BUF[0]));
#if USE_EVENT_TIME
    // Reset event timer for immediate transmission
    gTPDOConfig[i].event_timestamp = MCOHW_GetTime() - 2;
#endif
#if USE_INHIBIT_TIME
    gTPDOConfig[i].inhibit_status = 2; // Mark as ready for transmission
    // Reset inhibit timer for immediate transmission
    gTPDOConfig[i].inhibit_timestamp = MCOHW_GetTime() - 2;
#endif
#if USE_SYNC
    gTPDOConfig[i].SYNCcnt = 1;
#endif
    i++;
  }
  // ensure that MCO_ProcessStack starts with TPDO1
  gTPDONr = NR_OF_TPDOS;
}


#if USE_INHIBIT_TIME
/**************************************************************************
DOES:    Called by application when a TPDO should be transmitted.
         Can be called after a write to the process image to avoid lengthy
         auto-detection of a COS (Change Of State)
RETURNS: nothing
**************************************************************************/
void MCO_TriggerTPDO (
  UNSIGNED16 TPDONr  // TPDO number to transmit, as gTPDOConfig[] index
  )
{
	  int lp = 0;
	  while (gTPDOConfig[lp].PDONr != TPDONr)
	  {
	    lp++;
	    if (lp >= NR_OF_TPDOS)
	    	return;
	  }

      gTPDOConfig[lp].Trigger = 1;
}
#endif


/**************************************************************************
DOES:    Called when a TPDO needs to be transmitted
RETURNS: nothing
**************************************************************************/
void MCO_TransmitPDO (
  UNSIGNED16 PDONr  // TPDO number to transmit, as gTPDOConfig[] index
  )
{

#if USE_INHIBIT_TIME
  // new inhibit timer started
  gTPDOConfig[PDONr].inhibit_status = 1;
  gTPDOConfig[PDONr].inhibit_timestamp = MCOHW_GetTime() + gTPDOConfig[PDONr].inhibit_time;
#endif
#if USE_EVENT_TIME
  gTPDOConfig[PDONr].event_timestamp = MCOHW_GetTime() + gTPDOConfig[PDONr].event_time; 
#endif
#if USE_SYNC
  gTPDOConfig[PDONr].SYNCcnt = gTPDOConfig[PDONr].TType;
#endif
  if (!MCOHW_PushMessage(&gTPDOConfig[PDONr].CAN))
  {
    MCOUSER_FatalError(ERROFL_PDO);
  }
}
#endif // NR_OF_TPDOS > 0


/**************************************************************************
DOES:    Handles the NMT Master Message, switching NMT Slave state
RETURNS: nothing
**************************************************************************/
void MCO_HandleNMTRequest (
  UNSIGNED8 NMTReq
  )
{
  switch (NMTReq)
  {
    // start node
    case NMTMSG_OP:
      if (MY_NMT_STATE != NMTSTATE_OP)
      { // only if not already operational
        // set new state
        MY_NMT_STATE = NMTSTATE_OP;
#if USE_LEDS
        gMCOConfig.LEDRun = LED_ON;
#endif
#if NR_OF_TPDOS > 0          
        MCO_PrepareTPDOs();
#endif
#if NR_OF_RPDOS > 0          
        MCO_PrepareRPDOs();
#endif
#if USECB_NMTCHANGE
        // Call back to user / application
        MCOUSER_NMTChange(MY_NMT_STATE);
#endif
      }
      break;

    // stop node
    case NMTMSG_STOP:
      // set new state
      MY_NMT_STATE = NMTSTATE_STOP;
#if USE_LEDS
      gMCOConfig.LEDRun = LED_FLASH1;
#endif
#if USECB_NMTCHANGE
      // Call back to user / application
      MCOUSER_NMTChange(MY_NMT_STATE);
#endif
      break;

    // enter pre-operational
    case NMTMSG_PREOP:
      // set new state
      MY_NMT_STATE = NMTSTATE_PREOP;
#if USE_LEDS
      gMCOConfig.LEDRun = LED_BLINK;
#endif
      // Call back to user / application
#if USECB_NMTCHANGE
      // Call back to user / application
      MCOUSER_NMTChange(MY_NMT_STATE);
#endif
      break;
   
    // application reset
    case NMTMSG_RESETAPP:
      MCOUSER_ResetApplication();
      break;

    // node reset communication
    case NMTMSG_RESETCOM:
      MCOUSER_ResetCommunication(1);
      break;

    // unknown command
    default:
#if USE_EMCY
      // Only supported by MicroCANopen Plus
      // send EMCY clear message  
      if (!MCOP_PushEMCY(0x8200,0,0,0,0,0))
      {
        MCOUSER_FatalError(ERROFL_EMCY);
      }
#endif // USE_EMCY
      break;
  }
}


#if NR_OF_RPDOS > 0
/**************************************************************************
DOES:    Handles Receive PDOs
RETURNS: 0, if RPDO not processed
         1, if RPDO processed
**************************************************************************/
UNSIGNED8 MCO_HandleRPDO (
  CAN_MSG MEM_FAR *pRPDO
  )
{
UNSIGNED8 i; // loop variable

  if (MY_NMT_STATE != NMTSTATE_OP)
  { // node is not operational or PDO is disabled
    return 0;
  }

  i = 0;
  // loop through RPDOs
  if(MCOUSER_ReceiveRaw(pRPDO)){
	  return 1;
  }
  while (i < NR_OF_RPDOS)
  {
    // is this one of our RPDOs is not disabled
    if (pRPDO->ID != NMT_MASTER_ID && (pRPDO->ID == gRPDOConfig[i].CANID) &&
        ((gRPDOConfig[i].CANID & 0x8000) == 0)
       )
    {
#if USE_EMCY
// Only supported with MicroCANopen Plus
      if (gRxCAN.LEN != gRPDOConfig[i].len)
      { // Length of CAN message does not match PDO len
        gMCOConfig.error_register |= 1; // set generic error bit
        // send EMCY message PDO not processed
        if (!MCOP_PushEMCY(0x8210,0,0,0,0,0))
        {
          MCOUSER_FatalError(ERROFL_EMCY);
        }
        return 0;
      }
#endif // USE_EMCY
      if (gRPDOConfig[i].TType >= 254)
      { // This PDO is not synced
        // copy data from RPDO to process image
        PDO_RXCOPY(i,&(pRPDO->BUF.BUF[0]));
#if USECB_RPDORECEIVE
        MCOUSER_RPDOReceived(gRPDOConfig[i].PDONr,gRPDOConfig[i].offset,gRPDOConfig[i].len);
#endif // USECB_RPDORECEIVE
      }
#if USE_SYNC
      else if (gRPDOConfig[i].TType <= 240)
      { // This PDO is synced
        // copy data from CAN message to RPDO buffer
        memcpy(&(gRPDOConfig[i].BUF[0]),(const void *)&(pRPDO->BUF.BUF[0]),gRPDOConfig[i].len);
      }
#endif // USE_SYNC
      // exit the loop
      return 1;
    }  
    i++;
  } // for all RPDOs
  return 0; // not a RPDO for us
}
#endif // NR_OF_RPDOS > 0


#if NR_OF_TPDOS > 0
/**************************************************************************
DOES:    Handles Transmit PDOs, checks only one TPDO with each call
RETURNS: 0, if no TPDO was sent
         1, if TPDO was sent
**************************************************************************/
UNSIGNED8 MCO_HandleTPDO (
  UNSIGNED16 TPDONr // Number of TPDO to check, as gTPDOConfig[] array index
  )
{
  // is the TPDO 'TPDONr' in use?
  if ((gTPDOConfig[TPDONr].CAN.ID != 0) && 
      ((gTPDOConfig[TPDONr].CAN.ID & 0x8000) == 0)
#if USE_SYNC
      && (gTPDOConfig[TPDONr].TType >= 254) // Not a synced PDO
#endif
     )
  {

    // Send by using the MCO_TriggerTPDO(...)
    if (gTPDOConfig[TPDONr].Trigger)
    {
      gTPDOConfig[TPDONr].Trigger = 0;
#if USECB_TPDORDY
      MCOUSER_TPDOReady(TPDONr+1,0);
#endif
      // get data from process image and transmit
      PDO_TXCOPY(TPDONr,&(gTPDOConfig[TPDONr].CAN.BUF.BUF[0]));
      MCO_TransmitPDO(TPDONr);
      return 1;
    }

#if USE_EVENT_TIME
    // does TPDO use event timer and event timer is expired? if so we need to transmit now
    if ((gTPDOConfig[TPDONr].event_time != 0) && 
        (MCOHW_IsTimeExpired(gTPDOConfig[TPDONr].event_timestamp)) )
    {
#if USECB_TPDORDY
      MCOUSER_TPDOReady(TPDONr+1,0);
#endif
      // get data from process image and transmit
      PDO_TXCOPY(TPDONr,&(gTPDOConfig[TPDONr].CAN.BUF.BUF[0]));
      MCO_TransmitPDO(TPDONr);
      return 1;
    }
#endif // USE_EVENT_TIME
#if USE_INHIBIT_TIME
    // does the TPDO use an inhibit time? - COS transmission
    if (gTPDOConfig[TPDONr].inhibit_time != 0)
    {
      // is the inihibit timer currently running?
      if (gTPDOConfig[TPDONr].inhibit_status > 0)
      {
        // has the inhibit time expired?
        if (MCOHW_IsTimeExpired(gTPDOConfig[TPDONr].inhibit_timestamp))
        {
          // is there a new transmit message already waiting?
          if (gTPDOConfig[TPDONr].inhibit_status == 2)
          { 
#if USECB_TPDORDY
            MCOUSER_TPDOReady(TPDONr+1,3);
#endif
            // transmit now
            MCO_TransmitPDO(TPDONr);
            return 1;
          }
          // no new message waiting, but timer expired
          else 
          {
            gTPDOConfig[TPDONr].inhibit_status = 0;
          }
        }
      }
      // is inhibit status 0 or 1?
      if (gTPDOConfig[TPDONr].inhibit_status < 2)
      {
        // has application data changed?
        if (PDO_TXCOMP(TPDONr,&(gTPDOConfig[TPDONr].CAN.BUF.BUF[0])) != 0)
        {
          // Copy application data
          PDO_TXCOPY(TPDONr,&(gTPDOConfig[TPDONr].CAN.BUF.BUF[0]));
          // has inhibit time expired?
          if (gTPDOConfig[TPDONr].inhibit_status == 0)
          {
#if USECB_TPDORDY
            MCOUSER_TPDOReady(TPDONr+1,3);
#endif
            // transmit now
            MCO_TransmitPDO(TPDONr);
            return 1;
          }
          // inhibit status is 1
          else
          {
            // wait for inhibit time to expire 
            gTPDOConfig[TPDONr].inhibit_status = 2;
          }
        }
      }
    } // Inhibit Time != 0
#endif // USE_INHIBIT_TIME
  } // PDO active (CAN_ID != 0)  
  return 0;
}
#endif // NR_OF_TPDOS > 0


/**************************************************************************
PUBLIC FUNCTIONS
***************************************************************************/ 

/**************************************************************************
DOES:    Initializes the MicroCANopen stack
         It must be called from within MCOUSER_ResetApplication
RETURNS: TRUE, if init OK, else FALSE (also when unconfigured and in LSS)
**************************************************************************/
UNSIGNED8 MCO_Init (
  UNSIGNED16 Baudrate,  // CAN baudrate in kbit (1000,800,500,250,125,50,25 or 10)
  UNSIGNED8 Node_ID,    // CANopen node ID (1-126)
  UNSIGNED16 Heartbeat  // Heartbeat time in ms (0 for none)
  )
{
  UNSIGNED8 i;

  if (Baudrate == 0)
  {
    Baudrate = 125; // default baud rate
  }

  // Init the global variables
  // MY_NODE_ID is gMCOConfig.Node_ID
  if (Node_ID != 0)
  {
    MY_NODE_ID = Node_ID;
  }
  // else use previously assigned ID (for example LSS)
  gMCOConfig.error_code = 0;
  gMCOConfig.Baudrate = Baudrate;
  gMCOConfig.heartbeat_time = Heartbeat;
  gMCOConfig.error_register = 0;
#if USE_LEDS
  // Initialize LED blink control 200ms timer
  gMCOConfig.LED_timestamp = MCOHW_GetTime() + 200;
  gMCOConfig.LEDtoggle = 0;
  gMCOConfig.LEDcntR = 0;
  gMCOConfig.LEDcntE = 0;
  gMCOConfig.LEDRun = LED_OFF;
  gMCOConfig.LEDErr = LED_ON;
#endif // USE_LEDS
#if USE_SYNC
  gMCOConfig.SYNCid = 0x80;
#endif // USE_SYNC

  // init the CAN interface
  if (!MCOHW_Init(Baudrate))
  {
    MCOUSER_FatalError(ERRFT_INIT);
  }

#if USE_SLEEP
  // Wakeup message to everyone
  MCO_TransmitWakeup();
#endif

#if USECB_LSS_MASTER
  if (!MCOHW_SetCANFilter(0x7E4))
  {
	    MCOUSER_FatalError(ERRFT_RXFLTN);
  }
#endif

#if USE_LSS_SLAVE
  // Set receive filter for LSS master message 
  if (!MCOHW_SetCANFilter(LSS_MASTER_ID))
  {
    MCOUSER_FatalError(ERRFT_RXFLTN);
  }
  if (MY_NODE_ID == 0)
  {
    return FALSE; // stay in LSS mode
  }
#endif

  // continue with variable initialization
  gMCOConfig.heartbeat_msg.ID = 0x700+MY_NODE_ID;
  gMCOConfig.heartbeat_msg.LEN = 1;
  // current NMT state of this node = bootup
  MY_NMT_STATE = 0;

  // Init SDO Response/Abort message
  gTxSDO.ID = 0x0580+MY_NODE_ID;
  gTxSDO.LEN = 8;

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
  // init heartbeat consumption
  for (i = 0; i < NR_OF_HB_CONSUMER; i++)
  {
    gHBCons[i].status = 0; // disable consumption
  }
#endif // (NR_OF_HB_CONSUMER > 0)
#endif // MONITOR_ALL_NODES
   
#if NR_OF_TPDOS > 0
  i = 0;
  // init TPDOs
  while (i < NR_OF_TPDOS)
  {
    gTPDOConfig[i].CAN.ID = 0;
    gTPDOConfig[i].PDONr = 0;
    i++;
  }
#endif

#if NR_OF_RPDOS > 0
  i = 0;
  // init RPDOs
  while (i < NR_OF_RPDOS)
  {
    gRPDOConfig[i].CANID = 0;
    gRPDOConfig[i].PDONr = 0;
    i++;
  }
#endif

#if USE_TIME_OF_DAY_RX
  // filter for CAN ID 0x100
  if (!MCOHW_SetCANFilter(0x100))
  {
    MCOUSER_FatalError(ERRFT_RXFLTN);
  }
#endif

  // filter for nmt master message
  if (!MCOHW_SetCANFilter(0))
  {
    MCOUSER_FatalError(ERRFT_RXFLTN);
  }
#if USE_SYNC
  // for SYNC message
  if (!MCOHW_SetCANFilter(gMCOConfig.SYNCid))
  {
    MCOUSER_FatalError(ERRFT_RXFLTN);
  }
#endif

#if USE_EMCY
#if ERROR_FIELD_SIZE > 0
  MCOP_ErrField_Flush();
#endif
#endif

#if USE_SDOMESH
  // for receiving meshed SDO requests
  for (i=1;i<=16;i++)
  { // for all 16 node IDs
    if (i != MY_NODE_ID)
	{ // only proceed if node id is not own node id
      if (!MCOHW_SetCANFilter(CAN_ID_SDOREQUEST(i, MY_NODE_ID)))
      {
        MCOUSER_FatalError(ERRFT_RXFLTS);
	  }
    }
  }
#else // USE_SDOMESH
#if USE_CiA447
  // for receiving CiA447 SDO requests
  for (i=1;i<=16;i++)
  { // for all 16 node IDs
    if (i != MY_NODE_ID)
	{ // only proceed if node id is not own node id
      if (!MCOHW_SetCANFilter(CAN_ID_SDOREQUEST(i, MY_NODE_ID)))
      {
        MCOUSER_FatalError(ERRFT_RXFLTS);
	  }
    }
  }
#endif // USE_CiA447
  // for standard CANopen SDO requests
  if (!MCOHW_SetCANFilter(0x600+MY_NODE_ID))
  {
    MCOUSER_FatalError(ERRFT_RXFLTS);
  }
#endif // USE_SDOMESH

#if USE_NODE_GUARDING
  // for Node Guarding requests
  if (!MCOHW_SetCANFilter(0x8700+MY_NODE_ID))
  {
    MCOUSER_FatalError(ERRFT_RXFLTN);
  }
  gMCOConfig.NGtoggle = 0;
#endif

#if USE_SLEEP
  // for request sleep
  if (!MCOHW_SetCANFilter(0x682))
  {
    MCOUSER_FatalError(ERRFT_RXFLTS);
  }
  // for wake up
  if (!MCOHW_SetCANFilter(0x680))
  {
    MCOUSER_FatalError(ERRFT_RXFLTS);
  }
#endif

  // signal to MCO_ProcessStack: we just initialized
  gTPDONr = 0xFFFF;

  return TRUE;
}  


#if NR_OF_RPDOS > 0
/**************************************************************************
DOES:    This function initializes a receive PDO. Once initialized, the 
         MicroCANopen stack automatically updates the data at offset.
NOTE:    For data consistency, the application should not read the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_InitRPDO (
  UNSIGNED16 PDO_NR,      // RPDO number (1-512)
  UNSIGNED32 CAN_ID,      // CAN identifier to be used (set to 0 to use default)
  UNSIGNED8 len,          // Number of data bytes in RPDO
  UNSIGNED16 offset       // Offset to data location in process image
  )
{
UNSIGNED16 lp;

#if CHECK_PARAMETERS
  // check PDO range and check node id range 1 - 127
  // 29-bit COB-IDs are not supported
  if (((PDO_NR < 1)     || (PDO_NR > 512))     || 
      ((MY_NODE_ID < 1) || (MY_NODE_ID > 127)) ||
      (CAN_ID & 0x20000000UL) )
  {
    MCOUSER_FatalError(ERRFT_IPP);
  }
  // is size of process image exceeded?
  if (offset >= PROCIMG_SIZE)   
  { 
    MCOUSER_FatalError(ERRFT_PIR);
  }
#endif

  // find free data record in array of PDO configuration
  {
	  int found = 0;

	  for(lp = 0; lp < NR_OF_RPDOS; lp++){
		  if(gRPDOConfig[lp].PDONr == PDO_NR){
			  found = 1;
			  break;
		  }
	  }

	  if(!found){
		  lp = 0;
		  while (gRPDOConfig[lp].PDONr != 0)
		  {
		    lp++;
		    if (lp >= NR_OF_RPDOS)
		    { // all data records used!
		      MCOUSER_FatalError(ERRFT_RPDOR);
		    }
		  }
	  }
  }
  // if we reach here, lp points to free record
  gRPDOConfig[lp].PDONr = PDO_NR;
  // set PDO_NR to offset as working base
  PDO_NR = lp;
  // initialize PDO
  gRPDOConfig[PDO_NR].len = len;
  gRPDOConfig[PDO_NR].offset = offset;
  if ((CAN_ID & 0x7FFF) == 0)
  {
    gRPDOConfig[PDO_NR].CANID = 0x200 + (0x100 * ((UNSIGNED16)(PDO_NR))) + MY_NODE_ID;
  }
  else
  {
    gRPDOConfig[PDO_NR].CANID = (UNSIGNED16) CAN_ID;
  }

  if (CAN_ID & 0x80000000UL)
  { // PDO is disabled
    gRPDOConfig[PDO_NR].CANID |= 0x8000;
  }

  gRPDOConfig[PDO_NR].TType = 255;
  gMCOConfig.error_code &= 0x7F; // Signal that RPDO filter are not yet set

}

/**************************************************************************
DOES:    This function initializes a receive PDO. Once initialized, the
         MicroCANopen stack automatically updates the data at offset.
NOTE:    For data consistency, the application should not read the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_ResetRPDO (
  UNSIGNED16 PDO_NR       // RPDO number (1-512)
  )
{

#if CHECK_PARAMETERS
  // check PDO range and check node id range 1 - 127
  // 29-bit COB-IDs are not supported
  if (((PDO_NR < 1)     || (PDO_NR > 512))     ||
      ((MY_NODE_ID < 1) || (MY_NODE_ID > 127)))
  {
    MCOUSER_FatalError(ERRFT_IPP);
  }
#endif

  // find data record in array of PDO configuration
  int n;
  for(n = 0; n < NR_OF_RPDOS; n++){
  	  if(gRPDOConfig[n].PDONr == PDO_NR){
  		  MCOHW_ClearCANFilter(gRPDOConfig[n].CANID);
  		  gRPDOConfig[n].PDONr = 0;
  		  gRPDOConfig[n].CANID = 0;
  	  }
  }
}
#endif // NR_OF_RPDOS > 0


#if NR_OF_TPDOS > 0
/**************************************************************************
DOES:    This function initializes a transmit PDO. Once initialized, the 
         MicroCANopen stack automatically handles transmitting the PDO.
         The application can directly change the data at any time.
NOTE:    For data consistency, the application should not write to the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_InitTPDO
  (
  UNSIGNED16 PDO_NR,       // TPDO number (1-512)
  UNSIGNED32 CAN_ID,       // CAN identifier to be used (set to 0 to use default)
  UNSIGNED16 event_time,   // Transmitted every event_tim ms 
  UNSIGNED16 inhibit_time, // Inhibit time in ms for change-of-state transmit
                           // (set to 0 if ONLY event_tim should be used)
  UNSIGNED8 len,           // Number of data bytes in TPDO
  UNSIGNED16 offset        // Offset to data location in process image
  )
{
UNSIGNED16 lp;

#if CHECK_PARAMETERS
  // check PDO range, node id, len range 1 - 8 and event time or inhibit time set
  // 29-bit COB-IDs are not supported
  if (((PDO_NR < 1)     || (PDO_NR > 512))     ||
      ((MY_NODE_ID < 1) || (MY_NODE_ID > 127)) ||
      ((len < 1)        || (len > 8)           ||
      (CAN_ID & 0x20000000UL) )
     )
  {
    MCOUSER_FatalError(ERRFT_IPP);
  }
  // is size of process image exceeded?
  if (offset >= PROCIMG_SIZE)   
  { 
    MCOUSER_FatalError(ERRFT_PIR);
  }
#endif

  // find free data record in array of PDO configuration
  {
	  int found = 0;

	  for(lp = 0; lp < NR_OF_TPDOS; lp++){
		  if(gTPDOConfig[lp].PDONr == PDO_NR){
			  found = 1;
			  break;
		  }
	  }

	  if(!found){
		  lp = 0;
		  while (gTPDOConfig[lp].PDONr != 0)
		  {
		    lp++;
		    if (lp >= NR_OF_TPDOS)
		    { // all data records used!
		      MCOUSER_FatalError(ERRFT_TPDOR);
		    }
		  }
	  }
  }
  // if we reach here, lp points to free record
  gTPDOConfig[lp].PDONr = PDO_NR;
  // set PDO_NR to offset as working base
  PDO_NR = lp;

  // initialize PDO
  //if ((event_time == 0) && (inhibit_time == 0))
  //{ // In MicroCANopen this would disable the TPDO but it is useful to be able to use MCO_TriggerTPDO(...) to send PDOs
  //  inhibit_time = 1; // enable TPDO by selecting a minimal inhibit time
  //}

  if ((CAN_ID & 0x7FFF) == 0)
  {
    gTPDOConfig[PDO_NR].CAN.ID = 0x180 + (0x100 * ((UNSIGNED16)(PDO_NR))) + MY_NODE_ID;
  }
  else
  {
    gTPDOConfig[PDO_NR].CAN.ID = (UNSIGNED16) CAN_ID;
  }

  if (CAN_ID & 0x80000000UL)
  { // PDO is disabled
    gTPDOConfig[PDO_NR].CAN.ID |= 0x8000;
  }

  gTPDOConfig[PDO_NR].CAN.LEN = len;
  gTPDOConfig[PDO_NR].offset = offset;
#if USE_EVENT_TIME
  gTPDOConfig[PDO_NR].event_time = event_time;
#endif
#if USE_INHIBIT_TIME
  gTPDOConfig[PDO_NR].inhibit_time = inhibit_time;
#endif
  gTPDOConfig[PDO_NR].TType = -1;
}

/**************************************************************************
DOES:    This function initializes a transmit PDO. Once initialized, the
         MicroCANopen stack automatically handles transmitting the PDO.
         The application can directly change the data at any time.
NOTE:    For data consistency, the application should not write to the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_ResetTPDO
  (
  UNSIGNED16 PDO_NR        // TPDO number (1-512)
  )
{

#if CHECK_PARAMETERS
  // check PDO range, node id, len range 1 - 8 and event time or inhibit time set
  // 29-bit COB-IDs are not supported
  if (((PDO_NR < 1)     || (PDO_NR > 512))     ||
      ((MY_NODE_ID < 1) || (MY_NODE_ID > 127)))
  {
    MCOUSER_FatalError(ERRFT_IPP);
  }
#endif

  // find data record in array of PDO configuration
  int n;
  for(n = 0; n < NR_OF_TPDOS; n++){
	  if(gTPDOConfig[n].PDONr == PDO_NR){
		  gTPDOConfig[n].PDONr = 0;
		  gTPDOConfig[n].CAN.ID = 0;
	  }
  }
}
#endif // NR_OF_TPDOS > 0


/**************************************************************************
DOES:    This function processes the next CAN message from the CAN receive
         queue. When using an RTOS, this can be turned into a task
         triggered by a CAN receive event.
RETURNS: 0 if no message was processed, 
         1 if a CAN message received was processed
**************************************************************************/
UNSIGNED8 MCO_ProcessStackRx (
  void
  )
{
  // work on next incoming messages
  // if message received
  if (MCOHW_PullMessage(&gRxCAN))
  {

#if USE_TIME_OF_DAY_RX
    if ((gRxCAN.ID == 0x100) &&
	    ((MY_NMT_STATE == NMTSTATE_PREOP) || (MY_NMT_STATE == NMTSTATE_OP))
	   )
    { // time stamp received, needs to be processed
	  MCOSUSER_TimeOfDay(*(&gRxCAN.BUF.BUF32[0]),*((UNSIGNED16 *)&gRxCAN.BUF.BUF32[1]));
      return 1;
    }
#endif

#if USE_LSS_SLAVE || USECB_LSS_MASTER
    if ((MY_NMT_STATE == NMTSTATE_LSS) || (MY_NMT_STATE == NMTSTATE_STOP))
    { // in LSS slave mode only process this message, ignore the rest
      if (gRxCAN.ID == LSS_MASTER_ID)
      {
        LSS_HandleMsg(gRxCAN.LEN,&(gRxCAN.BUF.BUF[0]));
      }
#if USECB_LSS_MASTER
      else if (gRxCAN.ID == LSS_SLAVE_ID)
      {
        MCOUSER_LSSMasterReceived(gRxCAN.LEN,&(gRxCAN.BUF.BUF[0]));
      }
#endif
   	  return 1;
    }
#endif

    if (MY_NMT_STATE == NMTSTATE_AUTO_ID)
    {
        if(gRxCAN.ID >= 0x581 && gRxCAN.ID <= 0x67F)
        {
            MCOUSER_SDOResponse(gRxCAN.ID, gRxCAN.LEN,&(gRxCAN.BUF.BUF[0]));
            return 1;
        }
        return 0;
    }

    // is it an NMT master message?
    if (gRxCAN.ID == NMT_MASTER_ID)
    {
      // nmt message is for this node or all nodes
      if ((gRxCAN.BUF.BUF[1] == MY_NODE_ID) || (gRxCAN.BUF.BUF[1] == 0))
      {
        MCO_HandleNMTRequest(gRxCAN.BUF.BUF[0]);
        return 1;
      } // NMT message addressed to this node
    } // NMT master message received
    
#if USE_SYNC
    // Handle SYNC
    if (gRxCAN.ID == gMCOConfig.SYNCid)
    { // SYNC received
      if (MCOP_HandleSYNC() != 0)
      { // SYNC PDOs processed
        return 1;
      }
    }
#endif // USE_SYNC

#if NR_OF_RPDOS > 0
    // Handle RPDO
    if (MCO_HandleRPDO(&gRxCAN) == 1)
    { // RPDO processed
      return 1;
    }
#endif // NR_OF_RPDOS > 0

    // SDO Request, handle if node is not stopped...
    if (MY_NMT_STATE != NMTSTATE_STOP)
    {
      // Is this one of the SDO messages for us
      if (IS_CAN_ID_SDOREQUEST(gRxCAN.ID))
      {
	    // set response ID
        gTxSDO.ID = CAN_ID_SDORESPONSE_FROM_RXID(gRxCAN.ID);
        // handle SDO request - return value not used in this version
        MCO_HandleSDORequest(&gRxCAN.BUF.BUF[0]);
        return 1;
      }
#if USECB_SDOResponse
      if(gRxCAN.ID >= 0x581 && gRxCAN.ID <= 0x5FF)
      {
          MCOUSER_SDOResponse(gRxCAN.ID, gRxCAN.LEN,&(gRxCAN.BUF.BUF[0]));
      }
#endif
#if USECB_Emergency
      if(gRxCAN.ID >= 0x81 && gRxCAN.ID <= 0xFF)
      {
    	  MCOUSER_EmergencyReceived(gRxCAN.ID, gRxCAN.LEN,&(gRxCAN.BUF.BUF[0]));
      }
    }
#endif

#if USE_NODE_GUARDING
    if (MCOP_HandleGuarding(gRxCAN.ID) == 1)
    {
      return 1;
    }
#endif

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
    // Check if message received was a Heartbeat monitored
    if (MCOP_ConsumeHB(&gRxCAN))
    {
      return 1;
    }
#endif// (NR_OF_HB_CONSUMER > 0)
#endif // MONITOR_ALL_NODES

#if USE_SLEEP
    if (gRxCAN.ID == 0x682)
    { // sleep requst
      if (gRxCAN.BUF.BUF[0] == 1)
      { // query sleep objection
        MCOUSER_Sleep(1);  
      }
      else if (gRxCAN.BUF.BUF[0] == 2)
      { // set sleep mode
        MCOUSER_Sleep(2);  
      }
      return 1;
    } 
    else if (gRxCAN.ID == 0x680)
    { // wakeup request
      MCOUSER_Sleep(0);  
      return 1;
    }
#endif // USE SLEEP

  } // Message received
  
  return 0; // no message in receive queue
}


/**************************************************************************
DOES:    This function executes all sub functions required to keep the 
         CANopen stack operating. It should be called frequently. When used
         in an RTOS it should be called repeatedly every RTOS time tick
         until it returns zero.
RETURNS: 0 if there was nothing to process 
         1 if functions were
**************************************************************************/
int CAN_Autostart = 0;
UNSIGNED8 MCO_ProcessStackTick (
  void
  )
{
  // check if this is right after boot-up
  // was set by MCO_Init
  if (gTPDONr >= 0xFFFE)
  { // first call or wait for bootup to be transmited
	if(MY_NMT_STATE == NMTSTATE_AUTO_ID)
	  return 0;
    if (gTPDONr > 0xFFFE)
    {
      // init heartbeat time
      gMCOConfig.heartbeat_timestamp = MCOHW_GetTime() + gMCOConfig.heartbeat_time;
      // send boot-up message
      if (!MCOHW_PushMessage(&gMCOConfig.heartbeat_msg))
      {
        MCOUSER_FatalError(ERROFL_HBT);
      }
      gTPDONr--;
#if USECB_NMTCHANGE
      MCOUSER_NMTChange(NMTSTATE_BOOT);
#endif
    }
    else
    { // Now wait for bootup to go out
        if ((MCOHW_GetStatus() & HW_TXBSY) == 0)
        { // Bootup went out, now continue
#if USE_EMCY
        	// Only supported by MicroCANopen Plus
            // Init emergency inhibit time
            mEF.emcy_inhibit = EMCY_INHIBIT_TIME;
            // set time stamp to expired
            mEF.emcy_timestamp = MCOHW_GetTime() - 1;
            // send EMCY clear message
            if (!MCOP_PushEMCY(0,0,0,0,0,0))
            {
              MCOUSER_FatalError(ERROFL_EMCY);
            }
            if (mEF.emcy_msg.ID != 0)
            { // An emergency is due for transmission
              MCOHW_PushMessage(&(mEF.emcy_msg));
              mEF.emcy_msg.ID = 0; // Mark as transmitted
              // now reset the inhibit time
              mEF.emcy_timestamp = MCOHW_GetTime() + mEF.emcy_inhibit;
            }
#endif // USE_EMCY

        	if(CAN_Autostart){//#if AUTOSTART
                // going into operational state
                MY_NMT_STATE = NMTSTATE_OP;
        #if USE_LEDS
                gMCOConfig.LEDRun = LED_ON;
                gMCOConfig.LEDErr = LED_OFF;
        #endif
        #if NR_OF_TPDOS > 0
                MCO_PrepareTPDOs();
        #endif
        #if NR_OF_RPDOS > 0
                MCO_PrepareRPDOs();
        #endif
        #if USECB_NMTCHANGE
                MCOUSER_NMTChange(NMTSTATE_OP);
        #endif
            }else{//#else // AUTOSTART
                // going into pre-operational state
                MY_NMT_STATE = NMTSTATE_PREOP;
        #if USE_LEDS
                gMCOConfig.LEDRun = LED_BLINK;
                gMCOConfig.LEDErr = LED_OFF;
        #endif
        #if USECB_NMTCHANGE
                MCOUSER_NMTChange(NMTSTATE_PREOP);
        #endif
            }//#endif// AUTOSTART

        // return TPDONr value to default
        gTPDONr = NR_OF_TPDOS;
        }
      }
    return 1;
  }


#if USE_LSS_SLAVE
  if (LSS_DoLSS() || MY_NMT_STATE == NMTSTATE_AUTO_ID) // work on LSS
  {
    return 1; // Do nothing else when in LSS mode
  }
#endif

#if USE_EMCY
  // Check if an EMCY waits for transmission
  if (MCOHW_IsTimeExpired(mEF.emcy_timestamp))
  { // Timer overrun protection, ensure that it remains expired
    mEF.emcy_timestamp = MCOHW_GetTime() - 1;
    if (mEF.emcy_msg.ID != 0)
    { // An emergency is due for transmission
      MCOHW_PushMessage(&(mEF.emcy_msg));
      mEF.emcy_msg.ID = 0; // Mark as transmitted
      // now reset the inhibit time
      mEF.emcy_timestamp = MCOHW_GetTime() + mEF.emcy_inhibit;
      return 1;
    }
  }
#endif

#if NR_OF_TPDOS > 0
  // is the node operational?
  if (MY_NMT_STATE == NMTSTATE_OP)
  {
    // check next TPDO for transmission
    gTPDONr++;
    if (gTPDONr >= NR_OF_TPDOS)
    {
      gTPDONr = 0;
    }
    if (MCO_HandleTPDO(gTPDONr) == 1)
    { // TPDO was generated
      return 1;
    }
  } // if node is operational
#endif // NR_OF_TPDOS > 0
  
#if USE_BLOCKED_SDO
  // Check if we are in the middle of a block read transfer
  if (XSDO_BlkRdProgress())
  { // yes, message generated
    return 1;
  }
#endif //USE_BLOCKED_SDO

  // do we produce a heartbeat?
  if (gMCOConfig.heartbeat_time != 0)
  {
    // has heartbeat time passed?
    if ((MCOHW_IsTimeExpired(gMCOConfig.heartbeat_timestamp)) & (MY_NMT_STATE != NMTSTATE_BOOT))
    {
      // transmit heartbeat message
      if (!MCOHW_PushMessage(&gMCOConfig.heartbeat_msg))
      {
        MCOUSER_FatalError(ERROFL_HBT);
      }
      // get new heartbeat time for next transmission
      gMCOConfig.heartbeat_timestamp = MCOHW_GetTime() + gMCOConfig.heartbeat_time;
      return 1;
    }
  }

#if USE_LEDS
  if (MCOHW_IsTimeExpired(gMCOConfig.LED_timestamp)) 
  { // LED time did expire
    gMCOConfig.LED_timestamp = MCOHW_GetTime() + 200; 
    MCO_SwitchLEDs();
  }
#endif // USE_LEDS

  return 0;
}


/**************************************************************************
DOES:    This function implements the main MicroCANopen protocol stack. 
         It must be called frequently to ensure proper operation of the
         communication stack. 
         When using an RTOS this function should not be called, instead
         MCO_ProcessStackRx() and MCO_ProcessStackTick() should be used.
         Typically it is called from the while(1) loop in main.
RETURNS: 0 if nothing was done, 1 if a CAN message was sent or received
**************************************************************************/
UNSIGNED8 MCO_ProcessStack (
  void
  )
{
  if(MCO_ProcessStackRx()){
	  MCO_ProcessStackRx();
  }

  if(MCO_ProcessStackTick()){
	  MCO_ProcessStackTick();
  }

  return 0;
}

/*----------------------- END OF FILE ----------------------------------*/
