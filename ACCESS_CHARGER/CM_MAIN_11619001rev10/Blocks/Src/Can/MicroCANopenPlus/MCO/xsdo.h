/**************************************************************************
MODULE:    XSDO
CONTAINS:  MicroCANopen Plus, Extended SDO implementation
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS IS THE COMMERCIAL PLUS VERSION OF MICROCANOPEN
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2009-09-24 21:22:11 -0300 (Thu, 24 Sep 2009) $
           $LastChangedRevision: 1447 $
***************************************************************************/ 

#include "mco.h"

#if USE_EXTENDED_SDO

/**************************************************************************
PUBLIC FUNCTIONS
***************************************************************************/
void XSDO_Abort (
  UNSIGNED8 SDOServer // Number of SDO Server (1 to NR_OF_SDOSERVER)
  );


/**************************************************************************
DOES:    Process SDO Segmented Requests to generic OD entries
RETURNS: 0x00 Nothing was done
         0x01 OK, handled, response generated
         0x02 Abort, SDO Abort was generated
**************************************************************************/
UNSIGNED8 XSDO_HandleExtended (
  UNSIGNED8 *pReqBUF, // Pointer to 8 data bytes with SDO data from request
  CAN_MSG *pResCAN, // Pointer to SDO response
  UNSIGNED8 SDOServer // Number of SDO Server (1 to NR_OF_SDOSERVER)
  );


/**************************************************************************
DOES:    Called from ProcessStackTick
         Checks if we are in middle of Block Read transfer
RETURNS: FALSE, nothing done
         TRUE, transfer in progress, message generated
**************************************************************************/
UNSIGNED8 XSDO_BlkRdProgress (
  void
  );


#endif // USE_EXTENDED_SDO
/*----------------------- END OF FILE ----------------------------------*/
