/**************************************************************************
MODULE:    MCOP
CONTAINS:  MicroCANopen Plus implementation
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   THIS IS THE COMMERCIAL PLUS VERSION OF MICROCANOPEN
           ONLY USERS WHO PURCHASED A LICENSE MAY USE THIS SOFTWARE
           See file license_commercial_plus.txt
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2009-09-24 21:22:11 -0300 (Thu, 24 Sep 2009) $
           $LastChangedRevision: 1447 $
***************************************************************************/ 

#include "mco.h"
#include "mcop.h"
#include "mcohw.h"
#include <string.h>

// this structure holds all node specific configuration
extern MCO_CONFIG MEM_FAR gMCOConfig;

// external declaration for the process image array
extern UNSIGNED8 MEM_PROC gProcImg[];

// this structure holds the CAN message for SDO responses or aborts
extern CAN_MSG MEM_FAR gTxSDO;

#if NR_OF_TPDOS > 0
// this structure holds all the TPDO configuration data for up to 4 TPDOs
extern TPDO_CONFIG MEM_FAR gTPDOConfig[NR_OF_TPDOS];
#endif

#if NR_OF_RPDOS > 0
// this structure holds all the RPDO configuration data for up to 4 RPDOs
extern RPDO_CONFIG MEM_FAR gRPDOConfig[NR_OF_RPDOS];
#endif


/**************************************************************************
GLOBAL/MODULE VARIABLES
***************************************************************************/ 

#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
HBCONS_CONFIG gHBCons[NR_OF_HB_CONSUMER];
#endif // (NR_OF_HB_CONSUMER > 0)
#endif // MONITOR_ALL_NODES

#if USE_EMCY
EMCY_CONFIG MEM_FAR mEF; // Emergency configuration
#endif


/**************************************************************************
PUBLIC FUNCTIONS
***************************************************************************/ 

/**************************************************************************
DOES:    This function reads data from the process image and copies it
         to an OUTPUT location
RETURNS: Number of bytes that were copied
**************************************************************************/
UNSIGNED8 MCO_ReadProcessData (
  UNSIGNED8 MEM_PROC *pDest, // Destination pointer
  UNSIGNED8 length, // Number of bytes to copy
  UNSIGNED16 offset // Offset of source data in process image
  )
{
  PI_READ(PIACC_APP,offset,pDest,length);
  return length;
}


/**************************************************************************
DOES:    This function writes data from an INPUT location to the process 
         image
RETURNS: Number of bytes that were copied
**************************************************************************/
UNSIGNED8 MCO_WriteProcessData (
  UNSIGNED16 offset, // Offset of destination data in process image
  UNSIGNED8 length,  // Number of bytes to copy
  UNSIGNED8 MEM_PROC *pSrc // Source pointer
  )
{
  PI_WRITE(PIACC_APP,offset,pSrc,length);
  return length;
}


#if USE_EMCY
#if ERROR_FIELD_SIZE > 0
/**************************************************************************
DOES:    This function clears all entries of the error history [1003h]
RETURNS: Nothing
**************************************************************************/
void MCOP_ErrField_Flush (void)
{ // Reset pointer and counter for error field
  mEF.InPtr = 0;
  mEF.NrOfRec = 0;
}


/**************************************************************************
DOES:    This function adds an entry to the error history [1003h]
RETURNS: Nothing
**************************************************************************/
void MCOP_ErrField_Add (
  UNSIGNED32 err_value // the 32bit error code used in the last EMCY
  )
{ // add entry to field
  mEF.Field[mEF.InPtr] = err_value;
  // increment pointer and counter
  mEF.InPtr++;
  if (mEF.InPtr >= ERROR_FIELD_SIZE)
  { // roll over on end of buffer
    mEF.InPtr = 0;
  }
  mEF.NrOfRec++;
  if (mEF.NrOfRec >= ERROR_FIELD_SIZE)
  { // maximum is ERROR_FIELD_SIZE
    mEF.NrOfRec = ERROR_FIELD_SIZE;
  }
}


/**************************************************************************
DOES:    This function retrieves an entry from the error history [1003h]
         based on the subindex of [1003h]
RETURNS: 32bit error code stored at a subindex
**************************************************************************/
UNSIGNED32 MCOP_ErrField_Get (
  UNSIGNED8 subindex // Subindex number of [1003h]
  )
{
UNSIGNED32 ret_value = 0xFFFFFFFF;
INTEGER16 offset;

  if (subindex == 0)
  { // return number of entries
    ret_value = mEF.NrOfRec;
  }
  else
  { // return error value history (1 returns newest)
    if (subindex <= mEF.NrOfRec)
    { // only continue if subindex is in legal range
      offset = mEF.InPtr - subindex;
      if (offset < 0)
      {
        offset += ERROR_FIELD_SIZE;
      }
      ret_value = mEF.Field[offset];
    }
  }
  return ret_value;
}
#endif // ERROR_FIELD_SIZE > 0


/**************************************************************************
DOES:    Transmits an Emergency Message
RETURNS: TRUE - If msg was considered for transmit
         FALSE - If message was not sent due to duplicate
**************************************************************************/
UNSIGNED8 MCOP_PushEMCY
  (
  UNSIGNED16 emcy_code, // 16 bit error code
  UNSIGNED8  em_1, // 5 byte manufacturer specific error code
  UNSIGNED8  em_2,
  UNSIGNED8  em_3,
  UNSIGNED8  em_4,
  UNSIGNED8  em_5
  )
{
UNSIGNED8 ret_val;

#if ERROR_FIELD_SIZE > 0
  if (emcy_code != 0)
  {
    // add error to error history
    MCOP_ErrField_Add((UNSIGNED32)emcy_code +
                      ((UNSIGNED32)em_1 << 16) +
                      ((UNSIGNED32)em_2 << 24)
                     );
  }
#endif

  // Do not send same error code twice, unless its zero
/*
  if ( (mEF.emcy_msg.BUF.BUF[0] == (UNSIGNED8) emcy_code) &&
       (mEF.emcy_msg.BUF.BUF[1] == (UNSIGNED8) (emcy_code >> 8)) &&
       (emcy_code != 0)
     )
*/
  // Could not send pause/resume messages if pause/resume was set from another node
  if(0)
  { // same error as last
    ret_val = FALSE;
  }
  else 
  {
    mEF.emcy_msg.ID = 0x80 + MY_NODE_ID;
    mEF.emcy_msg.LEN = 8;
    mEF.emcy_msg.BUF.BUF[0] = (UNSIGNED8) emcy_code;
    mEF.emcy_msg.BUF.BUF[1] = (UNSIGNED8) (emcy_code >> 8);
    mEF.emcy_msg.BUF.BUF[2] = gMCOConfig.error_register;
    mEF.emcy_msg.BUF.BUF[3] = em_1;
    mEF.emcy_msg.BUF.BUF[4] = em_2;
    mEF.emcy_msg.BUF.BUF[5] = em_3;
    mEF.emcy_msg.BUF.BUF[6] = em_4;
    mEF.emcy_msg.BUF.BUF[7] = em_5;

    ret_val = TRUE;
  }
  return ret_val;
}
#endif // USE_EMCY


#if (NR_OF_RPDOS > 0) || (NR_OF_TPDOS > 0)
/**************************************************************************
DOES: Common exit routine for SDO_Handler. 
      Send SDO response with write confirmation.
      Assumes that gTxSDO.ID, LEN and BUF[1-3] are already set
**************************************************************************/
void MCOP_WriteConfirm (
  void
  )
{
UNSIGNED8 i;

  // Load SDO Response into transmit buffer
  gTxSDO.BUF.BUF[0] = 0x60; // Write response code
  // Clear unused bytes
  for (i = 4; i < 8; i++)
  {
    gTxSDO.BUF.BUF[i] = 0;
  }
    
  // Transmit SDO Response message
  if (!MCOHW_PushMessage(&gTxSDO))
  {
    MCOUSER_FatalError(ERROFL_SDO);
  }
}


/**************************************************************************
DOES:    Handles incoming SDO Request for accesses to PDO Communication
         Parameters
RETURNS: 0: Wrong access, SDO Abort sent
         1: Access was made, SDO Response sent
GLOBALS: Various global variables with configuration information
**************************************************************************/
UNSIGNED8 SDO_HandlePDOComParam (
  UNSIGNED8  PDOType,  // 0 for TPDO, 1 for RPDO
  UNSIGNED16 handlePDOComParamIndex,    // OD index
  UNSIGNED8  *pData    // pointer to SDO Request message
  )
{
UNSIGNED16 lp;
UNSIGNED16 PDONr;    // PDONr - 1
UNSIGNED8 cmd;       // SDO Request command byte
UNSIGNED8 len_req;   // length of SDO write access
UNSIGNED16 rdat = 0; // current response data
UNSIGNED8 reply[4];  // SDO reply value

  cmd = pData[0];
  PDONr = (handlePDOComParamIndex & 0x1FF) + 1;
  len_req = 4-((cmd >> 2) & 0x03);

  // calculate real PDONr offset
  if (PDOType == 0)
  { // TPDO, find the PDONr in array
#if (NR_OF_TPDOS > 0)
    lp = 0;
    while (gTPDOConfig[lp].PDONr != PDONr)
    {
      lp++;
      if (lp >= NR_OF_TPDOS)
      { // not found!
        MCO_SendSDOAbort(SDO_ABORT_NOT_EXISTS);
        return 0;
      }
    }
    // PDO found, set PDONr to index
    PDONr = lp;
#endif
  }
  else
  { // RPDO, find the PDONr in array
#if (NR_OF_RPDOS > 0)
    lp = 0;
    while (gRPDOConfig[lp].PDONr != PDONr)
    {
      lp++;
      if (lp >= NR_OF_RPDOS)
      { // not found!
        MCO_SendSDOAbort(SDO_ABORT_NOT_EXISTS);
        return 0;
      }
    }
    // PDO found, set PDONr to index
    PDONr = lp;
#endif
  }

  if (pData[3] == 0) // subindex
  { // Nr Of Entries: Read-only, "2" for RPDO, "5" for TPDO
    if (cmd == 0x40)
    { // Read
      if (PDOType == 0)
      { // TPDO
        reply[0] = 5;
      }
      else
      { // RPDO
        reply[0] = 2;
      }
      MCO_ReplyWith(reply,1);
      return 1;
    }
    else
    { // Write
      MCO_SendSDOAbort(SDO_ABORT_READONLY);
      return 0;
    }
  }

  if (PDOType == 0)
  { // TPDO
#if NR_OF_TPDOS >> 0
    rdat = gTPDOConfig[PDONr].CAN.ID;
#endif
  }
  else
  { // RPDO
#if NR_OF_RPDOS >> 0
    rdat = gRPDOConfig[PDONr].CANID;
#endif
  }

  if (pData[3] == 1) // subindex
  { // COB ID
    if (cmd == 0x40)
    { // Read
      // Load SDO Response into transmit buffer
      gTxSDO.BUF.BUF[7] = (UNSIGNED8)(rdat >> 8) & 0x80; // PDO Enable/Disable bit
      if (PDOType == 0)
      { // TPDO
        gTxSDO.BUF.BUF[7] |= 0x40; // No RTR
      }
      gTxSDO.BUF.BUF[6] = 0;
      gTxSDO.BUF.BUF[5] = (UNSIGNED8) ((rdat) >> 8) & 0x07; // Bits 8-10 of CAN ID
      gTxSDO.BUF.BUF[4] = (UNSIGNED8) rdat; // Bits 0-7 of CAN ID
      gTxSDO.BUF.BUF[0] = 0x43; // Expedited, 4 bytes
      if (!MCOHW_PushMessage(&gTxSDO))
      {
        MCOUSER_FatalError(ERROFL_SDO);
      }
      return 1;
    }
    else
    { // Write
      if (PDOType == 0)
      { // TPDO
#if NR_OF_TPDOS >> 0
        if ((pData[7] & 0x40) == 0)
        { // RTR not supported
          MCO_SendSDOAbort(SDO_ABORT_VALUE_RANGE);
          return 0;
        }
        if (((gTPDOConfig[PDONr].CAN.ID & 0x8000) != 0) ||
            ((pData[7] & 0x80) != 0)
           )
        { // Only allowed if PDO is disabled, or this access disables it
          // set new CAN ID
          gTPDOConfig[PDONr].CAN.ID = pData[4] + (((UNSIGNED16) (pData[5] | pData[7])) << 8);
          // write completed
          MCOP_WriteConfirm();
          return 1;
        }
#endif // NR_OF_TPDOS
      }
      else
      { // RPDO                                        
#if NR_OF_RPDOS >> 0
        if (((gRPDOConfig[PDONr].CANID & 0x8000) != 0) ||
            ((pData[7] & 0x80) != 0)
           )
        { // Only allowed if PDO is disabled, or this access disables it
          // remove CAN ID filter
          MCOHW_ClearCANFilter(gRPDOConfig[PDONr].CANID);
          // Signal that RPDO filters are NOT set
          gMCOConfig.error_code &= ~0x80; 
          // set new CAN ID
          gRPDOConfig[PDONr].CANID = pData[4] + (((UNSIGNED16) (pData[5] | pData[7])) << 8);
          // write completed
          MCOP_WriteConfirm();
          return 1;
        }
#endif // NR_OF_RPDOS
      }
      MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      return 0;
    } // write
  } // subindex 1
  
  // Now subindex is > 1, only allow writes if PDO is disabled
  if (cmd == 0x23)
  { // It is a write command
    if (!(rdat & 0x8000))
    { // PDO is not disabled
      MCO_SendSDOAbort(SDO_ABORT_UNSUPPORTED);
      return 0;
    }
  }
  
  // Now handle remaining subindexes
  if (pData[3] == 2) // subindex
  { // Transmission Type
    if (cmd == 0x40)
    { // Read
      if (PDOType == 0)
      { // TPDO
#if NR_OF_TPDOS >> 0
        reply[0] = gTPDOConfig[PDONr].TType;
#endif
      }
      else
      { // RPDO
#if NR_OF_RPDOS >> 0
        reply[0] = gRPDOConfig[PDONr].TType;
#endif
      }
      MCO_ReplyWith(reply,1);
      return 1;
    }

    // Write
    if (len_req != 1)
    {
      MCO_SendSDOAbort(SDO_ABORT_TYPEMISMATCH);
      return 0;
    }
#if ! USE_SYNC
    if (pData[4] <= 252)
    { // SYNC not supported
      MCO_SendSDOAbort(SDO_ABORT_VALUE_RANGE);
      return 0;
    }
#endif // USE_SYNC

    // RTR is not supported
    if (pData[4] == 253)
    { // RTR not supported
      MCO_SendSDOAbort(SDO_ABORT_VALUE_RANGE);
      return 0;
    }

    // This code version does not support the combination of SYNC with RTR
    if (pData[4] == 252)
    { // SYNC-RTR combination not supported
      MCO_SendSDOAbort(SDO_ABORT_VALUE_RANGE);
      return 0;
    }

    if (PDOType == 0)
    { // TPDO
#if NR_OF_TPDOS >> 0
      gTPDOConfig[PDONr].TType = pData[4];
#if USE_SYNC
      gTPDOConfig[PDONr].SYNCcnt = gTPDOConfig[PDONr].TType;
#endif // USE_SYNC
#endif // NR_OF_TPDOS
    }
    else
    { // RPDO
#if NR_OF_RPDOS >> 0
      gRPDOConfig[PDONr].TType = pData[4];
#endif
    }
    MCOP_WriteConfirm();
    return 1;
  }

  if (PDOType != 0)
  { // RPDO
    // No subindex > 2 supported for RPDO
    MCO_SendSDOAbort(SDO_ABORT_UNKNOWNSUB);
    return 0;
  }
  
#if NR_OF_TPDOS >> 0
  if (pData[3] == 3) // subindex
  { // Inhibit Time 
    if (cmd == 0x40)
    { // Read
#if USE_INHIBIT_TIME
      rdat = gTPDOConfig[PDONr].inhibit_time * 10;
#else
      rdat = 0;
#endif
      reply[0] = rdat;
      reply[1] = rdat >> 8;
      MCO_ReplyWith(reply,2);
      return 1;
    }
    // Write
    if (len_req != 2)
    {
      MCO_SendSDOAbort(SDO_ABORT_TYPEMISMATCH);
      return 0;
    }
    gTPDOConfig[PDONr].inhibit_time = 
        (((((UNSIGNED16) pData[5]) << 8) + pData[4]) + 9)/10;
    MCOP_WriteConfirm();
    return 1;
  }
  
  if (pData[3] == 5) // subindex
  { // Event Time
    if (cmd == 0x40)
    { // Read
      rdat = gTPDOConfig[PDONr].event_time;
      reply[0] = rdat;
      reply[1] = rdat >> 8;
      MCO_ReplyWith(reply,2);
      return 1;
    }
    // Write
    if (len_req != 2)
    {
      MCO_SendSDOAbort(SDO_ABORT_TYPEMISMATCH);
      return 0;
    }
    gTPDOConfig[PDONr].event_time = (((UNSIGNED16) pData[5]) << 8) + pData[4];;
    if (gTPDOConfig[PDONr].event_time > 0x7FFF)
    {
      gTPDOConfig[PDONr].event_time = 0x7FFF;
    }
    MCOP_WriteConfirm();
    return 1;
  }
#endif // NR_OF_TPDOS >> 0
      
  MCO_SendSDOAbort(SDO_ABORT_UNKNOWNSUB);
  return 0;
}
#endif // (NR_OF_RPDOS > 0) || (NR_OF_TPDOS > 0)


#if USE_SYNC
/**************************************************************************
DOES:    Processes reception of the SYNC message
RETURNS: 0: No messages processed
         Bit 0 set: SYNC TPDOs transmitted
         Bit 1 set: SYNC RPDOs received
**************************************************************************/
UNSIGNED8 MCOP_HandleSYNC (
  void
  )
{
UNSIGNED16 PDONr;
UNSIGNED8 retstat;

#if USECB_SYNCRECEIVE
  MCOUSER_SYNCReceived();
#endif
#if (NR_OF_RPDOS > 0) || (NR_OF_TPDOS > 0)
  if (MY_NMT_STATE != 5)
  { // node is not in operational state
    return 0;
  }
  retstat = 0;

#if (NR_OF_TPDOS > 0)
  for (PDONr = 0; PDONr < NR_OF_TPDOS; PDONr++)
  {
    if ((gTPDOConfig[PDONr].CAN.ID & 0x8000) == 0)
    { // this TPDO is used
      if (gTPDOConfig[PDONr].TType == 0)
      { // Combination COS and SYNC
        // has application data changed?
        if (PDO_TXCOMP(PDONr,&(gTPDOConfig[PDONr].CAN.BUF.BUF[0])) != 0)
        {
#if USECB_TPDORDY
          MCOUSER_TPDOReady(PDONr+1,2);
#endif
          // Copy application data
          PDO_TXCOPY(PDONr,&(gTPDOConfig[PDONr].CAN.BUF.BUF[0]));
          // transmit now
          MCO_TransmitPDO(PDONr);
          retstat |= 1;
        }
      }
      if ((gTPDOConfig[PDONr].TType >= 1) &&
          (gTPDOConfig[PDONr].TType <= 240)
         )
      { // This PDO is synced
        gTPDOConfig[PDONr].SYNCcnt--;
        if (gTPDOConfig[PDONr].SYNCcnt == 0)
        { // SYNC counter reached zero, reset counter and transmit PDO
#if USECB_TPDORDY
          MCOUSER_TPDOReady(PDONr+1,1);
#endif
           // Copy application data
          PDO_TXCOPY(PDONr,&(gTPDOConfig[PDONr].CAN.BUF.BUF[0]));
          // transmit now
          MCO_TransmitPDO(PDONr);
          retstat |= 1;
        }
      }
    }
  }
#endif // NR_OF_TPDOS

#if (NR_OF_RPDOS > 0)
  for (PDONr = 0; PDONr < NR_OF_RPDOS; PDONr++)
  {
    if ((gRPDOConfig[PDONr].CANID & 0x8000) == 0)
    { // this RPDO is used
      if (gRPDOConfig[PDONr].TType <= 240)
      { // This RPDO is synced
        // copy data from RPDO to process image
        PDO_RXCOPY(PDONr,&(gRPDOConfig[PDONr].BUF[0]));
#if USECB_RPDORECEIVE
        MCOUSER_RPDOReceived(gRPDOConfig[PDONr].PDONr,gRPDOConfig[PDONr].offset,gRPDOConfig[PDONr].len);
#endif // USECB_RPDORECEIVE
        retstat |= 2;
      }
    }
  }
#endif // NR_OF_RPDOS

  return retstat;
#else   // (NR_OF_RPDOS > 0) || (NR_OF_TPDOS > 0)
  return 0;
#endif  // (NR_OF_RPDOS > 0) || (NR_OF_TPDOS > 0)
}
#endif // USE_SYNC


#if USE_NODE_GUARDING
/**************************************************************************
DOES:    Checks if message received is guarding request
RETURNS: 0: message received is not guarding request
         1: guarding request received, response sent
**************************************************************************/
UNSIGNED8 MCOP_HandleGuarding (
  UNSIGNED16 can_id
  )
{
  if (can_id == (UNSIGNED16) 0x0700 + MY_NODE_ID)
  { // ID matches, so probably a request
    // transmit response / heartbeat message
    // Merge toggle bit into response
    MY_NMT_STATE += gMCOConfig.NGtoggle;
    if (!MCOHW_PushMessage(&gMCOConfig.heartbeat_msg))
    {
      MCOUSER_FatalError(ERROFL_HBT);
    }
    // Remove toggle bit again
    MY_NMT_STATE &= 0x7F;
    if (gMCOConfig.NGtoggle == 0)
    {
      gMCOConfig.NGtoggle = 0x80;
    }
    else
    {
      gMCOConfig.NGtoggle = 0;
    }
    return 1;
  }
  return 0;
}
#endif


#if ! MONITOR_ALL_NODES
#if (NR_OF_HB_CONSUMER > 0)
/**************************************************************************
DOES:    Checks if a node ID is already used, needed for conformance test
RETURNS: TRUE, if node_id is already used
**************************************************************************/
UNSIGNED8 MCOP_IsHBMonitored (
  UNSIGNED8 channel,
  UNSIGNED8 node_id
  )
{
UNSIGNED8 loop;

  if (gHBCons[--channel].can_id == 0x700 + (UNSIGNED16) node_id)
  { // the current channel already uses this node ID, so OK to modify
    return FALSE;
  }

  loop = NR_OF_HB_CONSUMER;  
  while (loop > 0)
  {
    loop--;
    if ((UNSIGNED16) node_id + 0x700 == gHBCons[loop].can_id)
    {
      return TRUE;
    }
  }
  return FALSE;
}


/**************************************************************************
DOES:    Initializes Heartbeat Consumer
GLOBALS: Inits gHBCons[consumer_channel-1]
**************************************************************************/
void MCOP_InitHBConsumer (
  UNSIGNED8 consumer_channel, // HB Consumer channel
  UNSIGNED8 node_id, // Node ID to monitor
  UNSIGNED16 hb_time // Timeout ot use (in ms)
  )
{
#if CHECK_PARAMETERS
  // check ranges
  if (((node_id < 1) || (node_id > 127)) ||
      ((consumer_channel == 0) || (consumer_channel > NR_OF_HB_CONSUMER))
     )
  {
    MCOUSER_FatalError(0x9901);
  }
#endif

  consumer_channel--; // adapt to range 0 to NR_OF_HB_CONSUMER-1

  gHBCons[consumer_channel].time = hb_time;
  gHBCons[consumer_channel].can_id = 0x700 + node_id;
  gHBCons[consumer_channel].status = 1;

  if (!MCOHW_SetCANFilter(gHBCons[consumer_channel].can_id))
  {
    MCOUSER_FatalError(0x9902);
  }

}

/**************************************************************************
DOES:    Checks if a message received contains a heartbeat to be consumed
GLOBALS: Updates gHBCons[consumer_channel-1]
RETURNS: one, if message received was a heartbeat monitored, else zero
**************************************************************************/
UNSIGNED8 MCOP_ConsumeHB (
  CAN_MSG *pRxCAN // CAN message received
  )
{
UNSIGNED8 loop;

  for (loop = 0; loop < NR_OF_HB_CONSUMER; loop++)
  {
    if ( (gHBCons[loop].status != 0) && // consumer is not disabled
         (pRxCAN->ID == gHBCons[loop].can_id) // CAN ID matches
       )
    { // Match found
      if (pRxCAN->BUF.BUF[0] != 0)
      { // This is not the bootup message
        gHBCons[loop].status = 2; // activate consumption
        // calculate expiration timestamp
        gHBCons[loop].timestamp = MCOHW_GetTime() + gHBCons[loop].time;
      }
      return 1;
    }
  }
  return 0;
}


/**************************************************************************
DOES:    Checks if a heartbeat consumer timeout occured
RETURNS: 0: channel not used
         1: channel used, but not started
         2: channel used, monitoring active
         127: channel used, timeout occured
**************************************************************************/
UNSIGNED8 MCOP_ProcessHBCheck (
  UNSIGNED8 consumer_channel
  )
{
#if CHECK_PARAMETERS
  // check ranges
  if ((consumer_channel == 0) || (consumer_channel > NR_OF_HB_CONSUMER))
  {
    MCOUSER_FatalError(0x9903);
  }
#endif

  consumer_channel--; // adapt to range 0 to NR_OF_HB_CONSUMER-1

  if (gHBCons[consumer_channel].status == 2)
  { // Heartbeat consumer is active
    if (MCOHW_IsTimeExpired(gHBCons[consumer_channel].timestamp))
    { // active and expired
      gHBCons[consumer_channel].status = 127;
#if USE_EMCY
      MCOP_PushEMCY(0x8130,consumer_channel+1,0,0,0,0);
#endif // USE_EMCY
    }
  }
  return gHBCons[consumer_channel].status;
}
#endif // (NR_OF_HB_CONSUMER > 0)
#endif // MONITOR_ALL_NODES



#if USE_SLEEP
/**************************************************************************
Description in mco.h
***************************************************************************/ 
UNSIGNED8 MCO_TransmitWakeup ( 
  void
  )
{
CAN_MSG TxMSG;

  TxMSG.ID = 0x680;
  TxMSG.LEN = 8;
  TxMSG.BUF.BUF[0] = 0;
  TxMSG.BUF.BUF[1] = 0;
  TxMSG.BUF.BUF[2] = 0;
  TxMSG.BUF.BUF[3] = 0;
  TxMSG.BUF.BUF[4] = 0;
  TxMSG.BUF.BUF[5] = 0;
  TxMSG.BUF.BUF[6] = 0;
  TxMSG.BUF.BUF[7] = 0;
  return MCOHW_PushMessage(&TxMSG);
}


/**************************************************************************
Description in mco.h
***************************************************************************/ 
UNSIGNED8 MCO_TransmitSleepObjection ( 
  void
  )
{
CAN_MSG TxMSG;

  TxMSG.ID = 0x681;
  TxMSG.LEN = 0;
  return MCOHW_PushMessage(&TxMSG);
}


/**************************************************************************
Description in mco.h
***************************************************************************/ 
UNSIGNED8 MCO_TransmitSleepRquest ( 
  void
  )
{
CAN_MSG TxMSG;

  TxMSG.ID = 0x683;
  TxMSG.LEN = 0;
  return MCOHW_PushMessage(&TxMSG);
}
#endif // USE_SLEEP

/*----------------------- END OF FILE ----------------------------------*/
