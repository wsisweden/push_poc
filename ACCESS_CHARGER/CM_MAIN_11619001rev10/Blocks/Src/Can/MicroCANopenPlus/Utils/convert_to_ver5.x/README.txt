CONVERT.BAT
CONVERT.PLX

Win32 batch file and Perl script to convert the pre-5.0 versions of
MicroCANopen Plus to the new configuration style of versions 5.0 and up.
Existence/non-existence of #defines are replaced with boolean values for the
respective configuration options.


Background
==========
In versions before 5.0 of MicroCANopen Plus, options were enabled by
preprocessor definitions in nodecfg.h such as this:

// If defined, the call-back function MCOUSER_NMTChange is called
// everytime the CANopen stack changes its NMT Slave State
#define USECB_NMTCHANGE

If the option should not be enabled, the definition had to be commented-
out, such as here:

// If defined, the call-back function MCOUSER_NMTChange is called
// everytime the CANopen stack changes its NMT Slave State
//#define USECB_NMTCHANGE

Versions from 5.0 require that all definitions are present as boolean
values in nodecfg.h. An enabled option would now be

#define USECB_NMTCHANGE 1

and a disabled option

#define USECB_NMTCHANGE 0


What it does
============
The batch file goes through all C files in a given directory and all
subdirectories and replaces active #defines without values with #defines
that are set to 1 (TRUE) and commented-out #defines of the same kind with
ones that are set to 0 (FALSE). Also, #ifdefs and #ifndefs are replaced with
appropriate #if directives.

There are several exceptions for special defines that shouldn't be
replaced, such as defines with a leading underscore or that end with _H.
Those defines are used to prevent multiple inclusion of include files. There
are other exceptions for cases that should stay plain defines without
values.


Requirements
============
The script uses Perl which is available for free for all major operating
systems. For Windows (Win32), it has been tested with Strawberry Perl which
can be obtained from

http://strawberryperl.com

No libraries are needed. You may copy the main executable and DLL into the
script directory because they are the only files that are needed:

perl.exe
perlXXX.dll  (XXX = version)


How to use
==========
Call the batch file by giving the topmost MicroCANopen Plus directory as the
only parameter, e.g.

> convert.bat "C:\Program Files\MicroCANopen Plus\"

The recommended method for upgrading from a pre-5.0 version is the following:

1. Run the batch file on your current project directory with pre-5.0
   MicroCANopen Plus sources. Try to limit the processing to sources that
   came with the stack and avoid running it on other parts and modules
   that are part of your application. If necessary, move those away
   temporarily.

2. Build and test the converted application. In particular, review the changes
   to the project-dependent files nodecfg.h and user_XXXX.c.

3. If the new version tests successfully, you are ready to replace the
   MicroCANopen base code with the new, version 5.0 or up, code. Keep the
   original, converted nodecfg.h and user_XXXX.c files. Only add defines that
   are new in the new version of MicroCANopen Plus to your nodecfg.h.


--------------- END --- OF --- FILE ---------------
