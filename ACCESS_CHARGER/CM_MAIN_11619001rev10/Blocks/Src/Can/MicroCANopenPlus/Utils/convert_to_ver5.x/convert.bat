@echo off
if "%~1"=="" goto nodir
for /R "%~1" %%f in (*.c *.h) do (
echo Processing "%%f"
perl -i.bak convert.plx "%%f"
)
goto end
:nodir
echo Usage:
echo noifdefs [Target directory with MicroCANopen Plus source]
:end
