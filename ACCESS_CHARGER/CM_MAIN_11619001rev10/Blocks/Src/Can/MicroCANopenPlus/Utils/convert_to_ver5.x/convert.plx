#!/usr/bin/perl

# Script to convert the pre-5.0 versions of MicroCANopen Plus to the new
# configuration style of versions 5.0 and up. Existence/non-existence of
# #defines are replaced with boolean values for the respective configuration
# options in the processed file.
#
# (c) 2009 Embedded Systems Academy

while (<>) {
  $_ =~ s/\n//;
  $_ =~ s/\r//;
  if ( /^[ \t]*#define[ \t]+\w+[ \t]*/  &&
      !/^[ \t]*#define[ \t]+\w[^ \t]*[ \t]+[^ \t].*/ &&
      !/^[ \t]*#define[ \t]+_/ &&
      !/^[ \t]*#define[ \t]+\w+(_H|_ADDR)\s*$/ &&
      !/^[ \t]*#define[ \t]+(MEM_|LED_)/ &&
      !/^[ \t]*#define[ \t]+(DUMMYBYTE_32BIT_ALIGNMENT|DUMMYWORD_32BIT_ALIGNMENT|DEBUG|SimulateProcessData|DECLARE_REGS|DECLARE_MAIN|RESET|RTOS_SLEEP|M16C6NK|HC812A4|TMS470R1A256|TMS470R1B1M|FAR_DATA)\s*/ &&
      !/^[ \t]*#define[ \t]+C\s*$/
  ) {
    print $_, " 1\n";
  } elsif ( m%^[ \t]*//[ \t]*#define[ \t]+\w+[ \t]*%  &&
           !m%^[ \t]*//[ \t]*#define[ \t]+\w+[ \t]+[^ \t].*% &&
           !m%^[ \t]*//[ \t]*#define[ \t]+(DUMMYBYTE_32BIT_ALIGNMENT|DUMMYWORD_32BIT_ALIGNMENT|DEBUG|DECLARE_REGS|DECLARE_MAIN|RESET|RTOS_SLEEP|M16C6NK|HC812A4|TMS470R1A256|TMS470R1B1M|FAR_DATA|kludge)%
  ) {
    $_ =~ s{//}{};
    print $_, " 0\n";
  }
  elsif ( /^[ \t]*#ifdef[ \t]+\w+[ \t]*/  &&
         !/^[ \t]*#ifdef[ \t]+\w+[ \t]+[^ \t].*/ &&
         !/^[ \t]*#ifdef[ \t]+_/ &&
         !/^[ \t]*#ifdef[ \t]+\w+(_H|_ADDR)\s*$/ &&
         !/^[ \t]*#ifdef[ \t]+(TRUE|FALSE|DEBUG|OD_SERIAL|SimulateProcessData|REBOOT_FLAG_ADR|DECLARE_REGS|DECLARE_MAIN|M16C6NK|HC812A4|TMS470R1A256|TMS470R1B1M|FAR_DATA)\s*/
  ) {
    $_ =~ s/#ifdef/#if/;
    print $_, "\n";
  }
  elsif ( /^[ \t]*#ifndef[ \t]+\w+[ \t]*/  &&
         !/^[ \t]*#ifndef[ \t]+\w+[ \t]+[^ \t].*/ &&
         !/^[ \t]*#ifndef[ \t]+_/ &&
         !/^[ \t]*#ifndef[ \t]+\w+(_H|_ADDR)\s*$/ &&
         !/^[ \t]*#ifndef[ \t]+(TRUE|FALSE|DEBUG|OD_SERIAL|SimulateProcessData|REBOOT_FLAG_ADR|DECLARE_REGS|DECLARE_MAIN|M16C6NK|HC812A4|TMS470R1A256|TMS470R1B1M|FAR_DATA)\s*/
  ) {
    $_ =~ s/#ifndef/#if !/;
    print $_, "\n";
  }
  else {
    print $_, "\n";
  }
}
