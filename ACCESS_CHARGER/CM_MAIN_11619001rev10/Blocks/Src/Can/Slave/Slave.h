#ifndef CAN_SLAVE_RECEIVE_H
#define CAN_SLAVE_RECEIVE_H

#include "../MicroCANopenPlus/MCO_DS401_LPC1768/types.h"
#include "cai_cc.h"

#define CAN_COMMUNICATION_WATCHDOG_TIMEOUT 2 // in (s)

void SlaveInit();

void SyncSlave(const ChargerMeas_type* meas);

int ReceiveSlave(
  UNSIGNED16 RPDONr, // RPDO Number
  UNSIGNED16 pRPDO,  // Offset to RPDO data in Process Image
  UNSIGNED8  len     // Length of RPDO
  );

int slaveWatchdog();										// Should be called once each second

int slaveGetInput(ChargerSet_type* input);
void slaveGetUactDisconnect(Interval_type* interval);
void slaveGetUactConnect(Interval_type* interval);
void slaveStartBattConnect2();
void slaveSendBattConnect2();
void slaveSendBmuCalibration1();
void slaveSendBmuCalibration2();
void slaveSendBmuCalibrationEnd();
void slaveStartVsense();
#endif
