#include "Slave/Slave.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/mcop.h"
#include "global.h"
#include "osa.h"
#include "reg.h"
#include "init.h"
#include "math.h"
#include "cai_cc.h"
#include "../Cc/ClusterControl.h"
#include "cai_cc.h"
#include "stackinit.h"
#include "../Regu/ControlLoop/SendToBattery.h"
#include "mpdo.h"

extern UNSIGNED8 MEM_NEAR gProcImg[];										// Make process image visible
int nodeIdMaster = 0;													// Node id of master, received with configuration message (id 0x2010)
static int UactIntervalNodeIdOld = 0;
static int watchdog[4] = {[0 ... 3] = 0};
static int received = 0;

#if NR_OF_RPDOS < 5
	#error there must be at least vit RPDOs
#endif

void SlaveInit(){
	extern MCO_CONFIG MEM_FAR gMCOConfig;
	UactIntervalNodeIdOld = 0;
    INITPDOS_CALLS;
}

void SyncSlave(const ChargerMeas_type* meas)
{
	PI_WRITE(PIACC_PDO, P201201_UMEAS, &meas->Meas.U, 4);					// Write measured voltage to process image
	PI_WRITE(PIACC_PDO, P201202_IMEAS, &meas->Meas.I, 4);					// Write measured current to process image

	PI_WRITE(PIACC_PDO, P201203_PMEAS, &meas->Meas.P, 4);					// Write measured power to process image
	PI_WRITE(PIACC_PDO, P201204_STATUS, &meas->Status.Sum, 4);				// Write status field to process image

	MCO_TriggerTPDO(1);														// Measurements
	MCO_TriggerTPDO(2);														// Measurements

	{
		/* CAN meas values to display */
		extern volatile ChargerMeas_type CanMeas;
		CanMeas = *meas;
	}
}

/**************************************************************************
DOES:    This function is called after an RPDO has been received and stored
         into the Process Image.
RETURNS: nothing
**************************************************************************/
int ReceiveSlave(
  UNSIGNED16 RPDONr, // RPDO Number
  UNSIGNED16 pRPDO,  // Offset to RPDO data in Process Image
  UNSIGNED8  len     // Length of RPDO
  )
{
	switch(RPDONr)
	{
	/* Sent by master received by slaves */
	case 1 :
		watchdog[0] = CAN_COMMUNICATION_WATCHDOG_TIMEOUT;
		received |= (1 << 0);
		break;
	case 2 :
		watchdog[1] = CAN_COMMUNICATION_WATCHDOG_TIMEOUT;
		received |= (1 << 1);
		{
			uint8_t data;

			PI_READ(PIACC_PDO, P201104_CONTROL + 3, &data, 1);					// Read at which node id battery detection interval is supposed to be received

			{
				int UactIntervalNodeId = data;									// Note, int use less flash memory than uint8_t

				if(UactIntervalNodeId != UactIntervalNodeIdOld){
					if(UactIntervalNodeIdOld){
						MCO_ResetRPDO(3);
						MCO_ResetRPDO(4);
						MCOHW_ClearCANFilter(UactIntervalNodeIdOld);			// so that we will receive emergencies addressed for master
					}
					if(UactIntervalNodeId){
						MCO_InitRPDO(3, 0x400 + UactIntervalNodeId, 8, P201301_REGUUACTOFFLOW);
						MCOHW_SetCANFilter(0x400 + UactIntervalNodeId);			// so that we will receive answer
						MCO_InitRPDO(4, 0x500 + UactIntervalNodeId, 8, P201303_REGUUACTONLOW);
						MCOHW_SetCANFilter(0x500 + UactIntervalNodeId);			// so that we will receive answer
						MCOHW_SetCANFilter(0x80 + UactIntervalNodeId);			// so that we will receive emergencies addressed for master
					}
					UactIntervalNodeIdOld = UactIntervalNodeId;
				}
			}
		}
		break;
	case 3 :
		watchdog[2] = CAN_COMMUNICATION_WATCHDOG_TIMEOUT;
		received |= (1 << 2);
		break;
	case 4 :
		watchdog[3] = CAN_COMMUNICATION_WATCHDOG_TIMEOUT;
		received |= (1 << 3);
		break;
	}

	return received;
}

int slaveGetInput(ChargerSet_type* input){
	int watchdogOk = watchdog[0] && watchdog[1] && (received | ((1<<1) | (1<<0)));

	if(watchdogOk){
		PI_READ(PIACC_PDO, P201101_USET, &input->Set.U, 4);							// Read voltage set value from process image
		PI_READ(PIACC_PDO, P201102_ISET, &input->Set.I, 4);							// Read current set value from process image
		PI_READ(PIACC_PDO, P201103_PSET, &input->Set.P, 4);							// Read power set value from process image
		PI_READ(PIACC_PDO, P201104_CONTROL, &input->Mode, 1);						// Read regulator mode from process image
		PI_READ(PIACC_PDO, P201104_CONTROL + 1, &input->Uact.both, 1);				// Read battery detection via voltage selection from process image
		PI_READ(PIACC_PDO, P201104_CONTROL + 2, &input->RemoteIn.both, 1);			// Read battery detection via remote in selection from process image
		PI_READ(PIACC_PDO, P201104_CONTROL + 3, &input->soc, 1);					// Read state of charge from process image

		if(input->Uact.off != BatterydetectCond_NotUsed){
			watchdogOk = watchdogOk && watchdog[2] && (received | (1<<2));
		}

		if(input->Uact.on != BatterydetectCond_NotUsed){
			watchdogOk = watchdogOk && watchdog[3] && (received | (1<<3));
		}

		{
			/* Trigger stop of battery communication */
			#include "../Regu/ControlLoop/sendToBattery.h"
			extern volatile batteryCommunication_type dataToBattery;
			if(input->Set.U != 0.0f){
				dataToBattery.Valid = 0;
			}
		}

		{
			/* CAN set values to display */
			extern volatile ChargerSet_type CanSet;
			CanSet = *input;
		}
	}
#if 0
	{
		/* CAN set values to display */
		extern volatile ChargerSet_type CanSet;
		ChargerSet_type CanSetTmp;
		if(watchdog[0]){
			PI_READ(PIACC_PDO, P201101_USET, &CanSetTmp.Set.U, 4);							// Read voltage set value from process image
			PI_READ(PIACC_PDO, P201102_ISET, &CanSetTmp.Set.I, 4);							// Read current set value from process image
		}
		else{
			CanSetTmp.Set.U = 0;
			CanSetTmp.Set.I = 0;
		}

		if(watchdog[1]){
			PI_READ(PIACC_PDO, P201103_PSET, &CanSetTmp.Set.P, 4);							// Read power set value from process image
			PI_READ(PIACC_PDO, P201104_CONTROL, &CanSetTmp.Mode, 1);						// Read regulator mode from process image
			PI_READ(PIACC_PDO, P201104_CONTROL + 1, &CanSetTmp.Uact.both, 1);				// Read battery detection via voltage selection from process image
			PI_READ(PIACC_PDO, P201104_CONTROL + 2, &CanSetTmp.RemoteIn.both, 1);			// Read battery detection via remote in selection from process image
			PI_READ(PIACC_PDO, P201104_CONTROL + 3, &CanSetTmp.soc, 1);					// Read state of charge from process image
		}
		else{
			CanSetTmp.Set.P = 0;
			CanSetTmp.Mode = 0;
			CanSetTmp.Uact.both = 0;
			CanSetTmp.RemoteIn.both = 0;
			CanSetTmp.soc = 0;
		}
		CanSet = CanSetTmp;
	}
#endif
	if(watchdogOk){
		received = 0;
		return 1;
	}
	else{
		return 0;
	}
}

void slaveGetUactDisconnect(Interval_type* interval){
	PI_READ(PIACC_PDO, P201301_REGUUACTOFFLOW, &interval->Low, 4);				// Read battery disconnect detection low limit
	PI_READ(PIACC_PDO, P201302_REGUUACTOFFHIGH, &interval->High, 4);			// Read battery disconnect detection high limit
}

void slaveGetUactConnect(Interval_type* interval){
	PI_READ(PIACC_PDO, P201303_REGUUACTONLOW, &interval->Low, 4);				// Read battery connect detection low limit
	PI_READ(PIACC_PDO, P201304_REGUUACTONHIGH, &interval->High, 4);				// Read battery connect detection high limit
}

int slaveWatchdog(){
	int n;
	for(n = 0; n < 4; n++){
		if(watchdog[n]){
			watchdog[n]--;
		}
		else{
			received &= ~(1 << n);
		}
	}

	return watchdog[1] && watchdog[0];
}

void slaveStartBattConnect2(){
	uint8_t bytes[4];

	bytes[0] = 0x20;
	bytes[1] = 0;
	bytes[2] = 0;
	bytes[3] = 0;

	mpdoDamSend(nodeIdMaster, 0x2010, 0x05, bytes);
}

void slaveSendBattConnect2(){
	uint8_t bytes[4];
	volatile CurrentCombination_t *pCurrentCombination;

	pCurrentCombination = sendToBattery2Get();

	bytes[0] = 0x21;
	bytes[1] = (Uint8)(pCurrentCombination->CurrentLevel1);
	bytes[2] = (Uint8)(pCurrentCombination->CurrentLevel2);
	bytes[3] = (Uint8)(pCurrentCombination->CurrentLevel3);

	mpdoDamSend(nodeIdMaster, 0x2010, 0x05, bytes);
}

void slaveSendBmuCalibration1(){
	uint8_t bytes[4];
	volatile BmCalibration_t *pBmCalibration;

	pBmCalibration = bmCalibrationGet();

	bytes[0] = 0x31;
	bytes[1] = 0;
	bytes[2] = 0;
	bytes[3] = 0;

	mpdoDamSend(nodeIdMaster, 0x2010, 0x04, bytes);

	bytes[0] = (Uint8)(pBmCalibration->CalibrationPoint1 >> 24);
	bytes[1] = (Uint8)(pBmCalibration->CalibrationPoint1 >> 16);
	bytes[2] = (Uint8)(pBmCalibration->CalibrationPoint1 >> 8);
	bytes[3] = (Uint8)(pBmCalibration->CalibrationPoint1);

	mpdoDamSend(nodeIdMaster, 0x2010, 0x05, bytes);
}

void slaveSendBmuCalibration2(){
	uint8_t bytes[4];
	volatile BmCalibration_t *pBmCalibration;

	pBmCalibration = bmCalibrationGet();

	bytes[0] = 0x32;
	bytes[1] = 0;
	bytes[2] = 0;
	bytes[3] = 0;

	mpdoDamSend(nodeIdMaster, 0x2010, 0x04, bytes);

	bytes[0] = (Uint8)(pBmCalibration->CalibrationPoint2 >> 24);
	bytes[1] = (Uint8)(pBmCalibration->CalibrationPoint2 >> 16);
	bytes[2] = (Uint8)(pBmCalibration->CalibrationPoint2 >> 8);
	bytes[3] = (Uint8)(pBmCalibration->CalibrationPoint2);

	mpdoDamSend(nodeIdMaster, 0x2010, 0x05, bytes);
}

void slaveSendBmuCalibrationEnd(){
	uint8_t bytes[4];

	bytes[0] = 0x33;
	bytes[1] = 0;
	bytes[2] = 0;
	bytes[3] = 0;

	mpdoDamSend(nodeIdMaster, 0x2010, 0x05, bytes);
}

void slaveStartVsense(){
	uint8_t bytes[4];

	bytes[0] = 0x40;;
	bytes[1] = 0;
	bytes[2] = 0;
	bytes[3] = 0;

	mpdoDamSend(nodeIdMaster, 0x2010, 0x05, bytes);
}
