#include "SetNodeId.h"
#include "MicroCANopenPlus/MCO/mco.h"
#include "can.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "NodesStatusAndMapping.h"
#include "MicroCANopenPlus/MCO/mcop.h"

#include "MicroCANopenPlus/MCO_DS401_LPC1768/procimg.h"
#include "MicroCANopenPlus/MCO/canfifo.h"

extern UNSIGNED8 MEM_NEAR gProcImg[];

static int NodeIdUsed; // Send information from SDO callback function to configuration during automatic node id assignment
static int NodeIdCounter = 0;

static enum {
	AutoIdState_Init,
	AutoIdState_CheckNumberFree,
	AutoIdState_Done,
	AutoIdState_NoFreeNodeId,
} AutoIdState = AutoIdState_Init;

static void CheckNodeIdFree();

/**************************************************************************
DOES:    Call-back function for reset communication.
         Re-initializes the process image and the entire MicroCANopen
         communication.
**************************************************************************/
void MCOUSER_ResetCommunication (int resetNodeId){
	extern UNSIGNED16 CanBps;

	enum {SetNodedIdAuto, SetNodeIdSet, SetNodeIdOff} SetNodeIdSelector;

	CAN_NodeStatusAndMapping_Reset();					// Reset mapping information
	if(resetNodeId){
		NodeIdCounter = 0;
	}

	extern int CAN_Autostart;
	extern enum can_CommProfile_enum CAN_CommProfile;
	switch(CAN_CommProfile){
	case CAN_COMM_PROFILE_DISABLED :										// CAN disabled
		CAN_Autostart = 0;
		SetNodeIdSelector = SetNodeIdOff;
		break;
	case CAN_COMM_PROFILE_MASTER :											// Communication profile master
		CAN_Autostart = 1;
		SetNodeIdSelector = SetNodedIdAuto;
		break;
	case CAN_COMM_PROFILE_SLAVE :											// Communication profile slave
		CAN_Autostart = 0;
		SetNodeIdSelector = SetNodedIdAuto;
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :								// Communication profile master external IO
		CAN_Autostart = 0;
		SetNodeIdSelector = SetNodeIdSet;
		break;
	case CAN_COMM_PROFILE_STATUS :											// Communication profile status
		CAN_Autostart = 1;
		SetNodeIdSelector = SetNodeIdSet;
		break;
	default :
		CAN_Autostart = 0;
		SetNodeIdSelector = SetNodedIdAuto;
		break;
	}

#if USE_LSS_SLAVE && USE_STORE_PARAMETERS
	LSS_LoadConfiguration(&CanBps,&node_id);
	LSS_Init(node_id);
#endif

	switch(SetNodeIdSelector){
	case SetNodedIdAuto :
		{
			extern MCO_CONFIG MEM_FAR gMCOConfig;
			if(NodeIdCounter <= AUTO_NODE_ID_LAST){
				AutoIdState = AutoIdState_Init;
			}
			else{
				AutoIdState = AutoIdState_NoFreeNodeId;
			}
			MY_NMT_STATE = NMTSTATE_AUTO_ID;						// Note do not call SetNodeIdAuto from here since any function in current state may be run after this
		}
		break;
	case SetNodeIdSet :
		{
			extern UNSIGNED8 NodeIdFixed;

			CAN_NodeStatusAndMapping_AddNode(NodeIdFixed);
			CAN_NodeStatusAndMapping_SetSerialNumber(NodeIdFixed, MCOUSER_GetSerial());	// Add this node to available nodes

			MCO_Init(CanBps, NodeIdFixed, DEFAULT_HEARTBEAT);
		}
		MCOUSER_ResetPDO();
		break;
	case SetNodeIdOff :
		{
			extern MCO_CONFIG MEM_FAR gMCOConfig;
			MY_NMT_STATE = NMTSTATE_STOP;
		}
		MCO_Init(CanBps, 0, DEFAULT_HEARTBEAT);
		break;
	}
}

void SetNodeIdAuto(){
	static UNSIGNED16 timer;
	extern MCO_CONFIG MEM_FAR gMCOConfig;
	extern UNSIGNED16 CanBps;

	switch (AutoIdState){
	case AutoIdState_Init :
		MCO_Init(CanBps, 0, 0);
		NodeIdCounter++;
		MY_NODE_ID = NodeIdCounter;							// Set Node-ID tentatively
		MY_NMT_STATE = NMTSTATE_AUTO_ID;					// Note do not call SetNodeIdAuto from here since any function in current state may be run after this

		CheckNodeIdFree();									// Check if Node-ID free
		timer = MCOHW_GetTime() + SDO_TIMEOUT;				// Set timeout

		AutoIdState = AutoIdState_CheckNumberFree;
		break;
	case AutoIdState_CheckNumberFree :
		if(NodeIdUsed){										// Node-ID already used ?
			MCOHW_ClearCANFilter(0x600 + MY_NODE_ID);		// so we can hear requests
			MCOHW_ClearCANFilter(0x580 + MY_NODE_ID);		// so we will receive answer

			NodeIdCounter++;
			MY_NODE_ID = NodeIdCounter;						// Try new Node-ID tentatively
			if(MY_NODE_ID > AUTO_NODE_ID_LAST){				// Tried all numbers
				MY_NODE_ID = 0;
				AutoIdState = AutoIdState_NoFreeNodeId;
			}
			else{
				CheckNodeIdFree();							// Check if Node-ID free
				timer = MCOHW_GetTime() + SDO_TIMEOUT;		// Set timeout
			}
		}
		else if( MCOHW_GetStatus() & HW_TXBSY){				// Message not sent ?
			timer = MCOHW_GetTime() + SDO_TIMEOUT;			// Set timeout
		}
		else if(MCOHW_IsTimeExpired(timer)) {				// No other node with this Node-ID answered ?
			MCOHW_ClearCANFilter(0x600 + MY_NODE_ID);	// so we can hear requests
			MCOHW_ClearCANFilter(0x580 + MY_NODE_ID);	// so we will receive answer

			CAN_NodeStatusAndMapping_AddNode(MY_NODE_ID);
			CAN_NodeStatusAndMapping_SetSerialNumber(MY_NODE_ID, MCOUSER_GetSerial());	// Add this node to available nodes

			MCO_Init(CanBps, MY_NODE_ID, DEFAULT_HEARTBEAT);// Set this Node-ID permanently
			MCOUSER_ResetPDO();
			extern volatile int CAN_AddressConflict;
			CAN_AddressConflict = 0;

			AutoIdState = AutoIdState_Done;
		}
		break;
	case AutoIdState_Done :
		break;
	case AutoIdState_NoFreeNodeId :
		break;
	}
}

void SetNodeIdSdoResponse(COBID_TYPE ID, UNSIGNED8 Len, UNSIGNED8 *pDat){
	extern MCO_CONFIG MEM_FAR gMCOConfig;
	if (ID == (unsigned) 0x580 + MY_NODE_ID){							// Received answer, node exist
		NodeIdUsed = 1;
	}
	else if(ID == (unsigned) 0x600 + MY_NODE_ID){						// Received request, node exist
		NodeIdUsed = 1;
		LPC_CAN1->CMR = ~((1<<7) | (1<<6) | (1<<5)) | (1<<1);			// Do not send request
		LPC_CAN1->MOD = 1;												// Reset, abort transmission if request is sent now
		CANTXFIFO_Flush();												// Remove all messages from transmission buffer
	}
}

static void CheckNodeIdFree(){
	static CAN_MSG SDO_Upload = { 0x600, { { 0x40, 0x00, 0x10, 0x00, 0, 0, 0, 0 } }, 8 }; // COB-ID = 600 + NodeId
	extern MCO_CONFIG MEM_FAR gMCOConfig;

	NodeIdUsed = 0;
	SDO_Upload.ID = 0x600 + MY_NODE_ID;

	MCOHW_SetCANFilter(0x600 + MY_NODE_ID);					// so we can hear requests
	MCOHW_SetCANFilter(0x580 + MY_NODE_ID);					// so we will receive answer
	MCOHW_PushMessage(&SDO_Upload);							// Check if Node-ID used
}
