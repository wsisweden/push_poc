#include "Status/Status.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/mcop.h"
#include "global.h"
#include "osa.h"
#include "reg.h"
#include "init.h"
#include "Slave/stackinit.h"
#include "cai_cc.h"
#include "chalg.h"

extern UNSIGNED8 MEM_NEAR gProcImg[];										// Make process image visible

#if NR_OF_RPDOS < 5
	#error there must be at least vit RPDOs
#endif

static Uint8 getChalgErrorCode(Uint16 ChalgError);
static Uint8 getReguErrorCode(ChargerError_type ReguError);
static Uint8 getBmuErrorCode(Uint16 ChalgError);

void StatusInit(){
	extern MCO_CONFIG MEM_FAR gMCOConfig;
    INITPDOS_CALLS;
}

void SyncStatus(const ChargerMeas_type* meas)
{
	ChargerStatusFields_type ReguStatusSum;
	Uint32 CID;
	Uint32 BID;
	Uint16 ChalgStatus;
	Uint16 ChargedAhP;
	Uint16 ChalgError;
	Uint16 BmuStatus;
	Uint16 Tbatt;
	Uint16 SOC;
	Uint16 u16temp;
	Uint8 u8temp;

	reg_get(&CID, can__CID);												// Get charger Id
	reg_get(&ChalgStatus, can__ChalgStatus);								// Get chalg status
	reg_get(&ReguStatusSum, can__ReguStatus);									// Get regu status and error
	reg_get(&ChargedAhP, can__chargedAhP);									// Get charged Ah in percent
	reg_get(&ChalgError, can__ChalgError);									// Get chalg error

	reg_get(&BID, can__BID);												// Get Battery Id
	reg_get(&BmuStatus, can__BmSatus);										// Get Bmu status
	reg_get(&Tbatt, can__BmTbatt);											// Get Battery temperature
	reg_get(&SOC, can__BmSOC);												// Get Battery state of charge

	/* Build TPDO 1 */
	PI_WRITE(PIACC_PDO, P201201_UMEAS, &meas->Meas.U, 4);					// Write measured voltage to process image
	PI_WRITE(PIACC_PDO, P201202_IMEAS, &meas->Meas.I, 4);					// Write measured current to process image

	/* Build TPDO 2 */
	//PI_WRITE(PIACC_PDO, P201203_PMEAS, &meas->Meas.P, 4);					// Write measured power to process image
	//PI_WRITE(PIACC_PDO, P201204_STATUS, &meas->Status.Sum, 4);			// Write status field to process image

	/* Build TPDO 3 */
	u16temp = (Uint16)(CID % 10000);										// Only use lower 4 digits of CID (0-9999)
	PI_WRITE(PIACC_PDO, P201501_CID, &u16temp, 2);							// Write CID to process image
	PI_WRITE(PIACC_PDO, P201502_CHALGSTATUS, &ChalgStatus, 2);				// Write ChalgStatus to process image
	u8temp = (Uint8)(ReguStatusSum.Detail.Info.Detail.limit & 0x0F);		// Mask out ReguStatus (lowest 4 bits from limit)
	PI_WRITE(PIACC_PDO, P201503_REGUSTATUS, &u8temp, 1);					// Write ReguStatus (limit) to process image
	u8temp = (Uint8)(ChargedAhP & 0xFF);									// Convert to Uint8 for process image
	PI_WRITE(PIACC_PDO, P201504_CHARGEDAHP, &u8temp, 1);					// Write chargedAhP to process image
	u8temp = getChalgErrorCode(ChalgError);									// Get ChalgError code for process image
	PI_WRITE(PIACC_PDO, P201505_CHALGERROR, &u8temp, 1);					// Write ChalgError to process image
	u8temp = getReguErrorCode(ReguStatusSum.Detail.Error);					// Get ReguError code for process image
	PI_WRITE(PIACC_PDO, P201506_REGUERROR, &u8temp, 1);						// Write ReguError to process image

	/* Build TPDO 4 */
	u16temp = (Uint16)(BID % 10000);										// Only use lower 4 digits of BID (0-9999)
	PI_WRITE(PIACC_PDO, P201601_BID, &u16temp, 2);							// Write BID to process image
	PI_WRITE(PIACC_PDO, P201602_BMUSTATUS, &BmuStatus, 2);					// Write BmuStatus to process image
	PI_WRITE(PIACC_PDO, P201603_TBATT, &Tbatt, 2);							// Write Tbatt to process image
	u8temp = (Uint8)(SOC & 0xFF);											// Convert to Uint8 for process image
	PI_WRITE(PIACC_PDO, P201604_SOC, &u8temp, 1);							// Write SOC to process image
	u8temp = getBmuErrorCode(ChalgError);									// Get BmuError code for process image
	PI_WRITE(PIACC_PDO, P201605_BMUERROR, &u8temp, 1);						// Write SOC to process image

	MCO_TriggerTPDO(1);														// Measurements
	//MCO_TriggerTPDO(2);														// Measurements
	MCO_TriggerTPDO(3);														// Charger status
	if(ChalgStatus & CHALG_STAT_BM_CONNTECTED){
		MCO_TriggerTPDO(4);													// Battery status. Send if Bmu connected
	}
}

static Uint8 getChalgErrorCode(Uint16 ChalgError){
	Uint8 code = 0;

	if(ChalgError & CHALG_ERR_LOW_BATTERY_VOL){
		code = 1;
	}
	else if(ChalgError & CHALG_ERR_HIGH_BATTERY_VOL){
		code = 2;
	}
	else if(ChalgError & CHALG_ERR_CHARGE_TIME_LIMIT){
		code = 3;
	}
	else if(ChalgError & CHALG_ERR_CHARGE_AH_LIMIT){
		code = 4;
	}
	else if(ChalgError & CHALG_ERR_INCORRECT_ALGORITHM){
		code = 5;
	}
	else if(ChalgError & CHALG_ERR_HIGH_BATTERY_LIMIT){
		code = 7;
	}
	else if(ChalgError & CHALG_ERR_BATTERY_ERROR){
		code = 8;
	}
	else if(ChalgError & CHALG_ERR_LOW_AIRPUMP_PRESSURE){
		code = 9;
	}
	else if(ChalgError & CHALG_ERR_HIGH_AIRPUMP_PRESSURE){
		code = 10;
	}
	else if(ChalgError & CHALG_ERR_INCORRECT_MOUNT){
		code = 6;
	}
	return code;
}

static Uint8 getReguErrorCode(ChargerError_type ReguError){
	Uint8 code = 0;

	if(ReguError.Detail.Phase){
		code = 17;
	}
	else if(ReguError.Detail.Regulator){
		code = 18;
	}
	else if(ReguError.Detail.LowChargerTemperature){
		code = 19;
	}
	else if(ReguError.Detail.HighChargerTemperature){
		code = 20;
	}
	else if(ReguError.Detail.LowBoardTemperature){
		code = 21;
	}
	else if(ReguError.Detail.HighBoardTemperature){
		code = 22;
	}
	else if(ReguError.Detail.HighTrafoTemperature){
		code = 23;
	}
	else if(ReguError.Detail.Watchdog){
		code = 24;
	}
	return code;
}

static Uint8 getBmuErrorCode(Uint16 ChalgError){
	Uint8 code = 0;

	if(ChalgError & CHALG_ERR_BM_TEMP){
		code = 101;
	}
	else if(ChalgError & CHALG_ERR_BM_ACID){
		code = 102;
	}
	else if(ChalgError & CHALG_ERR_BM_BALANCE){
		code = 103;
	}
	else if(ChalgError & CHALG_ERR_BM_LOW_SOC){
		code = 105;
	}
	return code;
}
