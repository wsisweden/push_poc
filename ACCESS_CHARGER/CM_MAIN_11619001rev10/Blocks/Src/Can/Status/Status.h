#ifndef CAN_STATUS_H
#define CAN_STATUS_H

#include "../MicroCANopenPlus/MCO_DS401_LPC1768/types.h"
#include "cai_cc.h"

//#define CAN_COMMUNICATION_WATCHDOG_TIMEOUT 2 // in (s)

void StatusInit();

void SyncStatus(const ChargerMeas_type* meas);

#endif
