#ifndef CONFIG_PARAMAETERS_H
#define CONFIG_PARAMAETERS_H

#include <stdint.h>

void ConfigParametersReceive(uint8_t subIndex);
void ConfigParametersBattDisconnected(void);

#endif
