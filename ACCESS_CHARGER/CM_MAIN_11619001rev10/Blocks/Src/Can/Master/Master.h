#ifndef CAN_MASTER_H
#define CAN_MASTER_H

#include "inttypes.h"
#include "../MicroCANopenPlus/MCO_DS401_LPC1768/types.h"
#include "../MicroCANopenPlus/MCO_DS401_LPC1768/nodecfg.h"
#include "../MicroCANopenPlus/MCO/mco.h"
#include "cai_cc.h"
#define CAN_COMMUNICATION_WATCHDOG_TIMEOUT 2 // in (s)

void MasterInit();

int MasterReceiveRaw(CAN_MSG MEM_FAR *pRPDO);
void battConnect(int NodeId, ChargerStatusFields_type status);
/* Sync internally called from CAN stack and then sync sent */

/* Should be called then tdpo is set and it transmit all datar from the master. */
void SendMaster(int SendSum);

void SendUactInterval();

void MasterSendPreOperational(uint8_t NodeId);
void MasterSendOperational(uint8_t NodeId);
void masterBmuInitSuccess();
void masterStartBmuCalibration();
void masterBmuCalibrationAck();
#endif
