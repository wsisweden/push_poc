#ifndef CAN_SCAN_NETWORK_H
#define CAN_SCAN_NETWORK_H

#include "inttypes.h"

void CAN_ScanNetwork();

void CAN_ScanNetworkReceiveSerialNr(uint8_t NodeId, uint32_t SerialNr);

#endif
