#ifndef CAN_IO_MODULE_H
#define CAN_IO_MODULE_H

#include "MicroCANopenPlus/MCO_DS401_LPC1768/types.h"
#include "MicroCANopenPlus/MCO_DS401_LPC1768/nodecfg.h"

#define CAN_COMMUNICATION_WATCHDOG_TIMEOUT 2 // in (s)

/* Initialize RPDOs */
void IoModuleInit();

int IoModuleReceiveRaw(CAN_MSG MEM_FAR *pRPDO);

/* Send data to PDOs to IO module */
void SendIoModule();

/* Should be called once each second */
void WatchdogIoModule();

#endif
