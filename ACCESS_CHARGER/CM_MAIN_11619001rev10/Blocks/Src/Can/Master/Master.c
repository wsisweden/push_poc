#include "global.h"
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"

#include "Master/Master.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/mcop.h"
#include "MicroCANopenPlus/MCO/lss.h"

#include "Can.h"
#include "cai_cc.h"
#include "NodesStatusAndMapping.h"
#include "../Cc/ClusterControl.h"
#include "cc.h"
#include "mpdo.h"
#include "../Regu/ControlLoop/SendToBattery.h"
#include "sup.h"
#include "chalg.h"

typedef union {
	float real;
	UNSIGNED32 u32;
} FloatUint32_union;

static int mpdoSamReceive(CAN_MSG MEM_FAR *pRPDO);

static CAN_MSG Nmt = { 0, { { 0x01, 0x7F, 0, 0, 0, 0, 0, 0 } }, 2 }; 		// Network management message

int NodeIdSlave = 0;
int bmuInitFromSlave = 0;

void MasterInit(){
	int n;

	for(n = 1; n < 128; n++){
		MCOHW_SetCANFilter(0x180 + n);										// so that we will receive answer
		MCOHW_SetCANFilter(0x280 + n);										// so that we will receive answer
	}
}

void MasterSendPreOperational(uint8_t NodeId){
	Nmt.BUF.BUF[0] = 0x80;													// Set set to pre operational
	Nmt.BUF.BUF[1] = NodeId;												// Set node ID

	MCOHW_PushMessage(&Nmt);												// Send message
}

void MasterSendOperational(uint8_t NodeId){
	Nmt.BUF.BUF[0] = 0x01;													// Set to operational
	Nmt.BUF.BUF[1] = NodeId;												// Set node ID

	MCOHW_PushMessage(&Nmt);												// Send message
}

void SendMaster(int SendSum)
{
	CAN_MSG PDO;														 	// PDO message
	Uint32 SerialNrThis;

	reg_get(&SerialNrThis, can__SerialNo);

	PDO.LEN = 8;
	int n;
	for(n = 0; n < CC_CHARGERS_LEN/*+1*/; n++){
		volatile const ChargerSet_type* Set;
		int NodeId;

		/*if(n < CC_CHARGERS_LEN)*/{
			const uint32_t SerialNr = CcSet(n, &Set);
			NodeId = CAN_NodeStatusAndMapping_GetNodeId(SerialNr);

			if(!SerialNr || SerialNr == SerialNrThis){
				continue;
			}
		}
/*		else{
			extern volatile ChargerSet_type CanToCc;
			extern MCO_CONFIG MEM_FAR gMCOConfig;

			NodeId = MY_NODE_ID;
			Set = &CanToCc;
		}*/
		{

			if(NodeId){
				FloatUint32_union value;

				value.real = Set->Set.U;
				PDO.BUF.BUF32[0] = value.u32;

				value.real = Set->Set.I;
				PDO.BUF.BUF32[1] = value.u32;

				PDO.ID = 0x200 + NodeId;
				MCOHW_PushMessage(&PDO);

				value.real = Set->Set.P;
				PDO.BUF.BUF32[0] = value.u32;

				PDO.BUF.BUF[4] = Set->Mode;
				PDO.BUF.BUF[5] = Set->Uact.both;
				PDO.BUF.BUF[6] = Set->RemoteIn.both;
				extern MCO_CONFIG MEM_FAR gMCOConfig;
				PDO.BUF.BUF[7] = MY_NODE_ID;

				PDO.ID = 0x300 + NodeId;
				MCOHW_PushMessage(&PDO);
			}
		}
	}
}

void SendUactInterval()
{
	extern MCO_CONFIG MEM_FAR gMCOConfig;
	CAN_MSG PDO;														 	// PDO message

	PDO.LEN = 8;

	PDO.ID = 0x400 + MY_NODE_ID;
	reg_get(&PDO.BUF.BUF32[0], canRegu_ReguOffUactLow);
	reg_get(&PDO.BUF.BUF32[1], canRegu_ReguOffUactHigh);
	MCOHW_PushMessage(&PDO);

	PDO.ID = 0x500 + MY_NODE_ID;
	reg_get(&PDO.BUF.BUF32[0], canRegu_ReguOnUactLow);
	reg_get(&PDO.BUF.BUF32[1], canRegu_ReguOnUactHigh);
	MCOHW_PushMessage(&PDO);
}

int MasterReceiveRaw(CAN_MSG MEM_FAR *pRPDO){
	const int NodeId = pRPDO->ID & 0x7F;
	const uint32_t SerialNr = CAN_NodeStatusAndMapping_GetSerialNr(NodeId);
	int processed = 0;
	Uint32 SerialNrThis;

	reg_get(&SerialNrThis, can__SerialNo);
	if(!SerialNr || SerialNr == SerialNrThis){
		;
	}
	else{
		switch(pRPDO->ID & ~(0x7F)){
		case 0x180 :
			{
				const FloatUint32_union U = {.u32 = pRPDO->BUF.BUF32[0]};
				const FloatUint32_union I = {.u32 = pRPDO->BUF.BUF32[1]};

				CcMeasUI(SerialNr, U.real, I.real);
			}
			processed = 1;
			break;
		case 0x280 :
			{
				const FloatUint32_union P = {.u32 = pRPDO->BUF.BUF32[0]};
				const ChargerStatusFields_type Status = {.Sum = LSS_GetDword(&pRPDO->BUF.BUF[4])};

				CcMeasPStatus(SerialNr, P.real, Status);
				if(CcConnectedOk(SerialNr)){
					battConnect(NodeId, Status);
				}
			}
			processed = 1;
			break;
		case 0x580 :
			processed = mpdoSamReceive(pRPDO);
			break;
		}
	}

	return processed;
}

//static void battConnect(int NodeId, ChargerStatusFields_type status){
void battConnect(int NodeId, ChargerStatusFields_type status){
	extern volatile batteryCommunication_type dataToBattery;
	int noLoad;
	{
		extern volatile ChargerMeas_type CcToCan;							// Values from cluster control, there are several conditions to calculate if battery connected

		noLoad = CcToCan.Status.Detail.Derate.Detail.NoLoad;
	}
	static int noLoadOld = 0;
	static int sent = 0;
	Uint8 SupState;

	reg_get(&SupState, can__supervisionState);

	if(sent){																// Already sent ?

		if(noLoad && !noLoadOld){											// CcToCan updated since last ?
			bmuInitFromSlave = 0;
			sent = 0;
			dataToBattery.PanId = 0;
			dataToBattery.Channel = 0;
			dataToBattery.NodeId = 0;
			dataToBattery.Valid = 0;
		}
	}
	else if(!status.Detail.Derate.Detail.NoLoad){									// Load connected ?{
		int startBattComm;
		Uint16 PanId;
		Uint16 NodeIdRadio;
		Uint8 Channel;
		Uint8 ChargingMode;
		reg_get(&PanId, can__PanId);
		reg_get(&NodeIdRadio, can__NodeId);
		reg_get(&Channel, can__Channel);
		reg_get(&ChargingMode, can__ChargingMode);

		if(ChargingMode == CHALG_BM){
			startBattComm = 1;
		}
		else if(ChargingMode == CHALG_DUAL || ChargingMode == CHALG_MULTI){
			startBattComm = 4;
		}
		else{
			startBattComm = 0;
		}

		if(NodeId){
			if(SupState == SUP_STATE_INIT_BM){								// Wait until SUP signals to start battery communication
				bmuInitFromSlave = 1;										// Slave will send data to battery
				sent = 1;													// Send once only to one node only at each battery connect
				uint8_t bytes[4];

				bytes[0] = 0x10;
				bytes[1] = (Uint8)(PanId >> 8);
				bytes[2] = (Uint8)(PanId);
				bytes[3] = Channel;

				mpdoDamSend(NodeId, 0x2010, 0x04, bytes);

				bytes[0] = (Uint8)(NodeIdRadio >> 8);
				bytes[1] = (Uint8)(NodeIdRadio);
				bytes[2] = (Uint8)(startBattComm);
				bytes[3] = 0;

				mpdoDamSend(NodeId, 0x2010, 0x05, bytes);

				// Store nodeId of slave for later use
				NodeIdSlave = NodeId;
			}
		}
		else{
			bmuInitFromSlave = 0;									// Master will send data itself
			sent = 1;
			dataToBattery.PanId = PanId;
			dataToBattery.Channel = Channel;
			dataToBattery.NodeId = NodeIdRadio;
			dataToBattery.Valid = startBattComm;

		}
	}
	noLoadOld = noLoad;
}

static int mpdoSamReceive(CAN_MSG MEM_FAR *pRPDO){
	int retval = 0;

	//if(pRPDO->ID == 0x580 && !(pRPDO->BUF.BUF[0] & (unsigned)0x80)){				// MPDO and SAM ?
	if(((pRPDO->ID & ~(0x7F)) == 0x580) && !(pRPDO->BUF.BUF[0] & (unsigned)0x80)){	// MPDO and SAM ? Master and slave
		const int NodeId  = pRPDO->BUF.BUF[0] & 0x7F;
		extern MCO_CONFIG MEM_FAR gMCOConfig;

		if(pRPDO->LEN == 8){														// Correct length ?
			const uint16_t idx = (pRPDO->BUF.BUF[2] << 8) + pRPDO->BUF.BUF[1];
			const uint8_t subIndex = pRPDO->BUF.BUF[3];

			if(idx == 0x2012 && subIndex == 0x04){									// Slave send status field ?
				const uint32_t SerialNr = CAN_NodeStatusAndMapping_GetSerialNr(NodeId);

				if(CcConnectedOk(SerialNr)){										// Charger selected to be used and OK ?
					const ChargerStatusFields_type Status = {.Sum = LSS_GetDword(&pRPDO->BUF.BUF[4])};

					battConnect(NodeId, Status);
				}
				retval = 1;
			}
		}
		else{
			MCOP_PushEMCY(0x8210, 0, 0 ,0 ,0, 0);									// 0x8210 = PDO not processed due to length error
		}
	}

	return retval;
}

void masterBmuInitSuccess(){
	uint8_t bytes[4];

	bytes[0] = 0x22;
	bytes[1] = 0;
	bytes[2] = 0;
	bytes[3] = 0;

	mpdoDamSend(NodeIdSlave, 0x2010, 0x05, bytes);
}

void masterStartBmuCalibration(){
	if(bmuInitFromSlave)
	{
		uint8_t bytes[4];

		bytes[0] = 0x30;
		bytes[1] = 0;
		bytes[2] = 0;
		bytes[3] = 0;

		mpdoDamSend(NodeIdSlave, 0x2010, 0x05, bytes);
	}
}

void masterBmuCalibrationAck(){
	uint8_t bytes[4];

	bytes[0] = 0x3F;
	bytes[1] = 0;
	bytes[2] = 0;
	bytes[3] = 0;

	mpdoDamSend(NodeIdSlave, 0x2010, 0x05, bytes);
}
