#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/mcop.h"

#include "master.h"
#include "global.h"
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "init.h"
#include "Can.h"

static int watchdogIoModule = 0;

void IoModuleInit(){
	watchdogIoModule = 0;
}

int IoModuleReceiveRaw(CAN_MSG MEM_FAR *pRPDO){
	if(pRPDO->ID == 0x1FF){
		watchdogIoModule = CAN_COMMUNICATION_WATCHDOG_TIMEOUT;

		reg_put(&pRPDO->BUF.BUF16[0], can_IoModuleADI1);
		reg_put(&pRPDO->BUF.BUF16[1], can_IoModuleADI2);
		reg_put(&pRPDO->BUF.BUF16[2], can_IoModuleADI3);
		reg_put(&pRPDO->BUF.BUF16[3], can_IoModuleADI4);

		return 1;
	}
	return 0;
}

void WatchdogIoModule()
{
	if(watchdogIoModule){
		watchdogIoModule--;
	}
}

void SendIoModule(){
	if(!watchdogIoModule){
		MasterSendOperational(0x7F);
	}

	{
		CAN_MSG PDO;// = { 0x27F, { { 0, 0x7F, 0, 0, 0, 0, 0, 0 } }, 8 };
		Uint16 value;

		PDO.ID = 0x27F;
		PDO.LEN = 8;

		reg_get(&value, can_IoModuleDO1);				// Spare 1
		PDO.BUF.BUF16[0] = value;

		reg_get(&value, can_IoModuleDO2);				// Spare 2
		PDO.BUF.BUF16[1] = value;

		reg_get(&value, can_IoModuleDO3);				// Air
		PDO.BUF.BUF16[2] = value;

		reg_get(&value, can_IoModuleDO4);				// Water
		PDO.BUF.BUF16[3] = value;

		MCOHW_PushMessage(&PDO);
	}
}
