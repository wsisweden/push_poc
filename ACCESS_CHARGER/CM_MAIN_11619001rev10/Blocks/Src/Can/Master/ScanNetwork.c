#include "Master/ScanNetwork.h"

#include "MicroCANopenPlus/MCO/mco.h"
#include "MicroCANopenPlus/MCO/mcohw.h"
#include "MicroCANopenPlus/MCO/mcop.h"
#include "MicroCANopenPlus/MCO/lss.h"

#include "SetNodeId.h"
#include "NodesStatusAndMapping.h"
#include "can.h"
#include "master.h"
#include "cc.h"

static void uploadSerialNumber(int NodeId);

static int CheckId = 0;															// Read of identity object for this node is currently ongoing
static uint32_t CheckNodeIdSerialNr = 0;										// Indicate if answered received

void CAN_ScanNetwork(){
	static int LimitSerialRequestTimer = 0;

	{																			// Listen for heart beats
		static int idS = 0;
		int id = idS;
		int n;
		extern volatile UNSIGNED8 Heartbeats[128];								// Heart beat status is written in this variable by interrupt then received

		for(n = 0; n < 16; n++){												// For all possible node IDs
			const int heartbeat = Heartbeats[id];
			if(heartbeat){														// Heart beat received ?
				Heartbeats[id] = 0;												// Set to heart beat not received
				if(CAN_NodeStatusAndMapping_AddNode(id)){						// Node unknown ?
					uploadSerialNumber(id);
					LimitSerialRequestTimer = CAN_LIMIT_SERIAL_REQUEST_LIMIT;	// Limit number of requests
					break;														// Do not send more messages this time
				}
				CAN_NodeStatusAndMapping_Heartbeat(id, heartbeat);				// Store heart beat
				if(heartbeat == 0x7F){
					const uint32_t SerialNr = CAN_NodeStatusAndMapping_GetSerialNr(id);

					if(CC_ChargerEnabled(SerialNr)){
						MasterSendOperational(id);
					}
					break;														// Do not send more messages this time
				}
			}
			if(id < 127){
				id++;
			}
			else{
				id = 0;
			}
		}
		idS = id;
	}

	if(LimitSerialRequestTimer){												// Limit number of serial requests ?
		LimitSerialRequestTimer--;												// Decrease timer
	}
	else {																		// Periodically check information about the nodes in the table
		LimitSerialRequestTimer = CAN_LIMIT_SERIAL_REQUEST_LIMIT;				// Limit number of request per second

		if(!CheckNodeIdSerialNr){
			CAN_NodeStatusAndMapping_RemoveNode(CheckId);
		}
		CheckNodeIdSerialNr = 0;
		CheckId = CAN_NodeStatusAndMapping_GetNext();							// Set to next node id table

		extern MCO_CONFIG MEM_FAR gMCOConfig;
		if(CheckId){
			if(CheckId == MY_NODE_ID){
				CheckNodeIdSerialNr = 1;
			}
			else{
				uploadSerialNumber(CheckId);
			}
		}
	}
}

void CAN_ScanNetworkReceiveSerialNr(uint8_t NodeId, uint32_t SerialNr){
	CheckNodeIdSerialNr = SerialNr;
	CAN_NodeStatusAndMapping_SetSerialNumber(NodeId, SerialNr);					// Add serial number, already known ?
}

void MCOUSER_SDOResponse(COBID_TYPE ID, UNSIGNED8 Len, UNSIGNED8 *pDat) {
	/* Used for bus scan */
	SetNodeIdSdoResponse(ID, Len, pDat);

	/* Used for software cloning in bootloaderUpdload */
	//CAN_bootloaderUpload(ID, Len, pDat);

	/* Check for serial for answer for serial number upload */
	if (pDat[0] == 0x43 && pDat[1] == 0x18 && pDat[2] == 0x10 && pDat[3] == 0x04) // correct: answer code, index, subindex
	{
		const int receivedNodeId = ID & 0x7F;
		const Uint32 SerialNr = LSS_GetDword(&pDat[4]);

		CAN_ScanNetworkReceiveSerialNr(receivedNodeId, SerialNr);
	}
}

static void uploadSerialNumber(int NodeId){
	CAN_MSG CAN_MSG_UploadSerialNumber = {0, {{0x40, 0x18, 0x10, 0x04, 0, 0, 0, 0}}, 8}; // COB-ID = 600 + NodeId

	CAN_MSG_UploadSerialNumber.ID = 0x600 + NodeId;									// Ask node for information
	MCOHW_SetCANFilter(0x580 + NodeId);												// so that we will receive answer
	MCOHW_PushMessage(&CAN_MSG_UploadSerialNumber);									// Send message
}
