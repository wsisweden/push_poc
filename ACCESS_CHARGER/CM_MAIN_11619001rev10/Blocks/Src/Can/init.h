/* 07-10-08 TJK
 ********************************************************************* tabs:[5 9]
 *
 *	F I L E   D E S C R I P T I O N
 *
 *	[ ]Lib FB main source file		[x]Lib FB source file
 *	[ ]Lib FB target source file	[ ]Project source file
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\file		Can/init.h
 *
 *	\ingroup	CAN
 *
 *	\brief
 *
 *	\details
 *
 *	\note
 *
 *	\version	\$Rev: 406 $ \n
 *				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
 *				\$Author: tlarsu $
 *
 *******************************************************************************/

#ifndef CAN_INIT_H_INCLUDED
#define CAN_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

/*
 #define name@lwc_reset		dummy_reset
 #define name@lwc_down		dummy_down
 #define name@lwc_read		dummy_read
 #define name@lwc_write		dummy_write
 #define name@lwc_ctrl		dummy_ctrl
 #define name@lwc_test		dummy_test
 */

#include "global.h"

typedef struct {
	Uint8 nDummy;
} can_Init;

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_CAN
#elif defined(EXE_GEN_CAN)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_CAN
#endif

#define EXE_APPL(n)			n##can

/********************************************************************/EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN /* Non-volatile numeric	*/
//#define EXE_USE_NS /* Non-volatile string	*/
#define EXE_USE_VN /* Volatile numeric		*/
//#define EXE_USE_VS /* Volatile string		*/
//#define EXE_USE_PN /* Process-point numeric	*/
//#define EXE_USE_PS /* Process-point string	*/
//#define EXE_USE_BL /* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name			Flags				Dim	Type	Def	Min	Max
 ---------------	-------------------	---	-------	---	---	----------*/
EXE_REG_VN( tpdoSet,		SYS_REG_DEFAULT,	1,	Uint8,	0,	0,		0xFF )          // /   message is received.
EXE_REG_VN( IoModuleDO1,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// \  .                                        in [0, 0x7FFF]
EXE_REG_VN( IoModuleDO2,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// 	\ IO module outputs are written then       in [0, 0x7FFF]
EXE_REG_VN( IoModuleDO3,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// 	/ sync message is sent                     in [0, 0x7FFF]
EXE_REG_VN( IoModuleDO4,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// /  .                                        in [0, 0x7FFF]
EXE_REG_VN( IoModuleADI1,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// \  .                                        in [0, 0x7FFF]
EXE_REG_VN( IoModuleADI2,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// 	\ IO module inputs are assumed to          in [0, 0x7FFF]
EXE_REG_VN( IoModuleADI3,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// 	/ sent by IO module once each second       in [0, 0x7FFF]
EXE_REG_VN( IoModuleADI4,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,		0x7FFFUL )		// /  .                                        in [0, 0x7FFF]
EXE_REG_VN( CANtraffic, 	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,		0xFF )

EXE_REG_NN( CAN_CommProfile, SYS_REG_DEFAULT,	1, 	Uint8,	0,	0,		4 )
EXE_REG_VN( State,			SYS_REG_DEFAULT,	1,	Uint8,	0,	0,		5 )				// Used to display current state
EXE_REG_VN( NodeId,			SYS_REG_DEFAULT,	1,	Uint8,	0,	0,		127 )			// Used to display node ID
EXE_REG_NN( NodeIdFixed,	SYS_REG_DEFAULT,	1,	Uint8,	1,	1,		127 )			// Use then node ID is set to fixed
EXE_REG_NN( CanBps,			SYS_REG_DEFAULT,	1,	Uint8,	2,	0,		6 )				// Use to configure CAN baudrate

EXE_REG_VN( CanReguOffUactLow,	SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) // \  .
EXE_REG_VN( CanReguOffUactHigh,	SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) //  \ .
EXE_REG_VN( CanReguOnUactLow,	SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) //  / Regulator on off interval.
EXE_REG_VN( CanReguOnUactHigh,	SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) // /  .

EXE_REG_VN( CanInput,		 SYS_REG_DEFAULT,	1, 	Uint8,	0,	0,		1 )				// Can input trigger
EXE_REG_NN( CanInputMode,	SYS_REG_DEFAULT,	1, 	Uint8,	0,	0,		1 )				// Can input mode trigger
EXE_REG_VN( CanInputModeSet,	SYS_REG_DEFAULT,	1, 	Uint8,	0,	0,		1 )			// Can input mode for application use

/*******************************************************************************
 * HMI attributes
 */
EXE_MMI_NONE( tpdoSet )

EXE_MMI_NONE( IoModuleDO1 )
EXE_MMI_NONE( IoModuleDO2 )
EXE_MMI_NONE( IoModuleDO3 )
EXE_MMI_NONE( IoModuleDO4 )
EXE_MMI_NONE( IoModuleADI1 )
EXE_MMI_NONE( IoModuleADI2 )
EXE_MMI_NONE( IoModuleADI3 )
EXE_MMI_NONE( IoModuleADI4 )
EXE_MMI_NONE( CANtraffic )

EXE_MMI_ENUM( CAN_CommProfile,		SYS_MMI_U_NONE, tCommProfile,	SYS_MMI_RW,		tCommProfileEnum)
EXE_MMI_ENUM( State,				SYS_MMI_U_NONE, tState, 		SYS_MMI_READ,	tStateEnum)
EXE_MMI_INT( NodeId,				SYS_MMI_U_NONE, tNodeIdAuto,	SYS_MMI_READ, 	SYS_Q_NONE)
EXE_MMI_INT( NodeIdFixed,			SYS_MMI_U_NONE, tNodeIdSet,		SYS_MMI_RW, 	SYS_Q_NONE)
EXE_MMI_ENUM( CanBps,				SYS_MMI_U_NONE, tCanBps,		SYS_MMI_RW,		tCanBpsEnum)

EXE_MMI_NONE(CanReguOffUactLow)
EXE_MMI_NONE(CanReguOffUactHigh)
EXE_MMI_NONE(CanReguOnUactLow)
EXE_MMI_NONE(CanReguOnUactHigh)

EXE_MMI_NONE( CanInput )
EXE_MMI_NONE( CanInputMode )
EXE_MMI_NONE( CanInputModeSet )

/******************************************************************************* 
 * read/write indications
 * (sorted alphabetically by instance name and by declaration order)
 */
EXE_IND_ME( tpdoSet,				tpdoSet )
EXE_IND_ME( CAN_CommProfile,		CAN_CommProfile )
EXE_IND_ME( NodeIdFixed,			NodeIdFixed )
EXE_IND_ME( CanBps,					CanBps )
EXE_IND_ME( CanInputModeSet,		CanInputModeSet )

/******************************************************************************* 
 * Language-dependent texts 
 *  - tExampleTxtHandle1 is reserved handlename and it's discarded by 
 *		textparser tools.
 *  - you can add txthandle comment at the end of the texthandle line, and this
 *		is parsed by textparser tools.
 */

EXE_TXT( tCommProfileEnum,	EXE_T_EN("Disabled\nMaster\nSlave\nN/A\nStatus") EXE_T_ES("Inactivo\nPrincipal\nEsclavo\nN/A\nEstado") EXE_T_EN_US("Disabled\nMaster\nSlave\nN/A\nStatus") EXE_T_PT("Desabilitado\nPrincipal\nAuxiliar\nN/A\nStatus") EXE_T_DE("Deaktiviert\nMaster\nSlave\nN/A\nStatus") EXE_T_JP("") EXE_T_SE("Av\nMaster\nSlave\nN/A\nStatus") EXE_T_IT("Disattivato\nMaster\nSlave\nN/D\nStatus") EXE_T_FI("") "") /*Menu:Service->CAN->Function (21-1-tCommProfile)*/
EXE_TXT( tCommProfile, EXE_T_EN("Function") EXE_T_ES("Funci\363n") EXE_T_EN_US("Function") EXE_T_PT("Fun\347\343o") EXE_T_DE("Funktion") EXE_T_JP("") EXE_T_FI("") EXE_T_SE("Funktion") EXE_T_IT("Funzione") "") /*Menu:Service->CAN->Function (21-1-tCommProfileEnum)*/
EXE_TXT( tStateEnum,		EXE_T_EN("Boot\nAuto ID\nStop\nOperational\nPre-Op\nUnknown") EXE_T_ES("Arranque\nAuto\nParo\nOperacion\341l\nPre-Op\nDesconocido") EXE_T_EN_US("Boot\nAuto ID\nStop\nOperational\nPre-Op\nUnknown") EXE_T_PT("Partida\nAuto ID\nParar\nOperacional\nPre-op.\nDesconhecido") EXE_T_DE("Boot\nAuto-ID\nStop\nOperational\nPre-Op\nUnbekannt") EXE_T_JP("") EXE_T_SE("Start\nAuto ID\nStop\nOperational\nPre-Op\nOk\344nd") EXE_T_IT("Boot\nAuto ID\nStop\nOperativo\nPre-Op\nSconosciuto") EXE_T_FI("") "") /*Menu:Service->CAN->State enum (21-1-tState)*/
EXE_TXT( tNodeIdAuto, EXE_T_EN("Node id\050auto\051") EXE_T_ES("ID nodo\050auto\051") EXE_T_EN_US("Node id\050auto\051") EXE_T_PT("ID unid.\050auto\051 ") EXE_T_DE("Knoten-ID\050auto\051") EXE_T_JP("") EXE_T_SE("Node id\050auto\051") EXE_T_IT("Node id\050auto\051") EXE_T_FI("") "") /*Menu:Service->CAN->Node id (17)*/
EXE_TXT( tNodeIdSet, EXE_T_EN("Node id\050set\051") EXE_T_ES("ID nodo\050set\051") EXE_T_EN_US("Node id\050set\051") EXE_T_PT("ID unid.\050set\051") EXE_T_DE("Knoten-ID\050Set\051") EXE_T_JP("") EXE_T_SE("Node id\050set\051") EXE_T_IT("Node id\050set\051") EXE_T_FI("") "") /*Menu:Service->CAN->Node id (17)*/
EXE_TXT( tState, EXE_T_EN("State") EXE_T_ES("Estado") EXE_T_EN_US("State") EXE_T_PT("Estado") EXE_T_DE("Status") EXE_T_JP("") EXE_T_FI("") EXE_T_SE("Tillst\345nd") EXE_T_IT("Status") "") /*Menu:Service->CAN->State (21-1-tStateEnum)*/
EXE_TXT( tCanBpsEnum,	EXE_T_EN("20\n50\n125\n250\n500\n800\n1000") EXE_T_ES("20\n50\n125\n250\n500\n800\n1000") EXE_T_EN_US("20\n50\n125\n250\n500\n800\n1000") EXE_T_PT("20\n50\n125\n250\n500\n800\n1000") EXE_T_DE("20\n50\n125\n250\n500\n800\n1000")	EXE_T_JP("") EXE_T_SE("20\n50\n125\n250\n500\n800\n1000") EXE_T_IT("20\n50\n125\n250\n500\n800\n1000") EXE_T_FI("") "") /*Menu:Service->CAN->Baudrate (21-1-tCanBps)*/
EXE_TXT( tCanBps, EXE_T_EN("Bitrate\050kbps\051") EXE_T_ES("Tasa de bits\050kbps\051") EXE_T_EN_US("Bitrate\050kbps\051") EXE_T_PT("Taxa de bits\050kbps\051") EXE_T_DE("Bitrate\050kbps\051") EXE_T_JP("") EXE_T_FI("") EXE_T_SE("Bitrate\050kbps\051") EXE_T_IT("Bitrate\050kbps\051") "") /*Menu:Service->CAN->Baudrate (21-1-tCanBpsEnum)*/

/**********************************************************************/EXE_END
EXE_REG_EX( can__SerialNo,				SerialNo,		sup1 )
EXE_REG_EX( can__supervisionState,		supervisionState,	sup1 )
EXE_REG_EX( can_CommProfile,			CAN_CommProfile,can1 )
EXE_REG_EX( can_State,					State,			can1 )
EXE_REG_EX( can_NodeId,					NodeId,			can1 )
EXE_REG_EX( can_NodeIdFixed,			NodeIdFixed,	can1 )
EXE_REG_EX( can_CanBps,					CanBps,			can1 )
EXE_REG_EX( can_ParallelControl,		ParallelControl_func,	cc1)
EXE_REG_EX( can_IoModuleDO1,			IoModuleDO1,	can1 )
EXE_REG_EX( can_IoModuleDO2,			IoModuleDO2,	can1 )
EXE_REG_EX( can_IoModuleDO3,			IoModuleDO3,	can1 )
EXE_REG_EX( can_IoModuleDO4,			IoModuleDO4,	can1 )
EXE_REG_EX( can_IoModuleADI1,			IoModuleADI1,	can1 )
EXE_REG_EX( can_IoModuleADI2,			IoModuleADI2,	can1 )
EXE_REG_EX( can_IoModuleADI3,			IoModuleADI3,	can1 )
EXE_REG_EX( can_IoModuleADI4,			IoModuleADI4,	can1 )
EXE_REG_EX( can_Input,					CanInput,		can1 )
EXE_REG_EX( can_InputMode,				CanInputMode,	can1 )
EXE_REG_EX( can_InputModeSet,			CanInputModeSet,	can1 )
EXE_REG_EX( canRegu_ReguOffUactLow,		CanReguOffUactLow,	can1 )
EXE_REG_EX( canRegu_ReguOffUactHigh,	CanReguOffUactHigh,	can1 )
EXE_REG_EX( canRegu_ReguOnUactLow,		CanReguOnUactLow,	can1 )
EXE_REG_EX( canRegu_ReguOnUactHigh,		CanReguOnUactHigh,	can1 )
EXE_REG_EX( can__CcSync,				Sync,			cc1 )
EXE_REG_EX( canRegu_Uset,				Uset,			regu1 )
EXE_REG_EX( canRegu_Iset,				Iset,			regu1 )
EXE_REG_EX( canRegu_Pset,				Pset,			regu1 )
//EXE_REG_EX( canRegu_RemoteInMode,		RemoteInMode,	regu1 )
EXE_REG_EX( canRegu_UactMode,			UactMode,		regu1 )
EXE_REG_EX( canRegu_ReguMode,			ReguMode,		regu1 )
EXE_REG_EX( canRegu_NewSet,				NewSet,			regu1 )
EXE_REG_EX( can__PowerGroup_func,		PowerGroup_func,	regu1 )
EXE_REG_EX( can__Blinkstate,			Blinkstate,		tui1 )
EXE_REG_EX( can__PanId,					PanId,			radio1 )
EXE_REG_EX( can__Channel,				Channel,		radio1 )
EXE_REG_EX( can__NodeId,				NodeId,			radio1 )
EXE_REG_EX( can__ChargingMode,			currChargingMode,	chalg1 )

EXE_REG_EX( can__CID,					ChargerId,		radio1 )		// Uint32
EXE_REG_EX( can__ChalgStatus,			ChalgStatus,	chalg1 )		// Uint16
EXE_REG_EX( can__ReguStatus,			StatusP,		regu1 )			// Uint32 Regu status and error
EXE_REG_EX( can__chargedAhP,			chargedAhPercent,	chalg1 )	// Uint16
EXE_REG_EX( can__ChalgError,			ChalgError,		chalg1 )		// Uint16

EXE_REG_EX( can__BID,					BID,			chalg1 )		// Uint32
EXE_REG_EX( can__BmSatus,				BmStatus,		radio1 )		// Uint16
EXE_REG_EX( can__BmTbatt,				BmTbatt,		radio1 )		// Uint16
EXE_REG_EX( can__BmSOC,					BmSOC,			radio1 )		// Uint16
#undef EXE_APPL
#endif
#undef EXE_INST
