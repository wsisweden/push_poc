#include "NodesStatusAndMapping.h"
#include "MicroCANopenPlus/MCO/mco.h"

static struct NodeInformation_type NodesStatus[NODES_STATUS_LEN] = { [0 ... NODES_STATUS_LEN - 1]= {0, 0, 0, 0, 0, 0}};

static int NodeId2Pos(UNSIGNED8 NodeId);
static void clear(struct NodeInformation_type* NodeInformation);

static void clear(struct NodeInformation_type* NodeInformation){
	NodeInformation->heartbeatStatus = 0;
	NodeInformation->NodeId = 0;
	NodeInformation->ProductCode = 0;
	NodeInformation->RevisionNr = 0;
	NodeInformation->SerialNr = 0;
	NodeInformation->VendorId = 0;
}

int CAN_NodeStatusAndMapping_AddNode(UNSIGNED8 NodeId){
	if(NodeId2Pos(NodeId) < 0){
		int n;

		for(n = 0; n < NODES_STATUS_LEN; n++)					// For all table positions
		{
			if(!NodesStatus[n].NodeId){							// Position free
				NodesStatus[n].NodeId = NodeId;					// Use position
				return 1;										// Return non zero
			}
		}
	}

	return 0;
}

static int NodeId2Pos(UNSIGNED8 NodeId)
{
	if(NodeId){
		int n;
		for(n = 0; n < NODES_STATUS_LEN; n++)
		{
			if(NodeId == NodesStatus[n].NodeId)
				return n;
		}
	}

	return -1;
}

void CAN_NodeStatusAndMapping_SetVendorId(UNSIGNED8 NodeId, UNSIGNED32 VendorId){
}

void CAN_NodeStatusAndMapping_SetProductCode(UNSIGNED8 NodeId, UNSIGNED32 ProductCode){
}

void CAN_NodeStatusAndMapping_SetRevisionNr(UNSIGNED8 NodeId, UNSIGNED32 RevisionNr){
}

void CAN_NodeStatusAndMapping_SetSerialNumber(UNSIGNED8 NodeId, UNSIGNED32 SerialNr)
{
	const int pos = NodeId2Pos(NodeId);

	if(pos >= 0){
		NodesStatus[pos].NodeId = NodeId;
		NodesStatus[pos].SerialNr = SerialNr;
	}
}

void CAN_NodeStatusAndMapping_RemoveNode(UNSIGNED8 NodeId){
	const int pos = NodeId2Pos(NodeId);

	if(pos >= 0){
		clear(&NodesStatus[pos]);
	}
}

void CAN_NodeStatusAndMapping_Reset(){
	{
		int n;
		for(n = 0; n < NODES_STATUS_LEN; n++){
			clear(&NodesStatus[n]);
		}
	}
}

int CAN_NodeStatusAndMapping_GetNext(){
	static int pos = 0;											// Position in table

	int n;
	for(n = 0; n < NODES_STATUS_LEN; n++){						// Limit the search to number of rows information table
		pos++;													// Try next position
		if(pos >= NODES_STATUS_LEN)								// Above array limit ?
			pos = 0;											// Start over from the beginning

		extern MCO_CONFIG MEM_FAR gMCOConfig;
		if(NodesStatus[pos].NodeId && NodesStatus[pos].NodeId != MY_NODE_ID){
			return NodesStatus[pos].NodeId;						// Node ID found
		}
	}

	return 0;													// Did not find node ID
}

UNSIGNED32 CAN_NodeStatusAndMapping_GetSerialNr(UNSIGNED8 NodeId){
	const int pos = NodeId2Pos(NodeId);

	if(pos < 0){
		return 0;
	}
	else{
		return NodesStatus[pos].SerialNr;
	}
}

UNSIGNED8 CAN_NodeStatusAndMapping_GetNodeId(UNSIGNED32 SerialNr){
	if(SerialNr){
		int n;
		for(n = 0; n < NODES_STATUS_LEN; n++)
		{
			if(SerialNr == NodesStatus[n].SerialNr)
				return NodesStatus[n].NodeId;
		}
	}

	return 0;
}

void CAN_NodeStatusAndMapping_Heartbeat(UNSIGNED8 NodeId, UNSIGNED8 Heartbeat){
	const int pos = NodeId2Pos(NodeId);

	if(pos >= 0){
		NodesStatus[pos].heartbeatStatus = Heartbeat;
	}
}

volatile struct NodeInformation_type* CAN_NodeStatusAndMapping_GetNodes(){
	return NodesStatus;
}
