#ifndef MPDO_H
#define MPDO_H

#include <stdint.h>
#include "MicroCANopenPlus/MCO/mco.h"

typedef enum {mpdoSAM_enum, mpdoDAM_enum} mpdo_enum;

void mpdoSamSend(int idx, int subIndex, int offset);							// Send a source address MPDO
void mpdoDamSend(uint8_t NodeId, int idx, int subIndex, uint8_t data[4]);		// Send a destination address MPDO
int mpdoDamReceive(CAN_MSG MEM_FAR *pRPDO);										// Check if the received message is a destination address PDO and in such case write to process image

#endif
