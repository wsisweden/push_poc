/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Fan.c
*
*	\ingroup	FAN
*
*	\brief		Main source file of the fan control.
*
*	\details
*
*	\note
*
*	\version	\$Rev: $ \n
*				\$Date: $ \n
*				\$Author: $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	FAN		FAN
*
*	\brief		Fan control.
*
********************************************************************************
*
*	\details	This function block controls the fan by reading the current 
*				temperature and setting an appropriate PWM duty cycle.
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "deb.h"
#include "swtimer.h"

#include "fan.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 *	\name	Task trigger reasons
 *	
 *	\brief	Trigger reason flags.
 */
/*@{*/
#define FAN__TRG_TIMER		(1<<0)		/**< Timer has expired.				*/
#define FAN__TRG_ON			(1<<1)		/**< Turn fan on.					*/
#define FAN__TRG_OFF		(1<<2)		/**< Turn fan off.					*/
/*@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type used for task trigger reason flags.
 */

typedef Uint8				fan__Triggers;

/**
 * FAN function block instance type.
 */
 
struct fan__inst {						/*''''''''''''''''''''''''''''' RAM	*/
	osa_CoTaskType			coTask;		/**< The main task.					*/
	swtimer_Timer_st		timer;		/**< Regulation timer.				*/
	fan_Init const_P * 		pInit;		/**< Initialization data.			*/
	Uint32					currDutyCycle;/**< Current duty cycle.			*/
	volatile fan__Triggers	triggers;	/**< Task triggers.					*/
	Uint8					delayCntr;	/**< Delay counter.					*/
	Uint8					delayIdx;	/**< Next delay array index.		*/
	Boolean					fanOn;		/**< True if fan is turned on.		*/
	volatile Boolean		fanOnPend;	/**< True if fan on is pending.		*/
	Uint32					delayedCycle[FAN_DELAY_COUNT];/**< Delayed duty cycle.			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void fan__coTask(osa_CoTaskType *);
PRIVATE void fan__timerExpired(swtimer_Timer_st *);
PRIVATE void fan__regulate(fan_Inst *);
PRIVATE void fan__resetRegulation(fan_Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/
extern int gTestMode;
/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Declaring a variable containing the file name so that it only has to be
 * stored once even if it is used several times in this file.
 */

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fan_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		pInit	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC fan_Inst * fan_init(
	fan_Init const_P *		pInit
) {
	fan_Inst *				pInst;

	pInst = (fan_Inst *) mem_reserve(
		&mem_normal,
		sizeof(fan_Inst) + sizeof(Uint32) * (pInit->delay / 2)
	);
	deb_assert(pInst != NULL);

	osa_newCoTask(
		&pInst->coTask,
		"fan__coTask",
		OSA_PRIORITY_NORMAL,
		fan__coTask,
		1024	/* TODO: Check the stack usage. */
	);

	swtimer_new(&pInst->timer, &fan__timerExpired, 1000, 1000);
	pInst->timer.pUtil = pInst;

	/* 
	 * Set initial values for instance variables.
	 */

	pInst->pInit = pInit;
	pInst->triggers = 0;

	fan__resetRegulation(pInst);

	if (pInit->options & FAN_OPT_STARTON)
	{
		pInst->fanOn = TRUE;
		pInst->fanOnPend = TRUE;
		swtimer_start(&pInst->timer);
	}
	else
	{
		pInst->fanOnPend = FALSE;
		pInst->fanOn = FALSE;
	}
	
	return pInst;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fan_on
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Turn on the fan.
*
*	\param		pInst	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void fan_on(
	fan_Inst *				pInst
) {
	Boolean					trigTask;

	trigTask = FALSE;	
		
	atomic(
		if not(pInst->fanOnPend)
		{
			pInst->fanOnPend = TRUE;
			pInst->triggers |= FAN__TRG_ON;
			trigTask = TRUE;
		}
	);

	if (trigTask)
	{
		osa_coTaskRun(&pInst->coTask);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fan_off
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Turn off the fan.
*
*	\param		pInst	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void fan_off(
	fan_Inst *				pInst
) {
	Boolean					trigTask;

	trigTask = FALSE;

	atomic(
		if (pInst->fanOnPend)
		{
			pInst->fanOnPend = FALSE;
			pInst->triggers |= FAN__TRG_OFF;
			trigTask = TRUE;
		}
	);

	if (trigTask)
	{
		osa_coTaskRun(&pInst->coTask);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fan__coTask
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void fan__coTask(
	osa_CoTaskType * 		pCoTask
)  {
	fan_Inst *				pInst;
	fan__Triggers			triggers;

	/* Waiting for REG to be operational */
	if(osa_syncPeek(OSA__SYNC_REG))
	{
		/*
		 *	The CoTask is the first element of the fan_Inst structure. They have
		 *	the same address.
		 */

		pInst = (fan_Inst *) pCoTask;

		/*
		 *	Get the triggering reason.
		 */

		atomic(
			triggers = pInst->triggers;
			pInst->triggers = 0;
		);

		if(gTestMode != TRUE)
		{
			if (pInst->fanOn == TRUE)
			{
				if (triggers & FAN__TRG_TIMER)
				{
					fan__regulate(pInst);
				}

				if (triggers & FAN__TRG_OFF)
				{
					swtimer_stop(&pInst->timer);
					pInst->fanOn = FALSE;
					pInst->pInit->pNewDutyFn(0);
					fan__resetRegulation(pInst);
				}
			}

			if (pInst->fanOn == FALSE)
			{
				if (triggers & FAN__TRG_ON)
				{
					fan__regulate(pInst);
					pInst->fanOn = TRUE;
					swtimer_start(&pInst->timer);
				}
			}
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fan__timerExpired
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Regulation timer expired.
*
*	\param		pCoTask 	Pointer to the coTask object
*
*	\details	Start coTask to regulate fan.
*
*	\note
*
*******************************************************************************/

PRIVATE void fan__timerExpired(
	swtimer_Timer_st * 		pTimer
) {
	fan_Inst *				pInst;

	pInst = (fan_Inst *) pTimer->pUtil;

	atomic(
		pInst->triggers |= FAN__TRG_TIMER;
	);

	osa_coTaskRun(&pInst->coTask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fan__regulate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculates a new duty cycle for the fan.
*
*	\param		pInst 	Pointer to function block instance data.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void fan__regulate(
	fan_Inst *				pInst
) {
	reg_Status				retVal;
	Int16					temperature;
	Uint32					newDutyCycle;
	static int 				startUpTimer = 3;

	newDutyCycle = pInst->pInit->maxCycle;

	if(startUpTimer)
	{
		startUpTimer--;
	}
	else
	{
		retVal = reg_get_i16(&temperature, pInst->pInit->temp_h);

		if (retVal == REG_OK)
		{
			Boolean					minCycle;
			static Boolean			minCycleOld = TRUE;

			/*
			 *	Calculate a new duty cycle.
			 */

			if (temperature < pInst->pInit->tMin)
			{
				newDutyCycle = pInst->pInit->minCycle;
				minCycle = TRUE;
			}
			else if (temperature >= pInst->pInit->tMax)
			{
				newDutyCycle = pInst->pInit->maxCycle;
				minCycle = FALSE;
			}
			else
			{
				Uint32 cycleDiff;
				Uint32 tempDiff;

				cycleDiff = pInst->pInit->maxCycle - pInst->pInit->minCycle;
				tempDiff = pInst->pInit->tMax - pInst->pInit->tMin;

				newDutyCycle =
					(temperature - pInst->pInit->tMin)
					* cycleDiff
					/ tempDiff
					+ pInst->pInit->minCycle;

				minCycle = FALSE;
			}

			/*
			 *	Stepping forward the delay array index each second. Store
			 *	the new calculated duty cycle for delayed speed reduction.
			 *	Init all array positions with newDutyCycle when temperature
			 *	rises above tMin to secure delay in all cases.
			 */
			if((minCycle == FALSE) && (minCycleOld == TRUE))
			{
				Ufast8 ii;

				ii = pInst->pInit->delay - 1;
				do
				{
					pInst->delayedCycle[ii] = newDutyCycle;
				}
				while (ii--);
			}
			else
			{
				pInst->delayedCycle[pInst->delayIdx] = newDutyCycle;
				if (++pInst->delayIdx >= pInst->pInit->delay)
				{
					pInst->delayIdx = 0;
				}
			}
			/*
			 *	Reduction of the fan speed is performed with a delay. Use the
			 *	highest duty cycle from the delay windows if speed is decreasing.
			 *
			 *	The code has to loop through all measurements from the window but
			 *	it shouldn't bee that time consuming.
			 */

			if (newDutyCycle < pInst->currDutyCycle)
			{
				Ufast8 ii;

				ii = pInst->pInit->delay - 1;
				do
				{
					newDutyCycle = MAX(newDutyCycle, pInst->delayedCycle[ii]);
				}
				while (ii--);
			}

			if(FAN_ENGINE_ON == FALSE)
			{
				if(temperature < pInst->pInit->tMax)
				{
					newDutyCycle = 0;
				}
			}
			minCycleOld = minCycle;
		}
	}
	pInst->currDutyCycle = newDutyCycle;
	newDutyCycle = pInst->pInit->maxCycle - newDutyCycle;	// Invert pwm output
	pInst->pInit->pNewDutyFn(newDutyCycle);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fan__resetRegulation
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Clear all regulation variables.
*
*	\param		pInst 	Pointer to function block instance data.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void fan__resetRegulation(
	fan_Inst *				pInst
) {
	Ufast8					ii;

	pInst->delayCntr = 0;
	pInst->delayIdx = 0;
	pInst->currDutyCycle = 0;

	ii = pInst->pInit->delay - 1;
	do
	{
		pInst->delayedCycle[ii] = 0;
	}
	while (ii-- != 0);
}
