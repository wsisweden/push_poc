/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		isr.C
*
*	\ingroup	MEAS
*
*	\brief		Contains the code that needs to be executed at 1ms intervalls.
*
*	\details
*
*	\note
*
*	\version	**-**-2009 / Ari Suomi
*
*******************************************************************************/

#include "../Src/Regu/ControlLoop/sendToBattery.h"
#include "local.h"
#include "measIsr.h"

/*	
 *  This code is to be included inside the 1ms ISR
 */
inline void meas_isr(void)
{
	meas__Inst *		meas_pInst;
	Uint16				meas_mainsVal;
	Uint16				meas_IAdVal;
	Uint16				meas_UAdVal;
	Uint16				meas_ThAdVal;
	Uint16				meas_TbAdVal;
	Uint16				meas_IoBoardAdVal;

	meas_pInst = meas__pInst;

	if (meas_pInst->flags & MEAS__FLG_RUNNING)
	{
		const Engine_type* const engineParameters = engineGetParameters();

		meas__readAdc(meas_pInst, &meas_UAdVal, &meas_IAdVal, &meas_ThAdVal, &meas_TbAdVal, &meas_mainsVal, &meas_IoBoardAdVal);

		meas_pInst->Uregusum[0] += (Int32)meas_UAdVal;
		meas_pInst->Iregusum[0] += (Int32)meas_IAdVal;
		Int32 pAdVal = (meas_UAdVal + (Int32)engineParameters->ScalefactorNom.NormToBit.UadOffset)*(meas_IAdVal + (Int32)engineParameters->ScalefactorNom.NormToBit.IadOffset);

		pAdVal = (meas_UAdVal + (Int32)engineParameters->ScalefactorNom.NormToBit.UadOffset)*(meas_IAdVal + (Int32)engineParameters->ScalefactorNom.NormToBit.IadOffset);
		meas_pInst->Pregusum[0] += pAdVal;
		meas_pInst->ThSum[0] += meas_ThAdVal;
		meas_pInst->TbSum[0] += meas_TbAdVal;
		meas_pInst->IoBoardSum[0] += meas_IoBoardAdVal;
		meas_pInst->IreguVals[meas_pInst->msCounter1][0] = meas_IAdVal + (Int32)engineParameters->ScalefactorNom.NormToBit.IadOffset;

		meas_pInst->Usum1sCanSynced += (Int32)meas_UAdVal;
		meas_pInst->Isum1sCanSynced += (Int32)meas_IAdVal;
		meas_pInst->Psum1sCanSynced += pAdVal >> 8;
		meas_pInst->sum1sCounter++;

		/*
		 * Filter mains measurement.
		 */

		if (meas_mainsVal < MEAS__ADVALUE_10V)
		{
			/*
			 * Mains is below 10V (AD value 3150 should be about 10V)
			 */

			meas_pInst->DSamples[MEAS__DIGI_MAINS] <<= 1;

			if ((meas_pInst->DSamples[MEAS__DIGI_MAINS] & 0x03) == 0x00 && meas_pInst->DCurrState[MEAS__DIGI_MAINS] != 0)
			{
				/*
				 * Two consecutive samples was below 10V.
				 */

				meas_pInst->DCurrState[MEAS__DIGI_MAINS] = 0;
				meas_pInst->triggers |= MEAS__TRG_MAINS;
				osa_semaSetIsr(&meas_pInst->taskSema);
			}
		}
		else if (meas_mainsVal > MEAS__ADVALUE_11V)
		{
			/*
			 * Mains is above 11V (AD value 3465 should be about 11V)
			 */

			meas_pInst->DSamples[MEAS__DIGI_MAINS] <<= 1;
			meas_pInst->DSamples[MEAS__DIGI_MAINS] |= 1;

			if ((meas_pInst->DSamples[MEAS__DIGI_MAINS] & 0x03) == 0x03 && meas_pInst->DCurrState[MEAS__DIGI_MAINS] != 1)
			{
				/*
				 * Two consecutive samples was above 11V.
				 */

				meas_pInst->DCurrState[MEAS__DIGI_MAINS] = 1;
				meas_pInst->triggers |= MEAS__TRG_MAINS;
				osa_semaSetIsr(&meas_pInst->taskSema);
			}
		}

		/*
		 * Filter remote input measurement.
		 * 5 consecutive measurements needed change of state.
		 */
		{
			Uint8					oldState;
			Uint8					ii;

			for (ii = MEAS__DIGI_REMOTEINA; ii <= MEAS__DIGI_REMOTEINB; ii++)
			{
				oldState = meas_pInst->DCurrState[ii];

				meas_pInst->DSamples[ii] <<= 1;
				meas_pInst->DSamples[ii] |= meas__hwGetDigiState(meas_pInst, ii);

				if((meas_pInst->DSamples[ii] & 0x1F) == 0)
				{
					/*
					 * The needed amount of consecutive values have been the same.
					 */
					meas_pInst->DCurrState[ii] = 0;
				}
				else if((meas_pInst->DSamples[ii] & 0x1F) == 0x1F)
				{
					/*
					 * The needed amount of consecutive values have been the same.
					 */
					meas_pInst->DCurrState[ii] = 1;
				}

				if(oldState != meas_pInst->DCurrState[ii])
				{
					if(ii == MEAS__DIGI_REMOTEINA){
						meas_pInst->triggers |= MEAS__TRG_REMOTEINA;
					}
					else{
						meas_pInst->triggers |= MEAS__TRG_REMOTEINB;
					}
					osa_semaSetIsr(&meas_pInst->taskSema);
				}
			}
		}

		/*
		 * Check if 10ms has elapsed
		 */
		sendToBattery1ms();

		if (meas_pInst->msCounter1 < 9)
		{
		    meas_pInst->msCounter1++;
		}
		else
		{
			/*
			 * 10ms has elapsed.
			 */

			meas_pInst->Uregusum[1] = meas_pInst->Uregusum[0];
			meas_pInst->Iregusum[1] = meas_pInst->Iregusum[0];
			meas_pInst->Pregusum[1] = meas_pInst->Pregusum[0];
			meas_pInst->TbSum[1] += meas_pInst->TbSum[0];
			meas_pInst->IoBoardSum[1] = meas_pInst->IoBoardSum[0];
			meas_pInst->ThSum[1] += meas_pInst->ThSum[0];
			meas_pInst->lastTbSum = meas_pInst->TbSum[0];
			meas_pInst->lastThSum = meas_pInst->ThSum[0];
			meas_pInst->IreguVals[0][1] = meas_pInst->IreguVals[0][0];
			meas_pInst->IreguVals[1][1] = meas_pInst->IreguVals[1][0];
			meas_pInst->IreguVals[2][1] = meas_pInst->IreguVals[2][0];
			meas_pInst->IreguVals[3][1] = meas_pInst->IreguVals[3][0];
			meas_pInst->IreguVals[4][1] = meas_pInst->IreguVals[4][0];
			meas_pInst->IreguVals[5][1] = meas_pInst->IreguVals[5][0];
			meas_pInst->IreguVals[6][1] = meas_pInst->IreguVals[6][0];
			meas_pInst->IreguVals[7][1] = meas_pInst->IreguVals[7][0];
			meas_pInst->IreguVals[8][1] = meas_pInst->IreguVals[8][0];
			meas_pInst->IreguVals[9][1] = meas_pInst->IreguVals[9][0];
			meas_pInst->Uregusum[0] = 0;
			meas_pInst->Iregusum[0] = 0;
			meas_pInst->Pregusum[0] = 0;
			meas_pInst->TbSum[0] = 0;
			meas_pInst->IoBoardSum[0] = 0;
			meas_pInst->ThSum[0] = 0;
			meas_pInst->IreguVals[0][0] = 0;
			meas_pInst->IreguVals[1][0] = 0;
			meas_pInst->IreguVals[2][0] = 0;
			meas_pInst->IreguVals[3][0] = 0;
			meas_pInst->IreguVals[4][0] = 0;
			meas_pInst->IreguVals[5][0] = 0;
			meas_pInst->IreguVals[6][0] = 0;
			meas_pInst->IreguVals[7][0] = 0;
			meas_pInst->IreguVals[8][0] = 0;
			meas_pInst->IreguVals[9][0] = 0;

			meas_pInst->triggers |= MEAS__TRG_NEWVALUES;
			osa_semaSetIsr(&meas_pInst->taskSema);

			meas_pInst->msCounter1 = 0;
		}
	}
}
