/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		measTiltSimu.c
*
*	\ingroup	MEAS
*
*	\brief		Orientation/Motion Detection Sensor hardware simulation.
*
*	\details	
*
*	\note
*
*	\version	16-02-2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "mem.h"
#include "iicmstr.h"
#include "protif.h"

#include "../local.h"
#include "../measTilt.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__tiltSimuProc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the request.
*
*	\param		pRequest	Pointer to the PROTIF request.
*
*	\return		
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC Uint8 meas__tiltSimuProc(
	protif_Request *		pRequest
) {
	Uint8					retCode = PROTIF_OK;

	if (
		pRequest->data.pData[0] == MEAS__TILTREG_TILT &&
		pRequest->data.flags & PROTIF_FLAG_WRITE
	) {
		if (pRequest->data.pNext->flags & PROTIF_FLAG_READ)
		{
			/*
			 * Reading TILT register
			 */

			pRequest->data.pNext->pData[0] = MEAS__TILTPOLA_VNORM << 2;
		}
	}
	else if (
		pRequest->data.pData[0] == MEAS__TILTREG_XOUT &&
		pRequest->data.flags & PROTIF_FLAG_WRITE
	) {
		if (pRequest->data.pNext->flags & PROTIF_FLAG_READ)
		{
			/*
			 * Reading XOUT register
			 */

			pRequest->data.pNext->pData[0] = 42;
		}
	}
	else if (
		pRequest->data.pData[0] == MEAS__TILTREG_YOUT &&
		pRequest->data.flags & PROTIF_FLAG_WRITE
	) {
		if (pRequest->data.pNext->flags & PROTIF_FLAG_READ)
		{
			/*
			 * Reading YOUT register
			 */

			pRequest->data.pNext->pData[0] = 0;
		}
	}
	else if (
		pRequest->data.pData[0] == MEAS__TILTREG_ZOUT &&
		pRequest->data.flags & PROTIF_FLAG_WRITE
	) {
		if (pRequest->data.pNext->flags & PROTIF_FLAG_READ)
		{
			/*
			 * Reading ZOUT register
			 */

			pRequest->data.pNext->pData[0] = 0;
		}
	}

	return(retCode);
}
