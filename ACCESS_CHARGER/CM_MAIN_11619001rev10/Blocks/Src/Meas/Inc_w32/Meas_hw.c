/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_w32/Meas_hw.c
*
*	\ingroup	MEAS_W32
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MEAS_W32	MEAS for Windows
*
*	\ingroup	MEAS
*
*	\brief		Windows specific code for Measurement handler.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <Windows.h>

#include "tools.h"
#include "osa.h"

#include "../local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint16		meas__uAdValue = 0;
PRIVATE Uint16		meas__iAdValue = 0;
PRIVATE Uint16		meas__thAdValue = 0;
PRIVATE Uint16		meas__tbAdValue = 0;
PRIVATE Uint16		meas__digiStates = 0;
PRIVATE Uint16		meas__mainsAdValue = 0;

PRIVATE CRITICAL_SECTION meas__critical;
PRIVATE Boolean meas__criticalInit = FALSE;

/* End of declaration module **************************************************/


PUBLIC void meas__hwInit(
	meas__Inst *				pInst
) {
	if (meas__criticalInit == FALSE)
	{
		InitializeCriticalSection(&meas__critical);
		meas__criticalInit = TRUE;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__hwGetDigiState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the state of a digital input.
*
*	\return		The state of the digital input (1 or 0).
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint8 meas__hwGetDigiState(
	meas__Inst *			pInst,
	Uint8					inputNum
) {
	Uint8 retVal;

	EnterCriticalSection(&meas__critical);
	retVal = (meas__digiStates >> inputNum) & 0x01;
	LeaveCriticalSection(&meas__critical);

	if (retVal == 1)
	{
		retVal = 0;
	}
	else
	{
		retVal = 1;
	}

	return(retVal);
}

PUBLIC void meas__hwProcessKeys(
	meas__Inst *			pInst	
) {
	Uint32					portData;
	Uint32					ii;

	portData = meas__digiStates >> MEAS__DIGI_COUNT;

	for(ii=0; ii < PROJECT_KEY_COUNT; ii++) {
		pInst->KSamples[ii] <<= 1;
		pInst->KSamples[ii] |= (portData >> ii) & 0x01;

		if ((pInst->KSamples[ii] & 0x1F) == 0x1F) {
			pInst->currKeyStates |= (1 << ii);
		}
		else if ((pInst->KSamples[ii] & 0x1F) == 0x00) {
			pInst->currKeyStates &= ~(1 << ii);
		}
	}
}

PUBLIC void meas__readAdc(
	struct meas__inst *		pInst,
	Uint16 *				pUAdVal,
	Uint16 *				pIAdVal,
	Uint16 *				pThAdVal,
	Uint16 *				pTbAdVal,
	Uint16 *				pMainsVal
) {				
	EnterCriticalSection(&meas__critical);
	*pUAdVal = meas__uAdValue; /* 10;*/
	*pIAdVal = meas__iAdValue; /* 10;*/
	*pThAdVal = meas__thAdValue; /* 10;*/
	*pTbAdVal = meas__tbAdValue; /* 10;*/
	*pMainsVal = meas__mainsAdValue;
	LeaveCriticalSection(&meas__critical);

	//pInst->msCounter1++;
}

PUBLIC Uint16 meas__hwReadMains(
	void
) {
	Uint16 retVal;

	EnterCriticalSection(&meas__critical);
	retVal = meas__mainsAdValue;
	LeaveCriticalSection(&meas__critical);

	return(retVal);
}


PUBLIC void meas__setUAdValue(
	Uint16					AdValue
) {
	if (meas__criticalInit == FALSE)
	{
		InitializeCriticalSection(&meas__critical);
		meas__criticalInit = TRUE;
	}
	EnterCriticalSection(&meas__critical);
	meas__uAdValue = AdValue;
	LeaveCriticalSection(&meas__critical);
}

PUBLIC void meas__setIAdValue(
	Uint16					AdValue
) {
	if (meas__criticalInit == FALSE)
	{
		InitializeCriticalSection(&meas__critical);
		meas__criticalInit = TRUE;
	}
	EnterCriticalSection(&meas__critical);
	meas__iAdValue = AdValue;
	LeaveCriticalSection(&meas__critical);
}

PUBLIC void meas__setThAdValue(
	Uint16					AdValue
) {
	if (meas__criticalInit == FALSE)
	{
		InitializeCriticalSection(&meas__critical);
		meas__criticalInit = TRUE;
	}
	EnterCriticalSection(&meas__critical);
	meas__thAdValue = AdValue;
	LeaveCriticalSection(&meas__critical);
}

PUBLIC void meas__setTbAdValue(
	Uint16					AdValue
) {
	if (meas__criticalInit == FALSE)
	{
		InitializeCriticalSection(&meas__critical);
		meas__criticalInit = TRUE;
	}
	EnterCriticalSection(&meas__critical);
	meas__tbAdValue = AdValue;
	LeaveCriticalSection(&meas__critical);
}

PUBLIC void meas__setMainsAdValue(
	Uint16					AdValue
) {
	if (meas__criticalInit == FALSE)
	{
		InitializeCriticalSection(&meas__critical);
		meas__criticalInit = TRUE;
	}
	EnterCriticalSection(&meas__critical);
	meas__mainsAdValue = AdValue;
	LeaveCriticalSection(&meas__critical);
}

PUBLIC void meas__updateDigiStates(
	Uint16					states
) {
	if (meas__criticalInit == FALSE)
	{
		InitializeCriticalSection(&meas__critical);
		meas__criticalInit = TRUE;
	}

	EnterCriticalSection(&meas__critical);
	meas__digiStates = states;
	LeaveCriticalSection(&meas__critical);
}
