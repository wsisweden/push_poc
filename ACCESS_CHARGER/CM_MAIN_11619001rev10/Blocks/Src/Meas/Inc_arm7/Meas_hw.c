/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_arm7/Meas_hw.c
*
*	\ingroup	MEAS_ARM7
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MEAS_ARM7	MEAS for NXP LPC2XXX
*
*	\ingroup	MEAS
*
*	\brief		NXP LPC2XXX specific code for Measurement handler.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"

#include "../local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Pin number map for HW pins.
 */

PRIVATE Uint8 const_P meas__hwPinMap[] = {
	PROJECT_HWP4_OVERTEMP,			/* MEAS__DIGI_OVERTEMP		*/
	PROJECT_HWP2_REMOTEINA,			/* MEAS__DIGI_REMOTEIN A	*/
	PROJECT_HWP2_REMOTEINB			/* MEAS__DIGI_REMOTEIN B	*/
	PROJECT_HWP1_CHALGTEST			/* MEAS__DIGI_CHALGTEST		*/
};						

/**
 * Port number map for HW pins.
 */

PRIVATE Uint8 const_P meas__hwPortMap[] = {
	4,								/* MEAS__DIGI_OVERTEMP		*/
	2,								/* MEAS__DIGI_REMOTEIN A	*/
	2,								/* MEAS__DIGI_REMOTEIN B	*/
	1								/* MEAS__DIGI_CHALGTEST		*/
};

/**
 * Pin number map for keypad pins. Must be in the same order as the meas__keys
 * enum.
 */

PRIVATE Uint8 const_P meas__hwKeyPinMap[] = {
	PROJECT_HWP1_STOP,						/* Key, STOP, DI					*/
	PROJECT_HWP1_F1,						/* Key, F1, DI						*/
	PROJECT_HWP1_F2,						/* Key, F2, DI						*/
	PROJECT_HWP1_ESC,						/* Key, ESC, DI						*/
	PROJECT_HWP1_OK,						/* Key, OK, DI						*/
	PROJECT_HWP1_LEFT,						/* Key, Left, DI					*/
	PROJECT_HWP1_RIGHT,						/* Key, Right, DI					*/
	PROJECT_HWP1_UP,						/* Key, UP, DI						*/
	PROJECT_HWP1_DOWN						/* Key, Down, DI					*/
};

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize MEAS hardware.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void meas__hwInit(
	meas__Inst *				pInst
) {

	/*
	 * Connect power to the ADC.
	 */

	PCONP |= (1 << 12);

	/*
	 * Connect analog input pins to ADC.
	 */

	PINSEL1 |= (1<<14) | (1<<16) | (1<<18) | (1<<20);
	PINSEL3 |= (1<< 28) | (1<<29);

	/* 
	 * Setup A/D: 12-bit A/D @ 4,5MHz "Burst"-Mode
	 */

	AD0CR = (0x1F) | ((8-1)<<8) | (1<<16) | (1<<21);

	/*
	 * Set direction of digital input pins. 0=input and 1=output.
	 */

	FIO1DIR &= ~(
		(1 << PROJECT_HWP1_STOP) |
		(1 << PROJECT_HWP1_F1) |
		(1 << PROJECT_HWP1_F2) |
		(1 << PROJECT_HWP1_ESC) |
		(1 << PROJECT_HWP1_LEFT) |
		(1 << PROJECT_HWP1_RIGHT) |
		(1 << PROJECT_HWP1_UP) |
		(1 << PROJECT_HWP1_DOWN) |
		(1 << PROJECT_HWP1_CHALGTEST)
	);
	
	FIO2DIR &= ~(
		(1 << PROJECT_HWP2_REMOTEINA) |
		(1 << PROJECT_HWP2_REMOTEINB)
	);

	FIO4DIR &= ~(1 << PROJECT_HWP4_OVERTEMP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__hwGetDigiState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the state of a digital input.
*
*	\return		The state of the digital input (1 or 0).
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint8 meas__hwGetDigiState(
	meas__Inst *			pInst,
	Uint8					inputNum
) {
	Uint8					portNum;
	Uint8					retVal = 0;

	portNum = meas__hwPortMap[inputNum];
	
	/* todo:	If remote IN is read then A or B input should be selected 
	 *			depending on the engine settings
	 */

	switch(portNum)
	{
		case 1:
			retVal = (FIO1PIN >> meas__hwPinMap[inputNum]) & 0x01;
			break;

		case 2:
			retVal = (FIO2PIN >> meas__hwPinMap[inputNum]) & 0x01;
			break;

		case 4:
			retVal = (FIO4PIN >> meas__hwPinMap[inputNum]) & 0x01;
			break;
	}

	return(retVal);
}

PUBLIC void meas__hwProcessKeys(
	meas__Inst *			pInst	
) {
	Uint32					portData;
	Uint32					ii;

	portData = FIO1PIN;
	
	for(ii=0; ii < MEAS__KEY_COUNT; ii++) {
		pInst->KSamples[ii] <<= 1;
		pInst->KSamples[ii] |= (portData >> meas__hwKeyPinMap[ii]) & 0x01;

		if ((pInst->KSamples[ii] & 0x1F) == 0x1F) {
			pInst->currKeyStates |= (1 << ii);
		}
		else if ((pInst->KSamples[ii] & 0x1F) == 0x00) {
			pInst->currKeyStates &= ~(1 << ii);
		}
	}
}

#if 0
PUBLIC void meas__hwProcessKeys2(
	meas__Inst *			pInst	
) {
	Uint32					portData;
	Uint32					ii;
	Uint32					val0;
	Uint32					mask;
	Uint32 *				pFilt;
	static Uint32			filterValue[5];
	static Uint32			currOutValue = 0;

	portData = FIO1PIN;

	pFilt = filterValue;
	val0 = pFilt[0]; /* Oldest value in the buffer */
	mask = 0;

	for (ii = 6; --ii; pFilt++) {

		Uint32 val1;

		mask |= val0 ^ (val1 = pFilt[1]);
		pFilt[0] = val0 = val1;
	}

	currOutValue = (currOutValue & mask) | (portData & ~mask);

}
#endif
