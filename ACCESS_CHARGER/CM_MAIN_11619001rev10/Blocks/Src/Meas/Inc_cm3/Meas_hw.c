/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/Meas_hw.c
*
*	\ingroup	MEAS_CM3
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MEAS_CM3	NXP LPC17XX
*
*	\ingroup	MEAS
*
*	\brief		NXP LPC17XX specific code for Measurement handler.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "Cm3_Arm7.h"
#include "engines_types.h"

#include "../local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * Pin numbers for digital inputs on port1.
 */

enum meas__port1Pins
{
	MEAS__HWP1_STOP = 0,				/**< Key, STOP, DI					*/
	MEAS__HWP1_F1 = 1,					/**< Key, F1, DI					*/
	MEAS__HWP1_F2 = 4,					/**< Key, F2, DI					*/
	MEAS__HWP1_ESC = 8,					/**< Key, ESC, DI					*/
	MEAS__HWP1_OK = 9,					/**< Key, OK, DI					*/
	MEAS__HWP1_LEFT = 10,				/**< Key, Left, DI					*/
	MEAS__HWP1_RIGHT = 14,				/**< Key, Right, DI					*/
	MEAS__HWP1_UP = 15,					/**< Key, UP, DI					*/
	MEAS__HWP1_DOWN = 16,				/**< Key, Down, DI					*/
	MEAS__HWP1_CHALGTEST = 25			/**< CHALG Test, DI					*/
};

/**
 * Pin numbers for digital inputs on port2.
 */

enum meas__port2Pins
{
	MEAS__HWP2_REMOTEINA = 2,			/**< Remote IN-A, DI				*/
	MEAS__HWP2_REMOTEINB = 3			/**< Remote IN-B, DI				*/
};

/**
 * Pin numbers for digital inputs on port4.
 */

enum meas__port4Pins
{
	MEAS__HWP4_OVERTEMP = 29			/**< Engine Overtemp, DI			*/
};

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Pin number map for HW pins.
 */

PRIVATE Uint8 const_P meas__hwPinMap[] = {
	MEAS__HWP4_OVERTEMP,			/* MEAS__DIGI_OVERTEMP		*/
	MEAS__HWP2_REMOTEINA,			/* MEAS__DIGI_REMOTEIN A	*/
	MEAS__HWP2_REMOTEINB,			/* MEAS__DIGI_REMOTEIN B	*/
	MEAS__HWP1_CHALGTEST			/* MEAS__DIGI_CHALGTEST		*/
};						

/**
 * Port number map for HW pins.
 */

PRIVATE Uint8 const_P meas__hwPortMap[] = {
	4,								/* MEAS__DIGI_OVERTEMP		*/
	2,								/* MEAS__DIGI_REMOTEIN A	*/
	2,								/* MEAS__DIGI_REMOTEIN B	*/
	1								/* MEAS__DIGI_CHALGTEST		*/
};

/**
 * Pin number map for keypad pins. Must be in the same order as the meas__keys
 * enum.
 */

PRIVATE Uint8 const_P meas__hwKeyPinMap[] = {
	MEAS__HWP1_STOP,					/* Key, STOP, DI					*/
	MEAS__HWP1_F1,						/* Key, F1, DI						*/
	MEAS__HWP1_F2,						/* Key, F2, DI						*/
	MEAS__HWP1_ESC,						/* Key, ESC, DI						*/
	MEAS__HWP1_OK,						/* Key, OK, DI						*/
	MEAS__HWP1_LEFT,					/* Key, Left, DI					*/
	MEAS__HWP1_RIGHT,					/* Key, Right, DI					*/
	MEAS__HWP1_UP,						/* Key, UP, DI						*/
	MEAS__HWP1_DOWN						/* Key, Down, DI					*/
};

/* End of declaration module **************************************************/


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize MEAS hardware.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void meas__hwInit(
	meas__Inst *				pInst
) {

	/*
	 * Connect power to the ADC.
	 */
	atomic(
		SC->PCONP |= (1 << 12);
	);

	/*
	 * Connect analog input pins to ADC.
	 */
	atomic(
		PINCON->PINSEL1 |= (1<<14) | (1<<16) | (1<<18) | (1<<20);
		PINCON->PINSEL3 |= (1<<31) | (1<<30) | (1<<28) | (1<<29);
	);

	/* 
	 * Setup A/D: 12-bit A/D @ 4,5MHz "Burst"-Mode
	 */
	atomic(
		AD0CR = (0x3F) | ((8-1)<<8) | (1<<16) | (1<<21);
	);

	/*
	 * Set direction of digital input pins. 0=input and 1=output.
	 */
	atomic(
		FIO1DIR &= ~(
			(1 << MEAS__HWP1_STOP) |
			(1 << MEAS__HWP1_F1) |
			(1 << MEAS__HWP1_F2) |
			(1 << MEAS__HWP1_ESC) |
			(1 << MEAS__HWP1_LEFT) |
			(1 << MEAS__HWP1_RIGHT) |
			(1 << MEAS__HWP1_UP) |
			(1 << MEAS__HWP1_DOWN) |
			(1 << MEAS__HWP1_CHALGTEST)
		);
	);

	atomic(
		FIO2DIR &= ~(
			(1 << MEAS__HWP2_REMOTEINA) |
			(1 << MEAS__HWP2_REMOTEINB)
		);
	);
	
	atomic(
		FIO4DIR &= ~(1 << MEAS__HWP4_OVERTEMP);
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__hwGetDigiState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the state of a digital input.
*
*	\return		The state of the digital input (1 or 0).
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint8 meas__hwGetDigiState(
	meas__Inst *			pInst,
	Uint8					inputNum
) {
	Uint8					portNum;
	Uint8					retVal = 0;

	portNum = meas__hwPortMap[inputNum];
	
	/* todo:	If remote IN is read then A or B input should be selected 
	 *			depending on the engine settings
	 */

	switch(portNum)
	{
		case 1:
			retVal = (FIO1PIN >> meas__hwPinMap[inputNum]) & 0x01;
			break;

		case 2:
			retVal = (FIO2PIN >> meas__hwPinMap[inputNum]) & 0x01;
			break;

		case 4:
			{
				const Engine_type *engineParameters;

				engineParameters = engineGetParameters();
				if(engineParameters->Ttrafo == TempSensorDigital_High)
				{
					retVal = (FIO4PIN >> meas__hwPinMap[inputNum]) & 0x01;
				}
				else
				{
					retVal = 0;
				}
			}
			break;
	}

	return(retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__hwProcessKeys
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read and filter key states.
*
*	\param		pInst	Pointer to the FB instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void meas__hwProcessKeys(
	meas__Inst *			pInst	
) {
	Uint32					portData;
	Uint32					ii;

	portData = FIO1PIN;
	
	for(ii=0; ii < PROJECT_KEY_COUNT; ii++)
	{
		pInst->KSamples[ii] <<= 1;
		pInst->KSamples[ii] |= (portData >> meas__hwKeyPinMap[ii]) & 0x01;

		if ((pInst->KSamples[ii] & 0x1F) == 0x1F)
		{
			pInst->currKeyStates &= ~(1 << ii);
		}
		else if ((pInst->KSamples[ii] & 0x1F) == 0x00)
		{
			pInst->currKeyStates |= (1 << ii);
		}
	}

	/*
	 * Make sure that no wrong bits are set.
	 */

	pInst->currKeyStates &= 0xFFFF >> (16 - PROJECT_KEY_COUNT);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__readMains
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read and filter key states.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 meas__hwReadMains(
	void
) {			
	Uint16					retVal;
	Uint32					adVal;

	/*
	 * The ADC probably does not need to be configured from scratch here but
	 * doing it anyway just to be sure its configured right.
	 */

	SC->PCONP |= (1 << 12);
	PINCON->PINSEL3 |= (1 << 28) | (1 << 29);

	AD0CR = (0x10) | /*((8-1) << 8) | */(1 << 21);

	/*
	 * Start the conversion and wait until it is done.
	 */

	AD0CR |= (1 << 24);

	do {
		adVal = AD0DR4;	
	} while not(adVal & (1<<31));

	AD0CR |= (1 << 24);

	do {
		adVal = AD0DR4;
	} while not(adVal & (1<<31));

	/*
	 * Conversion is done now. Read the value.
	 */

	retVal = (adVal >> 4) & 0x0FFF;

	AD0CR = ~(1 << 21);
	SC->PCONP &= ~(1 << 12);
	PINCON->PINSEL3 &= ~((1 << 28) | (1 << 29));
	return(retVal);
}
