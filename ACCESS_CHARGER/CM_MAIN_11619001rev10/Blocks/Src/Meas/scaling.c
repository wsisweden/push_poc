#include "tools.h"
#include "local.h"
#include "engines_types.h"
#include "math.h"
#include "pwm.h"

PRIVATE Int16 meas__scaleTemperatures(
	const Temperature_type*		Thermistor,
	Uint32						nSum
);
PRIVATE Int16 meas__scaleTemperaturesNtc(
	float						R_Thermistor_divided_R,
	const Temperature_type*		Thermistor
);
PRIVATE float meas__scaleTemperaturesR_Thermistor_divided_R_5V_Heatsink(
		Uint32						nSum
);
static Int16 Steinhart_Hart(
	float						R_Thermistor_divided_R,
	const Temperature_type*		Thermistor
);

/* Temperature board							  Type					A +			B*log(R_NTC/R) +	C*log(R_NTC/R)*log(R_NTC/R) +	D		Short circuit	Open circuit	Derate		Over temperature */
static const Temperature_type TempBoardScaling = {TempSensor_3_3V_Ntc,	0.003354,	0.000257,			0.000002,						NAN,	0.0067,			33.0,			70,			80};

/* Temperature battery (Access 30) 				Type					A +			B*log(R_NTC/R) +	C*log(R_NTC/R)*log(R_NTC/R) +	D		Short circuit	Open circuit	Derate		Over temperature */
static const Temperature_type TempBattScaling = {TempSensor_Robust_Batt,0.0,		0.0,				0.0,							NAN,	0.0067,			33.0,			70,			80}; /* Derate not used */

/**************************************************************************
 Name:		meas__calcScalefactor

 Does:		Calculate slope and offset with two corresponding ad-value
            and normalized value.

 Input:     Samples             -- Number of samples summed in ad value
            AdValue[2]          -- AdValue
            NormValue[2]        -- Normalized value (unit: mV or mA)

 Output:    Slope
            Offset

 Returns: 
**************************************************************************/
PUBLIC void meas__calcScalefactor(
	Uint32 *				pAdValue,
	Uint32 *				pNormValue,
	float *					pSlope,
	float *					pOffset
) {
	float					norm;
	Uint32					ad;

	norm = pNormValue[1] - pNormValue[0]; // (U_display - U0) in (mV)
	ad = (pAdValue[1] - pAdValue[0]); // (U_adc - U0_adc) in (bit/1000)

	norm = norm/ad; // in (mV/(bit/1000) = V/bit)

	*pSlope = norm;
	*pOffset = (float)pNormValue[1] - norm*pAdValue[1]; // in (mA - A/bit*bit/1000 = mA - mA = mA)
}

/**************************************************************************
  Does:		Convert from value read from ADC to volt. Offset should have
			been added before.

 Input:     Samples             -- Number of samples summed in ad value
            AdValue[2]          -- AdValue

 Returns:	Voltage in (V)
**************************************************************************/
PUBLIC float AdValueToVolt(
	Uint32					sampleCount,
	Int32 					AdValue
) {
	float retVal;
	const Engine_type* const engineParameters = engineGetParameters();

	retVal = AdValue * engineParameters->ScalefactorNom.BitToNorm.UadSlope / sampleCount;

	return retVal;
}

/**************************************************************************
  Does:		Convert from value read from ADC to ampere. Offset should have
			been added before.

 Input:     Samples             -- Number of samples summed in ad value
            AdValue[2]          -- AdValue

 Returns:	Current in (A)
**************************************************************************/
PUBLIC float AdValueToAmpere(
	Uint32					sampleCount,
	Int32 					AdValue
) {
	float retVal;
	const Engine_type* const engineParameters = engineGetParameters();

	retVal = AdValue * engineParameters->ScalefactorNom.BitToNorm.IadSlope / sampleCount;

	return retVal;
}

/**************************************************************************
  Does:		Convert from value read from ADC to volt. Offset should have
			been added before.

 Input:     Samples             -- Number of samples summed in ad value
            AdValue[2]          -- AdValue

 Returns:	Power in (W)
**************************************************************************/
PUBLIC float AdValueToPower(
	Uint32					sampleCount,
	Int32 					AdValue
) {
	float retVal;
	const Engine_type* const engineParameters = engineGetParameters();

	retVal = AdValue * engineParameters->ScalefactorNom.BitToNorm.UadSlope;
	retVal = retVal * engineParameters->ScalefactorNom.BitToNorm.IadSlope / sampleCount;

	return retVal;
}

/**************************************************************************
 Name:		Volt2ADC

 Does:		Calculate ADC value for a given voltage.

 Input:     Voltage             -- Voltage

 Output:    ADC Value           --

 Returns:
**************************************************************************/
PUBLIC Uint32 Volt2ADC(
	float 					Voltage
) {
	Int32 AdValue;
	const Engine_type* const engineParameters = engineGetParameters();

	Voltage = Voltage * engineParameters->ScalefactorNom.NormToBit.UadSlope * MEAS__10MS_AVG_VALUE_CNT;

	Voltage += 0.5; // correct rounding
	AdValue = (Int32)Voltage;

	if (AdValue > 0)
		return(AdValue);
	else
		return 0;
}

/**************************************************************************
 Name:		Ampere2ADC

 Does:		Calculate ADC value for a given current.

 Input:     Current             -- Current

 Output:    ADC Value           --

 Returns:
**************************************************************************/
PUBLIC Uint32 Ampere2ADC(
	float 					Current
) {
	Int32 AdValue;
	const Engine_type* const engineParameters = engineGetParameters();

	Current = Current * engineParameters->ScalefactorNom.NormToBit.IadSlope * MEAS__10MS_AVG_VALUE_CNT;

	Current += 0.5; // correct rounding
	AdValue = (Int32)Current;

	if (AdValue > 0)
		return(AdValue);
	else
		return 0;
}

/**************************************************************************
 Name:		Power2ADC

 Does:		Calculate ADC value for a given power.

 Input:     Power             -- Power

 Output:    ADC Value           --

 Returns:
**************************************************************************/
PUBLIC Uint32 Power2ADC(
	float 					Power
) {
	Uint32 AdValue;
	const Engine_type* const engineParameters = engineGetParameters();

	Power = Power*engineParameters->ScalefactorNom.NormToBit.UadSlope;
	Power = Power*engineParameters->ScalefactorNom.NormToBit.IadSlope;
	Power = Power*MEAS__10MS_AVG_VALUE_CNT*MEAS__10MS_AVG_VALUE_CNT;

	Power += 0.5; // correct rounding
	AdValue = (Uint32)Power;

	if (AdValue > 0)
		return(AdValue);
	else
		return 0;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__scaleTemperature
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate temperature from measured ADC values.
*
*	\param		pInst		Pointer to instance.
*	\param		nSum		Sum of samples.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Int16 meas__scaleTemperature(
        meas__Inst *                   pInst,
        Uint32                         nSum
) {
	extern int gTestMode;
	static Int16 tSum[MEAS__AVERAGE_LEN] = {0};
	static int pos = -1;
	Int16 tsum;
	Int16 tavg;
	Int16 tnew;

	/* Calculate 1s average temperature the resulting unit should be 0.1 degrees Celsius. */
	tnew = meas__scaleTemperatures(&TempBoardScaling, nSum);

	if(gTestMode == TRUE)
	{
		return tnew;
	}

	/* initialize array if first measurement */
	if(pos == -1){
		pos = 0;
		for(int i = 0; i < MEAS__AVERAGE_LEN; i++){
			tSum[i] = tnew;
		}
	}

	/* Calculate temperature the resulting unit should be 0.1 degrees Celsius. */
	tSum[pos] = tnew;
	pos = (pos + 1) % MEAS__AVERAGE_LEN;

	/* Calculate average of MEAS__AVERAGE_LEN seconds */
	tsum = 0;
	for(int i = 0; i < MEAS__AVERAGE_LEN; i++){
		tsum += tSum[i];
	}
	tavg = tsum / MEAS__AVERAGE_LEN;

	return tavg;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__scaleTemperatureBatt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate temperature from measured ADC values.
*
*	\param		pInst		Pointer to instance.
*	\param		nSum		Sum of samples.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Int16 meas__scaleTemperatureBatt(
       meas__Inst *                   pInst,
       Uint32                         nSum
) {
	extern int gTestMode;
	static Int16 tSum[MEAS__AVERAGE_LEN] = {0};
	static int pos = -1;
	Int16 tsum;
	Int16 tavg;
	Int16 tnew;

	/* Calculate 1s average temperature the resulting unit should be 0.1 degrees Celsius. */
	tnew = meas__scaleTemperatures(&TempBattScaling, nSum);

	if(gTestMode == TRUE)
	{
		return tnew;
	}

	/* initialize array if first measurement */
	if(pos == -1){
		pos = 0;
		for(int i = 0; i < MEAS__AVERAGE_LEN; i++){
			tSum[i] = tnew;
		}
	}

	/* Calculate temperature the resulting unit should be 0.1 degrees Celsius. */
	tSum[pos] = tnew;
	pos = (pos + 1) % MEAS__AVERAGE_LEN;

	/* Calculate average of MEAS__AVERAGE_LEN seconds */
	tsum = 0;
	for(int i = 0; i < MEAS__AVERAGE_LEN; i++){
		tsum += tSum[i];
	}
	tavg = tsum / MEAS__AVERAGE_LEN;

	return tavg;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas__scaleTemperature
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate heat sink temperature form measured ADC values.
*
*	\param		pInst		Pointer to instance.
*	\param		nSum		Sum of samples.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Int16 meas__scaleTemperatureHs(
	meas__Inst *			pInst,
	Uint32					nSum
) {
	static Int16 tSum[MEAS__AVERAGE_LEN] = {0};
	static int pos = -1;
	const Engine_type *engineParameters = engineGetParameters();
	Int16 tsum;
	Int16 tavg;
	Int16 tnew;

	/* Calculate 1s average temperature the resulting unit should be 0.1 degrees Celsius. */
	tnew = meas__scaleTemperatures(engineParameters->Theatsink, nSum);

	/* initialize array if first measurement */
	if(pos == -1){
		pos = 0;
		for(int i = 0; i < MEAS__AVERAGE_LEN; i++){
			tSum[i] = tnew;
		}
	}

	/* Calculate temperature the resulting unit should be 0.1 degrees Celsius. */
	tSum[pos] = tnew;
	pos = (pos + 1) % MEAS__AVERAGE_LEN;

	/* Calculate average of MEAS__AVERAGE_LEN seconds */
	tsum = 0;
	for(int i = 0; i < MEAS__AVERAGE_LEN; i++){
		tsum += tSum[i];
	}
	tavg = tsum / MEAS__AVERAGE_LEN;

	return tavg;
}

PRIVATE Int16 meas__scaleTemperatures(
	const Temperature_type*		Thermistor,
	Uint32						nSum
) {
	Int16					result;

	switch(Thermistor->Type)
	{
	case TempSensor_3_3V_Ntc :
		{
			float R_Thermistor_divided_R;
			/* First calculate the input voltage.
			 *
			 * Constants:
			 *   VREFP	Positive reference voltage
			 *   U_ADC	Input voltage
			 *   U_NTC	Thermistor voltage
			 *   ADC	Value in register
			 *
			 * Calculations:
			 *   ADC -> U_ADC
			 *     ADC = U_ADC*4096/VREFP <=> U_ADC = ADC*VREFP/4096 = ADC*3.0/4096
			 * 1000 samples.
			 */
			{
				float U_Thermistor;
				U_Thermistor = (float)nSum * (3.0/4096.0/1000.0); // voltage at ADC

				/* Second calculate Thermistor voltage. It is assumed that a resistor is connected
				 * to the input voltage and that a thermistor is connected to GND and that resistor
				 * and the thermistor is connected together.
				 *
				 * Constants:
				 *   U_in			Resistor-Thermistor voltage
				 *   U_THERMISTOR	Thermistor voltage
				 *   R				Resistor resistance
				 *   R_THERMISTOR	Thermistor resistance
				 *
				 * Calculations:
				 * U_THERMISTOR = R_THERMISTOR/(R_THERMISTOR + R)*U_in <=> U_THERMISTOR*(R_THERMISTOR + R) = R_THERMISTOR*U_in <=> R_THERMISTOR = U_THERMISTOR*R/(U_in - U_THERMISTOR)
				 *
				 */
				R_Thermistor_divided_R = U_Thermistor/(3.3 - U_Thermistor); // Thermistor resistance
			}

			result = meas__scaleTemperaturesNtc(R_Thermistor_divided_R, Thermistor);
		}
		break;
	case TempSensor_5V_Ntc :
		{
			float R_Thermistor_divided_R = meas__scaleTemperaturesR_Thermistor_divided_R_5V_Heatsink(nSum);

			result = meas__scaleTemperaturesNtc(R_Thermistor_divided_R, Thermistor);
		}
		break;
	case TempSensor_5V_Ptc :
		{
			float R_Thermistor_divided_R = meas__scaleTemperaturesR_Thermistor_divided_R_5V_Heatsink(nSum);

			if(R_Thermistor_divided_R < Thermistor->ShortCircuit) // short circuit
			{
				result = -32768;
			}
			else if(R_Thermistor_divided_R > Thermistor->OpenCircuit) // open circuit
			{
				result = 32767;
			}
			else
			{
				result = (Int16)(10.0*(Thermistor->A +
									   (Thermistor->B +
									   Thermistor->C*R_Thermistor_divided_R)*R_Thermistor_divided_R) - 2731.5);
			}
		}
		break;
	case TempSensor_MTM_HF2_NTC :
		{
			/* Temperature sensor MP-HF2 NTC:
			 *   Constants:
			 *     R			Resistance connected to GND in (V)
			 *     NTC			Thermistor connected to VREF in (V)
			 *     G			Operation amplifier gain
			 *     ADC1			ADC converter in MP-HF2 charger in (bit)
			 *     ADC2			ADC converter in MPAccess display in (bit)
			 *     U_ADC1		Voltage at ADC1 in (V)
			 *     U_ADC2		Voltage at ADC2 in (V)
			 *   Calculations:
			 *     U_ADC1 = Vcc*G*R/(R + NTC) <=> U_ADC1*(R + NTC) = Vcc*G*R <=> U_ADC1*NTC = (Vcc*G - U_ADC1)*R
			 *     NTC/R = (Vcc*G - U_ADC1)/U_ADC1 = (Vcc*G - ADC1*Vcc/ADC_max)/(ADC1*Vcc/ADC_max) = (G - ADC1/ADC_max)/(ADC1/ADC_max) = (G*ADC_max - ADC1)/ADC1
			 *
			 *     U_ADC2 = U_T*G*10kohm/(10kohm + 12kohm) = U_T*(1 + 100kohm/220kohm)*10/(10 + 12) = U_T*(1 + 100/220)*10/(10 + 12)
			 *     U_T = U_ADC2/(1 + 100/220)/10*(10 + 12)
			 *     U_ADC2 = ADC*3.0/409500 = 1.1081e-5
			 *     R_NTC = 10800.7/UADC2 - 1800
			 */

			float R_Thermistor = 10800.7/(1.1081e-6*nSum) - 1800.0;
			result = Steinhart_Hart(R_Thermistor, Thermistor);
		}
		break;
	case TempSensor_MTM_HF3_NTC :
		{
			float R_Thermistor_divided_R;
			/* First calculate the input voltage.
			 *
			 * Constants:
			 *   VREFP	Positive reference voltage
			 *   U_ADC	Input voltage
			 *   U_NTC	Thermistor voltage
			 *   ADC	Value in register
			 *
			 * Calculations:
			 *   ADC -> U_ADC
			 *     ADC = U_ADC*4096/VREFP <=> U_ADC = ADC*VREFP/4096 = ADC*3.0/4096
			 * 1000 samples.
			 */
			{
				float U_Thermistor;
				/* From file "12714005ASchemaRev02.pdf", "12714007A_Schema_rev02.pdf", "12713004A_schema_rev01.pdf"
				 * U = 6.8kohm/(6.8kohm + 10kohm)*5V
				 * R = 3.9kohm + 10kohm*6.8kohm/(10kohm + 6.8kohm)
				 * NTC22k -> ACPL-C87 unit gain -> G=10kohm/4.7kohm*(220kohm/100kohm + 1)*(10kohm/(10kohm + 12kohm))
				 * U_ADC = ADC*VREFP/4095;
				 * U_Thermistor = UADC/G
				*/
				U_Thermistor = (float)nSum * (float)(3.0/4095.0/1000.0/(10.0/4.7*(100.0/220.0 + 1)*(10.0/(10.0 + 12.0)))); // voltage at ADC

				/* Second calculate Thermistor voltage. It is assumed that a resistor is connected
				 * to the input voltage and that a thermistor is connected to GND and that resistor
				 * and the thermistor is connected together.
				 *
				 * Constants:
				 *   U_in			Resistor-Thermistor voltage
				 *   U_THERMISTOR	Thermistor voltage
				 *   R				Resistor resistance
				 *   R_THERMISTOR	Thermistor resistance
				 *
				 * Calculations:
				 * U_THERMISTOR = R_THERMISTOR/(R_THERMISTOR + R)*U_in <=> U_THERMISTOR*(R_THERMISTOR + R) = R_THERMISTOR*U_in <=> R_THERMISTOR = U_THERMISTOR*R/(U_in - U_THERMISTOR)
				 *
				 */
				R_Thermistor_divided_R = U_Thermistor/((float)(6.8/(6.8 + 10.0)*5.0) - U_Thermistor); // Thermistor resistance
			}

			result = Steinhart_Hart(R_Thermistor_divided_R, Thermistor);
		}
		break;
	case TempSensor_Robust_Int:
		/*
		 * Temperature is calculated with a third degree equation that has been
		 * determined with regression in Excel from the values in the
		 * "Rob temp sensor scaling.pdf" document made by Mikko (5.10.2012).
		 */
		result = (Int16) (
				(
					6.5909e-17f * nSum * nSum
					- 4.2942e-10f * nSum
					+ 1.7529e-03f
				) * nSum
				- 2.4646e+03f
		);
		break;
	case TempSensor_Robust_Batt:
		/*
		 * Temperature is calculated with a third degree equation that has been
		 * determined with regression in Excel from the values in the
		 * "Rob temp sensor scaling.pdf" document made by Mikko (20.6.2012).
		 */
		result = (Int16) (
				(
					5.806300e-17f * nSum * nSum
					- 4.228400e-10f * nSum
					+ 1.784300e-03f
				) * nSum
				- 2.949700e+03f
		);
		break;
	default :
		result = -32766;
		break;
	}

	return result;
}

PRIVATE float meas__scaleTemperaturesR_Thermistor_divided_R_5V_Heatsink(
		Uint32						nSum
) {
	float R_Thermistor_divided_R;

	/* First calculate the input voltage.
	 *
	 * Constants:
	 *   VREFP	Positive reference voltage
	 *   U_ADC	Input voltage
	 *   U_NTC	Thermistor voltage
	 *   ADC	Value in register
	 *
	 * Calculations:
	 *   ADC -> U_ADC
	 *     ADC = U_ADC*4096/VREFP <=> U_ADC = ADC*VREFP/4096 = ADC*3.0/4096
	 *   U_NTC -> U_ADC (OP-amp gain)
	 *     U_ADC = G*U_ADC = G*ADC*3.0/4096
	 *
	 *   G (from circuit diagram)
	 *     G = 10k/(10k + 12k)*(100k + 220k)/220k = 10/22*320/100 = 0.66116
	 *
	 * 1000 samples.
	 */
	{
		float U_Thermistor;
		U_Thermistor = (float)nSum * (1.0/0.6616*3.0/4096.0/1000.0); // voltage at ADC

		/* Second calculate Thermistor voltage. It is assumed that a resistor is connected
		 * to the input voltage and that a thermistor is connected to GND and that resistor
		 * and the thermistor is connected together.
		 *
		 * Constants:
		 *   U_in			Resistor-Thermistor voltage
		 *   U_THERMISTOR	Thermistor voltage
		 *   R				Resistor resistance
		 *   R_THERMISTOR	Thermistor resistance
		 *
		 * Calculations:
		 * U_THERMISTOR = R_THERMISTOR/(R_THERMISTOR + R)*U_in <=> U_THERMISTOR*(R_THERMISTOR + R) = R_THERMISTOR*U_in <=> R_THERMISTOR = U_THERMISTOR*R/(U_in - U_THERMISTOR)
		 *
		 */
		R_Thermistor_divided_R = U_Thermistor/(5.0 - U_Thermistor); // Thermistor resistance
	}
	return R_Thermistor_divided_R;
}

PRIVATE Int16 meas__scaleTemperaturesNtc(
	float						R_Thermistor_divided_R,
	const Temperature_type*		Thermistor
) {
	Int16					result;

	/* Calculate temperature from thermistor resistance by use of Steinhart-Hart equation.
	 *
	 *
	 */
	if(R_Thermistor_divided_R < Thermistor->ShortCircuit) // short circuit
	{
		result = 32767;
	}
	else if(R_Thermistor_divided_R > Thermistor->OpenCircuit) // open circuit
	{
		result = -32768;
	}
	else
	{
		float R_Thermistor_divided_R_log;

		R_Thermistor_divided_R_log = log(R_Thermistor_divided_R);
		result = (Int16)(10.0f/(Thermistor->A +
							   (Thermistor->B +
							   Thermistor->C*R_Thermistor_divided_R_log)*R_Thermistor_divided_R_log) - 2731.5f);
	}

	return result;
}

static Int16 Steinhart_Hart(
	float						R_Thermistor,
	const Temperature_type*		Thermistor
) {
	Int16					result;

	/* Calculate temperature from thermistor resistance by use of Steinhart-Hart equation.
	 *
	 *
	 */
	if(R_Thermistor < Thermistor->ShortCircuit) // short circuit
	{
		result = 32767;
	}
	else if(R_Thermistor > Thermistor->OpenCircuit) // open circuit
	{
		result = -32768;
	}
	else
	{
		float R_Thermistor_log;

		R_Thermistor_log = log(R_Thermistor);
		result = (Int16)(10.0f/(((Thermistor->A*R_Thermistor_log +
								 Thermistor->B)*R_Thermistor_log +
								 Thermistor->C)*R_Thermistor_log +
								 Thermistor->D)
								 - 2731.5f + 0.5f);						// Convert from absolute temperature and add 0.5 to get the rounding correct
	}

	return result;
}
/**************************************************************************
  Does:		Convert from ampere to signal that should be sent to the PWM
			module.

 Input:     Current             -- in (A)

 Returns:	Value
**************************************************************************/
PUBLIC Uint32 current2pwm(
	float 				   Current
) {
	Int32 AdValue;
	const Engine_type *engineParameters;

	engineParameters = engineGetParameters();

	Current = Current*engineParameters->ScalefactorNom.NormToBit.IpwmSlope;
	Current += engineParameters->ScalefactorNom.NormToBit.IpwmOffset;

	/* Offset should be the same on output as input is at zero current
	 * Constants:
	 *   D					Duty cycle in interval [0, 1]
	 *   PWM_CYCLE_FULL		in (bit)
	 *   U_Iset				in (V)
	 *   IpwmSlope			in (A/bit)
	 *   ISET_GAIN			in (A/V)
	 * Calculations:
	 *   ADC = U_ADC/3.0*4095 <=> U_ADC = ADC/4095*3.0
	 *   U_ADC = Ui_meas*5.6kohm/(5.6kohm + 1.5kohm + 3.9kohm)
	 *   Ui_meas = U_ADC*(5.6kohm + 1.5kohm + 3.9kohm)/5.6kohm = ADC/4095*3.0*(5.6kohm + 1.5kohm + 3.9kohm)/5.6kohm
	 *
	 *   G_amplifier = (47kohm + 91kohm)/91kohm = 138/91
	 *   U_Iset = D*3.3*G_amplifier*100kohm/(100kohm + 1kohm) = PWM/PWM_CYCLE_FULL*3.3*138/91*100/101
	 *   PWM = U_Iset*PWM_CYCLE_FULL/3.3/138*91/100*101
	 */
	//{
	//	const float UmeasOffset = -engineParameters->ScalefactorNom.NormToBit.IadOffset/4095.0*3.0*(5.6 + 1.5 + 3.9)/5.6;
	//	Current += UmeasOffset*(PWM_CYCLE_FULL/3.3/138*91/100*101);
	//}

	AdValue = (Int32)Current;

	if (AdValue > 0)
		return(AdValue);
	else
		return 0;
}
