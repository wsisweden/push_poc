/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		a25lx.c
*
*	\ingroup	A25LX
*
*	\brief		A25LXXX Serial Flash Memory driver.
*
*	\details
*
*	\note
*
*	\version	??-??-2009 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	A25LX	A25LX
*
*	\brief		A25LXXX flash memory driver
*
********************************************************************************
*
*	\details	The memory driver contains all code that is specific to the
*				A25LXXX flash memory chip.
*				
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "protif.h"
#include "deb.h"
#include "mem.h"
#include "osa.h"
#include "lib.h"

#include "a25lx.h"

#include "init.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * Memory page size in bytes.
 */

#define A25LX__PAGE_SIZE	256			

/**
 * Possible driver states
 */

enum a25lx__drvState {
	A25LX__STATE_IDLE,				/**< Flash memory driver is idle.		*/
	A25LX__STATE_NEWREQUEST,		/**< Handling a new request.			*/
	A25LX__STATE_INIT,				/**< Perform initialization				*/
	A25LX__STATE_WESET,				/**< Setting write enable latch.		*/
	A25LX__STATE_WPENABLE,			/**< Enable hardware write protection	*/
	A25LX__STATE_WRITE,				/**< Writing data to FLASH memory.		*/
	A25LX__STATE_READ,				/**< Reading data from FLASH memory		*/
	A25LX__STATE_DRVBUSY,			/**< Waiting for busy response for driver */
	A25LX__STATE_ERASE,				/**< Erasing memory sector				*/
	A25LX__STATE_ERASECHIP			/**< Erasing whole chip.				*/	
};

/**
 * Write data to memory steps.
 */

enum a25lx__writeSteps {
	A25LX__WRITESTEP_WAITBUSY1,			/**< Waiting until memory is ready.	*/
	A25LX__WRITESTEP_WESET,				/**< Setting write enable latch.	*/
	A25LX__WRITESTEP_WPDISABLE,			/**< Disabling write protection		*/
	A25LX__WRITESTEP_WAITBUSY2,			/**< Waiting until memory is ready.	*/
	A25LX__WRITESTEP_WESET2,			/**< Setting write enable latch		*/
	A25LX__WRITESTEP_WRITE,				/**< Writing data to FLASH memory.	*/
	A25LX__WRITESTEP_WAITBUSY3,			/**< Waiting until memory is ready.	*/
	A25LX__WRITESTEP_WESET3,			/**< Setting write enable latch.	*/
	A25LX__WRITESTEP_WPENABLE,			/**< Enabling write protection		*/
	A25LX__WRITESTEP_WAITBUSY4			/**< Waiting until memory is ready.	*/
};

/**
 * Sector erase steps.
 */

enum a25lx__eraseSteps {
	A25LX__ERASESTEP_WAITBUSY0,			/**< Waiting until memory is ready.	*/
	A25LX__ERASESTEP_WESET,				/**< Setting write enable latch.	*/
	A25LX__ERASESTEP_WPDISABLE,			/**< Disabling write protection		*/
	A25LX__ERASESTEP_WAITBUSY1,			/**< Waiting until memory is ready.	*/
	A25LX__ERASESTEP_WESET2,			/**< Setting write enable latch		*/
	A25LX__ERASESTEP_ERASE,				/**< Erasing memory sector.			*/
	A25LX__ERASESTEP_WAITBUSY2,			/**< Waiting until memory is ready.	*/
	A25LX__ERASESTEP_WESET3,			/**< Setting write enable latch.	*/
	A25LX__ERASESTEP_WPENABLE,			/**< Enabling write protection		*/
	A25LX__ERASESTEP_WAITBUSY3			/**< Waiting until memory is ready.	*/
};

/**
 * Chip erase steps.
 */

enum a25lx__chipEraseSteps {
	A25LX__CERASESTEP_WAITBUSY0,		/**< Waiting until memory is ready.	*/
	A25LX__CERASESTEP_WESET,			/**< Setting write enable latch.	*/
	A25LX__CERASESTEP_WPDISABLE,		/**< Disabling write protection		*/
	A25LX__CERASESTEP_WAITBUSY1,		/**< Waiting until memory is ready.	*/
	A25LX__CERASESTEP_WESET2,			/**< Setting write enable latch		*/
	A25LX__CERASESTEP_ERASE,			/**< Erasing chip.					*/
	A25LX__CERASESTEP_WAITBUSY2,		/**< Waiting until memory is ready.	*/
	A25LX__CERASESTEP_WESET3,			/**< Setting write enable latch.	*/
	A25LX__CERASESTEP_WPENABLE,			/**< Enabling write protection		*/
	A25LX__CERASESTEP_WAITBUSY3			/**< Waiting until memory is ready.	*/
};

/**
 * Read data from memory steps.
 */

enum a25lx__readSteps {
	A25LX__READSTEP_WAITBUSY,			/**< Waiting until memory is ready.	*/
	A25LX__READSTEP_READ				/**< Reading data from memory		*/
};

/**
 * 	\name		A25Lxxx/AT2xxx instruction set
 *
 *	\details	Common instruction set for A25LXXX/AT23XXX flash memories. The
 *				following instructions can be sent to the memory.
 */

/*@{*/
#define A25LX__OP_WREN		0x06		/**< Write Enable					*/
#define A25LX__OP_WRDI		0x04		/**< Write Disable					*/
#define A25LX__OP_RDSR		0x05		/**< Read Status Register			*/
#define A25LX__OP_WRSR		0x01		/**< Write Status Register			*/
#define A25LX__OP_READ		0x03		/**< Read Data 						*/
#define A25LX__OP_FREAD		0x0B		/**< Read Data highspeed			*/
#define A25LX__OP_PP		0x02		/**< Page Program					*/
#define A25LX__OP_SE		0x20		/**< Sector Erase					*/
#define A25LX__OP_BE		0xD8		/**< Block Erase					*/
#define A25LX__OP_CE		0xC7		/**< Chip Erase						*/
#define A25LX__OP_DP		0xB9		/**< Deep Power-down				*/
#define A25LX__OP_RES		0xAB		/**< Release from Deep Power-down	*/
/*@}*/

/**
 * 	\name		Atmel AT25xxx instructions
 */

/*@{*/
#define A25LX__ATOP_REMS	0x9F		/**< Read Manufacturer & Device Id	*/
#define A25LX__ATOP_BE32	0x52		/**< Block Erase (32 Kbytes)		*/
#define A25LX__ATOP_PSECT	0x36		/**< Protect sector					*/
#define A25LX__ATOP_UPSECT	0x39		/**< Unprotect sector				*/
/*@}*/

/**
 * 	\name		Amic A25xxx instructions
 */

/*@{*/
#define A25LX__AOP_RDID		0x9F		/**< Read Device Identification		*/
#define A25LX__AOP_REMS		0x90		/**< Read Manufacturer & Device Id	*/
#define A25LX__AOP_FREAD_DO	0x3B		/**< Read Data highspeed DualOut	*/
#define A25LX__AOP_FREAD_DIO 0xBB		/**< Read Data highspeed DualInOut	*/
/*@}*/

/**
 * A25LXXX instruction frame bytes.
 */

enum a25lx__frmByte {					/*''''''''''''''''''''''''''''''''''*/
	A25LX__FRM_INSTRUCTION,				/**< Instruction byte               */
	A25LX__FRM_ADDRH,					/**< Address byte high              */
	A25LX__FRM_ADDRM,					/**< Address byte middle			*/
	A25LX__FRM_ADDRL,					/**< Address byte low				*/
										/*==================================*/
	A25LX__FRM_LENGTH					/**< Frame length in bytes          */
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * 	\name		A25LXXX status register bits
 *
 *	\brief		The A25LXXX family FLASH memories have a status register that 
 *				can be read and written. The register contains the following
 *				bits.
 */

/*@{*/
#define A25LX__STAT_WIP		(1<<0)		/**< 1=Write in progress			*/
#define A25LX__STAT_WEL		(1<<1)		/**< 1=Write enable latch is on		*/
#define A25LX__STAT_BP0		(1<<2)		/**< Block write protection			*/
#define A25LX__STAT_BP1		(1<<3)		/**< Block write protection			*/
#define A25LX__STAT_BP2		(1<<4)		/**< Block write protection			*/
#define A25LX__STAT_SRWD	(1<<7)		/**< 1=Write protection enabled		*/
/*@}*/

/**
 * \name	Supported manufacturers IDs
 */

#define A25Lx__MID_AMIC		0x37		/**< Amic manufacturer ID			*/
#define A25Lx__MID_ATMEL	0x1F		/**< Atmel manufacturer ID			*/

/**
 * \name	Supported Amic chips
 */

/*@{*/
#define A25LX__ADID_A25L040	0x12		/**< Device ID of A25L040			*/
#define A25LX__ADID_A25L080	0x13		/**< Device ID of A25L080			*/
#define A25LX__ADID_A25L016	0x14		/**< Device ID of A25L016			*/
/*@}*/

/**
 * \name	Supported Atmel chips
 */

/*@{*/
#define A25LX__ATDID_AT25DF041A 0x44	/**< Device ID of AT25DF041A byte 1	*/
#define A25LX__ATDID_AT25SF041A 0x84	/**< Device ID of AT25SF041A byte 1	*/
#define A25LX__ATDID_AT25SF161B 0x14	/**< Device ID of AT25SF161B byte 1	*/
/*@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Driver instance type.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	a25lx_Init const_P *	pInit;		/**< Ptr to driver init data        */
	protif_Request			spiRequest;	/**< Protocol handler request       */
	protif_Data				reqData2;	/**< Second request data block      */
	lib_FifoType			reqFifo;	/**< FIFO containing requests       */
	memdrv_Alloc *			pActReq;	/**< Ptr to active request			*/
	Uint16					sentBytes;	/**< Number of bytes sent           */
	Uint16					retryCount;	/**< Number of retries made         */
	BYTE  					data[4];	/**< Buffer for flash instructions	*/
	Uint8 					state; 		/**< Driver state                   */
	Uint8					step;		/**< Current step of the current state*/
} a25lx__Inst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* Flash memory driver request handling functions */
PRIVATE void				a25lx__put(void *,memdrv_Alloc *);
PRIVATE void				a25lx__get(void *,memdrv_Alloc *);
PRIVATE void				a25lx__busy(void *,memdrv_Alloc *);
PRIVATE void				a25lx__doOp(void *,memdrv_Alloc *);
PRIVATE void				a25lx__handleRequest(a25lx__Inst *,memdrv_Alloc *);
PRIVATE void				a25lx__requestDone(a25lx__Inst *,Uint8);

/* protocol handler (SPI master) callback functions */
PRIVATE void				a25lx__stateMachine(Uint8,protif_Request *);
PRIVATE void				a25lx__postMachine(Uint8,protif_Request *);

/* Flash memory instruction functions */
PRIVATE void				a25lx__readStatus(a25lx__Inst *);
PRIVATE void				a25lx__enableWrite(a25lx__Inst *);
//PRIVATE void				a25lx__disableWrite(a25lx__Inst *);
PRIVATE void				a25lx__readDevIdAmic(a25lx__Inst *);
PRIVATE void				a25lx__readDevIdAtmel(a25lx__Inst *);
PRIVATE void				a25lx__setWriteProtection(a25lx__Inst *,Boolean);

/* State handling functions */
PRIVATE void				a25lx__handleWriteState(a25lx__Inst *,Uint8);
PRIVATE void				a25lx__handleReadState(a25lx__Inst *,Uint8);
PRIVATE void				a25lx__handleBusyCheckState(a25lx__Inst *,Uint8);
PRIVATE void				a25lx__handleEraseState(a25lx__Inst *,Uint8);
PRIVATE void				a25lx__handleCEraseState(a25lx__Inst *,Uint8);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface for the driver. The type of the interface object is
 * specified by the MEMDRV interface.
 */

PUBLIC memdrv_Interface const_P a25lx_driverIf = {
	&a25lx__put,
	&a25lx__get,
	&a25lx__busy,
	&a25lx__doOp
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the flash memory driver.
*
*	\param		pInit		Pointer to flash driver initialization data.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void * a25lx_init(
	 sys_FBInstId			iid,
	 void const_P *			pArgs
) {
	a25lx__Inst * 			pInst;

	pInst = (a25lx__Inst *) mem_reserve(&mem_normal, sizeof(a25lx__Inst));

	/*
	 * Initialize instance variables.
	 */

	pInst->pInit = (a25lx_Init *) pArgs;
	pInst->pActReq = NULL;
	pInst->sentBytes = 0;
	pInst->state = A25LX__STATE_IDLE;
	pInst->step = 0;
	pInst->retryCount = 0;
	
	/*
	 * Setup request info that remains constant after init
	 */
	
	pInst->spiRequest.fnCallback = &a25lx__postMachine;
	pInst->spiRequest.pTargetInfo = pInst->pInit->pTargetInfo;
	pInst->spiRequest.pUtil = pInst;

	/*
	 * Initialize request FIFO
	 */

	lib_fifoInit(&pInst->reqFifo);

	return(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__put
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write data to flash.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to memory driver request.
*
*	\return		
*
*	\details
*
*	\note		The address must be erased before it can be written to.
*
*******************************************************************************/

PRIVATE void a25lx__put(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	a25lx__Inst *			pInst;
	
	pInst = (a25lx__Inst *) pInstance;

	deb_assert(pRequest != NULL);
	deb_assert(pInst != NULL);

	pRequest->op = MEMDRV_OP_PUT;

	/*
	 * Driver must be in idle state to accept a new request.
	 */

	DISABLE;
	if (pInst->state == A25LX__STATE_IDLE)
	{
		pInst->state = A25LX__STATE_WRITE;
		ENABLE;

		pInst->pActReq = pRequest;
		pInst->retryCount = 0;
		pInst->sentBytes = 0;
		a25lx__readStatus(pInst);
	}
	else
	{
		/*
		 * Driver is busy. Add the request to the fifo.
		 */

		lib_fifoPut(&pInst->reqFifo, (void **) &pRequest->pNext);

		/*
		 * Interrupts are enabled by the fifo put function.
		 */
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__get
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read data from flash.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to memory driver request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__get(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	a25lx__Inst *			pInst;

	pInst = (a25lx__Inst *) pInstance;

	deb_assert(pRequest != NULL);
	deb_assert(pInst != NULL);

	pRequest->op = MEMDRV_OP_GET;

	/*
	 * State must be protected by disabling tasking.
	 */

	DISABLE;
	if (pInst->state == A25LX__STATE_IDLE)
	{
		pInst->state = A25LX__STATE_READ;
		ENABLE;

		pInst->pActReq = pRequest;
		pInst->retryCount = 0;
		a25lx__readStatus(pInst);
	}
	else
	{
		/*
		 * Driver is busy. Add the request to the fifo.
		 */

		lib_fifoPut(&pInst->reqFifo, (void **) &pRequest->pNext);

		/*
		 * Interrupts are enabled by the fifo put function.
		 */
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__busy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check if driver is busy.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to memory driver request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__busy(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	a25lx__Inst *			pInst;

	pInst = (a25lx__Inst *) pInstance;

	deb_assert(pRequest != NULL);
	deb_assert(pInst != NULL);

	pRequest->op = MEMDRV_OP_BUSY;

	/*
	 * State must be protected by disabling tasking.
	 */

	DISABLE;
	if (pInst->state == A25LX__STATE_IDLE)
	{
		pInst->state = A25LX__STATE_DRVBUSY;
		ENABLE;

		pInst->pActReq = pRequest;
		pInst->retryCount = 0;
		a25lx__readStatus(pInst);
	}
	else
	{
		/*
		 * Driver is busy. Add the request to the fifo.
		 */

		lib_fifoPut(&pInst->reqFifo, (void **) &pRequest->pNext);

		/*
		 * Interrupts are enabled by the fifo put function.
		 */
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__doOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform some other operation.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to the request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__doOp(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	a25lx__Inst *			pInst;

	pInst = (a25lx__Inst *) pInstance;

	deb_assert(pRequest != NULL);
	deb_assert(pInst != NULL);

	/*
	 * State must be protected by disabling tasking.
	 */

	DISABLE;
	if (pInst->state == A25LX__STATE_IDLE)
	{
		pInst->state = A25LX__STATE_NEWREQUEST;
		ENABLE;

		a25lx__handleRequest(pInst, pRequest);
	}
	else
	{
		/*
		 * Driver is busy. Add the request to the fifo. 
		 */

		lib_fifoPut(&pInst->reqFifo, (void **) &pRequest->pNext);

		/*
		 * Interrupts are enabled by the fifo put function.
		 */
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__handleRequest
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the supplied request.
*
*	\param		pInst		Pointer to driver instance.
*	\param		pRequest	Pointer to the request.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE void a25lx__handleRequest(
  	a25lx__Inst *			pInst,
	memdrv_Alloc *			pRequest
) {
	Uint8					newState;
	Uint8					newStep;

	newStep = 0;
	newState = A25LX__STATE_IDLE;
	pInst->pActReq = pRequest;

	switch(pRequest->op)
	{
		case MEMDRV_OP_ERASE_SECTOR:
			newState = A25LX__STATE_ERASE;
			newStep = A25LX__ERASESTEP_WAITBUSY0;
			break;

		case MEMDRV_OP_ERASE_CHIP:
			newState = A25LX__STATE_ERASECHIP;
			newStep = A25LX__CERASESTEP_WAITBUSY0;
			break;
	
		case MEMDRV_OP_GET:
			newState = A25LX__STATE_READ;
			break;

		case MEMDRV_OP_PUT:
			newState = A25LX__STATE_WRITE;
			pInst->sentBytes = 0;
			break;

		case MEMDRV_OP_BUSY:
			newState = A25LX__STATE_DRVBUSY;
			break;

		case MEMDRV_OP_SLEEP:
		case MEMDRV_OP_WAKEUP:
			/*
			 * Not supported but the application should still work ok.
			 */
			a25lx__requestDone(pInst, MEMDRV_OK);
			break;

		default:
			/*
			 * The driver does not support the requested operation.
			 */
			deb_assert(FALSE);
			a25lx__requestDone(pInst, MEMDRV_ERROR);
			return;
	}

	if (newState != A25LX__STATE_IDLE)
	{
		atomic(
			pInst->state = newState;
			pInst->step = newStep;
		);
	pInst->retryCount = 0;
	a25lx__readStatus(pInst);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__readStatus
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send status register read request to flash memory.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__readStatus(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent to the flash memory.
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_RDSR;

	/*
	 * Setup request object that will be passed to the protocol handler.
	 */

	/* First data block */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 1;
	pInst->spiRequest.data.pNext = &pInst->reqData2;

	/* Second data block */
	pInst->reqData2.flags = PROTIF_FLAG_READ;
	pInst->reqData2.pData = pInst->data;
	pInst->reqData2.size = 1;
	pInst->reqData2.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__writeData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write data to FLASH memory.
*
*	\param		pInst 	Pointer to driver instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__writeData(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	{
		Uint32 address;

		address = pInst->pActReq->address + pInst->sentBytes;

		deb_assert(
			address <= (pInst->pActReq->address + pInst->pActReq->count)
		);

		/*
		 * Build the frame that will be sent to the FLASH
		 */

		pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_PP;
		pInst->data[A25LX__FRM_ADDRH] = (BYTE) (address >> 16);
		pInst->data[A25LX__FRM_ADDRM] = (BYTE) (address >> 8);
		pInst->data[A25LX__FRM_ADDRL] = (BYTE) (address);

		/*
		 * Setup request that will be passed to the protocol handler.
		 */

		/* First data block (Contains the instruction) */
		pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
		pInst->spiRequest.data.pData = pInst->data;
		pInst->spiRequest.data.size = A25LX__FRM_LENGTH;
		pInst->spiRequest.data.pNext = &pInst->reqData2;

		/* Second data block (Contains the actual data) */
		pInst->reqData2.flags = PROTIF_FLAG_WRITE;
		pInst->reqData2.pData = pInst->pActReq->pData + pInst->sentBytes;
		pInst->reqData2.pNext = NULL;

		/*
		 * Calculate memory page boundary and limit the write request to within
		 * one page (256bytes/page).
		 */

		pInst->reqData2.size = (Uint16) (A25LX__PAGE_SIZE - (address & 0xFF));
		pInst->reqData2.size =
			MIN(
				pInst->reqData2.size,
				(pInst->pActReq->count - pInst->sentBytes)
			);
	}

	deb_assert(pInst->reqData2.size <= A25LX__PAGE_SIZE);
	deb_assert(pInst->reqData2.size > 0);

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__readData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send read instruction to the FLASH.
*
*	\param		pInst 	Pointer to fb instance
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__readData(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_READ;
	pInst->data[A25LX__FRM_ADDRH] = (BYTE) (pInst->pActReq->address >> 16);
	pInst->data[A25LX__FRM_ADDRM] = (BYTE) (pInst->pActReq->address >> 8);
	pInst->data[A25LX__FRM_ADDRL] = (BYTE) pInst->pActReq->address;

	/*
	 * Setup request object that will be passed to the protocol handler.
	 */

	/* First data block */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = A25LX__FRM_LENGTH;
	pInst->spiRequest.data.pNext = &pInst->reqData2;

	/* Second data block */
	pInst->reqData2.flags = PROTIF_FLAG_READ;
	pInst->reqData2.pData = pInst->pActReq->pData;
	pInst->reqData2.size = pInst->pActReq->count;
	pInst->reqData2.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__enableWrite
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set FLASH memory write latch.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details	Sends enable write latch instruction to FLASH.
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__enableWrite(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent to the FLASH
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_WREN;

	/*
	 * Setup request that will be passed to the protocol handler.
	 */

	/* First data block */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 1;
	pInst->spiRequest.data.pNext = NULL;

	/*
	 * Disable hardware write protection.
	 */

	pInst->pInit->pWpFn(TRUE);

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__eraseSector
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send sector erase instruction to the FLASH memory.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details	The sector address must be stored in the active request
*				before this function is called.
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__eraseSector(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_SE;
	pInst->data[A25LX__FRM_ADDRH] = (BYTE) (pInst->pActReq->address >> 16);
	pInst->data[A25LX__FRM_ADDRM] = (BYTE) (pInst->pActReq->address >> 8);
	pInst->data[A25LX__FRM_ADDRL] = (BYTE) pInst->pActReq->address;

	/*
	 * Setup request that will be passed to the protocol handler.
	 */

	/* First data block (Sector erase instruction) */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = A25LX__FRM_LENGTH;
	pInst->spiRequest.data.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__eraseChip
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send chip erase instruction to the flash memory.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__eraseChip(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_CE;
	pInst->data[A25LX__FRM_ADDRH] = 0;
	pInst->data[A25LX__FRM_ADDRM] = 0;
	pInst->data[A25LX__FRM_ADDRL] = 0;

	/*
	 * Setup PROTIF request that will be passed to the protocol handler.
	 */

	/* First protif data block (Chip erase instruction) */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 1;
	pInst->spiRequest.data.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
#if 0
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__enableSleep
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send deep power-down instruction to the FLASH memory.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__enableSleep(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_DP;

	/*
	 * Setup request that will be passed to the protocol handler.
	 */

	/* First data block (Deep power-down instruction) */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 1;
	pInst->spiRequest.data.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__disableSleep
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send deep power-down wakeup instruction to the FLASH memory.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__disableSleep(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_RES;

	/*
	 * Setup request that will be passed to the protocol handler.
	 */

	/* First data block (Deep power-down wakeup instruction) */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 1;
	pInst->spiRequest.data.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
#endif
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__readDevIdAmic
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read device and manufacturer ID from a Amic chip.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__readDevIdAmic(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__AOP_REMS;
	pInst->data[A25LX__FRM_ADDRH] = 0;
	pInst->data[A25LX__FRM_ADDRM] = 0;
	pInst->data[A25LX__FRM_ADDRL] = 0;

	/*
	 * Setup request that will be passed to the protocol handler.
	 */

	/* First data block (instruction) */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 4;
	pInst->spiRequest.data.pNext = &pInst->reqData2;

	/* Second data block (return data) */
	pInst->reqData2.flags = PROTIF_FLAG_READ;
	pInst->reqData2.pData = pInst->data;
	pInst->reqData2.size = 2;
	pInst->reqData2.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__readDevIdAtmel
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read device and manufacturer ID.
*
*	\param		pInst 	Pointer to driver instance
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__readDevIdAtmel(
	a25lx__Inst *			pInst
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__ATOP_REMS;
	pInst->data[A25LX__FRM_ADDRH] = 0;
	pInst->data[A25LX__FRM_ADDRM] = 0;
	pInst->data[A25LX__FRM_ADDRL] = 0;

	/*
	 * Setup request that will be passed to the protocol handler.
	 */

	/* First data block (instruction) */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 1;
	pInst->spiRequest.data.pNext = &pInst->reqData2;

	/* Second data block (return data) */
	pInst->reqData2.flags = PROTIF_FLAG_READ;
	pInst->reqData2.pData = pInst->data;
	pInst->reqData2.size = 2;
	pInst->reqData2.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__setWriteProtection
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read device and manufacturer ID.
*
*	\param		pInst 		Pointer to driver instance
*	\param		writeProt	TRUE if write protection should be enabled.
*							FALSE if it should be disabled.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__setWriteProtection(
	a25lx__Inst *			pInst,
	Boolean					writeProt
) {
	void * 					pHandler;

	/*
	 * Build the frame that will be sent
	 */

	pInst->data[A25LX__FRM_INSTRUCTION] = A25LX__OP_WRSR;

	if (writeProt)
	{
		pInst->data[1] = 
			A25LX__STAT_SRWD | A25LX__STAT_BP0 | A25LX__STAT_BP1 | A25LX__STAT_BP2;
	}
	else
	{
		pInst->data[1] = A25LX__STAT_SRWD;
	}

	/*
	 * Setup request that will be passed to the protocol handler.
	 */

	/* First data block (instruction) */
	pInst->spiRequest.data.flags = PROTIF_FLAG_WRITE;
	pInst->spiRequest.data.pData = pInst->data;
	pInst->spiRequest.data.size = 2;
	pInst->spiRequest.data.pNext = NULL;

	/*
	 * Send request to protocol handler.
	 */

	pHandler = sys_instIdToTHIS(pInst->pInit->handlerId);
	pInst->pInit->pProtIf->fnAccess(pHandler, &pInst->spiRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__stateMachine
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Main state machine for the driver.
*
*	\param		retCode		The code that was returned by the protocol handler.
*	\param		pRequest	Pointer to the protocol handler request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__stateMachine(
	 Uint8					retCode,
	 protif_Request *		pRequest
) {
	a25lx__Inst *			pInst;

	pInst = (a25lx__Inst *) pRequest->pUtil;

	switch(pInst->state)
	{
		case A25LX__STATE_IDLE:
			break;

		case A25LX__STATE_WRITE:
			a25lx__handleWriteState(pInst, retCode);
			break;

		case A25LX__STATE_READ:
			a25lx__handleReadState(pInst, retCode);
			break;
	
		case A25LX__STATE_DRVBUSY:
			a25lx__handleBusyCheckState(pInst, retCode);
			break;

		case A25LX__STATE_ERASE:
			a25lx__handleEraseState(pInst, retCode);
			break;

		case A25LX__STATE_ERASECHIP:
			a25lx__handleCEraseState(pInst, retCode);
			break;

		default:
			/*
			 * The state machine should handle all states.
			 */
			deb_assert(FALSE);
			break;
	}

	if (pInst->pActReq != NULL && pInst->retryCount > pInst->pInit->maxRetries)
	{
		/*
		 * The driver has retried the same protocol handler request too many
		 * times. The communication with the FLASH memory is probably broken.
		 * There is not much that can be done here. Reporting the the error as
		 * a fatal error.
		 */

		deb_assert(FALSE);

		a25lx__requestDone(pInst, MEMDRV_ERROR);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__postMachine
*
*--------------------------------------------------------------------------*//**
*
*	\brief		State machine used during power on self test.
*
*	\param		retCode		The code that was returned by the protocol handler.
*	\param		pRequest	Pointer to the protocol handler request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__postMachine(
	Uint8					retCode,
	protif_Request *		pRequest
) {
	a25lx__Inst *			pInst;

	pInst = (a25lx__Inst *) pRequest->pUtil;

	switch(pInst->state)
	{
		case A25LX__STATE_IDLE:
			break;

		case A25LX__STATE_WRITE:
		case A25LX__STATE_READ:
		case A25LX__STATE_ERASE:
		case A25LX__STATE_ERASECHIP:
		case A25LX__STATE_DRVBUSY:
		//case A25LX__STATE_S_BUSY:
			if (retCode != PROTIF_OK || (pInst->data[0] & A25LX__STAT_WIP))
			{
				/*
				 * Flash is busy. Ask immediately again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			else
			{
				/*
				 * Store the current state to sentBytes. sentBytes will only
				 * be used after POST so it can be used for storing the state
				 * during POST.
				 */

				pInst->sentBytes = pInst->state;
				atomic(;
					pInst->state = A25LX__STATE_INIT;
				);
				a25lx__readDevIdAmic(pInst);
			}
			break;

		case A25LX__STATE_INIT:
		{
			Boolean retry = TRUE;

			if (retCode == PROTIF_OK)
			{
				if (pInst->reqData2.pData[0] == A25Lx__MID_AMIC && (
					pInst->reqData2.pData[1] == A25LX__ADID_A25L080 ||
					pInst->reqData2.pData[1] == A25LX__ADID_A25L016 ||
					pInst->reqData2.pData[1] == A25LX__ADID_A25L040)
				) {
					/*
					 * IDs are ok. Supported Amic flash memory found. Set write
					 * latch so that the write protection bit can be written to
					 * the status register.
					 */

					atomic(;
						pInst->state = A25LX__STATE_WESET;
					);
					a25lx__enableWrite(pInst);
					retry = FALSE;

				} else if (pInst->reqData2.pData[0] == A25Lx__MID_ATMEL && (
						pInst->reqData2.pData[1] == A25LX__ATDID_AT25DF041A ||
						pInst->reqData2.pData[1] == A25LX__ATDID_AT25SF041A ||
						pInst->reqData2.pData[1] == A25LX__ATDID_AT25SF161B)
				) {
					/*
					 * IDs are ok. Supported Atmel flash memory found. Set write
					 * latch so that the write protection bit can be written to
					 * the status register.
					 */

					atomic(;
						pInst->state = A25LX__STATE_WESET;
					);
					a25lx__enableWrite(pInst);
					retry = FALSE;
				}
			}

			if (retry == TRUE) {
				/*
				 * Device ID or manufacturer ID did not match. There may be
				 * something wrong with the FLASH memory or it may be a
				 * unsupported memory.
				 *
				 * Try again a few times before giving up. Try to read Atmel
				 * chip id every second retry.
				 */

				if (pInst->retryCount & 0x01) {
					a25lx__readDevIdAmic(pInst);

				} else {
					a25lx__readDevIdAtmel(pInst);

				}
				pInst->retryCount++;
			}
			break;
		}
		
		case A25LX__STATE_WESET:			
			if (retCode == PROTIF_OK)
			{
				/*
				 * Write latch enabled.
				 */
				atomic(;
					pInst->state = A25LX__STATE_WPENABLE;
				);
				a25lx__setWriteProtection(pInst, FALSE);
			}
			else
			{
				pInst->retryCount++;
				a25lx__enableWrite(pInst);
			}
			break;		

		case A25LX__STATE_WPENABLE:
			if (retCode != PROTIF_OK)
			{
				//a25lx__setWriteProtection(pInst, TRUE);
				a25lx__setWriteProtection(pInst, FALSE);
				pInst->retryCount++;
			}
			else
			{
				/*
				 * Write protect bit written. proceed with normal operation.
				 */

				pInst->spiRequest.fnCallback = &a25lx__stateMachine;
				atomic(
					pInst->state = (Uint8) pInst->sentBytes;
				);
				a25lx__readStatus(pInst);
			}
			break;

		default:
			/*
			 * The state machine should handle all states.
			 */
			deb_assert(FALSE);
			break;
	}

	if (pInst->pActReq != NULL && pInst->retryCount > pInst->pInit->maxRetries)
	{
		/*
		 * The driver has retried the same protocol handler request too many
		 * times. The communication with the FLASH memory is probably broken.
		 * There is not much that can be done here. Reporting the the error as
		 * a fatal error.
		 */

		deb_assert(FALSE);

		pInst->pInit->pWpFn(FALSE);
		a25lx__requestDone(pInst, MEMDRV_ERROR);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__handleWriteState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle data write state.
*
*	\param		pInst	Pointer to driver instance.
*	\param		retCode	The code that was returned by the protocol handler.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__handleWriteState(
	a25lx__Inst *			pInst,
	Uint8					retCode
) {
	switch(pInst->step)
	{
		case A25LX__WRITESTEP_WAITBUSY1:
		{
			/*
			 * FLASH status register has been read. Continue with enabling
			 * the write latch if FLASH isn't busy.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				/*
				 * Set the write enable latch.
				 */
				
				pInst->step = A25LX__WRITESTEP_WESET;
				a25lx__enableWrite(pInst);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__WRITESTEP_WESET:
		{
			/*
			 * Write enable latch should be set now. Continue with disabling 
			 * write protection.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__WRITESTEP_WPDISABLE;
				//a25lx__setWriteProtection(pInst, FALSE);
				a25lx__readStatus(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}
		
		case A25LX__WRITESTEP_WPDISABLE:
		{
			/*
			 * Write protection should be disabled. Continue with reading the 
			 * status register before continuing with write operation.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__WRITESTEP_WAITBUSY2;
				a25lx__readStatus(pInst);
			}
			else
			{
				//a25lx__setWriteProtection(pInst, FALSE);
				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__WRITESTEP_WAITBUSY2:
		{
			/*
			 * Waiting for software write protection to be disabled. Continue
			 * with enabling the write latch if memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				pInst->step = A25LX__WRITESTEP_WESET2;
				a25lx__enableWrite(pInst);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__WRITESTEP_WESET2:
		{
			/*
			 * Write enable latch should be set now. Continue by writing one
			 * page of data.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__WRITESTEP_WRITE;
				a25lx__writeData(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__WRITESTEP_WRITE:
		{
			/*
			 * One page has been written to flash memory.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->sentBytes += pInst->reqData2.size;

				if (pInst->sentBytes < pInst->pActReq->count)
				{
					/*
					 * All data pages has not been sent yet. Check if FLASH is
					 * busy before writing next page.
					 */

					deb_assert(pInst->sentBytes > 0);
					
					pInst->step = A25LX__WRITESTEP_WAITBUSY2;
					a25lx__readStatus(pInst);
				}
				else
				{
					/*
					 * All data has been sent. Read status register to check if 
					 * flash memory is still busy.
					 */

					pInst->step = A25LX__WRITESTEP_WAITBUSY3;
					a25lx__readStatus(pInst);
				}
			}
			else
			{
				/*
				 * Error occurred while writing. Try to write the data again.
				 */
			
				a25lx__writeData(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__WRITESTEP_WAITBUSY3:
		{
			/*
			 * Waiting for data to be sent. Continue by setting write enable
			 * latch if the memory is ready.
			 *
			 * The driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				pInst->step = A25LX__WRITESTEP_WESET3;
				a25lx__enableWrite(pInst);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__WRITESTEP_WESET3:
		{
			/*
			 * Write enable latch should be enabled now. Continue by enabling 
			 * write protection on all blocks.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__WRITESTEP_WPENABLE;
				//a25lx__setWriteProtection(pInst, TRUE);
				a25lx__readStatus(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		
		case A25LX__WRITESTEP_WPENABLE:
		{
			/*
			 * Software write protection should now be enabled on all blocks.
			 * Enable hardware write protection.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__WRITESTEP_WAITBUSY4;
				a25lx__readStatus(pInst);
			}
			else
			{
				pInst->step = A25LX__WRITESTEP_WESET3;
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__WRITESTEP_WAITBUSY4:
		{
			/*
			 * Waiting for software write protection to be disabled. Continue
			 * with enabling the write latch if memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				pInst->pInit->pWpFn(FALSE);
				a25lx__requestDone(pInst, MEMDRV_OK);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		default:
			deb_assert(FALSE);
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__handleReadState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle read state.
*
*	\param		pInst		Pointer to driver instance.
*	\param		retCode		The code that was returned by the protocol handler.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__handleReadState(
	a25lx__Inst *			pInst,
	Uint8					retCode
) {
	switch(pInst->step)
	{
		case A25LX__READSTEP_WAITBUSY:
		{
			/*
			 * Flash status register has been read. Continue with read
			 * if flash isn't busy.
			 */

			if (retCode != PROTIF_OK || (pInst->data[0] & A25LX__STAT_WIP))
			{
				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			else
			{
				pInst->step = A25LX__READSTEP_READ;
				a25lx__readData(pInst);
			}
			break;
		}

		case A25LX__READSTEP_READ:
		{
			/*
			 * Data has been read from memory.
			 */

			a25lx__requestDone(pInst, MEMDRV_OK);
			break;
		}

		default:
		{
			deb_assert(FALSE);
			break;
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__handleBusyCheckState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		pInst	Pointer to driver instance.
*	\param		retCode	The return code that was returned by the protocol
*						handler.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void a25lx__handleBusyCheckState(
	a25lx__Inst *			pInst,
	Uint8					retCode
) {

	/*
	 * FLASH status register has been read. Report the status via callback.
	 */

	if (retCode != PROTIF_OK)
	{
		a25lx__readStatus(pInst);
		pInst->retryCount++;
	}
	else if (pInst->data[0] & A25LX__STAT_WIP)
	{
		a25lx__requestDone(pInst, MEMDRV_BUSY);
	}
	else
	{
		a25lx__requestDone(pInst, MEMDRV_FREE);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__handleEraseState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle erase sector state.
*
*	\param		pInst		Pointer to the driver instance.
*	\param		retCode		The callback return code.
*
*	\details	
*
*******************************************************************************/

PRIVATE void a25lx__handleEraseState(
	a25lx__Inst *			pInst,
	Uint8					retCode
) {
	switch(pInst->step)
	{
		case A25LX__ERASESTEP_WAITBUSY0:
		{
			/*
			 * FLASH status register has been read. Continue by setting write
			 * enable latch if memory is not busy.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				/*
				 * Write enable latch must be enabled before status register
				 * can be written.
				 */

				pInst->step = A25LX__ERASESTEP_WESET;
				a25lx__enableWrite(pInst);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;

			}
			break;
		}

		case A25LX__ERASESTEP_WESET:
		{
			/*
			 * Write enable latch should be set now. Continue by disabling
			 * write protection.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__ERASESTEP_WPDISABLE;
				//a25lx__setWriteProtection(pInst, FALSE);
				a25lx__readStatus(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__ERASESTEP_WPDISABLE:
		{
			/*
			 * Write protection disabling instruction has been sent. Continue
			 * by waiting until the instruction has been completed.
			 */
			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__ERASESTEP_WAITBUSY1;
				a25lx__readStatus(pInst);
			}
			else
			{
				/*
				 * Something went wrong when trying to disable write
				 * protection. Go back to first step and try again.
				 */

				pInst->step = A25LX__ERASESTEP_WAITBUSY0;
				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__ERASESTEP_WAITBUSY1:
		{
			/*
			 * Waiting for write protection disable instruction to complete.
			 * Continue by setting the write enable latch when the memory is
			 * ready.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				pInst->step = A25LX__ERASESTEP_WESET2;
				a25lx__enableWrite(pInst);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}


		case A25LX__ERASESTEP_WESET2:
		{
			/*
			 * Write enable latch should be set. Continue with erase.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__ERASESTEP_ERASE;
				a25lx__eraseSector(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__ERASESTEP_ERASE:
		{
			/*
			 * Sector erase instruction sent. Continue by reading status
			 * register to check if the instruction has been executed.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__ERASESTEP_WAITBUSY2;
				a25lx__readStatus(pInst);
			}
			else
			{
				/*
				 * Something went wrong when erasing. Wait until flash memory is 
				 * ready and try again.
				 */

				pInst->step = A25LX__ERASESTEP_WAITBUSY0;
				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__ERASESTEP_WAITBUSY2:
		{
			/*
			 * FLASH status register has been read. Continue by setting write
			 * enable latch if memory is not busy.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				/*
				 * Write enable latch must be enabled before write protection 
				 * can be enabled.
				 */

				pInst->step = A25LX__ERASESTEP_WESET3;
				a25lx__enableWrite(pInst);

			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__ERASESTEP_WESET3:
		{
			/*
			 * Write enable latch should be set. Continue by enabling write
			 * protection.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__ERASESTEP_WPENABLE;
				//a25lx__setWriteProtection(pInst, TRUE);
				a25lx__readStatus(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__ERASESTEP_WPENABLE:
		{
			/*
			 * Software write protection enable instruction has been sent.
			 * Continue by reading status register to check if the operation
			 * has completed.
			 */
			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__ERASESTEP_WAITBUSY3;
				a25lx__readStatus(pInst);
			}
			else
			{
				pInst->step = A25LX__ERASESTEP_WESET3;
				a25lx__enableWrite(pInst);
			}
			break;
		}

		case A25LX__ERASESTEP_WAITBUSY3:
		{
			/*
			 * Waiting for flash memory to complete the enable write protection
			 * instruction. Continue by enabling hardware write protection if
			 * memory is ready.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				pInst->pInit->pWpFn(FALSE);
				a25lx__requestDone(pInst, MEMDRV_OK);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		default:
			deb_assert(FALSE);
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__handleCEraseState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle erase chip state.
*
*	\param		pInst		Pointer to the driver instance.
*	\param		retCode		The callback return code.
*
*	\details	Should be called each time the callback is called while the 
*				driver is in erase chip state.
*
*******************************************************************************/

PRIVATE void a25lx__handleCEraseState(
	a25lx__Inst *			pInst,
	Uint8					retCode
) {
	switch(pInst->step)
	{
		case A25LX__CERASESTEP_WAITBUSY0:
		{
			/*
			 * Flash status register has been read. Continue by setting write
			 * enable latch if memory is not busy.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				/*
				 * Write enable latch must be enabled before status register
				 * can be written.
				 */

				pInst->step = A25LX__CERASESTEP_WESET;
				a25lx__enableWrite(pInst);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;

			}
			break;
		}

		case A25LX__CERASESTEP_WESET:
		{
			/*
			 * Write enable latch should be set now. Continue by disabling
			 * write protection.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__CERASESTEP_WPDISABLE;
				//a25lx__setWriteProtection(pInst, FALSE);
				a25lx__readStatus(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__CERASESTEP_WPDISABLE:
		{
			/*
			 * Write protection disabling instruction has been sent. Continue
			 * by waiting until the instruction has been completed.
			 */
			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__CERASESTEP_WAITBUSY1;
				a25lx__readStatus(pInst);
			}
			else
			{
				/*
				 * Something went wrong when trying to disable write
				 * protection. Go back to first step and try again.
				 */

				pInst->step = A25LX__CERASESTEP_WAITBUSY0;
				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__CERASESTEP_WAITBUSY1:
		{
			/*
			 * Waiting for write protection disable instruction to complete.
			 * Continue by setting the write enable latch when the memory is
			 * ready.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				pInst->step = A25LX__CERASESTEP_WESET2;
				a25lx__enableWrite(pInst);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}


		case A25LX__CERASESTEP_WESET2:
		{
			/*
			 * Write enable latch should be set. Continue with erase.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__CERASESTEP_ERASE;
				a25lx__eraseChip(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__CERASESTEP_ERASE:
		{
			/*
			 * Chip erase instruction sent. Continue by reading status
			 * register to check if the instruction has been executed.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__CERASESTEP_WAITBUSY2;
				a25lx__readStatus(pInst);
			}
			else
			{
				/*
				 * Something went wrong when erasing. Wait until flash memory is 
				 * ready and try again.
				 */

				pInst->step = A25LX__CERASESTEP_WAITBUSY0;
				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__CERASESTEP_WAITBUSY2:
		{
			/*
			 * FLASH status register has been read. Continue by setting write
			 * enable latch if memory is not busy.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				/*
				 * Write enable latch must be enabled before write protection 
				 * can be enabled.
				 */

				pInst->step = A25LX__CERASESTEP_WESET3;
				a25lx__enableWrite(pInst);

			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__CERASESTEP_WESET3:
		{
			/*
			 * Write enable latch should be set. Continue by enabling write
			 * protection.
			 */

			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__CERASESTEP_WPENABLE;
				//a25lx__setWriteProtection(pInst, TRUE);
				a25lx__readStatus(pInst);
			}
			else
			{
				a25lx__enableWrite(pInst);
				pInst->retryCount++;
			}
			break;
		}

		case A25LX__CERASESTEP_WPENABLE:
		{
			/*
			 * Software write protection enable instruction has been sent.
			 * Continue by reading status register to check if the operation
			 * has completed.
			 */
			if (retCode == PROTIF_OK)
			{
				pInst->step = A25LX__CERASESTEP_WAITBUSY3;
				a25lx__readStatus(pInst);
			}
			else
			{
				pInst->step = A25LX__CERASESTEP_WESET3;
				a25lx__enableWrite(pInst);
			}
			break;
		}

		case A25LX__CERASESTEP_WAITBUSY3:
		{
			/*
			 * Waiting for flash memory to complete the enable write protection
			 * instruction. Continue by enabling hardware write protection if
			 * memory is ready.
			 *
			 * Driver will remain on this step until the memory is ready.
			 */

			if (retCode == PROTIF_OK && not(pInst->data[0] & A25LX__STAT_WIP))
			{
				pInst->pInit->pWpFn(FALSE);
				a25lx__requestDone(pInst, MEMDRV_OK);
			}
			else
			{
				/*
				 * Still busy. Read status register again.
				 */

				a25lx__readStatus(pInst);
				pInst->retryCount++;
			}
			break;
		}

		default:
			deb_assert(FALSE);
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	a25lx__requestDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Report that the request has been handled.
*
*	\param		pInst		Pointer to the driver instance.
*	\param		retCode		The callback return code.
*
*	\details	This function will call the request callback function with the 
*				supplied return code. It will also set the driver into idle 
*				state or start handling the next request.
*
*******************************************************************************/

PRIVATE void a25lx__requestDone(
	a25lx__Inst *			pInst,
	Uint8					retCode
) {
	pInst->step = 0;

	if (pInst->pActReq->fnDone != NULL)									
	{																		
		pInst->pActReq->count = retCode;								
		pInst->pActReq->fnDone(pInst->pActReq);						
	}		

	osa_taskingDisable();	

	pInst->pActReq = (memdrv_Alloc *) lib_fifoGet(&pInst->reqFifo);	

	if (pInst->pActReq != NULL)											
	{																		
		osa_taskingEnable();	

		pInst->pActReq = tools_structPtr(memdrv_Alloc, pNext, pInst->pActReq);
		a25lx__handleRequest(pInst, pInst->pActReq);					
	}																		
	else																	
	{																		
		pInst->state = A25LX__STATE_IDLE;								
		osa_taskingEnable();												
	}																		
}
