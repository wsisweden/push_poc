/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		a25lx/init.h
*
*	\ingroup	A25LX
*
*	\brief		Init information for a25lxxx flash driver.
*
*	\details
*
*	\note
*
*	\version	11-12-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef A25LX_INIT_H_INCLUDED
#define A25LX_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

#include "tools.h"
#include "protif.h"
#include "sys.h"


#define a25lx_reset		dummy_reset
#define a25lx_down		dummy_down
#define a25lx_read		dummy_read
#define a25lx_write		dummy_write
#define a25lx_ctrl		dummy_ctrl
#define a25lx_test		dummy_test

/**
 * Type of function called to control write protection pin.
 */

typedef void a25lx_SetWpStateFn(Boolean);

/**
* Type of driver initialization values.
*/

typedef struct a25lx__init {			/*''''''''''''''''''''''''''' CONST */
	void const_P *			pTargetInfo;/**< Protocol specific target info  */
	protif_Interface const_P *pProtIf;	/**< Ptr to Protocol handler interface*/
	sys_FBInstId 			handlerId;	/**< Inst id of the protocol handler*/
	Uint16					maxRetries;	/**< Number of retries before abort.*/
	a25lx_SetWpStateFn *	pWpFn;		/**< Ptr ot write protection func.	*/	
} a25lx_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_A25LX
#elif defined(EXE_GEN_A25LX)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_A25LX
#endif

#define EXE_APPL(n)			n##a25lx

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

//#define EXE_USE_NN /* Non-volatile numeric	*/
//#define EXE_USE_NS /* Non-volatile string	*/
//#define EXE_USE_VN /* Volatile numeric		*/
//#define EXE_USE_VS /* Volatile string		*/
//#define EXE_USE_PN /* Process-point numeric	*/
//#define EXE_USE_PS /* Process-point string	*/
//#define EXE_USE_BL /* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name      Flags           Dim  Type  Def  Min  Max
            --------- --------------- ---- ----- ---- --- ----*/


/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
