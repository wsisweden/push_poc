/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_w32/Tui_hw.c
*
*	\ingroup	TUI_W32
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	08-12-2009 / Antero Rintam�ki
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	TUI_W32		TUI for Windows
*
*	\ingroup	TUI
*
*	\brief		Windows specific code for TUI.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <Windows.h>

#include "tools.h"
#include "osa.h"
#include "mem.h"

#include "HwSimu.h"
#include "pwm.h"

#include "../local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void				tui__hwSetPwmOutput(tui__Inst *, Uint8, Uint8);
PRIVATE void				tui__hwSetPwmEyeSaveCycle(void);
PRIVATE void				tui__hwSetPwmNormalCycle(void);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint8				tui__hwPwmStates[TUI__DIGIOUT_COUNT];

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes windows specific stuff.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC tui__Inst * tui__hwInit(
	tui_Init const_P *		pInit
) {
	tui__Inst *				pInst;

	pInst = (tui__Inst *) mem_reserve(&mem_normal, sizeof(tui__Inst));

	return(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwClearLeds
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		pInst	Pointer to TUI instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwClearLeds(
	tui__Inst *				pInst
){
	tui__hwDisableLed(pInst, TUI__DIGIOUT_CAN);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_F1);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_F2);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_ALARM);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_CHARG);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_CHARGRDY);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_STANDBY);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwEnableLed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void	tui__hwEnableLed(
	tui__Inst *				pInst,
	Uint8					outputNum
){
	tui__hwSetState(pInst, outputNum, TUI__OUTSTATE_ON);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwDisableLed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void	tui__hwDisableLed(
	tui__Inst *				pInst,
	Uint8					outputNum
){
	tui__hwSetState(pInst, outputNum, TUI__OUTSTATE_OFF);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwSetState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*	\param		states		The new state for the output.
*							Possible states:
*							- TUI__OUTSTATE_OFF
*							- TUI__OUTSTATE_ON
*							- TUI__OUTSTATE_BLINKING
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwSetState(
	tui__Inst *				pInst,
	Uint8					outputNum,
	Uint8					state
) {

	tui__hwPwmStates[outputNum] = state;

	switch(outputNum)
	{
		case TUI__DIGIOUT_ALARM:
			tui__hwSetPwmOutput(pInst, TUI__DIGIOUT_ALARM, state);
			break;
			
		case TUI__DIGIOUT_CHARG:
			tui__hwSetPwmOutput(pInst, TUI__DIGIOUT_CHARG, state);
			break;
			
		case TUI__DIGIOUT_CHARGRDY:
			tui__hwSetPwmOutput(pInst, TUI__DIGIOUT_CHARGRDY, state);
			break;
			
		case TUI__DIGIOUT_STANDBY:
			tui__hwSetPwmOutput(pInst, TUI__DIGIOUT_STANDBY, state);
			break;	

		case TUI__DIGIOUT_CAN:
			hwsimu_setParameter(HWSIMU_P_CANLED, state);
			break;

		case TUI__DIGIOUT_F1:
			hwsimu_setParameter(HWSIMU_P_F1LED, state);
			break;

		case TUI__DIGIOUT_F2:
			hwsimu_setParameter(HWSIMU_P_F2LED, state);
			break;

		case TUI__DIGIOUT_REMOTEOUT:
			hwsimu_setParameter(HWSIMU_P_REMOTEOUT, state);
			break;
	}
	pInst->outputState[outputNum] = state;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwDown
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Shutdown the hardware.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	This function is called when the device is going into sleep
*				mode.
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwDown(
	tui__Inst *				pInst
) {
	tui__hwClearLeds(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwUp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Startup the hardware.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	This function is called when the device is starting up from
*				sleep mode.
*
*	\note
*
*******************************************************************************/

extern void tui__hwUp(
	tui__Inst *				pInst
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwEyeSaveMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calls PWM to lower the PWM-cycle.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwEyeSaveMode(
	tui__Inst *				pInst
) {
	int						ii;

	pInst->dimmState = TUI__PWMSTATE_DIMMED;
	
	for (ii = 0; ii < TUI__DIGIOUT_COUNT; ii++)
	{
		tui__hwSetPwmOutput(pInst, ii, tui__hwPwmStates[ii]);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwNormalMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calls PWM to restore default PWM-cycle.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwNormalMode(
	tui__Inst *				pInst
) {
	int						ii;

	pInst->dimmState = TUI__PWMSTATE_NORMAL;

	for (ii = 0; ii < TUI__DIGIOUT_COUNT; ii++)
	{
		tui__hwSetPwmOutput(pInst, ii, tui__hwPwmStates[ii]);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui_hwSetPwmOutput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enables/Disables PWM-output.
*
*	\param		pInst 		Pointer to the FB instance.
*	\param		outputNum 	Number of output to be modified.
*	\param		state		Indicator of new output-state
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void tui__hwSetPwmOutput(
	tui__Inst *				pInst, 
	Uint8					outputNum, 
	Uint8					state
) {
	Uint32					newCycle = 0;

	if(state > 0)
	{
		tui__hwGetLedPwmCycle(pInst, &newCycle);
	}

	switch (outputNum)
	{
		case TUI__DIGIOUT_ALARM:
			hwsimu_setParameter(HWSIMU_P_ALARMLED, newCycle);
			break;

		case TUI__DIGIOUT_CHARG:
			hwsimu_setParameter(HWSIMU_P_CHARGINGLED, newCycle);
			break;

		case TUI__DIGIOUT_CHARGRDY:
			hwsimu_setParameter(HWSIMU_P_CHARGEREADYLED, newCycle);
			break;

		case TUI__DIGIOUT_STANDBY:
			hwsimu_setParameter(HWSIMU_P_STANDBYLED, newCycle);
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwGetLedPwmCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function finds and returns the current PWM-cycle for TUI-outputs.
*
*	\param		pInst			Pointer to tui-instance
*	\param		pCurrentCycle	Pointer to where the current cycle should be 
*								copied.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwGetLedPwmCycle(
	tui__Inst * 			pInst,
	Uint32 *				pCurrentCycle
) {
	// choose the cycle-value for pwm
	if(pInst->dimmState==TUI__PWMSTATE_DIMMED)
	{
		*pCurrentCycle = 1;
	}
	else
	{
		*pCurrentCycle = 2;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwBlinkOutput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function blinks tui-output that is using pwm-signal. 
*
*	\param		pInst			pointer to tui-instance
*	\param		output			tui-output number which tells which pwm-output to blink
*	\param		currentCycle	current default pwm-cycle
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwBlinkOutput(
	tui__Inst * 			pInst,
	Uint8					output,
	Uint32 					currentCycle
){
	Uint32					newCycle;

	newCycle = tui__hwPwmStates[output] == 0 ? currentCycle : 0;
	tui__hwPwmStates[output] = (Uint8) newCycle;

	switch(output)
	{
		case TUI__DIGIOUT_ALARM:
			hwsimu_setParameter(HWSIMU_P_ALARMLED, newCycle);
			break;
		
		case TUI__DIGIOUT_CHARG:
			hwsimu_setParameter(HWSIMU_P_CHARGINGLED, newCycle);
			break;
			
		case TUI__DIGIOUT_CHARGRDY:
			hwsimu_setParameter(HWSIMU_P_CHARGEREADYLED, newCycle);
			break;
			
		case TUI__DIGIOUT_STANDBY:
			hwsimu_setParameter(HWSIMU_P_STANDBYLED, newCycle);
			break;
	}
}
