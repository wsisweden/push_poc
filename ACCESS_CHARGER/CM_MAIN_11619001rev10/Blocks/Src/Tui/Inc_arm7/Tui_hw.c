/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_arm7/Tui_hw.c
*
*	\ingroup	TUI_ARM7
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Antero Rintam�ki
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	TUI_ARM7		TUI for LPC
*
*	\ingroup	TUI
*
*	\brief		TUI implementation for LPC23xx.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "mem.h"
#include "pwm.h"

#include "../local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define LED_CAN		(1<<22) /* P0.22 */
#define LED_F1		(1<<11) /* P2.11 */
#define LED_F2		(1<<12) /* P2.12 */


/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#define SET_LED_CAN(state) {						\
	if(state) 										\
		{FIO1SET = LED_CAN;}						\
	else 											\
		{FIO1CLR = LED_CAN;}						\
}											

#define SET_LED_F1(state) {							\
	if(state)										\
		{FIO2SET = LED_F1;}							\
	else											\
		{FIO2CLR = LED_F1;}							\
}													\

#define SET_LED_F2(state) {							\
	if(state)										\
		{FIO2SET = LED_F2;}							\
	else											\
		{FIO2CLR = LED_F2;}							\
}										


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/
PRIVATE void tui__setPwmEyeSaveCycle(void);
PRIVATE void tui__setPwmNormalCycle(void);
PRIVATE void tui__setPwmOutput(tui__Inst *, Uint32, Boolean);
PRIVATE void tui__hwSetState(tui__Inst *,Uint8,Boolean);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC tui__Inst * tui__hwInit(
	tui_Init const_P *		pInit
) {
	tui__Inst *			pInst;

	pInst = (tui__Inst *) mem_reserve(&mem_normal, sizeof(tui__Inst));

	pwm__init();
	
	// init leds
	FIO0DIR |= LED_CAN;
	FIO2DIR |= LED_F1 | LED_F2;
	
	// clear leds
	SET_LED_CAN( FALSE );
	SET_LED_F1( FALSE );
	SET_LED_F2( FALSE );
	
	return(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwEnableLed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void	tui__hwEnableLed(
	tui__Inst *				pInst,
	Uint8					outputNum
){
	tui__hwSetState(pInst, outputNum, TRUE);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwDisableLed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void	tui__hwDisableLed(
	tui__Inst *				pInst,
	Uint8					outputNum
){
	tui__hwSetState(pInst, outputNum, FALSE);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwSetState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*	\param		enabled		The new state for the output
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE void tui__hwSetState(
	tui__Inst *				pInst,
	Uint8					outputNum,
	Boolean					enabled
) {
	switch(outputNum)
	{
		case TUI__DIGIOUT_ALARM:
			tui_setPwmOutput(pInst, PWM_OUTPUT_ALARM, enabled);
			break;
			
		case TUI__DIGIOUT_CHARG:
			tui_setPwmOutput(pInst, PWM__OUTPUT_CHARG, enabled);
			break;
			
		case TUI__DIGIOUT_CHARGRDY:
			tui_setPwmOutput(pInst, PWM__OUTPUT_CHARGRDY, enabled);
			break;
			
		case TUI__DIGIOUT_STANDBY:
			tui_setPwmOutput(pInst, PWM__OUTPUT_STANDBY, enabled);
			break;
		
		case TUI__DIGIOUT_CAN:
			SET_LED_CAN(enabled);
			break;
			
		case TUI__DIGIOUT_F1:
			SET_LED_F1(enabled);
			break;
			
		case TUI__DIGIOUT_F2:
			SET_LED_F2(enabled);
			break;
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwDown
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Shutdown the hardware.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	This function is called when the device is going into sleep
*				mode.
*
*	\note
*
*******************************************************************************/
PUBLIC void tui__hwDown(
	tui__Inst *				pInst
) {	
	pwm__stop();
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwUp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Startup the hardware.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	This function is called when the device is starting up from
*				sleep mode.
*
*	\note
*
*******************************************************************************/
PUBLIC void tui__hwUp(
	tui__Inst *				pInst
) {
	pwm__start();
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwEyeSaveMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calls PWM to lower the PWM-cycle.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/
PUBLIC void tui__hwEyeSaveMode(
	tui__Inst *				pInst
) {
	pInst->dimmState = TUI__OUTSTATE_DIMMED;
	tui__setPwmEyeSaveCycle();
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwNormalMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calls PWM to restore default PWM-cycle.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/
PUBLIC void tui__hwNormalMode(
	tui__Inst *				pInst
) {
	pInst->dimmState = TUI__OUTSTATE_ON;
	tui__setPwmNormalCycle();
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui_setPwmEyeSaveCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Lowers the PWM-cycle at those PWM-outputs that are ON
*
*	\param		-
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void tui__setPwmEyeSaveCycle(void)
{
	if( pwm__getOutputCycle(PWM_OUTPUT_ALARM) )
	{
		pwm__setOutputCycle(PWM_OUTPUT_ALARM, PWM__CYCLE_TUI_EYE_SAVE);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_CHARG) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_CHARG, PWM__CYCLE_TUI_EYE_SAVE);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_CHARGRDY) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_CHARGRDY, PWM__CYCLE_TUI_EYE_SAVE);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_STANDBY) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_STANDBY, PWM__CYCLE_TUI_EYE_SAVE);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_CHARGE_CONTROL) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_CHARGE_CONTROL, PWM__CYCLE_TUI_EYE_SAVE);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui_setPwmNormalCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Restores the default PWM-cycle at those PWM-outputs that are dimmed
*
*	\param		-
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void tui__setPwmNormalCycle(void)
{
	if( pwm__getOutputCycle(PWM_OUTPUT_ALARM) )
	{
		pwm__setOutputCycle(PWM_OUTPUT_ALARM, PWM__CYCLE_TUI_NORMAL);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_CHARG) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_CHARG, PWM__CYCLE_TUI_NORMAL);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_CHARGRDY) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_CHARGRDY, PWM__CYCLE_TUI_NORMAL);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_STANDBY) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_STANDBY, PWM__CYCLE_TUI_NORMAL);
	}
	if( pwm__getOutputCycle(PWM__OUTPUT_CHARGE_CONTROL) )
	{
		pwm__setOutputCycle(PWM__OUTPUT_CHARGE_CONTROL, PWM__CYCLE_TUI_NORMAL);
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui_setPwmOutput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enables/Disables PWM-output.
*
*	\param		pInst 		Pointer to the FB instance.
*	\param		output 		Number of output to be modified.
*	\param		enabled		Indicator of new output-state
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/
PRIVATE void tui__setPwmOutput(
		tui__Inst * pInst, 
		Uint32 output, 
		Boolean enabled
) {
	 Uint32 newCycle = 0;
	 if(enabled)
	 {
		if(pInst->dimmState==TUI__OUTSTATE_DIMMED)
		{
			newCycle = PWM__CYCLE_TUI_EYE_SAVE;
		}
		else
		{
			newCycle = PWM__CYCLE_TUI_NORMAL;
		}
	}
	
	pwm__setOutputCycle(output, newCycle);
}
