/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Tui/Local.h
*
*	\ingroup	TUI
*
*	\brief		Local declarations for the TUI FB.
*
*	\details
*
*	\note
*
*	\version	08-12-2009 / Antero Rintam�ki
*				??-??-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef MEAS_LOCAL_H_INCLUDED
#define MEAS_LOCAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "swtimer.h"

#include "global.h"
#include "Tui.h"
#include "init.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * List of all digital outputs.
 */

enum tui__digiOutputs
{
	TUI__DIGIOUT_CAN = 0,
	TUI__DIGIOUT_F1,
	TUI__DIGIOUT_F2,
	TUI__DIGIOUT_ALARM,
	TUI__DIGIOUT_CHARG,
	TUI__DIGIOUT_CHARGRDY,
	TUI__DIGIOUT_STANDBY,
	TUI__DIGIOUT_REMOTEOUT,
	TUI__DIGIOUT_TEMPMUX,
	TUI__DIGIOUT_COUNT					/**< Total number of digital outputs */
};

/**
 * PWM-states at TUI
 */
enum tui__pwmState
{
	TUI__PWMSTATE_DIMMED,				/**< Dimmed PWM output.				*/
	TUI__PWMSTATE_NORMAL				/**< Normal PWM output				*/
};	

/**
 * TUI output states
 */
#define TUI__OUTSTATE_OFF		0
#define TUI__OUTSTATE_ON		(1<<0)
#define TUI__OUTSTATE_BLINKING	(1<<1)
#define TUI__OUTSTATE_BLINKING2	(1<<2)


/**
 * \name	Led status bits
 *
 * \brief	Used in LED masks to indicate led states.
 */

/*@{*/
#define TUI__LED_RED			(1<<0)	/**< Alarm led on					*/
#define TUI__LED_YELLOW			(1<<1)
#define TUI__LED_GREEN			(1<<2)
#define TUI__LED_BLUE			(1<<3)	/**< Standby led on					*/
#define TUI__LED_RED_BLINK		(1<<4)	/**< Alarm led blinking				*/
#define TUI__LED_YELLOW_BLINK	(1<<5)
#define TUI__LED_GREEN_BLINK	(1<<6)
#define TUI__LED_BLUE_BLINK		(1<<7)
/*@}*/

/**
 * Masked to the priority for status indications. Alarm indications will not
 * have this bit set in the priority.
 */

#define TUI__STATUSIND						(1<<7)

/**
 * List of possible indications.
 */

enum {
	TUI__IND_MAINS_ON = 0,
	TUI__IND_STANDBY,
	TUI__IND_REMOTE_OFF,
	TUI__IND_CHARG_RESTRAINED,
	TUI__IND_MAIN_CHARG,
	TUI__IND_ADD_CHARG,
	TUI__IND_CHARG_READY,
	TUI__IND_EQUALZ_CHARG,
	TUI__IND_MAINT_CHARG,
	TUI__IND_PAUSE,
	TUI__IND_LOW_BATTERY,
	TUI__IND_HIGH_BATTERY,
	TUI__IND_LOW_CHARG_TEMP,
	TUI__IND_HIGH_CHARG_TEMP,
	TUI__IND_AH_LIMIT_EXCEED,
	TUI__IND_TIME_LIMIT_EXCEED,
	TUI__IND_PHASE_ERROR,
	TUI__IND_INCORRECT_CHARG_PARAMS,
	TUI__IND_UNCKOWN_CHARG_ERROR,
	TUI__IND_CHARG_INCORRECT_MOUNT,
	TUI__IND_FW_MALFUNCTION,
	TUI__IND_BBC_CHARGING,
	TUI__IND_BBC_READY,

	TUI__IND_COUNT						/**< Number of indications			*/
};

/**
 * \name	Indication LED states
 */

/*@{*/
#define TUI__VAL_MAINS_ON					TUI__LED_BLUE
#define TUI__VAL_STANDBY					TUI__LED_BLUE
#define TUI__VAL_REMOTE_OFF					TUI__LED_BLUE | TUI__LED_YELLOW_BLINK
#define TUI__VAL_CHARG_RESTRAINED			TUI__LED_BLUE | TUI__LED_YELLOW_BLINK
#define TUI__VAL_MAIN_CHARG					TUI__LED_BLUE | TUI__LED_YELLOW
#define TUI__VAL_ADD_CHARG					TUI__LED_BLUE | TUI__LED_YELLOW | TUI__LED_GREEN
#define TUI__VAL_CHARG_READY				TUI__LED_BLUE | TUI__LED_GREEN
#define TUI__VAL_EQUALZ_CHARG				TUI__LED_BLUE | TUI__LED_GREEN
#define TUI__VAL_MAINT_CHARG				TUI__LED_BLUE | TUI__LED_GREEN
#define TUI__VAL_PAUSE						TUI__LED_BLUE | TUI__LED_GREEN_BLINK
#define TUI__VAL_LOW_BATTERY				TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_HIGH_BATTERY				TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_LOW_CHARG_TEMP				TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_HIGH_CHARG_TEMP			TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_AH_LIMIT_EXCEED			TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_TIME_LIMIT_EXCEED			TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_PHASE_ERROR				TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_INCORRECT_CHARG_PARAMS		TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_UNCKOWN_CHARG_ERROR		TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_CHARG_INCORRECT_MOUNT		TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_FW_MALFUNCTION				TUI__LED_BLUE | TUI__LED_RED
#define TUI__VAL_BBC_CHARGING				TUI__LED_BLUE | TUI__LED_YELLOW
#define TUI__VAL_BBC_READY					TUI__LED_BLUE | TUI__LED_GREEN
/*@{*/

/**
 * \name	Indication priorities
 */

/*@{*/
#define TUI__PRIOR_MAINS_ON					(99 | TUI__STATUSIND)
#define TUI__PRIOR_STANDBY					(99 | TUI__STATUSIND)
#define TUI__PRIOR_REMOTE_OFF				(8 | TUI__STATUSIND)
#define TUI__PRIOR_CHARG_RESTRAINED			(9 | TUI__STATUSIND)
#define TUI__PRIOR_MAIN_CHARG				(99 | TUI__STATUSIND)
#define TUI__PRIOR_ADD_CHARG				(99 | TUI__STATUSIND)
#define TUI__PRIOR_CHARG_READY				(99 | TUI__STATUSIND)
#define TUI__PRIOR_EQUALZ_CHARG				(99 | TUI__STATUSIND)
#define TUI__PRIOR_MAINT_CHARG				(99 | TUI__STATUSIND)
#define TUI__PRIOR_PAUSE					(98 | TUI__STATUSIND)
#define TUI__PRIOR_LOW_BATTERY				2
#define TUI__PRIOR_HIGH_BATTERY				2
#define TUI__PRIOR_LOW_CHARG_TEMP			7
#define TUI__PRIOR_HIGH_CHARG_TEMP			10
#define TUI__PRIOR_AH_LIMIT_EXCEED			5
#define TUI__PRIOR_TIME_LIMIT_EXCEED		4
#define TUI__PRIOR_PHASE_ERROR				3
#define TUI__PRIOR_INCORRECT_CHARG_PARAMS	1
#define TUI__PRIOR_UNCKOWN_CHARG_ERROR		6
#define TUI__PRIOR_CHARG_INCORRECT_MOUNT	1
#define TUI__PRIOR_FW_MALFUNCTION			1
#define TUI__PRIOR_BBC_CHARGING				(99 | TUI__STATUSIND)
#define TUI__PRIOR_BBC_READY				(99 | TUI__STATUSIND)
/*@}*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/**
 * Type for TUI instance.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	osa_TaskType		task;			/**< Task							*/
	tui_Init const_P * 	pInit;			/**< Initialization data			*/
	sys_FBInstId		nInstId;		/**< Instance ID					*/
	swtimer_Timer_st	timer;			/**< Timer							*/
	swtimer_Timer_st	ledStatusTimer;	/**< Alarm Timer					*/
	Uint16				oldChalgStat;	/**< Old CHALG status value			*/
	Uint32				triggers;		/**< TUI event triggers				*/
	Uint16				prevKeyStates;	/**< Previous keystates				*/
	Uint16				IdentifyModeTime;	/**< Time in 50ms resolution	*/
	Uint8				oldSupStatus;	/**< Old SUP status flags			*/
	Uint8				oldSupState;	/**< Old SUP state					*/
	Uint8				flags;			/**< TUI state flags				*/
	Uint8				dimmState;		/**< On state for dimmable LEDs		*/
	Uint8				outputState[TUI__DIGIOUT_COUNT]; /**< State of LED output */
	Int8				prevShownInd;	/**< Previusly show status/alarm indication	*/
	Uint8				indicatorVal[TUI__IND_COUNT];/**< Collection off currently enabled indications */
	Uint8				blinkState;		/**< On/off state for blinking LEDs	*/
} tui__Inst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/
extern tui__Inst *			tui__hwInit(tui_Init const_P *);

extern void 				tui__hwClearLeds(tui__Inst *);

extern void					tui__hwEnableLed(tui__Inst *,Uint8);
extern void					tui__hwDisableLed(tui__Inst *,Uint8);
extern void 				tui__hwSetState(tui__Inst *,Uint8,Uint8);

extern void					tui__hwDown(tui__Inst *);
extern void					tui__hwUp(tui__Inst *);

extern void 				tui__hwEyeSaveMode(tui__Inst *);
extern void 				tui__hwNormalMode(tui__Inst *);
extern void					tui__hwBlinkOutput(tui__Inst *,Uint8,Uint32);
extern void					tui__hwGetLedPwmCycle(tui__Inst *,Uint32 *);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif



