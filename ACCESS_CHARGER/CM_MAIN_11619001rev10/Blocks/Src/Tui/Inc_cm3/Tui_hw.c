/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/Tui_hw.c
*
*	\ingroup	TUI_CM3
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	08-12-2009 / Antero Rintam�ki
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	TUI_CM3		TUI for LPC
*
*	\ingroup	TUI
*
*	\brief		TUI implementation for LPC17xx.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "sys.h"
#include "reg.h"
#include "mem.h"
#include "pwm.h"

#include "_hw.h"
#include "../local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define LED_CAN		(1<<22) /* P0.22 */
#define LED_F1		(1<<11) /* P2.11 */
#define LED_F2		(1<<12) /* P2.12 */
#define REM_OUT		(1<<5)	/* P2.5 */
#define TEMP_MUX	(1<<22)	/* P1.22 */

/*
 * PWM cycles
 */

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#define TUI__SET_LED_CAN(state) {					\
	if(state) 										\
		{FIO0SET = LED_CAN;}						\
	else 											\
		{FIO0CLR = LED_CAN;}						\
}											

#define TUI__SET_LED_F1(state) {					\
	if(state)										\
		{FIO2SET = LED_F1;}							\
	else											\
		{FIO2CLR = LED_F1;}							\
}													\

#define TUI__SET_LED_F2(state) {					\
	if(state)										\
		{FIO2SET = LED_F2;}							\
	else											\
		{FIO2CLR = LED_F2;}							\
}

#define TUI__SET_REM_OUT(state) {					\
	if(state)										\
		{FIO2SET = REM_OUT;}						\
	else											\
		{FIO2CLR = REM_OUT;}						\
}										

#define TUI__SET_TEMP_MUX(state) {					\
	if(state)										\
		{FIO1SET = TEMP_MUX;}						\
	else											\
		{FIO1CLR = TEMP_MUX;}						\
}

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void tui__hwSetPwmOutput(tui__Inst *, Uint32, Uint8);
PRIVATE void tui__hwUpdateDimState(tui__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC tui__Inst * tui__hwInit(
	tui_Init const_P *		pInit
) {
	tui__Inst *			pInst;

	pInst = (tui__Inst *) mem_reserve(&mem_normal, sizeof(tui__Inst));

	// Digital out (remote out) init
	FIO2DIR |= REM_OUT;
	tui__hwSetState(pInst, TUI__DIGIOUT_REMOTEOUT, TUI__OUTSTATE_OFF);

	// Digital out (Temp MUX) init
	FIO1DIR |= TEMP_MUX;
	tui__hwSetState(pInst, TUI__DIGIOUT_TEMPMUX, TUI__OUTSTATE_OFF);

	// PWM init
	pwm_init();
	
	// init leds
	FIO0DIR |= LED_CAN;
	FIO2DIR |= LED_F1 | LED_F2;
	
	tui__hwClearLeds(pInst);
	
	pInst->dimmState = TUI__PWMSTATE_DIMMED;
	
	return(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwClearLeds
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwClearLeds(
	tui__Inst *				pInst
){
	tui__hwDisableLed(pInst, TUI__DIGIOUT_CAN);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_F1);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_F2);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_ALARM);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_CHARG);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_CHARGRDY);
	tui__hwDisableLed(pInst, TUI__DIGIOUT_STANDBY);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwEnableLed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void	tui__hwEnableLed(
	tui__Inst *				pInst,
	Uint8					outputNum
){
	tui__hwSetState(pInst, outputNum, TUI__OUTSTATE_ON);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwDisableLed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void	tui__hwDisableLed(
	tui__Inst *				pInst,
	Uint8					outputNum
){
	tui__hwSetState(pInst, outputNum, TUI__OUTSTATE_OFF);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwSetState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of a digital output.
*
*	\param		pInst		Pointer to the FB instance.
*	\param		outputNum	The number of the output to be changed.
*	\param		state		The new state for the output
*							
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwSetState(
	tui__Inst *				pInst,
	Uint8					outputNum,
	Uint8					state
) {

	switch(outputNum)
	{
		case TUI__DIGIOUT_ALARM:
			tui__hwSetPwmOutput(pInst, PWM_OUTPUT_ALARM, state);
			break;
			
		case TUI__DIGIOUT_CHARG:
			tui__hwSetPwmOutput(pInst, PWM_OUTPUT_CHARG, state);
			break;
			
		case TUI__DIGIOUT_CHARGRDY:
			tui__hwSetPwmOutput(pInst, PWM_OUTPUT_CHARGRDY, state);
			break;
			
		case TUI__DIGIOUT_STANDBY:
			tui__hwSetPwmOutput(pInst, PWM_OUTPUT_STANDBY, state);
			break;	
		
		case TUI__DIGIOUT_CAN:
			TUI__SET_LED_CAN( (state & TUI__OUTSTATE_ON) );
			break;
			
		case TUI__DIGIOUT_F1:
			TUI__SET_LED_F1( (state & TUI__OUTSTATE_ON) );
			break;
			
		case TUI__DIGIOUT_F2:
			TUI__SET_LED_F2( (state & TUI__OUTSTATE_ON) );
			break;

		case TUI__DIGIOUT_REMOTEOUT:
			TUI__SET_REM_OUT( (state & TUI__OUTSTATE_ON) );
			break;

		case TUI__DIGIOUT_TEMPMUX:
			TUI__SET_TEMP_MUX( (state & TUI__OUTSTATE_ON) );
			break;

	}
	pInst->outputState[outputNum] = state;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwDown
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Shutdown the hardware.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	This function is called when the device is going into sleep
*				mode.
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwDown(
	tui__Inst *				pInst
) {	
	tui__hwClearLeds(pInst);
	pwm_stop();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwUp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Startup the hardware.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	This function is called when the device is starting up from
*				sleep mode.
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwUp(
	tui__Inst *				pInst
) {
	pwm_start();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwEyeSaveMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calls PWM to lower the PWM-cycle.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwEyeSaveMode(
	tui__Inst *				pInst
) {
	pInst->dimmState = TUI__PWMSTATE_DIMMED;

	tui__hwUpdateDimState(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwNormalMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calls PWM to restore default PWM-cycle.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwNormalMode(
	tui__Inst *				pInst
) {
	pInst->dimmState = TUI__PWMSTATE_NORMAL;

	tui__hwUpdateDimState(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwUpdateDimState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Updates the LED dimmed states.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void tui__hwUpdateDimState(
	tui__Inst *				pInst
) {
	Uint32					cycle;

	tui__hwGetLedPwmCycle(pInst, &cycle);

	if( pInst->outputState[TUI__DIGIOUT_ALARM] > 0)
	{
		pwm_setOutputCycle(PWM_OUTPUT_ALARM, cycle);
	}

	if( pInst->outputState[TUI__DIGIOUT_CHARG] > 0)
	{
		pwm_setOutputCycle(PWM_OUTPUT_CHARG, cycle);
	}

	if( pInst->outputState[TUI__DIGIOUT_CHARGRDY] > 0)
	{
		pwm_setOutputCycle(PWM_OUTPUT_CHARGRDY, cycle);
	}

	if( pInst->outputState[TUI__DIGIOUT_STANDBY] > 0)
	{
		pwm_setOutputCycle(PWM_OUTPUT_STANDBY, cycle);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui_hwSetPwmOutput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enables/Disables PWM-output.
*
*	\param		pInst 		Pointer to the FB instance.
*	\param		output 		Number of output to be modified.
*	\param		state		Indicator of new output-state
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void tui__hwSetPwmOutput(
	tui__Inst * 		pInst, 
	Uint32 				output, 
	Uint8 				state
) {
	Uint32 newCycle = 0;

	if(state > 0)
	{
		if(state >= TUI__OUTSTATE_BLINKING)
			return;
		else
			tui__hwGetLedPwmCycle(pInst, &newCycle);
	}

	pwm_setOutputCycle(output, newCycle);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwGetLedPwmCycle
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function finds and returns the current PWM-cycle for
*				TUI-outputs.
*
*	\param		pInst			pointer to TUI instance.
*	\param		pCurrentCycle	Pointer to variable where the PWM cycle should
*								be stored.
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwGetLedPwmCycle(
	tui__Inst * 			pInst,
	Uint32 *				pCurrentCycle
) {
	Uint8 					max;
	Uint32					retValue;

	reg_getLocal(&max, reg_i_LedBrightnessMax, pInst->nInstId);

	retValue = ((Uint32) max * 1000 * PWM_CYCLE_FULL) / 100000;

	if(pInst->dimmState == TUI__PWMSTATE_DIMMED)
	{
		Uint8 dimmed;

		/*
		 * The PWM cycle should be limited to dim the LEDs.
		 */

		reg_getLocal(&dimmed, reg_i_LedBrightnessDim, pInst->nInstId);

		retValue = ((Uint32) dimmed * 1000 * retValue) / 100000;
	}

	*pCurrentCycle = retValue;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	tui__hwBlinkOutput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function blinks tui-output that is using pwm-signal. 
*
*	\param		pInst			pointer to tui-instance
*	\param		output			tui-output number which tells which pwm-output
*								to blink
*	\param		currentCycle	current default pwm-cycle
*
*	\return		
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void tui__hwBlinkOutput(
	tui__Inst * 			pInst,
	Uint8					output,
	Uint32 					currentCycle
) {
	Uint32					newCycle = 0;
	
	if(pInst->outputState[output] == TUI__OUTSTATE_BLINKING){
		switch(pInst->blinkState)
		{
			case 0:
				newCycle = currentCycle;
			break;

			case 1:
				newCycle = 0;
			break;

			case 2:
				newCycle = currentCycle;
			break;

			case 3:
				newCycle = 0;
			break;
		}
	}
	else if(pInst->outputState[output] == TUI__OUTSTATE_BLINKING2){
		switch(pInst->blinkState)
		{
			case 0:
				newCycle = currentCycle;
			break;

			case 1:
				newCycle = currentCycle;
			break;

			case 2:
				newCycle = 0;
			break;

			case 3:
				newCycle = 0;
			break;
		}
	}

	switch(output)
	{
		case TUI__DIGIOUT_ALARM:
			//newCycle = pwm_getOutputCycle(PWM_OUTPUT_ALARM)==0 ? currentCycle : 0;
			//newCycle = pInst->blinkState==0 ? currentCycle : 0;
			pwm_setOutputCycle(PWM_OUTPUT_ALARM, newCycle);
			break;
		
		case TUI__DIGIOUT_CHARG:
			//newCycle = pwm_getOutputCycle(PWM_OUTPUT_CHARG)==0 ? currentCycle : 0;
			//newCycle = pInst->blinkState==0 ? currentCycle : 0;
			pwm_setOutputCycle(PWM_OUTPUT_CHARG, newCycle);
			break;
			
		case TUI__DIGIOUT_CHARGRDY:
			//newCycle = pwm_getOutputCycle(PWM_OUTPUT_CHARGRDY)==0 ? currentCycle : 0;
			//newCycle = pInst->blinkState==0 ? currentCycle : 0;
			pwm_setOutputCycle(PWM_OUTPUT_CHARGRDY, newCycle);
			break;
			
		case TUI__DIGIOUT_STANDBY:
			//newCycle = pwm_getOutputCycle(PWM_OUTPUT_STANDBY)==0 ? currentCycle : 0;
			//newCycle = pInst->blinkState==0 ? currentCycle : 0;
			pwm_setOutputCycle(PWM_OUTPUT_STANDBY, newCycle);
			break;
	}
}
