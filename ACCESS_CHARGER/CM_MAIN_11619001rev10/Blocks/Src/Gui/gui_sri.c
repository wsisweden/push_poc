/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger selectedPageRow view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

#include "gui_callbackMenu.h"
#include "../IObus/IObusInputs.h"
#include "IObus.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define FIRST_ITEM	1
#define PAGE_ITEMS	8

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

static void DrawRow(gui__Inst* pInst, int item, int edited);
static void drawEditedField(gui__VKey key);
static void initItems(void);

/* Call back functions */
static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {0};
static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	PAGE_ITEMS - FIRST_ITEM,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_F_NONE,
	EDITABLE_ITEMS,
};

static struct{
	IObusInputs_enum signal;
	Boolean hasSubMenu;
	int registerIndex;
} Edited = {0, 0, 0};
static enum {subMenuNone,
			 subMenuInX
} subMenu = subMenuNone;
static int regIndexes[2*8] = {[0 ... 2*8 - 1] = 0};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



PUBLIC Int8 gui__viewSriProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	switch(subMenu){
	case subMenuNone :
		gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);
		break;
	case subMenuInX :
		pView->state |= GUI_MV_ST_SUBMENU;
		if(gui__viewSriScxProc(pInst, pView, msg, pArgs)){
			subMenu = subMenuNone;
			pView->state &= ~GUI_MV_ST_SUBMENU;
		}
		break;
	}

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
static void drawEditedField(
	gui__VKey key
) {
		switch(key){
		case GUI_VKEY_UP :
			if(Edited.signal < IObusInput_Length - 1){
				Edited.signal += 1;
			}
			else{
				Edited.signal = 0;
			}
			break;
		case GUI_VKEY_DOWN :
			if(Edited.signal > 0){
				Edited.signal -= 1;
			}
			else{
				Edited.signal = IObusInput_Length - 1;
			}
			break;
		}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawRow(
	gui__Inst *				pInst,
	int item,
	int edited
) {
	{
		const char* text = "IN";
		gui__dispDrawText(pInst, (Uint8 const_P *)text, 2);									// Text
	}
	dispPrintf(pInst, (regIndexes[item] - 1)%4 + 1, 0, -1, 1);								// Pin number
	{
		const char card = 0x41 + (regIndexes[item] - 1)/4;
		gui__dispDrawText(pInst, (Uint8 const_P *)&card, 1);								// Text
	}
}


static void callback_init(gui__CallbackView* pV, gui__Inst * pInst){
	initItems();
	/* Reset row selected */
	pV->selectedPage = 0;
	pV->selectedPageRow = 0;
	pV->selectedItem = 0;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	initItems();
	gui__DrawItems(pInst, pView, DrawRow, &callbackParameters);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
	if(callbackParameters.items){
		drawEditedField(key);
		gui__viewInvalidate(pView);
	}
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	int startEdit = 0;

	if(pView->selectedPageRow != GUI_NAV_INVALID){
		if(regIndexes[item]){
			subMenu = subMenuInX;
			gui__viewSriScxChange(regIndexes[item] - 1);

			/*
			 *	Store page and row from present view before entering new.
			 */
			//pInst->nav.stack[pInst->nav.stack_pos].page = pView->selectedPage;
			pInst->nav.stack[pInst->nav.stack_pos].pos = pView->selectedPageRow;
			pInst->nav.stack_pos++;
			pView->selectedPageRow = 0;												// Start from top row
			pView->selectedItem = 0;
		}
	}
	return startEdit;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	IoBusInRegister_type Select = {0};

	Select.signal = Edited.signal;
	IoBusRegIn_aPut(Edited.registerIndex, Select);
	initItems();

	return 1;
}

static void initItems(void){
	int n;
	int count = 0;
	const int len = 4*IObusGetChainLen();

	for(n = 0; n < len && n < 8; n++){
		regIndexes[count] = n + 1;
		count++;
	}

	callbackParameters.items = count;
}

PUBLIC void gui__viewSriResetView(void){
	subMenu = subMenuNone;
}
