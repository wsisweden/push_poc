/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger remote output BBC settings view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

#include "gui_callbackMenu.h"
#include "../IObus/IObusOutputs.h"
#include "IObus.h"
#include "../Chalg/inc_cm3/caiNames.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define FIRST_ITEM	1
#define PAGE_ITEMS	8

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

static void DrawRow(gui__Inst* pInst, int item, int edited);

/* Call back functions */
static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {3,'s','c','b'};
static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	PAGE_ITEMS - FIRST_ITEM,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_SINGLE,
	EDITABLE_ITEMS,
};

static struct{
	int stage;
	IoBusOutRegister_type reg;
	int registerIndex;
} Edited = {0, {0}, 0};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



PUBLIC Int8 gui__viewSroScbProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	callbackParameters.items = OutputBBC_Length;
	return gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawRow(
	gui__Inst *				pInst,
	int item,
	int edited
) {
	BYTE const_P * 			txt;
	Uint8					txtLen;

	txt = gui__regToEnumText(gui_RemoteOutBBC_var, item, &txtLen, GUI_NO_TEXT);

	gui__dispDrawText(pInst, (Uint8 const_P *)txt, txtLen);						// Text
	{
		int n;

		for(n = 0; n < 21 - 3 - txtLen; n++){
			Uint8 space = ' ';
			gui__dispDrawText(pInst, &space, 1);
		}
	}

	{
		/*
		 * Build checkbox text
		 */
		const char* unMarked = "[ ]";
		const char* marked = "[X]";
		const char* text;


		if(Edited.reg.BBCSettings == item){
			text = marked;
		}
		else{
			text = unMarked;
		}

		gui__dispDrawText(pInst, (Uint8 const_P *)text, 3);						// Checkbox
	}

	{
		/*
		 * Build text for sub menu header
		 */
		BYTE *buffer1;
		BYTE *buffer2;
		BYTE const_P * subText = txt_localText(pInst->instid, txt_i_tSettings);
		Uint8 len = gui__txtLen(subText);

		buffer1 = (BYTE*)subHeaderTxt;
		buffer2 = (BYTE*)subText;

		len = gui__textCopy(buffer1, buffer2, len+1);											// Copy text Settings to subHeaderText
	}
}

static void callback_init(gui__CallbackView* pV, gui__Inst * pInst){
	/* Restore row selected */
	pV->selectedPage = pInst->nav.stack[pInst->nav.stack_pos - 1].page;
	pV->selectedPageRow = pInst->nav.stack[pInst->nav.stack_pos - 1].pos;
	pV->selectedItem = pInst->nav.stack[pInst->nav.stack_pos - 1].pos;
	return;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	gui__DrawItems(pInst, pView, DrawRow, &callbackParameters);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	if(gui__navCheckAccess(pInst, TRUE)){
		Edited.reg.BBCSettings = item;
		IoBusReg_aPut(Edited.registerIndex, Edited.reg);
		return 1;
	}
	return 0;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	return 1;
}

void gui__viewSroScbChange(int item){
	Edited.registerIndex = item;
	Edited.reg = IoBusReg_aGet(item);
}
