/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger selectedPageRow view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

#include "gui_callbackMenu.h"
#include "../IObus/IObusInputs.h"
#include "IObus.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define FIRST_ITEM	1
#define PAGE_ITEMS	8

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

static void DrawRow(gui__Inst* pInst, int item, int edited);
static void drawEditedField(gui__VKey key);
static Boolean hasSubMenu(IObusInputs_enum signal);
static void initItems(void);

/* Call back functions */
static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {3,'s','c','x'};
static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	PAGE_ITEMS - FIRST_ITEM,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_F_NONE,
	EDITABLE_ITEMS,
};

static struct{
	IObusInputs_enum signal;
	Boolean hasSubMenu;
	int registerIndex;
} Edited = {0, 0, 0};
static enum {subMenuNone
} subMenu = subMenuNone;
static int regIndexes[2*8] = {[0 ... 2*8 - 1] = 0};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



PUBLIC Int8 gui__viewSriScxProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	int result = 0;

	switch(subMenu){
	case subMenuNone :
		result = gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);
		break;
	}

	return result;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
static void drawEditedField(
	gui__VKey key
) {
		switch(key){
		case GUI_VKEY_UP :
			if(Edited.signal < IObusInput_Length - 1){
				Edited.signal += 1;
			}
			else{
				Edited.signal = 0;
			}
			break;
		case GUI_VKEY_DOWN :
			if(Edited.signal > 0){
				Edited.signal -= 1;
			}
			else{
				Edited.signal = IObusInput_Length - 1;
			}
			break;
		}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawRow(
	gui__Inst *				pInst,
	int item,
	int edited
) {
	IObusInputs_enum signal;

	if(regIndexes[item]){
		if(edited){
			signal = Edited.signal;
		}
		else{
			const IoBusInRegister_type Select = IoBusRegIn_aGet(regIndexes[item] - 1);
			signal = Select.signal;
		}
	}
	else{
		BYTE const_P * txt = txt_localText(pInst->instid, txt_i_tSettings);
		Uint8 len = gui__txtLen(txt);
		gui__dispDrawText(pInst, (Uint8 const_P *)&txt[1], len);								// Settings text
		gui_DrawSpace(pInst, 21 - len);
		return;
	}

	{
		BYTE const_P * txt = txt_localText(pInst->instid, txt_i_tFunction);
		Uint8 len = gui__txtLen(txt);
		gui__dispDrawText(pInst, (Uint8 const_P *)&txt[1], len);								// Function text
	}

	if(item < 8){
		Uint8 textLen;
		BYTE const_P * text;
		text = gui__regToEnumText(0, signal, &textLen, txt_localHandle(pInst->instid, txt_i_tPinInSelect));

		gui_DrawSpace(pInst, 21 - 8 - textLen);

		if(edited){
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);								// Invert to mark pointer
			gui__dispDrawText(pInst, (Uint8 const_P *)text, textLen);						// Text
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);								// Remove invert
		}
		else{
			gui__dispDrawText(pInst, (Uint8 const_P *)text, textLen);						// Text
		}
	}
	else{
		const char* text = " out of register";

		gui__dispDrawText(pInst, (Uint8 const_P *)text, 16);
	}

	{
		/*
		 * Build text for sub menu header
		 */
		const char* text = "IN";															// Text IN
		const BYTE index = 0x30 + (regIndexes[item] - 1)%4 + 1;								// Index 1 to 5
		const BYTE card = 0x41 + (regIndexes[item] - 1)/4;									// Card A-D
		BYTE *buffer1;
		BYTE *buffer2;
		Uint8 len;

		len = 0;
		buffer1 = (BYTE*)&subHeaderTxt[1];													// Set buffer1 to point at subHeaderText
		buffer2 = (BYTE*)text;
		len += gui__textCopy(&buffer1[len], buffer2, 2);									// Add text RE
		buffer2 = (BYTE*)&index;
		len += gui__textCopy(&buffer1[len], buffer2, 1);									// Add text index
		buffer2 = (BYTE*)&card;
		len += gui__textCopy(&buffer1[len], buffer2, 1);									// Add text card

		subHeaderTxt[0] = len;																// Set length of subHeaderText
	}
}


static void callback_init(gui__CallbackView* pV, gui__Inst * pInst){
	initItems();
	/* Reset row selected */
	pV->selectedPage = 0;
	pV->selectedPageRow = 0;
	pV->selectedItem = 0;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	initItems();
	gui__DrawItems(pInst, pView, DrawRow, &callbackParameters);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
	if(callbackParameters.items){
		drawEditedField(key);
		gui__viewInvalidate(pView);
	}
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	int startEdit = 0;

	pInst->nav.page = pView->selectedPage;
	pInst->nav.item = pView->selectedPageRow;

	if(regIndexes[item]){
		if(gui__navCheckAccess(pInst, TRUE)){
			startEdit = 1;
			const IoBusInRegister_type Select = IoBusRegIn_aGet(regIndexes[item] - 1);

			Edited.signal = Select.signal;
			Edited.registerIndex = regIndexes[item] - 1;

			if(hasSubMenu(Select.signal)){
				Edited.hasSubMenu = TRUE;
			}
			else{
				Edited.hasSubMenu = FALSE;
			}
		}
	}
	else{
		const IoBusInRegister_type Select = IoBusRegIn_aGet(regIndexes[item - 1] - 1);

		switch(Select.signal){
			case IObusInput_Disabled :
			case IObusInput_StartStop :
			case IObusInput_Stop :
			case IObusInput_HighLow :
				break;
			case IObusInput_Length :															// Should not be set but needed for static calculation of length or last element
				break;
		}
		if(subMenu){
			/*
			 *	Store page and row from present view before entering new.
			 */
			//pInst->nav.stack[pInst->nav.stack_pos].page = pView->selectedPage;
			pInst->nav.stack[pInst->nav.stack_pos].pos = pView->selectedPageRow;	//pView->selectedItem;
			pInst->nav.stack_pos++;
			pView->selectedPageRow = 0;												// Start from top row
			pView->selectedItem = 0;
		}
	}

	return startEdit;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	IoBusInRegister_type Select = {0};

	Select.signal = Edited.signal;
	IoBusRegIn_aPut(Edited.registerIndex, Select);
	initItems();

	return 1;
}

static Boolean hasSubMenu(IObusInputs_enum signal){
	Boolean out = FALSE;

	switch(signal){
		case IObusInput_Disabled :
		case IObusInput_StartStop :
		case IObusInput_Stop :
		case IObusInput_HighLow :
			break;
		case IObusInput_Length :															// Should not be set but needed for static calculation of length or last element
			break;;
	}

	return out;
}

static void initItems(void){
	int n = Edited.registerIndex;
	int count = 0;
	const int len = 4*IObusGetChainLen();

	if( n < len && n < 8){
		const IoBusInRegister_type Select = IoBusRegIn_aGet(n);

		regIndexes[count] = n + 1;
		count++;
		if(hasSubMenu(Select.signal)){
			regIndexes[count] = 0;
			count++;
		}
	}

	callbackParameters.items = count;

}

void gui__viewSriScxChange(int item){
	Edited.registerIndex = item;
}

PUBLIC void gui__viewSriScxResetView(void){
	subMenu = subMenuNone;
}
