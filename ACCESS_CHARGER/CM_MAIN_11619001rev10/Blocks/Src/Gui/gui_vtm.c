/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Time table view handling.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "rtc.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define GUI_TV_ITEMS		3

#define GUI_TV_EDIT_CTRL	0
#define GUI_TV_EDIT_FROM	1
#define GUI_TV_EDIT_TO		2

#define GUI_TV_CTRL			0
#define GUI_TV_CTRL_WIDTH	5
#define GUI_TV_FROM			GUI_TV_CTRL_WIDTH
#define GUI_TV_FROM_WIDTH	8
#define GUI_TV_TO			GUI_TV_FROM + GUI_TV_FROM_WIDTH
#define GUI_TV_TO_WIDTH		8


/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gui__viewDayUpd(gui__Inst * pInst, gui__TimeView * pView, Uint8 idx);

PRIVATE void gui__viewTimeDrawHeader(gui__Inst * pInst, gui__TimeView * pV);
PRIVATE void gui__viewTimeDrawTimes(gui__Inst * pInst, gui__TimeView * pV);
//PRIVATE void gui__viewTimeFieldNav(gui__Inst * pInst, gui__TimeView * pV, gui__NavDir dir);
PRIVATE void gui__viewTimeFromAscii(gui__Inst * pInst, gui__TimeView * pV);

PRIVATE void gui__viewTimeStartEdit(gui__Inst * pInst, gui__MultiViewBase * pV);
PRIVATE void gui__viewTimeEndEdit(gui__Inst * pInst, gui__MultiViewBase * pV, Boolean cancel);
PRIVATE void gui__viewTimeEnterField(gui__Inst * pInst, gui__MultiViewBase * pView, Uint8 field);
PRIVATE void gui__viewTimeExitField(gui__Inst * pInst, gui__MultiViewBase * pView, Uint8 field);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Weekday names.
 */
PRIVATE sys_TxtHandle gui__viewTimeDays[] =
{
	txt_i_tMonday,
	txt_i_tTuesday,
	txt_i_tWednesday,
	txt_i_tThursday,
	txt_i_tFriday,
	txt_i_tSaturday,
	txt_i_tSunday
};

/** 
 *	Time view definition for the multiview base.
 */
PRIVATE gui__MultiViewInit gui__viewTimeInit =
{
	3,								/* Total number of items				*/
	5,								/* Max items per page					*/
	3,								/* Number of edit fields per row		*/
	0,								/* Max page index						*/
	gui__viewTimeStartEdit,			/* Start edit function ptr				*/
	gui__viewTimeEndEdit,			/* End edit function ptr				*/
	gui__viewTimeEnterField,		/* Enter field function ptr				*/
	gui__viewTimeExitField			/* Exit field function ptr				*/

};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Time table view message handler.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to process view
* \param		msg			view message id
* \param		pArgs		ptr to message args
*
* \return		Int8
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewTimeProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__TimeView *			pV = (gui__TimeView*)pView;

	switch (msg)
	{
	/*
	 *	Create message.
	 */
	case gui_msg_create:
		break;

	/*
	 *	Initialization message.
	 */
	case gui_msg_init:
		{
			pV->day = *(Uint8 *)pArgs;

			gui__viewMultiInit((gui__MultiViewBase *)pView, &gui__viewTimeInit);
		}
		break;

	/*
	 *	View closing.
	 */
	case gui_msg_close:
		if (pView->state & GUI_MV_ST_EDITING)
		{
			gui__viewTimeEndEdit(pInst, (gui__MultiViewBase *)pView, TRUE);
		}
		break;

	/*
	 *	View timer timed out.
	 */
	case gui_msg_timeout: 
		break;

	/*
	 *	Paint message.
	 */
	case gui_msg_paint:
		{
			gui__dispClear(pInst);

			gui__viewTimeDrawHeader(pInst, pV);
			gui__viewTimeDrawTimes(pInst, pV);

			gui__viewSetPainted(pView);
		}
		break;

	/*
	 *	Key down event
	 */
	case gui_msg_key_down:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_UP:
				gui__viewMultiNavUp(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_DOWN:
				gui__viewMultiNavDown(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_LEFT:
				gui__viewMultiNavLeft(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_RIGHT:
				gui__viewMultiNavRight(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_ESC:
			case GUI_VKEY_OK:
			default:
				break;
			}
		}
		break;

	/*
	 *	Key up event
	 */
	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_ESC:
				gui__viewMultiNavEsc(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_OK:
				gui__viewMultiNavOk(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_UP:
			case GUI_VKEY_DOWN:
			case GUI_VKEY_LEFT:
			case GUI_VKEY_RIGHT:
			default:
				break;
			}
		}
		break;

	default:
		break;
	}

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws permanent header texts.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeDrawHeader(
	gui__Inst *				pInst,
	gui__TimeView *			pV
) {
	/*
	 *	Draw day name
	 */

	gui__fontSelect(pInst, GUI_FNT_8x8);

	gui__dispSetTextPos(pInst, 0, 0);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, gui__viewTimeDays[pV->day]),
		0, 
		pInst->disp.chr_per_line,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

	/*
	 *	Draw 'Charging not allowed'
	 */
	gui__fontSelect(pInst, GUI_FNT_6x8);

	gui__dispSetTextPos(pInst, 0, 1);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tChargingNotAllowed),
		0, 
		pInst->disp.chr_per_line,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

	/*
	 *	Draw 'Ctrl'
	 */
	gui__dispSetTextPos(pInst, GUI_TV_CTRL, 2);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tCtrl),
		0, 
		GUI_TV_CTRL_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI_TV_CTRL_WIDTH);

	/*
	 *	Draw 'From'
	 */
	gui__dispSetTextPos(pInst, GUI_TV_FROM, 2);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tFrom),
		0, 
		GUI_TV_FROM_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI_TV_FROM_WIDTH);

	/*
	 *	Draw 'To'
	 */
	gui__dispSetTextPos(pInst, GUI_TV_TO, 2);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tTom),
		0, 
		GUI_TV_TO_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI_TV_TO_WIDTH);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws 'Ctrl' value to current buffer.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*	\param		val			enum value of ctrl to write
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeDrawCtrl(
	gui__Inst *				pInst,
	gui__TimeView *			pV,
	Uint8					val
) {
	BYTE const_P *			pTxt;
	Uint8					len;

	pTxt = gui__regToEnumText(gui_Ctrl, val, &len, GUI_NO_TEXT);

	gui__textWriteConst(pInst, pTxt, 0, len);
	gui__textWriteChars(pInst, ' ', len, GUI_TV_CTRL_WIDTH - len); 
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws 'From' value to current buffer.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*	\param		hour		hour value
*	\param		min			minutes value
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeDrawFrom(
	gui__Inst *				pInst,
	gui__TimeView *			pV,
	Uint8					hour,
	Uint8					min
) {
	Uint8					len;

	len = gui__frmtTime2ToAsc(
		pInst->dt_format, 
		&pInst->text.pCurrent[GUI_TV_FROM], 
		hour | (min << 8)
	);

	gui__textWriteChars(pInst, ' ', GUI_TV_FROM + len, GUI_TV_FROM_WIDTH - len); 
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws 'From' value to current buffer.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*	\param		hour		hour value
*	\param		min			minutes value
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeDrawTo(
	gui__Inst *				pInst,
	gui__TimeView *			pV,
	Uint8					hour,
	Uint8					min
) {
	Uint8					len;

	len = gui__frmtTime2ToAsc(
		pInst->dt_format, 
		&pInst->text.pCurrent[GUI_TV_TO], 
		hour | (min << 8)
	);

	gui__textWriteChars(pInst, ' ', GUI_TV_TO + len, GUI_TV_TO_WIDTH - len); 
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws single line when not editing.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*	\param		idx			line index (0 - 2)
*	\param		invert		if true line will be drawn inverted
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeDrawTime(
	gui__Inst *				pInst,
	gui__TimeView *			pV,
	Uint8					idx, 
	Boolean					invert
) {
	Uint8					tmp1;
	Uint8					tmp2;
	Uint8					base;

	base = (pV->day * 3) + idx;

	reg_aGet(&tmp1, gui_Ctrl, base);
	gui__viewTimeDrawCtrl(pInst, pV, tmp1);

	reg_aGet(&tmp1, gui_FromHour, base);
	reg_aGet(&tmp2, gui_FromMin, base);
	gui__viewTimeDrawFrom(pInst, pV, tmp1, tmp2);

	reg_aGet(&tmp1, gui_ToHour, base);
	reg_aGet(&tmp2, gui_ToMin, base);
	gui__viewTimeDrawTo(pInst, pV, tmp1, tmp2);


	if (invert)
	{
		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
	else
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}

	gui__dispSetTextPos(pInst, 0, 3 + idx);
	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

	if (invert)
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws edited value to current buffer. If value is shorter than
*				reserved space the rest if filled with spaces.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*	\param		pos			starting position in the buffer
*	\param		width		value width
*
*	\return		Written value length.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Uint8 gui__viewTimeDrawEdited(
	gui__Inst *				pInst,
	gui__TimeView *			pV,
	Uint8					pos,
	Uint8					width
) {
	Uint8					len = 0;

	while (len < pInst->edit.frmt_len)
	{
		pInst->text.pCurrent[pos++] = pInst->edit.buffer.str[len++];
	}

	while (len < width)
	{
		pInst->text.pCurrent[pos++] = ' ';
		len++;
	}

	return len;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws line being edited.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*	\param		idx			line index (0 - 2)
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeDrawEditTime(
	gui__Inst *				pInst,
	gui__TimeView *			pV,
	Uint8					idx
) {
	Uint8					edit_start;
	Uint8					edit_len;

	/*
	 *	Draw ctrl to buffer
	 */
	if (pV->base.edit_pos == GUI_TV_EDIT_CTRL)
	{
		gui__viewTimeDrawEdited(pInst, pV, GUI_TV_CTRL, GUI_TV_CTRL_WIDTH);
		edit_start = 0;
	}
	else
	{
		gui__viewTimeDrawCtrl(pInst, pV, pV->ctrl);
	}

	/*
	 *	Draw 'From' to buffer
	 */
	if (pV->base.edit_pos == GUI_TV_EDIT_FROM)
	{
		gui__viewTimeDrawEdited(pInst, pV, GUI_TV_FROM, GUI_TV_FROM_WIDTH);
		edit_start = GUI_TV_FROM + pInst->edit.edit_pos;
	}
	else
	{
		gui__viewTimeDrawFrom(pInst, pV, pV->hour_from, pV->min_from);
	}

	/*
	 *	Draw 'To' to buffer
	 */
	if (pV->base.edit_pos == GUI_TV_EDIT_TO)
	{
		gui__viewTimeDrawEdited(pInst, pV, GUI_TV_TO, GUI_TV_TO_WIDTH);
		edit_start = GUI_TV_TO + pInst->edit.edit_pos;
	}
	else
	{
		gui__viewTimeDrawTo(pInst, pV, pV->hour_to, pV->min_to);
	}

	gui__dispSetTextPos(pInst, 0, 3 + idx);

	edit_len = pInst->edit.edit_width;

	/*
	 *	Draw text before the field being edited
	 */
	if (edit_start > 0)
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
		gui__dispDrawText(pInst, pInst->text.pCurrent, edit_start);
	}

	/*
	 *	Draw field being edited
	 */
	gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
	gui__dispDrawText(pInst, &pInst->text.pCurrent[edit_start], edit_len);
	gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	
	/*
	 *	Draw text after the field being edited
	 */
	edit_start += edit_len;

	gui__dispDrawText(
		pInst, 
		&pInst->text.pCurrent[edit_start], 
		pInst->disp.chr_per_line - edit_start
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws all lines (0 - 2).
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeDrawTimes(
	gui__Inst *				pInst,
	gui__TimeView *			pV
) {
	Uint8					item;

	for (item = 0; item < GUI_TV_ITEMS; item++)
	{
		if ((pV->base.base.state & GUI_MV_ST_EDITING) && (item == pV->base.selection))
		{
			/*
			 *	Line is being edited.
			 */
			gui__viewTimeDrawEditTime(pInst, pV, item);
		}
		else
		{
			/*
			 *	Normal line.
			 */
			gui__viewTimeDrawTime(pInst, pV, item, item == pV->base.selection ? TRUE : FALSE);
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reads register values to internal registers.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view
*	\param		idx			line index (0 - 2)
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewDayUpd(
	gui__Inst *				pInst,
	gui__TimeView *			pView,
	Uint8					idx
) {
	Uint8					base;

	base = (pView->day * 3) + idx;

	reg_aGet(&pView->ctrl, gui_Ctrl, base);
	reg_aGet(&pView->hour_to, gui_ToHour, base);
	reg_aGet(&pView->min_to, gui_ToMin, base);
	reg_aGet(&pView->hour_from, gui_FromHour, base);
	reg_aGet(&pView->min_from, gui_FromMin, base);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialized editing of an edit item.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*	\param		item		edit item index (ctrl, from, to)
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeEditInit(
	gui__Inst *				pInst,
	gui__TimeView *			pV,
	Uint8					item
) {
	sys_RegHandle			reg;
	Uint32					flags;

	/*
	 *	Put the value into register buffer from where it will be converted into
	 *	a string by the editing code.
	 */
	switch (item)
	{
	case GUI_TV_EDIT_CTRL:
		reg = gui_Ctrl;
		pInst->buffer.reg.u32 = pV->ctrl;
		break;

	case GUI_TV_EDIT_FROM:
		{
			reg = gui_FromHour;

			pInst->buffer.reg.u32 = pV->hour_from;
			pInst->buffer.reg.u32 |= pV->min_from << 8;
		}
		break;

	case GUI_TV_EDIT_TO:
		{
			reg = gui_ToHour;

			pInst->buffer.reg.u32 = pV->hour_to;
			pInst->buffer.reg.u32 |= pV->min_to << 8;
		}
		break;

	default:
		return;
	}

	pV->base.edit_pos = item;

	flags = GUI_PRM_F_NONE|(GUI_ED_TIME2 << GUI_PRM__F_BITS);

	/* Initialize editing data */
	gui__editInitEx(pInst, reg, 0, flags, NULL);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts editing the current line.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeStartEdit(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	pView->base.state |= GUI_MV_ST_EDITING;

	/* Update values to buffers */
	gui__viewDayUpd(pInst, (gui__TimeView *)pView, pView->selection);

	gui__viewTimeEditInit(pInst, (gui__TimeView *)pView, GUI_TV_EDIT_CTRL);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Ends edit.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view
*	\param		cancel		if TRUE edit is cancelled and no values are stored
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeEndEdit(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView,
	Boolean					cancel
) {
	Boolean					valid = TRUE;

	if (!cancel)
	{
		gui__TimeView *		pV = (gui__TimeView *)pView;
		Uint8				hour_to;

		gui__viewTimeFromAscii(pInst, pV);

		hour_to = pV->hour_to;

		/*
		 *	If TO time is 00:00 it is interpret to mean the end of the day.
		 */
		if ((hour_to == 0) && (pV->min_to == 0))
		{
			hour_to = 24;
		}

		/*
		 *	Check that end time is after the start time.
		 */
		if (hour_to < pV->hour_from)
		{
			valid = FALSE;
		}
		else if (hour_to == pV->hour_from)
		{
			if (pV->min_to <= pV->min_from)
			{
				valid = FALSE;
			}
		}

		if (!valid)
		{
			/*
			 *	Validation failed, show popup with error message.
			 */
			gui__PopupViewArgs	args;

			args.delay = 30;
			args.text1 = txt_localHandle(pInst->instid, txt_i_tTimeError);
			args.text2 = GUI_NO_TEXT;
			args.on_exit = NULL;

			gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
		}
		else
		{
			/*
			 *	Store values to registers
			 */
			Uint8					base;

			base = (pV->day * 3) + pView->selection;

			reg_aPut(&pV->ctrl, gui_Ctrl, base);
			reg_aPut(&pV->hour_to, gui_ToHour, base);
			reg_aPut(&pV->min_to, gui_ToMin, base);
			reg_aPut(&pV->hour_from, gui_FromHour, base);
			reg_aPut(&pV->min_from, gui_FromMin, base);
		}
	}

	if (valid)
	{
		pView->base.state &= ~GUI_MV_ST_EDITING;
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Field enter event function when editing. Called by the multiview
*				code handler.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		field		field index
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeEnterField(
	gui__Inst *				pInst, 
	gui__MultiViewBase *	pView, 
	Uint8					field
) {
	gui__viewTimeEditInit(pInst, (gui__TimeView *)pView, field);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Field exit event function when editing. Called by the multiview
*				code handler.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		field		field index
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeExitField(
	gui__Inst *				pInst, 
	gui__MultiViewBase *	pView, 
	Uint8					field
) {
	gui__viewTimeFromAscii(pInst, (gui__TimeView *)pView);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts time value from ASCII to internal data.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewTimeFromAscii(
	gui__Inst *				pInst,
	gui__TimeView *			pV
) {
	if (pV->base.edit_pos == GUI_TV_EDIT_CTRL)
	{
		pV->ctrl = pInst->edit.edit_pos;
	}
	else
	{
		RTC_Time	tm;

		if (gui__ascToTimeStruct(pInst->edit.buffer.str, pInst->dt_format, &tm, 23))
		{
			if (pV->base.edit_pos == GUI_TV_EDIT_FROM)
			{
				pV->hour_from = (Uint8)tm.tm_hour;
				pV->min_from = (Uint8)tm.tm_min;
			}
			else if (pV->base.edit_pos == GUI_TV_EDIT_TO)
			{
				pV->hour_to = (Uint8)tm.tm_hour;
				pV->min_to = (Uint8)tm.tm_min;
			}
		}
	}
}