/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Code for handling navigation in a view with multiple editable
*				items on the same row (time restriction / can network setup).
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gui__viewMultiFieldNav(gui__Inst * pInst, gui__MultiViewBase * pV, gui__NavDir dir);
PRIVATE void gui__viewMultiUpdPage(gui__MultiViewBase * pView);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/




/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes view variables.
*
*	\param		pView		ptr to the view
*	\param		pInfo		ptr to the init info
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__viewMultiInit(
	gui__MultiViewBase *	pView,
	gui__MultiViewInit const_P * pInfo
) {
	pView->pInfo = pInfo;

	pView->edit_pos = GUI__INV_EDIT_POS;
	pView->page = 0;
	pView->selection = 0;

	if (pInfo->items < pInfo->items_per_page)
	{
		pView->last_item = pInfo->items;
	}
	else
	{
		pView->last_item = pInfo->items_per_page;
	}
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles OK button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__viewMultiNavOk(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	if (pView->base.state & GUI_MV_ST_EDITING)
	{
		/* Accept edit */
		pView->pInfo->end_edit(pInst, pView, FALSE);
		gui__viewInvalidate(pView);
	}
	else
	{
		/* Start editing */
		if (gui__navCheckAccess(pInst, TRUE))
		{
			pView->pInfo->start_edit(pInst, pView);
			gui__viewInvalidate(pView);
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles ESC button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__viewMultiNavEsc(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	if (pView->base.state & GUI_MV_ST_EDITING)
	{
		/* Cancel edit */
		pView->pInfo->end_edit(pInst, pView, TRUE);
		gui__viewInvalidate(pView);
	}
	else
	{
		/* Move back in menu */
		if (gui__navigate(pInst, gui_nav_out) == GUI_NAV_OK)
		{
			gui__viewInvalidate(pView);
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles up button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__viewMultiNavUp(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	if (pView->base.state & GUI_MV_ST_EDITING)
	{
		if (gui__editNavigate(pInst, gui_nav_up))
		{
			gui__viewInvalidate(pView);
		}
	}
	else
	{
		if (pView->selection > 0)
		{
			pView->selection--;
			gui__viewInvalidate(pView);
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles down button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__viewMultiNavDown(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	if (pView->base.state & GUI_MV_ST_EDITING)
	{
		/*
		 *	Decrement edit value
		 */
		if (gui__editNavigate(pInst, gui_nav_down))
		{
			gui__viewInvalidate(pView);
		}
	}
	else
	{
		Uint8					next;

		next = pView->selection + 1;

		if (next < pView->last_item)
		{
			pView->selection = next;
			gui__viewInvalidate(pView);
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles left button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__viewMultiNavLeft(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	if (pView->base.state & GUI_MV_ST_EDITING)
	{
		/*
		 *	Navigate left in the edit value
		 */
		if (gui__editNavigate(pInst, gui_nav_left))
		{
			/* Navigation inside field successful */
			gui__viewInvalidate(pView);
		}
		else
		{
			gui__viewMultiFieldNav(pInst, pView, gui_nav_left);
		}
	}
	else if (pView->page > 0)
	{
		pView->page--;
		pView->selection = 0;

		gui__viewMultiUpdPage(pView);

		gui__viewInvalidate(pView);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles right button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__viewMultiNavRight(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	if (pView->base.state & GUI_MV_ST_EDITING)
	{
		/*
		 *	Navigate right in the edit value
		 */
		if (gui__editNavigate(pInst, gui_nav_right))
		{
			gui__viewInvalidate(pView);
		}
		else
		{
			gui__viewMultiFieldNav(pInst, pView, gui_nav_right);
		}
	}
	else if (pView->page < pView->pInfo->max_page)
	{
		pView->selection = 0;
		pView->page++;

		gui__viewMultiUpdPage(pView);

		gui__viewInvalidate(pView);
	}
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles left / right navigation. Navigates either inside a
*				field or between fields.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		dir			navigaton direction
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewMultiFieldNav(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView,
	gui__NavDir				dir
) {
	Uint8					newpos = 0xFF;

	if (dir == gui_nav_left)
	{
		if (pView->edit_pos > 0)
		{
			newpos = pView->edit_pos - 1;
		}

	}
	else if (dir == gui_nav_right)
	{
		newpos = pView->edit_pos + 1;

		if (newpos >= pView->pInfo->fields)
		{
			newpos = 0xFF;
		}
	}
	else
	{
		deb_assert(FALSE);
	}

	if (newpos != 0xFF)
	{
		pView->pInfo->exit_field(pInst, pView, pView->edit_pos);
		pView->pInfo->enter_field(pInst, pView, newpos);

		if (dir == gui_nav_left)
		{
			while (gui__editNavigate(pInst, gui_nav_right))
			{
				/*
				 *	This makes sure the rightmost field of the new edit value
				 *	is selected when navigating left.
				 */
			}
		}

		gui__viewInvalidate(pView);
	}
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Updates item idexe after page has changed.
*
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewMultiUpdPage(
	gui__MultiViewBase *	pView
) {
	if (pView->page < pView->pInfo->max_page)
	{
		pView->last_item = pView->pInfo->items_per_page;
	}
	else
	{
		Uint8			u8;

		u8 = pView->pInfo->items % pView->pInfo->items_per_page;

		if (u8)
		{
			pView->last_item = u8;
		}
		else
		{
			pView->last_item = pView->pInfo->items_per_page;
		}
	}
}