/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Display interface functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "deb.h"

#include "global.h"


#include "gui.h"
#include "local.h"

#include "st7565.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes display data.
*
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispInitStructs(
	gui__Inst *				pInst
) {
	pInst->disp.pDispInst =  pInst->pInit->disp->initData(pInst->pInit->pDispInit);

	pInst->disp.mode = 0;

	gui__dispSetModeFlag(pInst, DISPLAY_DRAW_VERT);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes actual display, sets backlight on.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispInitDisplay(
	gui__Inst *				pInst
) {
	pInst->pInit->disp->initDisplay(pInst->disp.pDispInst);
	gui__dispBacklight(pInst, TRUE);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Drives the display down, turn off the backlight.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispDown(
	gui__Inst *				pInst
) {
	gui__dispBacklight(pInst, FALSE);
//	pInst->pInit->disp->onoff(pInst->disp.pDispInst, FALSE);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Sets display backlight on / off.
*
* \param		pInst		ptr to GUI instance
* \param		on			if true backlight is set on, else off
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispBacklight(
	gui__Inst *				pInst, 
	Boolean					on
) {
	pInst->pInit->disp->backlight(pInst->disp.pDispInst, on);

	reg_putLocal(&on, reg_i_backlight, pInst->instid);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Sets display contrast.
*
* \param		pInst		ptr to GUI instance
* \param		percent		contrast to set (0 - 100)
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispContrast(
	gui__Inst *				pInst, 
	Uint8					percent
) {
	pInst->pInit->disp->contrast(pInst->disp.pDispInst, percent);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Clears the dispay.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispClear(
	gui__Inst *				pInst
) {
	pInst->pInit->disp->clear(pInst->disp.pDispInst);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Flushes display RAM to hardware display.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispFlush(
	gui__Inst *				pInst
) {
	pInst->pInit->disp->flush(pInst->disp.pDispInst);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Sets drawing mode flag.
*
* \param		pInst		ptr to GUI instance
* \param		mode		flags to set
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispSetModeFlag(
	gui__Inst *				pInst,
	display_Mode			mode
) {
	pInst->disp.mode |= mode;
	pInst->pInit->disp->setmode(pInst->disp.pDispInst, pInst->disp.mode);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Clears drawing mode flag.
*
* \param		pInst		ptr to GUI instance
* \param		mode		flags to clear
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispClearModeFlag(
	gui__Inst *				pInst,
	display_Mode			mode
) {
	pInst->disp.mode &= ~mode;
	pInst->pInit->disp->setmode(pInst->disp.pDispInst, pInst->disp.mode);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Sets drawing position.
*
* \param		pInst		ptr to GUI instance
* \param		x			x coordinate
* \param		y			y coordinate
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispSetPos(
	gui__Inst *				pInst,
	display_Coord			x,
	display_Coord			y
) {
	pInst->disp.x = x;
	pInst->disp.y = y;
	pInst->pInit->disp->setpos(pInst->disp.pDispInst, x, y);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets drawing position using character coordinates.
*
*	\param		pInst		ptr to GUI instance
*	\param		x			character x position
*	\param		y			character y position
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__dispSetTextPos(
	gui__Inst *				pInst,
	display_Coord			x,
	display_Coord			y
) {
	pInst->disp.x = x * pInst->disp.pFont->width;
	pInst->disp.y = y * pInst->disp.pFont->height;
	pInst->pInit->disp->setpos(pInst->disp.pDispInst, pInst->disp.x, pInst->disp.y);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws image to current position.
*
* \param		pInst		ptr to GUI instance
* \param		pImage		ptr to image data
* \param		width		image width
* \param		height		image height
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispDrawImage(
	gui__Inst *				pInst,
	BYTE *					pImage,
	display_Coord			width,
	display_Coord			height
) {
	pInst->pInit->disp->drawImage(pInst->disp.pDispInst, pImage, width, height);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws text to current position.
*
* \param		pInst		ptr to GUI instance
* \param		pTxt		ptr to text buffer
* \param		len			text len in characters
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispDrawText(
	gui__Inst *				pInst,
	BYTE const_P *			pTxt,
	Uint8					len
) {
	Uint8					pos;

	deb_assert(pInst->disp.pFont);

	for (pos = 0; pos < len; pos++)
	{
		pInst->pInit->disp->drawImage(
			pInst->disp.pDispInst,
			gui__fontChar(pInst, pTxt[pos]), 
			pInst->disp.pFont->width, 
			pInst->disp.pFont->height
		);
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws text aligned inside a rectangle.
*
* \param		pInst		ptr to GUI instance
* \param		pTxt		ptr to text buffer
* \param		len			text length in characters
* \param		pRect		drawing rectangle
* \param		flags		alignment flags
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispDrawTextAligned(
	gui__Inst *				pInst,
	BYTE const_P *			pTxt,
	Uint8					len,
	display_Rect *			pRect,
	Uint8					flags
) {
	display_Coord			tmp;
	display_Coord			width;
	display_Coord			height;

	deb_assert(pInst->disp.pFont);

	width = len * pInst->disp.pFont->width;
	height = pInst->disp.pFont->height;

	if (width > pRect->width)
	{
		width = pRect->width;
	}

	if (height > pRect->height)
	{
		height = pRect->height;
	}

	if (flags & GUI_ALIGN_LEFT)
	{
		tmp = pRect->x;
	}
	else if (flags & GUI_ALIGN_RIGHT)
	{
		tmp = (pRect->x + pRect->width) - width;
	}
	else
	{
		deb_assert(flags & GUI_ALIGN_CENTER);

		tmp = pRect->x + (pRect->width - width) / 2;
	}

	gui__dispSetPos(pInst, tmp, pRect->y);
		
	while (width)
	{
		tmp = pInst->disp.pFont->width;
	
		if (tmp > width)
		{
			tmp = width;
		}

		pInst->pInit->disp->drawImage(
			pInst->disp.pDispInst,
			gui__fontChar(pInst, *pTxt), 
			tmp, 
			height
		);

		pTxt++;
		width -= tmp;
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Finds next space from a text buffer starting from given position.
*
* \param		pTxt		ptr to text buffer
* \param		len			text length
* \param		start		starting position
*
* \return		Index of found space of length of text.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
#if 0	// No longer in use
PRIVATE Uint8 gui__dispFindSpace(
	BYTE const_P *			pTxt,
	Uint8					len,
	Uint8					start
) {
	Uint8					pos = start;

	while (pTxt[pos] != ' ')
	{
		if (++pos >= len)
		{
			return len;
		}
	}

	return pos;
}
#endif

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws text aligned inside a rectangle. This versions performs
*				word wrapping if text is longer than rectangle width and
*				rectangle is higher than text height.
*
* \param		pInst		ptr to GUI instance
* \param		pTxt		ptr to text buffer
* \param		len			text length
* \param		pRect		drawing rectangle
* \param		flags		aligment flags
*
* \details		
*
* \note			If flag GUI_TEST_SIZE is set nothing is output but number of 
*				required vertical pixels (assuming width is pRect->width) is 
*				updated to pRect->height. 
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispDrawTextAlignedEx(
	gui__Inst *				pInst,
	BYTE const_P *			pTxt,
	Uint8					len,
	display_Rect *			pRect,
	Uint8					flags
) {
	Uint16					width;
	Uint16					height;

	deb_assert(pInst->disp.pFont);

	width = len * pInst->disp.pFont->width;
	height = pInst->disp.pFont->height;

	if ((width <= pRect->width) || (pRect->height <= height))
	{
		if (flags & GUI_TEST_SIZE)
		{
			pRect->height = pInst->disp.pFont->height;
		}
		else
		{
			/* Text firts to one row or only one row room to draw */
			gui__dispDrawTextAligned(pInst, pTxt, len, pRect, flags);
		}
	}
	else
	{
		display_Rect		rect;
		Uint8				chr_per_row;
		Uint8				rows;
		Uint8				rows_left;

		/*
		 *	Calculate max number of characters per row and max number of rows.
		 */
		chr_per_row = pRect->width / pInst->disp.pFont->width;
		rows = pRect->height / pInst->disp.pFont->height;
		rows_left = rows;

		/*
		 *	Set output rectangle. Only this updated during output is y pos.
		 */
		rect.x = pRect->x;
		rect.width = pRect->width;
		rect.y = pRect->y;
		rect.height = pInst->disp.pFont->height;

		/*
		 *	Output line by line.
		 */
		while (len)
		{
			Uint8			next_pos;

			if ((len < chr_per_row) || (rows_left == 1))
			{
				/* 
				 * Less than one row's worth of characters left or only one row
				 *	left to output => output all that is left (may be truncated).
				 */
				next_pos = len;
			}
			else
			{
				/*
				 *	Do backwards search from current pos + chr per line to find 
				 *	word break.
				 */
				next_pos = chr_per_row;

				while (pTxt[next_pos] != ' ')
				{
					next_pos--;

					if (next_pos == 0)
					{
						/* Word won't fit in one row */
						next_pos = chr_per_row;
						break;
					}
				}
			}

			if ((flags & GUI_TEST_SIZE) == 0)
			{
				gui__dispDrawTextAligned(
					pInst, 
					pTxt, 
					next_pos, 
					&rect, 
					flags
				);
			}

			if ((next_pos < len) && (next_pos != chr_per_row))
			{
				next_pos++; /* To skip the space */
			}

			rect.y += pInst->disp.pFont->height;
			pTxt += next_pos;
			len -= next_pos;
			rows_left--;
		}

		if (flags & GUI_TEST_SIZE)
		{
			pRect->height = (rows - rows_left) * pInst->disp.pFont->height;
		}
	}
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws a filled rectangle to current position.
*
* \param		pInst		ptr to GUI instance
* \param		pRect		drawing rectangle 
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__dispFilledRect(
	gui__Inst *				pInst,
	display_Rect *			pRect
) {
	pInst->pInit->disp->drawFilledRect(pInst->disp.pDispInst, pRect);
}
