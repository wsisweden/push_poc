/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Font handling functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/** 
 *	Language's offset in gui__fonts.
 */
PRIVATE const_P	Uint8 gui__fontMap[] =
{
	0,	/* GUI_LANG_GBR	*/
	0,	/* GUI_LANG_SWE	*/
	0,	/* GUI_LANG_USA	*/
	0,	/* GUI_LANG_DEU	*/
	0,	/* GUI_LANG_ITA	*/
	0,	/* GUI_LANG_SPA	*/
	0,	/* GUI_LANG_POR	*/
	3,	/* GUI_LANG_JPN	*/
};

/**
 *	Fonts table.
 */
PRIVATE const_P gui__Font * gui__fonts[] =
{
	&gui__fontAnsi6x8,
	&gui__fontAnsi8x8,
	&gui__fontAnsi8x16,
	&gui__fontJis6x8,
	&gui__fontJis8x8,
	&gui__fontJis8x16,
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes font data.
*
*
* \param		pInst		Gui instance.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__fontInit(
	gui__Inst *				pInst							
) {
	gui__fontSelect(pInst, 0);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Selects current font.
*
*
* \param		pInst		Gui instance.
* \param		font		Font index.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__fontSelect(
	gui__Inst *				pInst,
	Uint8					font
) {
	deb_assert(font < dim(gui__fonts));
	//deb_assert(pInst->language < dim(gui__fonts));

	if (pInst->language < dim(gui__fontMap))
	{
		font += gui__fontMap[pInst->language];
	}

	if (font < dim(gui__fonts))
	{
		pInst->disp.pFont = gui__fonts[font];

		pInst->disp.chr_per_line = pInst->pInit->width / pInst->disp.pFont->width;
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Returns ptr to font character bitmap from font character.
*
*
* \param		pInst		Gui instance.
* \param		chr			Character index.
*
* \return		Pointer into font bitmap.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC BYTE const_P * gui__fontChar(
	gui__Inst *				pInst,
	Uint16					chr
) {
	Uint16					idx;

	deb_assert(pInst->disp.pFont);

	idx = pInst->disp.pFont->invalid;

	if (chr >= pInst->disp.pFont->offset)
	{
		chr = chr - pInst->disp.pFont->offset;

		if (chr <= pInst->disp.pFont->count)
		{
			idx = chr;
		}
	}

	return &pInst->disp.pFont->pData[idx * pInst->disp.pFont->charsize];
}
