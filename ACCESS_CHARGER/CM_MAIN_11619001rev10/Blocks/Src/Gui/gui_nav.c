/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Menu navigation functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
//#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"

#include "txt.h"

#include "gui.h"
#include "local.h"

#include "gui_callbackMenu.h"
/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef Uint8 (*			gui__navFn)(gui__Inst *);

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gui__navOpenView(gui__Inst * pInst);

PRIVATE Uint8 gui__navRoot(gui__Inst * pInst);
PRIVATE Uint8 gui__navUp(gui__Inst * pInst);
PRIVATE Uint8 gui__navDown(gui__Inst * pInst);
PRIVATE Uint8 gui__navLeft(gui__Inst * pInst);
PRIVATE Uint8 gui__navRight(gui__Inst * pInst);
PRIVATE Uint8 gui__navIn(gui__Inst * pInst);
PRIVATE Uint8 gui__navOut(gui__Inst * pInst);
PRIVATE Uint8 gui__navMain(gui__Inst * pInst);

PRIVATE Uint8 gui__navMenuEnter(gui__Inst * pInst, gui_MenuTable const_P * pTable);
PRIVATE Uint8 gui__navMenuUpdate(gui__Inst * pInst, Uint8 page);

PRIVATE void gui__navBuildPageIndex(gui__Inst * pInst);

PRIVATE Uint8 gui__navCheckNavigable(gui__Inst * pInst, Uint8 idx);
PRIVATE Uint8 gui__navCheckNavigableP(gui__Inst * pInst, gui_MenuItem const_P * pItem);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/** Navigation function table. Must match order in gui__NavDir. */
PRIVATE gui__navFn gui__navFunctions[] =
{
	gui__navRoot,
	gui__navUp,
	gui__navDown,
	gui__navLeft,
	gui__navRight,
	gui__navIn,
	gui__navOut,
	gui__navMain,
};


SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes menu navigation data.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__navInit(
	gui__Inst *				pInst
) {
	pInst->nav.pTable = NULL;
	pInst->nav.last_cnd = GUI_CND_NONE;
	pInst->nav.last_cnd_res = TRUE;

	pInst->nav.item = 0;
	pInst->nav.stack_pos = 0;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates in menu.
*
* \param		pInst	ptr to GUI instance
* \param		dir		navigation direction.
*
* \return		Boolean, TRUE if navigation was successful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__navigate(
	gui__Inst *				pInst,
	gui__NavDir				dir
) {
	Uint8					ret;

	ret = gui__navFunctions[dir](pInst);

	return ret;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Rebuils current menu position by rebuilding menu page index.
*
* \param		pInst	ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__navRebuild(
	gui__Inst *				pInst
) {
	Uint8					first;

	gui__navBuildPageIndex(pInst);
	first = gui__navMenuUpdate(pInst, pInst->nav.page);
//	first = gui__navMenuEnter(pInst, pInst->nav.pTable);

	if (pInst->nav.items[pInst->nav.item] == NULL)
	{
		pInst->nav.page = 0;
		pInst->nav.item = first;
	}
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates to menu root (process view).
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navRoot(
	gui__Inst *				pInst
) {
	pInst->nav.stack_pos = 0;
	gui__viewSroScxResetView();
	gui__viewSroResetView();
	gui__viewSriScxResetView();
	gui__viewSriResetView();
	return gui__navMenuEnter(pInst, pInst->pInit->pMenuroot);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates to main view (one 'down'from process view).
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navMain(
	gui__Inst *				pInst
) {
	if (gui__navRoot(pInst) == GUI_NAV_OK)
	{
		return 	gui__navIn(pInst);
	}

	return GUI_NAV_NACK;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates up inside a page.
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navUp(
	gui__Inst *				pInst
) {
	Uint8					ret = GUI_NAV_NACK;

	if ((pInst->nav.item != GUI_NAV_INVALID) && (pInst->nav.item > 0))
	{
		Uint8				idx;

		idx = pInst->nav.item - 1;

		do 
		{
			ret = gui__navCheckNavigable(pInst, idx);

			if (ret == GUI_NAV_OK)
			{
				pInst->nav.item = idx;
				break;
			}
		} while (idx--);
	}

	return ret;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates down inside a page.
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navDown(
	gui__Inst *				pInst
) {
	Uint8					ret = GUI_NAV_NACK;
	Uint8					idx;

	if (pInst->nav.item != GUI_NAV_INVALID)
	{	
		for (idx = pInst->nav.item + 1; idx < GUI_ITEMS_PER_PAGE; idx++)
		{
			ret = gui__navCheckNavigable(pInst, idx);

			if (ret == GUI_NAV_OK)
			{
				pInst->nav.item = idx;
				break;
			}
		}
	}

	return ret;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates left inside a page.
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navLeft(
	gui__Inst *				pInst
) {
	if (pInst->nav.page > 0)
	{
		pInst->nav.page--;

		pInst->nav.item = gui__navMenuUpdate(pInst, pInst->nav.page);

		return GUI_NAV_OK;
	}

	return GUI_NAV_NACK;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates right inside a page.
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navRight(
	gui__Inst *				pInst
) {
	if (pInst->nav.page < pInst->nav.max_page)
	{
		pInst->nav.page++;

		pInst->nav.item = gui__navMenuUpdate(pInst, pInst->nav.page);

		return GUI_NAV_OK;
	}

	return GUI_NAV_NACK;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates into a sub menu.
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navIn(
	gui__Inst *				pInst
) {
	gui_MenuItem const_P *	pItem;

	deb_assert(pInst->nav.item < GUI_ITEMS_PER_PAGE);

	pItem = pInst->nav.items[pInst->nav.item];

	deb_assert(pItem);
	deb_assert(gui_isMenuRow(pItem));

	return gui__navInToTable(pInst, pItem->subitem);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Navigates into menu table.
*
*	\param		pInst		Ptr to GUI instance.
*	\param		pTable		Ptr to target menu table.
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Uint8 gui__navInToTable(
	gui__Inst *				pInst,
	gui_MenuTable const_P * pTable
) {
	Uint8					ret;

	pInst->nav.stack[pInst->nav.stack_pos].pTable = pInst->nav.pTable;
	pInst->nav.stack[pInst->nav.stack_pos].page = pInst->nav.page;
	pInst->nav.stack[pInst->nav.stack_pos].pos = pInst->nav.item;
	gui__callbackMenuNavigateIn(pInst);

	ret = gui__navMenuEnter(pInst, pTable);

	if (ret == GUI_NAV_OK)
	{
		pInst->nav.stack_pos++;
	}

	return ret;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates out of a sub menu.
*
* \param		pInst	ptr to GUI instance
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navOut(
	gui__Inst *				pInst
) {
	Uint8					ret = GUI_NAV_NACK;

	if (pInst->nav.stack_pos > 0)
	{
		ret = gui__navMenuEnter(pInst, pInst->nav.stack[pInst->nav.stack_pos - 1].pTable);

		if (ret == GUI_NAV_OK)
		{
			pInst->nav.stack_pos--;
			pInst->nav.item = pInst->nav.stack[pInst->nav.stack_pos].pos;
			pInst->nav.page = pInst->nav.stack[pInst->nav.stack_pos].page;

			gui__navMenuUpdate(pInst, pInst->nav.page);
		}
	}
	gui__callbackMenuNavigateOut(pInst);
	return ret;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Enters a menu page.
*
* \param		pInst		ptr to GUI instance
* \param		pTable		ptr to target table.
*
* \return		Navigation result, GUI_NAV_OK if ok.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navMenuEnter(
	gui__Inst *				pInst,
	gui_MenuTable const_P *	pTable
) {
	Boolean					tableswitch = FALSE;

	if (pInst->nav.pTable != pTable)
	{
		tableswitch = TRUE;

		if (
			pInst->nav.pTable &&
			(gui__tblType(pInst->nav.pTable) != GUI_TBL_TYPE_TIME) &&
			gui_tblExit(pInst->nav.pTable)
		) {
			deb_log("GUI: calling exit fn");
			pInst->pInit->pExitFn[gui_tblExit(pInst->nav.pTable) - 1](FALSE);
		}
	}

	pInst->nav.pTable = pTable;

	gui__navBuildPageIndex(pInst);

	pInst->nav.page = 0;
	pInst->nav.item = gui__navMenuUpdate(pInst, 0);

	if (tableswitch)
	{
		gui__navOpenView(pInst);

		if (
			(gui__tblType(pInst->nav.pTable) != GUI_TBL_TYPE_TIME) &&
			gui_tblEnter(pInst->nav.pTable)
		) {
			deb_log("GUI: calling enter fn");
			pInst->pInit->pEnterFn[gui_tblEnter(pInst->nav.pTable) - 1](FALSE);
		}
	}

	return GUI_NAV_OK;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Updates menu page information.
*
* \param		pInst	ptr to GUI instance
* \param		page	page index
*
* \return		Uint8, index of first navigable item inside the page or 
*				GUI_NAV_INVALID.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__navMenuUpdate(
	gui__Inst *				pInst,
	Uint8					page
) {
	Uint8					idx;
	Uint8					pos = 0;
	Uint8					first = GUI_NAV_INVALID;
	Uint8					count;

	/*
	 *	Space left for the header?
	 */
	if (!(pInst->nav.pTable->type & GUI_TBL_NO_HDR))
	{
		pInst->nav.items[pos++] = NULL;
	}

	count = pInst->nav.page_index[page].count;

	for (
		idx = pInst->nav.page_index[page].first_item; 
		count > 0; 
		idx++
	) {
		gui_MenuItem const_P * pItem;

		if (!(pInst->nav.visible & (1 << idx)))
		{
			/* Menu item is not visible => skip it */
			continue;
		}

		count--;

		pItem = &pInst->nav.pTable->pItems[idx];

		if (first == GUI_NAV_INVALID)
		{
			if (gui__navCheckNavigableP(pInst, pItem) == GUI_NAV_OK)
			{
				first = pos;
			}
		}

		/*
		 *	Add items to list of current page items.
		 */
		pInst->nav.items[pos++] = pItem;
	}

	/*
	 *	Fill left over positions with invalid position.
	 */
	while (pos < GUI_ITEMS_PER_PAGE)
	{
		pInst->nav.items[pos++] = NULL;
	}

	/*
	 *	Reset last checked item.
	 */
	pInst->nav.last_cnd = GUI_CND_NONE;
	pInst->nav.last_cnd_res = TRUE;

	return first;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Builds page index by checking visibility conditions and page
*				breaks.
*
* \param		pInst	ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__navBuildPageIndex(
	gui__Inst *				pInst
) {
	Uint8					idx;
	Uint8					page = 0;
	Uint8					items = 0;
	Uint8					max_items;

	if (pInst->nav.pTable->type & GUI_TBL_NO_HDR)
	{
		max_items = GUI_ITEMS_PER_PAGE;
	}
	else
	{
		max_items = GUI_ITEMS_PER_PAGE - 1;
	}

	pInst->nav.page_index[0].first_item = 0xFF;
	pInst->nav.page_index[0].count = 0;
	pInst->nav.visible = 0;

	for (idx = 0; idx < pInst->nav.pTable->count; idx++)
	{
		gui_MenuItem const_P * pItem;

		pItem = &pInst->nav.pTable->pItems[idx];

		if (!gui__cndCheck(pInst, pItem))
		{
			/* Menu item is hidden by condition */
			continue;
		}

		items++;

		pInst->nav.visible |= 1 << idx;

		if (pInst->nav.page_index[page].first_item == 0xFF)
		{
			/* Store first visible menu item */
			pInst->nav.page_index[page].first_item = idx;
		}

		if (
			(pItem->flags & GUI_FLG_PAGE_BREAK) || 
			(items > max_items)
		) {
			/*
			 *	Forced page break or full page's worth of items processed.
			 */
			pInst->nav.page_index[page].count = items - 1;

			page++;
			items = 1;
			pInst->nav.page_index[page].first_item = idx;
		}
	}

	pInst->nav.page_index[page].count = items;
	pInst->nav.max_page = page;

	while (++page < GUI_MAX_PAGES)
	{
		pInst->nav.page_index[page].count = 0;
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Opens a view matching current navigation position.
*
* \param		pInst	ptr to GUI instance.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__navOpenView(
	gui__Inst *				pInst
) {
	Uint8					type;
	void *					pArgs = NULL;
	Uint8					tmp;

	switch (pInst->nav.pTable->type & GUI_TBL_TYPE_MASK)
	{
	/*
	 *	Process view
	 */
	case GUI_TBL_TYPE_PROC:
		type = GUI_VIEW_PROCESS;
		break;

	/*
	 *	Time table vew
	 */
	case GUI_TBL_TYPE_TIME:
		type = GUI_VIEW_TIMETABLE;
		tmp = gui_tblDay(pInst->nav.pTable);
		pArgs = &tmp;
		break;

	/*
	 *	Error log
	 */
	case GUI_TBL_TYPE_ELOG:
		type = GUI_VIEW_LOG;
		break;

	case GUI_TBL_TYPE_CS:
		type = GUI_VIEW_CS;
		break;

	case GUI_TBL_TYPE_SCP:
		type = GUI_VIEW_SCP;
		break;

	case GUI_TBL_TYPE_SRO:
		type = GUI_VIEW_SRO;
		break;

	case GUI_TBL_TYPE_CAN:
		type = GUI_VIEW_CAN;
		break;

	case GUI_TBL_TYPE_SRI:
		type = GUI_VIEW_SRI;
		break;
	/*
	 *	Menu view.
	 */
	default:
		type = GUI_VIEW_MENU;
		break;
	}

	gui__viewOpen(pInst, type, pArgs);
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if login level allows edits in the current table.
*
*	\param		pInst		ptr to GUI instance
*	\param		showpopup	if TRUE warning popup is shown if access is denied
*
*	\return		TRUE is editing is allowed, else FALSE.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Boolean gui__navCheckAccess(
	gui__Inst *				pInst,
	Boolean					showpopup
) {
	if (pInst->login >=gui_tblAccess(pInst->nav.pTable))
	{
		return TRUE;
	}

	if (showpopup)
	{
		Uint8				arg;

		if (gui_tblAccess(pInst->nav.pTable) == GUI_ACCESS_LEVEL1)
		{
			arg = GUI__QRY_LOGIN1;
		}
		else
		{
			deb_assert(gui_tblAccess(pInst->nav.pTable) == GUI_ACCESS_LEVEL2);
			arg = GUI__QRY_LOGIN2;
		}

		gui__viewOpen(pInst, GUI_VIEW_QUERY, &arg);
	}

	return FALSE;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if menu item at given index is navigable.
*
*	\param		pInst		Ptr to GUI instance.
*	\param		idx			Menu item index.
*
*	\return		True if menu item is navigable by user.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Uint8 gui__navCheckNavigable(
	gui__Inst *				pInst,
	Uint8					idx
) {
	return gui__navCheckNavigableP(pInst, pInst->nav.items[idx]);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if menu item at given index is navigable.
*
*	\param		pInst		Ptr to GUI instance.
*	\param		pItem		Menu item info.
*
*	\return		True if menu item is navigable by user.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Uint8 gui__navCheckNavigableP(
	gui__Inst *				pInst,
	gui_MenuItem const_P *	pItem
) {
	Uint8					ret = GUI_NAV_NACK;

	if (pItem != NULL)
	{
		if (pItem->type & GUI_ROW_NAV)
		{
			if (gui_isParamRow(pItem))
			{
				gui_RegInfo info;

				gui__regGetInfo(pItem->reg, &info);

				if (pItem->flags & GUI_PRM_DBL_REG)
				{
					/*
					 *	In case of double register the line is always assumed to
					 *	be writeable.
					 */
					ret = GUI_NAV_OK;
				}
				else if (
					(sys_RegAdditAttr const_P *)info.pInfo &&
					(((sys_RegAdditAttr const_P *)info.pInfo)->status & SYS_MMI_WRITE)
				) {
					ret = GUI_NAV_OK;
				}
			}
			else
			{
				ret = GUI_NAV_OK;
			}
		}
	}

	return ret;
}
