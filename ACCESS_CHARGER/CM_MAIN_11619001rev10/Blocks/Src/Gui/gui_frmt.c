/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Formatting functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "real.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "rtc.h"
#include "lib.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define GUI_FRMT_T_SPACE	0

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint8 gui__frmtInt(gui__Inst * pInst, sys_RegHandle reg, gui_RegInfo * pInfo);
PRIVATE Uint8 gui__frmtReal(gui__Inst * pInst, sys_RegHandle reg, gui_RegInfo * pInfo, Uint8 maxdec);
/*PRIVATE Uint8 gui__frmtMask(gui__Inst * pInst, sys_RegHandle reg, gui_RegInfo * pInfo);*/
PRIVATE Uint8 gui__frmtStr(gui__Inst * pInst, sys_RegHandle reg, gui_RegInfo * pInfo);
PRIVATE Uint8 gui__frmtEnum(gui__Inst * pInst, sys_RegHandle reg, gui_RegInfo * pInfo);
PRIVATE Uint8 gui__frmtBitmaskEnum(gui__Inst * pInst, sys_RegHandle reg, Uint8 bit, sys_TxtHandle bit_text);
PRIVATE Uint8 gui__frmtDynamicEnum(gui__Inst * pInst, sys_RegHandle reg, gui_MenuItem const_P *	pItem);
PRIVATE Uint8 gui__frmtDate(gui__Inst * pInst, sys_RegHandle reg, gui_RegInfo * pInfo);
PRIVATE Uint8 gui__frmtTime(gui__Inst * pInst, sys_RegHandle reg, gui_RegInfo * pInfo);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 *	Multipliers for real value scaling.
 */
PUBLIC Uint32	gui__frmtMultipliers[] =
{
	1,
	10,
	100,
	1000,
	10000,
	100000,
	1000000
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/


/*
 *	Date format strings, order is the same as in the format enum register.
 */
PRIVATE const_P char * gui__frmtStrDate[] =
{
	"YYYY-MM-DD",
	"DD.MM.YYYY",
	"MM/DD/YYYY"
};

/*
 *	Time format strings, order is the same as in the format enum register.
 */
PRIVATE const_P char * gui__frmtStrTime[] =
{
#if GUI_SHOW_SECONDS == 1
	"hh:mm:ss",
	"hh.mm.ss",
#else
	"hh:mm",
	"hh.mm",
#endif
	"hh:mmAA"
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats menu item register value for displaying. Register
*				value is assumed to be in the register buffer and is written to 
*				current text buffer.
*
* \param		pInst		ptr to GUI instance
* \param		pItem		ptr to menu item info
* \param		pInfo		ptr to register info
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__frmtForDisplay(
	gui__Inst *				pInst,
	gui_MenuItem const_P *	pItem,
	gui_RegInfo *			pInfo
) {
	Uint8					type;
	Uint8					ret;

	type = reg_mmiStat(pItem->reg);

	switch (type & REG_MMI_TYPE_MASK) 
	{
		case SYS_MMI_T_INT:
			ret = gui__frmtInt(pInst, pItem->reg, pInfo);
			break;

		case SYS_MMI_T_REAL:
			ret = gui__frmtReal(pInst, pItem->reg, pInfo, (Uint8)pItem->decims);
			break;

		case SYS_MMI_T_ENUM:
			if (pItem->flags & GUI_PRM_BITMASK)
			{
				ret = gui__frmtBitmaskEnum(
					pInst, 
					pItem->reg, 
					pItem->enumval, 
					pItem->text2
				);
			}
			else if (pItem->flags & GUI_PRM_DYN_ENUM)
			{
				ret = gui__frmtDynamicEnum(pInst, pItem->reg, pItem);
			}
			else
			{
				ret = gui__frmtEnum(pInst, pItem->reg, pInfo);
			}
			break;

		case SYS_MMI_T_STR:
			ret = gui__frmtStr(pInst, pItem->reg, pInfo);
			break;
		
		case SYS_MMI_T_TIME:
			ret = gui__frmtTime(pInst, pItem->reg, pInfo);
			break;

		case SYS_MMI_T_DATE:
			ret = gui__frmtDate(pInst, pItem->reg, pInfo);
			break;

		case SYS_MMI_T_DT:
		case SYS_MMI_T_MASK:
		case SYS_MMI_T_NONE:
		default:
			deb_assert(0);
			ret = 0;
			break;
	}

	return ret;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Formats register value into a string using register MMI
*				settings. Register value is assumed to be in the register buffer
*				and is written to current text buffer.
*
*	\param		pInst		ptr to GUI instance
*	\param		reg			register handle
*
*	\return		Number of characters in the edited value.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Uint8 gui__frmtValue(
	gui__Inst *				pInst,
	sys_RegHandle			reg
) {
	Uint8					type;
	gui_RegInfo				info;
	Uint8					ret;

	type = reg_mmiStat(reg);

	gui__regGetInfo(reg, &info);

	switch (type & REG_MMI_TYPE_MASK) 
	{
		case SYS_MMI_T_INT:
			ret = gui__frmtInt(pInst, reg, &info);
			break;

		case SYS_MMI_T_REAL:
			ret = gui__frmtReal(pInst, reg, &info, 0xFF);
			break;

		case SYS_MMI_T_ENUM:
			ret = gui__frmtEnum(pInst, reg, &info);
			break;

		case SYS_MMI_T_STR:
			ret = gui__frmtStr(pInst, reg, &info);
			break;
		
		case SYS_MMI_T_TIME:
			ret = gui__frmtTime(pInst, reg, &info);
			break;

		case SYS_MMI_T_DATE:
			ret = gui__frmtDate(pInst, reg, &info);
			break;

		case SYS_MMI_T_DT:
		case SYS_MMI_T_MASK:
		case SYS_MMI_T_NONE:
		default:
			deb_assert(0);
			ret = 0;
			break;
	}

	return ret;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats INT value for displaying.
*
* \param		pInst		ptr to GUI instance
* \param		reg			register handle
* \param		pInfo		ptr to register info
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__frmtInt(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_RegInfo *			pInfo
) {
	reg_StatType			stat;
	Uint8					len;

	reg_stat(reg, &stat);

	switch (stat.status & REG_TYPE_MASK)
	{
	case SYS_REG_T_Int8:
		len = gui__i32toa(pInst->buffer.reg.i8, pInst->text.pCurrent, GUI_MAX_TXT);
		break;

	case SYS_REG_T_Uint8:
		len = gui__u32toa(pInst->buffer.reg.u8, pInst->text.pCurrent, GUI_MAX_TXT);
		break;

	case SYS_REG_T_Int16:
		len = gui__i32toa(pInst->buffer.reg.i16, pInst->text.pCurrent, GUI_MAX_TXT);
		break;

	case SYS_REG_T_Uint16:
		len = gui__u32toa(pInst->buffer.reg.u16, pInst->text.pCurrent, GUI_MAX_TXT);
		break;

	case SYS_REG_T_Int32:
		len = gui__i32toa(pInst->buffer.reg.i32, pInst->text.pCurrent, GUI_MAX_TXT);
		break;

	case SYS_REG_T_Uint32:
		len = gui__u32toa(pInst->buffer.reg.u32, pInst->text.pCurrent, GUI_MAX_TXT);
		break;

	default:
		deb_assert(0);
		len = 0;
		break;
	}

	return len;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats REAL value for displaying.
*
* \param		pInst		ptr to GUI instance
* \param		reg			register handle
* \param		pInfo		ptr to register info
* \param		maxdec		max number of decimals after the point
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__frmtReal(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_RegInfo *			pInfo,
	Uint8					maxdec
) {
	sys_AdditAttrReal const_P *	pReal = (sys_AdditAttrReal const_P *)pInfo->pInfo;
	Uint8					len = 0;
	Uint8					decims;
	Uint8					div = 0;

	if (pReal->numDecim > maxdec)
	{
		div = pReal->numDecim - maxdec;
		decims = maxdec;
	}
	else
	{
		decims = pReal->numDecim;
	}

	if (decims == 0)
	{
		len = gui__frmtInt(pInst, reg, NULL);
	}
	else
	{
		Int64					val;

		deb_assert(pInfo->reg_type == SYS_MMI_T_REAL);

		val = gui__regToInt64(reg, &pInst->buffer.reg);

		if (div > 0)
		{
			val += gui__frmtMultipliers[div] >> 1;
			val /= gui__frmtMultipliers[div];
		}

		len = gui__frmtRealValue(
			val, 
			pInst->text.pCurrent, 
			decims, 
			GUI_MAX_TXT
		);
	}

	return len;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Formats REAL value to a given buffer.
*
*	\param		val			REAL value as raw integer
*	\param		pBuf		ptr to target buffer
*	\param		decims		number of decimals in REAL
*	\param		maxlen		buffer size
*
*	\return		Formatted value length.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Uint8 gui__frmtRealValue(
	Int64					val,
	BYTE *					pBuf,
	Uint8					decims,
	Uint8					maxlen
) {
	Uint8					len = 0;
	Uint32					u32;

	if (maxlen == 0)
	{
		return 0;
	}

	if (val < 0)
	{
		val = -val;
		pBuf[len++] = '-';
	}

	u32 = (Uint32)val / gui__frmtMultipliers[decims];

	len += gui__u32toa(u32, &pBuf[len], maxlen - len);

	if (decims > 0)
	{
		if (len < maxlen)
		{
			pBuf[len++] = '.';
		}

		u32 = (Uint32)val % gui__frmtMultipliers[decims];

		maxlen -= len;

		if (decims > maxlen)
		{
			decims = maxlen;
		}

		len += gui__u32tofield(u32, &pBuf[len], decims);
	}

	return len;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats STRING value for displaying.
*
* \param		pInst		ptr to GUI instance
* \param		reg			register handle
* \param		pInfo		ptr to register info
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__frmtStr(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_RegInfo *			pInfo
) {
	return gui__textWriteAligned(
		pInst,
		pInst->buffer.reg.str,
		0, 
		pInst->disp.chr_per_line,
		GUI_ALIGN_LEFT|GUI_NO_FILL
	);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats DATE value for displaying.
*
* \param		pInst		ptr to GUI instance
* \param		reg			register handle
* \param		pInfo		ptr to register info
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__frmtDate(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_RegInfo *			pInfo
) {
#ifndef NDEBUG
	{
		reg_StatType			stat;

		reg_stat(reg, &stat);

		deb_assert((stat.status & REG_TYPE_MASK) == SYS_REG_T_Uint32);
	}
#endif

	return gui__frmtDateToAsc(
		pInst->dt_format, 
		pInst->text.pCurrent, 
		pInst->buffer.reg.u32
	);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats TIME value for displaying.
*
* \param		pInst		ptr to GUI instance
* \param		reg			register handle
* \param		pInfo		ptr to register info
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__frmtTime(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_RegInfo *			pInfo
) {
#ifndef NDEBUG
	{
		reg_StatType			stat;

		reg_stat(reg, &stat);

		deb_assert((stat.status & REG_TYPE_MASK) == SYS_REG_T_Uint32);
	}
#endif

	return gui__frmtTimeToAsc(
		pInst->dt_format, 
		pInst->text.pCurrent, 
		pInst->buffer.reg.u32
	);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats ENUM value for displaying.
*
* \param		pInst		ptr to GUI instance
* \param		reg			register handle
* \param		pInfo		ptr to register info
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__frmtEnum(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_RegInfo *			pInfo
) {
	Int64					enum_val;

	enum_val = gui__regToInt64(reg, &pInst->buffer.reg);

	if ((enum_val >= 0) && (enum_val <= 0xFF))
	{
		Uint8				len;
		BYTE const_P *		pTxt;
		Uint8				idx;

		pTxt = gui__regToEnumText(reg, (Uint8)enum_val, &len, GUI_NO_TEXT);

		for (idx = 0; idx < len; idx++)
		{
			pInst->text.pCurrent[idx] = *pTxt++;
		}

		return len;
	}

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Formats bitask enum for display.
*
*	\param		pInst		ptr to GUI instance
*	\param		reg			register handle
*	\param		bit			bit index
*	\param		bit_text	bit description text
*
*	\return		Number of characters in the edited value.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Uint8 gui__frmtBitmaskEnum(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	Uint8					bit,
	sys_TxtHandle			bit_text
) {
	Int64					enum_val;

	enum_val = gui__regToInt64(reg, &pInst->buffer.reg);

	if ((Uint32)enum_val & (1 << bit))
	{
		enum_val = 1;
	}
	else
	{
		enum_val = 0;
	}

	if ((enum_val >= 0) && (enum_val <= 0xFF))
	{
		Uint8				len;
		BYTE const_P *		pTxt;
		Uint8				idx;

		pTxt = gui__regToEnumText(reg, (Uint8)enum_val, &len, bit_text);

		for (idx = 0; idx < len; idx++)
		{
			pInst->text.pCurrent[idx] = *pTxt++;
		}

		return len;
	}

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Formats dynamic enum for display.
*
*	\param		pInst		ptr to GUI instance
*	\param		reg			register handle
*	\param		pItem		menu item info
*
*	\return		Number of characters in the edited value.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Uint8 gui__frmtDynamicEnum(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_MenuItem const_P *	pItem
) {
	Int64					enum_val;

	enum_val = gui__regToInt64(reg, &pInst->buffer.reg);

	if ((enum_val >= 0) && (enum_val <= 0xFF))
	{
		Uint8				len;
		Uint8				cnt;

		len = pItem->pEnumFn(
			(Uint8)enum_val,
			pInst->text.pCurrent, 
			&cnt
		);

		if (enum_val < cnt)
		{
			return len;
		}
	}

	return 0;
}

#if 0
/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats MASK value for displaying.
*
* \param		pInst		ptr to GUI instance
* \param		reg			register handle
* \param		pInfo		ptr to register info
*
* \return		Number of characters in the edited value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/

PRIVATE Uint8 gui__frmtMask(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	gui_RegInfo *			pInfo
) {
	deb_assert(FALSE);
	return 0;
}
#endif

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts ASCII to Int32.
*
* \param		pVal		ptr to ascii buffer
* \param		chars		number of chars to convert
* \param		pRes		conversion result will be put here
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__atoi32(
	BYTE *					pVal,
	Uint8					chars,
	Int32 *					pRes
) {
	Boolean					neg = FALSE;
	Int32					val = 0;
	BYTE *					pEnd = &pVal[chars];

	while (*pVal == ' ')
	{
		pVal++;
	}

	if (*pVal == '+')
	{
		pVal++;	
	}
	else if (*pVal == '-')
	{
		neg = TRUE;
		pVal++;
	}

	while (pVal < pEnd)
	{
		if ((*pVal >= '0') && (*pVal <= '9'))
		{
			Uint8 val_chr;

			val_chr = *pVal - '0';

			if (val > ((Int32_MAX / 10) - val_chr))
			{
				/* Overflow */
				return FALSE;
			}

			val = (val * 10) + val_chr;
		}
		else
		{
			/* Invalid character */
			return FALSE;
		}

		pVal++;
	}

	if (neg)
	{
		val = -val;
	}

	*pRes = val;

	return TRUE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts ASCII to Uint32.
*
* \param		pVal		ptr to ascii buffer
* \param		chars		number of chars to convert
* \param		pRes		conversion result will be put here
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__atou32(
	BYTE *					pVal,
	Uint8					chars,
	Uint32 *				pRes
) {
	Uint32					val = 0;
	BYTE *					pEnd = &pVal[chars];

	while (*pVal == ' ')
	{
		pVal++;
	}

	if (*pVal == '+')
	{
		pVal++;	
	}
	else if (*pVal == '-')
	{
		return FALSE;
	}

	while (pVal < pEnd)
	{
		if ((*pVal >= '0') && (*pVal <= '9'))
		{
			Uint8 val_chr;

			val_chr = *pVal - '0';

			if (val > ((Uint32_MAX / 10) - val_chr))
			{
				/* Overflow */
				return FALSE;
			}

			val = (val * 10) + val_chr;
		}
		else
		{
			/* Invalid character */
			return FALSE;
		}

		pVal++;
	}

	*pRes = val;

	return TRUE;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts REAL ASCII to Int32.
*
* \param		pVal		ptr to ascii buffer	
* \param		chars		number of chars to convert
* \param		decim		number of decimals
* \param		pRes		conversion result will be put here
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__atoi32Real(
	BYTE *					pVal,
	Uint8					chars,
	Uint8					decim,
	Int32 *					pRes
) {
	Boolean					neg = FALSE;
	Int32					val = 0;
	Int32					rem = 0;
	Int32 *					pCur;
	BYTE *					pEnd = &pVal[chars];

	while (*pVal == ' ')
	{
		pVal++;
	}

	if (*pVal == '+')
	{
		pVal++;	
	}
	else if (*pVal == '-')
	{
		neg = TRUE;
		pVal++;
	}

	pCur = &val;

	while (pVal < pEnd)
	{
		if ((*pVal >= '0') && (*pVal <= '9'))
		{
			Uint8 val_chr;

			val_chr = *pVal - '0';

			if (*pCur > ((Int32_MAX / 10) - val_chr))
			{
				/* Overflow */
				return FALSE;
			}

			*pCur = (*pCur * 10) + val_chr;
		}
		else if (*pVal == '.')
		{
			pCur = &rem;
		}
		else
		{
			/* Invalid character */
			break;
		}

		pVal++;
	}

	if (decim > 0)
	{
		if (val > ((Int32_MAX / (Int32)gui__frmtMultipliers[decim]) - rem))
		{
			return FALSE;
		}

		val *= (Int32)gui__frmtMultipliers[decim];
		val += rem;
	}

	if (neg)
	{
		val = -val;
	}

	*pRes = val;

	return TRUE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts REAL ASCII to Uint32.
*
* \param		pVal		ptr to ascii buffer	
* \param		chars		number of chars to convert
* \param		decim		number of decimals
* \param		pRes		conversion result will be put here
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__atou32Real(
	BYTE *					pVal,
	Uint8					chars,
	Uint8					decim,
	Uint32 *				pRes
) {
	Uint32					val = 0;
	Uint32					rem = 0;
	Uint32 *				pCur;
	BYTE *					pEnd = &pVal[chars];

	while (*pVal == ' ')
	{
		pVal++;
	}

	if (*pVal == '+')
	{
		pVal++;	
	}
	else if (*pVal == '-')
	{
		deb_assert(FALSE);
		pVal++;
	}

	pCur = &val;

	while (pVal < pEnd)
	{
		if ((*pVal >= '0') && (*pVal <= '9'))
		{
			Uint8 val_chr;

			val_chr = *pVal - '0';

			if (*pCur > ((Uint32_MAX / 10) - val_chr))
			{
				/* Overflow */
				return FALSE;
			}

			*pCur = (*pCur * 10) + val_chr;
		}
		else if (*pVal == '.')
		{
			pCur = &rem;
		}
		else
		{
			/* Invalid character */
			break;
		}

		pVal++;
	}

	if (decim > 0)
	{
		if (val > ((Uint32_MAX / (Uint32)gui__frmtMultipliers[decim]) - rem))
		{
			return FALSE;
		}

		val *= (Int32)gui__frmtMultipliers[decim];
		val += rem;
	}

	*pRes = val;

	return TRUE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts Int32 to ASCII.
*
* \param		val			value to convert
* \param		pBuf		destination buffer
* \param		maxlen		buffer length
*
* \return		Conversion result length.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__i32toa(
	Int32					val,
	BYTE *					pBuf,
	Uint8					maxlen
) {
	Uint32					u32;
	Uint8					len = 0;

	if (maxlen == 0) 
	{
		return 0;
	}

	if (val < 0)
	{
		*pBuf++ = '-';
		maxlen--;
		len++;

		if (val == Int32_MIN)
		{
			u32 = ((Uint32)Int32_MAX) + 1UL;
		}
		else
		{
			u32 = (Uint32)(val * -1L);
		}
	}
	else
	{
		u32 = val;
	}

	len += gui__u32toa(u32, pBuf, maxlen);

	return len;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts Uint32 to ASCII.
*
* \param		val			value to convert
* \param		pBuf		destination buffer
* \param		maxlen		buffer length
*
* \return		Conversion result length.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__u32toa(
	Uint32					val,
	BYTE *					pBuf,
	Uint8					maxlen
) {
	Uint8					len;
	BYTE *					pEnd;

	if (maxlen == 0) 
	{
		return 0;
	}

	pEnd = &pBuf[maxlen];
//	*pEnd = 0x00;

	do {
		if (pEnd == pBuf) 
		{
			break;
		}

		*--pEnd = (BYTE)((Uint32)(val % 10) + '0');
		
	} while((val /= 10) > 0);

	maxlen = (Uint8)(&pBuf[maxlen] - pEnd);

	len = maxlen;

	while (len--)
	{
		*pBuf++ = *pEnd++;
	}

	return maxlen;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats u32 value to a buffer with zero padding at the front.
*
* \param		val		value to convert
* \param		pBuf	destination buffer
* \param		len		field length
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__i32tofield(
	Int32					val,
	BYTE *					pBuf,
	Uint8					len
) {
	Uint32					u32;
	Uint8					ret = len;

	if (len == 0)
	{
		return 0;
	}

	if (val < 0)
	{
		*pBuf++ = '-';
		len--;

		if (val == Int32_MIN)
		{
			u32 = ((Uint32)Int32_MAX) + 1UL;
		}
		else
		{
			u32 = (Uint32)(val * -1L);
		}
	}
	else
	{
		u32 = val;
	}

	gui__u32tofield(u32, pBuf, len);

	return ret;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats u32 value to a buffer with zero padding at the front.
*
* \param		val		value to convert
* \param		pBuf	destination buffer
* \param		len		field length
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__u32tofield(
	Uint32					val,
	BYTE *					pBuf,
	Uint8					len
) {
	BYTE *					pEnd;

	if (len == 0) 
	{
		return 0;
	}

	pEnd = &pBuf[len];

	do {
		if (pEnd == pBuf) 
		{
			break;
		}

		*--pEnd = (BYTE)((Uint32)(val % 10) + '0');
		
	} while((val /= 10) > 0);

	while (pBuf < pEnd)
	{
		*pBuf++ = '0';
	}

	return len;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Returns time mask from date/time format.
*
* \param		type		date/time format
*
* \return		Ptr to time mask.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC BYTE const_P *  gui__frmtGetTimeMask(
	Uint8					type
) {
	if (type < dim(gui__frmtStrTime))
	{
		return (BYTE const_P *)gui__frmtStrTime[type];
	}

	return (BYTE const_P *)gui__frmtStrTime[0];
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Returns date mask from date/time format.
*
* \param		type		date/time format
*
* \return		Ptr to date mask.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC BYTE const_P * gui__frmtGetDateMask(
	Uint8					type
) {
	if (type < dim(gui__frmtStrDate))
	{
		return (BYTE const_P *)gui__frmtStrDate[type];
	}

	return (BYTE const_P *)gui__frmtStrDate[0];
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts POSIX time to ASCII DATE.
*
* \param		type		date/time format to use
* \param		pBuf		destination buffer
* \param		posix_time	time to format
*
* \return		Conversion result length.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__frmtDateToAsc(
	Uint8					type,
	BYTE *					pBuf,
	Posix_Time				posix_time
) {
	Uint8					len = 0;
	RTC_Time				tim;
	Uint8					mon;
	Uint16					tmp;

	rtc_convertToTime32(posix_time, &tim);

	mon = tim.tm_mon + 1; /* tim.tm_mon starts from 0 */

	switch (type)
	{
	case GUI_DT_EUR:
		{
			tmp = tim.tm_mday;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 31)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);

			pBuf[len++] = '.';

			if (mon < 10)
			{
				pBuf[len++] = '0';
			}
			else if (mon > 12)
			{
				mon = 10;
			}

			len += gui__u32toa(mon, &pBuf[len], GUI_MAX_TXT - len);

			pBuf[len++] = '.';

			tmp = tim.tm_year + 1900;

			if (tmp > 9999)
			{
				tmp = 2000;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
		}
		break;

	case GUI_DT_USA:
		{
			if (mon < 10)
			{
				pBuf[len++] = '0';
			}
			else if (mon > 12)
			{
				mon = 10;
			}

			len += gui__u32toa(mon, &pBuf[len], GUI_MAX_TXT - len);

			pBuf[len++] = '/';

			tmp = tim.tm_mday;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 31)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);

			pBuf[len++] = '/';

			tmp = tim.tm_year + 1900;

			if (tmp > 9999)
			{
				tmp = 2000;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
		}
		break;

	default:
	case GUI_DT_ISO:
		{
			tmp = tim.tm_year + 1900;

			if (tmp > 9999)
			{
				tmp = 2000;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
			pBuf[len++] = '-';

			if (mon < 10)
			{
				pBuf[len++] = '0';
			}
			else if (mon > 12)
			{
				mon = 10;
			}

			len += gui__u32toa(mon, &pBuf[len], GUI_MAX_TXT - len);

			pBuf[len++] = '-';

			tmp = tim.tm_mday;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 31)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
		}
		break;
	}

	return len;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts POSIX time to ASCII TIME.
*
* \param		type		date/time format to use
* \param		pBuf		destination buffer
* \param		posix_time	time to format
*
* \return		Conversion result length.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__frmtTimeToAsc(
	Uint8					type,
	BYTE *					pBuf,
	Posix_Time				posix_time
) {
	Uint8					len = 0;
	RTC_Time				tim;
	Uint16					tmp;

	rtc_convertToTime32(posix_time, &tim);

	switch (type)
	{
	case GUI_DT_EUR:
		{
			tmp = tim.tm_hour;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 23)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
			pBuf[len++] = '.';

			tmp = tim.tm_min;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 59)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);

#if GUI_SHOW_SECONDS == 1
			pBuf[len++] = '.';

			tmp = tim.tm_sec;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 59)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
#endif
		}
		break;

	case GUI_DT_USA:
		{
			BYTE chr;

			tmp = tim.tm_hour;

			/* Just to make sure display is not corrupted if time is invalid */
			if (tmp > 23)
			{
				tmp = 10;
			}

			if (tmp >= 12)
			{
				chr = 'P';

				if (tmp > 12)
				{
					tmp -= 12;
				}
			}
			else
			{
				chr = 'A';

				if (tmp == 0) /* 00:00 => 12:00 AM */
				{
					tmp = 12;
				}
			}

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
			pBuf[len++] = ':';

			tmp = tim.tm_min;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 59)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
/*			pBuf[len++] = ' ';*/

			pBuf[len++] = chr;
			pBuf[len++] = 'M';
		}
		break;

	default:
	case GUI_DT_ISO:
		{
			tmp = tim.tm_hour;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 23)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
			pBuf[len++] = ':';

			tmp = tim.tm_min;

			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 59)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);

#if GUI_SHOW_SECONDS == 1
			pBuf[len++] = ':';

			tmp = tim.tm_sec;
				
			if (tmp < 10)
			{
				pBuf[len++] = '0';
			}
			else if (tmp > 59)
			{
				tmp = 10;
			}

			len += gui__u32toa(tmp, &pBuf[len], GUI_MAX_TXT - len);
#endif
		}
		break;
	}

	return len;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts time to ASCII TIME.
*
*	\param		type	date/time format to use
*	\param		pBuf	destination buffer
*	\param		time	time to format
*
*	\return		Conversion result length.
*
*	\details	Time is stored as Uint16 with high word being minutes and low
*				word hours.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Uint8 gui__frmtTime2ToAsc(
	Uint8					type,
	BYTE *					pBuf,
	Uint32					time
) {
	Uint8					len = 0;
	Uint8					hour;
	Uint8					min;

	hour = (Uint8)(time & 0xFF); 
	min = (Uint8)((time >> 8) & 0xFF);

	switch (type)
	{
	case GUI_DT_EUR:
	case GUI_DT_ISO:
		{
			if (hour > 24)
			{
				hour = 0;
			}

			if (hour < 10)
			{
				pBuf[len++] = '0';
			}

			len += gui__u32toa(hour, &pBuf[len], GUI_MAX_TXT - len);

			if (type == GUI_DT_EUR)
			{
				pBuf[len++] = '.';
			}
			else
			{
				pBuf[len++] = ':';
			}

			if (min > 59)
			{
				min = 0;
			}

			if (min < 10)
			{
				pBuf[len++] = '0';
			}

			len += gui__u32toa(min, &pBuf[len], GUI_MAX_TXT - len);
		}
		break;

	case GUI_DT_USA:
		{
			BYTE chr;

			if (hour > 24)
			{
				hour = 0;
			}

			if (hour >= 12)
			{
				chr = 'P';

				if (hour > 12)
				{
					hour -= 12;
				}
			}
			else
			{
				chr = 'A';

				if (hour == 0) /* 00:00 => 12:00 AM */
				{
					hour = 12;
				}
			}

			if (hour < 10)
			{
				pBuf[len++] = '0';
			}

			len += gui__u32toa(hour, &pBuf[len], GUI_MAX_TXT - len);
			pBuf[len++] = ':';

			if (min > 59)
			{
				min = 0;
			}

			if (min < 10)
			{
				pBuf[len++] = '0';
			}

			len += gui__u32toa(min, &pBuf[len], GUI_MAX_TXT - len);

#if GUI_FRMT_T_SPACE > 0
			pBuf[len++] = ' ';
#endif
			pBuf[len++] = chr;
			pBuf[len++] = 'M';
		}
		break;

	default:
		break;
	}

	return len;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts from ASCII to TIME.
*
* \param		pVal	ptr to ascii buffer
* \param		type	date/time type
* \param		pRes	conversion result
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__ascToTime(
	BYTE *					pVal,
	Uint8					type,
	Posix_Time *			pRes
) {
	RTC_Time				tim;

	if (gui__ascToTimeStruct(pVal, type, &tim, 23))
	{
		return rtc_convertToPosix32(&tim, pRes);
	}

	return FALSE;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts ascii time to time struct.
*
*	\param		pVal	ptr to ascii buffer
*	\param		type	date/time type
*	\param		pRes	conversion result
*	\param		maxhour max allowed hour count (23 or 24)
*
*	\return		TRUE if conversion was succesful.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Boolean gui__ascToTimeStruct(
	BYTE *					pVal,
	Uint8					type,
	RTC_Time *				pRes,
	Uint8					maxhour
) {
	Uint32					tmp;

	switch (type)
	{
	case GUI_DT_EUR:
	case GUI_DT_ISO:
		{
			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			pRes->tm_hour = (Uint16)tmp;

			pVal += 3;

			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			pRes->tm_min = (Uint16)tmp;

#if GUI_SHOW_SECONDS == 1
			pVal += 3;

			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			pRes->tm_sec = (Uint16)tmp;
#else
			pRes->tm_sec = 0;
#endif
		}
		break;

	case GUI_DT_USA:
		{
			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			pRes->tm_hour = (Uint16)tmp;

			pVal += 3;

			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			pRes->tm_min = (Uint16)tmp;

			pVal += 2 + GUI_FRMT_T_SPACE;

			if (*pVal == 'A')
			{
				if (pRes->tm_hour == 12)
				{
					pRes->tm_hour = 0;
				}
			}
			else if (*pVal == 'P')
			{
				if (pRes->tm_hour != 12)
				{
					pRes->tm_hour += 12;
				}
			}
			else
			{
				return FALSE;
			}

			pRes->tm_sec = (Uint16)0;
		}
		break;

	default:
		deb_assert(FALSE);
		return FALSE;
	}

	/*
	 *	Validate value.
	 */
	if (pRes->tm_sec > 59)
	{
		return FALSE;
	}

	if (pRes->tm_min > 59)
	{
		return FALSE;
	}

	if (pRes->tm_hour > maxhour)
	{
		return FALSE;
	}

	pRes->tm_year = (Uint16)80;
	pRes->tm_mon = (Uint16)0;
	pRes->tm_mday = (Uint16)1;

	return TRUE;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts from ASCII to DATE.
*
* \param		pVal	ptr to ascii buffer
* \param		type	date/time type
* \param		pRes	conversion result
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/

PUBLIC Boolean gui__ascToDate(
	BYTE *					pVal,
	Uint8					type,
	Posix_Time *			pRes
) {
	RTC_Time				tim;
	Uint32					tmp;

	switch (type)
	{
	case GUI_DT_EUR:
		{
			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			tim.tm_mday = (Uint16)tmp;

			pVal += 3;

			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}

			/* tim.tm_mon starts from 0 */
			tim.tm_mon = (Uint16)tmp - 1;

			pVal += 3;

			if (gui__atou32(pVal, 4, &tmp) == FALSE)
			{
				return FALSE;
			}
			tim.tm_year = (Uint16)tmp;
		}
		break;

	case GUI_DT_USA:
		{
			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}

			/* tim.tm_mon starts from 0 */
			tim.tm_mon = (Uint16)tmp - 1;

			pVal += 3;

			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			tim.tm_mday = (Uint16)tmp;

			pVal += 3;

			if (gui__atou32(pVal, 4, &tmp) == FALSE)
			{
				return FALSE;
			}
			tim.tm_year = (Uint16)tmp;
		}
		break;

	case GUI_DT_ISO:
		{
			if (gui__atou32(pVal, 4, &tmp) == FALSE)
			{
				return FALSE;
			}
			tim.tm_year = (Uint16)tmp;

			pVal += 5;

			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}

			/* tim.tm_mon starts from 0 */
			tim.tm_mon = (Uint16)tmp - 1;

			pVal += 3;

			if (gui__atou32(pVal, 2, &tmp) == FALSE)
			{
				return FALSE;
			}
			tim.tm_mday = (Uint16)tmp;
		}
		break;

	default:
		deb_assert(FALSE);
		return FALSE;
	}

	/*
	 *	Validate values.
	 */
	if (tim.tm_mon > 11)
	{
		return FALSE;
	}

	if ((tim.tm_mday > 31) || (tim.tm_mday < 1))
	{
		return FALSE;
	}

	tim.tm_year -= 1900;

	if (tim.tm_year > 200)
	{
		return FALSE;
	}

	tim.tm_hour = 0;
	tim.tm_min = 0;
	tim.tm_sec = 0;

	return rtc_convertToPosix32(&tim, pRes);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats Int64 to INT ascii value with zero padding and sign as
*				needed.
*
* \param		val		value to convert
* \param		pBuf	destination buffer
* \param		sign	if TRUE sign is added
* \param		len		length of the destination field
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__frmtPaddedInt(
	Int64					val,
	BYTE *					pBuf,
	Boolean					sign,
	Uint8					len
) {
	if (sign)
	{
		if (val < 0)
		{
			gui__i32tofield((Int32)val, pBuf, len);
		}
		else
		{
			pBuf[0] = '+';
			gui__u32tofield((Uint32)val, &pBuf[1], len - 1);
		}
	}
	else
	{
		gui__u32tofield((Uint32)val, pBuf, len);
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Formats Int64 to REAL ascii value with zero padding and sign as
*				needed.	
*
* \param		val		value to convert
* \param		pBuf	destination buffer
* \param		sign	if TRUE sign is added
* \param		decim	number of decimals
* \param		len		length of the destination field
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__frmtPaddedReal(
	Int64					val,
	BYTE *					pBuf,
	Boolean					sign,
	Uint8					decim,
	Uint8					len
) {
	if (decim == 0)
	{
		gui__frmtPaddedInt(val, pBuf, sign, len);
	}
	else
	{
		Uint32					u32;
		Uint8					pos = 0;

		if (sign)
		{
			if (val < 0)
			{
				pBuf[pos++] = '-';
				val *= -1;
			}
			else
			{
				pBuf[pos++] = '+';
			}

			len--;
		}

		u32 = (Uint32)val / gui__frmtMultipliers[decim];

		pos += gui__u32tofield(u32, &pBuf[pos], len - decim - 1);
		pBuf[pos++] = '.';

		u32 = (Uint32)val % gui__frmtMultipliers[decim];

		gui__u32tofield(u32, &pBuf[pos], decim);
	}
}

