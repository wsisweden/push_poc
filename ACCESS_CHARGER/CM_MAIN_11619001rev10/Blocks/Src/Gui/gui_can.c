/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger selectedPageRow view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"
#include "../Can/NodesStatusAndMapping.h"

#include "gui_callbackMenu.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define FIRST_ITEM	2
#define PAGE_ITEMS	8

#define GUI__CAN_2ND_HDR_ROW	1
#define GUI__CAN_ID_LEFT	0
#define GUI__CAN_ID_WIDTH	4
#define GUI__CAN_ST_LEFT		(GUI__CAN_ID_LEFT + GUI__CAN_ID_WIDTH)
#define GUI__CAN_ST_WIDTH	7
#define GUI__CAN_SN_LEFT		(GUI__CAN_ST_LEFT + GUI__CAN_ST_WIDTH)
#define GUI__CAN_SN_WIDTH	10

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

static void DrawHeaderRow2(gui__Inst * pInst, const gui__CallbackView * pView);
static void DrawItems(gui__Inst * pInst, const gui__CallbackView * pView);
static void DrawRow(gui__Inst* pInst, volatile const struct NodeInformation_type* Nodes);

/* Call back functions */
static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {0};
static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	1,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_F_NONE,
	EDITABLE_ITEMS,
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



PUBLIC Int8 gui__viewCanProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws header part (first two rows) of the view.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawHeaderRow2(
	gui__Inst *				pInst,
	const gui__CallbackView *	pView
) {
	/*
	 *	Draw 2nd line header (ID	Status	CM(S/N)	)
	 */
	gui__fontSelect(pInst, GUI_FNT_6x8);

	gui__dispSetTextPos(pInst, GUI__CAN_ID_LEFT, GUI__CAN_2ND_HDR_ROW);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tId),
		0,
		GUI__CAN_ID_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CAN_ID_WIDTH);

	gui__dispSetTextPos(pInst, GUI__CAN_ST_LEFT, GUI__CAN_2ND_HDR_ROW);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tState),
		0,
		GUI__CAN_ST_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CAN_ST_WIDTH);

	gui__dispSetTextPos(pInst, GUI__CAN_SN_LEFT, GUI__CAN_2ND_HDR_ROW);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tSerialNo),
		0,
		GUI__CAN_SN_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CAN_SN_WIDTH);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawRow(
	gui__Inst *				pInst,
	volatile const struct NodeInformation_type* Nodes
) {
	{
		const int len = gui__u32toa(Nodes->NodeId, &pInst->text.pCurrent[0], 3);
		gui__textWriteChars(pInst, ' ', len, 3 - len);
	}

	{
		switch(Nodes->heartbeatStatus){
		case 5 :																		// Operational
			gui__textWriteConst(pInst, (Uint8 const_P *)" Op    ", 3, 7);
			break;
		case 0x7F :																		// Pre-operational
			gui__textWriteConst(pInst, (Uint8 const_P *)" Pre-Op", 3, 7);
			break;
		default :																		// Others
			gui__textWriteConst(pInst, (Uint8 const_P *)"       ", 3, 7);
			break;
		}
	}

	{
		if(Nodes->SerialNr){
			BYTE tmpText[10];
			const int len = gui__u32toa(Nodes->SerialNr, &tmpText[0], 10);

			gui__textWriteChars(pInst, ' ', 10, 1);
			gui__textCopy(&pInst->text.pCurrent[11], &tmpText[0],len);
			gui__textWriteChars(pInst, ' ', 11+len, 10-len);

		}
		else{
			gui__textWriteConst(pInst, (Uint8 const_P *)"    Unknown", 10, 11);
		}
	}

	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws other than header part of the view (CAN items).
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawItems(
	gui__Inst *				pInst,
	const gui__CallbackView *		pView
) {
	const int pageOffset = (PAGE_ITEMS - FIRST_ITEM)*pView->selectedPage;
	int item = 0;
	volatile const struct NodeInformation_type* const Nodes = CAN_NodeStatusAndMapping_GetNodes();
	int NodesPos = 0;
	int nodesCnt = 0;

	for(NodesPos = 0; NodesPos < NODES_STATUS_LEN; NodesPos++){
		if(Nodes[NodesPos].NodeId){
			nodesCnt++;
			if(nodesCnt > pageOffset && item < PAGE_ITEMS - FIRST_ITEM){
				gui__dispSetTextPos(pInst, 0, FIRST_ITEM + item);

				DrawRow(pInst, &Nodes[NodesPos]);									// Draw row
				item++;
			}
		}
	}
	callbackParameters.items = nodesCnt/(PAGE_ITEMS - FIRST_ITEM + 1) + 1;
}

static void callback_init(gui__CallbackView* pV, gui__Inst * pInst){
	/* Reset row selected */
	pV->selectedPage = 0;
	pV->selectedPageRow = 0;
	pV->selectedItem = 0;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	DrawItems(pInst, pView);
	DrawHeaderRow2(pInst, pView);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	if(gui__navCheckAccess(pInst, TRUE)){
	}
	return 0;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	return 1;
}
