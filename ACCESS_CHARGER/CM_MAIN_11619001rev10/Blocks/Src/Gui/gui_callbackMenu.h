#ifndef GUI_CALLBACKMENU_H
#define GUI_CALLBACKMENU_H

#include "local.h"

#define GUI_EDIT_NONE 	0
#define GUI_EDIT_HOUR 	1
#define GUI_EDIT_MIN	2

#define ITEM1		(1<<0)
#define ITEM2		(1<<1)
#define ITEM3		(1<<2)
#define ITEM4		(1<<3)
#define ITEM5		(1<<4)
#define ITEM6		(1<<5)
#define ITEM7		(1<<6)
#define ITEM8		(1<<7)

typedef struct
{
	BYTE const_P *								subHeaderTxt;					// Pointer to sub header text
	int											items;							// Number of rows, below zero inside sub menu
	const int									items_per_page;					// Number of rows per page
	void (*init)(gui__CallbackView* pV, gui__Inst * pInst);						// Called then edit start
	void (*paint)(gui__Inst*, const gui__CallbackView*);						// Called then edit start
	void (*keydown)(gui__Inst*, const gui__CallbackView* pView, gui__VKey key);	// Called then edit start
	int (*edit_start)(gui__Inst* pInst, int item, gui__CallbackView* pView);	// Called then edit start
	int (*edit_end)(gui__Inst* pInst, int item);								// Called then edit end
	Uint32										flags;							// Flags
	const Uint8									editable;						// Bit config (bit 0-7 = row 0-7). 1 = editable, 0 = non editable
} callbackMenu_type;															//

void gui__selectionLimit(const callbackMenu_type* instance);

int gui__callbackMenuNavigate(gui__Inst* pInst, gui__View* pView, gui__ViewMessage msg, void* pArgs, const callbackMenu_type* instance);

PUBLIC void gui__callbackMenuInit(gui__Inst* pInst);
PUBLIC void gui__callbackMenuNavigateIn(gui__Inst*	pInst);
PUBLIC void gui__callbackMenuNavigateOut(gui__Inst* pInst);

/* Print a value with decimal point and pointer
 * Parameters:
 *   pInst is the current instance
 *   value is the number
 *   decimal is the number of decimals for value
 *   pointerPos is the position of pointer or minus one for no pointer
 */
void dispPrintf(gui__Inst* pInst, int value, int decimals, int pointerPos, int digits);
void dispTimePrintf(gui__Inst* pInst, uint8_t hour, uint8_t minute, uint8_t pointerPos);
void gui__DrawItems(gui__Inst * pInst, const gui__CallbackView * pView, void (*DrawRow)(gui__Inst* pInst, int item, int edited), const callbackMenu_type* instance);
void gui__DrawItemsScs(gui__Inst * pInst, const gui__CallbackView * pView, void (*DrawRow)(gui__Inst* pInst, int item, int edited), const callbackMenu_type* instance);
void gui_DrawSpace(gui__Inst* pInst, int len);

#endif
