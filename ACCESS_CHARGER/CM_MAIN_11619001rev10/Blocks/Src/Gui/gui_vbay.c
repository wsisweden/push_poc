/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Process view handling.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "Chalg.h"
#include "Regu.h"
#include "Meas.h"
#include "Sup.h"
#include "CAN.h"
#include "Radio.h"
#include "tui.h"

#include "gui.h"
#include "local.h"
#include "../Regu/pause.h"


/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * 	\name	Process view states.
 */
/*@{*/
#define GUI_BAY_NO_CHARGE	0			/**< Not charging					*/ 
#define GUI_BAY_CHARGING	1			/**< Charging						*/ 
#define GUI_BAY_ALARM		2			/**< Alarm is on					*/ 
#define GUI_BAY_CAN_CTRL	3			/**< CAN controlled					*/
#define GUI_BAY_POPUP		4			/**< Popup message is on			*/
/*@}*/

/**
 * 	\name	Sub states.
 */
/*@{*/
#define GUI_BAY_ST_RADIO	(1<<0)		/**< Radio is active				*/ 
#define GUI_BAY_ST_TIME_RES	(1<<1)		/**< Time restricted				*/ 
#define GUI_BAY_ST_REMOTE	(1<<2)		/**< Remote start					*/ 
#define GUI_BAY_ST_PAUSED	(1<<3)		/**< Paused							*/ 
#define GUI_BAY_ST_BATCON	(1<<4)		/**< Battery connected				*/ 
#define GUI_BAY_ST_COMPLETE	(1<<5)		/**< Charging complete				*/ 
#define GUI_BAY_ST_MAIN		(1<<6)		/**< Charging state Main			*/ 
#define GUI_BAY_ST_ADDIT	(1<<7)		/**< Charging state Additional		*/ 
#define GUI_BAY_ST_EQU		(1<<8)		/**< Charging state Equalize		*/ 
#define GUI_BAY_ST_MAINTEN	(1<<9)		/**< Charging state Maintenance		*/ 
#define GUI_BAY_ST_RADIO_R	(1<<10)		/**< Radio sending / receiving		*/ 
#define GUI_BAY_ST_BBC_R	(1<<11)		/**< BBC ready						*/ 
#define GUI_BAY_ST_BBC_C	(1<<12)		/**< BBC charging					*/ 
#define GUI_BAY_ST_BM_INIT	(1<<13)		/**< BM init						*/ 
#define GUI_BAY_ST_MSTR_CTRL (1<<14)	/**< CAN Controlled master			*/
#define GUI_BAY_ST_SLAVE_CTRL (1<<15)	/**< CAN Controlled slave			*/
#define GUI_BAY_ST_PRE		(1<<16)		/**< Charging state pre				*/
#define GUI_BAY_ST_BM_CALIB	(1<<17)		/**< Calibrating BMU				*/
#define GUI_BAY_ST_VSENSE	(1<<18)		/**< Voltage sense					*/
#define GUI_BAY_ST_WATERING	(1<<19)		/**< Manual watering indication		*/
/*@}*/

#define GUI_BAY_ALARM_TICK	3			/**< Alarm rotation tick			*/ 
#define GUI_BAY_POPUP_TIME	3			/**< Popup visibility time			*/ 
#define GUI_BAY_ESC_TIME	0			/**< ESC disabled time				*/


/**
 * 	\name	Position defines.
 */
/*@{*/
#define GUI_BAY_RAD_LEFT	0			/**<								*/ 
#define GUI_BAY_RAD_TOP		0			/**<								*/ 
#define GUI_BAY_RAD_WIDTH	16			/**<								*/ 
#define GUI_BAY_RAD_HEIGHT	16			/**<								*/ 
#define GUI_BAY_BAT_LEFT	0			/**<								*/ 
#define GUI_BAY_BAT_TOP		16			/**<								*/ 
#define GUI_BAY_BAT_WIDTH	16			/**<								*/ 
#define GUI_BAY_BAT_HEIGHT	48			/**<								*/ 
#define GUI_BAY_BAT_I_TOP	(GUI_BAY_BAT_TOP + 6)
#define GUI_BAY_BAT_I_LEFT	(GUI_BAY_BAT_LEFT + 3)
#define GUI_BAY_BAT_I_WIDTH	(GUI_BAY_BAT_WIDTH - (2 * 3))	 
#define GUI_BAY_BAT_I_HEIGHT	(GUI_BAY_BAT_HEIGHT - (2 * 3 + 3))
#define GUI_BAY_BAT_I_BTM	(GUI_BAY_BAT_I_TOP + GUI_BAY_BAT_I_HEIGHT)
/*@}*/

/**
 * 	\name	Register indexes in process view menu table.
 */
/*@{*/
enum 
{
	GUI__BAY_IDX_I = 1,					/**< Current						*/
	GUI__BAY_IDX_U,						/**< Voltage						*/
	GUI__BAY_IDX_AH,					/**< Amper hours					*/
	GUI__BAY_IDX_VC,					/**< Volts per cell					*/
	GUI__BAY_IDX_SOC					/**< State Of Charge				*/
};
/*@}*/


#define GUI_BAY_CHALG_MASK		(CHALG_ERR_LOW_BATTERY_VOL|CHALG_ERR_HIGH_BATTERY_VOL|CHALG_ERR_CHARGE_AH_LIMIT|CHALG_ERR_CHARGE_TIME_LIMIT|CHALG_ERR_INCORRECT_ALGORITHM|CHALG_ERR_INCORRECT_MOUNT|CHALG_ERR_HIGH_BATTERY_LIMIT|CHALG_ERR_BATTERY_ERROR|CHALG_ERR_BM_TEMP|CHALG_ERR_BM_ACID|CHALG_ERR_BM_BALANCE|CHALG_ERR_LOW_AIRPUMP_PRESSURE|CHALG_ERR_HIGH_AIRPUMP_PRESSURE|CHALG_ERR_BM_LOW_SOC)
#define GUI_BAY_REGU_MASK		(REGU_ERR_PHASE_ERROR|REGU_ERR_REGULATOR_ERROR|REGU_ERR_LOW_CHARGER_TEMPERATURE|REGU_ERR_HIGH_CHARGER_TEMPERATURE|REGU_ERR_HIGH_TRAFO_TEMPERATURE|REGU_ERR_LOW_BOARD_TEMPERATURE|REGU_ERR_HIGH_BOARD_TEMPERATURE)
#define GUI_BAY_SUP_MASK		(SUP_STATUS_CORRUPTED|SUP_STATUS_RADIO_ERROR|SUP_STATUS_BM_FAILED|SUP_STATUS_BM_ALG_ERR|SUP_STATUS_ADDR_CONFLICT|SUP_STATUS_BM_ADDR_CONFLICT|SUP_STATUS_DPL_NO_MASTER|SUP_STATUS_DPL_MASTER_CONFLICT)

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef void (*				gui__fnBayDraw)(gui__Inst *, gui__BayView *);


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gui__viewBayUpdateState(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawIdle(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawCharging(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawAlarm(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawCAN(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawPopup(gui__Inst * pInst, gui__BayView * pView);

PRIVATE void gui__viewBayDrawRadio(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawBattery(gui__Inst * pInst, gui__BayView * pView, Uint8 percent);
PRIVATE void gui__viewBayDrawTime(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawId(gui__Inst * pInst, gui__BayView * pView);

PRIVATE void gui__viewBayNextAlarm(gui__Inst * pInst, gui__BayView * pView);

PRIVATE Uint8 gui__viewBayWriteValue(gui__Inst * pInst, Uint8 idx, sys_TxtHandle unit, Uint8 maxlen);

PRIVATE void gui__viewBayDrawStateCharging(gui__Inst * pInst, gui__BayView * pView);
PRIVATE void gui__viewBayDrawStateReady(gui__Inst * pInst, gui__BayView * pView);

PRIVATE Boolean gui__viewBayStartTime(gui__Inst * pInst, Uint8 * hour, Uint8 * min, Uint8 * pWday);

//PRIVATE void gui__viewBayDebugDraw(gui__Inst * pInst, gui__BayView * pView);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

#if 1

/**
 *	Radio connection image (14x16). Connected and sending / receiving.
 */
PRIVATE BYTE gui__imgRadioReceiving[] =
{
0x0C,0x00,
0x12,0x00,
0x21,0x00,
0x0C,0x00,
0x12,0x38,
0x00,0x17,
0xCC,0x15,
0x7E,0x15,
0x7E,0x15,
0xCC,0x15,
0x00,0x17,
0x12,0x38,
0x0C,0x00,
0x21,0x00,
0x12,0x00,
0x0C,0x00,
};

/**
 *	Radio connection image (14x16). Connected but not traffic.
 */
PRIVATE BYTE gui__imgRadioIdle[] =
{
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x38,
0x00,0x17,
0xCC,0x15,
0x7E,0x15,
0x7E,0x15,
0xCC,0x15,
0x00,0x17,
0x00,0x38,
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x00,
};


#else

/**
 *	Radio connection image (14x16). Connected and sending / receiving.
 */
PRIVATE BYTE gui__imgRadioReceiving[] =
{
0x00,0x00,
0x78,0x00,
0x84,0x00,
0x32,0x01,
0x49,0x02,
0x84,0x38,
0x30,0x14,
0x30,0x14,
0x84,0x38,
0x49,0x02,
0x32,0x01,
0x84,0x00,
0x78,0x00,
0x00,0x00,
};

/**
 *	Radio connection image (14x16). Connected but not traffic.
 */
PRIVATE BYTE gui__imgRadioIdle[] =
{
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x38,
0x30,0x14,
0x30,0x14,
0x00,0x38,
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x00,
0x00,0x00,
};

#endif

/**
 *	Battery image (16x48)
 */
PRIVATE BYTE gui__imgBattery[] =
{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x70, 0x77, 0x77, 0x77, 0x77, 0x77,
	0x10, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x1E, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x12, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x12, 0x00, 0x00, 0x00, 0x00, 0x40,

	0x12, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x12, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x1E, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x10, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x70, 0x77, 0x77, 0x77, 0x77, 0x77,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

/** 
 *	Drawing functions by state.
 */
PRIVATE const_P gui__fnBayDraw gui__bayDraw[] =
{
	gui__viewBayDrawIdle,
	gui__viewBayDrawCharging,
	gui__viewBayDrawAlarm,
	gui__viewBayDrawCAN,
	gui__viewBayDrawPopup
};

/** 
 *	Status change texts (pop ups).
 */
PRIVATE const_P sys_TxtHandle gui__bayStatusTexts[] =
{
	txt_i_tUnknownStatus,
	txt_i_tBmInitSuccess,
	txt_i_tBmInitFail,
	txt_i_tEqualizeEnabled,
	txt_i_tEqualizeDisabled,
	txt_i_tWaterEnabled,
	txt_i_tWaterDisabled,
	txt_i_tAirEnabled,
	txt_i_tAirDisabled,
	txt_i_tRemoteOutEnabled,
	txt_i_tRemoteOutDisabled,
	txt_i_tWaterNeeded,
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Process view message handler.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to process view
* \param		msg			view message id
* \param		pArgs		ptr to message args
*
* \return		Int8
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewBayProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__BayView *			pBay = (gui__BayView *)pView;

/*	DEB_DBG(deb_logN("GUI.bay.Proc: %s", gui__dbgCommands[msg]));*/

	switch (msg)
	{
	/*
	 *	Create message.
	 */
	case gui_msg_create:
		break;

	/*
	 *	Initialization message.
	 */
	case gui_msg_init:
		{
			pBay->alarm_idx = 0xFF;
			pBay->tick = 0;

			/*
			 *	Reset the status change indication. Only status changes that
			 *	happen while process view is open are indicated.
			 */
			pInst->status_rep = SUP_REP_NONE;

			pInst->state &= ~GUI_ST_PROCESS_POP;

			/* 
			 *	Clear radio activity flag so event when bay view is not visible 
			 *	do not affect the radio icon.
			 */
			pInst->state &= ~GUI_ST_RADIO;

			/* Start timer at 1s interval */
			gui__viewTmrStart(pView, 10, 10);
		}
		break;

	/*
	 *	View closing.
	 */
	case gui_msg_close:
		pInst->state &= ~GUI_ST_PROCESS_POP;
		gui__viewTmrStop(pView);
		break;

	/*
	 *	View timer timed out.
	 */
	case gui_msg_timeout:
		{
			/*
			 *	Refresh process view every 1s.
			 */
			gui__viewInvalidate(pView);

			if (pBay->baystate == GUI_BAY_ALARM)
			{
				if (++pBay->tick >= GUI_BAY_ALARM_TICK)
				{
					gui__viewBayNextAlarm(pInst, pBay);
					pBay->tick = 0;
				}
			}
			else if (pBay->baystate == GUI_BAY_POPUP)
			{
				if (++pBay->tick >= GUI_BAY_POPUP_TIME)
				{
					if(pBay->substate & GUI_BAY_ST_WATERING){
						pBay->tick = GUI_BAY_POPUP_TIME;	// No timeout, wait for ESC or battery disconnection
					}
					else{
						pInst->state &= ~GUI_ST_PROCESS_POP;
						pBay->tick = 0;
					}
				}
			}
			else if (pBay->substate & (GUI_BAY_ST_PAUSED|GUI_BAY_ST_TIME_RES))
			{
				/*
				 *	Time restriction (no charge) or manual stopped (charging).
				 */
				if (++pBay->tick >= GUI_BAY_ESC_TIME)
				{
					pBay->tick = GUI_BAY_ESC_TIME;
				}
			}
		}
		break;

	/*
	 *	Paint message.
	 */
	case gui_msg_paint:
		{
#if 0 /* For debugging */
gui__viewBayDebugDraw(pInst, pBay);
gui__viewSetPainted(pView);
break;
#endif
			/*
			 *	Update states
			 */	
			gui__viewBayUpdateState(pInst, pBay);

			gui__dispClear(pInst);

			/*
			 *	Draw stuff always present: radio status, time and battery id.
			 */
			gui__viewBayDrawRadio(pInst, pBay);
			gui__viewBayDrawTime(pInst, pBay);
			gui__viewBayDrawId(pInst, pBay);

			/*
			 *	Draw state dependent stuff.
			 */
			gui__bayDraw[pBay->baystate](pInst, pBay);

			gui__viewSetPainted(pView);
		}
		break;

	/*
	 *	Key down event
	 */
	case gui_msg_key_down:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
				case GUI_VKEY_DOWN:
					{
						/*
						 * Set Join enable if on network
						 * else try JoinNetwork
						 */
						Uint8	NwkStatus;
						Uint8 	NwkSettings;
						Uint8	JoinEnable = 1;
						Uint8	JoinNetwork = 1;

						if(pInst->keyCount < 15)
						{
							pInst->keyCount++;

							if(pInst->keyCount >= 15)
							{
								reg_get(&NwkSettings, gui_NwkSettings);
								if(NwkSettings != RADIO_NWKSETT_DEFAULT)
								{
									reg_get(&NwkStatus, gui_nwkStatus);
									if(NwkStatus == RADIO__NWKSTATUS_CONNECTED)
									{
										reg_put(&JoinEnable, gui_joinEnable);
									}
									else
									{
										reg_put(&JoinNetwork, gui_joinNetwork);
									}
								}
							}
						}
					}
					break;

				default:
					break;
			}

		}
		break;

	/*
	 *	Key up event
	 */
	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_OK:
				if (pInst->nav.pTable->count > 0)
				{
					if (gui_isMenuRow(&pInst->nav.pTable->pItems[0]))
					{
						gui__navigate(pInst, gui_nav_in);
					}
				}
				break;

			case GUI_VKEY_ESC:
			{
				if (
					(pBay->substate & (GUI_BAY_ST_PAUSED|GUI_BAY_ST_TIME_RES)) &&
					(pBay->tick >= GUI_BAY_ESC_TIME)
				) {
					msg_sendTry(
						MSG_LIST(statusRep),
						PROJECT_MSGT_STATUSREP,
						SUP_STAT_START_CHARGING
					);
				}
				else
				{
					if(pBay->substate & GUI_BAY_ST_WATERING){
						Uint8 manualWater = FALSE;
						reg_put(&manualWater, gui_Water_man);
					}
				}

				break;
			}

			case GUI_VKEY_UP:
			case GUI_VKEY_DOWN:
			case GUI_VKEY_LEFT:
			case GUI_VKEY_RIGHT:
			default:
				break;
			}
			pInst->keyCount = 0;
		}
		break;

	default:
		break;
	}

	return 0;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws radion status icon.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to the view
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayDrawRadio(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
//	pView->substate |= GUI_BAY_ST_RADIO;
//	pView->substate |= GUI_BAY_ST_RADIO_R;

	if (pView->substate & GUI_BAY_ST_RADIO)
	{
		gui__dispSetPos(pInst, GUI_BAY_RAD_LEFT, GUI_BAY_RAD_TOP);

		if (pView->substate & GUI_BAY_ST_RADIO_R)
		{
			/*
			 *	Radio traffic.
			 */
			gui__dispDrawImage(
				pInst, 
				&gui__imgRadioReceiving[0], 
				GUI_BAY_RAD_WIDTH, 
				GUI_BAY_RAD_HEIGHT
			);
		}
		else
		{
			/*
			 *	No radio traffic.
			 */
			gui__dispDrawImage(
				pInst, 
				&gui__imgRadioIdle[0], 
				GUI_BAY_RAD_WIDTH, 
				GUI_BAY_RAD_HEIGHT
			);
		}
	}
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws battery image.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to the view
* \param		percent		battery fill level
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayDrawBattery(
	gui__Inst *				pInst,
	gui__BayView *			pView,
	Uint8					percent
) {
	display_Rect			rect;

	gui__dispSetPos(pInst, GUI_BAY_BAT_LEFT, GUI_BAY_BAT_TOP);
	gui__dispDrawImage(pInst, &gui__imgBattery[0], GUI_BAY_BAT_WIDTH, GUI_BAY_BAT_HEIGHT);

	rect.x = GUI_BAY_BAT_I_LEFT;
	rect.width = GUI_BAY_BAT_I_WIDTH;

	if (percent > 100)
	{
		percent = 100;
	}

	/*
	 *	Image height is 48 pixels with one empty pixel at every side.
	 *	Battery 'top' is three pixels.
	 */
	rect.height = (display_Coord)((GUI_BAY_BAT_I_HEIGHT * (Uint32)percent) / 100UL);
	rect.y = GUI_BAY_BAT_I_BTM - rect.height;

	gui__dispFilledRect(pInst, &rect);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws current time.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to the view
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayDrawTime(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	Uint8					len;
	Posix_Time				time;
	display_Rect			rect;

	reg_get(&time, gui_datetime_read);

	len = gui__frmtDateToAsc(pInst->dt_format, pInst->buffer.buf1, time);
	pInst->buffer.buf1[len++] = ' ';
	len += gui__frmtTimeToAsc(pInst->dt_format, &pInst->buffer.buf1[len], time);

	if (pView->substate & GUI_BAY_ST_RADIO)
		rect.x = GUI_BAY_RAD_LEFT + GUI_BAY_RAD_WIDTH;
	else
		rect.x = GUI_BAY_RAD_LEFT + 0;
	rect.width = pInst->pInit->width - rect.x;
	rect.y = 0;
	rect.height = pInst->disp.pFont->height;

	gui__fontSelect(pInst, GUI_FNT_6x8);
	gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);

	gui__dispDrawTextAlignedEx(
		pInst, 
		pInst->buffer.buf1, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws connected battery id.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to view instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayDrawId(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	display_Rect			rect;
	Uint8					len = 0;
	Uint32					id;

	/*
	 *	Read battery id and check if battery with id is connected
	 */
	reg_get(&id, BID);

	if (id == 0)
	{
		return;
	}

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	/* Write B-ID */
	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tBID));		
	pInst->text.pCurrent[len++] = ' ';

	/* Write battery id */
	len += gui__u32toa(
		id, 
		&pInst->text.pCurrent[len], 
		pInst->disp.chr_per_line - len
	);

	/*
	 *	Draw to display
	 */
	gui__fontSelect(pInst, GUI_FNT_6x8);

	if (pView->substate & GUI_BAY_ST_RADIO)
		rect.x = GUI_BAY_RAD_LEFT + GUI_BAY_RAD_WIDTH;
	else
		rect.x = GUI_BAY_RAD_LEFT + 0;
	rect.width = pInst->pInit->width - rect.x;
	rect.y = 8;
	rect.height = pInst->disp.pFont->height;

	gui__dispDrawTextAligned(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Updates process view state by reading state registers from
*				other FB's.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to view instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayUpdateState(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	Uint16					u16;
	Uint16					sup_status;
	Uint8					radio_mode;
	Uint8					supervision_state;
	Boolean					restricted = FALSE;
	ChargerStatusFields_type ReguStatus;
	ChargerStatusFields_type CcStatus;

	reg_get(&ReguStatus, gui_ChargerStatus);
	reg_get(&CcStatus.Sum, gui_CcStatus);

	{
		Uint8 state;

		reg_get(&state, gui_State);
		if(state == 4){
			Uint8 CommProfile;

			reg_get(&CommProfile, gui_CAN_CommProfile);
			if(CommProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
				ReguStatus.Detail.Warning.Detail.Watchdog = 0;	// #JJ
				//CcStatus.Error.Detail.Watchdog = 0;
			}
		}
	}

	if (pView->substate & (GUI_BAY_ST_PAUSED|GUI_BAY_ST_TIME_RES))
	{
		restricted = TRUE;
	}

	/*
	 * Get supervision state
	 */
	reg_get(&supervision_state, gui_supervisionState);

	/*
	 *	Read all alarm registers and parse them into a single variable.
	 */
	reg_get(&u16, gui_ChalgError);
	pView->alarms = u16 & GUI_BAY_CHALG_MASK;
	pView->alarms |= ((Uint32)(ReguStatus.Detail.Error.Sum)) << 16;
	pView->alarms |= ((Uint32)(ReguStatus.Detail.Warning.Sum)) << 24;

	reg_get(&sup_status, gui_statusFlags);
	pView->alarms2 = (Uint32)(sup_status & GUI_BAY_SUP_MASK);

	/*
	 *	Update radio state
	 */
	reg_get(&radio_mode, gui_RadioMode);

	pView->substate = 0;

	/* Radio on / off state */
	if (radio_mode == RADIO_MODE_ENABLED)
	{
		/* Radio is on */
		pView->substate |= GUI_BAY_ST_RADIO;
	}

	/* Radio traffic */
	if (pInst->state & GUI_ST_RADIO)
	{
		pView->substate |= GUI_BAY_ST_RADIO_R;
		pInst->state &= ~GUI_ST_RADIO;
	}

	if (pInst->state & GUI_ST_REG_TIMEOUT)
	{
		/*
		 *	Only display alarm subview when REG fails
		 */
		pView->baystate = GUI_BAY_ALARM;
		return;
	}
	
	if ((pInst->state & GUI_ST_PROCESS_POP) && (!pView->alarms) && (!pView->alarms2))	// #JJ alarm view
	{
		if(pView->status_rep == SUP_REP_WATER_MANUAL_ENABLED){
			/*
			 * Special handling for manual water "popup" which does not time out
		 	*/
			if(pInst->status_rep == SUP_REP_WATER_MANUAL_DISABLED){
				pInst->state &= ~GUI_ST_PROCESS_POP;
				pInst->status_rep = SUP_REP_NONE;
				pView->tick = 0; /* Reset tick counter */
			}
			else{
				pView->baystate = GUI_BAY_POPUP;
				pView->substate |= GUI_BAY_ST_WATERING;
			}
		}
		else{
			/*
			 *	Popup is active and there are no alarms => let the popup stay
			 *	active until it is cleared by the timer.
			 */
			pView->baystate = GUI_BAY_POPUP;
		}

		return;
	}

	if ((pView->alarms != 0) || (pView->alarms2 != 0))	// #JJ alarm view
	{
		/*
		 *	Alarm is on. This has the highest priority.
		 */
		if (pView->baystate != GUI_BAY_ALARM)
		{
			/*
			 *	If currently not in alarm state reset tick counter so that first
			 *	item always stays on for the whole time. 
			 */
			pView->tick = 0;
			pView->alarm_idx = 0xFF;
		}

		if (supervision_state == SUP_STATE_PAUSE)
		{
			/*
			 *	Paused
			 */
			pView->substate |= GUI_BAY_ST_PAUSED;
		}

		pView->baystate = GUI_BAY_ALARM;	
	}
	else
	{
		pView->baystate = 0;

		if (pInst->status_rep != SUP_REP_NONE)
		{
			/*
			 *	Status report change has the second highest priority.
			 */
			pView->baystate = GUI_BAY_POPUP;
			pInst->state |= GUI_ST_PROCESS_POP;
			pView->status_rep = pInst->status_rep;
			pInst->status_act = pInst->status_rep;

			pView->tick = 0; /* Reset tick counter */

			pInst->status_rep = SUP_REP_NONE;
		}
		else
		{
			Uint16				chalg_status;
			Uint8				canProfile;

			/*
			 *	Check CAN profile.
			 */
			reg_get(&canProfile, gui_CAN_CommProfile);

			if (canProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT)
			{
				/*
				 *	CAN mode = master external
				 */
				pView->substate |= GUI_BAY_ST_MSTR_CTRL;
			}
			if (canProfile == CAN_COMM_PROFILE_SLAVE)
			{
				/*
				 *	CAN mode = slave
				 */
				pView->substate |= GUI_BAY_ST_SLAVE_CTRL;
			}

			reg_get(&chalg_status, gui_ChalgStatus);

			/*
			 *	Get battery connected status.
			 */
			if (!CcStatus.Detail.Derate.Detail.NoLoad)
			{
				pView->substate |= GUI_BAY_ST_BATCON;
			}

			/*
			 *	Get charging completed status.
			 */
			if (chalg_status & CHALG_STAT_CHARG_COMPLETED)
			{
				pView->substate |= GUI_BAY_ST_COMPLETE;
			}

			/*
			 *	Get charging state.
			 */
			if (chalg_status & CHALG_STAT_PRE_CHARG)
			{
				pView->substate |= GUI_BAY_ST_PRE;
			}
			if (chalg_status & CHALG_STAT_MAIN_CHARG)
			{
				pView->substate |= GUI_BAY_ST_MAIN;
			}
			else if (chalg_status & CHALG_STAT_ADDITIONAL_CHARG)
			{
				pView->substate |= GUI_BAY_ST_ADDIT;
			}
			else if (chalg_status & CHALG_STAT_EQUALIZE_CHARG)
			{
				pView->substate |= GUI_BAY_ST_EQU;
			}
			else if (chalg_status & CHALG_STAT_MAINTENANCE_CHARG)
			{
				pView->substate |= GUI_BAY_ST_MAINTEN;
			}

			if (pView->substate & (GUI_BAY_ST_MSTR_CTRL| GUI_BAY_ST_SLAVE_CTRL))
			{
				/*
				 *	CAN Controlled.
				 */
				if (pView->substate & GUI_BAY_ST_SLAVE_CTRL)
				{
					pView->baystate = GUI_BAY_CAN_CTRL;

					if (sup_status & SUP_STATUS_REMOTERESTR)
					{
						/*
						 *	Remote restricted.
						 */
						pView->baystate = GUI_BAY_NO_CHARGE;
						pView->substate |= (GUI_BAY_ST_REMOTE | GUI_BAY_ST_BATCON);
					}
					else if (supervision_state == SUP_STATE_PAUSE)
					{
						/*
						 *	Paused
						 */
						pView->substate |= GUI_BAY_ST_PAUSED;
					}
				}
				else if (pView->substate & GUI_BAY_ST_MSTR_CTRL)
				{
					if(supervision_state == SUP_STATE_IDLE)
					{
						pView->baystate = GUI_BAY_NO_CHARGE;
						pView->substate &=~GUI_BAY_ST_BATCON;
					}
					else if (supervision_state == SUP_STATE_CHARGING)
					{
						extern int chargingState;

						pView->baystate = GUI_BAY_CHARGING;

						if(chargingState == TUI__READY)
						{
							/* BBC ready */
							pView->substate |= GUI_BAY_ST_BBC_R;
						}
						else
						{
							/* BBC charging */
							pView->substate |= GUI_BAY_ST_BBC_C;
						}
					}
					else if (supervision_state == SUP_STATE_PAUSE)
					{
						/*
						 *	Paused
						 */
						pView->baystate = GUI_BAY_CHARGING;
						pView->substate |= GUI_BAY_ST_PAUSED;
					}
					else if (sup_status & SUP_STATUS_REMOTERESTR)
					{
						/*
						 *	Remote restricted.
						 */
						pView->baystate = GUI_BAY_NO_CHARGE;
						pView->substate |= (GUI_BAY_ST_REMOTE | GUI_BAY_ST_BATCON);
					}
					else if (sup_status & SUP_STATUS_TIMERESTRICT)
					{
						/*
						 *	Time constrained.
						 */
						pView->baystate = GUI_BAY_NO_CHARGE;
						pView->substate |= (GUI_BAY_ST_TIME_RES | GUI_BAY_ST_BATCON);
					}
				}
			}
			else if (supervision_state == SUP_STATE_PAUSE)
			{
				/*
				 *	Paused
				 */
				pView->substate |= GUI_BAY_ST_PAUSED;
				pView->baystate = GUI_BAY_CHARGING;
			}
			else if((CcStatus.Detail.Derate.Detail.NoLoad) ||
					(supervision_state == SUP_STATE_IDLE)) /*||
					(supervision_state == SUP_STATE_INIT_CHARGING))*/
			{
				pView->baystate = GUI_BAY_NO_CHARGE;
				pView->substate &=~GUI_BAY_ST_BATCON;
			}
			else if (sup_status & SUP_STATUS_REMOTERESTR)
			{
				/*
				 *	Remote restricted.
				 */
				pView->baystate = GUI_BAY_NO_CHARGE;
				pView->substate |= GUI_BAY_ST_REMOTE;
			}
			else if (sup_status & SUP_STATUS_TIMERESTRICT)
			{
				/*
				 *	Time constrained.
				 */
				pView->baystate = GUI_BAY_NO_CHARGE;
				pView->substate |= GUI_BAY_ST_TIME_RES;
			}
			else
			{
				Uint8			bbc_func;	//Only used if special window when BBC
				Uint8			NwkStatus;			//Only used if special window when BBC
				Uint8			ChargingMode;

				reg_get(&bbc_func, gui_BBC_func);	//Only used if special window when BBC
				reg_get(&NwkStatus, gui_nwkStatus);	//Only used if special window when BBC
				reg_get(&ChargingMode, gui_ChargingMode);

				pView->baystate = GUI_BAY_CHARGING;

				if (supervision_state == SUP_STATE_PAUSE)
				{
					/*
					 *	Paused
					 */
					pView->substate |= GUI_BAY_ST_PAUSED;
				}
				else if (
					(ChargingMode == CHALG_BM || ChargingMode == CHALG_DUAL || ChargingMode == CHALG_MULTI) &&
					((supervision_state == SUP_STATE_INIT_CHARGING) ||
					 (supervision_state == SUP_STATE_INIT_BM) ||
					 (supervision_state == SUP_STATE_INIT_BM_2) ||
					 (supervision_state == SUP_STATE_INIT_BM_3) ||
					 (supervision_state == SUP_STATE_RETRIEVE_BM_DATA))
				){
					/* BM init */
					pView->substate |= GUI_BAY_ST_BM_INIT;
				}
				else if((supervision_state == SUP_STATE_INIT_BM_4) ||
						(supervision_state == SUP_STATE_INIT_BM_5) ||
						(supervision_state == SUP_STATE_INIT_BM_6))
				{
					/* BMU calibration */
					pView->substate |= GUI_BAY_ST_BM_CALIB;
				}
				else if(supervision_state == SUP_STATE_VOLTAGE_SENSE)
				{
					/* Voltage sense  */
					pView->substate |= GUI_BAY_ST_VSENSE;
				}
				else if (bbc_func && (NwkStatus == RADIO__NWKSTATUS_CONNECTED))
				{
					Uint8		bbc;
					Uint8		BBCStatus;

					reg_get(&bbc, gui_BBC);
					reg_get(&BBCStatus, gui_BBCStatus);

					if (bbc && (BBCStatus & BBC_STAT_COMPLETED))
					{
						/* BBC ready */
						pView->substate |= GUI_BAY_ST_BBC_R;
					}
					else
					{
						/* BBC charging */
						pView->substate |= GUI_BAY_ST_BBC_C;
					}
				}
			}
		}
	}

	if (pView->substate & (GUI_BAY_ST_PAUSED|GUI_BAY_ST_TIME_RES))
	{
		if (restricted == FALSE)
		{
			/*
			 *	Restricted mode was not on but now is => start the timer	
			 */
			pView->tick = 0;
		}
	}
	else if (restricted)
	{
		// Was restricted but is not anymore
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws process view in idle state (no charging).
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to view instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayDrawIdle(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	display_Rect			rect;
	Uint8					len;

	BYTE *pTempText;

	gui__fontSelect(pInst, GUI_FNT_8x16);
	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	rect.x = 0;
	rect.width = pInst->pInit->width;
	rect.y = 3 * 8;
	rect.height = pInst->disp.pFont->height * 2;

	if (!(pView->substate & GUI_BAY_ST_BATCON))
	{
		/*
		 *	Battery not connected => draw CONNECT BATTERY
		 */
		len = gui__textWrite(
			pInst, 
			txt_localHandle(pInst->instid, txt_i_tConnectBattery)
		);

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			GUI_ALIGN_CENTER
		);
	}
	else if (pView->substate & GUI_BAY_ST_REMOTE)
	{
		/*
		 *	Battery connected, remote off => REMOTE START
		 */
		len = gui__textWrite(
			pInst, 
			txt_localHandle(pInst->instid, txt_i_tRemoteStart)
		);

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			GUI_ALIGN_CENTER
		);
	}
	else if (pView->substate & GUI_BAY_ST_TIME_RES)
	{
		Uint8				hour;
		Uint8				min;
		Uint8				wday;

		/*
		 *	Battery connected, restricted by time control.
		 */
		rect.y = 24;
		rect.x = 0;
		rect.width = pInst->pInit->width;
		len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tStart));

		pTempText = &pInst->text.pCurrent[len];

		if (gui__viewBayStartTime(pInst, &hour, &min, &wday))
		{
			len += gui__frmtTime2ToAsc(
				pInst->dt_format,
				pTempText,
				//pInst->text.pCurrent,
				hour | (min << 8)
			);
		}

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;
		rect.x = 0;
		rect.width = pInst->pInit->width;

		/*
		 *	Draw 'Weekday'
		 */
		gui__fontSelect(pInst, GUI_FNT_8x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);

		sys_TxtHandle			textHandle;
		switch(wday)
		{
			case 0:
				textHandle = txt_localHandle(pInst->instid, txt_i_tMonday);
				break;
			case 1:
				textHandle = txt_localHandle(pInst->instid, txt_i_tTuesday);
				break;
			case 2:
				textHandle = txt_localHandle(pInst->instid, txt_i_tWednesday);
				break;
			case 3:
				textHandle = txt_localHandle(pInst->instid, txt_i_tThursday);
				break;
			case 4:
				textHandle = txt_localHandle(pInst->instid, txt_i_tFriday);
				break;
			case 5:
				textHandle = txt_localHandle(pInst->instid, txt_i_tSaturday);
				break;
			case 6:
				textHandle = txt_localHandle(pInst->instid, txt_i_tSunday);
				break;
			default:
				textHandle = txt_localHandle(pInst->instid, txt_i_tSunday);
				break;

		}

		gui__textWriteAligned(
			pInst,
			txt_text(textHandle),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			pInst->disp.chr_per_line,
			&rect,
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;
#if 0
		/*
		 *	Draw 'Push ESC to'
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tResumeCharging)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			pInst->disp.chr_per_line,
			&rect,
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;

		/*
		 *	Draw 'start now'
		 */

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tStartNow)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			pInst->disp.chr_per_line, 
			&rect, 
			GUI_ALIGN_CENTER
		);
#endif

		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws controlled-by-master state.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view instance
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewBayDrawCAN(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	display_Rect			rect;
	Uint8					len;
	Uint8					u8;


	if (pView->substate & GUI_BAY_ST_PAUSED)
	{
		/*
		 *	Paused. Draw
		 *	MABUAL STOPPED
		 *	Push ESC to
		 *	resume charging
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);

		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tManualStopped));

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			u8,
			&rect,
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;


		/*
		 *	Draw 'Push ESC to'
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);
//		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tResumeCharging)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			pInst->disp.chr_per_line,
			&rect,
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;

		/*
		 *	Draw 'resume charging'
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);
//		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tResumeCharging2)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			pInst->disp.chr_per_line,
			&rect,
			GUI_ALIGN_CENTER
		);

		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);

		return;
	}

	/*
	 *	Draw CAN CONTROLLED
	 *	Draw MASTER/SLAVE
	 */
	gui__fontSelect(pInst, GUI_FNT_8x16);
	gui__textSelectBuffer(pInst, pInst->buffer.buf1);


	rect.x = 0;
	rect.width = pInst->pInit->width;
	rect.y = 3 * 8;
	rect.height = pInst->pInit->height - rect.y;

	if (pView->substate & GUI_BAY_ST_MSTR_CTRL){
		len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tMasterCtrl));
	}
	else{
		len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tSlaveCtrl));
	}

	gui__dispDrawTextAlignedEx(
		pInst,
		pInst->text.pCurrent,
		len,
		&rect,
		GUI_ALIGN_CENTER
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws status change message (a popup).
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view instance
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewBayDrawPopup(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	display_Rect			rect;
	Uint8					len;
	sys_TxtIndex			text;
	Uint8					status_rep = pView->status_rep;

	gui__fontSelect(pInst, GUI_FNT_8x16);
	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	rect.x = 0;
	rect.width = pInst->pInit->width;
	rect.y = 3 * 8;
	rect.height = pInst->pInit->height - rect.y;

	if (status_rep >= dim(gui__bayStatusTexts))
	{
		status_rep = 0;
	}

	text = gui__bayStatusTexts[status_rep];

	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, text));

	gui__dispDrawTextAlignedEx(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Draws process view in charging state.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view instance
*
*	\details		
*
*	\note			
*
*	\sa			
*
******************************************************************************/
PRIVATE void gui__viewBayDrawCharging(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	display_Rect			rect;
	sys_TxtHandle			txt;
	Uint8					u8;

	if (pView->substate & GUI_BAY_ST_BM_INIT)
	{
		/*
		 *	Just draw BM INIT text if battery module is initalizing.
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height * 2;
		
		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tBmInit));

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			u8, 
			&rect, 
			GUI_ALIGN_CENTER
		);

		{
			/*
			 * Get supervision state
			 */
			Uint8 supState;
			reg_get(&supState, gui_supervisionState);

			if(supState == SUP_STATE_INIT_BM_3)
			{

				rect.y += pInst->disp.pFont->height;

				/*
				 *	Draw 'Success!'
				 */
					gui__fontSelect(pInst, GUI_FNT_6x8);
					gui__textSelectBuffer(pInst, pInst->buffer.buf1);

				gui__textWriteAligned(
					pInst,
					txt_text(txt_localHandle(pInst->instid, txt_i_tSuccess)),
					0,
					pInst->disp.chr_per_line,
					GUI_ALIGN_CENTER
				);

				rect.height = pInst->disp.pFont->height;

				gui__dispDrawTextAlignedEx(
					pInst,
					pInst->text.pCurrent,
					pInst->disp.chr_per_line,
					&rect,
					GUI_ALIGN_CENTER
				);
			}
		}

		return;
	}

	if (pView->substate & GUI_BAY_ST_BM_CALIB)
	{
		/*
		 *	Just draw BMU CALIBRATION text if battery module is beeing calibrated.
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height * 2;

		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tBmCalibration));

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			u8,
			&rect,
			GUI_ALIGN_CENTER
		);

		{
			/*
			 * Get supervision state
			 */
			Uint8 supState;
			reg_get(&supState, gui_supervisionState);

			if(supState == SUP_STATE_INIT_BM_5)
			{

				rect.y += pInst->disp.pFont->height;

				/*
				 *	Draw 'Success!'
				 */
					gui__fontSelect(pInst, GUI_FNT_6x8);
					gui__textSelectBuffer(pInst, pInst->buffer.buf1);

				gui__textWriteAligned(
					pInst,
					txt_text(txt_localHandle(pInst->instid, txt_i_tSuccess)),
					0,
					pInst->disp.chr_per_line,
					GUI_ALIGN_CENTER
				);

				rect.height = pInst->disp.pFont->height;

				gui__dispDrawTextAlignedEx(
					pInst,
					pInst->text.pCurrent,
					pInst->disp.chr_per_line,
					&rect,
					GUI_ALIGN_CENTER
				);
			}

			if(supState == SUP_STATE_INIT_BM_6)
			{

				rect.y += pInst->disp.pFont->height;

				/*
				 *	Draw 'Fail!'
				 */
					gui__fontSelect(pInst, GUI_FNT_6x8);
					gui__textSelectBuffer(pInst, pInst->buffer.buf1);

				gui__textWriteAligned(
					pInst,
					txt_text(txt_localHandle(pInst->instid, txt_i_tFail)),
					0,
					pInst->disp.chr_per_line,
					GUI_ALIGN_CENTER
				);

				rect.height = pInst->disp.pFont->height;

				gui__dispDrawTextAlignedEx(
					pInst,
					pInst->text.pCurrent,
					pInst->disp.chr_per_line,
					&rect,
					GUI_ALIGN_CENTER
				);
			}
		}

		return;
	}

	if (pView->substate & GUI_BAY_ST_VSENSE)
	{
		/*
		 *	Just draw CHARGING INIT text while voltage sense state.
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height * 2;

		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tVoltageSense));

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			u8,
			&rect,
			GUI_ALIGN_CENTER
		);

		return;
	}

	if (pView->substate & GUI_BAY_ST_PAUSED)
	{
		/*
		 *	Paused. Draw
		 *	MABUAL STOPPED
		 *	Push ESC to
		 *	resume charging
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);

		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tManualStopped)); 

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			u8, 
			&rect, 
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;


		/*
		 *	Draw 'Push ESC to'
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);
//		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tResumeCharging)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			pInst->disp.chr_per_line, 
			&rect, 
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;

		/*
		 *	Draw 'resume charging'
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);
//		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tResumeCharging2)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			pInst->disp.chr_per_line,
			&rect,
			GUI_ALIGN_CENTER
		);

		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);

		return;
	}

	if (pView->substate & GUI_BAY_ST_BBC_C)
	{
		/*
		 *	BBC charging. Draw
		 *	CHARGING
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);

		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tCharging));

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			u8,
			&rect,
			GUI_ALIGN_CENTER
		);

		/*
		 *	Battery level from SUP's SOC register.
		 */
		Uint8 canProfile;
		reg_get(&canProfile, gui_CAN_CommProfile);

		if(canProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
			Uint8 len;
			Uint8 rowlen;

			reg_get(&u8, gui_SOC);
			gui__viewBayDrawBattery(pInst, pView, u8);

			/*
			 *	Draw 'XXX% SOC'
			 */
			rect.y += pInst->disp.pFont->height;
			gui__fontSelect(pInst, GUI_FNT_8x16);
			rect.height = pInst->disp.pFont->height;

			/* Get number of chars that can be fit into the space */
			rowlen = rect.width / pInst->disp.pFont->width;

			len = gui__viewBayWriteValue(pInst, GUI__BAY_IDX_SOC, txt_i_tSocUnit, rowlen);

			gui__dispDrawTextAligned(
				pInst,
				pInst->text.pCurrent,
				len,
				&rect,
				GUI_ALIGN_CENTER
			);
		}
		return;
	}

	if (pView->substate & GUI_BAY_ST_BBC_R)
	{
		/*
		 *	BBC charging. Draw
		 *	READY
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);

		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tReady));

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			u8,
			&rect,
			GUI_ALIGN_CENTER
		);

		/*
		 *	Battery level from SUP's SOC register.
		 */
		Uint8 canProfile;
		reg_get(&canProfile, gui_CAN_CommProfile);

		if(canProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
			Uint8 len;
			Uint8 rowlen;

			reg_get(&u8, gui_SOC);
			gui__viewBayDrawBattery(pInst, pView, u8);

			/*
			 *	Draw 'XXX% SOC'
			 */
			rect.y += pInst->disp.pFont->height;
			gui__fontSelect(pInst, GUI_FNT_8x16);
			rect.height = pInst->disp.pFont->height;

			/* Get number of chars that can be fit into the space */
			rowlen = rect.width / pInst->disp.pFont->width;

			len = gui__viewBayWriteValue(pInst, GUI__BAY_IDX_SOC, txt_i_tSocUnit, rowlen);

			gui__dispDrawTextAligned(
				pInst,
				pInst->text.pCurrent,
				len,
				&rect,
				GUI_ALIGN_CENTER
			);
		}
		return;
	}

	/*
	 *	Battery level from SUP's SOC register.
	 */
	Uint16 chalg_status;
	reg_get(&chalg_status, gui_ChalgStatus);

	if(chalg_status & CHALG_STAT_BM_CONNTECTED){
		reg_get(&u8, gui_SOC);
		gui__viewBayDrawBattery(pInst, pView, u8);
	}

	/*
	 *	Charging state text
	 */
	if (pView->substate & GUI_BAY_ST_PRE)
	{
		txt = txt_localHandle(pInst->instid, txt_i_tChargePre);
	}
	else if (pView->substate & GUI_BAY_ST_MAIN)
	{
		txt = txt_localHandle(pInst->instid, txt_i_tChargeMain);
	}
	else if (pView->substate & GUI_BAY_ST_ADDIT)
	{
		txt = txt_localHandle(pInst->instid, txt_i_tChargeAdditional);
	}
	else if (pView->substate & GUI_BAY_ST_EQU)
	{
		txt = txt_localHandle(pInst->instid, txt_i_tChargeEqualize);
	}
	else if (pView->substate & GUI_BAY_ST_MAINTEN)
	{
		txt = txt_localHandle(pInst->instid, txt_i_tChargeMaintenance);
	}
	else
	{
		txt = GUI_NO_TEXT;
	}

	if (txt != GUI_NO_TEXT)
	{
		gui__fontSelect(pInst, GUI_FNT_8x8);
		if(chalg_status & CHALG_STAT_BM_CONNTECTED)
			rect.x = GUI_BAY_BAT_LEFT + GUI_BAY_BAT_WIDTH;
		else
			rect.x = GUI_BAY_BAT_LEFT + 0;
		rect.width = pInst->pInit->width - rect.x;
		rect.y = 16;
		rect.height = pInst->disp.pFont->height;

		u8 = gui__textWrite(pInst, txt); 

		gui__dispDrawTextAligned(
			pInst, 
			pInst->text.pCurrent, 
			u8, 
			&rect, 
			GUI_ALIGN_CENTER
		);
	}

	if (pView->substate & GUI_BAY_ST_COMPLETE)
	{
		/*
		 *	Charging complete.
		 */
		gui__viewBayDrawStateReady(pInst, pView);
	}
	else
	{
		/*
		 *	Still charging.
		 */
		gui__viewBayDrawStateCharging(pInst, pView);
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws process view in alarm state.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to view instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayDrawAlarm(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	display_Rect			rect;
	Uint8					len1;
	Uint8					len2 = 0;
	Uint8					u8;
	sys_TxtHandle			txt;

	if (pView->substate & GUI_BAY_ST_PAUSED)
	{
		/*
		 *	Paused. Draw
		 *	MABUAL STOPPED
		 *	Push ESC to
		 *	resume charging
		 */
		gui__fontSelect(pInst, GUI_FNT_8x16);

		u8 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tManualStopped));

		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 24;
		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			u8,
			&rect,
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;


		/*
		 *	Draw 'Push ESC to'
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);
//		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tResumeCharging)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			pInst->disp.chr_per_line,
			&rect,
			GUI_ALIGN_CENTER
		);

		rect.y += pInst->disp.pFont->height;

		/*
		 *	Draw 'resume charging'
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);
//		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);

		gui__textWriteAligned(
			pInst,
			txt_text(txt_localHandle(pInst->instid, txt_i_tResumeCharging2)),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_CENTER
		);

		rect.height = pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst,
			pInst->text.pCurrent,
			pInst->disp.chr_per_line,
			&rect,
			GUI_ALIGN_CENTER
		);

		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);

		return;
	}

	/*
	 *	Draw ALARM!
	 */
	gui__fontSelect(pInst, GUI_FNT_8x16);
	gui__textSelectBuffer(pInst, pInst->buffer.buf1);


	rect.x = 0;
	rect.width = pInst->pInit->width;
	rect.y = 24;
	rect.height = pInst->disp.pFont->height;

	len1 = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tAlarm));

	gui__dispDrawTextAligned(
		pInst, 
		pInst->text.pCurrent, 
		len1, 
		&rect, 
		GUI_ALIGN_CENTER
	);

	if (pInst->state & GUI_ST_REG_TIMEOUT)
	{
		/*
		*	Draw reg fail message
		*/

		txt = txt_localHandle(pInst->instid, txt_i_tRegBlockFail);
	}
	else {
		/*
		*	Draw alarm reason
		*/
		
		if (pView->alarm_idx == 0xFF)
		{
			gui__viewBayNextAlarm(pInst, pView);
		}

		len1 = pView->alarm_idx;
		gui__textSelectBuffer(pInst, pInst->buffer.buf1);

		if (len1 >= 32)	// #JJ alarm view
		{
			/*
			*	Sup error.
			*/
			txt = gui__dataSupErrorText(pInst, len1 - 32);
		}
		else if (len1 >= 16)
		{
			/*
			*	Regu error.
			*/
			txt = gui__dataReguErrorText(pInst, len1 - 16, NULL);
		}
		else
		{
			/*
			*	Chalg error.
			*/
			txt = gui__dataChalgErrorText(pInst, len1, NULL);
		}	
	}

	if (txt != GUI_NO_TEXT)
	{
		len1 = gui__textWriteBuffer(txt, pInst->text.pCurrent, GUI_MAX_TXT3);
	}
	else
	{
		len1 = 0;
	}

	if (len1 > 0)
	{
		/*
		 *	Draw alarm text.
		 */
		gui__fontSelect(pInst, GUI_FNT_6x8);
		
		rect.x = 0;
		rect.width = pInst->pInit->width;
		rect.y = 5 * pInst->disp.pFont->height;
		rect.height = 3 * pInst->disp.pFont->height;

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			len1, 
			&rect, 
			GUI_ALIGN_CENTER
		);
	}

	if (len2 > 0)
	{
		/*
		 *	Draw alarm value.
		 */
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Finds next active alarm.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to view instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewBayNextAlarm(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	if ((pView->alarms == 0) && (pView->alarms2 == 0))	// #JJ alarms view
	{
		/* No alarms are on */
		pView->alarm_idx = 0xFF;
		return;
	}

	while (1)
	{
		if (++pView->alarm_idx > 31)
		{
			if(pView->alarm_idx > 63)
			{
				pView->alarm_idx = 0;
			}
			else
			{
				if (pView->alarms2 & (1 << (pView->alarm_idx - 32)))	// #JJ alarms view
				{
					break;
				}
			}

		}

		if (pView->alarms & (1 << pView->alarm_idx))
		{
			break;
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Retrieves given register value and formats it for process view
*				display.
*
*	\param		pInst		ptr to GUI instance
*	\param		reg			index of the register in process view menu table
*	\param		unit		unit text handle
*	\param		maxlen		maximum result length
*
*	\return		Lenght of the formatted string.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Uint8 gui__viewBayWriteValue(
	gui__Inst *				pInst,
	Uint8					idx,
	sys_TxtHandle			unit,
	Uint8					maxlen
) {
	gui_MenuItem const_P *	pItem;
	gui_RegInfo				info;
	Uint8					u8;

	pItem = &pInst->nav.pTable->pItems[idx];
	gui__regGetInfo(pItem->reg, &info);

	gui__regGet(pInst, pItem->reg, 0, NULL);

	u8 = gui__frmtForDisplay(pInst, pItem, &info);

	if (u8 < maxlen)
	{
		u8 += gui__textWriteBuffer(
			txt_localHandle(pInst->instid, unit),
			&pInst->text.pCurrent[u8],
			maxlen - u8
		);
	}

	return u8;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws values when charging is ready.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewBayDrawStateReady(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	Uint8					len;
	Uint8					rowlen;
	display_Rect			rect;

	/*
	 *	   Additional	B
	 *		 READY      L
	 *		 0.00A      s
	 *	     2.04V/c    s
	 *	 147Ah 49.60V	s
	 *
	 */

	/*
	 *	Draw READY.
	 */
	gui__fontSelect(pInst, GUI_FNT_8x16);

	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tReady)); 

	Uint16 chalg_status;
	reg_get(&chalg_status, gui_ChalgStatus);

	if(chalg_status & CHALG_STAT_BM_CONNTECTED)
		rect.x = GUI_BAY_BAT_LEFT + GUI_BAY_BAT_WIDTH;
	else
		rect.x = GUI_BAY_BAT_LEFT + 0;
	rect.width = pInst->pInit->width - rect.x;
	rect.y = 24;
	rect.height = pInst->disp.pFont->height;

	gui__dispDrawTextAligned(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);

	/*
	 *	Draw A
	 */
	rect.y += pInst->disp.pFont->height;
	gui__fontSelect(pInst, GUI_FNT_6x8);
	rect.height = pInst->disp.pFont->height;

	/* Get number of chars that can be fit into the space */
	rowlen = rect.width / pInst->disp.pFont->width;

	len = gui__viewBayWriteValue(pInst, GUI__BAY_IDX_I, txt_i_tAUnit, rowlen);

	gui__dispDrawTextAligned(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);

	/*
	 *	Draw V/c
	 */
	rect.y += pInst->disp.pFont->height;
	rect.height = pInst->disp.pFont->height;

	len = gui__viewBayWriteValue(pInst, GUI__BAY_IDX_VC, txt_i_tVcUnit, rowlen);

	gui__dispDrawTextAligned(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);
	
	/* 
	 *	Copy Ah value to buffer 1
	 */

	rect.y += pInst->disp.pFont->height;
	rect.height = pInst->disp.pFont->height;

	rowlen = rect.width / pInst->disp.pFont->width;

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	len = gui__viewBayWriteValue(
		pInst, 
		GUI__BAY_IDX_AH, 
		txt_i_tAhUnit, 
		rowlen
	);

	if (len < rowlen)
	{
		pInst->text.pCurrent[len++] = ' ';
	}

	/* 
	 *	Copy V value to buffer 2
	 */
	gui__textSelectBuffer(pInst, pInst->buffer.buf2);

	rowlen = gui__viewBayWriteValue(
		pInst, 
		GUI__BAY_IDX_U, 
		txt_i_tVUnit, 
		rowlen - len
	);


	/*
	 *	Copy V value from buffer 2 to end of buffer 1 and output buffer 1
	 */
	len += gui__textCopy(&pInst->buffer.buf1[len], pInst->buffer.buf2, rowlen);

	gui__dispDrawTextAligned(
		pInst, 
		pInst->buffer.buf1, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws values when charging.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewBayDrawStateCharging(
	gui__Inst *				pInst,
	gui__BayView *			pView
) {
	Uint8					len;
	Uint8					rowlen;
	display_Rect			rect;

	/*
	 *	   Additional	B
	 *		130.00A		L
	 *		2.07V/c		L
	 *	 147Ah 49.60V	S
	 *
	 */

	/*
	 *	Display A, V/c, AH and V.
	 */
	Uint16 chalg_status;
	reg_get(&chalg_status, gui_ChalgStatus);

	if(chalg_status & CHALG_STAT_BM_CONNTECTED)
		rect.x = GUI_BAY_BAT_LEFT + GUI_BAY_BAT_WIDTH;
	else
		rect.x = GUI_BAY_BAT_LEFT + 0;
	rect.width = pInst->pInit->width - rect.x;

	gui__fontSelect(pInst, GUI_FNT_8x16);

	/* 
	 *	Show A 
	 */
	rect.y = 24;
	rect.height = pInst->disp.pFont->height;

	len = gui__viewBayWriteValue(pInst, GUI__BAY_IDX_I, txt_i_tAUnit, 8);

	gui__dispDrawTextAligned(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);

	/* 
	 *	Show V/c 
	 */
	rect.y += pInst->disp.pFont->height;

	len = gui__viewBayWriteValue(pInst, GUI__BAY_IDX_VC, txt_i_tVcUnit, 8);

	gui__dispDrawTextAligned(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);

	/* 
	 *	Copy Ah value to buffer 1
	 */

	rect.y += pInst->disp.pFont->height;

	gui__fontSelect(pInst, GUI_FNT_6x8);

	rect.height = pInst->disp.pFont->height;

	rowlen = rect.width / pInst->disp.pFont->width;

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	len = gui__viewBayWriteValue(
		pInst, 
		GUI__BAY_IDX_AH, 
		txt_i_tAhUnit, 
		rowlen
	);

	if (len < rowlen)
	{
		pInst->text.pCurrent[len++] = ' ';
	}

	/* 
	 *	Copy V value to buffer 2
	 */
	gui__textSelectBuffer(pInst, pInst->buffer.buf2);

	rowlen = gui__viewBayWriteValue(
		pInst, 
		GUI__BAY_IDX_U, 
		txt_i_tVUnit, 
		rowlen - len
	);


	/*
	 *	Copy V value from buffer 2 to end of buffer 1 and output buffer 1
	 */
	len += gui__textCopy(&pInst->buffer.buf1[len], pInst->buffer.buf2, rowlen);

	gui__dispDrawTextAligned(
		pInst, 
		pInst->buffer.buf1, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		When time restriction is on this returns the starting time.
*
*	\param		pInst		ptr to GUI instance
*	\param		hour		starting time hours
*	\param		min			starting time minutes
*
*	\return		TRUE if starting times were succesfully gotten.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__viewBayStartTime(
	gui__Inst *				pInst,
	Uint8 *					hour,
	Uint8 *					min,
	Uint8 *					pWday
) {
	Posix_Time				time;

	reg_get(&time, gui_restrictEnd);
	reg_get(pWday, gui_restrictEndWday);

	if (time == 0xFFFF)
	{
		/* Unending time restriction */
		return FALSE;
	}
	
	*hour = (Uint8)(time / RTC_SECS_PER_HOUR);
	time -= *hour * RTC_SECS_PER_HOUR;
	*min = (Uint8)(time / RTC_SECS_PER_MIN);

	return TRUE;
}


#if 0
PRIVATE void gui__viewBayDebugDraw(
	gui__Inst *				pInst, 
	gui__BayView *			pView
) {
#if 1
	static Uint8 tmp = 0;

	gui__dispClear(pInst);

	if (tmp++ > 3)
	{
		pView->status_rep++;
	
		if (pView->status_rep >= dim(gui__bayStatusTexts))
		{
			pView->status_rep = 1;
		}

		tmp = 0;
	}

	gui__viewBayDrawPopup(pInst, pView);
#else
	display_Rect			rect;
	Uint8					len;

	gui__dispClear(pInst);

	gui__fontSelect(pInst, GUI_FNT_8x16);
	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	//rect.x = 0;
	//rect.width = pInst->pInit->width;
	//rect.y = 4 * 8;
	//rect.height = 3 * pInst->disp.pFont->height;
	rect.x = 0;
	rect.width = pInst->pInit->width;
	rect.y = 3 * 8;
	rect.height = pInst->pInit->height - rect.y;

	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tEqualizeDisabled));

	gui__dispDrawTextAlignedEx(
		pInst, 
		pInst->text.pCurrent, 
		len, 
		&rect, 
		GUI_ALIGN_CENTER
	);
#endif
}
#endif
