/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Text handling functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
//#include "reg.h"
#include "mem.h"
#include "global.h"

#include "txt.h"

#include "deb.h"
#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/


SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes text handling data.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__textInit(
	gui__Inst *				pInst
) {
	deb_assert(pInst->disp.pFont);

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);
	gui__textClear(pInst, 0, GUI_MAX_TXT);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Clears current text buffer.
*
* \param		pInst		ptr to GUI instance
* \param		pos			starting position
* \param		len			number of chars to clear
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__textClear(
	gui__Inst *				pInst,
	Uint8					pos,
	Uint8					len
) {
	Uint8					maxpos = pos + len;

	deb_assert(maxpos <= GUI_MAX_TXT);

	for (; pos < maxpos; pos++)
	{
		pInst->text.pCurrent[pos] = (BYTE)pInst->disp.pFont->empty;
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Selects current text buffer.
*
* \param		pInst		ptr to GUI instance
* \param		pBuf		buffer to select
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__textSelectBuffer(
	gui__Inst *				pInst,
	BYTE *					pBuf
) {
	pInst->text.pCurrent = pBuf;
}	

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Writes text to current buffer.
*
* \param		pInst		ptr to GUI instance
* \param		txt			text handle
*
* \return		Number of characters written.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__textWrite(
	gui__Inst *				pInst,
	sys_TxtHandle			txt
) {
	return gui__textWriteBuffer(txt, pInst->text.pCurrent, GUI_MAX_TXT);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Writes text to a given buffer.
*
*	\param		txt			text handle
*	\param		pBuf		target buffer
*	\param		maxlen		target buffer size
*
*	\return		Number of bytes written.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Uint8 gui__textWriteBuffer(
	sys_TxtHandle			txt,
	BYTE *					pBuf,
	Uint8					maxlen
) {
	BYTE const_P *			pTxt = txt_text(txt);
	Uint8					len = gui__txtLen(pTxt);
	Uint8					pos = 0;

	if (len > maxlen)
	{
		len = maxlen;
	}
	else
	{
		maxlen = len;
	}


	pTxt = gui__txtBegin(pTxt);

	while (len--)
	{
		pBuf[pos++] = *pTxt++;
	}

	return maxlen;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Writes text to current buffer. Supports text alignment.
*
* \param		pInst		ptr to GUI instance
* \param		txt			text handle
* \param		pos			starting position
* \param		maxlen		max length to write
* \param		flags		alignment flags
*
* \return		Number of characters written.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__textWriteAligned(
	gui__Inst *				pInst,
	BYTE const_P *			txt, 
	Uint8					pos, 
	Uint8					maxlen,
	Uint8					flags
) {
	Uint8					len = gui__txtLen(txt);
	Uint8					start_pos = pos;

	deb_assert((pos + maxlen) <= GUI_MAX_TXT);

	if (len > maxlen)
	{
		len = maxlen;
	}

	maxlen -= len; /* Number of chars left in the buffer after write */

	txt = gui__txtBegin(txt);

	if (flags & GUI_ALIGN_LEFT)
	{
		pos = gui__textWriteConst(pInst, txt, pos, len);

		if (!(flags & GUI_NO_FILL))
		{
			pos = gui__textWriteChars(
				pInst, 
				(BYTE)pInst->disp.pFont->empty, 
				pos, 
				maxlen
			);
		}
	}
	else if (flags & GUI_ALIGN_RIGHT)
	{
		if (!(flags & GUI_NO_FILL))
		{
			pos = gui__textWriteChars(
				pInst, 
				(BYTE)pInst->disp.pFont->empty, 
				pos, 
				maxlen
			);
		}

		pos = gui__textWriteConst(pInst, txt, pos, len);
	}
	else
	{
		Uint8				side;

		deb_assert(flags & GUI_ALIGN_CENTER);

		side = maxlen >> 1;

		if (!(flags & GUI_NO_FILL))
		{
			pos = gui__textWriteChars(pInst, (BYTE)pInst->disp.pFont->empty, pos, side);
		}

		pos = gui__textWriteConst(pInst, txt, pos, len);

		if (!(flags & GUI_NO_FILL))
		{
			pos = gui__textWriteChars(pInst, (BYTE)pInst->disp.pFont->empty, pos, maxlen - side);
		}
	}

	return pos - start_pos;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Puts bytes from source buffer to curret buffer.
*
* \param		pInst		ptr to GUI instance
* \param		pTxt		ptr to source buffer
* \param		pos			starting position
* \param		len			number of bytes to put
*
* \return		Last write position.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__textPutBytes(
	gui__Inst *				pInst,
	BYTE *					pTxt,
	Uint8					pos, 
	Uint8					len
) {
	while (len--)
	{
		pInst->text.pCurrent[pos++] = *pTxt++;
	}

	return pos;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Puts bytes from const source buffer to curret buffer.
*
* \param		pInst		ptr to GUI instance
* \param		pTxt		ptr to source buffer
* \param		pos			starting position
* \param		len			number of bytes to put
*
* \return		Last write position.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__textWriteConst(
	gui__Inst *				pInst,
	BYTE const_P *			pTxt,
	Uint8					pos, 
	Uint8					len
) {
	while (len--)
	{
		pInst->text.pCurrent[pos++] = *pTxt++;
	}

	return pos;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Fills buffer with given character.
*
* \param		pInst		ptr to GUI instance
* \param		chr			fill character
* \param		pos			starting position
* \param		len			number of characters to write
*
* \return		Last write position.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 gui__textWriteChars(
	gui__Inst *				pInst,
	BYTE					chr,
	Uint8					pos, 
	Uint8					len
) {
	while (len--)
	{
		pInst->text.pCurrent[pos++] = chr;
	}

	return pos;
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Copies text from source to target.
*
*	\param		pTarget		target buffer
*	\param		pSource		source buffer
*	\param		bytes		number of bytes to copy
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Uint8 gui__textCopy(
	BYTE *					pTarget,
	BYTE *					pSource,
	Uint8					bytes
) {
	Uint8					ret = bytes;

	while (bytes--)
	{
		*pTarget++ = *pSource++;
	}

	return ret;
}
