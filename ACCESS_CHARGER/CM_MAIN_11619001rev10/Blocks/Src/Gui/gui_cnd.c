/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Menu item visibility conditions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "real.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"

#include "Tui.h"
#include "lib.h"
#include "CAN.h"
#include "Sup.h"
#include "radio.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef Boolean (*			gui__navConditionFn)(gui__Inst *, gui_MenuItem const_P *);

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Checks menu item's visibility condition.
*
*	\param		pInst		ptr to GUI instance
*	\param		pItem		ptr to menu item info
*
*	\return		TRUE if menu item should be visible, FALSE if invisible.
*
*	\details		
*
*	\note						
*
******************************************************************************/

PUBLIC Boolean gui__cndCheck(
	gui__Inst *				pInst,
	gui_MenuItem const_P *	pItem
) {
	Uint8					condition;
	Uint8					idx;

	condition = (Uint8)gui_rowCondition(pItem);

	if (condition == pInst->nav.last_cnd)
	{
		return pInst->nav.last_cnd_res;
	}

	idx = gui_rowConditionIdx(pItem);

	if (idx >= pInst->pInit->pCond->count)
	{
		return TRUE;
	}

	pInst->nav.last_cnd = condition;
	pInst->nav.last_cnd_res = pInst->pInit->pCond->func[idx]();

	if (gui_rowConditionInv(pItem))
	{
		pInst->nav.last_cnd_res = !pInst->nav.last_cnd_res;
	}

	return pInst->nav.last_cnd_res;
}

