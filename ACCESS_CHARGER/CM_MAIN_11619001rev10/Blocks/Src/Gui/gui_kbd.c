/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Keyboard handling functions.
*	
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
//#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "Meas.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define GUI_REPEAT_START	300
#define GUI_REPEAT_LOOP		100


/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Key state to virtual key mapping info.
 */
typedef struct							/*''''''''''''''''''''''''''''' RAM	*/
{										/*									*/
	Uint16					keymask;	/**< Mask of keys that match the state*/
	gui__KeyState *			keyinfo;	/**< Virtual key info				*/
} gui__KeyCodes;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE gui__KeyState * gui__kbdGetKeyState(Uint16 keys);

PRIVATE void gui__kbdCoTask(osa_CoTaskType *);
PRIVATE void gui__kbdTmr(swtimer_Timer_st * pTimer);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 *	Key up states.
 */
PRIVATE gui__KeyState gui__keysUp[] =
{
	{
		GUI_VKEY_UP,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_UP|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key down states.
 */
PRIVATE gui__KeyState gui__keysDown[] =
{
	{
		GUI_VKEY_DOWN,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_DOWN|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key left states.
 */
PRIVATE gui__KeyState gui__keysLeft[] =
{
	{
		GUI_VKEY_LEFT,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_LEFT|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key right states.
 */
PRIVATE gui__KeyState gui__keysRight[] =
{
	{
		GUI_VKEY_RIGHT,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_RIGHT|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key ok states.
 */
PRIVATE gui__KeyState gui__keysOk[] =
{
	{
		GUI_VKEY_OK,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_OK|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key esc states.
 */
PRIVATE gui__KeyState gui__keysEsc[] =
{
	{
		GUI_VKEY_ESC,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_ESC|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key F1 states.
 */
PRIVATE gui__KeyState gui__keysF1[] =
{
	{
		GUI_VKEY_F1,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_F1|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key F2 states.
 */
PRIVATE gui__KeyState gui__keysF2[] =
{
	{
		GUI_VKEY_F2,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_F2|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/*
 *	Key STOP states.
 */
PRIVATE gui__KeyState gui__keysStop[] =
{
	{
		GUI_VKEY_STOP,
		GUI_REPEAT_START
	},
	{
		GUI_VKEY_STOP|GUI_VKEY_REPEAT|GUI_VKEY_LOOP,
		GUI_REPEAT_LOOP
	}
};

/**
 *	Key <-> state map.
 */
PRIVATE gui__KeyCodes gui__keycodes[] =
{
	{ MEAS_KEY_UP,			gui__keysUp			},
	{ MEAS_KEY_DOWN,		gui__keysDown		},
	{ MEAS_KEY_LEFT,		gui__keysLeft		},
	{ MEAS_KEY_RIGHT,		gui__keysRight		},
	{ MEAS_KEY_OK,			gui__keysOk			},
	{ MEAS_KEY_ESC,			gui__keysEsc		},
	{ MEAS_KEY_F1,			gui__keysF1			},
	{ MEAS_KEY_F2,			gui__keysF2			},
	{ MEAS_KEY_STOP,		gui__keysStop		},
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes the keyboard dat.
*
*
* \param		pInst		Ptr to gui instance.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__kbdInit(
	gui__Inst *				pInst
) {
	osa_newCoTask(
		&pInst->kbd.coTask,
		"gui__kbdCoTask",
		OSA_PRIORITY_NORMAL,
		gui__kbdCoTask,
		512
	);

	swtimer_new(&pInst->kbd.timer, gui__kbdTmr, 10, 0);
	pInst->kbd.timer.pUtil = pInst;


	pInst->kbd.keys = 0;
	pInst->kbd.state = 0;
	pInst->kbd.pState = NULL;

	pInst->kbd.fifo_read = 0;
	pInst->kbd.fifo_write = 0;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Handles system DOWN message.
*
* \param		pInst		ptr to gui instance.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__kbdDown(
	gui__Inst *				pInst
) {
	swtimer_stop(&pInst->kbd.timer);
	pInst->kbd.state &= ~GUI_KBD_ST_TO;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Puts virtual key info to key FIFO.
*
*
* \param		pInst		Ptr to gui instance.
* \param		key			Virtual key to insert into the FIFO.		
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__kbdFifoPut(
	gui__Inst *				pInst,
	gui__VKey				key
) {	
	DISABLE;

	pInst->kbd.fifo[pInst->kbd.fifo_write++] = key;
	pInst->kbd.fifo_write &= (GUI_KBD_FIFO - 1);

	if (pInst->kbd.fifo_write == pInst->kbd.fifo_read)
	{
		pInst->kbd.fifo_read++;
		pInst->kbd.fifo_read &= (GUI_KBD_FIFO - 1);
	}

	ENABLE;

	osa_signalSend(&pInst->task);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets virtual key from key FIFO.
*
*
* \param		pInst		Ptr to gui instance.
* \param		pKey		Key info will be put here.
*
* \return		TRUE if a key info was actually retrieved, FALSE if FIFO was
*				empty.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__kbdFifoGet(
	gui__Inst *				pInst,
	gui__VKey *				pKey
) {
	Boolean					ret = FALSE;

	DISABLE;

	if (pInst->kbd.fifo_read != pInst->kbd.fifo_write)
	{
		*pKey = pInst->kbd.fifo[pInst->kbd.fifo_read++];

		pInst->kbd.fifo_read &= (GUI_KBD_FIFO - 1);

		ret = TRUE;
	}

	ENABLE;

	return ret;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Flushes they key input FIFO.
*
*
* \param		pInst		Ptr to gui instance.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__kbdFifoFlush(
	gui__Inst *				pInst
) {
	DISABLE;

	pInst->kbd.fifo_read = 0;
	pInst->kbd.fifo_write = 0;

	ENABLE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Flushed keyboard states. If a key is pressed repeat and release
*				codes are not generated for it.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__kbdFlush(
	gui__Inst *				pInst
) {
	gui__kbdFifoFlush(pInst);
	pInst->kbd.pState = NULL;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Processes new input keys.
*
* \param		pInst		Ptr to gui instance.
* \param		keys		New input states.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__kbdProcessKeys(
	gui__Inst *				pInst,
	Uint16					keys
) {
	Uint16					change;

	change = keys ^ pInst->kbd.keys;

	if (change)
	{
		swtimer_stop(&pInst->kbd.timer);
		pInst->kbd.state &= ~GUI_KBD_ST_TO;

		if ((change & pInst->kbd.keys) && (pInst->kbd.pState != NULL))
		{
			/*
			 *	Release code.
			 */
			gui__kbdFifoPut(pInst, pInst->kbd.pState->vkey | GUI_VKEY_RELEASE);
		}

		pInst->kbd.pState = gui__kbdGetKeyState(keys);

		if ((pInst->kbd.pState != NULL) && (change & keys))
		{
			/*
			 *	Key press code.
			 */
			gui__kbdFifoPut(pInst, pInst->kbd.pState->vkey);
		}

		pInst->kbd.keys = keys;
	}
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Translates input key mask to virtual key state info.
*
*
* \param		keys		Input key mask.
*
* \return		Ptr to virtual key state info or NULL if key mask does not
*				match any virtual key.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE gui__KeyState * gui__kbdGetKeyState(
	Uint16					keys
) {
	gui__KeyState *			pState = NULL;
	Uint8					state;

	for (state = 0; state  < dim(gui__keycodes); state++)
	{
		if (gui__keycodes[state].keymask == keys)
		{
			pState = gui__keycodes[state].keyinfo;
			break;
		}
	}

	return pState;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Keyboard handling coTask. Processes new input keys and timed
*				operations.
*
* \param		coTask		Ptr to coTask structure.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__kbdCoTask(
	osa_CoTaskType *		coTask
) {
	gui__Inst *				pInst;	
	gui__Keyboard *			pKbd;

	pKbd = (gui__Keyboard *) coTask;
	pInst = tools_structPtr(gui__Inst, kbd, pKbd);

	if (pKbd->state & GUI_KBD_ST_KEYS)
	{
		msg_Ptr				messageHandle;

		/*
		 *	Input key states have changed.
		 */

		messageHandle = msg_receive(pInst->instid);

		while (messageHandle != NULL)
		{
			deb_assert(messageHandle != NULL);
			deb_assert(messageHandle->msgType == MSG_T_KEYBCHANGED);

			pKbd->state ^= GUI_KBD_ST_KEYS;
			gui__kbdProcessKeys(pInst, messageHandle->msgParam);
			msg_unlock(messageHandle);

			messageHandle = msg_receive(pInst->instid);
		}
	}
	else if (pKbd->state & GUI_KBD_ST_TO)
	{
		/*
		 *	Timeout of virtual key state occurred.	
		 */
		pKbd->state ^= GUI_KBD_ST_TO;

		if (pKbd->pState != NULL)
		{
			gui__kbdFifoPut(pInst, pKbd->pState->vkey);

			if (!(pKbd->pState->vkey & GUI_VKEY_LOOP))
			{
				pKbd->pState++;
			}
		}
	}

	/*
	 *	Current virtual key state exists and has a timed duration? 
	 *	=> start the timer.
	 */
	if (pKbd->pState != NULL)
	{
		if (pKbd->pState->duration>  0)
		{
			swtimer_stop(&pKbd->timer);
			swtimer_new(&pInst->kbd.timer, gui__kbdTmr, pKbd->pState->duration, 0);
			swtimer_start(&pKbd->timer);
		}
	}

}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Keyboard timer callback. Called when virtual key state timeout
*				occurs.
*
* \param		pTimer		Ptr to keyboard timer.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__kbdTmr(
	swtimer_Timer_st *		pTimer
) {
	gui__Inst *				pInst = (gui__Inst*)pTimer->pUtil;	

	pInst->kbd.state |= GUI_KBD_ST_TO;

	osa_coTaskRun(&pInst->kbd.coTask);
}
