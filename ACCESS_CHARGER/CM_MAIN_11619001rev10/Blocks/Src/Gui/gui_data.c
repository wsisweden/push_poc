/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Misc data.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "real.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"

#include "Chalg.h"
#include "Regu.h"
#include "sup.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/



SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns chalg error text.
*
*	\param		pInst		Ptr to GUI instance.
*	\param		idx			Error index.
*	\param		pCode		Error code will be put here if not NULL.
*
*	\return		Error text handle.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC sys_TxtHandle gui__dataChalgErrorText(
	gui__Inst *				pInst,
	Uint8					idx,
	Uint8 *					pCode
) {
	sys_TxtHandle			txt;
	Uint8					code = 0;

	switch ((Uint32)1 << idx)
	{
	case CHALG_ERR_LOW_BATTERY_VOL:
		txt = txt_localHandle(pInst->instid, txt_i_tLowBattery);			
		code = 1;
		break;

	case CHALG_ERR_HIGH_BATTERY_VOL:
		txt = txt_localHandle(pInst->instid, txt_i_tHighBattery);			
		code = 2;
		break;

	case CHALG_ERR_CHARGE_TIME_LIMIT:
		txt = txt_localHandle(pInst->instid, txt_i_tTimeLimitExceeded);			
		code = 3;
		break;

	case CHALG_ERR_CHARGE_AH_LIMIT:
		txt = txt_localHandle(pInst->instid, txt_i_tAhLimitExceeded);			
		code = 4;
		break;

	case CHALG_ERR_INCORRECT_ALGORITHM:
		txt = txt_localHandle(pInst->instid, txt_i_tIncorrentParams);			
		code = 5;
		break;

	case CHALG_ERR_INCORRECT_MOUNT:
		txt = txt_localHandle(pInst->instid, txt_i_tNotVertical);			
		code = 6;
		break;

	case CHALG_ERR_HIGH_BATTERY_LIMIT:
		txt = txt_localHandle(pInst->instid, txt_i_tHighBatteryLimit);
		code = 7;
		break;

	case CHALG_ERR_BATTERY_ERROR:
		txt = txt_localHandle(pInst->instid, txt_i_tBatteryError);
		code = 8;
		break;

	case CHALG_ERR_BM_TEMP:
		txt = txt_localHandle(pInst->instid, txt_i_tBmTempError);
		code = 101;
		break;

	case CHALG_ERR_BM_ACID:
		txt = txt_localHandle(pInst->instid, txt_i_tBmAcidError);
		code = 102;
		break;

	case CHALG_ERR_BM_BALANCE:
		txt = txt_localHandle(pInst->instid, txt_i_tBmBalanceError);
		code = 103;
		break;

	case CHALG_ERR_LOW_AIRPUMP_PRESSURE:
		txt = txt_localHandle(pInst->instid, txt_i_tLowAirPump);
		code = 9;
		break;

	case CHALG_ERR_HIGH_AIRPUMP_PRESSURE:
		txt = txt_localHandle(pInst->instid, txt_i_tHighAirPump);
		code = 10;
		break;

	case CHALG_ERR_BM_LOW_SOC:
		txt = txt_localHandle(pInst->instid, txt_i_tBmLowSocError);
		code = 105;
		break;

	default:
		txt = GUI_NO_TEXT;
		break;
	}		

	if (pCode)
	{
		*pCode = code;
	}

	return txt;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns regu error text.
*
*	\param		pInst		Ptr to GUI instance.
*	\param		idx			Error index.
*	\param		pCode		Error code will be put here if not NULL.
*
*	\return		Error text handle.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PUBLIC sys_TxtHandle gui__dataReguErrorText(
	gui__Inst *				pInst,
	Uint8					idx,
	Uint8 *					pCode
) {
	sys_TxtHandle			txt;
	Uint8					code;

	switch ((Uint32)1 << idx)
	{
	case (1 << 0):											// Phase error
		txt = txt_localHandle(pInst->instid, txt_i_tPhaseError);			
		code = 17;
		break;

	case (1 << 1) :										// Regulator error
		txt = txt_localHandle(pInst->instid, txt_i_tReguError);			
		code = 18;
		break;

	case (1 << 2) :										// Low charger temperature
		txt = txt_localHandle(pInst->instid, txt_i_tLowTemp);			
		code = 19;
		break;

	case (1 << 3) :										// High charger temperature
	{
		Uint8 SupState;

		reg_get(&SupState, gui_supervisionState);
		if(SupState & SUP_STATE_CHARGING)
		{
			txt = txt_localHandle(pInst->instid, txt_i_tHighTemp);
		}
		else
		{
			txt = txt_localHandle(pInst->instid, txt_i_tHighTempNoBatt);
		}
		code = 20;
		break;
	}
	case (1 << 4) :										// Low board temperature
		txt = txt_localHandle(pInst->instid, txt_i_tBoardLowTemp);
		code = 21;
		break;
	case (1 << 5) :										// High board temperature
		txt = txt_localHandle(pInst->instid, txt_i_tBoardHighTemp);
		code = 22;
		break;
	case (1 << 6) :										// Power unit error
		txt = txt_localHandle(pInst->instid, txt_i_tPowerUnitError);
		code = 23;
		break;
	case (1 << 7) :										// Watch dog
		txt = txt_localHandle(pInst->instid, txt_i_tWatchdog);
		code = 24;
		break;

// #JJ slave alarm
	case (1 << 8):											// Phase error
		txt = txt_localHandle(pInst->instid, txt_i_tPhaseErrorW);
		code = 25;
		break;

	case (1 << 9) :										// Regulator error
		txt = txt_localHandle(pInst->instid, txt_i_tReguErrorW);
		code = 26;
		break;

	case (1 << 10) :										// Low charger temperature
		txt = txt_localHandle(pInst->instid, txt_i_tLowTempW);
		code = 27;
		break;

	case (1 << 11) :										// High charger temperature
	{
		Uint8 SupState;

		reg_get(&SupState, gui_supervisionState);
		if(SupState & SUP_STATE_CHARGING)
		{
			txt = txt_localHandle(pInst->instid, txt_i_tHighTempW);
		}
		else
		{
			txt = txt_localHandle(pInst->instid, txt_i_tHighTempNoBattW);
		}
		code = 28;
		break;
	}
	case (1 << 12) :										// Low board temperature
		txt = txt_localHandle(pInst->instid, txt_i_tBoardLowTempW);
		code = 29;
		break;
	case (1 << 13) :										// High board temperature
		txt = txt_localHandle(pInst->instid, txt_i_tBoardHighTempW);
		code = 30;
		break;
	case (1 << 14) :										// Power unit error
		txt = txt_localHandle(pInst->instid, txt_i_tPowerUnitErrorW);
		code = 31;
		break;
	case (1 << 15) :										// Watch dog
		txt = txt_localHandle(pInst->instid, txt_i_tWatchdogW);
		code = 32;
		break;


	default:
		txt = GUI_NO_TEXT;
		code = 0;
		break;
	}

	if (pCode)
	{
		*pCode = code;
	}

	return txt;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns sup error text.
*
*	\param		pInst		Ptr to GUI instance.
*	\param		idx			Error index.
*
*	\return		Error text handle.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC sys_TxtHandle gui__dataSupErrorText(
	gui__Inst *				pInst,
	Uint8					idx
) {
	sys_TxtHandle			txt;

	switch ((Uint32)1 << idx)
	{
	case SUP_STATUS_RADIO_ERROR:
		txt = txt_localHandle(pInst->instid, txt_i_tRadioError);
		break;

	case SUP_STATUS_CORRUPTED:
		txt = txt_localHandle(pInst->instid, txt_i_tFwError);			
		break;

	case SUP_STATUS_BM_FAILED:
		txt = txt_localHandle(pInst->instid, txt_i_tBmInitFail);			
		break;

	case SUP_STATUS_BM_ALG_ERR:
		txt = txt_localHandle(pInst->instid, txt_i_tIncorrentParamsBm);
		break;

	case SUP_STATUS_ADDR_CONFLICT:
		txt = txt_localHandle(pInst->instid, txt_i_tAddressConflict);
		break;

	case SUP_STATUS_BM_ADDR_CONFLICT:
		txt = txt_localHandle(pInst->instid, txt_i_tBmAddressConflict);
		break;

	case SUP_STATUS_DPL_NO_MASTER:
		txt = txt_localHandle(pInst->instid, txt_i_tDplNoMaster);
		break;

	case SUP_STATUS_DPL_MASTER_CONFLICT:
		txt = txt_localHandle(pInst->instid, txt_i_tDplMasterConflict);
		break;

	default:
		txt = GUI_NO_TEXT;
		break;
	}

	return txt;
}
