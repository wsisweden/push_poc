/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Menu view handling functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "lib.h"
#include "txt.h"
#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef void (*				gui__viewMenuPaintFn)(gui__Inst *, gui__MenuView *, gui_MenuItem const_P *, Uint8);


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gui__viewMenuPaint(gui__Inst * pInst, gui__MenuView * pView);

PRIVATE void gui__viewMenuUpdateRegs(gui__Inst * pInst, gui__MenuView * pView);

PRIVATE void gui__viewMenuDrawHeader(gui__Inst * pInst);
PRIVATE void gui__viewMenuDrawText(gui__Inst * pInst, gui__MenuView * pView, gui_MenuItem const_P * pItem, Uint8 index);
PRIVATE void gui__viewMenuDrawParam(gui__Inst * pInst, gui__MenuView * pView, gui_MenuItem const_P * pItem, Uint8 index);
PRIVATE void gui__viewMenuDrawSub(gui__Inst * pInst, gui__MenuView * pView, gui_MenuItem const_P * pItem, Uint8 index);

PRIVATE void gui__viewMenuStartEdit(gui__Inst * pInst, gui__MenuView * pView, Uint8 index);
PRIVATE void gui__viewMenuEndEdit(gui__Inst * pInst, gui__MenuView * pView, Boolean cancel);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 *	Order is the same as with GUI_ROW_SUBMENU, GUI_ROW_TEXT and GUI_ROW_PARAM.
 */
PRIVATE gui__viewMenuPaintFn gui__viewMenuPaintHandlers[] =
{
	gui__viewMenuDrawSub,
	gui__viewMenuDrawText,
	gui__viewMenuDrawParam
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Menu view message proc.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to process view
* \param		msg			view message id
* \param		pArgs		ptr to message args
*
* \return		Int8
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewMenuProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	Int8					ret = 0;

	/*
	 *	Main message proc
	 */
	switch (msg)
	{
	/*
	 *	Create message.
	 */
	case gui_msg_create:
		break;

	/*
	 *	Initialization message. Update visible register data.
	 */
	case gui_msg_init:
		gui__viewMenuUpdateRegs(pInst, (gui__MenuView *)pView);
		gui__viewTmrStart(pView, 10, 10);
		break;

	/*
	 *	View closing.
	 */
	case gui_msg_close:
		gui__viewTmrStop(pView);

		if (pView->state & GUI_MV_ST_EDITING)
		{
			/*
			 *	Cancel editing.
			 */
			gui__viewMenuEndEdit(pInst, (gui__MenuView *)pView, TRUE);
		}
		break;

	/*
	 *	View timer timed out.
	 */
	case gui_msg_rebuild:
	case gui_msg_timeout:
		if ((pView->state & GUI_MV_ST_EDITING) == 0)
		{
			gui__navRebuild(pInst);
			gui__viewMenuUpdateRegs(pInst, (gui__MenuView *)pView);
		}
		gui__viewInvalidate(pInst->view.pCurrent);
		break;

	/*
	 *	Paint message.
	 */
	case gui_msg_paint:
		{
			gui__viewMenuPaint(pInst, (gui__MenuView *)pView);
			gui__viewSetPainted(pView);
		}
		break;

	/*
	 *	Key up event
	 */
	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_OK:
				if (pView->state & GUI_MV_ST_EDITING)
				{
					/*
					 *	Edit edit and save changes. Rebuild the menu as well to 
					 *	speed up conditional item visibility updates.
					 */
					gui__viewMenuEndEdit(pInst, (gui__MenuView *)pView, FALSE);
					gui__navRebuild(pInst);
					gui__viewMenuUpdateRegs(pInst, (gui__MenuView *)pView);
					gui__viewInvalidate(pView);
				}
				else if (pInst->nav.item == GUI_NAV_INVALID)
				{
					/* Do nothing if no valid menu item */
				}
				else if (gui_isParamRow(pInst->nav.items[pInst->nav.item]))
				{
					/*
					 *	Ok on parameter row => 
					 *	Start editing if access rights allow it.
					 */
					gui__viewMenuStartEdit(
						pInst, 
						(gui__MenuView *)pView, 
						pInst->nav.item
					);
				}
				else if (gui_isMenuRow(pInst->nav.items[pInst->nav.item]))
				{
					gui_MenuItem const_P *	pItem;
					Boolean nav = TRUE;
					/*
					 *	Ok on sub menu row => navigate to sub menu.
					 */
					pItem = pInst->nav.items[pInst->nav.item];

					if (
						(gui__tblType(pItem->subitem) != GUI_TBL_TYPE_TIME) &&
						gui_tblEnter(pItem->subitem)
					) {
						sys_TxtHandle ret_text;
						/*
						 *	Table has exit conditions associated with it.
						 */

						deb_log("GUI: calling validation fn");

						ret_text = pInst->pInit->pEnterFn[
							gui_tblEnter(pItem->subitem) - 1
						](TRUE);

						/* Handle DPL log in */
						if (ret_text == txt_localHandle(pInst->instid, txt_i_tDPL)){
							Uint8 arg = GUI__QRY_DPL_ACTIVATE;
							gui__viewOpen(pInst, GUI_VIEW_QUERY, &arg);
							nav = FALSE;
						}
						else if (ret_text != GUI_NO_TEXT)
						{
							gui__PopupViewArgs	args;

							nav = FALSE;

							/*
							 *	Show error message.
							 */
							args.delay = 30;
							args.text1 = ret_text;
							args.text2 = GUI_NO_TEXT;
							args.on_exit = NULL;

							gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
						}

					}
					if (nav)
					{
						if(gui__navigate(pInst, gui_nav_in) == GUI_NAV_OK){
							gui__viewInvalidate(pView);
						}
					}
				}

				break;

			case GUI_VKEY_ESC:
				{
					if (pView->state & GUI_MV_ST_EDITING)
					{
						/*
						 *	Cancel edit.
						 */
						gui__viewMenuEndEdit(pInst, (gui__MenuView *)pView, TRUE);
					}
					else
					{
						Boolean nav = TRUE;

						if (
							(gui__tblType(pInst->nav.pTable) != GUI_TBL_TYPE_TIME) &&
							gui_tblExit(pInst->nav.pTable)
						) {
							sys_TxtHandle ret_text;
							/*
							 *	Table has exit conditions associated with it.
							 */

							deb_log("GUI: calling validation fn");

							ret_text = pInst->pInit->pExitFn[
								gui_tblExit(pInst->nav.pTable) - 1
							](TRUE);

							if (ret_text != GUI_NO_TEXT)
							{
								gui__PopupViewArgs	args;

								if (ret_text == txt_localHandle(pInst->instid, txt_i_tNewParamsBm))
								{
									gui__navigate(pInst, gui_nav_out);
									nav = FALSE;
								}
								else{
									if (ret_text == txt_localHandle(pInst->instid, txt_i_tNewParamsOk))
									{
										gui__navigate(pInst, gui_nav_out);
									}

									nav = FALSE;

									/*
									 *	Show error message.
									 */
									args.delay = 30;
									args.text1 = ret_text;
									args.text2 = GUI_NO_TEXT;
									args.on_exit = NULL;

									gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
								}
							}

						}
						if (nav)
						{
							if(gui__navigate(pInst, gui_nav_out) == GUI_NAV_OK){
								gui__viewInvalidate(pView);
							}
						}
					}
				}
				break;

			default:
				if (pView->state & GUI_MV_ST_EDITING)
				{
					gui__editNavigate(pInst, gui_nav_root);

					/*	debug for Log data view
					 *	Rebuild the menu to speed up conditional item visibility updates.
					 */
					gui__navRebuild(pInst);

				}
				break;
			}
		}
		break;

	/*
	 *	Key down event
	 */
	case gui_msg_key_down:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			/*
			 *	If editing:
			 *		Try to navigate up in the edit field.
			 *	else:
			 *		Navigate up inside the page. If up direction is not availabe
			 *		try to navigate left to previous page.
			 */
			case GUI_VKEY_UP:
				if (pView->state & GUI_MV_ST_EDITING)
				{
					if (gui__editNavigate(pInst, gui_nav_up))
					{
						gui__viewInvalidate(pView);
					}
				}
				else if (gui__navigate(pInst, gui_nav_up) == GUI_NAV_OK)
				{
					gui__viewInvalidate(pView);
				}
				else if (gui__navigate(pInst, gui_nav_left) == GUI_NAV_OK)
				{
					/*
					 *	When navigating left through up key new position
					 *	should be at the end of the page.
					 */
					while (gui__navigate(pInst, gui_nav_down) == GUI_NAV_OK);
					gui__viewMenuUpdateRegs(pInst, (gui__MenuView *)pView);
					gui__viewInvalidate(pView);
				}
				break;

			/*
			 *	If editing:
			 *		Try to navigate down in the edit field.
			 *	else:
			 *		Navigate down inside the page
			 */
			case GUI_VKEY_DOWN:
				if (pView->state & GUI_MV_ST_EDITING)
				{
					if (gui__editNavigate(pInst, gui_nav_down))
					{
						gui__viewInvalidate(pView);
					}
				}
				else if (gui__navigate(pInst, gui_nav_down) == GUI_NAV_OK)
				{
					gui__viewInvalidate(pView);
				}
				else if (gui__navigate(pInst, gui_nav_right) == GUI_NAV_OK)
				{
					gui__viewMenuUpdateRegs(pInst, (gui__MenuView *)pView);
					gui__viewInvalidate(pView);
				}
				break;

			/*
			 *	If editing:
			 *		Try to navigate left in the edit field.
			 *	else:
			 *		Navigate to previous page
			 */
			case GUI_VKEY_LEFT:
				if (pView->state & GUI_MV_ST_EDITING)
				{
					if (gui__editNavigate(pInst, gui_nav_left))
					{
						gui__viewInvalidate(pView);
					}
				}
				else if (gui__navigate(pInst, gui_nav_left) == GUI_NAV_OK)
				{
					gui__viewMenuUpdateRegs(pInst, (gui__MenuView *)pView);
					gui__viewInvalidate(pView);
				}
				break;

			/*
			 *	If editing:
			 *		Try to navigate right in the edit field.
			 *	else:
			 *		Navigate to next page
			 */
			case GUI_VKEY_RIGHT:
				if (pView->state & GUI_MV_ST_EDITING)
				{
					if (gui__editNavigate(pInst, gui_nav_right))
					{
						gui__viewInvalidate(pView);
					}
				}
				else if (gui__navigate(pInst, gui_nav_right) == GUI_NAV_OK)
				{
					gui__viewMenuUpdateRegs(pInst, (gui__MenuView *)pView);
					gui__viewInvalidate(pView);
				}
				break;

			default:
				break;
			}
		}
		break;

	default:
		break;
	}

	return ret;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Starts editing currently selected parameter.
*
* \param		pInst	ptr to gui instance
* \param		pView	ptr to menu view
* \param		index	item index
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewMenuStartEdit(
	gui__Inst *				pInst,
	gui__MenuView *			pView,
	Uint8					index
) {
	gui_MenuItem const_P *	pItem;
	gui_RegInfo *			pInfo;

	pItem = pInst->nav.items[index]; 

	pInfo = &pView->regs[index];

	if (pItem->flags & GUI_PRM_DBL_REG)
	{
		/*
		 *	If double register is in use write access is not checked, it is
		 *	assumed to be always on in the write register.
		 */
	}
	else if (!(((sys_RegAdditAttr const_P *)pInfo->pInfo)->status & SYS_MMI_WRITE))
	{
		/*
		 *	Write to register through MMI not allowed.
		 */
		return;
	}

	/*
	 *	Check access rights (after WRITE flag check so we don't get access 
	 *	denied if editing is not possible).
	 */
	if (!gui__navCheckAccess(pInst, TRUE))
	{
		return;
	}

	if (pItem->flags & (GUI_FACT_DEFAULT|GUI_FACT_SET|GUI_CLEAR_STAT))
	{
		Uint8				u8;

		/*
		 *	Editing this item should cause either 'reset to factory defaults'
		 *	or 'set factory defaults' or 'clear all statistics' query.
		 */

		if (pItem->flags & GUI_FACT_DEFAULT)
		{
			u8 = GUI__QRY_FACT_DEF;
		}
		else if (pItem->flags & GUI_FACT_SET)
		{
			u8 = GUI__QRY_FACT_SET;
		}
		else
		{
			deb_assert(pItem->flags & GUI_CLEAR_STAT);
			u8 = GUI__QRY_CLR_STAT;
		}

		/*
		 *	Factory defaults view.
		 */
		gui__viewOpen(pInst, GUI_VIEW_QUERY, &u8);
	}
	else if (pItem->flags & GUI_PRM_SINGLE)
	{
		/*
		 *	Push button style parameter => no editing, just write the 
		 *	configured value to the register.
		 */
		pInst->buffer.reg.u8 = pItem->enumval;

		if (pItem->flags & GUI_PRM_DBL_REG)
		{
			gui__regPut(pInst, pItem->reg2, 0);
		}
		else
		{
			gui__regPut(pInst, pItem->reg, 0);
		}
	}
	else if (gui__editInit(pInst, pItem))
	{
		pView->base.state |= GUI_MV_ST_EDITING;
	}

	gui__viewInvalidate(pView);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Ends edit, stores edited value to reg if editing was not 
*				cancelled.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to menu view
* \param		cancel		if TRUE editing is cancelled and edited value
*							is discarded
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewMenuEndEdit(
	gui__Inst *				pInst,
	gui__MenuView *			pView,
	Boolean					cancel
) {
	pView->base.state &= ~GUI_MV_ST_EDITING;

	if (!cancel)
	{
		if (!gui__editEnd(pInst))
		{
			gui__PopupViewArgs	args;

			/*
			 *	Show error message.
			 */
			args.delay = 30;
			args.text1 = txt_localHandle(pInst->instid, txt_i_tInvalidValue);
			args.text2 = GUI_NO_TEXT;
			args.on_exit = NULL;

			gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
		}
	}
	else if (pInst->edit.pItem->flags & GUI_PRM_IMMEDIATE)
	{
		/*
		 *	Immediate editing is cancelled => store original value.
		 */
		gui__regPutBuffer(pInst, pInst->edit.pItem->reg, 0, &pInst->edit.original);
	}

	gui__viewInvalidate(pView);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws header part of menu view.
*
* \param		pInst		ptr to gui instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewMenuDrawHeader(
	gui__Inst *				pInst
) {
	gui__fontSelect(pInst, GUI_FNT_8x8);
	gui__dispSetPos(pInst, 0, 0);

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	/*
	 *	Draw table header
	 */
	gui__textWriteAligned(
		pInst,
		txt_text(pInst->nav.pTable->header),
		0, 
		pInst->disp.chr_per_line,
		GUI_ALIGN_LEFT
	);

	/*
	 *	Draw page count?
	 */
	if (pInst->nav.max_page > 0)
	{
		BYTE *				pPos = pInst->buffer.buf2;
		Uint8				len;

		pPos += gui__u32toa(pInst->nav.page + 1, pPos, 3);
		*pPos++ = '/';
		pPos += gui__u32toa(pInst->nav.max_page + 1, pPos, 3);

		len = pPos - pInst->buffer.buf2;

		gui__textPutBytes(
			pInst, 
			pInst->buffer.buf2, 
			pInst->disp.chr_per_line - len,
			len
		);
	}

	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws text item row in menu view.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to menu view
* \param		pItem		ptr to text item info	
* \param		index		row index
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewMenuDrawText(
	gui__Inst *				pInst,
	gui__MenuView *			pView,
	gui_MenuItem const_P *	pItem,
	Uint8					index
) {
	display_Rect			rect;
	Uint8					len;
	Uint8					flags;

	if (pItem->text == GUI_NO_TEXT)
	{
		return;
	}

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	if (pItem->flags & GUI_TEXT_BOLD)
	{
		gui__fontSelect(pInst, GUI_FNT_8x8);
	}
	else
	{
		gui__fontSelect(pInst, GUI_FNT_6x8);
	}

	rect.x = 0;
	rect.y = index * 8;
	rect.width = (
		pInst->pInit->width / pInst->disp.pFont->width
	) * pInst->disp.pFont->width;
	rect.height = 8;

	if (pItem->text2 != GUI_NO_TEXT)
	{
		len = gui__textWrite(pInst, pItem->text);

		gui__dispDrawTextAligned(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			GUI_ALIGN_LEFT
		);

		len = gui__textWrite(pInst, pItem->text2);

		gui__dispDrawTextAligned(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			GUI_ALIGN_RIGHT
		);
	}
	else
	{
		if (pItem->flags & GUI_TEXT_AL_RIGHT)
		{
			flags = GUI_ALIGN_RIGHT;
		}
		else if (pItem->flags & GUI_TEXT_AL_CENTER)
		{
			flags = GUI_ALIGN_CENTER;
		}
		else
		{
			flags = GUI_ALIGN_LEFT;
		}

		deb_assert(pInst->disp.pFont->height == 8);

		len = gui__textWrite(pInst, pItem->text);

		gui__dispDrawTextAligned(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			flags
		);
	}
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws parameter row in menu view.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to menu view
* \param		pItem		ptr to parameter row info	
* \param		index		row index
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewMenuDrawParam(
	gui__Inst *				pInst,
	gui__MenuView *			pView,
	gui_MenuItem const_P *	pItem,
	Uint8					index
) {
	Uint8					len;

	gui__fontSelect(pInst, GUI_FNT_6x8);
	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	if (pItem->flags & GUI_REG_VALUE)
	{
		/*
		 *	Draw just register value, centered.
		 */
		display_Rect		rect;
		sys_ArrIndex		array_idx;

		rect.x = 0;
		rect.y = index * pInst->disp.pFont->height;
		rect.width = pInst->pInit->width;
		rect.height = pInst->disp.pFont->height;
	
		if (pItem->flags & GUI_PRM_ARR_IDX)
		{
			array_idx = pItem->enumval;
		}
		else
		{
			array_idx = 0;
		}

		gui__regGet(pInst, pItem->reg, array_idx, NULL);

		len = gui__frmtForDisplay(pInst, pItem, &pView->regs[index]);

		gui__dispDrawTextAligned(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			GUI_ALIGN_CENTER
		);

		return;
	}

	/*
	 *	Draw parameter text
	 */
	gui__dispSetPos(pInst, 0, 8 * index);

	if ((index == pInst->nav.item) && !(pView->base.state & GUI_MV_ST_EDITING))
	{
		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
	else
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}

	if (pItem->flags & GUI_PRM_ENUM_TXT)
	{
		/*
		 *	Enum text should be used instead of register text.
		 */
		BYTE const_P *		pTxt;

		deb_assert(pItem->flags & GUI_PRM_SINGLE);

		pTxt = gui__regToEnumText(pItem->reg, pItem->enumval, &len, GUI_NO_TEXT);

		gui__textWriteChars(pInst, ' ', 0, pInst->disp.chr_per_line);
		gui__textWriteConst(pInst, pTxt, 0, len);
	}
	else
	{
		sys_TxtHandle		txt = GUI_NO_TEXT;

		if (pItem->flags & (GUI_PRM_BITMASK|GUI_PRM_TEXT))
		{
			/*
			 *	Take text from bitmask enum or additional text
			 *	handle.
			 */
			txt = pItem->text;
		}
		else
		{
			if (pView->regs[index].pInfo)
			{
				txt = gui__regGetText(&pView->regs[index]);
			}
		}

		if (txt != GUI_NO_TEXT)
		{
			gui__textWriteAligned(
				pInst,
				txt_text(txt), 
				0, 
				pInst->disp.chr_per_line,
				GUI_ALIGN_LEFT
			);
		}
	}

	/*
	 *	Button style parameter?
	 */
	if (pItem->flags & GUI_PRM_SINGLE)
	{
		if (pItem->flags & GUI_PRM_CHKBOX)
		{
			if (pItem->flags & GUI_PRM_ARR_IDX)
			{
				gui__regGet(pInst, pItem->reg, pItem->enumval, NULL);
			}
			else
			{
				gui__regGet(pInst, pItem->reg, 0, NULL);
			}

			gui__textSelectBuffer(pInst, pInst->buffer.buf2);

			if (pInst->buffer.reg.u8 == pItem->enumval)
			{
				gui__textWriteConst(pInst, (BYTE const_P *) "[X]", 0, 3);
			}
			else
			{
				gui__textWriteConst(pInst, (BYTE const_P *) "[ ]", 0, 3);
			}

			gui__textSelectBuffer(pInst, pInst->buffer.buf1);
			gui__textPutBytes(pInst, pInst->buffer.buf2, pInst->disp.chr_per_line - 3, 3);
			gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);
		}
		else
		{
			gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);
		}

		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);

		return;
	}

	/*
	 *	Draw parameter value
	 */
	if ((index == pInst->nav.item) && (pView->base.state & GUI_MV_ST_EDITING))
	{
		Uint8				buf_pos = 0;
		Uint8				out_pos = 0;

		/*
		 *	Value is being edited.
		 */

		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
		gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

		gui__textSelectBuffer(pInst, pInst->buffer.buf2);

		len = 0;
		buf_pos = 0;

		if (pItem->flags & GUI_PRM_MASKED)
		{
			/*
			 *	Masked (password) type value, show only the character being
			 *	edited.
			 */
			while (len < pInst->edit.edit_pos)
			{
				pInst->text.pCurrent[buf_pos++] = '*';
				len++;
			}			

			while (len < (pInst->edit.edit_pos + pInst->edit.edit_width))
			{
				pInst->text.pCurrent[buf_pos++] = pInst->edit.buffer.str[len++];
			}

			while (len < pInst->edit.frmt_len)
			{
				pInst->text.pCurrent[buf_pos++] = '*';
				len++;
			}
		}
		else
		{
			/*
			 *	Normal editing, show all characters.
			 */
			while (len < pInst->edit.frmt_len)
			{
				pInst->text.pCurrent[buf_pos++] = pInst->edit.buffer.str[len++];
			}
		}

		out_pos = pInst->disp.chr_per_line - pInst->edit.frmt_len;

		gui__dispSetPos(pInst, out_pos * pInst->disp.pFont->width, 8 * index);

		if (pInst->edit.flags & GUI_ED_SINGLE_CHR)
		{
			/*
			 *	Editing single character at the time. Show current character
			 *	inverted, others normal.
			 */
			gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->edit.edit_pos);

			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
			gui__dispDrawText(
				pInst, 
				&pInst->text.pCurrent[pInst->edit.edit_pos], 
				pInst->edit.edit_width
			);
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);

			len = pInst->edit.frmt_len - pInst->edit.edit_pos - pInst->edit.edit_width;

			if (len)
			{
				gui__dispDrawText(
					pInst, 
					&pInst->text.pCurrent[pInst->edit.edit_pos + pInst->edit.edit_width], 
					len
				);
			}
		}
		else
		{
			/*
			 *	Whole value is being edited, show the whole value inverted.
			 */
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
			gui__dispDrawText(
				pInst, 
				pInst->text.pCurrent, 
				pInst->edit.edit_width
			);
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);			
		}

	}
	else
	{
		/*
		 *	No editing => just draw the value right aligned.
		 */

		gui__textSelectBuffer(pInst, pInst->buffer.buf2);

		if (pItem->flags & GUI_PRM_MASKED)
		{
			/*
			 *	Masked value being shown => show asterisks (always 4)
			 */
			len = 4;
			pInst->text.pCurrent[0] = '*';
			pInst->text.pCurrent[1] = '*';
			pInst->text.pCurrent[2] = '*';
			pInst->text.pCurrent[3] = '*';
		}
		else
		{
			/*
			 *	Non-masked => show actual value.
			 */
			if (pItem->flags & GUI_PRM_ARR_IDX)
			{
				gui__regGet(pInst, pItem->reg, pItem->enumval, NULL);
			}
			else
			{
				gui__regGet(pInst, pItem->reg, 0, NULL);
			}
			len = gui__frmtForDisplay(pInst, pItem, &pView->regs[index]);
		}


		gui__textSelectBuffer(pInst, pInst->buffer.buf1);
		gui__textPutBytes(
			pInst, 
			pInst->buffer.buf2, 
			pInst->disp.chr_per_line - len, 
			len
		);
		gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Draws sub menu link row in menu view.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to menu view
* \param		pItem		ptr to sub item info	
* \param		index		row index
*
* \return		PRIVATE void
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewMenuDrawSub(
	gui__Inst *				pInst,
	gui__MenuView *			pView,
	gui_MenuItem const_P *	pItem,
	Uint8					index
) {
	/*
	 *	Draw parameter text
	 */
	gui__fontSelect(pInst, GUI_FNT_6x8);
	gui__dispSetPos(pInst, 0, 8 * index);

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	if (index == pInst->nav.item)
	{
		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
	else
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}

	gui__textWriteAligned(
		pInst,
		txt_text(pItem->subitem->header),
		0, 
		pInst->disp.chr_per_line,
		GUI_ALIGN_LEFT
	);

	if (pItem->flags & GUI_MENU_LANG)
	{
		BYTE const_P *		pTxt;
		Uint8				len;

		/*
		 *	Sub menu link is configured to show language abbreviation on the
		 *	right side of the row.
		 */

		pTxt = gui__regToEnumText(
			0, 
			pInst->language, 
			&len, 			
			txt_localHandle(pInst->instid, txt_i_tLanguageShort)
		);

		gui__textWriteConst(pInst, pTxt, pInst->disp.chr_per_line - len, len);
	}
	else if (pItem->flags & GUI_MENU_REG)
	{
		Uint8				len;

		/*
		 *	Sub menu link is configured to show a register value on the right
		 *	side of the row.
		 */
		gui__textSelectBuffer(pInst, pInst->buffer.buf2);

		gui__regGet(pInst, pItem->reg, 0, NULL);

		len = gui__frmtValue(pInst, pItem->reg);

		gui__textSelectBuffer(pInst, pInst->buffer.buf1);

		gui__textPutBytes(
			pInst, 
			pInst->buffer.buf2, 
			pInst->disp.chr_per_line - len, 
			len
		);
	}

	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

	gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Updates register info of visible registers.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to menu view
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__viewMenuUpdateRegs(
	gui__Inst *				pInst,
	gui__MenuView *			pView
) {
	Uint8					pos;

	for (pos = 0; pos < GUI_ITEMS_PER_PAGE; pos++)
	{
		if (pInst->nav.items[pos] == NULL)
		{
			pView->regs[pos].pInfo = NULL;
			continue;
		}
			
		gui__regGetInfo(pInst->nav.items[pos]->reg, &pView->regs[pos]);
	}
}




/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles paint message on the menu view.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		ptr to view instance
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewMenuPaint(
	gui__Inst *				pInst,
	gui__MenuView *			pView
) {
	Uint8			pos = 0;

	gui__dispClear(pInst);

	/*
	 *	Draw table header 
	 */
	if (!(pInst->nav.pTable->type & GUI_TBL_NO_HDR))
	{
		gui__viewMenuDrawHeader(pInst);
		pos++;
	}

	/*
	 *	Draw items.
	 */
	for (; pos < GUI_ITEMS_PER_PAGE; pos++)
	{
		if (pInst->nav.items[pos] != NULL)
		{
			gui__viewMenuPaintHandlers[gui_rowTypeToIdx(pInst->nav.items[pos])](
				pInst,
				(gui__MenuView *)pView,
				pInst->nav.items[pos],
				pos
			);
		}
	}
}
