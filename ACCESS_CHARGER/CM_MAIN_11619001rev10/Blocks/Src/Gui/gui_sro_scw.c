/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger remote output Water settings view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

#include "gui_callbackMenu.h"
#include "../IObus/IObusOutputs.h"
#include "IObus.h"
#include "../Chalg/inc_cm3/caiNames.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define FIRST_ITEM	1
#define PAGE_ITEMS	8

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/
static void DrawItems(gui__Inst * pInst, const gui__CallbackView * pView);
static void DrawRow(gui__Inst* pInst, int item, int edited, int key);
static void drawEditedField(gui__Inst* pInst, gui__VKey key, int decimals, int digits);
static int countDigits(uint32_t value);

/* Call back functions */
static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {3,'s','c','w'};
static int posX = 0;

static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	PAGE_ITEMS - FIRST_ITEM,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_F_NONE,
	EDITABLE_ITEMS,
};

/* Water item */
static const sys_TxtHandle pageTexts[] =
{
		txt_i_tWaterVar,
};

static struct{
	int32_t Min;
	int32_t Value;
	int32_t Max;
	IoBusOutRegister_type reg;
	int registerIndex;
} Edited = {0, 0, 0, {0}, 0};



SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



PUBLIC Int8 gui__viewSroScwProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	callbackParameters.items = dim(pageTexts);
	return gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*
*	\return		-
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void drawEditedField(
	gui__Inst *				pInst,
	gui__VKey key,
	int decimals,
	int digits
) {

	{
		int step = 1;
		{
			int n;
			for(n = 0; n < posX; n++){
				step *= 10;
			}
		}

		switch(key){
		case GUI_VKEY_UP :
			Edited.Value += step;
			if(Edited.Value > Edited.Max){
				Edited.Value = Edited.Max;
			}
			break;
		case GUI_VKEY_DOWN :
			Edited.Value -= step;
			if(Edited.Value < Edited.Min){
				Edited.Value = Edited.Min;
			}
			break;
		case GUI_VKEY_LEFT :
			if(posX < digits - 1){
				posX++;
			}
			break;
		case GUI_VKEY_RIGHT :
			if(posX){
				posX--;
			}
			break;
		}
	}

	dispPrintf(pInst, Edited.Value, decimals, posX, digits);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawRow(
		gui__Inst *				pInst,
		int item,
		int edited,
		int key
) {
	Uint8					txtLen;

	{
		/*
		 * Build text for sub menu header
		 */
		BYTE *buffer1;
		BYTE *buffer2;
		BYTE const_P * subText = txt_localText(pInst->instid, txt_i_tSettings);
		Uint8 len = gui__txtLen(subText);

		buffer1 = (BYTE*)subHeaderTxt;
		buffer2 = (BYTE*)subText;

		len = gui__textCopy(buffer1, buffer2, len+1);									// Copy text Settings to subHeaderText
	}

	{
		sys_TxtHandle			txt;

		txt = txt_localHandle(pInst->instid, pageTexts[item]);
		txtLen = gui__textWrite(pInst, txt);
		gui__dispDrawText(pInst, (Uint8 const_P *)pInst->text.pCurrent, txtLen);		// Text
	}

	{
		int value = 0;
		int digits = 0;
		int decimals = 0;

		switch (item)
		{
			case 0:
				value = Edited.reg.Detail.var1;
				if(edited){
					digits = 2;
				}
				else{
					digits = countDigits(value);
				}
				break;
		}

		{
			int n;

			for(n = 0; n < 21 - digits - txtLen; n++){									// Print space
				Uint8 space = ' ';
				gui__dispDrawText(pInst, &space, 1);
			}
		}

		if(edited){
			drawEditedField(pInst, key, decimals, digits);								// Print edited formatted number
		}
		else{
			dispPrintf(pInst, (int)value, decimals, -1, digits);						// Print formatted number
		}
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws other than header part of the view (CAN items).
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE void DrawItems(
	gui__Inst *				pInst,
	const gui__CallbackView *		pView
) {
	const int pageOffset = callbackParameters.items_per_page*pView->selectedPage;
	int item;

	for (item = 0; item < PAGE_ITEMS - FIRST_ITEM && pageOffset + item < callbackParameters.items; item++)
	{
		const int edited = pView->base.base.state & GUI_MV_ST_EDITING;
		const int atRow = item == pView->selectedPageRow;

		gui__dispSetTextPos(pInst, 0, FIRST_ITEM + item);

		if(atRow && !edited){													// Pointer at row and not edited ?
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Invert to mark pointer
			DrawRow(pInst, pageOffset + item, 0, 0);							// Draw row
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Remove invert
		}
		else{																	// Pointer not at row or edited
			DrawRow(pInst, pageOffset + item, edited && atRow, 0);				// Draw row
		}
	}
}

static void callback_init(gui__CallbackView* pV, gui__Inst * pInst){
	/* Restore row selected */
	pV->selectedPage = pInst->nav.stack[pInst->nav.stack_pos - 1].page;
	pV->selectedPageRow = pInst->nav.stack[pInst->nav.stack_pos - 1].pos;
	pV->selectedItem = pInst->nav.stack[pInst->nav.stack_pos - 1].pos;
	return;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	DrawItems(pInst, pView);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
	if(callbackParameters.items){
		const int pageOffset = callbackParameters.items_per_page*pView->selectedPage + pView->selectedPageRow;

		DrawRow(pInst, pageOffset, 1, key);						// Draw row
		gui__viewInvalidate(pView);
	}
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	int startEdit = FALSE;

	if(gui__navCheckAccess(pInst, TRUE)){
		switch (item)
		{
			case 0:
				Edited.Value = (int32_t)Edited.reg.Detail.var1;
				Edited.Min = 0;
				Edited.Max = 10;
				posX = 1;
				startEdit = TRUE;
				break;
		}																								// Reset position marker
	}
	return startEdit;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	switch (item)
	{
		case 0:
			Edited.reg.Detail.var1 = (uint8_t)Edited.Value;
			break;
	}
	IoBusReg_aPut(Edited.registerIndex, Edited.reg);

	return 1;
}

void gui__viewSroScwChange(int item){
	Edited.registerIndex = item;
	Edited.reg = IoBusReg_aGet(item);
}

static int countDigits(uint32_t value){
	uint32_t comparator = 10;
	int n;

	for(n = 0; n < 10; n++){
		if(value < comparator){
			return n + 1;
		}
		else{
			comparator *= 10;
		}
	}
	return 10 + 1;
}
