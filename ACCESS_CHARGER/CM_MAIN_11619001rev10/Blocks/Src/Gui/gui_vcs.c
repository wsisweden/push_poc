/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger selectedPageRow view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "Cc.h"
#include "gui_callbackMenu.h"
#include "../Can/NodesStatusAndMapping.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define PAGE_ITEMS	8
#define FIRST_ITEM	2

#define GUI__CS_2ND_HDR_ROW	1
#define GUI__CS_CTRL_LEFT	0
#define GUI__CS_CTRL_WIDTH	5
#define GUI__CS_SN_LEFT		(GUI__CS_CTRL_LEFT + GUI__CS_CTRL_WIDTH)
#define GUI__CS_SN_WIDTH	10
#define GUI__CS_ST_LEFT		(GUI__CS_SN_LEFT + GUI__CS_SN_WIDTH)
#define GUI__CS_ST_WIDTH	6

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/
typedef struct{
	Uint32 Used[16];
	int Myself;
	int AvailPos;
	int NodesPos;
} chargerList_type;

typedef struct{
	Uint32 SerialNr;
	enum ParallelStatus_enum status;
} chargerInfo_type;



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void DrawHeaderRow2(gui__Inst * pInst, const gui__CallbackView* pView);
static void DrawItems(gui__Inst * pInst, const gui__CallbackView * pView);
static void DrawRow(gui__Inst* pInst, int rowEdited, const chargerInfo_type* ChargerInfo);
PRIVATE void gui__viewCsDrawSN(
	gui__Inst *				pInst,
	Uint32					sn
);
PRIVATE void gui__viewCsDrawCtrl(
	gui__Inst *				pInst,
	Boolean					on
);

static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);

static void chargerListInit(chargerList_type* This);
static chargerInfo_type chargerListNext(chargerList_type* This);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {0};
static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	PAGE_ITEMS - FIRST_ITEM,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_F_NONE,
	EDITABLE_ITEMS,
};

static struct {
	int On;
	Uint32 SerialNr;
	int Start;
} edited = {0, 0, 0};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Charger selectedPageRow view message handler.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to process view
* \param		msg			view message id
* \param		pArgs		ptr to message args
*
* \return		Int8
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewCsProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws header part (first two rows) of the view.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawHeaderRow2(
	gui__Inst *					pInst,
	const gui__CallbackView*	pView
) {
	/*
	 *	Draw 2nd line header (Ctrl	CM(S/N)	Status)
	 */
	gui__fontSelect(pInst, GUI_FNT_6x8);

	gui__dispSetTextPos(pInst, GUI__CS_CTRL_LEFT, GUI__CS_2ND_HDR_ROW);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tCtrl),
		0, 
		GUI__CS_CTRL_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CS_CTRL_WIDTH);

	gui__dispSetTextPos(pInst, GUI__CS_SN_LEFT, GUI__CS_2ND_HDR_ROW);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tCMSN),
		0, 
		GUI__CS_SN_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CS_SN_WIDTH);

	gui__dispSetTextPos(pInst, GUI__CS_ST_LEFT, GUI__CS_2ND_HDR_ROW);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tStatus),
		0, 
		GUI__CS_ST_WIDTH,
		GUI_ALIGN_CENTER
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CS_ST_WIDTH);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws value of CTRL column into current text buffer.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		on			if true column is marked checked, else unchecked
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCsDrawCtrl(
	gui__Inst *				pInst,
	Boolean					on
) {
	if (on)
	{
		gui__textWriteConst(pInst, (Uint8 const_P *)"[X]", 0, 3);
	}
	else
	{
		gui__textWriteConst(pInst, (Uint8 const_P *)"[ ]", 0, 3);
	}

	gui__textWriteChars(pInst, ' ', 3, GUI__CS_CTRL_WIDTH - 3);
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws value of serial number column into current text buffer.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		sn			serial number
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCsDrawSN(
	gui__Inst *				pInst,
	Uint32					sn
) {
	Uint8					len;

	len = gui__u32toa(
		sn, 
		&pInst->text.pCurrent[GUI__CS_SN_LEFT], 
		GUI__CS_SN_WIDTH
	);

	if (len < GUI__CS_SN_WIDTH)
	{
		gui__textWriteChars(
			pInst, 
			' ', 
			GUI__CS_SN_LEFT + len, 
			GUI__CS_SN_WIDTH - len
		); 
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws value of status column into current text buffer.
*
*	\param		pInst		ptr to the GUI instance
*	\param		status		status to set as read from CAN register
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCsDrawStatus(
	gui__Inst *				pInst,
	Uint8					status
) {
	BYTE const_P *			pTxt;

	switch (status)
	{
	case ParallelStatus_Enum_NotAddedConnected :
		pTxt = txt_localText(pInst->instid, txt_i_tNotAddedConnected);
		break;

	case ParallelStatus_Enum_AddedNotConnected :
		pTxt = txt_localText(pInst->instid, txt_i_tAddedNotConnected);
		break;

	case ParallelStatus_Enum_AddedConnected :
		pTxt = txt_localText(pInst->instid, txt_i_tAddedConnected);
		break;

	default:
		pTxt = NULL;
		break;
	}

	if (pTxt != NULL)
	{
		gui__textWriteAligned(
			pInst,
			pTxt,
			GUI__CS_ST_LEFT,
			GUI__CS_ST_WIDTH,
			GUI_ALIGN_CENTER
		);
	}
	else
	{
		gui__textWriteChars(pInst, ' ', GUI__CS_ST_LEFT, GUI__CS_ST_WIDTH);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
static void DrawRow(gui__Inst* pInst, int rowEdited, const chargerInfo_type* ChargerInfo) {
	if(!ChargerInfo->SerialNr)
		return;

	const int on = ChargerInfo->status == ParallelStatus_Enum_AddedNotConnected ||
				   ChargerInfo->status == ParallelStatus_Enum_AddedConnected;

	gui__viewCsDrawSN(pInst, ChargerInfo->SerialNr);
	gui__viewCsDrawStatus(pInst, ChargerInfo->status);

	if(rowEdited){
		if(edited.Start){
			edited.Start = 0;
			edited.On = on;
			edited.SerialNr = ChargerInfo->SerialNr;
		}
		gui__viewCsDrawCtrl(pInst, edited.On);
		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
		gui__dispDrawText(pInst, pInst->text.pCurrent, 3);
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
		gui__dispDrawText(pInst, &pInst->text.pCurrent[3], pInst->disp.chr_per_line - 3);
	}
	else{
		gui__viewCsDrawCtrl(pInst, on);
		gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws other than header part of the view (CAN items).
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawItems(
	gui__Inst *				pInst,
	const gui__CallbackView*		pView
) {
	chargerInfo_type chargerInfo[PAGE_ITEMS - FIRST_ITEM + 1];
	int rows = 0;

	{																		// Get items for all rows which should be painted and count number of items
		const int pageOffset = (PAGE_ITEMS - FIRST_ITEM)*(pView->selectedPage + 1);
		int items = 0;
		chargerList_type chargerList;
		chargerListInit(&chargerList);

		do{
			chargerInfo[rows] = chargerListNext(&chargerList);

			if(!chargerInfo[rows].SerialNr){								// No more chargers ?
				if(items > PAGE_ITEMS - FIRST_ITEM){						// Not first page ?
					chargerInfo[0] = chargerInfo[PAGE_ITEMS - FIRST_ITEM];
				}
				break;
			}
			else{
				if(rows < PAGE_ITEMS - FIRST_ITEM){							// For all rows
					rows++;
				}
				else{
					rows = 1;
				}
				items++;
			}
		} while(items < pageOffset);										// Until selected page read

		do{																	// Until all serial numbers read
			chargerInfo[PAGE_ITEMS - FIRST_ITEM] = chargerListNext(&chargerList);
			items++;
		} while(chargerInfo[PAGE_ITEMS - FIRST_ITEM].SerialNr);

		callbackParameters.items = items - 1;
		gui__selectionLimit(&callbackParameters);
	}

	{																					// Hopefully the memory for the variables in the block above will be freed here
		const int aRowEdited = pView->base.base.state & GUI_MV_ST_EDITING;
		int item;

		for (item = 0; item < PAGE_ITEMS - FIRST_ITEM && item < rows; item++)			// For all rows which should be painted
		{
			const int Selected = item == pView->selectedPageRow;						// Row selected

			gui__dispSetTextPos(pInst, 0, FIRST_ITEM + item);
			if(Selected && !aRowEdited){												// Pointer at row and not edited ?
				gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);						// Invert to mark pointer
				DrawRow(pInst, 0, &chargerInfo[item]);									// Draw row
				gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);						// Remove invert
			}
			else{																		// Pointer not at row or edited
				DrawRow(pInst, aRowEdited && Selected, &chargerInfo[item]);				// Draw row
			}
		}
	}
}

PRIVATE void callback_init(gui__CallbackView* pV, gui__Inst * pInst) {
	/* Reset row selected */
	pV->selectedPage = 0;
	pV->selectedPageRow = 0;
	pV->selectedItem = 0;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	DrawItems(pInst, pView);
	DrawHeaderRow2(pInst, pView);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
	switch(key){
	case GUI_VKEY_UP :
	case GUI_VKEY_DOWN :
		if(edited.On){
			edited.On = 0;
		}
		else{
			edited.On = 1;
		}
		gui__viewInvalidate(pView);
		break;
	}
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	if(gui__navCheckAccess(pInst, TRUE)){
		edited.Start = 1;
		return 1;
	}
	return 0;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	Uint32 SerialNr;
	int emptyPos = -1;
	int n;
	for(n = 0; n < 16; n++){
		reg_aGet(&SerialNr, gui_ParallelAvail, n);
		if(!SerialNr){												// Remember empty position
			emptyPos = n;
		}
		else if(SerialNr == edited.SerialNr){						// Already in list ?
			if(!edited.On){											// Remove ?
				Uint32 Zero = 0;
				reg_aPut(&Zero, gui_ParallelAvail, n);				// Remove from list
			}
			return 1;
		}
	}

	if(edited.On && emptyPos >= 0){
		reg_aPut(&edited.SerialNr, gui_ParallelAvail, emptyPos);
	}
	return 1;
}

static void chargerListInit(chargerList_type* This){
	This->Myself = 0;
	This->AvailPos = 0;
	This->NodesPos = 0;

	int n;
	for(n = 0; n < 16; n++){
		reg_aGet(&This->Used[n], gui_ParallelAvail, n);
	}
}

static chargerInfo_type chargerListNext(chargerList_type* This){
	chargerInfo_type info;
	uint32_t mySerialNr;
	reg_get(&mySerialNr, gui_SerialNo);

	info.SerialNr = 0;

	if(!This->Myself){															// Myself
		This->Myself = 1;
		info.SerialNr = mySerialNr;

		int n;
		for(n = 0; n < 16; n++){
			if(This->Used[n] == mySerialNr){				// Already in list ?
				info.status = ParallelStatus_Enum_AddedConnected;
				return info;
			}
		}

		info.status = ParallelStatus_Enum_NotAddedConnected;
		return info;
	}

	while(This->AvailPos < 16){									// Used chargers
		info.SerialNr = This->Used[This->AvailPos];
		if(info.SerialNr && info.SerialNr != mySerialNr){
			Uint8 status;

			reg_aGet(&status, gui_ParallelStatus, This->AvailPos);
			if(status == ParallelStatus_Enum_AddedConnected){
				info.status = ParallelStatus_Enum_AddedConnected;
			}
			else{
				info.status = ParallelStatus_Enum_AddedNotConnected;
			}
			This->AvailPos++;
			return info;
		}
		This->AvailPos++;
	}

	{										// Can network
		volatile struct NodeInformation_type* const Nodes = CAN_NodeStatusAndMapping_GetNodes();
		while(This->NodesPos < NODES_STATUS_LEN){
			info.SerialNr = Nodes[This->NodesPos].SerialNr;
			This->NodesPos++;

			int n;
			for(n = 0; n < 16; n++){
				if(This->Used[n] == info.SerialNr){				// Already in list ?
					info.SerialNr = 0;
					break;
				}
			}

			if(info.SerialNr && info.SerialNr != mySerialNr){
				info.status = ParallelStatus_Enum_NotAddedConnected;
				return info;
			}
		}
	}

	return info;
}
