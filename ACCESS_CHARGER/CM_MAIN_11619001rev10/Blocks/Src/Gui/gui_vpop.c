/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI	
*
*	\brief		Popup view handling.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
//#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		'Popup' view proc.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to view
* \param		msg			message type
* \param		pArgs		message args
*
* \return		
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewPopupProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__PopupView *		pop = (gui__PopupView *)pView;

//	DEB_DBG(deb_logN("GUI.popup.Proc: %s", gui__dbgCommands[msg]);)

	switch (msg)
	{
	case gui_msg_create:
		break;

	case gui_msg_init:
		{
			gui__PopupViewArgs * pPopArgs = (gui__PopupViewArgs *)pArgs;

			pop->text1 = pPopArgs->text1;
			pop->text2 = pPopArgs->text2;
			pop->on_exit = pPopArgs->on_exit;

			if (pPopArgs->delay > 0)
			{
				/*
				 *	If view has expiration time start the timer.
				 */
				gui__viewTmrStart(pView, pPopArgs->delay, 0);
			}
		}
		break;

	case gui_msg_timeout:
		gui__viewClose(pInst, pView);
		break;

	case gui_msg_close:
		gui__viewTmrStop(pView);
		break;

	case gui_msg_paint:
		{
			display_Rect	rect;
			Uint8			bold_len;
			Uint8			normal_len;
			display_Coord	bold_height;
			display_Coord	normal_height;

			rect.y = 0;
			rect.x = 0;
			rect.width = pInst->pInit->width;
			rect.height = pInst->pInit->height;

			gui__dispClear(pInst);

			/*
			 *	Get bold text height.
			 */
			gui__fontSelect(pInst, GUI_FNT_8x8);

			gui__textSelectBuffer(pInst, pInst->buffer.buf1);
			//bold_len = gui__textWrite(pInst, pop->text1);
			bold_len = gui__textWriteBuffer(pop->text1, pInst->text.pCurrent, GUI_MAX_TXT3);

			gui__dispDrawTextAlignedEx(
				pInst, 
				pInst->buffer.buf1, 
				bold_len, 
				&rect, 
				GUI_ALIGN_CENTER|GUI_TEST_SIZE
			);

			bold_height = rect.height;

			/*
			 *	Get normal text height.
			 */
			if (pop->text2 != GUI_NO_TEXT)
			{
				gui__fontSelect(pInst, GUI_FNT_6x8);

				gui__textSelectBuffer(pInst, pInst->buffer.buf2);
				//normal_len = gui__textWrite(pInst, pop->text2);
				normal_len = gui__textWriteBuffer(pop->text2, pInst->text.pCurrent, GUI_MAX_TXT3);

				rect.height = pInst->pInit->height;

				gui__dispDrawTextAlignedEx(
					pInst, 
					pInst->buffer.buf2, 
					normal_len, 
					&rect, 
					GUI_ALIGN_CENTER|GUI_TEST_SIZE
				);

				normal_height = rect.height;
			}
			else
			{
				normal_len = 0;
				normal_height = 0;
			}

			/*
			 *	Draw bold text.
			 */
			rect.y = (pInst->pInit->height - (bold_height + normal_height)) / 2;
			rect.height = bold_height;

			gui__fontSelect(pInst, GUI_FNT_8x8);

			gui__dispDrawTextAlignedEx(
				pInst, 
				pInst->buffer.buf1, 
				bold_len, 
				&rect, 
				GUI_ALIGN_CENTER
			);

			/*
			 *	Draw normal text.
			 */
			gui__fontSelect(pInst, GUI_FNT_6x8);

			if (pop->text2 != GUI_NO_TEXT)
			{
				rect.y += bold_height;
				rect.height = normal_height;

				gui__dispDrawTextAlignedEx(
					pInst, 
					pInst->buffer.buf2, 
					normal_len, 
					&rect, 
					GUI_ALIGN_CENTER
				);
			}

			gui__viewSetPainted(pView);
		}
		break;

	case gui_msg_key_up:
		{
			Boolean Exit = TRUE;

			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_OK:
			case GUI_VKEY_ESC:
				{

					Exit &= (pop->text1 == txt_localHandle(pInst->instid, txt_i_tStartingNetwork)) ? FALSE:TRUE;
					Exit &= (pop->text1 == txt_localHandle(pInst->instid, txt_i_tJoiningNetwork)) ? FALSE:TRUE;
					Exit &= (pop->text1 == txt_localHandle(pInst->instid, txt_i_tLeavingNetwork)) ? FALSE:TRUE;

					if(Exit == TRUE)
					{
						if (pop->on_exit != NULL)
						{
							if (pop->on_exit(pInst))
							{
								gui__viewClose(pInst, pView);
							}
						}
						else
						{
							gui__viewClose(pInst, pView);
						}
					}
				}
				break;
			}
			pInst->keyCount = 0;
		}
		break;

	case gui_msg_key_down:
		break;

	default:
		break;
	}

	return 0;
}

