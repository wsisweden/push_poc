/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger selectedPageRow view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

#include "gui_callbackMenu.h"
#include "../IObus/IObusOutputs.h"
#include "IObus.h"
#include "../Chalg/inc_cm3/caiNames.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define FIRST_ITEM	1
#define PAGE_ITEMS	7

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

static void DrawRow(gui__Inst* pInst, int item, int edited);
static void drawEditedField(gui__VKey key);

static void DrawHeaderRow2(gui__Inst* pInst);

/* Call back functions */
static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {3,'s','c','s'};
static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	PAGE_ITEMS - FIRST_ITEM,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_F_NONE,
	EDITABLE_ITEMS,
};

static struct{
	int stage;
	IoBusOutRegister_type reg;
	int registerIndex;
} Edited = {0, {0}, 0};

/* Charging Stages names */
static const sys_TxtHandle stageNameTexts[] =
{
		[OutputStage_Pre] = txt_i_tChargePre,
		[OutputStage_Main] = txt_i_tChargeMain,
		[OutputStage_Additional] = txt_i_tChargeAdditional,
		[OutputStage_Ready] = txt_i_tChargeReady,
		[OutputStage_EQU] = txt_i_tChargeEqualize,
		[OutputStage_Idle] = txt_i_tChargeIdle,
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



PUBLIC Int8 gui__viewSroScsProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	callbackParameters.items = OutputStage_Length;
	return gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
static void drawEditedField(
	gui__VKey key
) {
		switch(key){
		case GUI_VKEY_UP :
		case GUI_VKEY_DOWN :
			Edited.stage = !Edited.stage;
			break;
		case GUI_VKEY_OK :
			break;
		}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawRow(
	gui__Inst *				pInst,
	int item,
	int edited
) {
	sys_TxtHandle			txt;
	Uint8					txtLen;

	txt = txt_localHandle(pInst->instid, stageNameTexts[item]);
	txtLen = gui__textWrite(pInst, txt);

	gui__dispDrawText(pInst, (Uint8 const_P *)pInst->text.pCurrent, txtLen);			// Text
	{
		int n;

		for(n = 0; n < 21 - 3 - txtLen; n++){
			Uint8 space = ' ';
			gui__dispDrawText(pInst, &space, 1);
		}
	}

	const char* unMarked = "[ ]";
	const char* marked = "[x]";
	const char* text;
	{
		int stage;

		if(edited){
			stage = Edited.stage;
		}
		else{
			stage = Edited.reg.ChargingStages & (1<<item);
		}

		if(stage){
			text = marked;
		}
		else{
			text = unMarked;
		}
	}

	if(edited){
		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);								// Invert to mark pointer
		gui__dispDrawText(pInst, (Uint8 const_P *)text, 3);								// Text
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);								// Remove invert
	}
	else{
		gui__dispDrawText(pInst, (Uint8 const_P *)text, 3);								// Text
	}

	{
		/*
		 * Build text for sub menu header
		 */
		BYTE *buffer1;
		BYTE *buffer2;
		BYTE const_P * subText = txt_localText(pInst->instid, txt_i_tSettings);
		Uint8 len = gui__txtLen(subText);

		buffer1 = (BYTE*)subHeaderTxt;
		buffer2 = (BYTE*)subText;

		len = gui__textCopy(buffer1, buffer2, len+1);											// Copy text Settings to subHeaderText
	}
}

static void callback_init(gui__CallbackView* pV, gui__Inst * pInst){
	/* Restore row selected */
	pV->selectedPage = pInst->nav.stack[pInst->nav.stack_pos - 1].page;
	pV->selectedPageRow = pInst->nav.stack[pInst->nav.stack_pos - 1].pos;
	pV->selectedItem = pInst->nav.stack[pInst->nav.stack_pos - 1].pos;
	return;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	DrawHeaderRow2(pInst);
	gui__DrawItemsScs(pInst, pView, DrawRow, &callbackParameters);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
	if(callbackParameters.items){
		drawEditedField(key);
		gui__viewInvalidate(pView);
	}
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	pInst->nav.page = pView->selectedPage;
	pInst->nav.item = pView->selectedPageRow;

	if(gui__navCheckAccess(pInst, TRUE)){
		Edited.stage = Edited.reg.ChargingStages & (1 << item);
		return 1;
	}
	return 0;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	const int bitMask = (1 << item);

	if(Edited.stage){
		Edited.reg.ChargingStages |= bitMask;
	}
	else{
		Edited.reg.ChargingStages &= ~bitMask;
	}
	IoBusReg_aPut(Edited.registerIndex, Edited.reg);
	return 1;
}

void gui__viewSroScsChange(int item){
	Edited.registerIndex = item;
	Edited.reg = IoBusReg_aGet(item);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws constant header part 2 of the view.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void DrawHeaderRow2(
	gui__Inst *				pInst
) {
	Uint8 					len;
	display_Rect			rect;

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);
	gui__fontSelect(pInst, GUI_FNT_6x8);

	rect.x = 0;
	rect.y = FIRST_ITEM * 8;
	rect.width = (
		pInst->pInit->width / pInst->disp.pFont->width
	) * pInst->disp.pFont->width;
	rect.height = 8;

	/*
	 *	Draw 'Phase'
	 */
	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tPhase));

	gui__dispDrawTextAligned(
		pInst,
		pInst->text.pCurrent,
		len,
		&rect,
		GUI_ALIGN_LEFT
	);

	/*
	 *	Draw 'Output'
	 */
	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tOutput));

	gui__dispDrawTextAligned(
		pInst,
		pInst->text.pCurrent,
		len,
		&rect,
		GUI_ALIGN_RIGHT
	);
}
