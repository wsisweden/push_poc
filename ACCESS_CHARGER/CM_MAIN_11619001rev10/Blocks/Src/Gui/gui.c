/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		GUI FB interface and task.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	GUI	GUI
*
*	\brief		Graphical user interface.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "txt.h"
#include "deb.h"

#include "global.h"
#include "rtc.h"

#include "local.h"

#include "Radio.h"
#include "st7565.h"
#include "Chalg.h"
#include "Sup.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	sys_TxtHandle			text1;		/**< Header text to show			*/
	sys_TxtHandle			text2;		/**< Extra text to show				*/
	Uint16					delay;		/**< Visibility timeout				*/
	gui__fnPopupExit		on_exit;	/**< Exit handler					*/
} gui__RadioEvent;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE OSA_TASK void		gui_task(void);

PRIVATE void gui__timer(swtimer_Timer_st * pTimer);

PRIVATE Uint16	gui__getTriggers(gui__Inst * pInst);
PRIVATE Boolean gui__exitJoinEnabled(gui__Inst * pInst);
PRIVATE Boolean gui__exitForceStart(gui__Inst * pInst);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG
PUBLIC Uint8 gui__debug = 0;
Uint32 gui_counter = 0;
tstamp_Stamp gui_lastRun;
#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Radion network event messages. Must match enum radio_cmdStatus in radio.h.
 */
PRIVATE const_P gui__RadioEvent	gui__radioMessages[] =
{
	/*
	 *	Message text handle			Extra text				Timeout	Exit handler
	 *	-------------------------	------------------		-------	-------------*/
	{	txt_i_tStartingNetwork,		GUI_NO_TEXT,			0,		NULL				},
	{	txt_i_tStartingNetwork,		txt_i_tFail,			30,		NULL				},
	{	txt_i_tStartingNetwork,		txt_i_tSuccess,			30,		NULL				},
	{	txt_i_tJoiningNetwork,		txt_i_tSearchNode,		0,		NULL				},
	{	txt_i_tJoiningNetwork,		txt_i_tFail,			30,		NULL				},
	{	txt_i_tJoiningNetwork,		txt_i_tSuccess,			30,		NULL				},
	{	txt_i_tLeavingNetwork,		GUI_NO_TEXT,			0,		NULL				},
	{	txt_i_tLeavingNetwork,		txt_i_tFail,			30,		NULL				},
	{	txt_i_tLeavingNetwork,		txt_i_tSuccess,			30,		NULL				},
	{	txt_i_tJoinEnabled,			GUI_NO_TEXT,			0,		gui__exitJoinEnabled},
	{	txt_i_tJoinEnabled,			txt_i_tRadioTimeout,	30,		NULL				},
	{	txt_i_tJoinEnabled,			txt_i_tRadioFail,		30,		NULL				},
	{	txt_i_tJoinEnabled,			GUI_NO_TEXT,			1,		NULL				},
	{	txt_i_tNewParamsBm,			GUI_NO_TEXT,			0,		NULL				},
	{	txt_i_tNewParamsBm,			txt_i_tFail,			30,		NULL				},
	{	txt_i_tNewParamsBm,			txt_i_tSuccess,			30,		NULL				},
	{   GUI_NO_TEXT,              	GUI_NO_TEXT,            0,    	NULL    	        }
};

SYS_DEFINE_FILE_NAME;
/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid		An index (0..) that is accepted by various functions
*						to identify the instance. Should be kept for later use,
*						or may be discarded if not required by the FB.
*	\param		pArgs	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * gui_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	gui__Inst *				pInst;

	pInst = (gui__Inst *) mem_reserve(&mem_normal, sizeof(gui__Inst));
	deb_assert(pInst);
	pInst->pInit = (gui_Init const_P *) pArgs;

	osa_newTask(
		&pInst->task,
		"gui__task",
		OSA_PRIORITY_NORMAL,
		gui_task,
		2048
	);

	pInst->instid = iid;
	pInst->state = 0;
	pInst->trigger = 0;
	pInst->task.pUtil = pInst;			/* Set ptr to instance for the task	*/
	pInst->login = GUI_ACCESS_LEVEL0;
	pInst->contrast = 100;
	pInst->dt_format = GUI_DT_EUR;
	pInst->backlight_del = 2 * 600;
	pInst->backlight_time = 0;

	pInst->idle_cnt = 0;
	swtimer_new(&pInst->timer, gui__timer, 100, 100);
	pInst->timer.pUtil = pInst;

	sys_initHeader(&pInst->header);

	gui__fontInit(pInst);
	gui__textInit(pInst);

	gui__kbdInit(pInst);

	gui__navInit(pInst);
	gui__viewInit(pInst);
		
	sys_mainInitCount(1);

#if 0
	{
		volatile Uint8 foo;
		Posix_Time tm;
		RTC_Time rtc;

		rtc_hwGetAsPosix(&tm);

		rtc_convertToTime32(tm, &rtc);

		foo = rtc_posixToWeekday(tm);


		foo++;
	}
#endif

	return((void *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		instance	Pointer to instance.
* 	\param		pArgs		Pointer to the initialization arguments.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void gui_reset(
	void *					pInstance,
	void const_P *			pArgs
) {	
	gui__Inst *			pInst;

	pInst = (gui__Inst *) pInstance;

	THIS->trigger |= GUI_TRG_RESET;
	osa_signalSend(&THIS->task);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		instance	Pointer to instance.
*	\param		nType		Down type.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void gui_down(
	void *					pInst, 
	sys_DownType			nType
) {
	Uint8					state;

	atomic(THIS->trigger |= GUI_TRG_DOWN;)

	osa_signalSend(&THIS->task);

	do 
	{
		osa_yield();

		atomic(state = THIS->state;)

	} 
	while (state & GUI_ST_RUNNING);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read
*				the value of a parameter owned by this FB.
*
*	\param		pInstance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue		Pointer to the storage for the value.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus gui_read(
	void *					pInstance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex,
	void *					pValue
) {	
	if (rIndex == reg_indic_LoginLevel)
	{
		*(Uint8 *)pValue = ((gui__Inst *)pInstance)->login;
		return REG_OK;
	}

	if (rIndex == reg_indic_backlight_delayP)
	{
		*(Uint8 *)pValue = (((gui__Inst *)pInstance)->backlight_del) / 600;
		return REG_OK;
	}

	if (rIndex == reg_indic_LanguageP)
	{
		*(Uint8 *)pValue = (((gui__Inst *)pInstance)->language);
		return REG_OK;
	}

	return REG_UNAVAIL;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		instance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus gui_write(
	void *					pInst, 
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex
) {

	switch (rIndex)
	{

	/*
	 *	Contrast was written.
	 */
	case reg_indic_DisplayContrast:
		THIS->trigger |= GUI_TRG_CONTRAST;
		break;

	/*
	 *	Login register was written.
	 */
	case reg_indic_login:
		THIS->trigger |= GUI_TRG_LOGIN;
		break;

	/*
	 *	Logout register was written.
	 */
	case reg_indic_logout:
		THIS->trigger |= GUI_TRG_LOGOUT;
		break;

	/*
	 *	Backlight (idle) delay was written.
	 */
	case reg_indic_backlight_delay:
		THIS->trigger |= GUI_TRG_DEL;
		break;
	
	/*
	 *	Language changed.
	 */
	case reg_indic_Language:
		THIS->trigger |= GUI_TRG_LANGUAGE;
		break;

	/*
	 *	Time and date format written.
	 */
	case reg_indic_TimeDateFormat:
		THIS->trigger |= GUI_TRG_FRMT;
		break;

	/*
	 *	Radio network status changed.
	 */
	case reg_indic_cmdStatus:
		THIS->trigger |= GUI_TRG_NWK;
		break;

	/*
	 *	Radio traffic
	 */
	case reg_indic_radioTraffic:
		THIS->trigger |= GUI_TRG_RADIOACT;
		break;

	/*
	 *	Remote out function changed.
	 */
	case reg_indic_RemoteOut_function:
	case reg_indic_enabled_tmp:
		THIS->trigger |= GUI_TRG_REPAINT;
		break;

	/*
	 *	Status reporting written (probably opens a popup).
	 */
	case reg_indic_statusRep:
		THIS->trigger |= GUI_TRG_STAT_REP;
		break;

	/*
	 *	Backlight (idle) delay was written.
	 */
	case reg_indic_backlight_time:
		THIS->trigger |= GUI_TRG_DELTIME;
		break;

	/*
	 *	ForceStart was written.
	 */
	case reg_indic_ForceStartView:
		THIS->trigger |= GUI_TRG_FORCESTART_VIEW;
		break;

	default:
		break;
	}

	if (THIS->trigger != 0)
	{
		osa_signalSend(&THIS->task);
	}

	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_msg
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Called when an event has been received.
*
*	\param		pInstance		Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void gui_msg(
	void *	pInstance
) {
    gui__Inst * pInst;

	pInst = (gui__Inst *) pInstance;

	pInst->kbd.state |= GUI_KBD_ST_KEYS;
	osa_coTaskRun(&pInst->kbd.coTask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_test
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Self test
*
*	\param		instance	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 gui_test(
	void *					pInst
) {
	return 60; /* next test in ~1s */
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_ctrl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Control interface.
*
*	\param		pInstance	Pointer to GUI instance.
*	\param		nCtrl		Control request type.
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void gui_ctrl(
	void *					pInstance,
	sys_CtrlType *			pCtrl
) {
	/*
	 *	0x80 = request to enter test mode. Handled as default request to
	 *	go down.
	 */
	if (pCtrl->cmd == 0x80)
	{
		gui__Inst *				pInst;
		Uint8					state;

		pInst = (gui__Inst *)pInstance;

		atomic(state = THIS->state;)

		if (state & GUI_ST_RUNNING)
		{
			atomic(THIS->trigger |= GUI_TRG_DOWN;)

			osa_signalSend(&THIS->task);

			do 
			{
				osa_yield();

				atomic(state = THIS->state;)

			} 
			while (state & GUI_ST_RUNNING);
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gui_task
*
*--------------------------------------------------------------------------*//**
*
*	\brief		GUI main task.
*
*	\param		
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE OSA_TASK void gui_task(
	void
) {
	gui__Inst *				pInst;

	pInst = (gui__Inst *) osa_taskCurrent()->pUtil;

	/*
	 * Wait for REG to be ready. This includes waiting for FLMEM to be
	 * ready.
	 */

	deb_log("GUI: Waiting for REG to be operational");
	if (osa_syncWait(OSA__SYNC_REG, 1000) != 0) {
		pInst->state |= GUI_ST_REG_TIMEOUT;
	}

	/* Step 2: Complete the cold-start initializations */
										/* Execute these as necessary:		*/
	//osa_syncSet(...);					/* - Announce new services avail	*/

	sys_mainInitReady();				/* Tell SYS that init completed		*/

	/*
	 *	Initialize display data structures.
	 */
	gui__dispInitStructs(pInst);

	if (!(pInst->state & GUI_ST_REG_TIMEOUT))
	{
		osa_syncGet(project_stateMachSync);
	}

	FOREVER 
	{
		Uint16				triggers;
		Boolean				firstcycle = TRUE;

		/*
		 *	Wait for reset.
		 */
		if (!(pInst->state & GUI_ST_REG_TIMEOUT)) {
			while (((triggers = gui__getTriggers(pInst)) & GUI_TRG_RESET) == 0)
			{
				osa_signalGet();
			}
		} else {
			triggers = gui__getTriggers(pInst);
		}

		deb_log("GUI: Reset detected");

		if(pInst->state == GUI_ST_WAKEUP)
		{
			pInst->state = pInst->oldState;
		}
		else {
			/*
			 *	Prevent the REG_TIMEOUT flag from being overwrite
			 */

			Uint8 oldState = pInst->state;
			pInst->state = GUI_ST_RUNNING;

			if (oldState & GUI_ST_REG_TIMEOUT){
				pInst->state |= GUI_ST_REG_TIMEOUT;
			}
		}

		gui__kbdFifoFlush(pInst);

		/*
		 *	Initialize display (if we are not going immediately down).
		 */
		if (!(triggers & GUI_TRG_DOWN))
		{
			gui__dispInitDisplay(pInst);
		}

		if(pInst->flags & GUI_FLG_DOWN){
			atomic(
				pInst->flags &= ~GUI_FLG_DOWN;
			);
		}
		else{
			gui__navigate(pInst, gui_nav_root);
			gui__viewInvalidate(pInst->view.pCurrent);
		}
		swtimer_start(&pInst->timer);

		FOREVER 
		{
			gui__VKey		key;

#ifndef NDEBUG
			tstamp_getStamp(&gui_lastRun);
			gui_counter++;
#endif

			if (!(triggers & GUI_TRG_DOWN))
			{
				triggers = gui__getTriggers(pInst);
			}

			if (firstcycle)
			{
				triggers |= GUI_TRG_LANGUAGE|GUI_TRG_FRMT|GUI_TRG_CONTRAST|GUI_TRG_DEL;
			}

#if 0
			/* Debugging code */
			{
				static Uint16 foo = 0;

				if (foo++ > 50)
				{
					foo = 0;
					triggers |= GUI_TRG_RADIOACT;
				}
			}
#endif

			/*-----------------------------------------------------------------
			 *
			 *	DOWN signal received. Drive the display down and allow down
			 *	call to continue;
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_DOWN)
			{

				deb_log("GUI: going down");
				swtimer_stop(&pInst->timer);
				gui__kbdDown(pInst);
				gui__dispDown(pInst);
				atomic(pInst->oldState = pInst->state;)
				atomic(pInst->flags |= GUI_FLG_DOWN;)
				atomic(pInst->state = GUI_ST_WAKEUP;)
				break;
			}

			/*-----------------------------------------------------------------
			 *
			 *	Log in/ out register was written?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & (GUI_TRG_LOGOUT|GUI_TRG_LOGIN))
			{
				gui__PopupViewArgs args;

				args.delay = 30;
				args.text2 = GUI_NO_TEXT;
				args.on_exit = NULL;

				if (triggers & GUI_TRG_LOGOUT)
				{
					/*
					 *	Logging out.
					 */
					pInst->login = GUI_ACCESS_LEVEL0;

					args.text1 = txt_localHandle(pInst->instid, txt_i_tLoggedOut);
				}
				else if (triggers & GUI_TRG_LOGIN)
				{
					Uint16	log;
					Uint16	required_level;
					Uint32	access;
					int 	navDpl = FALSE;
					gui_MenuItem const_P *	pItem = NULL;

					/*
					 *	Navigate one level back (root menu in normal login,
					 *	or the menu table that caused the login prompt in
					 *	automatic login) and get the access level it requires.
					 */
					gui__navigate(pInst, gui_nav_out);

					if(pInst->nav.item != GUI_NAV_INVALID){
						pItem = pInst->nav.items[pInst->nav.item];
						if(pItem != NULL){
							if(gui__tblType(pItem->subitem) == GUI_TBL_TYPE_DPL){
								navDpl = TRUE;
							}
						}
					}

					/*
					 *	Logging in, check if password matches.
					 */

					reg_getLocal(&log, reg_i_login, pInst->instid);

					if(navDpl){
						/*
						 * Algorithm for DPL access code:
						 * 1. Get serial number (8 lowest digits)
						 * 2. Divide into  4 bytes
						 * 3. Add 1 (DPL) to MSB (byte[3])
						 * 4. Multiply all bytes (byte[3]*byte[2]*byte[1]*byte[0]) if byte[x] != 0
						 * 5. mask out the 4 lowest digits of the result
						 * 6. Compare  with value entered
						 */
						Uint32 mySerialNr;
						Uint32 mySerialNrBytes;
						Uint8 mySerialNrByte[4];

						reg_get(&mySerialNr, gui_SerialNo);

						mySerialNr = mySerialNr % 100000000;						// step 1

						mySerialNrByte[0] = (Uint8) mySerialNr;						// step 2
						mySerialNrByte[1] = (Uint8) (mySerialNr >> 8);				// step 2
						mySerialNrByte[2] = (Uint8) (mySerialNr >> 16);				// step 2
						mySerialNrByte[3] = (Uint8) (mySerialNr >> 24);				// step 2

						mySerialNrBytes = mySerialNrByte[3] + 1;					// step 3

						if(mySerialNrByte[2] != 0)
						{
							mySerialNrBytes = mySerialNrBytes * mySerialNrByte[2];	// step 4
						}

						if(mySerialNrByte[1] != 0)
						{
							mySerialNrBytes = mySerialNrBytes * mySerialNrByte[1];	// step 4
						}

						if(mySerialNrByte[0] != 0)
						{
							mySerialNrBytes = mySerialNrBytes * mySerialNrByte[0];	// step 4
						}

						access = mySerialNrBytes % 10000;							// step 5

						if (log == access){											// step 6

							Uint8 bitConfig;

							reg_get(&bitConfig, gui_BitConfig);
							bitConfig |= CHALG_BITCFG_DPL;
							reg_put(&bitConfig, gui_BitConfig);

							args.text1 = txt_localHandle(
								pInst->instid,
								txt_i_tAccessGranted
							);

							gui__navigate(pInst, gui_nav_in);
						}
						else{
							args.text1 = txt_localHandle(
								pInst->instid,
								txt_i_tAccessDenied
							);
						}
					}
					else{
						required_level = gui_tblAccess(pInst->nav.pTable);
						reg_getLocal(&access, reg_i_SecurityCode1, pInst->instid);

						if (log == access)
						{
							pInst->login = GUI_ACCESS_LEVEL1;
						}
						else
						{
							reg_getLocal(&access, reg_i_SecurityCode2, pInst->instid);

							if (log == access)
							{
								pInst->login = GUI_ACCESS_LEVEL2;
							}
							else
							{
								pInst->login = GUI_ACCESS_LEVEL0;
							}
						}

						if ((pInst->login < required_level) || (pInst->login == GUI_ACCESS_LEVEL0))
						{
							/*
							 *	If login level is lower that access level required
							 *	by the table show 'Access denied' even if the
							 *	input login code was valid.
							 */
							args.text1 = txt_localHandle(
								pInst->instid,
								txt_i_tAccessDenied
							);
						}
						else
						{
							args.text1 = txt_localHandle(
								pInst->instid,
								txt_i_tAccessGranted
							);
						}
					}
				}

				gui__navRebuild(pInst);
				gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
			}

			/*-----------------------------------------------------------------
			 *
			 *	Language changed?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_LANGUAGE)
			{
				Uint8		langidx;

				reg_getLocal(&pInst->language, reg_i_Language, pInst->instid);

				langidx = pInst->pInit->pLangMap[pInst->language];

				if (langidx != 0xFF)
				{
					txt_setLanguage(langidx);
					gui__viewInvalidate(pInst->view.pCurrent);
#if 0
					if (!firstcycle)
					{
						Uint8 dt_format;

						if (pInst->language == gui_lang_english_us)
						{
							dt_format = gui_time_usa;
						}
/*
						else if (pInst->language<= gui_lang_spanish)
						{
							dt_format = gui_time_eur;
						}
*/
						else
						{
							dt_format = gui_time_iso;
						}

						reg_putLocal(&dt_format, reg_i_TimeDateFormat, pInst->instid);
					}
#endif
				}
			}

			/*-----------------------------------------------------------------
			 *
			 *	Date / time format changed?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_FRMT)
			{
				reg_getLocal(&pInst->dt_format, reg_i_TimeDateFormat, pInst->instid);
				gui__viewInvalidate(pInst->view.pCurrent);
			}

			/*-----------------------------------------------------------------
			 *
			 *	Contrast needs updating?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_CONTRAST)
			{
				reg_getLocal(&pInst->contrast, reg_i_DisplayContrast, pInst->instid);

				gui__dispContrast(pInst, pInst->contrast);
			}

			/*-----------------------------------------------------------------
			 *
			 *	Idle delay needs updating?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_DEL)
			{
				Uint8		tmp;

				reg_getLocal(&tmp, reg_i_backlight_delay, pInst->instid);

				atomic(
					pInst->backlight_del = tmp * 600UL;
					pInst->idle_cnt = 0;
					pInst->backlight_time = 0;
				);
			}

			/*-----------------------------------------------------------------
			 *
			 *	Radio network status changed?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_NWK)
			{
				Uint8				status;

				deb_assert(dim(gui__radioMessages) == RADIO_CMDS_COUNT);

				reg_get(&status, gui_cmdStatus);

				if (status < dim(gui__radioMessages))
				{
					gui__RadioEvent const_P * pEventCfg = &gui__radioMessages[status];

					if (pEventCfg->text1 == GUI_NO_TEXT)
					{
						/*
						 *	Request to close a popup view.
						 */
						if (gui__viewIsPopup(pInst->view.pCurrent))
						{
							gui__viewClose(pInst, pInst->view.pCurrent);
						}
					}
					else
					{
						/*
						 *	Request to open a popup view.
						 */
						gui__PopupViewArgs args;

						args.delay = pEventCfg->delay;
						args.text1 = txt_localHandle(
							pInst->instid,
							pEventCfg->text1
						);
						args.on_exit = pEventCfg->on_exit;

						if (pEventCfg->text2 != GUI_NO_TEXT)
						{
							args.text2 = txt_localHandle(
								pInst->instid,
								pEventCfg->text2
							);
						}
						else
						{
							args.text2 = GUI_NO_TEXT;
						}

						gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
					}
				}
			}

			/*-----------------------------------------------------------------
			 *
			 *	Status report changed?
			 *	Read the new status. It will be handled and reset in process
			 *	view.
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_STAT_REP)
			{
				reg_get(&pInst->status_rep, gui_StatusRep);
			}

			/*-----------------------------------------------------------------
			 *
			 *	new backlight delay time from SUP?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_DELTIME)
			{
				if ((pInst->state & GUI_ST_IDLE) || (pInst->language == gui_lang_english_us)) //if IDLE or FW_MODE = USA
				{
					Uint8 backlight_time;
					reg_get(&backlight_time, gui_backlight_time);

					atomic(
						pInst->backlight_time = backlight_time * 600UL;
						pInst->idle_cnt = 0;
					);

					gui__dispBacklight(pInst, TRUE);
					pInst->state &= ~GUI_ST_IDLE;

				}

			}
			
			/*-----------------------------------------------------------------
			 *
			 *	Radio network status changed?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_FORCESTART_VIEW)
			{
				Uint8				ForceStartView;

				reg_get(&ForceStartView, gui_ForceStartView);

				if (ForceStartView == FALSE)
				{
					/*
					 *	Request to close a popup view.
					 */
					if (gui__viewIsPopup(pInst->view.pCurrent))
					{
						gui__viewClose(pInst, pInst->view.pCurrent);
					}
				}
				else
				{
					/*
					 *	Request to open a popup view.
					 */
					gui__PopupViewArgs args;

					args.delay = 300;
					args.text2 = GUI_NO_TEXT;
					args.text1 = txt_localHandle(
						pInst->instid,
						txt_i_tForceStart
					);
					args.on_exit = gui__exitForceStart;

					gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
				}
			}

			/*-----------------------------------------------------------------
			 *
			 *	View rebuild requested?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_REPAINT)
			{
				if (pInst->view.pCurrent)
				{
					gui__viewSendMessage(
						pInst, 
						pInst->view.pCurrent, 
						gui_msg_rebuild, 
						NULL
					);
				}
			}

			/*-----------------------------------------------------------------
			 *
			 *	Refresh timer timed out?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_REFRESH)
			{
				if (pInst->view.pCurrent)
				{
					gui__viewSendMessage(
						pInst, 
						pInst->view.pCurrent, 
						gui_msg_tick, 
						NULL
					);
				}
			}

			/*-----------------------------------------------------------------
			 *
			 *	Process keyboard messages
			 *
			 *---------------------------------------------------------------*/
			while (gui__kbdFifoGet(pInst, &key))
			{
				atomic(pInst->idle_cnt = 0;);
				atomic(pInst->backlight_time = 0;);
				/*
				 *	Wake up from idle needed?
				 */
				if (pInst->state & GUI_ST_IDLE)
				{
					pInst->state &= ~GUI_ST_IDLE;
					gui__dispBacklight(pInst, TRUE);
					gui__kbdFlush(pInst);
					continue;
				}

				if (pInst->view.pCurrent)
				{
					gui__viewSendMessage(
						pInst, 
						pInst->view.pCurrent,
						key & GUI_VKEY_RELEASE ? gui_msg_key_up : gui_msg_key_down,
						&key
					);
				}
			}

			/*-----------------------------------------------------------------
			 *
			 *	Radio activity detected?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_RADIOACT)
			{
				pInst->state |= GUI_ST_RADIO;
			}

			/*-----------------------------------------------------------------
			 *
			 *	Enter idle state?
			 *
			 *---------------------------------------------------------------*/
			if (triggers & GUI_TRG_IDLE)
			{
				deb_log("GUI: entering idle");

				/*
				 *	Don't go to idle if REG failed, because button
				 *	no longer works, so there is no way to get out of 
				 *	idle state and turn the backlight back on
				 */
				if(!(pInst->state & GUI_ST_REG_TIMEOUT)) {
					gui__dispBacklight(pInst, FALSE);
					atomic(pInst->state |= GUI_ST_IDLE;)
				}

				pInst->login = GUI_ACCESS_LEVEL0;
				gui__navigate(pInst, gui_nav_root);

			}

			/*-----------------------------------------------------------------
			 *
			 *	Current view requires painting?
			 *
			 *---------------------------------------------------------------*/
			if (pInst->view.pCurrent)
			{
				if (gui__viewInvalidated(pInst->view.pCurrent)) 
				{
					gui__viewSendMessage(
						pInst, 
						pInst->view.pCurrent, 
						gui_msg_paint, 
						NULL
					);
				}
			}

			/*-----------------------------------------------------------------
			 *
			 *	Current view has been painted and needs flushing to display?
			 *
			 *---------------------------------------------------------------*/
			if (pInst->view.pCurrent)
			{
				if (gui__viewPainted(pInst->view.pCurrent))
				{
					gui__viewClrPainted(pInst->view.pCurrent);

#ifndef NDEBUG
					gui__debug++;
#endif

					gui__dispFlush(pInst);

#ifndef NDEBUG
					gui__debug--;
#endif
				}
			}

#ifndef NDEBUG
			gui_counter++;
#endif

			firstcycle = FALSE;

			osa_signalGet();
		}
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		SW timer callback.
*
* \param		pTimer		ptr to the expired timer
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__timer(
	swtimer_Timer_st *		pTimer
) {
	gui__Inst *				pInst = (gui__Inst*)pTimer->pUtil;	

	if (pInst->idle_cnt != 0xFFFF && pInst->status_act != SUP_REP_WATER_MANUAL_ENABLED)
	{
		if(pInst->backlight_time)
		{
			if (++pInst->idle_cnt > pInst->backlight_time)
			{
				/*
				 *	Idle delay expired.
				 */
				pInst->trigger |= GUI_TRG_IDLE;
				pInst->idle_cnt = 0xFFFF;
				pInst->backlight_time = 0;
			}
		}
		else
		{
			if (++pInst->idle_cnt > pInst->backlight_del)
			{
				/*
				 *
				 *	Idle delay expired.
				 */
				pInst->trigger |= GUI_TRG_IDLE;
				pInst->idle_cnt = 0xFFFF;
			}
		}
	}

	pInst->trigger |= GUI_TRG_REFRESH;

	osa_signalSend(&pInst->task);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Reads trigger variable and clears it.
*
* \param		pInst		ptr to gui instance
*
* \return		Uint8, read trigger values.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint16	gui__getTriggers(
	gui__Inst *				pInst	
) {
	Uint16					triggers;

	DISABLE;
	triggers = THIS->trigger;
	THIS->trigger = 0;
	ENABLE;

	return triggers;
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handler function for popup view showing join enabled.
*
*	\param		pInst	Ptr to GUI instance.
*
*	\return		TRUE if it is ok to close the view, else FALSE.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__exitJoinEnabled(
	gui__Inst *				pInst	
) {
	Uint8					value = 0;

	reg_put(&value, gui_joinEnable);

	return TRUE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handler function for popup view showing force start.
*
*	\param		pInst	Ptr to GUI instance.
*
*	\return		TRUE if it is ok to close the view, else FALSE.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean gui__exitForceStart(
	gui__Inst *				pInst
) {
	Uint8					value = 0;

	reg_put(&value, gui_ForceStart);

	return TRUE;
}
