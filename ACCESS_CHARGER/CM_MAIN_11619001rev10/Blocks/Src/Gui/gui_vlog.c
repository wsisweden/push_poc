/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Log view handling.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"
#include "cai_cc.h"

#include "Cm.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define GUI_LV_MAX_ALARMS	20

#define GUI_LV_NO_START		0
#define GUI_LV_NO_LEN		5
#define GUI_LV_DESC_START	(GUI_LV_NO_START + GUI_LV_NO_LEN)
#define GUI_LV_DESC_LEN		(21 - GUI_LV_DESC_START)

#define GUI_LV_FIRST_LINE	2
#define GUI_LV_PER_PAGE		((8 - GUI_LV_FIRST_LINE) / 3)

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	sys_TxtHandle			text;		/**< Text to show					*/
	Uint8					code;		/**< Error code						*/
} gui__AlarmsEvent;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gui__viewLogDrawHeaders(gui__Inst * pInst, gui__LogView * pV);
PRIVATE void gui__viewLogDrawPage(gui__Inst * pInst, gui__LogView * pV);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE gui__AlarmsEvent gui__logAlarms[GUI_LV_MAX_ALARMS] = 
{
	{0}
};


/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Log view message handler.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to process view
* \param		msg			view message id
* \param		pArgs		ptr to message args
*
* \return		Int8
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewLogProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__LogView *			pV = (gui__LogView *)pView;

	switch (msg)
	{
	/*
	 *	Create message.
	 */
	case gui_msg_create:
		break;

	/*
	 *	Initialization message. Builds alarm index.
	 */
	case gui_msg_init:
		{
			Uint16			tmp;
			Uint8			idx;

			/*
			 *	Get chalg errors.
			 */
			reg_get(&tmp, pInst->nav.pTable->pItems[1].reg);

			idx = 0;
			pV->items = 0;

			while (tmp)
			{
				if (tmp & 0x01)
				{
					gui__AlarmsEvent * pAlarm = &gui__logAlarms[pV->items];
					
					pAlarm->text = gui__dataChalgErrorText(pInst, idx, &pAlarm->code);

					pV->items++;
				}

				tmp >>= 1;
				idx++;
			}

			/*
			 *	Get regu errors.
			 */
			{
				chargerAlarm_type ReguError;	//#JJ slave alarm

				reg_get(&ReguError.Sum, pInst->nav.pTable->pItems[0].reg);

				idx = 0;

				while (ReguError.Sum)
				{
					if (ReguError.Sum & 0x01)
					{
						gui__AlarmsEvent * pAlarm = &gui__logAlarms[pV->items];

						pAlarm->text = gui__dataReguErrorText(pInst, idx, &pAlarm->code);

						pV->items++;
					}

					ReguError.Sum >>= 1;
					idx++;
				}
			}

			pV->first = 0;
			pV->page = 0;

			/*
			 *	Calculate max page
			 */
			if (pV->items)
			{
				tmp = pV->items / GUI_LV_PER_PAGE;

				if (pV->items % GUI_LV_PER_PAGE)
				{
					tmp++;
				}

				pV->max_page = tmp - 1;
			}
			else
			{
				pV->max_page = 0;
		}
			
		}
		break;

	/*
	 *	View closing.
	 */
	case gui_msg_close:
		break;

	/*
	 *	View timer timed out.
	 */
	case gui_msg_timeout:
		{
		}
		break;

	/*
	 *	Paint message.
	 */
	case gui_msg_paint:
		{
			gui__dispClear(pInst);

			gui__viewLogDrawHeaders(pInst, pV);
			gui__viewLogDrawPage(pInst, pV);

			gui__viewSetPainted(pView);
		}
		break;

	/*
	 *	Key down event
	 */
	case gui_msg_key_down:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_UP:
			case GUI_VKEY_LEFT: /* Go to prev logs entries */
				{
					if (pV->page)
					{
						pV->first -= GUI_LV_PER_PAGE;
							pV->page--;
							gui__viewInvalidate(pView);
						}
					}
				break;
			case GUI_VKEY_DOWN:
			case GUI_VKEY_RIGHT: /* Go to next logs entries */
				{
					if (pV->page < pV->max_page)
					{
						pV->first += GUI_LV_PER_PAGE;
							pV->page++;
							gui__viewInvalidate(pView);
						}
					}
				break;

			case GUI_VKEY_ESC: /* Navigate out */
//			case GUI_VKEY_UP:
//			case GUI_VKEY_DOWN:
			case GUI_VKEY_OK:
			default:
				break;
			}
		}
		break;

	/*
	 *	Key up event
	 */
	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_ESC: /* Navigate out */
				if (gui__navigate(pInst, gui_nav_out) == GUI_NAV_OK)
				{
					gui__viewInvalidate(pView);
				}
				break;

			case GUI_VKEY_LEFT: 
			case GUI_VKEY_RIGHT: 
			case GUI_VKEY_UP:
			case GUI_VKEY_DOWN:
			case GUI_VKEY_OK:
			default:
				break;
			}
		}
		break;

	default:
		break;
	}

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws constant data of the view.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewLogDrawHeaders(
	gui__Inst *				pInst,
	gui__LogView *			pV
) {
	Uint8					len;

	/*
	 *	Draw page header
	 */
	gui__fontSelect(pInst, GUI_FNT_8x8);

	gui__dispSetTextPos(pInst, 0, 0);
	len = gui__textWrite(pInst, pInst->nav.pTable->header);		
	gui__dispDrawText(pInst, pInst->text.pCurrent, len);

	/*
	 *	Draw page count
	 */
	if (pV->max_page)
	{
		len = gui__u32toa(pV->page + 1, pInst->text.pCurrent, 3);
		pInst->text.pCurrent[len++] = '/';
		len += gui__u32toa(
			pV->max_page + 1, 
			&pInst->text.pCurrent[len], 
			3
		);

		gui__dispSetTextPos(pInst, pInst->disp.chr_per_line - len, 0);
		gui__dispDrawText(pInst, pInst->text.pCurrent, len);
	}

	/*
	 *	Draw log headers
	 */
	gui__fontSelect(pInst, GUI_FNT_6x8);

	gui__dispSetTextPos(pInst, GUI_LV_NO_START, 1);
	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tCode));		
	gui__dispDrawText(pInst, pInst->text.pCurrent, len);

	gui__dispSetTextPos(pInst, GUI_LV_DESC_START, 1);

	len = gui__textWrite(pInst, txt_localHandle(pInst->instid, txt_i_tDescription));		
	gui__dispDrawText(pInst, pInst->text.pCurrent, len);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws log items.
*
*	\param		pInst		ptr to GUI instance
*	\param		pV			ptr to view
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewLogDrawPage(
	gui__Inst *				pInst,
	gui__LogView *			pV
) {
	Uint8					row = GUI_LV_FIRST_LINE;
	Uint8					item = pV->first;
	Uint8					count;
	display_Rect			rect;

	gui__fontSelect(pInst, GUI_FNT_6x8);

	count = pV->items - pV->first;

	if (count > GUI_LV_PER_PAGE)
	{
		count = GUI_LV_PER_PAGE;
	}

	rect.x = GUI_LV_DESC_START * pInst->disp.pFont->width;
	rect.width = GUI_LV_DESC_LEN * pInst->disp.pFont->width;
	rect.height = pInst->disp.pFont->height * 3;

	while (count--)
		{
			Uint8			len;

			/*
		 *	Write index.
			 */
			gui__dispSetTextPos(pInst, GUI_LV_NO_START, row);

			len = gui__u32toa(
			gui__logAlarms[item].code, 
				pInst->text.pCurrent, 
				GUI_LV_NO_LEN
			);

			gui__dispDrawText(pInst, pInst->text.pCurrent, len);

			/*
		 *	Write desc.
			 */
		rect.y = row * pInst->disp.pFont->height;
	
		len = gui__textWrite(pInst, gui__logAlarms[item].text);

		gui__dispDrawTextAlignedEx(
			pInst, 
			pInst->text.pCurrent, 
			len, 
			&rect, 
			GUI_ALIGN_LEFT
		);

		row += 3;
		item++;
	}	
}

