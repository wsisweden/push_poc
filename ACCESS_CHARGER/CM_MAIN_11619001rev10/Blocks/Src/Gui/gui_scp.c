/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Charger selectedPageRow view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

#include "../Chalg/inc_cm3/caiNames.h"
#include "gui_callbackMenu.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define FIRST_ITEM	1
#define PAGE_ITEMS	8

#define EDITABLE_ITEMS (ITEM1|ITEM2|ITEM3|ITEM4|ITEM5|ITEM6|ITEM7|ITEM8)
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

static void DrawItems(gui__Inst * pInst, const gui__CallbackView * pView);
static void DrawRow(gui__Inst* pInst, int item, int edited, int key);
static void drawEditedField(gui__Inst* pInst, gui__VKey key, int decimals, int digits);
static void drawTimeEditedField(gui__Inst* pInst, gui__VKey key);
static int countDigits(uint32_t value);

/* Call back functions */
static void callback_init(gui__CallbackView* pV, gui__Inst * pInst);
static void callback_paint(gui__Inst * pInst, const gui__CallbackView*);
static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key);
static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView);
static int callback_EndEdit(gui__Inst* pInst, int item);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/
volatile int gUserParamsEdited;
/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static unsigned char subHeaderTxt[16] = {0};
static int posX = 0;																						// Initialize position marker
static UserParam_ConstParam_Type* UserParameters;
static callbackMenu_type callbackParameters =
{
	subHeaderTxt,
	0,
	PAGE_ITEMS - FIRST_ITEM,
	callback_init,
	callback_paint,
	callback_keydown,
	callback_StartEdit,
	callback_EndEdit,
	GUI_PRM_F_NONE,
	EDITABLE_ITEMS,
};

static struct{
	int32_t Min;
	int32_t Value;
	int32_t Max;
} Edited = {0, 0, 0};

static struct{
	int32_t Min;
	int32_t Value;
	int32_t Max;
	uint8_t pos;
	uint8_t hour;
	uint8_t minute;
} TimeEdit = {0, 0, 0, 0, 0, 0};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



PUBLIC Int8 gui__viewScpProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__callbackMenuNavigate(pInst, pView, msg, pArgs, &callbackParameters);

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
static void drawEditedField(
	gui__Inst *				pInst,
	gui__VKey key,
	int decimals,
	int digits
) {

	{
		int step = 1;
		{
			int n;
			for(n = 0; n < posX + 2 - decimals; n++){
				step *= 10;
			}
		}

		switch(key){
		case GUI_VKEY_UP :
			Edited.Value += step;
			if(Edited.Value > Edited.Max){
				Edited.Value = Edited.Max;
			}
			break;
		case GUI_VKEY_DOWN :
			Edited.Value -= step;
			if(Edited.Value < Edited.Min){
				Edited.Value = Edited.Min;
			}
			break;
		case GUI_VKEY_LEFT :
			if(posX < digits - 1){
				posX++;
			}
			break;
		case GUI_VKEY_RIGHT :
			if(posX){
				posX--;
			}
			break;
		}
	}

	int step = 1;
	{
		int n;
		for(n = 0; n < 2 - decimals; n++){
			step *= 10;
		}
	}

	dispPrintf(pInst, Edited.Value/step, decimals, posX, digits);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*
*	\return		-
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void drawTimeEditedField(
	gui__Inst *				pInst,
	gui__VKey key
) {

	int step = 1;

	switch(key){
	case GUI_VKEY_UP :
		if(TimeEdit.pos == GUI_EDIT_HOUR){
			TimeEdit.hour += step;

			if(TimeEdit.hour > 99){
				TimeEdit.hour = 99;
			}
		}
		else if(TimeEdit.pos == GUI_EDIT_MIN){
			TimeEdit.minute += step;

			if(TimeEdit.minute > 59){
				TimeEdit.minute = 0;
				TimeEdit.hour += step;
			}
		}

		{
			TimeEdit.Value = (int32_t)(TimeEdit.hour * 3600 + TimeEdit.minute * 60);

			if(TimeEdit.Value > TimeEdit.Max){
				TimeEdit.Value = TimeEdit.Max;
				TimeEdit.hour = (uint8_t)(TimeEdit.Value / 3600);
				TimeEdit.minute = (uint8_t)((TimeEdit.Value % 3600) / 60);
			}
		}
		break;
	case GUI_VKEY_DOWN :
		if(TimeEdit.pos == GUI_EDIT_HOUR){
			if(TimeEdit.hour > 0){
				TimeEdit.hour -= step;
			}
			else{
				TimeEdit.hour = 0;
			}
		}
		else if(TimeEdit.pos == GUI_EDIT_MIN){
			if(TimeEdit.minute > 0){
				TimeEdit.minute -= step;
			}
			else{
				if(TimeEdit.hour){
					TimeEdit.minute = 59;
					TimeEdit.hour -= step;
				}
				else{
					TimeEdit.minute = 0;
				}
			}
		}

		{
			TimeEdit.Value = (int32_t)(TimeEdit.hour * 3600 + TimeEdit.minute * 60);

			if(TimeEdit.Value < TimeEdit.Min){
				TimeEdit.Value = TimeEdit.Min;
				TimeEdit.hour = (uint8_t)(TimeEdit.Value / 3600);
				TimeEdit.minute = (uint8_t)((TimeEdit.Value % 3600) / 60);
			}
		}
		break;
	case GUI_VKEY_LEFT :
		TimeEdit.pos = GUI_EDIT_HOUR;
		break;
	case GUI_VKEY_RIGHT :
		TimeEdit.pos = GUI_EDIT_MIN;
		break;
	}

	dispTimePrintf(pInst, TimeEdit.hour, TimeEdit.minute, TimeEdit.pos);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		idx			row index (0 = first editable item)
*	\param		invert		if true the row is drawn inverted
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawRow(
	gui__Inst *				pInst,
	int item,
	int edited,
	int key
) {
	float value;
	const int unit_len = unitNameLen(UserParameters[item].Unit);
	int digits = countDigits((uint32_t)UserParameters[item].Max);
	int decimals;
	int number_len;

	reg_aGet(&value, gui__UserParam, item);

	if((UserParameters[item].Unit == UserParam_Unit_Capacity) ||
	   (UserParameters[item].Unit == UserParam_Unit_Percent))
	{
		value *= 1000;
		digits = countDigits((uint32_t)(UserParameters[item].Max * 1000));
		decimals = 1;
		number_len = digits + decimals;
	}
	else
	{
		if(digits < 2){																									// Smaller than 10 ?
			decimals = 2;
			value *= 100;
		}
		else if(digits < 3){																							// At least 10 and smaller than 100 ?
			decimals = 1;
			value *= 10;
		}
		else{
			decimals = 0;
		}

		if(digits < 3){																									// Smaller than 100 ?
			digits = 3;
			number_len = 4;
		}
		else{
			number_len = digits;
		}

	}

	{
		const int label_len = UserParameters[item].NameSize - 1;

		gui__dispDrawText(pInst, (Uint8 const_P *)UserParameters[item].Name, label_len);							// Print name

		{
			int n;
			const int padding = 22 - label_len - unit_len - number_len - 1;

			for(n = 0; n < padding; n++){
				char space = ' ';
				gui__dispDrawText(pInst, (Uint8 const_P *)&space, 1);												// Print space
			}
		}
	}

	if((UserParameters[item].Unit == UserParam_Unit_Minute) ||
	   (UserParameters[item].Unit == UserParam_Unit_Hour))
	{
		if(edited){
			drawTimeEditedField(pInst, key);
		}
		else{
			int32_t seconds = (int32_t)(value + 0.5);
			if(seconds < UserParameters[item].Min){
				seconds = (int32_t)(UserParameters[item].Min + 0.5);
			}
			else if(seconds > UserParameters[item].Max){
				seconds = (int32_t)(UserParameters[item].Max + 0.5);
			}
			uint8_t hour = (uint8_t)(seconds / 3600);
			uint8_t minute = (uint8_t)((seconds % 3600) / 60);

			dispTimePrintf(pInst, hour, minute, GUI_EDIT_NONE);														// Print formatted number
		}
	}
	else
	{
		if(edited){
			drawEditedField(pInst, key, decimals, digits);
		}
		else{
			dispPrintf(pInst, (int)(value + 0.5), decimals, -1, digits);											// Print formatted number
		}
	}
	gui__dispDrawText(pInst, (Uint8 const_P *)unitName(UserParameters[item].Unit), unit_len);						// Print unit
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws other than header part of the view (CAN items).
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void DrawItems(
	gui__Inst *				pInst,
	const gui__CallbackView *		pView
) {
	const int pageOffset = callbackParameters.items_per_page*pView->selectedPage;
	int item;

	for (item = 0; item < PAGE_ITEMS - FIRST_ITEM && pageOffset + item < callbackParameters.items; item++)
	{
		const int edited = pView->base.base.state & GUI_MV_ST_EDITING;
		const int atRow = item == pView->selectedPageRow;

		gui__dispSetTextPos(pInst, 0, FIRST_ITEM + item);

		if(atRow && !edited){													// Pointer at row and not edited ?
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Invert to mark pointer
			DrawRow(pInst, pageOffset + item, 0, 0);							// Draw row
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Remove invert
		}
		else{																	// Pointer not at row or edited
			DrawRow(pInst, pageOffset + item, edited && atRow, 0);				// Draw row
		}
	}
}

static void callback_init(gui__CallbackView* pV, gui__Inst * pInst){
	extern const AlgDef_ConstPar_Type* volatile algorithm;
	if(algorithm){
		UserParameters = (UserParam_ConstParam_Type* )((Mandatory_ConstPar_Type* )algorithm->Mandatory_ConstPar_Addr)->UserParam.Addr;
		callbackParameters.items = ((Mandatory_ConstPar_Type* )algorithm->Mandatory_ConstPar_Addr)->UserParam.Size;
	}
	else{
		callbackParameters.items = 0;
	}
	/* Reset row selected */
	pV->selectedPage = 0;
	pV->selectedPageRow = 0;
	pV->selectedItem = 0;
	return;
}

static void callback_paint(gui__Inst * pInst, const gui__CallbackView* pView){
	DrawItems(pInst, pView);
}

static void callback_keydown(gui__Inst * pInst, const gui__CallbackView* pView, gui__VKey key){
	if(callbackParameters.items){
		const int pageOffset = callbackParameters.items_per_page*pView->selectedPage + pView->selectedPageRow;

		DrawRow(pInst, pageOffset, 1, key);						// Draw row
		gui__viewInvalidate(pView);
	}
}

static int callback_StartEdit(gui__Inst* pInst, int item, gui__CallbackView* pView){
	if(gui__navCheckAccess(pInst, TRUE)){
		float Parameter;

		reg_aGet(&Parameter, gui__UserParam, item);

		if((UserParameters[item].Unit == UserParam_Unit_Minute) ||
		   (UserParameters[item].Unit == UserParam_Unit_Hour))
		{
			TimeEdit.Value = (int32_t)(Parameter + 0.5);
			TimeEdit.hour = (uint8_t)(TimeEdit.Value / 3600);
			TimeEdit.minute = (uint8_t)((TimeEdit.Value % 3600) / 60);
			TimeEdit.Min = (int32_t)(UserParameters[item].Min + 0.5);
			TimeEdit.Max = (int32_t)(UserParameters[item].Max + 0.5);
			TimeEdit.pos = GUI_EDIT_HOUR;

		}
		else if((UserParameters[item].Unit == UserParam_Unit_Capacity) ||
				(UserParameters[item].Unit == UserParam_Unit_Percent))
		{
			Edited.Value = (int32_t)(Parameter*10000.0 + 0.5);

			Edited.Min = (int32_t)(UserParameters[item].Min*10000.0 + 0.5);
			Edited.Max = (int32_t)(UserParameters[item].Max*10000.0 + 0.5);
		}
		else{
			Edited.Value = (int32_t)(Parameter*100.0 + 0.5);

			Edited.Min = (int32_t)(UserParameters[item].Min*100.0 + 0.5);
			Edited.Max = (int32_t)(UserParameters[item].Max*100.0 + 0.5);
		}
		posX = 0;																								// Reset position marker
		return 1;
	}
	return 0;
}

static int callback_EndEdit(gui__Inst* pInst, int item){
	if((UserParameters[item].Unit == UserParam_Unit_Minute) ||
	   (UserParameters[item].Unit == UserParam_Unit_Hour))
	{
		float Parameter = (float)TimeEdit.Value;
		reg_aPut(&Parameter, gui__UserParam, item);
	}
	else if((UserParameters[item].Unit == UserParam_Unit_Capacity) ||
			(UserParameters[item].Unit == UserParam_Unit_Percent))
	{
		float Parameter = Edited.Value/10000.0;
		reg_aPut(&Parameter, gui__UserParam, item);
	}
	else{
		float Parameter = Edited.Value/100.0;
		reg_aPut(&Parameter, gui__UserParam, item);
	}
	gUserParamsEdited = TRUE;
	return 1;
}

static int countDigits(uint32_t value){
	uint32_t comparator = 10;
	int n;

	for(n = 0; n < 10; n++){
		if(value < comparator){
			return n + 1;
		}
		else{
			comparator *= 10;
		}
	}
	return 10 + 1;
}
