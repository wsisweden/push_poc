/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		CAN network setup view.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * 	\name	CAN network view configuration.
 */
/*@{*/
#define GUI__CAN_FIRST_ITEM	1			/**< Index of first item			*/
#define GUI__CAN_ITEMS		6			/**< Total number of items			*/
#define GUI__CAN_PER_PAGE	6			/**< Items per page					*/

#define GUI__CAN_HDR2_Y		1
#define GUI__CAN_FIRST_ROW	(GUI__CAN_HDR2_Y + 1)			

#define GUI__CAN_CTRL_LEFT	0
#define GUI__CAN_CTRL_WIDTH	5
#define GUI__CAN_DEV_LEFT	(GUI__CAN_CTRL_LEFT + GUI__CAN_CTRL_WIDTH)
#define GUI__CAN_DEV_WIDTH	9
#define GUI__CAN_SN_LEFT	(GUI__CAN_DEV_LEFT + GUI__CAN_DEV_WIDTH)
#define GUI__CAN_SN_WIDTH	(21 - GUI__CAN_SN_LEFT)
/*@}*/

/**
 * 	\name	Editing modes.
 */
/*@{*/
#define GUI__CAN_EDIT_CTRL	0			/**< Editing ctrl part				*/
#define GUI__CAN_EDIT_SN	1			/**< Editing SN part				*/
/*@}*/


/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gui__viewCanDrawHeader(gui__Inst * pInst, gui__CanView * pV);
PRIVATE void gui__viewCanDrawItems(gui__Inst * pInst, gui__CanView * pV);

PRIVATE void gui__viewCanStartEdit(gui__Inst * pInst, gui__MultiViewBase * pV);
PRIVATE void gui__viewCanEndEdit(gui__Inst * pInst, gui__MultiViewBase * pV, Boolean cancel);
PRIVATE void gui__viewCanEnterField(gui__Inst * pInst, gui__MultiViewBase * pView, Uint8 field);
PRIVATE void gui__viewCanExitField(gui__Inst * pInst, gui__MultiViewBase * pView, Uint8 field);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/** 
 *	Network setup item texts.
 */
PRIVATE sys_TxtHandle gui__viewCanTexts[] =
{
	txt_i_tSlave1,
	txt_i_tSlave2,
	txt_i_tSlave3,
	txt_i_tSlave4,
	txt_i_tSlave5,
	txt_i_tIO
};

/** 
 *	CAN view definition for the multiview base.
 */
PRIVATE gui__MultiViewInit gui__viewCanInit =
{
	GUI__CAN_ITEMS,					/* Total number of items				*/
	GUI__CAN_PER_PAGE,				/* Max items per page					*/
	2,								/* Number of edit fields per row		*/
	0,								/* Max page index						*/
	gui__viewCanStartEdit,			/* Start edit function ptr				*/
	gui__viewCanEndEdit,			/* End edit function ptr				*/
	gui__viewCanEnterField,			/* Enter field function ptr				*/
	gui__viewCanExitField			/* Exit field function ptr				*/

};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		CAN network setup view message handler.
*
* \param		pInst		ptr to gui instance
* \param		pView		ptr to process view
* \param		msg			view message id
* \param		pArgs		ptr to message args
*
* \return		Int8
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewCanProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__CanView *			pV = (gui__CanView *)pView;

	switch (msg)
	{
	/*
	 *	Create message.
	 */
	case gui_msg_create:
		break;

	/*
	 *	Initialization message.
	 */
	case gui_msg_init:
		{
			gui__viewMultiInit((gui__MultiViewBase *)pView, &gui__viewCanInit);
		}
		break;

	/*
	 *	View closing.
	 */
	case gui_msg_close:
		if (pView->state & GUI_MV_ST_EDITING)
		{
			gui__viewCanEndEdit(pInst, (gui__MultiViewBase *)pV, TRUE);
		}
		break;

	/*
	 *	View timer timed out.
	 */
	case gui_msg_timeout: 
		break;

	/*
	 *	Paint message.
	 */
	case gui_msg_paint:
		{
			gui__dispClear(pInst);

			gui__viewCanDrawHeader(pInst, pV);
			gui__viewCanDrawItems(pInst, pV);

			gui__viewSetPainted(pView);
		}
		break;

	/*
	 *	Key down event
	 */
	case gui_msg_key_down:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_UP:
				gui__viewMultiNavUp(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_DOWN:
				gui__viewMultiNavDown(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_LEFT:
				gui__viewMultiNavLeft(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_RIGHT:
				gui__viewMultiNavRight(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_ESC:
			case GUI_VKEY_OK:
			default:
				break;
			}
		}
		break;

	/*
	 *	Key up event
	 */
	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_ESC:
				gui__viewMultiNavEsc(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_OK:
				gui__viewMultiNavOk(pInst, (gui__MultiViewBase *)pView);
				break;

			case GUI_VKEY_UP:
			case GUI_VKEY_DOWN:
			case GUI_VKEY_LEFT:
			case GUI_VKEY_RIGHT:
			default:
				break;
			}
		}
		break;

	default:
		break;
	}

	return 0;
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws constant header part of the view.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanDrawHeader(
	gui__Inst *				pInst,
	gui__CanView *			pV
) {
	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	/*
	 *	Draw Header
	 */
	gui__fontSelect(pInst, GUI_FNT_8x8);

	gui__dispSetTextPos(pInst, 0, 0);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tNetworkSetup),
		0, 
		pInst->disp.chr_per_line,
		GUI_ALIGN_LEFT
	);

	if (pV->base.pInfo->max_page > 0)
	{
		BYTE *				pPos = pInst->buffer.buf2;
		Uint8				len;

		pPos += gui__u32toa(pV->base.page + 1, pPos, 3);
		*pPos++ = '/';
		pPos += gui__u32toa(pV->base.pInfo->max_page + 1, pPos, 3);

		len = pPos - pInst->buffer.buf2;

		gui__textPutBytes(
			pInst, 
			pInst->buffer.buf2, 
			pInst->disp.chr_per_line - len,
			len
		);
	}

	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);


	gui__fontSelect(pInst, GUI_FNT_6x8);

	/*
	 *	Draw 'Ctrl'
	 */
	gui__dispSetTextPos(pInst, GUI__CAN_CTRL_LEFT, GUI__CAN_HDR2_Y);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tCtrl),
		0, 
		GUI__CAN_CTRL_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CAN_CTRL_WIDTH);

	/*
	 *	Draw 'Device'
	 */
	gui__dispSetTextPos(pInst, GUI__CAN_DEV_LEFT, GUI__CAN_HDR2_Y);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tDevice),
		0, 
		GUI__CAN_DEV_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CAN_DEV_WIDTH);

	/*
	 *	Draw 'S/N'
	 */
	gui__dispSetTextPos(pInst, GUI__CAN_SN_LEFT, GUI__CAN_HDR2_Y);
	gui__textWriteAligned(
		pInst,
		txt_localText(pInst->instid, txt_i_tSN),
		0, 
		GUI__CAN_SN_WIDTH,
		GUI_ALIGN_LEFT
	);
	gui__dispDrawText(pInst, pInst->text.pCurrent, GUI__CAN_SN_WIDTH);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws edited value.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pos			output position in the row
*	\param		width		field width
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Uint8 gui__viewCanDrawEdited(
	gui__Inst *				pInst,
	Uint8					pos,
	Uint8					width
) {
	Uint8					len = 0;

	while (len < pInst->edit.frmt_len)
	{
		pInst->text.pCurrent[pos++] = pInst->buffer.data.str[len++];
	}

	while (len < width)
	{
		pInst->text.pCurrent[pos++] = ' ';
		len++;
	}

	return len;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws the Ctrl column of the row.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view
*	\param		on			if TRUE the row is marked active
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanDrawCtrl(
	gui__Inst *				pInst,
	gui__CanView *			pV,
	Boolean					on
) {
	if (on)
	{
		gui__textWriteConst(pInst, (Uint8 const_P *)"[X]", 0, 3);
	}
	else
	{
		gui__textWriteConst(pInst, (Uint8 const_P *)"[ ]", 0, 3);
	}

	gui__textWriteChars(pInst, ' ', 3, GUI__CAN_CTRL_WIDTH - 3); 
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws the name column of the row.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view
*	\param		idx			item index
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanDrawName(
	gui__Inst *				pInst,
	gui__CanView *			pV,
	Uint8					idx
) {
	Uint8					len;

	idx -= GUI__CAN_FIRST_ITEM;

	if (idx < GUI__CAN_ITEMS)
	{
		len = gui__textWriteBuffer(
			txt_localHandle(pInst->instid, gui__viewCanTexts[idx]),
			&pInst->text.pCurrent[GUI__CAN_DEV_LEFT],
			GUI__CAN_DEV_WIDTH
		);
	}
	else
	{
		len = 0;
	}

	if (len < GUI__CAN_DEV_WIDTH)
	{
		gui__textWriteChars(
			pInst, 
			' ', 
			GUI__CAN_DEV_LEFT + len, 
			GUI__CAN_DEV_WIDTH - len
		); 
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws serial number part of the row.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view
*	\param		sn			serial number to draw
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanDrawSN(
	gui__Inst *				pInst,
	gui__CanView *			pV,
	Uint32					sn
) {
	Uint8					len;

	len = gui__u32toa(
		sn, 
		&pInst->text.pCurrent[GUI__CAN_SN_LEFT], 
		GUI__CAN_SN_WIDTH
	);

	if (len < GUI__CAN_SN_WIDTH)
	{
		gui__textWriteChars(
			pInst, 
			' ', 
			GUI__CAN_SN_LEFT + len, 
			GUI__CAN_SN_WIDTH - len
		); 
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws item row when it is not being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view
*	\param		idx			row index
*	\param		invert		if TRUE row should be drawn inverted
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanDrawItem(
	gui__Inst *				pInst,
	gui__CanView *			pV,
	Uint8					idx, 
	Boolean					invert
) {
	Uint8					slave_idx;
	Uint32					id;

	slave_idx = GUI__CAN_FIRST_ITEM + (pV->base.page * GUI__CAN_PER_PAGE) + idx;

	reg_aGet(&id, gui_ParallelAvail, slave_idx);

	gui__viewCanDrawCtrl(pInst, pV, (pV->ctrl & (1 << slave_idx)) ? TRUE : FALSE);
	gui__viewCanDrawName(pInst, pV, slave_idx);
	gui__viewCanDrawSN(pInst, pV, id);

	if (invert)
	{
		gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
	else
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}

	gui__dispSetTextPos(pInst, 0, GUI__CAN_FIRST_ROW + idx);
	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

	if (invert)
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws row when it is being edited.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view
*	\param		idx			row index
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanDrawEdit(
	gui__Inst *				pInst,
	gui__CanView *			pV,
	Uint8					idx
) {
	Uint8					slave_idx;
	Uint8					edit_start = 0;
	Uint32					id;

	slave_idx = GUI__CAN_FIRST_ITEM + (pV->base.page * GUI__CAN_PER_PAGE) + idx;

	reg_aGet(&id, gui_ParallelAvail, slave_idx);

	if (pV->base.edit_pos == GUI__CAN_EDIT_CTRL)
	{
		gui__viewCanDrawEdited(pInst, GUI__CAN_CTRL_LEFT, GUI__CAN_CTRL_WIDTH);
		edit_start = GUI__CAN_CTRL_LEFT;
	}
	else
	{
		gui__viewCanDrawCtrl(pInst, pV, (pV->ctrl & (1 << slave_idx)) ? TRUE : FALSE);
	}

	gui__viewCanDrawName(pInst, pV, slave_idx);

	if (pV->base.edit_pos == GUI__CAN_EDIT_SN)
	{
		gui__viewCanDrawEdited(pInst, GUI__CAN_SN_LEFT, GUI__CAN_SN_WIDTH);
		edit_start = GUI__CAN_SN_LEFT + pInst->edit.edit_pos;
	}
	else
	{
		gui__viewCanDrawSN(pInst, pV, id);
	}


	gui__dispSetTextPos(pInst, 0, GUI__CAN_FIRST_ROW + idx);

	/*
	 *	Draw text before the field being edited
	 */
	if (edit_start > 0)
	{
		gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
		gui__dispDrawText(pInst, pInst->text.pCurrent, edit_start);
	}

	/*
	 *	Draw field being edited
	 */
	gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
	gui__dispDrawText(pInst, &pInst->text.pCurrent[edit_start], pInst->edit.edit_width);
	gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
	
	/*
	 *	Draw text after the field being edited
	 */
	edit_start += pInst->edit.edit_width;

	gui__dispDrawText(
		pInst, 
		&pInst->text.pCurrent[edit_start], 
		pInst->disp.chr_per_line - edit_start
	);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws editabe item rows.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pV			ptr to the view

*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanDrawItems(
	gui__Inst *				pInst,
	gui__CanView *			pV
) {
	int						item;

//	reg_aGet(&pV->ctrl, gui_ParallelSelect, 0);
	reg_get(&pV->ctrl, gui_ParallelSelect);

	for (item = 0; item < GUI__CAN_PER_PAGE; item++)
	{
		if ((pV->base.base.state & GUI_MV_ST_EDITING) && (item == pV->base.selection))
		{
			/*
			 *	Line is being edited.
			 */
			gui__viewCanDrawEdit(pInst, pV, item);
		}
		else
		{
			/*
			 *	Normal line.
			 */
			gui__viewCanDrawItem(
				pInst, 
				pV, 
				item, 
				item == pV->base.selection ? TRUE : FALSE
			);
		}	
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Starts editing.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanStartEdit(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView
) {
	Uint8					idx;

	pView->base.state |= GUI_MV_ST_EDITING;

	idx = GUI__CAN_FIRST_ITEM + (pView->page * pView->pInfo->items_per_page) + pView->selection;

//	reg_aGet(&((gui__CanView *)pView)->edit_ctrl, gui_ParallelSelect, 0);
	reg_get(&((gui__CanView *)pView)->edit_ctrl, gui_ParallelSelect);
	reg_aGet(&((gui__CanView *)pView)->edit_sn, gui_ParallelAvail, idx);

	gui__viewCanEnterField(pInst, pView, 0);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Ends editing.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		cancel		if TRUE editing is cancelled
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanEndEdit(
	gui__Inst *				pInst,
	gui__MultiViewBase *	pView,
	Boolean					cancel
) {
	if (!cancel)
	{	
		gui__viewCanExitField(pInst, pView, pView->edit_pos);
	}

	pView->base.state &= ~GUI_MV_ST_EDITING;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Field enter event function when editing. Called by the multiview
*				code handler.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		field		field index
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanEnterField(
	gui__Inst *				pInst, 
	gui__MultiViewBase *	pView, 
	Uint8					field
) {
	Uint8					idx;
	gui_MenuItem const_P *	pItem;

	idx = ((pView->page * pView->pInfo->items_per_page) + pView->selection) * 2;	

	if (field == GUI__CAN_EDIT_CTRL)
	{	
		pInst->buffer.data.u16 = ((gui__CanView *)pView)->edit_ctrl;	

		pItem = &pInst->nav.pTable->pItems[idx];
	}
	else if (field == GUI__CAN_EDIT_SN)
	{
		idx++;

		pItem = &pInst->nav.pTable->pItems[idx];

		pInst->buffer.data.u32 = ((gui__CanView *)pView)->edit_sn;	
	}
	else
	{
		pItem = NULL;
	}

	pView->edit_pos = field;

	if (pItem != NULL)
	{
		gui__editInitEx(pInst, pItem->reg, pItem->reg2, pItem->flags, pItem);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Field exit event function when editing. Called by the multiview
*				code handler.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*	\param		field		field index
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__viewCanExitField(
	gui__Inst *				pInst, 
	gui__MultiViewBase *	pView, 
	Uint8					field
) {
	if (gui__editEnd(pInst))
	{

	}
}
