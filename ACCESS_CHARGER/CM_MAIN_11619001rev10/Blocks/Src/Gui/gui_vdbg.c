/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
//#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG

PUBLIC char const_P * gui__dbgCommands[] =
{
	"Create",
	"Init",
	"Close",
	"Paint",
	"Key_Up",
	"Key_Down",
	"Tick",
	"Timeout"
};

#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

#if 0
PRIVATE BYTE gui__tst2[] =
{
	0x00, 0x00,
	0xE0, 0x07,
	0x10, 0x08,
	0x08, 0x10,
	0x64, 0x24,
	0x92, 0x48,
	0x62, 0x50,
	0x02, 0x50,
	0x02, 0x50,
	0x62, 0x50,
	0x92, 0x48,
	0x64, 0x24,
	0x08, 0x10,
	0x10, 0x08,
	0xE0, 0x07,
	0x00, 0x00
};

PRIVATE BYTE gui__tst3[] =
{
	0x01,
};
#endif

/* End of declaration module **************************************************/


PUBLIC Int8 gui__viewDbgProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__DebugView *		pDbg = (gui__DebugView *)pView;

	switch (msg)
	{
	case gui_msg_create:
		break;

	case gui_msg_init:
		{
			pDbg->font = 0;
			pDbg->pos = 0;

			gui__fontSelect(pInst, pDbg->font);

			pDbg->per_page = pInst->disp.chr_per_line * (pInst->pInit->height / pInst->disp.pFont->height);
		}
		break;

	case gui_msg_close:
		break;

	case gui_msg_paint:
		{
			Uint16 pos;
			Uint16 max;
			display_Coord x;
			display_Coord y;
			Uint8 item;

			gui__dispClear(pInst);

			max = pDbg->pos + pDbg->per_page;

			if (max > 0xFF)
			{
				max = 0xFF;
			}

			item = pInst->disp.chr_per_line;
			x = 0;
			y = 0;

			for (pos = pDbg->pos; pos < max; pos++)
			{
				gui__dispSetPos(pInst, x , y);

				pInst->pInit->disp->drawImage(
					pInst->disp.pDispInst,
					gui__fontChar(pInst, pos), 
					pInst->disp.pFont->width, 
					pInst->disp.pFont->height
				);


				if (--item == 0)
				{
					y += pInst->disp.pFont->height;
					x = 0;
					item = pInst->disp.chr_per_line;
				}
				else
				{
					x += pInst->disp.pFont->width;
				}
			}

			gui__viewSetPainted(pView);
		}
		break;

	case gui_msg_key_down:
		break;

	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_UP:
				{
					if (pDbg->font < 5)
					{
						pDbg->font++;
						pDbg->pos = 0;

						gui__fontSelect(pInst, pDbg->font);
						pDbg->per_page = pInst->disp.chr_per_line * (pInst->pInit->height / pInst->disp.pFont->height);

						gui__viewInvalidate(pView);
					}
				}
				break;

			case GUI_VKEY_DOWN:
				{
					if (pDbg->font > 0)
					{
						pDbg->font--;
						pDbg->pos = 0;

						gui__fontSelect(pInst, pDbg->font);
						pDbg->per_page = pInst->disp.chr_per_line * (pInst->pInit->height / pInst->disp.pFont->height);

						gui__viewInvalidate(pView);
					}
				}
				break;

			case GUI_VKEY_LEFT:
				{
					if (pDbg->pos > pDbg->per_page)
					{
						pDbg->pos -= pDbg->per_page;
					}
					else
					{
						pDbg->pos = 0;
					}

					gui__viewInvalidate(pView);
				}
				break;

			case GUI_VKEY_RIGHT:
				{
					Uint16 pos;

					pos = pDbg->pos + pDbg->per_page;

					if (pos < 0xFF)
					{
						pDbg->pos = pos;
						gui__viewInvalidate(pView);
					}
				}
				break;

			case GUI_VKEY_OK:
			case GUI_VKEY_ESC:
			default:
				break;
			}
		}
		break;

	default:
		break;
	}

	return 0;
}
