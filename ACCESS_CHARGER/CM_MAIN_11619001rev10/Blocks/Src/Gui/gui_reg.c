/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		GUI / REG interface functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "real.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"

#include "txt.h"


#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef sys_TxtHandle (*				gui__regGetTextFn)(gui_RegInfo * pInfo);

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE sys_TxtHandle gui__regNoneText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regIntText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regRealText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regMaskText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regStrText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regEnumText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regDateText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regTimeText(gui_RegInfo * pInfo);
PRIVATE sys_TxtHandle gui__regDateTimeText(gui_RegInfo * pInfo);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE gui__regGetTextFn gui__regGetTextTable[] = 
{
	gui__regNoneText,
	gui__regIntText,
	gui__regRealText,
	gui__regMaskText,
	gui__regStrText,
	gui__regEnumText,
	gui__regDateText,
	gui__regTimeText,
	gui__regDateTimeText,
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets value from register.
*
* \param		pInst		ptr to GIU instance
* \param		handle		register handle
* \param		index		array index
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__regGet(
	gui__Inst *				pInst,
	sys_RegHandle			handle,
	Uint8					index,
	void *					pSecondary
) {
	reg_StatType			stat;
	
	reg_stat(handle, &stat);

	switch (stat.storage)
	{
	case 1:
		reg_aGet(&pInst->buffer.reg.u8, handle, index);
		if (pSecondary)
		{
			*(Uint8 *)pSecondary = pInst->buffer.reg.u8;
		}
		break;

	case 2:
		reg_aGet(&pInst->buffer.reg.u16, handle, index);
		if (pSecondary)
		{
			*(Uint16 *)pSecondary = pInst->buffer.reg.u16;
		}
		break;

	case 4:
		reg_aGet(&pInst->buffer.reg.u32, handle, index);
		if (pSecondary)
		{
			*(Uint32 *)pSecondary = pInst->buffer.reg.u32;
		}
		break;

	case 8:
		reg_aGet(&pInst->buffer.reg.u64, handle, index);
		if (pSecondary)
		{
			*(Uint64 *)pSecondary = pInst->buffer.reg.u64;
		}
		break;

	default:
		reg_aGet(pInst->buffer.reg.str, handle, index);
		break;
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Writes value to register. Data is assumed to be in the editing
*				register.
*
* \param		pInst		ptr to GIU instance
* \param		handle		register handle
* \param		index		array index
*
* \return		TRUE if write was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__regPut(
	gui__Inst *				pInst,
	sys_RegHandle			handle,
	Uint8					index
) {
	reg_StatType			stat;
	reg_Status				ret;
	
	reg_stat(handle, &stat);

	switch (stat.storage)
	{
	case 1:
		ret = reg_aPut(&pInst->buffer.reg.u8, handle, index);
		break;

	case 2:
		ret = reg_aPut(&pInst->buffer.reg.u16, handle, index);
		break;

	case 4:
		ret = reg_aPut(&pInst->buffer.reg.u32, handle, index);
		break;

	default:
		deb_assert(FALSE);
		ret = REG_OUT_RANGE;
		break;
	}

	return (ret == REG_OK);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Writes value to register. Valuue pointer is given in the call.
*
*	\param		pInst		ptr to GIU instance
*	\param		handle		register handle
*	\param		index		array index
*	\param		pData		ptr to data buffer
*
*	\return		TRUE if write was succesful.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Boolean gui__regPutBuffer(
	gui__Inst *				pInst,
	sys_RegHandle			handle,
	Uint8					index,
	void *					pData
) {
	return (reg_aPut(pData, handle, index) == REG_OK);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets register info.
*
* \param		handle		register handle
* \param		pInfo		ptr to register info
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__regGetInfo(
	sys_RegHandle			handle,
	gui_RegInfo *			pInfo
) {
	pInfo->reg_type = reg_mmiStat(handle) & REG_MMI_TYPE_MASK;

	if (pInfo->reg_type != SYS_MMI_T_NONE)
	{
		pInfo->pInfo = reg_mmiAttrib(handle);
	}
	else
	{
		pInfo->pInfo = NULL;
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with a register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC sys_TxtHandle gui__regGetText(
	gui_RegInfo *			pInfo
) {	
	deb_assert(pInfo->reg_type < dim(gui__regGetTextTable));

	return gui__regGetTextTable[pInfo->reg_type](pInfo);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Dummy text retrieval function.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regNoneText(
	gui_RegInfo *			pInfo
) {	
	return GUI_NO_TEXT;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with an INT register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regIntText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrInt const_P *)pInfo->pInfo)->descr;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with a REAL register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.

*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regRealText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrReal const_P *)pInfo->pInfo)->descr;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with a MASK register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.

*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regMaskText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrMask const_P *)pInfo->pInfo)->descr;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with a sTRING register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.

*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regStrText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrStr const_P *)pInfo->pInfo)->descr;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with an ENUM register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.

*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regEnumText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrEnum const_P *)pInfo->pInfo)->descr;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with a TIME register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.

*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regTimeText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrTime const_P *)pInfo->pInfo)->descr;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with a DATE register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.

*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regDateText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrDate const_P *)pInfo->pInfo)->descr;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets text handle associated with a DATETIME register.
*
* \param		pInfo		ptr to register info
*
* \return		Register's MMI text handle.

*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE sys_TxtHandle gui__regDateTimeText(
	gui_RegInfo *			pInfo
) {	
	return ((sys_AdditAttrDT const_P *)pInfo->pInfo)->descr;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Returns register value stored in 'value' as Int64 value if 
*				possible. 
*
* \param		reg		register handle
* \param		value	value to convert
*
* \return		Converted value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int64 gui__regToInt64(
	sys_RegHandle			reg,
	gui__Types *			value
) {
	reg_StatType			stat;
	Int64					ret;

	reg_stat(reg, &stat);

	switch (stat.status & REG_TYPE_MASK)
	{
	case SYS_REG_T_Int8:
		ret = value->i8;
		break;

	case SYS_REG_T_Uint8:
		ret = value->u8;
		break;

	case SYS_REG_T_Int16:
		ret = value->i16;
		break;

	case SYS_REG_T_Uint16:
		ret = value->u16;
		break;

	case SYS_REG_T_Int32:
		ret = value->i32;
		break;

	case SYS_REG_T_Uint32:
		ret = value->u32;
		break;

	default:
		ret = 0;
		break;
	}

	return ret;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts enum index to enum text.
*
* \param		reg			register handle
* \param		enum_val	enum index
* \param		pLen		text length will be put here
* \param		text		text handle (optional)
*
* \return		Ptr to enum text.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC BYTE const_P * gui__regToEnumText(
	sys_RegHandle			reg,
	Uint8					enum_val,
	Uint8 *					pLen,
	sys_TxtHandle			text
) {
	BYTE const_P *			pText;
	BYTE const_P *			pRet;
	Uint8					len;
	Uint8					idx = 0;
	Uint8					pos = 0;

	if (text != GUI_NO_TEXT)
	{
		pText = txt_text(text);
	}
	else
	{
		sys_AdditAttrEnum const_P *	pAddit;

		pAddit = (sys_AdditAttrEnum const_P *)reg_mmiAttrib(reg);
		pText = txt_text(pAddit->enumValues);
	}

	len = gui__txtLen(pText);
	pText = gui__txtBegin(pText);

	/*
	 *	Find start position
	 */
	if (enum_val > 0)
	{
		while (pos < len)
		{
			if (pText[pos] == '\n')
			{
				idx++;

				if (idx == enum_val)
				{
					pos++;
					break;
				}
			}

			pos++;
		}
	}

	pRet = (BYTE *)&pText[pos];

	/*
	 *	Find length of the text	
	 */
	idx = 0;

	while (pos < len)
	{
		if (pText[pos] == '\n')
		{
			break;
		}

		pos++;
		idx++;
	}

	*pLen = idx;

	return pRet;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Gets register min and max values,
*
* \param		reg		registr handle
* \param		pMin	min value will be put here
* \param		pMax	max value will be put here
*
* \return		TRUE if values were found.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__regGetMinMax(
	sys_RegHandle			reg,
	Int64 *					pMin,
	Int64 *					pMax
) {
	reg_StatType			stat;
	Boolean					ret = TRUE;

	reg_stat(reg, &stat);

	switch (stat.status & REG_TYPE_MASK)
	{
	case SYS_REG_T_Int8:
		{
			Int8 const_P * min;
			Int8 const_P * max;

			min = (Int8 const_P *)reg_mmiMinMax(reg, (void const_P **)&max);

			*pMin = *min;
			*pMax = *max;
		}
		break;

	case SYS_REG_T_Uint8:
		{
			Uint8 const_P * min;
			Uint8 const_P * max;

			min = reg_mmiMinMax(reg, (void const_P **)&max);

			*pMin = *min;
			*pMax = *max;
		}
		break;

	case SYS_REG_T_Int16:
		{
			Int16 const_P * min;
			Int16 const_P * max;

			min = (Int16 const_P *)reg_mmiMinMax(reg, (void const_P **)&max);

			*pMin = *min;
			*pMax = *max;
		}
		break;

	case SYS_REG_T_Uint16:
		{
			Uint16 const_P * max;
			Uint16 const_P * min;

			min = reg_mmiMinMax(reg, (void const_P **)&max);

			*pMin = *min;
			*pMax = *max;
		}
		break;

	case SYS_REG_T_Int32:
		{
			Int32 const_P * min;
			Int32 const_P * max;

			min = (Int32 const_P *)reg_mmiMinMax(reg, (void const_P **)&max);

			*pMin = *min;
			*pMax = *max;
		}
		break;

	case SYS_REG_T_Uint32:
		{
			Uint32 const_P * max;
			Uint32 const_P * min;

			min = reg_mmiMinMax(reg, (void const_P **)&max);

			*pMin = *min;
			*pMax = *max;
		}
		break;

	default:
		deb_assert(0);
		ret = FALSE;
		break;
	}

	return ret;
}