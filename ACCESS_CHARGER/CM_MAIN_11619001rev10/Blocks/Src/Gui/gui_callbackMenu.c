/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Code for handling navigation in a view with multiple editable
*				items on the same row (time restriction / can network setup).
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"
#include "reg.h"

#include "gui.h"
#include "gui_callbackMenu.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/
#define FIRST_ITEM	1
#define FIRST_ITEM_SCS	2
#define PAGE_ITEMS	8

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

static void drawStandardHeader(gui__Inst* pInst, const gui__CallbackView* pView, const callbackMenu_type* instance);
static void NavigateOnlyUp(gui__CallbackView* pView, const callbackMenu_type* instance);
static void NavigateOnlyDown(gui__CallbackView* pView, const callbackMenu_type* instance);
static int NavigateOnlyLeft(gui__CallbackView* pView, const callbackMenu_type* instance);
static void NavigateOnlyRight(gui__CallbackView* pView, const callbackMenu_type* instance);
static void NavigateOnlyOk(gui__Inst * pInst, gui__CallbackView* pView, const callbackMenu_type* instance);
static int NavigateOnlyEsc(gui__Inst * pInst, gui__CallbackView * pView, const callbackMenu_type* instance);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
gui__CallbackView* pV;
gui__CallbackView cbStack[GUI_NAV_STACK];										/**< Callback navigation stack 		*/
Uint8 cbStack_pos = 0;															/**< Current stack position			*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/




/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes view variables.
*
*	\param		pView		ptr to the view
*	\param		pInfo		ptr to the init info
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void gui__callbackMenuInit(gui__Inst * pInst)
{
	pV->selectedPage = pInst->nav.page;
	pV->selectedPageRow = pInst->nav.item;
	pV->selectedItem = pInst->nav.item;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Updates item idexe after selectedPage has changed.
*
*	\param		pView		ptr to the view
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
void gui__selectionLimit(
		const callbackMenu_type*	instance
) {
	if(pV->selectedItem >= instance->items){											// Selected item to large ?
		pV->selectedItem = instance->items - 1;											// Set selected item to maximum allowed
	}

	{																					// Limit selected page
		int idx = 0;
		int items = 0;
		int selectedPageMax = 0;

		for(idx = 0; idx < instance->items; idx++)										// Calculate last possible page
		{
			items++;
			if(items > instance->items_per_page)
			{
				selectedPageMax++;
				items = 1;
			}
		}

		if(pV->selectedPage > selectedPageMax){											// Selected page to large ?
			pV->selectedPage = selectedPageMax;											// Set selected page maximum allowed
			pV->selectedPageRow = instance->items_per_page - 1;							// Set selected row to maximum allowed
			return;
		}
	}

	{																					// Limit selected page
		const int selectedRowMax = instance->items - pV->selectedPage*instance->items_per_page - 1;
		if(pV->selectedPageRow > selectedRowMax){										// Selected row to large?
			pV->selectedPageRow = selectedRowMax;										// Set selected row to maximum allowed
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles OK button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void NavigateOnlyOk(
		gui__Inst*					pInst,
		gui__CallbackView*			pView,
		const callbackMenu_type*	instance
) {
	const int item = instance->items_per_page*pView->selectedPage + pView->selectedPageRow;

	if (pView->base.base.state & GUI_MV_ST_EDITING)
	{
		/* Accept edit */
		if(instance->edit_end(pInst, item)){
			pView->base.base.state &= ~GUI_MV_ST_EDITING;
		}
		gui__viewInvalidate(pView);
	}
	else
	{
		/* Start editing */
		if(instance->edit_start(pInst, item, pView)){
			if(instance->flags == GUI_PRM_SINGLE){
				;	// edit right away, do not set edit flag
			}
			else{
				pView->base.base.state |= GUI_MV_ST_EDITING;
			}
		}

		gui__viewInvalidate(pView);
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles ESC button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static int NavigateOnlyEsc(
		gui__Inst*					pInst,
		gui__CallbackView*			pView,
		const callbackMenu_type*	instance
) {
	int returnCode = 0;

	if (pView->base.base.state & GUI_MV_ST_EDITING)
	{
		/* Cancel edit */
		pView->base.base.state &= ~GUI_MV_ST_EDITING;
		gui__viewInvalidate(pView);
	}
	else
	{
		Boolean popUpView = FALSE;
		gui__PopupViewArgs	args;

		if (gui_tblExit(pInst->nav.pTable))
		{
			sys_TxtHandle ret_text;
			/*
			 *	Table has exit conditions associated with it.
			 */

			ret_text = pInst->pInit->pExitFn[
				gui_tblExit(pInst->nav.pTable) - 1
			](TRUE);

			if (ret_text != GUI_NO_TEXT)
			{
				/*
				 *	Build PopUp message.
				 */
				args.delay = 30;
				args.text1 = ret_text;
				args.text2 = GUI_NO_TEXT;
				args.on_exit = NULL;

				popUpView = TRUE;
			}
		}

		/* Move back in menu */
		if(pView->base.base.state & GUI_MV_ST_SUBMENU){
			if(popUpView)
			{
				gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
			}
			else
			{
				gui__viewInvalidate(pView);
			}
			/*
			 *	Retrieve page and row from earlier view before entering it.
			 */
			pInst->nav.stack_pos--;
			pView->selectedPageRow = pInst->nav.stack[pInst->nav.stack_pos].pos;
			pView->selectedPage = pInst->nav.stack[pInst->nav.stack_pos].page;

			returnCode = 1;
		}
		else
		{
			if(popUpView)
			{
				gui__navigate(pInst, gui_nav_out);
				gui__viewOpen(pInst, GUI_VIEW_POPUP, &args);
			}
			else
			{
				if(gui__navigate(pInst, gui_nav_out) == GUI_NAV_OK)
				{
					gui__viewInvalidate(pView);
				}
			}
		}
	}

	return returnCode;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles up button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void NavigateOnlyUp(
		gui__CallbackView*			pView,
		const callbackMenu_type*	instance
) {
	Uint8	idx = pView->selectedPageRow - 1;

	do
	{
		if (pView->selectedPageRow > 0)
		{
			if (instance->editable & 1<<idx)
			{
				pView->selectedPageRow = idx;
				break;
			}
		}
		else
		{
			if(NavigateOnlyLeft(pView, instance))
			{
				for (Uint8 next = pView->selectedPageRow + 1; next < GUI_ITEMS_PER_PAGE; next++)
				{
					if (next < instance->items_per_page)
					{
						if (instance->editable & 1<<next)
						{
							if(next < instance->items){
								pView->selectedPageRow = next;
							}
						}
					}
				}
			}
			break;
		}
	} while (idx--);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles down button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void NavigateOnlyDown(
		gui__CallbackView*			pView,
		const callbackMenu_type*	instance
) {
	Uint8 	next = pView->selectedPageRow + 1;
	Uint8	idx = 1;

	for (; next < GUI_ITEMS_PER_PAGE; next++)
	{
		if (next < instance->items_per_page)
		{
			if (instance->editable & 1<<next)
			{
				if(pView->selectedItem + idx < instance->items)
				{
					pView->selectedPageRow = next;
				}
				break;
			}
			idx++;
		}
		else
		{
			NavigateOnlyRight(pView, instance);
			break;
		}
	}
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles left button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static int NavigateOnlyLeft(
		gui__CallbackView*			pView,
		const callbackMenu_type*	instance
) {
	int navOk = FALSE;

	if (pView->selectedPage > 0)
	{
		pView->selectedPage--;
		//pView->selectedPageRow = instance->items_per_page - 1;
		pView->selectedPageRow = 0;
		navOk = TRUE;
	}
	return navOk;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles right button press.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void NavigateOnlyRight(
		gui__CallbackView*			pView,
		const callbackMenu_type*	instance
) {
	if (instance->items_per_page*(pView->selectedPage + 1) < instance->items)
	{
		pView->selectedPage++;
		pView->selectedPageRow = 0;
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles navigation into menu.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
void gui__callbackMenuNavigateIn(
		gui__Inst *				pInst
) {
	pV = (gui__CallbackView *)&pInst->view.views[0];

	cbStack[cbStack_pos].selectedPage = pV->selectedPage;
	cbStack[cbStack_pos].selectedPageRow = pV->selectedPageRow;
	cbStack[cbStack_pos].selectedItem = pV->selectedItem;
	cbStack_pos++;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles navigation out of menu.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
void gui__callbackMenuNavigateOut(
		gui__Inst *				pInst
) {
	pV = (gui__CallbackView *)&pInst->view.views[0];

	cbStack_pos--;
	pV->selectedPage = cbStack[cbStack_pos].selectedPage;
	pV->selectedPageRow = cbStack[cbStack_pos].selectedPageRow;
	pV->selectedItem = cbStack[cbStack_pos].selectedItem;
}

int gui__callbackMenuNavigate(
		gui__Inst *					pInst,
		gui__View *					pView,
		gui__ViewMessage			msg,
		void *						pArgs,
		const callbackMenu_type*	instance
) {
	pV = (gui__CallbackView*)pView;

	gui__textSelectBuffer(pInst, pInst->buffer.buf1);

	switch (msg)
	{
	/*
	 *	Create message.
	 */
	case gui_msg_create:
	break;
	case gui_msg_init:
		instance->init(pV, pInst);
		gui__viewTmrStart(pView, 10, 10);
		break;
	case gui_msg_close:
		gui__viewTmrStop(pView);
		break;
	case gui_msg_timeout:
		if ((pView->state & GUI_MV_ST_EDITING) == 0)
		{
			gui__viewInvalidate(pInst->view.pCurrent);
		}
		break;
	case gui_msg_paint:
		gui__dispClear(pInst);
		instance->paint(pInst, pV);
		gui__selectionLimit(instance);
		drawStandardHeader(pInst, pV, instance);					// Wait with header until number of items is known
		gui__viewSetPainted(pView);
		break;

	case gui_msg_key_down:
		if ((pView->state & GUI_MV_ST_EDITING)){						// Editing ?
			const gui__VKey key = (*(gui__VKey *)pArgs) & GUI_VKEY_MASK;
			instance->keydown(pInst, pV, key);
		}
		else{															// Not editing, navigate
			const gui__VKey key = (*(gui__VKey *)pArgs) & GUI_VKEY_MASK;

			switch (key)
			{
			case GUI_VKEY_UP:
				NavigateOnlyUp(pV, instance);
				break;

			case GUI_VKEY_DOWN:
				NavigateOnlyDown(pV, instance);
				break;

			case GUI_VKEY_LEFT:
				NavigateOnlyLeft(pV, instance);
				break;

			case GUI_VKEY_RIGHT:
				NavigateOnlyRight(pV, instance);
				break;

			case GUI_VKEY_ESC:
			case GUI_VKEY_OK:
			default:
				break;
			}
			pV->selectedItem = pV->selectedPage*instance->items_per_page + pV->selectedPageRow;		// Update selected item
			gui__viewInvalidate(pV);
		}
		break;
	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			case GUI_VKEY_ESC:
				return NavigateOnlyEsc(pInst, pV, instance);
				break;

			case GUI_VKEY_OK:
				NavigateOnlyOk(pInst, pV, instance);
				break;

			case GUI_VKEY_UP:
			case GUI_VKEY_DOWN:
			case GUI_VKEY_LEFT:
			case GUI_VKEY_RIGHT:
			default:
				break;
			}
		}
		break;

	default:
		break;
	}

	return 0;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws header part (first two rows) of the view.
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
static void drawStandardHeader(
		gui__Inst*					pInst,
		const gui__CallbackView*	pView,
		const callbackMenu_type*	instance
) {
	int lastPage = 0;

	/*
	 *	Draw selectedPage header
	 */
	gui__fontSelect(pInst, GUI_FNT_8x8);

	gui__dispSetTextPos(pInst, 0, 0);
	if(instance->subHeaderTxt[0] == 0){
		gui__textWriteAligned(
			pInst,
			txt_text(pInst->nav.pTable->header),
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_LEFT
		);
	}
	else{
		gui__textWriteAligned(
			pInst,
			instance->subHeaderTxt,
			0,
			pInst->disp.chr_per_line,
			GUI_ALIGN_LEFT
		);
	}

	{
		int idx = 0;
		int items = 0;

		for(idx = 0; idx < instance->items; idx++)		// Calculate last possible page
		{
			items++;
			if(items > instance->items_per_page)
			{
				lastPage++;
				items = 1;
			}
		}
	}

	if (lastPage > 0)
	{
		BYTE *				pPos = pInst->buffer.buf2;
		Uint8				len;

		pPos += gui__u32toa(pView->selectedPage + 1, pPos, 3);
		*pPos++ = '/';
		pPos += gui__u32toa(lastPage + 1, pPos, 3);

		len = pPos - pInst->buffer.buf2;

		gui__textPutBytes(
			pInst,
			pInst->buffer.buf2,
			pInst->disp.chr_per_line - len,
			len
		);
	}

	gui__dispDrawText(pInst, pInst->text.pCurrent, pInst->disp.chr_per_line);

	gui__fontSelect(pInst, GUI_FNT_6x8);
}

void dispPrintf(gui__Inst* pInst, int value, int decimals, int pointerPos, int digits)
{
	const int charsToPrint = decimals ? digits + 1 : digits;
	int digitPos = 0;
	int n;

	pointerPos = digits - 1 - pointerPos;

	for(n = 0; n < charsToPrint; n++){										// For all digits and the decimal point
		if(n == digits - decimals){											// Draw decimal point ?
			gui__dispDrawText(pInst, (Uint8 const_P *)&".", 1);				// Draw decimal point
		}
		else {																// Draw digit
			char digit;
			int multiplier = 1;
			int k;

			for(k = 0; k < digits - digitPos - 1; k++){
				multiplier *= 10;
			}

			digit = value/multiplier;
			value -= digit*multiplier;
			digit += 0x30;

			if(digitPos == pointerPos){										// Draw pointer ?
				gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);			// Draw inverted text
				gui__dispDrawText(pInst, (Uint8 const_P *)&digit, 1);		// Draw digit
				gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);			// Draw normal text
			}
			else{															// Normal text no pointer
				gui__dispDrawText(pInst, (Uint8 const_P *)&digit, 1);		// Draw digit
			}
			digitPos++;														// Next digit
		}
	}
}

void dispTimePrintf(gui__Inst* pInst, uint8_t hour, uint8_t minute, uint8_t pointerPos)
{
	BYTE					pBuf[8];
	Uint8					len = 0;

	{
		if (hour < 10)
		{
			pBuf[len++] = '0';
		}

		len += gui__u32toa(hour, &pBuf[len], GUI_MAX_TXT - len);

		if(pointerPos == GUI_EDIT_HOUR){								// Draw pointer ?
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);			// Draw inverted text
			gui__dispDrawText(pInst, &pBuf[0], len);					// Draw hour
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);			// Draw normal text
		}
		else{															// Normal text no pointer
			gui__dispDrawText(pInst, &pBuf[0], len);					// Draw hour
		}

		len = 0;
		pBuf[len++] = ':';
		gui__dispDrawText(pInst, &pBuf[0], len);						// Draw separator ":"

		len = 0;
		if (minute < 10)
		{
			pBuf[len++] = '0';
		}

		len += gui__u32toa(minute, &pBuf[len], GUI_MAX_TXT - len);

		if(pointerPos == GUI_EDIT_MIN){									// Draw pointer ?
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);			// Draw inverted text
			gui__dispDrawText(pInst, &pBuf[0], len);					// Draw minute
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);			// Draw normal text
		}
		else{															// Normal text no pointer
			gui__dispDrawText(pInst, &pBuf[0], len);					// Draw minute
		}
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws other than header part of the view
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
void gui__DrawItems(
	gui__Inst *				pInst,
	const gui__CallbackView *		pView,
	void (*DrawRow)(gui__Inst* pInst, int item, int edited),
	const callbackMenu_type*	instance
) {
	const int pageOffset = (instance->items_per_page)*(pView->selectedPage);
	int item;

	for (item = 0; item < PAGE_ITEMS - FIRST_ITEM && pageOffset + item < instance->items; item++)
	{
		const int edited = pView->base.base.state & GUI_MV_ST_EDITING;
		const int atRow = item == pView->selectedPageRow;

		gui__dispSetTextPos(pInst, 0, FIRST_ITEM + item);

		if(atRow && !edited){													// Pointer at row and not edited ?
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Invert to mark pointer
			DrawRow(pInst, pageOffset + item, 0);								// Draw row
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Remove invert
		}
		else{																	// Pointer not at row or edited
			DrawRow(pInst, pageOffset + item, edited && atRow);					// Draw row
		}
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Draws other than header part of the view
*
*	\param		pInst		ptr to the GUI instance
*	\param		pView		ptr to the view
*
*	\return		-
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
void gui__DrawItemsScs(
	gui__Inst *				pInst,
	const gui__CallbackView *		pView,
	void (*DrawRow)(gui__Inst* pInst, int item, int edited),
	const callbackMenu_type*	instance
) {
	const int pageOffset = (instance->items_per_page)*(pView->selectedPage);
	int item;

	for (item = 0; item < PAGE_ITEMS - FIRST_ITEM_SCS && pageOffset + item < instance->items; item++)
	{
		const int edited = pView->base.base.state & GUI_MV_ST_EDITING;
		const int atRow = item == pView->selectedPageRow;

		gui__dispSetTextPos(pInst, 0, FIRST_ITEM_SCS + item);

		if(atRow && !edited){													// Pointer at row and not edited ?
			gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Invert to mark pointer
			DrawRow(pInst, pageOffset + item, 0);								// Draw row
			gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);					// Remove invert
		}
		else{																	// Pointer not at row or edited
			DrawRow(pInst, pageOffset + item, edited && atRow);					// Draw row
		}
	}
}

void gui_DrawSpace(gui__Inst* pInst, int len){
	const Uint8 space = ' ';
	int n;

	for(n = 0; n < len; n++){
		gui__dispDrawText(pInst, &space, 1);
	}
}
