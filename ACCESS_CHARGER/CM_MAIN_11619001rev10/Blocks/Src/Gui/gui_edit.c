/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		Parameter editing functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "real.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"
#include "txt.h"

#include "lib.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Boolean gui__editCreateMask(gui__Inst * pInst, gui_RegInfo * pInfo, gui_MenuItem const_P * pItem);

PRIVATE void gui__editCreateIntMask(gui__Inst * pInst, gui_RegInfo * pInfo);
PRIVATE void gui__editCreateEnumMask(gui__Inst * pInst, gui_RegInfo * pInfo);
PRIVATE void gui__editCreateRealMask(gui__Inst * pInst, gui_RegInfo * pInfo);
PRIVATE void gui__editCreateDateMask(gui__Inst * pInst, gui_RegInfo * pInfo);
PRIVATE void gui__editCreateTimeMask(gui__Inst * pInst, gui_RegInfo * pInfo);

PRIVATE void gui__editUpdateEnumText(gui__Inst * pInst);

PRIVATE Uint8 gui__editCalcDigits(Uint32 value);

PRIVATE Boolean gui__editMaskNavRight(gui__Inst * pInst);
PRIVATE Boolean gui__editMaskNavLeft(gui__Inst * pInst);
PRIVATE Boolean gui__editMaskNavUp(gui__Inst * pInst);
PRIVATE Boolean gui__editMaskNavDown(gui__Inst * pInst);

PRIVATE Boolean gui__editSlideNavUp(gui__Inst * pInst);
PRIVATE Boolean gui__editSlideNavDown(gui__Inst * pInst);

PRIVATE Boolean gui__editEnumNavUp(gui__Inst * pInst);
PRIVATE Boolean gui__editEnumNavDown(gui__Inst * pInst);

PRIVATE Boolean gui__editConvertToInt(gui__Inst * pInst, gui__Types * pSrc, gui__Types * pDst);
PRIVATE Boolean gui__editConvertToEnum(gui__Inst * pInst, gui__Types * pDst);
PRIVATE Boolean gui__editConvertToReal(gui__Inst * pInst, gui__Types * pSrc, gui__Types * pDst);
PRIVATE Boolean gui__editConvertToDate(gui__Inst * pInst, gui__Types * pSrc, gui__Types * pDst);
PRIVATE Boolean gui__editConvertToTime(gui__Inst * pInst, gui__Types * pSrc, gui__Types * pDst);

PRIVATE Boolean gui__editUpdateSlideValue(gui__Inst * pInst);

PRIVATE Uint8 gui__editFieldLen(BYTE chr);
PRIVATE Boolean gui__editTimeUpDown(gui__Inst * pInst, Boolean up);

PRIVATE Boolean gui__editBitToggle(gui__Inst * pInst, Uint8 bit, Boolean set);

PRIVATE void gui__editRollTime(gui__Inst * pInst, BYTE * pBuf, Uint8 pos);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 *	Special formatting characters (editable).
 */
PRIVATE BYTE gui__editFrmtChars[] =
{
	'#',
	'+',
	'A',
	'h',
	'm',
	's',
	'Y',
	'M',
	'D'
};



SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes editing data for given menu item.
*
* \param		pInst		ptr to GUI instance
* \param		pItem		ptr to menu item
*
* \return		TRUE if data was successfully initialized.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__editInit(
	gui__Inst *				pInst,
	gui_MenuItem const_P *	pItem
) {
	void *					pTmp = NULL;

	/*
	 *	If value is marked for immediate editing its value must be copied 
	 *	before editing so that it can be restored if editing is cancelled.
	 */
	if (pItem->flags & GUI_PRM_IMMEDIATE)
	{
		pTmp = &pInst->edit.original;
	}

	if (pItem->flags & GUI_PRM_ARR_IDX)
	{
		gui__regGet(pInst, pItem->reg, pItem->enumval, pTmp);
	}
	else
	{
		gui__regGet(pInst, pItem->reg, 0, pTmp);
	}

	return gui__editInitEx(pInst, pItem->reg, pItem->reg2, pItem->flags, pItem);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes editing data for given register.
*
*	\param		pInst		ptr to GUI instance
*	\param		reg			register handle
*	\param		reg2		second register handle (for dual registers)
*	\param		flags		register flags
*	\param		pItem		menu item info
*
*	\return		TRUE if data was successfully initialized.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC Boolean gui__editInitEx(
	gui__Inst *				pInst,
	sys_RegHandle			reg,
	sys_RegHandle			reg2,
	Uint32					flags,
	gui_MenuItem const_P *	pItem
) {
	gui_RegInfo				info;
	Boolean					ret;

	pInst->edit.reg = reg;
	pInst->edit.reg2 = reg2;
	pInst->edit.reg_flags = flags & GUI_PRM__F_MASK;

	gui__regGetInfo(reg, &info);

	pInst->edit.full_value = gui__regToInt64(reg, &pInst->buffer.reg);
	pInst->edit.step = 1;

	pInst->edit.edit_pos = 0;

	pInst->edit.flags = (Uint16)(flags >> GUI_PRM__F_BITS);

	pInst->edit.pItem = pItem;

	ret = gui__editCreateMask(pInst, &info, pItem);

	return ret;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Ends current editing. Converts value to register value
*				and stores it.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if value was successfully stored to REG.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__editEnd(
	gui__Inst *				pInst
) {
	Boolean					ret = FALSE;

	/*
	 *	Convert edit string to actual value.
	 */
	switch (pInst->edit.mmistat & REG_MMI_TYPE_MASK) 
	{
		case SYS_MMI_T_INT:
			ret = gui__editConvertToInt(
				pInst, 
				&pInst->edit.buffer, 
				&pInst->buffer.reg
			);
			break;

		case SYS_MMI_T_REAL:
			ret = gui__editConvertToReal(
				pInst, 
				&pInst->edit.buffer, 
				&pInst->buffer.reg
			);
			break;

		case SYS_MMI_T_ENUM:
			if (pInst->edit.reg_flags & GUI_PRM_BITMASK)
			{
				ret = TRUE;
			}
			else
			{
				ret = gui__editConvertToEnum(pInst, &pInst->buffer.reg);
			}
			break;

		case SYS_MMI_T_STR:
			break;

		case SYS_MMI_T_TIME:
			ret = gui__editConvertToTime(
				pInst, 
				&pInst->edit.buffer, 
				&pInst->buffer.reg
			);
			break;

		case SYS_MMI_T_DATE:
			ret = gui__editConvertToDate(
				pInst, 
				&pInst->edit.buffer, 
				&pInst->buffer.reg
			);
			break;

		case SYS_MMI_T_MASK:
		case SYS_MMI_T_NONE:
		default:
			break;
	}

	/*
	 *	Store the value.
	 */
	if (ret)
	{
		sys_RegHandle		handle;
		Uint8				arridx;

		if (pInst->edit.reg_flags & GUI_PRM_DBL_REG)
		{
			deb_assert((pInst->edit.reg_flags & GUI_PRM_BITMASK) == 0);
			handle = pInst->edit.reg2;
		}
		else
		{
			handle = pInst->edit.reg;
		}

		if (pInst->edit.reg_flags & GUI_PRM_BITMASK)
		{
			deb_assert(pInst->edit.pItem);
			gui__regGet(pInst, handle, 0, NULL);
			gui__editBitToggle(
				pInst, 
				pInst->edit.pItem->enumval, 
				pInst->edit.edit_pos > 0
			);
		}

		if (pInst->edit.reg_flags & GUI_PRM_ARR_IDX)
		{
			deb_assert((pInst->edit.reg_flags & GUI_PRM_BITMASK) == 0);
			arridx = pInst->edit.pItem->enumval;
		}
		else
		{
			arridx = 0;
		}

		ret = gui__regPut(pInst, handle, arridx);
	}

	return ret;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Gets min and max values of a register for editing.
*
*	\param		pInst		ptr to GUI instance
*	\param		pInfo		register info
*	\param		pItem		menu item info (may be NULL)
*
*	\return		TRUE if values were succesfully retrieved.
*
*	\details	If menu item is configured to use a register value to determine
*				max editable value it is taken into account.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__editGetMinMax(
	gui__Inst *				pInst,
	gui_RegInfo *			pInfo,
	gui_MenuItem const_P *	pItem
) {
	if (
		!gui__regGetMinMax(pInst->edit.reg, &pInst->edit.min, &pInst->edit.max)
	) {
		return FALSE;
	}

	/*
	 *	If registers max editable value is from register read it here.
	 */
	if (pItem && (pItem->flags & GUI_REG_MAX))
	{
		gui__Types			max;

		reg_get(&max, pItem->reg2);

		max.i64 = gui__regToInt64(pItem->reg2, &max);

		if (pInst->edit.max > max.i64)
		{
			pInst->edit.max = max.i64;
		}
	}

	return TRUE;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Creates editing mask for a register. Also formats value from
*				reg value to ASCII.
*
* \param		pInst		ptr to GUI instance
* \param		pInfo		ptr to register info
*
* \return		TRUE if mask was successfully created.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editCreateMask(
	gui__Inst *				pInst,
	gui_RegInfo *			pInfo,
	gui_MenuItem const_P *	pItem
) {
	Boolean					ret = TRUE;

	pInst->edit.mmistat = reg_mmiStat(pInst->edit.reg);

	switch (pInst->edit.mmistat & REG_MMI_TYPE_MASK) 
	{
		case SYS_MMI_T_INT:
			{				
				if (!gui__editGetMinMax(pInst, pInfo, pItem)) 
				{
					ret = FALSE;
					break;
				}

				gui__editCreateIntMask(pInst, pInfo);
				pInst->edit.flags |= GUI_ED_SINGLE_CHR;
			}
			break;

		case SYS_MMI_T_REAL:
			{
				if (!gui__editGetMinMax(pInst, pInfo, pItem)) 
				{
					ret = FALSE;
					break;
				}

				gui__editCreateRealMask(pInst, pInfo);
				pInst->edit.flags |= GUI_ED_SINGLE_CHR;
			}
			break;

		case SYS_MMI_T_ENUM:
			{
				if (!gui__editGetMinMax(pInst, pInfo, pItem)) 
				{
					ret = FALSE;
					break;
				}

				gui__editCreateEnumMask(pInst, pInfo);
				pInst->edit.flags &= ~GUI_ED_SINGLE_CHR;
			}
			break;

		case SYS_MMI_T_TIME:
			gui__editCreateTimeMask(pInst, pInfo);
			pInst->edit.flags |= GUI_ED_SINGLE_CHR;
			break;

		case SYS_MMI_T_DATE:
			gui__editCreateDateMask(pInst, pInfo);
			pInst->edit.flags |= GUI_ED_SINGLE_CHR;
			break;

		case SYS_MMI_T_STR:
		case SYS_MMI_T_MASK:
		case SYS_MMI_T_NONE:
		default:
			ret = FALSE;
			pInst->edit.flags |= GUI_ED_SINGLE_CHR;
			break;
	}

	return ret;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Creates edit mask and converts value to ASCII for type INT.
*
* \param		pInst		ptr to GUI instance
* \param		pInfo		ptr to register info
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__editCreateIntMask(
	gui__Inst *				pInst,
	gui_RegInfo *			pInfo
) {
	Int64					min = pInst->edit.min;
	Int64					max = pInst->edit.max;
	Uint8					sign;
	Uint8					digits;

	pInst->edit.frmt_len = 0;

	if (min < 0)
	{
		/* Add sign */
		pInst->edit.format[pInst->edit.frmt_len++] = '+';
		min = -min;
		sign = 1;
	}
	else
	{
		sign = 0;
	}

	if (max < 0)
	{
		max = -max;
	}

	if (min > max)
	{
		/* Max is not max value but represents max digits needed */
		max = min;
	}

	/*
	 *	Add '#'s
	 */
	digits = gui__editCalcDigits((Uint32)max);

	while (digits--)
	{
		pInst->edit.format[pInst->edit.frmt_len++] = '#';
	}

	/*
	 *	Format value to edit buffer (same as data buffer for strings).
	 */
	if (pInst->edit.reg_flags & GUI_PRM_MASKED)
	{
		/*
		 *	Masked password => fill wit zeroes.
		 */
		digits = pInst->edit.frmt_len;

		while (digits--)
		{
			pInst->edit.buffer.str[sign++] = '0';
		}
	}
	else
	{
		/*
		 *	Normal value, write to buffer with zero padding.
		 */
		gui__frmtPaddedInt(
			pInst->edit.full_value, 
			pInst->edit.buffer.str, 
			sign > 0 ? TRUE : FALSE,
			pInst->edit.frmt_len
		);
	}

	if (pInst->edit.reg_flags & GUI_PRM_SLIDE)
	{
		pInst->edit.edit_width = pInst->edit.frmt_len;
	}
	else
	{
		pInst->edit.edit_width = 1;
//		pInst->edit.edit_pos = pInst->edit.frmt_len - 1;
	}
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Creates edit mask and converts value to ASCII for type ENUM.
*
* \param		pInst		ptr to GUI instance
* \param		pInfo		ptr to register info
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__editCreateEnumMask(
	gui__Inst *				pInst,
	gui_RegInfo *			pInfo
) {
	Int64					enum_val;

	deb_assert(pInst->edit.min == 0);

	enum_val = gui__regToInt64(pInst->edit.reg, &pInst->buffer.reg);

	if ((enum_val >= 0) && (enum_val <= 0xFF))
	{
		if (pInst->edit.reg_flags & GUI_PRM_BITMASK)
		{
			deb_assert(pInst->edit.pItem != NULL);
//			deb_assert(pInst->edit.max <= Uint8_MAX);

			pInst->edit.info = 1;

			if ((Uint8)enum_val & (1 << pInst->edit.pItem->enumval))
			{
				pInst->edit.edit_pos = 1;
			}
			else
			{
				pInst->edit.edit_pos = 0;
			}
		}
		else if (pInst->edit.reg_flags & GUI_PRM_DYN_ENUM)
		{
			Uint8			max;

			pInst->edit.pItem->pEnumFn(0, NULL, &max);

			pInst->edit.max = max - 1;
			pInst->edit.info = (Uint8)pInst->edit.max;
			pInst->edit.edit_pos = (Uint8)enum_val;
		}
		else
		{
			deb_assert(pInst->edit.max <= Uint8_MAX);
	
			pInst->edit.info = (Uint8)pInst->edit.max;
			pInst->edit.edit_pos = (Uint8)enum_val;
		}

		gui__editUpdateEnumText(pInst);
	}
	else
	{
		pInst->edit.edit_pos = 0;
		pInst->edit.frmt_len = 0;
		pInst->edit.edit_width = 0;
	}
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Creates edit mask and converts value to ASCII for type REAL.
*
* \param		pInst		ptr to GUI instance
* \param		pInfo		ptr to register info
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__editCreateRealMask(
	gui__Inst *				pInst,
	gui_RegInfo *			pInfo
) {
	sys_AdditAttrReal const_P *	pAddit = pInfo->pInfo;
	Int64					min = pInst->edit.min;
	Int64					max = pInst->edit.max;
	Uint8					digits;
	Uint8					len;
	Uint8					decims;

	pInst->edit.frmt_len = 0;

	if (pAddit->numDecim > pInst->edit.pItem->decims)
	{
		Uint8				div;

		/*
		 *	Menu is configured to drop some digits, scale all values 
		 *	accordingly.
		 */
		div = pAddit->numDecim - pInst->edit.pItem->decims;

		min /= gui__frmtMultipliers[div];
		max /= gui__frmtMultipliers[div];
		pInst->edit.full_value /= gui__frmtMultipliers[div];

		decims = (Uint8)pInst->edit.pItem->decims;
	}
	else
	{
		decims = pAddit->numDecim;
	}

	if (min < 0)
	{
		/* Add sign */
		min = -min;
		pInst->edit.format[pInst->edit.frmt_len++] = '+';
	}

	if (max < 0)
	{
		max = -max;
	}

	if (min > max)
	{
		/* Max is not max value but represents max digits needed */
		max = min;
	}

	digits = gui__editCalcDigits((Uint32)max);

	if (digits <= decims)
	{
		pInst->edit.format[pInst->edit.frmt_len++] = '0';
		pInst->edit.format[pInst->edit.frmt_len++] = '.';

		len = decims - digits;

		while (len--)
		{
			pInst->edit.format[pInst->edit.frmt_len++] = '0';
		}

		len = digits;

		while (len--)
		{
			pInst->edit.format[pInst->edit.frmt_len++] = '#';
		}
	}
	else
	{
		if (decims > 0)
		{
			len = (digits - decims) + pInst->edit.frmt_len;

			digits++;
		}
		else
		{
			len = 0xFF;
		}

		while (digits--)
		{
			if (len == pInst->edit.frmt_len)
			{
				pInst->edit.format[pInst->edit.frmt_len++] = '.';
			}
			else
			{
				pInst->edit.format[pInst->edit.frmt_len++] = '#';
			}
		}
	}

	gui__frmtPaddedReal(
		pInst->edit.full_value, 
		pInst->edit.buffer.str,
		pInst->edit.format[0] == '+' ? TRUE : FALSE,
		decims,
		pInst->edit.frmt_len
	);

	pInst->edit.info = decims;

	if (pInst->edit.reg_flags & GUI_PRM_SLIDE)
	{
		pInst->edit.edit_width = pInst->edit.frmt_len;
	}
	else
	{
		pInst->edit.edit_width = 1;
//		pInst->edit.edit_pos = pInst->edit.frmt_len - 1;
	}
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Creates edit mask and converts value to ASCII for type DATE.
*
* \param		pInst		ptr to GUI instance
* \param		pInfo		ptr to register info
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__editCreateDateMask(
	gui__Inst *				pInst,
	gui_RegInfo *			pInfo
) {
	BYTE const_P *			pFrmt;

	pInst->edit.info = pInst->dt_format;

	pFrmt = gui__frmtGetDateMask(pInst->edit.info);

	pInst->edit.frmt_len = 0;
	pInst->edit.edit_width = gui__editFieldLen(*pFrmt);

	while (*pFrmt)
	{
		pInst->edit.format[pInst->edit.frmt_len++] = *pFrmt++;
	}

	gui__frmtDateToAsc(
		pInst->edit.info, 
		pInst->edit.buffer.str, 
		pInst->buffer.reg.i32
	);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Creates edit mask and converts value to ASCII for type TIME.
*
* \param		pInst		ptr to GUI instance
* \param		pInfo		ptr to register info
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__editCreateTimeMask(
	gui__Inst *				pInst,
	gui_RegInfo *			pInfo
) {
	BYTE const_P *			pFrmt;

	pInst->edit.info = pInst->dt_format;

	pFrmt = gui__frmtGetTimeMask(pInst->edit.info);

	pInst->edit.frmt_len = 0;
	pInst->edit.edit_width = gui__editFieldLen(*pFrmt);

	while (*pFrmt)
	{
		pInst->edit.format[pInst->edit.frmt_len++] = *pFrmt++;
	}

	if (pInst->edit.flags & GUI_ED_TIME2)
	{
		gui__frmtTime2ToAsc(
			pInst->edit.info, 
			pInst->edit.buffer.str, 
			pInst->buffer.reg.u32
		);
	}
	else
	{
		gui__frmtTimeToAsc(
			pInst->edit.info, 
			pInst->edit.buffer.str, 
			pInst->buffer.reg.i32
		);
	}
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Converts ASCII INT to register value. ASCII value is assumed to
*				be in the register buffer and is also stored there.
*
*	\param		pInst		ptr to GUI instance
*	\param		pSrc		source buffer
*	\param		pDst		destination buffer
*
*	\return		TRUE if conversion was succesful.
*
*	\details		
*
*	\note	
*
*	\sa			
*
******************************************************************************/
PRIVATE Boolean gui__editConvertToInt(
	gui__Inst *				pInst,
	gui__Types *			pSrc, 
	gui__Types *			pDst
) {
	reg_StatType			stat;
	Boolean					valid = TRUE;
	
	reg_stat(pInst->edit.reg, &stat);

	switch (stat.status & REG_TYPE_MASK)
	{
	case SYS_REG_T_Int8:
		{
			Int32 tmp;

			if (gui__atoi32(pSrc->str, pInst->edit.frmt_len, &tmp))
			{
				if ((tmp > Int8_MAX) || (tmp < Int8_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->i8 = (Int8)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Uint8:
		{
			Uint32 tmp;

			if (gui__atou32(pSrc->str, pInst->edit.frmt_len, &tmp))
			{
				if ((tmp > Uint8_MAX) || (tmp < Uint8_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->u8 = (Uint8)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Int16:
		{
			Int32 tmp;

			if (gui__atoi32(pSrc->str, pInst->edit.frmt_len, &tmp))
			{
				if ((tmp > Int16_MAX) || (tmp < Int16_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->i16 = (Int16)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Uint16:
		{
			Uint32 tmp;

			if (gui__atou32(pSrc->str, pInst->edit.frmt_len, &tmp))
			{
				if ((tmp > Uint16_MAX) || (tmp < Uint16_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->u16 = (Uint16)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Int32:
		{
			Int32 tmp;

			if (gui__atoi32(pSrc->str, pInst->edit.frmt_len, &tmp))
			{
				pDst->i32 = tmp;
			}
			else
			{
				valid = FALSE;
			}
		}		
		break;

	case SYS_REG_T_Uint32:
		{
			Uint32 tmp;

			if (gui__atou32(pSrc->str, pInst->edit.frmt_len, &tmp))
			{
				pDst->u32 = tmp;
			}
			else
			{
				valid = FALSE;
			}
		}		
		break;

	default:
		deb_assert(0);
		valid = FALSE;
		break;
	}	

	return valid;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts ASCII ENUM to register value. ASCII value is assumed to
*				be in the register buffer and is also stored there.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editConvertToEnum(
	gui__Inst *				pInst, 
	gui__Types *			pDst
) {
	reg_StatType			stat;
	
	reg_stat(pInst->edit.reg, &stat);

	switch (stat.status & REG_TYPE_MASK)
	{
	case SYS_REG_T_Int8:
		pDst->i8 = (Int8)pInst->edit.edit_pos;
		break;

	case SYS_REG_T_Uint8:
		pDst->u8 = (Uint8)pInst->edit.edit_pos;
		break;

	case SYS_REG_T_Int16:
		pDst->i16 = pInst->edit.edit_pos;
		break;

	case SYS_REG_T_Uint16:
		pDst->u16 = pInst->edit.edit_pos;
		break;

	case SYS_REG_T_Int32:
		pDst->i32 = pInst->edit.edit_pos;
		break;

	case SYS_REG_T_Uint32:
		pDst->u32 = pInst->edit.edit_pos;
		break;

	default:
		deb_assert(0);
		break;
	}	

	return TRUE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Converts ASCII REAL to register value. ASCII value is assumed to
*				be in the register buffer and is also stored there.
*
*	\param		pInst		ptr to GUI instance
*	\param		pSrc		source buffer
*	\param		pDst		destination buffer
*
*	\return		TRUE if conversion was succesful.
*
*	\details		
*
*	\note			
*
*	\sa			
*
******************************************************************************/
PRIVATE Boolean gui__editConvertToReal(
	gui__Inst *				pInst, 
	gui__Types *			pSrc, 
	gui__Types *			pDst
) {
	reg_StatType			stat;
	sys_AdditAttrReal const_P *	pAddit;
	Boolean					valid = TRUE;
	Uint8					decims;

	pAddit = (sys_AdditAttrReal const_P *)reg_mmiAttrib(pInst->edit.reg);

	if (pAddit->numDecim > pInst->edit.pItem->decims)
	{
		decims = (Uint8)pInst->edit.pItem->decims;
	}
	else
	{
		decims = pAddit->numDecim;
	}

	reg_stat(pInst->edit.reg, &stat);

	switch (stat.status & REG_TYPE_MASK)
	{
	case SYS_REG_T_Int8:
		{
			Int32 tmp;

			if (gui__atoi32Real(pSrc->str, pInst->edit.frmt_len, decims, &tmp))
			{
				if (pAddit->numDecim > pInst->edit.pItem->decims)
				{
					tmp *= gui__frmtMultipliers[pAddit->numDecim - pInst->edit.pItem->decims];
				}

				if ((tmp > Int8_MAX) || (tmp < Int8_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->i8 = (Int8)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Uint8:
		{
			Uint32 tmp;

			if (gui__atou32Real(pSrc->str, pInst->edit.frmt_len, decims, &tmp))
			{
				if (pAddit->numDecim > pInst->edit.pItem->decims)
				{
					tmp *= gui__frmtMultipliers[pAddit->numDecim - pInst->edit.pItem->decims];
				}

				if ((tmp > Uint8_MAX) || (tmp < Uint8_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->u8 = (Uint8)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Int16:
		{
			Int32 tmp;

			if (gui__atoi32Real(pSrc->str, pInst->edit.frmt_len, decims, &tmp))
			{
				if (pAddit->numDecim > pInst->edit.pItem->decims)
				{
					tmp *= gui__frmtMultipliers[pAddit->numDecim - pInst->edit.pItem->decims];
				}

				if ((tmp > Int16_MAX) || (tmp < Int16_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->i16 = (Int16)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Uint16:
		{
			Uint32 tmp;

			if (gui__atou32Real(pSrc->str, pInst->edit.frmt_len, decims, &tmp))
			{
				if (pAddit->numDecim > pInst->edit.pItem->decims)
				{
					tmp *= gui__frmtMultipliers[pAddit->numDecim - pInst->edit.pItem->decims];
				}

				if ((tmp > Uint16_MAX) || (tmp < Uint16_MIN))
				{
					valid = FALSE;
				}
				else
				{
					pDst->u16 = (Uint16)tmp;
				}
			}
			else
			{
				valid = FALSE;
			}
		}
		break;

	case SYS_REG_T_Int32:
		{
			Int32 tmp;

			if (gui__atoi32Real(pSrc->str, pInst->edit.frmt_len, decims, &tmp))
			{
				if (pAddit->numDecim > pInst->edit.pItem->decims)
				{
					tmp *= gui__frmtMultipliers[pAddit->numDecim - pInst->edit.pItem->decims];
				}

				pDst->i32 = tmp;
			}
			else
			{
				valid = FALSE;
			}
		}		
		break;

	case SYS_REG_T_Uint32:
		{
			Uint32 tmp;

			if (gui__atou32Real(pSrc->str, pInst->edit.frmt_len, decims, &tmp))
			{
				if (pAddit->numDecim > pInst->edit.pItem->decims)
				{
					tmp *= gui__frmtMultipliers[pAddit->numDecim - pInst->edit.pItem->decims];
				}

				pDst->u32 = tmp;
			}
			else
			{
				valid = FALSE;
			}			
		}		
		break;

	default:
		deb_assert(0);
		valid = FALSE;
		break;
	}	

	return valid;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Converts ASCII DATE to register value. ASCII value is assumed to
*				be in the register buffer and is also stored there.
		
*
*	\param		pInst		ptr to GUI instance
*	\param		pSrc		source buffer
*	\param		pDst		destination buffer
*
*	\return		TRUE if conversion was succesful.
*
*	\details		
*
*	\note			
*
*	\sa			
*
******************************************************************************/
PRIVATE Boolean gui__editConvertToDate(
	gui__Inst *				pInst, 
	gui__Types *			pSrc, 
	gui__Types *			pDst
) {
	return gui__ascToDate(pSrc->str, pInst->edit.info, &pDst->u32);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Converts ASCII TIME to register value. ASCII value is assumed to
*				be in the register buffer and is also stored there.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if conversion was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editConvertToTime(
	gui__Inst *				pInst, 
	gui__Types *			pSrc, 
	gui__Types *			pDst
) {
	return gui__ascToTime(pSrc->str, pInst->edit.info, &pDst->u32);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Updates current enum text to register buffer.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void gui__editUpdateEnumText(
	gui__Inst *				pInst
) {
	if (pInst->edit.reg_flags & GUI_PRM_DYN_ENUM)
	{
		Uint8				cnt;

		pInst->edit.frmt_len = pInst->edit.pItem->pEnumFn(
			pInst->edit.edit_pos, 
			pInst->edit.buffer.str, 
			&cnt
		);

		if (pInst->edit.edit_pos >= cnt)
		{
			/*
			 *	Enum count has changed?
			 */
			pInst->edit.edit_pos = 0;

			pInst->edit.frmt_len = pInst->edit.pItem->pEnumFn(
				pInst->edit.edit_pos, 
				pInst->edit.buffer.str, 
				&cnt
			);
		}
	}
	else
	{
		BYTE const_P *			pTxt;
		Uint8					idx;
		sys_TxtHandle			text;

		if (pInst->edit.reg_flags & GUI_PRM_BITMASK)
		{
			text = pInst->edit.pItem->text2;
		}
		else
		{
			text = GUI_NO_TEXT;
		}

		pTxt = gui__regToEnumText(
			pInst->edit.reg, 
			pInst->edit.edit_pos, 
			&pInst->edit.frmt_len,
			text
		);

		for (idx = 0; idx < pInst->edit.frmt_len; idx++)
		{
			pInst->edit.buffer.str[idx] = *pTxt++;
		}
	}

	pInst->edit.edit_width = pInst->edit.frmt_len;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Handles navigation inside edit value.
*
* \param		pInst		ptr to GUI instance
* \param		dir			navigation direction
*
* \return		TRUE if navigation was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean gui__editNavigate(
	gui__Inst *				pInst,
	gui__NavDir				dir
) {
	Boolean					ret = FALSE;

	if (pInst->edit.reg_flags & GUI_PRM_SLIDE)
	{
		/*
		 *	Sliding edit is on.
		 */
		if (dir == gui_nav_up)
		{
			ret = gui__editSlideNavUp(pInst);
		}
		else if (dir == gui_nav_down)
		{
			ret = gui__editSlideNavDown(pInst);
		} 
		else if (dir == gui_nav_root)
		{
			/* This resets the slide counter, should be called on key up */
			pInst->edit.step = 1;
		}
	}
	else 
	{
		switch (pInst->edit.mmistat & REG_MMI_TYPE_MASK) 
		{
			case SYS_MMI_T_INT:
			case SYS_MMI_T_REAL:
			case SYS_MMI_T_TIME:
			case SYS_MMI_T_DATE:
				{
					switch (dir)
					{
					case gui_nav_up:
						ret = gui__editMaskNavUp(pInst);
						break;

					case gui_nav_down:
						ret = gui__editMaskNavDown(pInst);
						break;

					case gui_nav_left:
						ret = gui__editMaskNavLeft(pInst);
						break;

					case gui_nav_right:
						ret = gui__editMaskNavRight(pInst);
						break;

					default:
						break;
					}

					if (ret)
					{
						pInst->edit.edit_width = gui__editFieldLen(
							pInst->edit.format[pInst->edit.edit_pos]
						);
					}
				}
				break;

			case SYS_MMI_T_ENUM:
				{
					switch (dir)
					{
					case gui_nav_up:
						ret = gui__editEnumNavUp(pInst);
						break;

					case gui_nav_down:
						ret = gui__editEnumNavDown(pInst);
						break;

					default:
						break;
					}

					if (ret)
					{
						gui__editUpdateEnumText(pInst);
					}
				}
				break;

			case SYS_MMI_T_STR:
				break;

			case SYS_MMI_T_MASK:
			case SYS_MMI_T_NONE:
			default:
				break;
		}
	}

	/*
	 *	If value is marked to be stored each time it changes do it here.
	 */
	if (
		ret && 
		pInst->edit.pItem && 
		(pInst->edit.pItem->flags & GUI_PRM_IMMEDIATE)
	) {
		gui__editEnd(pInst);
	}

	return ret;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates right in edit field.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if navigation was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editMaskNavRight(
	gui__Inst *				pInst
) {
	Uint8					pos;

	pos = pInst->edit.edit_pos + pInst->edit.edit_width;

	while (pos < pInst->edit.frmt_len)
	{
		Uint8				chr;

		for (chr = 0; chr < dim(gui__editFrmtChars); chr++)
		{
			if (pInst->edit.format[pos] == gui__editFrmtChars[chr])
			{
				pInst->edit.edit_pos = pos;
				return TRUE;
			}
		}

		pos++;
	}

	return FALSE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates left in edit field.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if navigation was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editMaskNavLeft(
	gui__Inst *				pInst
) {
	Uint8					pos;

	if (pInst->edit.edit_pos == 0)
	{
		return FALSE;
	}

	pos = pInst->edit.edit_pos - 1;

	while (1)
	{
		Uint8				chr;

		for (chr = 0; chr < dim(gui__editFrmtChars); chr++)
		{
			if (pInst->edit.format[pos] == gui__editFrmtChars[chr])
			{
				Uint8		len;

				len = gui__editFieldLen(pInst->edit.format[pos]);

				pos -= len - 1;

				pInst->edit.edit_pos = pos;

				return TRUE;
			}
		}

		if (pos == 0)
		{
			break;
		}

		pos--;
	}

	return FALSE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates up in edit field.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if navigation was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editMaskNavUp(
	gui__Inst *				pInst
) {
	BYTE *					pBuf = pInst->edit.buffer.str;

	switch (pInst->edit.format[pInst->edit.edit_pos])
	{
	case '#':
		{
			BYTE val = pBuf[pInst->edit.edit_pos];

			if ((val >= '0') && (val <= '9'))
			{
				val++;

				if (val > '9')
				{
					val = '0';
				}
			}
			else
			{
				val = '0';
			}

			pBuf[pInst->edit.edit_pos] = val;
		}
		break;

	case '+':
		if (pBuf[pInst->edit.edit_pos] == '+')
		{
			pBuf[pInst->edit.edit_pos] = '-';
		}
		else
		{
			pBuf[pInst->edit.edit_pos] = '+';
		}
		break;

	case 'A':
		if (pBuf[pInst->edit.edit_pos] == 'A')
		{
			pBuf[pInst->edit.edit_pos] = 'P';
		}
		else
		{
			pBuf[pInst->edit.edit_pos] = 'A';
		}
		break;

	case 'h':
	case 'm':
	case 's':
	case 'M':
	case 'D':
	case 'Y':
		gui__editTimeUpDown(pInst, TRUE);
		break;

	default:
		deb_assert(FALSE);
		break;
	}

	return TRUE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates down in edit field.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if navigation was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editMaskNavDown(
	gui__Inst *				pInst
) {
	BYTE *					pBuf = pInst->edit.buffer.str;

	switch (pInst->edit.format[pInst->edit.edit_pos])
	{
	case '#':
		{
			BYTE val = pBuf[pInst->edit.edit_pos];

			if ((val >= '0') && (val <= '9'))
			{
				val--;

				if (val < '0')
				{
					val = '9';
				}
			}
			else
			{
				val = '0';
			}

			pBuf[pInst->edit.edit_pos] = val;
		}
		break;

	case '+':
		if (pBuf[pInst->edit.edit_pos] == '+')
		{
			pBuf[pInst->edit.edit_pos] = '-';
		}
		else
		{
			pBuf[pInst->edit.edit_pos] = '+';
		}
		break;

	case 'A':
		if (pBuf[pInst->edit.edit_pos] == 'A')
		{
			pBuf[pInst->edit.edit_pos] = 'P';
		}
		else
		{
			pBuf[pInst->edit.edit_pos] = 'A';
		}
		break;

	case 'h':
	case 'm':
	case 's':
	case 'M':
	case 'D':
	case 'Y':
		gui__editTimeUpDown(pInst, FALSE);
		break;

	default:
		deb_assert(FALSE);
		break;
	}

	return TRUE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Increases value in slide editing if not already at the top.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if value changed.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editSlideNavUp(
	gui__Inst *				pInst
) {
	Int64					tmp;
	static int 				speedUpCount = 0;

	if (!(pInst->edit.flags & GUI_ED_SLIDE_UP))
	{
		pInst->edit.step = 1;
		pInst->edit.flags |= GUI_ED_SLIDE_UP;
		speedUpCount = 0;
	}

	tmp = pInst->edit.full_value + pInst->edit.step;
	speedUpCount++;

	if (tmp > pInst->edit.max)
	{
		if (pInst->edit.reg_flags & GUI_REG_LOOP)
		{
			// todo: take tmp - pInst->edit.max into account?
			tmp = pInst->edit.min;
			pInst->edit.step = 1;
		}
		else
		{
			tmp = pInst->edit.max;
		}
	}

	if (tmp <= pInst->edit.max)
	{
		if (pInst->edit.full_value != tmp)
		{
			Uint32 speedUpCompare = 30;

			pInst->edit.full_value = tmp;

			if (
				(pInst->edit.reg_flags & GUI_PRM_ACCEL) &&
				((speedUpCount % speedUpCompare) == 0)
			) {
					pInst->edit.step = pInst->edit.step * 2;
					if(pInst->edit.step > 10){
						Int64 tmpValue = tmp/10;
						pInst->edit.full_value = tmpValue*10 + 10;
						pInst->edit.step = 10;
					}
			}

			return gui__editUpdateSlideValue(pInst);
		}
	}

	return FALSE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Decreases value in slide editing if not already at the min 
*				value.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if value changed.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editSlideNavDown(
	gui__Inst *				pInst
) {
	Int64					tmp;
	static int 				speedUpCount = 0;

	if (pInst->edit.flags & GUI_ED_SLIDE_UP)
	{
		pInst->edit.step = 1;
		pInst->edit.flags &= ~GUI_ED_SLIDE_UP;
		speedUpCount = 0;
	}

	tmp = pInst->edit.full_value - pInst->edit.step;
	speedUpCount++;

	if (tmp < pInst->edit.min)
	{
		if (pInst->edit.reg_flags & GUI_REG_LOOP)
		{
			// todo: take pInst->edit.min - tmp into account?
			tmp = pInst->edit.max;
			pInst->edit.step = 1;
		}
		else
		{
			tmp = pInst->edit.min;
		}
	}

	if (tmp >= pInst->edit.min)
	{
		if (pInst->edit.full_value != tmp)
		{
			Uint32 speedUpCompare = 30;

			pInst->edit.full_value = tmp;

			if (
				(pInst->edit.reg_flags & GUI_PRM_ACCEL) &&
				((speedUpCount % speedUpCompare) == 0)
			) {
					pInst->edit.step = pInst->edit.step * 2;
					if(pInst->edit.step > 10){
						Int64 tmpValue = tmp/10;
						pInst->edit.full_value = tmpValue*10;
						pInst->edit.step = 10;
					}
			}

			return gui__editUpdateSlideValue(pInst);
		}
	}

	return FALSE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Updates value from slide editing buffer to string edit buffer.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if value was converted successfully.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editUpdateSlideValue(
	gui__Inst *				pInst
) {
	Boolean					ret = TRUE;

	switch (pInst->edit.mmistat & REG_MMI_TYPE_MASK) 
	{
		case SYS_MMI_T_INT:
			gui__frmtPaddedInt(
				pInst->edit.full_value, 
				pInst->edit.buffer.str, 
				pInst->edit.min < 0,
				pInst->edit.frmt_len
			);
			break;

		case SYS_MMI_T_REAL:
			gui__frmtPaddedReal(
				pInst->edit.full_value, 
				pInst->edit.buffer.str, 
				pInst->edit.min < 0,
				pInst->edit.info,
				pInst->edit.frmt_len
			);
			break;

		default:
			ret = FALSE;
			break;
	}

	return ret;
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Increments of decrements a date / time field.
*
* \param		pInst		ptr to gui instance
* \param		up			if true value is incremented else decremented
*
* \return		TRUE if value was modified successfully.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editTimeUpDown(
	gui__Inst *				pInst,
	Boolean					up
) {
	BYTE *					pBuf = pInst->edit.buffer.str;
	Uint32					max;
	Uint32					min;
	Uint32					val;
	Boolean					hour = FALSE;

	switch (pInst->edit.format[pInst->edit.edit_pos])
	{
		case 'h':
			if (pInst->edit.info == GUI_DT_USA)
			{
				max = 12;
				min = 1;
			}
			else
			{
				max = 23;
				min = 0;
			}
			hour = TRUE;
			break;

		case 'M':
			max = 12;
			min = 1;
			break;

		case 'D':
			max = 31;
			min = 1;
			break;

		case 'Y':
			max = 2100;
			min = 2000;
			break;

		default: /* m / s */
			max = 59;
			min = 0;
			break;
	}

	if (!gui__atou32(&pBuf[pInst->edit.edit_pos], pInst->edit.edit_width, &val))
	{
		val = min;
	}

	if (up)
	{
		if (val == max)
		{
			val = min;
		}
		else
		{
			val++;

			if (hour && (pInst->edit.info == GUI_DT_USA) && (val == max))
			{
				gui__editRollTime(pInst, pBuf, pInst->edit.edit_pos);
			}
		}
	}
	else
	{
		if (val == min)
		{
			val = max;
		}
		else
		{
			val--;

			if (hour && (pInst->edit.info == GUI_DT_USA) && (val == (max - 1)))
			{
				gui__editRollTime(pInst, pBuf, pInst->edit.edit_pos);
			}
		}
	}


	gui__u32tofield(val, &pBuf[pInst->edit.edit_pos], pInst->edit.edit_width);

	return FALSE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Toggles AM / PM state (if AM / PM is found in format string).
*				Scanning is started from given position.
*
*	\param		pInst	Ptr to GUI instance.
*	\param		pBuf	Ptr to editing buffer.
*	\param		pos		Character position to start from.
*
*	\return		
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void gui__editRollTime(
	gui__Inst *				pInst,
	BYTE *					pBuf,
	Uint8					pos
) {
	while (++pos < pInst->edit.frmt_len)
	{
		if (pInst->edit.format[pos] == 'A')
		{
			if (pBuf[pos] == 'A')
			{
				pBuf[pos] = 'P';
			}
			else
			{
				pBuf[pos] = 'A';
			}

			break;
		}
	}
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates up in enum edit field.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if navigation was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editEnumNavUp(
	gui__Inst *				pInst
) {
	if (pInst->edit.edit_pos < pInst->edit.info)
	{
		pInst->edit.edit_pos++;
	}
	else
	{
		pInst->edit.edit_pos = 0;
	}

	return TRUE;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Navigates down in enum edit field.
*
* \param		pInst		ptr to GUI instance
*
* \return		TRUE if navigation was succesful.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Boolean gui__editEnumNavDown(
	gui__Inst *				pInst
) {
	if (pInst->edit.edit_pos > 0)
	{
		pInst->edit.edit_pos--;
	}
	else
	{
		pInst->edit.edit_pos = pInst->edit.info;
	}

	return TRUE;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Claculates digits in a value.
*
* \param		value		input value
*
* \return		Number of digits in the value.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__editCalcDigits(
	Uint32					value
) {
	Uint8					digits = 0;

	while (value)
	{
		digits++;

		value /= 10;
	}

	return digits;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Returns formatting field width from format character.
*
* \param		chr		formatting character
*
* \return		Formatting field width.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE Uint8 gui__editFieldLen(
	BYTE					chr
) {
	Uint8					len;

	switch (chr)
	{
	case 'A':
	case 'h':
	case 'm':
	case 's':
	case 'D':
	case 'M':
		len = 2;
		break;

	case 'Y':
		len = 4;
		break;

	default:
		len = 1;
		break;
	}

	return len;
}




/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets / clears a bit in a value in register buffer.
*
*	\param		pInst		ptr to GUI instance
*	\param		bit			bit index
*	\param		set			if TRUE bit is set, else cleared
*
*	\return		TRUE if operation was succesful.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__editBitToggle(
	gui__Inst *				pInst,
	Uint8					bit,
	Boolean					set
) {
	reg_StatType			stat;
	
	reg_stat(pInst->edit.reg, &stat);

	switch (stat.status & REG_TYPE_MASK)
	{
	case SYS_REG_T_Int8:
	case SYS_REG_T_Uint8:
		if (set)
		{
			pInst->buffer.reg.u8 |= (Uint8)(1 << bit);
		}
		else
		{
			pInst->buffer.reg.u8 &= ~(Uint8)(1 << bit);
		}
		break;

		break;

	case SYS_REG_T_Int16:
	case SYS_REG_T_Uint16:
		if (set)
		{
			pInst->buffer.reg.u16 |= (Uint16)(1 << bit);
		}
		else
		{
			pInst->buffer.reg.u16 &= ~(Uint16)(1 << bit);
		}
		break;

	case SYS_REG_T_Int32:
	case SYS_REG_T_Uint32:
		if (set)
		{
			pInst->buffer.reg.u32 |= (Uint32)(1 << bit);
		}
		else
		{
			pInst->buffer.reg.u32 &= ~(Uint32)(1 << bit);
		}
		break;

	default:
		deb_assert(0);
		break;
	}	

	return TRUE;
}
