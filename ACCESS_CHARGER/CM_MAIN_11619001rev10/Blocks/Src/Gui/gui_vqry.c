/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI	
*
*	\brief		Popup view handling.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "tstamp.h"
#include "msg.h"
#include "reg.h"
#include "global.h"
#include "deb.h"
#include "txt.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define GUI__QRY_SEL_POS	(5 * 8)

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	sys_TxtHandle			text;		/**< Query selection text			*/
	gui__fnQuery			handler;	/**< Handler function				*/
} gui__QuerySel;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	sys_TxtHandle			text;		/**< Query text						*/
	gui__QuerySel const_P *	pOptions;	/**< Query selections				*/
	Uint8					count;		/**< Number of query selections		*/
} gui__QueryMode;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Boolean gui__qryCancel(gui__Inst * pInst);
PRIVATE Boolean gui__qryLogin(gui__Inst * pInst);
PRIVATE Boolean gui__qryFactoryDef(gui__Inst * pInst);
PRIVATE Boolean gui__qryFactorySet(gui__Inst * pInst);
PRIVATE Boolean gui__qryClrStat(gui__Inst * pInst);
PRIVATE Boolean gui__qryActivation(gui__Inst * pInst);


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/** 
 *	Login query configuration.
 */
PRIVATE const_P gui__QuerySel	gui__vQryLogin[] =
{
	/*
	 *	
	 *	Selection text				Selection handler
	 *	------------------------	-----------------*/
	{	txt_i_tLogMenu,				gui__qryLogin							},
	{	txt_i_tCancel,				gui__qryCancel							}
};

/** 
 *	Reset to factory defaults query configuration.
 */
PRIVATE const_P gui__QuerySel	gui__vQryFactDef[] =
{
	/*
	 *	
	 *	Selection text				Selection handler
	 *	------------------------	-----------------*/
	{	txt_i_tConfirm,				gui__qryFactoryDef						},
	{	txt_i_tCancel,				gui__qryCancel							}
};

/** 
 *	Set factory defaults query configuration.
 */
PRIVATE const_P gui__QuerySel	gui__vQryFactSet[] =
{
	/*
	 *
	 *	Selection text				Selection handler
	 *	------------------------	-----------------*/
	{	txt_i_tConfirm,				gui__qryFactorySet						},
	{	txt_i_tCancel,				gui__qryCancel							}
};

/**
 *	Clear statistics query configuration.
 */
PRIVATE const_P gui__QuerySel	gui__vQryClrStat[] =
{
	/*
	 *	
	 *	Selection text				Selection handler
	 *	------------------------	-----------------*/
	{	txt_i_tConfirm,				gui__qryClrStat							},
	{	txt_i_tCancel,				gui__qryCancel							}
};

/**
 *	Activate DPL query configuration.
 */
PRIVATE const_P gui__QuerySel	gui__vQryDpl[] =
{
	/*
	 *
	 *	Selection text				Selection handler
	 *	------------------------	-----------------*/
	{	txt_i_tActivate,			gui__qryActivation						},
	{	txt_i_tCancel,				gui__qryCancel							}
};

PRIVATE const_P gui__QueryMode	gui__vQrySelections[] =
{
	/*
	 *	
	 *	Query text				Query selections	Selection count
	 *	--------------------	-----------------	---------------*/
	{	txt_i_tLogIn1Required,	gui__vQryLogin,		dim(gui__vQryLogin)		},
	{	txt_i_tLogIn2Required,	gui__vQryLogin,		dim(gui__vQryLogin)		},
	{	txt_i_tResetToDefault,	gui__vQryFactDef,	dim(gui__vQryFactDef)	},
	{	txt_i_tSetDefault,		gui__vQryFactSet,	dim(gui__vQryFactSet)	},
	{	txt_i_tResetStatistics,	gui__vQryClrStat,	dim(gui__vQryClrStat)	},
	{	txt_i_tActivation,		gui__vQryDpl,		dim(gui__vQryDpl)		},
};

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		'Popup query' view proc.
*
* \param		pInst		ptr to GUI instance
* \param		pView		ptr to view
* \param		msg			message type
* \param		pArgs		message args
*
* \return		
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewQueryProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	gui__QueryPopupView *	pop = (gui__QueryPopupView *)pView;

//	DEB_DBG(deb_logN("GUI.query_popup.Proc: %s", gui__dbgCommands[msg]);)

	switch (msg)
	{
	case gui_msg_create:
		break;

	case gui_msg_init:
		{
			pop->query = *(Uint8 *)pArgs;
			pop->current_sel = 0;
		}
		break;

	case gui_msg_timeout:
		gui__viewClose(pInst, pView);
		break;

	case gui_msg_close:
		gui__viewTmrStop(pView);
		break;

	case gui_msg_paint:
		{
			gui__QueryMode const_P * pQuery;
			display_Rect	rect;
			Uint8			len;
			Uint8			sel;

			pQuery = &gui__vQrySelections[pop->query];

			gui__dispClear(pInst);
			gui__fontSelect(pInst, GUI_FNT_8x8);
			gui__textSelectBuffer(pInst, pInst->buffer.buf1);

			rect.x = 0;
			rect.width = pInst->pInit->width;
			rect.height = 5 * pInst->disp.pFont->height;
			rect.y = 0;

			/*
			 *	Paint message using wrapping.
			 */
			if (pQuery->text != GUI_NO_TEXT)
			{
				len = gui__textWriteBuffer(txt_localHandle(pInst->instid, pQuery->text), pInst->text.pCurrent, GUI_MAX_TXT3);

				/*
				 *	Find number of required rows.
				 */
				gui__dispDrawTextAlignedEx(
					pInst, 
					pInst->text.pCurrent, 
					len, 
					&rect, 
					GUI_ALIGN_CENTER|GUI_TEST_SIZE
				);


				/*
				 *	Output characters.
				 */
				rect.y = GUI__QRY_SEL_POS - rect.height;


				gui__dispDrawTextAlignedEx(
					pInst, 
					pInst->text.pCurrent, 
					len, 
					&rect, 
					GUI_ALIGN_CENTER
				);
			}
			
			/*
			 *	Paint selections.
			 */
			gui__fontSelect(pInst, GUI_FNT_6x8);

			rect.y = GUI__QRY_SEL_POS;
			rect.height = pInst->disp.pFont->height;

			for (sel = 0; sel < pQuery->count; sel++)
			{
				if (pQuery->pOptions[sel].text != GUI_NO_TEXT)
				{

					len = gui__textWriteAligned(
						pInst, 
						txt_localText(pInst->instid, pQuery->pOptions[sel].text),
						0,
						pInst->disp.chr_per_line,
						GUI_ALIGN_CENTER
					);

					if (sel == pop->current_sel)
					{
						gui__dispSetModeFlag(pInst, DISPLAY_DRAW_INVERT);
					}

					gui__dispDrawTextAligned(
						pInst, 
						pInst->text.pCurrent, 
						len, 
						&rect, 
						GUI_ALIGN_CENTER
					);


					if (sel == pop->current_sel)
					{
						gui__dispClearModeFlag(pInst, DISPLAY_DRAW_INVERT);
					}

					rect.y += pInst->disp.pFont->height;
				}
			}
			
			gui__viewSetPainted(pView);
		}
		break;

	case gui_msg_key_up:
		{
			gui__VKey key = *(gui__VKey *)pArgs;
			gui__QueryMode const_P * pQuery;

			pQuery = &gui__vQrySelections[pop->query];

			switch (key & GUI_VKEY_MASK)
			{
			/*
			 *	Execute current selection.
			 */
			case GUI_VKEY_OK:
				if (pQuery->pOptions[pop->current_sel].handler != NULL)
				{
					if (pQuery->pOptions[pop->current_sel].handler(pInst))
					{
						gui__viewClose(pInst, pView);
					}
				}
				break;

			/*
			 *	Close view.
			 */
			case GUI_VKEY_ESC:
				gui__viewClose(pInst, pView);
				break;

			default:
				break;
			}
		}
		break;

	case gui_msg_key_down:
		{
			gui__VKey key = *(gui__VKey *)pArgs;

			switch (key & GUI_VKEY_MASK)
			{
			/*
			 *	Navigate selection up.
			 */
			case GUI_VKEY_UP:
				if (pop->current_sel > 0)
				{
					pop->current_sel--;
					gui__viewInvalidate(pView);
				}
				break;

			/*
			 *	Navigate selection down.
			 */
			case GUI_VKEY_DOWN:
				{
					gui__QueryMode const_P * pQuery;

					pQuery = &gui__vQrySelections[pop->query];

					if (pop->current_sel < (pQuery->count - 1))
					{
						pop->current_sel++;
						gui__viewInvalidate(pView);
					}
				}
				break;

			default:
				break;
			}
		}
		break;

	default:
		break;
	}

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Default query popup Cancel function handler.
*
*	\param		pInst		Ptr to GUI instance.
*
*	\return		TRUE to indicate that popup should be closed.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__qryCancel(
	gui__Inst *				pInst		
) {
	return TRUE;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Query popup 'Login' function handler.
*
*	\param		pInst		Ptr to GUI instance.
*
*	\return		TRUE to indicate that popup should be closed.
*
*	\details	Navigates to login menu.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__qryLogin(
	gui__Inst *				pInst		
) {
	deb_log("GUI::QueryPopup: navigating to login view...");

	gui__navInToTable(pInst, pInst->pInit->pLogin);
	return TRUE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Query popup 'Reset to factory defaults' function handler.
*
*	\param		pInst		Ptr to GUI instance.
*
*	\return		TRUE to indicate that popup should be closed.
*
*	\details	Writes to SUP register that causes reset to factory defaults.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__qryFactoryDef(
	gui__Inst *				pInst		
) {
	Uint8	tmp = 1;

	deb_log("GUI::QueryPopup: generating reset-to-factory-defaults write...");

	reg_put(&tmp, gui_FactoryDefaults);

	return TRUE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Query popup 'Set factory defaults' function handler.
*
*	\param		pInst		Ptr to GUI instance.
*
*	\return		TRUE to indicate that popup should be closed.
*
*	\details	Writes to SUP register that causes set factory defaults.
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean gui__qryFactorySet(
	gui__Inst *				pInst
) {
	Uint8	tmp = 1;

	deb_log("GUI::QueryPopup: generating set-factory-defaults write...");

	reg_put(&tmp, gui_FactorySet);

	return TRUE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Query popup 'Clear statistics' function handler.
*
*	\param		pInst		Ptr to GUI instance.
*
*	\return		TRUE to indicate that popup should be closed.
*
*	\details	Writes to SUP register that causes clearing of all statistics.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__qryClrStat(
	gui__Inst *				pInst		
) {
	Uint8	tmp = 1;

	deb_log("GUI::QueryPopup: generating clear-all-statistics write...");

	reg_put(&tmp, gui_ClearStatistics);

	return TRUE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Query popup 'Activation' function handler.
*
*	\param		pInst		Ptr to GUI instance.
*
*	\return		TRUE to indicate that popup should be closed.
*
*	\details	Navigates to Activation menu.
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean gui__qryActivation(
	gui__Inst *				pInst
) {
	deb_log("GUI::QueryPopup: navigating to Activation view...");

	gui__navInToTable(pInst, pInst->pInit->pActivation);
	return TRUE;
}
