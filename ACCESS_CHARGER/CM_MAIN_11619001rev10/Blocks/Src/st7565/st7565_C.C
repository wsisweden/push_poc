/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		st7565_C.c
*
*	\ingroup	ST7565
*
*	\brief		ST7565 display communication functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"

#include "protif.h"

#include "st7565.h"
#include "local.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void st7565__comReqDone(Uint8 code, protif_Request * pReq);
PRIVATE void st7565__comWriteBytes(st7565_Instance * pInst, BYTE * pData, Uint16 bytes);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG
PUBLIC Uint8 st7565__debug = 0;
#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/


/* End of declaration module **************************************************/


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes communication data.
*
* \param		pInst		ptr to st7565 instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void st7565__comInit(
	st7565_Instance *		pInst
) {
	osa_newSemaTaken(&pInst->reqSema);

	pInst->req.fnCallback = st7565__comReqDone;
	pInst->req.pUtil = pInst;
	pInst->req.data.pNext = NULL;
	pInst->req.pTargetInfo = pInst->pInit->pTargetInfo;

	pInst->pReqHandler = sys_instIdToTHIS(pInst->pInit->protInst); 
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Write command byte to the display.
*
* \param		pInst		ptr to st7565 instance
* \param		data		command byte
*
* \details		
*
* \note			Blocks until data has been sent.	
*
* \sa			
*
******************************************************************************/
PUBLIC void st7565_comWriteCmdByte(
	st7565_Instance *		pInst, 
	BYTE 					data
) {
	pInst->cmd[0] = data;

	pInst->pInit->fnCommand(TRUE);
	st7565__comWriteBytes(pInst, pInst->cmd, 1);
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Writes command bytes to the display.
*
* \param		pInst		ptr to st7565 instance
* \param		pData		ptr to byte buffer
* \param		bytes		number of bytes in the buffer
*
* \details		
*
* \note			Blocks until data has been sent.		
*
* \sa			
*
******************************************************************************/
PUBLIC void st7565_comWriteCmd(
	st7565_Instance *		pInst, 
	BYTE *					pData, 
	Uint16					bytes
) {
	pInst->pInit->fnCommand(TRUE);
	st7565__comWriteBytes(pInst, pData, bytes);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Writes data bytes to the display.
*
* \param		pInst		ptr to st7565 instance
* \param		pData		ptr to byte buffer
* \param		bytes		number of bytes in the buffer
*
* \details		
*
* \note			Blocks until data has been sent.	
*
* \sa			
*
******************************************************************************/
PUBLIC void st7565_comWriteData(
	st7565_Instance *		pInst, 
	BYTE *					pData, 
	Uint16					bytes
) {
	pInst->pInit->fnCommand(FALSE);
	st7565__comWriteBytes(pInst, pData, bytes);
}




/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Reads status byte from the display.
*
* \param		pInst		ptr to st7565 instance
*
* \return		Status byte.
*
* \details		
*
* \note			NOT SUPPORTED!
*
* \sa			
*
******************************************************************************/
PUBLIC Uint8 st7565_comReadStatus(
	st7565_Instance *		pInst
) {
	osa_semaGet(&pInst->reqSema);

	return 0;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Returns true if the communication driver is busy.
*
* \param		pInst		ptr to st7565 instance
*
* \return		TRUE if driver is busy.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Boolean st7565_comBusy(
	st7565_Instance *		pInst
) {
	if (pInst->state & ST7565_STATE_BUSY)
	{
		return TRUE;
	}

	return FALSE;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Writes bytes to the display
*
* \param		pInst		ptr to st7565 instance
* \param		pData		ptr to byte buffer
* \param		bytes		number of bytes in the buffer
*
* \details		
*
* \note			Blocks until data has been sent.
*
* \sa			
*
******************************************************************************/
PRIVATE void st7565__comWriteBytes(
	st7565_Instance *		pInst, 
	BYTE *					pData, 
	Uint16					bytes
) {
	pInst->state |= ST7565_STATE_BUSY;

	pInst->req.data.flags = PROTIF_FLAG_WRITE;
	pInst->req.data.pData = pData;
	pInst->req.data.size = bytes;

#ifndef NDEBUG
	st7565__debug++;
#endif

	pInst->pInit->pProtIf->fnAccess(pInst->pReqHandler, &pInst->req);

	/* todo:	-	could be better to wait before new msg but this requires write 
	 *				buffer locking
	 */
	osa_semaGet(&pInst->reqSema);

#ifndef NDEBUG
	st7565__debug--;
#endif
}




/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		SPI master callback handler. Called when SPI request has been
*				handled.
*
* \param		code		SPIM return code
* \param		pReq		SPIM request
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PRIVATE void st7565__comReqDone(
	Uint8					code,
	protif_Request *		pReq
) {
	st7565_Instance *		pInst;

	pInst = (st7565_Instance *)pReq->pUtil;

	pInst->state &= ~ST7565_STATE_BUSY;

	switch (code)
	{
	/*
	 *	Slave replied with OK
	 */
	case PROTIF_OK:
		{
		}
		break;

	/*
	 *	Master replied with error code (protocol error)
	 */
	case PROTIF_ERROR:
		{
			/* todo: retry? */
		}
		break;

	/*
	 *	Slave replied with NACK.
	 */
	case PROTIF_NACK:
		{
			/* todo: never happens with spi */
		}
		break;

	default:
		{
		}
		break;
	}

	if (!(pInst->state & ST7565_STATE_BUSY))
	{
		osa_semaSet(&pInst->reqSema);
	}
}
