#include "ndef.h"
#include <string.h>

/**
 * Get NDEF from payload
 */
bool NdefFromPayload(const uint8_t* const payload, const uint8_t count,
    SramNdef_t* const ndef) {
  uint8_t index = 0;

  if (count > MAX_NDEF_PAYLOAD_SIZE)
    return false;

  ndef->payload[index++] = NDEF_SOF;
  ndef->payload[index++] = count + SRAM_NDEF_RECORD_HEADER_SIZE;
  ndef->payload[index++] = NDEF_RECORD_HEADER;
  ndef->payload[index++] = NDEF_TYPE_VALUE;
  ndef->payload[index++] = count;
  memcpy(&ndef->payload[index], payload, count);
  index += count;
  ndef->payload[index++] = NDEF_EOF;
  ndef->length = index;

  return true;
}

/**
 * Get payload from NDEF
 */
uint8_t NdefGetPayload(uint8_t* const payload, const SramNdef_t* const ndef) {
  uint8_t index = 0;
  uint8_t messageLength = 0;
  uint8_t payloadLength = 0;

  if (ndef->payload[index++] != NDEF_SOF)
    return 0;

  messageLength = ndef->payload[index++];
  if (messageLength > (MAX_NDEF_PAYLOAD_SIZE + SRAM_NDEF_RECORD_HEADER_SIZE))
    return 0;

  if (ndef->payload[index++] != NDEF_RECORD_HEADER)
    return 0;

  if (ndef->payload[index++] != NDEF_TYPE_VALUE)
    return 0;

  payloadLength = ndef->payload[index++];
  if (payloadLength != (messageLength - SRAM_NDEF_RECORD_HEADER_SIZE))
    return 0;

  memcpy(payload, &ndef->payload[index], payloadLength);
  index += payloadLength;

  if (ndef->payload[index++] != NDEF_EOF)
    return 0;

  return payloadLength;
}
