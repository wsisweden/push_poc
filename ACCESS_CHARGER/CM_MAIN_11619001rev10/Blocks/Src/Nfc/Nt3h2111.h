/*
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Nt3h2111.h
*
*	\ingroup	NFC
*
*	\brief		Public declarations of the Nt3h2111 driver.
*
*	\details
*
*	\note
*
*	\version	14-09-2018 / Andreas Carmvall
*
*******************************************************************************/

#ifndef NT3H2111_LOCAL_H_INCLUDED
#define NT3H2111_LOCAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "Nt3h2111_Defines.h"
#include "Ndef.h"

#include <stdbool.h>

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

struct nt3h2111__Inst;
typedef struct nt3h2111__Inst nt3h2111__Inst;

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

nt3h2111__Inst* nt3h2111_init(nfc_Init const_P * pInit);
void nt3h2111_warmInit(nt3h2111__Inst* pInst);
bool nt3h2111_isIdle(nt3h2111__Inst* pInst);
bool nt3h2111_isError(nt3h2111__Inst* pInst);
uint8_t nt3h2111_getRegisterValue(nt3h2111__Inst* pInst);
void nt3h2111_readReg(nt3h2111__Inst* pInst,
                      const eNt3h2111_SessionRegister_t reg);
void nt3h2111_writeReg(nt3h2111__Inst* pInst,
                       const eNt3h2111_SessionRegister_t reg,
                       const uint8_t mask, const uint8_t data);
void nt3h2111_readData(nt3h2111__Inst* pInst, const uint16_t address,
                       const uint8_t count, uint8_t* const pBuffer);
void nt3h2111_writeData(nt3h2111__Inst* pInst, const uint16_t address,
                        const uint8_t count, const uint8_t* const pData);
void nt3h2111_readEepromNdef(nt3h2111__Inst* pInst, EepromNdef_t* const ndef);
void nt3h2111_writeEepromNdef(nt3h2111__Inst* pInst,
                              const EepromNdef_t* const ndef);
void nt3h2111_writeSramNdef(nt3h2111__Inst* pInst,
                              const SramNdef_t* const ndef);
void nt3h2111_readSram(nt3h2111__Inst* pInst, uint8_t* const pBuffer);
void nt3h2111_writeSram(nt3h2111__Inst* pInst, const uint8_t* const pData,
  const uint8_t count);
bool nt3h2111_isRfField(nt3h2111__Inst* pInst, const uint8_t ns);
bool nt3h2111_isDataAvailable(nt3h2111__Inst* pInst, const uint8_t ns);
bool nt3h2111_isDataRead(nt3h2111__Inst* pInst, const uint8_t ns);
bool nt3h2111_isPassThroughMode(nt3h2111__Inst* pInst, const uint8_t nc);
bool nt3h2111_isReady(nt3h2111__Inst* pInst, const uint8_t ns);
uint8_t nt3h2111_enablePassThroughMode(nt3h2111__Inst* pInst, const uint8_t nc);
uint8_t nt3h2111_setPassThroughRfToI2c(nt3h2111__Inst* pInst, const uint8_t nc);
uint8_t nt3h2111_setPassThroughI2cToRf(nt3h2111__Inst* pInst, const uint8_t nc);
uint8_t nt3h2111_clearDataRead(uint8_t reg);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
