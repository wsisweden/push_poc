#ifndef NFC_NFCMESSAGEHANDLER_H_
#define NFC_NFCMESSAGEHANDLER_H_

#include <Cm.h>
#include <stdint.h>
#include <stdbool.h>
#include <GenericProtocol/GenericReceive.h>
#include <GenericProtocol/GenericPack.h>

// Public definition

#define BUFFER_SIZE 0x200

// Public typedefs

typedef enum {
  NfcMessageStateIdle,
  NfcMessageStateReading,
  NfcMessageStateSending,
  NfcMessageStateBuilding
} NfcMessageState_t;

typedef struct {
  uint8_t readBuffer[BUFFER_SIZE];
  uint8_t writeBuffer[BUFFER_SIZE];
  NfcMessageState_t state;
  GenericReceive_t genericReceive;
  GenericPack_t genericPack;
  cm_Request logRequest;
  uint8_t logBuffer[sizeof(cm_HistRecStd) + 4];
  uint32_t logCount;
  uint32_t logIndex;
  uint32_t logRequestCount;
} NfcMessageHandler_t;

void NfcMessageInit(NfcMessageHandler_t* const pInstance);
bool NfcMessageIsReading(NfcMessageHandler_t* const pInstance);
bool NfcMessageIsSending(NfcMessageHandler_t* const pInstance);
void NfcMessageHandle(NfcMessageHandler_t* const pInstance,
                      uint8_t* payload, const uint32_t count);
void NfcGetSendData(NfcMessageHandler_t* const pInstance,
                    uint8_t* const pBuffer, const uint32_t count);

#endif /* NFC_NFCMESSAGEHANDLER_H_ */
