/*
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Nfc/local.H
*
*	\ingroup	NFC
*
*	\brief		Local declarations for the NFC FB.
*
*	\details
*
*	\note
*
*	\version	14-09-2018 / Andreas Carmvall
*
*******************************************************************************/

#ifndef NFC_LOCAL_H_INCLUDED
#define NFC_LOCAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "_hw.h"

#include "global.h"
#include "nfc.h"

#include "init.h"
#include "Nt3h2111.h"
#include "NfcMessageHandler.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * \name  Task triggers
 */

/*@{*/
#define NFC__TRG_RESET   (1<<4)    /**< Reset signal received      */
#define NFC__TRG_DOWN    (1<<7)    /**< Down signal received.      */
#define NFC__TRG_POLL    (1<<0)    /**< Poll the state machine.    */
/*@}*/

/**
 * \name  FB state flags
 */

/*@{*/
#define NFC__FLG_RUNNING   (1<<0)  /**< NFC is running        */
#define NFC__FLG_DOWN      (1<<6)  /**< NFC is down         */
/*@}*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/**
 * List of states the FB can be in.
 */

typedef enum {
  eNfcStateInitialization,
  eNfcStateWaitForRf,
  eNfcStateActive,
  eNfcStateIdle
} eNfcState_t;

/**
 * List of states the initialization state can be in.
 */

typedef enum {
  eNfcStateInitStart,
  eNfcStateReadUid,
  eNfcStateInitReadNc,
  eNfcStateInitWriteNc,
  eNfcStateInitReadNdefConfig,
  eNfcStateInitWriteNdefConfig,
  eNfcStateInitReadEepromNdef,
  eNfcStateInitWriteEepromNdef,
  eNfcStateInitWriteSramNdef,
  eNfcStateInitIdle
} eNfcStateInitialization_t;

/**
 * List of states the active state can be in.
 */

typedef enum {
  eNfcStateActiveIdle,
  eNfcStateActiveReadNs,
  eNfcStateActiveReadNc,
  eNfcStateActiveCheckState,
  eNfcStateActiveReadData,
  eNfcStateActiveReadMessage,
  eNfcStateActiveMessage,
  eNfcStateActiveMessageWaitDataRead,
  eNfcStateActiveMessageReadDataRead,
  eNfcStateActiveMessageWait
} eNfcStateActive_t;

/**
 * NFC FB instance structure.
 */

typedef struct nfc__inst {	/*''''''''''''''''''''''''''''' RAM	*/
	osa_TaskType		task;		  /**< OSA Task		  		*/
	nfc_Init const_P * pInit;	/**< Initialization data */
	osa_SemaType		taskSema;	/**< Task triggering semaphore */
	sys_FBInstId		nInstId;	/**< Instance ID			*/
	uint8_t					flags;		/**< Status flags			*/
  uint16_t        triggers; /**< Triggers from ISR or write ind */
  Boolean         bTest;    /**< Test variable    */
  nt3h2111__Inst* pNfcDriver; /**< NFC driver     */
  uint32_t        msCounter1; /**< Counter with a 1 ms unit */
  eNfcState_t     state;    /**< FB state         */
  uint8_t         subState; /**< FB sub state     */
  uint8_t         nfcBuffer[EEPROM_NDEF_SIZE + 1];
  uint8_t         ns;
  uint8_t         nc;
  NfcMessageHandler_t messageHandler;
} nfc__Inst;							  /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern nfc__Inst* nfc__pInst;

/*****************************************************************************/

#endif
