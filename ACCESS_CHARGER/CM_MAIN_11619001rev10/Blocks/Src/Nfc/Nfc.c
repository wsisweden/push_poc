/*
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Nfc.c
*
*	\ingroup	NFC
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	14-09-2018 / Andreas Carmvall
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	NFC
*
*	\brief		Nfc handler.
*
********************************************************************************
*
*	\details	Handles NFC communication.
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "swtimer.h"
#include "global.h"
#include "deb.h"
#include "util.h"
#include "tstamp.h"
#include "msg.h"

#include "local.h"
#include "Nfc_Defines.h"
#include "NfcMessageHandler.h"

#include <string.h>

//#define SEGGER

#ifdef SEGGER
#include <TSTAMP.H>
#include <Segger/SEGGER_RTT.h>
static uint16_t lastTimeNfc = 0;
#endif


/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define NFC_STACK_SIZE     1536

static const uint8_t NDEF_INIT_DATA_ADDRESS = 12;
static const uint8_t NDEF_INIT_DATA[] = {
    0xe1, 0x10, 0x6d, 0x00
};

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE OSA_TASK void nfc__task(void);
PRIVATE void nfc_stateMachine(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineInit(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineWaitForRfEnter(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineWaitForRf(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineInitEnter(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineInitReadEepromNdefEnter(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineInitWriteEepromNdefEnter(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineInitWriteSramNdefEnter(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineActive(nfc__Inst* pInst);
PRIVATE void nfc_stateMachineActiveEnter(nfc__Inst* pInst);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * The NFC instance pointer is stored statically so that it can be accessed
 * from the 1m ISR.
 */

PUBLIC nfc__Inst* nfc__pInst = NULL;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nfc_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid		An index (0..) that is accepted by various functions
*						to identify the instance. Should be kept for later use,
*						or may be discarded if not required by the FB.
*	\param		pArgs	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void * nfc_init(
	sys_FBInstId iid,
	void const_P * pArgs
) {
	nfc__Inst* pInst;

	pInst = (nfc__Inst*)mem_reserve(&mem_normal, sizeof(nfc__Inst));

	osa_newTask(
		&pInst->task,
		"nfc__task",
		OSA_PRIORITY_NORMAL,
		nfc__task,
		NFC_STACK_SIZE
	);

  nfc__pInst = pInst;

  pInst->msCounter1 = 0;
	pInst->pInit = (nfc_Init const_P *)pArgs;
	pInst->task.pUtil = pInst;
	pInst->nInstId = iid;
	pInst->flags = 0;
  pInst->bTest = TRUE;
  pInst->pNfcDriver = nt3h2111_init(pInst->pInit);
  NfcMessageInit(&pInst->messageHandler);
  nfc_stateMachineInitEnter(pInst);

	osa_newSemaTaken(&pInst->taskSema);

	return((void *) pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nfc_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		instance	Pointer to instance.
* 	\param		pArgs		Pointer to the initialization arguments.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void nfc_reset(
	void* pInstance,
	void const_P * pArgs
) {	
	nfc__Inst* pInst;

	DUMMY_VAR(pArgs);

	pInst = (nfc__Inst*)pInstance;

	atomic(
		pInst->triggers |= NFC__TRG_RESET;
	);

	osa_semaSet(&pInst->taskSema);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nfc_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		pInstance	Pointer to instance.
*	\param		nType		Down type.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void nfc_down(
	void* pInstance,
	sys_DownType nType
) {
	nfc__Inst* pInst;

	pInst = (nfc__Inst*)pInstance;

	DUMMY_VAR(nType);

	atomic(
		pInst->triggers |= NFC__TRG_DOWN;
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	meas_ctrl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Control interface.
*
*	\param		pInstance	Pointer to GUI instance.
*	\param		nCtrl		Control request type.
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void nfc_ctrl(
	void* pInstance,
	sys_CtrlType* pCtrl
) {
  (void)pInstance;
  (void)pCtrl;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nfc_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read
*				the value of a parameter owned by this FB.
*
*	\param		pInstance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue		Pointer to the storage for the value.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC sys_IndStatus nfc_read(
	void* pInstance,
	sys_IndIndex rIndex,
	sys_ArrIndex aIndex,
	void* pValue
) {
	nfc__Inst* pInst;
	sys_IndStatus ret = REG_OK;

	pInst = (nfc__Inst*)pInstance;

	DUMMY_VAR(pInst);
	DUMMY_VAR(aIndex);

	switch (rIndex) {
    default:
      ret = REG_UNAVAIL;
      break;
	}

	return ret;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nfc_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		pInstance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC sys_IndStatus nfc_write(
	void* pInstance,
	sys_IndIndex rIndex,
	sys_ArrIndex aIndex
) {
	nfc__Inst* pInst;

	pInst = (nfc__Inst*)pInstance;

	DUMMY_VAR(aIndex);

	switch (rIndex) {
		case reg_indic_reset:
			atomic(
				pInst->triggers |= NFC__TRG_RESET;
			);
			break;
	}

	return(REG_OK);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nfc_test
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Self test
*
*	\param		pInstance	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC Uint16 nfc_test(
	void* pInstance
) {
	nfc__Inst* pInst;

	pInst = (nfc__Inst*)pInstance;

	if (pInst->flags & NFC__FLG_RUNNING) {
		if(!(pInst->bTest)) {
			project__errorHandler(PROJECT_ERROR_TEST);
		}

		pInst->bTest = FALSE;
	}

	return sys_msToTestTicks(10000);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nfc__task
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\return		
*
*	\details
*
*	\note		Activated each 10ms.
*
*******************************************************************************/
PRIVATE OSA_TASK void nfc__task(
	void
) {
	nfc__Inst* pInst;
	uint16_t triggers;

	pInst = (nfc__Inst*)osa_taskCurrent()->pUtil;

	/*
	 * Wait for REG to be ready. This includes waiting for FLMEM to be
	 * ready.
	 */
	deb_log("MEAS: Waiting for REG to be operational");
	osa_syncGet(OSA__SYNC_REG);

	FOREVER
	{
		osa_semaGet(&pInst->taskSema);

		pInst->bTest = TRUE;

		atomic(
			triggers = pInst->triggers;
			pInst->triggers = 0;
		);

		/***************************************************************************
		* Check if down signal was received.
		***************************************************************************/
		
		if (triggers & NFC__TRG_DOWN) {
			atomic(
				pInst->flags &= ~NFC__FLG_RUNNING;
				pInst->flags |= NFC__FLG_DOWN;
			);
		}

		/***************************************************************************
		* Check if reset signal has been received.
		***************************************************************************/

		if (triggers & NFC__TRG_RESET) {
			if (pInst->flags == NFC__FLG_DOWN) {
				pInst->flags &= ~NFC__FLG_DOWN;
			}

			pInst->flags |= NFC__FLG_RUNNING;
		}

		/***************************************************************************
		* Check that NFC is running.
		***************************************************************************/

		if not(pInst->flags & NFC__FLG_RUNNING) {
		  continue;
		}

		/***************************************************************************
		* Check that NT3H2111 is working.
		***************************************************************************/

		if (nt3h2111_isError(pInst->pNfcDriver)) {
		  nt3h2111_warmInit(pInst->pNfcDriver);
		  NfcMessageInit(&pInst->messageHandler);
		  nfc_stateMachineInitEnter(pInst);
		}

		/***************************************************************************
		* Poll NFC interface.
		***************************************************************************/

		if (triggers & NFC__TRG_POLL) {
			if (nt3h2111_isIdle(pInst->pNfcDriver)) {
			  nfc_stateMachine(pInst);
			}
		}
	}
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachine
*
*--------------------------------------------------------------------------*//**
*
* \brief    The state machine of the FB.
*
* \return
*
* \details
*
* \note   Polled from ISR.
*
*******************************************************************************/
PRIVATE void nfc_stateMachine(
    nfc__Inst* pInst
) {
  switch (pInst->state) {
    case eNfcStateIdle:
      // Wait for RF when idle
      nfc_stateMachineWaitForRfEnter(pInst);
      break;
    case eNfcStateInitialization:
      nfc_stateMachineInit(pInst);
      break;
    case eNfcStateWaitForRf:
      nfc_stateMachineWaitForRf(pInst);
      break;
    case eNfcStateActive:
      nfc_stateMachineActive(pInst);
      break;
    default:
      // Should not happen
      break;
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineInit
*
*--------------------------------------------------------------------------*//**
*
* \brief    The initialization state machine.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineInit(
    nfc__Inst* pInst
) {
  switch (pInst->subState) {
    case eNfcStateInitIdle:
      // Nothing to do
      break;
    case eNfcStateInitStart:
      pInst->subState = eNfcStateReadUid;
      nt3h2111_readData(pInst->pNfcDriver, 0, 7, pInst->nfcBuffer);
      break;
    case eNfcStateReadUid:
      reg_putLocal(&pInst->nfcBuffer[4], reg_i_NfcInfoH, pInst->nInstId);
      reg_putLocal(pInst->nfcBuffer, reg_i_NfcInfoL, pInst->nInstId);
      pInst->subState = eNfcStateInitReadNc;
      nt3h2111_readReg(pInst->pNfcDriver, eNt3h2111_SessionRegNc);
      break;
    case eNfcStateInitReadNc:
      {
        uint8_t data = nt3h2111_getRegisterValue(pInst->pNfcDriver);
        uint8_t mask = NT3H2111_NC_REG_MASK_I2C_RST_ON_OFF |
            NT3H2111_NC_REG_MASK_SRAM_MIRROR |
            NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF |
            NT3H2111_NC_REG_MASK_FD_OFF | NT3H2111_NC_REG_MASK_FD_ON;

        data &= ~(NT3H2111_NC_REG_MASK_I2C_RST_ON_OFF |
            NT3H2111_NC_REG_MASK_SRAM_MIRROR);
        data |= NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF |
            NT3H2111_NC_REG_MASK_FD_OFF | NT3H2111_NC_REG_MASK_FD_ON;

        pInst->subState = eNfcStateInitWriteNc;
        nt3h2111_writeReg(pInst->pNfcDriver, eNt3h2111_SessionRegNc, mask,
                          data);
        pInst->nc = data;
      }
      break;
    case eNfcStateInitWriteNc:
      pInst->subState = eNfcStateInitReadNdefConfig;
      nt3h2111_readData(pInst->pNfcDriver, NDEF_INIT_DATA_ADDRESS,
                     sizeof(NDEF_INIT_DATA), pInst->nfcBuffer);
      break;
    case eNfcStateInitReadNdefConfig:
      if (memcmp(pInst->nfcBuffer, NDEF_INIT_DATA, sizeof(NDEF_INIT_DATA))
          == 0) {
        nfc_stateMachineInitReadEepromNdefEnter(pInst);
      } else {
        pInst->subState = eNfcStateInitWriteNdefConfig;
        nt3h2111_writeData(pInst->pNfcDriver, NDEF_INIT_DATA_ADDRESS,
                       sizeof(NDEF_INIT_DATA), NDEF_INIT_DATA);
      }
      break;
    case eNfcStateInitWriteNdefConfig:
      nfc_stateMachineInitReadEepromNdefEnter(pInst);
      break;
    case eNfcStateInitReadEepromNdef:
      {
        EepromNdef_t eepromNdef;

        eepromNdef.length = NfcDefineGetNdef(eepromNdef.payload);

        if (memcmp(eepromNdef.payload, pInst->nfcBuffer, eepromNdef.length)
            == 0) {
          nfc_stateMachineInitWriteSramNdefEnter(pInst);
        } else {
          nfc_stateMachineInitWriteEepromNdefEnter(pInst);
        }
      }
      break;
    case eNfcStateInitWriteEepromNdef:
      nfc_stateMachineInitWriteSramNdefEnter(pInst);
      break;
    case eNfcStateInitWriteSramNdef:
      pInst->subState = eNfcStateInitIdle;
      pInst->state = eNfcStateIdle;
      break;
    default:
      // Should not happen
      break;
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineWaitForRfEnter
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enter the wait for RF state.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineWaitForRfEnter(
    nfc__Inst* pInst
) {
  pInst->state = eNfcStateWaitForRf;
  nt3h2111_readReg(pInst->pNfcDriver, eNt3h2111_SessionRegNs);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineWaitForRf
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enter the wait for RF state.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineWaitForRf(
    nfc__Inst* pInst
) {
  uint8_t ns = nt3h2111_getRegisterValue(pInst->pNfcDriver);

  if (nt3h2111_isRfField(pInst->pNfcDriver, ns)) {
    nfc_stateMachineActiveEnter(pInst);
  } else {
    nt3h2111_readReg(pInst->pNfcDriver, eNt3h2111_SessionRegNs);
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineInitEnter
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enter the initialization state machine.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineInitEnter(
    nfc__Inst* pInst
) {
  pInst->state = eNfcStateInitialization;
  pInst->subState = eNfcStateInitStart;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineInitEnter
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enter the initialization state machine.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineInitReadEepromNdefEnter(
    nfc__Inst* pInst
) {
  pInst->subState = eNfcStateInitReadEepromNdef;
  nt3h2111_readEepromNdef(pInst->pNfcDriver,
                          (EepromNdef_t* const)pInst->nfcBuffer);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineInitWriteEepromNdefEnter
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enter the initialization state machine.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineInitWriteEepromNdefEnter(
    nfc__Inst* pInst
) {
  EepromNdef_t* eepromNdef = (EepromNdef_t*)pInst->nfcBuffer;

  pInst->subState = eNfcStateInitWriteEepromNdef;
  eepromNdef->length = NfcDefineGetNdef(eepromNdef->payload);
  nt3h2111_writeEepromNdef(pInst->pNfcDriver, eepromNdef);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineInitWriteSramNdefEnter
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enter the initialization state machine.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineInitWriteSramNdefEnter(
    nfc__Inst* pInst
) {
  SramNdef_t* sramNdef = (SramNdef_t*)pInst->nfcBuffer;

  pInst->subState = eNfcStateInitWriteSramNdef;
  sramNdef->length = NfcDefineGetNdef(sramNdef->payload);
  sramNdef->length = NT3H2111_SIZE_SRAM;
  nt3h2111_writeSramNdef(pInst->pNfcDriver, sramNdef);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineActive
*
*--------------------------------------------------------------------------*//**
*
* \brief    The active state machine.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineActive(
    nfc__Inst* pInst
) {
  switch (pInst->subState) {
    case eNfcStateActiveIdle:
      // Nothing to do
      break;
    case eNfcStateActiveReadNs:
      nt3h2111_readReg(pInst->pNfcDriver, eNt3h2111_SessionRegNs);
      pInst->subState = eNfcStateActiveReadNc;
      break;
    case eNfcStateActiveReadNc:
      pInst->ns = nt3h2111_getRegisterValue(pInst->pNfcDriver);
      nt3h2111_readReg(pInst->pNfcDriver, eNt3h2111_SessionRegNc);
      pInst->subState = eNfcStateActiveCheckState;
      break;
    case eNfcStateActiveCheckState:
      pInst->nc = nt3h2111_getRegisterValue(pInst->pNfcDriver);

      if (!nt3h2111_isRfField(pInst->pNfcDriver, pInst->ns) ||
          !nt3h2111_isPassThroughMode(pInst->pNfcDriver, pInst->nc)) {
#ifdef SEGGER
        uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
        lastTimeNfc = tstamp_getMsCount();

        SEGGER_RTT_printf(0, "%3d, Lost RF field 1\r\n", diff);
#endif

        pInst->nc = nt3h2111_setPassThroughRfToI2c(pInst->pNfcDriver, pInst->nc);
        NfcMessageInit(&pInst->messageHandler);
        pInst->subState = eNfcStateActiveIdle;
        pInst->state = eNfcStateIdle;
      } else {
        if (nt3h2111_isDataAvailable(pInst->pNfcDriver, pInst->ns)) {
          pInst->subState = eNfcStateActiveReadData;
          nt3h2111_readSram(pInst->pNfcDriver, pInst->nfcBuffer);
        } else {
          pInst->subState = eNfcStateActiveReadNs;
        }
      }
      break;
    case eNfcStateActiveReadData:
      NfcMessageHandle(&pInst->messageHandler, pInst->nfcBuffer,
                       NT3H2111_SIZE_SRAM);

      if (NfcMessageIsSending(&pInst->messageHandler)) {
#ifdef SEGGER
        SEGGER_RTT_printf(0, "Enter message state\r\n");
#endif

        pInst->nc = nt3h2111_setPassThroughI2cToRf(pInst->pNfcDriver, pInst->nc);
        pInst->subState = eNfcStateActiveMessage;
      } else if (NfcMessageIsReading(&pInst->messageHandler)) {
#ifdef SEGGER
        SEGGER_RTT_printf(0, "Enter reading message state\r\n");
#endif

        pInst->subState = eNfcStateActiveReadMessage;
      } else {
        pInst->subState = eNfcStateActiveReadNs;
      }
      break;
    case eNfcStateActiveReadMessage:
      if (NfcMessageIsSending(&pInst->messageHandler)) {
#ifdef SEGGER
        uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
        lastTimeNfc = tstamp_getMsCount();

        SEGGER_RTT_printf(0, "%3d, Enter message state\r\n", diff);
#endif

        pInst->nc = nt3h2111_setPassThroughI2cToRf(pInst->pNfcDriver, pInst->nc);
        pInst->subState = eNfcStateActiveMessage;
      }
      break;
    case eNfcStateActiveMessage:
      {
#ifdef SEGGER
        uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
        lastTimeNfc = tstamp_getMsCount();

        SEGGER_RTT_printf(0, "%3d, Send message part\r\n", diff);
#endif

        NfcGetSendData(&pInst->messageHandler, pInst->nfcBuffer,
                       NT3H2111_SIZE_SRAM);
        nt3h2111_writeSram(pInst->pNfcDriver, pInst->nfcBuffer,
                           NT3H2111_SIZE_SRAM);
        pInst->subState = eNfcStateActiveMessageReadDataRead;
      }
      break;
    case eNfcStateActiveMessageReadDataRead:
      {
#ifdef SEGGER
        uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
        lastTimeNfc = tstamp_getMsCount();

        SEGGER_RTT_printf(0, "%3d, Send read NS\r\n", diff);
#endif

        pInst->subState = eNfcStateActiveMessageWaitDataRead;
        pInst->ns = nt3h2111_clearDataRead(pInst->ns);
        nt3h2111_readReg(pInst->pNfcDriver, eNt3h2111_SessionRegNs);
      }
      break;
    case eNfcStateActiveMessageWaitDataRead:
      pInst->ns = nt3h2111_getRegisterValue(pInst->pNfcDriver);

      if (nt3h2111_isRfField(pInst->pNfcDriver, pInst->ns)) {
        if (nt3h2111_isDataRead(pInst->pNfcDriver, pInst->ns)) {
          if (NfcMessageIsSending(&pInst->messageHandler)) {
#ifdef SEGGER
            uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
            lastTimeNfc = tstamp_getMsCount();

            SEGGER_RTT_printf(0, "%3d, Send message part read\r\n", diff);
#endif

            pInst->subState = eNfcStateActiveMessage;
          } else {
#ifdef SEGGER
            uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
            lastTimeNfc = tstamp_getMsCount();

            SEGGER_RTT_printf(0, "%3d, Send message sent\r\n", diff);
#endif

            pInst->nc = nt3h2111_setPassThroughRfToI2c(pInst->pNfcDriver, pInst->nc);
            pInst->subState = eNfcStateActiveReadNs;
          }
        } else {
#ifdef SEGGER
          uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
          lastTimeNfc = tstamp_getMsCount();

          SEGGER_RTT_printf(0, "%3d, Wait some more\r\n", diff);
#endif

          pInst->subState = eNfcStateActiveMessageReadDataRead;
        }
      } else {
#ifdef SEGGER
        uint16_t diff = tstamp_getMsCount() - lastTimeNfc;
        lastTimeNfc = tstamp_getMsCount();

        SEGGER_RTT_printf(0, "%3d, Lost RF field 2\r\n", diff);
#endif

        pInst->nc = nt3h2111_setPassThroughRfToI2c(pInst->pNfcDriver, pInst->nc);
        NfcMessageInit(&pInst->messageHandler);
        pInst->subState = eNfcStateActiveIdle;
        pInst->state = eNfcStateIdle;
      }
      break;
    default:
      // Should not happen
      break;
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nfc_stateMachineActiveEnter
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enter the active state machine.
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nfc_stateMachineActiveEnter(
    nfc__Inst* pInst
) {
  pInst->state = eNfcStateActive;
  pInst->subState = eNfcStateActiveReadNs;
  pInst->nc = nt3h2111_enablePassThroughMode(pInst->pNfcDriver, pInst->nc);
}
