/*
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Nt3h2111.c
*
*	\ingroup	NFC
*
*	\brief		NFC communication driver.
*
*	\details	
*
*	\note
*
*	\version	14-09-2018 / Andreas Carmvall
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "mem.h"
#include "iicmstr.h"
#include "protif.h"

#include "local.h"
#include "init.h"
#include "Nt3h2111.h"

#include <string.h>
#include <Segger/SEGGER_RTT.h>

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * List of states the driver can be in.
 */

typedef enum {
  eNt3h2111StateIdle,
  eNt3h2111StateBusy,
  eNt3h2111StateReadData,
  eNt3h2111StateWriteData,
  eNt3h2111StateError
} eNt3h2111State_t;

typedef enum {
  eNt3h2111StateReadDataIdle,
  eNt3h2111StateReadDataFirst,
  eNt3h2111StateReadDataNext
} eNt3h2111StateReadData_t;

typedef enum {
  eNt3h2111StateWriteDataIdle,
  eNt3h2111StateWriteDataReadBlock,
  eNt3h2111StateWriteDataWrite,
  eNt3h2111StateWriteDataWaitEeprom,
  eNt3h2111StateWriteDataWaitEepromCheck
} eNt3h2111StateWriteData_t;

typedef struct {
  union {
  uint8_t* pBuffer;
  const uint8_t* pData;
  } data;
  uint8_t count;
  uint16_t address;
  uint16_t bytesHandled;
} Nt3h2111_dataRequest_t;

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Nt3h2111 driver instance.
 */

struct nt3h2111__Inst {					/*''''''''''''''''''''''''''''' RAM	*/
	protif_Request	request;	/**< Protocol interface request		*/
	protif_Data			data2;		/**< Second data block				*/
	uint8_t					buffer[NT3H2111_BLOCK_SIZE + 1];
	protif_Interface const_P * pMstrIf;	/**< I2C master interface			*/
	sys_FBInstId		mstrInstId;	/**< I2C master instance id			*/
	eNt3h2111State_t state;		/**< Driver state					*/
	uint8_t         subState;
	Nt3h2111_dataRequest_t dataRequest;
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE bool nt3h2111_isBitsSet(const uint8_t value, const uint8_t bits);
PRIVATE uint8_t nt3h2111_setRegisterBits(nt3h2111__Inst* pInst,
                                      const eNt3h2111_SessionRegister_t reg,
                                      const uint8_t current,
                                      const uint8_t bits);
PRIVATE uint8_t nt3h2111_resetRegisterBits(nt3h2111__Inst* pInst,
                                        const eNt3h2111_SessionRegister_t reg,
                                        const uint8_t current,
                                        const uint8_t bits);
PRIVATE void nt3h2111_readRegister(nt3h2111__Inst* pInst,
                                   const eNt3h2111_SessionRegister_t reg);
PRIVATE void nt3h2111_stateMachine(uint8_t, protif_Request*);
PRIVATE uint8_t nt3h2111_getBlockAddress(const uint16_t address);
PRIVATE void nt3h2111_readBlock(nt3h2111__Inst* pInst,
                                const uint16_t address);
PRIVATE void nt3h2111_prepareWriteBlock(nt3h2111__Inst* pInst,
                                const uint16_t address,
                                const uint8_t* const pData,
                                const uint8_t count);
PRIVATE void nt3h2111_writeBlock(nt3h2111__Inst* pInst,
                                const uint16_t address,
                                const uint8_t* const pData);
PRIVATE void nt3h2111_cycleReadData(nt3h2111__Inst* pInst);
PRIVATE void nt3h2111_cycleReadDataFirst(nt3h2111__Inst* pInst);
PRIVATE void nt3h2111_cycleReadDataNext(nt3h2111__Inst* pInst);
PRIVATE void nt3h2111_cycleWriteData(nt3h2111__Inst* pInst);
PRIVATE void nt3h2111_cycleWriteDataReadBlock(nt3h2111__Inst* pInst);
PRIVATE void nt3h2111_cycleWriteDataWrite(nt3h2111__Inst* pInst);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nt3h2111_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Cold initialize nt3h2111 driver.
*
*	\param		pInit	Pointer to FB initialization structure.
*
*	\return		Pointer to the new allocated driver instance.
*
*	\details	This function will allocate a driver instance and initialize it.
*
*	\note		This function should be called before OSA is running.
*
*******************************************************************************/
PUBLIC nt3h2111__Inst* nt3h2111_init(
	nfc_Init const_P * pInit
) {
  nt3h2111__Inst* pInst;

	pInst = (nt3h2111__Inst*)mem_reserve(&mem_normal, sizeof(nt3h2111__Inst));

	pInst->pMstrIf = pInit->pMstrIf;
	pInst->mstrInstId = pInit->mstrInstId;
	pInst->state = eNt3h2111StateIdle;

	/*
	 * Setup the parts of the request that will remain the same.
	 */

	pInst->request.data.pData = pInst->buffer;
	pInst->request.data.flags = PROTIF_FLAG_WRITE;
  pInst->request.data.pNext = NULL;
  pInst->request.data.size = 0;
	pInst->request.fnCallback = &nt3h2111_stateMachine;
	pInst->request.pTargetInfo = pInit->pNfcSlave;
	pInst->request.pUtil = pInst;

	pInst->data2.flags = PROTIF_FLAG_READ;
	pInst->data2.pData = pInst->buffer;
	pInst->data2.pNext = NULL;
	pInst->data2.size = 1;

	return(pInst);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_warmInit
*
*--------------------------------------------------------------------------*//**
*
* \brief    Warm initialize nt3h2111 driver.
*
* \param    pInit Pointer to driver instance.
*
* \return   -
*
* \details  This function will warm initialize the NFC driver.
*
* \note   This function should be called when OSA is running.
*
*******************************************************************************/
PUBLIC void nt3h2111_warmInit(
    nt3h2111__Inst* pInst
) {
  pInst->state = eNt3h2111StateIdle;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_getRegisterValue
*
*--------------------------------------------------------------------------*//**
*
* \brief    Get the register value.
*
* \param    pInst Pointer to driver instance.
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC uint8_t nt3h2111_getRegisterValue(
  nt3h2111__Inst* pInst
) {
  return pInst->buffer[0];
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_isIdle
*
*--------------------------------------------------------------------------*//**
*
* \brief    Check if in idle state.
*
* \param    pInst Pointer to driver instance.
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool nt3h2111_isIdle(
  nt3h2111__Inst* pInst
) {
  return pInst->state == eNt3h2111StateIdle;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_isError
*
*--------------------------------------------------------------------------*//**
*
* \brief    Check if in error state.
*
* \param    pInst Pointer to driver instance.
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool nt3h2111_isError(
  nt3h2111__Inst* pInst
) {
  return pInst->state == eNt3h2111StateError;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nt3h2111_readReg
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read a register from the chip.
*
*	\param		pInst	Pointer to driver instance.
*	\param		reg		The register address.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void nt3h2111_readReg(
  nt3h2111__Inst* pInst,
  const eNt3h2111_SessionRegister_t reg
) {
  pInst->state = eNt3h2111StateBusy;
  nt3h2111_readRegister(pInst, reg);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_writeReg
*
*--------------------------------------------------------------------------*//**
*
* \brief    Write a register to the chip.
*
* \param    pInst Pointer to driver instance.
* \param    reg   The register address.
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_writeReg(
  nt3h2111__Inst* pInst,
  const eNt3h2111_SessionRegister_t reg,
  const uint8_t mask,
  const uint8_t data
) {
  void* pMstr;

  pInst->buffer[0] = NT3H2111_BLOCK_SESSION_REGS;
  pInst->buffer[1] = reg;
  pInst->buffer[2] = mask;
  pInst->buffer[3] = data;

  pInst->request.data.pNext = NULL;
  pInst->request.data.size = 4;

  pInst->state = eNt3h2111StateBusy;
  pMstr = sys_instIdToTHIS(pInst->mstrInstId);
  pInst->pMstrIf->fnAccess(pMstr, &pInst->request);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_readData
*
*--------------------------------------------------------------------------*//**
*
* \brief    Read data from the chip.
*
* \param    pInst Pointer to driver instance.
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_readData(
  nt3h2111__Inst* pInst,
  const uint16_t address,
  const uint8_t count,
  uint8_t* const pBuffer
) {
  pInst->dataRequest.data.pBuffer = pBuffer;
  pInst->dataRequest.count = count;
  pInst->dataRequest.address = address;
  pInst->dataRequest.bytesHandled = 0;
  pInst->state = eNt3h2111StateReadData;
  pInst->subState = eNt3h2111StateReadDataFirst;
  nt3h2111_readBlock(pInst, address);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_writeData
*
*--------------------------------------------------------------------------*//**
*
* \brief    Write data to the chip.
*
* \param    pInst Pointer to driver instance.
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_writeData(
  nt3h2111__Inst* pInst,
  const uint16_t address,
  const uint8_t count,
  const uint8_t* const pData
) {
  pInst->dataRequest.data.pData = pData;
  pInst->dataRequest.count = count;
  pInst->dataRequest.address = address;
  pInst->dataRequest.bytesHandled = 0;
  pInst->state = eNt3h2111StateWriteData;
  nt3h2111_prepareWriteBlock(pInst, address, pData, count);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_readEepromNdef
*
*--------------------------------------------------------------------------*//**
*
* \brief    Read NDEF from EEPROM.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_readEepromNdef(
    nt3h2111__Inst* pInst,
    EepromNdef_t* const ndef
) {
  nt3h2111_readData(pInst, NT3H2111_START_ADDR_USER_MEMORY,
                           EEPROM_NDEF_SIZE, ndef->payload);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_writeEepromNdef
*
*--------------------------------------------------------------------------*//**
*
* \brief    Write NDEF to EEPROM.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_writeEepromNdef(
    nt3h2111__Inst* pInst,
    const EepromNdef_t* const ndef
) {
  nt3h2111_writeData(pInst, NT3H2111_START_ADDR_USER_MEMORY,
                            ndef->length, ndef->payload);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_writeSramNdef
*
*--------------------------------------------------------------------------*//**
*
* \brief    Write NDEF to EEPROM.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_writeSramNdef(
    nt3h2111__Inst* pInst,
    const SramNdef_t* const ndef
) {
  nt3h2111_writeSram(pInst, ndef->payload, ndef->length);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_readSram
*
*--------------------------------------------------------------------------*//**
*
* \brief    Read the SRAM.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_readSram(
    nt3h2111__Inst* pInst,
    uint8_t* const pBuffer
) {
  nt3h2111_readData(pInst, NT3H2111_START_ADDR_SRAM, NT3H2111_SIZE_SRAM,
                           pBuffer);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_writeSram
*
*--------------------------------------------------------------------------*//**
*
* \brief    Write data to SRAM.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC void nt3h2111_writeSram(
  nt3h2111__Inst* pInst,
  const uint8_t* const pData,
  const uint8_t count
) {
  if (count == 0) {
    // Nothing to do
    return;
  }

  nt3h2111_writeData(pInst, NT3H2111_START_ADDR_SRAM, count, pData);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_isRfField
*
*--------------------------------------------------------------------------*//**
*
* \brief    If the RF field is detected.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool nt3h2111_isRfField(
    nt3h2111__Inst* pInst,
    const uint8_t ns
) {
  return nt3h2111_isBitsSet(ns, NT3H2111_NS_REG_MASK_RF_FIELD_PRESENT);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_isDataAvailable
*
*--------------------------------------------------------------------------*//**
*
* \brief    If data is available.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool nt3h2111_isDataAvailable(
    nt3h2111__Inst* pInst,
    const uint8_t ns
) {
  return nt3h2111_isBitsSet(ns, NT3H2111_NS_REG_MASK_SRAM_I2C_READY);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_isDataRead
*
*--------------------------------------------------------------------------*//**
*
* \brief    If I2C data is read by RF.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool nt3h2111_isDataRead(
    nt3h2111__Inst* pInst,
    const uint8_t ns
) {
  return !nt3h2111_isBitsSet(ns, NT3H2111_NS_REG_MASK_SRAM_RF_READY);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_isPassThroughMode
*
*--------------------------------------------------------------------------*//**
*
* \brief    If pass through mode is enabled.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool nt3h2111_isPassThroughMode(
    nt3h2111__Inst* pInst,
    const uint8_t nc
) {
  return nt3h2111_isBitsSet(nc, NT3H2111_NC_REG_MASK_PTHRU_SRAM);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* Nt3h2111_isReady
*
*--------------------------------------------------------------------------*//**
*
* \brief    If the NFC is ready for write.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool nt3h2111_isReady(
  nt3h2111__Inst* pInst,
  const uint8_t ns
) {
  return !nt3h2111_isBitsSet(ns, NT3H2111_NS_REG_MASK_RF_IF_ON_OFF);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_enablePassThroughMode
*
*--------------------------------------------------------------------------*//**
*
* \brief    Enable the pass through mode.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC uint8_t nt3h2111_enablePassThroughMode(
  nt3h2111__Inst* pInst,
  const uint8_t nc
) {
  return nt3h2111_setRegisterBits(pInst, eNt3h2111_SessionRegNc, nc,
      NT3H2111_NC_REG_MASK_PTHRU_SRAM);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_setPassThroughRfToI2c
*
*--------------------------------------------------------------------------*//**
*
* \brief    Set pass through direction RF to I2C.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC uint8_t nt3h2111_setPassThroughRfToI2c(
  nt3h2111__Inst* pInst,
  const uint8_t nc
) {
  return nt3h2111_setRegisterBits(pInst, eNt3h2111_SessionRegNc, nc,
      NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_setPassThroughI2cToRf
*
*--------------------------------------------------------------------------*//**
*
* \brief    Set pass through direction I2C to RF.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC uint8_t nt3h2111_setPassThroughI2cToRf(
  nt3h2111__Inst* pInst,
  const uint8_t nc
) {
  return nt3h2111_resetRegisterBits(pInst, eNt3h2111_SessionRegNc, nc,
      NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_clearDataRead
*
*--------------------------------------------------------------------------*//**
*
* \brief    Clear the data read value in register.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC uint8_t nt3h2111_clearDataRead(
  uint8_t reg
) {
  return reg | NT3H2111_NS_REG_MASK_SRAM_RF_READY;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_isBitsSet
*
*--------------------------------------------------------------------------*//**
*
* \brief    Check if bits is set.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE bool nt3h2111_isBitsSet(
  const uint8_t value,
  const uint8_t bits
) {
  return (bits & value) == bits;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_setRegisterBits
*
*--------------------------------------------------------------------------*//**
*
* \brief    Set register bits.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE uint8_t nt3h2111_setRegisterBits(
  nt3h2111__Inst* pInst,
  const eNt3h2111_SessionRegister_t reg,
  const uint8_t current,
  const uint8_t bits
) {
  if ((current & bits) != bits) {
    nt3h2111_writeReg(pInst, reg, bits, current | bits);
  }

  return current | bits;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_resetRegisterBits
*
*--------------------------------------------------------------------------*//**
*
* \brief    Reset register bits.
*
* \param
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE uint8_t nt3h2111_resetRegisterBits(
  nt3h2111__Inst* pInst,
  const eNt3h2111_SessionRegister_t reg,
  const uint8_t current,
  const uint8_t bits
  ) {
  if (current & bits) {
    nt3h2111_writeReg(pInst, reg, bits, current & ~bits);
  }

  return current & ~bits;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_readReg
*
*--------------------------------------------------------------------------*//**
*
* \brief    Read a register from the chip.
*
* \param    pInst Pointer to driver instance.
* \param    reg   The register address.
*
* \return   -
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_readRegister(
  nt3h2111__Inst* pInst,
  const eNt3h2111_SessionRegister_t reg
) {
  void* pMstr;

  pInst->buffer[0] = NT3H2111_BLOCK_SESSION_REGS;
  pInst->buffer[1] = reg;

  pInst->request.data.pNext = &pInst->data2;
  pInst->request.data.size = 2;

  pInst->data2.size = 1;

  pMstr = sys_instIdToTHIS(pInst->mstrInstId);
  pInst->pMstrIf->fnAccess(pMstr, &pInst->request);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nt3h2111__stateMachine
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Run the state machine.
*
*	\return		-
*
*	\details	This function is called by the i2c master when the request
*				has been handled.
*
*	\note
*
*******************************************************************************/
PRIVATE void nt3h2111_stateMachine(
	uint8_t	retCode,
	protif_Request* pRequest
) {
  nt3h2111__Inst* pInst;

	pInst = (nt3h2111__Inst*)pRequest->pUtil;

	if (retCode == PROTIF_OK) {
		switch (pInst->state) {
			case eNt3h2111StateIdle:
			  // Nothing to do
				break;
      case eNt3h2111StateBusy:
        pInst->state = eNt3h2111StateIdle;
        break;
      case eNt3h2111StateReadData:
        nt3h2111_cycleReadData(pInst);
        break;
      case eNt3h2111StateWriteData:
        nt3h2111_cycleWriteData(pInst);
        break;
      case eNt3h2111StateError:
        // Nothing to do
        break;
			default:
				deb_assert(FALSE);
				break;
		}
	} else {
	  pInst->state = eNt3h2111StateError;
    pRequest->pNext = NULL;

    SEGGER_RTT_printf(0, "Error code: %d\r\n", retCode);
	}
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_getBlockAddress
*
*--------------------------------------------------------------------------*//**
*
* \brief    Get the block address from the memory address.
*
* \return   The block address
*
* \details  -
*
* \note     -
*
*******************************************************************************/
PRIVATE uint8_t nt3h2111_getBlockAddress(const uint16_t address) {
  return address / NT3H2111_BLOCK_SIZE;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_readBlock
*
*--------------------------------------------------------------------------*//**
*
* \brief    Read a memory block.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_readBlock(nt3h2111__Inst* pInst,
                                const uint16_t address) {
  void* pMstr;

  pInst->buffer[0] = nt3h2111_getBlockAddress(address);

  pInst->request.data.pNext = &pInst->data2;
  pInst->request.data.size = 1;

  pInst->data2.size = NT3H2111_BLOCK_SIZE;

  pMstr = sys_instIdToTHIS(pInst->mstrInstId);
  pInst->pMstrIf->fnAccess(pMstr, &pInst->request);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_prepareWriteBlock
*
*--------------------------------------------------------------------------*//**
*
* \brief    Prepare write to a memory block.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_prepareWriteBlock(nt3h2111__Inst* pInst,
                                const uint16_t address,
                                const uint8_t* const pData,
                                const uint8_t count) {
  uint8_t offset = address % NT3H2111_BLOCK_SIZE;
  uint8_t blockCount = MIN(count, NT3H2111_BLOCK_SIZE - offset);

  if (blockCount < NT3H2111_BLOCK_SIZE) {
    pInst->subState = eNt3h2111StateWriteDataReadBlock;
    nt3h2111_readBlock(pInst, address);
  } else {
    pInst->subState = eNt3h2111StateWriteDataWrite;
    pInst->dataRequest.bytesHandled += NT3H2111_BLOCK_SIZE;
    nt3h2111_writeBlock(pInst, address, pData);
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_writeBlock
*
*--------------------------------------------------------------------------*//**
*
* \brief    Write a memory block.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_writeBlock(nt3h2111__Inst* pInst,
                                const uint16_t address,
                                const uint8_t* const pData) {
  void* pMstr;
  uint8_t offset = address % NT3H2111_BLOCK_SIZE;

  pInst->buffer[0] = nt3h2111_getBlockAddress(address);
  memcpy(&pInst->buffer[1], pData, NT3H2111_BLOCK_SIZE);

  // Don't overwrite the I2C address
  if ((NT3H2111_BLOCK_MANAGEMENT == pInst->buffer[0]) &&
      (NT3H2111_I2C_ADDRESS_SELECT < offset))
    pInst->buffer[1] = (NT3H2111_ADDRESS << 1);

  pInst->request.data.pNext = NULL;
  pInst->request.data.size = NT3H2111_BLOCK_SIZE + 1;

  // Don't wait if block is in SRAM
  if (pInst->buffer[0] < NT3H2111_BLOCK_START_ADDR_SRAM || pInst->buffer[0] > NT3H2111_BLOCK_END_ADDR_SRAM)
    pInst->subState = eNt3h2111StateWriteDataWaitEeprom;

  pMstr = sys_instIdToTHIS(pInst->mstrInstId);
  pInst->pMstrIf->fnAccess(pMstr, &pInst->request);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_cycleReadData
*
*--------------------------------------------------------------------------*//**
*
* \brief    Cycle the read data state.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_cycleReadData(nt3h2111__Inst* pInst) {
  switch (pInst->subState) {
    case eNt3h2111StateReadDataIdle:
      // Nothing to do
      break;
    case eNt3h2111StateReadDataFirst:
      nt3h2111_cycleReadDataFirst(pInst);
      break;
    case eNt3h2111StateReadDataNext:
      nt3h2111_cycleReadDataNext(pInst);
      break;
    default:
      deb_assert(FALSE);
      break;
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_cycleReadDataFirst
*
*--------------------------------------------------------------------------*//**
*
* \brief    Cycle the read data first state.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_cycleReadDataFirst(nt3h2111__Inst* pInst) {
  uint16_t offset = pInst->dataRequest.address % NT3H2111_BLOCK_SIZE;
  uint8_t blockCount = MIN(pInst->dataRequest.count,
                           NT3H2111_BLOCK_SIZE - offset);

  memcpy(&pInst->dataRequest.data.pBuffer[pInst->dataRequest.bytesHandled],
         &pInst->buffer[offset], blockCount);
  pInst->dataRequest.bytesHandled = blockCount;

  if (pInst->dataRequest.bytesHandled >= pInst->dataRequest.count) {
    pInst->state = eNt3h2111StateIdle;
    pInst->subState = eNt3h2111StateReadDataIdle;
  } else {
    pInst->subState = eNt3h2111StateReadDataNext;
    nt3h2111_readBlock(pInst, pInst->dataRequest.address +
                       pInst->dataRequest.bytesHandled);
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_cycleReadDataNext
*
*--------------------------------------------------------------------------*//**
*
* \brief    Cycle the read data next state.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_cycleReadDataNext(nt3h2111__Inst* pInst) {
  uint8_t blockCount = MIN(
      pInst->dataRequest.count - pInst->dataRequest.bytesHandled,
      NT3H2111_BLOCK_SIZE);

  memcpy(&pInst->dataRequest.data.pBuffer[pInst->dataRequest.bytesHandled],
         pInst->buffer, blockCount);
  pInst->dataRequest.bytesHandled += blockCount;

  if (pInst->dataRequest.bytesHandled >= pInst->dataRequest.count) {
    pInst->state = eNt3h2111StateIdle;
    pInst->subState = eNt3h2111StateReadDataIdle;
  } else {
    nt3h2111_readBlock(pInst, pInst->dataRequest.address +
                       pInst->dataRequest.bytesHandled);
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_cycleWriteData
*
*--------------------------------------------------------------------------*//**
*
* \brief    Cycle the write data state.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_cycleWriteData(nt3h2111__Inst* pInst) {
  switch (pInst->subState) {
    case eNt3h2111StateWriteDataIdle:
      // Nothing to do
      break;
    case eNt3h2111StateWriteDataReadBlock:
      nt3h2111_cycleWriteDataReadBlock(pInst);
      break;
    case eNt3h2111StateWriteDataWrite:
      nt3h2111_cycleWriteDataWrite(pInst);
      break;
    case eNt3h2111StateWriteDataWaitEeprom:
      pInst->subState = eNt3h2111StateWriteDataWaitEepromCheck;
      nt3h2111_readRegister(pInst, eNt3h2111_SessionRegNs);
      break;
    case eNt3h2111StateWriteDataWaitEepromCheck:
      if (nt3h2111_getRegisterValue(pInst) &
          NT3H2111_NS_REG_MASK_EEPROM_WR_BUSY) {
        nt3h2111_readRegister(pInst, eNt3h2111_SessionRegNs);
      } else {
        pInst->subState = eNt3h2111StateWriteDataWrite;
        nt3h2111_cycleWriteDataWrite(pInst);
      }
      break;
    default:
      deb_assert(FALSE);
      break;
  }
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_cycleWriteDataReadBlock
*
*--------------------------------------------------------------------------*//**
*
* \brief    Cycle the write data read block state.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_cycleWriteDataReadBlock(nt3h2111__Inst* pInst) {
  uint8_t blockData[NT3H2111_BLOCK_SIZE];
  uint16_t address = pInst->dataRequest.address +
      pInst->dataRequest.bytesHandled;
  uint8_t offset = address % NT3H2111_BLOCK_SIZE;
  uint8_t blockCount = MIN(
      pInst->dataRequest.count - pInst->dataRequest.bytesHandled,
      NT3H2111_BLOCK_SIZE - offset);

  memcpy(blockData, pInst->buffer, NT3H2111_BLOCK_SIZE);
  memcpy(&blockData[offset],
         &pInst->dataRequest.data.pData[pInst->dataRequest.bytesHandled],
         blockCount);

  pInst->dataRequest.bytesHandled += blockCount;
  pInst->subState = eNt3h2111StateWriteDataWrite;
  nt3h2111_writeBlock(pInst, address, blockData);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* nt3h2111_cycleWriteDataWrite
*
*--------------------------------------------------------------------------*//**
*
* \brief    Cycle the write data write state.
*
* \return   -
*
* \details  -
*
* \note
*
*******************************************************************************/
PRIVATE void nt3h2111_cycleWriteDataWrite(nt3h2111__Inst* pInst) {
  if (pInst->dataRequest.bytesHandled >= pInst->dataRequest.count) {
    pInst->state = eNt3h2111StateIdle;
    pInst->subState = eNt3h2111StateWriteDataIdle;
  } else {
    nt3h2111_prepareWriteBlock(pInst,
             pInst->dataRequest.address + pInst->dataRequest.bytesHandled,
             &pInst->dataRequest.data.pData[pInst->dataRequest.bytesHandled],
             pInst->dataRequest.count - pInst->dataRequest.bytesHandled);
  }
}
