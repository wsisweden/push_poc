#include "Nfc_Defines.h"

#include <string.h>
#include <stdio.h>

// NDEF handling see https://learn.adafruit.com/adafruit-pn532-rfid-nfc/ndef for documentation

// Constant values
const uint8_t HEADER_MESSAGE_BEGIN    = 0x80;
const uint8_t HEADER_MESSAGE_END      = 0x40;
const uint8_t HEADER_CHUNK_FLAG       = 0x20;
const uint8_t HEADER_SHORT_RECORD     = 0x10;
const uint8_t HEADER_ID_LENGTH        = 0x08;
const uint8_t HEADER_TNF_EMPTY        = 0x00;
const uint8_t HEADER_TNF_WELL_KNOWN   = 0x01;
const uint8_t HEADER_TNF_MIME         = 0x02;
const uint8_t HEADER_TNF_ABSOLUTE_URI = 0x03;
const uint8_t HEADER_TNF_EXTERNAL     = 0x04;
const uint8_t HEADER_TNF_UNKNOWN      = 0x05;
const uint8_t HEADER_TNF_UNCHANGED    = 0x06;

const uint8_t NDEF_START              = 0x03;
const uint8_t NDEF_LENGTH_POS         = 0x01;
const uint8_t NDEF_RECORD_POS         = 0x02;
const uint8_t NDEF_END                = 0xfe;

const uint8_t WELL_KNOWN_URI_TYPE     = 0x55;
const uint8_t WELL_KNOWN_URI_HTTP     = 0x03;

// Records
// External type record
const char* const EXTERNAL_TYPE = "com.micropower_group:cirrus";

// Well known type record
const char* const WELL_KNOWN_PAYLOAD = "docs.micropower-group.com/#!doc?view=list&$Product_id=ProductID_";

// External type 2 record
const char* const EXTERNAL_TYPE_2 = "android.com:pkg";
const char* const EXTERNAL_PAYLOAD_2 = "com.micropower_group.cirrus";

/**
 * Get the NDEF message
 * @param buffer The NDEF buffer
 * @return The NDEF length
 */
uint8_t NfcDefineGetNdef(uint8_t* const buffer) {
  uint8_t length = 0; // NDEF length - 3 (SOF + EOF + this byte)
  uint8_t idLength;
  uint16_t productId = 9;
  char productIdString[6];

  // Get product ID in string
  sprintf(productIdString, "%d", productId);
  idLength = strlen(productIdString);

  // Start of NDEF
  buffer[0] = NDEF_START;

  // Add records

  // Add external type record

  // Header
  buffer[NDEF_RECORD_POS + length++] = HEADER_MESSAGE_BEGIN | HEADER_SHORT_RECORD | HEADER_TNF_EXTERNAL;
  // Type length
  buffer[NDEF_RECORD_POS + length++] = (uint8_t)strlen(EXTERNAL_TYPE);
  // Payload length
  buffer[NDEF_RECORD_POS + length++] = 0;
  // Type
  strcpy((char*)&buffer[NDEF_RECORD_POS + length], EXTERNAL_TYPE);
  length += buffer[NDEF_RECORD_POS + length - 2];

  // Add well known type record

  // Header
  buffer[NDEF_RECORD_POS + length++] = HEADER_SHORT_RECORD | HEADER_TNF_WELL_KNOWN;
  // Type length
  buffer[NDEF_RECORD_POS + length++] = 1;
  // Payload length
  buffer[NDEF_RECORD_POS + length++] = 1 + strlen(WELL_KNOWN_PAYLOAD) + idLength; // Payload start byte + payload + id length
  // Type
  buffer[NDEF_RECORD_POS + length++] = WELL_KNOWN_URI_TYPE;
  // Payload
  buffer[NDEF_RECORD_POS + length++] = WELL_KNOWN_URI_HTTP;
  strcpy((char*)&buffer[NDEF_RECORD_POS + length], WELL_KNOWN_PAYLOAD);
  length += strlen(WELL_KNOWN_PAYLOAD);
  strcpy((char*)&buffer[NDEF_RECORD_POS + length], productIdString);
  length += idLength;

  // Add second external type record

  // Header
  buffer[NDEF_RECORD_POS + length++] = HEADER_MESSAGE_END | HEADER_SHORT_RECORD | HEADER_TNF_EXTERNAL;
  // Type length
  buffer[NDEF_RECORD_POS + length++] = (uint8_t)strlen(EXTERNAL_TYPE_2);
  // Payload length
  buffer[NDEF_RECORD_POS + length++] = (uint8_t)strlen(EXTERNAL_PAYLOAD_2);
  // Type
  strcpy((char*)&buffer[NDEF_RECORD_POS + length], EXTERNAL_TYPE_2);
  length += buffer[NDEF_RECORD_POS + length - 2];
  // Payload
  strcpy((char*)&buffer[NDEF_RECORD_POS + length], EXTERNAL_PAYLOAD_2);
  length += (uint8_t)strlen(EXTERNAL_PAYLOAD_2);

  // Add length byte
  buffer[NDEF_LENGTH_POS] = length;

  // End of NDEF
  buffer[NDEF_RECORD_POS + length] = NDEF_END;

  return NDEF_RECORD_POS + length + 1; // Start of message + length + end of message
}
