#include "NfcMessageHandler.h"
#include "../Chalg/inc_cm3/cai_constparam.h"

#include <tools.h>
#include <sys.h>
#include <reg.h>
#include <sup.h>
#include <chalg.h>
#include <global.h>
#include <pb_encode.h>
#include <GenericProtocol/GenericProtocol.h>
#include <GenericProtocol/ProtocolBuffers/Gen/AcknowledgeResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/ReadRequest.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/WriteRequest.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/CapabilityResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/InfoResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/AccessConfigResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/AccessStatusResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/ChargerLogResponse.pb.h>
#include <string.h>
#include <math.h>

//#define SEGGER

#ifdef SEGGER
#include <TSTAMP.H>
#include <Segger/SEGGER_RTT.h>
static uint16_t lastTime = 0;
#endif

// Private definition

#define IS_BIT_SET(value, bit) ((value & bit) == bit)

// Private typedefs

// Private variables

// Private function declarations

static void NfcMessageSendingCycle(NfcMessageHandler_t* const pInstance);
static void NfcMessageHandleReadRequest(NfcMessageHandler_t* const pInstance);
static void NfcMessageHandleWriteRequest(NfcMessageHandler_t* const pInstance);
static void NfcMessageSend(NfcMessageHandler_t* const pInstance);
static void NfcSendAcknowledge(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericType_t type);
static void NfcSendReadAcknowledge(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericProtocol_ReadRequest_Type type);
static void NfcSendWriteAcknowledge(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericProtocol_WriteRequest_Type type);
static void NfcHandleReadLog(NfcMessageHandler_t* const pInstance,
                              const GenericProtocol_LogRequest* const log);
static void NfcHandleReadCapability(NfcMessageHandler_t* const pInstance);
static void NfcHandleReadInfo(NfcMessageHandler_t* const pInstance);
static void NfcHandleReadStatus(NfcMessageHandler_t* const pInstance);
static void NfcHandleReadConfig(NfcMessageHandler_t* const pInstance);
static void NfcHandleReadChargingCurve(NfcMessageHandler_t* const pInstance,
                                       const uint32_t index);
static void NfcHandleReadUserParameter(NfcMessageHandler_t* const pInstance,
                         const GenericProtocol_UserParameterData* const data);
static void NfcHandleWriteTime(NfcMessageHandler_t* const pInstance,
                             const GenericProtocol_TimeRequest* const request);
static void NfcHandleWriteConfig(NfcMessageHandler_t* const pInstance,
    GenericProtocol_AccessConfigRequest* const request,
    GenericProtocol_WriteRequest* const message);
static void NfcHandleWriteCalibration(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_ChargerCalibrationRequest* const request);
static void NfcHandleWriteClearStorage(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_ClearStorageRequest* const request);
static void NfcHandleWriteResetDevice(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_ResetDeviceRequest* const request);
static void NfcHandleWriteUserParameter(NfcMessageHandler_t* const pInstance,
    GenericProtocol_UserParameterRequest* const request,
    GenericProtocol_WriteRequest* const message);
static bool NfcMessageHandlerListAlgorithmsCallback(pb_ostream_t *stream,
                                                    const pb_field_t *field,
                                                    void * const *arg);
static bool NfcMessageHandlerListPinSelectCallback(pb_ostream_t *stream,
                                                    const pb_field_t *field,
                                                    void * const *arg);
static bool NfcMessageHandlerChargingRestrictionCallback(pb_ostream_t *stream,
                                                    const pb_field_t *field,
                                                    void * const *arg);
static bool NfcMessageHandlerEncodeUserParameterName(pb_ostream_t *stream,
                                                     const pb_field_t *field,
                                                     void * const *arg);
static bool NfcMessageHandlerWritePinInSelectCallback(pb_istream_t *stream,
                                               const pb_field_t *field,
                                               void **arg);
static bool NfcMessageHandlerWritePinOutSelectCallback(pb_istream_t *stream,
                                               const pb_field_t *field,
                                               void **arg);
static bool NfcMessageHandlerWriteChargingRestrictionCallback(
    pb_istream_t *stream, const pb_field_t *field, void **arg);
static bool NfcMessageHandlerWriteUserParameterCallback(
    pb_istream_t *stream, const pb_field_t *field, void **arg);
static bool NfcMessageHandlerListStatusErrorCallback(pb_ostream_t *stream,
                                                     const pb_field_t *field,
                                                     void * const *arg);
static bool NfcMessageHandlerStatusCallback(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            void * const *arg);
static bool NfcMessageHandlerListUserParameterCurvesCallback(
    pb_ostream_t *stream, const pb_field_t *field, void * const *arg);
static bool NfcMessageHandlerEncodeU32Field(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            const uint32_t value);
static bool NfcMessageHandlerEncodeChargingRestrictionField(
    pb_ostream_t *stream, const pb_field_t *field,
    const GenericProtocol_ChargingRestriction* value);
static float NfcMessageConvertToCelsius(const float fahrenheit);
static float NfcMessageConvertToFahrenheit(const float celsius);
static void NfcMessageSendLogNack(NfcMessageHandler_t* const pInstance,
                         const GenericProtocol_AcknowledgeResponse_Type code);
static void NfcHandleLogCallback(cm_Request* pRequest);
static void NfcHandleSendEvent(cm_Request* pRequest,
                               GenericProtocol_LogResponse* message);
static void NfcHandleSendInstant(cm_Request* pRequest,
                               GenericProtocol_LogResponse* message);
static void NfcHandleSendHistory(cm_Request* pRequest,
                               GenericProtocol_LogResponse* message);
static bool NfcMessageHandlerInstantLogErrorCallback(pb_ostream_t *stream,
                                                     const pb_field_t *field,
                                                     void * const *arg);
static bool NfcMessageHandlerEventDataCallback(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            void * const *arg);
static bool NfcMessageHandlerEncodeGenericAlarm(pb_ostream_t *stream,
                                                const pb_field_t *field,
                                                const uint32_t code,
                                                const bool active);
static bool NfcMessageHandlerEventAlarmCallback(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            void * const *arg);
static bool NfcMessageHandlerListHistoryErrorCallback(pb_ostream_t *stream,
                                                      const pb_field_t *field,
                                                      void * const *arg);
static bool NfcMessageHandlerListHistoryStatusCallback(pb_ostream_t *stream,
                                                       const pb_field_t *field,
                                                       void * const *arg);

// Public functions

/**
 * Initialize the message handler
 */
void NfcMessageInit(NfcMessageHandler_t* const pInstance) {
  pInstance->state = NfcMessageStateIdle;

  GenericReceiveInit(&pInstance->genericReceive);
  GenericReceiveSetBuffer(&pInstance->genericReceive, pInstance->readBuffer,
                          sizeof(pInstance->readBuffer));

  GenericPackInit(&pInstance->genericPack, pInstance->writeBuffer,
                  sizeof(pInstance->writeBuffer));
}

/**
 * Check if message is reading
 */
bool NfcMessageIsReading(NfcMessageHandler_t* const pInstance) {
  return pInstance->state == NfcMessageStateReading;
}

/**
 * Check if message is sending
 */
bool NfcMessageIsSending(NfcMessageHandler_t* const pInstance) {
  if (pInstance->state == NfcMessageStateSending) {
    NfcMessageSendingCycle(pInstance);
  }

  return pInstance->state == NfcMessageStateSending;
}

/**
 * Handle a message received by NFC
 * @param payload The data
 * @param count The data length
 */
void NfcMessageHandle(NfcMessageHandler_t* const pInstance,
                      uint8_t* payload, const uint32_t count) {
  GenericReceiveAddData(&pInstance->genericReceive, payload, count);

  if (GenericReceiveIsComplete(&pInstance->genericReceive) !=
      GenericReceiveErrorLength) {
    pInstance->state = NfcMessageStateIdle;
    GenericPackStartFrame(&pInstance->genericPack);

    do {
      GenericType_t type = GenericReceiveGetCmdType(&pInstance->genericReceive);
      GenericReceiveError_t error = GenericReceiveIsCmdValid(
          &pInstance->genericReceive);

      if (error == GenericReceiveErrorOk) {
        switch (type) {
          case GenericTypeReadReq:
            NfcMessageHandleReadRequest(pInstance);
            break;
          case GenericTypeWriteReq:
            NfcMessageHandleWriteRequest(pInstance);
            break;
          default:
            // Not a valid command
            NfcSendAcknowledge(pInstance,
                GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND, type);
            break;
        }
      } else if (error == GenericReceiveErrorCrc) {
        NfcSendAcknowledge(pInstance,
                           GenericProtocol_AcknowledgeResponse_Type_CRC,
                           GenericTypeInvalid);
      } else if (error == GenericReceiveErrorLength) {
        NfcSendAcknowledge(pInstance,
                           GenericProtocol_AcknowledgeResponse_Type_DATA_LENGTH,
                           GenericTypeInvalid);
      }
    } while (GenericReceiveNextCmd(&pInstance->genericReceive));

    GenericReceiveClear(&pInstance->genericReceive);

    if (GenericPackIsFrameStarted(&pInstance->genericPack)) {
      GenericPackEndFrame(&pInstance->genericPack);
      NfcMessageSend(pInstance);
    }
  } else {
    pInstance->state = NfcMessageStateBuilding;
  }
}

/**
 * Get send data
 */
void NfcGetSendData(NfcMessageHandler_t* const pInstance,
                            uint8_t* const pBuffer, const uint32_t count) {
  GenericPackGetFrameChunk(&pInstance->genericPack, pBuffer, count);
}

// Private functions

/**
 * Cycle the sending state
 */
static void NfcMessageSendingCycle(NfcMessageHandler_t* const pInstance) {
  if (!GenericPackIsChunkLeft(&pInstance->genericPack)) {
    pInstance->state = NfcMessageStateIdle;
    return;
  }
}

/**
 * Handle a command received by NFC
 */
static void NfcMessageHandleReadRequest(NfcMessageHandler_t* const pInstance) {
  GenericProtocol_ReadRequest message =
  GenericProtocol_ReadRequest_init_default;
  bool status = GenericReceiveDecode(&pInstance->genericReceive,
                                     GenericProtocol_ReadRequest_fields,
                                     &message);

  // Check if message was parsed OK
  if (!status) {
    NfcSendAcknowledge(pInstance,
                       GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
                       GenericTypeReadReq);
    return;
  }

  switch (message.type) {
    case GenericProtocol_ReadRequest_Type_INFO:
      NfcHandleReadInfo(pInstance);
      break;
    case GenericProtocol_ReadRequest_Type_STATUS:
      NfcHandleReadStatus(pInstance);
      break;
    case GenericProtocol_ReadRequest_Type_CONFIG:
      NfcHandleReadConfig(pInstance);
      break;
    case GenericProtocol_ReadRequest_Type_LOG:
      NfcHandleReadLog(pInstance, &message.log);
      break;
    case GenericProtocol_ReadRequest_Type_CAPABILITY:
      NfcHandleReadCapability(pInstance);
      break;
    case GenericProtocol_ReadRequest_Type_CHARGING_CURVE:
      NfcHandleReadChargingCurve(pInstance, message.index);
      break;
    case GenericProtocol_ReadRequest_Type_USER_PARAMETER:
          NfcHandleReadUserParameter(pInstance, &message.userParameter);
          break;
    default:
      // Command not supported
      NfcSendAcknowledge(pInstance,
          GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
          GenericTypeReadReq);
      break;
  }
}

/**
 * Handle a command received by NFC
 */
static void NfcMessageHandleWriteRequest(NfcMessageHandler_t* const pInstance) {
  GenericProtocol_WriteRequest message =
      GenericProtocol_WriteRequest_init_default;
  bool status = GenericReceiveDecode(&pInstance->genericReceive,
                                     GenericProtocol_WriteRequest_fields,
                                     &message);

  if (status)
    status = GenericReceiveGetSubstreamFromRequest(&pInstance->genericReceive,
                                          GenericProtocol_WriteRequest_fields);

  // Check if message was parsed OK
  if (!status) {
    NfcSendAcknowledge(pInstance,
                       GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
                       GenericTypeWriteReq);
    return;
  }

  switch (message.type) {
    case GenericProtocol_WriteRequest_Type_TIME:
      NfcHandleWriteTime(pInstance, &message.data.time);
      break;
    case GenericProtocol_WriteRequest_Type_ACCESS_CONFIG:
      NfcHandleWriteConfig(pInstance, &message.data.accessConfig, &message);
      break;
    case GenericProtocol_WriteRequest_Type_CHARGER_CALIBRATION:
      NfcHandleWriteCalibration(pInstance, &message.data.chargerCalibration);
      break;
    case GenericProtocol_WriteRequest_Type_CLEAR_STORAGE:
      NfcHandleWriteClearStorage(pInstance, &message.data.clearStorage);
      break;
    case GenericProtocol_WriteRequest_Type_RESET_DEVICE:
      NfcHandleWriteResetDevice(pInstance, &message.data.resetDevice);
      break;
    case GenericProtocol_WriteRequest_Type_USER_PARAMETER:
      NfcHandleWriteUserParameter(pInstance, &message.data.userParameter,
                                  &message);
      break;
    default:
      // Command not supported
      NfcSendAcknowledge(pInstance,
          GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
          GenericTypeWriteReq);
      break;
  }
}

/**
 * Start sending the NFC message
 */
static void NfcMessageSend(NfcMessageHandler_t* const pInstance) {
  pInstance->state = NfcMessageStateSending;
}

/**
 * Send an acknowledge with a return code
 */
static void NfcSendAcknowledge(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericType_t type) {
  GenericProtocol_AcknowledgeResponse message =
  GenericProtocol_AcknowledgeResponse_init_default;

  message.type = code;
  message.genericType = type;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeAcknowledgeResp,
                    GenericProtocol_AcknowledgeResponse_fields, &message);
}

/**
 * Send an acknowledge with a return code
 */
static void NfcSendReadAcknowledge(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericProtocol_ReadRequest_Type type) {
  GenericProtocol_AcknowledgeResponse message =
  GenericProtocol_AcknowledgeResponse_init_default;

  message.type = code;
  message.genericType = GenericTypeReadReq;
  message.readType = type;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeAcknowledgeResp,
                    GenericProtocol_AcknowledgeResponse_fields, &message);
}

/**
 * Send an acknowledge with a return code
 */
static void NfcSendWriteAcknowledge(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericProtocol_WriteRequest_Type type) {
  GenericProtocol_AcknowledgeResponse message =
  GenericProtocol_AcknowledgeResponse_init_default;

  message.type = code;
  message.genericType = GenericTypeWriteReq;
  message.writeType = type;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeAcknowledgeResp,
                    GenericProtocol_AcknowledgeResponse_fields, &message);
}

/**
 * Handle the read log command
 */
static void NfcHandleReadLog(NfcMessageHandler_t* const pInstance,
                              const GenericProtocol_LogRequest* const log) {
  uint32_t headIndex = 0;
  uint32_t readCount = log->count;
  uint16_t logCount = 0;

  pInstance->logRequest.flags = CM_REQFLG_READ;
  pInstance->logRequest.pDoneFn = &NfcHandleLogCallback;
  pInstance->logRequest.pRecord = pInstance->logBuffer;
  pInstance->logRequest.pUtil = pInstance;
  pInstance->logRequest.size = 0; // set size of read record to zero
  pInstance->logRequest.index = log->index;

  switch (log->type) {
    case GenericProtocol_LogRequest_Type_EVENT:
      pInstance->logRequest.logType = CM_LOGTYPE_EVENT;
      reg_get(&headIndex, nfc__EventHeadIndex);
      reg_get(&logCount, nfc__EventCount);

      if (readCount > 5) {
    	  readCount = 5;
      }

      pInstance->logRequest.index = log->index - readCount + 1;
      break;
    case GenericProtocol_LogRequest_Type_HISTORY:
      pInstance->logRequest.logType = CM_LOGTYPE_HISTORICAL;
      reg_get(&headIndex, nfc__HistoryHeadIndex);
      reg_get(&logCount, nfc__HistoryCount);
      break;
    case GenericProtocol_LogRequest_Type_INSTANT:
      pInstance->logRequest.logType = CM_LOGTYPE_INSTANT;
      reg_get(&headIndex, nfc__InstantHeadIndex);
      reg_get(&logCount, nfc__InstantCount);
      break;
    default:
      // Command not supported
      NfcSendReadAcknowledge(pInstance,
          GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
          GenericProtocol_ReadRequest_Type_LOG);

#ifdef SEGGER
      SEGGER_RTT_printf(0, "Log type not supported: %d\r\n", log->type);
#endif
      return;
  }

#ifdef SEGGER
  {
    uint16_t diff = tstamp_getMsCount() - lastTime;
    lastTime = tstamp_getMsCount();

    SEGGER_RTT_printf(0, "%3d, Read logs:\r\n", diff);
    SEGGER_RTT_printf(0, " - Type: %d\r\n", pInstance->logRequest.logType);
    SEGGER_RTT_printf(0, " - Count: %d\r\n", log->count);
    SEGGER_RTT_printf(0, " - Index: %d\r\n", log->index);
  }
#endif

  // Validate parameters
  if ((log->index > headIndex)
      || ((log->index - readCount + 1) < (headIndex - logCount + 1))) {
    NfcSendReadAcknowledge(pInstance,
        GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
        GenericProtocol_ReadRequest_Type_LOG);

#ifdef SEGGER
    SEGGER_RTT_printf(0, "Log index and count validated false\r\n");
#endif
    return;
  }

  if (log->type == GenericProtocol_LogRequest_Type_EVENT) {
	  pInstance->logIndex = log->index + 1;
  } else {
	  pInstance->logIndex = log->index - 1;
  }

  pInstance->logCount = readCount - 1;
  pInstance->logRequestCount = pInstance->logCount;

  cm_processRequest(&pInstance->logRequest);
  pInstance->state = NfcMessageStateReading;
}

/**
 * Handle the read device capability command
 */
static void NfcHandleReadCapability(NfcMessageHandler_t* const pInstance) {
  GenericProtocol_CapabilityResponse message =
  GenericProtocol_CapabilityResponse_init_default;

  message.config = true;
  message.info = true;
  message.status = true;
  message.log = true;
  message.logCapability.event = true;
  message.logCapability.history = true;
  message.logCapability.instant = true;
  message.calibration = true;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeCapabilityResp,
                    GenericProtocol_CapabilityResponse_fields, &message);
}

/**
 * Handle the read device info command
 */
static void NfcHandleReadInfo(NfcMessageHandler_t* const pInstance) {
  GenericProtocol_InfoResponse message =
  GenericProtocol_InfoResponse_init_default;
  uint32_t firmwareVersionMain;
  uint32_t firmwareTypeMain;

  reg_get(&firmwareVersionMain, nfc__FirmwareVersionMain);
  reg_get(&firmwareTypeMain, nfc__FirmwareTypeMain);

  message.firmwareType = firmwareTypeMain;
  message.firmwareVersion = firmwareVersionMain;
  message.mui = project_getMui();
  message.protocolVersion = PROTOCOL_VERSION;
  message.productId = GenericProtocol_ProductId_ACCESS;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeInfoResp,
                    GenericProtocol_InfoResponse_fields,
                    &message);
}

/**
 * Handle the read status command
 */
static void NfcHandleReadStatus(NfcMessageHandler_t* const pInstance) {
  GenericProtocol_AccessStatusResponse message =
      GenericProtocol_AccessStatusResponse_init_default;
  int16_t heatSinkTemperature;
  int32_t chargerVoltage;
  int32_t chargerCurrent;
  int32_t batteryVoltage;
  int32_t batteryCurrent;
  uint32_t deviceTime;
  char algorithmName[9];
  uint16_t algorithmNumber;
  uint16_t algorithmVersion;
  uint16_t capacity;
  uint16_t cableResistance;
  uint16_t cells;
  uint16_t baseLoad;
  uint16_t cycles2To25;
  uint16_t cycles26To50;
  uint16_t cycles51To80;
  uint16_t cycles81To90;
  uint16_t cyclesAbove90;
  uint32_t totalChargeTime;
  uint32_t totalChargeAh;
  uint32_t totalAcConsumtion;
  uint32_t acPowerConsumption;
  uint16_t overVoltageCount;
  uint32_t historyTime;
  uint32_t eventTime;
  uint32_t historyHeadIndex;
  uint32_t eventHeadIndex;
  uint32_t instantHeadIndex;
  uint16_t historyCount;
  uint16_t eventCount;
  uint16_t instantCount;
  uint8_t language;
  uint8_t channel;
  uint16_t panId;
  uint32_t radioFirmwareType;
  uint32_t radioFirmwareVersion;

  memset(algorithmName, '\0', sizeof(algorithmName));

  reg_get(&heatSinkTemperature, nfc__HeatSinkTemperature);
  reg_get(&chargerVoltage, nfc__ChargerVoltage);
  reg_get(&chargerCurrent, nfc__ChargerCurrent);
  reg_get(&batteryVoltage, nfc__BatteryVoltage);
  reg_get(&batteryCurrent, nfc__BatteryCurrent);
  reg_get(&deviceTime, nfc__DeviceTime);
  reg_get(algorithmName, nfc__AlgorithmName);
  reg_get(&algorithmNumber, nfc__AlgorithmNumber);
  reg_get(&algorithmVersion, nfc__AlgorithmVersion);
  reg_get(&capacity, nfc__Capacity);
  reg_get(&cableResistance, nfc__CableResistance);
  reg_get(&cells, nfc__Cells);
  reg_get(&baseLoad, nfc__BaseLoad);
  reg_get(&cycles2To25, nfc__Cycles2To25);
  reg_get(&cycles26To50, nfc__Cycles26To50);
  reg_get(&cycles51To80, nfc__Cycles51To80);
  reg_get(&cycles81To90, nfc__Cycles81To90);
  reg_get(&cyclesAbove90, nfc__CyclesAbove90);
  reg_get(&totalChargeTime, nfc__TotalChargeTime);
  reg_get(&totalChargeAh, nfc__TotalChargeAh);
  reg_get(&totalAcConsumtion, nfc__TotalAcConsumtion);
  reg_get(&acPowerConsumption, nfc__AcPowerConsumption);
  reg_get(&overVoltageCount, nfc__OverVoltageCount);
  reg_get(&historyTime, nfc__HistoryTime);
  reg_get(&eventTime, nfc__EventTime);
  reg_get(&historyHeadIndex, nfc__HistoryHeadIndex);
  reg_get(&eventHeadIndex, nfc__EventHeadIndex);
  reg_get(&instantHeadIndex, nfc__InstantHeadIndex);
  reg_get(&historyCount, nfc__HistoryCount);
  reg_get(&eventCount, nfc__EventCount);
  reg_get(&instantCount, nfc__InstantCount);
  reg_get(&language, nfc__Language);
  reg_get(&channel, nfc__ActiveRadioChannel);
  reg_get(&panId, nfc__ActiveRadioPanId);
  reg_get(&radioFirmwareType, nfc__RadioFirmwareType);
  reg_get(&radioFirmwareVersion, nfc__RadioFirmwareVersion);

  message.status.chargerTemperature = ((float)heatSinkTemperature) / 10.0;

  // If language is US, convert to celcius
  if (language == 2) {
    message.status.chargerTemperature =
        NfcMessageConvertToCelsius(message.status.chargerTemperature);
  }

  message.status.chargerCurrent = ((float)chargerCurrent) / 1000.0;
  message.status.chargerVoltage = ((float)chargerVoltage) / 1000.0;
  message.status.batteryCurrent = ((float)batteryCurrent) / 1000.0;
  message.status.batteryVoltage = ((float)batteryVoltage) / 1000.0;
  message.status.deviceTime = deviceTime;
  memcpy(message.status.algorithmName, &algorithmName[1], 8);
  message.status.algorithmNumber = algorithmNumber;
  message.status.algorithmVersion = algorithmVersion;
  message.status.capacity = capacity;
  message.status.cableResistance = ((float)cableResistance) / 1000.0;
  message.status.cells = cells;
  message.status.baseLoad = ((float)baseLoad) / 1000.0;
  message.status.cycles2To25 = cycles2To25;
  message.status.cycles26To50 = cycles26To50;
  message.status.cycles51To80 = cycles51To80;
  message.status.cycles81To90 = cycles81To90;
  message.status.cyclesAbove90 = cyclesAbove90;
  message.status.totalAcConsumtion = totalAcConsumtion;
  message.status.totalChargeAh = totalChargeAh;
  message.status.totalChargeTime = totalChargeTime;
  message.status.acPowerConsumption = acPowerConsumption;
  message.status.overVoltageCount = overVoltageCount;
  message.status.eventLogCount = eventCount;
  message.status.eventLogIndex = eventHeadIndex - eventCount + 1;
  message.status.eventLogDate = eventTime;
  message.status.instantLogCount = instantCount;
  message.status.instantLogIndex = instantHeadIndex - instantCount + 1;
  message.status.historyLogCount = historyCount;
  message.status.historyLogIndex = historyHeadIndex - historyCount + 1;
  message.status.historyLogDate = historyTime;
  message.panId = panId;
  message.channel = channel;
  message.radioFirmwareType = radioFirmwareType;
  message.radioFirmwareVersion = radioFirmwareVersion;

  message.status.alarmCodes.funcs.encode = &NfcMessageHandlerListStatusErrorCallback;
  message.status.deviceStatus.funcs.encode = &NfcMessageHandlerStatusCallback;
  message.userParameterCurves.funcs.encode =
      &NfcMessageHandlerListUserParameterCurvesCallback;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeAccessStatusResp,
                    GenericProtocol_AccessStatusResponse_fields, &message);
}

/**
 * Handle the read configuration command
 */
static void NfcHandleReadConfig(NfcMessageHandler_t* const pInstance) {
  GenericProtocol_AccessConfigResponse message =
  GenericProtocol_AccessConfigResponse_init_default;
  uint32_t temp32;
  uint16_t temp16;
  uint8_t temp8;
  sys_RegHandle pinInSelect;
  sys_RegHandle pinOutSelect;

  reg_get(&temp32, nfc__EngineCode);
  message.engineCode = temp32;

  reg_get(&temp16, nfc__DefAlgorithmNumber);
  message.algorithmNumber = temp16;

  reg_get(&temp16, nfc__DefCapacity);
  message.capacity = temp16;

  reg_get(&temp16, nfc__DefCableResistance);
  message.cableResistance = ((float)temp16) / 1000.0;

  reg_get(&temp16, nfc__DefCells);
  message.cells = temp16;

  reg_get(&temp16, nfc__DefBaseLoad);
  message.baseLoad = ((float)temp16) / 1000.0;

  reg_get(&temp32, nfc__ChargerId);
  message.chargerId = temp32;

  reg_get(&temp32, nfc__IDcLimit);
  message.iDcLimit = temp32;

  reg_get(&temp16, nfc__InstantLogPeriod);
  message.instantLogPeriod = temp16;

  reg_get(&temp32, nfc__SecurityCode1);
  message.securityCode1 = temp32;

  reg_get(&temp32, nfc__SecurityCode2);
  message.securityCode2 = temp32;

  reg_get(&temp8, nfc__DefBatteryType);
  message.batteryType = temp8;

  reg_get(&temp8, nfc__Language);
  message.language = temp8;

  reg_get(&temp16, nfc__DefBatteryTemperature);
  message.batteryTemperature = ((float)temp16) / 10.0;

  // If language is US, convert to celcius
  if (message.language == 2) {
    message.batteryTemperature =
        NfcMessageConvertToCelsius(message.batteryTemperature);
  }

  reg_get(&temp8, nfc__BitConfig);
  message.cecMode = (temp8 & CHALG_BITCFG_CEC) == CHALG_BITCFG_CEC;
  message.userParamMode = (temp8 & CHALG_BITCFG_USER_P) == CHALG_BITCFG_USER_P;
  message.quietDerate = (temp8 & CHALG_BITCFG_QUIET_DERATE) == CHALG_BITCFG_QUIET_DERATE;
  message.dplMode = (temp8 & CHALG_BITCFG_DPL) == CHALG_BITCFG_DPL;

  reg_get(&temp16, nfc__RadioPanId);
  message.radioPanId = temp16;

  reg_get(&temp8, nfc__RadioChannel);
  message.radioChannel = temp8 + 11; // Send the correct channel

  reg_get(&temp16, nfc__RadioNodeAddress);
  message.radioNodeAddress = temp16;

  reg_get(&temp32, nfc__IAcLimit);
  message.mainsCurrentLimit = temp32;

  reg_get(&temp32, nfc__PAcLimit);
  message.mainsPowerLimit = temp32;

  reg_get(&temp8, nfc__PowerGroup);
  message.powerGroup = temp8;

  reg_get(&temp8, nfc__DisplayContrast);
  message.displayContrast = temp8;

  reg_get(&temp8, nfc__LedBrightnessMax);
  message.ledBrightnessMax = temp8;

  reg_get(&temp8, nfc__LedBrightnessDim);
  message.ledBrightnessDim = temp8;

  reg_get(&temp8, nfc__TimeDateFormat);
  message.timeDateFormat = temp8;

  reg_get(&temp8, nfc__ButtonF1Function);
  message.buttonF1Function = temp8;

  reg_get(&temp8, nfc__ButtonF2Function);
  message.buttonF2Function = temp8;

  reg_get(&temp8, nfc__RemoteInFunction);
  message.remoteInFunction = temp8;

  reg_get(&temp8, nfc__RemoteOutFunction);
  message.remoteOutFunction = temp8;

  reg_get(&temp8, nfc__RemoteOutAlarmVariable);
  message.remoteOutAlarmVariable = temp8;

  reg_get(&temp8, nfc__RemoteOutPhaseVariable);
  message.remoteOutPhaseVariable = temp8;

  reg_get(&temp8, nfc__WateringFunction);
  message.wateringFunction = temp8;

  reg_get(&temp8, nfc__WateringVariable);
  message.wateringVariable = temp8;

  reg_get(&temp8, nfc__AirPumpVariable);
  message.airPumpVariable = temp8;

  reg_get(&temp8, nfc__AirPumpVariable2);
  message.airPumpVariable2 = temp8;

  reg_get(&temp8, nfc__EqualizeFunction);
  message.equalizeFunction = temp8;

  reg_get(&temp8, nfc__EqualizeVariable);
  message.equalizeVariable = temp8;

  reg_get(&temp8, nfc__EqualizeVariable2);
  message.equalizeVariable2 = temp8;

#if PROJECT_USE_CAN == 1
  reg_get(&temp8, nfc__ParallelControlFunction);
  message.parallelControlFunction = temp8;

  reg_get(&temp8, nfc__CanMode);
  message.canMode = temp8;

  reg_get(&temp16, nfc__CanNetworkControl);
  message.canNetworkControl = temp16;

  reg_get(&temp8, nfc__CanBitPerSecond);
  message.canBps = temp8;

  reg_get(&temp8, nfc__CanNodeId);
  message.canNodeId = temp8;
#endif

  reg_get(&temp8, nfc__TimeRestriction);
  message.timeRestriction = temp8 == 1;

  reg_get(&temp8, nfc__RadioMode);
  message.radioMode = temp8;

  reg_get(&temp8, nfc__RadioNetworkSettings);
  message.radioNetworkSettings = temp8;

  reg_get(&temp8, nfc__BacklightTime);
  message.backlightTime = temp8;

  reg_get(&temp8, nfc__ExtraChargeFunction);
  message.extraChargeFunction = temp8;

  reg_get(&temp8, nfc__ExtraChargeVariable);
  message.extraChargeVariable = temp8;

  reg_get(&temp8, nfc__ExtraChargeVariable2);
  message.extraChargeVariable2 = temp8;

  reg_get(&temp8, nfc__ExtraChargeVariable3);
  message.extraChargeVariable3 = temp8;

  reg_get(&temp16, nfc__AirPumpAlarmLow);
  message.airPumpAlarmLow = temp16;

  reg_get(&temp16, nfc__AirPumpAlarmHigh);
  message.airPumpAlarmHigh = temp16;

  reg_get(&temp8, nfc__DefChargingMode);
  message.chargingMode = temp8;

  reg_get(&temp8, nfc__BbcFunction);
  message.bbcFunction = temp8;

  reg_get(&temp8, nfc__BbcVariable);
  message.bbcVariable = temp8;

  reg_get(&temp8, nfc__Routing);
  message.routing = temp8;

  reg_get(&temp8, nfc__RemoteOutBbcVariable);
  message.remoteOutBbcVariable = temp8;

  reg_get(&temp32, nfc__DplPowerLimitTotal);
  message.dplPowerLimitTotal = temp32;

  reg_get(&temp8, nfc__DplFunction);
  message.dplFunction = temp8;

  reg_get(&temp8, nfc__DplPriorityFactor);
  message.dplPriorityFactor = temp8;

  reg_get(&temp8, nfc__DplPowerLimitDefault);
  message.dplPowerLimitDefault = temp8;

  message.chargingAlgorithms.funcs.encode =
      &NfcMessageHandlerListAlgorithmsCallback;

  message.pinInSelect.funcs.encode =
      &NfcMessageHandlerListPinSelectCallback;
  pinInSelect = nfc__PinInSelect;
  message.pinInSelect.arg = &pinInSelect;

  message.pinOutSelect.funcs.encode =
      &NfcMessageHandlerListPinSelectCallback;
  pinOutSelect = nfc__PinOutSelect;
  message.pinOutSelect.arg = &pinOutSelect;

  message.chargingRestrictions.funcs.encode =
      &NfcMessageHandlerChargingRestrictionCallback;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeAccessConfigResp,
                    GenericProtocol_AccessConfigResponse_fields, &message);
}

/**
 * Handle the read charging curve command
 */
static void NfcHandleReadChargingCurve(NfcMessageHandler_t* const pInstance,
                                       const uint32_t index) {
  GenericProtocol_ChargingCurveResponse message =
      GenericProtocol_ChargingCurveResponse_init_default;
  AlgDef_ConstPar_Type* curveData;
  Identity_ConstPar_Type* curveInfo;
  Mandatory_ConstPar_Type* mandatory;

  if (!chalg_GetCurveData(index, (void**)&curveData)) {
    NfcSendReadAcknowledge(pInstance,
            GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
            GenericProtocol_ReadRequest_Type_CHARGING_CURVE);
    return;
  }

  curveInfo = (Identity_ConstPar_Type *)curveData->Identity_ConstPar_Addr;
  mandatory = (Mandatory_ConstPar_Type*)curveData->Mandatory_ConstPar_Addr;

  message.chargingCurve = curveInfo->IdNumber;
  message.revision = curveInfo->Version;
  message.userParameterCount = mandatory->UserParam.Size;

  GenericPackAddCmd(&pInstance->genericPack, GenericTypeChargingCurveResp,
                    GenericProtocol_ChargingCurveResponse_fields, &message);
}

/**
 * Handle the read user parameter command
 */
static void NfcHandleReadUserParameter(NfcMessageHandler_t* const pInstance,
                     const GenericProtocol_UserParameterData* const data) {
  GenericProtocol_UserParameterResponse message =
      GenericProtocol_UserParameterResponse_init_default;
  AlgDef_ConstPar_Type* curveData;
  Identity_ConstPar_Type* curveInfo;
  Mandatory_ConstPar_Type* mandatory;
  UserParam_ConstParam_Type* parameters;
  uint32_t i;
  uint16_t storedAlgorithmId;
  uint16_t storedAlgorithmVer;

  if (!chalg_GetCurveData(data->curveIndex, (void**)&curveData)) {
    NfcSendReadAcknowledge(pInstance,
            GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
            GenericProtocol_ReadRequest_Type_CHARGING_CURVE);
    return;
  }

  curveInfo = (Identity_ConstPar_Type *)curveData->Identity_ConstPar_Addr;
  mandatory = (Mandatory_ConstPar_Type*)curveData->Mandatory_ConstPar_Addr;
  parameters = (UserParam_ConstParam_Type*)mandatory->UserParam.Addr;

  if (data->index + data->count > mandatory->UserParam.Size) {
    NfcSendReadAcknowledge(pInstance,
            GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
            GenericProtocol_ReadRequest_Type_CHARGING_CURVE);
    return;
  }

  reg_get(&storedAlgorithmId, nfc__storedAlgorithmID);
  reg_get(&storedAlgorithmVer, nfc__storedAlgorithmVer);

  for (i = 0; i < data->count; ++i) {
    message.index = data->index + i;
    message.defaultValue = parameters[data->index + i].Default;
    message.maxValue = parameters[data->index + i].Max;
    message.minValue = parameters[data->index + i].Min;
    message.type =
        (GenericProtocol_UserParameterResponse_Type)parameters[data->index + i].Unit;

    if (curveInfo->IdNumber == storedAlgorithmId &&
    		curveInfo->Version == storedAlgorithmVer) {
      reg_aGet(&message.value, nfc__storedUserParam, data->index + i);
    } else {
      message.value = NAN;
    }

    message.name.arg = &parameters[data->index + i];
    message.name.funcs.encode = &NfcMessageHandlerEncodeUserParameterName;

    GenericPackAddCmd(&pInstance->genericPack, GenericTypeUserParameterResp,
                      GenericProtocol_UserParameterResponse_fields, &message);
  }
}

/**
 * Handle the write time command
 */
static void NfcHandleWriteTime(NfcMessageHandler_t* const pInstance,
                               const GenericProtocol_TimeRequest* const request) {
  reg_Status status = reg_put((void*)&(request->time), nfc__SetRTC);
  GenericProtocol_AcknowledgeResponse_Type returnValue = status == REG_OK ?
      GenericProtocol_AcknowledgeResponse_Type_OK :
      GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS;

  NfcSendWriteAcknowledge(pInstance, returnValue,
                          GenericProtocol_WriteRequest_Type_TIME);
}

/**
 * Handle the write configuration command
 */
static void NfcHandleWriteConfig(NfcMessageHandler_t* const pInstance,
    GenericProtocol_AccessConfigRequest* const request,
    GenericProtocol_WriteRequest* const message) {
  volatile uint16_t status = REG_OK;
  uint8_t temp8;
  uint16_t temp16;
  uint32_t temp32;
  float tempFloat;
  uint32_t pinInSelectIndex = 0;
  uint32_t pinOutSelectIndex = 0;
  uint8_t chargingRestrictionsIndex[7];
  uint8_t control = 0;

  memset(chargingRestrictionsIndex, 0, sizeof(chargingRestrictionsIndex));

  request->pinInSelect.funcs.decode = &NfcMessageHandlerWritePinInSelectCallback;
  request->pinInSelect.arg = &pinInSelectIndex;
  request->pinOutSelect.funcs.decode = &NfcMessageHandlerWritePinOutSelectCallback;
  request->pinOutSelect.arg = &pinOutSelectIndex;
  request->chargingRestrictions.funcs.decode = &NfcMessageHandlerWriteChargingRestrictionCallback;
  request->chargingRestrictions.arg = chargingRestrictionsIndex;

  for (temp8 = 0; temp8 < 21; ++temp8) {
    reg_aPut(&control, nfc__TimeTableControl, temp8);
  }

  status = GenericReceiveDecodeSubstream(&pInstance->genericReceive,
                                         GenericProtocol_AccessConfigRequest_fields,
                                         &message->data.accessConfig);

  if (!status) {
    NfcSendAcknowledge(pInstance,
              GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
              GenericProtocol_WriteRequest_Type_ACCESS_CONFIG);
    return;
  }

  status = REG_OK;

  project_storeEngineCode(request->engineCode);
  chalg_UpdateAlgIdx(request->algorithmNumber);

  temp16 = (uint16_t)request->capacity;
  status |= reg_put(&temp16, nfc__DefCapacity);

  temp16 = (uint16_t)request->cells;
  status |= reg_put(&temp16, nfc__DefCells);

  temp16 = (uint16_t)(request->cableResistance * 1000);
  status |= reg_put(&temp16, nfc__DefCableResistance);

  temp16 = (uint16_t)(request->baseLoad * 1000);
  status |= reg_put(&temp16, nfc__DefBaseLoad);

  temp32 = (uint32_t)request->iDcLimit;
  status |= reg_put(&temp32, nfc__IDcLimit);

  temp32 = request->securityCode1;
  status |= reg_put(&temp32, nfc__SecurityCode1);

  temp32 = request->securityCode2;
  status |= reg_put(&temp32, nfc__SecurityCode2);

  temp16 = (uint16_t)request->instantLogPeriod;
  status |= reg_put(&temp16, nfc__InstantLogPeriod);

  temp32 = request->chargerId;
  status |= reg_put(&temp32, nfc__ChargerId);

  temp8 = (uint8_t)request->batteryType;
  status |= reg_put(&temp8, nfc__DefBatteryType);

  temp8 = (uint8_t)request->language;
  status |= reg_put(&temp8, nfc__Language);

  // If language is US, convert to fahrenheit
  if (request->language == 2) {
    tempFloat = NfcMessageConvertToFahrenheit(request->batteryTemperature);
  } else {
    tempFloat = request->batteryTemperature;
  }

  temp16 = (uint16_t)(tempFloat * 10);
  status |= reg_put(&temp16, nfc__DefBatteryTemperature);

  temp8 = 0;
  if (request->cecMode)
    temp8 |= CHALG_BITCFG_CEC;
  if (request->userParamMode)
    temp8 |= CHALG_BITCFG_USER_P;
  if (request->quietDerate)
    temp8 |= CHALG_BITCFG_QUIET_DERATE;
  if (request->dplMode)
    temp8 |= CHALG_BITCFG_DPL;
  status |= reg_put(&temp8, nfc__BitConfig);

  temp16 = (uint16_t)request->radioPanId;
  status |= reg_put(&temp16, nfc__RadioPanId);

  temp8 = (uint8_t)(request->radioChannel - 11);
  status |= reg_put(&temp8, nfc__RadioChannel);

  temp16 = (uint16_t)request->radioNodeAddress;
  status |= reg_put(&temp16, nfc__RadioNodeAddress);

  temp32 = request->mainsCurrentLimit;
  status |= reg_put(&temp32, nfc__IAcLimit);

  temp32 = request->mainsPowerLimit;
  status |= reg_put(&temp32, nfc__PAcLimit);

  temp8 = (uint8_t)request->powerGroup;
  status |= reg_put(&temp8, nfc__PowerGroup);

  temp8 = (uint8_t)request->displayContrast;
  status |= reg_put(&temp8, nfc__DisplayContrast);

  temp8 = (uint8_t)request->ledBrightnessMax;
  status |= reg_put(&temp8, nfc__LedBrightnessMax);

  temp8 = (uint8_t)request->ledBrightnessDim;
  status |= reg_put(&temp8, nfc__LedBrightnessDim);

  temp8 = (uint8_t)request->timeDateFormat;
  status |= reg_put(&temp8, nfc__TimeDateFormat);

  temp8 = (uint8_t)request->buttonF1Function;
  status |= reg_put(&temp8, nfc__ButtonF1Function);

  temp8 = (uint8_t)request->buttonF2Function;
  status |= reg_put(&temp8, nfc__ButtonF2Function);

  temp8 = (uint8_t)request->remoteInFunction;
  status |= reg_put(&temp8, nfc__RemoteInFunction);

  temp8 = (uint8_t)request->remoteOutFunction;
  status |= reg_put(&temp8, nfc__RemoteOutFunction);

  temp8 = (uint8_t)request->remoteOutAlarmVariable;
  status |= reg_put(&temp8, nfc__RemoteOutAlarmVariable);

  temp8 = (uint8_t)request->remoteOutPhaseVariable;
  status |= reg_put(&temp8, nfc__RemoteOutPhaseVariable);

  temp8 = (uint8_t)request->wateringFunction;
  status |= reg_put(&temp8, nfc__WateringFunction);

  temp8 = (uint8_t)request->wateringVariable;
  status |= reg_put(&temp8, nfc__WateringVariable);

  temp8 = (uint8_t)request->airPumpVariable;
  status |= reg_put(&temp8, nfc__AirPumpVariable);

  temp8 = (uint8_t)request->airPumpVariable2;
  status |= reg_put(&temp8, nfc__AirPumpVariable2);

  temp8 = (uint8_t)request->equalizeFunction;
  status |= reg_put(&temp8, nfc__EqualizeFunction);

  temp8 = (uint8_t)request->equalizeVariable;
  status |= reg_put(&temp8, nfc__EqualizeVariable);

  temp8 = (uint8_t)request->equalizeVariable2;
  status |= reg_put(&temp8, nfc__EqualizeVariable2);

#if PROJECT_USE_CAN == 1
  temp8 = (uint8_t)request->parallelControlFunction;
  status |= reg_put(&temp8, nfc__ParallelControlFunction);

  temp8 = (uint8_t)request->canMode;
  status |= reg_put(&temp8, nfc__CanMode);

  temp16 = (uint16_t)request->canNetworkControl;
  status |= reg_put(&temp16, nfc__CanNetworkControl);

  temp8 = (uint8_t)request->canBps;
  status |= reg_put(&temp8, nfc__CanBitPerSecond);

  temp8 = (uint8_t)request->canNodeId;
  status |= reg_put(&temp8, nfc__CanNodeId);
#endif

  temp8 = (uint8_t)request->timeRestriction;
  status |= reg_put(&temp8, nfc__TimeRestriction);

  temp8 = (uint8_t)request->radioMode;
  status |= reg_put(&temp8, nfc__RadioMode);

  temp8 = (uint8_t)request->radioNetworkSettings;
  status |= reg_put(&temp8, nfc__RadioNetworkSettings);

  temp8 = (uint8_t)request->backlightTime;
  status |= reg_put(&temp8, nfc__BacklightTime);

  temp8 = (uint8_t)request->extraChargeFunction;
  status |= reg_put(&temp8, nfc__ExtraChargeFunction);

  temp8 = (uint8_t)request->extraChargeVariable;
  status |= reg_put(&temp8, nfc__ExtraChargeVariable);

  temp8 = (uint8_t)request->extraChargeVariable2;
  status |= reg_put(&temp8, nfc__ExtraChargeVariable2);

  temp8 = (uint8_t)request->extraChargeVariable3;
  status |= reg_put(&temp8, nfc__ExtraChargeVariable3);

  temp16 = (uint16_t)request->airPumpAlarmLow;
  status |= reg_put(&temp16, nfc__AirPumpAlarmLow);

  temp16 = (uint16_t)request->airPumpAlarmHigh;
  status |= reg_put(&temp16, nfc__AirPumpAlarmHigh);

  temp8 = (uint8_t)request->chargingMode;
  status |= reg_put(&temp8, nfc__DefChargingMode);

  temp8 = (uint8_t)request->bbcFunction;
  status |= reg_put(&temp8, nfc__BbcFunction);

  temp8 = (uint8_t)request->bbcVariable;
  status |= reg_put(&temp8, nfc__BbcVariable);

  temp8 = (uint8_t)request->routing;
  status |= reg_put(&temp8, nfc__Routing);

  temp8 = (uint8_t)request->remoteOutBbcVariable;
  status |= reg_put(&temp8, nfc__RemoteOutBbcVariable);

  temp32 = request->dplPowerLimitTotal;
  status |= reg_put(&temp32, nfc__DplPowerLimitTotal);

  temp8 = (uint8_t)request->dplFunction;
  status |= reg_put(&temp8, nfc__DplFunction);

  temp8 = (uint8_t)request->dplPriorityFactor;
  status |= reg_put(&temp8, nfc__DplPriorityFactor);

  temp8 = (uint8_t)request->dplPowerLimitDefault;
  status |= reg_put(&temp8, nfc__DplPowerLimitDefault);

  if (status == REG_OK) {
    project_storeBatteryInfo(TRUE);
  }

  /* Trigger silent start of network if radio parameters changed */
  temp8 = 1;
  reg_put(&temp8,	nfc__RadioNwkParam);

  NfcSendWriteAcknowledge(pInstance,
                          GenericProtocol_AcknowledgeResponse_Type_OK,
                          GenericProtocol_WriteRequest_Type_ACCESS_CONFIG);
}

/**
 * Handle the write calibration command
 */
static void NfcHandleWriteCalibration(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_ChargerCalibrationRequest* const request) {
  int32_t measuredVoltage;
  int32_t measuredCurrent;
  int32_t calibrationCurrent = (int32_t)(request->current * 1000);
  int32_t calibrationVoltage = (int32_t)(request->voltage * 1000);
  float voltage;
  float current;
  float voltageLimit;
  float currentLimit;

  reg_get(&measuredVoltage, nfc__ChargerVoltage);
  reg_get(&measuredCurrent, nfc__ChargerCurrent);

  voltage = measuredVoltage / 1000.0;
  current = measuredCurrent / 1000.0;
  voltageLimit = voltage * 0.15;
  currentLimit = current * 0.15;

  if ((request->current > 0) && (request->current <= (current + currentLimit))
      && (request->current >= (current - currentLimit))) {
    reg_put(&calibrationCurrent, nfc__SetCalibrationCurrent);
  }

  if ((request->voltage > 0) && (request->voltage <= (voltage + voltageLimit))
      && (request->voltage >= (voltage - voltageLimit))) {
    reg_put(&calibrationVoltage, nfc__SetCalibrationVoltage);
  }

    NfcSendWriteAcknowledge(pInstance,
        GenericProtocol_AcknowledgeResponse_Type_OK,
        GenericProtocol_WriteRequest_Type_CHARGER_CALIBRATION);
}
/**
 * Handle the write clear storage command
 */
static void NfcHandleWriteClearStorage(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_ClearStorageRequest* const request) {
  GenericProtocol_ClearStorageRequest_Type type = request->type;
  uint8_t clearFlag = 1;
  reg_Status status = REG_OK;

  switch (type) {
    case GenericProtocol_ClearStorageRequest_Type_ALL:
      status = reg_put(&clearFlag, nfc__ClearStatistics);
      break;
    default:
      NfcSendWriteAcknowledge(pInstance,
          GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
          GenericProtocol_WriteRequest_Type_CLEAR_STORAGE);
      return;
  }

  NfcSendWriteAcknowledge(pInstance,
                          status == REG_OK ?
                              GenericProtocol_AcknowledgeResponse_Type_OK :
                              GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
                          GenericProtocol_WriteRequest_Type_CLEAR_STORAGE);
}

/**
 * Handle the write reset device command
 */
static void NfcHandleWriteResetDevice(NfcMessageHandler_t* const pInstance,
    const GenericProtocol_ResetDeviceRequest* const request) {
  GenericProtocol_ResetDeviceRequest_Type type = request->type;
  uint8_t clearFlag = 1;
  reg_Status status = REG_OK;

  switch (type) {
    case GenericProtocol_ResetDeviceRequest_Type_ALL:
      status |= reg_put(&clearFlag, nfc__FactoryDefaults);
      break;
    default:
      NfcSendWriteAcknowledge(pInstance,
          GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
          GenericProtocol_WriteRequest_Type_RESET_DEVICE);
      return;
  }

  NfcSendWriteAcknowledge(pInstance,
                          status == REG_OK ?
                              GenericProtocol_AcknowledgeResponse_Type_OK :
                              GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
                          GenericProtocol_WriteRequest_Type_RESET_DEVICE);
}

/**
 * Handle the write user parameter command
 */
static void NfcHandleWriteUserParameter(NfcMessageHandler_t* const pInstance,
    GenericProtocol_UserParameterRequest* const request,
    GenericProtocol_WriteRequest* const message) {
  bool parseOk;
  AlgDef_ConstPar_Type* curveData;
  Identity_ConstPar_Type* curveInfo;
  uint16_t temp16;
  uint16_t storedAlgorithmID;
  uint16_t storedAlgorithmVer;

  if (!chalg_GetCurveData(request->chargingCurve, (void**)&curveData)) {
    NfcSendAcknowledge(pInstance,
              GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
              GenericProtocol_WriteRequest_Type_USER_PARAMETER);
    return;
  }

  curveInfo = (Identity_ConstPar_Type*)curveData->Identity_ConstPar_Addr;

  if (curveInfo->Version != request->revision) {
    NfcSendAcknowledge(pInstance,
              GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
              GenericProtocol_WriteRequest_Type_USER_PARAMETER);
    return;
  }

  reg_get(&storedAlgorithmID, nfc__storedAlgorithmID);
  reg_get(&storedAlgorithmVer, nfc__storedAlgorithmVer);

  if(request->chargingCurve != storedAlgorithmID ||
		  request->revision != storedAlgorithmVer) {

	  chalg_resetStoredParameters(request->chargingCurve, request->revision);
  }

  request->userParameters.funcs.decode =
      &NfcMessageHandlerWriteUserParameterCallback;

  parseOk = GenericReceiveDecodeSubstream(&pInstance->genericReceive,
                                         GenericProtocol_UserParameterRequest_fields,
                                         &message->data.userParameter);

  if (!parseOk) {
    NfcSendAcknowledge(pInstance,
              GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
              GenericProtocol_WriteRequest_Type_USER_PARAMETER);
    return;
  }

  // Write charging curve ID to stored user parameter
  temp16 = request->chargingCurve;
  reg_put(&temp16, nfc__storedAlgorithmID);
  temp16 = request->revision;
  reg_put(&temp16, nfc__storedAlgorithmVer);
  temp16 = ((Mandatory_ConstPar_Type*)curveData->Mandatory_ConstPar_Addr)->UserParam.Size;
  project_setStoredUserParamReq(temp16);

  NfcSendWriteAcknowledge(pInstance,
                          GenericProtocol_AcknowledgeResponse_Type_OK,
                          GenericProtocol_WriteRequest_Type_USER_PARAMETER);
}

/**
 * Handle callback for writing charging algorithms to message
 */
static bool NfcMessageHandlerListAlgorithmsCallback(pb_ostream_t *stream,
                                                    const pb_field_t *field,
                                                    void * const *arg) {
  uint32_t index = 0;
  uint16_t i;
  uint16_t count = chalg_GetAlgorithmCount();

  (void) arg;

  for (i = 0; i < count; ++i) {
    index = chalg_GetAlgNo(i);

    if (!NfcMessageHandlerEncodeU32Field(stream, field, index))
      return false;
  }

  return true;
}

/**
 * Handle callback for writing pin select to message
 */
static bool NfcMessageHandlerListPinSelectCallback(pb_ostream_t *stream,
                                                    const pb_field_t *field,
                                                    void * const *arg) {
  uint32_t i;
  uint32_t value;
  sys_RegHandle* handle = (sys_RegHandle*)*arg;

  for (i = 0; i < 8; ++i) {
    reg_aGet(&value, *handle, i);

    if (!NfcMessageHandlerEncodeU32Field(stream, field, value))
      return false;
  }

  return true;
}

/**
 * Handle callback for writing charging restriction to message
 */
static bool NfcMessageHandlerChargingRestrictionCallback(pb_ostream_t *stream,
                                                    const pb_field_t *field,
                                                    void * const *arg) {
  uint32_t i;
  uint8_t control;
  uint8_t fromHour;
  uint8_t fromMin;
  uint8_t toHour;
  uint8_t toMin;
  GenericProtocol_ChargingRestriction value;

  (void)arg;

  for (i = 0; i < 21; ++i) {
    reg_aGet(&control, nfc__TimeTableControl, i);

    // Skip table row if not active
    if ((control & 0x01) != 0x01)
      continue;

    reg_aGet(&fromHour, nfc__TimeTableFromHour, i);
    reg_aGet(&fromMin, nfc__TimeTableFromMin, i);
    reg_aGet(&toHour, nfc__TimeTableToHour, i);
    reg_aGet(&toMin, nfc__TimeTableToMin, i);

    value.day = i / 3;
    value.fromTime = (fromHour * 60) + fromMin;
    value.toTime = (toHour * 60) + toMin;

    if (!NfcMessageHandlerEncodeChargingRestrictionField(stream, field, &value))
      return false;
  }

  return true;
}

/**
 * Encode user parameter name
 */
static bool NfcMessageHandlerEncodeUserParameterName(pb_ostream_t *stream,
                                                     const pb_field_t *field,
                                                     void * const *arg) {
  UserParam_ConstParam_Type* const userParameter =
      (UserParam_ConstParam_Type*)*arg;

  /* This encodes the header for the field, based on the constant info
   * from pb_field_t. */
  if (!pb_encode_tag_for_field(stream, field))
    return false;
  /* This encodes the data for the field. */
  return pb_encode_string(stream, (pb_byte_t*)userParameter->Name,
  	  	  	  	  	  	  userParameter->NameSize - 1);
}

/**
 * Handle callback for writing pin in select configuration
 */
static bool NfcMessageHandlerWritePinInSelectCallback(pb_istream_t *stream,
                                               const pb_field_t *field,
                                               void **arg) {
  uint32_t* index = (uint32_t*)*arg;
  uint64_t value;
  uint32_t temp32;

  if (!pb_decode_varint(stream, &value))
      return false;

  temp32 = (uint32_t)value;
  reg_aPut(&temp32, nfc__PinInSelect, *index);
  *index += 1;

  return true;
}

/**
 * Handle callback for writing pin out select configuration
 */
static bool NfcMessageHandlerWritePinOutSelectCallback(pb_istream_t *stream,
                                               const pb_field_t *field,
                                               void **arg) {
  uint32_t* index = (uint32_t*)*arg;
  uint64_t value;
  uint32_t temp32;

  if (!pb_decode_varint(stream, &value))
      return false;

  temp32 = (uint32_t)value;
  reg_aPut(&temp32, nfc__PinOutSelect, *index);
  *index += 1;

  return true;
}

/**
 * Handle callback for writing charging restriction configuration
 */
static bool NfcMessageHandlerWriteChargingRestrictionCallback(
    pb_istream_t *stream, const pb_field_t *field, void **arg) {
  GenericProtocol_ChargingRestriction chargingRestriction =
      GenericProtocol_ChargingRestriction_init_zero;

  if (pb_decode(stream, GenericProtocol_ChargingRestriction_fields,
                 &chargingRestriction)) {
    uint8_t* index = (uint8_t*)*arg;
    uint8_t dayIndex = (3 * chargingRestriction.day) + index[chargingRestriction.day];
    uint8_t control = 1;
    uint8_t fromHour = chargingRestriction.fromTime / 60;
    uint8_t fromMin = chargingRestriction.fromTime % 60;
    uint8_t toHour = chargingRestriction.toTime / 60;
    uint8_t toMin = chargingRestriction.toTime % 60;

    reg_aPut(&control, nfc__TimeTableControl, dayIndex);
    reg_aPut(&fromHour, nfc__TimeTableFromHour, dayIndex);
    reg_aPut(&fromMin, nfc__TimeTableFromMin, dayIndex);
    reg_aPut(&toHour, nfc__TimeTableToHour, dayIndex);
    reg_aPut(&toMin, nfc__TimeTableToMin, dayIndex);
    index[chargingRestriction.day]++;

    return true;
  }

  return false;
}

/**
 * Handle callback for writing user parameter configuration
 */
static bool NfcMessageHandlerWriteUserParameterCallback(
    pb_istream_t *stream, const pb_field_t *field, void **arg) {
  GenericProtocol_UserParameter userParameter =
      GenericProtocol_UserParameter_init_zero;

  if (pb_decode(stream, GenericProtocol_UserParameter_fields,
                 &userParameter)) {
    // Write value
	//reg_aPut(&userParameter.value, nfc__userParam, userParameter.index);
	reg_aPut(&userParameter.value, nfc__storedUserParam, userParameter.index);

    return true;
  }

  return false;
}

/**
 * Handle callback for writing status errors to message
 */
static bool NfcMessageHandlerListStatusErrorCallback(pb_ostream_t *stream,
                                                     const pb_field_t *field,
                                                     void * const *arg) {
  ChargerStatusFields_type Status;
  uint16_t chargingAlgorithmError;
  uint16_t supStatus;

  reg_get(&supStatus, nfc__supStatus);
  reg_get(&Status.Sum, nfc__RegulatorStatus);
  reg_get(&chargingAlgorithmError, nfc__ChargingAlgorithmError);

  if (IS_BIT_SET(supStatus, SUP_STATUS_BM_INIT2))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmBmInit1Fail))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_CORRUPTED))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmInternalFlashCorrupted))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_RADIO_ERROR))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmRadioError))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_BM_FAILED))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmBmInitFail))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_BM_ALG_ERR))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmBmAlgorithmError))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_ADDR_CONFLICT))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmChargerAddressConflict))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_BM_ADDR_CONFLICT))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmAddressConflict))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_DPL_NO_MASTER))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmDplNoMaster))
      return false;
  if (IS_BIT_SET(supStatus, SUP_STATUS_DPL_MASTER_CONFLICT))
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmDplMasterConflict))
      return false;

  if (Status.Detail.Error.Detail.Phase)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmPhaseDeviceAlarm))
      return false;
  if (Status.Detail.Error.Detail.Regulator)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmRegulatorError))
      return false;
  if (Status.Detail.Error.Detail.LowChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowChargerTemperature))
      return false;
  if (Status.Detail.Error.Detail.HighTrafoTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                       eDeviceAlarmHighTransformerTemperature))
      return false;
  if (Status.Detail.Error.Detail.HighChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighChargerTemperature))
      return false;
  if (Status.Detail.Error.Detail.HighBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBoardTemperature))
      return false;
  if (Status.Detail.Error.Detail.LowBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowBoardTemperature))
      return false;
  if (Status.Detail.Error.Detail.Watchdog)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmCanTimeout))
      return false;
  if (Status.Detail.Warning.Detail.Phase)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmSlavePhaseDeviceAlarm))
      return false;
  if (Status.Detail.Warning.Detail.Regulator)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmSlaveRegulatorError))
      return false;
  if (Status.Detail.Warning.Detail.LowChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmSlaveLowChargerTemperature))
      return false;
  if (Status.Detail.Warning.Detail.HighTrafoTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                       eDeviceAlarmSlaveHighTransformerTemperature))
      return false;
  if (Status.Detail.Warning.Detail.HighChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmSlaveHighChargerTemperature))
      return false;
  if (Status.Detail.Warning.Detail.HighBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmSlaveHighBoardTemperature))
      return false;
  if (Status.Detail.Warning.Detail.LowBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmSlaveLowBoardTemperature))
      return false;
  if (Status.Detail.Warning.Detail.Watchdog)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
    		eDeviceAlarmSlaveCanTimeout))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_LOW_BATTERY_VOL)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowBatteryVoltage))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_BM_LOW_SOC)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowStateOfCharge))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_HIGH_BATTERY_VOL)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryVoltage))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_CHARGE_TIME_LIMIT)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmTimeLimitExceeded))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_CHARGE_AH_LIMIT)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmAboveAhMax))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_INCORRECT_ALGORITHM)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmInvalidParameters))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_INCORRECT_MOUNT)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmIncorrectMount))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_HIGH_BATTERY_LIMIT)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryLimitError))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_BATTERY_ERROR)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmBatteryError))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_BM_TEMP)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryTemperature))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_BM_ACID)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowElectrolyteLevel))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_BM_BALANCE)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmVoltageBalanceDeviceAlarm))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_LOW_AIRPUMP_PRESSURE)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowAirPressure))
      return false;
  if (chargingAlgorithmError & CHALG_ERR_HIGH_AIRPUMP_PRESSURE)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighAirPressure))
      return false;

  return true;
}

/**
 * Handle callback for writing status to message
 */
static bool NfcMessageHandlerStatusCallback(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            void * const *arg) {
  uint16_t chargingAlgorithmStatus;
  ChargerStatusFields_type Status;
  eDeviceDeviceStatus_t limitStatus = eDeviceStatusUnknown;

  reg_get(&chargingAlgorithmStatus, nfc__ChargingAlgorithmStatus);
  reg_get(&Status.Sum, nfc__RegulatorStatus);

  if (chargingAlgorithmStatus & CHALG_STAT_MAINS_CONN)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusMainsConnected))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_BATTRY_CONN)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusBatteryConnected))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_BM_CONNTECTED)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusBmuConnected))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_MAIN_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusMainCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_ADDITIONAL_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusAdditionalCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_EQUALIZE_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusEqualizing))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_EQUALIZE_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusMaintenanceCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_CHARG_COMPLETED)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusChargingCompleted))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_PAUSE)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusPaused))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_PRE_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusPreCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_FORCE)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusForceStart))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_BBC)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusBbc))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_TBATTLOW)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusLowBatteryTemperature))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_TBATTHIGH)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusHighBatteryTemperature))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_RESTRICTED)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusChargingRestricted))
      return false;

  if (Status.Detail.Info.Detail.CurrentIsNearSetValue)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusCurrentIsNearSetValue))
      return false;
  if (Status.Detail.Info.Detail.PowerIsNearSetValue)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusPowerIsNearSetValue))
      return false;
  if (Status.Detail.Info.Detail.VoltageIsNearSetValue)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusVoltageIsNearSetValue))
      return false;

  switch (Status.Detail.Info.Detail.limit) {
    case LimitNone:
      // Nothing to do
      break;
    case LimitUset:
      limitStatus = eDeviceStatusLimitSetVoltage;
      break;
    case LimitIset:
      limitStatus = eDeviceStatusLimitSetCurrent;
      break;
    case LimitPset:
      limitStatus = eDeviceStatusLimitSetPower;
      break;
    case LimitT:
      limitStatus = eDeviceStatusLimitTemperature;
      break;
    case LimitUengine:
      limitStatus = eDeviceStatusLimitEngineVoltage;
      break;
    case LimitIengine:
      limitStatus = eDeviceStatusLimitEngineCurrent;
      break;
    case LimitPengine:
      limitStatus = eDeviceStatusLimitEnginePower;
      break;
    case LimitIdc:
      limitStatus = eDeviceStatusLimitChargingCurrent;
      break;
    case LimitIac:
      limitStatus = eDeviceStatusLimitMainsCurrent;
      break;
    case LimitPac:
      limitStatus = eDeviceStatusLimitMainsPower;
      break;
    case LimitPhase:
      limitStatus = eDeviceStatusLimitPhase;
      break;
    default:
      // Nothing to do
      break;
  }

  if (limitStatus != eDeviceStatusUnknown)
    if (!NfcMessageHandlerEncodeU32Field(stream, field, limitStatus))
      return false;

  if (Status.Detail.Derate.Detail.Phase)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusPhaseDerate))
      return false;
  if (Status.Detail.Derate.Detail.UserParam)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusUserParamDerate))
      return false;
  if (Status.Detail.Derate.Detail.Engine)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusEngineDerate))
      return false;
  if (Status.Detail.Derate.Detail.Temperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusTemperatureDerate))
      return false;
  if (Status.Detail.Derate.Detail.NoLoad)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusNoLoadDerate))
      return false;
  if (Status.Detail.Derate.Detail.LowVoltage)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusLowVoltageDerate))
      return false;
  if (Status.Detail.Derate.Detail.HighVoltage)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusHighVoltageDerate))
      return false;

  return true;
}

/**
 * Handle callback for writing charging algorithms that contains user
 * parameters to message
 */
static bool NfcMessageHandlerListUserParameterCurvesCallback(
    pb_ostream_t *stream, const pb_field_t *field, void * const *arg) {
  uint32_t index = 0;
  uint16_t i;
  uint16_t count = chalg_GetUserParameterAlgorithmCount();

  (void) arg;

  for (i = 0; i < count; ++i) {
    index = chalg_GetUserParamAlgNo(i);

    if (!NfcMessageHandlerEncodeU32Field(stream, field, index))
      return false;
  }

  return true;
}

/**
 * Encode uint32 value in field
 */
static bool NfcMessageHandlerEncodeU32Field(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            const uint32_t value) {
  /* This encodes the header for the field, based on the constant info
   * from pb_field_t. */
  if (!pb_encode_tag_for_field(stream, field))
    return false;
  /* This encodes the data for the field. */
  if (!pb_encode_varint(stream, value))
    return false;
  return true;
}

/**
 * Encode charging restriction value in field
 */
static bool NfcMessageHandlerEncodeChargingRestrictionField(
    pb_ostream_t *stream, const pb_field_t *field,
    const GenericProtocol_ChargingRestriction* value) {
  /* This encodes the header for the field, based on the constant info
   * from pb_field_t. */
  if (!pb_encode_tag_for_field(stream, field))
    return false;
  /* This encodes the data for the field. */
  if (!pb_encode_submessage(stream, GenericProtocol_ChargingRestriction_fields,
                            value))
    return false;
  return true;
}

/**
 * Convert value from fahrenheit to celcius
 * @param fahrenheit The temperature in fahrenheit
 * @return The value in celcius
 */
static float NfcMessageConvertToCelsius(const float fahrenheit) {
  return (fahrenheit - 32) / 1.8;
}

/**
 * Convert value from celcius to fahrenheit
 * @param celcius The temperature in celcius
 * @return The value in fahrenheit
 */
static float NfcMessageConvertToFahrenheit(const float celsius) {
  return (celsius * 1.8) + 32;
}

/**
 * Send NACK from log building
 */
static void NfcMessageSendLogNack(NfcMessageHandler_t* const pInstance,
                         const GenericProtocol_AcknowledgeResponse_Type code) {
  GenericPackStartFrame(&pInstance->genericPack);
  NfcSendReadAcknowledge(pInstance, code, GenericProtocol_ReadRequest_Type_LOG);
  GenericPackEndFrame(&pInstance->genericPack);
  NfcMessageSend(pInstance);
}

/**
 * Handle log read callback
 */
static void NfcHandleLogCallback(cm_Request* pRequest) {
  NfcMessageHandler_t* pInstance = (NfcMessageHandler_t*)pRequest->pUtil;
  GenericProtocol_LogResponse message =
  GenericProtocol_LogResponse_init_default;

#ifdef SEGGER
  uint16_t diff = tstamp_getMsCount() - lastTime;
  lastTime = tstamp_getMsCount();
#endif

  if ((pRequest->flags & CM_REQFLG_SUCCESS) != CM_REQFLG_SUCCESS) {
	  if (pInstance->logRequestCount == pInstance->logCount) {
		  NfcMessageSendLogNack(pInstance,
				  GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS);
	  } else {
		  GenericPackEndFrame(&pInstance->genericPack);
		  NfcMessageSend(pInstance);
	  }

#ifdef SEGGER
	  SEGGER_RTT_printf(0, "%3d, Read log failed\r\n", diff);
#endif
	  return;
  }

  switch (pRequest->logType) {
    case CM_LOGTYPE_EVENT:
      NfcHandleSendEvent(pRequest, &message);
      break;
    case CM_LOGTYPE_INSTANT:
      NfcHandleSendInstant(pRequest, &message);
      break;
    case CM_LOGTYPE_HISTORICAL:
      NfcHandleSendHistory(pRequest, &message);
      break;
    default:
      NfcMessageSendLogNack(pInstance,
             GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS);

#ifdef SEGGER
      SEGGER_RTT_printf(0, "%3d, Read wrong log type: %d\r\n", diff, pRequest->logType);
#endif
      return;
  }

  if (GenericPackAddCmd(&pInstance->genericPack, GenericTypeChargerLogResp,
                          GenericProtocol_LogResponse_fields, &message)
        && pInstance->logCount > 0) {
    pInstance->logRequest.index = pInstance->logIndex;

#ifdef SEGGER
    SEGGER_RTT_printf(0, "%3d, Read logs:\r\n", diff);
    SEGGER_RTT_printf(0, " - Type: %d\r\n", pRequest->logType);
    SEGGER_RTT_printf(0, " - Count: %d\r\n", pInstance->logCount);
    SEGGER_RTT_printf(0, " - Index: %d\r\n", pInstance->logRequest.index);
#endif

    if (pRequest->logType == CM_LOGTYPE_EVENT) {
        pInstance->logRequest.flags = CM_REQFLG_READNEXT;
        pInstance->logIndex++;
    } else {
        pInstance->logRequest.flags = CM_REQFLG_READPREV;
        pInstance->logIndex--;
    }

    pInstance->logCount--;

    cm_processRequest(&pInstance->logRequest);
  } else {
#ifdef SEGGER
    SEGGER_RTT_printf(0, "%3d, Send logs\r\n", diff);
#endif

    GenericPackEndFrame(&pInstance->genericPack);
    NfcMessageSend(pInstance);
  }
}

/**
 * Handle send event log
 */
static void NfcHandleSendEvent(cm_Request* pRequest,
                               GenericProtocol_LogResponse* message) {
  NfcMessageHandler_t* pInstance = (NfcMessageHandler_t*)pRequest->pUtil;
  cm_EvtLogHdr* log = (cm_EvtLogHdr*)pInstance->logBuffer;

  message->index = log->Index;
  message->type = GenericProtocol_LogRequest_Type_EVENT;
  message->eventLog.time = log->Time;

  switch (log->EventId) {
    case CM_EVENT_TIMESET: {
      cm_EvtRecTimeSet *timeSet = (cm_EvtRecTimeSet*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_TIME_SET;
      message->eventLog.data.timeSetEvent.oldTime = timeSet->OldTime;
      message->eventLog.which_data = GenericProtocol_EventLog_timeSetEvent_tag;

      switch (timeSet->Source) {
        case 1:
          message->eventLog.data.timeSetEvent.source =
              GenericProtocol_EventTimeSet_Source_SM;
          break;
        case 2:
          message->eventLog.data.timeSetEvent.source =
              GenericProtocol_EventTimeSet_Source_CM;
          break;
        default:
          message->eventLog.data.timeSetEvent.source =
              GenericProtocol_EventTimeSet_Source_UNKNOWN;
          break;
      }
      break;
    }
    case CM_EVENT_CHARGESTARTED:
      message->eventLog.type = GenericProtocol_EventLog_Type_CHARGE_STARTED;
      break;
    case CM_EVENT_CHARGEENDED:
      message->eventLog.type = GenericProtocol_EventLog_Type_CHARGE_END;
      break;
    case CM_EVENT_CURVEDATA: {
      cm_EvtRecCurveData *curveData = (cm_EvtRecCurveData*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_CURVE_DATA;
      message->eventLog.data.curveDataEvent.algorithmNumberNew = curveData->AlgNo_new;
      message->eventLog.data.curveDataEvent.algorithmNumberOld = curveData->AlgNo_old;
      message->eventLog.data.curveDataEvent.baseLoadNew = curveData->Baseload_new;
      message->eventLog.data.curveDataEvent.baseLoadOld = curveData->Baseload_old;
      message->eventLog.data.curveDataEvent.cableResistanceNew = curveData->Ri_new;
      message->eventLog.data.curveDataEvent.cableResistanceOld = curveData->Ri_old;
      message->eventLog.data.curveDataEvent.capacityNew = curveData->Capacity_new;
      message->eventLog.data.curveDataEvent.capacityOld = curveData->Capacity_old;
      message->eventLog.data.curveDataEvent.cellsNew = curveData->Cells_new;
      message->eventLog.data.curveDataEvent.cellsOld = curveData->Cells_old;
      message->eventLog.which_data = GenericProtocol_EventLog_curveDataEvent_tag;
      break;
    }
    case CM_EVENT_VCC: {
      cm_EvtRecVcc *vcc = (cm_EvtRecVcc*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_MAINS_STATE;
      message->eventLog.data.mainsChangedEvent.active = vcc->Active != 0;
      message->eventLog.which_data = GenericProtocol_EventLog_mainsChangedEvent_tag;

      switch (vcc->VccStatus) {
        case 1:
          message->eventLog.data.mainsChangedEvent.vccStatus = GenericProtocol_EventMainsChanged_MainsState_MAINS;
          break;
        case 2:
          message->eventLog.data.mainsChangedEvent.vccStatus = GenericProtocol_EventMainsChanged_MainsState_BATTERY;
          break;
        default:
          // Should not happen
          break;
      }
      break;
    }
    case CM_EVENT_VOLTAGECALIBRATION: {
      cm_EvtRecVolCalib *voltageCalibration = (cm_EvtRecVolCalib*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_VOLTAGE_CALIBRATION;
      memcpy(&message->eventLog.data.voltageCalibrationEvent.voltageSlopeNew,
             &voltageCalibration->UslopeNew,
             sizeof(voltageCalibration->UslopeNew));
      memcpy(&message->eventLog.data.voltageCalibrationEvent.voltageSlopeOld,
             &voltageCalibration->UslopeOld,
             sizeof(voltageCalibration->UslopeOld));
      message->eventLog.which_data = GenericProtocol_EventLog_voltageCalibrationEvent_tag;
      break;
    }
    case CM_EVENT_CURRENTCALIBRATION: {
      cm_EvtRecCurrCalib *currentCalibration = (cm_EvtRecCurrCalib*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_CURRENT_CALIBRATION;
      memcpy(&message->eventLog.data.currentCalibrationEvent.currentSlopeNew,
             &currentCalibration->IslopeNew,
             sizeof(currentCalibration->IslopeNew));
      memcpy(&message->eventLog.data.currentCalibrationEvent.currentSlopeOld,
             &currentCalibration->IslopeOld,
             sizeof(currentCalibration->IslopeOld));
      message->eventLog.which_data = GenericProtocol_EventLog_currentCalibrationEvent_tag;
      break;
    }
    case CM_EVENT_STARTNETWORK:
      message->eventLog.type = GenericProtocol_EventLog_Type_NETWORK_STARTED;
      break;
    case CM_EVENT_JOINNETWORK:
      message->eventLog.type = GenericProtocol_EventLog_Type_NETWORK_JOINED;
      break;
    case CM_EVENT_STARTUP: {
      cm_EvtRecStartUp *startUp = (cm_EvtRecStartUp*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_START_UP;
      message->eventLog.data.startUpEvent.firmwareType = startUp->MainFwType;
      message->eventLog.data.startUpEvent.firmwareVersion = startUp->MainFwVer;
      message->eventLog.data.startUpEvent.radioFirmwareType = startUp->RadioFwType;
      message->eventLog.data.startUpEvent.radioFirmwareVersion = startUp->RadioFwVer;
      message->eventLog.which_data = GenericProtocol_EventLog_startUpEvent_tag;
      break;
    }
    case CM_EVENT_SETTINGS:
      message->eventLog.type = GenericProtocol_EventLog_Type_SETTINGS_CHANGED;
      break;
    case CM_EVENT_ASSERT: {
      cm_EvtRecAssert *assert = (cm_EvtRecAssert*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_ASSERT;
      message->eventLog.data.assertEvent.id = assert->AssertId;
      message->eventLog.which_data = GenericProtocol_EventLog_assertEvent_tag;
      break;
    }
    case CM_EVENT_CHALGERROR:
      // Fall through
    case CM_EVENT_REGUERROR:
      // Fall through
    case CM_EVENT_SUPERROR:
      message->eventLog.type = GenericProtocol_EventLog_Type_ALARM_LIST;
      message->eventLog.alarmList.arg = &pInstance->logBuffer;
      message->eventLog.alarmList.funcs.encode =
          &NfcMessageHandlerEventAlarmCallback;
      break;
    case CM_EVENT_CABLERISLOPE: {
      cm_EvtRecCableRiSlope *cableRi = (cm_EvtRecCableRiSlope*)pInstance->logBuffer;

      message->eventLog.type = GenericProtocol_EventLog_Type_CABLE_RI;
      message->eventLog.data.cableRi.slopeNew = cableRi->RiSlopeNew;
      message->eventLog.data.cableRi.slopeOld = cableRi->RiSlopeOld;
      message->eventLog.which_data = GenericProtocol_EventLog_cableRi_tag;
      break;
    }
    default:
      message->eventLog.type = GenericProtocol_EventLog_Type_UNKNOWN;
      message->eventLog.unknownEvent.data.arg = &pInstance->logBuffer;
      message->eventLog.unknownEvent.data.funcs.encode =
          &NfcMessageHandlerEventDataCallback;
      message->eventLog.which_data = GenericProtocol_EventLog_unknownEvent_tag;
      break;
  }
}

/**
 * Handle send instant log
 */
static void NfcHandleSendInstant(cm_Request* pRequest,
                               GenericProtocol_LogResponse* message) {
  NfcMessageHandler_t* pInstance = (NfcMessageHandler_t*)pRequest->pUtil;
  cm_InstRecStd* log = (cm_InstRecStd*)pInstance->logBuffer;
  uint16_t temp16;
  uint32_t headerTime;
  uint32_t headerIndex;
  uint8_t language;

  reg_get(&temp16, nfc__InstantLogPeriod);
  reg_get(&headerTime, nfc__hInstHeaderTime);
  reg_get(&headerIndex, nfc__InstantHeaderIndex);
  reg_get(&language, nfc__Language);

  message->index = pInstance->logIndex + 1;
  message->type = GenericProtocol_LogRequest_Type_INSTANT;
  message->instantLog.alarmCodes.funcs.encode =
      &NfcMessageHandlerInstantLogErrorCallback;
  message->instantLog.alarmCodes.arg = log;
  message->instantLog.chargerCurrent = ((float)log->Ichalg) / 1000.0;
  message->instantLog.chargerTemperature = ((float)log->Ths) / 10.0;

  // If language is US, convert to celcius
  if (language == 2) {
    message->instantLog.chargerTemperature =
        NfcMessageConvertToCelsius(message->instantLog.chargerTemperature);
  }

  message->instantLog.chargerVoltage = ((float)log->Uchalg) / 1000.0;
  message->instantLog.samplePeriod = temp16;
  message->instantLog.time = headerTime +
      ((message->index - headerIndex) * message->instantLog.samplePeriod);
}

/**
 * Handle send history log
 */
static void NfcHandleSendHistory(cm_Request* pRequest,
                               GenericProtocol_LogResponse* message) {
  NfcMessageHandler_t* pInstance = (NfcMessageHandler_t*)pRequest->pUtil;
  cm_HistRecStd* log = (cm_HistRecStd*)pInstance->logBuffer;
  uint8_t language;

  reg_get(&language, nfc__Language);

  message->index = pInstance->logIndex + 1;
  message->type = GenericProtocol_LogRequest_Type_HISTORY;
  message->historyLog.alarmCodes.arg = log;
  message->historyLog.BSN = log->BSN;
  message->historyLog.FID = log->FID;
  strcpy(message->historyLog.algorithmName, log->AlgNoName);
  message->historyLog.algorithmNumber = log->AlgNo;
  message->historyLog.baseLoad =  ((float)log->Baseload) / 1000.0;
  message->historyLog.batteryId = log->BID;
  message->historyLog.batteryManufacturerId = log->BMfgId;
  message->historyLog.batteryType = log->BatteryType;
  message->historyLog.cableResistance = ((float)log->Ri) / 1000.0;
  message->historyLog.capacity = log->Capacity;
  message->historyLog.cells = log->Cells;
  message->historyLog.chargeAh = log->chargedAh;
  message->historyLog.chargeAhP = log->chargedAhP;
  message->historyLog.chargeTime = log->ChargeTime;
  message->historyLog.chargeTimeEnd = log->chargeEndTime;
  message->historyLog.chargeTimeStart = log->chargeStartTime;
  message->historyLog.chargeWh = log->chargedWh;
  message->historyLog.chargerTemperatureEnd = ((float)log->ThsEnd) / 10.0;
  message->historyLog.chargerTemperatureMax = ((float)log->ThsMax) / 10.0;
  message->historyLog.chargerTemperatureStart = ((float)log->ThsStart) / 10.0;

  // If language is US, convert to celcius
  if (language == 2) {
    message->historyLog.chargerTemperatureEnd =
        NfcMessageConvertToCelsius(message->historyLog.chargerTemperatureEnd);
    message->historyLog.chargerTemperatureMax =
        NfcMessageConvertToCelsius(message->historyLog.chargerTemperatureMax);
    message->historyLog.chargerTemperatureStart =
        NfcMessageConvertToCelsius(message->historyLog.chargerTemperatureStart);
  }

  message->historyLog.chargerType = log->CMtype;
  message->historyLog.chargerVpcEnd = ((float)log->endVoltage) / 1000.0;
  message->historyLog.chargerVpcStart = ((float)log->startVoltage) / 1000.0;
  message->historyLog.chargingMode = log->ChargingMode;
  message->historyLog.equalizeAh = log->equAh;
  message->historyLog.equalizeTime = log->equTime;
  message->historyLog.equalizeWh = log->equWh;
  message->historyLog.eventIndexEnd = log->EvtIdxStop;
  message->historyLog.eventIndexStart = log->EvtIdxStart;
  message->historyLog.mui = log->SerialNo;

  message->historyLog.alarmCodes.arg = log;
  message->historyLog.alarmCodes.funcs.encode =
      &NfcMessageHandlerListHistoryErrorCallback;
  message->historyLog.deviceStatus.arg = log;
  message->historyLog.deviceStatus.funcs.encode =
      &NfcMessageHandlerListHistoryStatusCallback;
}

/**
 * Handle callback for writing instant log errors to message
 */
static bool NfcMessageHandlerInstantLogErrorCallback(pb_ostream_t *stream,
                                                     const pb_field_t *field,
                                                     void * const *arg) {
  cm_InstRecStd* log = (cm_InstRecStd*)*arg;

  if (log->ReguError.Error.Detail.Phase)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmPhaseDeviceAlarm))
      return false;
  if (log->ReguError.Error.Detail.Regulator)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmRegulatorError))
      return false;
  if (log->ReguError.Error.Detail.LowChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowChargerTemperature))
      return false;
  if (log->ReguError.Error.Detail.HighTrafoTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                       eDeviceAlarmHighTransformerTemperature))
      return false;
  if (log->ReguError.Error.Detail.HighChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighChargerTemperature))
      return false;
  if (log->ReguError.Error.Detail.HighBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBoardTemperature))
      return false;
  if (log->ReguError.Error.Detail.LowBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowBoardTemperature))
      return false;
  if (log->ReguError.Error.Detail.Watchdog)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmCanTimeout))
      return false;


  if (log->ChalgError & CHALG_ERR_LOW_BATTERY_VOL) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowBatteryVoltage))
      return false;
  } else if (log->ChalgError & CHALG_ERR_BM_LOW_SOC) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowStateOfCharge))
      return false;
  } else if (log->ChalgError & CHALG_ERR_HIGH_BATTERY_VOL) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryVoltage))
      return false;
  } else if (log->ChalgError & CHALG_ERR_CHARGE_TIME_LIMIT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmTimeLimitExceeded))
      return false;
  } else if (log->ChalgError & CHALG_ERR_CHARGE_AH_LIMIT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmAboveAhMax))
      return false;
  } else if (log->ChalgError & CHALG_ERR_INCORRECT_ALGORITHM) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmInvalidParameters))
      return false;
  } else if (log->ChalgError & CHALG_ERR_INCORRECT_MOUNT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmIncorrectMount))
      return false;
  } else if (log->ChalgError & CHALG_ERR_HIGH_BATTERY_LIMIT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryLimitError))
      return false;
  } else if (log->ChalgError & CHALG_ERR_BATTERY_ERROR) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmBatteryError))
      return false;
  } else if (log->ChalgError & CHALG_ERR_BM_TEMP) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryTemperature))
      return false;
  } else if (log->ChalgError & CHALG_ERR_BM_ACID) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowElectrolyteLevel))
      return false;
  } else if (log->ChalgError & CHALG_ERR_BM_BALANCE) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmVoltageBalanceDeviceAlarm))
      return false;
  } else if (log->ChalgError & CHALG_ERR_LOW_AIRPUMP_PRESSURE) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowAirPressure))
      return false;
  } else if (log->ChalgError & CHALG_ERR_HIGH_AIRPUMP_PRESSURE) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighAirPressure))
      return false;
  }

  return true;
}

/**
 * Handle callback for writing event data
 */
static bool NfcMessageHandlerEventDataCallback(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            void * const *arg) {
  cm_EvtLogHdr* log = (cm_EvtLogHdr*)*arg;
  uint8_t* data = (uint8_t*)*arg;

  /* This encodes the header for the field, based on the constant info
   * from pb_field_t. */
  if (!pb_encode_tag_for_field(stream, field))
    return false;
  /* This encodes the data for the field. */
  return pb_encode_string(stream, data, log->DataSize + sizeof(cm_EvtLogHdr));
}

/**
 * Encode generic alarm to stream
 * @param stream The stream
 * @param code The alarm code
 * @param active If the alarm went active
 * @return If encoded
 */
static bool NfcMessageHandlerEncodeGenericAlarm(pb_ostream_t *stream,
                                                const pb_field_t *field,
                                                const uint32_t code,
                                                const bool active) {
  GenericProtocol_GenericAlarm genericAlarm =
      GenericProtocol_GenericAlarm_init_default;

  genericAlarm.code = code;
  genericAlarm.active = active;

  /* This encodes the header for the field, based on the constant info
   * from pb_field_t. */
  if (!pb_encode_tag_for_field(stream, field))
    return false;
  /* This encodes the data for the field. */
  return pb_encode_submessage(stream, GenericProtocol_GenericAlarm_fields,
                       &genericAlarm);
}

/**
 * Handle callback for writing event alarm
 */
static bool NfcMessageHandlerEventAlarmCallback(pb_ostream_t *stream,
                                            const pb_field_t *field,
                                            void * const *arg) {
  cm_EvtLogHdr* log = (cm_EvtLogHdr*)*arg;
  bool encodeOk = true;

  switch (log->EventId) {
    case CM_EVENT_CHALGERROR: {
      cm_EvtRecChalgError *chalgError = (cm_EvtRecChalgError*)*arg;

      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_LOW_BATTERY_VOL))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmLowBatteryVoltage,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_LOW_BATTERY_VOL));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_HIGH_BATTERY_VOL))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmHighBatteryVoltage,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_HIGH_BATTERY_VOL));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_CHARGE_TIME_LIMIT))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmTimeLimitExceeded,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_CHARGE_TIME_LIMIT));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_CHARGE_AH_LIMIT))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmAboveAhMax,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_CHARGE_AH_LIMIT));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_INCORRECT_ALGORITHM))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmInvalidParameters,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_INCORRECT_ALGORITHM));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_INCORRECT_MOUNT))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmIncorrectMount,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_INCORRECT_MOUNT));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_HIGH_BATTERY_LIMIT))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmHighBatteryLimitError,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_HIGH_BATTERY_LIMIT));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_BATTERY_ERROR))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmBatteryError,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_BATTERY_ERROR));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_BM_TEMP))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmHighBatteryTemperature,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_BM_TEMP));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_BM_ACID))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmLowElectrolyteLevel,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_BM_ACID));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_BM_BALANCE))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmVoltageBalanceDeviceAlarm,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_BM_BALANCE));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_LOW_AIRPUMP_PRESSURE))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmLowAirPressure,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_LOW_AIRPUMP_PRESSURE));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_HIGH_AIRPUMP_PRESSURE))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmHighAirPressure,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_HIGH_AIRPUMP_PRESSURE));
      if (encodeOk &&
          IS_BIT_SET(chalgError->ChalgError, CHALG_ERR_BM_LOW_SOC))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmLowStateOfCharge,
            IS_BIT_SET(chalgError->Active, CHALG_ERR_BM_LOW_SOC));
      break;
    }
    case CM_EVENT_REGUERROR: {
      cm_EvtRecReguError *reguError = (cm_EvtRecReguError*)*arg;
      chargerAlarm_type status;
      chargerAlarm_type active;

      status.Sum = reguError->ReguError;
      active.Sum = reguError->Active;

      if (encodeOk && status.Error.Detail.Phase)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmPhaseDeviceAlarm,
            active.Error.Detail.Phase);
      if (encodeOk && status.Error.Detail.Regulator)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmRegulatorError,
            active.Error.Detail.Regulator);
      if (encodeOk && status.Error.Detail.LowChargerTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmLowChargerTemperature,
            active.Error.Detail.LowChargerTemperature);
      if (encodeOk && status.Error.Detail.HighTrafoTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmHighTransformerTemperature,
            active.Error.Detail.HighTrafoTemperature);
      if (encodeOk && status.Error.Detail.HighChargerTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmHighChargerTemperature,
            active.Error.Detail.HighChargerTemperature);
      if (encodeOk && status.Error.Detail.HighBoardTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmHighBoardTemperature,
            active.Error.Detail.HighBoardTemperature);
      if (encodeOk && status.Error.Detail.LowBoardTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmLowBoardTemperature,
            active.Error.Detail.LowBoardTemperature);
      if (encodeOk && status.Error.Detail.Watchdog)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmCanTimeout,
            active.Error.Detail.Watchdog);
      if (encodeOk && status.Warning.Detail.Phase)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlavePhaseDeviceAlarm,
            active.Warning.Detail.Phase);
      if (encodeOk && status.Warning.Detail.Regulator)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlaveRegulatorError,
            active.Warning.Detail.Regulator);
      if (encodeOk && status.Warning.Detail.LowChargerTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlaveLowChargerTemperature,
            active.Warning.Detail.LowChargerTemperature);
      if (encodeOk && status.Warning.Detail.HighTrafoTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlaveHighTransformerTemperature,
            active.Warning.Detail.HighTrafoTemperature);
      if (encodeOk && status.Warning.Detail.HighChargerTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlaveHighChargerTemperature,
            active.Warning.Detail.HighChargerTemperature);
      if (encodeOk && status.Warning.Detail.HighBoardTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlaveHighBoardTemperature,
            active.Warning.Detail.HighBoardTemperature);
      if (encodeOk && status.Warning.Detail.LowBoardTemperature)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlaveLowBoardTemperature,
            active.Warning.Detail.LowBoardTemperature);
      if (encodeOk && status.Warning.Detail.Watchdog)
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmSlaveCanTimeout,
            active.Warning.Detail.Watchdog);
      break;
    }
    case CM_EVENT_SUPERROR: {
      cm_EvtRecSupError *supError = (cm_EvtRecSupError*)*arg;

      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_BM_INIT2))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmBmInit1Fail,
            IS_BIT_SET(supError->Active, SUP_STATUS_BM_INIT2));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_CORRUPTED))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmInternalFlashCorrupted,
            IS_BIT_SET(supError->Active, SUP_STATUS_CORRUPTED));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_RADIO_ERROR))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmRadioError,
            IS_BIT_SET(supError->Active, SUP_STATUS_RADIO_ERROR));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_BM_FAILED))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmBmInitFail,
            IS_BIT_SET(supError->Active, SUP_STATUS_BM_FAILED));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_BM_ALG_ERR))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmBmAlgorithmError,
            IS_BIT_SET(supError->Active, SUP_STATUS_BM_ALG_ERR));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_ADDR_CONFLICT))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmChargerAddressConflict,
            IS_BIT_SET(supError->Active, SUP_STATUS_ADDR_CONFLICT));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_BM_ADDR_CONFLICT))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmAddressConflict,
            IS_BIT_SET(supError->Active, SUP_STATUS_BM_ADDR_CONFLICT));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_DPL_NO_MASTER))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmDplNoMaster,
            IS_BIT_SET(supError->Active, SUP_STATUS_DPL_NO_MASTER));
      if (encodeOk && IS_BIT_SET(supError->SupError, SUP_STATUS_DPL_MASTER_CONFLICT))
        encodeOk = NfcMessageHandlerEncodeGenericAlarm(
            stream, field, eDeviceAlarmDplMasterConflict,
            IS_BIT_SET(supError->Active, SUP_STATUS_DPL_MASTER_CONFLICT));
      break;
    }
  }

  return encodeOk;
}

/**
 * Handle callback for writing history log errors to message
 */
static bool NfcMessageHandlerListHistoryErrorCallback(pb_ostream_t *stream,
                                                      const pb_field_t *field,
                                                      void * const *arg) {
  cm_HistRecStd* log = (cm_HistRecStd*) *arg;
  chargerAlarm_type status;

  status.Sum = log->ReguErrorSum;

  if (status.Error.Detail.Phase)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmPhaseDeviceAlarm))
      return false;
  if (status.Error.Detail.Regulator)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmRegulatorError))
      return false;
  if (status.Error.Detail.LowChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowChargerTemperature))
      return false;
  if (status.Error.Detail.HighTrafoTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                       eDeviceAlarmHighTransformerTemperature))
      return false;
  if (status.Error.Detail.HighChargerTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighChargerTemperature))
      return false;
  if (status.Error.Detail.HighBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBoardTemperature))
      return false;
  if (status.Error.Detail.LowBoardTemperature)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowBoardTemperature))
      return false;
  if (status.Error.Detail.Watchdog)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmCanTimeout))
      return false;


  if (log->ChalgErrorSum & CHALG_ERR_LOW_BATTERY_VOL) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowBatteryVoltage))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_HIGH_BATTERY_VOL) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryVoltage))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_BM_LOW_SOC) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowStateOfCharge))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_CHARGE_TIME_LIMIT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmTimeLimitExceeded))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_CHARGE_AH_LIMIT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmAboveAhMax))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_INCORRECT_ALGORITHM) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmInvalidParameters))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_INCORRECT_MOUNT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmIncorrectMount))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_HIGH_BATTERY_LIMIT) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryLimitError))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_BATTERY_ERROR) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmBatteryError))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_BM_TEMP) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighBatteryTemperature))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_BM_ACID) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowElectrolyteLevel))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_BM_BALANCE) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmVoltageBalanceDeviceAlarm))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_LOW_AIRPUMP_PRESSURE) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmLowAirPressure))
      return false;
  } else if (log->ChalgErrorSum & CHALG_ERR_HIGH_AIRPUMP_PRESSURE) {
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceAlarmHighAirPressure))
      return false;
  }

  return true;
}

/**
 * Handle callback for writing history log status to message
 */
static bool NfcMessageHandlerListHistoryStatusCallback(pb_ostream_t *stream,
                                                       const pb_field_t *field,
                                                       void * const *arg) {
  cm_HistRecStd* log = (cm_HistRecStd*) *arg;
  uint16_t chargingAlgorithmStatus = log->ChalgStatusSum;

  if (chargingAlgorithmStatus & CHALG_STAT_MAINS_CONN)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusMainsConnected))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_BATTRY_CONN)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusBatteryConnected))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_BM_CONNTECTED)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusBmuConnected))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_MAIN_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusMainCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_ADDITIONAL_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusAdditionalCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_EQUALIZE_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusEqualizing))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_EQUALIZE_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusMaintenanceCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_CHARG_COMPLETED)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusChargingCompleted))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_PAUSE)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusPaused))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_PRE_CHARG)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusPreCharging))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_FORCE)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusForceStart))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_BBC)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusBbc))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_TBATTLOW)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusLowBatteryTemperature))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_TBATTHIGH)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusHighBatteryTemperature))
      return false;
  if (chargingAlgorithmStatus & CHALG_STAT_RESTRICTED)
    if (!NfcMessageHandlerEncodeU32Field(stream, field,
                                         eDeviceStatusChargingRestricted))
      return false;

  return true;
}
