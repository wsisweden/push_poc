/*
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Nfc/INIT.H
*
*	\ingroup	NFC
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	14-09-2018 / Andreas Carmvall
*
*******************************************************************************/

#ifndef NFC_INIT_H_INCLUDED
#define NFC_INIT_H_INCLUDED

#include <global.h>
#include <iicmstr.h>

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	iicmstr_SlaveInfo const_P *	pNfcSlave;	/**< Pointer to NFC slave info	*/
	protif_Interface const_P * pMstrIf;	/**< I2C master interface			*/
	sys_FBInstId mstrInstId;	/**< I2C master instance id			*/
} nfc_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_NFC
#elif defined(EXE_GEN_NFC)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_NFC
#endif

#define EXE_APPL(n)			n##nfc

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN		/* Non-volatile numeric	*/
//#define EXE_USE_NS	/* Non-volatile string	*/
#define EXE_USE_VN		/* Volatile numeric		*/
//#define EXE_USE_VS	/* Volatile string		*/
#define EXE_USE_PN	/* Process-point numeric	*/
//#define EXE_USE_PS	/* Process-point string	*/
//#define EXE_USE_BL	/* Block of byte-data	*/

/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name		Flags				Dim	Type	Def	Min		Max
            -----------	-------------------	---	-------	---	-------	----*/
EXE_REG_VN( NfcInfoH, SYS_REG_DEFAULT, 1, Uint32, 0xFFFFFFFFUL, 0, 0xFFFFFFFFUL )
EXE_REG_VN( NfcInfoL, SYS_REG_DEFAULT, 1, Uint32, 0xFFFFFFFFUL, 0, 0xFFFFFFFFUL )

/*******************************************************************************
 *  HMI attributes
 */

EXE_MMI_NONE( NfcInfoH )
EXE_MMI_NONE( NfcInfoL )

/*******************************************************************************
 * References to external registers
            local name                  register name           owning instance
            -----------------------     ----------------------- ------------------*/
EXE_REG_EX( nfc__FirmwareVersionMain,   FirmwareVerMain,        sup1 )
EXE_REG_EX( nfc__FirmwareTypeMain,      FirmwareTypeMain,       sup1 )

EXE_REG_EX( nfc__HeatSinkTemperature,   Ths,                    meas1 )
EXE_REG_EX( nfc__BatteryVoltage,        UmeasSum_Real,          chalg1 )
EXE_REG_EX( nfc__BatteryCurrent,        ImeasSum_Real,          chalg1 )
EXE_REG_EX( nfc__ChargerVoltage,        Umeas1s,                meas1 )
EXE_REG_EX( nfc__ChargerCurrent,        Imeas1s,                meas1 )
EXE_REG_EX( nfc__DeviceTime,            LocalTime,              sup1 )
EXE_REG_EX( nfc__AlgorithmName,         currAlgName,            chalg1 )
EXE_REG_EX( nfc__AlgorithmNumber,       currAlgNo,              chalg1 )
EXE_REG_EX( nfc__AlgorithmVersion,      currVersion,            chalg1 )
EXE_REG_EX( nfc__Capacity,              currCapacity,           chalg1 )
EXE_REG_EX( nfc__CableResistance,       currCableRes,           chalg1 )
EXE_REG_EX( nfc__Cells,                 currCells,              chalg1 )
EXE_REG_EX( nfc__BaseLoad,              currBaseLoad,           chalg1 )
EXE_REG_EX( nfc__Cycles2To25,           CyclesSumBelow25,       sup1 )
EXE_REG_EX( nfc__Cycles26To50,          CyclesSum26to50,        sup1 )
EXE_REG_EX( nfc__Cycles51To80,          CyclesSum51to80,        sup1 )
EXE_REG_EX( nfc__Cycles81To90,          CyclesSum81to90,        sup1 )
EXE_REG_EX( nfc__CyclesAbove90,         CyclesSumAbove90,       sup1 )
EXE_REG_EX( nfc__TotalChargeTime,       ChargeTimeTotal,        sup1 )
EXE_REG_EX( nfc__TotalChargeAh,         ChargedAhTotal,         sup1 )
EXE_REG_EX( nfc__TotalAcConsumtion,     ChargedWhTotal,         sup1 )
EXE_REG_EX( nfc__AcPowerConsumption,    AcWhTotal,              sup1 )
EXE_REG_EX( nfc__OverVoltageCount,      OverVoltCnt,            meas1 )
EXE_REG_EX( nfc__HistoryTime,           LatestHistLogTime,      sup1)
EXE_REG_EX( nfc__EventTime,             LatestEvtLogTime,       sup1)
EXE_REG_EX( nfc__HistoryHeadIndex,      histLogIndex,           cm1 )
EXE_REG_EX( nfc__EventHeadIndex,        eventLogIndex,          cm1 )
EXE_REG_EX( nfc__InstantHeadIndex,      instLogIndex,           cm1 )
EXE_REG_EX( nfc__HistoryCount,          histLogCount,           cm1 )
EXE_REG_EX( nfc__EventCount,            eventLogCount,          cm1 )
EXE_REG_EX( nfc__InstantCount,          instLogCount,           cm1 )
EXE_REG_EX( nfc__InstantHeaderIndex,    InstLogHdrIndex,        cm1 )
EXE_REG_EX( nfc__hInstHeaderTime,       InstLogHdrTime,         cm1 )
EXE_REG_EX( nfc__RegulatorStatus,       Status,                 regu1 )
EXE_REG_EX( nfc__ChargingAlgorithmError, ChalgError,            chalg1 )
EXE_REG_EX( nfc__ChargingAlgorithmStatus, ChalgStatus,          chalg1 )
EXE_REG_EX( nfc__ActiveRadioPanId,      PanId,                  radio1 )
EXE_REG_EX( nfc__ActiveRadioChannel,    Channel,                radio1 )
EXE_REG_EX( nfc__RadioFirmwareVersion,  FirmwareVerRF,          radio1 )
EXE_REG_EX( nfc__RadioFirmwareType,     FirmwareTypeRF,         radio1 )
EXE_REG_EX( nfc__supStatus,             statusFlags,            sup1 )
//EXE_REG_EX( nfc__userParam,             UserParam,              chalg1 )
EXE_REG_EX( nfc__storedUserParam,       storedUserParam,        chalg1 )
EXE_REG_EX( nfc__storedAlgorithmID,     storedAlgorithmID,      chalg1 )
EXE_REG_EX( nfc__storedAlgorithmVer,    storedAlgorithmVer,     chalg1 )

EXE_REG_EX( nfc__EngineCode,            EngineCode,             sup1 )
EXE_REG_EX( nfc__DefAlgorithmNumber,    algNo_default,          chalg1 )
EXE_REG_EX( nfc__DefCapacity,           capacity_default,       chalg1 )
EXE_REG_EX( nfc__DefCableResistance,    cableRes_default,       chalg1 )
EXE_REG_EX( nfc__DefCells,              cells_default,          chalg1 )
EXE_REG_EX( nfc__DefBaseLoad,           baseLoad_default,       chalg1 )
EXE_REG_EX( nfc__ChargerId,             ChargerId,              radio1 )
EXE_REG_EX( nfc__IDcLimit,              IdcLimit,               regu1 )
EXE_REG_EX( nfc__IAcLimit,              IacLimit,               regu1 )
EXE_REG_EX( nfc__PAcLimit,              PacLimit,               regu1 )
EXE_REG_EX( nfc__PowerGroup,            PowerGroup,             regu1 )
EXE_REG_EX( nfc__InstantLogPeriod,      InstLogSamplePeriod,    sup1 )
EXE_REG_EX( nfc__SecurityCode1,         SecurityCode1,          gui1 )
EXE_REG_EX( nfc__SecurityCode2,         SecurityCode2,          gui1 )
EXE_REG_EX( nfc__DefBatteryType,        BatteryType_default,    chalg1 )
EXE_REG_EX( nfc__DefBatteryTemperature, BatteryTemp_default,    chalg1 )
EXE_REG_EX( nfc__BitConfig,             BitConfig,              chalg1 )
EXE_REG_EX( nfc__RadioPanId,            setPanId,               radio1 )
EXE_REG_EX( nfc__RadioChannel,          setChannel,             radio1 )
EXE_REG_EX( nfc__RadioNodeAddress,      setNodeId,              radio1 )
EXE_REG_EX( nfc__RadioNwkParam,      	setNwkParam,            radio1 )
EXE_REG_EX( nfc__DisplayContrast,       DisplayContrast,        gui1 )
EXE_REG_EX( nfc__LedBrightnessMax,      LedBrightnessMax,       tui1 )
EXE_REG_EX( nfc__LedBrightnessDim,      LedBrightnessDim,       tui1 )
EXE_REG_EX( nfc__Language,              Language,               gui1 )
EXE_REG_EX( nfc__TimeDateFormat,        TimeDateFormat,         gui1 )
EXE_REG_EX( nfc__ButtonF1Function,      ButtonF1_func,          sup1 )
EXE_REG_EX( nfc__ButtonF2Function,      ButtonF2_func,          sup1 )
EXE_REG_EX( nfc__RemoteInFunction,      RemoteIn_func,          sup1 )
EXE_REG_EX( nfc__RemoteOutFunction,     RemoteOut_func,         sup1 )
EXE_REG_EX( nfc__RemoteOutAlarmVariable, RemoteOutAlarm_var,    sup1 )
EXE_REG_EX( nfc__RemoteOutPhaseVariable, RemoteOutPhase_var,    sup1 )
EXE_REG_EX( nfc__WateringFunction,      Water_func,             sup1 )
EXE_REG_EX( nfc__WateringVariable,      Water_var,              sup1 )
EXE_REG_EX( nfc__AirPumpVariable,       AirPump_var,            sup1 )
EXE_REG_EX( nfc__AirPumpVariable2,      AirPump_var2,           sup1 )
EXE_REG_EX( nfc__EqualizeFunction,      Equalize_func,          sup1 )
EXE_REG_EX( nfc__EqualizeVariable,      Equalize_var,           sup1 )
EXE_REG_EX( nfc__EqualizeVariable2,     Equalize_var2,          sup1 )
EXE_REG_EX( nfc__TimeRestriction,       TimeRestriction,        sup1 )
EXE_REG_EX( nfc__RadioMode,             RadioMode,              radio1 )
EXE_REG_EX( nfc__RadioNetworkSettings,  NwkSettings,            radio1 )
EXE_REG_EX( nfc__BacklightTime,         backlight_delay,        gui1 )
EXE_REG_EX( nfc__ExtraChargeFunction,   ExtraCharge_func,       sup1 )
EXE_REG_EX( nfc__ExtraChargeVariable,   ExtraCharge_var,        sup1 )
EXE_REG_EX( nfc__ExtraChargeVariable2,  ExtraCharge_var2,       sup1 )
EXE_REG_EX( nfc__ExtraChargeVariable3,  ExtraCharge_var3,       sup1 )
EXE_REG_EX( nfc__AirPumpAlarmLow,       AirPumpAlarmLow,        IObus1 )
EXE_REG_EX( nfc__AirPumpAlarmHigh,      AirPumpAlarmHigh,       IObus1 )
EXE_REG_EX( nfc__DefChargingMode,       ChargingMode_default,   chalg1 )
EXE_REG_EX( nfc__BbcFunction,           BBC_func,               sup1 )
EXE_REG_EX( nfc__BbcVariable,           BBC_var,                sup1 )
EXE_REG_EX( nfc__Routing,               Routing,                radio1 )
EXE_REG_EX( nfc__RemoteOutBbcVariable,  RemoteOutBBC_var,       sup1 )
EXE_REG_EX( nfc__DplPowerLimitTotal,    DplPowerLimitTotal,     regu1 )
EXE_REG_EX( nfc__DplFunction,           PowerGroup_func,        regu1 )
EXE_REG_EX( nfc__DplPriorityFactor,     DplPriorityFactor,      regu1 )
EXE_REG_EX( nfc__DplPowerLimitDefault,  DplPacLimit_default,    regu1 )
EXE_REG_EX( nfc__PinOutSelect,          PinOutSelect,           IObus1 )
EXE_REG_EX( nfc__PinInSelect,           PinInSelect,            IObus1 )
EXE_REG_EX( nfc__TimeTableControl,      Ctrl,                   sup1 )
EXE_REG_EX( nfc__TimeTableFromHour,     FromHour,               sup1 )
EXE_REG_EX( nfc__TimeTableFromMin,      FromMin,                sup1 )
EXE_REG_EX( nfc__TimeTableToHour,       ToHour,                 sup1 )
EXE_REG_EX( nfc__TimeTableToMin,        ToMin,                  sup1 )

#if PROJECT_USE_CAN == 1
EXE_REG_EX( nfc__CanMode,               CAN_CommProfile,        can1 )
EXE_REG_EX( nfc__CanNodeId,             NodeIdFixed,            can1 )
EXE_REG_EX( nfc__CanNetworkControl,     ParallelSelect,         cc1 )
EXE_REG_EX( nfc__ParallelControlFunction, ParallelControl_func, cc1 )
EXE_REG_EX( nfc__CanBitPerSecond,       CanBps,                 can1 )
#endif

EXE_REG_EX( nfc__SetRTC,                setRTC,                 sup1 )

EXE_REG_EX( nfc__SetCalibrationVoltage, Ucalib,                 meas1 )
EXE_REG_EX( nfc__SetCalibrationCurrent, Icalib,                 meas1 )

EXE_REG_EX( nfc__ClearStatistics,       ClearStatistics,        sup1 )
EXE_REG_EX( nfc__FactoryDefaults,       FactoryDefaults,        sup1 )

/******************************************************************************* 
 * read/write indications
 * (sorted alphabetically by instance name and by declaration order)
 */
EXE_IND_EX( reset,    reset,    sup1 )

/******************************************************************************* 
 * Language-dependent texts 
 *  - tExampleTxtHandle1 is reserved handlename and it's discarded by 
 *		textparser tools.
 *  - you can add txthandle comment at the end of the texthandle line, and this
 *		is parsed by textparser tools.
 */

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
