/*
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		isr.C
*
*	\ingroup	NFC
*
*	\brief		Contains the code that needs to be executed at 1ms intervalls.
*
*	\details
*
*	\note
*
*	\version	14-09-2018 / Andreas Carmvall
*
*******************************************************************************/

#include "local.h"
#include "nfcIsr.h"

/*	
 *  This code is to be included inside the 1ms ISR
 */
inline void nfc_isr(void)
{
	nfc__Inst* nfc_pInst = nfc__pInst;

	if (nfc_pInst->flags & NFC__FLG_RUNNING) {
		nfc_pInst->msCounter1++;

    // 2ms has elapsed.
		if (nfc_pInst->msCounter1 % 2 == 0) {
			nfc_pInst->triggers |= NFC__TRG_POLL;

			osa_semaSetIsr(&nfc_pInst->taskSema);
		}
	}
}
