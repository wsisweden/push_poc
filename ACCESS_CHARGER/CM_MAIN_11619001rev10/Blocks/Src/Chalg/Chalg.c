/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Chalg.c
*
*	\ingroup	CHALG
*
*	\brief		CHALG main source file.Exception occurred executing command line.
Cannot run program "C:\_jakob\Access\Tietoleite\SVN\CM\CM (Debug).launch" (in directory "C:\_jakob\Access\Tietoleite\SVN\CM"): CreateProcess error=193, %1 �r inte ett giltigt Win32-program

*
*	\details	The function block interface is implemented in this file.
*
*	\note
*
*	\version	dd-mm-2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	CHALG		CHALG
*
*	\brief		Charging algorithm.
*
********************************************************************************
*
*	\details	CHALG calculates the set value for REGU according to the 
*				charging algorithm.
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "string.h"

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "deb.h"

#include "txt.h"
#include "global.h"

#if TARGET_SELECTED & TARGET_W32
typedef Boolean _Bool;
#endif

#include "Chalg.h"
#include "Regu.h"
#include "cai_cc.h"
#include "sup.h"
#include "radio.h"
#include "cm.h"
#include "gui.h"
#include "IObus.h"
#include "../IObus/IObusOutputs.h"

#include <math.h>
#include <float.h>

#include "cai_cc.h"
#include "inc_cm3/cai_constparam.h"
#include "inc_cm3/caimain.h"
#include "inc_cm3/I01_10_06.h"
#include "inc_cm3/I02_10_04.h"
#include "inc_cm3/I03_20_09.h"
#include "inc_cm3/I04_CFLA_01.h"
#include "inc_cm3/I05_CIMX_01.h"
#include "inc_cm3/I06_OP25_01.h"
#include "inc_cm3/I07_OP30_01.h"
#include "inc_cm3/I08_OP35_01.h"
#include "inc_cm3/I09_OP40_01.h"
#include "inc_cm3/I11_FCIUI.h"
#include "inc_cm3/I12_FCIU.h"
#include "inc_cm3/I13_CFLA_02.h"
#include "inc_cm3/I16_10_05.h"
#include "inc_cm3/I20_23_03.h"
#include "inc_cm3/I21_10_16.h"
#include "inc_cm3/I22_10_99.h"
#include "inc_cm3/I23_OP25_02.h"
#include "inc_cm3/I24_OP30_02.h"
#include "inc_cm3/I25_OP35_02.h"
#include "inc_cm3/I26_OP40_02.h"
#include "inc_cm3/I27_OP45_01.h"
#include "inc_cm3/I28_10_98.h"
#include "inc_cm3/I29_FCI25_02.h"
#include "inc_cm3/I31_10_97.h"
#include "inc_cm3/I32_10_96.h"
#include "inc_cm3/I34_SC03_01.h"
#include "inc_cm3/I35_SC06_01.h"
#include "inc_cm3/I36_SC12_01.h"
#include "inc_cm3/I37_SC24_01.h"
#include "inc_cm3/I38_OP25_03.h"
#include "inc_cm3/I41_10_18.h"
#include "inc_cm3/I43_OPXX_01.h"
#include "inc_cm3/I46_10_95.h"
#include "inc_cm3/I47_FVLA.h"
#include "inc_cm3/I52_10_21.h"
#include "inc_cm3/I53_10_22.h"
#include "inc_cm3/I57_80_03.h"
#include "inc_cm3/I62_Float.h"

//#warning debug
//#include "inc_cm3/I99_TEST.h"
//#include "inc_cm3/Demo_steps.h"

#include "engines_types.h"
#include "../Regu/pause.h"
#include "../Cc/ClusterControl.h"
/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Task execution triggers
 */

/*@{*/
#define CHALG__TRG_SENDID		(1<<0)	/**< Send radio ID pending.			*/
#define CHALG__TRG_MAINS		(1<<1)	/**< Mains state changed.			*/
#define CHALG__TRG_INIT			(1<<2)	/**< Perform warm init				*/
#define CHALG__TRG_FORCE		(1<<3)	/**< Force start					*/
#define CHALG__TRG_NEWCHARGE	(1<<4)	/**< New charge started             */
#define CHALG__TRG_EQUALIZE		(1<<5)	/**< Equalize triggered             */
#define CHALG__TRG_BATTERYTEMP	(1<<6)	/**< Equalize triggered             */
#define CHALG__TRG_LANGUAGE		(1<<7)	/**< Language triggered             */
#define CHALG__TRG_BMUBATT		(1<<8)	/**< BmUbatt triggered             	*/
#define CHALG__TRG_BMCABLERI	(1<<9)	/**< currCableResBm triggered       */
#define CHALG__TRG_CECMODE		(1<<10)	/**< CEC mode register written      */
#define CHALG__TRG_AIRPUMP_VAR	(1<<11)	/**< AirPump_var register written   */
#define CHALG__TRG_AIRPUMP_VAR2	(1<<12)	/**< AitrPump_var2 register written */
#define CHALG__TRG_AIRPUMP_LOW	(1<<13)	/**< AirPump low register written   */
#define CHALG__TRG_AIRPUMP_HIGH	(1<<14)	/**< AitrPump high register written */
#define CHALG__TRG_WATER_VAR	(1<<15)	/**< Water_var register written   	*/
#define CHALG__TRG_BITCONFIG	(1<<16)	/**< BitConfig register written     */

/*@}*/

/**
 * \name	Equalize triggers
 */

/*@{*/
#define CHALG__EQU_MANUAL	(1<<0)		/**< Manual trigger F1/F2.			*/
#define CHALG__EQU_WEEKDAY	(1<<1)		/**< Weekday trigger.				*/
#define CHALG__EQU_CYCLIC	(1<<2)		/**< Cyclic trigger					*/
/*@}*/

/**
 * \name	Charging states
 */

/*@{*/
#define CHALG_STAT_CHARGING	(CHALG_STAT_PRE_CHARG|CHALG_STAT_MAIN_CHARG|CHALG_STAT_ADDITIONAL_CHARG|CHALG_STAT_EQUALIZE_CHARG|CHALG_STAT_MAINTENANCE_CHARG|CHALG_STAT_CHARG_COMPLETED)
/*@}*/

/**
 * Regu and chalg error mask
 */
/*@{*/
#define CHALG_REGU_ERR_MASK		(/*REGU_ERR_PHASE_ERROR|*/REGU_ERR_REGULATOR_ERROR|REGU_ERR_LOW_CHARGER_TEMPERATURE|/*REGU_ERR_HIGH_CHARGER_TEMPERATURE|*/REGU_ERR_HIGH_TRAFO_TEMPERATURE|REGU_ERR_LOW_BOARD_TEMPERATURE|REGU_ERR_HIGH_BOARD_TEMPERATURE/*|REGU_ERR_WATCHDOG*/)
#define CHALG_CHALG_ERR_MASK	(CHALG_ERR_LOW_BATTERY_VOL|CHALG_ERR_HIGH_BATTERY_VOL|CHALG_ERR_CHARGE_AH_LIMIT|CHALG_ERR_CHARGE_TIME_LIMIT|CHALG_ERR_INCORRECT_ALGORITHM|/*CHALG_ERR_INCORRECT_MOUNT|*/CHALG_ERR_HIGH_BATTERY_LIMIT|CHALG_ERR_BATTERY_ERROR)
/*@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Template function block instance type
 */
 
typedef struct chalg__inst {			/*''''''''''''''''''''''''''''' RAM	*/
	chalg_Init const_P * 	pInit;		/**< Initialization data			*/
	sys_FBInstId			instId;		/**< Instance id					*/
	Uint16					chalgStatus;/**< FB status flags.				*/
	Uint32					triggers;	/**< Trigger flags.					*/
	Boolean					test;		/**< Internal test flag				*/
	float AcWs;							/**< Consumed Ac power Watt*sec		*/
	Uint32 ChargeTime;					/**< Charge time in seconds         */
	Uint32 aUVc[6];						/**< Array with UVc 5s back in time */
	Uint32 BBCStatusTime;				/**< Time(s)since last status change*/
	Int16 BatteryTemp;					/**< Static Battery temperature		*/
	Uint8 BBCStatus;					/**< Masked out from chalgStatus 	*/
	Uint8 ForceStartTimer;				/**< Timer for force start.			*/
	Uint16 CableResBm;					/**< Cable Ri from BM				*/
	float CableResOld;					/**< Cable Ri used in calculation	*/
	Uint8 CECModeOld;					/**< CEC mode static variable		*/
	Uint16 AirPumpPumpOn;				/**< AirPump_var process variable	*/
	Uint16 AirPumpPumpCycle;			/**< AirPump_var2 process variable	*/
	Uint16 AirPumpAlarmLow;				/**< AirPump low process variable	*/
	Uint16 AirPumpAlarmHigh;			/**< AirPump high process variable	*/
	Uint16 WaterDuration;				/**< Water_var process variable		*/
} chalg__Inst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
chalg__Inst* chalgInst;

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE reg_Status	chalg__validateBattInfo(sys_RegHandle,void const_D *,void const_P *);
PRIVATE reg_Status	chalg__validateBattInfoMulti(sys_RegHandle,void const_D *,void const_P *);
PRIVATE void Chalg_SaveBatteryMeasurements(BatteryMeas_type*     BatteryMeas);
static void setUserParameters(UserParam_Type* UserParam, chalg__Inst* pInst, int len);
static void readDefaultParameters(UserParam_Type* UserParam);
static void updateUserParameters(const AlgDef_ConstPar_Type *AlgDef_p);
static void resetUserParameters(UserParam_Type* UserParam);
static void storeUserParameters(int len);
/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

PUBLIC sys_TypeTools		chalg_BattInfoTools = {
	sizeof(chalg_BattInfoType),
	&chalg__validateBattInfo
};

PUBLIC sys_TypeTools		chalg_BattInfoMultiTools = {
	sizeof(chalg_BattInfoMultiType),
	&chalg__validateBattInfoMulti
};

/**
 * Ram array for storing the charging parameter package.
 */

PRIVATE BYTE				chalg__caPack[1000];


PUBLIC chalg_CaPackHandle const_P chalg_caPackHandle = {
	chalg__caPack
};

volatile int nfcUserParamsEdited;
volatile int nfcStoredUserParamsEdited;
/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
PRIVATE void ChalgCalculateOutput(const Output_enum in, unsigned int* timer, sys_RegHandle reg, sys_RegHandle reg2);
PRIVATE const AlgDef_ConstPar_Type* ChargingAlgorithms[] = {&I01_10_06_AlgDef_ConstPar,
															&I02_10_04_AlgDef_ConstPar,
															&I03_20_09_AlgDef_ConstPar,
															&I04_CFLA_01_AlgDef_ConstPar,
															&I05_CIMX_01_AlgDef_ConstPar,
															&I06_OP25_01_AlgDef_ConstPar,
															&I07_OP30_01_AlgDef_ConstPar,
															&I08_OP35_01_AlgDef_ConstPar,
															&I09_OP40_01_AlgDef_ConstPar,
															&I11_FCIUI_AlgDef_ConstPar,
															&I12_FCIU_AlgDef_ConstPar,
															&I13_CFLA_02_AlgDef_ConstPar,
															&I16_10_05_AlgDef_ConstPar,
															&I20_23_03_AlgDef_ConstPar,
															&I21_10_16_AlgDef_ConstPar,
															&I22_10_99_AlgDef_ConstPar,
															&I23_OP25_02_AlgDef_ConstPar,
															&I24_OP30_02_AlgDef_ConstPar,
															&I25_OP35_02_AlgDef_ConstPar,
															&I26_OP40_02_AlgDef_ConstPar,
															&I27_OP45_01_AlgDef_ConstPar,
															&I28_10_98_AlgDef_ConstPar,
															&I29_FCI25_02_AlgDef_ConstPar,
															&I31_10_97_AlgDef_ConstPar,
															&I32_10_96_AlgDef_ConstPar,
															&I34_SC03_01_AlgDef_ConstPar,
															&I35_SC06_01_AlgDef_ConstPar,
															&I36_SC12_01_AlgDef_ConstPar,
															&I37_SC24_01_AlgDef_ConstPar,
															&I38_OP25_03_AlgDef_ConstPar,
															&I41_10_18_AlgDef_ConstPar,
															&I43_OPXX_01_AlgDef_ConstPar,
															&I46_10_95_AlgDef_ConstPar,
															&I47_FVLA_AlgDef_ConstPar,
															&I52_10_21_AlgDef_ConstPar,
															&I53_10_22_AlgDef_ConstPar,
															&I57_80_03_AlgDef_ConstPar,
															&I62_Float_AlgDef_ConstPar};
															//&I99_TEST_AlgDef_ConstPar};	// Used during function testing

PRIVATE const AlgDef_ConstPar_Type* ChargingAlgorithmsCEC[] = {&I04_CFLA_01_AlgDef_ConstPar,
															   &I05_CIMX_01_AlgDef_ConstPar,
															   &I06_OP25_01_AlgDef_ConstPar,
															   &I07_OP30_01_AlgDef_ConstPar,
															   &I08_OP35_01_AlgDef_ConstPar,
															   &I09_OP40_01_AlgDef_ConstPar,
															   &I13_CFLA_02_AlgDef_ConstPar,
															   &I23_OP25_02_AlgDef_ConstPar,
															   &I24_OP30_02_AlgDef_ConstPar,
															   &I25_OP35_02_AlgDef_ConstPar,
															   &I26_OP40_02_AlgDef_ConstPar,
															   &I27_OP45_01_AlgDef_ConstPar};
															   //&I99_TEST_AlgDef_ConstPar};	// Used during function testing

PRIVATE const AlgDef_ConstPar_Type* ChargingAlgorithmsUserP[] = {&I03_20_09_AlgDef_ConstPar,
																 &I43_OPXX_01_AlgDef_ConstPar,
																 &I47_FVLA_AlgDef_ConstPar,
																 &I53_10_22_AlgDef_ConstPar,
																 &I57_80_03_AlgDef_ConstPar};
															   //&I99_TEST_AlgDef_ConstPar};	// Used during function testing

PRIVATE CaiInput_type  CaiInput;
PRIVATE CaiOutput_type CaiOutput;
volatile int BatteryConnected = 0;
int	HighVoltageLimit = 0;
int chargingParamOk = 0;
volatile float chargerForceStart = NAN;

/* Air pump failure */
PRIVATE bool AirPumpOk = TRUE;		// Air pump OK (no error flag) - updated all the time
static bool AirPumpFailed = FALSE;	// Air pump error flag to remain until battery disconnected
static bool AirPumpHPEflag  = FALSE;// Air pump high pressure error flag
static bool AirPumpLPEflag  = FALSE;// Air pump low pressure error flag

/* Output air/water functions from charging algorithm */
static bool ChalgAirPumpOn = FALSE;	// Air pump function from Chalg
static bool ChalgWaterPumpOn = FALSE;// Water function from Chalg

/* Cable resistance event */
static Uint16 cableResCalcStart = 0;// [mOhm]
static Uint16 cableResCalcMax = 0;	// [mOhm]
static Uint16 cableRes_tick = 0;	// [min]
/**********************************/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid		An index (0..) that is accepted by various functions
*						to identify the instance. Should be kept for later use,
*						or may be discarded if not required by the FB.
*	\param		pArgs	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * chalg_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	chalg__Inst *				pInst;

	pInst = (chalg__Inst *) mem_reserve(&mem_normal, sizeof(chalg__Inst));
	chalgInst = pInst;

	pInst->pInit = (chalg_Init const_P *) pArgs;

	pInst->chalgStatus = 0;
	pInst->instId = iid;
	pInst->test = TRUE;
	pInst->triggers = CHALG__TRG_INIT|CHALG__TRG_AIRPUMP_VAR|CHALG__TRG_AIRPUMP_VAR2|CHALG__TRG_AIRPUMP_LOW|CHALG__TRG_AIRPUMP_HIGH|CHALG__TRG_WATER_VAR|CHALG__TRG_BITCONFIG;
	pInst->AcWs = 0;
	pInst->aUVc[0] = 0;
	pInst->aUVc[1] = 0;
	pInst->aUVc[2] = 0;
	pInst->aUVc[3] = 0;
	pInst->aUVc[4] = 0;
	pInst->aUVc[5] = 0;
	pInst->ChargeTime = 0;
	pInst->BBCStatusTime = 0;
	pInst->BBCStatus = 0;
	pInst->AirPumpPumpOn = 0;
	pInst->AirPumpPumpCycle = 0;
	pInst->AirPumpAlarmLow = 0;
	pInst->AirPumpAlarmHigh = 0;
	pInst->WaterDuration = 0;

	return((void *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		instance	Pointer to instance.
* 	\param		pArgs		Pointer to the initialization arguments.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void chalg_reset(
	void *					instance,
	void const_P *			pArgs
) {	
	chalg__Inst *			pInst;

	DUMMY_VAR(pArgs);

	pInst = (chalg__Inst *) instance;

	pInst->chalgStatus &= ~CHALG_STAT_DOWN;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		instance	Pointer to instance.
*	\param		nType		Down type.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void chalg_down(
	void *					instance, 
	sys_DownType			nType
) {
	chalg__Inst *			pInst;

	pInst = (chalg__Inst *) instance;

	pInst->chalgStatus |= CHALG_STAT_DOWN;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read
*				the value of a parameter owned by this FB.
*
*	\param		instance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue		Pointer to the storage for the value.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus chalg_read(
	void *					instance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex,
	void *					pValue
) {	
	chalg__Inst *			pInst;
	sys_IndStatus			retVal;

	pInst = (chalg__Inst *) instance;

	DUMMY_VAR(aIndex);

	switch(rIndex)
	{
		case reg_indic_ChalgStatus:
			*((Uint16 *)pValue) = pInst->chalgStatus;
			retVal = REG_OK;
			break;
/*
		case reg_indic_ChargingModeP:
			retVal = reg_getLocal(pValue, reg_i_ChargingMode, pInst->instId);
			break;
*/
		case reg_indic_chalgPhase:
			if (pInst->chalgStatus & CHALG_STAT_RESTRICTED)
			{
				*((Uint8 *) pValue) = 6;
			}
			else if (pInst->chalgStatus & CHALG_STAT_PAUSE)
			{
				*((Uint8 *) pValue) = 6;
			}
			else if (pInst->chalgStatus & CHALG_STAT_PRE_CHARG)
			{
				*((Uint8 *) pValue) = 0;
			}
			else if (pInst->chalgStatus & CHALG_STAT_MAIN_CHARG)
			{
				*((Uint8 *) pValue) = 1;
			}
			else if (pInst->chalgStatus & CHALG_STAT_ADDITIONAL_CHARG)
			{
				*((Uint8 *) pValue) = 2;
			}
			else if (pInst->chalgStatus & CHALG_STAT_MAINTENANCE_CHARG)
			{
				*((Uint8 *) pValue) = 3;
			}
			else if (pInst->chalgStatus & CHALG_STAT_EQUALIZE_CHARG)
			{
				*((Uint8 *) pValue) = 4;
			}
			else
			{
				*((Uint8 *) pValue) = 5;
			}
			retVal = REG_OK;
			break;

		case reg_indic_UVc:
		{
			Uint16 cells;

			reg_getLocal(&cells, reg_i_currCells, pInst->instId);

			if (cells != 0)
			{
				Uint32 Uchalg;
				reg_getLocal(&Uchalg, reg_i_UmeasSum_Real, pInst->instId);
				*((Uint32 *) pValue) = (Uint32) (Uchalg / cells);
			}
			else
			{
				*((Uint32 *) pValue) = 0;
			}
			retVal = REG_OK;
			break;
		}

		case reg_indic_AlarmCount:
		{
			Uint8 * pAlarmCount;
			Uint32	errorSum;
			Uint16	u16Temp;
			
			/*
			 * Calculate how many bits are set in the error sum registers.
			 */

			pAlarmCount = (Uint8 *) pValue;
			*pAlarmCount = 0;

			reg_getLocal(&u16Temp, reg_i_ChalgErrorSum, pInst->instId);
			errorSum = u16Temp;

			{
				ChargerStatusFields_type Status;
				chargerAlarm_type reguError;

				reg_get(&Status, chalg__reguStatusSum);
				reguError.Warning = Status.Detail.Warning;
				reguError.Error = Status.Detail.Error;
				u16Temp = reguError.Sum;
			}
			errorSum |= u16Temp << 16;

			while (errorSum != 0)
			{
				if (errorSum & 1)
				{
					(*pAlarmCount)++;
				}
				errorSum >>= 1;
			}
			retVal = REG_OK;
			break;
		}

		case reg_indic_BBCStatusTime:
			*((Uint32 *)pValue) = pInst->BBCStatusTime;
			retVal = REG_OK;
			break;

		case reg_indic_BBCStatus:
			*((Uint8 *)pValue) = pInst->BBCStatus;
			retVal = REG_OK;
			break;

		case reg_indic_chalgSoc:
		{
			Uint8 chalgSoc;
			Uint8 chargedAhP;

			reg_getLocal(&chargedAhP, reg_i_chargedAhPercent, pInst->instId);

			chalgSoc = 20 + chargedAhP;

			if((chalgSoc > 100) ||
			   (pInst->chalgStatus & CHALG_STAT_ADDITIONAL_CHARG) ||
			   (pInst->chalgStatus & CHALG_STAT_CHARG_COMPLETED)){
				chalgSoc = 100;
			}

			*((Uint8 *)pValue) = chalgSoc;
			retVal = REG_OK;
			break;
		}

		default:
			retVal = REG_UNAVAIL;
			break;
	}
	
	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		instance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC sys_IndStatus chalg_write(
	void *					instance, 
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex
) {
	chalg__Inst *			pInst;

	pInst = (chalg__Inst *) instance;

	DUMMY_VAR(aIndex);
	
	switch(rIndex)
	{
		case reg_indic_mains:
			pInst->triggers |= CHALG__TRG_MAINS;
			break;

		case reg_indic_reset :	// 100922 jakob
			pInst->triggers |= CHALG__TRG_INIT;
			break;

		case reg_indic_ForceStart :
			pInst->triggers |= CHALG__TRG_FORCE;
			break;

		case reg_indic_newCharge :
			pInst->triggers |= CHALG__TRG_NEWCHARGE;
			break;

		case reg_indic_Equalize_active :
			pInst->triggers |= CHALG__TRG_EQUALIZE;
			break;

		case reg_indic_BatteryTemp_default :
			pInst->triggers |= CHALG__TRG_BATTERYTEMP;
			break;

		case reg_indic_Language :
			pInst->triggers |= CHALG__TRG_LANGUAGE;
			break;

		case reg_indic_BmUbatt :
			pInst->triggers |= CHALG__TRG_BMUBATT;
			break;

		case reg_indic_currCableResBm :
			pInst->triggers |= CHALG__TRG_BMCABLERI;
			break;

		case reg_indic_CECMode :
			pInst->triggers |= CHALG__TRG_CECMODE;
			break;

		case reg_indic_AirPump_var :
			pInst->triggers |= CHALG__TRG_AIRPUMP_VAR;
			break;

		case reg_indic_AirPump_var2 :
			pInst->triggers |= CHALG__TRG_AIRPUMP_VAR2;
			break;

		case reg_indic_AirPumpAlarmLow :
			pInst->triggers |= CHALG__TRG_AIRPUMP_LOW;
			break;

		case reg_indic_AirPumpAlarmHigh :
			pInst->triggers |= CHALG__TRG_AIRPUMP_HIGH;
			break;

		case reg_indic_Water_var :
			pInst->triggers |= CHALG__TRG_WATER_VAR;
			break;

		case reg_indic_BitConfig :
			pInst->triggers |= CHALG__TRG_BITCONFIG;
			break;
	}


	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_ctrl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		instance	Pointer to the data of the instance.
*	\param		pCtrl		Pointer to the control indication.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void chalg_ctrl(
	void *					instance,
	sys_CtrlType *			pCtrl
) {
	DUMMY_VAR(pCtrl);

	atomic(
		((chalg__Inst *) instance)->chalgStatus |= CHALG_STAT_DOWN;
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_test
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Self test
*
*	\param		instance	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 chalg_test(
	void *					instance
) {
	chalg__Inst *			pInst;

	pInst = (chalg__Inst *) instance;
	
	if (
		osa_syncPeek(project_stateMachSync) &&
		not(pInst->chalgStatus & CHALG_STAT_DOWN)
	) {
		/*
		 *	Only do test check if sup's sync flag is set.
		 */
		if(pInst->test == FALSE)
		{
			project__errorHandler(PROJECT_ERROR_TEST);
		}

		pInst->test = FALSE;
	}

	return sys_msToTestTicks(5000);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_activate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		
*
*	\details
*
*	\note		This function is called each second by SUP.
*
*******************************************************************************/
PUBLIC void chalg_activate(
		void *					pInstance
) {
	Uint32					triggers;
	chalg__Inst *			pInst;

	pInst = chalgInst;

	pInst->test = TRUE; /* Reset internal test flag */

	atomic(
		triggers = pInst->triggers;
		pInst->triggers = 0;
	);
	
	/***************************************************************************
	 * Check if warm init should be performed.
	 */

	if (triggers & CHALG__TRG_INIT)
	{
		chalg_BattInfoType battInfo;
		Int16 BatteryTemp;
		Int16 BatteryTempC;
		Int16 BatteryTempF;
		Uint8 Status;
		Uint8 ChargingMode;
		Uint8 BatteryType;
		Uint8 language;
		Uint8 BitConfig;

		/*
		 * Read default battery info from flash register and update the RAM
		 * registers.
		 */

		reg_getLocal(&battInfo, reg_i_batteryInfo, pInst->instId);

		reg_putLocal(&battInfo.algNo,		reg_i_algNo_default,	pInst->instId);
		reg_putLocal(&battInfo.capacity,	reg_i_capacity_default, pInst->instId);
		reg_putLocal(&battInfo.cableRes,	reg_i_cableRes_default, pInst->instId);
		reg_putLocal(&battInfo.cell,		reg_i_cells_default,	pInst->instId);
		reg_putLocal(&battInfo.baseLoad,	reg_i_baseLoad_default, pInst->instId);

		reg_putLocal(&battInfo.algNo,		reg_i_currAlgNo,		pInst->instId);
		reg_putLocal(&battInfo.baseLoad,	reg_i_currBaseLoad, 	pInst->instId);
		reg_putLocal(&battInfo.cableRes,	reg_i_currCableRes, 	pInst->instId);
		reg_putLocal(&battInfo.capacity,	reg_i_currCapacity, 	pInst->instId);
		reg_putLocal(&battInfo.cell,		reg_i_currCells,		pInst->instId);

		/*
		 * Read ChargingMode from flash and update to RAM
		 */
		reg_getLocal(&ChargingMode, reg_i_ChargingMode, pInst->instId);
		reg_getLocal(&BatteryType, reg_i_BatteryType, pInst->instId);
		reg_getLocal(&BatteryTempC, reg_i_BatteryTempC, pInst->instId);
		reg_getLocal(&BatteryTempF, reg_i_BatteryTempF, pInst->instId);

		reg_putLocal(&ChargingMode,	reg_i_currChargingMode, pInst->instId);
		reg_putLocal(&ChargingMode,	reg_i_ChargingMode_default, pInst->instId);

		reg_putLocal(&BatteryType,	reg_i_currBatteryType, pInst->instId);
		reg_putLocal(&BatteryType,	reg_i_BatteryType_default, pInst->instId);

		reg_get(&language, chalg__LanguageP);

		if(language == gui_lang_english_us)
		{
			BatteryTemp = BatteryTempF;
		}
		else
		{
			BatteryTemp = BatteryTempC;
		}

		reg_putLocal(&BatteryTemp,	reg_i_BatteryTemp_default, pInst->instId); 				// Set value shown in display
		pInst->BatteryTemp = BatteryTempC;													// Set value used for compensation

		/*
		 * Get BitConfig parameter (CEC mode)
		 */
		reg_get(&BitConfig, chalg__BitConfig);

		/*
		 * Update AlgIdx
		 */
		Status = chalg_UpdateAlgIdx(battInfo.algNo);

		if(Status == TRUE)
		{
			Uint16 AlgVersion;
			Uint16 AlgIdx;
			Uint8 AlgName[CHALG_ALG_NAME_LEN_MAX + 1];

			reg_getLocal(&AlgVersion,	reg_i_version_default,	pInst->instId);
			reg_putLocal(&AlgVersion,	reg_i_currVersion,	pInst->instId);

			reg_getLocal(&AlgIdx,	reg_i_algIdx_default,	pInst->instId);
			reg_putLocal(&AlgIdx,	reg_i_currAlgIdx,	pInst->instId);

			reg_getLocal(&AlgName,		reg_i_algName_default,	pInst->instId);
			reg_putLocal(&AlgName,		reg_i_currAlgName,		pInst->instId);

			/* Get charging parameters */
			if(BitConfig & CHALG_BITCFG_CEC)
			{
				CaiInput.Algorithm.AlgDef_p = (AlgDef_ConstPar_Type *)ChargingAlgorithmsCEC[AlgIdx];
			}
			else
			{
				CaiInput.Algorithm.AlgDef_p = (AlgDef_ConstPar_Type *)ChargingAlgorithms[AlgIdx];
			}
			CaiInput.Algorithm.BaseLoad = (float)(battInfo.baseLoad)*(1/1000.0);
			CaiInput.Algorithm.CableRes = (float)(battInfo.cableRes)*(1/1000.0);
			CaiInput.Algorithm.Capacity = battInfo.capacity;
			CaiInput.Algorithm.Cells = battInfo.cell;

			/* Update user parameters */
			setUserParameters(&CaiInput.UserParam, pInst, ((Mandatory_ConstPar_Type* )CaiInput.Algorithm.AlgDef_p->Mandatory_ConstPar_Addr)->UserParam.Size);

			/* Update supported battery voltages and capacity */
			engineSupportedBatteryVoltage();
			if((BitConfig & CHALG_BITCFG_CEC) && (ChargingMode == CHALG_USER_DEF || ChargingMode == CHALG_DUAL))
			{
				if(engineValidateParamsCEC(battInfo) == BATTERY_NO_ERROR)
				{
					chargingParamOk = TRUE;
				}
				else
				{
					chargingParamOk = FALSE;
				}
			}
			else
			{
				chargingParamOk = TRUE;
			}
		}
		else
		{
			chargingParamOk = FALSE;
		}

		if(chargingParamOk == FALSE)
		{
			// Charging parameters not set. Do not run Chalg. Trigger alarm?
			Uint16 chalgError;

			chalgError = CHALG_ERR_INCORRECT_ALGORITHM;
			reg_putLocal(&chalgError, reg_i_ChalgError, pInst->instId);
		}

		/*
		 * Reset charging algorithm
		 */
		CaiInput.Control |= CAI_INPUT_CONTROL_RESET;	// 100923 jakob
		CaiOutput.ChalgToCc.Mode = ReguMode_Off;		// #JJ fix for battery connected when not supposed to
		CaiOutput.ChargingStage = 0;					// #JJ fix set charging phase to idle when reset
		/*
		 * Reset counters and timers
		 */
		{
			int i;
			for(i = 0; i < 10; i++){
				CaiOutput.ChargingValues[i].Index = 0;
				//start
				CaiOutput.ChargingValues[i].Start.Tick = 0;
				CaiOutput.ChargingValues[i].Start.Ubatt = 0;
				CaiOutput.ChargingValues[i].Start.Ibatt = 0;
				CaiOutput.ChargingValues[i].Start.Tbatt = 0;
				//stop
				CaiOutput.ChargingValues[i].Stop.Tick = 0;
				CaiOutput.ChargingValues[i].Stop.Ubatt = 0;
				CaiOutput.ChargingValues[i].Stop.Ibatt = 0;
				CaiOutput.ChargingValues[i].Stop.Tbatt = 0;

				CaiOutput.ChargingValues[i].Time = 0;
				CaiOutput.ChargingValues[i].AmpSecCnt = 0;
				CaiOutput.ChargingValues[i].WattSecCnt = 0;

			}
		}

		/*
		 * Reset other variables used by CHALG
		 */
		pInst->BBCStatusTime = 0;
		pInst->BBCStatus = 0;
		pInst->AcWs = 0;
		pInst->CableResBm = 0;
		pInst->CableResOld = 0.0;

		/* Cable resistance event */
		cableResCalcStart = 0;
		cableResCalcMax = 0;
		cableRes_tick = 0;
		/************************************/

		{
			Uint8 BBC;
			Uint32 AcWh;
			Uint16 chalgErrorSum;
			Uint16 chalgStatusSum;
			Uint8 funcOverride;
			Uint8 equalizeActive;
			Uint16 bmError;
			Uint16 bmStatus;
			Int16  bmTbatt;



			BBC = 0;
			reg_putLocal(&BBC, reg_i_BBC, pInst->instId);
			AcWh = 0;
			reg_putLocal(&AcWh, reg_i_AcWh, pInst->instId);
			pInst->ChargeTime = 0;
			reg_putLocal(&pInst->ChargeTime, reg_i_ChargeTime, pInst->instId);
			chalgErrorSum = 0;
			reg_putLocal(&chalgErrorSum, reg_i_ChalgErrorSum, pInst->instId);
			chalgStatusSum = 0;
			reg_putLocal(&chalgStatusSum, reg_i_ChalgStatusSum, pInst->instId);
			funcOverride = 0;
			reg_put(&funcOverride, chalg__Equalize_override);
			reg_get(&equalizeActive, chalg__Equalize_active);
			if(equalizeActive == CHALG_EQUALIZE_ENABLED)
			{
				equalizeActive = 0;
				reg_put(&equalizeActive, chalg__Equalize_active);
			}
			bmError = 0;
			reg_put(&bmError, chalg__BmError);
			bmStatus = 0;
			reg_put(&bmStatus, chalg__BmStatus);
			bmTbatt = Int16_MAX;
			reg_put(&bmTbatt, chalg__BmTbatt);
		}

		HighVoltageLimit = 0;
		gUserParamsEdited = FALSE;
		nfcUserParamsEdited = FALSE;

		/* Reset BID to zero, BM not connected */
		{
			Int32 bid = 0;
			reg_putLocal(&bid, reg_i_BID, pInst->instId);
		}

		// Reset timer used for Bmu Initialization
		//InitBmuTimerReset();

		/* Reset air pump failure flags */
		AirPumpFailed = FALSE;	// Air pump error flag to remain until battery disconnected
		AirPumpHPEflag  = FALSE;// Air pump high pressure error flag
		AirPumpLPEflag  = FALSE;// Air pump low pressure error flag

		ChalgAirPumpOn = FALSE;	// Air pump function from Chalg
		ChalgWaterPumpOn = FALSE;// Water function from Chalg
	}

	/***************************************************************************
	 * Check if mains state has changed.
	 */

	if (triggers & CHALG__TRG_MAINS)
	{
		Uint8 mainsIsConnected;

		reg_get(&mainsIsConnected, chalg__mains);

		if (mainsIsConnected)
		{
			atomic(
				pInst->chalgStatus |= CHALG_STAT_MAINS_CONN;
			);
		}
		else
		{
			atomic(
				pInst->chalgStatus &= ~CHALG_STAT_MAINS_CONN;
			);
		}
	}

	if (triggers & CHALG__TRG_FORCE)
	{
		Uint8 ForceStart;	// off

		reg_getLocal(&ForceStart, reg_i_ForceStart, pInst->instId);

		if(ForceStart){
			atomic(
				pInst->chalgStatus |= CHALG_STAT_FORCE;
			);
			pInst->ForceStartTimer = 30;
			reg_putLocal(&ForceStart, reg_i_ForceStartView, pInst->instId);
			chargerForceStart = 2.0f*CaiInput.Algorithm.Cells;
		}
		else{
			atomic(
				pInst->chalgStatus &= ~CHALG_STAT_FORCE;
			);
			pInst->ForceStartTimer = 0;
			chargerForceStart = NAN;
		}
	}

	if(triggers & CHALG__TRG_NEWCHARGE)
	{
		// reset UVc array used for endVoltage
		pInst->aUVc[0] = 0;
		pInst->aUVc[1] = 0;
		pInst->aUVc[2] = 0;
		pInst->aUVc[3] = 0;
		pInst->aUVc[4] = 0;
		pInst->aUVc[5] = 0;
	}

	if (triggers & CHALG__TRG_EQUALIZE)
	{

		Uint8 Equalize;

		//reg_get(&Equalize, chalg__Equalize_func);
		reg_get(&Equalize, chalg__Equalize_active);

		if (Equalize)
		{
			atomic(
				CaiInput.Input.Detail.ManEquOn = TRUE;
				CaiInput.Input.Detail.ManEquOff = FALSE;
			);
		}
		else
		{
			atomic(
				CaiInput.Input.Detail.ManEquOff = TRUE;
				CaiInput.Input.Detail.ManEquOn = FALSE;
			);
		}

	}

	if(triggers & CHALG__TRG_BATTERYTEMP)
	{
		/*
		 * Handle battery temp storage
		 */
		int UpdateTemp = FALSE;
		Int16 BatteryTemp;
		Int16 BatteryTempC;
		Int16 BatteryTempF;
		Uint8 language;

		reg_getLocal(&BatteryTemp, reg_i_BatteryTemp_default, pInst->instId);
		reg_get(&language, chalg__LanguageP);

		if(language == gui_lang_english_us)
		{
			reg_getLocal(&BatteryTempF, reg_i_BatteryTempF, pInst->instId);

			if(BatteryTemp != BatteryTempF)
			{
				BatteryTempC = ((BatteryTemp - 320)*5)/9;							// Convert to degrees Celcius
				BatteryTempF = BatteryTemp;
				UpdateTemp = TRUE;
			}
		}
		else
		{
			reg_getLocal(&BatteryTempC, reg_i_BatteryTempC, pInst->instId);

			if(BatteryTemp != BatteryTempC)
			{
				BatteryTempC = BatteryTemp;
				BatteryTempF = (BatteryTemp*9)/5 +320;								// Convert to degrees Fahrenheit
				UpdateTemp = TRUE;
			}
		}

		if(UpdateTemp)
		{
			pInst->BatteryTemp = BatteryTempC;
			reg_putLocal(&BatteryTempC, reg_i_BatteryTempC, pInst->instId);
			reg_putLocal(&BatteryTempF, reg_i_BatteryTempF, pInst->instId);
		}

	}

	if(triggers & CHALG__TRG_LANGUAGE)
	{
		/*
		 * Set battery temp shown in display depending on language setting
		 */
		Int16 BatteryTemp;
		Uint8 language;

		reg_get(&language, chalg__Language);

		if(language == gui_lang_english_us)
		{
			reg_getLocal(&BatteryTemp, reg_i_BatteryTempF, pInst->instId);
		}
		else
		{
			reg_getLocal(&BatteryTemp, reg_i_BatteryTempC, pInst->instId);
		}

		reg_putLocal(&BatteryTemp, reg_i_BatteryTemp_default, pInst->instId);


	}

	if(triggers & CHALG__TRG_BMUBATT)
	{
		/*
		 * If cable resistance from BMU is 0:
		 * Calculate new cable resistance from output current and battery voltage measured from BMU
		 * Cable resistance (in Ohm) = (Ucharger-Ubatt)/Icharger
		 * Two following calculations must not diff to much to avoid using a calculated value measured
		 * when current is rising and voltage may differ
		 */

		if(pInst->CableResBm == 0){																								// Cable resistance set in BMU must be 0
			Uint32 bmUbatt;
			Int32 idcLimit;
			float minIact;																										// Minimum current limit
			float maxCableRes = 0.015;																							// Maximum cable resistance
			float minCableRes = 0.0;																							// Minimum cable resistance
			float cableResCalc = 0;
			float cableResNew = 0;

			reg_get(&idcLimit, chalg__IdcLimit);																				// Get IdcLimit for comparison
			minIact = (float)(idcLimit)/2;

			if(CaiInput.Measure.Iact > minIact){																				// Current must be above minimum limit
				reg_get(&bmUbatt, chalg__BmUbatt);
				cableResCalc = (CaiInput.Measure.Uact - (float)(bmUbatt)/1000)/CaiInput.Measure.Iact;							// Calculate current cable resistance

				if((cableResCalc > (pInst->CableResOld - maxCableRes)) && (cableResCalc < (pInst->CableResOld + maxCableRes))){	// new calculated value must not diff to much from previous

					if(cableResCalc > maxCableRes){																				// Limit cable resistance
						cableResNew = maxCableRes;
					}
					else if(cableResCalc < minCableRes){																		// Limit cable resistance
						cableResNew = minCableRes;
					}
					else{
						cableResNew = cableResCalc;																				// No need to limit
					}

					CaiInput.Algorithm.CableRes = cableResNew;																	// Update currently used cable resistance

					{
						/* Handle update of displayed cable resistance value */
						Uint16 currCableRes;
						Uint16 newCableRes;

						reg_getLocal(&currCableRes,	reg_i_currCableRes, pInst->instId);											// Get currently highest value displayed
						newCableRes = (Uint16)(cableResNew*1000);																// Convert from Ohm to mOhm

						if(newCableRes > currCableRes){																			// Update displayed cable resistance
							reg_putLocal(&newCableRes,	reg_i_currCableRes, pInst->instId);
						}
					}
				}

				/* Cable resistance event */
				if ((!cableResCalcStart) && (cableResCalc > minCableRes)){
					cableResCalcStart = (Uint16)(cableResCalc * 1000);															// Convert from Ohm to mOhm
				}
				if ((!cableResCalcMax) && (cableResCalcStart))
				{
					cableRes_tick ++;			// BM_Ubatt is triggered every minute

					float minRaise = 0.0005; 	// 0,5 mOhm
					float cableResCalcDiff = cableResCalc + minRaise;

					if ((pInst->CableResOld >= cableResCalcDiff) && (cableResCalc > minCableRes)) {
						cableResCalcMax = (Uint16)(pInst->CableResOld * 1000);													// Convert from Ohm to mOhm

						Uint32 RiSlope = 0;
						Uint32 oldRiSlope = 0;
						Uint8 minTime = 2; 			// [minute]
						Uint8 maxRiSlope = 5;		// [mOhm / h]

						reg_getLocal(&oldRiSlope, reg_i_CurrRiSlope, pInst->instId);
						reg_putLocal(&oldRiSlope, reg_i_RiSlopeOld, pInst->instId);

						if (cableRes_tick >= minTime){	// After at least "minTime" minutes
							RiSlope = (Uint32)((((cableResCalcMax - cableResCalcStart) * 100) / (cableRes_tick * 100 / 60)) + 0.5);	// [mOhm / h]
							reg_putLocal(&RiSlope, reg_i_CurrRiSlope, pInst->instId);
						}
						if ((RiSlope > maxRiSlope) && oldRiSlope){
							reg_putLocal(&RiSlope, reg_i_RiSlope, pInst->instId);
						}
					}
				}
				/*********************************/
				pInst->CableResOld = cableResCalc;																				// Save calculated value for comparison next time
			}
		}
	}

	if(triggers & CHALG__TRG_BMCABLERI)
	{
		/*
		 * Cable resistance value received from BMU (sent at init of BMU in BmBatteryData)
		 * Update currCableRes and set parameter for automatic cable resistance calculation
		 */
		Uint16 cableResBm;

		reg_getLocal(&cableResBm,	reg_i_currCableResBm, pInst->instId);		// Get Cable res parameter from BM
		reg_putLocal(&cableResBm,	reg_i_currCableRes, pInst->instId);			// Store in charger (displayed in charging->Charging params)
		pInst->CableResBm = cableResBm;											// Init process parameter
		pInst->CableResOld = 0.0;												// Init process parameter
	}

	if(triggers & CHALG__TRG_CECMODE)
	{
		/*
		 * CEC mode flash parameter has changed.
		 * Re initialize parameters and process variable directly if battery not connected
		 */
		if(!(pInst->chalgStatus & CHALG_STAT_BATTRY_CONN))
		{
			pInst->triggers |= CHALG__TRG_INIT;
		}
	}

	if(triggers & CHALG__TRG_AIRPUMP_VAR)
	{
		/*
		 * AirPump_var (PumpOn) flash parameter has changed.
		 * Update process variable and convert to seconds resolution
		 */
		Uint8 AirPumpPumpOn;

		reg_get(&AirPumpPumpOn,	chalg__AirPump_var);
		pInst->AirPumpPumpOn = (Uint16)(60 * AirPumpPumpOn);

	}

	if(triggers & CHALG__TRG_AIRPUMP_VAR2)
	{
		/*
		 * AirPump_var2 (PumpCycle) flash parameter has changed.
		 * Update process variable and convert to seconds resolution
		 */
		Uint8 AirPumpPumpCycle;

		reg_get(&AirPumpPumpCycle,	chalg__AirPump_var2);
		pInst->AirPumpPumpCycle = (Uint16)(60 * AirPumpPumpCycle);
	}

	if(triggers & CHALG__TRG_AIRPUMP_LOW)
	{
		/*
		 * AirPumpAlarmLow flash parameter has changed.
		 * Update process variable
		 */
		reg_get(&pInst->AirPumpAlarmLow,	chalg__AirPumpAlarmLow);
	}

	if(triggers & CHALG__TRG_AIRPUMP_HIGH)
	{
		/*
		 * AirPumpAlarmHigh flash parameter has changed.
		 * Update process variable
		 */
		reg_get(&pInst->AirPumpAlarmHigh,	chalg__AirPumpAlarmHigh);
	}

	if(triggers & CHALG__TRG_WATER_VAR)
	{
		/*
		 * Water_var (PumpOn) flash parameter has changed.
		 * Update process variable and convert to seconds resolution
		 */
		Uint8 WaterVar;

		reg_get(&WaterVar,	chalg__Water_var);
		pInst->WaterDuration = (Uint16)(60 * WaterVar);
	}

	if(triggers & CHALG__TRG_BITCONFIG)
	{
		/*
		 * BitConfig register has changed.
		 */
		Uint8 bitConfig;

		reg_get(&bitConfig,	chalg__BitConfig);

		if((bitConfig & CHALG_BITCFG_DPL) == FALSE){
			// set DPL function to disabled if not already..
			Uint8 powerGroup_func;

			reg_get(&powerGroup_func,	chalg__PowerGroup_func);

			if(powerGroup_func != 0){
				powerGroup_func = 0;
				reg_put(&powerGroup_func,	chalg__PowerGroup_func);
			}
		}
	}
}

ChargerStatusFields_type StatusSum;
const AlgDef_ConstPar_Type* volatile algorithm = 0;
void chalg_Chalg(ChargerMeas_type Measurements, ChargerSet_type* ChalgOutput, Interval_type* UactOffInterval, Interval_type* UactOnInterval)
{
#if 1 || TARGET_SELECTED & TARGET_CM3
	chalg__Inst *			pInst;

	pInst = chalgInst;
	Uint16 supStatus;
	Uint8 supState;
	Uint8 BBC;
	Uint8 BitConfig;

	if(chargingParamOk)
	{
		/***************************************************************************
		 * CHALG code.
		 */

		/* Set chalg status bit BM connected or not */
		{
			Int32 bid;
			reg_getLocal(&bid, reg_i_BID, pInst->instId);
			if(bid){
				pInst->chalgStatus |= CHALG_STAT_BM_CONNTECTED;

			}
			else{
				pInst->chalgStatus &=~ CHALG_STAT_BM_CONNTECTED;
			}

		}

		{
			Int16 T;
			//reg_get(&T, chalg__BmTbatt);	// BmTbatt in Degrees Celcius / 10
			reg_get(&T, chalg__batteryTemp);	// BmTbatt or TbattExt in Degrees Celcius / 10

			if((T == Int16_MAX) || (T == Int16_MIN)) // not available
				CaiInput.Measure.Tbatt = (1.0/10.0)*(float)pInst->BatteryTemp;
			else
				CaiInput.Measure.Tbatt = (1.0/10.0)*(float)T; // observe multiplication with constant instead of division
		}

		/* Handle BitConfig and CECMode */
		{
			static int chalgInit = 1;

			reg_get(&BitConfig, chalg__BitConfig);

			// Initialize old value if startup from reset
			if(chalgInit){
				pInst->CECModeOld = BitConfig & CHALG_BITCFG_CEC;
				chalgInit = 0;
			}

			// Check if CECMode has changed
			if((BitConfig & CHALG_BITCFG_CEC) != pInst->CECModeOld){
				pInst->triggers |= CHALG__TRG_CECMODE;
			}

			pInst->CECModeOld = BitConfig & CHALG_BITCFG_CEC;
		}

		CaiOutput.Status = 0;
		CaiInput.Measure.Uact = Measurements.Meas.U;
		CaiInput.Measure.Iact = Measurements.Meas.I;
		CaiInput.Measure.Pact = Measurements.Meas.P;
		CaiInput.CcStatus = Measurements.Status;
		CaiInput.Control &= ~CAI_INPUT_CONTROL_CHARG;

		/* Get remoteIn */ // sorab - kalmar / exide
		{
			uint8 remoteIn;
			reg_get(&remoteIn, chalg__remoteIn);
			if (remoteIn) // input high
			{
				CaiInput.Input.Detail.RemoteIn = TRUE;
			}
			else // input low - grounded
			{
				CaiInput.Input.Detail.RemoteIn = FALSE;
			}
		}

		/* Get supervision state from SUP */
		reg_get(&supState, chalg__supState);
		/* Get supervision status from SUP */
		reg_get(&supStatus, chalg__statusFlags);
		/* Get BBC state for later use*/
		reg_getLocal(&BBC, reg_i_BBC, pInst->instId);

		switch(supState)
		{
		case SUP_STATE_PAUSE :
		case SUP_STATE_REMOTERESTRICTION:
			break;
		case SUP_STATE_CHARGING :
			{
				int turnOffOutput = FALSE;

				if((Measurements.Status.Detail.Error.Sum & CHALG_REGU_ERR_MASK) ||
				   (supStatus & SUP_STATUS_ADDR_CONFLICT) ||
				   (supStatus & SUP_STATUS_BM_ADDR_CONFLICT))
				{
					turnOffOutput = TRUE;
				}

				if(turnOffOutput)
				{
					// Turn off output if error
					CaiOutput.ChalgToCc.Mode = ReguMode_Off;
					CaiOutput.ChalgToCc.Set.U = 0.0;
					CaiOutput.ChalgToCc.Set.I = 0.0;
					CaiOutput.ChalgToCc.Set.P = 0.0;
				}
				else
				{
					int enginesOn;
					/*
					 * Set condition for CAI, to run timers and calculate set values
					 */
					CaiInput.Control |= CAI_INPUT_CONTROL_CHARG;

					/*
					 * Update charge time if engine switch is on (on any charger if cluster control)
					 */
					enginesOn = CcGetEnginesOn();
					if(CHALG_ENGINE_ON || enginesOn)
					{
						pInst->ChargeTime++;
						reg_putLocal(&pInst->ChargeTime, reg_i_ChargeTime, pInst->instId);
					}

					/*
					 * Update BBCStatusTime
					 */
					pInst->BBCStatusTime++;
				}
			}
			break;
		default :
			/*
			 * When not in state charging CAI only compensates input values
			 * e.g. for display
			 */
			CaiOutput.ChalgToCc.Mode = ReguMode_Off;
			CaiOutput.ChalgToCc.Set.U = 0.0;
			CaiOutput.ChalgToCc.Set.I = 0.0;
			CaiOutput.ChalgToCc.Set.P = 0.0;
			break;
		}

		if(pInst->chalgStatus & CHALG_STAT_FORCE)
		{
			if(pInst->ForceStartTimer && CaiInput.Measure.Uact <= chargerForceStart){
				pInst->ForceStartTimer--;
			}
			else{
				chargerForceStart = NAN;
				pInst->ForceStartTimer = 0;

				Uint8 ForceStartView = FALSE;
				reg_putLocal(&ForceStartView, reg_i_ForceStartView, pInst->instId);

				atomic(
						pInst->chalgStatus &= ~CHALG_STAT_FORCE;
					);
			}
		}

		/*
		 * Call CAI to calculate set values
		 */
		StatusSum.Sum = CaiInput.CcStatus.Sum;
		CaiMain(&CaiInput, &CaiOutput);

		/*
		 * clear reset flag in CaiInput control register
		 */
		CaiInput.Control &=~ CAI_INPUT_CONTROL_RESET;

		/*
		 * Get LED indication from Chalg
		 */
		Uint8 LedRed;
		Uint8 LedYellow;
		Uint8 LedGreen;

		{
			/* Normal LED indication from algorithm */
			/* Red */
			LedRed = CaiOutput.Output.Detail.LedRed;
			/* Yellow */
			LedYellow = CaiOutput.Output.Detail.LedYellow;
			/* Green */
			LedGreen = CaiOutput.Output.Detail.LedGreen;
		}

		/* IO module spare 1 */
		{
			static unsigned int timer = 0;

			ChalgCalculateOutput(CaiOutput.Output.Detail.IoSpare1, &timer, chalg__IoModuleSpare1, chalg__u8DummyReg);
		}

		/* IO module spare 2 */
		{
			static unsigned int timer = 0;

			ChalgCalculateOutput(CaiOutput.Output.Detail.IoSpare2, &timer, chalg__IoModuleSpare2, chalg__u8DummyReg);
		}

		/* IO module air */
		{
			static unsigned int timer = 0;
			Uint8 airPumpOn = FALSE;
			Uint8 airPumpActive;
			Uint8 remoteOutFunc;
			Uint8 extraOutFunc;

			reg_get(&airPumpActive, chalg__AirPump_active);
			reg_get(&remoteOutFunc, chalg__RemoteOut_func);
			reg_get(&extraOutFunc, chalg__ExtraOut_func);

			/* Air pump must be selected as an output function */
			if((extraOutFunc & IO_OUTFUNC_AIRPUMP) || (remoteOutFunc == SUP_ROFUNC_AIR))
			{
				/* Air pump is enabled during charging if no regulator error and current is above 1% of capacity */
				if(supState == SUP_STATE_CHARGING)
				{
					float Imeas = CaiInput.Measure.Iact;
					float ImeasLimit = CaiInput.Algorithm.Capacity * 0.01f;

					if(!(Measurements.Status.Detail.Error.Sum || (Imeas < ImeasLimit)))
					{
						airPumpOn = TRUE;
					}
				}
			}
			/* If air pump is enabled output is set according to charging algorithm */
			if(airPumpOn && (CaiOutput.Output.Detail.Airpump == Output_On))
			{
				ChalgCalculateOutput(Output_AirPump, &timer, chalg__IoModuleAir, chalg__AirPump_on);
				ChalgAirPumpOn = TRUE; // to be used in Extra outputs
			}
			else
			{
				ChalgCalculateOutput(Output_Off, &timer, chalg__IoModuleAir, chalg__AirPump_on);
				ChalgAirPumpOn = FALSE; // to be used in Extra outputs
			}
			/* Display indication is changed if air pump is changed from disabled to enabled or vice versa*/
			if(airPumpOn != airPumpActive)
			{
				reg_put(&airPumpOn, chalg__AirPump_active);
			}
		}

		/* IO module water */
		{
			static unsigned int timer = 0;
			Uint8 waterOn = FALSE;
			Uint8 waterActive;
			Uint8 remoteOutFunc;
			Uint8 extraOutFunc;
			Uint16 bmStatus;

			reg_get(&waterActive, chalg__WaterPump_active);
			reg_get(&remoteOutFunc, chalg__RemoteOut_func);
			reg_get(&extraOutFunc, chalg__ExtraOut_func);
			reg_get(&bmStatus, chalg__BmStatus);

			if((extraOutFunc & IO_OUTFUNC_WATER) || (remoteOutFunc == SUP_ROFUNC_WATER))
			{
				if(supState == SUP_STATE_CHARGING)
				{
					Uint8 chargingMode;
					reg_getLocal(&chargingMode, reg_i_ChargingMode, pInst->instId);

					if(chargingMode == CHALG_BM || chargingMode == CHALG_DUAL || chargingMode == CHALG_MULTI)
					{
						if(pInst->chalgStatus & CHALG_STAT_BM_CONNTECTED)
						{
							if(bmStatus != 0xFFFF)
							{
								if(bmStatus & RADIO_BMSTATUS_FUNC_WATER)
								{
									if(bmStatus & RADIO_BMSTATUS_NEED_WATER)
										waterOn = TRUE;
								}
								else
								{
									waterOn = TRUE;
								}
							}
						}
						else
						{
							if(chargingMode == CHALG_DUAL || chargingMode == CHALG_MULTI)
								waterOn = TRUE;		// No function for handling water. Always true if charging without BMU
						}
					}
					else
					{
						waterOn = TRUE;			// No function for handling water. Always true if charging without BMU
					}
				}
			}
			else{
				/*
				 * Trigger watering indication if BmStatus NeedWatering and chalgStatus is ready
				 */
				static int manualWaterActivated = FALSE;
				Uint8 manualWater;
				Uint8 backlight;
				reg_get(&manualWater, chalg__Water_man);

				if(	(bmStatus & RADIO_BMSTATUS_NEED_WATER) &&
					(pInst->chalgStatus & CHALG_STAT_CHARG_COMPLETED))
				{
					if(!manualWater && !manualWaterActivated){
						manualWaterActivated = TRUE;
						manualWater = TRUE;
						backlight = TRUE;
						reg_put(&manualWater, chalg__Water_man);													// Display indication
						reg_put(&backlight, chalg__backlight_time);													// Display should be lit up (as long as display indication is active)
					}
				}
				else{
					manualWaterActivated = FALSE;

					if(manualWater){
						manualWater = FALSE;
						reg_put(&manualWater, chalg__Water_man);													// Display indication
					}
				}
			}

			/* If Water enabled output is set according to charging algorithm */
			if(waterOn && (CaiOutput.Output.Detail.WaterFill == Output_On))
			{
				ChalgCalculateOutput(Output_Water, &timer, chalg__IoModuleWater, chalg__WaterPump_on);
				ChalgWaterPumpOn = TRUE; // to be used in Extra outputs
			}
			else
			{
				ChalgCalculateOutput(Output_Off, &timer, chalg__IoModuleWater, chalg__WaterPump_on);
				ChalgWaterPumpOn = FALSE; // to be used in Extra outputs
			}

			if(waterOn != waterActive)
			{
				reg_put(&waterOn, chalg__WaterPump_active);													// Display indication
			}
		}

		/* Status */
		Uint16 oldChalgStatus = 0;
		oldChalgStatus = pInst->chalgStatus;

		pInst->chalgStatus &= ~(CHALG_STAT_BATTRY_CONN | CHALG_STAT_PRE_CHARG | CHALG_STAT_ADDITIONAL_CHARG | CHALG_STAT_EQUALIZE_CHARG | CHALG_STAT_MAIN_CHARG |
								CHALG_STAT_CHARG_COMPLETED | CHALG_STAT_MAINTENANCE_CHARG | CHALG_STAT_PAUSE | CHALG_STAT_BBC | CHALG_STAT_TBATTLOW | CHALG_STAT_TBATTHIGH |
								CHALG_STAT_RESTRICTED);

		if(!Measurements.Status.Detail.Derate.Detail.NoLoad)
			pInst->chalgStatus |= CHALG_STAT_BATTRY_CONN;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.PreCharging)
			pInst->chalgStatus |= CHALG_STAT_PRE_CHARG;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.MainCharging)
			pInst->chalgStatus |= CHALG_STAT_MAIN_CHARG;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.Completion)
			pInst->chalgStatus |= CHALG_STAT_ADDITIONAL_CHARG;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.Equaliztion)
			pInst->chalgStatus |= CHALG_STAT_EQUALIZE_CHARG;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.Ready)
			pInst->chalgStatus |= CHALG_STAT_CHARG_COMPLETED;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.Trickle)
			pInst->chalgStatus |= CHALG_STAT_MAINTENANCE_CHARG;
		if(supState == SUP_STATE_PAUSE)
			pInst->chalgStatus |= CHALG_STAT_PAUSE;
		if(BBC == TRUE)
			pInst->chalgStatus |= CHALG_STAT_BBC;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.TbattLow)
			pInst->chalgStatus |= CHALG_STAT_TBATTLOW;
		if(CaiOutput.ChargingStatus.Sticky.Info.Detail.TbattHigh)
			pInst->chalgStatus |= CHALG_STAT_TBATTHIGH;
		if(supState == SUP_STATE_REMOTERESTRICTION || supState == SUP_STATE_TIMERESTRICTION)
			pInst->chalgStatus |= CHALG_STAT_RESTRICTED;



		/*
		 * Write to REG ChalgStatusSum if new status
		 */
		if (oldChalgStatus != pInst->chalgStatus)
		{
			/* Sum all status bits during this charge */
			Uint16	chalgStatusSum;
			reg_getLocal(&chalgStatusSum, reg_i_ChalgStatusSum, pInst->instId);
			chalgStatusSum |= pInst->chalgStatus;
			reg_putLocal(&chalgStatusSum, reg_i_ChalgStatusSum, pInst->instId);
		}

		/*
		 * If CEC mode block all charger output (maintenance charging) when READY
		 */
		if((BitConfig & CHALG_BITCFG_CEC) && (pInst->chalgStatus & CHALG_STAT_CHARG_COMPLETED))
		{
			CaiOutput.ChalgToCc.Mode = ReguMode_Off;
			CaiOutput.ChalgToCc.Set.U = 0.0;
			CaiOutput.ChalgToCc.Set.I = 0.0;
			CaiOutput.ChalgToCc.Set.P = 0.0;
		}

		/*
		 * If FW_MODE = USA. Update display backlight if charging completed.
		 */
		Uint8 language;
		reg_get(&language, chalg__LanguageP);
		if(language == gui_lang_english_us)
		{
			if ((pInst->chalgStatus & CHALG_STAT_CHARG_COMPLETED) && !(oldChalgStatus & CHALG_STAT_CHARG_COMPLETED))
			{
				//Display should be lit up for 1 min
				Uint8 backlight_time;
				backlight_time = 1;
				reg_put(&backlight_time, chalg__backlight_time);
			}
		}

		/*
		 * Reset equalize trigger condition when when equalize started.
		 */
		if ((pInst->chalgStatus & CHALG_STAT_EQUALIZE_CHARG) && !(oldChalgStatus & CHALG_STAT_EQUALIZE_CHARG))
		{
			// reset trigger for manual equalize
			CaiInput.Input.Detail.ManEquOn = FALSE;
		}

		/*
		 * Reset equalize flag when equalize complete.
		 */
		if ((oldChalgStatus & CHALG_STAT_EQUALIZE_CHARG) && !(pInst->chalgStatus & CHALG_STAT_EQUALIZE_CHARG))
		{
			Uint8 equalizeActive;
			equalizeActive = 0;
			reg_put(&equalizeActive, chalg__Equalize_active);
		}

		/* Coulomb counter */
		{
			Uint16 Ah = 0;
			for(int i = 0; i < 10; i++){
				Ah += (Uint16)(CaiOutput.ChargingValues[i].AmpSecCnt/3600.0 + 0.5);
			}
			reg_putLocal(&Ah, reg_i_chargedAh, pInst->instId);
		}

		/* percent charged of battery capacity */
		/* In stage main (I1, U1) */
		{
			Uint16 AhP = 0;
			AhP = (Uint16)((100/3600.0)*CaiOutput.ChargingValues[STAGE_I1].AmpSecCnt/CaiInput.Algorithm.Capacity + 0.5);
			AhP += (Uint16)((100/3600.0)*CaiOutput.ChargingValues[STAGE_U1].AmpSecCnt/CaiInput.Algorithm.Capacity + 0.5);
			reg_putLocal(&AhP, reg_i_chargedAhPercent, pInst->instId);
		}

		/* Charged Ah in Equalize stage (normal + manual)*/
		/* Charged Wh in Equalize stage (normal + manual)*/
		/* Charged Time in Equalize stage (normal + manual)*/
		{
			Uint32 EquAh = 0;
			Uint32 EquWh = 0;
			Uint32 EquTime = 0;

			EquAh = (Uint32)(CaiOutput.ChargingValues[STAGE_EQU].AmpSecCnt/3600.0 + 0.5);
			EquAh += (Uint32)(CaiOutput.ChargingValues[STAGE_EQUMAN].AmpSecCnt/3600.0 + 0.5);
			reg_putLocal(&EquAh, reg_i_equAh, pInst->instId);

			EquWh = (Uint32)(CaiOutput.ChargingValues[STAGE_EQU].WattSecCnt/3600.0 + 0.5);
			EquWh += (Uint32)(CaiOutput.ChargingValues[STAGE_EQUMAN].WattSecCnt/3600.0 + 0.5);
			reg_putLocal(&EquWh, reg_i_equWh, pInst->instId);

			EquTime = (Uint32)(CaiOutput.ChargingValues[STAGE_EQU].Time);
			EquTime += (Uint32)(CaiOutput.ChargingValues[STAGE_EQUMAN].Time);
			reg_putLocal(&EquTime, reg_i_equTime, pInst->instId);

		}

		/* Energy counter */
		{
			// charged Wh (to battery)
			Uint32 Wh = 0;
			for(int i = 0; i < 10; i++){
				Wh += (Uint32)(CaiOutput.ChargingValues[i].WattSecCnt/3600.0 + 0.5);
			}
			reg_putLocal(&Wh, reg_i_chargedWh, pInst->instId);

			// consumed power and Wh (from Ac power net)
			const Engine_type *engineParameters = engineGetParameters();
			float EfficiencyFactor = engineParameters->ConversionfactorPowerInToDc;
			float AcW;
			Uint32 AcP;
			Uint32 AcWh;

			AcW = (Measurements.Meas.P / EfficiencyFactor + 0.5);
			AcP = (Uint32)AcW;

			pInst->AcWs += AcW;

			AcWh = (Uint32)(pInst->AcWs/3600.0 + 0.5);

			reg_putLocal(&AcWh, reg_i_AcWh, pInst->instId);
			reg_putLocal(&AcP, reg_i_AcP, pInst->instId);


		}

		/* End Vpc counter*/
		{
			Uint32 UVc = 0;
			reg_getLocal(&UVc, reg_i_UVc, pInst->instId);

			//Update array
			pInst->aUVc[5] = pInst->aUVc[4];
			pInst->aUVc[4] = pInst->aUVc[3];
			pInst->aUVc[3] = pInst->aUVc[2];
			pInst->aUVc[2] = pInst->aUVc[1];
			pInst->aUVc[1] = pInst->aUVc[0];
			pInst->aUVc[0] = UVc;

			// get 5s old UVc
			Uint32 UVc5s = 0;
			UVc5s = pInst->aUVc[5];
			reg_putLocal(&UVc5s, reg_i_UVc5s, pInst->instId);
		}

	//	*ChalgToCc = CaiOutput.ChalgToCc;

		/* Write to error register */
		{
			Uint16 					bmError;
			Uint16 					chalgError;
			Uint16					oldChalgError;
			Uint16					airPumpPressure;
			Uint8					airPumpOn;
			Uint8 					airPumpActive;
			ChargerStatusFields_type regu_status;

			reg_get(&bmError, chalg__BmError);
			reg_getLocal(&chalgError, reg_i_ChalgError, pInst->instId);
			reg_get(&airPumpPressure, chalg__AirPumpPressure);
			reg_get(&airPumpOn, chalg__AirPump_on);

			reg_get(&airPumpActive, chalg__AirPump_active);
			AirPumpOk = TRUE;	// Reset air pump OK flag

			regu_status.Sum = 0;
			oldChalgError = chalgError;

			chalgError &= ~(CHALG_ERR_INCORRECT_MOUNT | CHALG_ERR_LOW_BATTERY_VOL | CHALG_ERR_HIGH_BATTERY_VOL | CHALG_ERR_CHARGE_TIME_LIMIT | CHALG_ERR_CHARGE_AH_LIMIT |
							CHALG_ERR_INCORRECT_ALGORITHM | CHALG_ERR_HIGH_BATTERY_LIMIT | CHALG_ERR_BATTERY_ERROR | CHALG_ERR_BM_TEMP | CHALG_ERR_BM_ACID | CHALG_ERR_BM_BALANCE |
							CHALG_ERR_LOW_AIRPUMP_PRESSURE | CHALG_ERR_HIGH_AIRPUMP_PRESSURE | CHALG_ERR_BM_LOW_SOC);

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.VoltageLow || (supStatus & SUP_STATUS_VSENSE_LOW))
			{
				chalgError |= CHALG_ERR_LOW_BATTERY_VOL;
			}
			else
			{
				if(supState == SUP_STATE_CHARGING)
				{
					reg_get(&regu_status, chalg__reguStatusP);

					if(regu_status.Detail.Derate.Detail.LowVoltage){
						chalgError |= CHALG_ERR_LOW_BATTERY_VOL;
					}
				}
			}

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.VoltageHigh || (supStatus & SUP_STATUS_VSENSE_HIGH))
			{
				chalgError |= CHALG_ERR_HIGH_BATTERY_VOL;
			}
			else
			{
				if(supState == SUP_STATE_CHARGING)
				{
					reg_get(&regu_status, chalg__reguStatusP);

					if(regu_status.Detail.Derate.Detail.HighVoltage){
						chalgError |= CHALG_ERR_HIGH_BATTERY_VOL;
					}
				}
			}

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.TimeMax)
			{
				chalgError |= CHALG_ERR_CHARGE_TIME_LIMIT;
			}

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.AhMax)
			{
				chalgError |= CHALG_ERR_CHARGE_AH_LIMIT;
			}

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.DeltaU)
			{
				chalgError |= CHALG_ERR_BATTERY_ERROR;
			}

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.DeltaI)
			{
				chalgError |= CHALG_ERR_BATTERY_ERROR;
			}

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.DeltaP)
			{
				chalgError |= CHALG_ERR_BATTERY_ERROR;
			}

			if(CaiOutput.ChargingStatus.Instant.Error.Detail.AirPumpFailure)	// Air pump failure
			{
				AirPumpFailed = TRUE;
			}

			if(supStatus & SUP_STATUS_VSENSE_ERR)
			{
				chalgError |= CHALG_ERR_BATTERY_ERROR;
			}

			if(supState == SUP_STATE_CHARGING)
			{
				static Uint32 sumLow = 0;
				static Uint32 sumHigh = 0;
				sumLow <<= 1;
				sumHigh <<= 1;

				if(airPumpOn)
				{
					if(airPumpPressure < pInst->AirPumpAlarmLow)
					{
						sumLow |= 1;
						/* 30 seconds delay of alarm for low air pump pressure */
						if((sumLow & 0x3FFFFFFF) == 0x3FFFFFFF)
						{
							chalgError |= CHALG_ERR_LOW_AIRPUMP_PRESSURE;
							/* Air pump failure */
							AirPumpOk = FALSE;		// Air pump not OK
							AirPumpHPEflag = FALSE;	// High pressure error flag low
							AirPumpLPEflag = TRUE; 	// Low pressure error flag high
						}
					}
					else if(airPumpPressure > pInst->AirPumpAlarmHigh)
					{
						sumHigh |= 1;
						/* 10 seconds delay of alarm for high air pump pressure */
						if((sumHigh & 0x000003FF) == 0x000003FF)
						{
							chalgError |= CHALG_ERR_HIGH_AIRPUMP_PRESSURE;
							/* Air pump failure */
							AirPumpOk = FALSE;		// Air pump not OK
							AirPumpHPEflag = TRUE;	// High pressure error flag high
							AirPumpLPEflag = FALSE; // Low pressure error flag low
						}
					}
				}
				if (AirPumpOk)
				{	// sorab - input to the charging curve
					CaiInput.Input.Detail.IoSpare3 = FALSE; // error flag low
					if (!airPumpActive)
					{
						CaiInput.Input.Detail.IoSpare3 = TRUE; // error flag high
					}
				}
				else
				{	// sorab - error flag high - input to the charging curve
					CaiInput.Input.Detail.IoSpare3 = TRUE;
				}
				if (AirPumpFailed)
				{	// Show the last air pump error on display until the battery disconnected
					if (AirPumpHPEflag)
					{
						chalgError |= CHALG_ERR_HIGH_AIRPUMP_PRESSURE;
					}
					else if (AirPumpLPEflag)
					{
						chalgError |= CHALG_ERR_LOW_AIRPUMP_PRESSURE;
					}
				}
			}

			/*
			 * Handle errors set outside charging algorithm
			 */
			if(regu_status.Detail.Derate.Detail.HighVoltage || regu_status.Detail.Derate.Detail.LowVoltage)
			{
				/*
				 * Set values are set to zero
				 */
				CaiOutput.ChalgToCc.Mode = ReguMode_Off ;
				CaiOutput.ChalgToCc.Set.U = 0.0;
				CaiOutput.ChalgToCc.Set.I = 0.0;
				CaiOutput.ChalgToCc.Set.P = 0.0;

				/* Red */
				LedRed = Output_On;
				/* Yellow */
				LedYellow = Output_Off;
				/* Green */
				LedGreen = Output_Off;
			}

			/* BM error handling */
			if (bmError & RADIO_BMERROR_HIGH_BATTERY_TEMP)
			{
				chalgError |= CHALG_ERR_BM_TEMP;
			}

			if (bmError & RADIO_BMERROR_LOW_ACID_LEVEL)
			{
				chalgError |= CHALG_ERR_BM_ACID;
			}

			if (bmError & RADIO_BMERROR_VOLT_BALANCE_ERR)
			{
				chalgError |= CHALG_ERR_BM_BALANCE;
			}

			if (bmError & RADIO_BMERROR_LOW_SOC)
			{
				chalgError |= CHALG_ERR_BM_LOW_SOC;
			}

			/*
			 * Write to REG ChalgError if new error occured
			 */
			if (oldChalgError != chalgError)
			{
				reg_putLocal(&chalgError, reg_i_ChalgError, pInst->instId);

				/* Sum all errors during this charge */
				Uint16	chalgErrorSum;
				reg_getLocal(&chalgErrorSum, reg_i_ChalgErrorSum, pInst->instId);
				chalgErrorSum |= chalgError;
				reg_putLocal(&chalgErrorSum, reg_i_ChalgErrorSum, pInst->instId);
			}

			/*
			 * Update BBCStatus depending on error
			 */
			{
				Uint8 BBCStatusOld;
				Uint32 BBCError;
				Uint32 BBCErrorOld;

				/*
				 * Check status
				 */
				{
					/* Get old BBCStatus */
					BBCStatusOld = pInst->BBCStatus;
					/* Update BBCStatus*/
					if(pInst->chalgStatus & CHALG_STAT_PRE_CHARG)
						pInst->BBCStatus = BBC_STAT_PRE;
					if(pInst->chalgStatus & CHALG_STAT_MAIN_CHARG)
						pInst->BBCStatus = BBC_STAT_MAIN;
					if(pInst->chalgStatus & CHALG_STAT_ADDITIONAL_CHARG)
						pInst->BBCStatus = BBC_STAT_ADDITIONAL;
					if(pInst->chalgStatus & CHALG_STAT_EQUALIZE_CHARG)
						pInst->BBCStatus = BBC_STAT_EQUALIZE;
					if(pInst->chalgStatus & CHALG_STAT_MAINTENANCE_CHARG)
						pInst->BBCStatus = BBC_STAT_MAINTENANCE;
					if(pInst->chalgStatus & CHALG_STAT_CHARG_COMPLETED)
						pInst->BBCStatus |= BBC_STAT_COMPLETED;

					/* Compare new status with old */
					if(pInst->BBCStatus != BBCStatusOld)
					{
						/* Reset BBCStatusTime*/
						pInst->BBCStatusTime = 0;
					}

					/*
					 * BBC LED indication?
					 */
					Uint8 BBCMode;
					Uint8 RadioNwkStatus;

					reg_get(&BBCMode, chalg__BBC_func);
					reg_get(&RadioNwkStatus, chalg__nwkStatus);

					if(BBCMode && (RadioNwkStatus == RADIO__NWKSTATUS_CONNECTED))
					{
						/* Red */
						// Set from chalg
						/* Yellow */
						LedYellow = Output_Off;
						/* Green */
						LedGreen = Output_Off;

						if(BBC && (pInst->BBCStatus & BBC_STAT_COMPLETED))
						{
							/* If Ready */
							/* Green */
							LedGreen = Output_On;
						}
						else
						{
							//if(pInst->BBCStatus)
							if(pInst->chalgStatus & CHALG_STAT_CHARGING)
							{
								/* If charging */
								/* Yellow */
								LedYellow = Output_On;
							}

						}
					}

				}
				/*
				 * Check error
				 */

				/* Mask out BBC error from regu and chalg error */
				if((chalgError & CHALG_CHALG_ERR_MASK) ||
				   (Measurements.Status.Detail.Error.Sum & CHALG_REGU_ERR_MASK) ||
				   (supStatus & SUP_STATUS_ADDR_CONFLICT) ||
				   (supStatus & SUP_STATUS_BM_ADDR_CONFLICT))
				{
					BBCError = TRUE;
				}
				else
				{
					BBCError = FALSE;
				}

				if(BBCError)
				{
					/* Reset BBCStatus*/
					pInst->BBCStatus = 0;
					/* Reset BBCStatusTime*/
					pInst->BBCStatusTime = 0;
					/* Reset BBC flag*/
					BBC = FALSE;
					reg_putLocal(&BBC, reg_i_BBC, pInst->instId);

				}

				/* Trigger BBC error if error has changed (occurred/disappeared)*/
				reg_getLocal(&BBCErrorOld, reg_i_BBCError, pInst->instId);
				if(BBCError != BBCErrorOld)
				{
					/* Set BBCError */
					reg_putLocal(&BBCError, reg_i_BBCError, pInst->instId);
				}
			}

			/*
			 * Update compensated measurements for use in other FBs
			 */
			Chalg_SaveBatteryMeasurements(&CaiOutput.BatteryMeas);

			/*
			 * Set LED indication
			 */
			{
				reg_putLocal(&LedRed, reg_i_Red, pInst->instId);
				reg_putLocal(&LedYellow, reg_i_Yellow, pInst->instId);
				reg_putLocal(&LedGreen, reg_i_Green, pInst->instId);
			}

			/*
			 * Set output pointer
			 */
			ChalgOutput->Mode = CaiOutput.ChalgToCc.Mode;
			ChalgOutput->Set = CaiOutput.ChalgToCc.Set;
			ChalgOutput->Uact.off = CaiOutput.ChalgToCc.ReguOff.Condition.Detail.Uact;
			ChalgOutput->Uact.on = CaiOutput.ChalgToCc.ReguOn.Condition.Detail.Uact;
			*UactOffInterval = CaiOutput.ChalgToCc.ReguOff.Uact;
			*UactOnInterval = CaiOutput.ChalgToCc.ReguOn.Uact;
#if 0
			{
				Uint8 RemoteIn;
				reg_get(&RemoteIn, chalg__RemoteIn_func);
				if(RemoteIn == SUP_RIFUNC_START_STOP){
					ChalgOutput->RemoteIn.off = BatterydetectCond_True;
					ChalgOutput->RemoteIn.on = BatterydetectCond_False;
				}
				else{
					ChalgOutput->RemoteIn.off = BatterydetectCond_NotUsed;
					ChalgOutput->RemoteIn.on = BatterydetectCond_NotUsed;
				}
			}
#endif
		}
	}
	else{
		ChalgOutput->Mode = ReguMode_Off;
		ChalgOutput->Set.U = NAN;
		ChalgOutput->Set.I = NAN;
		ChalgOutput->Set.P = NAN;
	}

	/*
	 * Generate new CHALG values event.
	 */
	{
		Boolean sentOk;

		sentOk =
			msg_sendTry(
				MSG_LIST(statusRep),
				PROJECT_MSGT_STATUSREP,
				SUP_STAT_CHALG_VALUES
			);

		if(!sentOk)
		{
			Uint8 AssertId = 5;
			reg_put(&AssertId, chalg__EvtAssert);
		}
	}
#endif
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg__validateBattInfo
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Validate battery info.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE reg_Status chalg__validateBattInfo(
	sys_RegHandle			regHandle,
	void const_D *			pData,
	void const_P *			pMinMax
) {
	//chalg_BattInfoType *	pBattInfo;

	//pBattInfo = (chalg_BattInfoType *) pData;

	/*
	 * TODO:	The battery values can be checked here or in the 
	 *			project_storeBatteryInfo() function in project.c.
	 *
	 *			Checking them in project.c makes it possible to return a more
	 *			specific error code so that GUI can inform the user of the
	 *			error.
	 */

	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg__validateBattInfoMulti
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Validate battery info multi.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE reg_Status chalg__validateBattInfoMulti(
	sys_RegHandle			regHandle,
	void const_D *			pData,
	void const_P *			pMinMax
) {
	//chalg_BattInfoMultiType *	pBattInfoMulti;

	//pBattInfoMulti = (chalg_BattInfoMultiType *) pData;

	/*
	 * TODO:	The battery values can be checked here or in the
	 *			project_storeBatteryInfoMulti() function in project.c.
	 *
	 *			Checking them in project.c makes it possible to return a more
	 *			specific error code so that GUI can inform the user of the
	 *			error.
	 */

	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_caPackWrite
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write data to CaPack.
*
*	\param		pHandle		Ptr to the file "handle" i.e. struct of type
*							regd_StdioFile.
*	\param		pFileIo		Ptr to the structure describing the data to write.
*
*	\return		REG_OK if successful, an error code otherwise. In case the error
*				code is REG_ERRNO_ERR, the ioError field of 'pFileIo' is set
*				to one of:
*				- EBADF
*				- EINVAL
*				- ENOSPC
*
*	\details	This function should not be called directly from application 
*				code. It is part of the REG file register interface. REG will
*				call the function when the CaPack register should be written.
*
*	\note		Scheduling is disabled when REG calls the function.
*
*******************************************************************************/

PUBLIC reg_Status chalg_caPackWrite(
	void const_P *			pHandle,
	reg_FileIo *			pFileIo
) {
	chalg_CaPackHandle const_P * pCaPackHandle;

	pCaPackHandle = (chalg_CaPackHandle const_P *) pHandle;

	memcpy(
		&pCaPackHandle->pCaPack[pFileIo->offset],
		pFileIo->ioBuffer,
		pFileIo->x.length
	);

	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_caPackRead
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read data from CaPack.
*
*	\param		pHandle Ptr to the file "handle" i.e. struct of type
*						regd_StdioFile.
*	\param		pFileIo Ptr to the structure describing the buffer where to read
*						data.
*
*	\return		REG_OK if successful, an error code otherwise. In case the error
*				code is REG_ERRNO_ERR, the ioError field of 'pFileIo' is set
*				to one of:
*				- EBADF
*				- EINVAL
*
*	\details	This function should not be called directly from application 
*				code. It is part of the REG file register interface. 
*
*	\note		Scheduling is disabled when REG calls the function.
*
*******************************************************************************/

PUBLIC reg_Status chalg_caPackRead(
	void const_P *			pHandle,
	reg_FileIo *			pFileIo
) {
	/*
	 * CaPack is probably never read via REG.
	 */

	deb_assert(FALSE);

	return(REG_NO_DATA);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_caPackControl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Open or close CaPack.
*
*	\param		pHandle		Ptr to the file "handle" i.e. struct of type
*							regd_StdioFile.
*	\param		pArgm 		Ptr to arguments, if any.
*	\param		pCommand	Ptr to the command byte (reg_ControlCmd) and 
*							optional arguments following it.
*
*	\return		REG_OK if successful, an error code otherwise. In case the error
*				code is REG_ERRNO_ERR, the errno variable should be set to one
*				of:
*				- EBADF
*				- EACCES
*				- EMFILE
*				- ENOENT
*
*	\details	This function should not be called directly from application 
*				code. It is part of the REG file register interface. 
*
*	\note		Scheduling may be disabled when REG calls the function.
*
*******************************************************************************/

PUBLIC reg_Status chalg_caPackControl(
	void const_P *			pHandle,
	void *					pArgm,
	void const_P *			pCommand
) {
//	chalg_CaPackHandle const_P * pCaPackHandle;

//	pCaPackHandle = (chalg_CaPackHandle const_P *) pHandle;

//	reg_FileIo * test;

	

	return(REG_OK);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_capackEnum
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get algorithm name from algorithm number.
*
*	\param		idx		Algorithm number.
*	\param		pBuf 	Pointer to buffer where the name will be copied.
*	\param		pCnt	Number of algorithms available.
*
*	\return		
*
*	\details	
*
*	\note		
*
*******************************************************************************/
PUBLIC Uint8 chalg_capackEnum(
	Uint8					idx,
	BYTE *					pBuf,
	Uint8 *					pCnt
) {
	Uint8 BitConfig;

	deb_assert(pCnt);

	reg_get(&BitConfig, chalg__BitConfig);
	if(BitConfig & CHALG_BITCFG_CEC)
	{
		*pCnt = dim(ChargingAlgorithmsCEC);
	}
	else
	{
		*pCnt = dim(ChargingAlgorithms); // TODO: cnt
	}

	if (pBuf != NULL)
	{
		if (idx < *pCnt)
		{
			Identity_ConstPar_Type* tmp;

			if(BitConfig & CHALG_BITCFG_CEC)
			{
				tmp = (Identity_ConstPar_Type *)ChargingAlgorithmsCEC[idx]->Identity_ConstPar_Addr;
			}
			else
			{
				tmp = (Identity_ConstPar_Type *)ChargingAlgorithms[idx]->Identity_ConstPar_Addr;
			}
			BYTE *			pSrc = (BYTE *) tmp->Name.Addr; // TODO: src
			Uint8			len = tmp->Name.Size - 1; // TODO: len
			Uint8			pos;

			len = MIN(len, CHALG_ALG_NAME_LEN_MAX); // Name is max 12 bytes.

			for (pos = 0; pos < len; pos++)
			{
				*pBuf++ = *pSrc++;
			}

			return len;
		}
	}

	return 0;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_GetAlgNo
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get algorithm number from algorithm index and store it.
*
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC Uint16 chalg_GetAlgNo(Uint16 AlgIdx)
{
	Identity_ConstPar_Type* tmp;
	Uint16 AlgNo;
	Uint8 BitConfig;

	reg_get(&BitConfig, chalg__BitConfig);
	if(BitConfig & CHALG_BITCFG_CEC)
	{
		tmp = (Identity_ConstPar_Type *)ChargingAlgorithmsCEC[AlgIdx]->Identity_ConstPar_Addr;
	}
	else
	{
		tmp = (Identity_ConstPar_Type *)ChargingAlgorithms[AlgIdx]->Identity_ConstPar_Addr;
	}
	AlgNo = (Uint16) tmp->IdNumber;

	return AlgNo;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* chalg_GetUserParamAlgNo
*
*--------------------------------------------------------------------------*//**
*
* \brief    Get user parameter algorithm number from algorithm index.
*
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC Uint16 chalg_GetUserParamAlgNo(Uint16 AlgIdx)
{
  Identity_ConstPar_Type* tmp;
  Uint16 AlgNo;

  tmp = (Identity_ConstPar_Type *)ChargingAlgorithmsUserP[AlgIdx]->Identity_ConstPar_Addr;
  AlgNo = (Uint16) tmp->IdNumber;

  return AlgNo;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* chalg_GetCurveData
*
*--------------------------------------------------------------------------*//**
*
* \brief    Get the curve data.
*
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC bool chalg_GetCurveData(Uint16 idNumber,
                               void** curveData)
{
  const AlgDef_ConstPar_Type* algDef;
  uint32_t algCount = dim(ChargingAlgorithms);
  uint32_t i;

  // Find the charging curve
  for (i = 0; i < algCount; ++i) {
    Identity_ConstPar_Type* algId;

    algDef = ChargingAlgorithms[i];
    algId = (Identity_ConstPar_Type*)algDef->Identity_ConstPar_Addr;

    if (idNumber == algId->IdNumber) {
      break;
    }
  }

  // Curve not found
  if (i >= algCount) {
    return false;
  }

  // Return curve data
  *curveData = (void*)algDef;

  return true;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	chalg_GetAlgIdx
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get algorithm index from algorithm number.
*
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC Uint16 chalg_GetAlgIdx(Uint16 AlgNo)
{
	bool AlgFound = FALSE;
	Uint8 MaxIdx;
	Uint8 BitConfig;
	Uint16 AlgIdx = 0;	// Default value if no valid algorithm

	reg_get(&BitConfig, chalg__BitConfig);
	if(BitConfig & CHALG_BITCFG_CEC)
	{
		MaxIdx = dim(ChargingAlgorithmsCEC);
	}
	else
	{
		MaxIdx = dim(ChargingAlgorithms);
	}

	while((AlgIdx < MaxIdx) && (AlgFound != TRUE))
	{
		Identity_ConstPar_Type* algData;

		if(BitConfig & CHALG_BITCFG_CEC)
		{
			algData = (Identity_ConstPar_Type *)ChargingAlgorithmsCEC[AlgIdx]->Identity_ConstPar_Addr;
		}
		else
		{
			algData = (Identity_ConstPar_Type *)ChargingAlgorithms[AlgIdx]->Identity_ConstPar_Addr;
		}
		Uint16 AlgId = (Uint16) algData->IdNumber;
		if(AlgId == AlgNo)
		{
			AlgFound = TRUE;
		}
		else
		{
			AlgIdx++;
		}
	}
	return AlgIdx;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* chalg_GetAlgorithmCount
*
*--------------------------------------------------------------------------*//**
*
* \brief    Get algorithm count.
*
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC uint16_t chalg_GetAlgorithmCount(void)
{
  Uint8 count;
  Uint8 bitConfig;

  reg_get(&bitConfig, chalg__BitConfig);

  if (bitConfig & CHALG_BITCFG_CEC) {
    count = dim(ChargingAlgorithmsCEC);
  } else {
    count = dim(ChargingAlgorithms);
  }

  return count;
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* chalg_GetUserParameterAlgorithmCount
*
*--------------------------------------------------------------------------*//**
*
* \brief    Get user parameter algorithm count.
*
*
* \return
*
* \details
*
* \note
*
*******************************************************************************/
PUBLIC uint16_t chalg_GetUserParameterAlgorithmCount(void)
{
  return dim(ChargingAlgorithmsUserP);
}


PRIVATE void ChalgCalculateOutput(const Output_enum in, unsigned int* timer, sys_RegHandle reg, sys_RegHandle reg2)
{
	chalg__Inst *			pInst;

	pInst = chalgInst;
	Uint16 Out; // analog output signal [0, 0x7FFF] <-> [off, on]
	Uint8 RemoteOut;

	switch(in)
	{
	case Output_Egal : // keep current value
		Out = 0;
		break;
	case Output_Off : // turn off
		Out = 0;
		reg_put(&Out, reg);
		*timer = 0;
		break;
	case Output_On : // turn on
		Out = 0x7FFF;
		reg_put(&Out, reg);
		break;
	case Output_Toggle1 : // toggle fast
		if(*timer)
		{
			*timer = 0;
			Out = 0;
		}
		else
		{
			*timer = 1;
			Out = 0x7FFF;
		}
		reg_put(&Out, reg);
		break;
	case Output_Toggle2 : // toggle slow
		{
			if(*timer < 2) // in period [0, 1]
				Out = 0;
			else if(*timer < 4) // in period [2, 3]
				Out = 0x7FFF;
			if(*timer == 3)
				*timer = 0;

			*timer = *timer + 1;
		}
		reg_put(&Out, reg);
		break;
	case Output_AirPump : // Air pump cyclic control
		{
			if(pInst->AirPumpPumpOn == 0 || pInst->AirPumpPumpCycle == 0){
				Out = 0;		// Pump off (invalid values)
			}
			else if((pInst->AirPumpPumpOn == pInst->AirPumpPumpCycle)&&(!AirPumpFailed)){ 	// And if hasn't failed
				Out = 0x7FFF;	// Pump constantly on
			}
			else if((*timer < pInst->AirPumpPumpOn)&&(!AirPumpFailed)){						// And if hasn't failed
				Out = 0x7FFF;	// Pump on time
			}
			else{
				Out = 0;		// Pump off time
			}

			*timer = (*timer + 1) % pInst->AirPumpPumpCycle;
		}
		reg_put(&Out, reg);
		break;
	case Output_Water :		 	// Water duration control
		{
			if(pInst->WaterDuration == 0){
				Out = 0;		// Water off (invalid values)
			}
			else if(*timer < pInst->WaterDuration){
				Out = 0x7FFF;	// Water on time
			}
			else{
				Out = 0;		// Water off time
			}

			*timer = *timer + 1;
		}
		reg_put(&Out, reg);
		break;
	}

	if(Out)
	{
		RemoteOut = 1;
	}
	else
	{
		RemoteOut = 0;
	}

	reg_put(&RemoteOut, reg2);

}

PRIVATE void Chalg_SaveBatteryMeasurements(BatteryMeas_type*     BatteryMeas)
{
	chalg__Inst *			pInst;

	pInst = chalgInst;

	{ // UmeasSum
		Uint32 UmeasSum;

		UmeasSum = (Uint32) (BatteryMeas->Ubatt * 1000 + 0.5);
		reg_putLocal(&UmeasSum, reg_i_UmeasSum_Real, pInst->instId); // Display use Real

		UmeasSum = (Uint32) (BatteryMeas->Ubatt + 0.5);
		reg_putLocal(&UmeasSum, reg_i_UmeasSum, pInst->instId);

		reg_putLocal((void *)&BatteryMeas->Ubatt, reg_i_UmeasSum_Float, pInst->instId);
	}

	{ // ImeasSum
		Uint32 ImeasSum;

		ImeasSum = (Uint32) (BatteryMeas->Ibatt * 1000 + 0.5);
		reg_putLocal(&ImeasSum, reg_i_ImeasSum_Real, pInst->instId); // Display use Real

		ImeasSum = (Uint32) (BatteryMeas->Ibatt + 0.5);
		reg_putLocal(&ImeasSum, reg_i_ImeasSum, pInst->instId);

		reg_putLocal((void *)&BatteryMeas->Ibatt, reg_i_ImeasSum_Float, pInst->instId);
	}

	{ // PmeasSum
		Uint32 PmeasSum;

		PmeasSum = (Uint32) (BatteryMeas->Pbatt + 0.5);
		reg_putLocal(&PmeasSum, reg_i_PmeasSum, pInst->instId);

		reg_putLocal((void *)&BatteryMeas->Pbatt, reg_i_PmeasSum_Float, pInst->instId);
	}
}

PUBLIC Uint8 chalg_UpdateAlgIdx(Uint16 AlgNo)
{
	chalg__Inst *			pInst;
	AlgDef_ConstPar_Type *  pAlgDef;
	bool AlgFound;
	Uint8 MaxIdx;
	Uint16 AlgIdx;
	Uint8 Status;
	Uint8 AlgName[CHALG_ALG_NAME_LEN_MAX + 1] = {3,'N','/','A'}; // Ari: At least "Demo-board" name is larger than 9.
	Uint8 BitConfig;
	Uint8 ChargingMode;

	pInst = chalgInst;
	Status = TRUE;

	/*
	 * Update AlgIdx
	 */

	AlgFound = FALSE;
	AlgIdx = 0;

	reg_get(&ChargingMode, chalg__ChargingMode);
	if(ChargingMode != CHALG_MULTI)
	{
		reg_get(&BitConfig, chalg__BitConfig);
		if(BitConfig & CHALG_BITCFG_CEC)
		{
			MaxIdx = dim(ChargingAlgorithmsCEC);
		}
		else
		{
			MaxIdx = dim(ChargingAlgorithms);
		}

		while((AlgIdx < MaxIdx) && (AlgFound != TRUE))
		{
			Identity_ConstPar_Type* algData;
			if(BitConfig & CHALG_BITCFG_CEC)
			{
				algData = (Identity_ConstPar_Type *)ChargingAlgorithmsCEC[AlgIdx]->Identity_ConstPar_Addr;
			}
			else
			{
				algData = (Identity_ConstPar_Type *)ChargingAlgorithms[AlgIdx]->Identity_ConstPar_Addr;
			}
			Uint16 AlgId = (Uint16) algData->IdNumber;
			if(AlgId == AlgNo)
			{
				Uint16 AlgVersion = (Uint16) algData->Version;
				AlgFound = TRUE;
				// set algorithm version
				reg_putLocal(&AlgVersion,	reg_i_version_default,	pInst->instId);

			}
			else
			{
				AlgIdx++;
			}
		}
		// if valid algorithm
		if(AlgFound == TRUE)
		{
			Uint8 algNoMax = 0;
			Uint8 len = 0;

			/*
			 * Update user parameters
			 */
			if(BitConfig & CHALG_BITCFG_CEC)
			{
				pAlgDef = (AlgDef_ConstPar_Type *)ChargingAlgorithmsCEC[AlgIdx];
			}
			else
			{
				pAlgDef = (AlgDef_ConstPar_Type *)ChargingAlgorithms[AlgIdx];
			}
			updateUserParameters(pAlgDef);

			/*
			 * Set algorithm default index
			 */
			reg_putLocal(&AlgIdx,	reg_i_algIdx_default,	pInst->instId);

			/*
			 * Get algorithm default name
			 */
			if((len = chalg_capackEnum(AlgIdx, &AlgName[1], &algNoMax))){
				AlgName[0] = MIN(len, CHALG_ALG_NAME_LEN_MAX); // Name is max 12 bytes.
			}

		}
		else
		{
			// set firmware malfunction?
			Status = FALSE;

			// Reset user parameters if no valid alg.
			resetUserParameters(&CaiInput.UserParam);
		}
	}
	/*
	 *Set algorithm default name anyway
	 */
	reg_putLocal(&AlgName, reg_i_algName_default, pInst->instId);

	return Status;
}

PUBLIC Uint8 chalg_UpdateAlgIdxFromBm(Uint16 AlgNo)
{
	chalg__Inst *			pInst;

	bool AlgFound;
	Uint8 MaxIdx;
	Uint16 AlgIdx;
	Uint8 Status;
	Uint8 AlgName[CHALG_ALG_NAME_LEN_MAX + 1] = {3,'N','/','A'}; // Ari: At least "Demo-board" name is larger than 9.
	Uint8 BitConfig;

	pInst = chalgInst;
	Status = TRUE;

	/*
	 * Update AlgIdx
	 */

	AlgFound = FALSE;
	reg_get(&BitConfig, chalg__BitConfig);
	if(BitConfig & CHALG_BITCFG_CEC)
	{
		MaxIdx = dim(ChargingAlgorithmsCEC);
	}
	else
	{
		MaxIdx = dim(ChargingAlgorithms);
	}
	AlgIdx = 0;

	while((AlgIdx < MaxIdx) && (AlgFound != TRUE))
	{
		Identity_ConstPar_Type* algData;
		if(BitConfig & CHALG_BITCFG_CEC)
		{
			algData = (Identity_ConstPar_Type *)ChargingAlgorithmsCEC[AlgIdx]->Identity_ConstPar_Addr;
		}
		else
		{
			algData = (Identity_ConstPar_Type *)ChargingAlgorithms[AlgIdx]->Identity_ConstPar_Addr;
		}
		Uint16 AlgId = (Uint16) algData->IdNumber;
		if(AlgId == AlgNo)
		{
			Uint16 AlgVersion = (Uint16) algData->Version;
			AlgFound = TRUE;
			reg_putLocal(&AlgVersion,	reg_i_currVersion,	pInst->instId);
		}
		else
		{
			AlgIdx++;
		}
	}
	// if valid algorithm
	if(AlgFound == TRUE)
	{
		Uint8 algNoMax = 0;
		Uint8 len = 0;
		chalg_BattInfoType		battInfo;

		/*
		 * Set algorithm current index
		 */
		reg_putLocal(&AlgIdx,	reg_i_currAlgIdx,	pInst->instId);
		reg_putLocal(&AlgIdx,	reg_i_algIdx_bm,	pInst->instId);	// BMU charging params

		/*
		 * Get algorithm current name
		 */
		if((len = chalg_capackEnum(AlgIdx, &AlgName[1], &algNoMax))){
			AlgName[0] = MIN(len, CHALG_ALG_NAME_LEN_MAX); // Name is max 12 bytes.
		}

		/*
		 * Reset charging algorithm
		 */
		CaiInput.Control |= CAI_INPUT_CONTROL_RESET;

		/*
		 * Set charging parameters for CaiInit
		 */
		battInfo.algNo = AlgNo;
		if(BitConfig & CHALG_BITCFG_CEC)
		{
			CaiInput.Algorithm.AlgDef_p = (AlgDef_ConstPar_Type *)ChargingAlgorithmsCEC[AlgIdx];
		}
		else
		{
			CaiInput.Algorithm.AlgDef_p = (AlgDef_ConstPar_Type *)ChargingAlgorithms[AlgIdx];
		}

		reg_getLocal(&battInfo.baseLoad,	reg_i_currBaseLoad, 	pInst->instId);
		CaiInput.Algorithm.BaseLoad = (float)battInfo.baseLoad*(1/1000.0);

		reg_getLocal(&battInfo.cableRes,	reg_i_currCableResBm, 	pInst->instId);
		CaiInput.Algorithm.CableRes = (float)battInfo.cableRes*(1/1000.0);

		reg_getLocal(&battInfo.capacity,	reg_i_currCapacity, 	pInst->instId);
		CaiInput.Algorithm.Capacity = battInfo.capacity;

		reg_getLocal(&battInfo.cell,	reg_i_currCells,		pInst->instId);
		CaiInput.Algorithm.Cells = battInfo.cell;

		reg_putLocal(&battInfo.algNo,		reg_i_algNo_bm,		pInst->instId);
		reg_putLocal(&battInfo.capacity,	reg_i_capacity_bm,	pInst->instId);
		reg_putLocal(&battInfo.cableRes,	reg_i_cableRes_bm,	pInst->instId);
		reg_putLocal(&battInfo.cell,		reg_i_cells_bm,		pInst->instId);
		reg_putLocal(&battInfo.baseLoad,	reg_i_baseLoad_bm,	pInst->instId);

		if(BitConfig & CHALG_BITCFG_CEC)
		{
			if(engineValidateParamsCEC(battInfo) != BATTERY_NO_ERROR)
			{
				// indicate incorrect charging parameters
				Status = FALSE;
			}
		}

		/*
		 * Update curve parameters
		 */
		updateUserParameters(CaiInput.Algorithm.AlgDef_p);
		//readDefaultParameters(&CaiInput.UserParam); // Do not read default
		setUserParameters(&CaiInput.UserParam, pInst, ((Mandatory_ConstPar_Type* )CaiInput.Algorithm.AlgDef_p->Mandatory_ConstPar_Addr)->UserParam.Size);
	}
	else
	{
		// indicate incorrect charging parameters
		Status = FALSE;

		// Reset user parameters if no valid alg.
		resetUserParameters(&CaiInput.UserParam);
	}

	/*
	 *Set algorithm current name anyway
	 */
	reg_putLocal(&AlgName,		reg_i_currAlgName,		pInst->instId);

	return Status;
}

PUBLIC Uint8 chalg_UserParamIsValid(Uint16 AlgNo)
{
	Uint16 AlgIdx;
	Uint8 MaxIdx;
	Uint8 AlgFound;

	AlgIdx = 0;
	MaxIdx = dim(ChargingAlgorithmsUserP);
	AlgFound = FALSE;

	while((AlgIdx < MaxIdx) && (AlgFound != TRUE))
	{
		Identity_ConstPar_Type* algData;
		algData = (Identity_ConstPar_Type *)ChargingAlgorithmsUserP[AlgIdx]->Identity_ConstPar_Addr;

		if(algData->IdNumber == AlgNo)
		{
			AlgFound = TRUE;
		}
		else
		{
			AlgIdx++;
		}
	}
	return AlgFound;
}

PUBLIC Boolean chalg_GetAirPumpFailed(void) // Return error flag
{
	return AirPumpFailed;
}

PUBLIC Boolean chalg_GetAirPumpOutput(void) // Return Airpump function from Chalg
{
	return ChalgAirPumpOn;
}

PUBLIC Boolean chalg_GetWaterPumpOutput(void) // Return Water function from Chalg
{
	return ChalgWaterPumpOn;
}

static void setUserParameters(UserParam_Type* UserParam, chalg__Inst* pInst, int len){
	int n;
	uint32_t valid = 0;
	bool allZeros = TRUE;
	for(n = 0; n < len; n++){
		float Param;

		reg_aGet(&Param, chalg__UserParam, n);
		UserParam->Value[n] = Param;
		valid |= (1 << n);
		
		if (Param){
			allZeros = FALSE;
		}
	}

	UserParam->Valid = valid;
	
	if(allZeros){                                   // If all parameters are 0, get default parameters
		readDefaultParameters(&CaiInput.UserParam);
	}
}

static void readDefaultParameters(UserParam_Type* UserParam){
	if(!algorithm){
		return;
	}
	UserParam_ConstParam_Type* const parameters = (UserParam_ConstParam_Type* )((Mandatory_ConstPar_Type* )algorithm->Mandatory_ConstPar_Addr)->UserParam.Addr;
	const int len = ((Mandatory_ConstPar_Type* )algorithm->Mandatory_ConstPar_Addr)->UserParam.Size;
	int n;

	for(n = 0; n < len; n++){
		float Param = parameters[n].Default;

		reg_aPut(&Param, chalg__UserParam, n);
		UserParam->Value[n] = Param;
	}
}

PUBLIC void chalg_resetStoredParameters(const int algID, const int algVer){
	AlgDef_ConstPar_Type* algDef;

	if (!chalg_GetCurveData(algID, (void**)&algDef)) {
	  return;
	}

	const int version = ((Identity_ConstPar_Type* )algDef->Identity_ConstPar_Addr)->Version;
	UserParam_ConstParam_Type* const parameters = (UserParam_ConstParam_Type* )((Mandatory_ConstPar_Type* )algDef->Mandatory_ConstPar_Addr)->UserParam.Addr;
	const int len = ((Mandatory_ConstPar_Type* )algDef->Mandatory_ConstPar_Addr)->UserParam.Size;

	if (algVer != version) {
	  return;
	}

	for(int n = 0; n < len; n++){
		float Param = parameters[n].Default;
		reg_aPut(&Param, chalg__storedUserParam, n);
	}
}

static void updateUserParameters(const AlgDef_ConstPar_Type *AlgDef_p){

	Uint16 storedAlgID;
	Uint16 storedAlgVer;

	reg_get(&storedAlgID, chalg__storedAlgorithmID);
	reg_get(&storedAlgVer, chalg__storedAlgorithmVer);

	if(AlgDef_p != NULL)
	{
		if(!algorithm || (algorithm && algorithm != AlgDef_p)) {

			if (algorithm && gUserParamsEdited) {				// If user parameters have been edited, store them
				storeUserParameters(((Mandatory_ConstPar_Type*)algorithm->Mandatory_ConstPar_Addr)->UserParam.Size);

				// Update stored algorithm ID and version
				storedAlgID = ((Identity_ConstPar_Type*) algorithm->Identity_ConstPar_Addr)->IdNumber;
				storedAlgVer = ((Identity_ConstPar_Type*) algorithm->Identity_ConstPar_Addr)->Version;

				reg_put(&storedAlgID, chalg__storedAlgorithmID);
				reg_put(&storedAlgVer, chalg__storedAlgorithmVer);
			}

			algorithm = AlgDef_p;

			if (storedAlgID == ((Identity_ConstPar_Type*) algorithm->Identity_ConstPar_Addr)->IdNumber &&
					storedAlgVer == ((Identity_ConstPar_Type*) algorithm->Identity_ConstPar_Addr)->Version) { 					// Check if stored algorithm matches the new set algorithm

				chalg_setStoredUserParameters(((Mandatory_ConstPar_Type*)algorithm->Mandatory_ConstPar_Addr)->UserParam.Size);	// Set stored user parameters
			}
			else {
				readDefaultParameters(&CaiInput.UserParam);
			}
		}
		else {
			algorithm = AlgDef_p;

			if (gUserParamsEdited) {			// If user parameters have been edited, store them
				storeUserParameters(((Mandatory_ConstPar_Type*)algorithm->Mandatory_ConstPar_Addr)->UserParam.Size);

				// Update stored algorithm ID and version
				storedAlgID = ((Identity_ConstPar_Type*) algorithm->Identity_ConstPar_Addr)->IdNumber;
				storedAlgVer = ((Identity_ConstPar_Type*) algorithm->Identity_ConstPar_Addr)->Version;

				reg_put(&storedAlgID, chalg__storedAlgorithmID);
				reg_put(&storedAlgVer, chalg__storedAlgorithmVer);
			}

			if (nfcStoredUserParamsEdited) {	// If stored user parameters have been edited via APP, set them
				chalg_setStoredUserParameters(((Mandatory_ConstPar_Type*)algorithm->Mandatory_ConstPar_Addr)->UserParam.Size);	// Set stored user parameters
				nfcStoredUserParamsEdited = FALSE;
				nfcUserParamsEdited = TRUE;
			}
		}
	}
}

static void resetUserParameters(UserParam_Type* UserParam){
	if(!algorithm){
		const int len = USERPARAM_VALUE_SIZE;
		int n;
		float Param = 0.0;

		for(n = 0; n < len; n++){
			reg_aPut(&Param, chalg__UserParam, n);
			UserParam->Value[n] = Param;
		}
	}
}

PUBLIC void	chalg_setStoredUserParameters(int len){
	int n;
	float Param;

	for(n = 0; n < len; n++){
		reg_aGet(&Param, chalg__storedUserParam, n);
		reg_aPut(&Param, chalg__UserParam, n);
	}
}

static void storeUserParameters(int len){
	int n;
	float Param;

	for(n = 0; n < len; n++){
		reg_aGet(&Param, chalg__UserParam, n);
		reg_aPut(&Param, chalg__storedUserParam, n);
	}
}
