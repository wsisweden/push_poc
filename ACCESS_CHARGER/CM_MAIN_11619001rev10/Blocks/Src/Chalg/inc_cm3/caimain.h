/*
 $Revision: 117 $
 $Date: 2010-03-29 08:44:46 +0200 (må, 29 mar 2010) $
 $Author: bsi $
 $HeadURL: https://subversion.mpi.local/svn/micropower/_user/bsi/alg/alg_types.h $
 */

/**************************************************************************
 Project:     ACCESS:CM
 Module:      caimain.h
 Contains:    Type definitions.
 Environment: Compiled and tested with GNU GCC.
 Copyright:   Micropower ED
 Version:
 ---------------------------------------------------------------------------
 History:
 Rev      Date      Sign  Description
 bsi   First version.
 ***************************************************************************/

/***************************************************************************
 Definition and Type
 ***************************************************************************/
#ifndef CAIMAIN_H
#define CAIMAIN_H

#include "cai_cc.h"
#include "cai_constparam.h"

typedef struct {
	union {
		uint32_t Sum;
		struct {
			BatterydetectCond_enum Uact:4;
			BatterydetectCond_enum RemoteIn:4;
			BatterydetectCond_enum CaiIn:4;
		} Detail;
	} Condition;
	Interval_type Uact;
} ReguOnOff_Type;

typedef struct {
	ReguMode_enum           Mode;
	ReguValue_type          Set;         // Set Values
	ReguValue_type          ErrWin;      // Windows where to consider regulating
	ReguOnOff_Type          ReguOn;      // If regulator is not delivering output and Mode == Auto and ReguOn condition is fulfilled, then start delivering output.
	ReguOnOff_Type          ReguOff;     // If regulator delivering output and Mode == On and ReguOff condition is fulfilled, then stop delivering output and reset chalg
                                         // If regulator delivering output and Mode == Auto and ReguOff condition is fulfilled, then stop delivering output.
} ChalgToCc_Type;

typedef struct {
	uint16 Phase;
	uint32 Ctrl;
	float UbattLow;
	float UbattHigh;
} ReguOnOff_ConstParam_Type;

typedef union {
	struct {
		uint32 UerrWin:1;
		uint32 IerrWin:1;
		uint32 PerrWin:1;
		uint32 UsetDerate:1;
		uint32 IsetDerate:1;
		uint32 PsetDerate:1;
		uint32 TempDerate:1;
		uint32 PacDerate:1;
		uint32 SumDerate:1;
	} Detail;
	uint32 Sum;
} ReguStatus_type;

typedef struct {
	uint16 Cells;
	float Capacity;
	float BaseLoad;
	float CableRes;
	const AlgDef_ConstPar_Type *AlgDef_p;
} Algorithm_type;

typedef struct Measure_t {
	float Uact;			/* Measured voltage */
	float Iact;			/* Measured current */
	float Pact;			/* Measured power */
	float Tbatt;		/* Measured battery temperature */
} Measure_type;

#define USERPARAM_VALUE_SIZE 32
typedef struct {
	uint32 Valid;
	float Value[USERPARAM_VALUE_SIZE];
} UserParam_Type;

typedef union {
	struct {
		_Bool Stop:1;
		bool F1:1;
		bool F2:1;
		bool RemoteIn:1;
		bool WaterLow:1;
		bool CaiIn:1;
		bool ManEquOn:1;
		bool ManEquOff:1;
		bool IoSpare3:1;	// sorab - used for setting air pump error flag - input to the charging curve
		bool IoSpare4:1;
	} Detail;
	uint32 Sum;
} Input_type;

// Control
// -------
#define CAI_INPUT_CONTROL_RESET (1<<0)
#define CAI_INPUT_CONTROL_FORCE (1<<1)
#define CAI_INPUT_CONTROL_CHARG (1<<2)

typedef struct CaiInput_tag {
	Algorithm_type				Algorithm;
	Measure_type				Measure;
	ChargerStatusFields_type	CcStatus;
	UserParam_Type				UserParam;
	Input_type					Input;
	uint32						Control;
} CaiInput_type;

typedef struct {
	ReguMode_enum Mode;
	float Uset; /* Output voltage */
	float Iset; /* Output current */
	float Pset; /* Output power */
} Regulator_type;

typedef struct BatteryStatus_t {
	float Ubatt;		/* Battery voltage */
	float Ibatt;		/* Battery current */
	float Pbatt;		/* Battery power */
} BatteryMeas_type;

/*
 * CHARGING STAGES
 * OBS!!
 * Must match with stages defined in charging algorithm include file
 */
//#define STAGE_NAME 				0
#define STAGE_IDLE              0	//1
#define STAGE_PRE	            1	//2
#define STAGE_I1	            2	//3
#define STAGE_U1                3	//4
#define STAGE_ADDITIONAL        4	//5
#define STAGE_MAINTENANCE       5	//6
#define STAGE_EQU	        	6
#define STAGE_SPARE2     		7
#define STAGE_SPARE3		    8
#define STAGE_EQUMAN	        9

typedef enum {ChargingStage_Idle = STAGE_IDLE,
			  ChargingStage_Pre = STAGE_PRE,
			  ChargingStage_I1 = STAGE_I1,
			  ChargingStage_U1 = STAGE_U1,
			  ChargingStage_Additional = STAGE_ADDITIONAL,
			  ChargingStage_Maintenance = STAGE_MAINTENANCE,
			  ChargingStage_EQU = STAGE_EQU,
			  ChargingStage_Spare2 = STAGE_SPARE2,
			  ChargingStage_Spare3 = STAGE_SPARE3,
			  ChargingStage_EquMan = STAGE_EQUMAN,
			  ChargingStage_Length} CharginStage_type;

typedef union {
	struct {
		uint32 spare_0_11:12;		// Bit 0..11
		uint32 BatterConnected:1;	// Bit 12
		uint32 PreCharging:1;		// Bit 13
		uint32 MainCharging:1;		// Bit 14
		uint32 Completion:1;		// Bit 15
		uint32 Trickle:1;			// Bit 16
		uint32 Equaliztion:1;		// Bit 17
		uint32 Ready:1;				// Bit 18
		uint32 TbattLow:1;			// Bit 19
		uint32 TbattHigh:1;			// Bit 20
		uint32 spare_21_31:11;		// Bit 21..31
	} Detail;
	uint32 Sum;
} ChargingInfo_type;

typedef union {
	struct {
		uint32 spare_0_11:12;
		uint32 VoltageLow:1;
		uint32 VoltageHigh:1;
		uint32 TemperatureLow:1;
		uint32 TemperatureHigh:1;
		uint32 AhMax:1;
		uint32 WhMax:1;
		uint32 DeltaU:1;
		uint32 DeltaI:1;
		uint32 DeltaP:1;
		uint32 DeltaTbatt:1;
		uint32 TimeMax:1;
		uint32 AirPumpFailure:1;	// Using spare 23
		uint32 spare_24_31:8;
	} Detail;
	uint32 Sum;
} ChargingError_type;

typedef struct {
	uint8 Index;
	struct {
		uint32 Tick;
		float Ubatt;
		float Ibatt;
		float Tbatt;
	} Start;
	struct {
		uint32 Tick;
		float Ubatt;
		float Ibatt;
		float Tbatt;
	} Stop;
	uint32 Time;
	float AmpSecCnt;
	float WattSecCnt;
} ChargingValues_type;

typedef struct {
	struct {
		ChargingInfo_type Info;
		ChargingError_type Error;
	} Instant;
	struct {
		ChargingInfo_type Info;
		ChargingError_type Error;
	} Sticky;
} ChargingStatus_type;

// Status bit definition
#define CAIOUTPUT_STATUS_USERPARAM_OK         0x1

// Output Bit definition
// Nibble 0 Led Green                   76543210
#define OUTPUT_GREEN_MASK                    0xF
#define OUTPUT_GREEN_EGAL                    0x0
#define OUTPUT_GREEN_OFF                     0x1
#define OUTPUT_GREEN_ON                      0x2
#define OUTPUT_GREEN_TOGGLE1                 0x3
#define OUTPUT_GREEN_TOGGLE2                 0x4
// Nibble 1 Led Yellow                  76543210
#define OUTPUT_YELLOW_MASK                  0xF0
#define OUTPUT_YELLOW_EGAL                  0x00
#define OUTPUT_YELLOW_OFF                   0x10
#define OUTPUT_YELLOW_ON                    0x20
#define OUTPUT_YELLOW_TOGGLE1               0x30
#define OUTPUT_YELLOW_TOGGLE2               0x40
// Nibble 2 Led Red                     76543210
#define OUTPUT_RED_MASK                    0xF00
#define OUTPUT_RED_EGAL                    0x000
#define OUTPUT_RED_OFF                     0x100
#define OUTPUT_RED_ON                      0x200
#define OUTPUT_RED_TOGGLE1                 0x300
#define OUTPUT_RED_TOGGLE2                 0x400
// Nibble 3 Discrete Out1               76543210
#define OUTPUT_DOUT1_MASK                 0xF000
#define OUTPUT_DOUT1_EGAL                 0x0000
#define OUTPUT_DOUT1_OFF                  0x1000
#define OUTPUT_DOUT1_ON                   0x2000
#define OUTPUT_DOUT1_TOGGLE1              0x3000
#define OUTPUT_DOUT1_TOGGLE2              0x4000
// Nibble 4 Water Fill                  76543210
#define OUTPUT_WATER_MASK                0xF0000
#define OUTPUT_WATER_EGAL                0x00000
#define OUTPUT_WATER_OFF                 0x10000
#define OUTPUT_WATER_ON                  0x20000
#define OUTPUT_WATER_TOGGLE1             0x30000
#define OUTPUT_WATER_TOGGLE2             0x40000
// Nibble 5 Air Pump l                  76543210
#define OUTPUT_AIR_MASK                 0xF00000
#define OUTPUT_AIR_EGAL                 0x000000
#define OUTPUT_AIR_OFF                  0x100000
#define OUTPUT_AIR_ON                   0x200000
#define OUTPUT_AIR_TOGGLE1              0x300000
#define OUTPUT_AIR_TOGGLE2              0x400000
// Nibble 6 IO module spare 0           76543210
#define OUTPUT_IOSPARE0_MASK           0xF000000
#define OUTPUT_IOSPARE0_EGAL           0x0000000
#define OUTPUT_IOSPARE0_OFF            0x1000000
#define OUTPUT_IOSPARE0_ON             0x2000000
#define OUTPUT_IOSPARE0_TOGGLE1        0x3000000
#define OUTPUT_IOSPARE0_TOGGLE2        0x4000000
// Nibble 7 IO module spare 1           76543210
#define OUTPUT_IOSPARE1_MASK          0xF0000000
#define OUTPUT_IOSPARE1_EGAL          0x00000000
#define OUTPUT_IOSPARE1_OFF           0x10000000
#define OUTPUT_IOSPARE1_ON            0x20000000
#define OUTPUT_IOSPARE1_TOGGLE1       0x30000000
#define OUTPUT_IOSPARE1_TOGGLE2       0x40000000

typedef enum {
	Output_Egal,
	Output_Off,
	Output_On,
	Output_Toggle1,
	Output_Toggle2,
	Output_AirPump,
	Output_Water
} Output_enum;

typedef union {
	struct {
		Output_enum LedGreen:4;
		Output_enum LedYellow:4;
		Output_enum LedRed:4;
		Output_enum Dout1:4;
		Output_enum WaterFill:4;
		Output_enum Airpump:4;
		Output_enum IoSpare1:4;
		Output_enum IoSpare2:4;
	} Detail;
	uint32 Sum;
} Output_type;


typedef struct CaiOutput_tag {
	ChalgToCc_Type       ChalgToCc;
	BatteryMeas_type     BatteryMeas;
	ChargingStatus_type  ChargingStatus;
	uint8                ChargingStage;
	ChargingValues_type  ChargingValues[10];
	uint32               Status;
	Output_type          Output;
} CaiOutput_type;


#define MSTATUS_BC_FULLFILLED 0x1
#define MODULE_STATUS_BREAKCOND_FULLFILLED 0x1  // Break Condition full filled
typedef struct Module_tag {
	uint32 LocAddr;			// Address to the definition of the module
	uint16 Phases;			// Number of phases in the module
	uint32 PhaseAddr;		// Address to the active phase (if 0, not active)
	union {
		uint32 Uint;        // Mostly used for break value
		float Float;        // Mostly used for break value
		uint32 Index;       // Used when the module need more variables.
	} Value;
	uint32 Status;
} Module_type;


typedef struct CompValue_tag {
	struct {
		float CableRes;
		float Tbatt;
	} Uact;
	struct {
		float BaseLoad;
		float Tbatt;
	} Iact;
	struct {
		float BaseLoad;
	} Pact;
} CompValue_type;
CompValue_type CompValue;


typedef enum Error_tag {
	Error_No,
	Error_PhaseInit,
	Error_Compare,
	Error_TimerBreak,
	Error_AhBreak,
	Error_Break_WhileLoop,
	Error_UbattWinbreak_Index,
	Error_ReguOnOff_id,
	Error_PhaseSearch,

	Error_size
} Error_enum;


typedef enum Timer_etag {
	Timer0,
	Timer1,
	Timer2,

	Timer_size
} Timer_enum;

typedef struct {
	uint32 Timer[Timer_size];
	float AmpSecCnt[2];  	// Ampere Second Counter
	float WattSecCnt[2];  	// Watt Second Counter
} CounterValue_type;

#define DELTA_SUMS 11
#define DELTA_SIZE 10 		// new Delta0

typedef struct DeltaTrunk_tag {
	int Index; // Index for sum
	uint32 Tick; // Cai_Tick when to change Index
	uint32 Samples; // Number of samples in each sum
	struct {
		float Ubatt;
		float Ibatt;
		float Pbatt;
		float Tbatt;
	} Old;
	struct {
		float Ubatt;
		float Ibatt;
		float Pbatt;
		float Tbatt;
	} Delta;
	struct {
		float Ubatt;
		float Ibatt;
		float Pbatt;
		float Tbatt;
	} Sums[DELTA_SUMS];		// last sum is faded away and first filled with new samples
	struct {	// new Delta0 Circular
		uint8 IdxS; 		// Index for sum
		uint8 IdxDS; 		// Index for delta sum
		uint8 CtrS;			// Counter for sum
		uint8 CtrDS;		// Counter for delta sum
		uint32 Tick;
		uint32 Samp;		// Number of samples in each buffer
		float Ubatt;
		float Ibatt;
		float Pbatt;		// not used
		float Tbatt;		// not used
		struct {
			float Ubatt;
			float Ibatt;
			float Pbatt;	// not used
			float Tbatt;	// not used
		} Sum[DELTA_SIZE];
		struct {
			float Ubatt;
			float Ibatt;
			float Pbatt;	// not used
			float Tbatt;	// not used
		} DSum[DELTA_SIZE];
		struct {
			float Ubatt;
			float Ibatt;
			float Pbatt;	// not used
			float Tbatt;	// not used
		} DeltaSum;
		struct {
			int OldAvgFlag;	// if false old avg. value not found yet.
			float AvgUbatt;
			float AvgIbatt;
			float AvgPbatt;	// not used
			float AvgTbatt;	// not used
		} Old;
		struct {
			float Ubatt;
			float Ibatt;
			float Pbatt;	// not used
			float Tbatt;	// not used
		} Delta;
	} Avg;	// new Delta0
} DeltaValue_type;
extern DeltaValue_type DeltaValue[];


extern uint32 Cai_Tick;
extern Error_enum Error;
extern Module_type Module[idModule_length];
extern uint16 Phase;
extern CounterValue_type CounterValue;

extern void PhaseInit(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p, uint16 PhaseNo);
extern void CaiMain(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p);
extern void CaiInit(const CaiInput_type *CaiInput_p);
extern void Break(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p);


#endif  // #ifndef CAIMAIN_H

/*******************************************************************************
END OF FILE
*******************************************************************************/






