/*
$Revision:$
$Date:$
$Author:$
$HeadURL:$
*/

// Filename: I47_FVLA
// Date:     2020-06-25 13:02:39
// Author:   JJ


#include "cai_constparam.h"

// Make the definition unique, so we be able to compile several algorithms
#undef ID
#define ID(i) I47_FVLA_##i

#define MILLI *0.001
#define PERCE *0.01
#define MINUT *60
#define HOUR *60*60

// Definition of location for Identity, Mandatory and Module
// ---------------------------------------------------------
extern const AlgDef_ConstPar_Type ID(AlgDef_ConstPar);
extern const Identity_ConstPar_Type ID(Identity_ConstPar);
extern const Mandatory_ConstPar_Type ID(Mandatory_ConstPar);
extern const Module_ConstPar_Type ID(Module_ConstPar[idModule_length]);
const AlgDef_ConstPar_Type ID(AlgDef_ConstPar) = {
  (uint32) &ID(AlgDef_ConstPar),
  (uint32) &ID(Identity_ConstPar),
  (uint32) &ID(Mandatory_ConstPar),
  (uint32) &ID(Module_ConstPar)
};


// Identity
// --------
const char ID(Name[]) =         "47.FVLA";

const Identity_ConstPar_Type ID(Identity_ConstPar) = {
      47,  // IdNumber
       3,  // Version
       1,  // InterpreterVer
  { (uint32) ID(Name), sizeof(ID(Name)) }
};


// Mandatory
// ---------
const Power_type ID(Power[]) = {
//  Volt/Cell     Capacity
//  -----------   -----------
  {       1.000,     20 PERCE, },
  {       2.400,     20 PERCE, },
  {       2.800,      5 PERCE, },
  {       2.900,            0, }
};

const Uaccuracy_type ID(Uaccuracy[]) = {
//  Volt/Cell     Accuracy (V)
//  -----------   -----------
  {       1.000,     10 MILLI, },
  {       2.400,     10 MILLI, },
  {       2.800,     20 MILLI, },
  {       2.900,     40 MILLI, }
};


const Iaccuracy_type ID(Iaccuracy[]) = {
//  Capcaity      Accuracy (A)
//  -----------   -----------
  {           0,      1 PERCE, },
  {     1 PERCE,      1 PERCE, },
  {    10 PERCE,    2.5 PERCE, },
  {    20 PERCE,      5 PERCE, }
};

const uint32 ID(IOsignals) = 
//IO_Tbatt   IO_AirPump   IO_Water
//--------   ----------   --------
  0        | 0          | 0       ;



// UserParam
const char ID(UserParam_Name00[]) = "I0_Uset";
const char ID(UserParam_Name01[]) = "I0_Iset";
const char ID(UserParam_Name02[]) = "I0_Tmax";
const char ID(UserParam_Name03[]) = "I1_Iset";
const char ID(UserParam_Name04[]) = "U1_Uset";
const char ID(UserParam_Name05[]) = "I1U1_Tmin";
const char ID(UserParam_Name06[]) = "U1_Ibrk";
const char ID(UserParam_Name07[]) = "I2_Umax";
const char ID(UserParam_Name08[]) = "I2_Iset";
const char ID(UserParam_Name09[]) = "I2_Tmin";
const char ID(UserParam_Name10[]) = "ChFactor";
const char ID(UserParam_Name11[]) = "I3_Ubrk";
const char ID(UserParam_Name12[]) = "I3_Umax";
const char ID(UserParam_Name13[]) = "I3_Iset";
const char ID(UserParam_Name14[]) = "I3_Tpulse";
const char ID(UserParam_Name15[]) = "I3_Tpause";
const UserParam_ConstParam_Type ID(UserParam_ConstParam[]) = {
//   Name Pointer          Name Size                    Default       Min           Max           Unit                       Operator                      Par A   Par B
//   ------------          ---------                    ------------  ------------  ------------  -------------------------  ----------------------------  ------  ------
   { ID(UserParam_Name00), sizeof(ID(UserParam_Name00)),        2.35,          0.0,          5.0, UserParam_Unit_VoltPerCell, UserParam_Operator_No      ,      1,      1 },
   { ID(UserParam_Name01), sizeof(ID(UserParam_Name01)),  25.0 PERCE,    0.0 PERCE,  400.0 PERCE, UserParam_Unit_Capacity   , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name02), sizeof(ID(UserParam_Name02)),   0.0 MINUT,    0.0 MINUT,  600.0 MINUT, UserParam_Unit_Minute     , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name03), sizeof(ID(UserParam_Name03)),  20.0 PERCE,    0.0 PERCE,  400.0 PERCE, UserParam_Unit_Capacity   , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name04), sizeof(ID(UserParam_Name04)),        2.40,          0.0,          5.0, UserParam_Unit_VoltPerCell, UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name05), sizeof(ID(UserParam_Name05)), 120.0 MINUT,    0.0 MINUT,  600.0 MINUT, UserParam_Unit_Minute     , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name06), sizeof(ID(UserParam_Name06)),   5.0 PERCE,    0.0 PERCE,  400.0 PERCE, UserParam_Unit_Capacity   , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name07), sizeof(ID(UserParam_Name07)),        2.80,          0.0,          5.0, UserParam_Unit_VoltPerCell, UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name08), sizeof(ID(UserParam_Name08)),   5.0 PERCE,    0.0 PERCE,  400.0 PERCE, UserParam_Unit_Capacity   , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name09), sizeof(ID(UserParam_Name09)),  30.0 MINUT,    0.0 MINUT,  600.0 MINUT, UserParam_Unit_Minute     , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name10), sizeof(ID(UserParam_Name10)),        1.15,         1.00,         2.00, UserParam_Unit_Ah         , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name11), sizeof(ID(UserParam_Name11)),        2.17,          0.0,          5.0, UserParam_Unit_VoltPerCell, UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name12), sizeof(ID(UserParam_Name12)),        2.80,          0.0,          5.0, UserParam_Unit_VoltPerCell, UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name13), sizeof(ID(UserParam_Name13)),   5.0 PERCE,    0.0 PERCE,  400.0 PERCE, UserParam_Unit_Capacity   , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name14), sizeof(ID(UserParam_Name14)),   2.0 MINUT,    0.0 MINUT,  600.0 MINUT, UserParam_Unit_Minute     , UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name15), sizeof(ID(UserParam_Name15)),  30.0 MINUT,    0.0 MINUT,  600.0 MINUT, UserParam_Unit_Minute     , UserParam_Operator_No      ,      0,      1 }
};


// Mandatory location
const Mandatory_ConstPar_Type ID(Mandatory_ConstPar) = {
  { (uint32) ID(Power), sizeof(ID(Power))/sizeof(Power_type) },
  { (uint32) ID(Uaccuracy), sizeof(ID(Uaccuracy))/sizeof(Uaccuracy_type) },
  { (uint32) ID(Iaccuracy), sizeof(ID(Iaccuracy))/sizeof(Iaccuracy_type) },
    (uint32) &ID(IOsignals),
  { (uint32) ID(UserParam_ConstParam), sizeof(ID(UserParam_ConstParam))/sizeof(UserParam_ConstParam_Type) },
};


// Module
// ------
#define I47_FVLA_ChargingStage_Idle  0
#define I47_FVLA_ChargingStage_Pre  1
#define I47_FVLA_ChargingStage_I1  2
#define I47_FVLA_ChargingStage_U1  3
#define I47_FVLA_ChargingStage_Completion  4
#define I47_FVLA_ChargingStage_Trickle  5
#define I47_FVLA_ChargingStage_Equalize  6
#define I47_FVLA_ChargingStage_I0  7
#define I47_FVLA_ChargingStage_spare3  8
#define I47_FVLA_ChargingStage_EquMan  9
const ChargingStage_Param_type I47_FVLA_ChargingStage[] = {
// Phase   Next Phase  Stage                                     
// ------  ----------  --------------------------------
  {    10,         11, I47_FVLA_ChargingStage_Pre },
  {    20,         21, I47_FVLA_ChargingStage_I1 },
  {    30,         31, I47_FVLA_ChargingStage_U1 },
  {    40,         41, I47_FVLA_ChargingStage_Completion },
  {    50,         51, I47_FVLA_ChargingStage_Trickle },
  {    70,         71, I47_FVLA_ChargingStage_I0 },
  {    90,         91, I47_FVLA_ChargingStage_EquMan }
};

const Delta_ConstParam_Type ID(Delta0[]) = {
//  Phase  Continou   Init       UserParam Period    Period     
//  -----  --------   --------   -----------------   -----------
  {    30, 0        | EXE_INIT | 0                ,     30 MINUT },
  {    31, 0        | 0        | 0                ,     30 MINUT },
  {    32, 0        | 0        | 0                ,     30 MINUT }
};

const Output_ConstParam_Type ID(Output[]) = {
// Phase   Continou   Init       Check      Led Green              Led Yellow              Led Red              Discrete Out1          Water Fill            Air Pump             IOmodule spare 0          IOmodule spare 1       
// ------  --------   --------   ---------  --------------------   ---------------------   ------------------   --------------------   -------------------   ------------------   -----------------------   -----------------------
  {     1, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    10, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    20, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    30, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    40, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    50, 0        | EXE_INIT | 0        , OUTPUT_GREEN_ON      | OUTPUT_YELLOW_OFF     | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    70, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    90, 0        | EXE_INIT | 0        , OUTPUT_GREEN_ON      | OUTPUT_YELLOW_TOGGLE1 | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    93, 0        | EXE_INIT | 0        , OUTPUT_GREEN_ON      | OUTPUT_YELLOW_TOGGLE1 | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_ON      | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    94, 0        | EXE_INIT | 0        , OUTPUT_GREEN_ON      | OUTPUT_YELLOW_TOGGLE1 | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   100, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_ON      | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   101, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_ON      | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   102, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_ON      | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   103, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_ON      | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   104, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_TOGGLE1 | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    }
};

const ChargingInfo_Param_type ID(ChargingInfo[]) = {
// Phase   Continou   Init       Check       Battery Connected     Pre Charging   Main Charging     Completion       Trickle          Equalization     Ready            Temper. Low      Temper. High
// ------  --------   --------   ---------   -------------------   -------------  ---------------   --------------   --------------   --------------   --------------   --------------   --------------
  {     1, 0        | EXE_INIT | 0         | 0                   | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {    10, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | CHRG_INFO_PRE | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {    20, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | CHRG_INFO_MAIN | 0              | 0              | 0              | 0              | 0              | 0              },
  {    30, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | CHRG_INFO_MAIN | 0              | 0              | 0              | 0              | 0              | 0              },
  {    40, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | CHRG_INFO_COMP | 0              | 0              | 0              | 0              | 0              },
  {    50, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | CHRG_INFO_TRIC | 0              | CHRG_INFO_REDY | 0              | 0              },
  {    70, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | CHRG_INFO_MAIN | 0              | 0              | 0              | 0              | 0              | 0              },
  {    90, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | CHRG_INFO_EQUA | 0              | 0              | 0              },
  {   100, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {   101, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {   102, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {   103, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {   104, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              }
};

const ChargingError_Param_type ID(ChargingError[]) = {
// Phase   Continou   Init       Check       Voltage Low      Voltage High     Tbatt Low        Tbastt High      Ah Max           Ph Max           delta U        delta I        delta P        delta Tbatt      Time Max
// ------  --------   --------   ---------   --------------   --------------   --------------   --------------   --------------   --------------   ------------   ------------   ------------   --------------   -------------
  {     1, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    10, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    20, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    30, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    40, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    50, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    70, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    90, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {   100, 0        | EXE_INIT | 0         | CHRG_ERRO_VOLO | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {   101, 0        | EXE_INIT | 0         | 0              | CHRG_ERRO_VOHI | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {   102, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | CHRG_ERRO_AHMA | 0              | 0            | 0            | 0            | 0              | 0              },
  {   103, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | CHRG_ERRO_TM   },
  {   104, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | CHRG_ERRO_DI | 0            | 0              | 0              }
};

const RegWin_ConstParam_Type ID(RegWin[]) = {
// Phase   Continou   UserParam Uerr     UserParam Uerr     UserParam Uerr     UerrWin       IerrWin       PerrWin
// ------  --------   ----------------   ----------------   ----------------   ------------  ------------  ------------
  {     1, EXE_CONT | 0                | 0                | 0                ,     10 MILLI,   0.05 PERCE,      1 PERCE }
};

const ReguOnOff_ConstParam_Type ID(ReguOn[]) = {
// Phase   Continou   UserParam Ubatt Low        UserParam Ubatt High        Condition Ubatt               Condition Remote In               Condition Cai In             Ubatt Low     Ubatt High
// ------  --------   ------------------------   -------------------------   ---------------------------   -------------------------------   ---------------------------  ------------  ------------
  {     1, EXE_CONT | 0                        | 0                         | REGUONOFF_COND_UACT_TRUE    | REGUONOFF_COND_REMOTEIN_NOTUSED | REGUONOFF_COND_CAIN_NOTUSED,         1.05,         2.65 }
};

const ReguOnOff_ConstParam_Type ID(ReguOff[]) = {
// Phase   Continou   UserParam Ubatt Low        UserParam Ubatt High        Condition Ubatt               Condition Remote In               Condition Cai In             Ubatt Low     Ubatt High
// ------  --------   ------------------------   -------------------------   ---------------------------   -------------------------------   ---------------------------  ------------  ------------
  {     1, EXE_CONT | 0                        | 0                         | REGUONOFF_COND_UACT_FALSE   | REGUONOFF_COND_REMOTEIN_NOTUSED | REGUONOFF_COND_CAIN_NOTUSED,          1.0,          2.9 }
};

const RegSet_ConstParam_Type ID(RegSet_ConstParam[]) = {
//  Phase  Continou   Uset UserPar.   Iset UserPar.   Pset UserPar.   Mode               Uset Active    Iset Active    Pset Active   Uset Value     Iset Value    Pset Value  
//  -----  --------   -------------   -------------   -------------   ----------------   ------------   ------------   ------------  -------------  ------------  ------------
  {     1, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.4,      0 PERCE,            0 },
  {    11, 0        | 0             | 0             | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,           2.4,     10 PERCE,            0 },
  {    12, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.4,      0 PERCE,            0 },
  {    21, EXE_CONT | REGSET_U_USER | REGSET_I_USER | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,             4,            3,            0 },
  {    31, EXE_CONT | REGSET_U_USER | REGSET_I_USER | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,             4,            3,            0 },
  {    41, EXE_CONT | REGSET_U_USER | REGSET_I_USER | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,             7,            8,            0 },
  {    51, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {    52, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {    53, 0        | REGSET_U_USER | REGSET_I_USER | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,            12,           13,            0 },
  {    71, EXE_CONT | REGSET_U_USER | REGSET_I_USER | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,             0,            1,            0 },
  {    91, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {    92, 0        | REGSET_U_USER | REGSET_I_USER | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,             7,            8,            0 },
  {    93, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {    94, 0        | REGSET_U_USER | REGSET_I_USER | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,             7,            8,            0 },
  {   100, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {   101, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {   102, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {   103, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 },
  {   104, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,           2.8,      0 PERCE,            0 }
};

const UbattWinBreak_ConstParam_Type ID(UbattWinBreak0[]) = {
// Phase   Continou   Init       Check       Userparam Ulow       Userparam Ulow        Check UbattWinn            Check UerrWin          Check IerrWin          Check PerrWin          Branch Act   Ulow         Uhigh         BranchPhase
// ------  --------   --------   ---------   ------------------   -------------------   ------------------------   --------------------   --------------------   --------------------   -----------  -----------  ------------  ------     
  {     1, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.4,          1.8,     10 }
};

const UbattWinBreak_ConstParam_Type ID(UbattWinBreak1[]) = {
// Phase   Continou   Init       Check       Userparam Ulow       Userparam Ulow        Check UbattWinn            Check UerrWin          Check IerrWin          Check PerrWin          Branch Act   Ulow         Uhigh         BranchPhase
// ------  --------   --------   ---------   ------------------   -------------------   ------------------------   --------------------   --------------------   --------------------   -----------  -----------  ------------  ------     
  {     1, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.8,          2.5,     70 }
};

const UbattWinBreak_ConstParam_Type ID(UbattWinBreak2[]) = {
// Phase   Continou   Init       Check       Userparam Ulow       Userparam Ulow        Check UbattWinn            Check UerrWin          Check IerrWin          Check PerrWin          Branch Act   Ulow         Uhigh         BranchPhase
// ------  --------   --------   ---------   ------------------   -------------------   ------------------------   --------------------   --------------------   --------------------   -----------  -----------  ------------  ------     
  {     1, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.0,          1.4,    100 }
};

const UbattBreak_ConstParam_Type ID(UbattBreak0[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparat   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   --------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {     1, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          2.5,    101 },
  {    11, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.8,     70 },
  {    20, 0        | EXE_INIT | 0         | UBB_USER_PARAM1 | CMP_GR   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_EGAL | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            4,     30 },
  {    21, 0        | 0        | EXE_CHECK | UBB_USER_PARAM1 | CMP_GREQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_EGAL | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            4,     30 },
  {    52, 0        | EXE_INIT | EXE_CHECK | UBB_USER_PARAM1 | CMP_LE   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,           11,     53 },
  {    70, 0        | EXE_INIT | 0         | UBB_USER_PARAM1 | CMP_GR   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_EGAL | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            0,     20 },
  {    71, 0        | 0        | EXE_CHECK | UBB_USER_PARAM1 | CMP_GREQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_EGAL | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            0,     20 },
  {   100, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.4,      1 },
  {   101, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_LE   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          2.5,      1 }
};

const Counter_type ID(Counter[]) = {
//                    **** Timer0 *****   **** Timer1 *****   **** Timer2 *****   ****** Ah0 ******   ****** Ah1 ******   ****** Wh0 ******   ****** Wh1 ******
//  Phase  Continou   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *
//  -----  --------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------
  {     1, 0        | TI0_RES | 0       | TI1_RES | 0       | TI2_RES | 0       | AH0_RES | 0       | AH1_RES | 0       | WH0_RES | 0       | WH1_RES | 0       },
  {    10, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       },
  {    20, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       },
  {    30, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       },
  {    41, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       },
  {    51, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       },
  {    70, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | AH0_RES | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       },
  {    91, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       },
  {   104, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       }
};

const TimerBreak_ConstParam_Type ID(Timer0Break0[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparato   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   ---------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    10, 0        | EXE_INIT | 0         | 0               | CMP_GR   | TIB_P0_TIM0 | OP_NO  | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       1 HOUR,    103 },
  {    11, 0        | 0        | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_NO  | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       1 HOUR,    103 },
  {    20, 0        | EXE_INIT | 0         | TBB_USER_PARAM1 | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            5,     32 },
  {    31, 0        | 0        | EXE_CHECK | TBB_USER_PARAM1 | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            5,     32 },
  {    40, 0        | EXE_INIT | 0         | TBB_USER_PARAM1 | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            9,     42 },
  {    41, 0        | 0        | EXE_CHECK | TBB_USER_PARAM1 | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            9,     42 },
  {    51, 0        | EXE_INIT | EXE_CHECK | TBB_USER_PARAM1 | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,           15,     52 },
  {    53, 0        | EXE_INIT | EXE_CHECK | TBB_USER_PARAM1 | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,           14,     51 },
  {    71, 0        | EXE_INIT | EXE_CHECK | TBB_USER_PARAM1 | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            2,     20 },
  {    91, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            1,     92 },
  {    92, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       2 HOUR,     93 },
  {    93, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,     10 MINUT,     94 },
  {    94, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       1 HOUR,     50 },
  {   104, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            1,     40 }
};

const TimerBreak_ConstParam_Type ID(Timer0Break1[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparato   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   ---------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    11, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      5 MINUT,     12 },
  {    12, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      1 MINUT,     11 },
  {    30, 0        | EXE_INIT | 0         | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       5 HOUR,     40 },
  {    32, 0        | 0        | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       5 HOUR,     40 },
  {    40, 0        | EXE_INIT | 0         | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       5 HOUR,     49 },
  {    42, 0        | 0        | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       5 HOUR,     49 }
};

const AhBreak_ConstParam_Type ID(Ah0Break0[]) = {
// Phase   Continou   Init       UserParam Par1    Check       UserParam Par1   Comparato   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------------   ---------   ---------------  ---------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    21, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | AHB_P0_CAPA | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    100 PERCE,    102 },
  {    31, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | AHB_P0_CAPA | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    120 PERCE,    102 },
  {    32, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | AHB_P0_CAPA | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    120 PERCE,    102 },
  {    40, 0        | EXE_INIT | 0         | AHB_USER_PARAM1 | CMP_GREQ | AHB_P0_AH0  | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,           10,     49 },
  {    42, 0        | 0        | EXE_CHECK | AHB_USER_PARAM1 | CMP_GREQ | AHB_P0_AH0  | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,           10,     49 },
  {    71, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | AHB_P0_CAPA | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    100 PERCE,    102 }
};

const IbattBreak_ConstParam_Type ID(IbattBreak0[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparat   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   --------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    30, 0        | EXE_INIT | 0         | IBB_USER_PARAM1 | CMP_LE   | IBB_P0_CAPA | OP_MUL | IBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            6,     40 },
  {    32, 0        | 0        | EXE_CHECK | IBB_USER_PARAM1 | CMP_LE   | IBB_P0_CAPA | OP_MUL | IBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,            6,     40 }
};

const IbattBreak_ConstParam_Type ID(IbattDelta0Break0[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparat   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   --------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    31, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | IBB_P0_CAPA | OP_MUL | IBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,   0.25 PERCE,    104 },
  {    32, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | IBB_P0_CAPA | OP_MUL | IBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,   0.25 PERCE,    104 }
};

const InputBreak_ConstParam_Type ID(InputBreak0[]) = {
// Phase   Continou   Init       Check       Check Stop             Check F1             Check F2             Check Remote In           Check Water Low             Check CaiIn             Check ManEquOn             Check ManEquOff           Check IO spare 3           Check IO spare 4           Branch Act   BranchPhase
// ------  --------   --------   ---------   --------------------   ------------------   ------------------   -----------------------   -------------------------   ---------------------   ------------------------   ------------------------   ------------------------   ------------------------   -----------  ------     
  {     1, 0        | 0        | EXE_CHECK | INPU_CHECK_STOP_EGAL | INPU_CHECK_F1_EGAL | INPU_CHECK_F2_EGAL | INPU_CHECK_REMOTEIN_EGAL | INPU_CHECK_WATERLOW_EGAL | INPU_CHECK_CAIIN_EGAL | INPU_CHECK_MANEQUON_EGAL | INPU_CHECK_MANEQUOFF_EGAL | INPU_CHECK_IOSPARE3_EGAL | INPU_CHECK_IOSPARE4_EGAL | 0         ,     90 },
  {    49, 0        | 0        | EXE_CHECK | INPU_CHECK_STOP_EGAL | INPU_CHECK_F1_EGAL | INPU_CHECK_F2_EGAL | INPU_CHECK_REMOTEIN_EGAL | INPU_CHECK_WATERLOW_EGAL | INPU_CHECK_CAIIN_EGAL | INPU_CHECK_MANEQUON_TRUE | INPU_CHECK_MANEQUOFF_EGAL | INPU_CHECK_IOSPARE3_EGAL | INPU_CHECK_IOSPARE4_EGAL | EXE_BRANCH,     90 },
  {    50, EXE_CONT | 0        | EXE_CHECK | INPU_CHECK_STOP_EGAL | INPU_CHECK_F1_EGAL | INPU_CHECK_F2_EGAL | INPU_CHECK_REMOTEIN_EGAL | INPU_CHECK_WATERLOW_EGAL | INPU_CHECK_CAIIN_EGAL | INPU_CHECK_MANEQUON_TRUE | INPU_CHECK_MANEQUOFF_EGAL | INPU_CHECK_IOSPARE3_EGAL | INPU_CHECK_IOSPARE4_EGAL | EXE_BRANCH,     90 },
  {    91, EXE_CONT | 0        | EXE_CHECK | INPU_CHECK_STOP_EGAL | INPU_CHECK_F1_EGAL | INPU_CHECK_F2_EGAL | INPU_CHECK_REMOTEIN_EGAL | INPU_CHECK_WATERLOW_EGAL | INPU_CHECK_CAIIN_EGAL | INPU_CHECK_MANEQUON_EGAL | INPU_CHECK_MANEQUOFF_TRUE | INPU_CHECK_IOSPARE3_EGAL | INPU_CHECK_IOSPARE4_EGAL | EXE_BRANCH,     50 }
};

const InputBreak_ConstParam_Type ID(InputBreak1[]) = {
// Phase   Continou   Init       Check       Check Stop             Check F1             Check F2             Check Remote In           Check Water Low             Check CaiIn             Check ManEquOn             Check ManEquOff           Check IO spare 3           Check IO spare 4           Branch Act   BranchPhase
// ------  --------   --------   ---------   --------------------   ------------------   ------------------   -----------------------   -------------------------   ---------------------   ------------------------   ------------------------   ------------------------   ------------------------   -----------  ------     
  {     1, 0        | 0        | EXE_CHECK | INPU_CHECK_STOP_EGAL | INPU_CHECK_F1_EGAL | INPU_CHECK_F2_EGAL | INPU_CHECK_REMOTEIN_EGAL | INPU_CHECK_WATERLOW_EGAL | INPU_CHECK_CAIIN_EGAL | INPU_CHECK_MANEQUON_EGAL | INPU_CHECK_MANEQUOFF_EGAL | INPU_CHECK_IOSPARE3_EGAL | INPU_CHECK_IOSPARE4_EGAL | 0         ,     90 },
  {    49, 0        | 0        | EXE_CHECK | INPU_CHECK_STOP_EGAL | INPU_CHECK_F1_EGAL | INPU_CHECK_F2_EGAL | INPU_CHECK_REMOTEIN_EGAL | INPU_CHECK_WATERLOW_EGAL | INPU_CHECK_CAIIN_EGAL | INPU_CHECK_MANEQUON_FALS | INPU_CHECK_MANEQUOFF_EGAL | INPU_CHECK_IOSPARE3_EGAL | INPU_CHECK_IOSPARE4_EGAL | EXE_BRANCH,     50 }
};

const UcompTbatt_ConstParam_Type ID(UcompTbatt[]) = {
//  Phase  Continou  UserPararam Low        UserParam Neutral         UserParam High         UserParam Slope        Low           Neutral       High          Slope
//  -----  --------  --------------------   -----------------------   --------------------   ---------------------  ------------  ------------  ------------  ------------
  {     1, EXE_CONT | 0                   | 0                       | 0                    | 0                    ,          -10,           30,           50,  3.000 MILLI }
};

const IcompTbatt_ConstParam_Type ID(IcompTbatt[]) = {
// Phase   Continou   UserParam Cold.Low        UserParam Cold.High        UserParam Warm.Low        UserParam Warm.High      Cold Low      Cold High    Warm Low       Warm High
// ------  --------   -----------------------   ------------------------   -----------------------   ------------------------ ------------  ------------  ------------  ------------
  {     1, EXE_CONT | 0                       | 0                        | 0                       | 0                       ,          -35,          -30,           45,           60 }
};

// Module location
// Must match idModule_enum
const Module_ConstPar_Type ID(Module_ConstPar[idModule_length]) = {
  { (void*) ID(RegSet_ConstParam), sizeof(ID(RegSet_ConstParam))/sizeof(RegSet_ConstParam_Type) },
  { (void*) ID(RegWin), sizeof(ID(RegWin))/sizeof(RegWin_ConstParam_Type) },
  { (void*) ID(ReguOn), sizeof(ID(ReguOn))/sizeof(ReguOnOff_ConstParam_Type) },
  { (void*) ID(ReguOff), sizeof(ID(ReguOff))/sizeof(ReguOnOff_ConstParam_Type) },
  { (void*) ID(UcompTbatt), sizeof(ID(UcompTbatt))/sizeof(UcompTbatt_ConstParam_Type) },
  { (void*) ID(IcompTbatt), sizeof(ID(IcompTbatt))/sizeof(IcompTbatt_ConstParam_Type) },
  { (void*) ID(UbattWinBreak0), sizeof(ID(UbattWinBreak0))/sizeof(UbattWinBreak_ConstParam_Type) },
  { (void*) ID(UbattWinBreak1), sizeof(ID(UbattWinBreak1))/sizeof(UbattWinBreak_ConstParam_Type) },
  { (void*) ID(UbattWinBreak2), sizeof(ID(UbattWinBreak2))/sizeof(UbattWinBreak_ConstParam_Type) },
  { 0, 0 }, // idUbattWinBreak3, not defined
  { (void*) ID(UbattBreak0), sizeof(ID(UbattBreak0))/sizeof(UbattBreak_ConstParam_Type) },
  { 0, 0 }, // idUbattBreak1, not defined
  { 0, 0 }, // idUbattBreak2, not defined
  { 0, 0 }, // idUbattBreak3, not defined
  { 0, 0 }, // idUbattDelta0Break0, not defined
  { 0, 0 }, // idUbattDelta0Break1, not defined
  { 0, 0 }, // idUbattDelta1Break0, not defined
  { 0, 0 }, // idUbattDelta1Break1, not defined
  { (void*) ID(IbattBreak0), sizeof(ID(IbattBreak0))/sizeof(IbattBreak_ConstParam_Type) },
  { 0, 0 }, // idIbattBreak1, not defined
  { 0, 0 }, // idIbattBreak2, not defined
  { 0, 0 }, // idIbattBreak3, not defined
  { (void*) ID(IbattDelta0Break0), sizeof(ID(IbattDelta0Break0))/sizeof(IbattBreak_ConstParam_Type) },
  { 0, 0 }, // idIbattDelta0Break1, not defined
  { 0, 0 }, // idIbattDelta1Break0, not defined
  { 0, 0 }, // idIbattDelta1Break1, not defined
  { 0, 0 }, // idPbattBreak0, not defined
  { 0, 0 }, // idPbattBreak1, not defined
  { 0, 0 }, // idPbattBreak2, not defined
  { 0, 0 }, // idPbattBreak3, not defined
  { 0, 0 }, // idPbattDelta0Break0, not defined
  { 0, 0 }, // idPbattDelta0Break1, not defined
  { 0, 0 }, // idPbattDelta1Break0, not defined
  { 0, 0 }, // idPbattDelta1Break1, not defined
  { 0, 0 }, // idTbattBreak0, not defined
  { 0, 0 }, // idTbattBreak1, not defined
  { 0, 0 }, // idTbattBreak2, not defined
  { 0, 0 }, // idTbattBreak3, not defined
  { 0, 0 }, // idTbattDelta0Break0, not defined
  { 0, 0 }, // idTbattDelta0Break1, not defined
  { 0, 0 }, // idTbattDelta1Break0, not defined
  { 0, 0 }, // idTbattDelta1Break1, not defined
  { 0, 0 }, // idReguModeBreak0, not defined
  { 0, 0 }, // idReguModeBreak0, not defined
  { 0, 0 }, // idReguDerateBreak0, not defined
  { 0, 0 }, // idReguDerateBreak1, not defined
  { 0, 0 }, // idReguEngineBreak0, not defined
  { 0, 0 }, // idReguEngineBreak1, not defined
  { (void*) ID(InputBreak0), sizeof(ID(InputBreak0))/sizeof(InputBreak_ConstParam_Type) },
  { (void*) ID(InputBreak1), sizeof(ID(InputBreak1))/sizeof(InputBreak_ConstParam_Type) },
  { (void*) ID(Timer0Break0), sizeof(ID(Timer0Break0))/sizeof(TimerBreak_ConstParam_Type) },
  { (void*) ID(Timer0Break1), sizeof(ID(Timer0Break1))/sizeof(TimerBreak_ConstParam_Type) },
  { 0, 0 }, // idTimer1Break0, not defined
  { 0, 0 }, // idTimer1Break1, not defined
  { 0, 0 }, // idTimer2Break0, not defined
  { 0, 0 }, // idTimer2Break1, not defined
  { (void*) ID(Ah0Break0), sizeof(ID(Ah0Break0))/sizeof(AhBreak_ConstParam_Type) },
  { 0, 0 }, // idAh0Break1, not defined
  { 0, 0 }, // idAh1Break0, not defined
  { 0, 0 }, // idAh1Break1, not defined
  { 0, 0 }, // idWh0Break0, not defined
  { 0, 0 }, // idWh0Break1, not defined
  { 0, 0 }, // idWh1Break0, not defined
  { 0, 0 }, // idWh1Break1, not defined
  { 0, 0 }, // idAndBreak0, not defined
  { 0, 0 }, // idAndBreak1, not defined
  { (void*) ID(Counter), sizeof(ID(Counter))/sizeof(Counter_type) },
  { (void*) ID(Delta0), sizeof(ID(Delta0))/sizeof(Delta_ConstParam_Type) },
  { 0, 0 }, // idDelta1, not defined
  { (void*) ID(Output), sizeof(ID(Output))/sizeof(Output_ConstParam_Type) },
  { (void*) ID(ChargingStage), sizeof(ID(ChargingStage))/sizeof(ChargingStage_Param_type) },
  { (void*) ID(ChargingInfo), sizeof(ID(ChargingInfo))/sizeof(ChargingInfo_Param_type) },
  { (void*) ID(ChargingError), sizeof(ID(ChargingError))/sizeof(ChargingError_Param_type) }
};

