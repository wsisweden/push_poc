/*
 $Revision: 156 $
 $Date: 2010-05-17 17:04:01 +0200 (må, 17 maj 2010) $
 $Author: bsi $
 $HeadURL: https://subversion.mpi.local/svn/micropower/_user/bsi/alg/caimain.c $
 */

/**************************************************************************
 Project:     ACCESS:CM
 Module:      caimain.c
 Contains:    - CaiInit. Initialize pointers to Algorithm-package
              - CaiMain. Schedule the state.
 Environment: Compiled and tested with GNU GCC.
 Copyright:   Micropower ED
 Version:
 ---------------------------------------------------------------------------
 History:
 Rev      Date      Sign  Description
 bsi   First version.

 ***************************************************************************/
#include <float.h>
#include "cai_constparam.h"
#include "caimain.h"
#include "math.h"

/**************************************************************************
 Local Definition
 ***************************************************************************/
// ------- Shall be static -------
uint16 Phase = 0;
AlgDef_ConstPar_Type AlgDef;
Identity_ConstPar_Type Identity;
Mandatory_ConstPar_Type Mandatory;
Module_type Module[idModule_length];

CounterValue_type CounterValue;

#define CAISTATUS_USERPARAM_OK   0x1  // UserParam checked and ok.
uint32 CaiStatus = CAISTATUS_USERPARAM_OK;

typedef struct {
	struct {
		float UsetV;
		float IsetA;
		float PsetW;
	} RegSet;
} CaiControl_Type;
CaiControl_Type CaiControl;

DeltaValue_type DeltaValue[2];

typedef struct {
  float Low;
  float Neutral;
  float High;
  float Slope;
} UcompTbatt_Type;
UcompTbatt_Type UcompTbatt;

typedef struct {
	float ColdLow;
	float ColdHigh;
	float WarmLow;
	float WarmHigh;
} IcompTbatt_Type;
IcompTbatt_Type IcompTbatt;

#if (IDMODULE_LENGTH == 73)
#define UBATTWINBREAK_LENGTH  (\
		sizeof(idUbattWinBreak0) + \
		sizeof(idUbattWinBreak1) + \
		sizeof(idUbattWinBreak2) + \
		sizeof(idUbattWinBreak3) \
		) / sizeof(idModule_length)
UbattWinBreak_Type UbattWinBreak[UBATTWINBREAK_LENGTH] = {
		{idUbattWinBreak0, 0, 0},
		{idUbattWinBreak1, 0, 0},
		{idUbattWinBreak2, 0, 0},
		{idUbattWinBreak3, 0, 0}
};
#else
#error *** BEWARE NEW MODULE ***
#error A new module is added in idModule_enum (filename: cai_idmodule)
#error If it a UbattWinBreakX module, modify UBATTWINBREAK_SIZE and UbattWinBreak
#error and type in the correct numbers of IDMODULE_SIZE.
#endif




Error_enum Error = Error_No;
/**************************************************************************
 External References
 ***************************************************************************/

/**************************************************************************
 Public Definitions
 ***************************************************************************/
uint32 CaiInit_Cnt = 0;
uint32 Cai_Tick = 0;

/**************************************************************************
 Local Function Prototypes
 ***************************************************************************/
static void Compensate(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p);
static void Delta(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p);
static void Set(const CaiInput_type *Input_p, CaiOutput_type *Output_p);
static void Counter(CaiOutput_type *CaiOutput_p);

/**************************************************************************
 Public Functions
 ***************************************************************************/

/**************************************************************************
 Name:		CaiInit
 Does:		Initialize pointers to Algorithm-package.
            Because the algorithm parameters can be compiled outside
            this project.
            The values in the struct AlgDef_ConstPar_Type contain the
            address when the algorithm was compiled.
 Returns:
 **************************************************************************/
void CaiInit(const CaiInput_type *CaiInput_p) {
	AlgDef_ConstPar_Type *AlgDef_InMem_p;					// Used to calculate real address
	Identity_ConstPar_Type *IdentityLoc_p;
	Mandatory_ConstPar_Type *MandatoryLoc_p;
	Module_ConstPar_Type *Module_InMem_p;
	idModule_enum id;

	CaiInit_Cnt++; // DEBUG

	// Initialize AlgDef
	AlgDef_InMem_p = (AlgDef_ConstPar_Type*)(CaiInput_p->Algorithm.AlgDef_p);
	AlgDef.Identity_ConstPar_Addr = (uint32) ((uint32) AlgDef_InMem_p +
			(AlgDef_InMem_p->Identity_ConstPar_Addr - AlgDef_InMem_p->AlgDef_ConstPar_Addr));
	AlgDef.Mandatory_ConstPar_Addr = (uint32) ((uint32) AlgDef_InMem_p +
			(AlgDef_InMem_p->Mandatory_ConstPar_Addr - AlgDef_InMem_p->AlgDef_ConstPar_Addr));
	AlgDef.Module_ConstPar_Addr = (uint32) ((uint32) AlgDef_InMem_p +
			(AlgDef_InMem_p->Module_ConstPar_Addr - AlgDef_InMem_p->AlgDef_ConstPar_Addr));

	// Initialize Identity
	IdentityLoc_p = (Identity_ConstPar_Type*) AlgDef.Identity_ConstPar_Addr;
	Identity.IdNumber = IdentityLoc_p->IdNumber;
	Identity.Version = IdentityLoc_p->Version;
	Identity.InterpretVer = IdentityLoc_p->InterpretVer;
	Identity.Name.Addr = (uint32) AlgDef_InMem_p + (IdentityLoc_p->Name.Addr
			- AlgDef_InMem_p->AlgDef_ConstPar_Addr);
	Identity.Name.Size = IdentityLoc_p->Name.Size;

	// Initialize Mandatory
	MandatoryLoc_p = (Mandatory_ConstPar_Type*) AlgDef.Mandatory_ConstPar_Addr;
	Mandatory.Power.Addr = (uint32) AlgDef_InMem_p + (MandatoryLoc_p->Power.Addr
			- AlgDef_InMem_p->AlgDef_ConstPar_Addr);
	Mandatory.Power.Points = MandatoryLoc_p->Power.Points;
	Mandatory.Uaccuracy.Addr = (uint32) AlgDef_InMem_p
			+ (MandatoryLoc_p->Uaccuracy.Addr - AlgDef_InMem_p->AlgDef_ConstPar_Addr);
	Mandatory.Uaccuracy.Points = MandatoryLoc_p->Uaccuracy.Points;
	Mandatory.Iaccuracy.Addr = (uint32) AlgDef_InMem_p
			+ (MandatoryLoc_p->Iaccuracy.Addr - AlgDef_InMem_p->AlgDef_ConstPar_Addr);
	Mandatory.Iaccuracy.Points = MandatoryLoc_p->Iaccuracy.Points;
	Mandatory.IOsignals_Addr = (uint32) AlgDef_InMem_p
			+ (MandatoryLoc_p->IOsignals_Addr - AlgDef_InMem_p->AlgDef_ConstPar_Addr);
	Mandatory.UserParam.Addr = (uint32) AlgDef_InMem_p
			+ (MandatoryLoc_p->UserParam.Addr - AlgDef_InMem_p->AlgDef_ConstPar_Addr);
	Mandatory.UserParam.Size = MandatoryLoc_p->UserParam.Size;

	// Initialize Module Location address Mandatory_ConstPar_Type
	Module_InMem_p = (Module_ConstPar_Type*) (AlgDef.Module_ConstPar_Addr);
	for (id = idRegSet; id < idModule_length; id++) {
		Module[id].LocAddr = ((uint32) AlgDef_InMem_p + ((uint32) Module_InMem_p->Addr
				- AlgDef_InMem_p->AlgDef_ConstPar_Addr));
		Module[id].Phases = Module_InMem_p->Phases;
		Module_InMem_p++;
	}

}
uint32 Dummy001 = 0;
/**************************************************************************
 Name:		CaiMain
 Does:		Main function for charging algorithm
 Returns:
 **************************************************************************/
void CaiMain(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p) {


	if (CaiInput_p->Control & CAI_INPUT_CONTROL_RESET){
		Phase = 0;
	}

	if (Cai_Tick >= 0) {
		if (Phase == 0){
			CaiOutput_p->ChalgToCc.ReguOn.Condition.Detail.Uact = BatterydetectCond_NotUsed;
			CaiInit(CaiInput_p);
			Phase = 1;
			PhaseInit(CaiInput_p, CaiOutput_p, Phase);
		}

		extern volatile int BatteryConnected; // sorab - kalmar / exide
		extern int gRemoteRestricted; // sorab - kalmar / exide
		extern int gStartStopFunc;// sorab - kalmar / exide

		Compensate(CaiInput_p, CaiOutput_p);
		if(CaiInput_p->Control & CAI_INPUT_CONTROL_CHARG){
			Counter(CaiOutput_p);
			Delta(CaiInput_p, CaiOutput_p);
			Break(CaiInput_p, CaiOutput_p);
			Set(CaiInput_p, CaiOutput_p);
		}
		else if (BatteryConnected & gRemoteRestricted & gStartStopFunc & (Identity.IdNumber == 48)){ // sorab - kalmar / exide
		Break(CaiInput_p, CaiOutput_p); // sorab - kalmar / exide
		}// sorab - kalmar / exide
	}
	Cai_Tick++;

}


/**************************************************************************
 Name:		   CaiCheckUserParam
 Does:		   Check UserParam
 Precondition: CaiInit must have been executed before call of this
               function.
 Returns:
 **************************************************************************/
bool CaiCheckUserParam(const UserParam_Type *UserParam_p, UserParam_Error_Type *UserParam_Error_p) {
	uint8 Index;
	float Par;
	float ParA = 0;
	float ParB = 0;
	uint32 ValidMask;

	// Initialize
	UserParam_ConstParam_Type *UserParam_ConstParam_Start_p;  // First row in definition
	UserParam_ConstParam_Type *UserParam_ConstParam_p;

	UserParam_Error_p->Error = UserParam_Error_No;
	UserParam_Error_p->Param = 0;
	UserParam_ConstParam_Start_p = (UserParam_ConstParam_Type*) Mandatory.UserParam.Addr;
	UserParam_ConstParam_p = UserParam_ConstParam_Start_p;

	// First check if there are any Param defined in the algorithm
	if (Mandatory.UserParam.Size == 0) {
		UserParam_Error_p->Param = 0xFF;
		UserParam_Error_p->Error = UserParam_Error_NoParDefined;
		return FALSE;
	}


	// Check all parameters defined in algorithm
	// -----------------------------------------
	for (Index = 0; Index<Mandatory.UserParam.Size; Index++) {

		// Check if Parameter from user
		if (UserParam_p->Valid & ((uint32) 1)<<Index) {
			Par = UserParam_p->Value[Index];
		}
		else {
			// Not valid use default
			Par = UserParam_ConstParam_p->Default;
		}

		// Check min
		if ( UserParam_ConstParam_p->Min >= UserParam_p->Value[Index] ) {
			UserParam_Error_p->Error = UserParam_Error_Min;
			return FALSE;
		}
		// Check max
		if ( UserParam_ConstParam_p->Max <= UserParam_p->Value[Index] ) {
			UserParam_Error_p->Error = UserParam_Error_Max;
			return FALSE;
		}

		// Control
		// ParA
		if (UserParam_ConstParam_p->Operator == UserParam_Operator_Inside) {
			// Check that ParA index is defined in algorithm.
			if (UserParam_ConstParam_p->ParA >= Mandatory.UserParam.Size) {
				// This is a fatal error and should be controlled when creating the algorithm
				// Must exit this function due to risk with pointers.
				UserParam_Error_p->Error = UserParam_Error_Fatal_ParA_NotInAlg;
				return FALSE;
			}
			// Check if data from user i valid, if not use default value
			if ( UserParam_p->Valid & (((uint32) 1)<<UserParam_ConstParam_p->ParA) ) {
				ParA = UserParam_p->Value[UserParam_ConstParam_p->ParA];
			}
			else
				ParA = UserParam_ConstParam_Start_p[UserParam_ConstParam_p->ParA].Default;
		}
		// ParB
		if (UserParam_ConstParam_p->Operator != UserParam_Operator_No) {
			// Check that ParA index is defined in algorithm.
			if (UserParam_ConstParam_p->ParB >= Mandatory.UserParam.Size) {
				// This is a fatal error and should be controlled when creating the algorithm
				// Must exit this function due to risk with pointers.
				UserParam_Error_p->Error = UserParam_Error_Fatal_ParB_NotInAlg;
				return FALSE;
			}
			// Check if data from user i valid, if not use default value
			if ( UserParam_p->Valid & (((uint32) 1)<<UserParam_ConstParam_p->ParB) ) {
				ParB = UserParam_p->Value[UserParam_ConstParam_p->ParB];
			}
			else
				ParB = UserParam_ConstParam_Start_p[UserParam_ConstParam_p->ParB].Default;
		}
		// Control
		switch (UserParam_ConstParam_p->Operator) {
		case UserParam_Operator_No:
			// Do nothing
			break;
		case UserParam_Operator_Lesser:
			if (Par >= ParB) {
				UserParam_Error_p->Error = UserParam_Error_Min;
				return FALSE;
			}
			break;
		case UserParam_Operator_Greather:
			if (Par <= ParB) {
				UserParam_Error_p->Error = UserParam_Error_Min;
				return FALSE;
			}
			break;
		case UserParam_Operator_Inside:
			if ( (ParA >= Par) || (Par >= ParB) ) {
				UserParam_Error_p->Error = UserParam_Error_Min;
				return FALSE;
			}
			break;

		case UserParam_Operator_size:
		default:
			UserParam_Error_p->Error = UserParam_Error_Fatal_Operator;
			return FALSE;
			// break;
		}
		UserParam_ConstParam_p++;  // Point at next row
	} // for (Index = 0; Index<Mandatory.UserParam.Size; Index++) {

	// Check so there is no valid parameters from user which is not defined in algorithm
	// ---------------------------------------------------------------------------------
	if (Index < USERPARAM_VALUE_SIZE) {
		ValidMask = (0xFFFFFFFF<<Index);
		if (UserParam_p->Valid & ValidMask) {
			UserParam_Error_p->Error = UserParam_Error_Fatal_ToManyValidPar;
			return FALSE;
		}
	}


	return TRUE;
}

/**************************************************************************
 Name:		CaiCheckChargingParam
 Does:		Check
 Returns:
 **************************************************************************/
bool CaiCheckChargingParam(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p) {


	return TRUE;
}

/**************************************************************************
 Local Functions
 ***************************************************************************/
/**************************************************************************
 Name:		PhaseSearch
 Does:		Check if the phase number exist.
            The Phase is always located first and Ctrl located second in
            the module struct.
 Returns:   TRUE if phase found.
 **************************************************************************/
static bool PhaseSearch(idModule_enum id, uint16 PhaseSize, uint16 PhaseNo) {
	uint32 PhaseAddr;
	uint16 Phases;
	uint16 PhaseIndex = 0;
	bool Found = FALSE;
	Module_PhaseCtrl_Type *Module_PhaseCtrl_p;

	// Point at first phase in the module
	Phases = Module[id].Phases;
	Module_PhaseCtrl_p = (Module_PhaseCtrl_Type*) Module[id].LocAddr;
	PhaseAddr = (uint32) Module[id].LocAddr;

	while ((PhaseIndex < Phases) && !Found) {
		// Check if the phase i found
		if (Module_PhaseCtrl_p->Phase == PhaseNo) {
			Found = TRUE;
		} else {
			// Point at next phase
			PhaseAddr += PhaseSize;
			Module_PhaseCtrl_p = (Module_PhaseCtrl_Type*) PhaseAddr;
			PhaseIndex++;
		}
	}

	if (Found) {
		Module[id].PhaseAddr = PhaseAddr;
	}
	else {
		// If the recent phase was defined to 'Continue' then
		// use the definition if there is no definition for current phase.
		Module_PhaseCtrl_p = (Module_PhaseCtrl_Type*) Module[id].PhaseAddr;
		if(Module_PhaseCtrl_p){
			if (!(Module_PhaseCtrl_p->Ctrl & EXE_CONT)) {
				Module[id].PhaseAddr = 0;
			}
		}
		else
		{
			Module[id].PhaseAddr = 0;
		}
	}
	return Found;
}


/**************************************************************************
 Name:		PhaseSearchFunc
 Does:		Check if the phase number exist.
            The Phase is always located first and Ctrl located second in
            the module struct.
 Returns:   TRUE if phase found.
 **************************************************************************/
typedef enum {
	PhaseSearch_New,
	PhaseSearch_Continuous,
	PhaseSearch_NotDefined
} PhaseSearch_enum;
static PhaseSearch_enum PhaseSearchFunc(idModule_enum id, uint16 PhaseSize, uint16 PhaseNo) {
	uint32 PhaseAddr;
	uint16 Phases;
	uint16 PhaseIndex = 0;
	bool Found = FALSE;
	PhaseSearch_enum retValue;
	Module_PhaseCtrl_Type *Module_PhaseCtrl_p;

	// Point at first phase in the module
	Phases = Module[id].Phases;
	Module_PhaseCtrl_p = (Module_PhaseCtrl_Type*) Module[id].LocAddr;
	PhaseAddr = (uint32) Module[id].LocAddr;

	while ((PhaseIndex < Phases) && !Found) {
		// Check if the phase i found
		if (Module_PhaseCtrl_p->Phase == PhaseNo) {
			Found = TRUE;
		} else {
			// Point at next phase
			PhaseAddr += PhaseSize;
			Module_PhaseCtrl_p = (Module_PhaseCtrl_Type*) PhaseAddr;
			PhaseIndex++;
		}
	}

	if (Found) {
		Module[id].PhaseAddr = PhaseAddr;
		retValue = PhaseSearch_New;
	}
	else {
		// If the recent phase was defined to 'Continue' then
		// use the definition if there is no definition for current phase.
		if(Module[id].PhaseAddr)
		{
			Module_PhaseCtrl_p = (Module_PhaseCtrl_Type*) Module[id].PhaseAddr;
		}
		else
		{
			Module[id].PhaseAddr = 0;
			retValue = PhaseSearch_NotDefined;
			return retValue;
		}
		if ( Module_PhaseCtrl_p->Ctrl & EXE_CONT ) {
			retValue = PhaseSearch_Continuous;
		}
		else {
			Module[id].PhaseAddr = 0;
			retValue = PhaseSearch_NotDefined;
		}
	}
	return retValue;
}


/**************************************************************************
 Name:		Counter
 Does:		Handle timers and Ah counter
 Returns:
 **************************************************************************/
static void Counter(CaiOutput_type *CaiOutput_p) {
	Counter_type *Counter_p;

	Counter_p = (Counter_type*) Module[idCounter].PhaseAddr;
	if (Counter_p) {
		// Timer
		if (Counter_p->Ctrl & TI0_RUN)
			CounterValue.Timer[Timer0]++;
		if (Counter_p->Ctrl & TI1_RUN)
			CounterValue.Timer[Timer1]++;
		if (Counter_p->Ctrl & TI2_RUN)
			CounterValue.Timer[Timer2]++;

		// Ah
		if (Counter_p->Ctrl & AH0_RUN)
			CounterValue.AmpSecCnt[0] += CaiOutput_p->BatteryMeas.Ibatt;
		if (Counter_p->Ctrl & AH1_RUN)
			CounterValue.AmpSecCnt[1] += CaiOutput_p->BatteryMeas.Ibatt;

		// Wh
		if (Counter_p->Ctrl & WH0_RUN)
			CounterValue.WattSecCnt[0] += CaiOutput_p->BatteryMeas.Pbatt;
		if (Counter_p->Ctrl & WH1_RUN)
			CounterValue.WattSecCnt[1] += CaiOutput_p->BatteryMeas.Pbatt;

		if (Module[idChargingStage].Status & CHARGING_STAGE_CTRL_VALIDATA) {
			CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].AmpSecCnt += CaiOutput_p->BatteryMeas.Ibatt;
			CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].WattSecCnt += CaiOutput_p->BatteryMeas.Pbatt;
			CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Time++;
		}
	}

}

/**************************************************************************
 Name:		Set
 Does:		Calculate set values for regulator...
 Returns:
 **************************************************************************/
static void Set(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p) {
	RegSet_ConstParam_Type *RegSet_p;

	RegSet_p = (RegSet_ConstParam_Type*) Module[idRegSet].PhaseAddr;
	if (RegSet_p) {
		// Mode
		CaiOutput_p->ChalgToCc.Mode = (ReguMode_enum) ( (RegSet_p->Ctrl & REGSET_MODE_MASK) >> REGSET_MODE_LSB );
		// Uset
		if (RegSet_p->Ctrl & REGSET_U_ACT) {
			CaiOutput_p->ChalgToCc.Set.U = (RegSetParam.U
					* CaiInput_p->Algorithm.Cells) + CompValue.Uact.CableRes
					+ CompValue.Uact.Tbatt;
		} else {
			CaiOutput_p->ChalgToCc.Set.U = FLT_MAX;
		}
		// Iset
		if (RegSet_p->Ctrl & REGSET_I_ACT) {
			CaiOutput_p->ChalgToCc.Set.I = ((RegSetParam.I
					* CaiInput_p->Algorithm.Capacity) * CompValue.Iact.Tbatt)
					+ CaiInput_p->Algorithm.BaseLoad;
		} else {
			CaiOutput_p->ChalgToCc.Set.I = FLT_MAX;
		}
		// Pset
		if (RegSet_p->Ctrl & REGSET_P_ACT) {
			CaiOutput_p->ChalgToCc.Set.P = 123; // ----------- TODO ------------
		} else {
			CaiOutput_p->ChalgToCc.Set.P = FLT_MAX;
		}

	}
	else {
		CaiOutput_p->ChalgToCc.Mode = ReguMode_Off;
		CaiOutput_p->ChalgToCc.Set.U = 0;
		CaiOutput_p->ChalgToCc.Set.I = 0;
		CaiOutput_p->ChalgToCc.Set.P = 0;
	}

}

/**************************************************************************
 Name:		Compensate
 Does:		Compensate measured values...
 Returns:
 **************************************************************************/
static void Compensate(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p) {
	UcompTbatt_ConstParam_Type *UcompTbatt_ConstParam_p;
	IcompTbatt_ConstParam_Type *IcompTbatt_ConstParam_p;
	float Tbatt;

	// Voltage
	// -------
	// Voltage Tbatt
	UcompTbatt_ConstParam_p = (UcompTbatt_ConstParam_Type*) Module[idUcompTbatt].PhaseAddr;
	if (UcompTbatt_ConstParam_p) { // Check if there is definition
		Tbatt = CaiInput_p->Measure.Tbatt;
		if (Tbatt < UcompTbatt.Low)
			Tbatt = UcompTbatt.Low;
		else if (Tbatt > UcompTbatt.High)
			Tbatt = UcompTbatt.High;

		CompValue.Uact.Tbatt = UcompTbatt.Slope * (UcompTbatt.Neutral - Tbatt) * CaiInput_p->Algorithm.Cells;
#if 0
		if(CompValue.Uact.Tbatt > 0){
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattLow = 1;

		}
		else if(CompValue.Uact.Tbatt < 0){
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattHigh = 1;
		}
		else{
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattLow = 0;
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattHigh = 0;
		}
#endif

	} // if (UcompTbatt_p) {
	else{
		CompValue.Uact.Tbatt = 0;
#if 0
		CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattLow = 0;
		CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattHigh = 0;
#endif
	}
	// Voltage Cable Res
	CompValue.Uact.CableRes = CaiInput_p->Algorithm.CableRes
			* CaiInput_p->Measure.Iact;
	// Voltage sum
	CaiOutput_p->BatteryMeas.Ubatt = CaiInput_p->Measure.Uact
			- CompValue.Uact.CableRes /*+ CompValue.Uact.Tbatt*/;

	// Current
	// -------

	// Current Tbatt
	IcompTbatt_ConstParam_p = (IcompTbatt_ConstParam_Type*) Module[idIcompTbatt].PhaseAddr;
	if (IcompTbatt_ConstParam_p) { // Check if there is definition
		Tbatt = CaiInput_p->Measure.Tbatt;
		if (Tbatt < IcompTbatt.ColdLow)
			Tbatt = IcompTbatt.ColdLow;
		else if (Tbatt > IcompTbatt.WarmHigh)
			Tbatt = IcompTbatt.WarmHigh;

		if (Tbatt < IcompTbatt.ColdHigh){
			CompValue.Iact.Tbatt = (Tbatt - IcompTbatt.ColdLow) / (IcompTbatt.ColdHigh - IcompTbatt.ColdLow);
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattLow = 1;
		}
		else if (Tbatt > IcompTbatt.WarmLow){
			CompValue.Iact.Tbatt = (IcompTbatt.WarmHigh - Tbatt) / (IcompTbatt.WarmHigh - IcompTbatt.WarmLow);
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattHigh = 1;
		}
		else{
			CompValue.Iact.Tbatt = 1;	// 100%
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattLow = 0;
			CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattHigh = 0;
		}
	}
	else{ // if (IcompTbatt_p) {
		CompValue.Iact.Tbatt = 1;	// 100%
		CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattLow = 0;
		CaiOutput_p->ChargingStatus.Sticky.Info.Detail.TbattHigh = 0;
	}

	// Current BaseLoad
	CompValue.Iact.BaseLoad = CaiInput_p->Algorithm.BaseLoad;
	// Current sum
	CaiOutput_p->BatteryMeas.Ibatt = CaiInput_p->Measure.Iact
			- CompValue.Iact.BaseLoad /*+ CompValue.Iact.Tbatt*/;

	// Power
	// -----
	CaiOutput_p->BatteryMeas.Pbatt = CaiInput_p->Measure.Pact
			- (CompValue.Uact.CableRes * CompValue.Iact.BaseLoad);

}

/**************************************************************************
 Name:		Delta
 Does:		Calculate delta value
 Returns:
 **************************************************************************/
static void Delta(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p) {
	Delta_ConstParam_Type *Delta_ConstParam_p;
	idModule_enum id;
	DeltaValue_type *DeltaValue_p;

	for (id = idDelta0; id<=idDelta1; id++) {
		int Index; 		// Current Index
		int IndexNext;	// Next Index which also is last index

		switch(id){
		case idDelta0 :
			DeltaValue_p = &DeltaValue[0];
			break;
		case idDelta1 :
			DeltaValue_p = &DeltaValue[1];
			break;
		default :
			break;
		}

		Index = DeltaValue_p->Index;
		IndexNext = (DeltaValue_p->Index + 1) % DELTA_SUMS;

		Delta_ConstParam_p = (Delta_ConstParam_Type*) Module[id].PhaseAddr;
		if (Delta_ConstParam_p) {
			// Sum values
			DeltaValue_p->Sums[Index].Ubatt += CaiOutput_p->BatteryMeas.Ubatt - DeltaValue_p->Old.Ubatt;
			DeltaValue_p->Sums[Index].Ibatt += CaiOutput_p->BatteryMeas.Ibatt - DeltaValue_p->Old.Ibatt;
			DeltaValue_p->Sums[Index].Pbatt += CaiOutput_p->BatteryMeas.Pbatt - DeltaValue_p->Old.Pbatt;
			DeltaValue_p->Sums[Index].Tbatt += CaiInput_p->Measure.Tbatt - DeltaValue_p->Old.Tbatt;

			DeltaValue_p->Old.Ibatt = CaiOutput_p->BatteryMeas.Ibatt;
			DeltaValue_p->Old.Pbatt = CaiOutput_p->BatteryMeas.Pbatt;
			DeltaValue_p->Old.Tbatt = CaiInput_p->Measure.Tbatt;
			DeltaValue_p->Old.Ubatt = CaiOutput_p->BatteryMeas.Ubatt;

			DeltaValue_p->Delta.Ibatt = 0.0;
			DeltaValue_p->Delta.Pbatt = 0.0;
			DeltaValue_p->Delta.Tbatt = 0.0;
			DeltaValue_p->Delta.Ubatt = 0.0;

			int n;
			for(n = 0; n < DELTA_SUMS; n++){
				if(n == Index){
					DeltaValue_p->Delta.Ibatt += DeltaValue_p->Sums[n].Ibatt;
					DeltaValue_p->Delta.Pbatt += DeltaValue_p->Sums[n].Pbatt;
					DeltaValue_p->Delta.Tbatt += DeltaValue_p->Sums[n].Tbatt;
					DeltaValue_p->Delta.Ubatt += DeltaValue_p->Sums[n].Ubatt; // 0.000033 ... 0.001
				}
				else if(n == IndexNext){
					DeltaValue_p->Delta.Ibatt += DeltaValue_p->Sums[n].Ibatt*(DeltaValue_p->Samples - DeltaValue_p->Tick)/DeltaValue_p->Samples;
					DeltaValue_p->Delta.Pbatt += DeltaValue_p->Sums[n].Pbatt*(DeltaValue_p->Samples - DeltaValue_p->Tick)/DeltaValue_p->Samples;
					DeltaValue_p->Delta.Tbatt += DeltaValue_p->Sums[n].Tbatt*(DeltaValue_p->Samples - DeltaValue_p->Tick)/DeltaValue_p->Samples;
					DeltaValue_p->Delta.Ubatt += DeltaValue_p->Sums[n].Ubatt*(DeltaValue_p->Samples - DeltaValue_p->Tick)/DeltaValue_p->Samples; // 0.0 ... 0.000967
				}
				else{
					DeltaValue_p->Delta.Ibatt += DeltaValue_p->Sums[n].Ibatt;
					DeltaValue_p->Delta.Pbatt += DeltaValue_p->Sums[n].Pbatt;
					DeltaValue_p->Delta.Tbatt += DeltaValue_p->Sums[n].Tbatt;
					DeltaValue_p->Delta.Ubatt += DeltaValue_p->Sums[n].Ubatt;
				}
			}

			DeltaValue_p->Delta.Ibatt = DeltaValue_p->Delta.Ibatt/((DELTA_SUMS - 1)*DeltaValue_p->Samples);
			DeltaValue_p->Delta.Pbatt = DeltaValue_p->Delta.Pbatt/((DELTA_SUMS - 1)*DeltaValue_p->Samples);
			DeltaValue_p->Delta.Tbatt = DeltaValue_p->Delta.Tbatt/((DELTA_SUMS - 1)*DeltaValue_p->Samples);
			DeltaValue_p->Delta.Ubatt = DeltaValue_p->Delta.Ubatt/((DELTA_SUMS - 1)*DeltaValue_p->Samples);

			// Check if new sum
			if (DeltaValue_p->Tick == DeltaValue_p->Samples) {
				DeltaValue_p->Tick = 0;

				DeltaValue_p->Sums[IndexNext].Ibatt = 0.0;
				DeltaValue_p->Sums[IndexNext].Pbatt = 0.0;
				DeltaValue_p->Sums[IndexNext].Tbatt = 0.0;
				DeltaValue_p->Sums[IndexNext].Ubatt = 0.0;

				DeltaValue_p->Index = IndexNext;
			}
			DeltaValue_p->Tick++;

			// new Delta0
			// circular buffer to store data for one period and then update it
			DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Ubatt += CaiOutput_p->BatteryMeas.Ubatt;
			DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Ibatt += CaiOutput_p->BatteryMeas.Ibatt;
			DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Pbatt += CaiOutput_p->BatteryMeas.Pbatt;				// not used
			DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Tbatt += CaiInput_p->Measure.Tbatt;					// not used

			DeltaValue_p->Avg.Tick ++;

			if (DeltaValue_p->Avg.Tick == DeltaValue_p->Avg.Samp)
			{
				DeltaValue_p->Avg.IdxS = (DeltaValue_p->Avg.IdxS + 1) % DELTA_SIZE;
				DeltaValue_p->Avg.CtrS ++;
				DeltaValue_p->Avg.Tick = 0;

				if (DeltaValue_p->Avg.CtrS == DELTA_SIZE)
				{
					int i;
					for (i = 0; i < DELTA_SIZE; i++)
					{
						DeltaValue_p->Avg.Ubatt += DeltaValue_p->Avg.Sum[i].Ubatt;
						DeltaValue_p->Avg.Ibatt += DeltaValue_p->Avg.Sum[i].Ibatt;
						DeltaValue_p->Avg.Pbatt += DeltaValue_p->Avg.Sum[i].Pbatt;									// not used
						DeltaValue_p->Avg.Tbatt += DeltaValue_p->Avg.Sum[i].Tbatt;									// not used
					} // end of for loop

					DeltaValue_p->Avg.Ubatt = (DeltaValue_p->Avg.Ubatt / (DELTA_SIZE * DeltaValue_p->Avg.Samp));
					DeltaValue_p->Avg.Ibatt = (DeltaValue_p->Avg.Ibatt / (DELTA_SIZE * DeltaValue_p->Avg.Samp));
					DeltaValue_p->Avg.Pbatt = (DeltaValue_p->Avg.Pbatt / (DELTA_SIZE * DeltaValue_p->Avg.Samp));	// not used
					DeltaValue_p->Avg.Tbatt = (DeltaValue_p->Avg.Tbatt / (DELTA_SIZE * DeltaValue_p->Avg.Samp));	// not used

					DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Ubatt = 0;
					DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Ibatt = 0;
					DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Pbatt = 0;										// not used
					DeltaValue_p->Avg.Sum[DeltaValue_p->Avg.IdxS].Tbatt = 0;										// not used

					if (DeltaValue_p->Avg.Old.OldAvgFlag)
					{
						DeltaValue_p->Avg.DSum[DeltaValue_p->Avg.IdxDS].Ubatt = DeltaValue_p->Avg.Ubatt - DeltaValue_p->Avg.Old.AvgUbatt;
						DeltaValue_p->Avg.DSum[DeltaValue_p->Avg.IdxDS].Ibatt = DeltaValue_p->Avg.Ibatt - DeltaValue_p->Avg.Old.AvgIbatt;
						DeltaValue_p->Avg.DSum[DeltaValue_p->Avg.IdxDS].Pbatt = DeltaValue_p->Avg.Pbatt - DeltaValue_p->Avg.Old.AvgPbatt;	// not used
						DeltaValue_p->Avg.DSum[DeltaValue_p->Avg.IdxDS].Tbatt = DeltaValue_p->Avg.Tbatt - DeltaValue_p->Avg.Old.AvgTbatt;	// not used

						DeltaValue_p->Avg.IdxDS = (DeltaValue_p->Avg.IdxDS + 1) % DELTA_SIZE;
						DeltaValue_p->Avg.CtrDS ++;

						if (DeltaValue_p->Avg.CtrDS == DELTA_SIZE)
						{
							int j;
							for (j = 0; j < DELTA_SIZE; j++)
							{
								DeltaValue_p->Avg.DeltaSum.Ubatt += DeltaValue_p->Avg.DSum[j].Ubatt;
								DeltaValue_p->Avg.DeltaSum.Ibatt += DeltaValue_p->Avg.DSum[j].Ibatt;
								DeltaValue_p->Avg.DeltaSum.Pbatt += DeltaValue_p->Avg.DSum[j].Pbatt;	// not used
								DeltaValue_p->Avg.DeltaSum.Tbatt += DeltaValue_p->Avg.DSum[j].Tbatt;	// not used
							} // end of for loop

							DeltaValue_p->Avg.Delta.Ubatt = DeltaValue_p->Avg.DeltaSum.Ubatt;
							DeltaValue_p->Avg.Delta.Ibatt = DeltaValue_p->Avg.DeltaSum.Ibatt;
							DeltaValue_p->Avg.Delta.Pbatt = DeltaValue_p->Avg.DeltaSum.Pbatt;			// not used
							DeltaValue_p->Avg.Delta.Tbatt = DeltaValue_p->Avg.DeltaSum.Tbatt;			// not used

							DeltaValue_p->Avg.DeltaSum.Ubatt = 0;
							DeltaValue_p->Avg.DeltaSum.Ibatt = 0;
							DeltaValue_p->Avg.DeltaSum.Pbatt = 0;										// not used
							DeltaValue_p->Avg.DeltaSum.Tbatt = 0;										// not used

							DeltaValue_p->Avg.CtrDS --;
						}
					}

					DeltaValue_p->Avg.Old.AvgUbatt = DeltaValue_p->Avg.Ubatt;
					DeltaValue_p->Avg.Old.AvgIbatt = DeltaValue_p->Avg.Ibatt;
					DeltaValue_p->Avg.Old.AvgPbatt = DeltaValue_p->Avg.Pbatt;	// not used
					DeltaValue_p->Avg.Old.AvgTbatt = DeltaValue_p->Avg.Tbatt;	// not used
					DeltaValue_p->Avg.Old.OldAvgFlag = TRUE;

					DeltaValue_p->Avg.Ubatt = 0;
					DeltaValue_p->Avg.Ibatt = 0;
					DeltaValue_p->Avg.Pbatt = 0;	// not used
					DeltaValue_p->Avg.Tbatt = 0;	// not used

					DeltaValue_p->Avg.CtrS --;
				} // end of if
			} // end of if
		}
	}
}

/**************************************************************************

 Name:		BreakValueFloat
 Does:		
 Returns:
 **************************************************************************/
static void BreakValueFloat(idModule_enum id, uint32 Ctrl, float Param0,
		float Param1) {
	float BreakValue = 0;

	switch (Ctrl & OP_MASK) {
	case OP_NO:
		BreakValue = Param1;
		break;
	case OP_PLU:
		BreakValue = Param0 + Param1;
		break;
	case OP_MIN:
		BreakValue = Param0 - Param1;
		break;
	case OP_MUL:
		BreakValue = Param0 * Param1;
		break;
	default:
		break;
	}

	Module[id].Value.Float = BreakValue;
}


/**************************************************************************

 Name:		BreakValueUF
 Does:
 Returns:
 **************************************************************************/
static void BreakValueUF(idModule_enum id, uint32 Ctrl, uint32 Param0,
		float Param1) {
	uint32 BreakValue = 0;

	switch (Ctrl & OP_MASK) {
	case OP_NO:
		BreakValue = (uint32) Param1;
		break;
	case OP_PLU:
		BreakValue = (uint32) (Param0 + Param1);
		break;
	case OP_MIN:
		BreakValue = (uint32) (Param0 - Param1);
		break;
	case OP_MUL:
		BreakValue = (uint32) (Param0 * Param1);
		break;
	default:
		break;
	}

	Module[id].Value.Uint = BreakValue;
}


/**************************************************************************

 Name:		UserParamGet
 Does:      Get value when parameter can be defined as a UserParam.
            Note! control that the UserParam exists is done when making the
                  algorithm.
 Returns:
 **************************************************************************/
void UserParamGet(
		const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p,
		uint32 UserParam_Defined,
		float *ContParamValue_p,
		float *ParamTarget)
{
	UserParam_ConstParam_Type *UserParam_p;
	uint32 UserParam_Index;

	if (UserParam_Defined) { // Is the parameter a UserParam
		UserParam_Index = (uint32) *ContParamValue_p;
		// Check if user parameter from user or default
		if ((CaiInput_p->UserParam.Valid & (1<<UserParam_Index)) ) {
			// Parameter from user i valid
			*ParamTarget = CaiInput_p->UserParam.Value[UserParam_Index];
		}
		else {
			// No Parameter from user, use default
			UserParam_p = (UserParam_ConstParam_Type*) Mandatory.UserParam.Addr;
			*ParamTarget = UserParam_p[UserParam_Index].Default;
		}
	}
	else {
		// No UserParam
		*ParamTarget = *ContParamValue_p;
	}
}

/**************************************************************************
 Name:		PhaseInit
 Does:		Initials phase
 Returns:
 **************************************************************************/
void PhaseInit(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p,
		uint16 PhaseNo) {

	union {
		RegSet_ConstParam_Type          *RegSet;
		RegWin_ConstParam_Type          *RegWin;
		ReguOnOff_ConstParam_Type       *ReguOnOff;

		UbattBreak_ConstParam_Type      *UbattBreak;
		IbattBreak_ConstParam_Type      *IbattBreak;
		PbattBreak_ConstParam_Type      *PbattBreak;
		TbattBreak_ConstParam_Type      *TbattBreak;
		TimerBreak_ConstParam_Type      *TimerBreak;
		AhBreak_ConstParam_Type         *AhBreak;
		WhBreak_ConstParam_Type         *WhBreak;
		Delta_ConstParam_Type           *Delta;
	} ConstParam_p;

	float fParam0 = 0;
	float fParam1 = 0;
	uint32 uParam0 = 0;
	UcompTbatt_ConstParam_Type    *UcompTbatt_ConstParam_p;
	IcompTbatt_ConstParam_Type    *IcompTbatt_ConstParam_p;
	UbattWinBreak_ConstParam_Type *UbattWinBreak_ConstParam_p;
	DeltaValue_type *DeltaValue_p;
	Counter_type *Counter_p = 0;
	ChargingStage_Param_type *ChargingStage_Param_p;
	ChargingInfo_Param_type  *ChargingInfo_Param_p;
	ChargingError_Param_type *ChargingError_Param_p;
	Output_ConstParam_Type   *Output_p;
	idModule_enum id;
	bool Found;
	uint32 Index;
	uint16 NextPhase = 0;
	uint16 PhaseInit_Lap = 0;
	float TempFloat;
	PhaseSearch_enum PhaseSearch_Result;
	ReguOnOff_Type *ReguOnOff_p;
#define PHASE_INIT_LAP_MAX 3    // Max laps for InitPhase



	do {  // while ( (id < idModule_size) && (PhaseInit_Lap <= PHASE_INIT_LAP_MAX) && (NextPhase > 0));
		NextPhase = 0;
		for (id=idRegSet; id<idModule_length; id++) {
			switch (id) {
			case idRegSet:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(RegSet_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.RegSet = (RegSet_ConstParam_Type*) Module[id].PhaseAddr;
					// Uset
					UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.RegSet->Ctrl & REGSET_U_USER,
							&ConstParam_p.RegSet->Uset, &RegSetParam.U);
					// Iset
					UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.RegSet->Ctrl & REGSET_I_USER,
							&ConstParam_p.RegSet->Iset, &RegSetParam.I);
					// Pset
					UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.RegSet->Ctrl & REGSET_P_USER,
							&ConstParam_p.RegSet->Pset, &RegSetParam.P);
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					break;
				default:
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearch_Result ) {
				break;
			case idRegWin:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(RegWin_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					//ConstParam_p.RegWin = (RegWin_ConstParam_Type*) Module[id].PhaseAddr;
					// UerrWin
					//UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.RegSet->Ctrl & REGSET_U_USER,
					//		&ConstParam_p.RegWin->Uerr, &fTemp);
					//CaiOutput_p->ChalgToCc.ErrWin.U = fTemp * CaiInput_p->Algorithm.Cells;
					// IerrWin
					//UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.RegSet->Ctrl & REGSET_I_USER,
					//		&ConstParam_p.RegWin->Ierr, &fTemp);
					//CaiOutput_p->ChalgToCc.ErrWin.I = fTemp * CaiInput_p->Algorithm.Capacity;
					// PerrWin
					//UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.RegSet->Ctrl & REGSET_P_USER,
					//		&ConstParam_p.RegWin->Perr, &fTemp);
					//CaiOutput_p->ChalgToCc.ErrWin.P = fTemp * CaiInput_p->Algorithm.Cells * CaiInput_p->Algorithm.Capacity;
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					//CaiOutput_p->ChalgToCc.ErrWin.U = 0;
					//CaiOutput_p->ChalgToCc.ErrWin.I = 0;
					//CaiOutput_p->ChalgToCc.ErrWin.P = 0;
					break;
				default:
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearch_Result ) {
				break;
			case idReguOn:
			case idReguOff:
				switch (id) {
				case idReguOn:
					ReguOnOff_p = &CaiOutput_p->ChalgToCc.ReguOn;
					break;
				case idReguOff:
					ReguOnOff_p = &CaiOutput_p->ChalgToCc.ReguOff;
					break;
				default:
					ReguOnOff_p = &CaiOutput_p->ChalgToCc.ReguOn; // No function, just get rid of compiler warning
					Error = Error_ReguOnOff_id;
					break;
				} // switch (id) {
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(ReguOnOff_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.ReguOnOff = (ReguOnOff_ConstParam_Type*) Module[id].PhaseAddr;
					// Condition
					ReguOnOff_p->Condition.Sum = ( ConstParam_p.ReguOnOff->Ctrl &
							(REGUONOFF_COND_UACT_MASK | REGUONOFF_COND_REMOTEIN_MASK | REGUONOFF_COND_CAIN_MASK) ) >> 16;
					// UactLow
					UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.ReguOnOff->Ctrl & REGUONOFF_USER_UBATT_LOW,
							&ConstParam_p.ReguOnOff->UbattLow, &TempFloat);
					ReguOnOff_p->Uact.Low = TempFloat * CaiInput_p->Algorithm.Cells + CompValue.Uact.CableRes + CompValue.Uact.Tbatt;
					// UactHigh
					UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.ReguOnOff->Ctrl & REGUONOFF_USER_UBATT_HIGH,
							&ConstParam_p.ReguOnOff->UbattHigh, &TempFloat);
					ReguOnOff_p->Uact.High = TempFloat * CaiInput_p->Algorithm.Cells + CompValue.Uact.CableRes + CompValue.Uact.Tbatt;
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					CaiOutput_p->ChalgToCc.ReguOn.Condition.Sum = 0;
					break;
				default:
					CaiOutput_p->ChalgToCc.ReguOn.Condition.Sum = 0;
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearchFunc(id, sizeof(ReguOnOff_ConstParam_Type), PhaseNo) ) {
				break;
			case idUcompTbatt:
				if ( PhaseSearch(id, sizeof(UcompTbatt_ConstParam_Type), PhaseNo) ) {
					UcompTbatt_ConstParam_p = (UcompTbatt_ConstParam_Type*) Module[id].PhaseAddr;
					// Low
					UserParamGet(CaiInput_p, CaiOutput_p, UcompTbatt_ConstParam_p->Ctrl & UCOMPTBATT_USER_LOW,
							&UcompTbatt_ConstParam_p->Low, &UcompTbatt.Low);
					// Neutral
					UserParamGet(CaiInput_p, CaiOutput_p, UcompTbatt_ConstParam_p->Ctrl & UCOMPTBATT_USER_NEUTRAL,
							&UcompTbatt_ConstParam_p->Neutral, &UcompTbatt.Neutral);
					// High
					UserParamGet(CaiInput_p, CaiOutput_p, UcompTbatt_ConstParam_p->Ctrl & UCOMPTBATT_USER_HIGH,
							&UcompTbatt_ConstParam_p->High, &UcompTbatt.High);
					// Slope
					UserParamGet(CaiInput_p, CaiOutput_p, UcompTbatt_ConstParam_p->Ctrl & UCOMPTBATT_USER_SLOPE,
							&UcompTbatt_ConstParam_p->Slope, &UcompTbatt.Slope);
				}
				break;
			case idIcompTbatt:
				if ( PhaseSearch(id, sizeof(IcompTbatt_ConstParam_Type), PhaseNo) ) {
					IcompTbatt_ConstParam_p = (IcompTbatt_ConstParam_Type*) Module[id].PhaseAddr;
					// Cold.Low
					UserParamGet(CaiInput_p, CaiOutput_p, IcompTbatt_ConstParam_p->Ctrl & ICOMPTBATT_USER_COLDLOW,
							&IcompTbatt_ConstParam_p->ColdLow, &IcompTbatt.ColdLow);
					// Cold.High
					UserParamGet(CaiInput_p, CaiOutput_p, IcompTbatt_ConstParam_p->Ctrl & ICOMPTBATT_USER_COLDHIGH,
							&IcompTbatt_ConstParam_p->ColdHigh, &IcompTbatt.ColdHigh);
					// Warm.Low
					UserParamGet(CaiInput_p, CaiOutput_p, IcompTbatt_ConstParam_p->Ctrl & ICOMPTBATT_USER_WARMLOW,
							&IcompTbatt_ConstParam_p->WarmLow, &IcompTbatt.WarmLow);
					// Warm.High
					UserParamGet(CaiInput_p, CaiOutput_p, IcompTbatt_ConstParam_p->Ctrl & ICOMPTBATT_USER_WARMHIGH,
							&IcompTbatt_ConstParam_p->WarmHigh, &IcompTbatt.WarmHigh);
				}
				break;
			case idUbattWinBreak0:
			case idUbattWinBreak1:
			case idUbattWinBreak2:
			case idUbattWinBreak3:
				if ( PhaseSearch(id, sizeof(UbattWinBreak_ConstParam_Type), PhaseNo) ) {
					UbattWinBreak_ConstParam_p = (UbattWinBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Find index for the id in UbattWinBreak
					Found = FALSE;
					for (Index=0; Index<UBATTWINBREAK_LENGTH; Index++) {
						if (UbattWinBreak[Index].id == id) {
							Found = TRUE;
							break;
						}
					}
					// Initialize
					if (Found) {
						Module[id].Value.Index = Index;  // Save index, used in Break function
						// Ulow
						UserParamGet(CaiInput_p, CaiOutput_p,
								UbattWinBreak_ConstParam_p->Ctrl & UBATTWIN_USER_ULOW,
								&UbattWinBreak_ConstParam_p->Ulow, &UbattWinBreak[Index].Ulow);

						// Uhigh
						UserParamGet(CaiInput_p, CaiOutput_p,
								UbattWinBreak_ConstParam_p->Ctrl & UBATTWIN_USER_UHIGH,
								&UbattWinBreak_ConstParam_p->Uhigh, &UbattWinBreak[Index].Uhigh);

					}
					else
						Error = Error_UbattWinbreak_Index;
				}
				break;
			case idUbattBreak0:
			case idUbattBreak1:
			case idUbattBreak2:
			case idUbattBreak3:
			case idUbattDelta0Break0:
			case idUbattDelta0Break1:
			case idUbattDelta1Break0:
			case idUbattDelta1Break1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(UbattBreak_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.UbattBreak = (UbattBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if the flag EXE_INIT i set
					if (ConstParam_p.UbattBreak->Ctrl & EXE_INIT) {
						// Param0
						switch (ConstParam_p.UbattBreak->Ctrl & UBB_P0_MASK) {
						case UBB_P0_BREA:
							fParam0 = Module[id].Value.Float;
							break;
						case UBB_P0_UBAT:
							fParam0 = CaiOutput_p->BatteryMeas.Ubatt;
							break;
						case UBB_P0_CELL:
							fParam0 = CaiInput_p->Algorithm.Cells;
							break;
						default:
							break;
						}
						// Param1
						UserParamGet(CaiInput_p, CaiOutput_p,
								ConstParam_p.UbattBreak->Ctrl & UBB_USER_PARAM1,
								&ConstParam_p.UbattBreak->Param1, &fParam1);
						// Calculate BreakValue
						BreakValueFloat(id, ConstParam_p.UbattBreak->Ctrl, fParam0, fParam1);
					} // if (ConstParam_p.UbattBreak->Ctrl & EXE_INIT) {
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					break;
				default:
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearch_Result ) {
				break;
			case idIbattBreak0:
			case idIbattBreak1:
			case idIbattBreak2:
			case idIbattBreak3:
			case idIbattDelta0Break0:
			case idIbattDelta0Break1:
			case idIbattDelta1Break0:
			case idIbattDelta1Break1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(IbattBreak_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.IbattBreak = (IbattBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if the flag EXE_INIT i set
					if (ConstParam_p.IbattBreak->Ctrl & EXE_INIT) {
						// Param0
						switch (ConstParam_p.IbattBreak->Ctrl & IBB_P0_MASK) {
						case IBB_P0_BREA:
							fParam0 = Module[id].Value.Float;
							break;
						case IBB_P0_IBAT:
							fParam0 = CaiOutput_p->BatteryMeas.Ibatt;
							break;
						case IBB_P0_CAPA:
							fParam0 = CaiInput_p->Algorithm.Capacity;
							break;
						default:
							break;
						}
						// Param1
						UserParamGet(CaiInput_p, CaiOutput_p,
								ConstParam_p.IbattBreak->Ctrl & IBB_USER_PARAM1,
								&ConstParam_p.IbattBreak->Param1, &fParam1);
						// Calculate BreakValue
						BreakValueFloat(id, ConstParam_p.IbattBreak->Ctrl, fParam0, fParam1);
					} // if (ConstParam_p.IbattBreak->Ctrl & EXE_INIT) {
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					break;
				default:
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearch_Result ) {
				break;
			case idPbattBreak0:
			case idPbattBreak1:
			case idPbattBreak2:
			case idPbattBreak3:
			case idPbattDelta0Break0:
			case idPbattDelta0Break1:
			case idPbattDelta1Break0:
			case idPbattDelta1Break1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(PbattBreak_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.PbattBreak = (PbattBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if the flag EXE_INIT i set
					if (ConstParam_p.PbattBreak->Ctrl & EXE_INIT) {
						// Param0
						switch (ConstParam_p.PbattBreak->Ctrl & PBB_P0_MASK) {
						case IBB_P0_BREA:
							fParam0 = Module[id].Value.Float;
							break;
						case IBB_P0_IBAT:
							fParam0 = CaiOutput_p->BatteryMeas.Pbatt;
							break;
						case IBB_P0_CAPA:
							fParam0 = CaiInput_p->Algorithm.Cells * CaiInput_p->Algorithm.Capacity;
							break;
						default:
							break;
						}
						// UserParam Param1
						UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.PbattBreak->Ctrl & PBB_USER_PARAM1,
								&ConstParam_p.PbattBreak->Param1, &fParam1);
						// Calculate BreakValue
						BreakValueFloat(id, ConstParam_p.PbattBreak->Ctrl, fParam0, fParam1);
					} // if (ConstParam_p.PbattBreak->Ctrl & EXE_INIT) {
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					break;
				default:
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearchFunc(id, sizeof(ReguOnOff_ConstParam_Type), PhaseNo) ) {
				break;
			case idTbattBreak0:
			case idTbattBreak1:
			case idTbattBreak2:
			case idTbattBreak3:
			case idTbattDelta0Break0:
			case idTbattDelta0Break1:
			case idTbattDelta1Break0:
			case idTbattDelta1Break1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(TbattBreak_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.TbattBreak = (TbattBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if the flag EXE_INIT i set
					if (ConstParam_p.TbattBreak->Ctrl & EXE_INIT) {
						// Param0
						switch (ConstParam_p.TbattBreak->Ctrl & TBB_P0_MASK) {
						case TBB_P0_BREA:
							fParam0 = Module[id].Value.Float;
							break;
						case TBB_P0_TBAT:
							fParam0 = CaiInput_p->Measure.Tbatt;
							break;
						default:
							break;
						}
						// UserParam Param1
						UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.TbattBreak->Ctrl & TBB_USER_PARAM1,
								&ConstParam_p.TbattBreak->Param1, &fParam1);
						// Calculate BreakValue
						BreakValueFloat(id, ConstParam_p.TbattBreak->Ctrl, fParam0, fParam1);
					} // if (ConstParam_p.TbattBreak->Ctrl & EXE_INIT) {
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					break;
				default:
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearchFunc(id, sizeof(ReguOnOff_ConstParam_Type), PhaseNo) ) {
				break;
			case idReguModeBreak0:
			case idReguModeBreak1:
				(void) PhaseSearch(id, sizeof(ReguModeBreak_ConstParam_Type), PhaseNo);
				break;
			case idReguDerateBreak0:
			case idReguDerateBreak1:
				(void) PhaseSearch(id, sizeof(ReguDerateBreak_ConstParam_Type), PhaseNo);
				break;
			case idReguEngineBreak0:
			case idReguEngineBreak1:
				(void) PhaseSearch(id, sizeof(ReguEngineBreak_ConstParam_Type), PhaseNo);
				break;
			case idInputBreak0:
			case idInputBreak1:
				(void) PhaseSearch(id, sizeof(InputBreak_ConstParam_Type), PhaseNo);
				break;
			case idTimer0Break0:
			case idTimer0Break1:
			case idTimer1Break0:
			case idTimer1Break1:
			case idTimer2Break0:
			case idTimer2Break1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(TimerBreak_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.TimerBreak = (TimerBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if the flag EXE_INIT i set
					if (ConstParam_p.TimerBreak->Ctrl & EXE_INIT) {
						// Param0
						switch (ConstParam_p.TimerBreak->Ctrl & TIB_P0_MASK) {
						case TIB_P0_BREA:
							uParam0 = Module[id].Value.Uint;
							break;
						case TIB_P0_TIM0:
							uParam0 = CounterValue.Timer[Timer0];
							break;
						case TIB_P0_TIM1:
							uParam0 = CounterValue.Timer[Timer1];
							break;
						case TIB_P0_TIM2:
							uParam0 = CounterValue.Timer[Timer2];
							break;
						case TIB_P0_AH0C:
							uParam0 = (uint32) ( CounterValue.AmpSecCnt[0] / (CaiInput_p->Algorithm.Capacity * 60 * 60) );
							break;
						case TIB_P0_AH1C:
							uParam0 = (uint32) ( CounterValue.AmpSecCnt[1] / (CaiInput_p->Algorithm.Capacity * 60 * 60) );
							break;
						default:
							break;
						} // switch (TimerBreak_p->Ctrl & UB_P0_MASK) {
						// UserParam Param1
						UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.TimerBreak->Ctrl & TBB_USER_PARAM1,
								&ConstParam_p.TimerBreak->Param1, &fParam1);
						BreakValueUF(id, ConstParam_p.TimerBreak->Ctrl, uParam0, fParam1);
					} // if (ConstParam_p.TimerBreak->Ctrl & EXE_INIT) {
					break;
					case PhaseSearch_Continuous:
						break;
					case PhaseSearch_NotDefined:
						break;
					default:
						Error = Error_PhaseSearch;
						break;
				} // switch ( PhaseSearchFunc(id, sizeof(ReguOnOff_ConstParam_Type), PhaseNo) ) {
				break;
			case idAh0Break0:
			case idAh0Break1:
			case idAh1Break0:
			case idAh1Break1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(AhBreak_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.AhBreak = (AhBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if the flag EXE_INIT i set
					if (ConstParam_p.AhBreak->Ctrl & EXE_INIT) {
						// Param0
						switch (ConstParam_p.AhBreak->Ctrl & AHB_P0_MASK) {
						case AHB_P0_BREA:
							fParam0 = Module[id].Value.Float;
							break;
						case AHB_P0_AH0:
							fParam0 = CounterValue.AmpSecCnt[0];
							break;
						case AHB_P0_AH1:
							fParam0 = CounterValue.AmpSecCnt[1];
							break;
						case AHB_P0_CAPA:
							fParam0 = CaiInput_p->Algorithm.Capacity * 60 * 60; // Convert Ah to As
							break;
						default:
							break;
						} // switch (TimerBreak_p->Ctrl & UB_P0_MASK) {
						// UserParam Param1
						UserParamGet(CaiInput_p, CaiOutput_p, (ConstParam_p.AhBreak->Ctrl & AHB_USER_PARAM1),
								&ConstParam_p.AhBreak->Param1, &fParam1);
						BreakValueFloat(id, ConstParam_p.AhBreak->Ctrl, fParam0, fParam1);
					} // if (ConstParam_p.AhBreak->Ctrl & EXE_INIT) {
					break;
					case PhaseSearch_Continuous:
						break;
					case PhaseSearch_NotDefined:
						break;
					default:
						Error = Error_PhaseSearch;
						break;
				} // switch ( PhaseSearchFunc(id, sizeof(ReguOnOff_ConstParam_Type), PhaseNo) ) {
				break;
			case idWh0Break0:
			case idWh0Break1:
			case idWh1Break0:
			case idWh1Break1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(WhBreak_ConstParam_Type), PhaseNo);
				switch ( PhaseSearch_Result ) {
				case PhaseSearch_New:
					ConstParam_p.WhBreak = (WhBreak_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if the flag EXE_INIT i set
					if (ConstParam_p.WhBreak->Ctrl & EXE_INIT) {
						// Param0
						switch (ConstParam_p.WhBreak->Ctrl & WHB_P0_MASK) {
						case WHB_P0_BREA:
							fParam0 = Module[id].Value.Uint;
							break;
						case WHB_P0_WH0:
							fParam0 = CounterValue.WattSecCnt[0];
							break;
						case WHB_P0_WH1:
							fParam0 = CounterValue.WattSecCnt[1];
							break;
						case WHB_P0_CECA:
							fParam0 = CaiInput_p->Algorithm.Capacity;
							break;
						default:
							break;
						} // switch (ConstParam_p.WhBreak->Ctrl & WHB_P0_MASK) {
						// UserParam Param1
						UserParamGet(CaiInput_p, CaiOutput_p, ConstParam_p.WhBreak->Ctrl & WHB_USER_PARAM1,
								&ConstParam_p.WhBreak->Param1, &fParam1);
						BreakValueFloat(id, ConstParam_p.WhBreak->Ctrl, fParam0, fParam1);
					} // if (ConstParam_p.WhBreak->Ctrl & EXE_INIT) {
					break;
				case PhaseSearch_Continuous:
					break;
				case PhaseSearch_NotDefined:
					break;
				default:
					Error = Error_PhaseSearch;
					break;
				} // switch ( PhaseSearch_Result ) {
				break;
			case idAndBreak0:
			case idAndBreak1:
				(void) PhaseSearch(id, sizeof(AndBreak_type), PhaseNo);
				break;
			case idCounter:
				if (PhaseSearch(id, sizeof(Counter_type), PhaseNo)) {
					Counter_p = (Counter_type*) Module[id].PhaseAddr;
					// Timer
					if (Counter_p->Ctrl & TI0_RES)
						CounterValue.Timer[Timer0] = 0;
					if (Counter_p->Ctrl & TI1_RES)
						CounterValue.Timer[Timer1] = 0;
					if (Counter_p->Ctrl & TI2_RES)
						CounterValue.Timer[Timer2] = 0;
					// Ah
					if (Counter_p->Ctrl & AH0_RES)
						CounterValue.AmpSecCnt[0] = 0;
					if (Counter_p->Ctrl & AH1_RES)
						CounterValue.AmpSecCnt[1] = 0;
					// Wh
					if (Counter_p->Ctrl & AH0_RES)
						CounterValue.WattSecCnt[0] = 0;
					if (Counter_p->Ctrl & AH1_RES)
						CounterValue.WattSecCnt[1] = 0;
				}
				break;
			case idDelta0:
			case idDelta1:
				PhaseSearch_Result = PhaseSearchFunc(id, sizeof(Delta_ConstParam_Type), PhaseNo);
				if ( PhaseSearch_Result == PhaseSearch_New ) {
					DeltaValue_p = &DeltaValue[0];
					if (id == idDelta1)
						// TODO Change initialization of DeltaValue buffer (if a new Delta is added).
						DeltaValue_p++;
					ConstParam_p.Delta = (Delta_ConstParam_Type*) Module[id].PhaseAddr;
					// Initialize if active
					if (ConstParam_p.Delta->Ctrl & EXE_INIT) {

						DeltaValue_p->Index = 0;
						DeltaValue_p->Tick = 0;
						DeltaValue_p->Samples = (uint32)((ConstParam_p.Delta->Period + 5)/10);
						DeltaValue_p->Delta.Ubatt = 0;
						DeltaValue_p->Delta.Ibatt = 0;
						DeltaValue_p->Delta.Pbatt = 0;
						DeltaValue_p->Delta.Tbatt = 0;

						int n;
						for(n = 0; n < DELTA_SUMS; n++){
							DeltaValue_p->Sums[n].Ubatt = 0;
							DeltaValue_p->Sums[n].Ibatt = 0;
							DeltaValue_p->Sums[n].Pbatt = 0;
							DeltaValue_p->Sums[n].Tbatt = 0;
						}
						// new Delta0
						DeltaValue_p->Avg.IdxS = 0;
						DeltaValue_p->Avg.IdxDS = 0;
						DeltaValue_p->Avg.CtrS = 0;
						DeltaValue_p->Avg.CtrDS = 0;
						DeltaValue_p->Avg.Tick = 0;
						DeltaValue_p->Avg.Samp = (uint32)((ConstParam_p.Delta->Period + 5)/10);

						int i;
						for(i = 0; i < DELTA_SIZE; i++){
							DeltaValue_p->Avg.Sum[i].Ubatt = 0;
							DeltaValue_p->Avg.Sum[i].Ibatt = 0;
							DeltaValue_p->Avg.Sum[i].Pbatt = 0;		// not used
							DeltaValue_p->Avg.Sum[i].Tbatt = 0;		// not used
						}

						DeltaValue_p->Avg.Ubatt = 0;
						DeltaValue_p->Avg.Ibatt = 0;
						DeltaValue_p->Avg.Pbatt = 0;				// not used
						DeltaValue_p->Avg.Tbatt = 0;				// not used

						DeltaValue_p->Avg.Old.AvgUbatt = 0;
						DeltaValue_p->Avg.Old.AvgIbatt = 0;
						DeltaValue_p->Avg.Old.AvgPbatt = 0;			// not used
						DeltaValue_p->Avg.Old.AvgTbatt = 0;			// not used
						DeltaValue_p->Avg.Old.OldAvgFlag = FALSE;

						int j;
						for(j = 0; j < DELTA_SIZE; j++){
							DeltaValue_p->Avg.DSum[j].Ubatt = 0;
							DeltaValue_p->Avg.DSum[j].Ibatt = 0;
							DeltaValue_p->Avg.DSum[j].Pbatt = 0;	// not used
							DeltaValue_p->Avg.DSum[j].Tbatt = 0;	// not used
						}

						DeltaValue_p->Avg.DeltaSum.Ubatt = 0;
						DeltaValue_p->Avg.DeltaSum.Ibatt = 0;
						DeltaValue_p->Avg.DeltaSum.Pbatt = 0;		// not used
						DeltaValue_p->Avg.DeltaSum.Tbatt = 0;		// not used

						DeltaValue_p->Avg.Delta.Ubatt = 1;			// cannot be zero, the condition will always meet then
						DeltaValue_p->Avg.Delta.Ibatt = 0;
						DeltaValue_p->Avg.Delta.Pbatt = 0;			// not used
						DeltaValue_p->Avg.Delta.Tbatt = 0;			// not used
					}
				}
				break;
			case idOutput:
				if (PhaseSearch(id, sizeof(Output_ConstParam_Type), PhaseNo)) {
					Output_p = (Output_ConstParam_Type*) Module[id].PhaseAddr;
					CaiOutput_p->Output.Sum = Output_p->Value;
				}
				break;
			case idChargingStage:
				if (PhaseSearch(id, sizeof(ChargingStage_Param_type), PhaseNo)) {
					ChargingStage_Param_p = (ChargingStage_Param_type*) Module[id].PhaseAddr;
					// Check if it's the first stage
					// (I.E. first initialization of a phase in this module
					if (Module[id].Status & CHARGING_STAGE_CTRL_VALIDATA) {
						// Save values for the old stage
						CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Stop.Tick = Cai_Tick;
						CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Stop.Ubatt = CaiOutput_p->BatteryMeas.Ubatt;
						CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Stop.Ibatt = CaiOutput_p->BatteryMeas.Ibatt;
						CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Stop.Tbatt = CaiInput_p->Measure.Tbatt;
						CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Time += Cai_Tick -
						CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Stop.Tick;
					}
					// The new Stage (no check if there isn't a change)
					Module[id].Status |= CHARGING_STAGE_CTRL_VALIDATA;
					//CaiOutput_p->ChargingStage = Module[id].Status & CHARGING_STAGE_CTRL_STAGENO_MASK;	// charging stage allways 0
					CaiOutput_p->ChargingStage = ChargingStage_Param_p->Ctrl;	//use this to set charging stage
					CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Start.Ubatt = CaiOutput_p->BatteryMeas.Ubatt;
					CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Start.Ibatt = CaiOutput_p->BatteryMeas.Ibatt;
					CaiOutput_p->ChargingValues[CaiOutput_p->ChargingStage].Start.Tbatt = CaiInput_p->Measure.Tbatt;
					// New Phase
					if (ChargingStage_Param_p->NextPhase > 0)
						NextPhase = ChargingStage_Param_p->NextPhase;
				}
				break;
			case idChargingInfo:
				if (PhaseSearch(id, sizeof(ChargingInfo_Param_type), PhaseNo)) {
					ChargingInfo_Param_p = (ChargingInfo_Param_type*) Module[id].PhaseAddr;
					CaiOutput_p->ChargingStatus.Instant.Info.Sum = ChargingInfo_Param_p->Ctrl & CHRG_INFO_MASK;
					CaiOutput_p->ChargingStatus.Sticky.Info.Sum = ChargingInfo_Param_p->Ctrl & CHRG_INFO_MASK;
				}
				break;
			case idChargingError:
				if (PhaseSearch(id, sizeof(ChargingError_Param_type), PhaseNo)) {
					ChargingError_Param_p = (ChargingError_Param_type*) Module[id].PhaseAddr;
					CaiOutput_p->ChargingStatus.Instant.Error.Sum = ChargingError_Param_p->Ctrl & CHRG_ERRO_MASK;
					CaiOutput_p->ChargingStatus.Sticky.Error.Sum = ChargingError_Param_p->Ctrl & CHRG_ERRO_MASK;
				}
				break;


			case idModule_length:   // For Lint
			default:
				Error = Error_PhaseInit;
				break;
			} // switch (id) {

		} // for (id=idRegset; id<idModule_size; id+) {


		if ( NextPhase && (PhaseInit_Lap <= PHASE_INIT_LAP_MAX) ) {
			PhaseNo = NextPhase;
			Phase = NextPhase;
		}
		else {
			// We have run to many laps
			NextPhase = 0;
		}
		PhaseInit_Lap++;
	} while (NextPhase > 0);

}

/***************************************************************************
 End of File
 ***************************************************************************/

