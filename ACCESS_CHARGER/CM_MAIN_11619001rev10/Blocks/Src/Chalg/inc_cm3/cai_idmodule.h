/*
 $Revision: 155 $
 $Date: 2010-05-17 10:04:04 +0200 (må, 17 maj 2010) $
 $Author: bsi $
 $HeadURL: https://subversion.mpi.local/svn/micropower/_user/bsi/alg/alg_types.h $
 */

/**************************************************************************
 Project:     ACCESS:CM
 Module:      cai_idmodule.h
 Contains:    Identity for all modules.
              NOTE! A new module must be inserted directly above
                    idModule_size in the definition. This is the only
                    modification allowed in this file.
                    Reason is to be backward compatible.
 Environment: Compiled and tested with GNU GCC.
 Copyright:   Micropower ED
 ***************************************************************************/
typedef enum {
#if !(__LINE__ == 20)
ERROR: No adding of line is allowed before line 19.
#endif
	idRegSet,
	idRegWin,
	idReguOn,
	idReguOff,
	idUcompTbatt,
	idIcompTbatt,
	idUbattWinBreak0,
	idUbattWinBreak1,
	idUbattWinBreak2,
	idUbattWinBreak3,
	idUbattBreak0,  // 10
	idUbattBreak1,
	idUbattBreak2,
	idUbattBreak3,
	idUbattDelta0Break0,
	idUbattDelta0Break1,
	idUbattDelta1Break0,
	idUbattDelta1Break1,
	idIbattBreak0,
	idIbattBreak1,
	idIbattBreak2, // 20
	idIbattBreak3,
	idIbattDelta0Break0,
	idIbattDelta0Break1,
	idIbattDelta1Break0,
	idIbattDelta1Break1,
	idPbattBreak0,
	idPbattBreak1,
	idPbattBreak2,
	idPbattBreak3,
	idPbattDelta0Break0,  // 30
	idPbattDelta0Break1,
	idPbattDelta1Break0,
	idPbattDelta1Break1,
	idTbattBreak0,
	idTbattBreak1,
	idTbattBreak2,
	idTbattBreak3,
	idTbattDelta0Break0,
	idTbattDelta0Break1,
	idTbattDelta1Break0,  // 40
	idTbattDelta1Break1,
	idReguModeBreak0,
	idReguModeBreak1,
	idReguDerateBreak0,
	idReguDerateBreak1,
	idReguEngineBreak0,
	idReguEngineBreak1,
	idInputBreak0,
	idInputBreak1,
	idTimer0Break0,  // 50
	idTimer0Break1,
	idTimer1Break0,
	idTimer1Break1,
	idTimer2Break0,
	idTimer2Break1,
	idAh0Break0,
	idAh0Break1,
	idAh1Break0,
	idAh1Break1,
	idWh0Break0,  // 60
	idWh0Break1,
	idWh1Break0,
	idWh1Break1,
	idAndBreak0,
	idAndBreak1,
	idCounter,
	idDelta0,
	idDelta1,
	idOutput,
	idChargingStage,  // 70
	idChargingInfo,
	idChargingError,  // 72
// Add new module on this row
	idModule_length     // 73
} idModule_enum;
#define IDMODULE_LENGTH 73     // *** Edit when adding new module, must be the same value as idModule_size. ***
#if !(__LINE__ == (27 + IDMODULE_LENGTH))
#error A new module is added edit IDMODULE_LENGTH (2 rows above).
#endif




/*******************************************************************************
END OF FILE
*******************************************************************************/






