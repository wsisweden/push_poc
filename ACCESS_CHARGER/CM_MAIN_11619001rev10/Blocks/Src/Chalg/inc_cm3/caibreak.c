/*
 $Revision: 153 $
 $Date: 2010-05-14 15:22:51 +0200 (fr, 14 maj 2010) $
 $Author: bsi $
 $HeadURL: https://subversion.mpi.local/svn/micropower/_user/bsi/alg/caibreak.c $
 */

/**************************************************************************
 Project:     ACCESS:CM
 Module:      caibreak.c
 Contains:    Handle break condition for changing phase.
 Environment: Compiled and tested with GNU GCC.
 Copyright:   Micropower ED
 Version:
 ---------------------------------------------------------------------------
 History:
 Rev      Date      Sign  Description
 bsi   First version.
 ***************************************************************************/
#include "cai_constparam.h"
#include "caimain.h"
#include "math.h"

/***************************************************************************
 Definition and Type
 ***************************************************************************/


/**************************************************************************
 Local Definition
 ***************************************************************************/


/**************************************************************************
 External References
 ***************************************************************************/

/**************************************************************************
 Public Definitions
 ***************************************************************************/

/**************************************************************************
 Local Function Prototypes
 ***************************************************************************/
static bool Compare_float(idModule_enum id, float ActValue, float BreakValue);
static bool Compare_uint(idModule_enum id, uint32 ActValue, uint32 BreakValue);
static bool CheckXerrWin(const CaiInput_type *CaiInput_p, uint32 Ctrl);

/**************************************************************************
 Public Functions
 ***************************************************************************/

/**************************************************************************
 Name:		Break
 Does:		Break condition
 Descr:     Module is defined for the phase when Module[id].Addr > 0.
 Returns:
 **************************************************************************/
void Break(const CaiInput_type *CaiInput_p, CaiOutput_type *CaiOutput_p) {

	union {
		UbattBreak_ConstParam_Type      *UbattBreak;
		IbattBreak_ConstParam_Type      *IbattBreak;
		UbattWinBreak_ConstParam_Type   *UbattWinBreak;
		TbattBreak_ConstParam_Type      *TbattBreak;
		TimerBreak_ConstParam_Type      *TimerBreak;
		AhBreak_ConstParam_Type         *AhBreak;
		WhBreak_ConstParam_Type         *WhBreak;
	} ConstParam_p;

	uint32 uActValue;
	float fActValue;
	float fSetValue;
//	uint64 BreakValue;
	UbattBreak_ConstParam_Type      *UbattBreak_ConstParam_p;
	IbattBreak_ConstParam_Type      *IbattBreak_ConstParam_p;
	TbattBreak_ConstParam_Type      *TbattBreak_ConstParam_p;
//	TimerBreak_type                 *TimerBreak_p;
//	AhBreak_type                    *AhBreak_p;
//	WhBreak_type                    *WhBreak_p;
	ReguModeBreak_ConstParam_Type   *ReguModeBreak_p;
	ReguDerateBreak_ConstParam_Type *ReguDerateBreak_p;
	ReguEngineBreak_ConstParam_Type *ReguEngineBreak_p;
	InputBreak_ConstParam_Type      *InputBreak_p;
	AndBreak_type      *AndBreak_p;
//	UbattWinBreak_ConstParam_Type *UbattWinBreak_p;
	float Ulow;
	float Uhigh;

	uint16 NewPhase = 0;
	idModule_enum id = idUbattWinBreak0;
	uint8 AndIndex;
	bool CheckOk;


	// Going through all modules, even if it's no breakModule.
	// reason is to be backward compatible if new modules will be added.
	while ( (id<idModule_length) && (NewPhase == 0) ) {
		Module[id].Status &= ~MSTATUS_BC_FULLFILLED;  // Set Break condition not full filled
		switch (id) {
		case idRegSet:
		case idRegWin:
		case idReguOn:
		case idReguOff:
		case idUcompTbatt:
		case idIcompTbatt:
			// Not a BreakModule, do nothing (just for for Lint).
			break;
		case idUbattWinBreak0:
		case idUbattWinBreak1:
		case idUbattWinBreak2:
		case idUbattWinBreak3:
			ConstParam_p.UbattWinBreak = (UbattWinBreak_ConstParam_Type*) Module[id].PhaseAddr;
			if ( ConstParam_p.UbattWinBreak ) {
				// Check Condition?
				if (ConstParam_p.UbattWinBreak->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;

					// Check Condition UbattWin
					if ( (ConstParam_p.UbattWinBreak->Ctrl & UBATTWIN_CHECK_BRVA_MASK) != UBATTWIN_CHECK_BRVA_EGAL ) {

						Ulow = UbattWinBreak[Module[id].Value.Index].Ulow * CaiInput_p->Algorithm.Cells + CompValue.Uact.Tbatt;
						Uhigh = UbattWinBreak[Module[id].Value.Index].Uhigh * CaiInput_p->Algorithm.Cells + CompValue.Uact.Tbatt;

						if ( (Ulow < CaiOutput_p->BatteryMeas.Ubatt) && (CaiOutput_p->BatteryMeas.Ubatt < Uhigh) ) {
							if ( (ConstParam_p.UbattWinBreak->Ctrl & UBATTWIN_CHECK_BRVA_MASK) == UBATTWIN_CHECK_BRVA_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ConstParam_p.UbattWinBreak->Ctrl & UBATTWIN_CHECK_BRVA_MASK) == UBATTWIN_CHECK_BRVA_TRUE )
								CheckOk = FALSE;
						}
					}

					// Check Condition Regulator
					if ( !CheckXerrWin(CaiInput_p, ConstParam_p.UbattWinBreak->Ctrl) )
						CheckOk = FALSE;

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (ConstParam_p.UbattWinBreak->Ctrl &  EXE_BRANCH)
							NewPhase = ConstParam_p.UbattWinBreak->BranchPhase;
					}

				} // if (ConstParam_p.UbattWinBreak->Ctrl & EXE_CHECK) {
			} // if ( ConstParam_p.UbattWinBreak ) {
			break;
		case idUbattBreak0:
		case idUbattBreak1:
		case idUbattBreak2:
		case idUbattBreak3:
		case idUbattDelta0Break0:
		case idUbattDelta0Break1:
		case idUbattDelta1Break0:
		case idUbattDelta1Break1:
			UbattBreak_ConstParam_p = (UbattBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Valid pointer
			if ( UbattBreak_ConstParam_p ) {
				// Check Condition?
				if (UbattBreak_ConstParam_p->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;

					// Check Condition BreakValue
					if ( (UbattBreak_ConstParam_p->Ctrl & UBB_CHECK_BRVA_MASK) != UBB_CHECK_BRVA_EGAL ) {
						fSetValue = Module[id].Value.Float;
						// Get actual value
						switch (id) {
						case idUbattBreak0:
						case idUbattBreak1:
						case idUbattBreak2:
						case idUbattBreak3:
							fSetValue += CompValue.Uact.Tbatt;
							fActValue = CaiOutput_p->BatteryMeas.Ubatt;
							break;
						case idUbattDelta0Break0:
						case idUbattDelta0Break1:
							fActValue = DeltaValue[0].Avg.Delta.Ubatt;
							break;
						case idUbattDelta1Break0:
						case idUbattDelta1Break1:
							fActValue = DeltaValue[1].Delta.Ubatt;
							break;
						default:
							fActValue = NAN;
							break;
						}

						if ( Compare_float(id, fActValue, fSetValue) ) {
							if ( (UbattBreak_ConstParam_p->Ctrl & UBB_CHECK_BRVA_MASK) == UBB_CHECK_BRVA_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (UbattBreak_ConstParam_p->Ctrl & UBB_CHECK_BRVA_MASK) == UBB_CHECK_BRVA_TRUE )
								CheckOk = FALSE;
						}
					}

					// Check Condition Regulator
					if ( !CheckXerrWin(CaiInput_p, UbattBreak_ConstParam_p->Ctrl) )
						CheckOk = FALSE;

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (UbattBreak_ConstParam_p->Ctrl &  EXE_BRANCH)
							NewPhase = UbattBreak_ConstParam_p->BranchPhase;
					}

				} // if (UbattBreak_ConstParam_p->Ctrl & EXE_CHECK) {
			} // if ( UbattBreak_ConstParam_p ) {
			break;
		case idIbattBreak0:
		case idIbattBreak1:
		case idIbattBreak2:
		case idIbattBreak3:
		case idIbattDelta0Break0:
		case idIbattDelta0Break1:
		case idIbattDelta1Break0:
		case idIbattDelta1Break1:
			IbattBreak_ConstParam_p = (IbattBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Valid pointer?
			if ( IbattBreak_ConstParam_p ) {
				// Check Condition?
				if (IbattBreak_ConstParam_p->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;
					// Check Condition BreakValue
					if ( (IbattBreak_ConstParam_p->Ctrl & IBB_CHECK_BRVA_MASK) != IBB_CHECK_BRVA_EGAL ) {
						// Get actual value
						switch (id) {
						case idIbattBreak0:
						case idIbattBreak1:
						case idIbattBreak2:
						case idIbattBreak3:
							fActValue = CaiOutput_p->BatteryMeas.Ibatt;
							break;
						case idIbattDelta0Break0:
						case idIbattDelta0Break1:
							fActValue = DeltaValue[0].Avg.Delta.Ibatt;
							break;
						case idIbattDelta1Break0:
						case idIbattDelta1Break1:
							fActValue = DeltaValue[1].Delta.Ibatt;
							break;
						default:
							fActValue = NAN;
							break;
						}
						if ( Compare_float(id, fActValue, Module[id].Value.Float) ) {
							if ( (IbattBreak_ConstParam_p->Ctrl & IBB_CHECK_BRVA_MASK) == IBB_CHECK_BRVA_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (IbattBreak_ConstParam_p->Ctrl & IBB_CHECK_BRVA_MASK) == IBB_CHECK_BRVA_TRUE )
								CheckOk = FALSE;
						}
					}

					// Check Condition Regulator
					if ( !CheckXerrWin(CaiInput_p, IbattBreak_ConstParam_p->Ctrl) )
						CheckOk = FALSE;

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (IbattBreak_ConstParam_p->Ctrl &  EXE_BRANCH)
							NewPhase = IbattBreak_ConstParam_p->BranchPhase;
					}

				} // if (IbattBreak_ConstParam_p->Ctrl & EXE_CHECK) {
			} // if ( IbattBreak_ConstParam_p ) {
			break;
		case idPbattBreak0:
		case idPbattBreak1:
		case idPbattBreak2:
		case idPbattBreak3:
			// Todo
			break;
		case idPbattDelta0Break0:
		case idPbattDelta0Break1:
		case idPbattDelta1Break0:
		case idPbattDelta1Break1:
			// Todo
			break;
		case idTbattBreak0:
		case idTbattBreak1:
		case idTbattBreak2:
		case idTbattBreak3:
		case idTbattDelta0Break0:
		case idTbattDelta0Break1:
		case idTbattDelta1Break0:
		case idTbattDelta1Break1:
			TbattBreak_ConstParam_p = (TbattBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Valid pointer
			if ( TbattBreak_ConstParam_p ) {
				// Check Condition?
				if (TbattBreak_ConstParam_p->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;
					// Check Condition BreakValue
					if ( (TbattBreak_ConstParam_p->Ctrl & TBB_CHECK_BRVA_MASK) != TBB_CHECK_BRVA_EGAL ) {
						// Get actual value
						switch (id) {
						case idTbattBreak0:
						case idTbattBreak1:
						case idTbattBreak2:
						case idTbattBreak3:
							fActValue = CaiInput_p->Measure.Tbatt;
							break;
						case idTbattDelta0Break0:
						case idTbattDelta0Break1:
							fActValue = DeltaValue[0].Avg.Delta.Tbatt; // not used
							break;
						case idTbattDelta1Break0:
						case idTbattDelta1Break1:
							fActValue = DeltaValue[1].Delta.Tbatt; // not used
							break;
						default:
							fActValue = NAN;
							break;
						}
						if ( Compare_float(id, fActValue, Module[id].Value.Float) ) {
							if ( (TbattBreak_ConstParam_p->Ctrl & TBB_CHECK_BRVA_MASK) == TBB_CHECK_BRVA_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (TbattBreak_ConstParam_p->Ctrl & TBB_CHECK_BRVA_MASK) == TBB_CHECK_BRVA_TRUE )
								CheckOk = FALSE;
						}
					}

					// Check Condition Regulator
					if ( !CheckXerrWin(CaiInput_p, TbattBreak_ConstParam_p->Ctrl) )
						CheckOk = FALSE;

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (TbattBreak_ConstParam_p->Ctrl &  EXE_BRANCH)
							NewPhase = TbattBreak_ConstParam_p->BranchPhase;
					}

				} // if (TbattBreak_ConstParam_p->Ctrl & EXE_CHECK) {
			} // if ( TbattBreak_ConstParam_p ) {
			break;
		case idReguModeBreak0:
		case idReguModeBreak1:
			ReguModeBreak_p = (ReguModeBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Valid pointer?
			if ( ReguModeBreak_p ) {
				// Check Condition?
				if (ReguModeBreak_p->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;

					// Check Condition Voltage Mode
					if ( (ReguModeBreak_p->Ctrl & REGU_VOLTAGE_MODE_MASK) != REGU_VOLTAGE_MODE_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.VoltageIsNearSetValue) {
							if ( (ReguModeBreak_p->Ctrl & REGU_VOLTAGE_MODE_MASK) == REGU_VOLTAGE_MODE_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguModeBreak_p->Ctrl & REGU_VOLTAGE_MODE_MASK) == REGU_VOLTAGE_MODE_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Current Mode
					if ( (ReguModeBreak_p->Ctrl & REGU_CURRENT_MODE_MASK) != REGU_CURRENT_MODE_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.CurrentIsNearSetValue) {
							if ( (ReguModeBreak_p->Ctrl & REGU_CURRENT_MODE_MASK) == REGU_CURRENT_MODE_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguModeBreak_p->Ctrl & REGU_CURRENT_MODE_MASK) == REGU_VOLTAGE_MODE_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Power Mode
					if ( (ReguModeBreak_p->Ctrl & REGU_POWER_MODE_MASK) != REGU_POWER_MODE_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.PowerIsNearSetValue) {
							if ( (ReguModeBreak_p->Ctrl & REGU_POWER_MODE_MASK) == REGU_POWER_MODE_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguModeBreak_p->Ctrl & REGU_POWER_MODE_MASK) == REGU_POWER_MODE_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition UerrWin
					if ( (ReguModeBreak_p->Ctrl & REGU_UERRWIN_MASK) != REGU_UERRWIN_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.VoltageIsNearSetValue) {
							if ( (ReguModeBreak_p->Ctrl & REGU_UERRWIN_MASK) == REGU_UERRWIN_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguModeBreak_p->Ctrl & REGU_UERRWIN_MASK) == REGU_UERRWIN_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition IerrWin
					if ( (ReguModeBreak_p->Ctrl & REGU_IERRWIN_MASK) != REGU_IERRWIN_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.CurrentIsNearSetValue) {
							if ( (ReguModeBreak_p->Ctrl & REGU_IERRWIN_MASK) == REGU_IERRWIN_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguModeBreak_p->Ctrl & REGU_IERRWIN_MASK) == REGU_IERRWIN_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition PerrWin
					if ( (ReguModeBreak_p->Ctrl & REGU_PERRWIN_MASK) != REGU_PERRWIN_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.PowerIsNearSetValue) {
							if ( (ReguModeBreak_p->Ctrl & REGU_PERRWIN_MASK) == REGU_PERRWIN_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguModeBreak_p->Ctrl & REGU_PERRWIN_MASK) == REGU_PERRWIN_TRUE )
								CheckOk = FALSE;
						}
					}

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (ReguModeBreak_p->Ctrl &  EXE_BRANCH)
							NewPhase = ReguModeBreak_p->BranchPhase;
					}

				} // if (ReguBreak_p->Ctrl & EXE_CHECK) {
			} // if ( ReguBreak_p ) {
			break;
		case idReguDerateBreak0:
		case idReguDerateBreak1:
			ReguDerateBreak_p = (ReguDerateBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Valid pointer?
			if ( ReguDerateBreak_p ) {
				// Check Condition?
				if (ReguDerateBreak_p->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;

					// Check Condition Derate Temperature
					if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_TEMP_MASK) != REGU_DERATE_TEMP_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Derate.Detail.Temperature) {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_TEMP_MASK) == REGU_DERATE_TEMP_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_TEMP_MASK) == REGU_DERATE_TEMP_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Derate Uset
					if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_USET_MASK) != REGU_DERATE_USET_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.VoltageIsNearSetValue) {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_USET_MASK) == REGU_DERATE_USET_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_USET_MASK) == REGU_DERATE_USET_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Derate Iset
					if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_ISET_MASK) != REGU_DERATE_ISET_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.CurrentIsNearSetValue) {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_ISET_MASK) == REGU_DERATE_ISET_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_ISET_MASK) == REGU_DERATE_ISET_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Derate Pset
					if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_PSET_MASK) != REGU_DERATE_PSET_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.PowerIsNearSetValue) {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_PSET_MASK) == REGU_DERATE_PSET_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_PSET_MASK) == REGU_DERATE_PSET_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Derate Sum
					if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_SUM_MASK) != REGU_DERATE_SUM_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Info.Detail.VoltageIsNearSetValue && CaiInput_p->CcStatus.Detail.Info.Detail.CurrentIsNearSetValue && CaiInput_p->CcStatus.Detail.Info.Detail.PowerIsNearSetValue) {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_SUM_MASK) == REGU_DERATE_SUM_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguDerateBreak_p->Ctrl & REGU_DERATE_SUM_MASK) == REGU_DERATE_SUM_TRUE )
								CheckOk = FALSE;
						}
					}

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (ReguDerateBreak_p->Ctrl &  EXE_BRANCH)
							NewPhase = ReguDerateBreak_p->BranchPhase;
					}

				} // if (ReguBreak_p->Ctrl & EXE_CHECK) {
			} // if ( ReguBreak_p ) {
			break;
		case idReguEngineBreak0:
		case idReguEngineBreak1:
			ReguEngineBreak_p = (ReguEngineBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Valid pointer?
			if ( ReguEngineBreak_p ) {
				// Check Condition?
				if (ReguEngineBreak_p->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;

					// Check Condition Over Temperature
					if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_TEMP_MASK) != REGU_OVER_TEMP_EGAL ) {
						if ( CaiInput_p->CcStatus.Detail.Error.Detail.HighChargerTemperature) {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_TEMP_MASK) == REGU_OVER_TEMP_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_TEMP_MASK) == REGU_OVER_TEMP_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Over Voltage
					if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_VOLT_MASK) != REGU_OVER_VOLT_EGAL ) {
						if ( 0 ) {//CaiInput_p->CcStatus & REGUSTATUS_BIT_OVERVOLTAGE) {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_VOLT_MASK) == REGU_OVER_VOLT_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_VOLT_MASK) == REGU_OVER_VOLT_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Over Current
					if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_CURR_MASK) != REGU_OVER_CURR_EGAL ) {
						if ( 0 ) {//CaiInput_p->CcStatus & REGUSTATUS_BIT_OVERCURRENT) {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_CURR_MASK) == REGU_OVER_CURR_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_CURR_MASK) == REGU_OVER_CURR_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Over Power
					if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_POWER_MASK) != REGU_OVER_POWER_EGAL ) {
						if ( 0 ){//CaiInput_p->CcStatus & REGUSTATUS_BIT_OVERPOWER) {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_POWER_MASK) == REGU_OVER_POWER_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguEngineBreak_p->Ctrl & REGU_OVER_POWER_MASK) == REGU_OVER_POWER_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Can't Deliver Rated Output
					if ( (ReguEngineBreak_p->Ctrl & REGU_NO_RATED_OUTPUT_MASK) != REGU_NO_RATED_OUTPUT_MASK ) {
						if ( CaiInput_p->CcStatus.Detail.Derate.Sum ){
							if ( (ReguEngineBreak_p->Ctrl & REGU_NO_RATED_OUTPUT_MASK) == REGU_NO_RATED_OUTPUT_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguEngineBreak_p->Ctrl & REGU_NO_RATED_OUTPUT_MASK) == REGU_NO_RATED_OUTPUT_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Can't Deliver Output
					if ( (ReguEngineBreak_p->Ctrl & REGU_NO_OUTPUT_MASK) != REGU_NO_OUTPUT_MASK ) {
						if ( CaiInput_p->CcStatus.Detail.Derate.Sum ) {
							if ( (ReguEngineBreak_p->Ctrl & REGU_NO_OUTPUT_MASK) == REGU_NO_OUTPUT_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (ReguEngineBreak_p->Ctrl & REGU_NO_OUTPUT_MASK) == REGU_NO_OUTPUT_TRUE )
								CheckOk = FALSE;
						}
					}

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (ReguEngineBreak_p->Ctrl &  EXE_BRANCH)
							NewPhase = ReguEngineBreak_p->BranchPhase;
					}

				} // if (ReguBreak_p->Ctrl & EXE_CHECK) {
			} // if ( ReguBreak_p ) {
			break;
		case idInputBreak0:
		case idInputBreak1:
			InputBreak_p = (InputBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Valid pointer?
			if ( InputBreak_p ) {
				// Check Condition?
				if (InputBreak_p->Ctrl & EXE_CHECK) {
					CheckOk = TRUE;

					// Check Condition Stop
					if ( (InputBreak_p->Ctrl & INPU_CHECK_STOP_MASK) != INPU_CHECK_STOP_EGAL ) {
						if ( CaiInput_p->Input.Detail.Stop) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_STOP_MASK) == INPU_CHECK_STOP_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_STOP_MASK) == INPU_CHECK_STOP_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition F1
					if ( (InputBreak_p->Ctrl & INPU_CHECK_F1_MASK) != INPU_CHECK_F1_EGAL ) {
						if ( CaiInput_p->Input.Detail.F1) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_F1_MASK) == INPU_CHECK_F1_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_F1_MASK) == INPU_CHECK_F1_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition F2
					if ( (InputBreak_p->Ctrl & INPU_CHECK_F2_MASK) != INPU_CHECK_F2_EGAL ) {
						if ( CaiInput_p->Input.Detail.F2) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_F2_MASK) == INPU_CHECK_F2_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_F2_MASK) == INPU_CHECK_F2_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Remote In
					if ( (InputBreak_p->Ctrl & INPU_CHECK_REMOTEIN_MASK) != INPU_CHECK_REMOTEIN_EGAL ) {
						if ( CaiInput_p->Input.Detail.RemoteIn) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_REMOTEIN_MASK) == INPU_CHECK_REMOTEIN_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_REMOTEIN_MASK) == INPU_CHECK_REMOTEIN_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition Water Low
					if ( (InputBreak_p->Ctrl & INPU_CHECK_WATERLOW_MASK) != INPU_CHECK_WATERLOW_EGAL ) {
						if ( CaiInput_p->Input.Detail.WaterLow) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_WATERLOW_MASK) == INPU_CHECK_WATERLOW_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_WATERLOW_MASK) == INPU_CHECK_WATERLOW_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition CaiIn
					if ( (InputBreak_p->Ctrl & INPU_CHECK_CAIIN_MASK) != INPU_CHECK_CAIIN_EGAL ) {
						if ( CaiInput_p->Input.Detail.CaiIn) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_CAIIN_MASK) == INPU_CHECK_CAIIN_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_CAIIN_MASK) == INPU_CHECK_CAIIN_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition ManEquOn
					if ( (InputBreak_p->Ctrl & INPU_CHECK_MANEQUON_MASK) != INPU_CHECK_MANEQUON_EGAL ) {
						if ( CaiInput_p->Input.Detail.ManEquOn) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_MANEQUON_MASK) == INPU_CHECK_MANEQUON_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_MANEQUON_MASK) == INPU_CHECK_MANEQUON_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition ManEquOff
					if ( (InputBreak_p->Ctrl & INPU_CHECK_MANEQUOFF_MASK) != INPU_CHECK_MANEQUOFF_EGAL ) {
						if ( CaiInput_p->Input.Detail.ManEquOff) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_MANEQUOFF_MASK) == INPU_CHECK_MANEQUOFF_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_MANEQUOFF_MASK) == INPU_CHECK_MANEQUOFF_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition IoSpare3
					if ( (InputBreak_p->Ctrl & INPU_CHECK_IOSPARE3_MASK) != INPU_CHECK_IOSPARE3_EGAL ) {
						if ( CaiInput_p->Input.Detail.IoSpare3) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_IOSPARE3_MASK) == INPU_CHECK_IOSPARE3_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_IOSPARE3_MASK) == INPU_CHECK_IOSPARE3_TRUE )
								CheckOk = FALSE;
						}
					}
					// Check Condition IoSpare4
					if ( (InputBreak_p->Ctrl & INPU_CHECK_IOSPARE4_MASK) != INPU_CHECK_IOSPARE4_EGAL ) {
						if ( CaiInput_p->Input.Detail.IoSpare4) {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_IOSPARE4_MASK) == INPU_CHECK_IOSPARE4_FALS )
								CheckOk = FALSE;
						}
						else {
							if ( (InputBreak_p->Ctrl & INPU_CHECK_IOSPARE4_MASK) == INPU_CHECK_IOSPARE4_TRUE )
								CheckOk = FALSE;
						}
					}

					// Condition conclusion
					if ( CheckOk ) {
						Module[id].Status |= MSTATUS_BC_FULLFILLED;
						if (InputBreak_p->Ctrl &  EXE_BRANCH)
							NewPhase = InputBreak_p->BranchPhase;
					}

				} // if (InputBreak_p->Ctrl & EXE_CHECK) {
			} // if ( InputBreak_p ) {
			break;
		case idTimer0Break0:
		case idTimer0Break1:
		case idTimer1Break0:
		case idTimer1Break1:
		case idTimer2Break0:
		case idTimer2Break1:
			ConstParam_p.TimerBreak = (TimerBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Check valid pointer and there are any active Break condition
			if ( ConstParam_p.TimerBreak && (ConstParam_p.TimerBreak->Ctrl & EXE_CHECK) ) {
				// ActValue for TimerX
				// Could be defined in alg parameter
				switch (id) {
				case idTimer0Break0:
				case idTimer0Break1:
					uActValue = CounterValue.Timer[0];
					break;
				case idTimer1Break0:
				case idTimer1Break1:
					uActValue = CounterValue.Timer[1];
					break;
				case idTimer2Break0:
				case idTimer2Break1:
					uActValue = CounterValue.Timer[2];
					break;
				default:
					uActValue = 0;
					Error = Error_TimerBreak;
					break;
				}
				// Condition Conclusion
				if ( Compare_uint(id, uActValue, Module[id].Value.Uint) ) {
					Module[id].Status |= MSTATUS_BC_FULLFILLED;
					if (ConstParam_p.TimerBreak->Ctrl & EXE_BRANCH)  // Branch Active
						NewPhase = ConstParam_p.TimerBreak->BranchPhase;
				}
			} // if ( ConstParam_p.TimerBreak && (ConstParam_p.TimerBreak->Ctrl & EXE_CHECK) ) {
			break;
		case idAh0Break0:
		case idAh0Break1:
		case idAh1Break0:
		case idAh1Break1:
			ConstParam_p.AhBreak = (AhBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Check valid pointer and there are any active Break condition
			if ( ConstParam_p.AhBreak && (ConstParam_p.AhBreak->Ctrl & EXE_CHECK) ) {
				// ActValue for AhX
				// Could be defined in alg parameter
				switch (id) {
				case idAh0Break0:
				case idAh0Break1:
					fActValue = CounterValue.AmpSecCnt[0];
					break;
				case idAh1Break0:
				case idAh1Break1:
					fActValue = CounterValue.AmpSecCnt[1];
					break;
				default:
					fActValue = 0.0;
					Error = Error_AhBreak;
					break;
				}
				// Condition Conclusion
				if ( Compare_float(id, fActValue, Module[id].Value.Float) ) {
					Module[id].Status |= MSTATUS_BC_FULLFILLED;
					if (ConstParam_p.AhBreak->Ctrl & EXE_BRANCH)  // Branch Active
						NewPhase = ConstParam_p.AhBreak->BranchPhase;
				}
			} // if ( TimerBreak_p && (TimerBreak_p->Ctrl & TB_CNDA_BV) ) {
			break;
		case idWh0Break0:
		case idWh0Break1:
		case idWh1Break0:
		case idWh1Break1:
			ConstParam_p.WhBreak = (WhBreak_ConstParam_Type*) Module[id].PhaseAddr;
			// Check valid pointer and there are any active Break condition
			if ( ConstParam_p.WhBreak  && (ConstParam_p.WhBreak->Ctrl & EXE_CHECK) ) {
				// ActValue for TimerX
				// Could be defined in alg parameter
				switch (id) {
				case idWh0Break0:
					fActValue = CounterValue.WattSecCnt[0];
					break;
				default:
					fActValue = 0;
					Error = Error_TimerBreak;
					break;
				}
				// Condition Conclusion
				if ( Compare_float(id, fActValue, Module[id].Value.Float) ) {
					Module[id].Status |= MSTATUS_BC_FULLFILLED;
					if (ConstParam_p.WhBreak ->Ctrl & EXE_BRANCH)  // Branch Active
						NewPhase = ConstParam_p.WhBreak ->BranchPhase;
				}
			} // if ( ConstParam_p.WhBreak  && (WhBreak_p->Ctrl & EXE_CHECK) ) {
			break;
		case idAndBreak0:
		case idAndBreak1:
			AndBreak_p = (AndBreak_type*) Module[id].PhaseAddr;
			// Check valid pointer and there are any active Break condition
			if ( AndBreak_p && (AndBreak_p->Ctrl & EXE_CHECK) ) {
				CheckOk = TRUE;		// Consider condition true.

				// BreakModule0
				AndIndex = AndBreak_p->BreakModule[0];
				if ( (AndBreak_p->Ctrl & AND_BM0_ACT) && !(Module[AndIndex].Status & MSTATUS_BC_FULLFILLED) )
					CheckOk = FALSE;
				// BreakModule1
				AndIndex = AndBreak_p->BreakModule[1];
				if ( (AndBreak_p->Ctrl & AND_BM1_ACT) && !(Module[AndIndex].Status & MSTATUS_BC_FULLFILLED) )
					CheckOk = FALSE;
				// BreakModule2
				AndIndex = AndBreak_p->BreakModule[2];
				if ( (AndBreak_p->Ctrl & AND_BM2_ACT) && !(Module[AndIndex].Status & MSTATUS_BC_FULLFILLED) )
					CheckOk = FALSE;
				// BreakModule3
				AndIndex = AndBreak_p->BreakModule[3];
				if ( (AndBreak_p->Ctrl & AND_BM3_ACT) && !(Module[AndIndex].Status & MSTATUS_BC_FULLFILLED) )
					CheckOk = FALSE;

				// Condition Conclusion
				if (CheckOk) {
					Module[id].Status |= MSTATUS_BC_FULLFILLED;
					if (AndBreak_p->Ctrl & EXE_BRANCH)  // Branch Active
						NewPhase = AndBreak_p->BranchPhase;
				}

			} // if ( AndBreak_p && (AndBreak_p->Ctrl & EXE_CHECK) ) {
			break;
		case idCounter:
		case idDelta0:
		case idDelta1:
		case idOutput:
		case idChargingStage:
		case idChargingInfo:
		case idChargingError:
		case idModule_length:
			// Not a BreakModule, do nothing (just for for Lint).
			break;
		default:
			Error = Error_Break_WhileLoop;
			break;
		} // switch (id) {
		id++;
	} // while ( (id<=idModule_size) && (NewPhase == 0) ) {

	if (NewPhase) {
		Phase = NewPhase;
		PhaseInit(CaiInput_p, CaiOutput_p, NewPhase);
	}

}

/**************************************************************************
 Local Functions
 ***************************************************************************/

/**************************************************************************
 Name:		CheckXerrWin
 Does:		Check if regulator status is full filled.
 Returns:
 **************************************************************************/
static bool CheckXerrWin(const CaiInput_type *CaiInput_p, uint32 Ctrl) {
	bool CheckOk = TRUE;


	// Check Condition UerrWin
	if ( (Ctrl & REGU_CHECK_UERR_MASK) != REGU_CHECK_UERR_EGAL ) {
		if ( CaiInput_p->CcStatus.Detail.Info.Detail.VoltageIsNearSetValue) {
			if ( (Ctrl & REGU_CHECK_UERR_MASK) == REGU_CHECK_UERR_FALS )
				CheckOk = FALSE;
		}
		else {
			if ( (Ctrl & REGU_CHECK_UERR_MASK) == REGU_CHECK_UERR_TRUE )
				CheckOk = FALSE;
		}
	}

	// Check Condition IerrWin
	if ( (Ctrl & REGU_CHECK_IERR_MASK) != REGU_CHECK_IERR_EGAL ) {
		if ( CaiInput_p->CcStatus.Detail.Info.Detail.CurrentIsNearSetValue) {
			if ( (Ctrl & REGU_CHECK_IERR_MASK) == REGU_CHECK_IERR_FALS ) {
				CheckOk = FALSE;
			}
		}
		else {
			if ( (Ctrl & REGU_CHECK_IERR_MASK) == REGU_CHECK_IERR_TRUE )
				CheckOk = FALSE;
		}
	}

	// Check Condition PerrWin
	if ( (Ctrl & REGU_CHECK_PERR_MASK) != REGU_CHECK_PERR_EGAL ) {
		if ( CaiInput_p->CcStatus.Detail.Info.Detail.PowerIsNearSetValue) {
			if ( (Ctrl & REGU_CHECK_PERR_MASK) == REGU_CHECK_PERR_FALS ) {
				CheckOk = FALSE;
			}
		}
		else {
			if ( (Ctrl & REGU_CHECK_PERR_MASK) == REGU_CHECK_PERR_TRUE )
				CheckOk = FALSE;
		}
	}


	return CheckOk;
}
/**************************************************************************
 Name:		Compare
 Does:		Compare Act value with Break value.
 Returns:
 **************************************************************************/
static bool Compare_float(idModule_enum id, float ActValue, float BreakValue) {
	bool Fullfilled;
	uint32 Comparator;


	/* Check if break condition full-filled */
	/* ==================================== */
	Fullfilled = FALSE;
	Comparator = ((Module_PhaseCtrl_Type*) Module[id].PhaseAddr)->Ctrl & CMP_MASK;
	switch (Comparator) {
		case CMP_GR:		/* >  (Greather than) */
			if (ActValue > BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_GREQ:	/* >= (Greather than or Equal) */
			if (ActValue >= BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_EQ:		/* == (Equal) */
			if (ActValue == BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_LEEQ:	/* <= (Lesser than or Equal) */
			if (ActValue <= BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_LE:		/* <  (Lesser than) */
			if (ActValue < BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_NOEQ:		/* != (Not equal) */
			if (ActValue != BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		default:
			Fullfilled = FALSE;
			Error = Error_Compare;
			break;
	} // switch (Operator) {

	return Fullfilled;
}


/**************************************************************************
 Name:		Compare_uint
 Does:		Compare Act value with Break value.
 Returns:
 **************************************************************************/
static bool Compare_uint(idModule_enum id, uint32 ActValue, uint32 BreakValue) {
	bool Fullfilled;
	uint32 Comparator;


	/* Check if break condition full-filled */
	/* ==================================== */
	Fullfilled = FALSE;
	Comparator = ((Module_PhaseCtrl_Type*) Module[id].PhaseAddr)->Ctrl & CMP_MASK;
	switch (Comparator) {
		case CMP_GR:		/* >  (Greather than) */
			if (ActValue > BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_GREQ:	/* >= (Greather than or Equal) */
			if (ActValue >= BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_EQ:		/* == (Equal) */
			if (ActValue == BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_LEEQ:	/* <= (Lesser than or Equal) */
			if (ActValue <= BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_LE:		/* <  (Lesser than) */
			if (ActValue < BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		case CMP_NOEQ:		/* != (Not equal) */
			if (ActValue != BreakValue) {
				Fullfilled = TRUE;
			}
			break;
		default:
			Fullfilled = FALSE;
			Error = Error_Compare;
			break;
	} // switch (Operator) {

	return Fullfilled;
}
/***************************************************************************
 End of File
 ***************************************************************************/
