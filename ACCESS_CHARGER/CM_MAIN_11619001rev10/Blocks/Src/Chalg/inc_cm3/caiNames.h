#ifndef CAI_NAMES_H
#define CAI_NAMES_H

#include "cai_constparam.h"
#include "caimain.h"

typedef struct{
	int textLen;
	const char* text;
} text_type;

typedef struct{
	int bits;
	const text_type* names;
} bitNames_type;


/* Units */
const char* unitName(UserParam_Unit_enum unit);
int unitNameLen(UserParam_Unit_enum unit);

/* Charging stages */
const char* ChargingStageName(CharginStage_type stage);
int ChargingStageNameLen(CharginStage_type stage);

/* Instant info */
const bitNames_type* ChargingInfoNames(void);

/* Instant error */
const bitNames_type* ChargingErrorNames(void);

#endif
