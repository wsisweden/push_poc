#include "caiNames.h"
#include <string.h>


/* Units names */
static const char* unitNames[UserParam_Unit_size] = {
		[UserParam_Unit_VoltPerCell] = " VPC",
		[UserParam_Unit_Capacity] = " %Cap",
		[UserParam_Unit_CellMulCap] = " Cell*Ah",
		[UserParam_Unit_Second] = " s",
		[UserParam_Unit_Minute] = " hh:mm",
		[UserParam_Unit_Hour] = " hh:mm",
		[UserParam_Unit_Percent] = " %",
		[UserParam_Unit_Integer] = " n",
		[UserParam_Unit_Ah] = " *Ah"
};

const char* unitName(UserParam_Unit_enum unit){
	return unitNames[unit];
}

int unitNameLen(UserParam_Unit_enum unit){
	return strlen(unitName(unit));
}

/* Charging Stages names */
static const char* stageNames[ChargingStage_Length] = {
		[ChargingStage_Idle] = "Idle",
		[ChargingStage_Pre] = "Pre",
		[ChargingStage_I1] = "I1",
		[ChargingStage_U1] = "U1",
		[ChargingStage_Additional] = "Additional",
		[ChargingStage_Maintenance] = "Maintenance",
		[ChargingStage_EQU] = "EQU",
		[ChargingStage_Spare2] = "I0",
		[ChargingStage_Spare3] = "Spare3",
		[ChargingStage_EquMan] = "EQUMAN"
};

const char* ChargingStageName(CharginStage_type stage){
	return stageNames[stage];
}

int ChargingStageNameLen(CharginStage_type stage){
	return strlen(ChargingStageName(stage));
}

/* ChargingInfo_type bit names */
static const text_type infoNames[] = {[0 ... 11] = {11, "Spare 0..11"},
									  [12] = {17, "Battery connected"},
									  [13] = {12, "Pre charging"},
									  [14] = {13, "Main charging"},
									  [15] = {10, "Completion"},
									  [16] = {7, "Trickle"},
									  [17] = {12, "Equalization"},
									  [18] = {5, "Ready"},
									  [19] = {13, "T battery low"},
									  [20] = {14, "T battery high"},
									  [21 ... 31] = {12, "Spare 21..31"}};

static const bitNames_type infoNamedBits = {32, infoNames};

const bitNames_type* ChargingInfoNames(void){
	return &infoNamedBits;
}

/* ChargingError_type bit names */
static const text_type errorNames[] = {[0 ... 11] = {11, "Spare 0..11"},
									   [12] = {10, "VoltageLow"},
									   [13] = {11, "VoltageHigh"},
									   [14] = {14, "TemperatureLow"},
									   [15] = {15, "TemperatureHigh"},
									   [16] = {5, "AhMax"},
									   [17] = {5, "WhMax"},
									   [18] = {6, "DeltaU"},
									   [19] = {6, "DeltaI"},
									   [20] = {6, "DeltaP"},
									   [21] = {10, "DeltaTbatt"},
									   [22] = {7, "TimeMax"},
									   [23] = {14, "AirPumpFailure"},	// Using spare 23
									   [24 ... 31] = {12, "Spare 24..31"}};

static const bitNames_type errorNamedBits = {32, errorNames};

const bitNames_type* ChargingErrorNames(void){
	return &errorNamedBits;
}
