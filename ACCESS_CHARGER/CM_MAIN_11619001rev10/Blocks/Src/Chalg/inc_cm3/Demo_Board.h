/*
$Revision:$
$Date:$
$Author:$
$HeadURL:$
*/

// Filename: Demo_board
// Date:     2014-01-13 10:37:16
// Author:   


#include "cai_constparam.h"

// Make the definition unique, so we be able to compile several algorithms
#undef ID
#define ID(i) Demo_board_##i

#define MILLI *0.001
#define PERCE *0.01
#define SECOND *1
#define MINUT *60
#define HOUR *60*60

// Definition of location for Identity, Mandatory and Module
// ---------------------------------------------------------
extern const AlgDef_ConstPar_Type ID(AlgDef_ConstPar);
extern const Identity_ConstPar_Type ID(Identity_ConstPar);
extern const Mandatory_ConstPar_Type ID(Mandatory_ConstPar);
extern const Module_ConstPar_Type ID(Module_ConstPar[idModule_length]);
const AlgDef_ConstPar_Type ID(AlgDef_ConstPar) = {
  (uint32) &ID(AlgDef_ConstPar),
  (uint32) &ID(Identity_ConstPar),
  (uint32) &ID(Mandatory_ConstPar),
  (uint32) &ID(Module_ConstPar)
};


// Identity
// --------
const char ID(Name[]) =         "Demo-board";

const Identity_ConstPar_Type ID(Identity_ConstPar) = {
       1,  // IdNumber
       1,  // Version
       1,  // InterpreterVer
  { (uint32) ID(Name), sizeof(ID(Name)) }
};


// Mandatory
// ---------
const Power_type ID(Power[]) = {
//  Volt/Cell     Capacity
//  -----------   -----------
  {       1.000,     20 PERCE, },
  {       2.400,     20 PERCE, },
  {       2.800,      5 PERCE, },
  {       2.900,            0, }
};

const Uaccuracy_type ID(Uaccuracy[]) = {
//  Volt/Cell     Accuracy (V)
//  -----------   -----------
  {       1.000,     10 MILLI, },
  {       2.400,     10 MILLI, },
  {       2.800,     20 MILLI, },
  {       2.900,     40 MILLI, }
};


const Iaccuracy_type ID(Iaccuracy[]) = {
//  Capcaity      Accuracy (A)
//  -----------   -----------
  {           0,      1 PERCE, },
  {     1 PERCE,      1 PERCE, },
  {    10 PERCE,    2.5 PERCE, },
  {    20 PERCE,      5 PERCE, }
};

const uint32 ID(IOsignals) = 
//IO_Tbatt   IO_AirPump   IO_Water
//--------   ----------   --------
  0        | 0          | 0       ;



// UserParam
const char ID(UserParam_Name00[]) = "Pre_Uset";
const char ID(UserParam_Name01[]) = "Pre_Iset";
const char ID(UserParam_Name02[]) = "I1_Uset";
const char ID(UserParam_Name03[]) = "I1_Iset";
const UserParam_ConstParam_Type ID(UserParam_ConstParam[]) = {
//   Name Pointer          Name Size                    Default       Min           Max           Unit                       Operator                      Par A   Par B
//   ------------          ---------                    ------------  ------------  ------------  -------------------------  ----------------------------  ------  ------
   { ID(UserParam_Name00), sizeof(ID(UserParam_Name00)),UserParam_Unit_VoltPerCell,          1,4,            0,          1,8, UserParam_Operator_Lesser  ,      1,      1 },
   { ID(UserParam_Name01), sizeof(ID(UserParam_Name01)),UserParam_Unit_VoltPerCell,          1,8,          1,4,            2, UserParam_Operator_Lesser  ,      0,      1 },
   { ID(UserParam_Name02), sizeof(ID(UserParam_Name02)),UserParam_Unit_VoltPerCell,          2,5,          1,8,          2,8, UserParam_Operator_No      ,      0,      1 },
   { ID(UserParam_Name03), sizeof(ID(UserParam_Name03)),UserParam_Unit_VoltPerCell,          2,5,          1,8,          2,8, UserParam_Operator_No      ,      0,      1 }
};


// Mandatory location
const Mandatory_ConstPar_Type ID(Mandatory_ConstPar) = {
  { (uint32) ID(Power), sizeof(ID(Power))/sizeof(Power_type) },
  { (uint32) ID(Uaccuracy), sizeof(ID(Uaccuracy))/sizeof(Uaccuracy_type) },
  { (uint32) ID(Iaccuracy), sizeof(ID(Iaccuracy))/sizeof(Iaccuracy_type) },
    (uint32) &ID(IOsignals),
  { (uint32) ID(UserParam_ConstParam), sizeof(ID(UserParam_ConstParam))/sizeof(UserParam_ConstParam_Type) },
};


// Module
// ------
#define Demo_board_ChargingStage_Name  0
#define Demo_board_ChargingStage_Idle  1
#define Demo_board_ChargingStage_Pre  2
#define Demo_board_ChargingStage_I1  3
#define Demo_board_ChargingStage_U1  4
#define Demo_board_ChargingStage_Completion  5
#define Demo_board_ChargingStage_Trickle  6
#define Demo_board_ChargingStage_Equalize  7
#define Demo_board_ChargingStage_spare2  8
#define Demo_board_ChargingStage_spare3  9
const ChargingStage_Param_type Demo_board_ChargingStage[] = {
// Phase   Next Phase  Stage                                     
// ------  ----------  --------------------------------
  {    10,         11, Demo_board_ChargingStage_Pre },
  {    20,         21, Demo_board_ChargingStage_I1 },
  {    30,         31, Demo_board_ChargingStage_U1 },
  {    40,         41, Demo_board_ChargingStage_Completion },
  {    50,         51, Demo_board_ChargingStage_Trickle }
};

const Output_ConstParam_Type ID(Output[]) = {
// Phase   Continou   Init       Check      Led Green              Led Yellow              Led Red              Discrete Out1          Water Fill            Air Pump             IOmodule spare 0          IOmodule spare 1       
// ------  --------   --------   ---------  --------------------   ---------------------   ------------------   --------------------   -------------------   ------------------   -----------------------   -----------------------
  {     1, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    10, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    20, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    30, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    40, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_ON      | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {    50, 0        | EXE_INIT | 0        , OUTPUT_GREEN_ON      | OUTPUT_YELLOW_OFF     | OUTPUT_RED_OFF     | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_ON      | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   100, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_ON      | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   101, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_ON      | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    },
  {   102, 0        | EXE_INIT | 0        , OUTPUT_GREEN_OFF     | OUTPUT_YELLOW_OFF     | OUTPUT_RED_ON      | OUTPUT_DOUT1_EGAL    | OUTPUT_WATER_OFF     | OUTPUT_AIR_OFF     | OUTPUT_IOSPARE0_EGAL    | OUTPUT_IOSPARE1_EGAL    }
};

const ChargingInfo_Param_type ID(ChargingInfo[]) = {
// Phase   Continou   Init       Check       Battery Connected     Pre Charging   Main Charging     Completion       Trickle          Equalization     Ready            Temper. Low      Temper. High
// ------  --------   --------   ---------   -------------------   -------------  ---------------   --------------   --------------   --------------   --------------   --------------   --------------
  {     1, 0        | EXE_INIT | 0         | 0                   | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {    10, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | CHRG_INFO_PRE | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {    20, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | CHRG_INFO_MAIN | 0              | 0              | 0              | 0              | 0              | 0              },
  {    30, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | CHRG_INFO_MAIN | 0              | 0              | 0              | 0              | 0              | 0              },
  {    40, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | CHRG_INFO_COMP | 0              | 0              | 0              | 0              | 0              },
  {    50, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | CHRG_INFO_TRIC | 0              | CHRG_INFO_REDY | 0              | 0              },
  {   100, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {   101, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              },
  {   102, 0        | EXE_INIT | 0         | CHRG_INFO_BATT_CONN | 0             | 0              | 0              | 0              | 0              | 0              | 0              | 0              }
};

const ChargingError_Param_type ID(ChargingError[]) = {
// Phase   Continou   Init       Check       Voltage Low      Voltage High     Tbatt Low        Tbastt High      Ah Max           Ph Max           delta U        delta I        delta P        delta Tbatt      Time Max
// ------  --------   --------   ---------   --------------   --------------   --------------   --------------   --------------   --------------   ------------   ------------   ------------   --------------   -------------
  {     1, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    10, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    20, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    30, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    40, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {    50, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {   100, 0        | EXE_INIT | 0         | CHRG_ERRO_VOLO | 0              | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {   101, 0        | EXE_INIT | 0         | 0              | CHRG_ERRO_VOHI | 0              | 0              | 0              | 0              | 0            | 0            | 0            | 0              | 0              },
  {   102, 0        | EXE_INIT | 0         | 0              | 0              | 0              | 0              | CHRG_ERRO_AHMA | 0              | 0            | 0            | 0            | 0              | 0              }
};

const RegWin_ConstParam_Type ID(RegWin[]) = {
// Phase   Continou   UserParam Uerr     UserParam Uerr     UserParam Uerr     UerrWin       IerrWin       PerrWin
// ------  --------   ----------------   ----------------   ----------------   ------------  ------------  ------------
  {     1, EXE_CONT | 0                | 0                | 0                ,     10 MILLI,   0.05 PERCE,      1 PERCE }
};

const ReguOnOff_ConstParam_Type ID(ReguOn[]) = {
// Phase   Continou   UserParam Ubatt Low        UserParam Ubatt High        Condition Ubatt               Condition Remote In               Condition Cai In             Ubatt Low     Ubatt High
// ------  --------   ------------------------   -------------------------   ---------------------------   -------------------------------   ---------------------------  ------------  ------------
  {     1, EXE_CONT | 0                        | 0                         | REGUONOFF_COND_UACT_TRUE    | REGUONOFF_COND_REMOTEIN_NOTUSED | REGUONOFF_COND_CAIN_NOTUSED,          1,4,          2,5 }
};

const ReguOnOff_ConstParam_Type ID(ReguOff[]) = {
// Phase   Continou   UserParam Ubatt Low        UserParam Ubatt High        Condition Ubatt               Condition Remote In               Condition Cai In             Ubatt Low     Ubatt High
// ------  --------   ------------------------   -------------------------   ---------------------------   -------------------------------   ---------------------------  ------------  ------------
  {     1, EXE_CONT | 0                        | 0                         | REGUONOFF_COND_UACT_FALSE   | REGUONOFF_COND_REMOTEIN_NOTUSED | REGUONOFF_COND_CAIN_NOTUSED,         1,35,          2,9 }
};

const RegSet_ConstParam_Type ID(RegSet_ConstParam[]) = {
//  Phase  Continou   Uset UserPar.   Iset UserPar.   Pset UserPar.   Mode               Uset Active    Iset Active    Pset Active   Uset Value     Iset Value    Pset Value  
//  -----  --------   -------------   -------------   -------------   ----------------   ------------   ------------   ------------  -------------  ------------  ------------
  {    11, 0        | 0             | 0             | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,          2.40,     10 PERCE,            0 },
  {    12, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | 0            | 0            | 0           ,          2.40,     10 PERCE,            0 },
  {    21, EXE_CONT | 0             | 0             | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,           2,5,     20 PERCE,            0 },
  {    31, EXE_CONT | 0             | 0             | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,           2,5,     20 PERCE,            0 },
  {    40, EXE_CONT | 0             | 0             | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,           2,8,      5 PERCE,            0 },
  {    51, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | REGSET_U_ACT | REGSET_I_ACT | 0           ,          2.80,     20 PERCE,            0 },
  {    52, 0        | 0             | 0             | 0             | REGSET_MODE_OFF  | REGSET_U_ACT | REGSET_I_ACT | 0           ,          2.80,     20 PERCE,            0 },
  {    53, 0        | 0             | 0             | 0             | REGSET_MODE_ON   | REGSET_U_ACT | REGSET_I_ACT | 0           ,          2.80,      5 PERCE,            0 }
};

const UbattWinBreak_ConstParam_Type ID(UbattWinBreak0[]) = {
// Phase   Continou   Init       Check       Userparam Ulow       Userparam Ulow        Check UbattWinn            Check UerrWin          Check IerrWin          Check PerrWin          Branch Act   Ulow         Uhigh         BranchPhase
// ------  --------   --------   ---------   ------------------   -------------------   ------------------------   --------------------   --------------------   --------------------   -----------  -----------  ------------  ------     
  {     1, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.4,          1.8,     10 },
  {   100, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.4,          1.8,     10 },
  {   101, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1,4,          1.8,     10 }
};

const UbattWinBreak_ConstParam_Type ID(UbattWinBreak1[]) = {
// Phase   Continou   Init       Check       Userparam Ulow       Userparam Ulow        Check UbattWinn            Check UerrWin          Check IerrWin          Check PerrWin          Branch Act   Ulow         Uhigh         BranchPhase
// ------  --------   --------   ---------   ------------------   -------------------   ------------------------   --------------------   --------------------   --------------------   -----------  -----------  ------------  ------     
  {     1, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.8,          2,5,     20 },
  {   100, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.8,          2,4,     20 },
  {   101, 0        | 0        | EXE_CHECK | 0                  | 0                   | UBATTWIN_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.4,          2,4,     20 }
};

const UbattBreak_ConstParam_Type ID(UbattBreak0[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparat   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   --------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {     1, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_LEEQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | 0         ,          1.4,    100 },
  {    11, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.8,     20 },
  {    20, 0        | EXE_INIT | 0         | 0               | CMP_GR   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_EGAL | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          2,5,     30 },
  {    22, 0        | 0        | EXE_CHECK | 0               | CMP_GREQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_EGAL | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1.8,     30 },
  {    52, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_LE   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,         2.17,     53 },
  {   101, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_LE   | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          1,4,      1 }
};

const UbattBreak_ConstParam_Type ID(UbattBreak1[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparat   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   --------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {     1, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GREQ | UBB_P0_CELL | OP_MUL | UBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,          2,9,    101 }
};

const Counter_type ID(Counter[]) = {
//                    **** Timer0 *****   **** Timer1 *****   **** Timer2 *****   ****** Ah0 ******   ****** Ah1 ******   ****** Wh0 ******   ****** Wh1 ******
//  Phase  Continou   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *   * Reset    Run  *
//  -----  --------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------   -------
  {     1, 0        | TI0_RES | 0       | TI1_RES | 0       | TI2_RES | 0       | AH0_RES | 0       | AH1_RES | 0       | WH0_RES | 0       | WH1_RES | 0       },
  {    10, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       | 0       },
  {    20, EXE_CONT | TI0_RES | TI0_RUN | TI1_RES | 0       | 0       | 0       | AH0_RES | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       },
  {    51, EXE_CONT | 0       | TI0_RUN | 0       | 0       | 0       | 0       | 0       | AH0_RUN | 0       | 0       | 0       | 0       | 0       | 0       }
};

const TimerBreak_ConstParam_Type ID(Timer0Break0[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparato   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   ---------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    10, 0        | EXE_INIT | 0         | 0               | CMP_GR   | TIB_P0_TIM0 | OP_NO  | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      1 MINUT,    100 },
  {    11, 0        | 0        | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_NO  | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       1 HOUR,    100 },
  {    21, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    15 SECOND,     22 },
  {    30, 0        | EXE_INIT | 0         | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    15 SECOND,     32 },
  {    31, 0        | 0        | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       1 HOUR,     32 },
  {    40, 0        | EXE_INIT | 0         | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | 0         ,    15 SECOND,    102 },
  {    41, 0        | 0        | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,       1 HOUR,     42 },
  {    51, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    15 SECOND,     52 },
  {    53, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    15 SECOND,     51 }
};

const TimerBreak_ConstParam_Type ID(Timer0Break1[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparato   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   ---------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    11, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      1 MINUT,     12 },
  {    12, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      1 MINUT,     11 },
  {    30, 0        | EXE_INIT | 0         | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      5 MINUT,     11 },
  {    32, 0        | 0        | EXE_CHECK | 0               | CMP_GR   | TIB_P0_TIM0 | OP_PLU | TIB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      1 MINUT,     40 }
};

const AhBreak_ConstParam_Type ID(Ah0Break0[]) = {
// Phase   Continou   Init       UserParam Par1    Check       UserParam Par1   Comparato   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------------   ---------   ---------------  ---------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    20, 0        | EXE_INIT | 0         | 0               | CMP_GREQ | AHB_P0_CAPA | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    100 PERCE,    103 },
  {    22, 0        | 0        | EXE_CHECK | 0               | CMP_GREQ | AHB_P0_CAPA | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,    100 PERCE,    102 },
  {    40, 0        | EXE_INIT | 0         | 0               | CMP_GREQ | AHB_P0_AH0  | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,         1.15,    103 },
  {    42, 0        | 0        | EXE_CHECK | 0               | CMP_GREQ | AHB_P0_CAPA | OP_MUL | AHB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,         1.15,     50 }
};

const IbattBreak_ConstParam_Type ID(IbattBreak0[]) = {
// Phase   Continou   Init       Check       UserParam Par1    Comparat   Param0        Operat   Check BreakValue      Check UerrWin          Check IerrWin          Check PerrWin         Branch Act   Param1        BranchPhase
// ------  --------   --------   ---------   ---------------   --------   -----------   ------   -------------------   --------------------   --------------------   --------------------  -----------  ------------  ------     
  {    30, 0        | EXE_INIT | 0         | 0               | CMP_LE   | IBB_P0_CAPA | OP_MUL | IBB_CHECK_BRVA_TRUE | REGU_CHECK_UERR_TRUE | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | 0         ,      5 PERCE,     40 },
  {    32, 0        | EXE_INIT | EXE_CHECK | 0               | CMP_LE   | IBB_P0_CAPA | OP_MUL | IBB_CHECK_BRVA_EGAL | REGU_CHECK_UERR_EGAL | REGU_CHECK_IERR_EGAL | REGU_CHECK_PERR_EGAL | EXE_BRANCH,      5 PERCE,     40 }
};

const AndBreak_type ID(AndBreak0[]) = {
// Phase   Continou   Init       Check      BM0 Active     BM1 Active    BM2 Active    BM3 Active    Branch Act    BM0 Identity           BM1 Identity           BM2 Identity           BM3 Identity             BranchPhase
// ------  --------   --------   ---------  ------------   -----------   -----------   -----------   ----------    ---------------------  ---------------------  ---------------------  ---------------------    ------
  {    51, 0        | EXE_INIT | EXE_CHECK | AND_BM0_ACT | AND_BM1_ACT | 0           | 0           | EXE_BRANCH, {        idTimer0Break0,         idUbattBreak0,   idTbattDelta1Break1,         idUbattBreak1 },     52 }
};

// Module location
// Must match idModule_enum
const Module_ConstPar_Type ID(Module_ConstPar[idModule_length]) = {
  { (void*) ID(RegSet_ConstParam), sizeof(ID(RegSet_ConstParam))/sizeof(RegSet_ConstParam_Type) },
  { (void*) ID(RegWin), sizeof(ID(RegWin))/sizeof(RegWin_ConstParam_Type) },
  { (void*) ID(ReguOn), sizeof(ID(ReguOn))/sizeof(ReguOnOff_ConstParam_Type) },
  { (void*) ID(ReguOff), sizeof(ID(ReguOff))/sizeof(ReguOnOff_ConstParam_Type) },
  { 0, 0 }, // idUcompTbatt, not defined
  { 0, 0 }, // idIcompTbatt, not defined
  { (void*) ID(UbattWinBreak0), sizeof(ID(UbattWinBreak0))/sizeof(UbattWinBreak_ConstParam_Type) },
  { (void*) ID(UbattWinBreak1), sizeof(ID(UbattWinBreak1))/sizeof(UbattWinBreak_ConstParam_Type) },
  { 0, 0 }, // idUbattWinBreak2, not defined
  { 0, 0 }, // idUbattWinBreak3, not defined
  { (void*) ID(UbattBreak0), sizeof(ID(UbattBreak0))/sizeof(UbattBreak_ConstParam_Type) },
  { (void*) ID(UbattBreak1), sizeof(ID(UbattBreak1))/sizeof(UbattBreak_ConstParam_Type) },
  { 0, 0 }, // idUbattBreak2, not defined
  { 0, 0 }, // idUbattBreak3, not defined
  { 0, 0 }, // idUbattDelta0Break0, not defined
  { 0, 0 }, // idUbattDelta0Break1, not defined
  { 0, 0 }, // idUbattDelta1Break0, not defined
  { 0, 0 }, // idUbattDelta1Break1, not defined
  { (void*) ID(IbattBreak0), sizeof(ID(IbattBreak0))/sizeof(IbattBreak_ConstParam_Type) },
  { 0, 0 }, // idIbattBreak1, not defined
  { 0, 0 }, // idIbattBreak2, not defined
  { 0, 0 }, // idIbattBreak3, not defined
  { 0, 0 }, // idIbattDelta0Break0, not defined
  { 0, 0 }, // idIbattDelta0Break1, not defined
  { 0, 0 }, // idIbattDelta1Break0, not defined
  { 0, 0 }, // idIbattDelta1Break1, not defined
  { 0, 0 }, // idPbattBreak0, not defined
  { 0, 0 }, // idPbattBreak1, not defined
  { 0, 0 }, // idPbattBreak2, not defined
  { 0, 0 }, // idPbattBreak3, not defined
  { 0, 0 }, // idPbattDelta0Break0, not defined
  { 0, 0 }, // idPbattDelta0Break1, not defined
  { 0, 0 }, // idPbattDelta1Break0, not defined
  { 0, 0 }, // idPbattDelta1Break1, not defined
  { 0, 0 }, // idTbattBreak0, not defined
  { 0, 0 }, // idTbattBreak1, not defined
  { 0, 0 }, // idTbattBreak2, not defined
  { 0, 0 }, // idTbattBreak3, not defined
  { 0, 0 }, // idTbattDelta0Break0, not defined
  { 0, 0 }, // idTbattDelta0Break1, not defined
  { 0, 0 }, // idTbattDelta1Break0, not defined
  { 0, 0 }, // idTbattDelta1Break1, not defined
  { 0, 0 }, // idReguModeBreak0, not defined
  { 0, 0 }, // idReguModeBreak0, not defined
  { 0, 0 }, // idReguDerateBreak0, not defined
  { 0, 0 }, // idReguDerateBreak1, not defined
  { 0, 0 }, // idReguEngineBreak0, not defined
  { 0, 0 }, // idReguEngineBreak1, not defined
  { 0, 0 }, // idInputBreak0, not defined
  { 0, 0 }, // idInputBreak1, not defined
  { (void*) ID(Timer0Break0), sizeof(ID(Timer0Break0))/sizeof(TimerBreak_ConstParam_Type) },
  { (void*) ID(Timer0Break1), sizeof(ID(Timer0Break1))/sizeof(TimerBreak_ConstParam_Type) },
  { 0, 0 }, // idTimer1Break0, not defined
  { 0, 0 }, // idTimer1Break1, not defined
  { 0, 0 }, // idTimer2Break0, not defined
  { 0, 0 }, // idTimer2Break1, not defined
  { (void*) ID(Ah0Break0), sizeof(ID(Ah0Break0))/sizeof(AhBreak_ConstParam_Type) },
  { 0, 0 }, // idAh0Break1, not defined
  { 0, 0 }, // idAh1Break0, not defined
  { 0, 0 }, // idAh1Break1, not defined
  { 0, 0 }, // idWh0Break0, not defined
  { 0, 0 }, // idWh0Break1, not defined
  { 0, 0 }, // idWh1Break0, not defined
  { 0, 0 }, // idWh1Break1, not defined
  { (void*) ID(AndBreak0), sizeof(ID(AndBreak0))/sizeof(AndBreak_type) },
  { 0, 0 }, // idAndBreak1, not defined
  { (void*) ID(Counter), sizeof(ID(Counter))/sizeof(Counter_type) },
  { 0, 0 }, // idDelta0, not defined
  { 0, 0 }, // idDelta1, not defined
  { (void*) ID(Output), sizeof(ID(Output))/sizeof(Output_ConstParam_Type) },
  { (void*) ID(ChargingStage), sizeof(ID(ChargingStage))/sizeof(ChargingStage_Param_type) },
  { (void*) ID(ChargingInfo), sizeof(ID(ChargingInfo))/sizeof(ChargingInfo_Param_type) },
  { (void*) ID(ChargingError), sizeof(ID(ChargingError))/sizeof(ChargingError_Param_type) }
};

