#include "regu_batteryDetection.h"
#include "engines_types.h"
#include "meas.h"					// To use Volt2ADC
#include "regu.h"

static Uint8 regu_BatteryVoltageRipple(const ControlLoopMeas_type* meas, const regu_BatteryDetect_type* BatteryDetectionVars);
static Uint32 regu_AverageVoltage(const ControlLoopMeas_type* meas);

static void VoltageRippleReset();

int regu_BatteryDetection(const ControlLoopMeas_type* meas, const regu_BatteryDetect_type* BatteryDetectionVars)
{
	/* MTM 48/130 battery disconnect behavior
	 *   If turned on and not low current => voltage rippel
	 *   If turned on and low current => smooth voltage, no reliable way to detect battery disconnect have been found
	 *   If turned off => voltage decrease to zero
	 *
	 * Finnish charger battery disconnect behavior
	 *   If input signal a little bit larger than zero and battery disconnect => voltage high
	 *   If input signal close to zero => voltage slowly decrease towards zero (like capacitor discharging) */

	static int batteryConnectedOld = 0;
	int batteryConnected = 0;

	/* Uact */
	{
		static int timer = 0;
		int BatteryVoltageRipple;
		Uint32 BatteryVoltage;

		BatteryVoltageRipple = regu_BatteryVoltageRipple(meas, BatteryDetectionVars);
		BatteryVoltage = regu_AverageVoltage(meas);

		if(meas->RegulatorStatus.RestartDelay){
			return 1;
		}
		
		if(timer)
		{
			timer--;
			return 0;
		}
		if(BatteryDetectionVars->OnUactLow == 0)
		{
			// chalg parameters not initialized yet
			return 0;
		}

		switch(BatteryDetectionVars->ReguMode.ReguMode)
		{
		case ReguMode_PowerSupply :
			batteryConnected = 1;
			break;
		case ReguMode_Off :
		case ReguMode_On :
		case ReguMode_Auto :
			if(batteryConnectedOld) // connected check if disconnect
			{
				int inOffVoltageInterval;

				Uint32 Ulow = BatteryDetectionVars->OffUactLow;
				Uint32 Uhigh = BatteryDetectionVars->OffUactHigh;
				inOffVoltageInterval = BatteryVoltage > Ulow && BatteryVoltage < Uhigh;

				switch(BatteryDetectionVars->UactMode.off)
				{
				case BatterydetectCond_NotUsed :
					batteryConnected = batteryConnectedOld;
					break;
				case BatterydetectCond_False :
					batteryConnected = inOffVoltageInterval; // turn off if not in interval
					break;
				case BatterydetectCond_True :
					batteryConnected = !inOffVoltageInterval; // turn off if in interval
					break;
				}
			}
			else // not connected check if connect
			{
				int inOnVoltageInterval;

				Uint32 Ulow = BatteryDetectionVars->OnUactLow;
				Uint32 Uhigh = BatteryDetectionVars->OnUactHigh;
				inOnVoltageInterval = BatteryVoltage > Ulow && BatteryVoltage < Uhigh;

				switch(BatteryDetectionVars->UactMode.on)
				{
				case BatterydetectCond_NotUsed :
					batteryConnected = batteryConnectedOld;
					break;
				case BatterydetectCond_False :
					batteryConnected = !inOnVoltageInterval; // turn off if not in interval
					break;
				case BatterydetectCond_True :
					batteryConnected = inOnVoltageInterval; // turn off if in interval
					break;
				}
			}
			break;
		default :
			batteryConnected = 0;
			break;
		}

		batteryConnected = batteryConnected && !(meas->BatteryDeltaDcon);

		if(meas->BatteryCommunication == 0)
		{
			batteryConnected = batteryConnected && !BatteryVoltageRipple;
		}

		if(batteryConnectedOld && !batteryConnected)			// Disconnect flank
		{
			timer = 1000;	// #JJ
		}
		else if(!batteryConnectedOld && batteryConnected){		// Connect flank
			VoltageRippleReset();
		}
	}

	batteryConnectedOld = batteryConnected;

	return batteryConnected;
}

#define VOLTAGE_RIPPLE_LEN 100
static Uint16 AbsVoltDiffs[VOLTAGE_RIPPLE_LEN] = {[0 ... VOLTAGE_RIPPLE_LEN - 1] = 0};
static Uint16 Iacts[VOLTAGE_RIPPLE_LEN] = {[0 ... VOLTAGE_RIPPLE_LEN - 1] = 0};
static Uint32 AbsVoltDiffSum = 0;
static Int32 IactSum = 0;

static void VoltageRippleReset(){
	int n;
	for(n = 0; n < VOLTAGE_RIPPLE_LEN; n++)
	{
		AbsVoltDiffs[n] = 0;
		Iacts[n] = 0;
	}

	AbsVoltDiffSum = 0;
	IactSum = 0;
}
#if 1
static Uint8 regu_BatteryVoltageRipple(const ControlLoopMeas_type* meas, const regu_BatteryDetect_type* BatteryDetectionVars)
{
	Uint32 AbsVoltDiff;
	/* Calculate absolute value */
	{
		static Uint32 oldUact = 0;

		if(meas->Regulator.Uact > oldUact)
			AbsVoltDiff = meas->Regulator.Uact - oldUact;
		else
			AbsVoltDiff = oldUact - meas->Regulator.Uact;

		oldUact = meas->Regulator.Uact;
	}

	/* Calculate moving average of absolute value */
	{
		static int pos = 0;

		AbsVoltDiffSum -= AbsVoltDiffs[pos];			// Subtract old values here
		IactSum -= Iacts[pos];							// Subtract old values here

		AbsVoltDiffs[pos] = AbsVoltDiff;
		Iacts[pos] = meas->Regulator.Iact;

		pos++;											// Increase position
		if(pos >= VOLTAGE_RIPPLE_LEN)					// Reached the end ?
			pos = 0;									// Start from beginning

		AbsVoltDiffSum += AbsVoltDiffs[pos];			// Add new values here
		IactSum += Iacts[pos];							// Add new values here
	}

	/* Calculate if battery connected */
	if((IactSum < 40*10*100) && (AbsVoltDiffSum > 100*100))
		return 1;
	else
		return 0;
}
#endif
static Uint32 regu_AverageVoltage(const ControlLoopMeas_type* meas)
{
	static int init = 1;
	static Uint16 Uacts[100];
	const int averageLen = 100;
	static int pos = 0;
	static Int32 UactSum = 0;

	if(init)
	{
		int n;

		init = 0;
		for(n = 0; n < averageLen; n++)
		{
			Uacts[n] = 0;
		}
	}

		UactSum -= Uacts[pos];
		Uacts[pos] = meas->Regulator.Uact;
		UactSum += Uacts[pos];

		pos++;
		if(pos >= averageLen)
			pos = 0;

	/* Calculate if battery connected */
	return UactSum/100;
}

int regu_BatteryVoltagehigh(const ControlLoopMeas_type* meas)
{
	/*
	 * Check if measured voltage is out of range
	 * If 10 measurements is above offUactHigh (or UadcMax) level a flag for voltage out of range is set.
	 * Flag for voltage out of range is reset if delta voltage is above battery on detection level.
	 */


	static int outOfRange = FALSE;
	static int outOfRangeCnt = 0;
	Uint32 uiCharUmax;
	Uint32 offUactHigh;
	int deltaVoltageOff = meas_GetDeltaVoltageOff();

	uiCharUmax = Volt2ADC(engineVoltageMax() + 1.0); // Plus 1V margin
	//uiCharUmax = (meas->Limiter.Umax / 10) + 500; // One measurement of ADC value (plus some margin ~0,94V) - sorab: This could also be used directly

	if (meas->ReguOffUactHigh > uiCharUmax) // Use max. voltage level from UI-characteristics if it was below offUactHigh level from charging algorithm
	{
		offUactHigh = uiCharUmax;
	}
	else
	{
		offUactHigh = meas->ReguOffUactHigh;
	}

	if((meas->Regulator.Uact > offUactHigh) || (meas->Regulator.Uact > REGU_ADC_MAX))
	{
		if(outOfRangeCnt >= 3){
			if(REGU_ENGINE_ON){
				outOfRange = TRUE;
				outOfRangeCnt = 300;	// Delay reset of flag
			}
		}
		else{
			outOfRangeCnt++;
		}
	}
	else{
		if(outOfRangeCnt){
			outOfRangeCnt--;
		}
		else{
			if(!deltaVoltageOff){
				outOfRange = FALSE;
			}
		}
	}

	return outOfRange;
}
