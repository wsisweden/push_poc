#include "ControlLoop.h"
#include "regu_batteryDetection.h"
#include "regu_Interpolate.h"
#include "sendToBattery.h"
#include "pwm.h"
#include "Regu.h"
#include "sup.h"
#include "../../Meas/local.h"
#include "math.h"
#include "local.h"

typedef struct{
	ReguValueUint32_type		Set;										// Ramp to this value then turned on
	regu_BatteryDetect_type		BatteryDetect;								// Select battery detection mechanism
	ChargerDerate_type			DerateFromBattVoltage;
} ControlLoopInputs_type;

static ControlLoopInputs_type inputs = {{0, 0, 0, 0}, {{ReguMode_Off}, {{0}}, 0, 0, 0, 0}};			// Inputs to regulator
static int NewSet = 0;							// Written to different from zero then new input values are set

int batteryHigh = 0;
volatile Uint32 ReguIpwm = 0;					// Read by interrupt and set output signal
volatile int turnOffTimer;						// Used to indicate then current offset calibration should be done

void ControlLoopInit()
{
	const Engine_type* const engineParameters = engineGetParameters();

	turnOffTimer = 0;
	regu_RegulatorInit(&engineParameters->Regulator, PWM_CYCLE_FULL, engineParameters->Theatsink->Derate);
	regu_StatusAndErrorInit(engineParameters->Theatsink);
}

void ControlLoopSetInputs(ChargerSet_type* Input, Interval_type* UactOff, Interval_type* UactOn, ChargerDerate_type* Derate){
	{
		int on = 1;

		on = on && !isnan(Input->Set.U);
		inputs.Set.u = Volt2ADC(Input->Set.U);
		if(inputs.Set.u > 40950)
			inputs.Set.u = 40950;

		on = on && !isnan(Input->Set.I);
		on = on && (Input->Set.I >= 0.5f);
		inputs.Set.i = Ampere2ADC(Input->Set.I);
		if(inputs.Set.i > 40950)
			inputs.Set.i = 40950;

		on = on && !isnan(Input->Set.P);
		inputs.Set.p = Power2ADC(Input->Set.P);
		if(inputs.Set.p > 40950*40950)
			inputs.Set.p = 40950*40950;

		inputs.Set.RefOn = on;

		NewSet = 1;
	}

	inputs.BatteryDetect.ReguMode.ReguMode = Input->Mode;
	inputs.BatteryDetect.UactMode = Input->Uact;

	inputs.BatteryDetect.OffUactLow = Volt2ADC(UactOff->Low);
	inputs.BatteryDetect.OffUactHigh = Volt2ADC(UactOff->High);

	inputs.BatteryDetect.OnUactLow = Volt2ADC(UactOn->Low);
	inputs.BatteryDetect.OnUactHigh = Volt2ADC(UactOn->High);

	inputs.DerateFromBattVoltage = *Derate;
}

ChargerStatusFields_type ControlLoop(ControlLoopMeas_type* meas){
	ChargerStatusFields_type Status;
	static struct regu_regulatorOutput IpwmLimit = {LimitNone, 0};											// Note, old values are used by error calculation
	extern volatile int BatteryConnected;
	extern volatile int ChargerSelected;
	extern int gNoCanComm;
	static int detectReguError = 0;
	static int bmuInitReguError = 0;

	Status.Detail.Error = regu_StatusCalculateError(&meas->Regulator, &meas->RegulatorStatus, detectReguError, IpwmLimit.Ipwm, inputs.BatteryDetect.UactMode, BatteryConnected, ChargerSelected, bmuInitReguError);	// Errors may effect control loop but running control loop will not cause an error
	Status.Detail.Warning = regu_StatusCalculateWarning(meas->RegulatorStatus);									// Warnings may effect control loop but running control loop will not cause a warning
	IpwmLimit.Ipwm = 0;
	IpwmLimit.Limit = 0;

	int on;
	int pause;
	{
		on = inputs.Set.RefOn;
		on = on || meas->BatteryCommunication;

		on = on && !meas->EngineOutsideSpecification;
		on = on && !meas->Pause;
		on = on && !Status.Detail.Error.Sum;
		on = on && !meas->HwEngineOff;
		on = on && !meas->RegulatorStatus.regulatorError;
		on = on && !meas->RemoteRestriction;
		on = on && !gNoCanComm;
		on = on && !meas->FirmwareError;
		
		pause = !on;

		switch(inputs.BatteryDetect.ReguMode.ReguMode){
		case ReguMode_PowerSupply:
			BatteryConnected = 1;																			// Detect battery
			break;
		case ReguMode_Off :
			BatteryConnected = regu_BatteryDetection(meas, &inputs.BatteryDetect);							// Detect battery
			on = 0;
			break;
		case ReguMode_On :
		case ReguMode_Auto :
			BatteryConnected = regu_BatteryDetection(meas, &inputs.BatteryDetect);							// Detect battery
			batteryHigh = regu_BatteryVoltagehigh(meas);													// Detect high battery
			on = on && BatteryConnected;
			on = on && !batteryHigh;
			break;
		default :
			BatteryConnected = 0;																			// Do not detect battery
			on = 0;
			break;
		}

		/*
		 * Store the battery connection state to the RAM location which will 
		 * retain the value over a system reset. The value can be checked during
		 * startup to determine if a battery was connected before the reset.
		 * This is needed if the watchdog is enabled because it might reset the
		 * device at any time.
		 */
		SUP_RESET_STATUS = (Uint32) BatteryConnected;
	}

	{
		enum Interpolate_enum InterpolateState = InterpolateStateTransition(inputs.BatteryDetect.ReguMode.ReguMode, BatteryConnected, meas->BatteryCommunication, pause, &bmuInitReguError);

		int StateTransition;																				// Indicate if state changed
		{
			static enum Interpolate_enum old = Interpolate_BatteryDisconnected;
			StateTransition = old != InterpolateState;
			old = InterpolateState;
		}

		switch(InterpolateState){
		case Interpolate_BatteryDisconnected :											// Set output to zero and wait 100 milliseconds before turning off
			sendToBatteryDisable();
			if(StateTransition || NewSet){
				regu_SetRef(&inputs.Set, &meas->Regulator, on);
			}
			on = 0;																		// Turn off engine
			regu_InterpolateRef(&meas->Regulator, 11);									// Needed in off so that start is fast
			break;
		case Interpolate_NormalPre :													// Prepare HW by turning on engine if requested
			sendToBatteryDisable();
			if(StateTransition || NewSet){
				regu_SetRef(&inputs.Set, &meas->Regulator, on);
			}
			regu_InterpolateRef(&meas->Regulator, 11);									// Interpolate from old position to new position so that transitions are smooth
			break;
		case Interpolate_Normal :														// Interpolate to new reference values within one second
			sendToBatteryDisable();
			if(StateTransition){
				regu_RegulatorReset();
			}
			if(StateTransition || NewSet){
				regu_SetRef(&inputs.Set, &meas->Regulator, on);
			}
			regu_InterpolateRef(&meas->Regulator, 11);									// Interpolate from old position to new position so that transitions are smooth
			{
				regu_LimitRef(&meas->Regulator, Status.Detail.Warning.Detail.Phase, &meas->Limiter, &on);		// Limit reference values
				IpwmLimit = regu_Regulator(&meas->Regulator);							// Calculate new output (run regulator)
				detectReguError = on;
			}
			break;
		case Interpolate_NormalPause :													// Interpolate to new reference values within 100 milliseconds
			sendToBatteryDisable();
			if(StateTransition){
				regu_RegulatorReset();
			}
			if(StateTransition || NewSet){
				regu_SetRef(&inputs.Set, &meas->Regulator, 0);
			}
			regu_InterpolateRef(&meas->Regulator, 220);									// Interpolate from old position to new position so that transitions are smooth
			{
				regu_LimitRef(&meas->Regulator, Status.Detail.Warning.Detail.Phase, &meas->Limiter, &on);		// Limit reference values
				IpwmLimit = regu_Regulator(&meas->Regulator);							// Calculate new output (run regulator)
				detectReguError = on;
			}
			break;
		case Interpolate_BatteryCommunication :											// Send information via current pulses to battery
			on = 1;																		// Turn on engine
			detectReguError = 0;														// Turn off regulator error detection
			if(StateTransition){
				const Engine_type* const engineParameters = engineGetParameters();
				sendToBatteryReset(current2pwm(engineParameters->BitCurrent.Low), current2pwm(engineParameters->BitCurrent.High), engineParameters->BitCurrent.StartTime);
			}
			sendToBattery();
			break;
		case Interpolate_BatteryCommunicationPause :									// Interpolate to new reference values within 100 milliseconds
			sendToBatteryDisable();
			on = 0;
			break;
		case Interpolate_BatteryCommunication2 :
			if(StateTransition){														// Inform SUP to switch to new state
				msg_sendTry(
					MSG_LIST(statusRep),
					PROJECT_MSGT_STATUSREP,
					SUP_STAT_BM_INIT2
				);
				sendToBattery2Reset();
			}
			on = sendToBattery2(meas->Regulator.Iact, &bmuInitReguError);
			break;
		case Interpolate_BatteryCommunication2Pause :									// Interpolate to new reference values within 100 milliseconds
			sendToBatteryDisable();
			on = 0;
			break;
		case Interpolate_BatteryCommunicationFail :										// Battery communication failed
			sendToBatteryDisable();														// Disable battery communication
			on = 0;																		// Turn off engine
			break;
		case Interpolate_BmCalibration :
			if(StateTransition){
				bmCalibrationReset();													// Reset counter for Current calibration
			}
			on = bmCalibration(meas->Regulator.Iact);
			break;
		case Interpolate_VoltSense :
			if(StateTransition){														// Inform SUP to switch to new state
				msg_sendTry(
					MSG_LIST(statusRep),
					PROJECT_MSGT_STATUSREP,
					SUP_STAT_VSENSE
				);
			}
			sendToBatteryDisable();
			on = 0;
			break;
		case Interpolate_VoltSensePause :												// Interpolate to new reference values within 100 milliseconds
			sendToBatteryDisable();
			on = 0;
			break;
		default :																		// Should never happen but just in case
			sendToBatteryDisable();														// Disable battery communication
			on = 0;																		// Turn off engine
			break;
		}
	}

	{
		const int time = regu_TurnOffOn(BatteryConnected, meas, on);					// Calculate maximum time to turn off

		if(time < 10){																	// Always turn off output signal before relay
			atomic(ReguIpwm = 0;);														// This value is written to the hardware register at the same time as new values are sampled
			IpwmLimit.Ipwm = 0;															// Set duty cycle to zero
			pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, 0);							// Set duty cycle to zero now (a short cut)
			regu_RegulatorReset();
		}
		else{
			atomic(ReguIpwm = IpwmLimit.Ipwm;);											// This value is written to the hardware register at the same time as new values are sampled
		}

		if(time){
			turnOffTimer = 0;															// Count down below zero but not forever this timer is also used to determine then current offset should be calibrated
		}
		else{
			if(turnOffTimer > -20000)													// Count down below zero but not forever this timer is also used to determine then current offset should be calibrated
				turnOffTimer--;
		}

		/* Handle error flag for high charger temperature */
		/* This is done here to allow derate of current   */
		{
			uint8_t	highChargerTemp = (uint8_t)Status.Detail.Error.Detail.HighChargerTemperature;
			Status.Detail.Error.Detail.HighChargerTemperature = regu_StatusHighChargerTemp(&meas->Regulator, &meas->RegulatorStatus, ChargerSelected, highChargerTemp);
		}
	}

	Status.Detail.Info = regu_StatusCalculateInfo(IpwmLimit.Limit, &meas->Limiter, &meas->Regulator);	// Info should not change what happen only give information of what happened
	Status.Detail.Derate = regu_StatusCalculateDerate(Status.Detail.Info.Detail.limit);						// De-rate should not change what happen only give information of what happened
	Status.Detail.Derate.Detail.LowVoltage = inputs.DerateFromBattVoltage.Detail.LowVoltage;
	Status.Detail.Derate.Detail.HighVoltage = inputs.DerateFromBattVoltage.Detail.HighVoltage;
	switch(inputs.BatteryDetect.ReguMode.ReguMode){
	case ReguMode_PowerSupply:
		break;
	case ReguMode_Off :
	case ReguMode_On :
	case ReguMode_Auto :
		Status.Detail.Derate.Detail.NoLoad = !BatteryConnected;								// De-rate should not change what happen only give information of what happened
		break;
	}

	NewSet = 0;
	Status.Detail.Error.Sum |= Status.Detail.Warning.Sum;		// #JJ Add warnings to error for display purpose
	Status.Detail.Warning.Sum = 0; 					// #JJ Clear local warnings. Warning bitfield is used to display slave errors in master
	return Status;
}

