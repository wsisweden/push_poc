#ifndef REGU_BATTERYDETECTION_H_
#define REGU_BATTERYDETECTION_H_

#include "Regulator/regu_Regulator.h"
#include "cai_cc.h"
#include "ControlLoop.h"

#define REGU_ADC_MAX			(4095*10 - 10)	/* 10 measurements of Max adc value (minus some margin) */

typedef struct{
	union{
		ReguMode_enum ReguMode:8;
		uint8_t ReguModeReg;
	} ReguMode;
	union ReguOnAndOff_type UactMode;
	uint32_t OffUactLow;
	uint32_t OffUactHigh;
	uint32_t OnUactLow;
	uint32_t OnUactHigh;
} regu_BatteryDetect_type;

int regu_BatteryDetection(const ControlLoopMeas_type* meas, const regu_BatteryDetect_type* BatteryDetectionVars);
int regu_BatteryVoltagehigh(const ControlLoopMeas_type* meas);

#endif /* REGU_BATTERYDETECTION_H_ */
