/*
 * regu_Regulator.h
 *
 *  Created on: May 17, 2010
 *      Author: nicka
 */

#ifndef REGU_REGULATOR_H_
#define REGU_REGULATOR_H_

#include "PI.h"
#include "cai_cc.h"
#include "engines_types.h"

struct regu_Regulator_Input_Type
{
	/* Local variables */
	uint32_t				Uset;		/* Reference voltage 				*/
	uint32_t				Iset;		/* Reference current				*/
	uint32_t				Pset;		/* Reference power value			*/

	uint32_t				Uact;		/* Voltage measurement 				*/
	uint32_t				Iact;		/* Current measurement				*/
	uint32_t				Pact;		/* Power measurement				*/
	int16_t					Tact;		/* Temperature measurement			*/
};

struct regu_Regulator_Type
{
	int16_t					Tset;		/* Maximum allowed temperature		*/
	uint32_t				Rengine;    /* Engine internal resistance		*/

	struct PI_type 			Regulator;	/* PI - Regulator		*/

	uint32_t				KpU;		/* Voltage regulation gain			*/
	uint32_t				KiU;		/* Voltage regulation gain			*/
	uint32_t				KchooseU;	/* Weighting coefficient used to use regulator */

	uint32_t				KpI;		/* Voltage regulation gain			*/
	uint32_t				KiI;		/* Voltage regulation gain			*/
	uint32_t				KchooseI;	/* Weighting coefficient used to use regulator */

	uint32_t				KpP;		/* Voltage regulation gain			*/
	uint32_t				KiP;		/* Voltage regulation gain			*/
	uint32_t				KchooseP;	/* Weighting coefficient used to use regulator */

	uint32_t				KpT;		/* Voltage regulation gain			*/
	uint32_t				KiT;		/* Voltage regulation gain			*/
	uint32_t				KchooseT;	/* Weighting coefficient used to use regulator */
};

struct regu_regulatorOutput{
	limiter_type			Limit;
	uint32_t				Ipwm;		/* PWM value */
};

void regu_RegulatorInit(const EngineRegulator_type* parameters, int32_t outputMax, float Derate);
void regu_RegulatorReset();
struct regu_regulatorOutput regu_Regulator(const struct regu_Regulator_Input_Type* In);


#endif /* REGU_REGULATOR_H_ */
