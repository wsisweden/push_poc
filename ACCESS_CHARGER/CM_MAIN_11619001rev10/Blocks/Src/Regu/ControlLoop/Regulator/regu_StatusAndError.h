/*
 * regu_Regulator.h
 *
 *  Created on: May 17, 2010
 *      Author: nicka
 */

#ifndef REGU_STATUS_AND_ERROR_H_
#define REGU_STATUS_AND_ERROR_H_

#include "regu_Regulator.h"
#include "cai_cc.h"
#include "engines_types.h"

#define PHASE_ERROR_MAX_OFFSET (4095/10)
#define PHASE_ERROR_TIMEOUT (1*60)
#define REGU_STATUS_AND_ERRROR_CURRENTSAMPLES_LEN 10
#define REGU_TEMP_ERROR_TIMOUT (60*100)				// 1 minutes (60 seconds) in 10ms resolution

struct regu_near_type{
	int nearCnt;
	int average;
};

struct regu_Limiter_Type
{
	Uint32	Umax;								// Limit voltage
	Uint32	ImaxDc;								// Limit current
	Uint32	ImaxPower;							// Limit according to engine parameters
	Int32	IacLimit;							// Limit phase current
	float	ConversionfactorPhaseCurrentToDc;	// Phase current conversion factor
	Int32	PacLimit;							// Limit input power
	float	ConversionfactorApparentPower;		// Apparent power conversion factor

	int		uMaxEngine;
	int		iMaxEngine;
	int		pMaxEngine;
	int		PacMax;
	int		IacMax;
	int		iMaxDc;
	int		PhaseError;
};

struct regu_StatusMeas_Type
{
	int16_t						currentSamples[REGU_STATUS_AND_ERRROR_CURRENTSAMPLES_LEN];
	int							overTemperature;
	int							quietDerate;
	int*						WatchDog;
	int							Restarts;
	int							RestartDelay;
	int							phaseError;
	int							regulatorError;
	uint8_t						regulatorErrorCnt;
	uint16_t					enginePwmOffset;
};

#define REGU_WATCHDOG_TIME (4*100)
enum ReguWatchDog {ReguWatchDogNewSet_enum,
				   ReguWatchDogUset_enum,
				   ReguWatchDogIset_enum,
				   ReguWatchDogPset_enum,
				   ReguWatchDogUactMode_enum,
				   ReguWatchDogReguMode_enum,
				   ReguWatchDogReguOffUactLow_enum,
				   ReguWatchDogReguOffUactHigh_enum,
				   ReguWatchDogReguOnUactLow_enum,
				   ReguWatchDogReguOnUactHigh_enum,
				   ReguWatchDog_enum_size,
};

void regu_StatusAndErrorInit(const const Temperature_type* Theatsink);

void regu_LimitRef(struct regu_Regulator_Input_Type* In, int phaseError, struct regu_Limiter_Type* This, int *on);

ChargerInfo_type regu_StatusCalculateInfo(limiter_type limit, struct regu_Limiter_Type* This, const struct regu_Regulator_Input_Type* In);
ChargerDerate_type regu_StatusCalculateDerate(limiter_type limit);
//ChargerWarning_type regu_StatusCalculateWarning();
ChargerWarning_type regu_StatusCalculateWarning(struct regu_StatusMeas_Type RegulatorStatus);
ChargerError_type regu_StatusCalculateError(const struct regu_Regulator_Input_Type *	In, struct regu_StatusMeas_Type* RegulatorStatus, int on, uint32_t Ipwm, const union ReguOnAndOff_type UseUact, int batteryConnected, int chargerSelected, int bmuInitReguError);
uint8_t regu_StatusHighChargerTemp(const struct regu_Regulator_Input_Type *	In, struct regu_StatusMeas_Type* RegulatorStatus, int chargerSelected, uint8_t	highChargerTemp);

#endif /* REGU_REGULATOR_H_ */
