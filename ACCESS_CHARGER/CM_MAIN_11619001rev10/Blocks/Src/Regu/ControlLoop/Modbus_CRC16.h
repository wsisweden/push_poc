#ifndef MODBUS_CRC16_H
#define MODBUS_CRC16_H

#include "inttypes.h"
//#include <EmbeddedTypes.h>

uint16_t modbus__calculateCRC(uint8_t* pBuf, uint8_t bufferLength);

#endif
