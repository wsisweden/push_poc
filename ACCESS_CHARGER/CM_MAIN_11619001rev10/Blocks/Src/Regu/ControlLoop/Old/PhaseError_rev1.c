static void regu_DetectPhaseError(const struct regu_Regulator_Input_Type* In, struct regu_StatusMeas_Type* RegulatorStatus);

static void regu_DetectPhaseError(const struct regu_Regulator_Input_Type* In, struct regu_StatusMeas_Type* RegulatorStatus)
{
	int n;
	const Uint16 maxOffsetError = PHASE_ERROR_MAX_OFFSET;
	Int32 mean = 0;
	Int32 errorMeasure = 0;
	Int16 errorMax;

	/* Calculate mean */
	for (n = 0; n < REGU_STATUS_AND_ERRROR_CURRENTSAMPLES_LEN; n++)
	{
		mean += RegulatorStatus->currentSamples[n];
	}
	mean = mean/10;

	/* Calculate phase error measure */
	for (n = 0; n < 10; n++)
	{
		if(RegulatorStatus->currentSamples[n] > mean)
			errorMeasure += RegulatorStatus->currentSamples[n] - mean;
		else
			errorMeasure += mean - RegulatorStatus->currentSamples[n];
	}

	if(mean > 0)
	{
		errorMax = maxOffsetError + 3*mean;
	}
	else
	{
		errorMax = maxOffsetError - 3*mean;
	}

	{
		if(In->on && errorMeasure > errorMax)
		{
			if(RegulatorStatus->phaseErrorCounter >= 100*5)
			{
				RegulatorStatus->Status.Error.Detail.Phase = 1;
				RegulatorStatus->phaseErrorCounter = PHASE_ERROR_TIMEOUT*100;
			}
			else
			{
				RegulatorStatus->phaseErrorCounter = RegulatorStatus->phaseErrorCounter + 4; // observe that phase error must be detected faster than regulator error
			}
		}
		else if(RegulatorStatus->phaseErrorCounter)
		{
			RegulatorStatus->phaseErrorCounter--;
		}
		else // not error and not error counter
		{
			RegulatorStatus->Status.Error.Detail.Phase = 0;
		}
	}
}
