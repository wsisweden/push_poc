#ifndef CONTROL_LOOP_H
#define CONTROL_LOOP_H

#include "cai_cc.h"
#include "Regulator/regu_StatusAndError.h"

typedef struct{
	int									RemoteRestriction;					// Remote start/Stop. Charging restricted if TRUE
	int									Pause;								// Pause
	int									BatteryCommunication;				// Indicate if battery communication should be activated at battery connect
	int									EngineOutsideSpecification;			// Indicate if battery communication should be activated at battery connect
	int									FirmwareError;						// Indicate if firmware malfunction state
	Uint32								ReguOffUactHigh;					// High battery voltage detection level from charging algorithm
	Uint32								HwEngineOff;						// Indicate if hardware has turned engine off
	Uint32								BatteryDeltaDcon;					// Indicate if battery is disconnected due to delta voltage
	Uint32								Umeas1s;							// 1 second average voltage measurement from MEAS.
	struct regu_Regulator_Input_Type 	Regulator;
	struct regu_StatusMeas_Type			RegulatorStatus;
	struct regu_Limiter_Type			Limiter;
} ControlLoopMeas_type;

void ControlLoopInit();														// Initialize
void ControlLoopSetInputs(ChargerSet_type* Input, Interval_type* UactOff, Interval_type* UactOn, ChargerDerate_type* Derate);					// Set new input values
ChargerStatusFields_type ControlLoop(ControlLoopMeas_type* meas);			// Run control loop

#endif
