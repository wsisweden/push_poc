#include "sendToBattery.h"
#include "engines_types.h"
#include "meas.h"
#include "radio.h"
#include "sup.h"
#include "pwm.h"
#include "Modbus_CRC16.h"
#include "can.h"
#include "../../Can/Slave/Slave.h"
#include "../../Can/Master/Master.h"
#include "tools.h"
#include "Random.h"

#include "sys.h"
#include "reg.h"
#include "local.h"

enum regu__CableCom {
	REGU__CABLECOM_STX = 0, 	/**< Stx byte				*/
	REGU__CABLECOM_PANID_H, 	/**< PanId high byte		*/
	REGU__CABLECOM_PANID_L, 	/**< PanId low byte			*/
	REGU__CABLECOM_CHANNEL, 	/**< Channel				*/
	REGU__CABLECOM_NODEID_H,	/**< NodeId high byte		*/
	REGU__CABLECOM_NODEID_L,	/**< NodeId low byte		*/
	REGU__CABLECOM_U8SPARE1,	/**< Spare byte 1			*/
	REGU__CABLECOM_CHKSUM_H,	/**< Checksum (Mod_CRC16  byte 2-7)*/
	REGU__CABLECOM_CHKSUM_L,	/**< Checksum (Mod_CRC16  byte 2-7)*/
	REGU__CABLECOM_LEN,			/**< Length of message		*/
};

#define REGU_BMUINIT_TIMEOUT	1
#define REGU_BMUINIT_REGUERROR	2

void resetPwmDefault();
static void setData(batteryCommunication_type data);
static void setLocalData(void);
static void sendTobatteryFromTestMode();

static volatile int sendToBatteryToIsrEnable = 0;							// Set to less than zero to disable or greater than zero to enable or two to start send data, written here read zeroed in ISR in interrupt
static volatile enum sendToBatteryFromIsr_Type sendToBatteryFromIsr = 0;	// read here written in interrupt
static volatile uint32_t sendToBatteryBitLow;								// written here read by interrupt
static volatile uint32_t sendToBatteryBitHigh;								// written here read by interrupt
static volatile uint8_t* volatile sendToBatteryData;						// written here read by interrupt
static volatile unsigned int sendToBatteryDataLen;							// written here read by interrupt
uint8_t Data[REGU__CABLECOM_LEN];
static volatile int EnablingTime = 0;										// Temporary solution so that values could easily be adjusted in debugger
static volatile int multiplier = 0;

static int try = 0;
/* Current Combination (Alt. battery cable communication) variables */
const CurrentCombination_t sCurrentCombination[COMBINATIONS2_MAX] = COMBINATIONS2;
static volatile CurrentCombination_t sCurrentCombinationRead;
static volatile int timer2 = 0;
static volatile Uint8 ccIndex = 0;
static volatile Uint32 ccIsetPwm = 0;
static volatile float ccIsetFloat = 0;
volatile batteryCommunication_type dataToBattery = {{{0, 0, 0}}, 0};
static enum {dataValidNo, dataValidYes, DataValidWaitCan} dataValid = dataValidNo;
static enum can_CommProfile_enum CAN_CommProfile;
/* Bm Current Calibration variables */
static volatile BmCalibration_t sBmCalibrationRead;
//static volatile int timer3 = 0;
static volatile int BmCalibRadioAckEvent = FALSE;
static volatile int BmCalibState = 0;
static volatile int BmCalibStateTimer = 0;
static int iError = 0;
static int iCycleCnt = 2;

void sendToBatteryReset(/*regu__Inst *pInst*,*/ uint32_t bitLow, uint32_t bitHigh, int startTime)
{
	{
		Uint8 tmp;

		reg_get(&tmp, Regu_CAN_CommProfile);
		CAN_CommProfile = tmp;
	}

	switch(CAN_CommProfile){
	case CAN_COMM_PROFILE_DISABLED :
	case CAN_COMM_PROFILE_MASTER :
	case CAN_COMM_PROFILE_STATUS :
		{
			Uint8 ParallelControl;

			reg_get(&ParallelControl, regu__ParallelControl);

			if(ParallelControl){
				dataValid = DataValidWaitCan;
			}
			else{
				setLocalData();
				dataValid = dataValidYes;
			}
		}
		break;
	case CAN_COMM_PROFILE_SLAVE :
		dataValid = DataValidWaitCan;
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :
		dataValid = dataValidNo;
		break;
	}

	sendToBatteryBitLow = bitLow;
	sendToBatteryBitHigh = bitHigh;
	EnablingTime = startTime;
	multiplier = 0;
	sendToBatteryData = Data;
	sendToBatteryDataLen = sizeof(Data);
	sendToBatteryToIsrEnable = 1;							// send signal to ISR to enable battery communication

	try = 7;
}

void sendToBatteryDisable()
{
	sendToBatteryToIsrEnable = -1;
}

void sendToBattery(){
	switch(dataValid){
	case dataValidNo :
		break;
	case dataValidYes :
		if(try){
			if(sendToBatteryFromIsr == sendToBatteryFromIsrEnabled && !sendToBatteryToIsrEnable){
				switch(try){
				case 1 :
					multiplier = 16;
					break;
				case 2 :
					multiplier = 16;
					break;
				case 3 :
					multiplier = 8;
					break;
				case 4 :
					multiplier = 4;
					break;
				case 5 :
					multiplier = 2;
					break;
				default :
					multiplier = 1;
					break;
				}
				try--;

				sendToBatteryToIsrEnable = 2; // send signal to ISR to start send data
			}
		}
		else if(sendToBatteryFromIsr != sendToBatteryFromIsrBusy){
			sendToBatteryDisable();
		}
		break;
	case DataValidWaitCan :
		if(dataToBattery.Valid){
			setData(dataToBattery);
			dataValid = dataValidYes;
		}
		break;
	}
}

int sendToBattery2(Int32 Iact, int* bmuInitReguError){
	static int iEngineOn = 0;
	static int iCnt = 0;
	static int ImeasuredSum[2] = {0,0};
	float Imeasured = 0;
	int iRandom;
	Uint8 sendToRadio;

	if(iError){
		iEngineOn = 0;
	}
	else{
		if(iCnt == 10){							// Sum 10ms average to 100ms average
			ImeasuredSum[1] = ImeasuredSum[0];
			ImeasuredSum[0] = 0;
			iCnt = 0;
		}
		ImeasuredSum[0] += Iact;
		iCnt++;

		switch(timer2)
		{
			case 0:
				// Set output level to zero
				iEngineOn = 0;
				ccIsetPwm = 0;
				break;
			case 500:
				// check if new cycle should be started
				if(iCycleCnt){
					iCycleCnt--;
				}
				else{
					iError = REGU_BMUINIT_TIMEOUT;
				}
				break;
			case 600:
				// Get random number
				iRandom = mp_random();
				// Get random index 0 to max number of combinations
				ccIndex = (Uint8)(iRandom % COMBINATIONS2_MAX);
				// Set first CurrentLevel
				iEngineOn = 1;
				ccIsetFloat = (float)(sCurrentCombination[ccIndex].CurrentLevel1);
				ccIsetPwm = current2pwm(ccIsetFloat);
				break;

			case 1000:
				// Get first CurrentLevel
				Imeasured = AdValueToAmpere(100,ImeasuredSum[1]);
				if(Imeasured > 2.5 && Imeasured < 25.5)
					sCurrentCombinationRead.CurrentLevel1 = (Uint8)(Imeasured*10);
				else
					iError = REGU_BMUINIT_REGUERROR;
				break;

			case 1100:
				// Set second CurrentLevel
				iEngineOn = 1;
				ccIsetFloat = (float)(sCurrentCombination[ccIndex].CurrentLevel2);
				ccIsetPwm = current2pwm(ccIsetFloat);
				break;

			case 1500:
				// Get second CurrentLevel
				Imeasured = AdValueToAmpere(100,ImeasuredSum[1]);
				if(Imeasured > 2.5 && Imeasured < 25.5)
					sCurrentCombinationRead.CurrentLevel2 = (Uint8)(Imeasured*10);
				else
					iError = REGU_BMUINIT_REGUERROR;
				break;

			case 1600:
				// Set third CurrentLevel
				iEngineOn = 1;
				ccIsetFloat = (float)(sCurrentCombination[ccIndex].CurrentLevel3);
				ccIsetPwm = current2pwm(ccIsetFloat);
				break;

			case 2000:
				// Get third CurrentLevel
				Imeasured = AdValueToAmpere(100,ImeasuredSum[1]);
				if(Imeasured > 2.5 && Imeasured < 25.5)
					sCurrentCombinationRead.CurrentLevel3 = (Uint8)(Imeasured*10);
				else
					iError = REGU_BMUINIT_REGUERROR;
				break;

			case 2200:
				// Set result to true, to send radio message
				if(CAN_CommProfile == CAN_COMM_PROFILE_SLAVE){
					slaveSendBattConnect2();
				}
				else{
					sendToRadio = 1;
					reg_put(&sendToRadio, regu__BmInit);
				}

				iEngineOn = 0;
				ccIsetPwm = 0;
				break;

		}

		// Handle error
		if(iError == REGU_BMUINIT_TIMEOUT){
			msg_sendTry(
				MSG_LIST(statusRep),
				PROJECT_MSGT_STATUSREP,
				SUP_STAT_BM_INIT2_FAIL
			);
		}
		else if(iError == REGU_BMUINIT_REGUERROR){
			*bmuInitReguError = 1;	// Trigger regu error
		}
		else{
			sendToBatteryToIsrEnable = 3; // send signal to ISR to start send data
		}

		// Increase counter
		if(timer2 < 2200)
			timer2++;
		else
			timer2 = 0;
	}
	return iEngineOn;
}

void sendToBattery2Reset(void){
	timer2 = 0;
	sendToBatteryToIsrEnable = 1;							// send signal to ISR to enable battery communication
	iError = 0;
	iCycleCnt = 2;
}

volatile CurrentCombination_t * sendToBattery2Get(void){
	return &sCurrentCombinationRead;
}

void sendToBattery2Set(CurrentCombination_t *CurrentCobmination){
	Uint8 sendToRadio;

	// Store current levels in struct
	sCurrentCombinationRead.CurrentLevel1 = CurrentCobmination->CurrentLevel1;
	sCurrentCombinationRead.CurrentLevel2 = CurrentCobmination->CurrentLevel2;
	sCurrentCombinationRead.CurrentLevel3 = CurrentCobmination->CurrentLevel3;

	// Set result to true, to send radio message
	sendToRadio = 1;
	reg_put(&sendToRadio, regu__BmInit);
}

int bmCalibration(Int32 Iact){
	static int iEngineOn = 0;
	static int iCnt = 0;
	static int ImeasuredSum[2] = {0,0};
	float Imeasured = 0;
	Int32 Iset;
	Uint8 bmCalib;

	/* Get average measured current */
	if(iCnt == 10){							// Sum 10ms average to 100ms average
		ImeasuredSum[1] = ImeasuredSum[0];
		ImeasuredSum[0] = 0;
		iCnt = 0;
	}
	ImeasuredSum[0] += Iact;
	iCnt++;

	/* Handle radio acknowledge event */
	if(BmCalibRadioAckEvent == TRUE){
		BmCalibRadioAckEvent = FALSE;											// Reset event flag

		if(BmCalibState == 3 || BmCalibState == 7)
			BmCalibState++;														// Increase state
	}

	/* Handle BM calibration state machine */
	switch(BmCalibState)
	{
		case 0:
			/* Set first Current calibration point */
			iEngineOn = 0;														// Engine off
			ccIsetPwm = 0;														// Set value is zero

			BmCalibState++;														// Increase state
			BmCalibStateTimer = 0;												// Reset state timer for next state
			break;

		case 1:
			/* Wait for current to settle */
			if(BmCalibStateTimer >= 300){										// Wait 3 seconds
				BmCalibState++;
			}
			break;

		case 2:
			/* Get first Current calibration point */
			//Imeasured = AdValueToAmpere(100,ImeasuredSum[1]);					// sum of 10*10ms(sum) AD value
			Imeasured = 0.0;													// Since engine is off current output should be zero
			sBmCalibrationRead.CalibrationPoint1 = (Int32)(Imeasured*1000);		// in mA

			if(CAN_CommProfile == CAN_COMM_PROFILE_SLAVE){
				slaveSendBmuCalibration1();
			}
			else{
				bmCalib = RADIO_BMCALIB_POINT1;										// Trigger sending of calibration message with calibration point 1
				reg_put(&bmCalib, regu__BmCalib);
			}

			BmCalibState++;														// Increase state
			BmCalibStateTimer = 0;												// Reset state timer for next state
			break;

		case 3:
			/* Wait for BmCalibration radio acknowledge message */
				;																// Do nothing, SUP state machine will timeout if no answer
			break;

		case 4:
			/* Set second Current calibration point */
			reg_get(&Iset, regu__IdcLimit);
			iEngineOn = 1;
			ccIsetFloat = (float)(Iset/2);										// Calibrate at 50% of IdcLimit
			ccIsetPwm = current2pwm(ccIsetFloat);

			BmCalibState++;														// Increase state
			BmCalibStateTimer = 0;												// Reset state timer for next state
			break;

		case 5:
			/* Wait for current to settle */
			if(BmCalibStateTimer >= 500){										// Wait 5 seconds
				BmCalibState++;
			}
			break;

		case 6:
			/* Get second Current calibration point */
			Imeasured = AdValueToAmpere(100,ImeasuredSum[1]);					// sum of 10*10ms(sum) AD value
			sBmCalibrationRead.CalibrationPoint2 = (Int32)(Imeasured*1000);		// in mA

			if(CAN_CommProfile == CAN_COMM_PROFILE_SLAVE){
				slaveSendBmuCalibration2();
			}
			else{
				bmCalib = RADIO_BMCALIB_POINT2;										// Trigger sending of calibration message with calibration point 2
				reg_put(&bmCalib, regu__BmCalib);
			}

			BmCalibState++;														// Increase state
			BmCalibStateTimer = 0;												// Reset state timer for next state
			break;

		case 7:
			/* Wait for BmCalibration radio acknowledge message */
				;																// Do nothing, SUP state machine will timeout if no answer
			break;

		case 8:
			/* End calibration and start charging */
			if(CAN_CommProfile == CAN_COMM_PROFILE_SLAVE){
				slaveSendBmuCalibrationEnd();
			}
			else{
				bmCalib = RADIO_BMCALIB_END;
				reg_put(&bmCalib, regu__BmCalib);
			}
			iEngineOn = 0;
			ccIsetPwm = 0;
			break;

	}

	// send signal to ISR to start send data
	sendToBatteryToIsrEnable = 3;

	// Increase counter
	if(BmCalibStateTimer < Int32_MAX)
		BmCalibStateTimer++;

	// Return flag to set power unit on/off
	return iEngineOn;
}

void bmCalibrationReset(void){
	BmCalibState = 0;
	BmCalibStateTimer = 0;
	sendToBatteryToIsrEnable = 1;							// send signal to ISR to enable battery communication
}

volatile BmCalibration_t * bmCalibrationGet(void){
	return &sBmCalibrationRead;
}

void bmCalibration1SetFromSlave(BmCalibration_t *pBmCalibrationReadFromSlave){
	Uint8 bmCalib;

	// Store calibration value in struct
	sBmCalibrationRead.CalibrationPoint1 = pBmCalibrationReadFromSlave->CalibrationPoint1;

	bmCalib = RADIO_BMCALIB_POINT1;										// Trigger sending of calibration message with calibration point 1
	reg_put(&bmCalib, regu__BmCalib);
}

void bmCalibration2SetFromSlave(BmCalibration_t *pBmCalibrationReadFromSlave){
	Uint8 bmCalib;

	// Store calibration value in struct
	sBmCalibrationRead.CalibrationPoint2 = pBmCalibrationReadFromSlave->CalibrationPoint2;

	bmCalib = RADIO_BMCALIB_POINT2;										// Trigger sending of calibration message with calibration point 1
	reg_put(&bmCalib, regu__BmCalib);
}

void bmCalibrationEndFromSlave(){
	Uint8 bmCalib;

	bmCalib = RADIO_BMCALIB_END;
	reg_put(&bmCalib, regu__BmCalib);
}

void bmCalibrationAckFromMaster(){
	BmCalibRadioAckEvent = TRUE;									// send event to bmCalibration function
}

void bmCalibrationRadioAck(void){
	BmCalibRadioAckEvent = TRUE;									// send event to bmCalibration function
	masterBmuCalibrationAck();										// Send event from master to slave
}

void sendToBattery1ms(){
	static int bitPos; // position include start and stop bits
	static unsigned int bytePos;
	static int divider; // baud rate divider
	static int timer = 0;

	switch(sendToBatteryFromIsr)
	{
	case sendToBatteryFromIsrDisabled : // normal charging
		if(sendToBatteryToIsrEnable > 0) // enable
		{
			if(sendToBatteryToIsrEnable == 3){
				sendToBatteryToIsrEnable = 0;
				pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, ccIsetPwm);
				sendToBatteryFromIsr = sendToBattery2FromIsrBusy;
			}
			else{
				sendToBatteryToIsrEnable = 0;
				atomic(PINCON->PINSEL3 &= ~((1<<9) | (1<<8)););
				atomic(PINCON->PINSEL7 |= (1<<19) | (1<<18););
				PWM1MR0 = PWM_CYCLE_FULL >> 8;
				PWM1TC = 0;
				atomic(PWM1LER |= LER0_EN;);
				pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
				sendToBatteryFromIsr = sendToBatteryFromIsrEnabling;
				timer = 0;
			}
		}
		else
		{
			volatile extern Uint32 ReguIpwm;
			pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, ReguIpwm);
		}
		break;
	case sendToBatteryFromIsrEnabling : // wait for current to reach working point
		{
			pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
		}
		if(sendToBatteryToIsrEnable < 0) // disable
		{
			resetPwmDefault();
		}
		else
		{
			timer++;
			if(timer >= EnablingTime)
			{
				timer = 0;
				sendToBatteryFromIsr = sendToBatteryFromIsrEnabled;
			}
		}
		break;
	case sendToBatteryFromIsrEnabled : // wait for data
		{
			pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
		}
		if(sendToBatteryToIsrEnable < 0) // disable
		{
			resetPwmDefault();
		}
		else if(sendToBatteryToIsrEnable == 2) // enable
		{
			sendToBatteryToIsrEnable = 0;
			divider = 5;
			bitPos = -12;
			bytePos = 0;
			sendToBatteryFromIsr = sendToBatteryFromIsrBusy;
		}
		break;
	case sendToBatteryFromIsrBusy : // send data
		{
			static unsigned int bit = 0;
			divider++;
			if(divider >= multiplier*5)
			{
				divider = 0;
				switch(bitPos)
				{
				case -100 ... -13 :												// set low so that change occur then start sending stop bits
					bit = 0;
					bitPos++;
					break;
				case -12 ... -2 :												// send stop bits
					bit = 1;
					bitPos++;
					break;
				case -1 : 														// send start bit
					bit = 0;
					bitPos++;
					break;
				case 0 ... 6 :													// send bit
					{
						bit = (sendToBatteryData[bytePos] >> bitPos) & 1;
						bitPos++;
					}
					break;
				case 7 :														// send stop bit and start next byte or stop
					{
						bit = (sendToBatteryData[bytePos] >> bitPos) & 1;
					}
					bitPos++;
					break;
				case 8 :
					{
						bytePos++;
						if(bytePos >= sendToBatteryDataLen)						// no more byte ?
						{
							sendToBatteryFromIsr = sendToBatteryFromIsrEnabled;	// go back to wait for data
							bit = 1;
						}
						else
						{
							bitPos = 0;											// reset to new bit
							bit = 0;											// Send start bit§
						}
					}
					break;
				}
			}
			if(bit)
			{
				pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
			}
			else
			{
				if(bitPos == 0 && (divider && divider <= multiplier*2)){
					pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitHigh >> 8);
				}
				else if(bitPos != 0 && (divider >= multiplier*3 && divider <= multiplier*4)){
					pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitHigh >> 8);
				}
				else{
					pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
				}
			}

			if(sendToBatteryToIsrEnable < 0) // disable
			{
				resetPwmDefault();
			}
		}
		break;

	case sendToBattery2FromIsrBusy : // send data
		{
			pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, ccIsetPwm);

			if(sendToBatteryToIsrEnable < 0) // disable
			{
				resetPwmDefault();
			}
		}
		break;

	case sendToBatteryFromTest : // Test mode
		{
			sendTobatteryFromTestMode();	// Run BMU_CURR_MOD test
		}
		break;
	}
}

void resetPwmDefault()
{
	sendToBatteryToIsrEnable = 0;
	pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, 0);
	sendToBatteryFromIsr = sendToBatteryFromIsrDisabled;
	atomic(PINCON->PINSEL3 |= (1<<9) | (0<<8););
	atomic(PINCON->PINSEL7 &= ~((1<<19) | (1<<18)););
	PWM1MR0 = PWM_CYCLE_FULL;
	atomic(PWM1LER |= LER0_EN;);
}

static void setData(batteryCommunication_type data){
	Data[REGU__CABLECOM_STX] = 0x02;
	Data[REGU__CABLECOM_PANID_H] = (Uint8)(data.PanId >> 8);
	Data[REGU__CABLECOM_PANID_L] = (Uint8)data.PanId;
	Data[REGU__CABLECOM_CHANNEL] = data.Channel;
	Data[REGU__CABLECOM_NODEID_H] = (Uint8)(data.NodeId >> 8);
	Data[REGU__CABLECOM_NODEID_L] = (Uint8)data.NodeId;
	Data[REGU__CABLECOM_U8SPARE1] = data.raw[5];

	{
		Uint16 Checksum;
		Checksum = modbus__calculateCRC(&Data[REGU__CABLECOM_PANID_H], (REGU__CABLECOM_CHKSUM_H - REGU__CABLECOM_PANID_H));
		Data[REGU__CABLECOM_CHKSUM_H] = (Uint8)(Checksum >> 8);
		Data[REGU__CABLECOM_CHKSUM_L] = (Uint8)Checksum;
	}
}

static void setLocalData(void){
	batteryCommunication_type data;

	reg_get(&data.PanId, regu__PanId);
	reg_get(&data.NodeId, regu__NodeId);
	reg_get(&data.Channel, regu__Channel);

	setData(data);
}

void sendTobatteryFromTestModeStart()
{
	const Engine_type* const engineParameters = engineGetParameters();

	sendToBatteryFromIsr = sendToBatteryFromTest;
	multiplier = 1;	// 1 = Fastest bitrate, about 200bit/s = 200Hz
	sendToBatteryBitLow = current2pwm(engineParameters->BitCurrent.Low);
	sendToBatteryBitHigh = current2pwm(engineParameters->BitCurrent.High);

	/* Configure PWM output on BMU_CURR_MOD pin */
	atomic(PINCON->PINSEL3 &= ~((1<<9) | (1<<8)););
	atomic(PINCON->PINSEL7 |= (1<<19) | (1<<18););
	PWM1MR0 = PWM_CYCLE_FULL >> 8;
	PWM1TC = 0;
	atomic(PWM1LER |= LER0_EN;);
	pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
}

void sendTobatteryFromTestModeStop()
{
	sendToBatteryFromIsr = sendToBatteryFromIsrDisabled;
	resetPwmDefault();
}

static void sendTobatteryFromTestMode()
{
	/* Send contineous bitpattern with all bits logic low (0) */
	static int divider = 0; // baud rate divider
	static unsigned int bit = 0;
	divider++;

	if(divider >= multiplier*5)
	{
		divider = 0;
	}

	if(bit)
	{
		pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
	}
	else
	{
		if((divider >= multiplier*3) && (divider <= multiplier*4)){
			pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitHigh >> 8);
		}
		else{
			pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, sendToBatteryBitLow >> 8);
		}
	}
}
