#ifndef SENDTOBATTERY_H_
#define SENDTOBATTERY_H_

#include <inttypes.h>
#include "tools.h"
/* This interface is not thread safe and should only be used from one process */

#define COMBINATIONS2_MAX 60
#define COMBINATIONS2 /* Current level combinations (4A, 6A, 8A, 10A, 12A) */ \
    {{4, 4, 6}, 	\
     {4, 4, 8}, 	\
     {4, 4, 10}, 	\
     {4, 4, 12}, 	\
     {4, 6, 4}, 	\
     {4, 6, 6}, 	\
     {4, 6, 8}, 	\
     {4, 6, 10}, 	\
     {4, 6, 12}, 	\
     {4, 8, 4}, 	\
     {4, 8, 6}, 	\
     {4, 8, 8}, 	\
     {4, 8, 10}, 	\
     {4, 8, 12}, 	\
     {4, 10, 4}, 	\
     {4, 10, 6}, 	\
     {4, 10, 8}, 	\
     {4, 10, 10}, 	\
     {4, 10, 12}, 	\
     {4, 12, 4}, 	\
     {4, 12, 6}, 	\
     {4, 12, 8}, 	\
     {4, 12, 10}, 	\
     {4, 12, 12}, 	\
     {6, 4, 4}, 	\
     {6, 4, 6}, 	\
     {6, 4, 8}, 	\
     {6, 4, 10}, 	\
     {6, 4, 12}, 	\
     {6, 6, 4}, 	\
     {6, 8, 4}, 	\
     {6, 10, 4}, 	\
     {6, 12, 4}, 	\
     {8, 4, 4}, 	\
     {8, 4, 6}, 	\
     {8, 4, 8}, 	\
     {8, 4, 10}, 	\
     {8, 4, 12}, 	\
     {8, 6, 4}, 	\
     {8, 8, 4}, 	\
     {8, 10, 4}, 	\
     {8, 12, 4}, 	\
     {10, 4, 4}, 	\
     {10, 4, 6}, 	\
     {10, 4, 8}, 	\
     {10, 4, 10}, 	\
     {10, 4, 12}, 	\
     {10, 6, 4}, 	\
     {10, 8, 4}, 	\
     {10, 10, 4}, 	\
     {10, 12, 4}, 	\
     {12, 4, 4}, 	\
     {12, 4, 6}, 	\
     {12, 4, 8}, 	\
     {12, 4, 10}, 	\
     {12, 4, 12}, 	\
     {12, 6, 4}, 	\
     {12, 8, 4}, 	\
     {12, 10, 4}, 	\
     {12, 12, 4} 	\
    }

typedef struct{
	union{
		struct{
			Uint16 PanId;
			Uint8 Channel;
			Uint16 NodeId;
		};
		Uint8 raw[8];
	};
	int Valid;
} batteryCommunication_type;

typedef struct CurrentCombination_tag
{
	Uint8 CurrentLevel1;
	Uint8 CurrentLevel2;
	Uint8 CurrentLevel3;
}CurrentCombination_t;

typedef struct BmCalibration_tag
{
	Int32 CalibrationPoint1;
	Int32 CalibrationPoint2;
}BmCalibration_t;

int sendToBattery2(Int32 Iact, int* bmuInitReguError);				/* Initialize BMU with Current output level combination */
void sendToBattery2Reset(void);									/* Reset timer for BMU init */
volatile CurrentCombination_t * sendToBattery2Get(void);		/* Get Current output level combination */
void sendToBattery2Set(CurrentCombination_t *CurrentCombination);			/* Get Current output level combination */

int bmCalibration(Int32 Iact);									/* BMU Current calibration*/
void bmCalibrationReset(void);									/* Reset timer for BMU Current calibration */
volatile BmCalibration_t * bmCalibrationGet(void);				/* Get Current calibration point values */
void bmCalibrationRadioAck(void);								/* Update BMU Current calibration state at reception of radio acknowledge message */
void bmCalibration1SetFromSlave(BmCalibration_t *pBmCalibrationReadFromSlave);
void bmCalibration2SetFromSlave(BmCalibration_t *pBmCalibrationReadFromSlave);
void bmCalibrationEndFromSlave();
void bmCalibrationAckFromMaster();

enum sendToBatteryFromIsr_Type {
	sendToBatteryFromIsrDisabled,	// turned off
	sendToBatteryFromIsrEnabling,	// turned on waiting for current to reach operation point
	sendToBatteryFromIsrEnabled,	// ready to send data
	sendToBatteryFromIsrBusy,		// busy sending data
	sendToBattery2FromIsrBusy,		// busy sending data by alternative method
	sendToBatteryFromTest,			// busy sending data from Test mode
};

/* Must be called and return nonzero before data is sent to battery.
 *   return 0 if not ready and nonzero if ready to send data to battery */
void sendToBatteryReset(/*regu__Inst *pInst,*/ uint32_t bitLow, uint32_t bitHigh, int startTime);

/* Return current control to normal mode */
void sendToBatteryDisable();

void sendToBattery();				// Called once each 10 milliseconds from regulator control loop
void sendToBattery1ms();			// Called once each millisecond from interrupt

void sendTobatteryFromTestModeStart();	// Called to start Test mode BMU_CURR_MOD test
void sendTobatteryFromTestModeStop();	// Called to stop Test mode BMU_CURR_MOD test

#endif /* SENDTOBATTERY_H_ */
