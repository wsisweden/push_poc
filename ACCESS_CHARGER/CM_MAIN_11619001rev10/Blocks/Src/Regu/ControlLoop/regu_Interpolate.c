#include "regu_Interpolate.h"
#include "regu.h"

static struct regu_interpolate_type This = {{0, 0}, {0, 0}, {0, 0}, 0};

static uint32_t regu_TwoPointInterpolate(struct regu_TwoPointInterpolate_type* Points, uint16_t pos);
static uint32_t suppressTransient(uint32_t old, uint32_t new, uint32_t meas);		// Suppress transients then moving limits in different directions

void regu_InterpolateRef(struct regu_Regulator_Input_Type *Set, uint16_t speed) {
  uint32_t pos;

  pos = This.InterpolatePos;

  Set->Uset = regu_TwoPointInterpolate(&This.Uset, pos); // Uset
  Set->Iset = regu_TwoPointInterpolate(&This.Iset, pos); // Iset
  Set->Pset = regu_TwoPointInterpolate(&This.Pset, pos); // Pset

  This.InterpolatePos += speed;

  if(This.InterpolatePos > 1024){
      This.InterpolatePos = 1024;
  }
}

void regu_SetRef(ReguValueUint32_type* set, const struct regu_Regulator_Input_Type* meas, int on){
  const uint32_t pos = This.InterpolatePos;

  This.Uset.Old = regu_TwoPointInterpolate(&This.Uset, pos);	// start from old value
  This.Uset.Old = suppressTransient(This.Uset.Old, set->u, meas->Uact);
  This.Uset.New = set->u;

  This.Iset.Old = regu_TwoPointInterpolate(&This.Iset, pos);	// start from old value
  This.Iset.Old = suppressTransient(This.Iset.Old, set->i, meas->Iact);
  if(on){
	  This.Iset.New = set->i;
  }
  else{
	  This.Iset.New = 0;
  }

  This.Pset.Old = regu_TwoPointInterpolate(&This.Pset, pos); // start from old value
  This.Pset.Old = suppressTransient(This.Pset.Old, set->p, meas->Pact);
  This.Pset.New = set->p;

  This.InterpolatePos = 0;
}

static uint32_t regu_TwoPointInterpolate(struct regu_TwoPointInterpolate_type* Points, uint16_t pos){
  return Points->Old + (((int64_t)pos*(Points->New - (int64_t)Points->Old)) >> 10);
}

enum Interpolate_enum InterpolateStateTransition(ReguMode_enum Mode, int Battery, int BatteryCommunication, int pause, int *bmuInitReguError){
	static enum Interpolate_enum state = Interpolate_BatteryDisconnected;
	static int Timer = 0;
	static int exitTimer = 0;

	if(Mode == ReguMode_PowerSupply){
		switch(state){
		case Interpolate_NormalPre :											// Wait for HW before ramping up set value
			if(exitTimer){
				exitTimer--;
			}
			else{
				state = Interpolate_Normal;
			}
			break;
		case Interpolate_Normal :												// Interpolate to new reference values within one second
			if(pause){
				state = Interpolate_NormalPause;
				Timer = 100;													// Stay in pause at least one second
				exitTimer = 0;
			}
			break;
		case Interpolate_NormalPause :											// Interpolate to new reference values within 100 milliseconds
			if(Timer){
				Timer--;
			}
			else if(!pause){
				if(exitTimer){
					exitTimer--;
				}
				else{
					state = Interpolate_NormalPre;
					Timer = 0;
					exitTimer = 50; 											// Wait at least 500 milliseconds before exit
				}
			}
			break;
		default :
			if(pause){
				state = Interpolate_NormalPause;
				Timer = 100;													// Stay in pause at least one second
				exitTimer = 0;
			}
			else{
				state = Interpolate_Normal;
				Timer = 0;
			}
			break;
		}
	}
	else{
		switch(state){
		case Interpolate_BatteryDisconnected :									// Set output to zero and wait 100 milliseconds before turning, interpolate to new reference values within one second
			if(Battery){
				if(pause){
					state = Interpolate_NormalPause;
					Timer = 100;											// Stay in pause at least one second
					exitTimer = 0;
				}
				else{
					state = Interpolate_NormalPre;
					Timer = 0;
					exitTimer = 50; 											// Wait at least 500 milliseconds before exit
				}
			}
			break;
		case Interpolate_NormalPre :											// Wait for HW before ramping up set value
			if(exitTimer){
				exitTimer--;
			}
			else{
				state = Interpolate_Normal;
			}
			break;
		case Interpolate_Normal :												// Interpolate to new reference values within one second
			if(Battery){
				if(BatteryCommunication == 1 || BatteryCommunication == 4){
					state = Interpolate_BatteryCommunication;
					Timer = 0;
					exitTimer = 2500;											// Exit after 25 seconds
				}
				else if(BatteryCommunication == 3){
					state = Interpolate_BmCalibration;
					Timer = 0;
					exitTimer = 2000;											// Exit after 20 seconds
				}
				else {
					if(pause){
						state = Interpolate_NormalPause;
						Timer = 100;												// Stay in pause at least one second
						exitTimer = 0;
					}
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 200;													// Stay disconnected at least one second
			}
			break;
		case Interpolate_NormalPause :											// Interpolate to new reference values within 100 milliseconds
			if(Battery){
				if(Timer){
					Timer--;
				}
				else if(!pause){
					if(exitTimer){
						exitTimer--;
					}
					else{
						state = Interpolate_NormalPre;
						Timer = 0;
						exitTimer = 50; 											// Wait at least 500 milliseconds before exit
					}
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 200;													// Stay disconnected at least one second
			}
			break;
		case Interpolate_BatteryCommunication :									// Send information via current pulses to battery
			if(Battery){
				if(BatteryCommunication == 1 || BatteryCommunication == 4){
					if(exitTimer){
						exitTimer--;
					}
					else{
						if(BatteryCommunication == 4){
							state = Interpolate_VoltSense;
							Timer = 100;
						}
						else{
							state = Interpolate_BatteryCommunication2;
							Timer = 100;
						}
					}
				}
				else{
					if(pause){
						state = Interpolate_BatteryCommunicationPause;
						Timer = 100;												// Stay in pause at least one second
					}
					else{
						state = Interpolate_Normal;
					}
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 100;
			}
			break;
		case Interpolate_BatteryCommunicationPause :							// Send information via current pulses to battery
			if(Battery){
				if(Timer){
					Timer--;
				}
				else if(!pause){
					state = Interpolate_BatteryCommunication;
					exitTimer = 2500;	//3500;											// Exit after three seconds
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 200;													// Stay disconnected at least one second
			}
			break;
		case Interpolate_BatteryCommunication2 :
			if(Battery){
				if(Timer){
					Timer--;
				}
				else{
					if(BatteryCommunication != 2){
						if(pause){
							state = Interpolate_BatteryCommunication2Pause;
							Timer = 100;												// Stay in pause at least one second
							exitTimer = 50; 											// Wait at least 500 milliseconds before exit
						}
						else{
							state = Interpolate_Normal;
						}
						*bmuInitReguError = 0;
					}
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 100;
				*bmuInitReguError = 0;
			}
			break;
		case Interpolate_BatteryCommunication2Pause:							// Send information via current pulses to battery
			if(Battery){
				if(Timer){
					Timer--;
				}
				else if(!pause){
					state = Interpolate_BatteryCommunication2;
					Timer = 100;
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 200;													// Stay disconnected at least one second
			}
			break;
		case Interpolate_BatteryCommunicationFail :								// Interpolate to new reference values within 100 milliseconds
			if(!Battery){
				state = Interpolate_BatteryDisconnected;
				Timer = 100;													// Stay disconnected at least one second
			}
			break;
		case Interpolate_BmCalibration :
			if(Battery){
				if(Timer){
					Timer--;
				}
				else{
					if(BatteryCommunication != 3){
						if(pause){
							state = Interpolate_NormalPause;
							Timer = 100;												// Stay in pause at least one second
							exitTimer = 0;
						}
						else{
							state = Interpolate_Normal;
						}
					}
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 100;
			}
			break;
		case Interpolate_VoltSense :
			if(Battery){
				if(Timer){
					Timer--;
				}
				else{
					if(BatteryCommunication != 4){
						if(pause){
							state = Interpolate_VoltSensePause;
							Timer = 100;												// Stay in pause at least one second
							exitTimer = 10; 											// Wait at least 100 milliseconds before exit
						}
						else{
							state = Interpolate_Normal;
						}
					}
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 100;
			}
			break;
		case Interpolate_VoltSensePause:							// Send information via current pulses to battery
			if(Battery){
				if(Timer){
					Timer--;
				}
				else if(!pause){
					if(exitTimer){
						exitTimer--;
					}
					else{
						state = Interpolate_VoltSense;
						Timer = 100;
					}
				}
			}
			else{
				state = Interpolate_BatteryDisconnected;
				Timer = 200;													// Stay disconnected at least one second
			}
			break;
		default :																// Should not happen but just in case
			state = Interpolate_BatteryDisconnected;
			Timer = 200;														// Stay disconnected at least one second
			break;
		}
	}

	return state;
}

int regu_TurnOffOn(int BatteryConnected, const ControlLoopMeas_type* meas, int on){							// Handle engine off/on signal, in particular the turn off delay
	/* This function only use local variables and do not depend on interpolation or
	 * control loop to be correct. It is implemented to have as low complexity as
	 * possible to reduce risk for mistakes.
	 */
	static int turnOffTimer = 0;

	if(meas->RemoteRestriction || meas->HwEngineOff){
		turnOffTimer = 0;
	}
	else{
		if(BatteryConnected){																// Battery connected ?
			if(meas->Pause){																		// Pause ?
				if(turnOffTimer > 20){														// Timer to high ?
					turnOffTimer = 20;														// Turn down timer
				}
			}
			else{																			// Not pause
				if(on){																		// Turned on ?
					turnOffTimer = 20;														// Keep timer turned up while turned on
				}
			}
		}																					// Battery disconnected
		else{
			if(turnOffTimer > 10){															// Timer to high ?
				turnOffTimer = 10;															// Turn down timer
			}
		}
	}

	if(meas->RegulatorStatus.RestartDelay){
		turnOffTimer = 0;																// Reset timer during restart after wakeup
	}

	if(turnOffTimer){																	// Reached zero ?
		REGU_CHARGING_ON_DO_ON();														// Turn on engine
		turnOffTimer--;																	// Count downwards
	}
	else{
		REGU_CHARGING_ON_DO_OFF();														// Turn off engine
	}

	return turnOffTimer;
}

static uint32_t suppressTransient(uint32_t old, uint32_t new, uint32_t meas){
	/* The old signal may be lowered down to the new signal */

	if(meas < old && new < old){			// Suppress ?
		if(meas < new){						// Below new signal ?
			return new;
		}
		else{								// Above new signal
			return meas;
		}
	}
	else{									// Do not suppress
		return old;
	}
}
