#ifndef REGU_INTERPOLATE_H
#define REGU_INTERPOLATE_H

#include <inttypes.h>
#include "Regulator/regu_Regulator.h"
#include "ControlLoop.h"

typedef struct {
	uint32_t u;
	uint32_t i;
	uint32_t p;
	int RefOn;
} ReguValueUint32_type;

struct regu_TwoPointInterpolate_type
{
	uint32_t Old;
	uint32_t New;
};

struct regu_interpolate_type
{
	struct regu_TwoPointInterpolate_type	Uset;
	struct regu_TwoPointInterpolate_type	Iset;
	struct regu_TwoPointInterpolate_type	Pset;
	unsigned int							InterpolatePos;
};

enum Interpolate_enum {Interpolate_BatteryDisconnected,
	   	   	   	   	   Interpolate_NormalPre,
					   Interpolate_Normal,
					   Interpolate_NormalPause,
					   Interpolate_BatteryCommunication,
					   Interpolate_BatteryCommunicationPause,
					   Interpolate_BatteryCommunication2,
					   Interpolate_BatteryCommunication2Pause,
					   Interpolate_BatteryCommunicationFail,
					   Interpolate_BmCalibration,
					   Interpolate_VoltSense,
					   Interpolate_VoltSensePause};

int regu_TurnOffOn(int BatteryConnected, const ControlLoopMeas_type* meas, int on);			// Handle engine off/on signal, in particular the turn off delay

void regu_InterpolateRef(struct regu_Regulator_Input_Type *Set, uint16_t speed);

/* speed = 100/1024 seconds */
void regu_SetRef(ReguValueUint32_type* set, const struct regu_Regulator_Input_Type* meas, int on);

enum Interpolate_enum InterpolateStateTransition(ReguMode_enum Mode, int Battery, int BatteryCommunication, int pause, int *bmuInitReguError);

#endif
