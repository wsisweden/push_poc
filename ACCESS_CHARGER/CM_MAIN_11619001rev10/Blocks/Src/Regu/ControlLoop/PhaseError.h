#ifndef PHASE_ERROR_H
#define PHASE_ERROR_H

#include "inttypes.h"

/* The principle for detection of phase error is that the output current ripple frequency change from the
 * ordinary 300Hz or 360Hz to 50Hz or 60 Hz. It is implemented by an AC-amplifier connected to a schmitt-
 * trigger and a frequency counter connected to the output of the schmitt-trigger. The DC level used in
 * the AC amplifier is calculated by using moving average. The hysteresis is dynamically adjusted to1/8
 * of the DC level.
 */
int regu_DetectMissingPhase(const int16_t* i, int len);			// Detect phase error from measured output current

#endif /* PHASE_ERROR_H */
