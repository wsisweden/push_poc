#ifndef REGU_IO_H
#define REGU_IO_H

#include "local.h"
#include "ControlLoop/Regulator/regu_StatusAndError.h"
#include "ControlLoop/ControlLoop.h"

#define REGU__LIMITER_ISET 		(1<<0)	/* Iset 							*/
#define REGU__LIMITER_USET 		(1<<1) 	/* Uset 							*/
#define REGU__LIMITER_PSET 		(1<<2) 	/* Pset 							*/
#define REGU__LIMITER_IMAXENG 	(1<<3) 	/* ImaxEngine 						*/
#define REGU__LIMITER_UMAXENG 	(1<<4) 	/* UmaxEngine 						*/
#define REGU__LIMITER_PMAXENG 	(1<<5) 	/* PmaxEngine 						*/
#define REGU__LIMITER_IACLIM 	(1<<6) 	/* IacLimit 						*/
#define REGU__LIMITER_PACLIM 	(1<<7)	/* PacLimit 						*/
#define REGU__LIMITER_THS 		(1<<8)	/* Ths (heat sink temp) 			*/
#define REGU__LIMITER_IDCLIM 	(1<<9) 	/* IdcLimit 						*/

void regu_IoGetMeas(regu__Inst* pInst, ControlLoopMeas_type* meas);										// Read measurements from register
void regu_IoGetControlLoopInputs(ChargerSet_type* Input, Interval_type* UactOff, Interval_type* UactOn, ChargerDerate_type* Derate, const sys_FBInstId instId);	// Read inputs from register
float regu_IoGetConversionfactorApparentPower(void);													// Get conversion factor from apparent power(VA) to DC power(W)
void writeOutputToReg(regu__Inst* pInst, ChargerStatusFields_type Status);								// Write outputs to register
#endif
