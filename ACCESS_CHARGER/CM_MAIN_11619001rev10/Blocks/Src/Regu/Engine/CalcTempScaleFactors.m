## Author: nicka <nicka@MICROPOWER41>
## Created: 2010-11-09

	% Temperature sensor theoretical computations:
	% Constants:
	%   R			  Resistance connected to GND in (V)
	%   NTC			Thermistor connected to U in (ohm)
	%   G			  Arbitrary gain for example from operational amplifier gain is unitless
	%   ADC		  ADC converter value in (bit)
	%   ADC_MAX ADC converter maximum value in (bit)
	%   U_ADC   Voltage at ADC in (V)
	%   U       Voltage divider supply voltage in (V)
	%   
	% ADC ratio to voltage:
	%   ADC = U_ADC/Vcc*ADC_max <=> U_ADC = ADC*Vcc/ADC_max
	% Thermistor calculations:
	%   U_ADC = U*G*R/(R + NTC) <=> U_ADC*(R + NTC) = U*G*R <=> U_ADC*NTC = (U*G - U_ADC)*R
	%   NTC/R = (U*G - U_ADC)/U_ADC = (U*G - ADC*Vcc/ADC_max)/(ADC*Vcc/ADC_max) = (U*G*ADC_max/Vcc - ADC)/ADC
	%
	% Steinhart-Hart equation transformation:
	%   1/T = A*ln(R_NTC)^3 + B*ln(R_NTC)^2 + C*ln(R_NTC) + D Steinhart-Hart equation
	%   measured is R_NTC/R and observe the equation ln(R_NTC/R*R) = ln(R_NTC/R) + ln(R)
	%   1/T = A*ln(R_NTC/R*R)^3 + B*ln(R_NTC/R*R)^2 + C*ln(R_NTC/R*R) + D =
	%   A*(ln(R_NTC/R) + ln(R))^3 + B*(ln(R_NTC/R) + ln(R))^2 + C*(ln(R_NTC/R) + ln(R)) + D =
	%   A*(ln(R_NTC/R)^3 + 3*ln(R_NTC/R)^2*ln(R) + 3*ln(R_NTC/R)*ln(R)^2 + ln(R)^3) + B*(ln(R_NTC/R)^2 + 2*ln(R_NTC/R)*ln(R) + ln(R)^2) + C*(ln(R_NTC/R) + ln(R)) + D =
	%   A*ln(R_NTC/R)^3 + (3*A*ln(R) + B)*ln(R_NTC/R)^2 + (3*A*ln(R)^2 + 2*B*ln(R) + C)*ln(R_NTC/R) + (A*ln(R)^3 + B*ln(R)^2 + C*ln(R) + D)
	%
	%   This formula should be possible to fit to any combination of R and NTC thermistor. The formula however fit badly with an added value
	%
	% Conclussion:
	%   The Steinhart-Hart equation could be fitted to any value for the voltage divider resistor and addition of a constant. This is rather
	%   important since the same formula with different constants could be then calculating the temperature.

function CalcTempScaleFactors(type="NTC10k")

switch type
case "MP-HF1_KTY83_120"
	% The next time add switch here but plot the same values
	tr = KTY83_120();
	temperatures = 273.15 + tr.temperatures;
	R_PTC = tr.R_PTC;

	R = 10e3;
	P = polyfit(R_PTC(5:end-5)/R, temperatures(5:end-5), 2);

	A = P(3);
	B = P(2);
	C = P(1);

	figure
	plot(R_PTC, temperatures - polyval(P, R_PTC/R))
	title("Error between datasheet and calculated")
	legend("temperatures - calculated")
	xlabel("log(R_{PTC})")
	ylabel("Temperature degree Celsius")

	figure
	plot(R_PTC, temperatures-273.15, [50 R_PTC 3000 3500 4000 5000], polyval(P, [50 R_PTC 3000 3500 4000 5000]/R)-273.15)
	title("Calculated as in software")
	legend("temperatures", "calculated")
	xlabel("R_{PTC}")
	ylabel("Temperature degree Celsius")
	text(600, -50, sprintf("R = 10kohm\nT = %f + %f*ln(R_{PTC}/R) + %f*ln(R_{PTC}/R)*ln(R_{PTC}/R)", A, B, C))
  return
case "MP-HF1_NTC10k"
	% The next time add switch here but plot the same values
	tr = NTC10k();
	temperatures = tr.temperatures;
	R_NTC = tr.R_NTC;
	R = 10e3;
case "MPAccess_board_NTC10k"
	% The next time add switch here but plot the same values
	tr = NTC10k();
	temperatures = tr.temperatures;
	R_NTC = tr.R_NTC;
	R = 10e3;
case "NTC10k_10k"
	% The next time add switch here but plot the same values
	tr = NTC10k();
	temperatures = tr.temperatures;
	R_NTC = tr.R_NTC;
  R = 10e3;
case "MP-HF2_NTC5k"
	% The next time add switch here but plot the same values
	tr = NTC10k();
	temperatures = tr.temperatures;
	R_NTC = tr.R_NTC;
	R=1;
case "MP-HF3_NTC22k_10k"
	% The next time add switch here but plot the same values
	tr = ntc22k();
	temperatures = tr.temperatures;
	R_NTC = tr.R_NTC;
	R = 820;
otherwise
	error("Usage: KTY83_120(""MP-HF1_KTY83_120""|""MP-HF1_NTC10k""|""MPAccess_board_NTC10k""|""NTC10k_10k""|""MP-HF2_NTC5k"")\n")
endswitch
	measured = log(R_NTC/R);
	P = polyfit(measured, 1./temperatures, 3)

	figure
	plot(R_NTC, temperatures-273.15, R_NTC, 1./polyval(P, measured)-273.15)
	title("Comparison between from datasheet and calculated")
	legend("temperatures", "calculated")
	xlabel("R_{NTC}")
	ylabel("Temperature degree Celsius")
	text(10000, 80, sprintf("1/T = (%e*ln(R_{NTC})^3 + %e*ln(R_{NTC})^2 + %e*ln(R_{NTC}) + %e) ", P(1), P(2), P(3), P(4)))

	figure
	plot(R_NTC, temperatures - 1./polyval(P, measured))
	title("Error between datasheet and calculated")
	legend("temperatures - calculated")
	xlabel("R_{NTC}")
	ylabel("Temperature degree Celsius")
endfunction

function tr = ntc22k()
	tr.temperatures = 273.15 + [-55 -50 -45 -40 -35 -30 -25 -20 -15 -10 -5 0 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 105 110 115 120 125 130 135 140 145 150 155 160 165 170 175 180 185 190 195 200];
	tr.R_NTC = [241.1 177.4 131.9 99.09 75.17 57.54 44.44 34.60 27.16 21.48 17.11 13.72 11.08 9.001 7.357 6.048 5.000 4.156 3.472 2.914 2.458 2.083 1.773 1.515 1.300 1.120 0.9683 0.8404 0.7319 0.6397 0.5608 0.4933 0.4352 0.3851 0.3418 0.3042 0.2714 0.2428 0.2178 0.1959 0.1765 0.1595 0.1444 0.1310 0.1192 0.1086 0.0991 0.0907 0.0831 0.0763 0.0702 0.0647]*1e3;
endfunction

function tr = KTY83_120()
	tr.temperatures = 273.15 + [-55 -50 -40 -30 -20 -10 0 10 20 25 30 40 50 60 70 80 90 100 110 120 125 130 140 150 160 170 175];
	tr.R_PTC = [500 525 577 632 691 754 820 889 962 1000 1039 1118 1202 1288 1379 1472 1569 1670 1774 1882 1937 1993 2107 2225 2346 2471 2535];
endfunction

function tr = NTC10k()
	tr.temperatures = 273.15 + [-40 -35 -30 -25 -20 -15 -10 -5 0 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 105 110 115 120 125];
	tr.R_NTC = [336450 242660 176960 130410 97072 72951 55326 42326 32650 25391 19899 15711 12492 10000 8056.8 6531.4 5326.4 4368.4 3602.3 2986.1 2487.8 2082.7 1751.6 1479.3 1255.2 1069.7 915.43 786.63 678.6 587.58 510.59 445.24 389.56 341.92];
endfunction

function tr = NTC5k()
	tr.temperatures = 273.15 + [-55 -50 -45 -40 -35 -30 -25 -20 -15 -10 -5 0 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 105 110 115 120 125 130 135 140 145 150 155 160 165 170 175 180 185 190 195 200];
	tr.R_NTC = [241.1 177.4 131.9 99.09 75.17 57.54 44.44 34.60 27.16 21.48 17.11 13.72 11.08 9.001 7.357 6.048 5.000 4.156 3.472 2.914 2.458 2.083 1.773 1.515 1.300 1.120 0.9683 0.8404 0.7319 0.6397 0.5608 0.4933 0.4352 0.3851 0.3418 0.3042 0.2714 0.2428 0.2178 0.1959 0.1765 0.1595 0.1444 0.1310 0.1192 0.1086 0.0991 0.0907 0.0831 0.0763 0.0702 0.0647]*1e3;
endfunction
