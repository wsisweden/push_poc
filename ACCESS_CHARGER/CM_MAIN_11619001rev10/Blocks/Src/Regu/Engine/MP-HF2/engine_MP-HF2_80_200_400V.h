/*
 ********************************* \
 * Engine MP-HF2 80V 200A 400VAC * \
 ********************************* \
 *
 *  Created: 2012-03-14
 *   Author: nicka
 */

#ifndef ENGINE_MP_HF2_80_200_400V_H
#define ENGINE_MP_HF2_80_200_400V_H_

#include "engines_types.h"
#include "engine_MP-HF2_standard_parameters.h"
#include "math.h"

static const EngineUIchar_type MP_HF2_80_200_400V__UIchar[] = ENGINE_MP_HF2_STANDARD_UI_CHARACTERISTICS(12, 40, 200);

#define ENGINE_MP_HF2_80_200_400V \
	{ \
		103,												/* Code */ \
		ENGINE_MP_HF2_STANDARD_RI,							/* Ri */ \
		ENGINE_MP_HF2_STANDARD_EFFICIENCY,					/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
		ENGINE_MP_HF2_STANDARD_EFFICIENCY_APPARENT,			/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
		\
		MP_HF2_80_200_400V__UIchar, 9,						/* UI-characteristic pointer */ \
		Engine_NoAccuracy,									/* Voltage accuracy */ \
		Engine_NoAccuracy, 									/* Current accuracy */ \
		\
		{													/* Regulator						*/ \
			ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF2_STD_ISET__GAIN_200A4440mV*ENGINE_MP_HF2_STD_80V), \
			ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF2_STD_ISET__GAIN_200A4440mV*ENGINE_MP_HF2_STD__GAIN_200A4440mV), \
			ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF2_STD_ISET__GAIN_200A4440mV*ENGINE_MP_HF2_STD__GAIN_200A4440mV*ENGINE_MP_HF2_STD_80V), \
			ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF2_STD_ISET__GAIN_200A4440mV) \
		}, \
		\
		{													/* Scale factors nominal								*/ \
			{												/* Normalized to bit									*/ \
				ENGINE_MP_HF2_STD_80V, 0.0,					/* Voltage scaling slope, offset						*/ \
				ENGINE_MP_HF2_STD__GAIN_200A4440mV,			/* Current scaling slope								*/ \
				ENGINE_MP_HF2_STD_OFFSET,					/* automatically adjusted offset, initialize anyway		*/ \
				ENGINE_MP_HF2_STD_ISET__GAIN_200A4440mV,	/* PWM current output scaling							*/ \
				ENGINE_MP_HF2_STD_PWM_OFFSET				/* PWM offset											*/ \
			}, \
			{NAN, NAN, NAN, NAN, NAN, NAN}					/* Bit to normalized, automatically calculated			*/ \
		}, \
		\
		&Engine_MP_HF2_StdTempHeatsink_T60_67,				/* Temperature heat sink */ \
		TempSensorDigital_High,								/* Temperature transformer */ \
		ENGINE_MP_HF2_STD_BIT_CURRENT(200.0)				/* Battery signal logic levels */ \
	} \

#endif /* ENGINE_MP_HF2_80_200_400V_H */
