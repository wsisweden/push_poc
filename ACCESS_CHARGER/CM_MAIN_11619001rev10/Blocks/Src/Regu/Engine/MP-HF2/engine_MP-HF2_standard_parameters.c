#include "engine_MP-HF2_standard_parameters.h"

/* Accuracy voltage */
/* Accuracy current */

/* Ri */

/* Conversion between input power and output power */

/* Conversion between phase current and output power */

/* Controller */

/* Voltage measurement standard scaling factors */

/* Current measurement standard scaling factors */

/* Temperature heat sink						 		Type					A*(log(R_NTC))^3 +		B*(log(R_NTC))^2 +		C*log(R_NTC) +		D				Short circuit	Open circuit	Derate		Over temperature */
const Temperature_type Engine_MP_HF2_StdTempHeatsink = {TempSensor_MTM_HF2_NTC,	1.588524e-7,			-6.541889e-8,			2.661412e-4,		9.940488e-4,	200.0,			60000.0,		87,			92}; /* NTC 10k, R = 10kΩ */

/* Temperature heat sink						 		Type						A +			B*log(R_NTC/R) +	C*log(R_NTC/R)*log(R_NTC/R) +	unused	Short circuit	Open circuit	Derate		Over temperature */
const Temperature_type Engine_MP_HF2_StdTempHeatsink_T60_67 = {TempSensor_5V_Ntc,	0.003354,	0.000257,			0.000002,						NAN,	0.03,			7.0,			60,			67}; /* NTC 10k, R = 10kΩ */
const Temperature_type Engine_MP_HF2_StdTempHeatsink_T63_70 = {TempSensor_5V_Ntc,	0.003354,	0.000257,			0.000002,						NAN,	0.03,			7.0,			63,			70}; /* NTC 10k, R = 10kΩ */

/* Temperature transformer */
const TempSenseDigital_enum Engine_MP_HF2_StdTempTrafo = TempSensorDigital_No;
