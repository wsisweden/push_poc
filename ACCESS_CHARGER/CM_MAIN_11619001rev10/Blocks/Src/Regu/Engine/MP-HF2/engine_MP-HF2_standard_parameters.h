#ifndef ENGINE_MP_HF2_STANDARD_PARAMETERS_H
#define ENGINE_MP_HF2_STANDARD_PARAMETERS_H

#include "engines_types.h"
#include "math.h"

#define ENGINE_MP_HF2_STANDARD_UI_CHARACTERISTICS(nominalCellsMin, nominalCellsMax, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,						0.10*nominalCurrent},	/* Voltage absolute				All currents should be multiplied with nominal current */ \
	{6.0,						0.10*nominalCurrent},	/* Voltage absolute */ \
	{6.0,						0.10*nominalCurrent},	/* Voltage absolute */ \
	{1.8*nominalCellsMin + 0.2,	0.10*nominalCurrent},	/* (1.8V/cell + 0.2V) */ \
	{1.8*nominalCellsMin + 0.2,	1.00*nominalCurrent},	/* (1.8V/Cell + 0.2V) */ \
	{2.4*nominalCellsMax + 0.8,	1.00*nominalCurrent},	/* (2.4V/cell + 0.6V) */ \
	{2.6*nominalCellsMax + 0.6,	0.92*nominalCurrent},	/* (2.6V/cell + 0.5V) */ \
	{2.8*nominalCellsMax + 0.4,	0.25*nominalCurrent},	/* (2.8V/cell + 0.2V) */ \
	{2.8*nominalCellsMax + 0.4,	0.00*nominalCurrent}	/* (2.8V/cell + 0.2V) */ \
};

/* Ri */
#define ENGINE_MP_HF2_STANDARD_RI (0.0/1000)

/* Conversion between input power and output power */
#define ENGINE_MP_HF2_STANDARD_EFFICIENCY (0.94)

/* Conversion between phase current and output power */
#define ENGINE_MP_HF2_STANDARD_EFFICIENCY_APPARENT (1.7320508*400.0*0.94*0.95)

/* Accuracy voltage */
/* Accuracy current */
extern const EngineAccuracy_type Engine_NoAccuracy[2];

/* Controller
 * A block diagram could be found in the folder Regu.
 * Constants:
 *   R(s)			Reference signal
 *   F(s)			PI-regulator
 *   G(s)			System, input PWM signal output several values that should be limited
 *   Kpwm			Conversion constant from PWM signal in (bit) to current in (A)
 *   Kmeas			Conversion constant from value that should be limited in (unit) to (bit)
 * Calculations:
 *   Y(s) = PI(s)*Kpwm*G(s)(R(s) - Y(s)*Kmeas) <=> Y(s)(1 + PI(s)*Kpwm*G(s)*Kmeas) = PI(s)*Kpwm*G(s)R(s)
 *   Y(s)/R(S) =  PI(s)*Kpwm*G(s)/(1 + PI(s)*Kpwm*G(s)*Kmeas)
 *   Kpwm and Kmeas change for different chargers and if the dynamic response is the same it should be possible to adjust the regulator so that
 *   the same close loop dynamic response is achieved.
 *   Y(s)/R(S) =  PI(s)*(Kpwm*Kmeas)/(Kpwm*Kmeas)*Kpwm*G(s)/(1 + PI(s)*(Kpwm*Kmeas)/(Kpwm*Kmeas)*Kpwm*G(s)*Kmeas), PIsubstitute(s) = PI(s)*(Kpwm*Kmeas)
 *   Y(s)/R(S) =  PI_substitute(s)/Kmeas*G(s)/(1 + PI_substitue(s)*G(s)), PIsubstitute(s) = PI(s)*(Kpwm*Kmeas)
 *   This equation with PI_substitute(s) is what we really want so PI(s) = PIsubstitute(s)/(Kpwm*Kmeas)
 */
#define ENGINE_MP_HF2_STD_REGULATOR_U(k) 0.050*107.114*53.222/(k) /* KpU */,	0.01*4.0*107.114*53.222/(k) /* KiU */,	2.000 /* KchooseI */
#define ENGINE_MP_HF2_STD_REGULATOR_I(k) 0.050*107.114*19.209/(k) /* KpI */,	0.01*5.0*107.114*19.209/(k) /* KiI */,	1.000 /* KchooseI */
#define ENGINE_MP_HF2_STD_REGULATOR_P(k) 0.080*107.114*1022.3/(k) /* KpP */,	0.01*8.0*107.114*1022.3/(k) /* KiP */,	1.000 /* KchooseI */
#define ENGINE_MP_HF2_STD_REGULATOR_T(k) 100.0*107.114*1.0000/(k) /* KpT */,	0.01*0.3*107.114*1.0000/(k) /* KiT */,	400.0 /* KchooseI */

/* Voltage measurement standard scaling factors
 * Calculations:
 *   U_GTM_OPEN = Ubatt*R2/(R1 + R2)
 *   I_GTM_SHORT = Ubatt/R1
 *   R_GTM = U_GTM_OPEN/I_GTM_SHORT = R1*R2/(R1 + R2)
 *   U_ADC = U_GTM_OPEN*8.2kΩ/(R_GTM + 1kΩ + 8.2kΩ) = Ubatt*R2/(R1 + R2)*8.2kΩ/(R_GTM + 1kΩ + 8.2kΩ)
 *   ADC = U_ADC/3.0*4095 = Ubatt*R2/(R1 + R2)*8.2kΩ/(R_GTM + 1kΩ + 8.2kΩ)*3.0*4095
 *   UadSlope = ADC/Ubatt = R2/(R1 + R2)*8.2kΩ/(R_GTM + 1kΩ + 8.2kΩ)*3.0*4095
 */
#define ENGINE_MP_HF2_STD_48V 54.76255					/* in (bit/V) */
#define ENGINE_MP_HF2_STD_80V 31.50994					/* in (bit/V) */

/* Current measurement standard scaling factors
 * Calculations:
 *   U_GTM_OPEN = Ui
 *   I_GTM_SHORT = Ui/1.5kΩ
 *   R_GTM = U_GTM_OPEN/I_GTM_SHORT = 1.5kΩ
 *   U_ADC = U_GTM_OPEN*5.6kΩ/(1.5kΩ + 3.9kΩ + 5.6kΩ) = Ui*5.6/11
 *   ADC = U_ADC/3.0*4095 = Ui*5.6/11/3.0*4095
 *   IadSlope = G*ADC/Ubatt = G*5.6/11/3.0*4095, G in (V/A)
 */
#define ENGINE_MP_HF2_STD__GAIN_100A4440mV 28.64381		/* in (bit/A) */
#define ENGINE_MP_HF2_STD__GAIN_160A4420mV 16.57500 	/* in (bit/A) */
#define ENGINE_MP_HF2_STD__GAIN_200A4440mV 14.31513		/* in (bit/A) */
#define ENGINE_MP_HF2_STD__GAIN_320A4420mV 8.90352		/* in (bit/A) */

#define ENGINE_MP_HF2_STD_OFFSET -198.99				/* in (bit) */

/* Iset output standard scaling factors
 * Constants:
 *   D					Duty cycle in interval [0, 1]
 *   PWM_CYCLE_FULL		in (bit)
 *   U_Iset				in (V)
 *   IpwmSlope			in (A/bit)
 *   ISET_GAIN			in (A/V)
 * Calculations:
 *   G_amplifier = (47kΩ + 91kΩ)/91kΩ = 138/91
 *   U_Iset = D*3.3*G_amplifier*100kΩ/(100kΩ + 1kΩ) = D*3.3*138/91*100/101
 *   I = U_Iset*ISET__GAIN = PWM/PWM_CYCLE_FULL*3.3*138/91*100/101*ISET__GAIN
 *   IpwmSlope = PWM/I = PWM_CYCLE_FULL/3.3/138*91/100*101/ISET__GAIN)
 */
#define ENGINE_MP_HF2_STD_ISET__GAIN_100A4440mV 172.050
#define ENGINE_MP_HF2_STD_ISET__GAIN_160A4420mV 107.047
#define ENGINE_MP_HF2_STD_ISET__GAIN_200A4440mV 86.025
#define ENGINE_MP_HF2_STD_ISET__GAIN_320A4420mV 53.523

#define ENGINE_MP_HF2_STD_PWM_OFFSET 1509				/* in (bit) */

/* Temperature heat sink */
extern const Temperature_type Engine_MP_HF2_StdTempHeatsink;
extern const Temperature_type Engine_MP_HF2_StdTempHeatsink_T60_67;
extern const Temperature_type Engine_MP_HF2_StdTempHeatsink_T63_70;

/* Temperature transformer */

/* Battery signal logic level */
#define ENGINE_MP_HF2_STD_BIT_CURRENT(nominalCurrent) {-0.03*nominalCurrent, 0.6*nominalCurrent, 200}

#endif
