/*
 * engine.c
 *
 *  Created on: Oct 1, 2010
 *      Author: nicka
 */
#include "engines_types.h"
#include "global.h"
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "chalg.h"

#include "engine_default.h"

#include "MP-HF1/engine_MP-HF1_24_200_400V.h" 	// Engine code 28
#include "MP-HF1/engine_MP-HF1_24_340_400V.h" 	// Engine code 17
#include "MP-HF1/engine_MP-HF1_24_400_400V.h" 	// Engine code 23
#include "MP-HF1/engine_MP-HF1_24_170_480V.h"	// Engine code 30

#include "PF-HF1/engine_PF-HF1_36_80_230V.h"	// Engine code 11
#include "MP-HF1/engine_MP-HF1_24_150_400V.h"	// Engine code 7
#include "MP-HF1/engine_MP-HF1_36_150_400V.h"	// Engine code 16
#include "MP-HF1/engine_MP-HF1_24_150_440V.h"	// Engine code 40
#include "MP-HF1/engine_MP-HF1_24_300_440V.h"	// Engine code 60
#include "MP-HF1/engine_MP-HF1_36_150_440V.h"	// Engine code 41
#include "MP-HF1/engine_MP-HF1_36_170_220V.h"	// Engine code 33
#include "MP-HF1/engine_MP-HF1_36_170_480V.h"	// Engine code 24
#include "MP-HF1/engine_MP-HF1_36_200_400V.h"	// Engine code 58
#include "MP-HF1/engine_MP-HF1_36_200_440V.h"	// Engine code 131
#include "MP-HF1/engine_MP-HF1_36_220_480V.h"	// Engine code 27
#include "MP-HF1/engine_MP-HF1_36_220_600V.h"	// Engine code 49
#include "MP-HF1/engine_MP-HF1_36_300_440V.h"	// Engine code 61
#include "MP-HF1/engine_MP-HF1_36_340_220V.h"	// Engine code 55
#include "MP-HF1/engine_MP-HF1_36_340_480V.h"	// Engine code 25
#include "MP-HF1/engine_MP-HF1_36_400_400V.h"	// Engine code 59
#include "MP-HF1/engine_MP-HF1_36_400_440V.h"	// Engine code 132
#include "MP-HF1/engine_MP-HF1_36_420_480V.h"	// Engine code 46
#include "MP-HF1/engine_MP-HF1_36_420_600V.h"	// Engine code 52

#include "MP-HF1/engine_MP-HF1_48_100_400V.h"	// Engine code 8
#include "MP-HF1/engine_MP-HF1_48_100_440V.h"	// Engine code 42
#include "MP-HF1/engine_MP-HF1_48_130_220V.h"	// Engine code 34
#include "MP-HF1/engine_MP-HF1_48_130_400V.h"	// Engine code 14
#include "MP-HF1/engine_MP-HF1_48_130_440V.h"	// Engine code 43
#include "MP-HF1/engine_MP-HF1_48_165_400V.h"	// Engine code 36
#include "MP-HF1/engine_MP-HF1_48_165_440V.h"	// Engine code 65
#include "MP-HF1/engine_MP-HF1_48_165_480V.h"	// Engine code 31
#include "MP-HF1/engine_MP-HF1_48_165_600V.h"	// Engine code 50
#include "MP-HF1/engine_MP-HF1_48_200_400V.h"	// Engine code 18
#include "MP-HF1/engine_MP-HF1_48_260_400V.h"	// Engine code 22
#include "MP-HF1/engine_MP-HF1_48_260_220V.h"	// Engine code 56
#include "MP-HF1/engine_MP-HF1_48_260_440V.h"	// Engine code 62
#include "MP-HF1/engine_MP-HF1_48_330_400V.h"	// Engine code 38
#include "MP-HF1/engine_MP-HF1_48_330_440V.h"	// Engine code 135
#include "MP-HF1/engine_MP-HF1_48_330_480V.h"	// Engine code 47
#include "MP-HF1/engine_MP-HF1_48_330_600V.h"	// Engine code 53

#include "MP-HF1/engine_MP-HF1_80_60_400V.h"	// Engine code 9
#include "MP-HF1/engine_MP-HF1_80_60_440V.h"	// Engine code 44
#include "MP-HF1/engine_MP-HF1_80_80_220V.h"	// Engine code 35
#include "MP-HF1/engine_MP-HF1_80_80_400V.h"	// Engine code 15
#include "MP-HF1/engine_MP-HF1_80_80_440V.h"	// Engine code 45
#include "MP-HF1/engine_MP-HF1_80_80_480V.h"	// Engine code 26
#include "MP-HF1/engine_MP-HF1_80_100_400V.h"	// Engine code 37
#include "MP-HF1/engine_MP-HF1_80_100_440V.h"	// Engine code 63
#include "MP-HF1/engine_MP-HF1_80_100_480V.h"	// Engine code 32
#include "MP-HF1/engine_MP-HF1_80_100_600V.h"	// Engine code 51
#include "MP-HF1/engine_MP-HF1_80_120_400V.h"	// Engine code 19
#include "MP-HF1/engine_MP-HF1_80_160_400V.h"	// Engine code 20
#include "MP-HF1/engine_MP-HF1_80_160_440V.h"	// Engine code 64
#include "MP-HF1/engine_MP-HF1_80_160_480V.h"	// Engine code 29
#include "MP-HF1/engine_MP-HF1_80_160_220V.h"	// Engine code 57
#include "MP-HF1/engine_MP-HF1_80_200_400V.h"	// Engine code 39
#include "MP-HF1/engine_MP-HF1_80_200_480V.h"	// Engine code 48
#include "MP-HF1/engine_MP-HF1_80_200_600V.h"	// Engine code 54

#include "PF-HF1/engine_PF-HF1_24_120_240V.h"	// Engine code 12
#include "PF-HF1/engine_PF-HF1_48_60_230V.h"	// Engine code 13
//#include "PF-HF1/engine_PF-HF1_400_7_240V.h"	// Engine code 114 - Model 32 - Included in Lion from 11617011rev02 (for info)

#include "MP-HF2/engine_MP-HF2_48_165_400V.h"	// Engine code 100 - 2015 model
//#include "MP-HF2/engine_MP-HF2_48_320_400V.h"	// Engine code 101 - GTM engine is postponed
#include "MP-HF2/engine_MP-HF2_80_100_400V.h"	// Engine code 102 - 2015 model
//#include "MP-HF2/engine_MP-HF2_80_200_400V.h"	// Engine code 103 - GTM engine is postponed
#include "MP-HF3/engine_MP-HF3_24_130__220V.h"	// Engine code 111 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_24_130__400V.h"	// Engine code 106 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_24_130__480V.h"	// Engine code 107 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_36_105__220V.h"	// Engine code 112 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_36_105__400V.h"	// Engine code 109 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_36_105__480V.h"	// Engine code 108 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_48_80__220V.h"	// Engine code 113 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_48_80__400V.h"	// Engine code 105 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_48_80__480V.h"	// Engine code 110 - Pinguin engine
#include "MP-HF3/engine_MP-HF3_48_130__400V.h"	// Engine code 136 - KingsPinguin engine
#include "MP-HF3/engine_MP-HF3_80_80__400V.h"	// Engine code 137 - KingsPinguin engine

//#include "MP-HF4/engine_MP-HF4_450_25_400V.h"	// Engine code 300 - High voltage engine - Included in Lion from 11619002rev08 (for info)

#include "MP-HF1/engine_MP-HF1_36_300_480V.h"	// Engine code 115
#include "MP-HF1/engine_MP-HF1_48_210_480V.h"	// Engine code 116
#include "MP-HF1/engine_MP-HF1_80_130_480V.h"	// Engine code 117
#include "MP-HF1/engine_MP-HF1_36_220_480V_13kW.h"	// Engine code 122
#include "MP-HF1/engine_MP-HF1_36_300_400V.h"	// Engine code 118
#include "MP-HF1/engine_MP-HF1_48_210_400V.h"	// Engine code 119
#include "MP-HF1/engine_MP-HF1_80_130_400V.h"	// Engine code 120
#include "MP-HF1/engine_MP-HF1_36_220_400V_13kW.h"	// Engine code 123
#include "MP-HF1/engine_MP-HF1_120_90_400V.h"	// Engine code 121
#include "MP-HF1/engine_MP-HF1_36_600_480V.h"	// Engine code 124
#include "MP-HF1/engine_MP-HF1_48_440_480V.h"	// Engine code 125
#include "MP-HF1/engine_MP-HF1_80_260_480V.h"	// Engine code 126
#include "MP-HF1/engine_MP-HF1_36_600_400V.h"	// Engine code 127
#include "MP-HF1/engine_MP-HF1_48_440_400V.h"	// Engine code 128
#include "MP-HF1/engine_MP-HF1_80_260_400V.h"	// Engine code 129
#include "MP-HF1/engine_MP-HF1_120_180_400V.h"	// Engine code 130
#include "MP-HF1/engine_MP-HF1_120_90_480V.h"	// Engine code 133
#include "MP-HF1/engine_MP-HF1_120_180_480V.h"	// Engine code 134

#include "PF-HF2/engine_PF-HF2_24_40_230V.h"	// Engine code 0 - Robust engine
#include "PF-HF2/engine_PF-HF2_24_80_230V.h"	// Engine code 1 - Robust engine
#include "PF-HF2/engine_PF-HF2_36_22_230V.h"	// Engine code 2 - Robust engine
#include "PF-HF2/engine_PF-HF2_48_20_230V.h"	// Engine code 3 - Robust engine
#include "PF-HF2/engine_PF-HF2_36_53_230V.h"	// Engine code 4 - Robust engine
#include "PF-HF2/engine_PF-HF2_48_40_230V.h"	// Engine code 5 - Robust engine
#include "PF-HF2/engine_PF-HF2_24_105_230V.h"	// Engine code 6 - Robust engine
//#include "PF-HF2/engine_PF-HF2_280_10_230V.h"	// Engine code 200 - Robust engine - Robust test revision - not tested for Talgoxen
//#include "PF-HF2/engine_PF-HF2_96_30_230V.h"	// Engine code 202 - Robust engine - Robust test revision - not tested for Talgoxen
#include "PF-HF2/engine_PF-HF2_48_60_230V.h"	// Engine code 209 (is 7 in Robust program) - Robust engine

/**
 * Engine table
 */
static const Engine_type const_P Engine[ENGINE_LEN] =
{
		ENGINE_PF_HF2_24_40_230V,	// Engine code 0 - Robust engine
		ENGINE_PF_HF2_24_80_230V, 	// Engine code 1 - Robust engine
		ENGINE_PF_HF2_36_22_230V,	// Engine code 2 - Robust engine
		ENGINE_PF_HF2_48_20_230V,	// Engine code 3 - Robust engine
		ENGINE_PF_HF2_36_53_230V,	// Engine code 4 - Robust engine
		ENGINE_PF_HF2_48_40_230V,	// Engine code 5 - Robust engine
		ENGINE_PF_HF2_24_105_230V,	// Engine code 6 - Robust engine
		ENGINE_24_150_400V,			// Engine code 7
		ENGINE_48_100_400V,			// Engine code 8
		ENGINE_80_60_400V,			// Engine code 9
		ENGINE_36_80_230V,			// Engine code 11 - Model 32
		ENGINE_24_120_240V,			// Engine code 12 - Model 32
		ENGINE_48_60_230V,			// Engine code 13 - Model 32
		ENGINE_48_130_400V,			// Engine code 14
		ENGINE_80_80_400V,			// Engine code 15
		ENGINE_36_150_400V,			// Engine code 16
		ENGINE_24_340_400V,			// Engine code 17
		ENGINE_48_200_400V,			// Engine code 18
		ENGINE_80_120_400V,			// Engine code 19
		ENGINE_80_160_400V,			// Engine code 20
		ENGINE_48_260_400V,			// Engine code 22
		ENGINE_24_400_400V,			// Engine code 23
		ENGINE_36_170_480V,			// Engine code 24
		ENGINE_36_340_480V,			// Engine code 25
		ENGINE_80_80_480V,			// Engine code 26
		ENGINE_36_220_480V,			// Engine code 27
		ENGINE_24_200_400V,			// Engine code 28
		ENGINE_80_160_480V,			// Engine code 29
		ENGINE_24_170_480V,			// Engine code 30
		ENGINE_48_165_480V,			// Engine code 31
		ENGINE_80_100_480V,			// Engine code 32
		ENGINE_36_170_220V,			// Engine code 33
		ENGINE_48_130_220V,			// Engine code 34
		ENGINE_80_80_220V,			// Engine code 35
		ENGINE_48_165_400V,			// Engine code 36
		ENGINE_80_100_400V,			// Engine code 37
		ENGINE_48_330_400V,			// Engine code 38
		ENGINE_80_200_400V,			// Engine code 39
		ENGINE_24_150_440V,			// Engine code 40
		ENGINE_36_150_440V,			// Engine code 41
		ENGINE_48_100_440V,			// Engine code 42
		ENGINE_48_130_440V,			// Engine code 43
		ENGINE_80_60_440V,			// Engine code 44
		ENGINE_80_80_440V,			// Engine code 45
		ENGINE_36_420_480V,			// Engine code 46
		ENGINE_48_330_480V,			// Engine code 47
		ENGINE_80_200_480V,			// Engine code 48
		ENGINE_36_220_600V,			// Engine code 49
		ENGINE_48_165_600V,			// Engine code 50
		ENGINE_80_100_600V,			// Engine code 51
		ENGINE_36_420_600V,			// Engine code 52
		ENGINE_48_330_600V,			// Engine code 53
		ENGINE_80_200_600V,			// Engine code 54
		ENGINE_36_340_220V,			// Engine code 55
		ENGINE_48_260_220V,			// Engine code 56
		ENGINE_80_160_220V,			// Engine code 57
		ENGINE_36_200_400V,			// Engine code 58
		ENGINE_36_400_400V,			// Engine code 59
		ENGINE_24_300_440V,			// Engine code 60
		ENGINE_36_300_440V,			// Engine code 61
		ENGINE_48_260_440V,			// Engine code 62
		ENGINE_80_100_440V,			// Engine code 63
		ENGINE_80_160_440V,			// Engine code 64
		ENGINE_48_165_440V,			// Engine code 65
		ENGINE_MP_HF2_48_165_400V,	// Engine code 100 - 2015 model
		//ENGINE_MP_HF2_48_320_400V,	// Engine code 101 - GTM engine is postponed
		ENGINE_MP_HF2_80_100_400V,	// Engine code 102 - 2015 model
		//ENGINE_MP_HF2_80_200_400V,	// Engine code 103 - GTM engine is postponed
		ENGINE_MP_HF3_48_80__400V,	// Engine code 105 - Pinguin engine
		ENGINE_MP_HF3_24_130__400V,	// Engine code 106 - Pinguin engine
		ENGINE_MP_HF3_24_130__480V,	// Engine code 107 - Pinguin engine
		ENGINE_MP_HF3_36_105__480V,	// Engine code 108 - Pinguin engine
		ENGINE_MP_HF3_36_105__400V,	// Engine code 109 - Pinguin engine
		ENGINE_MP_HF3_48_80__480V,	// Engine code 110 - Pinguin engine
		ENGINE_MP_HF3_24_130__220V,	// Engine code 111 - Pinguin engine
		ENGINE_MP_HF3_36_105__220V,	// Engine code 112 - Pinguin engine
		ENGINE_MP_HF3_48_80__220V,	// Engine code 113 - Pinguin engine
		//ENGINE_400_7_240V,			// Engine code 114 - Model 32 - Included in Lion from 11617011rev02 (for info)
		ENGINE_36_300_480V,			// Engine code 115 - 13kW engine
		ENGINE_48_210_480V,			// Engine code 116 - 13kW engine
		ENGINE_80_130_480V,			// Engine code 117 - 13kW engine
		ENGINE_36_300_400V,			// Engine code 118 - 13kW engine
		ENGINE_48_210_400V,			// Engine code 119 - 13kW engine
		ENGINE_80_130_400V,			// Engine code 120 - 13kW engine
		ENGINE_120_90_400V,			// Engine code 121 - 13kW engine
		ENGINE_36_220_480V_13KW,	// Engine code 122 - 13kW engine
		ENGINE_36_220_400V_13KW,	// Engine code 123 - 13kW engine
		ENGINE_36_600_480V,			// Engine code 124 - 13kW engine
		ENGINE_48_440_480V,			// Engine code 125 - 13kW engine
		ENGINE_80_260_480V,			// Engine code 126 - 13kW engine
		ENGINE_36_600_400V,			// Engine code 127 - 13kW engine
		ENGINE_48_440_400V,			// Engine code 128 - 13kW engine
		ENGINE_80_260_400V,			// Engine code 129 - 13kW engine
		ENGINE_120_180_400V,		// Engine code 130 - 13kW engine
		ENGINE_36_200_440V,			// Engine code 131
		ENGINE_36_400_440V,			// Engine code 132
		ENGINE_120_90_480V,			// Engine code 133 - 13kW engine
		ENGINE_120_180_480V,		// Engine code 134 - 13kW engine
		ENGINE_48_330_440V,			// Engine code 135
		ENGINE_MP_HF3_48_130__400V,	// Engine code 136 - KingsPinguin engine
		ENGINE_MP_HF3_80_80__400V,	// Engine code 137 - KingsPinguin engine
		//ENGINE_PF_HF2_280_10_230V,	// Engine code 200 - Robust engine - Robust test revision - not tested for Talgoxen
		//ENGINE_PF_HF2_96_30_230V,	// Engine code 202 - Robust engine - Robust test revision - not tested for Talgoxen
		ENGINE_PF_HF2_48_60_230V,	// Engine code 209 (is 7 in Robust program) - Robust engine
		//ENGINE_450_25_400V,			// Engine code 300 - High voltage engine - Included in Lion from 11619002rev08 (for info)
};

/**
 * CEC restrictions type
 */
typedef struct engine__CECRestrictions {			/*''''''''''''''''''''''''''''' RAM	*/
	Uint8 	type;									/* */
	Uint16	minAh6c;								/* */
	Uint16	maxAh6c;								/* */
	Uint16	minAh12c;								/* */
	Uint16	maxAh12c;								/* */
	Uint16	minAh18c;								/* */
	Uint16	maxAh18c;								/* */
	Uint16	minAh24c;								/* */
	Uint16	maxAh24c;								/* */
	Uint16	minAh40c;								/* */
	Uint16	maxAh40c;								/* */
} engine__CECRestrictions;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * CEC restrictions table
 */
static const engine__CECRestrictions CECRestrictions[CEC_ENGINE_LEN] =
{
		{27, 0,	0,	510, 	1360, 	510,	1360, 	0,		0,		0,		0},			/*	ENGINE_36_220_480V			*/
		{46, 0,	0,	1020, 	1360, 	680, 	1360, 	0, 		0, 		0,		0},			/*	ENGINE_36_420_480V	double	*/
		{31, 0,	0,	510, 	1190, 	510, 	1190, 	510, 	1190,	0,		0},			/*	ENGINE_48_165_480V			*/
		{47, 0,	0,	0, 		0,		510, 	1190, 	510, 	1190, 	0,		0},			/*	ENGINE_48_330_480V	double	*/
		{32, 0,	0,	510, 	900, 	340, 	930, 	300, 	930, 	300,	930},		/*	ENGINE_80_100_480V			*/
		{48, 0,	0,	0, 		0, 		510, 	930, 	510, 	930, 	600,	930},		/*	ENGINE_80_200_480V 	double	*/
		{33, 0,	0,	510, 	1360, 	510,	1360, 	0,		0,		0,		0},			/*	ENGINE_36_170_220V			*/
		{55, 0,	0,	1020, 	1360, 	680, 	1360, 	0, 		0, 		0,		0},			/*	ENGINE_36_340_220V 	double	*/
		{34, 0,	0,	510, 	1360, 	510,	1190, 	510,	1190,	0,		0},			/*	ENGINE_48_130_220V			*/
		{56, 0,	0,	0,	 	0,		510, 	1190, 	510, 	1190, 	0,		0},			/*	ENGINE_48_260_220V double	*/
		{35, 0,	0,	0, 		0,	 	510, 	900, 	380, 	930, 	255,	930},		/*	ENGINE_80_80_220V			*/
		{57, 0,	0,	0, 		0,		0, 		0, 		510, 	930, 	300,	930},		/*	ENGINE_80_160_220V double	*/
		{24, 0,	0,	510, 	1360, 	510,	1360, 	0,		0,		0,		0},			/*	ENGINE_36_170_480V			*/
		{25, 0,	0,	1020, 	1360, 	680, 	1360, 	0, 		0, 		0,		0},			/*	ENGINE_36_340_480V	double	*/
		{14, 0,	0,	510, 	1360, 	510, 	1190, 	510, 	1190, 	0,		0},			/*	ENGINE_48_130_400V			*/
		{22, 0,	0,	0, 		0,		510, 	1190, 	510, 	1190, 	0,		0},			/*	ENGINE_48_260_400V	double	*/
		{26, 0,	0,	0, 		0, 		510, 	930, 	510, 	930, 	300,	930},		/*	ENGINE_80_80_480V			*/
		{29, 0,	0,	0, 		0, 		0, 		0, 		510, 	930, 	300,	930},		/*	ENGINE_80_160_480V 	double	*/
		{107,0,	0,	255, 	930, 	0,		0, 		0,		0,		0,		0},			/*	ENGINE_MP_HF3_24_130__480V	*/
		{111,0,	0,	255, 	930, 	0,		0, 		0,		0,		0,		0},			/*	ENGINE_MP_HF3_24_130__220V	*/
		{108,0,	0,	255, 	930, 	255,	930, 	0,		0,		0,		0},			/*	ENGINE_MP_HF3_36_105__480V	*/
		{112,0,	0,	255, 	930, 	255,	930, 	0,		0,		0,		0},			/*	ENGINE_MP_HF3_36_105__220V	*/
		{110,0,	0,	0, 		0, 		255,	930, 	255,	930,	0,		0},			/*	ENGINE_MP_HF3_48_80__480V	*/
		{113,0,	0,	0, 		0, 		255,	930, 	255,	930,	0,		0},			/*	ENGINE_MP_HF3_48_80__220V	*/
		{115,0,	0,	510, 	1360, 	510,	1360, 	0,		0,		0,		0},			/*	ENGINE_36_300_480V			*/
		{116,0,	0,	510, 	1360, 	510,	1360, 	510,	1360,	0,		0},			/*	ENGINE_48_210_480V			*/
		{117,0,	0,	510, 	900, 	340,	930, 	255,	930,	255,	930},		/*	ENGINE_80_130_480V			*/
		{124,0,	0,	0, 		0, 		510,	1360, 	0,		0,		0,		0},			/*	ENGINE_36_600_480V			*/
		{125,0,	0,	0, 		0, 		510,	1190, 	510,	1190,	0,		0},			/*	ENGINE_48_440_480V			*/
		{126,0,	0,	0, 		0, 		510,	930, 	510,	930,	510,	930},		/*	ENGINE_80_260_480V			*/
};

static void engineUpdateEngineType(void);		// Update engine type depending on engine code.

static Engine_type current = ENGINE_DEFAULT;
static Uint32 engineIndex;
static Uint32 engineType;

PUBLIC void engineUpdate()
{
	Uint32 engineCode;

	reg_get(&engineCode, regu__EngineCode);

	if(engineCode != current.Code){
		int n = 0;

		// Get index of engine code
		while((Engine[n].Code != engineCode) && (n < ENGINE_LEN))
		{
			n++;
		}
		// Check if valid index
		if(n >= ENGINE_LEN){
			n = 0;
		}
		// Check if engine code from index matches engine code selected
		if(Engine[n].Code == engineCode)
		{
			atomic(engineIndex = n;);

			current = Engine[engineIndex];

			// Set default slope
			current.ScalefactorNom.BitToNorm.UadSlope = 1/Engine[engineIndex].ScalefactorNom.NormToBit.UadSlope;
			current.ScalefactorNom.BitToNorm.UadOffset = current.ScalefactorNom.BitToNorm.UadSlope*Engine[engineIndex].ScalefactorNom.NormToBit.UadOffset;
			current.ScalefactorNom.BitToNorm.IadSlope = 1/Engine[engineIndex].ScalefactorNom.NormToBit.IadSlope;
			current.ScalefactorNom.BitToNorm.IadOffset = current.ScalefactorNom.BitToNorm.IadSlope*current.ScalefactorNom.NormToBit.IadOffset;
			current.ScalefactorNom.BitToNorm.IpwmSlope = 1/Engine[engineIndex].ScalefactorNom.NormToBit.IpwmSlope;
			current.ScalefactorNom.BitToNorm.IpwmOffset = -current.ScalefactorNom.BitToNorm.IpwmSlope*Engine[engineIndex].ScalefactorNom.NormToBit.IpwmOffset;

			// update default index used by Menu
			reg_put(&engineIndex, regu__engineIdx_default);

			// Update supported battery voltages for MULTI mode
			engineSupportedBatteryVoltage();

			// Update engine type for later use
			engineUpdateEngineType();

			/* Use calibrated slope if available */
			{
				float Slope;

				/* Voltage scaling */
				reg_get(&Slope, regu__UadSlope);

				DISABLE;
				if(isnan(Slope))
				{
				}
				else
				{
					engineCalibrateUadslope(Slope);
				}
				ENABLE;

				/* Current scaling */
				reg_get(&Slope, regu__IadSlope);

				DISABLE;
				if(isnan(Slope))
				{
				}
				else
				{
					engineCalibrateIadslope(Slope);
				}
				ENABLE;
			}

			/* Use calibrated Current PWM if available */
			{
				float Slope;
				float Offset;

				/* Current PWM slope scaling */
				reg_get(&Slope, regu__IpwmSlope);

				DISABLE;
				if(isnan(Slope))
				{
				}
				else
				{
					engineCalibrateIpwmslope(Slope);
				}
				ENABLE;

				/* Current PWM offset scaling */
				reg_get(&Offset, regu__IpwmOffset);

				DISABLE;
				if(isnan(Offset))
				{
				}
				else
				{
					engineCalibrateIpwmoffset(Offset);
				}
				ENABLE;
			}

			/* Trigger for I offset calibration */
			{
				Int32 IoffsetCalib = 1;
				reg_put(&IoffsetCalib, regu__IoffsetCalib);
			}
		}
		else
		{
			engineCode = 0;
		}
		// Update current used engine code
		reg_put(&engineCode, regu__currEngineCode);
		current.Code = engineCode;
	}
}

PUBLIC const Engine_type *engineGetParameters()
{
	return &current;
}

PUBLIC Uint32 engineGetEngineType()
{
	return engineType;
}

int VoltageAboveEngineMax(const float voltage)
{
	const float voltageMax = engineVoltageMax();

	if (voltage > voltageMax)
	{
		return 1;
	}
	return 0;
}

float engineVoltageMax()
{
	const EngineUIchar_type* ui;
	int len;
	const Engine_type *engineParameters;

	engineParameters = engineGetParameters();

	len = engineParameters->UIcharLength;
	ui = engineParameters->UIchar_p;

	return ui[len - 1].Volt;
}

float engineCurrentMax(float voltage)
{
	const EngineUIchar_type* ui;
	int len;
	const Engine_type *engineParameters;

	engineParameters = engineGetParameters();

	len = engineParameters->UIcharLength;
	ui = engineParameters->UIchar_p;

	if(voltage <= ui[0].Volt)
	{
		return ui[0].Curr;
	}
	else
	{
		int n;
		for(n = 1; n < len; n++)
		{
			if(voltage <= ui[n].Volt)
			{
				float delta;
				delta = (voltage - ui[n-1].Volt)/(ui[n].Volt - ui[n-1].Volt);
				delta = ui[n-1].Curr + delta*(ui[n].Curr - ui[n-1].Curr);
				return delta;
			}
		}
	}
	return 0.0;
}

float enginePowerMax()
{
	const Engine_type* const engineParameters = engineGetParameters();
	const int len = engineParameters->UIcharLength;
	const EngineUIchar_type* ui = engineParameters->UIchar_p;
	float powerMax = 0.0f;
	int n;

	for(n = 0; n < len; n++)
	{
		const float power = ui[n].Volt*ui[n].Curr;

		if(power > powerMax){
			powerMax = power;
		}
	}

	return powerMax;
}

float engineNominalCurrent(){
	float nominalCurrent = 0.0f;
	int n;
	const EngineUIchar_type* ui;
	int len;

	{
		const Engine_type const *engineParameters = engineGetParameters();
		ui = engineParameters->UIchar_p;
		len = engineParameters->UIcharLength;
	}


	for(n = 0; n < len; n++){
		float tmp = ui[n].Curr;

		if(nominalCurrent < tmp){
			nominalCurrent = tmp;
		}
	}

	return nominalCurrent;
}

Uint32 engineGetCode(int index){
	return Engine[index].Code;
}

int engineGetLen(){
	return ENGINE_LEN;
}

void engineCalibrateUadslope(float gain){
	current.ScalefactorNom.BitToNorm.UadSlope = gain;
	current.ScalefactorNom.NormToBit.UadSlope = 1/gain;
	current.ScalefactorNom.BitToNorm.UadOffset = current.ScalefactorNom.NormToBit.UadOffset*current.ScalefactorNom.BitToNorm.UadSlope;
}

void engineCalibrateIadslope(float gain){
	current.ScalefactorNom.BitToNorm.IadSlope = gain;
	current.ScalefactorNom.NormToBit.IadSlope = 1/gain;
	current.ScalefactorNom.BitToNorm.IadOffset = current.ScalefactorNom.NormToBit.IadOffset*current.ScalefactorNom.BitToNorm.IadSlope;
}

void engineCalibrateIadoffset(float offset){
	current.ScalefactorNom.NormToBit.IadOffset = offset;
	current.ScalefactorNom.BitToNorm.IadOffset = current.ScalefactorNom.NormToBit.IadOffset*current.ScalefactorNom.BitToNorm.IadSlope;
}

void engineCalibrateIpwmslope(float gain){
	current.ScalefactorNom.BitToNorm.IpwmSlope = gain;
	current.ScalefactorNom.NormToBit.IpwmSlope = 1/gain;
	current.ScalefactorNom.BitToNorm.IpwmOffset = current.ScalefactorNom.NormToBit.IpwmOffset*current.ScalefactorNom.BitToNorm.IpwmSlope;
}

void engineCalibrateIpwmoffset(float offset){
	current.ScalefactorNom.NormToBit.IpwmOffset = offset;
	current.ScalefactorNom.BitToNorm.IpwmOffset = -current.ScalefactorNom.NormToBit.IpwmOffset*current.ScalefactorNom.BitToNorm.IpwmSlope;
}

uint32_t engineIadslopeCompensation(uint32_t Ipwm){
	float IadSlopeCurrent;
	float IadSlopeDefault;
	float IpwmCompensated;

	IadSlopeCurrent = current.ScalefactorNom.BitToNorm.IadSlope;
	IadSlopeDefault = 1/Engine[engineIndex].ScalefactorNom.NormToBit.IadSlope;
	IpwmCompensated = Ipwm*IadSlopeCurrent/IadSlopeDefault;

	return (uint32_t)IpwmCompensated;
}

void engineSupportedBatteryVoltage(void){
	chalg_BattInfoMultiType		battInfo6;
	chalg_BattInfoMultiType		battInfo12;
	chalg_BattInfoMultiType		battInfo18;
	chalg_BattInfoMultiType		battInfo24;
	chalg_BattInfoMultiType		battInfo40;
	float nominalVoltageMin;
	float nominalVoltageMax;
	Uint8 BitConfig;

	// Read battery info from flash
	reg_get(&battInfo6, regu__batteryInfo6);
	reg_get(&battInfo12, regu__batteryInfo12);
	reg_get(&battInfo18, regu__batteryInfo18);
	reg_get(&battInfo24, regu__batteryInfo24);
	reg_get(&battInfo40, regu__batteryInfo40);

	// Set default to not supported
	battInfo6.suported = 0;
	battInfo12.suported = 0;
	battInfo18.suported = 0;
	battInfo24.suported = 0;
	battInfo40.suported = 0;

	reg_get(&BitConfig, regu__BitConfig);
	if(BitConfig & CHALG_BITCFG_CEC)
	{
		int n;

		for(n = 0; n < CEC_ENGINE_LEN; n++)
		{
			if(CECRestrictions[n].type == current.Code)
			{
				if(CECRestrictions[n].maxAh6c > 0)
				{
					battInfo6.suported = 1;
				}
				if(CECRestrictions[n].maxAh12c > 0)
				{
					battInfo12.suported = 1;
				}
				if(CECRestrictions[n].maxAh18c > 0)
				{
					battInfo18.suported = 1;
				}
				if(CECRestrictions[n].maxAh24c > 0)
				{
					battInfo24.suported = 1;
				}
				if(CECRestrictions[n].maxAh40c > 0)
				{
					battInfo40.suported = 1;
				}
			}
		}
	}
	else
	{
		if(engineType == EngineType_PF_HF1)
		{
			// Get nominal min and max voltages from current engine
			nominalVoltageMin = current.UIchar_p[0].Volt;
			nominalVoltageMax = current.UIchar_p[6].Volt;
		}
		else if(engineType == EngineType_PF_HF2)
		{
			// Get nominal min and max voltages from current engine
			nominalVoltageMin = current.UIchar_p[0].Volt;
			nominalVoltageMax = current.UIchar_p[2].Volt;
		}
		else
		{
			// Get nominal min and max voltages from current engine
			nominalVoltageMin = current.UIchar_p[4].Volt;
			nominalVoltageMax = current.UIchar_p[5].Volt;
		}

		// 6 cell battery
		if(nominalVoltageMin < 1.9*6 && nominalVoltageMax > 2.2*6){
			battInfo6.suported = 1;
		}
		// 12 cell battery
		if(nominalVoltageMin < 1.9*12 && nominalVoltageMax > 2.2*12){
			battInfo12.suported = 1;
		}
		// 18 cell battery
		if(nominalVoltageMin < 1.9*18 && nominalVoltageMax > 2.2*18){
			battInfo18.suported = 1;
		}
		// 24 cell battery
		if(nominalVoltageMin < 1.9*24 && nominalVoltageMax > 2.2*24){
			battInfo24.suported = 1;
		}
		// 40 cell battery
		if(nominalVoltageMin < 1.9*40 && nominalVoltageMax > 2.2*40){
			battInfo40.suported = 1;
		}
	}

	// Write updated battery info back to flash
	reg_put(&battInfo6, regu__batteryInfo6);
	reg_put(&battInfo12, regu__batteryInfo12);
	reg_put(&battInfo18, regu__batteryInfo18);
	reg_put(&battInfo24, regu__batteryInfo24);
	reg_put(&battInfo40, regu__batteryInfo40);
}

PUBLIC int engineValidateParamsCEC(
	chalg_BattInfoType		battInfo
) {
	int retVal = BATTERY_NO_ERROR;
	int n;

	for(n = 0; n < CEC_ENGINE_LEN; n++)
	{
		if(CECRestrictions[n].type == current.Code)
		{
			if(battInfo.cell == 6)
			{
				if(CECRestrictions[n].minAh6c > 0)
				{
					if(battInfo.capacity < CECRestrictions[n].minAh6c ||
					   battInfo.capacity > CECRestrictions[n].maxAh6c
					){
						retVal = BATTERY_INVALID_CAPACITY;
					}
				}
				else
				{
					retVal = BATTERY_INVALID_CELLS;
				}
				break;
			}
			else if(battInfo.cell == 12)
			{
				if(CECRestrictions[n].minAh12c > 0)
				{
					if(battInfo.capacity < CECRestrictions[n].minAh12c ||
					   battInfo.capacity > CECRestrictions[n].maxAh12c
					){
						retVal = BATTERY_INVALID_CAPACITY;
					}
				}
				else
				{
					retVal = BATTERY_INVALID_CELLS;
				}
				break;
			}
			else if(battInfo.cell == 18)
			{
				if(CECRestrictions[n].minAh18c > 0)
				{
					if(battInfo.capacity < CECRestrictions[n].minAh18c ||
					   battInfo.capacity > CECRestrictions[n].maxAh18c
					){
						retVal = BATTERY_INVALID_CAPACITY;
					}
				}
				else
				{
					retVal = BATTERY_INVALID_CELLS;
				}
				break;
			}
			else if(battInfo.cell == 24)
			{
				if(CECRestrictions[n].minAh24c > 0)
				{
					if(battInfo.capacity < CECRestrictions[n].minAh24c ||
					   battInfo.capacity > CECRestrictions[n].maxAh24c
					){
						retVal = BATTERY_INVALID_CAPACITY;
					}
				}
				else
				{
					retVal = BATTERY_INVALID_CELLS;
				}
				break;
			}
			else if(battInfo.cell == 40)
			{
				if(CECRestrictions[n].minAh40c > 0)
				{
					if(battInfo.capacity < CECRestrictions[n].minAh40c ||
					   battInfo.capacity > CECRestrictions[n].maxAh40c
					){
						retVal = BATTERY_INVALID_CAPACITY;
					}
				}
				else
				{
					retVal = BATTERY_INVALID_CELLS;
				}
				break;
			}
			else
			{
				retVal = BATTERY_INVALID_CELLS;
				break;
			}
		}
	}
	return(retVal);
}

static void engineUpdateEngineType(void){
	/*
	 * Update engine type depending on engine code.
	 */
	if(current.Code <= 6 || (current.Code >= 200 && current.Code <= 299))
	{
		engineType = EngineType_PF_HF2;
	}
	else if((current.Code >= 11 && current.Code <= 13) || current.Code == 114)
	{
		engineType = EngineType_PF_HF1;
	}
	else
	{
		engineType = EngineType_NA;
	}
}
