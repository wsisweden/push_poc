#ifndef ENGINE_PF_HF2_36_22_230V_H_
#define ENGINE_PF_HF2_36_22_230V_H_


#include "engines_types.h"
#include "engine_PF-HF2_standard_parameters.h"

PRIVATE EngineUIchar_type const_P PF_HF2_36_22_230V__UIchar[] = ENGINE_PF_HF2_36_22_230V_UI_CHARACTERISTICS(18, 22);

#define ENGINE_PF_HF2_36_22_230V \
		/*	    *********************** \
			    * 36V 22A 230VAC * \
			    ***********************/ \
				{ \
				2,													/* Code */ \
				ENGINE_PF_HF2_STANDARD_RI,							/* Ri */ \
				ENGINE_PF_HF2_STANDARD_EFFICIENCY,					/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_PF_HF2_STANDARD_EFFICIENCY_APPARENT_230V,	/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				PF_HF2_36_22_230V__UIchar, 5, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					0.1,		/* KpU 		\	*/ \
					0.01,		/* KIU 		 }	*/ \
					10.0,		/* KchooseU /																	*/ \
					0.1 ,		/* KpI 		\	*/ \
					0.01,		/* KII 		 }	*/ \
					1.0,		/* KchooseI /																	*/ \
					0.06,		/* KpP 		\	*/ \
					0.006,		/* KIP 		 }	*/ \
					1.0,		/* KchooseP /																	*/ \
					100.00,		/* KpT */ \
					0.01*0.3,	/* KIT */ \
					400.0		/* KchooseT */ \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ /* Checked that scaling constants seems to be correctly entered */ \
							75.98,				/* UadSlope measured by Jorma */ \
							0,					/* UadOffset measured by Jorma */ \
							153.18,				/* IadSlope measured by Jorma */ \
							-459.54,			/* IadOffset measured by Jorma */ \
							718.20,				/* IpwmSlope measured by Jorma */ \
							2154.6,				/* IpwmOffset measured by Jorma */ \
						}, \
					    /* BitToNorm */ \
						{ \
							0.013160896,		/* UadSlope measured by Jorma */ \
							0.0,				/* UadOffset measured by Jorma */ \
							0.006528287,		/* IadSlope measured by Jorma */ \
							3.0,				/* IadOffset measured by Jorma */ \
							0.001392361,		/* IpwmSlope measured by Jorma */ \
							-3.0,				/* IpwmOffset measured by Jorma */ \
						} \
				 }, \
\
				/* Theatsink */ \
				&Engine_OthersHeatsink2, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_No, \
				ENGINE_PF_HF2_STD_BIT_CURRENT(22.0) \
			} \

#endif /* ENGINE_PF_HF2_36_22_230V_H_ */
