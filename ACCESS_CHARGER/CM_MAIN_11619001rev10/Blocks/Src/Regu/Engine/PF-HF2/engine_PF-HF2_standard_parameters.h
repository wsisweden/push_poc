#ifndef ENGINE_PF_HF2_STANDARD_PARAMETERS_H
#define ENGINE_PF_HF2_STANDARD_PARAMETERS_H

#include "engines_types.h"
#include "math.h"

/* Engine output curve */
//ENGINE_PF_HF2_STANDARD_UI_CHARACTERISTICS

#define ENGINE_PF_HF2_24_40_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage				Current */ \
	{0.0,					1.00*nominalCurrent},	/* 0V		40A		All currents should be multiplied with nominal current */ \
	{2.25*nominalCells,		1.00*nominalCurrent},	/* 27V		40A		*/ \
	{3.00*nominalCells,		0.75*nominalCurrent},	/* 36V		30A 	*/ \
	{3.00*nominalCells,		0.75*nominalCurrent},	/* 36V		30A 	*/ \
	{3.00*nominalCells,		0.00*nominalCurrent}	/* 36V		0A 		*/ \
};

#define ENGINE_PF_HF2_24_80_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage				Current */ \
	{0.0,					1.00*nominalCurrent},		/* 0V		80A		All currents should be multiplied with nominal current */ \
	{2.40*nominalCells,		1.00*nominalCurrent},		/* 28.8V	80A		*/ \
	{3.00*nominalCells,		0.77*nominalCurrent + 0.4},	/* 36V		62A		*/ \
	{3.00*nominalCells,		0.77*nominalCurrent + 0.4},	/* 36V		62A		*/ \
	{3.00*nominalCells,		0.00*nominalCurrent}		/* 36V		0A		*/ \
};

#define ENGINE_PF_HF2_24_105_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage				Current */ \
	{0.0,					1.00*nominalCurrent},	/* 0V		105A	All currents should be multiplied with nominal current */ \
	{2.40*nominalCells,		1.00*nominalCurrent},	/* 28.8V	105A	*/ \
	{3.00*nominalCells,		0.80*nominalCurrent},	/* 36V		84A		*/ \
	{3.00*nominalCells,		0.00*nominalCurrent},	/* 36V		0A		*/ \
	{3.00*nominalCells,		0.00*nominalCurrent}	/* 36V		0A		*/ \
};

#define ENGINE_PF_HF2_36_22_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,						1.00*nominalCurrent},		/* 0V		22A		All currents should be multiplied with nominal current */ \
	{0.0,						1.00*nominalCurrent},		/* 0V		22A		*/ \
	{2.72*nominalCells + 0.04,	1.00*nominalCurrent},		/* 49V		22A		*/ \
	{3.00*nominalCells,			0.90*nominalCurrent + 0.2},	/* 54V		20A		*/ \
	{3.00*nominalCells,			0.00*nominalCurrent}		/* 54V		0A		*/ \
};

#define ENGINE_PF_HF2_36_53_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage				Current */ \
	{0.0,					1.00*nominalCurrent},			/* 0V		53A		All currents should be multiplied with nominal current */ \
	{0.0,					1.00*nominalCurrent},			/* 0V		53A		*/ \
	{2.40*nominalCells,		1.00*nominalCurrent},			/* 43.2V	53A		*/ \
	{3.00*nominalCells,		0.79*nominalCurrent + 0.13},	/* 54V		42A		*/ \
	{3.00*nominalCells,		0.00*nominalCurrent}			/* 54V		0A		*/ \
};

#define ENGINE_PF_HF2_48_20_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage				Current */ \
	{0.0,					1.00*nominalCurrent},	/* 0V		20A		All currents should be multiplied with nominal current */ \
	{0.0,					1.00*nominalCurrent},	/* 0V		20A		*/ \
	{2.25*nominalCells,		1.00*nominalCurrent},	/* 54V		20A		*/ \
	{3.00*nominalCells,		0.75*nominalCurrent},	/* 72V		15A		*/ \
	{3.00*nominalCells,		0.00*nominalCurrent}	/* 72V		0A		*/ \
};

#define ENGINE_PF_HF2_48_40_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage				Current */ \
	{0.0,					1.00*nominalCurrent},		/* 0V		40A		All currents should be multiplied with nominal current */ \
	{0.0,					1.00*nominalCurrent},		/* 0V		40A		*/ \
	{2.40*nominalCells,		1.00*nominalCurrent},		/* 57.6V	40A		*/ \
	{3.00*nominalCells,		0.77*nominalCurrent + 0.2},	/* 72V		31A		*/ \
	{3.00*nominalCells,		0.00*nominalCurrent}		/* 72V		0A		*/ \
};

#define ENGINE_PF_HF2_48_60_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,						1.00*nominalCurrent},	/* 0V		60A		All currents should be multiplied with nominal current */ \
	{2.08*nominalCells + 0.08,	1.00*nominalCurrent},	/* 50V		60A		*/ \
	{3.00*nominalCells,			0.70*nominalCurrent},	/* 72V		42A		*/ \
	{3.00*nominalCells,			0.00*nominalCurrent},	/* 72V		0A		*/ \
	{3.00*nominalCells,			0.00*nominalCurrent}	/* 72V		0A		*/ \
};

#define ENGINE_PF_HF2_96_30_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,						1.00*nominalCurrent},	/* 0V		30A		All currents should be multiplied with nominal current */ \
	{0.0,						1.00*nominalCurrent},	/* 0V		30A		*/ \
	{2.08*nominalCells + 0.16,	1.00*nominalCurrent},	/* 100V		30A		*/ \
	{3.0*nominalCells,			0.70*nominalCurrent},	/* 144V		21A		*/ \
	{3.0*nominalCells,			0.00*nominalCurrent}	/* 144V		0A		*/ \
};

#define ENGINE_PF_HF2_280_10_230V_UI_CHARACTERISTICS(nominalCells, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,						1.00*nominalCurrent},	/* 0V		10A		All currents should be multiplied with nominal current */ \
	{2.14*nominalCells + 0.4,	1.00*nominalCurrent},	/* 300V		10A		*/ \
	{2.68*nominalCells + 0.4,	1.00*nominalCurrent},	/* 375V		8.0A	*/ \
	{3.70*nominalCells + 2.0,	0.57*nominalCurrent},	/* 520V		5.7A	*/ \
	{3.70*nominalCells + 2.0,	0.00*nominalCurrent}	/* 520V		0A		*/ \
};

/* Ri */
#define ENGINE_PF_HF2_STANDARD_RI (0.0/1000)

/* Conversion between input power and output power */
#define ENGINE_PF_HF2_STANDARD_EFFICIENCY (0.93)

/* Conversion between phase current and output power */
#define ENGINE_PF_HF2_STANDARD_EFFICIENCY_APPARENT_230V (240.0*0.93*0.95)

/* Accuracy voltage */
/* Accuracy current */
extern const EngineAccuracy_type Engine_NoAccuracy[2];

/* Controller */

/* Voltage measurement standard scaling factors */

/* Current measurement standard scaling factors */

/* Pwm output standard scaling factors */

/* Temperature board */

/* Temperature heat sink */
extern const Temperature_type Engine_OthersHeatsink2;
//extern const Temperature_type Engine_OthersHeatsink3;

/* Temperature transformer */
extern const TempSenseDigital_enum Engine_PF_HF2_StdTempTrafo;

/* Battery signal logic level */
#define ENGINE_PF_HF2_STD_BIT_CURRENT(nominalCurrent) {0.1875*nominalCurrent, nominalCurrent, 500}
/* old values: {7.5, 30.0} -- battery signal logic levels (5.0, 15.0) and (10.0, 25.0) tested with 22nF capacitor on input and looks good */

#endif
