#ifndef ENGINE_PF_HF2_48_20_230V_H_
#define ENGINE_PF_HF2_48_20_230V_H_


#include "engines_types.h"
#include "engine_PF-HF2_standard_parameters.h"

PRIVATE EngineUIchar_type const_P PF_HF2_48_20_230V__UIchar[] = ENGINE_PF_HF2_48_20_230V_UI_CHARACTERISTICS(24, 20);

#define ENGINE_PF_HF2_48_20_230V \
		/*	    *********************** \
			    * 48V 20A 230VAC * \
			    ***********************/ \
				{ \
				3,													/* Code */ \
				ENGINE_PF_HF2_STANDARD_RI,							/* Ri */ \
				ENGINE_PF_HF2_STANDARD_EFFICIENCY,					/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_PF_HF2_STANDARD_EFFICIENCY_APPARENT_230V,	/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				PF_HF2_48_20_230V__UIchar, 5, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					0.1,		/* KpU 		\	*/ \
					0.01,		/* KIU 		 }	*/ \
					10.0,		/* KchooseU /																	*/ \
					0.1 ,		/* KpI 		\	*/ \
					0.01,		/* KII 		 }	*/ \
					1.0,		/* KchooseI /																	*/ \
					0.06,		/* KpP 		\	*/ \
					0.006,		/* KIP 		 }	*/ \
					1.0,		/* KchooseP /																	*/ \
					100.00,		/* KpT */ \
					0.01*0.3,	/* KIT */ \
					400.0		/* KchooseT */ \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ /* Checked that scaling constants seems to be correctly entered */ \
							54.26,				/* UadSlope, 1/UadSlope(BitToNorm) */ \
							0.0,				/* UadOffset, 0V */ \
							158.25,				/* IadSlope 1/IadSlope(BitToNorm) */ \
							-345.40,			/* IadOffset, IadSlope*IadOffset(BitToNorm) */ \
							742.77,			/* IpwmSlope, 1/IpwmSlope(BitToNorm) */ \
							1611.73,			/* IpwmOffset, IpwmSlope*IpwmOffset(BitToNorm)m */ \
						}, \
						/* BitToNorm */ \
						{ \
							0.018429473,		/* UadSlope, Measure average in production */ \
							0.0,				/* UadOffset, Considered as zero */ \
							0.006318945,		/* IadSlope,  Measure average in production */ \
							2.18,				/* IadOffset,  Measure average in production */ \
							0.001346304,		/* IpwmSlope,  Measure average i production */ \
							-2.17,				/* IpwmOffset,  Measure average i production */ \
						} \
				 }, \
\
				/* Theatsink */ \
				&Engine_OthersHeatsink2, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_No, \
				ENGINE_PF_HF2_STD_BIT_CURRENT(20.0) \
			} \

#endif /* ENGINE_PF_HF2_48_20_230V_H_ */
