#ifndef ENGINE_PF_HF2_24_105_230V_H_
#define ENGINE_PF_HF2_24_105_230V_H_


#include "engines_types.h"
#include "engine_PF-HF2_standard_parameters.h"

PRIVATE EngineUIchar_type const_P PF_HF2_24_105_230V__UIchar[] = ENGINE_PF_HF2_24_105_230V_UI_CHARACTERISTICS(12, 105);

#define ENGINE_PF_HF2_24_105_230V \
		/*	    *********************** \
			    * 24V 105A 230VAC * \
			    ***********************/ \
				{ \
				6,													/* Code */ \
				ENGINE_PF_HF2_STANDARD_RI,							/* Ri */ \
				ENGINE_PF_HF2_STANDARD_EFFICIENCY,					/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_PF_HF2_STANDARD_EFFICIENCY_APPARENT_230V,	/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				PF_HF2_24_105_230V__UIchar, 5, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					0.1,		/* KpU 		\	*/ \
					0.01,		/* KIU 		 }	*/ \
					10.0,		/* KchooseU /																	*/ \
					0.1 ,		/* KpI 		\	*/ \
					0.01,		/* KII 		 }	*/ \
					1.0,		/* KchooseI /																	*/ \
					0.06,		/* KpP 		\	*/ \
					0.006,		/* KIP 		 }	*/ \
					1.0,		/* KchooseP /																	*/ \
					100.00,		/* KpT */ \
					0.01*0.3,	/* KIT */ \
					400.0		/* KchooseT */ \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ /* Checked that scaling constants seems to be correctly entered */ \
							109.07,				/* UadSlope, 1/UadSlope(BitToNorm) */ \
							0,					/* UadOffset, 0V */ \
							31.77,				/* IadSlope 1/IadSlope(BitToNorm) */ \
							-133.95,			/* IadOffset, IadSlope*IadOffset(BitToNorm) */ \
							149.15,				/* IpwmSlope, 1/IpwmSlope(BitToNorm) */ \
							620.27,				/* IpwmOffset, IpwmSlope*IpwmOffset(BitToNorm)m */ \
						}, \
						/* BitToNorm */ \
						{ \
							0.009168638,		/* UadSlope, Measure average in production */ \
							0.0,				/* UadOffset, Considered as zero */ \
							0.031472836,		/* IadSlope,  Measure average in production */ \
							4.22,				/* IadOffset,  Measure average in production */ \
							0.006704638,		/* IpwmSlope,  Measure average i production */ \
							-4.16,				/* IpwmOffset,  Measure average i production */ \
						} \
				 }, \
\
				/* Theatsink */ \
				&Engine_OthersHeatsink2, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_No, \
				ENGINE_PF_HF2_STD_BIT_CURRENT(105.0) \
			} \

#endif /* ENGINE_PF_HF2_24_105_230V_H_ */
