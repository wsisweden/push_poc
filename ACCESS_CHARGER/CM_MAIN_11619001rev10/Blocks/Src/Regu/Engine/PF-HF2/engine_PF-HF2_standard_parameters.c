#include "engine_PF-HF2_standard_parameters.h"

#include "math.h"

/* Accuracy voltage */
/* Accuracy current */

/* Ri */

/* Conversion between input power and output power */

/* Conversion between phase current and output power */

/* Controller */

/* Voltage measurement standard scaling factors */

/* Current measurement standard scaling factors */

/* Temperature heat sink						 Type					A +				B*(R_PTC/R) +		C*(R_PTC/R)*(R_PTC/R) +	unused	Short circuit	Open circuit	Derate		Over temperature */
const Temperature_type Engine_OthersHeatsink2 = {TempSensor_Robust_Int,	0.0,			0.0,				0.0,					NAN,	0.5,			3.5,			92,			97}; /* KTY 83/120, R = 1.5kOhm */
//const Temperature_type Engine_OthersHeatsink3 = {TempSensor_Robust_Int,	0.0,			0.0,				0.0,					NAN,	0.5,			3.5,			80,			85}; /* KTY 83/120, R = 1.5kOhm */

/* Temperature transformer */
const TempSenseDigital_enum Engine_PF_HF2_StdTempTrafo = TempSensorDigital_No;
