#ifndef ENGINE_36_220_480V_13KW_H_
#define ENGINE_36_220_480V_13KW_H_

#include "engines_types.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_36_220_480V_13KW__UIchar[] = ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(12, 18, 220);

#define ENGINE_36_220_480V_13KW \
		/*	    *********************** \
			    * MP-HF1 36V 220A 480VAC * \
			    ***********************/ \
				{ \
				122,												/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_480V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_36_220_480V_13KW__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_220A13kW*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_220A13kW*ENGINE_MP_HF1_STD_220A13kW), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_220A13kW*ENGINE_MP_HF1_STD_220A13kW*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_220A13kW) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ \
							ENGINE_MP_HF1_STD_48V, 0.0, \
							ENGINE_MP_HF1_STD_220A13kW, ENGINE_MP_HF1_STD_13kW_OFFSET, \
							ENGINE_MP_HF1_STD_PWM_220A13kW, ENGINE_MP_HF1_STD_PWM_13KW_OFFSET \
						}, \
					    /* BitToNorm */ \
						{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
\
				/* Theatsink */ \
				&Engine_MP_HF1_27kTempHeatsink, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT(220.0) \
			} \

#endif /* ENGINE_36_220_480V_13KW_H_ */
