#ifndef ENGINE_120_180_400V_H_
#define ENGINE_120_180_400V_H_


#include "engines_types.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_120_180_400V__UIchar[] = ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(12, 60, 180);

#define ENGINE_120_180_400V \
		/*	    *********************** \
			    * Mtm 120V 180A 400VAC * \
			    ***********************/ \
				{ \
				130,											/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_400V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_120_180_400V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_90A13kW_DOUBLE*ENGINE_MP_HF1_STD_120V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_90A13kW_DOUBLE*ENGINE_MP_HF1_STD_90A13kW_DOUBLE), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_90A13kW_DOUBLE*ENGINE_MP_HF1_STD_90A13kW_DOUBLE*ENGINE_MP_HF1_STD_120V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_90A13kW_DOUBLE) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ \
							ENGINE_MP_HF1_STD_120V, 0.0, \
							ENGINE_MP_HF1_STD_90A13kW_DOUBLE, ENGINE_MP_HF1_STD_13kW_OFFSET, \
							ENGINE_MP_HF1_STD_PWM_90A13kW_DOUBLE, ENGINE_MP_HF1_STD_PWM_13KW_OFFSET \
						}, \
					    /* BitToNorm */ \
						{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
\
				/* Theatsink */ \
				&Engine_MP_HF1_27kTempHeatsink_T60_67, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT(180.0) \
			} \

#endif /* ENGINE_120_180_400V_H_ */
