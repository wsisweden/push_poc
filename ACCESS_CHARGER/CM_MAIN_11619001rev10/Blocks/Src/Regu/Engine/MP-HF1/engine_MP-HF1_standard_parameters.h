#ifndef ENGINE_MP_HF1_STANDARD_PARAMETERS_H
#define ENGINE_MP_HF1_STANDARD_PARAMETERS_H

#include "engines_types.h"
#include "math.h"

#define ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(nominalCellsMin, nominalCellsMax, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,							0.10*nominalCurrent},	/* Voltage absolute				All currents should be multiplied with nominal current */ \
	{6.0,							0.10*nominalCurrent},	/* Voltage absolute */ \
	{6.0,							0.10*nominalCurrent},	/* Voltage absolute */ \
	{1.80*nominalCellsMin + 0.2,	0.10*nominalCurrent},	/* (1.8V/cell + 0.2V) */ \
	{1.80*nominalCellsMin + 0.2,	1.00*nominalCurrent},	/* (1.8V/Cell + 0.2V) */ \
	{2.40*nominalCellsMax + 0.9,	1.00*nominalCurrent},	/* (2.4V/cell + 0.8V) */ \
	{2.60*nominalCellsMax + 0.6,	0.92*nominalCurrent},	/* (2.6V/cell + 0.6V) */ \
	{2.82*nominalCellsMax + 0.4,	0.25*nominalCurrent},	/* (2.82V/cell + 0.4V) */ \
	{2.82*nominalCellsMax + 0.4,	0.00*nominalCurrent}	/* (2.82V/cell + 0.4V) */ \
};

#define ENGINE_MP_HF1_220V_UI_CHARACTERISTICS(nominalCellsMin, nominalCellsMax, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,							0.10*nominalCurrent},	/* Voltage absolute				All currents should be multiplied with nominal current */ \
	{6.0,							0.10*nominalCurrent},	/* Voltage absolute */ \
	{6.0,							0.10*nominalCurrent},	/* Voltage absolute */ \
	{1.80*nominalCellsMin + 0.2,	0.10*nominalCurrent},	/* (1.8V/cell + 0.2V) */ \
	{1.80*nominalCellsMin + 0.2,	1.00*nominalCurrent},	/* (1.8V/Cell + 0.2V) */ \
	{2.25*nominalCellsMax + 0.9,	1.00*nominalCurrent},	/* (2.4V/cell + 0.8V) */ \
	{2.60*nominalCellsMax + 0.6,	0.87*nominalCurrent},	/* (2.6V/cell + 0.6V) */ \
	{2.82*nominalCellsMax + 0.4,	0.25*nominalCurrent},	/* (2.82V/cell + 0.4V) */ \
	{2.82*nominalCellsMax + 0.4,	0.00*nominalCurrent}	/* (2.82V/cell + 0.4V) */ \
};

/* Ri */
#define ENGINE_MP_HF1_STANDARD_RI (0.0/1000)

/* Conversion between input power and output power */
#define ENGINE_MP_HF1_STANDARD_EFFICIENCY (0.93)

/* Conversion between phase current and output power */
#define ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_400V (1.7320508*400.0*0.93*0.95)
#define ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_440V (1.7320508*440.0*0.93*0.95)
#define ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_480V (1.7320508*480.0*0.93*0.95)
#define ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_220V (1.7320508*220.0*0.93*0.95)
#define ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_600V (1.7320508*600.0*0.93*0.95)

/* Accuracy voltage */
/* Accuracy current */
extern const EngineAccuracy_type Engine_NoAccuracy[2];

/* Controller
 * A block diagram could be found in the folder Regu.
 * Constants:
 *   R(s)			Reference signal
 *   F(s)			PI-regulator
 *   G(s)			System, input PWM signal output several values that should be limited
 *   Kpwm			Conversion constant from PWM signal in (bit) to current in (A)
 *   Kmeas			Conversion constant from value that should be limited in (unit) to (bit)
 * Calculations:
 *   Y(s) = PI(s)*Kpwm*G(s)(R(s) - Y(s)*Kmeas) <=> Y(s)(1 + PI(s)*Kpwm*G(s)*Kmeas) = PI(s)*Kpwm*G(s)R(s)
 *   Y(s)/R(S) =  PI(s)*Kpwm*G(s)/(1 + PI(s)*Kpwm*G(s)*Kmeas)
 *   Kpwm and Kmeas change for different chargers and if the dynamic response is the same it should be possible to adjust the regulator so that
 *   the same close loop dynamic response is achieved.
 *   Y(s)/R(S) =  PI(s)*(Kpwm*Kmeas)/(Kpwm*Kmeas)*Kpwm*G(s)/(1 + PI(s)*(Kpwm*Kmeas)/(Kpwm*Kmeas)*Kpwm*G(s)*Kmeas), PIsubstitute(s) = PI(s)*(Kpwm*Kmeas)
 *   Y(s)/R(S) =  PI_substitute(s)/Kmeas*G(s)/(1 + PI_substitue(s)*G(s)), PIsubstitute(s) = PI(s)*(Kpwm*Kmeas)
 *   This equation with PI_substitute(s) is what we really want so PI(s) = PIsubstitute(s)/(Kpwm*Kmeas)
 */
#define ENGINE_MP_HF1_STD_REGULATOR_U(k) 0.050*107.114*53.222/(k) /* KpU */,	0.01*4.0*107.114*53.222/(k) /* KiU */,	2.000 /* KchooseI */
#define ENGINE_MP_HF1_STD_REGULATOR_I(k) 0.050*107.114*19.209/(k) /* KpI */,	0.01*5.0*107.114*19.209/(k) /* KiI */,	1.000 /* KchooseI */
#define ENGINE_MP_HF1_STD_REGULATOR_P(k) 0.080*107.114*1022.3/(k) /* KpP */,	0.01*8.0*107.114*1022.3/(k) /* KiP */,	1.000 /* KchooseI */
#define ENGINE_MP_HF1_STD_REGULATOR_T(k) 100.0*107.114*1.0000/(k) /* KpT */,	0.01*0.3*107.114*1.0000/(k) /* KiT */,	400.0 /* KchooseI */

/* Voltage measurement standard scaling factors
 * Constants:
 *   Umeas		Open circuit voltage in cable between charger and display in (V)
 *   U_R		Output resistance in (Ω)
 *   U_ADC		Voltage at ADC in (V)
 *   U_slope	Scaling constants in (bit/V)
 * Calculations:
 *   Umeas = Ubatt*4.7kΩ/(4.7kΩ + 56kΩ + 12 kΩ) = Ubatt*4.7kΩ/72.7kΩ, for 48 volt
 *   U_R = Umeas/(Ubatt/(12kΩ + 56kΩ)) = 4.7kΩ/72.7kΩ*68kΩ
 *   U_ADC = Umeas*8.2kΩ/(8.2kΩ + 1kΩ + U_R) = Umeas*8.2kΩ/(9.2kΩ + 4.7kΩ/72.7kΩ*68kΩ)
 *   ADC = U_ADC/3.0*4095 = Ubatt*4.7kΩ/72.7kΩ*8.2kΩ/(9.2kΩ + 4.7kΩ/72.7kΩ*68kΩ)/3.0*4095
 *   U_slope = ADC/Ubatt = 4.7kΩ/72.7kΩ*8.2kΩ/(9.2kΩ + 4.7kΩ/72.7kΩ*68kΩ)/3.0*4095
 */

/* Nominal values are used */
#define ENGINE_MP_HF1_STD_24V 109.41
#define ENGINE_MP_HF1_STD_36V 70.056
#define ENGINE_MP_HF1_STD_48V 53.076
#define ENGINE_MP_HF1_STD_80V 31.556
#define ENGINE_MP_HF1_STD_120V 21.244	//(80/120 0f 31,866)

/* Current measurement standard scaling factors
 * Offset:
 * Constants:
 *   Ui_op_amp_open		Voltage at a point in the bias network for the positive input of the current measurement OP-amp
 *   Ui_op_amp_short	Short circuit current at a point in the bias network for the positive input of the current measurement OP-amp
 *   Ui_op_amp_R		Resistance for the Thevenin equivalent for the positive input of the current measurement OP-amp
 *   Ui_op_amp			Voltage at the positive input of the current measurement OP-AMP
 *   Ui_offset			Offset voltage at output of current measurement OP-amp
 * Calculations:
 *   Ui_op_amp_open = 5V*2.7kΩ/(2.7kΩ + 39kΩ) = 5V*2.7kΩ/41.7kΩ
 *   Ui_op_amp_short = 5V/39kΩ
 *   Ui_op_amp_R = Ui_op_amp_open/Ui_op_amp_short + 68kΩ
 *   Ui_op_amp = 820Ω/(820Ω + Ui_op_amp_R)*Ui_op_amp_open
 *   Ui_op_amp = Ui_op_amp*68e3/820 = 0.30856V
 *
 * Slope:
 * Constants:
 *   Ui_meas			Open circuit current measurement voltage in (V)
 *   Ui_meas_R			Imeas resistance in (Ω)
 *   U_ADC				Voltage at ADC in (V)
 *   ADC				ADC value in (bit)
 * Calculations:
 *   Ui_meas = Ui_offset + R_shunt*i*68kΩ/820Ω
 *   Ui_meas_R = 1.5kΩ
 *   U_ADC = Ui_meas*5.6kΩ/(5.6kΩ + 1.5kΩ + 3.9kΩ) = Ui_meas*5.6kΩ/11kΩ
 *   ADC = U_ADC/3.0*4095
 *   Slope = ADC/i = Ui_meas*5.6kΩ/11kΩ/3.0*4095/i = R_shunt*i*68kΩ/820Ω*5.6kΩ/11kΩ/3.0*4095/i = R_shunt*68kΩ/820Ω*5.6kΩ/11kΩ/3.0*4095
 *
 * Parallel board:
 * Constants:
 *   Ui_meas			Open circuit current measurement voltage in (V)
 *   Ui_meas_R			Imeas resistance in (Ω)
 *   U_ADC				Voltage at ADC in (V)
 *   ADC				ADC value in (bit)
 * Calculations:
 *   Ui_meas = (Ui_offset + R_shunt*i*68kΩ/820Ω)/2
 *   Ui_meas_R = 1kΩ
 *   U_ADC = Ui_meas*5.6kΩ/(5.6kΩ + 1kΩ + 3.9kΩ) = Ui_meas/2*5.6kΩ/10.5kΩ
 *   ADC = U_ADC/3.0*4095
 *   Slope = ADC/i = Ui_meas/2*5.6kΩ/10.5kΩ/3.0*4095/i = R_shunt*i/2*68kΩ/820Ω*5.6kΩ/10.5kΩ/3.0*4095/i = R_shunt/2*68kΩ/820Ω*5.6kΩ/10.5kΩ/3.0*4095
 */

/* Nominal values are used for the constants although the shunts probably have a bias because material may be removed to adjust resistance. */
#define ENGINE_MP_HF1_STD_75A50mV 39.044				/* in (bit/A) measured average, calculated 38.418 */
#define ENGINE_MP_HF1_STD_100A50mV 27.933				/* in (bit/A) measured average, calculated 28.813 */
#define ENGINE_MP_HF1_STD_125A50mV 24.011				/* in (bit/A) */
#define ENGINE_MP_HF1_STD_150A50mV 18.901				/* in (bit/A) measured average, calculated 19.209 */
#define ENGINE_MP_HF1_STD_180A50mV 15.756				/* in (bit/A) measured average, calculated 16.007 */
#define ENGINE_MP_HF1_STD_200A40mV 11.525				/* in (bit/A) */
#define ENGINE_MP_HF1_STD_250A50mV 11.423				/* in (bit/A) measured average, calculated 11.525 */
#define ENGINE_MP_HF1_STD_300A13kW 7.820				/* in (bit/A) measured average (24V and 36V)	  */
#define ENGINE_MP_HF1_STD_220A13kW 10.703				/* in (bit/A) measured average 					  */
#define ENGINE_MP_HF1_STD_130A13kW 18.234				/* in (bit/A) measured average 					  */
#define ENGINE_MP_HF1_STD_90A13kW  26.329				/* in (bit/A) measured average 					  */

#define ENGINE_MP_HF1_STD_75A50mV_DOUBLE 14.9780		/* in (bit/A) measured average, calculated 20.1236 */
#define ENGINE_MP_HF1_STD_100A50mV_DOUBLE 15.0927		/* in (bit/A) */
#define ENGINE_MP_HF1_STD_125A50mV_DOUBLE 12.5773		/* in (bit/A) */
#define ENGINE_MP_HF1_STD_150A50mV_DOUBLE 9.9871		/* in (bit/A) measured average, calculated 10.0618 */
#define ENGINE_MP_HF1_STD_180A50mV_DOUBLE 8.3080		/* in (bit/A) measured average, calculated 8.3848 */
#define ENGINE_MP_HF1_STD_200A40mV_DOUBLE 6.0371		/* in (bit/A) */
#define ENGINE_MP_HF1_STD_250A50mV_DOUBLE 6.1318		/* in (bit/A) measured average, calculated 6.0371 */
#define ENGINE_MP_HF1_STD_300A13kW_DOUBLE 3.910			/* in (bit/A) linear assumption from single engine */
#define ENGINE_MP_HF1_STD_220A13kW_DOUBLE 5.352			/* in (bit/A) linear assumption from single engine */
#define ENGINE_MP_HF1_STD_130A13kW_DOUBLE 9.117			/* in (bit/A) linear assumption from single engine */
#define ENGINE_MP_HF1_STD_90A13kW_DOUBLE  13.165		/* in (bit/A) linear assumption from single engine */

#define ENGINE_MP_HF1_STD_OFFSET -320.53				/* in (bit) */
#define ENGINE_MP_HF1_STD_13kW_OFFSET -118.81			/* in (bit) */

/* Pwm output standard scaling factors
 * Iset with zero output impedance and Imeas with zero load are equal. Iset static load is 100kΩ and Imeas static impedance is 1.5kΩ.
 * Constants:
 *   Islope				Measurement slope in (bit/A)
 *   IpwmSlope			Ouput slope in (A/bit)
 *   PWM				PWM signal in (bit)
 *   PWM_TOP			Maximum PWM signal in (bit)
 * Calculations:
 *   U_Iset_open = 3.3V*PWM/PWM_TOP*(1 + 47kΩ/91kΩ)
 *   U_Iset_R = 1kΩ
 *   U_i = 100kΩ/(100kΩ + 1kΩ)*U_Iset_open = 100kΩ/101kΩ*3.3V*PWM/PWM_TOP*(1 + 47kΩ/91kΩ) = Ui_meas = R_shunt*i*68kΩ/820Ω
 *   IpwmSlope = PWM/i = 1/(100kΩ/101kΩ*3.3V/PWM_TOP*(1 + 47kΩ/91kΩ)*820Ω/68kΩ/R_shunt)
 */
#define ENGINE_MP_HF1_STD_PWM_75A50mV 214.227			/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_100A50mV 160.670			/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_125A50mV 128.536			/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_150A50mV 107.114			/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_180A50mV 89.261			/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_200A40mV 64.268			/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_250A50mV 64.268			/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_300A13kW 76.058			/* in (bit/A) Measured, see document A02389	  */
#define ENGINE_MP_HF1_STD_PWM_220A13kW 82.113			/* in (bit/A) Measured	  */
#define ENGINE_MP_HF1_STD_PWM_130A13kW 108.230			/* in (bit/A) Measured	  */
#define ENGINE_MP_HF1_STD_PWM_90A13kW  155.37			/* in (bit/A) Measured	  */

#define ENGINE_MP_HF1_STD_PWM_75A50mV_DOUBLE 107.114	/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE 80.335	/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_125A50mV_DOUBLE 64.268	/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_150A50mV_DOUBLE 53.557	/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_180A50mV_DOUBLE 44.631	/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_200A40mV_DOUBLE 32.134	/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_250A50mV_DOUBLE 32.134	/* in (bit/A) */
#define ENGINE_MP_HF1_STD_PWM_300A13kW_DOUBLE 47.867	/* in (bit/A) Measured*/
#define ENGINE_MP_HF1_STD_PWM_220A13kW_DOUBLE 50.91	    /* in (bit/A) Calculated, 0.62*ENGINE_MP_HF1_STD_PWM_220A13kW */
#define ENGINE_MP_HF1_STD_PWM_130A13kW_DOUBLE 65.754	/* in (bit/A) Measured */
#define ENGINE_MP_HF1_STD_PWM_90A13kW_DOUBLE  96.329	/* in (bit/A) Calculated, 0.62*ENGINE_MP_HF1_STD_PWM_130A13kW */


#define ENGINE_MP_HF1_STD_PWM_OFFSET 1590.23			/* in (bit) */
#define ENGINE_MP_HF1_STD_PWM_13KW_OFFSET 928.79		/* in (bit) */

/* Temperature heat sink */
extern const Temperature_type Engine_MP_HF1_StdTempHeatsink;
extern const Temperature_type Engine_MP_HF1_StdTempHeatsink_T60_67;
extern const Temperature_type Engine_MP_HF1_StdTempHeatsink_T63_70;
extern const Temperature_type Engine_MP_HF1_StdTempHeatsink_T65_72;
extern const Temperature_type Engine_MP_HF1_StdTempHeatsink_T70_77;
extern const Temperature_type Engine_MP_HF1_StdTempHeatSink_T95_102;

extern const Temperature_type Engine_MP_HF1_27kTempHeatsink;
extern const Temperature_type Engine_MP_HF1_27kTempHeatsink_T60_67;

/* Temperature transformer */
extern const TempSenseDigital_enum Engine_MP_HF1_StdTempTrafo;

/* Battery signal logic level */
#define ENGINE_MP_HF1_STD_BIT_CURRENT(nominalCurrent) {-0.03*nominalCurrent, 0.6*nominalCurrent, 200}
#define ENGINE_MP_HF1_10KW_STD_BIT_CURRENT(nominalCurrent) {0.02*nominalCurrent, 0.6*nominalCurrent, 200}
#define ENGINE_MP_HF1_STD_BIT_CURRENT_DOUBLE(nominalCurrent) {-0.20*nominalCurrent, 0.8*nominalCurrent, 200}
#define ENGINE_MP_HF1_10KW_STD_BIT_CURRENT_DOUBLE(nominalCurrent) {0.04*nominalCurrent, 0.8*nominalCurrent, 200}

#endif
