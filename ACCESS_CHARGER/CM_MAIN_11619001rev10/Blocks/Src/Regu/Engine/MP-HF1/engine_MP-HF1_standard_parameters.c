#include "engine_MP-HF1_standard_parameters.h"

#include "math.h"

/* Accuracy voltage */
/* Accuracy current */
const EngineAccuracy_type Engine_NoAccuracy[2] = {{NAN,  NAN},
												  {NAN,  NAN}};

/* Ri */

/* Conversion between input power and output power */

/* Conversion between phase current and output power */

/* Controller */

/* Voltage measurement standard scaling factors */

/* Current measurement standard scaling factors */

/* Temperature heat sink						 		Type						A +			B*log(R_NTC/R) +	C*log(R_NTC/R)*log(R_NTC/R) +	unused	Short circuit	Open circuit	Derate		Over temperature */
const Temperature_type Engine_MP_HF1_StdTempHeatsink = {TempSensor_5V_Ntc,			0.003354,	0.000257,			0.000002,						NAN,	0.0067,			33.0,			65,			72}; /* NTC 10k, R = 10kΩ */
const Temperature_type Engine_MP_HF1_StdTempHeatsink_T70_77 = {TempSensor_5V_Ntc,	0.003354,	0.000257,			0.000002,						NAN,	0.0067,			33.0,			70,			77}; /* NTC 10k, R = 10kΩ */
const Temperature_type Engine_MP_HF1_StdTempHeatSink_T95_102 = {TempSensor_5V_Ntc,	0.003354,	0.000257,			0.000002,						NAN,	0.0067,			33.0,			95,			102}; /* NTC 10k, R = 10kΩ */
const Temperature_type Engine_MP_HF1_StdTempHeatsink_T60_67 = {TempSensor_5V_Ntc,	0.003354,	0.000257,			0.000002,						NAN,	0.0067,			33.0,			60,			67}; /* NTC 10k, R = 10kΩ */
const Temperature_type Engine_MP_HF1_StdTempHeatsink_T63_70 = {TempSensor_5V_Ntc,	0.003354,	0.000257,			0.000002,						NAN,	0.0067,			33.0,			63,			70}; /* NTC 10k, R = 10kΩ */

const Temperature_type Engine_MP_HF1_27kTempHeatsink = {TempSensor_5V_Ntc,			0.0036039,	0.00025157,			0.000000,						NAN,	0.0067,			33.0,			65,			72}; /* NTC 10k, R = 27kΩ */
const Temperature_type Engine_MP_HF1_27kTempHeatsink_T60_67 = {TempSensor_5V_Ntc,	0.0036039,	0.00025157,			0.000000,						NAN,	0.0067,			33.0,			60,			67}; /* NTC 10k, R = 27kΩ */

/* Temperature transformer */
const TempSenseDigital_enum Engine_MP_HF1_StdTempTrafo = TempSensorDigital_No;
