/*
 * engine_80_160_220V.h
 *
 *  Created on: May 12, 2014
 *      Author: HeLi
 */

#ifndef ENGINE_80_160_220VAC_H_
#define ENGINE_80_160_220VAC_H_

#include "engines_types.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_80_160_220V__UIchar[] = ENGINE_MP_HF1_220V_UI_CHARACTERISTICS(12, 40, 160);

#define ENGINE_80_160_220V \
		/*	    *********************** \
			    * Mtm 80V 160A 220VAC * \
			    ***********************/ \
				{ \
				57,												/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_220V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_80_160_220V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator */ \
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_80V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_100A50mV_DOUBLE), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_80V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
					/* NormToBit */ \
					{ \
						ENGINE_MP_HF1_STD_80V, 0.0, \
						ENGINE_MP_HF1_STD_100A50mV_DOUBLE, ENGINE_MP_HF1_STD_OFFSET, \
						ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE, ENGINE_MP_HF1_STD_PWM_OFFSET \
					}, \
				    /* BitToNorm */ \
					{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
				 \
				/* Theatsink */ \
				&Engine_MP_HF1_StdTempHeatsink, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT_DOUBLE(160.0) \
			} \

#endif /* ENGINE_80_160_220V_H_ */
