/*
 * engine_24_200_4000V.h
 *
 *  Created on: Sept 27, 2013
 *      Author: HeLi
 */

#ifndef ENGINE_24_200_400V_H_
#define ENGINE_24_200_400V_H_

#include "engines_types.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_24_200_400V__UIchar[] = ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(6, 12, 200);

#define ENGINE_24_200_400V \
		/*	    *********************** \
			    * Mtm 24V 200A 400VAC * \
			    ***********************/ \
				{ \
				28,												/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_400V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_24_200_400V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_250A50mV*ENGINE_MP_HF1_STD_24V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_250A50mV*ENGINE_MP_HF1_STD_250A50mV), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_250A50mV*ENGINE_MP_HF1_STD_250A50mV*ENGINE_MP_HF1_STD_24V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_250A50mV) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
					/* NormToBit */ \
					{ \
						ENGINE_MP_HF1_STD_24V, 0.0, \
						ENGINE_MP_HF1_STD_250A50mV, ENGINE_MP_HF1_STD_OFFSET, \
						ENGINE_MP_HF1_STD_PWM_250A50mV, ENGINE_MP_HF1_STD_PWM_OFFSET \
					}, \
				    /* BitToNorm */ \
					{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
\
				/* Theatsink */ \
				&Engine_MP_HF1_StdTempHeatsink, /* NTC 10k, R = 10kΩ */ \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT(200.0) \
			} \

#endif /* ENGINE_24_200_400V_H_ */
