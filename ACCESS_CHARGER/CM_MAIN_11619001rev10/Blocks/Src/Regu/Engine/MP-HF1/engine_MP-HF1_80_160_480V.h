/*
 * engine_24_120_230V.h
 *
 *  Created on: Oct 29, 2010
 *      Author: nicka
 */

#ifndef ENGINE_80_160_480VAC_H_
#define ENGINE_80_160_480VAC_H_

#include "engines_types.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_80_160_480V__UIchar[] = ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(12, 40, 160);

#define ENGINE_80_160_480V \
		/*	    *********************** \
			    * Mtm 80V 160A 480VAC * \
			    ***********************/ \
				{ \
				29,												/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_480V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_80_160_480V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator */ \
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_80V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_100A50mV_DOUBLE), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_80V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
					/* NormToBit */ \
					{ \
						ENGINE_MP_HF1_STD_80V, 0.0, \
						ENGINE_MP_HF1_STD_100A50mV_DOUBLE, ENGINE_MP_HF1_STD_OFFSET, \
						ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE, ENGINE_MP_HF1_STD_PWM_OFFSET \
					}, \
				    /* BitToNorm */ \
					{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
				 \
				/* Theatsink */ \
				&Engine_MP_HF1_StdTempHeatsink, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT_DOUBLE(160.0) \
			} \

#endif /* ENGINE_80_160_480V_H_ */
