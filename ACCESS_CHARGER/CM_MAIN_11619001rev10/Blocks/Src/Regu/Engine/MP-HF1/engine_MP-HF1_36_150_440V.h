/*
 * engine_36_150_440V.h
 *
 *  Created on: Jan 24, 2013
 *      Author: HeLi
 */

#ifndef ENGINE_36_150_440V_H_
#define ENGINE_36_150_440V_H_

#include "engines_types.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_36_150_440V__UIchar[] = ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(12, 18, 150);

#define ENGINE_36_150_440V \
		/*	    *********************** \
			    * Mtm 36V 150A 440VAC * \
			    ***********************/ \
				{ \
				41,												/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_440V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_36_150_440V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_150A50mV*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_150A50mV*ENGINE_MP_HF1_STD_150A50mV), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_150A50mV*ENGINE_MP_HF1_STD_150A50mV*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_150A50mV) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
					/* NormToBit */ \
					{ \
						ENGINE_MP_HF1_STD_48V, 0.0, \
						ENGINE_MP_HF1_STD_150A50mV, ENGINE_MP_HF1_STD_OFFSET, \
						ENGINE_MP_HF1_STD_PWM_150A50mV, ENGINE_MP_HF1_STD_PWM_OFFSET \
					}, \
				    /* BitToNorm */ \
					{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
\
				/* Theatsink */ \
				 &Engine_MP_HF1_StdTempHeatsink_T70_77, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT(150.0) \
			} \

#endif /* ENGINE_36_150_440V_H_ */
