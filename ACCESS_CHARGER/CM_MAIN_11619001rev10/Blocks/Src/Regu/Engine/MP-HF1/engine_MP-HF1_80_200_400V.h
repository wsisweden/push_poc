#ifndef ENGINE_80_200_400V_H_
#define ENGINE_80_200_400V_H_


#include "engines_types.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_80_200_400V__UIchar[] = ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(12, 40, 200);

#define ENGINE_80_200_400V \
		/*	    *********************** \
			    * Mtm 80V 200A 400VAC * \
			    ***********************/ \
				{ \
				39,												/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_400V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_80_200_400V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_80V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_100A50mV_DOUBLE), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_100A50mV_DOUBLE*ENGINE_MP_HF1_STD_80V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ \
							ENGINE_MP_HF1_STD_80V, 0.0, \
							ENGINE_MP_HF1_STD_100A50mV_DOUBLE, ENGINE_MP_HF1_STD_OFFSET, \
							ENGINE_MP_HF1_STD_PWM_100A50mV_DOUBLE, ENGINE_MP_HF1_STD_PWM_OFFSET \
						}, \
					    /* BitToNorm */ \
						{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
\
				/* Theatsink */ \
				&Engine_MP_HF1_StdTempHeatsink_T60_67, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT(200.0) \
			} \

#endif /* ENGINE_80_200_400V_H_ */
