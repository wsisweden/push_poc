/*
 * engine_24_120_230V.h
 *
 *  Created on: Oct 29, 2010
 *      Author: nicka
 */

#ifndef ENGINE_48_260_400V_H_
#define ENGINE_48_260_400V_H_

#include "engine_MP-HF1_standard_parameters.h"
#include "engine_MP-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_48_260_400V__UIchar[] = ENGINE_MP_HF1_STANDARD_UI_CHARACTERISTICS(12, 24, 260);

#define ENGINE_48_260_400V \
		/*	    *********************** \
			    * Mtm 48V 150A 400VAC * \
			    ***********************/ \
				{ \
				22,												/* Code */ \
				ENGINE_MP_HF1_STANDARD_RI,						/* Ri */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY,				/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				ENGINE_MP_HF1_STANDARD_EFFICIENCY_APPARENT_400V,/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_48_260_400V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator */ \
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_150A50mV_DOUBLE*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_150A50mV_DOUBLE*ENGINE_MP_HF1_STD_150A50mV_DOUBLE), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_150A50mV_DOUBLE*ENGINE_MP_HF1_STD_150A50mV_DOUBLE*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_150A50mV_DOUBLE) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
					/* NormToBit */ \
					{ \
						ENGINE_MP_HF1_STD_48V, 0.0, \
						ENGINE_MP_HF1_STD_150A50mV_DOUBLE, ENGINE_MP_HF1_STD_OFFSET, \
						ENGINE_MP_HF1_STD_PWM_150A50mV_DOUBLE, ENGINE_MP_HF1_STD_PWM_OFFSET \
					}, \
				    /* BitToNorm */ \
					{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
\
				/* Theatsink \
				 * Type					A +			B*log(R_NTC/R) +	C*log(R_NTC/R)*log(R_NTC/R)		Short circuit	Open circuit	Derate		Over temperature */ \
				&Engine_MP_HF1_StdTempHeatsink_T70_77, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_High, \
				ENGINE_MP_HF1_STD_BIT_CURRENT_DOUBLE(260.0) \
			} \

#endif /* ENGINE_48_260_400V_H_ */
