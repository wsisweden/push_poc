/*
 * engine_48_130_400V.h
 *
 *  Created on: Oct 29, 2010
 *      Author: nicka
 */

#ifndef ENGINE_DEFAULT_H_
#define ENGINE_DEFAULT_H_

#include "engines_types.h"
#include "math.h"

PUBLIC EngineUIchar_type const_P Default__UIchar[] = {{NAN, NAN}};

#define ENGINE_DEFAULT \
		/*	    *********************** \
			    * Default             * \
			    ***********************/ \
				{ \
				-1,												/* Code */ \
				0.0,											/* Ri */ \
				0.0,											/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				0.0,											/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Default__UIchar, 1, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF1_STD_PWM_150A50mV*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF1_STD_PWM_150A50mV*ENGINE_MP_HF1_STD_150A50mV), \
					ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF1_STD_PWM_150A50mV*ENGINE_MP_HF1_STD_150A50mV*ENGINE_MP_HF1_STD_48V), \
					ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF1_STD_PWM_150A50mV) \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ \
							ENGINE_MP_HF1_STD_48V, 0.0, \
							ENGINE_MP_HF1_STD_150A50mV, ENGINE_MP_HF1_STD_OFFSET, \
							ENGINE_MP_HF1_STD_PWM_150A50mV, ENGINE_MP_HF1_STD_PWM_OFFSET \
						}, \
					    /* BitToNorm */ \
						{NAN, NAN, NAN, NAN, NAN, NAN} \
				 }, \
\
				/* Theatsink */ \
				&Engine_MP_HF1_StdTempHeatsink_T70_77, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_No, \
				ENGINE_MP_HF1_STD_BIT_CURRENT(130.0) \
			} \

#endif /* ENGINE_DEFAULT_H_ */
