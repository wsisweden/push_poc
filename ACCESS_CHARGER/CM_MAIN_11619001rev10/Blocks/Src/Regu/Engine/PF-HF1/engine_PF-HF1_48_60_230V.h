#ifndef ENGINE_48_60_230V_H_
#define ENGINE_48_60_230V_H_


#include "engines_types.h"

PRIVATE EngineUIchar_type const_P Mtm_48_60_230V__UIchar[] =
{
//    Voltage  Current
		{    0.0,     6.0 },
		{    6.0,     6.0 },
		{    6.0,     6.0 },
		{   12.0,     6.0 },
		{   12.0,    60.0 },
		{   49.0,    60.0 },
		{   62.0,    40.0 },
		{   67.3,    25.0 },
		{   67.3,     0.0 }
};

#define ENGINE_48_60_230V \
		/*	    *********************** \
			    * Mtm 48V 150A 400VAC * \
			    ***********************/ \
				{ \
				13,						/* Code */ \
				1.2/1000,					/* Ri */ \
				1.0*0.93,					/* Idc = P*n/Udc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				230.0*0.93*0.95,			/* Idc = P*n/Udc = U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_48_60_230V__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator */ \
				{ \
					0.1,		/* KpU 		\	2019-02-22														*/ \
					0.01*1.0,	/* KIU 		 }	Tested with battery and resistive load with small overshoot		*/ \
					5.0,		/* KchooseU /																	*/ \
					0.05 ,		/* KpI 		\	2019-02-22														*/ \
					0.01*1.0,	/* KII 		 }	Tested with battery and resistive load with small overshoot		*/ \
					1.0,		/* KchooseI /																	*/ \
					0.08,		/* KpP 		\	2011-02-08														*/ \
					0.01*8,		/* KIP 		 }	Tested with square wave smooth transitions without overshoot	*/ \
					1.0,		/* KchooseP /																	*/ \
					100.00,		/* KpT */ \
					0.01*0.3,	/* KIT */ \
					400.0		/* KchooseT */ \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ \
								49.60072,			/* UadSlope */ \
								-5.089539,			/* UadOffset */ \
								50.8326,			/* IadSlope */ \
								-91.455,			/* IadOffset */ \
								281.4,				/* IpwmSlope */ \
								500.00				/* IpwmOffset */ \
						}, \
					    /* BitToNorm */ \
						{0.0, 0.0, 0.0, 0.0, 0.0, 0.0} \
				 }, \
\
				/* Theatsink \
				 * Type					A +				B*(R_PTC/R) +		C*(R_PTC/R)*(R_PTC/R)	Short circuit	Open circuit	Derate		Over temperature */ \
				&Engine_OthersHeatsink1, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_No, \
				ENGINE_PF_HF1_STD_BIT_CURRENT(60.0)				/* battery signal logic levels (5.0, 15.0) and (10.0, 25.0) tested with 22nF capacitor on input and looks good */ \
			} \

#endif /* ENGINE_48_60_230V_H_ */
