#ifndef ENGINE_24_120_240V_H_
#define ENGINE_24_120_240V_H_

PRIVATE EngineUIchar_type const_P Mtm_24_120_240V__UIchar[] =
{
//    Voltage  Current
	{    0.0,    12.0 },
	{    6.0,    12.0 },
	{    6.0,    12.0 },
	{   12.0,    12.0 },
	{   12.0,   120.0 },
	{   25.68,  120.0 },
	{   30.3,   104.0 },
	{   36.0,     0.0 } // Remember to change length further down then changing number of elements
};

#define ENGINE_24_120_240V \
		/*	    *********************** \
			    * 24V 120A 230VAC * \
			    ***********************/ \
				{ \
				12,						/* Code */ \
				0.0/1000,					/* Ri */ \
				0.93,						/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				240.0*0.93*0.95,			/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_24_120_240V__UIchar, 8, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator 2011-02-08 The regulators was not tested then crossing each other. */ \
				{ \
					0.1,		/* KpU 		\	2019-02-22														*/ \
					0.01*1.0,	/* KIU 		 }	Tested with battery and resistive load with small overshoot		*/ \
					5.0,		/* KchooseU /																	*/ \
					0.05 ,		/* KpI 		\	2019-02-22														*/ \
					0.01*1.0,	/* KII 		 }	Tested with battery and resistive load with small overshoot		*/ \
					1.0,		/* KchooseI /																	*/ \
					0.08,		/* KpP 		\	2011-02-08														*/ \
					0.01*8,		/* KIP 		 }	Tested with square wave smooth transitions without overshoot	*/ \
					1.0,		/* KchooseP /																	*/ \
					100.00,		/* KpT */ \
					0.01*0.3,	/* KIT */ \
					400.0		/* KchooseT */ \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ /* Checked that scaling constants seems to be correctly entered */ \
								98.896,				/* UadSlope */ \
								12.495,				/* UadOffset */ \
								25.503,				/* IadSlope */ \
								-74.616,			/* IadOffset */ \
								141.96,				/* IpwmSlope */ \
								340.56				/* IpwmOffset */ \
						}, \
					    /* BitToNorm */ \
						{0.0, 0.0, 0.0, 0.0, 0.0, 0.0} \
				 }, \
\
				/* Theatsink */ \
				&Engine_OthersHeatsink1, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_No, \
				ENGINE_PF_HF1_STD_BIT_CURRENT(120.0)				/* battery signal logic levels (5.0, 15.0) and (10.0, 25.0) tested with 22nF capacitor on input and looks good */ \
			} \

#endif /* ENGINE_24_120_240V_H_ */
