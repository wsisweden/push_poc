#ifndef ENGINE_PARAMETERS_OTHERS_H
#define ENGINE_PARAMETERS_OTHERS_H

#include "engines_types.h"

/* Engine output curve */

/* Accuracy voltage */

/* Ri */

/* Conversion between input power and output power */

/* Conversion between phase current and output power */

/* Accuracy current */

/* Controller */

/* Voltage measurement standard scaling factors */

/* Current measurement standard scaling factors */

/* Pwm output standard scaling factors */

/* Temperature board */

/* Temperature heat sink */
extern const Temperature_type Engine_OthersHeatsink1;

/* Temperature transformer */

/* Battery signal logic level */
#define ENGINE_PF_HF1_STD_BIT_CURRENT(nominalCurrent) {0.0625*nominalCurrent, 0.25*nominalCurrent, 1850}

#endif
