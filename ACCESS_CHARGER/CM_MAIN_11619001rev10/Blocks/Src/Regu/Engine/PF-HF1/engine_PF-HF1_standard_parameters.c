#include "engine_PF-HF1_standard_parameters.h"

#include "math.h"

/* Temperature heat sink						 Type					A +				B*(R_PTC/R) +		C*(R_PTC/R)*(R_PTC/R) +	unused	Short circuit	Open circuit	Derate		Over temperature */
const Temperature_type Engine_OthersHeatsink1 = {TempSensor_5V_Ptc,		140.107754,		275.048877,			-58.471031,				NAN,	0.5,			3.5,			95,			102}; /* KTY 83/120, R = 1.5kΩ */
