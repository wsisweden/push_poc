/*
 * engine_24_120_230V.h
 *
 *  Created on: Oct 29, 2010
 *      Author: nicka
 */

#ifndef ENGINE_36_80_230V_H_
#define ENGINE_36_80_230V_H_


#include "engines_types.h"
#include "engine_PF-HF1_standard_parameters.h"

PRIVATE EngineUIchar_type const_P Mtm_36_80_230__UIchar[] =
{
//    Voltage  Current
	{    0.0,     8.0 },
	{    6.0,     8.0 },
	{    6.0,     8.0 },
	{    9.0,     8.0 },
	{    9.0,    80.0 },
	{   43.6,    80.0 },
	{   47.0,    73.8 },
	{   50.5,    20.0 },
	{   50.5,     0.0 }
};

#define ENGINE_36_80_230V \
		/*	    *********************** \
			    * Mtm 48V 150A 400VAC * \
			    ***********************/ \
				{ \
				11,						/* Code */ \
				0.0/1000,					/* Ri */ \
				1.0*0.93,					/* Idc = P*n/Udc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
				230.0*0.93*0.95,			/* Idc = P*n/Udc = U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
\
				/* UI-characteristic pointer */ \
				Mtm_36_80_230__UIchar, 9, \
				/* U-accuracy pointer */ \
				Engine_NoAccuracy, \
				/* I-accuracy pointer */ \
				Engine_NoAccuracy, \
\
				/* Regulator */ \
				{ \
					0.1,		/* KpU 		\	2019-02-22														*/ \
					0.01*1.0,	/* KIU 		 }	Tested with battery and resistive load with small overshoot		*/ \
					5.0,		/* KchooseU /																	*/ \
					0.05 ,		/* KpI 		\	2019-02-22														*/ \
					0.01*1.0,	/* KII 		 }	Tested with battery and resistive load with small overshoot		*/ \
					1.0,		/* KchooseI */ \
					1.08,		/* KpP */ \
					0.01*8,		/* KIP */ \
					1.0,		/* KchooseP */ \
					100.00,		/* KpT */ \
					0.01*0.3,	/* KIT */ \
					400.0		/* KchooseT */ \
				}, \
\
				/* ScalefactorNom */ \
				{ \
						/* NormToBit */ \
						{ \
								52.9191,			/* UadSlope */ \
								7.02223,			/* UadOffset */ \
								24.696223,			/* IadSlope */ \
								-323.9283,			/* IadOffset */ \
								119.76,				/* IpwmSlope */ \
								1590.23				/* IpwmOffset */ \
						}, \
					    /* BitToNorm */ \
						{0.0, 0.0, 0.0, 0.0, 0.0, 0.0} \
				 }, \
\
				/* Theatsink \
				 * Type					A +				B*(R_PTC/R) +		C*(R_PTC/R)*(R_PTC/R)		Short circuit	Open circuit	Derate		Over temperature */ \
				&Engine_OthersHeatsink1, \
\
				/* Ttrafo \
				 * Type */ \
				TempSensorDigital_No, \
				ENGINE_PF_HF1_STD_BIT_CURRENT(80.0) \
			} \

#endif /* ENGINE_36_80_230V_H_ */
