#include "engine_MP-HF3_standard_parameters.h"

/* Accuracy voltage */
/* Accuracy current */

/* Ri */

/* Conversion between input power and output power */

/* Conversion between phase current and output power */

/* Controller */

/* Voltage measurement standard scaling factors */

/* Current measurement standard scaling factors */

/* Temperature heat sink						 		Type					A*(log(R_NTC))^3 +		B*(log(R_NTC))^2 +		C*log(R_NTC) +			D						Short circuit	Open circuit	Derate		Over temperature */
const Temperature_type Engine_MP_HF3_StdTempHeatsink_T75_80 = {TempSensor_MTM_HF3_NTC, 9.221227e-8,            1.857224e-006,          2.513281e-004,          3.096346e-003,          0.02,          	100.0,        	75,         80}; /* NTC 22k, R = 820Ω */
const Temperature_type Engine_MP_HF3_StdTempHeatsink_T70_75 = {TempSensor_MTM_HF3_NTC, 9.221227e-8,            1.857224e-006,          2.513281e-004,          3.096346e-003,          0.02,          	100.0,        	70,         75}; /* NTC 22k, R = 820Ω */

/* Temperature transformer */
const TempSenseDigital_enum Engine_MP_HF3_StdTempTrafo = TempSensorDigital_No;
