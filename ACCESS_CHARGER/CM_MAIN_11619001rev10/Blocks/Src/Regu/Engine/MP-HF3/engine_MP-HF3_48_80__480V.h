/*
 ********************************* \
 * Engine MP-HF3 48V 80A 480VAC * \
 ********************************* \
 *
 *  Created: 2015-01-07
 *   Author: nicka
 */

#ifndef ENGINE_MP_HF3_48_80_480V_H
#define ENGINE_MP_HF3_48_80_480V_H

#include "engines_types.h"
#include "../MP-HF1/engine_MP-HF1_standard_parameters.h"
#include "engine_MP-HF3_standard_parameters.h"
#include "math.h"

static const EngineUIchar_type MP_HF3_48_80_480V__UIchar[] = ENGINE_MP_HF3_STANDARD_UI_CHARACTERISTICS(6, 24, 80);

#define ENGINE_MP_HF3_48_80__480V \
	{ \
		110,												/* Code */ \
		ENGINE_MP_HF3_STANDARD_RI,							/* Ri */ \
		ENGINE_MP_HF3_STANDARD_EFFICIENCY,					/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
		ENGINE_MP_HF3_STANDARD_EFFICIENCY_APPARENT_480V,	/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
		\
		MP_HF3_48_80_480V__UIchar, 9,						/* UI-characteristic pointer */ \
		Engine_NoAccuracy,									/* Voltage accuracy */ \
		Engine_NoAccuracy, 									/* Current accuracy */ \
		\
		{													/* Regulator						*/ \
			ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF3_STD_ISET_80A*ENGINE_MP_HF3_STD_48V), \
			ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF3_STD_ISET_80A*ENGINE_MP_HF3_STD_80A), \
			ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF3_STD_ISET_80A*ENGINE_MP_HF3_STD_80A*ENGINE_MP_HF3_STD_48V), \
			ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF3_STD_ISET_80A) \
		}, \
		\
		{													/* Scale factors nominal								*/ \
			{												/* Normalized to bit									*/ \
				ENGINE_MP_HF3_STD_48V, 0.0,					/* Voltage scaling slope, offset						*/ \
				ENGINE_MP_HF3_STD_80A,						/* Current scaling slope								*/ \
				ENGINE_MP_HF3_STD_OFFSET,					/* automatically adjusted offset, initialize anyway		*/ \
				ENGINE_MP_HF3_STD_ISET_80A,					/* PWM current output scaling							*/ \
				ENGINE_MP_HF3_STD_PWM_OFFSET				/* PWM offset											*/ \
			}, \
			{NAN, NAN, NAN, NAN, NAN, NAN}					/* Bit to normalized, automatically calculated			*/ \
		}, \
		\
		&Engine_MP_HF3_StdTempHeatsink_T70_75,				/* Temperature heat sink */ \
		TempSensorDigital_High,								/* Temperature transformer */ \
		ENGINE_MP_HF3_STD_BIT_CURRENT(80.0)					/* Battery signal logic levels */ \
	} \

#endif /* ENGINE_MP_HF3_48_80_480V_H */
