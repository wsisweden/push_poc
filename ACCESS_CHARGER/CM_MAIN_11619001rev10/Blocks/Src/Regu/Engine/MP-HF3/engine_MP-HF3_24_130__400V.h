/*
 ********************************* \
 * Engine MP-HF3 24V 130A 380-440VAC * \
 ********************************* \
 *
 *  Created: 2015-01-07
 *   Author: nicka
 */

#ifndef ENGINE_MP_HF3_24_130_400V_H
#define ENGINE_MP_HF3_24_130_400V_H_

#include "engines_types.h"
#include "engine_MP-HF3_standard_parameters.h"
#include "math.h"

static const EngineUIchar_type MP_HF3_24_130_400V__UIchar[] = ENGINE_MP_HF3_STANDARD_UI_CHARACTERISTICS(6, 12, 130);

#define ENGINE_MP_HF3_24_130__400V \
	{ \
		106,												/* Code */ \
		ENGINE_MP_HF3_STANDARD_RI,							/* Ri */ \
		ENGINE_MP_HF3_STANDARD_EFFICIENCY,					/* Idc = P*n/Udc, P power, n efficiency, Udc battery voltage */ \
		ENGINE_MP_HF3_STANDARD_EFFICIENCY_APPARENT,			/* Idc = P*n/Udc = sqrt(3)*U*Iphase*k*n/Udc, P power, n efficiency, Udc battery voltage, k power factor */ \
		\
		MP_HF3_24_130_400V__UIchar, 9,						/* UI-characteristic pointer */ \
		Engine_NoAccuracy,									/* Voltage accuracy */ \
		Engine_NoAccuracy, 									/* Current accuracy */ \
		\
		{													/* Regulator						*/ \
			ENGINE_MP_HF1_STD_REGULATOR_U(ENGINE_MP_HF3_STD_ISET_130A*ENGINE_MP_HF3_STD_24V), \
			ENGINE_MP_HF1_STD_REGULATOR_I(ENGINE_MP_HF3_STD_ISET_130A*ENGINE_MP_HF3_STD_130A), \
			ENGINE_MP_HF1_STD_REGULATOR_P(ENGINE_MP_HF3_STD_ISET_130A*ENGINE_MP_HF3_STD_130A*ENGINE_MP_HF3_STD_24V), \
			ENGINE_MP_HF1_STD_REGULATOR_T(ENGINE_MP_HF3_STD_ISET_130A) \
		}, \
		\
		{													/* Scale factors nominal								*/ \
			{												/* Normalized to bit									*/ \
				ENGINE_MP_HF3_STD_24V, 0.0,					/* Voltage scaling slope, offset						*/ \
				ENGINE_MP_HF3_STD_130A,						/* Current scaling slope								*/ \
				ENGINE_MP_HF3_STD_OFFSET,					/* automatically adjusted offset, initialize anyway		*/ \
				ENGINE_MP_HF3_STD_ISET_130A,				/* PWM current output scaling							*/ \
				ENGINE_MP_HF3_STD_PWM_OFFSET				/* PWM offset											*/ \
			}, \
			{NAN, NAN, NAN, NAN, NAN, NAN}					/* Bit to normalized, automatically calculated			*/ \
		}, \
		\
		&Engine_MP_HF3_StdTempHeatsink_T70_75,				/* Temperature heat sink */ \
		TempSensorDigital_High,								/* Temperature transformer */ \
		ENGINE_MP_HF3_STD_BIT_CURRENT(130.0)				/* Battery signal logic levels */ \
	} \

#endif /* ENGINE_MP_HF3_24_130_400V_H */
