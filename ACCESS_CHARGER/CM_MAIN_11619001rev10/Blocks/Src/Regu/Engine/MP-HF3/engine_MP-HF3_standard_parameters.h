#ifndef ENGINE_MP_HF3_STANDARD_PARAMETERS_H
#define ENGINE_MP_HF3_STANDARD_PARAMETERS_H

#include "engines_types.h"
#include "math.h"
#include "pwm.h"

#define ENGINE_MP_HF3_STANDARD_UI_CHARACTERISTICS(nominalCellsMin, nominalCellsMax, nominalCurrent) \
{ \
/*	 Voltage					Current */ \
	{0.0,						0.10*nominalCurrent},	/* Voltage absolute				All currents should be multiplied with nominal current */ \
	{6.0,						0.10*nominalCurrent},	/* Voltage absolute */ \
	{6.0,						0.10*nominalCurrent},	/* Voltage absolute */ \
	{1.8*nominalCellsMin + 0.2,	0.10*nominalCurrent},	/* (1.8V/cell + 0.2V) */ \
	{1.8*nominalCellsMin + 0.2,	1.00*nominalCurrent},	/* (1.8V/Cell + 0.2V) */ \
	{2.4*nominalCellsMax + 0.8,	1.00*nominalCurrent},	/* (2.4V/cell + 0.6V) */ \
	{2.6*nominalCellsMax + 0.6,	0.92*nominalCurrent},	/* (2.6V/cell + 0.5V) */ \
	{2.8*nominalCellsMax + 0.4,	0.25*nominalCurrent},	/* (2.8V/cell + 0.2V) */ \
	{2.8*nominalCellsMax + 0.4,	0.00*nominalCurrent}	/* (2.8V/cell + 0.2V) */ \
};

/* Ri */
#define ENGINE_MP_HF3_STANDARD_RI (float)(0.0/1000)

/* Conversion between input power and output power */
#define ENGINE_MP_HF3_STANDARD_EFFICIENCY (float)(0.94)

/* Conversion between phase current and output power */
#define ENGINE_MP_HF3_STANDARD_EFFICIENCY_APPARENT (float)(1.7320508*400.0*0.94*0.95)
#define ENGINE_MP_HF3_STANDARD_EFFICIENCY_APPARENT_480V (float)(1.7320508*480.0*0.94*0.95)
#define ENGINE_MP_HF3_STANDARD_EFFICIENCY_APPARENT_220V (float)(1.7320508*220.0*0.94*0.95)

/* Accuracy voltage */
/* Accuracy current */
extern const EngineAccuracy_type Engine_NoAccuracy[2];

/* Controller
 * A block diagram could be found in the folder Regu.
 * Constants:
 *   R(s)			Reference signal
 *   F(s)			PI-regulator
 *   G(s)			System, input PWM signal output several values that should be limited
 *   Kpwm			Conversion constant from PWM signal in (bit) to current in (A)
 *   Kmeas			Conversion constant from value that should be limited in (unit) to (bit)
 * Calculations:
 *   Y(s) = PI(s)*Kpwm*G(s)(R(s) - Y(s)*Kmeas) <=> Y(s)(1 + PI(s)*Kpwm*G(s)*Kmeas) = PI(s)*Kpwm*G(s)R(s)
 *   Y(s)/R(S) =  PI(s)*Kpwm*G(s)/(1 + PI(s)*Kpwm*G(s)*Kmeas)
 *   Kpwm and Kmeas change for different chargers and if the dynamic response is the same it should be possible to adjust the regulator so that
 *   the same close loop dynamic response is achieved.
 *   Y(s)/R(S) =  PI(s)*(Kpwm*Kmeas)/(Kpwm*Kmeas)*Kpwm*G(s)/(1 + PI(s)*(Kpwm*Kmeas)/(Kpwm*Kmeas)*Kpwm*G(s)*Kmeas), PIsubstitute(s) = PI(s)*(Kpwm*Kmeas)
 *   Y(s)/R(S) =  PI_substitute(s)/Kmeas*G(s)/(1 + PI_substitue(s)*G(s)), PIsubstitute(s) = PI(s)*(Kpwm*Kmeas)
 *   This equation with PI_substitute(s) is what we really want so PI(s) = PIsubstitute(s)/(Kpwm*Kmeas)
 */
#define ENGINE_MP_HF3_STD_REGULATOR_U(k) 0.050*107.114*53.222/(k) /* KpU */,	0.01*4.0*107.114*53.222/(k) /* KiU */,	2.000 /* KchooseI */
#define ENGINE_MP_HF3_STD_REGULATOR_I(k) 0.050*107.114*19.209/(k) /* KpI */,	0.01*5.0*107.114*19.209/(k) /* KiI */,	1.000 /* KchooseI */
#define ENGINE_MP_HF3_STD_REGULATOR_P(k) 0.080*107.114*1022.3/(k) /* KpP */,	0.01*8.0*107.114*1022.3/(k) /* KiP */,	1.000 /* KchooseI */
#define ENGINE_MP_HF3_STD_REGULATOR_T(k) 100.0*107.114*1.0000/(k) /* KpT */,	0.01*0.3*107.114*1.0000/(k) /* KiT */,	400.0 /* KchooseI */

/* Voltage measurement standard scaling factors:
 *   There are two simple voltage divisors the fist inside the engine and the second on the display card to scale the voltage to a suitable level
 *
 * Constants:
 *   U_OPEN		Open circuit voltage
 *   I_SHORT	Short circuit current
 *   R_S		Output resistance at open circuit voltage
 *   R2			Resistance to ground inside engine
 *   R1			Resistance to battery inside engine
 *   1kΩ		Series resistance on display card
 *   8.2kΩ		Resistance to ground on display card
 *
 * Calculations:
 *   U_OPEN = Ubatt*R2/(R1 + R2)
 *   I_SHORT = Ubatt/R1
 *   R_S = U_OPEN/I_SHORT = R1*R2/(R1 + R2)
 *   U_ADC = U_OPEN*8.2kΩ/(R_S + 1kΩ + 8.2kΩ) = Ubatt*R2/(R1 + R2)*8.2kΩ/(R_S + 1kΩ + 8.2kΩ)
 *   ADC = U_ADC/3.0*4095 = Ubatt*R2/(R1 + R2)*8.2kΩ/(R_S + 1kΩ + 8.2kΩ)/3.0*4095
 *   UadSlope = ADC/Ubatt = R2/(R1 + R2)*8.2kΩ/(R_S + 1kΩ + 8.2kΩ)/3.0*4095
 */
#define ENGINE_MP_HF3_STD_U(R1, R2) (float)(R2/(R1 + R2)*8.2e3/(R1*R2/(R1 + R2) + 1e3 + 8.2e3)/3.0*4095)	// Gain for voltage calculated with secondary resistor network
#define ENGINE_MP_HF3_STD_24V		(float)(1/0.009258327)	//ENGINE_MP_HF3_STD_U(34e3, 4.7e3)				/* in (bit/V) OBS!! measured value */
#define ENGINE_MP_HF3_STD_36V		ENGINE_MP_HF3_STD_U(50.7e3, 4.7e3)										/* in (bit/V) */
#define ENGINE_MP_HF3_STD_48V		(float)(1/0.018760508)	//ENGINE_MP_HF3_STD_U(68e3, 4.7e3)				/* in (bit/V) OBS!! measured value */
#define ENGINE_MP_HF3_STD_80V		ENGINE_MP_HF3_STD_U(117e3, 4.7e3)										/* in (bit/V) */

/* Current measurement standard scaling factors
 * Constants:
 *   G		Shunt gain in V/A = Ω
 *
 * Calculations:
 *   U_OPEN = Ui
 *   I_SHORT = Ui/1.5kΩ
 *   R_S = U_OPEN/I_SHORT = 1.5kΩ
 *   U_ADC = U_OPEN*5.6kΩ/(1.5kΩ + 3.9kΩ + 5.6kΩ) = Ui*5.6/11
 *   ADC = U_ADC/3.0*4095 = Ui*5.6/11/3.0*4095
 *   IadSlope = G*ADC/Ubatt = G*5.6/11/3.0*4095, G in (V/A)
 */
#define ENGINE_MP_HF3_STD_80A_GAIN		(float)(0.15e-3*270e3/820)						/* shunt gain 80A*/
#define ENGINE_MP_HF3_STD_105A_GAIN		(float)(0.10e-3*309e3/820)						/* shunt gain) 105A*/
#define ENGINE_MP_HF3_STD_130A_GAIN		(float)(0.10e-3*255e3/820)						/* shunt gain) 130A*/

#define ENGINE_MP_HF3_STD_I(G) (float)(G*5.6/11/3.0*4095)								// Gain for 0.3mΩ shunt calculated with operational amplifier gain
#define ENGINE_MP_HF3_STD_80A		(float)(1/0.027936642)	//ENGINE_MP_HF3_STD_I(ENGINE_MP_HF3_STD_80A_GAIN)		/* in (bit/A) OBS!! measured value */
#define ENGINE_MP_HF3_STD_105A		ENGINE_MP_HF3_STD_I(ENGINE_MP_HF3_STD_105A_GAIN)	/* in (bit/A) */
#define ENGINE_MP_HF3_STD_130A		(float)(1/0.043314226)	//ENGINE_MP_HF3_STD_I(ENGINE_MP_HF3_STD_130A_GAIN)	/* in (bit/A) OBS!! measured value */

#define ENGINE_MP_HF3_STD_OFFSET -320.53												/* in (bit) */

/* Iset output standard scaling factors
 * Constants:
 *   D					Duty cycle in interval [0, 1]
 *   PWM_CYCLE_FULL		in (bit)
 *   U_Iset				in (V)
 *   IpwmSlope			in (A/bit)
 *   ISET_GAIN			in (A/V)
 * Calculations:
 *   G_amplifier = (47kΩ + 91kΩ)/91kΩ = 138/91
 *   U_Iset = D*3.3*G_amplifier*100kΩ/(100kΩ + 1kΩ) = PWM/PWM_CYCLE_FULL*3.3*138/91*100/101
 *   Ui_meas = I*ISET__GAIN
 *   U_Iset = Ui_meas <==> I*ISET__GAIN = PWM/PWM_CYCLE_FULL*3.3*138/91*100/101
 *   IpwmSlope = PWM/I = PWM_CYCLE_FULL/3.3/138*91/100*101*ISET__GAIN
 */
#define ENGINE_MP_HF3_STD_ISET(G) (float)(PWM_CYCLE_FULL/3.3/138*91/100*101*G)
#define ENGINE_MP_HF3_STD_ISET_80A ENGINE_MP_HF3_STD_ISET(ENGINE_MP_HF3_STD_80A_GAIN)
#define ENGINE_MP_HF3_STD_ISET_105A ENGINE_MP_HF3_STD_ISET(ENGINE_MP_HF3_STD_105A_GAIN)
#define ENGINE_MP_HF3_STD_ISET_130A ENGINE_MP_HF3_STD_ISET(ENGINE_MP_HF3_STD_130A_GAIN)

#define ENGINE_MP_HF3_STD_PWM_OFFSET 1509				/* in (bit) */

/* Temperature heat sink */
extern const Temperature_type Engine_MP_HF3_StdTempHeatsink_T70_75;
extern const Temperature_type Engine_MP_HF3_StdTempHeatsink_T75_80;

/* Temperature transformer */
extern const TempSenseDigital_enum Engine_MP_HF3_StdTempTrafo;

/* Battery signal logic level */
#define ENGINE_MP_HF3_STD_BIT_CURRENT(nominalCurrent) {-0.03*nominalCurrent, 0.6*nominalCurrent, 200}

#endif
