/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Regu/INIT.H
*
*	\ingroup	REGU
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	22-10-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef REGU_INIT_H_INCLUDED
#define REGU_INIT_H_INCLUDED

#include "tools.h"

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

/*
#define name@lwc_reset		dummy_reset
#define name@lwc_down		dummy_down
#define name@lwc_read		dummy_read
#define name@lwc_write		dummy_write
#define name@lwc_ctrl		dummy_ctrl
#define name@lwc_test		dummy_test
*/


typedef struct {						
	Uint8					nDummy;
} regu_Init;							

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_REGU
#elif defined(EXE_GEN_REGU)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_REGU
#endif

#define EXE_APPL(n)			n##regu

/********************************************************************/ EXE_BEGIN

/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN	/* Non-volatile numeric	*/
//#define EXE_USE_NS	/* Non-volatile string	*/
#define EXE_USE_VN		/* Volatile numeric		*/
//#define EXE_USE_VS	/* Volatile string		*/
#define EXE_USE_PN	/* Process-point numeric*/
//#define EXE_USE_PS	/* Process-point string	*/
//#define EXE_USE_BL	/* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name		Flags				Dim	Type	Def	Min	Max
            ---------	---------------		---	-----	---	---	----*/
EXE_REG_VN( Status,		SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFFUL )
EXE_REG_PN( StatusP,	SYS_REG_DEFAULT,	1,	Uint32,		0,	0xFFFFFFFFUL )
EXE_REG_VN( StatusSum,	SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFFUL )
EXE_REG_VN( Uset,		SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFFUL )
EXE_REG_VN( Iset,		SYS_REG_DEFAULT,	1,	Uint32, 0,	0,	0xFFFFFFFFUL )
EXE_REG_VN( Pset,		SYS_REG_DEFAULT,	1,	Uint32, 0,	0,	0xFFFFFFFFUL )
EXE_REG_VN( UactMode,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFFUL )
EXE_REG_VN( ReguMode,	SYS_REG_DEFAULT,	1,	Uint8,	1,	0,	0xFFUL )				// #JJ Regumode = off by default
EXE_REG_VN( ReguOffUactLow,	SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) // \  .
EXE_REG_VN( ReguOffUactHigh,SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) //  \ .
EXE_REG_VN( ReguOnUactLow,	SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) //  / Regulator on off interval.
EXE_REG_VN( ReguOnUactHigh,	SYS_REG_DEFAULT,    1,	Uint32,	0,	0,		0xFFFFFFFFUL ) // /  .

EXE_REG_PN( limiterGui,	SYS_REG_DEFAULT,	1,	Uint8, 		0,	0xFF )
EXE_REG_PN( ReguErrorSum,	SYS_REG_DEFAULT,	1,	Uint16, 		0,	0xFFFF )	// #JJ alarm view
EXE_REG_VN( NewSet,		SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFF )
EXE_REG_VN( StartFromSleep,		SYS_REG_DEFAULT,	3,	Uint32,	0,	0, 0xFFFFFFFFUL )	// Replaces REG Start
EXE_REG_NN( Start,		SYS_REG_DEFAULT,	3,	Uint32,	0,	0, 0xFFFFFFFFUL )			// Not used because 

EXE_REG_NN( IsetSlope,	SYS_REG_DEFAULT,	1,	Uint32,	1024,0,	0xFFFFFFFFUL )
EXE_REG_NN( IsetOffset,	SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFFUL )

EXE_REG_NN( PowerGroup,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFF )
EXE_REG_NN( IacLimit,	SYS_REG_DEFAULT,	1,	Int32,	16,	0,	999)
EXE_REG_NN( PacLimit,	SYS_REG_DEFAULT,	1,	Int32,	8000,	0,	99999 )
EXE_REG_NN( IdcLimit,	SYS_REG_DEFAULT,	1,	Int32,	0,	0,	999 )

EXE_REG_NN( PowerGroup_func,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	2 )
EXE_REG_NN( DplPowerLimitTotal,	SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	9999999 )
EXE_REG_NN( DplPriorityFactor,	SYS_REG_DEFAULT,	1,	Uint8,	1,	0,	2 )
EXE_REG_NN( DplPacLimit_default,	SYS_REG_DEFAULT,	1,	Uint8,	50,	0,	100 )
EXE_REG_PN( DplPacLimit,	SYS_REG_DEFAULT,	1,	Uint32,	0,	99999 )
EXE_REG_PN( DplId,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0xFF )

EXE_REG_VN( ReguErrorEvent,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFF )
EXE_REG_PN( ReguErrorCntP,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0xFF )
EXE_REG_PN( SyncP,		SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Sync (to display) from CAN master ext/BMS

/*******************************************************************************
 * HMI attributes
 */

EXE_MMI_NONE(Status)
EXE_MMI_NONE(StatusP)
EXE_MMI_NONE(StatusSum)

EXE_MMI_NONE(Uset)
EXE_MMI_NONE(Iset)
EXE_MMI_NONE(Pset)
EXE_MMI_NONE(UactMode)
EXE_MMI_NONE(ReguMode)
EXE_MMI_NONE(ReguOffUactLow)
EXE_MMI_NONE(ReguOffUactHigh)
EXE_MMI_NONE(ReguOnUactLow)
EXE_MMI_NONE(ReguOnUactHigh)

EXE_MMI_NONE(mode)
EXE_MMI_ENUM(limiterGui,	SYS_MMI_U_NONE, tLimiter,	SYS_MMI_READ, tLimiterEnum )
EXE_MMI_NONE(ReguErrorSum)
EXE_MMI_NONE(NewSet)
EXE_MMI_NONE(StartFromSleep)
EXE_MMI_NONE(Start)

EXE_MMI_NONE(IsetSlope)
EXE_MMI_NONE(IsetOffset)

//EXE_MMI_NONE(PowerGroup)
//EXE_MMI_NONE(IacLimit)
//EXE_MMI_NONE(PacLimit)
EXE_MMI_INT( PowerGroup, SYS_MMI_U_NONE,tPowerGroup,	SYS_MMI_RW, SYS_Q_NONE)
EXE_MMI_INT( IacLimit, SYS_MMI_U_NONE,	tIacLimit,	SYS_MMI_RW, SYS_Q_NONE)
EXE_MMI_INT( PacLimit, SYS_MMI_U_NONE,	tPacLimit,	SYS_MMI_RW, SYS_Q_NONE)
EXE_MMI_INT( IdcLimit, SYS_MMI_U_NONE,	tIdcLimit,	SYS_MMI_RW, SYS_Q_NONE)

EXE_MMI_ENUM( PowerGroup_func,	SYS_MMI_U_NONE,	tFunction,	SYS_MMI_RW, tDplPwrGrpEnum)
EXE_MMI_INT( DplPowerLimitTotal, SYS_MMI_U_NONE,	tDplPwrLimTot,	SYS_MMI_RW, SYS_Q_NONE)
EXE_MMI_ENUM( DplPriorityFactor,	SYS_MMI_U_NONE,	tDplPrio,	SYS_MMI_RW, tDplPrioEnum)
EXE_MMI_INT( DplPacLimit_default, SYS_MMI_U_NONE,	tDplPacLimitDef,	SYS_MMI_RW, SYS_Q_NONE)
EXE_MMI_INT( DplPacLimit, SYS_MMI_U_NONE,	tDplPacLimit,	SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_INT( DplId, SYS_MMI_U_NONE,	tDplId,	SYS_MMI_READ, SYS_Q_NONE)

EXE_MMI_NONE(ReguErrorEvent)
EXE_MMI_NONE(ReguErrorCntP)
EXE_MMI_ENUM( SyncP,	SYS_MMI_U_NONE, tSyncP,		SYS_MMI_READ, 	tNoYes)
/*******************************************************************************
 * References to external registers
 */
EXE_REG_EX( regu__CcStatus,			Status,						cc1 )
EXE_REG_EX( regu__ParallelControl,	ParallelControl_func,		cc1	)
EXE_REG_EX( regu__exUregu,		Uregu,		meas1 )
EXE_REG_EX( regu__exIregu,		Iregu,		meas1 )
EXE_REG_EX( regu__exPregu,		Pregu,		meas1 )
EXE_REG_EX( regu__Uset,			Uset,				regu1 )
EXE_REG_EX( regu__Iset,			Iset,				regu1 )
EXE_REG_EX( regu__Pset,			Pset,				regu1 )
EXE_REG_EX( regu__ReguMode,		ReguMode,			regu1 )
EXE_REG_EX( regu__UactMode,		UactMode,			regu1 )
EXE_REG_EX( regu__OffUactLow,	ReguOffUactLow,		regu1 )
EXE_REG_EX( regu__OffUactHigh,	ReguOffUactHigh,	regu1 )
EXE_REG_EX( regu__OnUactLow,	ReguOnUactLow,		regu1 )
EXE_REG_EX( regu__OnUactHigh,	ReguOnUactHigh,		regu1 )
EXE_REG_EX( regu__exTboard,		Tboard,		meas1 )
EXE_REG_EX( regu__exThs,		ThsRegu,	meas1 )
EXE_REG_EX( regu__exMains,		mains,		meas1 )
EXE_REG_EX( regu__nwkStatus,	nwkStatus,	radio1 )
EXE_REG_EX( regu__PanId,		PanId,		radio1 )
EXE_REG_EX( regu__Channel,		Channel,	radio1 )
EXE_REG_EX( regu__NodeId,		NodeId,		radio1 )
EXE_REG_EX( regu__BmSOC,		BmSOC,		radio1 )
EXE_REG_EX( regu__BmInit,		BmInit,		radio1 )
EXE_REG_EX( regu__BmCalib,		BmCalib,	radio1 )
EXE_REG_EX( regu__radioAck,		radioAck,	radio1 )
EXE_REG_EX( regu__exNewCharge,	newCharge,	sup1 )
EXE_REG_EX( regu__Overtemp,		Overtemp,	meas1 )
EXE_REG_EX( regu__IadRegu,		IadRegu,	meas1 )
EXE_REG_EX( regu__LocalTime,	LocalTime,	sup1 )
EXE_REG_EX( regu__Start,		StartFromSleep,		regu1 )
EXE_REG_EX( regu__IdcLimit,		IdcLimit,	regu1 )
EXE_REG_EX( regu__EngineCode,	EngineCode,	sup1 )
EXE_REG_EX( regu__currEngineCode,	currEngineCode,	sup1 )
EXE_REG_EX( regu__engineIdx_default,	engineIdx_default,	sup1 )
EXE_REG_EX( regu__supState,	supervisionState,	sup1 )
EXE_REG_EX( regu__ChalgStatus,	ChalgStatus,	chalg1 )
EXE_REG_EX( regu__ChalgError,	ChalgError,	chalg1 )
EXE_REG_EX( regu__ChargingMode,	ChargingMode,	chalg1 )
EXE_REG_EX( Regu_CAN_CommProfile,	CAN_CommProfile,	can1 )
EXE_REG_EX( Regu_CAN_State,		State,		can1 )
EXE_REG_EX( regu__CanReguOffUactLow,		CanReguOffUactLow,	can1 )
EXE_REG_EX( regu__CanReguOffUactHigh,	CanReguOffUactHigh,	can1 )
EXE_REG_EX( regu__CanReguOnUactLow,		CanReguOnUactLow,	can1 )
EXE_REG_EX( regu__CanReguOnUactHigh,		CanReguOnUactHigh,	can1 )
EXE_REG_EX( regu__UadSlope,		UadSlope,	meas1 )
EXE_REG_EX( regu__IadSlope,		IadSlope,	meas1 )
EXE_REG_EX( regu__IpwmSlope,	IpwmSlope,	meas1 )
EXE_REG_EX( regu__IpwmOffset,	IpwmOffset,	meas1 )
EXE_REG_EX( regu__Umeas1s,		Umeas1s,	meas1 )
EXE_REG_EX( regu__Smeas1s,		Smeas1s,	meas1 )
EXE_REG_EX( regu__HwEngineOff,	HwEngineOff,	meas1 )
EXE_REG_EX( regu__UmeasSum_Float,	UmeasSum_Float,	chalg1 )
EXE_REG_EX( regu__IoffsetCalib,	IoffsetCalib,	meas1 )
EXE_REG_EX( regu__statusFlags,	statusFlags,	sup1 )
EXE_REG_EX( regu__batteryInfo6,	batteryInfo6,	chalg1 )
EXE_REG_EX( regu__batteryInfo12,	batteryInfo12,	chalg1 )
EXE_REG_EX( regu__batteryInfo18,	batteryInfo18,	chalg1 )
EXE_REG_EX( regu__batteryInfo24,	batteryInfo24,	chalg1 )
EXE_REG_EX( regu__batteryInfo40,	batteryInfo40,	chalg1 )
EXE_REG_EX( regu__BitConfig,		BitConfig,		chalg1 )
EXE_REG_EX( regu__BatteryDcon,		BatteryDcon,	meas1 )
EXE_REG_EX( regu__PowerGroup,		PowerGroup,		regu1 )
EXE_REG_EX( regu__PowerGroup_func,	PowerGroup_func,		regu1 )
EXE_REG_EX( regu__DplPowerLimitTotal,	DplPowerLimitTotal,		regu1 )
EXE_REG_EX( regu__DplPriorityFactor,	DplPriorityFactor,	regu1 )
EXE_REG_EX( regu__DplPacLimit_default,	DplPacLimit_default,	regu1 )
EXE_REG_EX( regu__PacLimit,		PacLimit,		regu1 )
EXE_REG_EX( regu__ChalgSoc,		chalgSoc,		chalg1 )
EXE_REG_EX( regu__dplMasterNwkAddr,		dplMasterNwkAddr,	radio1 )
EXE_REG_EX( regu__BBCError,		BBCError,	chalg1 )

/******************************************************************************* 
 * read/write indications
 * (sorted alphabetically by instance name and by declaration order)
 */
EXE_IND_EX( radioAck,			radioAck,	radio1 )
EXE_IND_ME( StatusP,			StatusP )
EXE_IND_ME( Uset,				Uset )
EXE_IND_ME( Iset,				Iset )
EXE_IND_ME( Pset,				Pset )
EXE_IND_ME( UactMode,			UactMode )
EXE_IND_ME( ReguMode,			ReguMode )
EXE_IND_ME( ReguOffUactLow,		ReguOffUactLow )
EXE_IND_ME( ReguOffUactHigh,	ReguOffUactHigh )
EXE_IND_ME( ReguOnUactLow,		ReguOnUactLow )
EXE_IND_ME( ReguOnUactHigh,		ReguOnUactHigh )
EXE_IND_ME( limiterGui,			limiterGui )
EXE_IND_ME( ReguErrorSum,		ReguErrorSum )
EXE_IND_ME( NewSet,				NewSet )
EXE_IND_ME( PowerGroup,			PowerGroup )
EXE_IND_ME( PacLimit,			PacLimit )
EXE_IND_ME( PowerGroup_func,	PowerGroup_func )
EXE_IND_ME( DplPowerLimitTotal,	DplPowerLimitTotal )
EXE_IND_ME( DplPriorityFactor,	DplPriorityFactor )
EXE_IND_ME( DplPacLimit_default,	DplPacLimit_default )
EXE_IND_ME( DplPacLimit,		DplPacLimit )
EXE_IND_ME( DplId,				DplId )
EXE_IND_ME( ReguErrorCntP,		ReguErrorCntP )
EXE_IND_ME( SyncP,				SyncP )
EXE_IND_EX( reset,				reset,		sup1 )
EXE_IND_EX( newCharge,			newCharge,	sup1 )

/******************************************************************************* 
 * Language-dependent texts 
 *  - tExampleTxtHandle1 is reserved handlename and it's discarded by 
 *		textparser tools.
 *  - you can add txthandle comment at the end of the texthandle line, and this
 *		is parsed by textparser tools.
 */
EXE_TXT( tNoYes, EXE_T_EN("No\nYes") EXE_T_ES("No\nYes") EXE_T_EN_US("No\nYes") EXE_T_PT("No\nYes") EXE_T_DE("Nein\nJa") EXE_T_JP("") EXE_T_SE("No\nYes") EXE_T_IT("No\nSi") EXE_T_FI("") "") /*Menu:Service->CAN->Network info (10)*/

EXE_TXT( tLimiter, EXE_T_EN("Limiter") EXE_T_ES("Limitador") EXE_T_EN_US("Limiter") EXE_T_PT("limitador") EXE_T_DE("Begrenzer") EXE_T_FI("") EXE_T_SE("Begr\344nsning") EXE_T_IT("Limitat.") EXE_T_JP("") "") /*Limiting factor (21-1-tLimiterEnum)*/
//typedef enum {LimitNone, LimitUset, LimitIset, LimitPset, LimitT, LimitUengine, LimitIengine, LimitPengine, LimitIdc, LimitIac, LimitPac, LimitPhase} limiter_type;
EXE_TXT( tLimiterEnum, EXE_T_EN("None\nUset\nIset\nPset\nCharger temp\nUmaxEngine\nImaxEngine\nPmaxEngine\nIdcLimit\nIacLimit\nPacLimit\n-") EXE_T_ES("None\nUset\nIset\nPset\nCharger temp\nUmaxEngine\nImaxEngine\nPmaxEngine\nIdcLimit\nIacLimit\nPacLimit\n-") EXE_T_EN_US("None\nUset\nIset\nPset\nCharger temp\nUmaxEngine\nImaxEngine\nPmaxEngine\nIdcLimit\nIacLimit\nPacLimit\n-") EXE_T_PT("None\nUset\nIset\nPset\nCharger temp\nUmaxEngine\nImaxEngine\nPmaxEngine\nIdcLimit\nIacLimit\nPacLimit\n-") EXE_T_DE("Keine\nUnter\nSetzt\nPset\nGer\344tetemp.\nUmaxEngine\nImaxEngine\nPmaxEngine\nIdcLimit\nIacLimit\nPacLimit\n-") EXE_T_FI("") EXE_T_SE("None\nUset\nIset\nPset\nCharger temp\nUmaxEngine\nImaxEngine\nPmaxEngine\nIdcLimit\nIacLimit\nPacLimit\n-") EXE_T_IT("Nessuno\nU\050V\051Set\nISet\nPset\nTemp.caricab.\nU\050V\051MaxEngine\nIMaxEngine\nPmaxEngine\nIDC Limite\nIACLimite\nPacLimite\n-") EXE_T_JP("") "") /*Limiting factor value strings (21-1-tLimiter)*/
EXE_TXT( tIdcLimit, EXE_T_EN("Idc max\050A\051") EXE_T_ES("Idc M\341ximo\050A\051") EXE_T_EN_US("Idc max\050A\051") EXE_T_PT("Idc max \050A\051") EXE_T_DE("Idc max\050A\051") EXE_T_FI("") EXE_T_SE("Idc max\050A\051") EXE_T_IT("Idc max\050A\051") EXE_T_JP("") "") /*Menu:Service->Factory settings->Idc max limit (17)*/
EXE_TXT( tIacLimit, EXE_T_EN("Iac max\050A\051") EXE_T_ES("Iac M\341ximo\050A\051") EXE_T_EN_US("Iac max\050A\051") EXE_T_PT("Iac max \050A\051") EXE_T_DE("Iac max\050A\051") EXE_T_FI("") EXE_T_SE("Iac max\050A\051") EXE_T_IT("Iac max\050A\051") EXE_T_JP("") "") /*Menu:Service->Factory settings->Iac max limit (17)*/
EXE_TXT( tPacLimit, EXE_T_EN("Pac max\050VA\051") EXE_T_ES("Pac M\341ximo\050VA\051") EXE_T_EN_US("Pac max\050VA\051") EXE_T_PT("Pac max \050VA\051") EXE_T_DE("Pac max\050VA\051") EXE_T_FI("") EXE_T_SE("Pac max\050VA\051") EXE_T_IT("Pac max\050VA\051") EXE_T_JP("") "") /*Menu:Service->Factory settings->Pac max limit (15)*/
EXE_TXT( tPowerGroup, EXE_T_EN("Power group") EXE_T_ES("Grupo Potenciador") EXE_T_EN_US("Power group") EXE_T_PT("Grupo de energia") EXE_T_DE("Leistungsgruppe") EXE_T_FI("") EXE_T_SE("Effektgrupp") EXE_T_IT("Potenza gruppo") EXE_T_JP("") "") /*Menu:Service->Advanced->Power group (17)*/

EXE_TXT( tFunction,	EXE_T_EN("Function") EXE_T_ES("Funci\363n") EXE_T_EN_US("Function") EXE_T_PT("Fun\347\343o") EXE_T_DE("Funktion") EXE_T_JP("") EXE_T_SE("Funktion") EXE_T_IT("Funzione") EXE_T_FI("") "") /*Menu:Service->Power Limit->Function (21-1-tDplPwrGrpEnum)*/
EXE_TXT( tDplPwrGrpEnum,	EXE_T_EN("Disabled\nMaster\nSlave") EXE_T_ES("Inactivo\nPrincipal\nEsclavo") EXE_T_EN_US("Disabled\nMaster\nSlave") EXE_T_PT("Desabilitado\nPrincipal\nAuxiliar") EXE_T_DE("Deaktiviert\nMaster\nSlave") EXE_T_JP("") EXE_T_SE("Av\nMaster\nSlave") EXE_T_IT("Disattivato\nMaster\nSlave") EXE_T_FI("") "") /*Menu:Service->Power Limit->Function (21-1-tFunction)*/
EXE_TXT( tDplPwrLimTot, EXE_T_EN("Pac tot\050VA\051") EXE_T_ES("Pac tot\050VA\051") EXE_T_EN_US("Pac tot\050VA\051") EXE_T_PT("Pac tot\050VA\051") EXE_T_DE("Pac tot\050VA\051") EXE_T_FI("") EXE_T_SE("Pac tot\050VA\051") EXE_T_IT("Pac tot\050VA\051") EXE_T_JP("") "") /*Menu:Service->Power Limit->Pac tot(W) (14)*/
EXE_TXT( tDplPrio, EXE_T_EN("Priority") EXE_T_ES("Prioridad") EXE_T_EN_US("Priority") EXE_T_PT("Prioridade") EXE_T_DE("Priorit\344t") EXE_T_FI("") EXE_T_SE("Prioritet") EXE_T_IT("Priorit\341") EXE_T_JP("") "") /*Menu:Service->Power Limit->Prio (16)*/
EXE_TXT( tDplPrioEnum,	EXE_T_EN("Low\nNormal\nHigh") EXE_T_ES("Bajo\nNormal\nAlto") EXE_T_EN_US("Low\nNormal\nHigh") EXE_T_PT("Baixo\nNormal\nAlto") EXE_T_DE("Niedrig\nNormal\nHoch") EXE_T_JP("") EXE_T_SE("L\345g\nNormal\nH\366g") EXE_T_IT("Bassa\nNormale\nAlta") EXE_T_FI("") "") /*Menu:Service->Power Limit->Prio (21-1-tDplPrio)*/
EXE_TXT( tDplPacLimitDef, EXE_T_EN("Pac def\050%\051") EXE_T_ES("Pac def\050%\051") EXE_T_EN_US("Pac def\050%\051") EXE_T_PT("Pac def\050%\051") EXE_T_DE("Pac def\050%\051") EXE_T_FI("") EXE_T_SE("Pac def\050%\051") EXE_T_IT("Pac def\050%\051") EXE_T_JP("") "") /*Menu:Service->Power Limit->Pac default(W)  (15)*/
EXE_TXT( tDplPacLimit, EXE_T_EN("Pac lim\050VA\051") EXE_T_ES("Pac lim\050VA\051") EXE_T_EN_US("Pac lim\050VA\051") EXE_T_PT("Pac lim\050VA\051") EXE_T_DE("Pac lim\050VA\051") EXE_T_FI("") EXE_T_SE("Pac lim\050VA\051") EXE_T_IT("Pac lim\050VA\051") EXE_T_JP("") "") /*Menu:Service->Power Limit->Pac lim(W) (14)*/
EXE_TXT( tDplId, EXE_T_EN("DPL ID") EXE_T_ES("DPL ID") EXE_T_EN_US("DPL ID") EXE_T_PT("DPL ID") EXE_T_DE("DPL ID") EXE_T_FI("") EXE_T_SE("DPL ID") EXE_T_IT("DPL ID") EXE_T_JP("") "") /*Menu:Service->Power Limit->DPL ID (15)*/

EXE_TXT( tSyncP,	EXE_T_EN("Sync") EXE_T_ES("Sync") EXE_T_EN_US("Sync") EXE_T_PT("Sync") EXE_T_DE("Sync") EXE_T_JP("") EXE_T_SE("Sync") EXE_T_IT("Sync") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (17)*/
/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
