#ifndef PAUSE_H
#define PAUSE_H

void updatePause();					// Update pause so that both pause states are the same
int getPause();						// Return current pause state

/* There are two sources for pause, the button on this display or a button sent via CAN from another display. The sources may be in
 * four different combinations and may have been in four different combinations the last time, in total 16 different combinations.
 */

#endif /* PAUSE_H */
