#include "regu_IO.h"
#include "regu.h"

#include "global.h"
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"

#include "math.h"
#include "chalg.h"
#include "meas.h"
#include "../Meas/local.h"
#include "ControlLoop/Regulator/regu_StatusAndError.h"
#include "pause.h"
#include "sup.h"
#include "can.h"
#include "ControlLoop/sendToBattery.h"
#include "dpl.h"

static void getInputs(ChargerSet_type* Input, const sys_FBInstId instId);
static void getBattDetect(Interval_type* UactOff, Interval_type* UactOn, ChargerDerate_type* Derate, const sys_FBInstId instId);
static int getBatteryCommunication();
static int getOvertemp();
static int getQuietDerate();
static void regu_GetLimit(regu__Inst* const pInst, struct regu_Limiter_Type* Limiter, const ControlLoopMeas_type* In);
static int getRemoteRestriction();
static Uint32 getReguOffUactHigh(const sys_FBInstId instId);
static void GetCurrentSamples(int16_t* currents);
static int OutsideSpecificationStateMachine(regu__Inst* const pInst, ControlLoopMeas_type* const meas);

static int getBatteryCommunication(){
	Uint8 SupState;
	Uint8 CAN_CommProfile;
	Uint8 ParallelControl;
	Uint8 ChargingMode;
	int batteryCommunication = 0;
	extern volatile batteryCommunication_type dataToBattery;

	reg_get(&SupState, regu__supState);
	reg_get(&CAN_CommProfile, Regu_CAN_CommProfile);

	switch(CAN_CommProfile){
	case CAN_COMM_PROFILE_DISABLED :							// CAN disabled
	case CAN_COMM_PROFILE_MASTER :								// Communication profile master
	case CAN_COMM_PROFILE_STATUS :								// Communication profile status
		switch(SupState){
		case SUP_STATE_INIT_BM:
			reg_get(&ParallelControl, regu__ParallelControl);
			if(ParallelControl && (CAN_CommProfile == CAN_COMM_PROFILE_MASTER)){
				batteryCommunication = dataToBattery.Valid;
			}
			else{
				reg_get(&ChargingMode, regu__ChargingMode);
				if(ChargingMode == CHALG_DUAL || ChargingMode == CHALG_MULTI)
					batteryCommunication = 4;
				else
					batteryCommunication = 1;
			}
			break;
		case SUP_STATE_INIT_BM_2:
			batteryCommunication = 2;
			break;
		case SUP_STATE_INIT_BM_4:
			batteryCommunication = 3;
			break;
		case SUP_STAT_VSENSE:
			batteryCommunication = 4;
			break;
		default:
			batteryCommunication = 0;
			break;
		}
		break;
	case CAN_COMM_PROFILE_SLAVE :								// Communication profile slave
		batteryCommunication = dataToBattery.Valid;
		break;
	case CAN_COMM_PROFILE_MASTER_CAN_INPUT :					// Communication profile master external IO
		batteryCommunication = 0;
		break;
	}															// No default give compilation warning in case of missing enumaration value

	return batteryCommunication;
}

static int getOvertemp(){
	Uint8 Overtemp;

	reg_get(&Overtemp, regu__Overtemp);
	return !Overtemp;	// Overtemp if 0
}

static int getQuietDerate(){
	int quietDerate;
	Uint8 bitConfig;

	reg_get(&bitConfig, regu__BitConfig);

	quietDerate = (int)(bitConfig & CHALG_BITCFG_QUIET_DERATE);

	return quietDerate;
}

static int getFirmwareError(){
	Uint16 supStatusFlags;
	reg_get(&supStatusFlags, regu__statusFlags);															// Read Firmware malfunction flag
	return (int) supStatusFlags & SUP_STATUS_CORRUPTED;
}

static void getBattDetect(Interval_type* UactOff, Interval_type* UactOn, ChargerDerate_type* Derate, const sys_FBInstId instId)
{
	/*
	 * Set hardcoded battery detect values instead of values defined in charging algorithm
	 * Use battery detect levels from charging algorithm for error handling to avoid gap in
	 * error indication from charging algorithm
	 */
	Uint8 SupState;

	reg_get(&SupState, regu__supState);

	if(SupState >= SUP_STATE_CHARGING)
	{
		static int lowVoltageFlag = 0;
		static int lowVoltageTimer = 0;
		static int highVoltageFlag = 0;
		static int highVoltageTimer = 0;
		float BatteryVoltage;
		float UactOffLow;
		float UactOffHigh;
		int deltaVoltageOff;

		reg_get(&BatteryVoltage, regu__UmeasSum_Float);
		reg_getLocal(&UactOffLow, reg_i_ReguOffUactLow, instId);										// Battery detect off low level
		reg_getLocal(&UactOffHigh, reg_i_ReguOffUactHigh, instId);									// Battery detect off high level
		deltaVoltageOff = meas_GetDeltaVoltageOff();												// Delta voltage off flag trigged

		if(BatteryVoltage < UactOffLow){
			if(lowVoltageFlag == FALSE){
				lowVoltageFlag = TRUE;
				lowVoltageTimer = 4;
			}
			else{
				if(lowVoltageTimer){
					lowVoltageTimer--;
				}
				else if(!deltaVoltageOff){
					Derate->Detail.LowVoltage = 1;
				}
			}
		}
		else{
			lowVoltageFlag = 0;
			lowVoltageTimer = 0;
		}

		if(BatteryVoltage > UactOffHigh){
			if(highVoltageFlag == FALSE){
				highVoltageFlag = TRUE;
				highVoltageTimer = 4;
			}
			else{
				if(highVoltageTimer){
					highVoltageTimer--;
				}
				else if(!deltaVoltageOff){
					Derate->Detail.HighVoltage = 1;
				}
			}
		}
		else{
			highVoltageFlag = 0;
			highVoltageTimer = 0;
		}
	}

#if 0
	{
		if(engineGetEngineType() == EngineType_PF_HF2){									// Access/Lion 30 engines
			const Engine_type* const engineParameters = engineGetParameters();			// Get engine parameters

			if(engineParameters->Code == 209){											// ENGINE_ROBUST_48_60 (7 in Robust program)
				UactOff->Low = 14.0;
				UactOn->Low = 15.0;
			}
			else{
				UactOff->Low = 7.0;
				UactOn->Low = 8.0;
			}
		}
		else{
			UactOff->Low = 5.0;
			UactOn->Low = 6.0;
		}

		UactOff->High = INFINITY;
		UactOn->High = INFINITY;
	}
#endif
	regu_getUactOnOffLimits(UactOff, UactOn);										// Get limits for battery detection

	/* Put modified values to new REG register to be sent from master to slave */
	reg_put(&UactOn->Low, regu__CanReguOnUactLow);
	reg_put(&UactOn->High, regu__CanReguOnUactHigh);
	reg_put(&UactOff->Low, regu__CanReguOffUactLow);									// Battery detect off low level
	reg_put(&UactOff->High, regu__CanReguOffUactHigh);									// Battery detect off high level
}

void regu_GetLimit(regu__Inst* const pInst, struct regu_Limiter_Type* Limiter, const ControlLoopMeas_type* In)
{
	const Engine_type* const engineParameters = engineGetParameters();								// Get engine parameters
	Uint32 Imax;
	Uint8 powerGroupFunc;
	Uint8 parallelControl;

	Limiter->Umax = Volt2ADC((Uint32)(engineParameters->UIchar_p[engineParameters->UIcharLength - 1].Volt*MEAS__10MS_AVG_VALUE_CNT));
	Limiter->ImaxPower = Ampere2ADC(engineCurrentMax(((float)In->Umeas1s)/1000));

	reg_getLocal(&Limiter->IacLimit, reg_i_IacLimit, pInst->instId);
	Limiter->ConversionfactorPhaseCurrentToDc = engineParameters->ConversionfactorPhaseCurrentToDc;

	powerGroupFunc = dplGetPowerGroupFunc();
	reg_get(&parallelControl, regu__ParallelControl);

	if(powerGroupFunc == DplDisabled || parallelControl){
		reg_getLocal(&Limiter->PacLimit, reg_i_PacLimit, pInst->instId);
	}
	else{
		Limiter->PacLimit = (Int32)dplGetDplPacLimit();
	}
	Limiter->ConversionfactorApparentPower = regu_IoGetConversionfactorApparentPower();

	{
		extern int gLowCurrentMode;

		if(gLowCurrentMode == TRUE){
			Limiter->ImaxDc = Ampere2ADC(0.5);												// Limit current to 500mA
		}
		else{
			reg_getLocal(&Imax, reg_i_IdcLimit, pInst->instId);
			Limiter->ImaxDc = Ampere2ADC((float)Imax);										// Normal current limit
		}
	}
}

static int getRemoteRestriction()
{
	extern int gRemoteRestricted;
	return gRemoteRestricted;
}

static Uint32 getReguOffUactHigh(const sys_FBInstId instId)
{
	Interval_type UactOff;
	Uint32 UactOffHigh;

	reg_getLocal(&UactOff.High, reg_i_ReguOffUactHigh, instId);											// Battery detect off high level

	UactOffHigh = Volt2ADC(UactOff.High);																// Convert to ADC value

	return UactOffHigh;
}

void writeOutputToReg(regu__Inst* pInst, ChargerStatusFields_type Status){
	{
		//	#JJ slave alarm
		ChargerStatusFields_type CcStatus;
		reg_get(&CcStatus, regu__CcStatus);
		Status.Detail.Warning.Sum |= CcStatus.Detail.Warning.Sum;														// Add cluster control error and warning sum
		Status.Detail.Error.Sum |= CcStatus.Detail.Error.Sum;

		Uint8 CommProfile;
		static int old = -1;

		reg_get(&CommProfile, Regu_CAN_CommProfile);
		if(CommProfile != old){
			ChargerSet_type input = {{NAN, NAN, NAN}, ReguMode_Off, {.both=0}, {.both=0}};

			old = CommProfile;
			regu_setInput(&input);																		// Set values to remove timeout
			Status.Detail.Error.Detail.Watchdog = 0;
		}
		else{
			Uint8 state;
			static int timer = 0;

			reg_get(&state, Regu_CAN_State);
			if(state == 3){
				if(timer){
					Status.Detail.Error.Detail.Watchdog = 0;
					timer--;
				}
			}
			else{
				timer = 150;

				if(CommProfile == CAN_COMM_PROFILE_SLAVE || CommProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT){
					Status.Detail.Error.Detail.Watchdog = 0;
				}
			}
		}
	}

	{
		static ChargerStatusFields_type StatusOld = {.Sum = 0};

		if(StatusOld.Sum != Status.Sum){

			if(StatusOld.Detail.Error.Sum != Status.Detail.Error.Sum || StatusOld.Detail.Warning.Sum != Status.Detail.Warning.Sum){												// Trigger event if error status has changed
				reg_putLocal(&Status.Sum, reg_i_Status, pInst->instId);
			}

			{																							// Sum all statuses during this charge
				ChargerStatusFields_type StatusSum;

				reg_getLocal(&StatusSum.Sum, reg_i_StatusSum, pInst->instId);
				StatusSum.Sum |= Status.Sum;
				reg_putLocal(&StatusSum.Sum, reg_i_StatusSum, pInst->instId);
			}
			StatusOld = Status;
		}
	}

	pInst->Status = Status;																				// Update REGU FB public status
}

int OutsideSpecificationStateMachine(regu__Inst* const pInst, ControlLoopMeas_type* const meas)
{
	int outside = 1; //assume outside

	switch (pInst->outsideSpecState)
	{
		case WITHIN_SPEC_STATE:
			if (VoltageAboveEngineMax(AdValueToVolt(MEAS__10MS_AVG_VALUE_CNT, meas->Regulator.Uact)) != 0U)  //High voltage
			{
				pInst->outsideSpecState = OUTSIDE_SPEC_STATE;
			}
			else
			{
				outside = 0;
			}
			break;

		case OUTSIDE_SPEC_STATE:
			if (VoltageAboveEngineMax(AdValueToVolt(MEAS__10MS_AVG_VALUE_CNT, meas->Regulator.Uact)) == 0U)  //Voltage within specification
			{
				pInst->outsideSpecState = WAIT_SPEC_STATE;
				pInst->outsideSpecDelayCnt = 0U;
			}
			break;

		case WAIT_SPEC_STATE:
			if (VoltageAboveEngineMax(AdValueToVolt(MEAS__10MS_AVG_VALUE_CNT, meas->Regulator.Uact)) != 0U)  //High voltage
			{
				pInst->outsideSpecState = OUTSIDE_SPEC_STATE;
			}
			else
			{
				if (pInst->outsideSpecDelayCnt > SPEC_STATE_DELAY_TIME)
				{
					pInst->outsideSpecState = WITHIN_SPEC_STATE;
				}
				++(pInst->outsideSpecDelayCnt);
			}
			break;

		default:
			if (VoltageAboveEngineMax(AdValueToVolt(MEAS__10MS_AVG_VALUE_CNT, meas->Regulator.Uact)) != 0U)  //High voltage
			{
				pInst->outsideSpecState = OUTSIDE_SPEC_STATE;
			}
			else
			{
				pInst->outsideSpecState = WITHIN_SPEC_STATE;
			}
			break;
	}
	return outside;
}

void regu_IoGetMeas(regu__Inst* pInst, ControlLoopMeas_type* meas){
	reg_get(&meas->Regulator.Uact, regu__exUregu);														// Measured voltage
	reg_get(&meas->Regulator.Iact, regu__exIregu);														// Measured current
	reg_get(&meas->Regulator.Pact, regu__exPregu);														// Measured power
	reg_get(&meas->Regulator.Tact, regu__exThs);														// Measured heat sink temperature
	reg_get(&meas->Umeas1s, regu__Umeas1s);

	meas->Pause = getPause();																			// Detect if pause is active
	meas->RemoteRestriction = getRemoteRestriction();													// Detect if remote restriction. Charging restricted if TRUE
	meas->BatteryCommunication = getBatteryCommunication();												// Detect if battery communication should be active
	meas->EngineOutsideSpecification = OutsideSpecificationStateMachine(pInst, meas);        			//State machine to prevent running outside specification
	reg_get(&meas->HwEngineOff, regu__HwEngineOff);
	GetCurrentSamples(meas->RegulatorStatus.currentSamples);											// Current samples
	meas->RegulatorStatus.overTemperature = getOvertemp();												// Read over temperature
	meas->RegulatorStatus.quietDerate = getQuietDerate();												// Get quiet derate configuration
	meas->RegulatorStatus.WatchDog = pInst->ReguWatchDog;												// Set pointer to watch dog counters
	meas->RegulatorStatus.Restarts = pInst->Restarts;													// Write number of restarts
	meas->RegulatorStatus.RestartDelay = pInst->RestartDelay;											// Get restart delay counter
	meas->RegulatorStatus.regulatorErrorCnt = pInst->RegulatorErrorCnt;									// Get regulator error counter
	const Engine_type* const engineParameters = engineGetParameters();									// Get engine parameters
	meas->RegulatorStatus.enginePwmOffset = (Uint16)engineParameters->ScalefactorNom.NormToBit.IpwmOffset;
	meas->RegulatorStatus.phaseError = meas_getPhaseError();											// Get phase error from MEAS
	regu_GetLimit(pInst, &meas->Limiter, meas);															// Read limits
	meas->ReguOffUactHigh = getReguOffUactHigh(pInst->instId);											// Get level for high battery voltage detection
	reg_get(&meas->BatteryDeltaDcon, regu__BatteryDcon);												// Get flag for battery disconnection due to delta voltage
	meas->FirmwareError = getFirmwareError();															// Get flag for firmware malfunction
}

void regu_IoGetControlLoopInputs(ChargerSet_type* Input, Interval_type* UactOff, Interval_type* UactOn, ChargerDerate_type* Derate, const sys_FBInstId instId){
	getInputs(Input, instId);
	getBattDetect(UactOff, UactOn, Derate, instId);
}

float regu_IoGetConversionfactorApparentPower(void)
{
	float conversionfactor;
	const Engine_type* const engineParameters = engineGetParameters();								// Get engine parameters

	conversionfactor = engineParameters->ConversionfactorPowerInToDc * ENGINE_MP_HF1_STANDARD_POWERFACTOR;

	return conversionfactor;
}

static void getInputs(ChargerSet_type* Input, const sys_FBInstId instId)
{
	reg_getLocal(&Input->Set.U, reg_i_Uset, instId);
	reg_getLocal(&Input->Set.I, reg_i_Iset, instId);
	reg_getLocal(&Input->Set.P, reg_i_Pset, instId);

	reg_getLocal(&Input->Mode, reg_i_ReguMode, instId);
	reg_getLocal(&Input->Uact.both, reg_i_UactMode, instId);
}

static void GetCurrentSamples(int16_t* currents){
	int n;

	for (n = 0; n < REGU_STATUS_AND_ERRROR_CURRENTSAMPLES_LEN; n++)
	{
		reg_aGet(&currents[n], regu__IadRegu, n);
	}
}

void regu_setInput(ChargerSet_type* input){
	reg_put(&input->Set.U, regu__Uset);							// Write voltage set value so that regulator may access it
	reg_put(&input->Set.I, regu__Iset);							// Write current set value so that regulator may access it
	reg_put(&input->Set.P, regu__Pset);							// Write power set value so that regulator may access it

	reg_put(&input->Mode, regu__ReguMode);						// Write regulator mode so that regulator may access it
	reg_put(&input->Uact, regu__UactMode);						// Write battery detection via voltage selection so that battery detection function may access it
}

void regu_setUactDisconnect(Interval_type* interval){
	reg_put(&interval->Low, regu__OffUactLow);					// Write disconnect interval low limit
	reg_put(&interval->High, regu__OffUactHigh);				// Write disconnect interval high limit
}

void regu_setUactConnect(Interval_type* interval){
	reg_put(&interval->Low, regu__OnUactLow);					// Write connect interval low limit
	reg_put(&interval->High, regu__OnUactHigh);					// Write connect interval high limit
}

void regu_getUactOnOffLimits(Interval_type* UactOff, Interval_type* UactOn){
	if(engineGetEngineType() == EngineType_PF_HF2){									// Access/Lion 30 engines
		const Engine_type* const engineParameters = engineGetParameters();			// Get engine parameters

		if(engineParameters->Code == 209){											// ENGINE_ROBUST_48_60 (0x07 in Robust program)
			UactOff->Low = 14.0;
			UactOn->Low = 15.0;
		}
		else{
			UactOff->Low = 7.0;
			UactOn->Low = 8.0;
		}
	}
	else{
		UactOff->Low = 5.0;
		UactOn->Low = 6.0;
	}

	UactOff->High = INFINITY;
	UactOn->High = INFINITY;
}
