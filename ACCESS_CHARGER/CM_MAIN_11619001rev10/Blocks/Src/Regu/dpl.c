#include "global.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"

#include "CAN.h"
#include "chalg.h"
#include "Sup.h"
#include "dpl.h"
#include "../Cc/ClusterControl.h"
#include "engines_types.h"
#include "regu_IO.h"

static dplList_t dplList[DPL_LIST_SIZE] = {0};

static Uint8 dplAddSlave(Uint16 nodeId, Uint8 index);
static void dplRemoveSlave(Uint8 index);
static Boolean dplGetPowerOutput();

// Variables
static int dplmasterConflict = 0;													// Master conflict error
static int dplSlaveWatchdog = 0;													// Slave watchdog counter
static Uint32 dplPacLimit = 0;														// DPL variable power limitation for each charger (set from DPL master)
static Uint32 dplPowerUnitsCtrl = 0;												// Power units under control. Bitcoded value, bit1: dplId = 1 etc
static Uint32 dplPowerRequestedTotal = 0;											// Total amount of power requested from slaves
static Uint32 dplPowerAllocatedTotal = 0;											// Total amount of power allocated in system
static Uint32 dplPowerRemainingTotal = 0;											// Total amount of power in system remaining for distribution to slaves
static Uint32 dplPowerConsumedTotal = 0;											// Total amount of power consumed in system
static Uint8 dplMode = DplModeSafe;													// DPL power output mode
static Uint8 dplPowerUnitId = 0;													// DPL Id set from master (powerUnit Id)

// NVM parameters
static Uint8 dplPowerGroup = 0;														// Power group, definition of a DPL system
static Uint8 dplPowerGroupFunc = DplDisabled;										// Power group function (disabled, master, slave)
static Uint8 dplPriorityFactor = DplPrioNormal;										// Priority for each charger
static Uint8 dplPacLimit_default = 0;												// DPL default/safe mode power limitation in percent of nvmPacLimit
static Uint32 nvmPacLimit = 0;														// Power limitation for each charger (Factory setting NVM)
static Uint32 dplPowerLimitTotal = 0;												// Total amount of power in system for distribution to slaves

static Uint8 dplAddSlave(Uint16 nodeId, Uint8 index){
	Uint8 dplId = 0;
	int i;

	for(i = index; i < DPL_LIST_SIZE; i++){
		if(dplList[i].dplId == 0){
			dplList[i].dplId = i;
			dplList[i].nodeId = nodeId;
			dplList[i].watchdog = DPL_MASTER_WATCHDOG_CNT;
			dplId = dplList[i].dplId;
			dplPowerUnitsCtrl |= 1<<i;
			break;
		}
	}

	return dplId;
}

static void dplRemoveSlave(Uint8 index){
	int i;

	for(i = 1; i < DPL_LIST_SIZE; i++){
		if(dplList[i].dplId == index || index == DPL_ALL){
			dplList[i].dplId = 0;
			dplList[i].nodeId = 0;
			dplList[i].powerAllocated = 0;
			dplList[i].powerRequested = 0;
			dplList[i].priority = 0;
			dplList[i].priorityFactor = 0;
			dplList[i].soc = 0;
			dplList[i].powerConsumed = 0;
			dplList[i].watchdog = 0;
			dplPowerUnitsCtrl &= ~(1<<i);
		}
	}
}

static Boolean dplGetPowerOutput(){
	Boolean dplPowerOutput;
	Uint8 supState;
	Uint32 dplError;

	reg_get(&supState, regu__supState);
	reg_get(&dplError, regu__BBCError);

	if(dplError){
		dplPowerOutput = DplPowerOff;
	}
	else{
		if(supState == SUP_STATE_CHARGING){
			dplPowerOutput = DplPowerOn;
		}
		else{
			dplPowerOutput = DplPowerOff;
		}
	}

	return dplPowerOutput;
}

void dplMasterWatchdogReset(){
	dplRemoveSlave(DPL_ALL);
}

void dplMasterWatchdogRun(){
	int i;

	for(i = 1; i < DPL_LIST_SIZE; i++){
		if(dplList[i].watchdog == 0){
			dplRemoveSlave(i);
		}
		else{
			dplList[i].watchdog--;
			dplPowerUnitsCtrl |= 1<<i;
		}
	}
}

void dplMasterConflictSet(){
	dplmasterConflict = TRUE;

	msg_sendTry(
		MSG_LIST(statusRep),
		PROJECT_MSGT_STATUSREP,
		SUP_STAT_DPL_MASTER_CONFLICT_SET
	);
}

void dplMasterConflictClear(){
	dplmasterConflict = FALSE;

	msg_sendTry(
		MSG_LIST(statusRep),
		PROJECT_MSGT_STATUSREP,
		SUP_STAT_DPL_MASTER_CONFLICT_CLEAR
	);
}

void dplNoMasterSet(){
	// Reset master address
	Uint16 dplMasterNwkAddr = 0xFFFF;
	reg_put(&dplMasterNwkAddr, regu__dplMasterNwkAddr);
	// Clear DplId
	dplPutDplId(0);

	msg_sendTry(
		MSG_LIST(statusRep),
		PROJECT_MSGT_STATUSREP,
		SUP_STAT_DPL_NO_MASTER
	);

}

void dplNoMasterClear(){
	msg_sendTry(
		MSG_LIST(statusRep),
		PROJECT_MSGT_STATUSREP,
		SUP_STAT_DPL_MASTER
	);
}

/*
void dplSlaveWatchdogReset(){
	dplSlaveWatchdog = 0;
}
*/
int dplSlaveWatchdogRun(){
	// Decrease watchdog (if not zero) and then check if it has timed out
	if(dplSlaveWatchdog){
		dplSlaveWatchdog--;
	}

	if(dplSlaveWatchdog == 0){
		// Reset PacLimit to "safe mode" level (if not turned off)
		if(dplMode == DplModeNormal){
			dplMode = DplModeSafe;
		}
	}

	return dplSlaveWatchdog;
}

void dplInit(){
	Uint8 parallelControl;

	// Get parallel control parameter
	reg_get(&parallelControl, regu__ParallelControl);

	// Read NVM parameters to RAM parameters used during operation
	reg_get(&dplPowerGroup, regu__PowerGroup);
	reg_get(&dplPowerGroupFunc, regu__PowerGroup_func);
	reg_get(&dplPriorityFactor, regu__DplPriorityFactor);
	reg_get(&dplPacLimit_default, regu__DplPacLimit_default);
	reg_get(&dplPowerLimitTotal, regu__DplPowerLimitTotal);
	reg_get(&nvmPacLimit, regu__PacLimit);

	// Reset DPL ID shown in display
	dplPowerUnitId = 0;

	if(dplPowerGroupFunc == DplMaster){
		// Master control bit always set
		dplPowerUnitsCtrl = 1;
		// Set normal mode
		dplMode = DplModeNormal;
		// Set default power limit
		if(parallelControl){
			int n = CcGetSelectedChargers();
			dplPacLimit = n * nvmPacLimit;
		}
		else{
			dplPacLimit = nvmPacLimit;
		}
		// Reset DPL master node list
		dplMasterWatchdogReset();
		// Update and run first calculation
		dplUpdateMaster();
		dplPowerCalculation();
	}
	else if(dplPowerGroupFunc == DplSlave){
		Uint32 paclimit;

		// Set safe mode
		dplMode = DplModeSafe;
		// Get default power limit
		paclimit = (nvmPacLimit * dplPacLimit_default)/100;
		if(parallelControl){
			int n = CcGetSelectedChargers();
			paclimit = n * paclimit;
		}
		// Set default power limit
		dplPutDplPacLimit(paclimit);
	}
	else{
		// Disabled
		;
	}
}

Uint8 dplUpdateSlave(Uint16 nodeId, Uint8 powerUnit, Uint8 soc, Uint8 priorityFactor, Uint16 powerRequested, Uint32 powerConsumed){
	Uint8 dplId = powerUnit;
	Uint8 index;

	if(dplId == 0){
		//Try add node to list
		index = 1;	// Start at first slave position
		dplId = dplAddSlave(nodeId, index);
	}
	else if(dplList[dplId].dplId == 0){
		// Re-add slave node to list
		index = dplId;	// Start at PowerUnit Id position
		dplId = dplAddSlave(nodeId, index);
	}

	if(dplId != 0 && dplList[dplId].nodeId == nodeId){
		// update existing slave unit
		if(soc > 100){
			soc = 100;	// Just make sure soc is always <= 100
		}
		dplList[dplId].soc = soc;
		dplList[dplId].priorityFactor = priorityFactor;
		dplList[dplId].powerRequested = powerRequested;
		dplList[dplId].powerConsumed = powerConsumed;
		dplList[dplId].watchdog = DPL_MASTER_WATCHDOG_CNT;
		dplPowerUnitsCtrl |= 1<<dplId;
	}
	else{
		// Not in list
		dplId = 0;
	}

	return dplId;
}

void dplUpdateMaster(){
	// update master unit
	dplList[0].soc = dplGetSoc();
	dplList[0].priorityFactor = dplPriorityFactor;
	dplList[0].powerRequested = dplGetPacRequest();
	dplList[0].powerConsumed = dplGetPowerConsumed();
}

void dplPowerCalculation(){
	int priorityTotal = 0;
	int powerRequestedTotal = 0;
	int powerAllocatedTotal = 0;
	int powerRemainingTotal = 0;
	int powerFirstCalcTotal = 0;
	int powerConsumedTotal = 0;
	int i;

	// Get total power limit for first calculation (80% of total power)
	powerFirstCalcTotal = (int)(dplPowerLimitTotal * 8 / 10);
	powerRemainingTotal = (int)dplPowerLimitTotal - powerFirstCalcTotal;

	// Calculate and sum priority and requested power
	for(i = 0; i < DPL_LIST_SIZE; i++){
		if(i == 0 || dplList[i].dplId != 0){
			if(dplList[i].powerRequested != 0){
				Uint32 priority = (100 - dplList[i].soc) * (dplList[i].powerRequested / 100) + 1;

				switch(dplList[i].priorityFactor)
				{
				case DplPriolow:
					dplList[i].priority = priority / 2;
					break;

				case DplPrioNormal:
					dplList[i].priority = priority;
					break;

				case DplPrioHigh:
					dplList[i].priority = priority * 2;
					break;

				default:
					dplList[i].priority = priority;
					break;
				}

				priorityTotal += dplList[i].priority;
				powerRequestedTotal += dplList[i].powerRequested;
			}
		}
	}

	// First calcualtion of power allocation
	for(i = 0; i < DPL_LIST_SIZE; i++){
		if(i == 0 || dplList[i].dplId != 0){
			if(dplList[i].powerRequested != 0 && priorityTotal != 0){
				Uint16 powerCalculated;

				if((int)dplPowerLimitTotal >= powerRequestedTotal){
					powerCalculated	= dplList[i].powerRequested;
				}
				else{
					powerCalculated = (Uint16)(dplList[i].priority * powerFirstCalcTotal / priorityTotal);
				}

				if(powerCalculated > dplList[i].powerRequested){
					dplList[i].powerAllocated = dplList[i].powerRequested;
				}
				else{
					dplList[i].powerAllocated = powerCalculated;
				}
			}
			else{
				dplList[i].powerAllocated = 0;
			}

			powerAllocatedTotal += dplList[i].powerAllocated;
			powerConsumedTotal += dplList[i].powerConsumed;
			// Remove allocated power from request in second calculation
			dplList[i].powerRemaining = dplList[i].powerRequested - dplList[i].powerAllocated;
		}
	}

	// Calculate remaining power
	powerRemainingTotal += powerFirstCalcTotal - powerAllocatedTotal;

	// Proceed with second calculation if power remaining
	if(powerRemainingTotal > 0){
		priorityTotal = 0;
		// Calculate and sum priority and requested power
		for(i = 0; i < DPL_LIST_SIZE; i++){
			if(i == 0 || dplList[i].dplId != 0){
				if(dplList[i].powerRemaining != 0){
					Uint32 priority = dplList[i].soc * (dplList[i].powerRemaining / 100) + 1;

					switch(dplList[i].priorityFactor)
					{
					case DplPriolow:
						dplList[i].priority = priority / 2;
						break;

					case DplPrioNormal:
						dplList[i].priority = priority;
						break;

					case DplPrioHigh:
						dplList[i].priority = priority * 2;
						break;

					default:
						dplList[i].priority = priority;
						break;
					}

					priorityTotal += dplList[i].priority;
				}
			}
		}

		// Second calcualtion of power allocation
		for(i = 0; i < DPL_LIST_SIZE; i++){
			if(i == 0 || dplList[i].dplId != 0){
				if(dplList[i].powerRemaining != 0 && priorityTotal != 0){
					int powerRequest = dplList[i].powerRemaining;

					Uint16 powerCalculated = (Uint16)(dplList[i].priority * powerRemainingTotal / priorityTotal);

					if(powerCalculated > powerRequest){
						dplList[i].powerAllocated += powerRequest;
						powerAllocatedTotal += powerRequest;
					}
					else{
						dplList[i].powerAllocated += powerCalculated;
						powerAllocatedTotal += powerCalculated;
					}
				}
			}
		}
	}

	dplPowerRequestedTotal = powerRequestedTotal;
	dplPowerAllocatedTotal = powerAllocatedTotal;
	dplPowerRemainingTotal = dplPowerLimitTotal - dplPowerAllocatedTotal;
	dplPowerConsumedTotal = powerConsumedTotal;
	// Set master power limitation
	dplPacLimit = dplList[0].powerAllocated;
}

/*
 * Get/put "local" variables
 */
Uint32 dplGetPowerUnitsCtrl(){
	return dplPowerUnitsCtrl;
}

Uint32 dplGetPowerRequestedTotal(){
	return dplPowerRequestedTotal;
}

Uint32 dplGetPowerConsumedTotal(){
	return dplPowerConsumedTotal;
}

Uint32 dplGetPowerAllocatedTotal(){
	Uint32 powerAllocatedTotal = 0;

	if(dplmasterConflict != TRUE){
		powerAllocatedTotal = dplPowerAllocatedTotal;
	}
	return powerAllocatedTotal;
}

Uint16 dplGetPowerAllocated(Uint8 dplId){
	Uint16 powerAllocated = 0;

	if(dplId < DPL_LIST_SIZE && dplmasterConflict != TRUE){
		powerAllocated = dplList[dplId].powerAllocated;
	}
	return powerAllocated;
}

Uint16 dplGetPowerRequested(Uint16 pwrReq){
	Uint16 powerAllocated = 0;

	if(pwrReq < dplPowerRemainingTotal){
		powerAllocated = pwrReq;
	}
	else{
		powerAllocated = dplPowerRemainingTotal;
	}

	dplPowerAllocatedTotal += powerAllocated;
	dplPowerRemainingTotal -= powerAllocated;

	return powerAllocated;
}

Uint32 dplGetPowerConsumed(){
	Uint32 powerConsumed;

	reg_get(&powerConsumed, regu__Smeas1s);

	return powerConsumed;
}

Uint32 dplGetDplPacLimit(){
	Uint32 pacLimit = 0;

	if(dplmasterConflict != TRUE){
		if(dplMode == DplModeOff){
			pacLimit = 0;
		}
		else if(dplMode == DplModeNormal){
			pacLimit = dplPacLimit;
		}
		else if(dplMode == DplModeSafe){
			pacLimit = (nvmPacLimit * dplPacLimit_default)/100;
		}
	}

	return pacLimit;
}

Uint16 dplGetPacRequest(){
	Uint16 pacRequest = 0;

	if(dplGetPowerOutput() == DplPowerOn){
		Uint8 parallelControl;

		// If parallel control master/BMS ctrl, multiply with number of units
		reg_get(&parallelControl, regu__ParallelControl);

		if(parallelControl){
			int n;
			int pacRequestSum;

			n = CcGetSelectedChargers();
			pacRequestSum = n * nvmPacLimit;

			if(pacRequestSum < 0xFFFF){
				pacRequest = (Uint16)pacRequestSum;
			}
			else{
				pacRequest = 0xFFFF;
			}
		}
		else{
			pacRequest = (Uint16)nvmPacLimit;
		}
	}

	return pacRequest;
}

Uint8 dplGetSoc(){
	Uint16 chalgStatus;
	Uint8 chalgSoc;
	Uint8 bmSoc;
	Uint8 dplSoc;
	Uint8 canCommProfile;

	reg_get(&chalgStatus, regu__ChalgStatus);
	reg_get(&bmSoc, regu__BmSOC);
	reg_get(&chalgSoc, regu__ChalgSoc);
	reg_get(&canCommProfile, Regu_CAN_CommProfile);

	if((canCommProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT) ||
	   (chalgStatus & CHALG_STAT_BM_CONNTECTED))
	{
		if(bmSoc > 0){
			dplSoc = bmSoc;
		}
		else{
			dplSoc = chalgSoc;
		}
	}
	else{
		dplSoc = chalgSoc;
	}

	if(dplSoc > 100){
		dplSoc = 100;	// Just make sure soc is always <= 100
	}

	return dplSoc;
}

Uint8 dplGetDplId(){
	return dplPowerUnitId;
}

Uint32 dplGetPset(){
	float ConversionfactorApparentPower = regu_IoGetConversionfactorApparentPower();
	Uint32 pacLimit = dplGetDplPacLimit();
	Uint32 Pset = pacLimit * ConversionfactorApparentPower;

	return Pset;
}

void dplPutMode(Uint8 mode){
	// Set DPL mode
	if(mode == DplModeOff){
		dplMode = DplModeOff;
	}
	else if(mode == DplModeNormal){
		dplMode = DplModeNormal;
	}
	else if(mode == DplModeSafe){
		dplMode = DplModeSafe;
	}
}

void dplPutDplPacLimit(Uint32 pacLimit){
	// Update DplPacLimit and watchdog because new value has been received
	dplPacLimit = pacLimit;
	dplSlaveWatchdog = DPL_SLAVE_WATCHDOG_CNT;
}

void dplPutDplId(Uint8 dplId){
	// Set DPL Id
	dplPowerUnitId = dplId;
}

/*
 * "NVM parameter" get/put functions
 */
Uint8 dplGetPowerGroup(){
	return dplPowerGroup;
}

Uint8 dplGetPowerGroupFunc(){
	return dplPowerGroupFunc;
}

Uint8 dplGetDplPriorityFactor(){
	return dplPriorityFactor;
}

Uint8 dplGetDplPacLimit_default(){
	return dplPacLimit_default;
}

Uint32 dplGetDplPowerLimitTotal(){
	return dplPowerLimitTotal;
}

void dplPutPacLimit(Uint32 pacLimit){
	nvmPacLimit = pacLimit;
}

void dplPutPowerGroup(Uint8 powerGroup){
	dplPowerGroup = powerGroup;
}

void dplPutPowerGroupFunc(Uint8 powerGroupFunc){
	dplPowerGroupFunc = powerGroupFunc;
}

void dplPutDplPriorityFactor(Uint8 priorityFactor){
	dplPriorityFactor = priorityFactor;
}

void dplPutDplPacLimitDefault(Uint8 pacLimit_default){
	dplPacLimit_default = pacLimit_default;
}

void dplPutDplPowerLimitTotal(Uint32 powerLimitTotal){
	dplPowerLimitTotal = powerLimitTotal;
}
