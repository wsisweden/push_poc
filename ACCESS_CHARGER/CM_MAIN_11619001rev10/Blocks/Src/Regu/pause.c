#include "pause.h"
#include "global.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "sup.h"

static int getSupPause();

volatile static int pause = 0;							// Remember current pause state

static int getSupPause(){
	Uint8 SupState;
	reg_get(&SupState, regu__supState);

	return SupState == SUP_STATE_PAUSE;
}

void updatePause(){
	pause = getSupPause();
}

int getPause(){
	return pause;
}
