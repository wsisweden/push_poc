/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Regu.c
*
*	\ingroup	REGU
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	REGU		REGU
*
*	\brief		Regulator
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "global.h"
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"

#include "Regu.h"
#include "local.h"
#include "dpl.h"

#include "pause.h"
#include "regu_IO.h"
#include "ControlLoop/ControlLoop.h"
#include "ControlLoop/sendToBattery.h"
#include "Cm.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/
/**
 * \name	Task execution triggers
 */

/*@{*/
#define REGU__TRG_INIT		(1<<0)		/**< Perform warm init				*/
#define REGU__TRG_RADIOACK	(1<<1)		/**< Radio triggered function call	*/
#define REGU__TRG_PACLIMIT	(1<<2)		/**< Pac limit set from Menu/Config	*/
#define REGU__TRG_PWRGROUP	(1<<3)		/**< Power group set from Menu/Config	*/
#define REGU__TRG_PWRGROUP_FUNC	(1<<4)		/**< Power group set from Menu/Config	*/
#define REGU__TRG_DPL_PWRLIMTOT	(1<<5)		/**< Pac limit total set from Menu/Config	*/
#define REGU__TRG_DPL_PRIO	(1<<6)		/**< Priority factor set from Menu/Config	*/
#define REGU__TRG_DPL_PACLIMIT_DEF	(1<<7)		/**< Safe mode Pac limit set from Menu/Config	*/
#define REGU__TRG_DPL_INIT	(1<<8)		/**< Init DPL parameters at startup 	*/
/*@}*/

/**
 * Cable comm defines
 */

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Instance type of the REGU FB.
 */
 
/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/
PRIVATE int Regu_init(int inputInit);
PRIVATE void RestartCount(regu__Inst* const pInst);
PRIVATE void ErrorCount(regu__Inst* const pInst, ControlLoopMeas_type* meas);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/
PUBLIC regu__Inst*		regu_pInstance;
volatile int Start = 0;

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid		An index (0..) that is accepted by various functions
*						to identify the instance. Should be kept for later use,
*						or may be discarded if not required by the FB.
*	\param		pArgs	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/
PUBLIC void * regu_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	regu__Inst *				pInst;

	pInst = (regu__Inst *) mem_reserve(&mem_normal, sizeof(regu__Inst));
	regu_pInstance = pInst;

	pInst->instId = iid;
	pInst->triggers = REGU__TRG_INIT | REGU__TRG_DPL_INIT;
	pInst->RestartDelay = 0;
	pInst->RegulatorErrorCnt = 0;

	pInst->outsideSpecState = WITHIN_SPEC_STATE;
	pInst->outsideSpecDelayCnt = 0U;

	FIO4DIR |= (1<<28);

	return((void *) pInst);
}

PRIVATE int Regu_init(int inputInit)
{
	static int init = 0;

	if(inputInit)
	{
		ControlLoopInit();

		{
			int n;
			for(n = 0; n < ReguWatchDog_enum_size; n++){
				regu_pInstance->ReguWatchDog[n] = REGU_WATCHDOG_TIME;
			}
		}
		regu_pInstance->NewSet = 0;
		regu_pInstance->Restarts = 0;
		regu_pInstance->RegulatorErrorCnt = 0;

		REGU_CHARGING_ON_DO_INIT();

		init = 1;
	}

	return init;
}

PRIVATE void RestartCount(regu__Inst* const pInst)
{
	Uint32 time;
	static Uint32 RestartTime = 0;

	reg_get(&time, regu__LocalTime);

	if(Start)													// Started now ?
	{
		int n;
		int k = 0;
		int Restarts = 0;
		Uint32 timeStoredOldest = 0;

		Start = 0;												// Reset start indicator

		for(n = 0; n < 3; n++)									// All stored times
		{
			Uint32 timeStored;
			reg_aGet(&timeStored, regu__Start, n);

			if(time - timeStored < 1*30)						// restarted within 30 seconds?
			{
				Restarts++;										// Count restart
			}

			{
				const uint32_t timeSince = time - timeStored;
				if(timeStoredOldest <= timeSince)				// Older position available for overwrite ?
				{
					timeStoredOldest = timeSince;				// Remember oldest time until now
					k = n;										// Remember position of oldest stored time until now
				}
			}
		}
		reg_aPut(&time, regu__Start, k);						// Remember current start time
		pInst->RestartDelay = 300;								// Set delay after restart
		pInst->Restarts = Restarts;								// Save number of restarts the last 60 seconds
		RestartTime = time;										// Store timestamp of restart
	}
	else{
		if(pInst->RestartDelay){
			pInst->RestartDelay--;								// decrease delay counter
		}

		if(time - RestartTime > 5*60){
			pInst->Restarts = 0;								// Reset error indication
		}
	}
}

PRIVATE void ErrorCount(regu__Inst* const pInst, ControlLoopMeas_type* meas)
{
	static int activeError = FALSE;

	if(activeError == FALSE){
		if(meas->RegulatorStatus.regulatorError){
			pInst->RegulatorErrorCnt++;
			if(pInst->RegulatorErrorCnt == 1){
				// Trigger event
				Uint8 reguErrorEvent = TRUE;
				reg_putLocal(&reguErrorEvent, reg_i_ReguErrorEvent, pInst->instId);
			}
		}
	}
	activeError = meas->RegulatorStatus.regulatorError;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		instance	Pointer to instance.
* 	\param		pArgs		Pointer to the initialization arguments.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void regu_reset(
	void *					instance,
	void const_P *			pArgs
) {	
	regu__Inst *			pInst;

	DUMMY_VAR(pArgs);

	pInst = (regu__Inst *) instance;

	if(pInst->flags == REGU_FLG_DOWN){
		atomic(
			pInst->flags &= ~REGU_FLG_DOWN;
		);
	}
	else{
		pInst->flags = 0;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		instance	Pointer to instance.
*	\param		nType		Down type.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void regu_down(
	void *					instance, 
	sys_DownType			nType
) {
	regu__Inst *			pInst;

	pInst = (regu__Inst *) instance;

	if (nType == SYS_DOWN_POWER_LOSS)
	{
		/*
		 * Flash corruption detected. Just abort everything.
		 */

		pInst->flags = 1;
	}
	else{
		/*
		 * Set down flag
		 */
		atomic(
			pInst->flags |= REGU_FLG_DOWN;
		);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read
*				the value of a parameter owned by this FB.
*
*	\param		instance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue		Pointer to the storage for the value.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus regu_read(
	void *					instance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex,
	void *					pValue
) {	
	regu__Inst *			pInst;
	sys_IndStatus			retVal;

	pInst = (regu__Inst *) instance;

	DUMMY_VAR(aIndex);

	switch(rIndex)
	{
		case reg_indic_limiterGui:
		{
			//ChargerStatusFields_type Status;

			//reg_getLocal(&Status, reg_i_Status, pInst->instId);

			*((Uint8 *) pValue) = pInst->Status.Detail.Info.Detail.limit;

			retVal = REG_OK;
			break;
		}

		case reg_indic_ReguErrorSum:	// #JJ alarm view
		{
			ChargerStatusFields_type StatusSum;
			chargerAlarm_type ReguError;

			reg_getLocal(&StatusSum, reg_i_StatusSum, pInst->instId);
			ReguError.Error = StatusSum.Detail.Error;
			ReguError.Warning = StatusSum.Detail.Warning;

			*((Uint16 *) pValue) = ReguError.Sum;

			retVal = REG_OK;
			break;
		}

		case reg_indic_StatusP:
		{
			*((ChargerStatusFields_type *) pValue) = pInst->Status;
			retVal = REG_OK;
			break;
		}

		case reg_indic_ReguErrorCntP:
		{
			*((Uint8 *) pValue) = pInst->RegulatorErrorCnt;
			retVal = REG_OK;
			break;
		}

		case reg_indic_SyncP:
		{
			*((Uint8 *) pValue) = pInst->ReguWatchDog[ReguWatchDogNewSet_enum] != 0 ? TRUE:FALSE;
			retVal = REG_OK;
			break;
		}

		case reg_indic_DplPacLimit:
		{
			*((Uint32 *) pValue) = dplGetDplPacLimit();
			retVal = REG_OK;
			break;
		}

		case reg_indic_DplId:
		{
			*((Uint8 *) pValue) = dplGetDplId();
			retVal = REG_OK;
			break;
		}

		default:
			retVal = REG_UNAVAIL;
			break;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		instance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus regu_write(
	void *					instance, 
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex
) {
	regu__Inst *			pInst;

	pInst = (regu__Inst *) instance;
	
	DUMMY_VAR(aIndex);

	switch(rIndex)
	{
		case reg_indic_NewSet :		// Sync
			pInst->NewSet = 1;
			pInst->ReguWatchDog[ReguWatchDogNewSet_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_Uset :		// Iset
			pInst->ReguWatchDog[ReguWatchDogUset_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_Iset :		// Pset
			pInst->ReguWatchDog[ReguWatchDogIset_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_Pset :		// ReguMode
			pInst->ReguWatchDog[ReguWatchDogPset_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_UactMode :		// Uset
			pInst->ReguWatchDog[ReguWatchDogUactMode_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_ReguMode :		// Pset
			pInst->ReguWatchDog[ReguWatchDogReguMode_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_ReguOffUactLow :		// Battery detect off Uact low
			pInst->ReguWatchDog[ReguWatchDogReguOffUactLow_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_ReguOffUactHigh :		// Battery detect off Uact high
			pInst->ReguWatchDog[ReguWatchDogReguOffUactHigh_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_ReguOnUactLow :		// Battery detect on Uact low
			pInst->ReguWatchDog[ReguWatchDogReguOnUactLow_enum] = REGU_WATCHDOG_TIME;
			break;
		case reg_indic_ReguOnUactHigh :		// Battery detect on Uact high
			pInst->ReguWatchDog[ReguWatchDogReguOnUactHigh_enum] = REGU_WATCHDOG_TIME;
			break;

		case reg_indic_PacLimit :
			pInst->triggers |= REGU__TRG_PACLIMIT;
			break;

		case reg_indic_PowerGroup :
			pInst->triggers |= REGU__TRG_PWRGROUP;
			break;

		case reg_indic_PowerGroup_func :
			pInst->triggers |= REGU__TRG_PWRGROUP_FUNC;
			break;

		case reg_indic_DplPowerLimitTotal :
			pInst->triggers |= REGU__TRG_DPL_PWRLIMTOT;
			break;

		case reg_indic_DplPriorityFactor :
			pInst->triggers |= REGU__TRG_DPL_PRIO;
			break;

		case reg_indic_DplPacLimit_default :
			pInst->triggers |= REGU__TRG_DPL_PACLIMIT_DEF;
			break;

		case reg_indic_reset :
			pInst->triggers |= REGU__TRG_INIT;
			break;

		case reg_indic_radioAck :
			pInst->triggers |= REGU__TRG_RADIOACK;
			break;

	}
	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_ctrl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		instance	Pointer to the data of the instance.
*	\param		pCtrl		Pointer to the control indication.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void regu_ctrl(
	void *					instance,
	sys_CtrlType *			pCtrl
) {
	/*
	 *	0x80 = request to enter test mode.
	 */

	if (pCtrl->cmd == 0x80)
	{
		atomic(
			((regu__Inst *) instance)->flags |= REGU_FLG_TESTMODE;
		);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_test
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Self test
*
*	\param		instance	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 regu_test(
	void *					instance
) {
	regu__Inst *			pInst;

	pInst = (regu__Inst *) instance;
	
	if (osa_syncPeek(project_stateMachSync) && not(pInst->flags & (1<<0)))
	{
		/*
		 *	Only do test check if sup's sync flag is set.
		 */
		if(pInst->test == FALSE)
		{
			;//project__errorHandler(PROJECT_ERROR_TEST);
		}

		pInst->test = FALSE;
	}
	
	return sys_msToTestTicks(1000);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	regu_activate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\return		
*
*	\details
*
*	\note		Called each 10ms by SUP.
*
*******************************************************************************/
PUBLIC void regu_activate(
void	*pInstance
) {
	Uint16					triggers;
	regu__Inst *			pInst;

	pInst = regu_pInstance;

	pInst->test = TRUE; /* Reset internal test flag */

	atomic(
		triggers = pInst->triggers;
		pInst->triggers = 0;
	);

	/***************************************************************************
	 * Check if warm init should be performed.
	 */

	if (triggers & REGU__TRG_INIT)
	{
		/* Reset charging error sum*/

		ChargerStatusFields_type StatusSum = {.Sum = 0};
		reg_putLocal(&StatusSum, reg_i_StatusSum, pInst->instId);

		/*
		 * re initialize regu parameters
		 */
		Regu_init(1);
	}

	/***************************************************************************
	 * Trigger BM calibration function state change.
	 */
	if (triggers & REGU__TRG_RADIOACK)
	{
		/* Call function*/
		bmCalibrationRadioAck();
	}

	if (triggers & REGU__TRG_PACLIMIT)
	{
		/* Get parameter from NVM and set to RAM */
		Uint32 pacLimit;

		reg_getLocal(&pacLimit, reg_i_PacLimit, pInst->instId);

		dplPutPacLimit(pacLimit);
	}

	if (triggers & REGU__TRG_PWRGROUP)
	{
		/* Get parameter from NVM and set to RAM */
		Uint8 powerGroup;

		reg_getLocal(&powerGroup, reg_i_PowerGroup, pInst->instId);

		dplPutPowerGroup(powerGroup);
	}

	if (triggers & REGU__TRG_PWRGROUP_FUNC)
	{
		/* Get parameter from NVM and set to RAM */
		Uint8 powerGroup_func;

		reg_getLocal(&powerGroup_func, reg_i_PowerGroup_func, pInst->instId);

		dplPutPowerGroupFunc(powerGroup_func);
	}

	if (triggers & REGU__TRG_DPL_PWRLIMTOT)
	{
		/* Get parameter from NVM and set to RAM */
		Uint32 powerLimitTotal;

		reg_getLocal(&powerLimitTotal, reg_i_DplPowerLimitTotal, pInst->instId);

		dplPutDplPowerLimitTotal(powerLimitTotal);
	}

	if (triggers & REGU__TRG_DPL_PRIO)
	{
		/* Get parameter from NVM and set to RAM */
		Uint8 priorityFactor;

		reg_getLocal(&priorityFactor, reg_i_DplPriorityFactor, pInst->instId);

		dplPutDplPriorityFactor(priorityFactor);
	}

	if (triggers & REGU__TRG_DPL_PACLIMIT_DEF)
	{
		/* Get parameter from NVM and set to RAM */
		Uint8 pacLimit_default;

		reg_getLocal(&pacLimit_default, reg_i_DplPacLimit_default, pInst->instId);

		dplPutDplPacLimitDefault(pacLimit_default);
	}

	if (triggers & REGU__TRG_DPL_INIT)
	{
		/*
		 * Initialize DPL function
		 */
		dplInit();
	}
}

PUBLIC ChargerStatusFields_type regu_Regulate()
{
	regu__Inst* const pInst = regu_pInstance;

	if(!Regu_init(0)){ // Initialize if needed and return if never initialized
		ChargerStatusFields_type Status = {.Sum = 0};
		Status.Detail.Derate.Detail.NoLoad = 1;	// #JJ
		return Status;
	}

	DISABLE;
	if (pInst->flags & REGU_FLG_TESTMODE)
	{
		ChargerStatusFields_type Status = {.Sum = 0};
		Status.Detail.Derate.Detail.NoLoad = 1;	// #JJ
		ENABLE;
		/* Production test mode enabled. REGU should not do anything. */
		return Status;
	}
	ENABLE;
	RestartCount(pInst);

	updatePause();																							// Update pause state

	{
		ChargerStatusFields_type Status = {.Sum = 0};
		ChargerDerate_type Derate = {.Sum = 0};
		static ControlLoopMeas_type meas = {0};

		if(pInst->NewSet){																					// Start to use new input values ?
			pInst->NewSet = 0;																				// Indicator detected now reset indicator

			ChargerSet_type Input;
			Interval_type UactOff;
			Interval_type UactOn;

			regu_IoGetControlLoopInputs(&Input, &UactOff, &UactOn, &Derate, pInst->instId);					// Read inputs
			ControlLoopSetInputs(&Input, &UactOff, &UactOn, &Derate);										// Tell control loop to use the new values
		}
																											// Read measurements and run control loop
		regu_IoGetMeas(pInst, &meas);																		// Read measurements
		Status = ControlLoop(&meas);																		// Run control loop

		writeOutputToReg(pInst, Status);																	// Write output to register
		ErrorCount(pInst, &meas);

		return Status;																						// Return status
	}
}
