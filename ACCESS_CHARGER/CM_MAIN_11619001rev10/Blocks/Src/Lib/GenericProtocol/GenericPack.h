#pragma once

#include "GenericProtocol.h"
#include <pb.h>
#include <stdint.h>
#include <stdbool.h>

// Public typedef

typedef struct {
  uint32_t offset;
  uint32_t chunkPos;
  uint8_t* pBuffer;
  uint32_t bufferSize;
  uint8_t counter;
} GenericPack_t;

void GenericPackInit(GenericPack_t* const pInstance, uint8_t* pBuffer,
                     const uint32_t size);
void GenericPackStartFrame(GenericPack_t* const pInstance);
uint32_t GenericPackGetCmdMaxSize(GenericPack_t* const pInstance);
bool GenericPackAddCmd(GenericPack_t* const pInstance, const GenericType_t type,
    const pb_field_t fields[], const void *src_struct);
void GenericPackEndFrame(GenericPack_t* const pInstance);
void GenericPackGetFrameChunk(GenericPack_t* const pInstance, uint8_t* pBuffer,
                              const uint32_t size);
bool GenericPackIsChunkLeft(GenericPack_t* const pInstance);
bool GenericPackIsFrameStarted(GenericPack_t* const pInstance);
