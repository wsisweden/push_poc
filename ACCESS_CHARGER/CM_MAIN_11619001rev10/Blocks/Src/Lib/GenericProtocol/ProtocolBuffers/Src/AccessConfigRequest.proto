syntax = "proto3";

import "ChargingRestriction.proto";

option optimize_for = LITE_RUNTIME;
package GenericProtocol;

// Sharp configuration response
message AccessConfigRequest {
	// Configuration for selected engine code, from a list of engines. Res (0 - uint32Max)
	uint32 engineCode = 1;
	// Charging algorithm number, list of available algorithms. Res (0 - uint16Max)
	uint32 algorithmNumber = 2;
	// The capacity of the battery. Unit (Ah). Res (0 - floatMax)
	float capacity = 3;
	// Amount of cells in battery. Res (0 - uint16Max)
	uint32 cells = 4;
	// Cable resistance. Unit (mOhm). Res (0 - uint16Max)
	float cableResistance = 5;
	// Base load. Unit (mA). Res (0 - uint16Max)
	float baseLoad = 6;
	// Configuration for limiting nominal current on selected engine. Unit (A). Res (0.001)
	float iDcLimit = 7;
	// Security code for changing configuration. Res (0 - uint32Max)
	uint32 securityCode2 = 8;
	// The amount of seconds between instant logs, 0 = off. Res (0 - uint16Max)
	uint32 instantLogPeriod = 9;
	// Configurable user ID, may not be uniqe
	uint32 chargerId = 11;
	// Security code for changing configuration. Res (0 - uint32Max)
	uint32 securityCode1 = 12;
	// The battery type, not used
	uint32 batteryType = 13;
	// The current language code. Res (0 - uint8Max)
	uint32 language = 14;
	// Static battery temperature used for voltage/current compensation if no BMU temperature
	float batteryTemperature = 15;
  // If CEC mode is active
  bool cecMode = 16;
  // If user parameters is active
  bool userParamMode = 17;
  //
  bool quietDerate = 18;
  // If DPL is active
  bool dplMode = 19;
  // The radio PAN ID. Res (0 - uint16Max)
  uint32 radioPanId = 20;
  // The radio channel. Res 11-26.
  uint32 radioChannel = 21;
  // The radio node address. Res (0 - uint16Max)
  uint32 radioNodeAddress = 22;
  // The current limit on mains. Res (0 - uint32Max)
  uint32 mainsCurrentLimit = 23;
  // The power limit on mains. Res (0 - uint32Max)
  uint32 mainsPowerLimit = 24;
  // The power group. Res (0 - uint8Max)
  uint32 powerGroup = 25;
  // The display contrast. Res (0 - uint8Max)
  uint32 displayContrast = 26;
  // The LED brightness max. Res (0 - uint8Max)
  uint32 ledBrightnessMax = 27;
  // The LED brightness dim. Res (0 - uint8Max)
  uint32 ledBrightnessDim = 28;
  // The time and date format. Res (0 - uint8Max)
  uint32 timeDateFormat = 29;
  // The F1 button function. Res (0 - uint8Max)
  uint32 buttonF1Function = 30;
  // The F2 button function. Res (0 - uint8Max)
  uint32 buttonF2Function = 31;
  // The remote in function. Res (0 - uint8Max)
  uint32 remoteInFunction = 32;
  // The remote out function. Res (0 - uint8Max)
  uint32 remoteOutFunction = 33;
  // The remote out alarm variable. Res (0 - uint8Max)
  uint32 remoteOutAlarmVariable = 34;
  // The remote out phase variable. Res (0 - uint8Max)
  uint32 remoteOutPhaseVariable = 35;
  // The watering function. Res (0 - uint8Max)
  uint32 wateringFunction = 36;
  // The watering variable. Res (0 - uint8Max)
  uint32 wateringVariable = 37;
  // The air pump variable. Res (0 - uint8Max)
  uint32 airPumpVariable = 38;
  // The 2nd air pump variable. Res (0 - uint8Max)
  uint32 airPumpVariable2 = 39;
  // The equalize function. Res (0 - uint8Max)
  uint32 equalizeFunction = 40;
  // The equalize variable. Res (0 - uint8Max)
  uint32 equalizeVariable = 41;
  // The 2nd equalize variable. Res (0 - uint8Max)
  uint32 equalizeVariable2 = 42;
  // The parallel control function. Res (0 - uint8Max)
  uint32 parallelControlFunction = 43;
  // The CAN mode. Res (0 - uint8Max)
  uint32 canMode = 44;
  // The CAN network control. Res (0 - uint16Max)
  uint32 canNetworkControl = 45;
  // The CAN speed in BPS. Res (0 - uint8Max)
  uint32 canBps = 46;
  // The CAN node id. Res (0 - uint8Max)
  uint32 canNodeId = 47;
  // If charging restrictions is active
  bool timeRestriction = 48;
  // The radio mode. Res (0 - uint8Max)
  uint32 radioMode = 49;
  // The radio network settings. Res (0 - uint8Max)
  uint32 radioNetworkSettings = 50;
  // The backlight time. Res (0 - uint8Max)
  uint32 backlightTime = 51;
  // The extra charge function. Res (0 - uint8Max)
  uint32 extraChargeFunction = 52;
  // The extra charge variable. Res (0 - uint8Max)
  uint32 extraChargeVariable = 53;
  // The 2nd extra charge variable. Res (0 - uint8Max)
  uint32 extraChargeVariable2 = 54;
  // The 3rd extra charge variable. Res (0 - uint8Max)
  uint32 extraChargeVariable3 = 55;
  // The low level for air pump alarm. Res (0 - uint16Max)
  uint32 airPumpAlarmLow = 56;
  // The high level for air pump alarm. Res (0 - uint16Max)
  uint32 airPumpAlarmHigh = 57;
  // The charging mode. Res (0 - uint8Max)
  uint32 chargingMode = 58;
  // The BBC function. Res (0 - uint8Max)
  uint32 bbcFunction = 59;
  // The BBC variable. Res (0 - uint8Max)
  uint32 bbcVariable = 60;
  // Routing. Res (0 - uint8Max)
  uint32 routing = 61;
  // The remote out BBC variable. Res (0 - uint8Max)
  uint32 remoteOutBbcVariable = 62;
  // The DPL total power limit. Res (0 - uint32Max)
  uint32 dplPowerLimitTotal = 63;
  // The DPL function. Res (0 - uint8Max)
  uint32 dplFunction = 64;
  // The DPL priority factor. Res (0 - uint8Max)
  uint32 dplPriorityFactor = 65;
  // The DPL default power limit. Res (0 - uint8Max)
  uint32 dplPowerLimitDefault = 66;
  // The pin in select values
  repeated uint32 pinInSelect = 67;
  // The pin out select values
  repeated uint32 pinOutSelect = 68;
  // The charging restrictions
  repeated ChargingRestriction chargingRestrictions = 69;
}
