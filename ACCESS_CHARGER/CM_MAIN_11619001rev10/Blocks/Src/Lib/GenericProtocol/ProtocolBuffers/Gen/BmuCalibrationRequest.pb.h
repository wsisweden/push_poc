/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.1 */

#ifndef PB_GENERICPROTOCOL_BMUCALIBRATIONREQUEST_PB_H_INCLUDED
#define PB_GENERICPROTOCOL_BMUCALIBRATIONREQUEST_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Enum definitions */
typedef enum _GenericProtocol_BmuCalibrationRequest_Type {
    GenericProtocol_BmuCalibrationRequest_Type_NONE = 0,
    GenericProtocol_BmuCalibrationRequest_Type_VOLTAGE = 1,
    GenericProtocol_BmuCalibrationRequest_Type_MIDDLEVOLTAGE = 2,
    GenericProtocol_BmuCalibrationRequest_Type_ELECTROLYTE = 3,
    GenericProtocol_BmuCalibrationRequest_Type_CURRENT = 4
} GenericProtocol_BmuCalibrationRequest_Type;
#define _GenericProtocol_BmuCalibrationRequest_Type_MIN GenericProtocol_BmuCalibrationRequest_Type_NONE
#define _GenericProtocol_BmuCalibrationRequest_Type_MAX GenericProtocol_BmuCalibrationRequest_Type_CURRENT
#define _GenericProtocol_BmuCalibrationRequest_Type_ARRAYSIZE ((GenericProtocol_BmuCalibrationRequest_Type)(GenericProtocol_BmuCalibrationRequest_Type_CURRENT+1))

/* Struct definitions */
typedef struct _GenericProtocol_BmuCalibrationRequest {
    GenericProtocol_BmuCalibrationRequest_Type type;
    uint32_t index;
    float value;
/* @@protoc_insertion_point(struct:GenericProtocol_BmuCalibrationRequest) */
} GenericProtocol_BmuCalibrationRequest;

/* Default values for struct fields */

/* Initializer values for message structs */
#define GenericProtocol_BmuCalibrationRequest_init_default {_GenericProtocol_BmuCalibrationRequest_Type_MIN, 0, 0}
#define GenericProtocol_BmuCalibrationRequest_init_zero {_GenericProtocol_BmuCalibrationRequest_Type_MIN, 0, 0}

/* Field tags (for use in manual encoding/decoding) */
#define GenericProtocol_BmuCalibrationRequest_type_tag 1
#define GenericProtocol_BmuCalibrationRequest_index_tag 2
#define GenericProtocol_BmuCalibrationRequest_value_tag 3

/* Struct field encoding specification for nanopb */
extern const pb_field_t GenericProtocol_BmuCalibrationRequest_fields[4];

/* Maximum encoded size of messages (where known) */
#define GenericProtocol_BmuCalibrationRequest_size 13

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define BMUCALIBRATIONREQUEST_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
