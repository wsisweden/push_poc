/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.1 */

#include "SharpConfigResponse.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t GenericProtocol_SharpConfigResponse_fields[12] = {
    PB_FIELD(  1, UINT32  , SINGULAR, STATIC  , FIRST, GenericProtocol_SharpConfigResponse, engineCode, engineCode, 0),
    PB_FIELD(  2, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, algorithmNumber, engineCode, 0),
    PB_FIELD(  3, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, capacity, algorithmNumber, 0),
    PB_FIELD(  4, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, cells, capacity, 0),
    PB_FIELD(  5, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, cableResistance, cells, 0),
    PB_FIELD(  6, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, baseLoad, cableResistance, 0),
    PB_FIELD(  7, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, iDcLimit, baseLoad, 0),
    PB_FIELD(  8, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, securityCode, iDcLimit, 0),
    PB_FIELD(  9, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, instantLogPeriod, securityCode, 0),
    PB_FIELD( 10, UINT32  , REPEATED, CALLBACK, OTHER, GenericProtocol_SharpConfigResponse, chargingAlgorithms, instantLogPeriod, 0),
    PB_FIELD( 11, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_SharpConfigResponse, chargerId, chargingAlgorithms, 0),
    PB_LAST_FIELD
};


/* @@protoc_insertion_point(eof) */
