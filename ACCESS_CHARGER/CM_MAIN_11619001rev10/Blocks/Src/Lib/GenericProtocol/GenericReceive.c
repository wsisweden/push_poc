#include "GenericReceive.h"
#include "GenericParse.h"
#include <Crc16/Crc16.h>
#include <stddef.h>
#include <string.h>

// Private typedef

// Private variables

// Private function declarations

static uint8_t* GenericReceiveGetCmdData(GenericReceive_t* const pInstance);
static uint16_t GenericReceiveGetCmdLength(GenericReceive_t* const pInstance);
static const pb_field_t* GenericReceiveDecodeUnionMessageType(
    pb_istream_t *stream, const pb_field_t fields[]);

// Public functions

/**
 * Initialize the data
 * @param pInstance The instance
 */
void GenericReceiveInit(GenericReceive_t* const pInstance) {
  pInstance->pBuffer = NULL;
  pInstance->bufferSize = 0;
  pInstance->length = 0;
  pInstance->offset = 0;
}

/**
 * Set the buffer to use for receive data
 * @param pInstance The instance
 * @param pBuffer The buffer
 * @param size The size of the buffer
 */
void GenericReceiveSetBuffer(GenericReceive_t* const pInstance,
                             uint8_t* pBuffer, const uint32_t size) {
  pInstance->pBuffer = pBuffer;
  pInstance->bufferSize = size;
}

/**
 * Clear the received data and prepare for new data
 * @param pInstance The instance
 */
void GenericReceiveClear(GenericReceive_t* const pInstance) {
  pInstance->length = 0;
  pInstance->offset = 0;
}

/**
 * Add data to the packet
 * @param pInstance The instance
 * @param data The data to be added
 * @param count The length of the data
 * @return If the data could be added
 */
bool GenericReceiveAddData(GenericReceive_t* const pInstance,
                           const uint8_t* const data, const uint32_t count) {
  uint32_t rawCount = count - 1;
  const uint8_t* const rawData = &data[1];

  if (pInstance->pBuffer == NULL) {
    return false;
  }

  if ((pInstance->length + rawCount) > pInstance->bufferSize) {
    return false;
  }

  memcpy(&pInstance->pBuffer[pInstance->length], rawData, rawCount);
  pInstance->length += rawCount;

  return true;
}

/**
 * Check if the received packet is complete
 * @param pInstance The instance
 * @return If complete
 */
GenericReceiveError_t GenericReceiveIsComplete(
    GenericReceive_t* const pInstance) {
  pInstance->offset = 0;

  if (pInstance->pBuffer == NULL) {
    return GenericReceiveErrorInternal;
  }

  while (GenericReceiveIsCmdValid(pInstance) == GenericReceiveErrorOk) {
    if (!GenericReceiveNextCmd(pInstance)) {
      if (GenericReceiveGetCmdType(pInstance) == GenericTypeEndResp) {
        pInstance->offset = 0; // Reset offset to 0
        return GenericReceiveErrorOk;
      }

      return GenericReceiveErrorLength;
    }
  }

  return GenericReceiveIsCmdValid(pInstance);
}

/**
 * Check if the received command is valid
 * @param pInstance The instance
 * @return If valid
 */
GenericReceiveError_t GenericReceiveIsCmdValid(
    GenericReceive_t* const pInstance) {
  uint16_t length = GenericReceiveGetCmdLength(pInstance) + CMD_DATA_POS;
  uint16_t receivedCrc;
  uint16_t crc16;

  if (pInstance->pBuffer == NULL) {
    return GenericReceiveErrorInternal;
  }

  if (pInstance->offset + length > pInstance->length) {
    return GenericReceiveErrorLength;
  }

  GenericReadU16(pInstance->offset, &receivedCrc, pInstance->pBuffer);
  crc16 = Crc16(&pInstance->pBuffer[pInstance->offset + CRC_SIZE],
      length - CRC_SIZE);

  if (crc16 != receivedCrc) {
    return GenericReceiveErrorCrc;
  }

  return GenericReceiveErrorOk;
}

/**
 * Get the command type
 * @param pInstance The instance
 * @return The type
 */
GenericType_t GenericReceiveGetCmdType(
    GenericReceive_t* const pInstance) {
  uint16_t type;

  if (pInstance->pBuffer == NULL) {
    return GenericTypeInvalid;
  }

  GenericReadU16(pInstance->offset + CRC_SIZE, &type,
                 pInstance->pBuffer);

  return (GenericType_t)type;
}

/**
 * Step to next command
 * @param pInstance The instance
 * @return If next more commands
 */
bool GenericReceiveNextCmd(GenericReceive_t* const pInstance) {
  pInstance->offset += GenericReceiveGetCmdLength(pInstance) +
      CMD_DATA_POS;

  if (pInstance->length > pInstance->offset) {
    GenericType_t type = GenericReceiveGetCmdType(pInstance);

    if (type == GenericTypeEndResp) {
      GenericReceiveError_t error = GenericReceiveIsCmdValid(pInstance);

      if (error == GenericReceiveErrorOk) {
        return false;
      }
    }

    return true;
  }

  return false;
}

/**
 * Decode the received data
 * @param pInstance The instance
 * @param fields The message fields
 * @param src_struct The message
 * @return If the operation was successful
 */
bool GenericReceiveDecode(GenericReceive_t* const pInstance,
                          const pb_field_t fields[], void* src_struct) {
  uint8_t* data = GenericReceiveGetCmdData(pInstance);
  uint16_t length = GenericReceiveGetCmdLength(pInstance);
  pb_istream_t stream = pb_istream_from_buffer(data, length);

  return pb_decode(&stream, fields, src_struct);
}

/**
 * Decode the sub-stream
 * @param pInstance The instance
 * @param fields The message fields
 * @param src_struct The message
 * @return If the operation was successful
 */
bool GenericReceiveDecodeSubstream(GenericReceive_t* const pInstance,
                          const pb_field_t fields[], void* src_struct) {
  return pb_decode(&pInstance->substream, fields, src_struct);
}

/**
 * Get substream from request with sub-messages
 * @param pInstance The instance
 * @param fields The message fields
 * @return If the operation was successful
 */
bool GenericReceiveGetSubstreamFromRequest(GenericReceive_t* const pInstance,
                                           const pb_field_t fields[]) {
  uint8_t* data = GenericReceiveGetCmdData(pInstance);
  uint16_t length = GenericReceiveGetCmdLength(pInstance);
  const pb_field_t* type;

  pInstance->stream = pb_istream_from_buffer(data, length);
  type = GenericReceiveDecodeUnionMessageType(&pInstance->stream, fields);

  if (type == NULL)
    return false;

  if (!pb_make_string_substream(&pInstance->stream, &pInstance->substream))
      return false;

  return true;
}

// Private functions

/**
 * Get the command data
 * @param pInstance The instance
 * @return Pointer to data
 */
static uint8_t* GenericReceiveGetCmdData(
    GenericReceive_t* const pInstance) {
  if (pInstance->pBuffer == NULL) {
    return NULL;
  }

  return &pInstance->pBuffer[pInstance->offset + CMD_DATA_POS];
}

/**
 * Get the command length
 * @param pInstance The instance
 * @return The length
 */
static uint16_t GenericReceiveGetCmdLength(
    GenericReceive_t* const pInstance) {
  uint16_t length;

  if (pInstance->pBuffer == NULL) {
    return 0;
  }

  GenericReadU16(pInstance->offset + CRC_SIZE + CMD_TYPE_SIZE, &length,
                 pInstance->pBuffer);

  return length;
}

/**
 * Decode the union message type
 */
static const pb_field_t* GenericReceiveDecodeUnionMessageType(
    pb_istream_t *stream, const pb_field_t fields[])
{
    pb_wire_type_t wire_type;
    uint32_t tag;
    bool eof;

    while (pb_decode_tag(stream, &wire_type, &tag, &eof))
    {
        if (wire_type == PB_WT_STRING)
        {
            const pb_field_t *field;
            for (field = fields; field->tag != 0; field++)
            {
                if (field->tag == tag && (field->type & PB_LTYPE_SUBMESSAGE))
                {
                    /* Found our field. */
                    return field->ptr;
                }
            }
        }

        /* Wasn't our field.. */
        pb_skip_field(stream, wire_type);
    }

    return NULL;
}
