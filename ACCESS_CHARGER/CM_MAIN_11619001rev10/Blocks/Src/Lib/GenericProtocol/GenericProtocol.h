#pragma once

// Public definitions

#define PROTOCOL_VERSION 2

#define CRC_SIZE 2
#define CMD_TYPE_SIZE 2
#define CMD_LENGTH_SIZE 2
#define CMD_TYPE_POS CRC_SIZE
#define CMD_DATA_POS (CRC_SIZE + CMD_TYPE_SIZE + CMD_LENGTH_SIZE)
#define END_CMD CMD_DATA_POS

// Public enumerations

typedef enum {
  GenericTypeInvalid = 0,
  GenericTypeAcknowledgeResp = 1,
  GenericTypeReadReq = 2,
  GenericTypeWriteReq = 3,
  GenericTypeBmuStatusResp = 4,
  GenericTypeInfoResp = 5,
  GenericTypeBmuLogResp = 6,
  GenericTypeBmuConfigResp = 7,
  GenericTypeCapabilityResp = 8,
  GenericTypeEndResp = 9,
  GenericTypeChargerStatusResp = 10,
  GenericTypeSharpConfigResp = 11,
  GenericTypeChargerLogResp = 12,
  GenericTypeParameterResp = 13,
  GenericTypeAccessConfigResp = 14,
  GenericTypeAccessStatusResp = 15,
  GenericTypeChargingCurveResp = 16,
  GenericTypeUserParameterResp = 17
} GenericType_t;

typedef enum {
  eDeviceStatusUnknown = -1,
  eDeviceStatusCharging = 1,
  eDeviceStatusEqualizing,
  eDeviceStatusNeedEqualize,
  eDeviceStatusWatering,
  eDeviceStatusNeedWatering,
  eDeviceStatusChargerConnected,
  eDeviceStatusRadioConnected,
  eDeviceStatusRadioJoining,
  eDeviceStatusRadioStarting,
  eDeviceStatusRadioJoinEnable,
  eDeviceStatusRemoteIn,
  eDeviceStatusPanelSwitch,
  eDeviceStatusBatteryConnected,
  eDeviceStatusPaused,
  eDeviceStatusRemoteOff,
  eDeviceStatusMainsConnected,
  eDeviceStatusBmuConnected,
  eDeviceStatusMainCharging,
  eDeviceStatusAdditionalCharging,
  eDeviceStatusMaintenanceCharging,
  eDeviceStatusChargingCompleted,
  eDeviceStatusPreCharging,
  eDeviceStatusForceStart,
  eDeviceStatusLowBatteryTemperature,
  eDeviceStatusHighBatteryTemperature,
  eDeviceStatusChargingRestricted,
  eDeviceStatusBbc,
  eDeviceStatusNeedCalibration,
  eDeviceStatusShuntTemperatureConnected,
  eDeviceStatusPhaseDerate,
  eDeviceStatusUserParamDerate,
  eDeviceStatusEngineDerate,
  eDeviceStatusTemperatureDerate,
  eDeviceStatusNoLoadDerate,
  eDeviceStatusLowVoltageDerate,
  eDeviceStatusHighVoltageDerate,
  eDeviceStatusVoltageIsNearSetValue,
  eDeviceStatusCurrentIsNearSetValue,
  eDeviceStatusPowerIsNearSetValue,
  eDeviceStatusLimitSetVoltage,
  eDeviceStatusLimitSetCurrent,
  eDeviceStatusLimitSetPower,
  eDeviceStatusLimitTemperature,
  eDeviceStatusLimitEngineVoltage,
  eDeviceStatusLimitEngineCurrent,
  eDeviceStatusLimitEnginePower,
  eDeviceStatusLimitChargingCurrent,
  eDeviceStatusLimitMainsCurrent,
  eDeviceStatusLimitMainsPower,
  eDeviceStatusLimitPhase
} eDeviceDeviceStatus_t;

typedef enum {
  eDeviceAlarmUnknown = -1,
  eDeviceAlarmLowBatteryVoltage = 1,
  eDeviceAlarmHighBatteryVoltage,
  eDeviceAlarmTimeLimitExceeded,
  eDeviceAlarmAboveAhMax,
  eDeviceAlarmInvalidParameters,
  eDeviceAlarmIncorrectMount,
  eDeviceAlarmHighBatteryLimitError,
  eDeviceAlarmBatteryError,
  eDeviceAlarmLowAirPressure,
  eDeviceAlarmHighAirPressure,
  eDeviceAlarmPhaseDeviceAlarm = 17,
  eDeviceAlarmRegulatorError,
  eDeviceAlarmLowChargerTemperature,
  eDeviceAlarmHighChargerTemperature,
  eDeviceAlarmLowBoardTemperature,
  eDeviceAlarmHighBoardTemperature,
  eDeviceAlarmHighTransformerTemperature,
  eDeviceAlarmCanTimeout,
  eDeviceAlarmSlavePhaseDeviceAlarm,
  eDeviceAlarmSlaveRegulatorError,
  eDeviceAlarmSlaveLowChargerTemperature,
  eDeviceAlarmSlaveHighChargerTemperature,
  eDeviceAlarmSlaveLowBoardTemperature,
  eDeviceAlarmSlaveHighBoardTemperature,
  eDeviceAlarmSlaveHighTransformerTemperature,
  eDeviceAlarmSlaveCanTimeout,
  eDeviceAlarmBmAlgorithmError,
  eDeviceAlarmBmInitFail,
  eDeviceAlarmChargerAddressConflict,
  eDeviceAlarmInternalFlashCorrupted,
  eDeviceAlarmRadioError,
  eDeviceAlarmVoltageSenseDeviceAlarm,
  eDeviceAlarmVoltageSenseHigh,
  eDeviceAlarmVoltageSenseLow,
  eDeviceAlarmWatchdogError,
  eDeviceAlarmBmInit1Fail,
  eDeviceAlarmDplNoMaster,
  eDeviceAlarmDplMasterConflict,
  eDeviceAlarmHighBatteryTemperature = 101,
  eDeviceAlarmLowElectrolyteLevel,
  eDeviceAlarmVoltageBalanceDeviceAlarm,
  eDeviceAlarmTimeNotSet,
  eDeviceAlarmLowStateOfCharge,
  eDeviceAlarmAddressConflict,
  eDeviceAlarmHardwareOrSoftwareDeviceAlarm = 116
} eDeviceAlarm_t;
