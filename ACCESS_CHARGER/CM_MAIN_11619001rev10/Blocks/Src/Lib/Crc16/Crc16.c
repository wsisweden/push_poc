#include "Crc16.h"
#include <stdbool.h>

/**
 * Calculates the CRC
 * @param data The data
 * @param length The data length
 * @return The CRC value
 */
uint16_t Crc16(const uint8_t* const data, const uint32_t length) {
  uint16_t crc = 0xffff;
  uint_fast32_t i, j;

  for (i = 0; i < length; ++i) {
    crc ^= (uint16_t)data[i];

    for (j = 0; j < 8; ++j) {
      bool lsbSet = (crc & 1) == 1;

      crc >>= 1;
      if (lsbSet) {
        crc ^= 0xa001;
      }
    }
  }

  return crc;
}
