/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Cm.c
*
*	\ingroup	CM
*
*	\brief		Main source file of the Log writer.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	CM		CM
*
*	\brief		Log writer
*
********************************************************************************
*
*	\details	The log writer handles all log writes to the flash memory.
*				It also takes care of reading log entries.
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>

#include "tools.h"

#include "global.h"

#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "mem.h"
#include "lib.h"
#include "deb.h"
#include "util.h"

#include "a25lx.h"
#include "Cm.h"
#include "Sup.h"

#include "init.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Task execution triggers
 */

/*@{*/
#define CM__TRG_RESET		(1<<0)		/**< Power up pending.				*/
#define CM__TRG_DONE		(1<<1)		/**< Request has been handled.		*/
#define CM__TRG_DOWN		(1<<2)		/**< Down mode pending.				*/
#define CM__TRG_READ		(1<<3)		/**< Read log pending.				*/
#define CM__TRG_SANITY		(1<<4)		/**< Perform sanity check.			*/
#define CM__TRG_UPDATESTAT	(1<<5)		/**< Update log index for statistics.	*/
/*@}*/

/**
 * \name	Task state flags
 */

/*@{*/
#define CM__FLG_RUNNING		(1<<0)		/**< CM is running.					*/
#define CM__FLG_ERROR		(1<<1)		/**< Fatal error with FLASH memory.	*/
#define CM__FLG_BUSY		(1<<2)		/**< CM is busy handling a request.	*/
#define CM__FLG_LSEARCH		(1<<3)		/**< Performing linear search		*/
#define CM__FLG_ERASE		(1<<4)		/**< Performing sector erase.		*/
#define CM__FLG_READ		(1<<5)		/**< Reading a record				*/
#define CM__FLG_WRITE		(1<<6)		/**< Writing a record.				*/
#define CM__FLG_BSEARCH		(1<<7)		/**< Performing binary search.		*/
#define CM__FLG_BSEARCH2	(1<<8)		/**< Performing binary search.		*/
#define CM__FLG_RLOCK		(1<<9)		/**< Read request locked.			*/
#define CM__FLG_WRITETSTMP	(1<<10)		/**< Writing instant timestamp		*/
#define CM__FLG_SANITY		(1<<11)		/**< Performing sanity check.		*/
#define CM__FLG_CLEAR		(1<<12)		/**< Log clearing in progress		*/
#define CM__FLG_CLEARINST	(1<<13)		/**< InstLog clearing in progress	*/
#define CM__FLG_ERASEALL	(1<<14)		/**< Erase all flash				*/

#define CM__FLG_BUSYMASK	(CM__FLG_LSEARCH|CM__FLG_ERASE|CM__FLG_READ|CM__FLG_WRITE|CM__FLG_BSEARCH|CM__FLG_BSEARCH2|CM__FLG_WRITETSTMP|CM__FLG_SANITY|CM__FLG_CLEAR|CM__FLG_CLEARINST|CM__FLG_ERASEALL)		/**< Requests that set CM__FLG_BUSY	*/
/*@}*/

/**
 * Sanity check steps.
 */

enum cm__sanitySteps
{
	CM__SANITY_READHIST,				/**< Finding history log end		*/
	CM__SANITY_READINST,				/**< Finding instant log end		*/
	CM__SANITY_READEVENT				/**< Finding event log end			*/
};

/**
 * Clear log steps.
 */

enum cm__clearSteps
{
	CM__CLEAR_START = 0,				/**< Initializing log clearing		*/
	CM__CLEAR_HISTORICAL,				/**< Erasing historical log			*/
	CM__CLEAR_MOMENTARY,				/**< Erasing momentary log			*/
	CM__CLEAR_EVENT						/**< Erasing event log				*/
};

/**
 *	Sector erase steps.
 */

enum cm__eraseSteps
{
	CM__ERASE_START = 0,				/**< Initializing sector erase		*/
	CM__ERASE_COUNTING,					/**< Counting sector records		*/
	CM__ERASE_,				/**< 		*/
	CM__ERASE_ERASING						/**< 				*/
};

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/
/**
 * Resets the temporary read data.
 */

#define cm__resetTmpReadData(pInst_)			\
{												\
	(pInst_)->tmpReadData.chargedAh = 0;		\
	(pInst_)->tmpReadData.startDate = 0;		\
	(pInst_)->tmpReadData.startTime = 0;		\
	(pInst_)->tmpReadData.BID = 0;				\
	(pInst_)->tmpReadData.chargedAhP = 0;		\
	(pInst_)->tmpReadData.AlgNoName[0] = 3;		\
	(pInst_)->tmpReadData.AlgNoName[1] = 'N';	\
	(pInst_)->tmpReadData.AlgNoName[2] = '/';	\
	(pInst_)->tmpReadData.AlgNoName[3] = 'A';	\
	(pInst_)->tmpReadData.capacity = 0;			\
	(pInst_)->tmpReadData.cells = 0;			\
	(pInst_)->tmpReadData.cableRi = 0;			\
	(pInst_)->tmpReadData.baseload = 0;			\
	(pInst_)->tmpReadData.ChalgErrorSum = 0;	\
	(pInst_)->tmpReadData.ReguErrorSum = 0;		\
	(pInst_)->tmpReadData.startVoltage = 0;		\
	(pInst_)->tmpReadData.endVoltage = 0;		\
	(pInst_)->tmpReadData.ChargingMode = 0;		\
	(pInst_)->tmpReadData.ChalgStatusSum = 0;	\
}

/**
 * Gives the last address of the memory area.
 */

#define cm__getLastAreaAddress(pArea_) \
	(pArea_)->startAddr + ((pArea_)->count * (pArea_)->sectorSize) - 1


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Temporary log data that is used when reading log with the cylcleNo register.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	Uint32			chargedAh;			/**<		*/
	Uint32			startDate;			/**<		*/
	Uint32			startTime;			/**<		*/
	Uint32			BID;				/**<		*/
	Uint16			chargedAhP;			/**<		*/
	char			AlgNoName[9];		/**<		*/
	Uint16			capacity;			/**<		*/
	Uint16			cells;				/**<		*/
	Uint16			cableRi;			/**<		*/
	Uint16			baseload;			/**<		*/
	Uint16			ChalgErrorSum;		/**<		*/
	Uint16			ReguErrorSum;		/**<		*/
	Uint32			startVoltage;		/**<		100924 jakob*/
	Uint32			endVoltage;			/**<		100924 jakob*/
	Uint8			ChargingMode;		/**<		*/
	Uint16			ChalgStatusSum;		/**<		*/
} cm__TmpReadData;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type for binary search state information.
 */

typedef struct cm__bsearch {			/*''''''''''''''''''''''''''''' RAM	*/
	Uint32					midValue2;	/**< Record index after middle sect.*/
	Uint8					highIdx;	/**< High sector index.				*/
	Uint8					lowIdx;		/**< Low sector index.				*/
	Uint8					midIdx1;	/**< Middle sector index.			*/
} cm__BSearchInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * CM function block instance type.
 */
 
typedef struct cm__inst {				/*''''''''''''''''''''''''''''' RAM	*/
	osa_CoTaskType			coTask;		/**< CM main task.					*/
	memdrv_Alloc			drvRequest;	/**< The driver request.			*/
	lib_FifoType			reqFifo;	/**< FIFO containing requests.		*/
	cm__BSearchInfo			bSearchInfo;/**< Binary search info.			*/
	cm_Request				readReq;	/**< GUI read request.				*/
	cm__TmpReadData			tmpReadData;/**< Temp read data.				*/
	cm_Init const_P * 		pInit;		/**< Initialization data.			*/
	cm_Request *			pActReq;	/**< Pointer to the active request.	*/
	Uint32					nextSect;	/**< Address of next sector.		*/
	BYTE					buff[sizeof(cm_HistRecStd)];/**< Buffer 		*/
	Uint16					flags;		/**< Task state flags.				*/
	Uint16					counter;
	sys_FBInstId			instId;		/**< Instance id of the FB.			*/
	Uint8					triggers;	/**< Task triggers.					*/
	Uint8					instRecSize;/**< Current instant record size.	*/
	Uint8					currLogType;/**< Log type for current operation.*/
	Uint8					step;		/**< Current operation step.		*/
} cm__Inst;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE reg_Status cm__validateLogInfo(sys_RegHandle,void const_D *,void const_P *);
PRIVATE void	cm__coTask(osa_CoTaskType *);
PRIVATE void	cm__drvCallback(memdrv_Alloc *);
PRIVATE void	cm__fillInstLogTStamp(cm__Inst *,cm_InstLogTstamp *);
PRIVATE void	cm__doQuickSanityCheck(cm__Inst *);

PRIVATE void	cm__read(cm__Inst *,cm_Request *);
PRIVATE void	cm__readNext(cm__Inst *,cm_Request *);
PRIVATE void  cm__readPrevious(cm__Inst *,cm_Request *);
PRIVATE void	cm__write(cm__Inst *,cm_Request *);
PRIVATE void	cm__eraseIfNeeded(cm__Inst *,cm__LogInfoType *,cm_LogAreaInfo const_P *);
PRIVATE Boolean cm__checkIfEntryFits(cm__LogInfoType *,cm_LogAreaInfo const_P *,Uint32);
PRIVATE Boolean cm__checkIfEntryFitsPrevious(cm__LogInfoType *,cm_LogAreaInfo const_P *,Uint32);

PRIVATE void	cm__stateMachine(cm__Inst *);
PRIVATE void	cm__handleBSearch(cm__Inst *);
PRIVATE void	cm__handleLSearch(cm__Inst *);
PRIVATE void	cm__handleErase(cm__Inst *);
PRIVATE void	cm__handleEraseAll(cm__Inst *);
PRIVATE void	cm__handleReadDone(cm__Inst *);
PRIVATE void	cm__handleWriteDone(cm__Inst *);
PRIVATE void 	cm__handleSanityCheck(cm__Inst *);
PRIVATE void	cm__handleClear(cm__Inst *);
PRIVATE void	cm__handleClearInst(cm__Inst *);

PRIVATE Uint8	cm__getEvtRecordSize(cm_EvtLogHdr *);
PRIVATE Uint8	cm__getHistRecordSize(cm_HistLogHdr *);
PRIVATE Uint8	cm__getMomRecordSize(BYTE *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG
tstamp_Stamp cm_lastRun;
Uint32 cm_counter = 0;
#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static int clearStatDone = FALSE;
static int flashEraseDone = FALSE;
/**
 * Tools for log info registers.
 */

PUBLIC sys_TypeTools		cm__LogInfoTools = {
	sizeof(cm__LogInfoType),
	&cm__validateLogInfo
};

/**
 * Pointer to the only instance of this function block.
 */

PRIVATE cm__Inst *			cm__pInst = NULL;

/** 
 *	Conversion table from historical record type to record size.
 */

PRIVATE Uint8 cm__histReqSizes[] = {
	0,
	sizeof(cm_HistRecStd)
};

/** 
 *	Conversion table from event ID to record size.
 */

PRIVATE Uint8 cm__evtReqSizes[] = {
	0,									/**< Dummy size						*/
	sizeof(cm_EvtRecTimeSet),			/**< CM_EVENT_TIMESET				*/
	sizeof(cm_EvtRecChargeStarted),		/**< CM_EVENT_CHARGESTARTED			*/
	sizeof(cm_EvtRecChargeEnded),		/**< CM_EVENT_CHARGEENDED			*/
	sizeof(cm_EvtRecChalgError),		/**< CM_EVENT_CHALGERROR			*/
	sizeof(cm_EvtRecReguError),			/**< CM_EVENT_REGUERROR				*/
	sizeof(cm_EvtRecCurveData),			/**< CM_EVENT_CURVEDATA				*/
	sizeof(cm_EvtRecVcc),				/**< CM_EVENT_VCC					*/
	sizeof(cm_EvtRecVolCalib),			/**< CM_EVENT_VOLTAGECALIBRATION	*/
	sizeof(cm_EvtRecCurrCalib),			/**< CM_EVENT_CURRENTCALIBRATION	*/
	sizeof(cm_EvtRecStartNet),			/**< CM_EVENT_STARTNETWORK			*/
	sizeof(cm_EvtRecJoinNet),			/**< CM_EVENT_JOINNETWORK			*/
	sizeof(cm_EvtRecStartUp),			/**< CM_EVENT_STARTUP				*/
	sizeof(cm_EvtRecSettings),			/**< CM_EVENT_SETTINGS				*/
	sizeof(cm_EvtRecAssert),			/**< CM_EVENT_ASSERT				*/
	sizeof(cm_EvtRecSupError),			/**< CM_EVENT_SUPERROR				*/
	sizeof(cm_EvtRecCableRiSlope)		/**< CM_EVENT_CABLERISLOPE			*/
};

/**
 * Declaring a variable containing the file name so that it only has to be
 * stored once even if it is used several times in this file.
 */

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid		An index (0..) that is accepted by various functions
*						to identify the instance. Should be kept for later use,
*						or may be discarded if not required by the FB.
*	\param		pArgs	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * cm_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	deb_assert(dim(cm__evtReqSizes) == CM_EVENT__COUNT);


	cm__Inst *				pInst;

	pInst = (cm__Inst *) mem_reserve(&mem_normal, sizeof(cm__Inst));

	osa_newCoTask(
		&pInst->coTask,
		"cm__coTask",
		OSA_PRIORITY_NORMAL,
		cm__coTask,
		1024
	);

	/* 
	 * Set initial values for instance variables.
	 */

	pInst->pInit = pArgs;
	pInst->flags = 0;
	pInst->triggers = CM__TRG_SANITY;
	pInst->pActReq = NULL;
	pInst->instId = iid;
	pInst->step = 0;

	cm__resetTmpReadData(pInst);

	/*
	 * Set FLASH driver request settings that will stay the same.
	 */

	pInst->drvRequest.fnDone = &cm__drvCallback;

	/*
	 * There can only be one instance of this FB. Store the instance pointer
	 * here.
	 */

	deb_assert(cm__pInst == NULL);
	cm__pInst = pInst;

	/*
	 * Initialize request FIFO
	 */

	lib_fifoInit(&pInst->reqFifo);

	return((void *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		pInstance	Pointer to instance.
* 	\param		pArgs		Pointer to the initialization arguments.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void cm_reset(
	void *					pInstance,
	void const_P *			pArgs
) {	
	cm__Inst *				pInst;

	pInst = (cm__Inst *) pInstance;

	pInst->triggers |= CM__TRG_RESET;
	osa_coTaskRun(&pInst->coTask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		pInstance	Pointer to instance.
*	\param		nType		Down type.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void cm_down(
	void *					pInstance, 
	sys_DownType			nType
) {
	cm__Inst *				pInst;

	pInst = (cm__Inst *) pInstance;

	atomic(
		pInst->flags &= ~CM__FLG_RUNNING;
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read
*				the value of a parameter owned by this FB.
*
*	\param		pInstance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue		Pointer to the storage for the value.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus cm_read(
	void *					pInstance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex,
	void *					pValue
) {	
	cm__Inst *				pInst;
	sys_IndStatus			retVal;

	DUMMY_VAR(aIndex);

	pInst = (cm__Inst *) pInstance;
	retVal = REG_OK;

	switch(rIndex)
	{
		case reg_indic_histLogCount:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
			*(Uint16 *) pValue = logInfo.recCount;
			break;
		}

		case reg_indic_eventLogCount:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
			*(Uint16 *) pValue = logInfo.recCount;
			break;
		}

		case reg_indic_instLogCount:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
			*(Uint16 *) pValue = logInfo.recCount;
			break;
		}

		case reg_indic_histLogIndex:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
			*(Uint32 *) pValue = logInfo.recTotal;
			break;
		}

		case reg_indic_eventLogIndex:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
			*(Uint32 *) pValue = logInfo.recTotal;
			break;
		}

		case reg_indic_instLogIndex:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
			*(Uint32 *) pValue = logInfo.recTotal;
			break;
		}

		case reg_indic_histLogIndexReset:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
			*(Uint32 *) pValue = logInfo.recReset;
			break;
		}

		case reg_indic_eventLogIndexReset:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
			*(Uint32 *) pValue = logInfo.recReset;
			break;
		}

		case reg_indic_instLogIndexReset:
		{
			cm__LogInfoType logInfo;
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
			*(Uint32 *) pValue = logInfo.recReset;
			break;
		}

		case reg_indic_chargedAh:
			*(Uint32 *) pValue = pInst->tmpReadData.chargedAh;
			break;

		case reg_indic_chargedAhP:
			*(Uint16 *) pValue = pInst->tmpReadData.chargedAhP;
			break;

		case reg_indic_startDate:
			*(Uint32 *) pValue = pInst->tmpReadData.startDate;
			break;

		case reg_indic_startTime:
			*(Uint32 *) pValue = pInst->tmpReadData.startTime;
			break;
		/* 100924 jakob */
		case reg_indic_startVoltage:
			*(Uint32 *) pValue = pInst->tmpReadData.startVoltage;
			break;
			/* 100924 jakob */
		case reg_indic_endVoltage:
			*(Uint32 *) pValue = pInst->tmpReadData.endVoltage;
			break;

		case reg_indic_BID:
			*(Uint32 *) pValue = pInst->tmpReadData.BID;
			break;

		case reg_indic_BIDString:
		{
			char * pString;
			char * pEnd;

			pString = (char *) pValue;
			
			if (pInst->tmpReadData.BID == 0)
			{
				strcpy(&pString[1], "Default");
				pString[0] = 7;
			}
			else
			{
				pString[1] = 'B';
				pString[2] = '-';
				pString[3] = 'I';
				pString[4] = 'D';
				pString[5] = ' ';

				pEnd = util_ltoaEx(
					&pString[6],
					&pString[20],
					pInst->tmpReadData.BID
				);

				pString[0] = pEnd - pString; 
			}
			break;
		}

		case reg_indic_AlgNoName:
			memcpy(
				pValue,
				&pInst->tmpReadData.AlgNoName,
				pInst->tmpReadData.AlgNoName[0] + 1
			);
			break;

		case reg_indic_capacity:
			*(Uint16 *) pValue = pInst->tmpReadData.capacity;
			break;

		case reg_indic_cells:
			*(Uint16 *) pValue = pInst->tmpReadData.cells;
			break;

		case reg_indic_cableRi:
			*(Uint16 *) pValue = pInst->tmpReadData.cableRi;
			break;

		case reg_indic_baseload:
			*(Uint16 *) pValue = pInst->tmpReadData.baseload;
			break;

		case reg_indic_ChalgErrorSum:
			*(Uint16 *) pValue = pInst->tmpReadData.ChalgErrorSum;
			break;

		case reg_indic_ReguErrorSum:
			*(Uint16 *) pValue = pInst->tmpReadData.ReguErrorSum;
			break;

		case reg_indic_ChargingMode:
			*(Uint8 *) pValue = pInst->tmpReadData.ChargingMode;
			break;

		case reg_indic_ChalgStatusSum:
			*(Uint16 *) pValue = pInst->tmpReadData.ChalgStatusSum;
			break;

		case reg_indic_AlarmCount:
		{
			Uint8 * pAlarmCount;
			Uint32	errorSum;

			/*
			 * Calculate how many bits are set in the error sum registers.
			 */

			pAlarmCount = (Uint8 *) pValue;
			errorSum = pInst->tmpReadData.ChalgErrorSum;
			errorSum |= pInst->tmpReadData.ReguErrorSum << 16;
			*pAlarmCount = 0;

			while (errorSum != 0)
			{
				if (errorSum & 1)
				{
					(*pAlarmCount)++;
				}
				errorSum >>= 1;
			}
			break;
		}

		default:
			retVal = REG_UNAVAIL;
			break;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		pInstance	Pointer to the data of the instance.
*	\param		rIndex		Register index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus cm_write(
	void *					pInstance, 
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex
) {
	cm__Inst *				pInst;

	DUMMY_VAR(aIndex);

	pInst = (cm__Inst *) pInstance;

	switch (rIndex)
	{
		case reg_indic_cycleNo:
			atomic(
				pInst->triggers |= CM__TRG_READ;		
			);
			osa_coTaskRun(&pInst->coTask);
			break;

		case reg_indic_histLogInfo:
			atomic(
				pInst->triggers |= CM__TRG_UPDATESTAT;
			);
			osa_coTaskRun(&pInst->coTask);
			break;

	}

	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__validateLogInfo
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Validate the log info
*
*	\param		regHandle	The register handle.
*	\param		pData		Pointer to the data.
*	\param		pMinMax		Pointer to minimum and maximum values.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE reg_Status cm__validateLogInfo(
	sys_RegHandle			regHandle,
	void const_D *			pData,
	void const_P *			pMinMax
) {
	return(REG_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm_processRequest
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Queue request for handling.
*	
*	\param		pRequest	Pointer to the CM request.
*
*	\return		
*
*	\details	Adds the supplied request pointer to the request FIFO. CM
*				will call the request callback function when the
*				request has been handled if non-blocking operation is selected.
*
*	\note
*
*******************************************************************************/

PUBLIC void cm_processRequest(
	cm_Request *			pRequest
) {
	cm__Inst *				pInst;

	pInst = cm__pInst;

	deb_assert(pRequest != NULL);

	lib_fifoPut(&pInst->reqFifo, (void **) &pRequest->pNext);

	osa_coTaskRun(&pInst->coTask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__drvCallback
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Callback function called by the FLASH memory driver when a 
*				request has been handled.
*	
*	\param		pRequest	Pointer to the FLASH memory request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__drvCallback(
	memdrv_Alloc *			pRequest
) {
	cm__Inst *				pInst;

	pInst = cm__pInst;

	atomic(
		pInst->triggers |= CM__TRG_DONE;
	);
	osa_coTaskRun(&pInst->coTask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm_task
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__coTask(
	osa_CoTaskType * 		pCoTask
)  {
	cm__Inst *				pInst;
	Uint8					triggers;

	pInst = cm__pInst;
	
#ifndef NDEBUG
	tstamp_getStamp(&cm_lastRun);
	cm_counter++;
#endif

	atomic(
		triggers = pInst->triggers;
		pInst->triggers = 0;
	);

	/***************************************************************************
	 * Check if FB is running or if it should be started.
	 */

	if (not(pInst->flags & CM__FLG_RUNNING))
	{
		if ((triggers & CM__TRG_RESET) && not(pInst->flags & CM__FLG_ERROR))
		{
			pInst->flags |= CM__FLG_RUNNING;
		}
		else
		{
			/*
			 * FB is not running and reset trigger was not set.
			 */

			atomic(
				pInst->triggers |= triggers;
			);
			return;
		}
	}

	/***************************************************************************
	 * Check if fatal error has occurred.
	 */

	if (pInst->flags & CM__FLG_ERROR)
	{
		pInst->flags &= ~CM__FLG_RUNNING;

		return;
	}

	/***************************************************************************
	 * Check if the memdrv request has been handled. 
	 */

	if (triggers & CM__TRG_DONE)
	{
		if (pInst->drvRequest.count == MEMDRV_ERROR)
		{
			pInst->flags |= CM__FLG_ERROR;
			return;
		}

		cm__stateMachine(pInst);
	}

	/***************************************************************************
	 * Check if startup sanity test is in progress.
	 */
	
	if (triggers & CM__TRG_SANITY)
	{
		cm__doQuickSanityCheck(pInst);
	}

	/***************************************************************************
	 * Check if next sector should be erased.
	 */

	if not(pInst->flags & CM__FLG_BUSY)
	{
		cm__LogInfoType				logInfo;
		cm_LogAreaInfo const_P *	pAreaInfo;

		reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
		pAreaInfo = &pInst->pInit->eventArea;
		pInst->currLogType = CM_LOGTYPE_EVENT;
		cm__eraseIfNeeded(pInst, &logInfo, pAreaInfo);

		if not(pInst->flags & CM__FLG_BUSY)
		{
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->instArea;
			pInst->currLogType = CM_LOGTYPE_INSTANT;
			cm__eraseIfNeeded(pInst, &logInfo, pAreaInfo);
		}

		if not(pInst->flags & CM__FLG_BUSY)
		{
			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->histArea;
			pInst->currLogType = CM_LOGTYPE_HISTORICAL;
			cm__eraseIfNeeded(pInst, &logInfo, pAreaInfo);
		}
	}

	/***************************************************************************
	 * Check if read trigger is set.
	 */

	if ((triggers & CM__TRG_READ) && not(pInst->flags & CM__FLG_RLOCK))
	{
		Uint32 logIndex;
		Uint32 CycleNo;
		Uint32 ChargeIndex;
		Uint32 ChargeIndexReset;
		/*
		 * Log read has been requested by writing the cycleNo register.
		 */

		pInst->flags |= CM__FLG_RLOCK;

		reg_getLocal(&CycleNo, reg_i_cycleNo, pInst->instId);
		reg_getLocal(&ChargeIndex, reg_i_histLogIndex, pInst->instId);
		reg_getLocal(&ChargeIndexReset, reg_i_histLogIndexReset, pInst->instId);

		logIndex = (ChargeIndex - ChargeIndexReset) + CycleNo;

		pInst->readReq.flags = CM_REQFLG_READ;
		pInst->readReq.index = logIndex;
		pInst->readReq.logType = CM_LOGTYPE_HISTORICAL;
		pInst->readReq.pDoneFn = NULL;
		pInst->readReq.pRecord = pInst->buff;

		lib_fifoPut(&pInst->reqFifo, (void **) &pInst->readReq.pNext);
	}

	/***************************************************************************
	 * Check if index for statistics menu should be updated .
	 */

	if (triggers & CM__TRG_UPDATESTAT)
	{
		Uint32 CycleNo;
		/*
		 * Update index for cycle log.
		 */
		reg_getLocal(&CycleNo, reg_i_histLogIndexReset, pInst->instId);
		reg_putLocal(&CycleNo, reg_i_cycleNo, pInst->instId);

	}

	/***************************************************************************
	 * Check if a new request should be handled.
	 */

	if not(pInst->flags & CM__FLG_BUSY)
	{
		pInst->pActReq = (cm_Request *) lib_fifoGet(&pInst->reqFifo);

		if (pInst->pActReq != NULL)
		{
			pInst->flags |= CM__FLG_BUSY;

			if (pInst->pActReq->flags & CM_REQFLG_READ)
			{
				cm__read(pInst, pInst->pActReq);
			}
			else if (pInst->pActReq->flags & CM_REQFLG_READNEXT)
			{
				cm__readNext(pInst, pInst->pActReq);
			}
			else if (pInst->pActReq->flags & CM_REQFLG_READPREV)
			{
				cm__readPrevious(pInst, pInst->pActReq);
			}
			else if (pInst->pActReq->flags & CM_REQFLG_WRITE)
			{
				cm__write(pInst, pInst->pActReq);
			}
			else if (pInst->pActReq->flags & CM_REQFLG_CLEAR)
			{
				clearStatDone = FALSE;
				pInst->flags |= CM__FLG_CLEAR;
				pInst->step = CM__CLEAR_START;
				cm__handleClear(pInst);
			}
			else if (pInst->pActReq->flags & CM_REQFLG_CLEARINST)
			{
				pInst->flags |= CM__FLG_CLEARINST;
				pInst->step = CM__CLEAR_START;
				cm__handleClearInst(pInst);
			}
			else
			{
				/*
				 * No valid action flag in the request.
				 */

				deb_assert(FALSE);
			}
		}
	}
	else
	{
		if not(pInst->flags & CM__FLG_BUSYMASK)
		{
			pInst->flags &= ~(CM__FLG_BUSY | CM__FLG_RLOCK);
		}
	}

#ifndef NDEBUG
	cm_counter++;
#endif

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__doQuickSanityCheck
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Performs a quick sanity check on the non-volatile data.
*
*	\return		-
*
*	\details	Values that clearly are wrong will be changed to valid values.
*				The function does not check the actual log entries. It only
*				checks that the next entry address is valid.
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__doQuickSanityCheck(
	cm__Inst *				pInst
) {
	cm__LogInfoType 		logInfo;
	Boolean 				modified = FALSE;
	void * 					pDrvInst;

	/*
	 * Check log info addresses are inside the log area for the instant log.
	 */

	reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

	if (pInst->pInit->instArea.startAddr > logInfo.firstAddr)
	{
		logInfo.firstAddr = pInst->pInit->instArea.startAddr;
		modified = TRUE;
	}

	if (pInst->pInit->instArea.startAddr > logInfo.nextAddr)
	{
		logInfo.nextAddr = pInst->pInit->instArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.firstAddr > cm__getLastAreaAddress(&pInst->pInit->instArea))
	{
		logInfo.firstAddr = pInst->pInit->instArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.nextAddr > cm__getLastAreaAddress(&pInst->pInit->instArea))
	{
		logInfo.nextAddr = cm__getLastAreaAddress(&pInst->pInit->instArea);
		modified = TRUE;
	}

	if (modified)
	{
		reg_putLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
	}

	/*
	 * Check log info addresses are inside the log area for the event log.
	 */

	modified = FALSE;

	reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);

	if (logInfo.firstAddr < pInst->pInit->eventArea.startAddr)
	{
		logInfo.firstAddr = pInst->pInit->eventArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.nextAddr < pInst->pInit->eventArea.startAddr)
	{
		logInfo.nextAddr = pInst->pInit->eventArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.firstAddr > cm__getLastAreaAddress(&pInst->pInit->eventArea))
	{
		logInfo.firstAddr = pInst->pInit->eventArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.nextAddr > cm__getLastAreaAddress(&pInst->pInit->eventArea))
	{
		logInfo.nextAddr = cm__getLastAreaAddress(&pInst->pInit->eventArea);
		modified = TRUE;
	}

	if (modified)
	{
		reg_putLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
	}

	/*
	 * Check log info addresses are inside the log area for the historical log.
	 */

	modified = FALSE;

	reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);

	if (logInfo.firstAddr < pInst->pInit->histArea.startAddr)
	{
		logInfo.firstAddr = pInst->pInit->histArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.nextAddr < pInst->pInit->histArea.startAddr)
	{
		logInfo.nextAddr = pInst->pInit->histArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.firstAddr > cm__getLastAreaAddress(&pInst->pInit->histArea))
	{
		logInfo.firstAddr = pInst->pInit->histArea.startAddr;
		modified = TRUE;
	}

	if (logInfo.nextAddr > cm__getLastAreaAddress(&pInst->pInit->histArea))
	{
		logInfo.nextAddr = cm__getLastAreaAddress(&pInst->pInit->histArea);
		modified = TRUE;
	}

	if (modified)
	{
		reg_putLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
	}

	pInst->drvRequest.count = sizeof(cm_HistRecStd);
	pInst->drvRequest.pData = pInst->buff;
	pInst->drvRequest.address = logInfo.nextAddr;

	pInst->flags |= CM__FLG_BUSY | CM__FLG_SANITY | CM__FLG_RLOCK;
	pInst->counter = 0;
	pInst->step = CM__SANITY_READHIST;
	pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
	pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__eraseIfNeeded
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Erase the next sector if needed.
*
*	\param		pInst		Poninter to FB instance.
*	\param		pLogInfo	Pointer to the log info sturcture.
*	\param		pAreaInfo	Pointer to flash area information.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__eraseIfNeeded(
	cm__Inst *				pInst,
	cm__LogInfoType *		pLogInfo,
	cm_LogAreaInfo const_P * pAreaInfo
) {
	Uint32					usedSpace;

	usedSpace = pLogInfo->nextAddr % pAreaInfo->sectorSize;

	/*
	 * Checking if the largest possible record will fit.
	 */

	if (usedSpace + sizeof(cm_HistRecStd) + 1 > pAreaInfo->sectorSize)
	{
		Uint32 lastAreaAddr;
		Uint32 nextAddr;

		nextAddr = pLogInfo->nextAddr;

		/*
		 * The next record might not fit into the sector. Erase the
		 * next sector. If this is the last sector in the area then
		 * the first sector should be erased instead.
		 */

		nextAddr = (nextAddr - usedSpace) + pAreaInfo->sectorSize;
		lastAreaAddr = pAreaInfo->startAddr + (pAreaInfo->count * pAreaInfo->sectorSize) - 1;
		if (nextAddr > lastAreaAddr)
		{
			nextAddr = pAreaInfo->startAddr;
		}

		pInst->nextSect = nextAddr;

		//if (pLogInfo->firstAddr == nextAddr)
		//{
			/*
			 * The sector has not been erased yet. Erase it.
			 */

			pInst->flags |= CM__FLG_BUSY | CM__FLG_ERASE;
			pInst->step = CM__ERASE_START;
			cm__handleErase(pInst);
		//}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the supplied read request.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pRequest	Pointer to the CM request.
*
*	\return
*
*	\details	This will start a binary or linear search that searches for 
*				the requested log entry.
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__read(
	cm__Inst *				pInst,
	cm_Request *			pRequest
) {
	cm__LogInfoType			logInfo;
	void *					pDrvInst;
	cm_LogAreaInfo const_P * pAreaInfo = NULL;
	Uint8					headerSize = 0;
			
	switch(pRequest->logType)
	{
		case CM_LOGTYPE_HISTORICAL:
			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->histArea;
			headerSize = sizeof(cm_HistLogHdr);
			break;

		case CM_LOGTYPE_EVENT:
			reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->eventArea;
			headerSize = sizeof(cm_EvtLogHdr);
			break;

		case CM_LOGTYPE_INSTANT:
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->instArea;
			headerSize = sizeof(cm_InstLogTstamp);
			break;
	}
	
	pInst->bSearchInfo.lowIdx =
		(Uint8)
		(
			(logInfo.firstAddr - pAreaInfo->startAddr) /pAreaInfo->sectorSize
		);

	pInst->bSearchInfo.highIdx =
		(Uint8)
		(
			(
				(
					logInfo.nextAddr
					- (logInfo.nextAddr % pAreaInfo->sectorSize)
				) - pAreaInfo->startAddr
			) / pAreaInfo->sectorSize
		);

	if (pInst->bSearchInfo.lowIdx > pInst->bSearchInfo.highIdx)
	{
		/*
		 * Log has wrapped. Count as if there were more sectors
		 * in the log.
		 */

		pInst->bSearchInfo.highIdx += (Uint8) pAreaInfo->count;
	}

	if (pInst->bSearchInfo.lowIdx == pInst->bSearchInfo.highIdx)
	{
		DEB_MSG("Only one sector in use yet. Proceeding with linear search");
		
		/*
		 * Only one sector is in use yet. Start linear search directly.
		 */

		pInst->flags |= CM__FLG_LSEARCH;

		/*
		 * Setup memory driver request.
		 */

		pInst->drvRequest.address = pAreaInfo->startAddr;
		pInst->drvRequest.count = headerSize;
		pInst->drvRequest.pData = pInst->buff;

		/*
		 * Call driver.
		 */

		pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
		pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
	}
	else
	{
		Uint8 midIdx2;

		pInst->flags |= CM__FLG_BSEARCH;

		/*
		 * Pick two sectors in the middle of the area.
		 */

		pInst->bSearchInfo.midIdx1 =
			pInst->bSearchInfo.lowIdx + (
				(pInst->bSearchInfo.highIdx - pInst->bSearchInfo.lowIdx) >> 1
			);

		midIdx2 = pInst->bSearchInfo.midIdx1 + 1;

		if (midIdx2 >= pAreaInfo->count)
		{
			/*
			 * The second middle sector has wrapped to the beginning of the
			 * area.
			 */

			midIdx2 = (Uint8) (midIdx2 - pAreaInfo->count);
		}

		/*
		 * Setup memory driver request.
		 */

		pInst->drvRequest.address =
			pAreaInfo->startAddr + midIdx2 * pAreaInfo->sectorSize;
		pInst->drvRequest.count = headerSize;
		pInst->drvRequest.pData = pInst->buff;
		
		/*
		 * Call driver.
		 */

		pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
		pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__readNext
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the supplied read next request.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pRequest	Pointer to the CM request.
*
*	\return
*
*	\details	This will first read the header of the next record to determine
*				the size of the record.
*
*	\note
*
*******************************************************************************/
PRIVATE void cm__readNext(
	cm__Inst *				pInst,
	cm_Request *			pRequest
) {
	Uint32					address;
	void *					pDrvInst;

	{
		cm_LogAreaInfo const_P * pAreaInfo = NULL;			// Default address to avoid compilation warnings
		//sys_RegHandle			regHandle;
		cm__LogInfoType			logInfo;

		switch(pRequest->logType)
		{
			case CM_LOGTYPE_HISTORICAL:
				pInst->drvRequest.count = sizeof(cm_HistRecStd);
				pAreaInfo = &pInst->pInit->histArea;
				//regHandle = reg_i_histLogInfo;
				break;

			case CM_LOGTYPE_EVENT:
				pInst->drvRequest.count = sizeof(cm_EvtRecUnion);
				pAreaInfo = &pInst->pInit->eventArea;
				//regHandle = reg_i_eventLogInfo;
				break;

			case CM_LOGTYPE_INSTANT:
				pInst->drvRequest.count = sizeof(cm_InstRecStd);
				pAreaInfo = &pInst->pInit->instArea;
				//regHandle = reg_i_instLogInfo;
				break;
		}

		/*
		 *	The next record might be in the next sector. Move the address to the
		 *	first address of the next sector if there is no room for a record in
		 *	the current sector.
		 */

		logInfo.nextAddr = pRequest->currAddr + pRequest->size;
		cm__checkIfEntryFits(&logInfo, pAreaInfo, sizeof(cm_HistRecStd));
		address = logInfo.nextAddr;
	}

	/*
	 * Read the next record.
	 */

	pInst->drvRequest.address = address;
	pInst->drvRequest.pData = pRequest->pRecord;

	pInst->flags |= CM__FLG_READ;
	pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
	pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* cm__readPrevious
*
*--------------------------------------------------------------------------*//**
*
* \brief    Handle the supplied read previous request.
*
* \param    pInst   Pointer to FB instance.
* \param    pRequest  Pointer to the CM request.
*
* \return
*
* \details  This will first read the header of the previous record to determine
*       the size of the record.
*
* \note
*
*******************************************************************************/
PRIVATE void cm__readPrevious(
 cm__Inst *        pInst,
 cm_Request *      pRequest
) {
 Uint32          address;
 void *          pDrvInst;
 cm_LogAreaInfo const_P * pAreaInfo = NULL;
 cm__LogInfoType     logInfo;

 switch(pRequest->logType)
 {
   case CM_LOGTYPE_HISTORICAL:
     pInst->drvRequest.count = sizeof(cm_HistRecStd);
     pAreaInfo = &pInst->pInit->histArea;
     break;

   case CM_LOGTYPE_EVENT:
     pInst->drvRequest.count = sizeof(cm_EvtRecUnion);
     pAreaInfo = &pInst->pInit->eventArea;
     break;

   case CM_LOGTYPE_INSTANT:
     pInst->drvRequest.count = sizeof(cm_InstRecStd);
     pAreaInfo = &pInst->pInit->instArea;
     break;

   default:
     return;
 }

 /*
  *  The previous record might be in the previous sector. Move the address to
  *  the last address of the previous sector if there is no room for a record
  *  in the current sector.
  */

 logInfo.nextAddr = pRequest->currAddr - pRequest->size;

 if (cm__checkIfEntryFitsPrevious(&logInfo, pAreaInfo, pInst->drvRequest.count)) {
   cm__read(pInst, pRequest);
   return;
 }

 address = logInfo.nextAddr;

 /*
  * Read the next record.
  */

 pInst->drvRequest.address = address;
 pInst->drvRequest.pData = pRequest->pRecord;

 pInst->flags |= CM__FLG_READ;
 pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
 pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the supplied write request.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pRequest	Pointer to the CM request.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__write(
	cm__Inst *				pInst,
	cm_Request *			pRequest
) {
	cm__LogInfoType			logInfo;
	void *					pDrvInst;
	cm_LogAreaInfo const_P * pAreaInfo;
	Boolean					newSector = FALSE;

	switch(pRequest->logType)
	{
		case CM_LOGTYPE_HISTORICAL:
			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->histArea;
			break;

		case CM_LOGTYPE_EVENT:
			reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->eventArea;
			break;

		case CM_LOGTYPE_INSTANT:
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
			pAreaInfo = &pInst->pInit->instArea;
			if (*((BYTE *) pRequest->pRecord) & 0x80)
			{
				cm__fillInstLogTStamp(
					pInst,
					(cm_InstLogTstamp *) pRequest->pRecord
				);
				DEB_MSG("CM: Starting tstamp instant log record write");
			}
			break;

		default:
			deb_assert(FALSE);
			pAreaInfo = NULL;
			break;
	}
	

	/*
	 * Checking if the record will fit into the active sector.
	 */

	newSector = cm__checkIfEntryFits(&logInfo, pAreaInfo, sizeof(cm_HistRecStd));


	if (newSector && pRequest->logType == CM_LOGTYPE_INSTANT)
	{
		DEB_MSG("CM: Starting timestamp record write");

		/*
		 * A timestamp record should be written to the beginning of the
		 * new sector.
		 */

		pInst->flags |= CM__FLG_WRITETSTMP;

		cm__fillInstLogTStamp(
			pInst,
			(cm_InstLogTstamp *) ((void *) pInst->buff)
		);

		/*
		 * Setup memory driver request.
		 */

		pInst->drvRequest.address = logInfo.nextAddr;
		pInst->drvRequest.count = sizeof(cm_InstLogTstamp);
		pInst->drvRequest.pData = pInst->buff;
	}
	else
	{
		pInst->flags |= CM__FLG_WRITE;

		/*
		 * Setup memory driver request.
		 */

		pInst->drvRequest.address = logInfo.nextAddr;
		pInst->drvRequest.count = pRequest->size;
		pInst->drvRequest.pData = pRequest->pRecord;
	}

	/*
	 * Call driver.
	 */

	pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
	pInst->pInit->pDrvIf->put(pDrvInst, &pInst->drvRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__fillInstLogTStamp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Fills data into the supplied timestamp record.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pTstamp		Pointer to the timestamp record.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__fillInstLogTStamp(
	cm__Inst *				pInst,
	cm_InstLogTstamp *		pTstamp
) {
	pTstamp->RecordSync = 0x80;
	reg_getLocal(&pTstamp->Index, reg_i_instLogIndex, pInst->instId);
	pTstamp->Index++;
	reg_get(&pTstamp->Time, cm__LocalTime);
	reg_get(&pTstamp->SamplePeriod, cm__instLogInterval);
	pTstamp->RecordType = CM_INSTREC_STANDARD;
	pTstamp->RecordSize = sizeof(cm_InstRecStd);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__stateMachine
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Runs the state machine.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__stateMachine(
	cm__Inst *				pInst
) {

	if (pInst->flags & CM__FLG_LSEARCH)
	{
		cm__handleLSearch(pInst);
	}
	else if (pInst->flags & CM__FLG_BSEARCH)
	{
		cm__handleBSearch(pInst);
	}
	else if (pInst->flags & CM__FLG_ERASE)
	{
		cm__handleErase(pInst);
	}
	else if (pInst->flags & CM__FLG_ERASEALL)
	{
		cm__handleEraseAll(pInst);
	}
	else if (pInst->flags & CM__FLG_READ)
	{												
		cm__handleReadDone(pInst);										
	}
	else if (pInst->flags & CM__FLG_SANITY)
	{
		cm__handleSanityCheck(pInst);
	}
	else if (pInst->flags & CM__FLG_CLEAR)
	{
		cm__handleClear(pInst);
	}
	else if (pInst->flags & CM__FLG_CLEARINST)
	{
		cm__handleClearInst(pInst);
	}
	else if (pInst->flags & CM__FLG_WRITETSTMP)
	{
		cm__LogInfoType logInfo;

		pInst->flags &= ~CM__FLG_WRITETSTMP;
		
		DEB_MSG("CM: Timestamp record written, continue with standard instant record write");

		/*
		 * Update log info.
		 */

		reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
		logInfo.nextAddr = pInst->drvRequest.address + sizeof(cm_InstLogTstamp);
		reg_putLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

		cm__write(pInst, pInst->pActReq);
	}
	else if (pInst->flags & CM__FLG_WRITE)
	{
		cm__handleWriteDone(pInst);
	}

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleClear
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle a clear request.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pRequest	Pointer to the CM request.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleClear(
	cm__Inst *				pInst
) {
	switch (pInst->step)
	{
		case CM__CLEAR_START:
		{
			void * pDrvInst;

			DEB_MSG("Erasing first historical log sector");

			/*
			 * Erase first historical log sector.
			 */

			pInst->drvRequest.address = pInst->pInit->histArea.startAddr;
			pInst->drvRequest.count = 0;
			pInst->drvRequest.op = MEMDRV_OP_ERASE_SECTOR;

			pInst->step = CM__CLEAR_HISTORICAL;

			pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
			pInst->pInit->pDrvIf->doOp(pDrvInst, &pInst->drvRequest);
			break;
		}

		case CM__CLEAR_HISTORICAL:
		{
			cm__LogInfoType logInfo;
			void *			pDrvInst;

			/*
			 * Clear the historical log status counters.
			 */
			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);

			logInfo.firstAddr = pInst->pInit->histArea.startAddr;
			logInfo.nextAddr = pInst->pInit->histArea.startAddr;
			logInfo.recCount = 0;
			logInfo.recReset = 0;

			reg_putLocal(&logInfo, reg_i_histLogInfo, pInst->instId);

			DEB_MSG("Erasing first momentary log sector");

			/*
			 * Proceed by erasing the first momentary log sector.
			 */

			pInst->drvRequest.address = pInst->pInit->instArea.startAddr;
			pInst->drvRequest.count = 0;
			pInst->drvRequest.op = MEMDRV_OP_ERASE_SECTOR;

			pInst->step = CM__CLEAR_MOMENTARY;

			pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
			pInst->pInit->pDrvIf->doOp(pDrvInst, &pInst->drvRequest);
			break;
		}

		case CM__CLEAR_MOMENTARY:
		{
			cm__LogInfoType logInfo;
			void *			pDrvInst;

			/*
			 * Clear the momentary log status counters.
			 */
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

			logInfo.firstAddr = pInst->pInit->instArea.startAddr;
			logInfo.nextAddr = pInst->pInit->instArea.startAddr;
			logInfo.recCount = 0;
			logInfo.recReset = 0;

			reg_putLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

			DEB_MSG("Erasing first event log sector");

			/*
			 * Proceed by erasing the first event log sector.
			 */

			pInst->drvRequest.address = pInst->pInit->eventArea.startAddr;
			pInst->drvRequest.count = 0;
			pInst->drvRequest.op = MEMDRV_OP_ERASE_SECTOR;

			pInst->step = CM__CLEAR_EVENT;

			pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
			pInst->pInit->pDrvIf->doOp(pDrvInst, &pInst->drvRequest);
			break;
		}
		
		case CM__CLEAR_EVENT:
		{
			cm__LogInfoType logInfo;

			DEB_MSG("Clearing event log counters");

			/*
			 * Clear the event log status counters.
			 */
			reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);

			logInfo.firstAddr = pInst->pInit->eventArea.startAddr;
			logInfo.nextAddr = pInst->pInit->eventArea.startAddr;
			logInfo.recCount = 0;
			logInfo.recReset = 0;

			reg_putLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);

			pInst->pActReq->pDoneFn(pInst->pActReq);

			pInst->flags &=	~(CM__FLG_BUSY | CM__FLG_CLEAR);
			clearStatDone = TRUE;
			break;
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleClearInst
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle a InstLog clear request.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pRequest	Pointer to the CM request.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleClearInst(
	cm__Inst *				pInst
) {
	switch (pInst->step)
	{
		case CM__CLEAR_START:
		{
			void * pDrvInst;

			DEB_MSG("Erasing first instant log sector");

			/*
			 * Erase the first momentary log sector.
			 */

			pInst->drvRequest.address = pInst->pInit->instArea.startAddr;
			pInst->drvRequest.count = 0;
			pInst->drvRequest.op = MEMDRV_OP_ERASE_SECTOR;

			pInst->step = CM__CLEAR_MOMENTARY;

			pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
			pInst->pInit->pDrvIf->doOp(pDrvInst, &pInst->drvRequest);
			break;
		}

		case CM__CLEAR_MOMENTARY:
		{
			cm__LogInfoType logInfo;

			/*
			 * Clear the momentary log status counters.
			 */
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

			logInfo.firstAddr = pInst->pInit->instArea.startAddr;
			logInfo.nextAddr = pInst->pInit->instArea.startAddr;
			logInfo.recCount = 0;
			logInfo.recReset = 0;

			reg_putLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

			pInst->pActReq->pDoneFn(pInst->pActReq);

			pInst->flags &=	~(CM__FLG_BUSY | CM__FLG_CLEARINST);

			break;
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleSanityCheck
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue with sanity check.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleSanityCheck(
	cm__Inst * 				pInst
) {
	switch (pInst->step)
	{
		case CM__SANITY_READHIST:
		{
			cm_HistLogHdr * pHistHeader;
			void * 			pDrvInst;
			cm__LogInfoType logInfo;
			Boolean			proceed;

			pHistHeader = (void *) pInst->buff;
			proceed = TRUE;

			reg_getLocal(&logInfo, reg_i_histLogInfo, pInst->instId);

			if (
				pHistHeader->ChargeIndex != 0xFFFFFFFF
				|| pHistHeader->RecordSize != 0xFF
				|| pHistHeader->RecordType != 0xFFFF
			) {
				/*
				 * There seems to be a record here already. Search forward until
				 * a empty space is found.
				 */

				pInst->counter++;
				pInst->drvRequest.address += cm__getHistRecordSize(pHistHeader);
				logInfo.nextAddr = pInst->drvRequest.address;

				cm__checkIfEntryFits(
					&logInfo,
					&pInst->pInit->histArea,
					sizeof(cm_HistRecStd)
				);

				if (logInfo.nextAddr == pInst->drvRequest.address)
				{
					/*
					 * Read next address to check if the log ends there.
					 */

					pInst->drvRequest.count = sizeof(cm_HistLogHdr);
					pInst->drvRequest.pData = pInst->buff;

					pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
					pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
	
					proceed = FALSE;
				}
			}

			if (proceed)
			{
				/*
				 *	Store the current address.
				 */

				if (logInfo.nextAddr != pInst->drvRequest.address)
				{
					logInfo.recCount += pInst->counter;
					logInfo.recTotal += (Uint32)pInst->counter;
					logInfo.recReset += (Uint32)pInst->counter;
					logInfo.nextAddr = pInst->drvRequest.address;
					reg_putLocal(&logInfo, reg_i_histLogInfo, pInst->instId);
				}

				/*
				 * Proceed to next step.
				 */

				reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);

				pInst->step = CM__SANITY_READEVENT;
				pInst->counter = 0;

				pInst->drvRequest.count = sizeof(cm_EvtRecUnion);
				pInst->drvRequest.address = logInfo.nextAddr;
				pInst->drvRequest.pData = pInst->buff;

				pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
				pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
			}
			break;
		}

		case CM__SANITY_READEVENT:
		{
			cm_EvtLogHdr * 	pEventHeader;
			void * 			pDrvInst;
			cm__LogInfoType logInfo;
			Boolean			proceed;

			pEventHeader = (void *) pInst->buff;
			proceed = TRUE;

			reg_getLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);

			if (
				pEventHeader->Index != 0xFFFFFFFF
				|| pEventHeader->Time != 0xFFFFFFFF
				|| pEventHeader->EventId != 0xFFFF
				|| pEventHeader->DataSize != 0xFF
			) {
				/*
				 * There seems to be a record here already. Search forward until
				 * a empty space is found.
				 */

				pInst->counter++;
				pInst->drvRequest.address += cm__getEvtRecordSize(pEventHeader);
				logInfo.nextAddr = pInst->drvRequest.address;

				cm__checkIfEntryFits(
					&logInfo,
					&pInst->pInit->eventArea,
					sizeof(cm_HistRecStd)
				);

				if (logInfo.nextAddr == pInst->drvRequest.address)
				{
					/*
					 *	At least one more record will fit into this sector.
					 *	Read next address to check if the log ends there.
					 */

					pInst->drvRequest.count = sizeof(cm_EvtLogHdr);
					pInst->drvRequest.pData = pInst->buff;

					pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
					pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);

					proceed = FALSE;
				}
			}

			if (proceed)
			{
				/*
				 *	Store the current address as the next address.
				 */

				if (logInfo.nextAddr != pInst->drvRequest.address)
				{
					logInfo.recCount += pInst->counter;
					logInfo.recTotal += (Uint32)pInst->counter;
					logInfo.recReset += (Uint32)pInst->counter;
					logInfo.nextAddr = pInst->drvRequest.address;
					reg_putLocal(&logInfo, reg_i_eventLogInfo, pInst->instId);
				}

				/*
				 * Proceed to next step.
				 */

				reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

				pInst->step = CM__SANITY_READINST;
				pInst->counter = 0;

				pInst->drvRequest.count = sizeof(cm_InstRecUnion);
				pInst->drvRequest.address = logInfo.nextAddr;
				pInst->drvRequest.pData = pInst->buff;

				pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
				pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
			}

			break;
		}

		case CM__SANITY_READINST:
		{
			cm__LogInfoType logInfo;
			Boolean			proceed;

			proceed = TRUE;
			reg_getLocal(&logInfo, reg_i_instLogInfo, pInst->instId);

			if (
				pInst->buff[0] != 0xFF
				|| pInst->buff[1] != 0xFF
				|| pInst->buff[2] != 0xFF
				|| pInst->buff[3] != 0xFF
				|| pInst->buff[4] != 0xFF
				|| pInst->buff[5] != 0xFF
				|| pInst->buff[6] != 0xFF
				|| pInst->buff[7] != 0xFF
			) {
				void * pDrvInst;

				/*
				 *	There seems to be a record here already. Search forward 
				 *	until a empty space is found.
				 */
				if(pInst->buff[0] != 0x80)
				{
					pInst->counter++;
				}
				pInst->drvRequest.address += cm__getMomRecordSize(pInst->buff);
				logInfo.nextAddr = pInst->drvRequest.address;

				cm__checkIfEntryFits(
					&logInfo,
					&pInst->pInit->instArea,
					sizeof(cm_HistRecStd)
				);

				if (logInfo.nextAddr == pInst->drvRequest.address)
				{
					/*
					 * Read next address to check if the log ends there.
					 */

					pInst->drvRequest.count = sizeof(cm_InstRecUnion);
					pInst->drvRequest.pData = pInst->buff;

					pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
					pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);

					proceed = FALSE;
				}
			}

			if (proceed)
			{
				/*
				 *	Store the current address.
				 */

				if (logInfo.nextAddr != pInst->drvRequest.address)
				{
					logInfo.recCount += pInst->counter;
					logInfo.recTotal += (Uint32)pInst->counter;
					logInfo.recReset += (Uint32)pInst->counter;
					logInfo.nextAddr = pInst->drvRequest.address;
					reg_putLocal(&logInfo, reg_i_instLogInfo, pInst->instId);
				}

				/*
				 * Proceed by ending the sanity check and starting normal
				 * operation.
				 */

				pInst->flags &=
					~(CM__FLG_BUSY | CM__FLG_RLOCK | CM__FLG_SANITY);
			}
			break;
		}
	}

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleLSearch
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue with linear search.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleLSearch(
	cm__Inst *				pInst
) {
	Uint32					recordIndex;
	Uint8					recordSize;
	Uint8					headerSize;
	Boolean					InstLogTStamp;

	/*
	 * Read data from the log specific header.
	 */
	InstLogTStamp	= FALSE;

	switch(pInst->pActReq->logType)
	{
		case CM_LOGTYPE_HISTORICAL:
		{
			cm_HistLogHdr * pHeader;

			pHeader = (cm_HistLogHdr *) ((void *) pInst->buff);

			recordIndex = pHeader->ChargeIndex;
			recordSize = cm__getHistRecordSize(pHeader);
			headerSize = sizeof(cm_HistLogHdr);
			break;
		}

		case CM_LOGTYPE_EVENT:
		{
			cm_EvtLogHdr * pHeader;

			pHeader = (cm_EvtLogHdr *) ((void *) pInst->buff);

			recordIndex = pHeader->Index;
			recordSize = cm__getEvtRecordSize(pHeader);
			headerSize = sizeof(cm_EvtLogHdr);
			break;
		}

		case CM_LOGTYPE_INSTANT:
		{
			if (*((Uint8 *) ((void *) pInst->buff)) == 0x80)
			{
				cm_InstLogTstamp * pRecord;
				
				/*
				 * Bit7 is 1. This is a timestamp record.
				 */
				InstLogTStamp	= TRUE;
				pRecord = (cm_InstLogTstamp *) ((void *) pInst->buff);

				recordIndex = pRecord->Index - 1;
				pInst->pActReq->tmpIdx = pRecord->Index - 1;
				recordSize = sizeof(cm_InstLogTstamp);
				headerSize = sizeof(cm_InstLogTstamp);
				pInst->instRecSize = pRecord->RecordSize;

				/*
				 * Store header info for Radio FB
				 */

				{
					Uint32 Time = pRecord->Time;
					Uint16 SamplePeriod = pRecord->SamplePeriod;
					Uint16 RecordType = pRecord->RecordType;
					Uint8 RecordSize = pRecord->RecordSize;

					reg_putLocal(&pRecord->Index, reg_i_InstLogHdrIndex, pInst->instId);
					reg_putLocal(&Time, reg_i_InstLogHdrTime, pInst->instId);
					reg_putLocal(&SamplePeriod, reg_i_InstLogHdrSamplePer, pInst->instId);
					reg_putLocal(&RecordType, reg_i_InstLogHdrRecType, pInst->instId);
					reg_putLocal(&RecordSize, reg_i_InstLogHdrRecSize, pInst->instId);
				}

			}
			else
			{
				if (*((Uint8 *) ((void *) pInst->buff)) == 0x7F)
				{
					//cm_InstRecStd * pRecord;

					/*
					 * This should be a standard record.
					 */

					//pRecord = (cm_InstRecStd *) ((void *) pInst->buff);

					pInst->pActReq->tmpIdx++;
					recordIndex = pInst->pActReq->tmpIdx;
					recordSize = sizeof(cm_InstRecStd);
					headerSize = sizeof(cm_InstRecStd);
				}
				else
				{
					// no record, set "invalid" index
					recordIndex = 0xFFFFFFFF;
					recordSize = 0;
					headerSize = 0;
				}
			}
			break;
		}

		default:
			deb_assert(FALSE);
			headerSize = 0;
			recordSize = 0;
			recordIndex = 0;
			break;
	}


	if ((pInst->pActReq->index == recordIndex) && (InstLogTStamp == FALSE))
	{
		void * pDrvInst;
		
		DEB_MSG("CM: The record was found.");

		/*
		 * Record found. Stop the search and read the whole record.
		 */

		pInst->flags &= ~CM__FLG_LSEARCH;
		pInst->flags |= CM__FLG_READ;

		pInst->drvRequest.count = recordSize;
		pInst->drvRequest.pData = pInst->pActReq->pRecord;

		pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
		pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
	}
	else if (pInst->pActReq->index > recordIndex)
	{
		void * pDrvInst;

		/*
		 * Record not found yet. Read next record header.
		 */

		pInst->drvRequest.address += recordSize;
		pInst->drvRequest.count = headerSize;
		pInst->drvRequest.pData = pInst->buff;

		pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
		pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
	}
	else
	{
		DEB_MSG("CM: The record could not be found.");

		/*
		 * Record with the wanted index does not exist.
		 */

		pInst->pActReq->flags &= ~CM_REQFLG_SUCCESS;
		pInst->flags &= ~(CM__FLG_BUSY | CM__FLG_LSEARCH);

		if (pInst->pActReq == &pInst->readReq)
		{
			pInst->flags &= ~CM__FLG_RLOCK;

			cm__resetTmpReadData(pInst);
		}
		else
		{
			deb_assert(pInst->pActReq->pDoneFn != NULL);
			pInst->pActReq->pDoneFn(pInst->pActReq);
		}
		pInst->pActReq = NULL;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleBSearch
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue with binary search.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleBSearch(
	cm__Inst *				pInst
) {
	Uint32					recordIndex = 0;
	Uint8					headerSize = 0;
	cm_LogAreaInfo const_P * pAreaInfo = NULL;

	/*
	 * Read data from the log specific header.
	 */

	switch(pInst->pActReq->logType)
	{
		case CM_LOGTYPE_HISTORICAL:
		{
			cm_HistLogHdr * pHeader;

			pHeader = (cm_HistLogHdr *) ((void *) pInst->buff);

			recordIndex = pHeader->ChargeIndex;
			headerSize = sizeof(cm_HistLogHdr);
			pAreaInfo = &pInst->pInit->histArea;
			break;
		}

		case CM_LOGTYPE_EVENT:
		{
			cm_EvtLogHdr * pHeader;

			pHeader = (cm_EvtLogHdr *) ((void *) pInst->buff);

			recordIndex = pHeader->Index;
			headerSize = sizeof(cm_EvtLogHdr);
			pAreaInfo = &pInst->pInit->eventArea;
			break;
		}

		case CM_LOGTYPE_INSTANT:
		{
			cm_InstLogTstamp * pTstamp;

			pTstamp = (cm_InstLogTstamp *) ((void *) pInst->buff);

			recordIndex = pTstamp->Index;
			headerSize = sizeof(cm_InstLogTstamp);
			pAreaInfo = &pInst->pInit->instArea;
			break;
		}
	}

	if (pInst->flags & CM__FLG_BSEARCH2)
	{
		/*
		 * First middle sector has been read. Compare the record index of both
		 * middle sectors and continue search if needed.
		 */

		if (pInst->pActReq->index < pInst->bSearchInfo.midValue2 &&
			pInst->pActReq->index >= recordIndex)
		{
			/*
			 * The record should be in the middle sector. Start linear search.
			 */
			
			DEB_MSG_ARGS(("CM: Found record sector (%u). Proceeding with linear search", pInst->bSearchInfo.midIdx1));

			pInst->flags &= ~(CM__FLG_BSEARCH | CM__FLG_BSEARCH2);
			pInst->flags |= CM__FLG_LSEARCH;

			cm__handleLSearch(pInst);
		}
		else
		{
			if (pInst->bSearchInfo.highIdx == pInst->bSearchInfo.lowIdx)
			{
				DEB_MSG("CM: Could not find the record.");

				/*
				 * Could not find a sector that contains the wanted record
				 * index.
				 */

				pInst->flags &= ~(
					CM__FLG_BUSY | CM__FLG_BSEARCH | CM__FLG_BSEARCH2
				);

				pInst->pActReq->flags &= ~CM_REQFLG_SUCCESS;

				if (pInst->pActReq == &pInst->readReq)
				{
					pInst->flags &= ~CM__FLG_RLOCK;

					cm__resetTmpReadData(pInst);
				}
				else
				{
					deb_assert(pInst->pActReq->pDoneFn != NULL);
					pInst->pActReq->pDoneFn(pInst->pActReq);
				}
				pInst->pActReq = NULL;
			}
			else
			{
				Uint8 midIdx2;
				void * pDrvInst;

				if (pInst->pActReq->index < recordIndex)
				{
					/*
					 * The record is somewhere before the middle sector.
					 */

					pInst->bSearchInfo.highIdx = pInst->bSearchInfo.midIdx1 - 1;

					if (pInst->bSearchInfo.highIdx == 0xFF)
					{
						pInst->bSearchInfo.highIdx += (Uint8) pAreaInfo->count;
					}

					if (pInst->bSearchInfo.highIdx < pInst->bSearchInfo.lowIdx)
					{
						pInst->bSearchInfo.highIdx = pInst->bSearchInfo.lowIdx;
					}
				}
				else 
				{
					/*
					 * The record is somewhere after the middle sector.
					 */

					pInst->bSearchInfo.lowIdx = pInst->bSearchInfo.midIdx1 + 1;

					if (pInst->bSearchInfo.lowIdx == pInst->bSearchInfo.highIdx)
					{
						/*
						 *	The record could be in the newest sector. Proceed
						 *	with linear search.
						 */

						midIdx2 = pInst->bSearchInfo.lowIdx;
						if (midIdx2 >= pAreaInfo->count)
						{
							/*
							 * Log has wrapped.
							 */

							midIdx2 -= (Uint8) pAreaInfo->count;
						}

						DEB_MSG("Could be in newest sector. Proceeding with linear search");

						pInst->flags &= ~(CM__FLG_BSEARCH | CM__FLG_BSEARCH2);
						pInst->flags |= CM__FLG_LSEARCH;

						pInst->drvRequest.address =
							pAreaInfo->startAddr + midIdx2 * pAreaInfo->sectorSize;
						pInst->drvRequest.count = headerSize;
						pInst->drvRequest.pData = pInst->buff;
						pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
						pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
						return;
					}
				}

				pInst->flags &= ~CM__FLG_BSEARCH2;

				deb_assert(pInst->bSearchInfo.lowIdx <= pInst->bSearchInfo.highIdx);

				/*
				 * Calculate new middle sector index.
				 */

				pInst->bSearchInfo.midIdx1 =
					pInst->bSearchInfo.lowIdx +
					(
						(
							pInst->bSearchInfo.highIdx
							- pInst->bSearchInfo.lowIdx
						) >> 1
					);
				
				deb_assert(pInst->bSearchInfo.midIdx1 < 128);

				midIdx2 = pInst->bSearchInfo.midIdx1 + 1;

				if (midIdx2 >= pAreaInfo->count)
				{
					/*
					 * Log has wrapped.
					 */

					midIdx2 -= (Uint8) pAreaInfo->count;
				}

				/*
				 * Setup memory driver request and call memory driver.
				 */

				pInst->drvRequest.address =
					pAreaInfo->startAddr + midIdx2 * pAreaInfo->sectorSize;
				pInst->drvRequest.count = headerSize;
				pInst->drvRequest.pData = pInst->buff;
				pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
				pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
			}
		}
	}
	else
	{
		Uint32 midIdx1;
		void * pDrvInst;

		/*
		 * Store the record index of the second middle sector and start reading
		 * the first middle sector.
		 */

		pInst->bSearchInfo.midValue2 = recordIndex;

		midIdx1 = pInst->bSearchInfo.midIdx1;
		
		if (midIdx1 >= pAreaInfo->count)
		{
			/*
			 * Log has wrapped. 
			 */

			midIdx1 -= pAreaInfo->count;
		}

		pInst->flags |= CM__FLG_BSEARCH2;

		/*
		 * Setup memory driver request.
		 */

		pInst->drvRequest.address =
			pAreaInfo->startAddr + midIdx1 * pAreaInfo->sectorSize;
		pInst->drvRequest.count = headerSize;
		pInst->drvRequest.pData = pInst->buff;
		
		/*
		 * Call driver.
		 */

		pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
		pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleReadDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle erase completed.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	Should be called when a record has been read from the log.
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleReadDone(
	cm__Inst *				pInst
) {
	Boolean					done = TRUE;
	Boolean					logFound = TRUE;
	Uint8					dataSize = 0;

	deb_assert(pInst->pActReq != NULL);

	/*
	 * Store information in the request for a possible read next request.
	 */

	switch (pInst->pActReq->logType)
	{
		case CM_LOGTYPE_EVENT:
		{
			cm_EvtLogHdr * pHeader;

			pHeader = (cm_EvtLogHdr *) pInst->pActReq->pRecord;
			dataSize = pHeader->DataSize;
			pInst->pActReq->size = dataSize + sizeof(cm_EvtLogHdr);
			break;
		}

		case CM_LOGTYPE_INSTANT:
		{
			/*
			 * The record may be a timestamp record. If so then the next record
			 * should be read.
			 */

			if (*((Uint8 *) pInst->pActReq->pRecord) == 0x80)
			{
				void * pDrvInst;

				/*
				 * Bit7 is 1. This is a timestamp record.
				 */

				pInst->drvRequest.address = 
					pInst->drvRequest.address + sizeof(cm_InstLogTstamp);
				pInst->drvRequest.count = sizeof(cm_InstRecStd);

				pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
				pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);

				done = FALSE;
			}
			else
			{
				if (*((Uint8 *) pInst->pActReq->pRecord) == 0x7F)
				{
					/*
					 * This should be a standard record.
					 */

					pInst->pActReq->size = sizeof(cm_InstRecStd);
				}
				else
				{
					/*
					 * No Record sync, No record found
					 */
					logFound = FALSE;
					dataSize = 255;
				}
			}
			break;
		}

		case CM_LOGTYPE_HISTORICAL:
		{
			cm_HistLogHdr * pHeader;

			pHeader = (cm_HistLogHdr *) pInst->pActReq->pRecord;
			dataSize = pHeader->RecordSize;
			pInst->pActReq->size = (sizeof(cm_HistLogHdr) + dataSize);
			break;
		}

	}

	pInst->pActReq->currAddr = pInst->drvRequest.address;

	if (done)
	{
		pInst->pActReq->flags |= CM_REQFLG_SUCCESS;


		if ((pInst->pActReq == &pInst->readReq) && logFound)
		{
			cm_HistRecStd * pRec;
			Uint8			algNoNameLen;

			/*
			 * The handled request was the CM read request.
			 */

			pRec = pInst->pActReq->pRecord;

			/*
			 * Check algorithm name length.
			 */

			for (algNoNameLen = 0; algNoNameLen < 8; algNoNameLen++)
			{
				if (pRec->AlgNoName[algNoNameLen] == 0)
				{
					break;
				}
			}

			pInst->tmpReadData.chargedAh = pRec->chargedAh;	
			pInst->tmpReadData.startDate = pRec->chargeStartTime;		
			pInst->tmpReadData.startTime = pRec->chargeStartTime;
			pInst->tmpReadData.BID = pRec->BID;			
			pInst->tmpReadData.chargedAhP = pRec->chargedAhP;
			pInst->tmpReadData.AlgNoName[0] = algNoNameLen;
			memcpy(&pInst->tmpReadData.AlgNoName[1], pRec->AlgNoName, algNoNameLen);
			pInst->tmpReadData.capacity = pRec->Capacity;		
			pInst->tmpReadData.cells = pRec->Cells;			
			pInst->tmpReadData.cableRi = pRec->Ri;		
			pInst->tmpReadData.baseload = pRec->Baseload;		
			pInst->tmpReadData.ChalgErrorSum = pRec->ChalgErrorSum;
			pInst->tmpReadData.ReguErrorSum = pRec->ReguErrorSum;
			pInst->tmpReadData.startVoltage = pRec->startVoltage;
			pInst->tmpReadData.endVoltage = pRec->endVoltage;
			pInst->tmpReadData.ChargingMode = pRec->ChargingMode;
			pInst->tmpReadData.ChalgStatusSum = pRec->ChalgStatusSum;

			pInst->flags &= ~CM__FLG_RLOCK;
		}
		else
		{
			/*
			 * Calling the CM request callback function to report that the request
			 * has been handled.
			 */

			if (dataSize == 255)
			{
				/*
				 *	Empty flash has been read. End of log has been reached.
				 *	This may happen because of read next command.
				 */
				pInst->pActReq->flags &= ~CM_REQFLG_SUCCESS;
			}

			deb_assert(pInst->pActReq->pDoneFn != NULL);
			pInst->pActReq->pDoneFn(pInst->pActReq);
		}
			
		pInst->pActReq = NULL;

		pInst->flags &= ~(CM__FLG_BUSY | CM__FLG_READ);	
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleWriteDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle write completed.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	Should be called when a record has been written to the log.
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleWriteDone(
	cm__Inst *				pInst
) {
	cm__LogInfoType			logInfo;
	//cm_LogAreaInfo const_P * pAreaInfo;
	sys_RegHandle			regHandle;
	Boolean					logTypeIsOk;

	/*
	 * Record written to log.
	 */	
	logTypeIsOk = TRUE;

	switch (pInst->pActReq->logType)
	{
		case CM_LOGTYPE_EVENT:
			regHandle = reg_i_eventLogInfo;
			//pAreaInfo = &pInst->pInit->eventArea;
			break;

		case CM_LOGTYPE_INSTANT:
		{
			regHandle = reg_i_instLogInfo;
			//pAreaInfo = &pInst->pInit->instArea;
			break;
		}
		
		case CM_LOGTYPE_HISTORICAL:
			regHandle = reg_i_histLogInfo;
			//pAreaInfo = &pInst->pInit->histArea;
			break;

		default:
		{
			logTypeIsOk = FALSE;
		}
	}
	
	deb_assert(pInst->pActReq != NULL);

	/*
	 * Update log info.
	 */
	if(logTypeIsOk == TRUE)
	{
		reg_getLocal(&logInfo, regHandle, pInst->instId);

		logInfo.nextAddr = pInst->drvRequest.address + pInst->pActReq->size;
		logInfo.recCount++;
		logInfo.recTotal++;
		logInfo.recReset++;

		reg_putLocal(&logInfo, regHandle, pInst->instId);
	}
	
	/*
	 * Calling the CM request callback function to report that the request
	 * has been handled.
	 */

	deb_assert(pInst->pActReq->pDoneFn != NULL);

	pInst->pActReq->flags |= CM_REQFLG_SUCCESS;
	pInst->pActReq->pDoneFn(pInst->pActReq);
	pInst->pActReq = NULL;

	pInst->flags &= ~(CM__FLG_BUSY | CM__FLG_WRITE);	
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__checkIfEntryFits
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if a entry fits in to the current sector.
*
*	\param		pLogInfo		Pointer to log info structure. The nextAddr of
*								the struct will be changed to the next sector
*								address if the record doesn't fit into the
*								current sector.
*	\param		pAreaInfo		Pointer to log area info.
*	\param		size			Size of the record.
*
*	\return		true if the record does not fit into the sector and the next
*				sector must be used.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean cm__checkIfEntryFits(
	cm__LogInfoType *		pLogInfo,
	cm_LogAreaInfo const_P * pAreaInfo,
	Uint32					size
) {
	Uint32 					usedSpace;
	Boolean					newSector;

	newSector = FALSE;
	usedSpace = pLogInfo->nextAddr % pAreaInfo->sectorSize;

	if (usedSpace + size + 1 > pAreaInfo->sectorSize)
	{
		Uint32 lastAreaAddr;

		newSector = TRUE;

		/*
		 * The record does not fit into the active sector. Move to the
		 * next sector. If the active sector is the last sector in the area
		 * then the first sector should be used instead.
		 */

		pLogInfo->nextAddr =
			(pLogInfo->nextAddr - usedSpace) + pAreaInfo->sectorSize;

		lastAreaAddr =
			pAreaInfo->startAddr
			+ (pAreaInfo->count * pAreaInfo->sectorSize)
			- 1;

		if (pLogInfo->nextAddr > lastAreaAddr)
		{
			pLogInfo->nextAddr = pAreaInfo->startAddr;
		}
	}
	else if (usedSpace == 0)
	{
		newSector = TRUE;
	}

	return(newSector);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* cm__checkIfEntryFitsPrevious
*
*--------------------------------------------------------------------------*//**
*
* \brief    Checks if a entry fits in to the current sector.
*
* \param    pLogInfo    Pointer to log info structure. The nextAddr of
*               the struct will be changed to the next sector
*               address if the record doesn't fit into the
*               current sector.
* \param    pAreaInfo   Pointer to log area info.
* \param    size      Size of the record.
*
* \return   true if the record does not fit into the sector and the next
*       sector must be used.
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE Boolean cm__checkIfEntryFitsPrevious(
 cm__LogInfoType *   pLogInfo,
 cm_LogAreaInfo const_P * pAreaInfo,
 Uint32          size
) {
 Uint32          sectorOffset;
 Boolean         newSector;
 Uint32          maxLogSize = sizeof(cm_HistRecStd);

 newSector = FALSE;
 sectorOffset = pLogInfo->nextAddr % pAreaInfo->sectorSize;

 if (sectorOffset + maxLogSize > pAreaInfo->sectorSize)
 {
   newSector = TRUE;
 }

 return(newSector);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__getEvtRecordSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the size of a event log record.
*
*	\param		pEventHeader	Pointer to a event log record header.
*
*	\return		The size of the record.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE Uint8 cm__getEvtRecordSize(
	cm_EvtLogHdr *			pEventHeader
) {
	Uint8					size;
	
	/*
	 *	Using the maximum possible event record size if the record is corrupted
	 *	in a way so that the size is unknown.
	 */

	size = sizeof(cm_EvtRecUnion);

	if (
		pEventHeader->EventId > CM_EVENT__MIN
		&& pEventHeader->EventId < CM_EVENT__COUNT
	) {
		/*
		 *	The event id seems to be valid.
		 */

		if (cm__evtReqSizes[pEventHeader->EventId] == (sizeof(cm_EvtLogHdr) + pEventHeader->DataSize))
		{
			/*
			 *	The record size matches with the event id. We can assume the
			 *	size is correct.
			 */

			size = (sizeof(cm_EvtLogHdr) + pEventHeader->DataSize);
		} 
		else
		{
			/*
			 *	The size or event id has been corrupted. The event ID seems
			 *	to be valid so we are using that to get the record size.
			 */

			size  = cm__evtReqSizes[pEventHeader->EventId];
		}
	}

	return(size);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__getHistRecordSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the size of a historical log record.
*
*	\param		pHistHeader		Pointer to a historical log record header.
*
*	\return		The size of the record.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE Uint8 cm__getHistRecordSize(
	cm_HistLogHdr *			pHistHeader
) {
	Uint8					size;

	/*
	 *	Using the standard record size as default size. All historical records
	 *	should be of this size anyway (There is only one historical log record
	 *	type).
	 *
	 *	This should be changed to a union of all types if more types are added.
	 */

	size = sizeof(cm_HistRecStd);

	if (
		pHistHeader->RecordType >= CM_HISTREC_STANDARD
		&& pHistHeader->RecordType <= CM_HISTREC_STANDARD
	) {
			/*
			 *	The record type seems to be valid.
			 */

			if (cm__histReqSizes[pHistHeader->RecordType] == (sizeof(cm_HistLogHdr) + pHistHeader->RecordSize))
			{
				/*
				 *	The record size matches with the record type. We can assume
				 *	the size is correct.
				 */

				size = (pHistHeader->RecordSize + sizeof(cm_HistLogHdr));
			} 
			else
			{
				/*
				 *	The size or event id has been corrupted. The event ID seems
				 *	to be valid so we are using that to get the record size.
				 */

				size = cm__histReqSizes[pHistHeader->RecordType];
			}
	}

	return(size);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__getMomRecordSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the size of a momentary log record.
*
*	\param		pRecord		Pointer to a momentary log record.
*
*	\return		The size of the record.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE Uint8 cm__getMomRecordSize(
	BYTE *					pRecord
) {
	Uint8					size;

	if (*((Uint8 *) ((void *) pRecord)) == 0x80)
	{
		/*
		 * Bit7 is 1. This is a timestamp record.
		 */

		size = sizeof(cm_InstLogTstamp);
	}
	else
	{
		/*
		 * This should be a standard record.
		 */

		size = sizeof(cm_InstRecStd);
	}

	return(size);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleErase
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle a sector erase.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void cm__handleErase(
	cm__Inst *				pInst
) {
	void *					pDrvInst;
	cm_LogAreaInfo const_P * pAreaInfo = NULL;
	sys_RegHandle			regHandle = 0;
	cm__LogInfoType			logInfo;
	Uint8					headerSize = 0;

	pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);

	switch(pInst->currLogType)
	{
		case CM_LOGTYPE_HISTORICAL:
			regHandle = reg_i_histLogInfo;
			pAreaInfo = &pInst->pInit->histArea;
			headerSize = sizeof(cm_HistLogHdr);
			break;

		case CM_LOGTYPE_EVENT:
			regHandle = reg_i_eventLogInfo;
			pAreaInfo = &pInst->pInit->eventArea;
			headerSize = sizeof(cm_EvtLogHdr);
			break;

		case CM_LOGTYPE_INSTANT:
			regHandle = reg_i_instLogInfo;
			pAreaInfo = &pInst->pInit->instArea;
			headerSize = sizeof(cm_InstLogTstamp);
			break;
	}

	reg_getLocal(&logInfo, regHandle, pInst->instId);

	switch (pInst->step)
	{
		case CM__ERASE_START:
		{
			if (pInst->nextSect == logInfo.firstAddr)
			{
				/*
				 *	The records must be counted before the sector is erased. 
				 */

				DEB_MSG("CM: Counting sector records before erasing");

				/*
				 *	Setup memory driver request to read the first record in the
				 *	sector and call the flash memory driver.
				 */

				pInst->step = CM__ERASE_COUNTING;
				pInst->counter = 0;
				pInst->drvRequest.address = pInst->nextSect;
				pInst->drvRequest.count = headerSize;
				pInst->drvRequest.pData = pInst->buff;
				pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
			}
			else
			{
				DEB_MSG("FLOG: Erasing next sector.");

				pInst->drvRequest.address = pInst->nextSect;
				pInst->drvRequest.count = 0;
				pInst->drvRequest.op = MEMDRV_OP_ERASE_SECTOR;

				pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
				pInst->pInit->pDrvIf->doOp(pDrvInst, &pInst->drvRequest);

				pInst->step = CM__ERASE_ERASING;
			}
			break;
		}

		case CM__ERASE_COUNTING:
		{
			Uint16	countedSpace = 0;
			Uint8	recordSize = 0;

			switch(pInst->currLogType)
			{
				case CM_LOGTYPE_HISTORICAL:
					recordSize = cm__getHistRecordSize((void *) pInst->buff);
					pInst->counter++;
					break;

				case CM_LOGTYPE_EVENT:
					recordSize = cm__getEvtRecordSize((void *) pInst->buff);
					pInst->counter++;
					break;

				case CM_LOGTYPE_INSTANT:
					recordSize = cm__getMomRecordSize(pInst->buff);
					if(pInst->buff[0] != 0x80)
					{
						pInst->counter++;
					}
					break;
			}

			pInst->drvRequest.address += recordSize;

			countedSpace =
				(Uint16) (pInst->drvRequest.address - pInst->nextSect);
			
			if (countedSpace + sizeof(cm_HistRecStd) + 1 <= pAreaInfo->sectorSize)
			{
				/*
				 *	There is more records in this sector. Read the next record.
				 */

				pInst->drvRequest.count = headerSize;
				pInst->drvRequest.pData = pInst->buff;

				pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
				pInst->pInit->pDrvIf->get(pDrvInst, &pInst->drvRequest);
			}
			else
			{
				DEB_MSG("CM: Erasing next sector.");

				/*
				 *	All records have been counted. Erase the sector.
				 */

				pInst->step = CM__ERASE_ERASING;

				pInst->drvRequest.address = pInst->nextSect;
				pInst->drvRequest.count = 0;
				pInst->drvRequest.op = MEMDRV_OP_ERASE_SECTOR;

				pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
				pInst->pInit->pDrvIf->doOp(pDrvInst, &pInst->drvRequest);
			}
			break;
		}
		
		case CM__ERASE_ERASING:
		{
			Uint32 lastAreaAddress;

			DEB_MSG("CM: Sector has been erased");

			/*
			 * Sector erase completed.
			 */

			if (pInst->nextSect == logInfo.firstAddr)
			{
				logInfo.recCount -= pInst->counter;
				
				logInfo.firstAddr += pAreaInfo->sectorSize;
				lastAreaAddress = cm__getLastAreaAddress(pAreaInfo);

				if (logInfo.firstAddr > lastAreaAddress)
				{
					/*
					 * The erased sector was the last sector in the area.
					 */

					logInfo.firstAddr = pAreaInfo->startAddr;
				}
			}

			logInfo.nextAddr = pInst->nextSect;
			reg_putLocal(&logInfo, regHandle, pInst->instId);

			pInst->flags &= ~(CM__FLG_BUSY | CM__FLG_ERASE);
			break;
		}
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	cm__handleEraseAll
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle erase all flash.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE void cm__handleEraseAll(
	cm__Inst *				pInst
) {
	void *					pDrvInst;

	pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);

	if(pInst->step == CM__ERASE_START){
		//pInst->drvRequest.address = pInst->nextSect;
		//pInst->drvRequest.count = 0;
		pInst->drvRequest.op = MEMDRV_OP_ERASE_CHIP;

		pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
		pInst->pInit->pDrvIf->doOp(pDrvInst, &pInst->drvRequest);

		pInst->step = CM__ERASE_ERASING;
		flashEraseDone = FALSE;
	}
	else if(pInst->step == CM__ERASE_ERASING){
		pInst->flags &= ~(CM__FLG_BUSY | CM__FLG_ERASE);
		flashEraseDone = TRUE;
	}
}

PUBLIC void cm__TestEraseFlashStart(
) {
	cm__pInst->flags |= CM__FLG_BUSY | CM__FLG_ERASEALL;
	cm__pInst->step = CM__ERASE_START;
	cm__handleEraseAll(cm__pInst);
}

PUBLIC int cm__TestEraseFlashDone(
) {
	return flashEraseDone;
}

PUBLIC void cm__TestClearStatStart(
) {
	sup_testClearStat();
}

PUBLIC int cm__TestClearStatDone(
) {
	return clearStatDone;
}
