/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		PTest_te.c
*
*	\ingroup	RADIO
*
*	\brief		Production test case handling.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 472 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E */
#include "tools.h"
#include "mem.h"
#include "sys.h"
#include "reg.h"
#include "deb.h"
#include "msg.h"
#include "_HW.H"

/* MP Access CM */
#include "pwm.h"
#include "Meas.h"
#include "tstamp.h"
#include "rtc.h"
#include "engines_types.h"
#include "tui.h"
#include "../Regu/ControlLoop/SendToBattery.h"

/* Radio FB */
#include "radio_pn.h"
#include "Local.h"

#include <string.h>

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/
#define BOARD_TEMP_MUX		0
#define BATTERY_TEMP_MUX	1
/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_CM3

#define radio__setCanPower(state_)	\
	if ((state_) == 0) {			\
		FIO1SET = (1<<29);			\
	} else {						\
		FIO1CLR = (1<<29);			\
	}

#define radio__setBacklight(state_)	\
	if ((state_) == 0) {			\
		FIO2CLR = (1<<8);			\
	} else {						\
		FIO2SET = (1<<8);			\
	}

#define radio__setRemoteOut(state_)	\
	if ((state_) == 0) {			\
		FIO2CLR = (1<<5);			\
	} else {						\
		FIO2SET = (1<<5);			\
	}

#define radio__setPower(state_)		\
	if ((state_) == 0) {			\
		FIO2SET = (1<<13);			\
	} else {						\
		FIO2CLR = (1<<13);			\
	}

#define radio__setCanLed(state_)	\
	if ((state_) == 0) {			\
		FIO0CLR = (1<<22);			\
	} else {						\
		FIO0SET = (1<<22);			\
	}

#define radio__setI2CSda(state_)	\
	if ((state_) == 0) {			\
		FIO0CLR = (1<<19);			\
	} else {						\
		FIO0SET = (1<<19);			\
	}

#define radio__setI2CScl(state_)	\
	if ((state_) == 0) {			\
		FIO0CLR = (1<<20);			\
	} else {						\
		FIO0SET = (1<<20);			\
	}

#define radio__setF1Led(state_)		\
	if ((state_) == 0) {			\
		FIO2CLR = (1<<11);			\
	} else {						\
		FIO2SET = (1<<11);			\
	}

#define radio__setF2Led(state_)		\
	if ((state_) == 0) {			\
		FIO2CLR = (1<<12);			\
	} else {						\
		FIO2SET = (1<<12);			\
	}

#define radio__setCurrMod(state_)	\
	if ((state_) == 0) {			\
		FIO2CLR = (1<<28);			\
	} else {						\
		FIO2SET = (1<<28);			\
	}

#define radio__getRemoteIn() FIO2PIN & (1<<3) ? 1 : 0
#define radio__getOvertemp() FIO4PIN & (1<<29) ? 1 : 0

#elif TARGET_SELECTED & TARGET_W32

#define radio__setCanPower(state_)
#define radio__setBacklight(state_)
#define radio__setRemoteOut(state_)
#define radio__setPower(state_)
#define radio__setCanLed(state_)
#define radio__setI2CSda(state_)
#define radio__setI2CScl(state_)
#define radio__setF1Led(state_)
#define radio__setF2Led(state_)
#define radio__setCurrMod(state_)

#define radio__getRemoteIn()		0
#define radio__getOvertemp()		0

#endif

/**
 * Copy a 32-bit variable to the buffer. Increment the buffer pointer.
 */

#define radio__buildValue32(ppBuffer_, pData_) {							\
	*(*(ppBuffer_))++ = (BYTE) ((*(pData_)) >> 24);							\
	*(*(ppBuffer_))++ = (BYTE) ((*(pData_)) >> 16);							\
	*(*(ppBuffer_))++ = (BYTE) ((*(pData_)) >> 8);							\
	*(*(ppBuffer_))++ = (BYTE) ((*(pData_)) >> 0);							\
}

/**
 * Copy a 16-bit variable to the buffer. Increment the buffer pointer.
 */

#define radio__buildValue16(ppBuffer_, pData_) {							\
	*(*(ppBuffer_))++ = (BYTE) ((*(pData_)) >> 8);							\
	*(*(ppBuffer_))++ = (BYTE) ((*(pData_)) >> 0);							\
}

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef radio__ErrCode radio__testProcFn(radio__PnInst *,BYTE *,Uint8);

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE radio__ErrCode	radio__testProcDeepSleep(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcCanPower(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcLeds(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcBacklight(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcSwitches(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcRtcCalib(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcRemoteIn(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcRemoteOut(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcIset(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcUmeas(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcImeas(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcPower(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcHsTemp(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcOvertemp(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcCanLed(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcI2CSda(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcI2CScl(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcFlashErase(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcSerial(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcGetConf(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcSetConf(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcSetTime(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcFwVer(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcHsTempC(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcPartNo(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode  radio__testProcNfc(radio__PnInst*, BYTE*, Uint8);
PRIVATE radio__ErrCode  radio__testProcSetMui(radio__PnInst*, BYTE*, Uint8);
PRIVATE radio__ErrCode 	radio__testProcFan(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode 	radio__testProcGetDigitalIn(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcBoardTemp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode	radio__testProcBattTemp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode	radio__testProcBmuCurrMod(radio__PnInst *, BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcExtraIO(radio__PnInst *, BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcGetTime(radio__PnInst *,BYTE *, Uint8);
PRIVATE radio__ErrCode	radio__testProcStart(radio__PnInst *,BYTE *, Uint8);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/
int gTestMode = FALSE;

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
static int testMode = FALSE;
SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcess
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process test protocol packet.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pData		Pointer to packet data.
*	\param		size		Size of packet data.
*	\param		testId		Test id number.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC radio__ErrCode radio__testProcess(
	radio__Inst *			pInst,
	BYTE *					pData,
	Uint8					size,
	Uint8					testId,
	Uint8*        reqCommand
) {
	radio__ErrCode			retVal;
	radio__testProcFn *		pProcFn;
	
	retVal = RADIO__ERR_ERROR;
	pProcFn = NULL;

	switch (testId)
	{
		case RADIO__MPTEST_DEEPSLEEP:
			pProcFn = &radio__testProcDeepSleep;
			break;

		case RADIO__MPTEST_CANPOWER1:
		case RADIO__MPTEST_CANPOWER2:
			pProcFn = &radio__testProcCanPower;
			break;

		case RADIO__MPTEST_LEDTEST:
			pProcFn = &radio__testProcLeds;
			break;

		case RADIO__MPTEST_BACKLIGHT:
			pProcFn = &radio__testProcBacklight;
			break;

		case RADIO__MPTEST_SWITCHES:
			pProcFn = &radio__testProcSwitches;
			break;

		case RADIO__MPTEST_CALIBRTC:
			pProcFn = &radio__testProcRtcCalib;
			break;

		case RADIO__MPTEST_REMOTEIN:
			pProcFn = &radio__testProcRemoteIn;
			break;

		case RADIO__MPTEST_REMOTEOUT:
			pProcFn = &radio__testProcRemoteOut;
			break;

		case RADIO__MPTEST_ISET:
			pProcFn = &radio__testProcIset;
			break;

		case RADIO__MPTEST_UMEAS:
			pProcFn = &radio__testProcUmeas;
			break;

		case RADIO__MPTEST_IMEAS:
			pProcFn = &radio__testProcImeas;
			break;

		case RADIO__MPTEST_POWER:
			pProcFn = &radio__testProcPower;
			break;

		case RADIO__MPTEST_HSTEMP:
			pProcFn = &radio__testProcHsTemp;
			break;

		case RADIO__MPTEST_OVERTEMP:
			pProcFn = &radio__testProcOvertemp;
			break;

		case RADIO__MPTEST_CANLED:
			pProcFn = &radio__testProcCanLed;
			break;

		case RADIO__MPTEST_I2CSDA:
			pProcFn = &radio__testProcI2CSda;
			break;

		case RADIO__MPTEST_I2CSCL:
			pProcFn = &radio__testProcI2CScl;
			break;

		case RADIO__MPTEST_ERASEFLASH:
			if(testMode){
				pProcFn = &radio__testProcFlashErase;
			}
			break;

		case RADIO__MPTEST_SERIAL:
			pProcFn = &radio__testProcSerial;
			break;

		case RADIO__MPTEST_GETCONF:
			pProcFn = &radio__testProcGetConf;
			break;

		case RADIO__MPTEST_SETCONF:
			pProcFn = &radio__testProcSetConf;
			break;

		case RADIO__MPTEST_SETTIME:
			pProcFn = &radio__testProcSetTime;
			break;

		case RADIO__MPTEST_FWVER:
			pProcFn = &radio__testProcFwVer;
			break;

		case RADIO__MPTEST_HSTEMPC:
			pProcFn = &radio__testProcHsTempC;
			break;

		case RADIO__MPTEST_PARTNO:
			pProcFn = &radio__testProcPartNo;
			break;

		case RADIO__MPTEST_NFC:
			pProcFn = &radio__testProcNfc;
			break;

		case RADIO__MPTEST_SET_MUI:
			pProcFn = &radio__testProcSetMui;
			*reqCommand = RADIO__EVTID_SETMUIREQ;
			break;

		case RADIO__MPTEST_FAN:
			pProcFn = &radio__testProcFan;
			break;

		case RADIO__MPTEST_GETDIGITALIN:
			pProcFn = &radio__testProcGetDigitalIn;
			break;

		case RADIO__MPTEST_BOARDTEMP:
			pProcFn = &radio__testProcBoardTemp;
			break;

		case RADIO__MPTEST_BATTTEMP:
			pProcFn = &radio__testProcBattTemp;
			break;

		case RADIO__MPTEST_BMUCURRMOD:
			pProcFn = &radio__testProcBmuCurrMod;
			break;

		case RADIO__MPTEST_EXTRAIO:
			pProcFn = &radio__testProcExtraIO;
			break;

		case RADIO__MPTEST_GETTIME:
			pProcFn = &radio__testProcGetTime;
			break;

		case RADIO__MPTEST_START:
			pProcFn = &radio__testProcStart;
			break;			
	}

	if (pProcFn != NULL)
	{
		//tstamp_getStamp(&pInst->startTime);
		retVal = pProcFn((radio__PnInst *) pInst, pData, size);
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testKeyboardMon
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Stores information about pressed keys.
*
*	\param		pInst			Pointer to FB instance.
*	\param		messageHandle	Received keyboard message.
*
*	\return		TRUE if a button was pressed down. FALSE if a button was
*				released.
*
*	\details
*
*	\note		This function is called from the message notification function.
*
*******************************************************************************/

PUBLIC Boolean radio__testKeyboardMon(
	radio__Inst *			pInst,
	msg_Ptr					messageHandle
) {
	radio__PnInst *			pPnInst;
	Boolean					retVal;

	pPnInst = (radio__PnInst *) pInst;
	retVal = FALSE;

	DISABLE;
	if (pPnInst->flags & RADIO__PNFLAG_KEYMON)
	{
		if (messageHandle->msgParam & ~pPnInst->testData.oldKeys)
		{
			/*
			 *	A button was pressed down.
			 */

			pPnInst->testData.pressedKeys |= messageHandle->msgParam;
			retVal = TRUE;
		}

		pPnInst->testData.oldKeys = messageHandle->msgParam;
	}
	ENABLE;

	return retVal;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testButtonPressed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Called when a button has been pressed.
*
*	\param		pInst			Pointer to FB instance.
*	\param		messageHandle	Received keyboard message.
*
*	\return		TRUE if a response message was sent.
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC Boolean radio__testButtonPressed(
	radio__Inst *			pInst
) {
	radio__PnInst *			pPnInst;
	Boolean					retVal;

	pPnInst = (radio__PnInst *) pInst;
	retVal = FALSE;

	if (pPnInst->testData.activeTest == RADIO__MPTEST_SWITCHES)
	{
		Uint16 pressedKeys;

		atomic(
			pressedKeys = pPnInst->testData.pressedKeys;
		);

		pressedKeys <<= 1;

		pInst->pTxBuffer[RADIO__PCK_DATA] = (BYTE) (pressedKeys >> 8);
		pInst->pTxBuffer[RADIO__PCK_DATA + 1] = (BYTE) pressedKeys;
		pInst->usedTxBuff = 2;
		
		radio__pnSendTestResponse(pInst, RADIO__MPTEST_SWITCHES);
		retVal = TRUE;
	}

	return retVal;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testTimeout
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This function will take care of tests which are time based.
*
*	\param		pInst	Pointer to FB instance.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details	
*
*	\note		Should be called every now and then.
*
*******************************************************************************/

PUBLIC radio__ErrCode radio__testTick(
	radio__Inst *			pInst
) {
	radio__ErrCode			retVal;
	Uint16					elapsed;
	radio__PnInst *			pPnInst;

	pPnInst = (radio__PnInst *) pInst;

	tstamp_getDiffCurr(&pPnInst->testData.testTime, &elapsed);

	retVal = RADIO__ERR_PROCOK;

	if (elapsed >= pPnInst->testData.testTimeout)
	{
		if (pPnInst->testData.activeTest == RADIO__MPTEST_SWITCHES)
		{
			pInst->usedTxBuff = 0;
			pInst->pTxBuffer[RADIO__PCK_DATA] = 0;
			pInst->pTxBuffer[RADIO__PCK_DATA + 1] = 0;
			retVal = radio__testProcSwitches(
				pPnInst,
				&pInst->pTxBuffer[RADIO__PCK_DATA],
				2
			);
			/*
			 *	Setting the active test again because the info is needed when
			 *	building the header of the response message.
			 */
			pPnInst->testData.activeTest = RADIO__MPTEST_SWITCHES;
		}

		if (pPnInst->testData.activeTest == RADIO__MPTEST_SERIAL)
		{
			/*
			 *	Serial communication timeout. Just do nothing.
			 */

			pPnInst->testData.activeTest = RADIO__MPTEST_NA;
		}
	}

	if(pPnInst->testData.activeTest == RADIO__MPTEST_ERASEFLASH)
	{
		retVal = radio__testProcFlashErase(
				pPnInst,
				&pInst->pTxBuffer[RADIO__PCK_DATA],
				1
		);
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testDataSent
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue handling the active test case.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		-
*
*	\details
*
*	\note		Should be called when data has been sent.
*
*******************************************************************************/

PUBLIC void radio__testDataSent(
	radio__Inst *			pInstance
) {
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	if (pInst->testData.activeTest == RADIO__MPTEST_GETCONF) {
		radio__ErrCode retVal = radio__acProcGetSegment(pInstance, pInst->testData.segment--);

		if (retVal == RADIO__ERR_PROCOK_RESP) {
			radio__pnSendTestResponse(pInstance, RADIO__MPTEST_GETCONF);
		} else {
			pInst->testData.activeTest = RADIO__MPTEST_NA;
		}
	}
	else if (pInst->testData.activeTest == RADIO__MPTEST_DEEPSLEEP)
	{
		/*
		 *	Enter sleep mode. Send message when device wakes up from sleep.
		 */

		//pInst->parent.pInit->pSleepFn(pInst->parent.pInit->measInstId);
		pInst->testData.activeTest = RADIO__MPTEST_NA;
		radio__pnSendTestResponse(pInstance, RADIO__MPTEST_DEEPSLEEP);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcDeepSleep
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enter deep sleep and wake up when MAINS is connected.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcDeepSleep(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	pInst->testData.activeTest = RADIO__MPTEST_DEEPSLEEP;

	/*
	 *	Continue processing test case when data has been sent.
	 */

	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcCanPower
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set CAN power pin to desired level.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcCanPower(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__setCanPower(pData[0]);
	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcLeds
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcLeds(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint8					states;

	/*
	 * Action (bit coded, 1=ON, 0=OFF):
	 * Bit0:Not used
	 * Bit1: LED1
	 * Bit2: LED2
	 * Bit3: LED3
	 * Bit4: LED4
	 * Bit5: LED5
	 * Bit6: LED6
	 * Bit7: LED7
	 */

	states = pData[0];
	states >>= 1;

	/* LED1 (blue) */
	if (states & 0x01)
	{
		pwm_setOutputCycle(PWM_OUTPUT_STANDBY, PWM_CYCLE_FULL);
	}
	else
	{
		pwm_setOutputCycle(PWM_OUTPUT_STANDBY, 0);
	}
	states >>= 1;

	/* LED2 and LED3 (Green) */
	if (states & 0x02)
	{
		pwm_setOutputCycle(PWM_OUTPUT_CHARGRDY, PWM_CYCLE_FULL);
	}
	else
	{
		pwm_setOutputCycle(PWM_OUTPUT_CHARGRDY, 0);
	}
	states >>= 2;

	/* LED4 (Yellow) */
	if (states & 0x01)
	{
		pwm_setOutputCycle(PWM_OUTPUT_CHARG, PWM_CYCLE_FULL);
	}
	else
	{
		pwm_setOutputCycle(PWM_OUTPUT_CHARG, 0);
	}
	states >>= 1;

	/* LED5 (Red) */
	if (states & 0x01)
	{
		pwm_setOutputCycle(PWM_OUTPUT_ALARM, PWM_CYCLE_FULL);
	}
	else
	{
		pwm_setOutputCycle(PWM_OUTPUT_ALARM, 0);
	}
	states >>= 1;

	/* LED6 (Yellow) */
	radio__setF1Led(states & 0x01)
	states >>= 1;

	/* LED7 (Yellow) */
	radio__setF2Led(states & 0x01)

	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcBacklight
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Turn display backlight on or off.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcBacklight(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__setBacklight(pData[0]);
	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcSwitches
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read switches until timeout or stop command received.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcSwitches(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint16					timeout;
	radio__ErrCode			retVal;

	BYTE * pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	/*
	 *	Switches pressed (bitcoded, 1=pressed, 0=not pressed). Summary of button
	 *	status since start of test.
	 *
	 *	Bit0:Not used
	 *	Bit1: S1
	 *	Bit2: S2
	 *	Bit3: S3
	 *	Bit4: S4
	 *	Bit5: S5
	 *	Bit6: S6
	 *	Bit7: S7
	 *	Bit8: S8
	 *	Bit9: S9
	 */

	timeout = pData[0] << 8 | pData[1];

	if (timeout == 0)
	{
		Uint16 pressedKeys;

		/*
		 *	End test.
		 */

		atomic(
			pInst->flags &= ~RADIO__PNFLAG_KEYMON;
		);

		pressedKeys = pInst->testData.pressedKeys;
		pressedKeys <<= 1;

		pDataRsp[0] = (BYTE) (pressedKeys >> 8);
		pDataRsp[1] = (BYTE) pressedKeys;
		pInst->parent.usedTxBuff += 2;

		pInst->testData.activeTest = RADIO__MPTEST_NA;
		retVal = RADIO__ERR_PROCOK_RESP;
	}
	else
	{
		/*
		 *	Start test.
		 */

		pInst->testData.pressedKeys = 0;
		atomic(
			pInst->flags |= RADIO__PNFLAG_KEYMON;
		);
		
		pInst->testData.testTimeout = timeout;
		tstamp_getStamp(&pInst->testData.testTime);
		pInst->testData.activeTest = RADIO__MPTEST_SWITCHES;

		pDataRsp[0] = 0;
		pDataRsp[1] = 0;
		pInst->parent.usedTxBuff += 2;
		retVal = RADIO__ERR_PROCOK_RESP;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcRtcCalib
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Output selected clock on CLKOUT pin and calibrate RTC clock.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcRtcCalib(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint8					clockSelect;
	Uint32					calibValue;
	radio__ErrCode			retVal;
	Uint32					clkOutCfg;
	Uint8					direction;

	clockSelect = pData[0];
	calibValue = pData[1] << 16;
	calibValue |= pData[2] << 8;
	calibValue |= pData[3];
	direction = pData[4];
	/* Re-configure to make sure correct pins are set */

	if (calibValue > 0x1FFFF) calibValue = 0xFFFFFF;

	/*
	 * Calibration action:
	 * 0	OFF
	 * 1	RTC oscillator on CLKOUT
	 * 2	Main oscillator on CLKOUT
	 *
	 * Calibration value:
	 * FFFFFF	no calibration value is set.
	 * 0-1FFFF	calibration value to be stored in
	 *			register for the active oscillator defined in
	 *			calibration action above.
	 */

	if (clockSelect == 0)
	{
		clkOutCfg = 0;
		retVal = RADIO__ERR_PROCOK_RESP;
	}
	else if (clockSelect == 1)
	{
		/*
		 *	RTC oscillator on CLKOUT
		 */

		if (calibValue != 0xFFFFFF)
		{
			if (direction == 0xFF)
			{
				Uint32 oldDirection;

				/*
				 *	Read the old value to get the direction bit.
				 */

				reg_get(&oldDirection, radio__rtcCalib);
				direction = oldDirection & (1<<17) ? 1 : 0;
			}

			/*
			 *	Add the direction bit to the calibValue and store the value to
			 *	REG.
			 */

			direction = direction ? 1 : 0;
			calibValue |= direction << 17;
			reg_put(&calibValue, radio__rtcCalib);
			
			/*
			 * Must set calibration value here by directly calling the RTC
			 * function because SUP is disabled in test mode.
			 */

			rtc_hwSetCalibration(calibValue);
		}

		clkOutCfg = (1<<2) | (1<<8);
		retVal = RADIO__ERR_PROCOK_RESP;
	}
	else if (clockSelect == 2)
	{
		/*
		 *	Main oscillator on CLKOUT
		 */

		clkOutCfg = (1<<0) | (1<<8);
		retVal = RADIO__ERR_PROCOK_RESP;
	}
	else
	{
		clkOutCfg = 0;
		retVal = RADIO__ERR_ERROR;
	}
	
#if TARGET_SELECTED & TARGET_CM3
	SC->CLKOUTCFG = clkOutCfg;
#endif

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcRemoteIn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the state of the remote in pin.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcRemoteIn(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	pDataRsp[0] = radio__getRemoteIn();
	pInst->parent.usedTxBuff += 1;

	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcRemoteOut
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of the remote out pin.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcRemoteOut(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__setRemoteOut(pData[0]);
	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcIset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the PWM output and start current modulation if requested.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcIset(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint8					actionFlags;
	Uint32 					pwmValue = 0;
	extern 					Uint32 ReguIpwm;

	BYTE * pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	actionFlags = pData[0];

	if (actionFlags & (1<<0))
	{
		/* Set PWM value from scaled value in mA*/
		Uint32 setValue;
		float current;

		setValue = pData[1] << 24;
		setValue |= pData[2] << 16;
		setValue |= pData[3] << 8;
		setValue |= pData[4];

		current = (float)setValue;
		current = current / 1000;
		pwmValue = current2pwm(current);
	}
	else if (actionFlags & (1<<1))
	{
		/* Set PWM value in precent of max */
		Uint16 dutyCycle;

		dutyCycle = pData[1] << 8;
		dutyCycle |= pData[2];

		pwmValue = dutyCycle * (PWM_CYCLE_FULL/100);
	}
	else
	{
		if (actionFlags & (1<<3))
		{
			/* Calibrate Ipwm slope */
			Uint32 data;
			float slope;

			data = pData[1] << 24;
			data |= pData[2] << 16;
			data |= pData[3] << 8;
			data |= pData[4];

			slope = *((float *)&data);
			reg_put(&slope, radio__hIpwmSlope);

			engineCalibrateIpwmslope(slope);
		}
		else if (actionFlags & (1<<4))
		{
			/* Calibrate Ipwm offset */
			Uint32 data;
			float offset;

			data = pData[1] << 24;
			data |= pData[2] << 16;
			data |= pData[3] << 8;
			data |= pData[4];

			offset = (float)data;
			reg_put(&offset, radio__hIpwmOffset);

			engineCalibrateIpwmoffset(offset);
		}
	}

	if (actionFlags & (1<<2))
	{
		/* Set BMU current PWM modulation mode */
		radio__setCurrMod(1);
	}
	else
	{
		/* Set normal PWM mode */
		radio__setCurrMod(0);
	}


	/* Set output PWM */
	if (pwmValue > PWM_CYCLE_FULL)
	{
		pwmValue = PWM_CYCLE_FULL;
	}
	atomic(
		ReguIpwm = pwmValue;
	);
	pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, pwmValue);

	/* Set response data and length */
	pDataRsp[0] = (BYTE) (pwmValue >> 24);
	pDataRsp[1] = (BYTE) (pwmValue >> 16);
	pDataRsp[2] = (BYTE) (pwmValue >> 8);
	pDataRsp[3] = (BYTE) (pwmValue);
	pInst->parent.usedTxBuff += 4;

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcUmeas
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the measured voltage and set the calibration gain.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcUmeas(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint32					gain;
	Uint32					adcVoltage;
	Int32					scaledUmeas;

	BYTE * pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	/*
	 * Calibration gain, FLOAT type
	 * FFFFFFFF		no calibration gain is set.
	 * 0-FFFFFFFE	calibration gain to be stored
	 *				in NVM.
	 */

	gain = pData[0] << 24;
	gain |= pData[1] << 16;
	gain |= pData[2] << 8;
	gain |= pData[3];
	
	if (gain != 0xFFFFFFFF)
	{
		float USlope;
		//Engine_type const_P * pEngine;
		sys_CtrlType ctrlCommand;

		/*
		 *	The stored UadSlope is the combined calibration gain and
		 *	engine scaling. The engine scaling factor must be added to the
		 *	sent calibration gain.
		 */

		//pEngine = engineGetParameters();
		//USlope = pEngine->ScalefactorNom.BitToNorm.UadSlope * *((float *)&gain);
		USlope = *((float *)&gain);
		reg_put(&USlope, radio__hUadSlope);

		/* 
		 *	The new slope could be automatically set via a write indication on
		 *	the UadSlope register.
		 *	But it is activated instead with a control call because it might not 
		 *	always be desirable to update it when the register is written.
		 */

		ctrlCommand.cmd = 0x81;
		sys_callCtrl(pInst->parent.pInit->measInstId, &ctrlCommand);

		/* 
		 *	Wait until new calibration value has been used in a measurement
		 *	before continuing.
		 */

		osa_taskWait(osa_msToTicks(1100));
	}
	
	reg_get(&adcVoltage, radio__UregumV);
	reg_get(&scaledUmeas, radio__Umeas1s);

	pDataRsp[0] = (BYTE) (adcVoltage >> 24);
	pDataRsp[1] = (BYTE) (adcVoltage >> 16);
	pDataRsp[2] = (BYTE) (adcVoltage >> 8);
	pDataRsp[3] = (BYTE) (adcVoltage);
	pDataRsp[4] = (BYTE) (scaledUmeas >> 24);
	pDataRsp[5] = (BYTE) (scaledUmeas >> 16);
	pDataRsp[6] = (BYTE) (scaledUmeas >> 8);
	pDataRsp[7] = (BYTE) (scaledUmeas);
	pInst->parent.usedTxBuff += 8;

	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcImeas
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the measured current and set the calibration gain.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcImeas(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint32					gain;
	Uint32					adcVoltage;
	Int32					scaledImeas;

	BYTE * pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	/*
	 * Calibration gain, FLOAT type
	 * FFFFFFFF		No calibration gain is set.
	 * 0-FFFFFFFE	Calibration gain to be stored
	 *				in NVM.
	 */

	gain = pData[0] << 24;
	gain |= pData[1] << 16;
	gain |= pData[2] << 8;
	gain |= pData[3];
	
	if (gain != 0xFFFFFFFF)
	{
		float USlope;
		//Engine_type const_P * pEngine;
		sys_CtrlType ctrlCommand;

		/*
		 *	The stored UadSlope is the combined calibration gain and
		 *	engine scaling. The engine scaling factor must be added to the
		 *	sent calibration gain.
		 */

		//pEngine = engineGetParameters();
		//USlope = pEngine->ScalefactorNom.BitToNorm.IadSlope * *((float *)&gain);
		USlope = *((float *)&gain);
		reg_put(&USlope, radio__hIadSlope);

		/* 
		 *	The new slope could be automatically set via a write indication on
		 *	the UadSlope register.
		 *	But it is activated instead with a control call because it might not 
		 *	always be desirable to update it when the register is written.
		 */

		ctrlCommand.cmd = 0x81;
		sys_callCtrl(pInst->parent.pInit->measInstId, &ctrlCommand);

		/* 
		 *	Wait until new calibration value has been used in a measurement
		 *	before continuing.
		 */

		osa_taskWait(osa_msToTicks(1100));
	}

	reg_get(&adcVoltage, radio__IregumV);
	reg_get(&scaledImeas, radio__Imeas1s);

	pDataRsp[0] = (BYTE) (adcVoltage >> 24);
	pDataRsp[1] = (BYTE) (adcVoltage >> 16);
	pDataRsp[2] = (BYTE) (adcVoltage >> 8);
	pDataRsp[3] = (BYTE) (adcVoltage);
	pDataRsp[4] = (BYTE) (scaledImeas >> 24);
	pDataRsp[5] = (BYTE) (scaledImeas >> 16);
	pDataRsp[6] = (BYTE) (scaledImeas >> 8);
	pDataRsp[7] = (BYTE) (scaledImeas);
	pInst->parent.usedTxBuff += 8;

	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcPower
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Turn power unit on or off.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcPower(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__setPower(pData[0]);
	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcHsTemp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get measured heatsink temperature in mV.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcHsTemp(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint32					ThsmV;

	BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	reg_get(&ThsmV, radio__ThsmV);
	pDataRsp[0] = (Uint8) (ThsmV >> 8);
	pDataRsp[1] = (Uint8) ThsmV;
	pInst->parent.usedTxBuff += 2;

	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcOvertemp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the state of the overtemp input pin.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcOvertemp(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	pDataRsp[0] = radio__getOvertemp();
	pInst->parent.usedTxBuff += 1;

	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcCanLed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the state of the CAN transmit LED.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcCanLed(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__setCanLed(pData[0]);
	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcI2CSda
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set state of GTM I2C sda pin.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcI2CSda(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__setI2CSda(pData[0]);
	return(RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcI2CScl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set state of GTM I2C scl pin.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcI2CScl(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__setI2CScl(pData[0]);
	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcFlashErase
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Erase part of flash or all.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcFlashErase(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__ErrCode retVal = RADIO__ERR_PROCOK;
	BYTE * pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;
	static int testStarted = RADIO__MPTEST_ERASEFLASH_NONE;

	if(testStarted == RADIO__MPTEST_ERASEFLASH_NONE){
		pInst->testData.testTimeout = 10000;
		tstamp_getStamp(&pInst->testData.testTime);
		pInst->testData.activeTest = RADIO__MPTEST_ERASEFLASH;

		if(pData[0] == RADIO__MPTEST_ERASEFLASH_CLEARSTAT){
			cm__TestClearStatStart();			// Start flash command clear statistics
			testStarted = RADIO__MPTEST_ERASEFLASH_CLEARSTAT;
		}
		else if(pData[0] == RADIO__MPTEST_ERASEFLASH_ERASEALL){
			cm__TestEraseFlashStart();			// Start flash command Erase all flash
			testStarted = RADIO__MPTEST_ERASEFLASH_ERASEALL;
		}

	}
	else{
		Uint16 elapsed;
		tstamp_getDiffCurr(&pInst->testData.testTime, &elapsed);

		if((testStarted == RADIO__MPTEST_ERASEFLASH_CLEARSTAT) && (cm__TestClearStatDone() == TRUE)){	// Check if clear statistics command completed
			pDataRsp[0] = 0;				// Prepare response
			pInst->parent.usedTxBuff += 1;
			retVal = RADIO__ERR_PROCOK_RESP;
			testStarted = RADIO__MPTEST_ERASEFLASH_NONE;
		}
		else if((testStarted == RADIO__MPTEST_ERASEFLASH_ERASEALL) && (cm__TestEraseFlashDone() == TRUE)){	// Check if erase flash command completed
			pDataRsp[0] = 0;				// Prepare response
			pInst->parent.usedTxBuff += 1;
			retVal = RADIO__ERR_PROCOK_RESP;
			testStarted = RADIO__MPTEST_ERASEFLASH_NONE;
		}
		else if(elapsed >= pInst->testData.testTimeout){
			pDataRsp[0] = 0xFF;				// Prepare response
			pInst->parent.usedTxBuff += 1;
			retVal = RADIO__ERR_PROCOK_RESP;
			testStarted = RADIO__MPTEST_ERASEFLASH_NONE;
		}
	}
	return(retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcSerial
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get radio MAC address through the PopNet gateway.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcSerial(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	/*
	 *	Send the request message to the radio chip and wait for a response.
	 *	The wait timeout is set to 1000ms.
	 */

	radio__pnSendMpGetRadioDataReq((radio__Inst *) pInst);
	tstamp_getStamp(&pInst->testData.testTime);
	pInst->testData.testTimeout = 1000;
	pInst->testData.activeTest = RADIO__MPTEST_SERIAL;

	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcCmId
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sends serial communication test case response message.
*
*	\param		pInst	Pointer to FB instance.
*
*	\return		-
*
*	\details	Should be called when radio data has been received from the
*				radio chip over the PopNet gateway.
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__testProcCmId(
	radio__Inst *			pInst
) {
	if (((radio__PnInst *) pInst)->testData.activeTest == RADIO__MPTEST_SERIAL)
	{
		Uint32	CmIdH;
		Uint32	CmIdL;
		BYTE *	pBuffer;

		reg_getLocal(&CmIdH, reg_i_CmIdH, pInst->instId);
		reg_getLocal(&CmIdL, reg_i_CmIdL, pInst->instId);

		pBuffer = pInst->pTxBuffer + RADIO__PCK_DATA;

		pBuffer[0] = (BYTE) (CmIdH >> 24);
		pBuffer[1] = (BYTE) (CmIdH >> 16);
		pBuffer[2] = (BYTE) (CmIdH >> 8);
		pBuffer[3] = (BYTE) CmIdH;

		pBuffer[4] = (BYTE) (CmIdL >> 24);
		pBuffer[5] = (BYTE) (CmIdL >> 16);
		pBuffer[6] = (BYTE) (CmIdL >> 8);
		pBuffer[7] = (BYTE) CmIdL;

		pInst->usedTxBuff += 8;

		radio__pnSendTestResponse(pInst, RADIO__MPTEST_SERIAL);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcGetConf
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get configuration.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcGetConf(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__ErrCode			retVal;

	pInst->testData.activeTest = RADIO__MPTEST_GETCONF;
	pInst->testData.segment = 3;
	retVal = radio__acProcessMessage(&pInst->parent, pData, size, 0xFFFF);
#if 0
	if (retVal == RADIO__ERR_PROCOK_RESP)
	{
		/*
		 *	The PopNet gateway data indication contains three header bytes.
		 *	radio__acProcessMessage() function leaves room for the
		 *	three bytes on the buffer when it builds the config message.
		 *	The three data indication header bytes are not needed in the
		 *	testing protocol but they are left there because then the same
		 *	functions can be used.
		 */
		BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

		pDataRsp[0] = 0;
		pDataRsp[1] = 0;
		pDataRsp[2] = 0;
		pInst->parent.usedTxBuff += 3;
	}
#endif
	/*
	 *	Test case processing continues in the radio__testDataSent() function
	 *	when the segment has been sent.
	 */

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcSetConf
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set configuration.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcSetConf(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__ErrCode			retVal = RADIO__ERR_PROCOK;
	Uint8					StoreDefault = TRUE;
	
	/*
	 * No response for this test case.
	 */

	radio__acProcessMessage(&pInst->parent, pData, size, 0);

	// Trigger store of new config as default when last segment have been received
	if(pData[2] == 1)
		reg_put(&StoreDefault, radio__FactorySet);


	return(retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcSetTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set time.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcSetTime(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Posix_Time		pxTime;
	/*
	 * Parse message and set the device time.
	 */

	pxTime = pData[0] << 24;
	pxTime |= pData[1] << 16;
	pxTime |= pData[2] << 8;
	pxTime |= pData[3];

	rtc_hwSetAsPosix(&pxTime);

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcFwVer
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the main firmware type and version.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcFwVer(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint32 MainFwType;
	Uint32 MainFwVer;
	Uint32 RadioFwType;
	Uint32 RadioFwVer;

	BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	reg_get(&MainFwType, radio__FirmwareTypeMain);
	reg_get(&MainFwVer, radio__FirmwareVerMain);
	reg_getLocal(&RadioFwType, reg_i_FirmwareTypeRF,pInst->parent.instId);
	reg_getLocal(&RadioFwVer, reg_i_FirmwareVerRF, pInst->parent.instId);

	pDataRsp[0] = (BYTE) (MainFwType >> 24);
	pDataRsp[1] = (BYTE) (MainFwType >> 16);
	pDataRsp[2] = (BYTE) (MainFwType >> 8);
	pDataRsp[3] = (BYTE) MainFwType;
	pDataRsp[4] = (BYTE) MainFwVer;
	pDataRsp[5] = (BYTE) (RadioFwType >> 24);
	pDataRsp[6] = (BYTE) (RadioFwType >> 16);
	pDataRsp[7] = (BYTE) (RadioFwType >> 8);
	pDataRsp[8] = (BYTE) RadioFwType;
	pDataRsp[9] = (BYTE) RadioFwVer;

	pInst->parent.usedTxBuff += 10;

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcHsTempC
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get measured heatsink temperature in C.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcHsTempC(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Int16					Ths;
	float					fThs;
	Uint8 *					pThs;
	Uint8 					language;

	BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	reg_get(&Ths, radio__hThs);

	reg_get(&language, radio__hLanguageP);
	if(language == gui_lang_english_us)	// USA English, convert shown temperature to degrees Fahrenheit
	{
		Ths = ((Ths - 320)*5)/9;
	}

	fThs = (float)Ths;
	pThs = (void *)&fThs;

	pDataRsp[0] = pThs[3];
	pDataRsp[1] = pThs[2];
	pDataRsp[2] = pThs[1];
	pDataRsp[3] = pThs[0];
	pInst->parent.usedTxBuff += 4;

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcPartNo
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set/Get hardware part number.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcPartNo(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Uint32					PartNo;
	Uint8					Revision;
	reg_Status 				status = REG_UNAVAIL;

	BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	if(pData[0] == 0){
		// Get data from flash
		status = reg_get(&PartNo, radio__PartNo_base);
		status |= reg_get(&Revision, radio__PartNo_ext);
	}
	else if(pData[0] == 1){
		// Store data to flash
		// PartNo
		PartNo = pData[1] << 24;
		PartNo |= pData[2] << 16;
		PartNo |= pData[3] << 8;
		PartNo |= pData[4];
		// Revision
		Revision = pData[5];

		status = reg_put(&PartNo, radio__PartNo_base);
		status |= reg_put(&Revision, radio__PartNo_ext);
	}

	if (status != REG_OK){
		// Indicate error by setting values to all 0xFFs
		PartNo = 0xFFFFFFFF;
		Revision = 0xFF;
	}

	//PartNo
	pDataRsp[0] = (BYTE) (PartNo >> 24);
	pDataRsp[1] = (BYTE) (PartNo >> 16);
	pDataRsp[2] = (BYTE) (PartNo >> 8);
	pDataRsp[3] = (BYTE) PartNo;
	//Revision
	pDataRsp[4] = Revision;

	pInst->parent.usedTxBuff += 5;

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* radio__testProcNfc
*
*--------------------------------------------------------------------------*//**
*
* \brief    Read NFC info.
*
* \param    pInst Pointer to FB instance.
* \param    pData Pointer to test request data.
* \param    size  Size of test request data.
*
* \retval   RADIO__ERR_PROCOK_RESP  If the test was done ok and a response
*                   should be sent.
* \retval   RADIO__ERR_PROCOK   If the test was done ok but no response
*                   should be sent.
* \retval   RADIO__ERR_ERROR    An error occurred while processing the
*                   command.
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE radio__ErrCode  radio__testProcNfc(
    radio__PnInst* pInst,
    BYTE* pData,
    Uint8 size
) {
  uint32_t nfcInfoH;
  uint32_t nfcInfoL;
  uint8_t* pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

  if ((reg_get(&nfcInfoH, radio__NfcInfoH) != REG_OK) ||
      (reg_get(&nfcInfoL, radio__NfcInfoL) != REG_OK)) {
    // Indicate error by setting values to all 0xFFs
    memset(&nfcInfoH, 0xff, sizeof(nfcInfoH));
    memset(&nfcInfoL, 0xff, sizeof(nfcInfoL));
  }

  pDataRsp[0] = (uint8_t)(nfcInfoH >> 16);
  pDataRsp[1] = (uint8_t)(nfcInfoH >> 8);
  pDataRsp[2] = (uint8_t) nfcInfoH;
  pDataRsp[3] = (uint8_t)(nfcInfoL >> 24);
  pDataRsp[4] = (uint8_t)(nfcInfoL >> 16);
  pDataRsp[5] = (uint8_t)(nfcInfoL >> 8);
  pDataRsp[6] = (uint8_t) nfcInfoL;
  pInst->parent.usedTxBuff += 7;

  return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
* F U N C T I O N   D E S C R I P T I O N
*
* radio__testProcSetMui
*
*--------------------------------------------------------------------------*//**
*
* \brief    Set MUI.
*
* \param    pInst Pointer to FB instance.
* \param    pData Pointer to test request data.
* \param    size  Size of test request data.
*
* \retval   RADIO__ERR_PROCOK_RESP  If the test was done ok and a response
*                   should be sent.
* \retval   RADIO__ERR_PROCOK   If the test was done ok but no response
*                   should be sent.
* \retval   RADIO__ERR_ERROR    An error occurred while processing the
*                   command.
*
* \details
*
* \note
*
*******************************************************************************/
PRIVATE radio__ErrCode  radio__testProcSetMui(
    radio__PnInst* pInst,
    BYTE* pData,
    Uint8 size
) {
  if (size != 8) {
    uint8_t* dataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;
    dataRsp[0] = 1;
    pInst->parent.usedTxBuff += 1;

    return RADIO__ERR_PROCOK_RESP;
  } else {
    uint8_t* dataReq = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

    memcpy(dataReq, pData, 8);
    pInst->parent.usedTxBuff = 8;

    return RADIO__ERR_PROCOK_FALL_TROUGH;
  }
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcFan
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set fan PWM duty cycle.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcFan(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__ErrCode			retVal;

	retVal = RADIO__ERR_PROCOK;

	if (size == 1)
	{
		Uint32 dutyCycle;

		/*
		 * Scale the received duty cycle and set it to the PWM output.
		 */

		dutyCycle = pData[0];

		if (dutyCycle > 100)
		{
			dutyCycle = 100;
		}

		dutyCycle = dutyCycle * (PWM_CYCLE_FULL / 100);
		dutyCycle = PWM_CYCLE_FULL - dutyCycle;	// Invert pwm output
		pwm_setOutputCycle(PWM_OUTPUT_FAN, dutyCycle);

		retVal = RADIO__ERR_PROCOK_RESP;
	}

	return retVal;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcGetDigitalIn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get state of digital inputs.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcGetDigitalIn(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__ErrCode			retVal;

	retVal = RADIO__ERR_ERROR;
	/*
	 * Get the device time and send as response.
	 */
	if (size == 0)
	{
		BYTE *	pDataRsp;

		Uint32 digitalInputs = 0;
		Uint8 remoteIn = 0;
		Uint8 mains = 0;

		reg_get(&remoteIn, radio__hRemoteIn);
		reg_get(&mains, radio__mains);

		digitalInputs |= (remoteIn == FALSE ? TRUE:FALSE) << 0;	// Set remote input (bit0)
		digitalInputs |= (mains == TRUE ? TRUE:FALSE) << 1;		// Set mains input (bit1)

		pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;
		radio__buildValue32(&pDataRsp, &digitalInputs);
		pInst->parent.usedTxBuff += 4;
		retVal = RADIO__ERR_PROCOK_RESP;
	}
	return(retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcBoardTemp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get measured board temperature.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcBoardTemp(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Int16					boardTemp;
	BYTE *					pDataRsp;

	pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	tui__tempMux(BOARD_TEMP_MUX);

	/*
	 *	Wait for new ADC measurement before reading
	 */
	osa_taskWait(osa_msToTicks(100));
	reg_get(&boardTemp, radio__TbTest);

	radio__buildValue16(&pDataRsp, &boardTemp);
	pInst->parent.usedTxBuff += 2;

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcBattTemp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get measured battery temperature.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcBattTemp(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	Int16					battTemp;
	BYTE *					pDataRsp;

	pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	tui__tempMux(BATTERY_TEMP_MUX);
	/*
	 *	Wait for new ADC measurement before reading
	 */
	osa_taskWait(osa_msToTicks(100));
	reg_get(&battTemp, radio__TbattTest);

	radio__buildValue16(&pDataRsp, &battTemp);
	pInst->parent.usedTxBuff += 2;

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcGetTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the time as Posix Time(seconds from 1970).
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcGetTime(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {

	BYTE *pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	Posix_Time		pxTime;

	rtc_hwGetAsPosix(&pxTime);

	pDataRsp[0] = (BYTE)(pxTime >> 24);
	pDataRsp[1] = (BYTE)(pxTime >> 16);
	pDataRsp[2] = (BYTE)(pxTime >> 8);
	pDataRsp[3] = (BYTE)(pxTime);
	pInst->parent.usedTxBuff += 4;

	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcBmuCurrMod
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start/Stop BMU_CURR_MOD pwm output (Access 30).
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcBmuCurrMod(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__ErrCode			retVal;

	retVal = RADIO__ERR_ERROR;
	/*
	 * Get the device time and send as response.
	 */
	if (size == 1)
	{
		if(pData[0] == 0)
		{
			/*
			 *	Stop test
			 */
			sendTobatteryFromTestModeStop();
		}
		else if(pData[0] == 1)
		{
			/*
			 *	Start test
			 */
			sendTobatteryFromTestModeStart();
		}

		retVal = RADIO__ERR_PROCOK_RESP;
	}
	return(retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcExtraIO
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Test Extra I/O pins.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcExtraIO(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	radio__ErrCode			retVal;
	BYTE *					pDataRsp;

	retVal = RADIO__ERR_ERROR;
	pDataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	/*
	 * Get the device time and send as response.
	 */
	if (size == 1)
	{
		Uint16 analogInput;
		Uint8 digitalInput;
		Uint8 digitalOutput;

		/*
		 *	Initialize pins
		 */
		/* Configure SCK (P0.15) as digital output */
		PINCON->PINSEL0 &= ~((1<<30) | (1<<31));
		FIO0DIR |= (1<<15);

		/* Configure SDI (P0.17) as digital input */
		PINCON->PINSEL1 &= ~((1<<2) | (1<<3));
		FIO0DIR &= ~(1<<17);

		/* Configure SDO (P0.18) as digital output */
		PINCON->PINSEL1 &= ~((1<<4) | (1<<5));
		FIO0DIR |= (1<<18);

		/* Configure ACTUATE (P1.17) as digital output */
		PINCON->PINSEL3 &= ~((1<<2) | (1<<3));
		FIO1DIR |= (1<<17);

		/* Configure AD0_5 (P1.31) already as analog input */
		// Already done

		/*
		 * Set digital outputs
		 */
		digitalOutput = pData[0];

		if(digitalOutput != 0)
		{
			FIO0SET |= (1<<15);
			FIO0SET |= (1<<18);
			FIO1SET |= (1<<17);
		}
		else
		{
			FIO0CLR |= (1<<15);
			FIO0CLR |= (1<<18);
			FIO1CLR |= (1<<17);
		}

		/*
		 *	Wait for new ADC measurement before reading analog (and digital) inputs
		 */
		osa_taskWait(osa_msToTicks(100));
		analogInput = meas_getIoBoardSum() / 10;
		digitalInput = (FIO0PIN >> 17) & 0x01;

		/*
		 *	Build response
		 */
		pDataRsp[0] = digitalInput;
		pDataRsp[1] = (Uint8) (analogInput >> 8);
		pDataRsp[2] = (Uint8) analogInput;
		pInst->parent.usedTxBuff += 3;

		retVal = RADIO__ERR_PROCOK_RESP;
	}
	return(retVal);





	return(RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__testProcStart
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set device into testing mode.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pData	Pointer to test request data.
*	\param		size	Size of test request data.
*
*	\retval		RADIO__ERR_PROCOK_RESP	If the test was done ok and a response
*										should be sent.
*	\retval		RADIO__ERR_PROCOK		If the test was done ok but no response
*										should be sent.
*	\retval		RADIO__ERR_ERROR		An error occurred while processing the
*										command.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__testProcStart(
	radio__PnInst *			pInst,
	BYTE *					pData,
	Uint8					size
) {
	if (pData[0] == 1)
	{
		sys_CtrlType ctrlCommand;
		sys_FBInstId const_P * pInstId;

		/*
		 *	Disable other function blocks.
		 */

		ctrlCommand.cmd = 0x80; /* 0x80 is used as enter test mode command */
		pInstId = pInst->parent.pInit->pIdArray;
		
		do {
			sys_callCtrl(*pInstId, &ctrlCommand);
			pInstId++;
		} while (*pInstId != SYS_BAD_FB_INST_ID);

		gTestMode = TRUE;

		/*
		 *	Initialize hardware.
		 */

#if TARGET_SELECTED & TARGET_CM3
		/* CAN power pin */
		PINCON->PINSEL3 &= ~((1<<26) | (1<<27));
		FIO1DIR |= (1<<29);

		/* F1 and F2 leds */
		PINCON->PINSEL4 &= ~((1<<22) | (1<<23) | (1<<24) | (1<<25));
		FIO2DIR = (1<<11) | (1<<12);

		/* Display backlight */
		PINCON->PINSEL4 &= ~((1<<16) | (1<<17));
		FIO2DIR |= (1<<8);

		/* Remote out pin */
		PINCON->PINSEL4 &= ~((1<<10) | (1<<11));
		FIO2DIR |= (1<<5);

		/* Power unit on/off pin */
		PINCON->PINSEL4 &= ~((1<<26) | (1<<27));
		FIO2DIR |= (1<<13);

		/* CAN led pin */
		PINCON->PINSEL1 &= ~((1<<12) | (1<<13));
		FIO0DIR |= (1<<22);

		/* Set i2c pins to GPIO outputs */
		PINCON->PINSEL1 &= ~((1<<6) | (1<<7) | (1<<8) | (1<<9));
		FIO0DIR |= (1<<19) | (1<<20);
		FIO0CLR |= (1<<19) | (1<<20);
		 
		/* CLKOUT */
		PINCON->PINSEL3 |= (1<<22);
		PINCON->PINSEL3 &= ~(1<<23);
#endif
		
		radio__setCanPower(0);
		radio__setF1Led(0);
		radio__setF2Led(0);
		radio__setBacklight(0);
		radio__setRemoteOut(0);
		radio__setPower(0);
		radio__setCanLed(0);
		radio__setCurrMod(0);
		radio__setI2CSda(0);
		radio__setI2CScl(0);
		pwm_start();

		pInst->testData.oldKeys = 0;

		/* Set flag for test mode entered */
		testMode = TRUE;
	}

	return(RADIO__ERR_PROCOK_RESP);
}


