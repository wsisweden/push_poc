/* 07-10-08 TJK
 ********************************************************************* tabs:[5 9]
 *
 *	F I L E   D E S C R I P T I O N
 *
 *	[ ]Lib FB main source file		[x]Lib FB source file
 *	[ ]Lib FB target source file	[ ]Project source file
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\file		Radio_ac.c
 *
 *	\ingroup	RADIO
 *
 *	\brief		Micropower Access radio protocol implementation.
 *
 *	\details
 *
 *	\note
 *
 *	\version	\$Rev: 522 $ \n
 *				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
 *				\$Author: tlarsu $
 *
 *******************************************************************************/

/*******************************************************************************
 ;
 ;	D E C L A R A T I O N   M O D U L E
 ;
 ;*******************************************************************************

 ;*******************************************************************************
 ;
 ;	HEADER / INCLUDE FILES
 ;
 ;-----------------------------------------------------------------------------*/

//#include "../Chalg/inc_cm3/cai_constparam.h" // sorabtest magnus J

#include "string.h"

#include "tools.h"
#include "mem.h"
#include "sys.h"
#include "reg.h"
#include "deb.h"
#include "tstamp.h"
#include "msg.h"

#include "Sup.h"
#include "Cm.h"
#include "chalg.h"
#include "dpl.h"

#include "Local.h"
#include "radio_pn.h"
#include "Random.h"

/*******************************************************************************
 ;
 ;	CONSTANTS
 ;
 ;-----------------------------------------------------------------------------*/

/**
 * Access OTA protocol commands.
 */

enum radio__acCommand {
	/* Common request */
	RADIO__ACCMD_GETSEG = 0x01,
	RADIO__ACCMD_SET_TIME, /**< Set time */
	RADIO__ACCMD_GET_ADDRPOOLADDR,
	RADIO__ACCMD_BBC,

	/* Common response */
	RADIO__ACCMD_ACK = 0x10,
	RADIO__ACCMD_JOININD,
	RADIO__ACCMD_ADDRPOOLADDR,

	/* BM response */
	RADIO__ACCMD_BMSTATUS = 0x30,
	RADIO__ACCMD_BMMEAS,
	RADIO__ACCMD_BMCONFIG,
	RADIO__ACCMD_BMLOG,
	RADIO__ACCMD_BMDATA,
	RADIO__ACCMD_BMCAPACK,
	RADIO__ACCMD_BMINFO,

	/* CM request */
	RADIO__ACCMD_GET_BMDATA = 0x40,
	RADIO__ACCMD_GET_BMCAPACK,
	RADIO__ACCMD_GET_BMMEAS, /**< CM meas data message			*/
	RADIO__ACCMD_CMEOC, /**< End of charge message to BM	*/
	RADIO__ACCMD_SET_BMDATA,
	RADIO__ACCMD_CMDPLSLAVE,
	RADIO__ACCMD_CMDPLMASTERSLAVE,

	/* CM response */
	RADIO__ACCMD_CMSTATUS = 0x50,
	RADIO__ACCMD_CMMEAS,
	RADIO__ACCMD_CMCONFIG, /**< CM config message				*/
	RADIO__ACCMD_CMLOG, /**< CM log message					*/
	RADIO__ACCMD_CMINFO,
	RADIO__ACCMD_GET_CMDPLSLAVE,
	RADIO__ACCMD_CMDPLMASTERALL,
	//RADIO__ACCMD_CMUSERPARAMS = 0x59, // sorabtest magnus J

	/* SM request */
	RADIO__ACCMD_GET_STATUS = 0x60,
	RADIO__ACCMD_GET_MEAS,
	RADIO__ACCMD_GET_CONFIG,
	RADIO__ACCMD_GET_LOG,
	RADIO__ACCMD_SET_BMCAPACK,
	RADIO__ACCMD_SET_BMCONFIG,
	RADIO__ACCMD_SET_MPL,
	RADIO__ACCMD_SET_CMCONFIG,
	RADIO__ACCMD_GET_INFO,
	RADIO__ACCMD_SET_IDENTIFY,
	RADIO__ACCMD_GET_ACKNIWLEDGE,
	RADIO__ACCMD_SET_BMCALIBRATION,
	//RADIO__ACCMD_GET_USERPARAMS = 0xB3, // sorabtest magnus J
	//RADIO__ACCMD_SET_USERPARAMS, // sorabtest magnus J

	RADIO__ACCMD_INVALID = 0xff
};

/**
 * Access OTA protocol header bytes.
 */

enum radio__acMsgHeader {
	/**
	 * A command byte to identify the data to come.
	 */
	RADIO__MSGH_CMD = 0,

	/**
	 * If the message is a request (trigged from application) the MpSequence
	 * number is taken from the local MpSequence counter. But if the message is
	 * a response then the MpSequence number is taken from the incoming
	 * request message MpSequence number.
	 */
	RADIO__MSGH_MPSEQ,

	/**
	 * Control byte which indicated if message is segmented or not.
	 * 0 = Single message
	 * 1 = segmented, first message
	 * 2 = segmented, consecutive message
	 * 3 = segmented last message
	 */
	RADIO__MSGH_CONTROL,

	/**
	 * Starting position for msg data
	 */
	RADIO__MSGH_DATA
};

/**
 * \name	Message control byte
 */

/*@{*/
#define RADIO__MSGHCTRL_SINGLE		0	/**< Single message					*/
#define RADIO__MSGHCTRL_FIRST		128	/**< segmented msg, first package	*/
#define RADIO__MSGHCTRL_LAST		1	/**< segmented msg, last package	*/

#define RADIO__MSGHCTRL_CFG_1ST		(RADIO__MSGHCTRL_FIRST + 4)	/**< first configuration segment 	*/
#define RADIO__MSGHCTRL_CFG_2ND		3							/**< second configuration segment 	*/
#define RADIO__MSGHCTRL_CFG_3RD		2							/**< third configuration segment 	*/
#define RADIO__MSGHCTRL_CFG_4TH		1							/**< fourth configuration segment 	*/
#define RADIO__MSGHCTRL_CFG_DONE	0							/**< All configuration segment sent	*/
/*@}*/

/**
 * Send as request when receiving a segmented message as a response.
 */

enum radio__acMsgGetSegment {
	RADIO__MSGGETSEG_CONTROL = 0,
	RADIO__MSGGETSEG_SIZE
/**< Size of get segment message	*/
};

/**
 * Access OTA protocol set time message bytes.
 */

enum radio__acMsgSetTime {
	RADIO__MSGST_SOURCE = 0,/**< 0=SM, 1=CM, 2=BM, 3=VM			*/
	RADIO__MSGST_TIME_H, 	/**< POSIX time	highest 8-bits		*/
	RADIO__MSGST_TIME_HM, 	/**< POSIX time	higher middle 8-bits*/
	RADIO__MSGST_TIME_LM, 	/**< POSIX time	lower middle 8-bits	*/
	RADIO__MSGST_TIME_L, 	/**< POSIX time	lowest 8-bits		*/
	RADIO__MSGST_SIZE
};

/**
 * Access OTA protocol Bm calibration message bytes.
 */

enum radio__acMsgSetBmCalibration {
	RADIO__MSGSCA_TYPE = 0, 	/**< 0= N/A, 1=Battery+, 2=Battery middle, 3=Electrolyte, 4=Current	*/
	RADIO__MSGSCA_MEASPOINT, 	/**< 0=first point, 1= second point									*/
	RADIO__MSGSCA_MEASVALUE_H, 	/**< Measured value, for type 1,2,3 in mV, type 4 in mA				*/
	RADIO__MSGSCA_MEASVALUE_HM, /**< POSIX time	higher middle 8-bits								*/
	RADIO__MSGSCA_MEASVALUE_LM, /**< POSIX time	lower middle 8-bits									*/
	RADIO__MSGSCA_MEASVALUE_L, 	/**< POSIX time	lowest 8-bits										*/
	RADIO__MSGSCA_SIZE
};

/**
 * Acknowledge message for those commands that don�t expect any other answer.
 */

enum radio__acMsgAcknowledge {
	RADIO__MSGACK_REQCMD = 0, /**< The Cmd of the request message that
	 * this message is responding to.	*/
	RADIO__MSGACK_STATUS, /**< Status:
	 * 0x00 = OK
	 * 0x01 = Resend previous message
	 * 0xFF = Unknown Error				*/
	RADIO__MSGACK_SIZE
/**< Size of Ack message			*/
};

/**
 * Join indication message sent when join success.
 */

enum radio__acMsgJoinIndication {
	RADIO__MSGJOININD_SIZE = 0
};

/**
 * Sent from BM to CM every minute during charging as response to CMmeas.
 * Also sent as response to Get_Meas.
 */

enum radio__acMsgBMmeas {
	RADIO__MSGBMM_TBATT_H = 0, /**< Battery temperature		*/
	RADIO__MSGBMM_TBATT_L,
	RADIO__MSGBMM_IBATT_H, /**< Battery current				*/
	RADIO__MSGBMM_IBATT_HM,
	RADIO__MSGBMM_IBATT_LM,
	RADIO__MSGBMM_IBATT_L,
	RADIO__MSGBMM_UBATT_H, /**< Battery voltage				*/
	RADIO__MSGBMM_UBATT_HM,
	RADIO__MSGBMM_UBATT_LM,
	RADIO__MSGBMM_UBATT_L,
	RADIO__MSGBMM_SOC, /**< State of charge (SOC)			*/
	RADIO__MSGBMM_ERROR_H, /**< Bm error					*/
	RADIO__MSGBMM_ERROR_L,
	RADIO__MSGBMM_UCBATT_H, /**< Battery center voltage		*/
	RADIO__MSGBMM_UCBATT_HM,
	RADIO__MSGBMM_UCBATT_LM,
	RADIO__MSGBMM_UCBATT_L,
	RADIO__MSGBMM_STATUS_H, /**< BmStatus					*/
	RADIO__MSGBMM_STATUS_L,
	RADIO__MSGBMM_UELECTROLYTE_H,
	RADIO__MSGBMM_UELECTROLYTE_HM,
	RADIO__MSGBMM_UELECTROLYTE_LM,
	RADIO__MSGBMM_UELECTROLYTE_L,
	RADIO__MSGBMM_U32SPARE1_H, /**< Uint32 spare			*/
	RADIO__MSGBMM_U32SPARE1_HM,
	RADIO__MSGBMM_U32SPARE1_LM,
	RADIO__MSGBMM_U32SPARE1_L,
	RADIO__MSGBMM_U16SPARE1_H, /**< Uint16 spare			*/
	RADIO__MSGBMM_U16SPARE1_L,
	//RADIO__MSGBMM_U8SPARE1, 	/**< Uint8 spare				*/
	RADIO__MSGBMM_UNITSCALE, 	/**< Scale for e.g. temperature	*/
	RADIO__MSGBMM_SIZE
/**< Size of BMmeas message.		*/
};

/**
 * Sent from BM to CM When the NwkAddr from CM has been received over cable
 * communication or as response to Get_BMBatteryData.
 */

enum radio__acMsgBmBatteryData { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGBMDA_BID_H, 			/**< Unique battery identifier		*/
	RADIO__MSGBMDA_BID_HM, 			/**< Unique battery identifier		*/
	RADIO__MSGBMDA_BID_LM, 			/**< Unique battery identifier		*/
	RADIO__MSGBMDA_BID_L, 			/**< Unique battery identifier		*/
	RADIO__MSGBMDA_BATTERYTEXT_0,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATTERYTEXT_1,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATTERYTEXT_2,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATTERYTEXT_3,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATTERYTEXT_4,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATTERYTEXT_5,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATTERYTEXT_6,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATTERYTEXT_7,  	/**< Battery description			*/
	RADIO__MSGBMDA_BATT_TYPE, 		/**< Battery type e.g. gel, open led*/
	RADIO__MSGBMDA_ALGNO_H, 		/**< Charging algorithm number		*/
	RADIO__MSGBMDA_ALGNO_L,			/**< Charging algorithm number		*/
	RADIO__MSGBMDA_ALGNOVER_H, 		/**< Algorithm number version		*/
	RADIO__MSGBMDA_ALGNOVER_L, 		/**< Algorithm number version		*/
	RADIO__MSGBMDA_CAP_H, 			/**< Battery capacity				*/
	RADIO__MSGBMDA_CAP_L, 			/**< Battery capacity				*/
	RADIO__MSGBMDA_CELLS_H, 		/**< Number of cells in battery		*/
	RADIO__MSGBMDA_CELLS_L, 		/**< Number of cells in battery		*/
	RADIO__MSGBMDA_CABLERES_H, 		/**< Cable resistance				*/
	RADIO__MSGBMDA_CALBERES_L, 		/**< Cable resistance				*/
	RADIO__MSGBMDA_BASELOAD_H, 		/**< Vehicle current consumption	*/
	RADIO__MSGBMDA_BASELOAD_L, 		/**< Vehicle current consumption	*/
	RADIO__MSGBMDA_BMNWKID_H, 		/**< BM radio network address		*/
	RADIO__MSGBMDA_BMNWKID_L, 		/**< BM radio network address		*/
	RADIO__MSGBMDA_U32SPARE1_H, 	/**< Uint32 spare		*/
	RADIO__MSGBMDA_U32SPARE1_HM, 	/**< 					*/
	RADIO__MSGBMDA_U32SPARE1_LM, 	/**< 					*/
	RADIO__MSGBMDA_U32SPARE1_L, 	/**< 					*/
	RADIO__MSGBMDA_U32SPARE2_H, 	/**< Uint32 spare		*/
	RADIO__MSGBMDA_U32SPARE2_HM, 	/**< 					*/
	RADIO__MSGBMDA_U32SPARE2_LM, 	/**< 					*/
	RADIO__MSGBMDA_U32SPARE2_L, 	/**< 					*/
	//RADIO__MSGBMDA_U16SPARE1_H, 	/**< Uint16 spare		*/
	//RADIO__MSGBMDA_U16SPARE1_L, 	/**< 					*/
	RADIO__MSGBMDA_STATUS_H, 		/**< BmStatus			*/
	RADIO__MSGBMDA_STATUS_L, 		/**< 					*/
	RADIO__MSGBMDA_U16SPARE2_H, 	/**< Uint16 spare		*/
	RADIO__MSGBMDA_U16SPARE2_L, 	/**< 					*/
	RADIO__MSGBMDA_U8SPARE1, 		/**< Uint32 spare		*/
	RADIO__MSGBMDA_U8SPARE2, 		/**< Uint32 spare		*/
	RADIO__MSGBMDA_SIZE
/**< Size of battery data message	*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Sent from BM to CM When requested or from CM to BM
 */
enum radio_acMsgBmConfig_enum{
  RADIO_MSGBMCONFIG_PANID_H,
  RADIO_MSGBMCONFIG_PANID_L,
  RADIO_MSGBMCONFIG_CHANNEL,
  RADIO_MSGBMCONFIG_NODEADDR_H,
  RADIO_MSGBMCONFIG_NODEADDR_L,
  RADIO_MSGBMCONFIG_NWKSETTINGS,
  RADIO_MSGBMCONFIG_SERIALNO_H,
  RADIO_MSGBMCONFIG_SERIALNO_HM,
  RADIO_MSGBMCONFIG_SERIALNO_LM,
  RADIO_MSGBMCONFIG_SERIALNO_L,
  RADIO_MSGBMCONFIG_BMTYPE,
  RADIO_MSGBMCONFIG_BID_H,
  RADIO_MSGBMCONFIG_BID_HM,
  RADIO_MSGBMCONFIG_BID_LM,
  RADIO_MSGBMCONFIG_BID_L,
  RADIO_MSGBMCONFIG_EQUFUNC,
  RADIO_MSGBMCONFIG_EQUPARAM,
  RADIO_MSGBMCONFIG_WATERFUNC,
  RADIO_MSGBMCONFIG_WATERPARAM,
  RADIO_MSGBMCONFIG_BMFGID_HHH,
  RADIO_MSGBMCONFIG_BMFGID_HH,
  RADIO_MSGBMCONFIG_BMFGID_H,
  RADIO_MSGBMCONFIG_BMFGID_HM,
  RADIO_MSGBMCONFIG_BMFGID_LM,
  RADIO_MSGBMCONFIG_BMFGID_L,
  RADIO_MSGBMCONFIG_BMFGID_LL,
  RADIO_MSGBMCONFIG_BMFGID_LLL,
  RADIO_MSGBMCONFIG_CELLS_H,
  RADIO_MSGBMCONFIG_CELLS_L,
  RADIO_MSGBMCONFIG_CAPACITY_H,
  RADIO_MSGBMCONFIG_CAPACITY_L,
  RADIO_MSGBMCONFIG_CYCLESAVAILABLE_H,
  RADIO_MSGBMCONFIG_CYCLESAVAILABLE_L,
  RADIO_MSGBMCONFIG_AHAVAILABLE_H,
  RADIO_MSGBMCONFIG_AHAVAILABLE_HM,
  RADIO_MSGBMCONFIG_AHAVAILABLE_LM,
  RADIO_MSGBMCONFIG_AHAVAILABLE_L,
  RADIO_MSGBMCONFIG_BATTERYTYPE,
  RADIO_MSGBMCONFIG_BASELOAD_H,
  RADIO_MSGBMCONFIG_BASELOAD_L,
  RADIO_MSGBMCONFIG_CABLERES_H,
  RADIO_MSGBMCONFIG_CABLERES_L,
  RADIO_MSGBMCONFIG_ALGNO_H,
  RADIO_MSGBMCONFIG_ALGNO_L,
  RADIO_MSGBMCONFIG_U16SPARE5_H,
  RADIO_MSGBMCONFIG_U16SPARE5_L,
  RADIO_MSGBMCONFIG_BBCGROUP_H,
  RADIO_MSGBMCONFIG_BBCGROUP_L,
  RADIO_MSGBMCONFIG_INSTPERIOD_H,
  RADIO_MSGBMCONFIG_INSTPERIOD_L,
  RADIO_MSGBMCONFIG_CELLTAP,
  RADIO_MSGBMCONFIG_VBL,
  RADIO_MSGBMCONFIG_ACIDSENSOR,
  RADIO_MSGBMCONFIG_TMAX_H,
  RADIO_MSGBMCONFIG_TMAX_L,
  RADIO_MSGBMCONFIG_ROUTFUNC,
  RADIO_MSGBMCONFIG_DEL_H,
  RADIO_MSGBMCONFIG_DEL_L,
  RADIO_MSGBMCONFIG_CEL_H,
  RADIO_MSGBMCONFIG_CEL_L,
  RADIO_MSGBMCONFIG_CEFFICIENCY,
  RADIO_MSGBMCONFIG_CLEARSTATISTICS,
  RADIO_MSGBMCONFIG_U32SPARE1_H,
  RADIO_MSGBMCONFIG_U32SPARE1_HM,
  RADIO_MSGBMCONFIG_U32SPARE1_LM,
  RADIO_MSGBMCONFIG_U32SPARE1_L,
  RADIO_MSGBMCONFIG_U32SPARE2_H,
  RADIO_MSGBMCONFIG_U32SPARE2_HM,
  RADIO_MSGBMCONFIG_U32SPARE2_LM,
  RADIO_MSGBMCONFIG_U32SPARE2_L,
  RADIO_MSGBMCONFIG_U32SPARE3_H,
  RADIO_MSGBMCONFIG_U32SPARE3_HM,
  RADIO_MSGBMCONFIG_U32SPARE3_LM,
  RADIO_MSGBMCONFIG_U32SPARE3_L,
  RADIO_MSGBMCONFIG_SECURITYCODE1_H,
  RADIO_MSGBMCONFIG_SECURITYCODE1_L,
  RADIO_MSGBMCONFIG_SECURITYCODE2_H,
  RADIO_MSGBMCONFIG_SECURITYCODE2_L,
  RADIO_MSGBMCONFIG_U16SPARE3_H,
  RADIO_MSGBMCONFIG_U16SPARE3_L,
  RADIO_MSGBMCONFIG_U16SPARE4_H,
  RADIO_MSGBMCONFIG_U16SPARE4_L,
  RADIO_MSGBMCONFIG_BMBITCONFIG,
  RADIO_MSGBMCONFIG_U8SPARE2,
  RADIO_MSGBMCONFIG_U8SPARE3,
  RADIO_MSGBMCONFIG_U8SPARE4,
  RADIO_MSGBMCONFIG_SIZE
};

/**
 * Access OTA protocol BM Get_BmBatteryData message bytes.
 */

enum radio__acMsgGetBmBatteryData {
	RADIO__MSGGETBMBDATA_CURRLEVEL1 = 0,
	RADIO__MSGGETBMBDATA_CURRLEVEL2,
	RADIO__MSGGETBMBDATA_CURRLEVEL3,
	RADIO__MSGGETBMBDATA_SIZE
/**< Size of GetBmBatteryData message	*/
};

/**
 * Access OTA protocol BM Get_Config message bytes.
 */

enum radio__acMsgGetConfig {
	RADIO__MSGGETCONFIG_SIZE = 0
/**< Size of GetConfig message	*/
};

/**
 * Access OTA protocol BM CaPack message bytes.
 */

enum radio__acMsgBMCaPack {
	RADIO__MSGBMCP_CAPACK = 0
/**< Package data					*/
};

/**
 * Access OTA protocol CM meas message bytes.
 * Sent as response to GetMeas.
 */

enum radio__acMsgCMmeas { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMMEAS_PAC_H = 0, /**< AC Power consumption			*/
	RADIO__MSGCMMEAS_PAC_HM, /**< AC Power consumption			*/
	RADIO__MSGCMMEAS_PAC_HL, /**< AC Power consumption			*/
	RADIO__MSGCMMEAS_PAC_L, /**< AC Power consumption			*/
	RADIO__MSGCMMEAS_UCHALG_H, /**< Measured voltage				*/
	RADIO__MSGCMMEAS_UCHALG_HM, /**< Measured voltage				*/
	RADIO__MSGCMMEAS_UCHALG_LM, /**< Measured voltage				*/
	RADIO__MSGCMMEAS_UCHALG_L, /**< Measured voltage				*/
	RADIO__MSGCMMEAS_ICHALG_H, /**< Current output from charger	*/
	RADIO__MSGCMMEAS_ICHALG_HM, /**< Current output from charger	*/
	RADIO__MSGCMMEAS_ICHALG_LM, /**< Current output from charger	*/
	RADIO__MSGCMMEAS_ICHALG_L, /**< Current output from charger	*/
	RADIO__MSGCMMEAS_TBOARD_H, /**< CM board temperature			*/
	RADIO__MSGCMMEAS_TBOARD_L, /**< CM board temperature			*/
	RADIO__MSGCMMEAS_THS_H, /**< Heatsink temperature			*/
	RADIO__MSGCMMEAS_THS_L, /**< Heatsink temperature			*/
	RADIO__MSGCMMEAS_U32SPARE1_H, /**< u32 spare			*/
	RADIO__MSGCMMEAS_U32SPARE1_HM, /**< u32 spare			*/
	RADIO__MSGCMMEAS_U32SPARE1_LM, /**< u32 spare			*/
	RADIO__MSGCMMEAS_U32SPARE1_L, /**< u32 spare			*/
	RADIO__MSGCMMEAS_U16SPARE1_H, /**< u16 spare			*/
	RADIO__MSGCMMEAS_U16SPARE1_L, /**< u16 spare			*/
	RADIO__MSGCMMEAS_U8SPARE1, /**< u8 spare			*/
	RADIO__MSGCMMEAS_SIZE
/**< Size of CM Meas message		*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol GetBmMeas message bytes.
 * Sent from CM to BM every minute during charging. Expected BMmeas as response.
 */

enum radio__acMsgGetBMmeas { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGGETBMMEAS_STATUS_H = 0, /**< Chalg status				*/
	RADIO__MSGGETBMMEAS_STATUS_L, /**< Chalg status					*/
	RADIO__MSGGETBMMEAS_UCHALG_H, /**< Measured voltage				*/
	RADIO__MSGGETBMMEAS_UCHALG_HM, /**< Measured voltage			*/
	RADIO__MSGGETBMMEAS_UCHALG_LM, /**< Measured voltage			*/
	RADIO__MSGGETBMMEAS_UCHALG_L, /**< Measured voltage				*/
	RADIO__MSGGETBMMEAS_ICHALG_H, /**< Current output from charger	*/
	RADIO__MSGGETBMMEAS_ICHALG_HM, /**< Current output from charger	*/
	RADIO__MSGGETBMMEAS_ICHALG_LM, /**< Current output from charger	*/
	RADIO__MSGGETBMMEAS_ICHALG_L, /**< Current output from charger	*/
	RADIO__MSGGETBMMEAS_U32SPARE1_H, /**< u32 spare1				*/
	RADIO__MSGGETBMMEAS_U32SPARE1_HM, /**< 							*/
	RADIO__MSGGETBMMEAS_U32SPARE1_LM, /**< 							*/
	RADIO__MSGGETBMMEAS_U32SPARE1_L, /**< 							*/
	RADIO__MSGGETBMMEAS_U32SPARE2_H, /**< u32 spare2				*/
	RADIO__MSGGETBMMEAS_U32SPARE2_HM, /**< 							*/
	RADIO__MSGGETBMMEAS_U32SPARE2_LM, /**< 							*/
	RADIO__MSGGETBMMEAS_U32SPARE2_L, /**< 							*/
	RADIO__MSGGETBMMEAS_U16SPARE1_H, /**< u16 spare1				*/
	RADIO__MSGGETBMMEAS_U16SPARE1_L, /**< 							*/
	RADIO__MSGGETBMMEAS_U16SPARE2_H, /**< u16 spare2				*/
	RADIO__MSGGETBMMEAS_U16SPARE2_L, /**< 							*/
	RADIO__MSGGETBMMEAS_U8SPARE1, /**< u8 spare1					*/
	RADIO__MSGGETBMMEAS_U8SPARE2, /**< u8 spare2					*/
	RADIO__MSGGETBMMEAS_SIZE
/**< Size of CM Meas message		*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol BBC bytes.
 * Best battery choice sent from CM.
 */

enum radio__acMsgBBC { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGBBC_GROUP = 0, /**< BBC power group				*/
	RADIO__MSGBBC_STATUS, /**< Selected bitcoded chalgstatus	*/
	RADIO__MSGBBC_TIME_H, /**< Time since last status change	*/
	RADIO__MSGBBC_TIME_HM, /**< Time since last status change	*/
	RADIO__MSGBBC_TIME_LM, /**< Time since last status change	*/
	RADIO__MSGBBC_TIME_L, /**< Time since last status change	*/
	RADIO__MSGBBC_TBATT_H, /**< Battery temperature				*/
	RADIO__MSGBBC_TBATT_L, /**< Battery temperature				*/
	RADIO__MSGBBC_CID_H,	/**< Charger id						*/
	RADIO__MSGBBC_CID_HM,	/**< Charger id						*/
	RADIO__MSGBBC_CID_LM,	/**< Charger id						*/
	RADIO__MSGBBC_CID_L,	/**< Charger id						*/
	RADIO__MSGBBC_U32SPARE1_H, /**< u32 spare			*/
	RADIO__MSGBBC_U32SPARE1_HM, /**< u32 spare			*/
	RADIO__MSGBBC_U32SPARE1_LM, /**< u32 spare			*/
	RADIO__MSGBBC_U32SPARE1_L, /**< u32 spare			*/
	RADIO__MSGBBC_U16SPARE1_H, /**< u16 spare			*/
	RADIO__MSGBBC_U16SPARE1_L, /**< u16 spare			*/
	RADIO__MSGBBC_U8SPARE1, /**< u8 spare			*/
	RADIO__MSGBBC_SIZE
/**< Size of BBC message			*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol CmEoc bytes.
 * End of charge. Sent from CM to BM when battery cable is disconnected.
 * Acknowledge from BM expected.
 */

enum radio__acMsgCmEoc { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMEOC_CHALGSTATUSSUM_H = 0, /**< */
	RADIO__MSGCMEOC_CHALGSTATUSSUM_L, /**< */
	RADIO__MSGCMEOC_CHARGEDAH_H, /**< */
	RADIO__MSGCMEOC_CHARGEDAH_HM, /**< */
	RADIO__MSGCMEOC_CHARGEDAH_LM, /**< */
	RADIO__MSGCMEOC_CHARGEDAH_L, /**< */
	RADIO__MSGCMEOC_CHARGEDWH_H, /**< */
	RADIO__MSGCMEOC_CHARGEDWH_HM, /**< */
	RADIO__MSGCMEOC_CHARGEDWH_LM, /**< */
	RADIO__MSGCMEOC_CHARGEDWH_L, /**< */
	RADIO__MSGCMEOC_U32SPARE1_H, /**< u32 spare1			*/
	RADIO__MSGCMEOC_U32SPARE1_HM, /**< 						*/
	RADIO__MSGCMEOC_U32SPARE1_LM, /**< 						*/
	RADIO__MSGCMEOC_U32SPARE1_L, /**< 						*/
	RADIO__MSGCMEOC_U32SPARE2_H, /**< u32 spare2			*/
	RADIO__MSGCMEOC_U32SPARE2_HM, /**< 						*/
	RADIO__MSGCMEOC_U32SPARE2_LM, /**< 						*/
	RADIO__MSGCMEOC_U32SPARE2_L, /**< 						*/
	RADIO__MSGCMEOC_U16SPARE1_H, /**< u16 spare1			*/
	RADIO__MSGCMEOC_U16SPARE1_L, /**< 						*/
	RADIO__MSGCMEOC_U16SPARE2_H, /**< u16 spare2			*/
	RADIO__MSGCMEOC_U16SPARE2_L, /**< 						*/
	RADIO__MSGCMEOC_U8SPARE1, /**< u8 spare1				*/
	RADIO__MSGCMEOC_U8SPARE2, /**< u8 spare2				*/
	RADIO__MSGCMEOC_SIZE
/**< Size of CmEoc message			*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/**
 * Access OTA protocol CmDplMasterAll bytes.
 * CmDplMasterAll. Sent broadcast from CM DPL master to all CM DPL slaves to update power allocation.
 */

enum radio__acMsgCmDplMasterAll { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMDPLMA_PWRGROUP = 0, /**< */
	RADIO__MSGCMDPLMA_PWRLIMTOT_H, /**< */
	RADIO__MSGCMDPLMA_PWRLIMTOT_HM, /**< */
	RADIO__MSGCMDPLMA_PWRLIMTOT_LM, /**< */
	RADIO__MSGCMDPLMA_PWRLIMTOT_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCTOT_H, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCTOT_HM, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCTOT_LM, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCTOT_L, /**< */
	RADIO__MSGCMDPLMA_PWRREQTOT_H, /**< */
	RADIO__MSGCMDPLMA_PWRREQTOT_HM, /**< */
	RADIO__MSGCMDPLMA_PWRREQTOT_LM, /**< */
	RADIO__MSGCMDPLMA_PWRREQTOT_L, /**< */
	RADIO__MSGCMDPLMA_PWRUNITS_H, /**< */
	RADIO__MSGCMDPLMA_PWRUNITS_HM, /**< */
	RADIO__MSGCMDPLMA_PWRUNITS_LM, /**< */
	RADIO__MSGCMDPLMA_PWRUNITS_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCM_H, /**< PowerAllocatedMaster	*/
	RADIO__MSGCMDPLMA_PWRALLOCM_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS1_H, /**< PowerAllocatedSlave1	*/
	RADIO__MSGCMDPLMA_PWRALLOCS1_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS2_H, /**< PowerAllocatedSlave2	*/
	RADIO__MSGCMDPLMA_PWRALLOCS2_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS3_H, /**< PowerAllocatedSlave3	*/
	RADIO__MSGCMDPLMA_PWRALLOCS3_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS4_H, /**< PowerAllocatedSlave4	*/
	RADIO__MSGCMDPLMA_PWRALLOCS4_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS5_H, /**< PowerAllocatedSlave5	*/
	RADIO__MSGCMDPLMA_PWRALLOCS5_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS6_H, /**< PowerAllocatedSlave6	*/
	RADIO__MSGCMDPLMA_PWRALLOCS6_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS7_H, /**< PowerAllocatedSlave7	*/
	RADIO__MSGCMDPLMA_PWRALLOCS7_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS8_H, /**< PowerAllocatedSlave8	*/
	RADIO__MSGCMDPLMA_PWRALLOCS8_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS9_H, /**< PowerAllocatedSlave9	*/
	RADIO__MSGCMDPLMA_PWRALLOCS9_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS10_H, /**< PowerAllocatedSlave10	*/
	RADIO__MSGCMDPLMA_PWRALLOCS10_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS11_H, /**< PowerAllocatedSlave11	*/
	RADIO__MSGCMDPLMA_PWRALLOCS11_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS12_H, /**< PowerAllocatedSlave12	*/
	RADIO__MSGCMDPLMA_PWRALLOCS12_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS13_H, /**< PowerAllocatedSlave13	*/
	RADIO__MSGCMDPLMA_PWRALLOCS13_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS14_H, /**< PowerAllocatedSlave14	*/
	RADIO__MSGCMDPLMA_PWRALLOCS14_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS15_H, /**< PowerAllocatedSlave15	*/
	RADIO__MSGCMDPLMA_PWRALLOCS15_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS16_H, /**< PowerAllocatedSlave16	*/
	RADIO__MSGCMDPLMA_PWRALLOCS16_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS17_H, /**< PowerAllocatedSlave17	*/
	RADIO__MSGCMDPLMA_PWRALLOCS17_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS18_H, /**< PowerAllocatedSlave18	*/
	RADIO__MSGCMDPLMA_PWRALLOCS18_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS19_H, /**< PowerAllocatedSlave19	*/
	RADIO__MSGCMDPLMA_PWRALLOCS19_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS20_H, /**< PowerAllocatedSlave20	*/
	RADIO__MSGCMDPLMA_PWRALLOCS20_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS21_H, /**< PowerAllocatedSlave21	*/
	RADIO__MSGCMDPLMA_PWRALLOCS21_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS22_H, /**< PowerAllocatedSlave22	*/
	RADIO__MSGCMDPLMA_PWRALLOCS22_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS23_H, /**< PowerAllocatedSlave23	*/
	RADIO__MSGCMDPLMA_PWRALLOCS23_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS24_H, /**< PowerAllocatedSlave24	*/
	RADIO__MSGCMDPLMA_PWRALLOCS24_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS25_H, /**< PowerAllocatedSlave25	*/
	RADIO__MSGCMDPLMA_PWRALLOCS25_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS26_H, /**< PowerAllocatedSlave26	*/
	RADIO__MSGCMDPLMA_PWRALLOCS26_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS27_H, /**< PowerAllocatedSlave27	*/
	RADIO__MSGCMDPLMA_PWRALLOCS27_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS28_H, /**< PowerAllocatedSlave28	*/
	RADIO__MSGCMDPLMA_PWRALLOCS28_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS29_H, /**< PowerAllocatedSlave29	*/
	RADIO__MSGCMDPLMA_PWRALLOCS29_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS30_H, /**< PowerAllocatedSlave30	*/
	RADIO__MSGCMDPLMA_PWRALLOCS30_L, /**< */
	RADIO__MSGCMDPLMA_PWRALLOCS31_H, /**< PowerAllocatedSlave31	*/
	RADIO__MSGCMDPLMA_PWRALLOCS31_L, /**< */
	RADIO__MSGCMDPLMA_PWRCSUMDTOT_H, /**< Power consumed total	*/
	RADIO__MSGCMDPLMA_PWRCSUMDTOT_HM, /**< */
	RADIO__MSGCMDPLMA_PWRCSUMDTOT_LM, /**< */
	RADIO__MSGCMDPLMA_PWRCSUMDTOT_L, /**< */
	RADIO__MSGCMDPLMA_U16SPARE1_H, /**< u16 spare1	*/
	RADIO__MSGCMDPLMA_U16SPARE1_L, /**< */
	RADIO__MSGCMDPLMA_U8SPARE1, /**< u8 spare1		*/
	RADIO__MSGCMDPLMA_SIZE
/**< Size of CmDplMasterAll message			*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol CmDplMasterSlave bytes.
 * CmDplMasterSlave. Sent unicast from CM DPL master to one CM DPL slave to update slave unit ID.
 */

enum radio__acMsgCmDplMasterSlave { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMDPLMS_PWRGROUP = 0, /**< */
	RADIO__MSGCMDPLMS_PWRUINT, /**< */
	RADIO__MSGCMDPLMS_PWRALLOC_H, /**< PowerAllocatedMaster	*/
	RADIO__MSGCMDPLMS_PWRALLOC_L, /**< */
	RADIO__MSGCMDPLMS_U32SPARE1_H, /**< u32 spare1	*/
	RADIO__MSGCMDPLMS_U32SPARE1_HM, /**< */
	RADIO__MSGCMDPLMS_U32SPARE1_LM, /**< */
	RADIO__MSGCMDPLMS_U32SPARE1_L, /**< */
	RADIO__MSGCMDPLMS_U16SPARE1_H, /**< u16 spare1	*/
	RADIO__MSGCMDPLMS_U16SPARE1_L, /**< */
	RADIO__MSGCMDPLMS_U8SPARE1, /**< u8 spare1		*/
	RADIO__MSGCMDPLMS_SIZE
/**< Size of CmDplMasterSlave message			*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol Get_CmDplSlave bytes.
 * Get_CmDplSlave. Sent unicast/broadcast from CM DPL master to CM DPL slave(s) to get updated information.
 */

enum radio__acMsgGetCmDplSlave { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGGETCMDPLS_PWRGROUP = 0, /**< */
	RADIO__MSGGETCMDPLS_PWRUINT, /**< */
	RADIO__MSGGETCMDPLS_U32SPARE1_H, /**< u32 spare1	*/
	RADIO__MSGGETCMDPLS_U32SPARE1_HM, /**< */
	RADIO__MSGGETCMDPLS_U32SPARE1_LM, /**< */
	RADIO__MSGGETCMDPLS_U32SPARE1_L, /**< */
	RADIO__MSGGETCMDPLS_U16SPARE1_H, /**< u16 spare1	*/
	RADIO__MSGGETCMDPLS_U16SPARE1_L, /**< */
	RADIO__MSGGETCMDPLS_U8SPARE1, /**< u8 spare1		*/
	RADIO__MSGGETCMDPLS_SIZE
/**< Size of Get_CmDplSlave message			*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol CmDplSlave bytes.
 * CmDplSlave. Sent unicast from CM DPL slave to CM DPL master with power request.
 */

enum radio__acMsgCmDplSlave { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMDPLS_PWRGROUP = 0, /**< */
	RADIO__MSGCMDPLS_PWRUINT, /**< */
	RADIO__MSGCMDPLS_SOC, /**< State of charge	*/
	RADIO__MSGCMDPLS_PRIO, /**< Priority factor		*/
	RADIO__MSGCMDPLS_PWRREQ_H, /**< Power request	*/
	RADIO__MSGCMDPLS_PWRREQ_L, /**< */
	RADIO__MSGCMDPLS_PWRCSUMD_H, /**< Power consumed	*/
	RADIO__MSGCMDPLS_PWRCSUMD_HM, /**< */
	RADIO__MSGCMDPLS_PWRCSUMD_LM, /**< */
	RADIO__MSGCMDPLS_PWRCSUMD_L, /**< */
	RADIO__MSGCMDPLS_U16SPARE1_H, /**< u16 spare1	*/
	RADIO__MSGCMDPLS_U16SPARE1_L, /**< */
	RADIO__MSGCMDPLS_U8SPARE1, /**< u8 spare1		*/
	RADIO__MSGCMDPLS_SIZE
/**< Size of CmDplSlave message			*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol CM status message bytes.
 */

enum radio__acMsgCMStatus { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMST_CMTYPE_H = 0, /**< Charger type					*/
	RADIO__MSGCMST_CMTYPE_HM,
	RADIO__MSGCMST_CMTYPE_LM,
	RADIO__MSGCMST_CMTYPE_L,
//	RADIO__MSGCMST_SERIALNO_H,
//	RADIO__MSGCMST_SERIALNO_HM,
//	RADIO__MSGCMST_SERIALNO_LM,
//	RADIO__MSGCMST_SERIALNO_L,
	RADIO__MSGCMST_CMID_HHH, /**< First byte ofCm ID (64bit)		*/
	RADIO__MSGCMST_CMID_HH,
	RADIO__MSGCMST_CMID_H,
	RADIO__MSGCMST_CMID_HM,
	RADIO__MSGCMST_CMID_LM,
	RADIO__MSGCMST_CMID_L,
	RADIO__MSGCMST_CMID_LL,
	RADIO__MSGCMST_CMID_LLL,
	RADIO__MSGCMST_TIME_H, /**< First byte of Local CM time (POSIX) */
	RADIO__MSGCMST_TIME_HM,
	RADIO__MSGCMST_TIME_LM,
	RADIO__MSGCMST_TIME_L,
	RADIO__MSGCMST_CTIMETOTAL_H, /**< Total charge time in hours*/
	RADIO__MSGCMST_CTIMETOTAL_HM,
	RADIO__MSGCMST_CTIMETOTAL_LM,
	RADIO__MSGCMST_CTIMETOTAL_L,
	RADIO__MSGCMST_CAHTOTAL_H, /**< Total charged Ah */
	RADIO__MSGCMST_CAHTOTAL_HM,
	RADIO__MSGCMST_CAHTOTAL_LM,
	RADIO__MSGCMST_CAHTOTAL_L,
	RADIO__MSGCMST_ACWHTOTAL_H, /**< Total consumed KWh */
	RADIO__MSGCMST_ACWHTOTAL_HM,
	RADIO__MSGCMST_ACWHTOTAL_LM,
	RADIO__MSGCMST_ACWHTOTAL_L,
	RADIO__MSGCMST_CYCLESUM1_H,	/**< Cycles in interval 1 (2-25%) */
	RADIO__MSGCMST_CYCLESUM1_L,
	RADIO__MSGCMST_CYCLESUM2_H,	/**< Cycles in interval 1 (26-50%) */
	RADIO__MSGCMST_CYCLESUM2_L,
	RADIO__MSGCMST_CYCLESUM3_H,	/**< Cycles in interval 1 (51-80%) */
	RADIO__MSGCMST_CYCLESUM3_L,
	RADIO__MSGCMST_CYCLESUM4_H,	/**< Cycles in interval 1 (81-90%) */
	RADIO__MSGCMST_CYCLESUM4_L,
	RADIO__MSGCMST_CYCLESUM5_H,	/**< Cycles in interval 1 (>90%) */
	RADIO__MSGCMST_CYCLESUM5_L,
//	RADIO__MSGCMST_FID_H, /**< Forklift Id if available. Else set to 0x00000000. */
//	RADIO__MSGCMST_FID_HM,
//	RADIO__MSGCMST_FID_LM,
//	RADIO__MSGCMST_FID_L,
//	RADIO__MSGCMST_BSN_H,
//	RADIO__MSGCMST_BSN_HM,
//	RADIO__MSGCMST_BSN_LM,
//	RADIO__MSGCMST_BSN_L,
//	RADIO__MSGCMST_BMFGID, /**< Battery Manufacturing Id i.e. Exide, Enersys. If available else set to 0x00. */
//	RADIO__MSGCMST_BATTERYTYPE,
	RADIO__MSGCMST_CHARGINGMODE,
	RADIO__MSGCMST_BID_H,
	RADIO__MSGCMST_BID_HM,
	RADIO__MSGCMST_BID_LM,
	RADIO__MSGCMST_BID_L,
	RADIO__MSGCMST_ALGNONAME_0, /**< Algorithm name byte 1.			*/
	RADIO__MSGCMST_ALGNONAME_1, /**< Algorithm name byte 2.			*/
	RADIO__MSGCMST_ALGNONAME_2, /**< Algorithm name byte 3.			*/
	RADIO__MSGCMST_ALGNONAME_3, /**< Algorithm name byte 4.			*/
	RADIO__MSGCMST_ALGNONAME_4, /**< Algorithm name byte 5.			*/
	RADIO__MSGCMST_ALGNONAME_5, /**< Algorithm name byte 6.			*/
	RADIO__MSGCMST_ALGNONAME_6, /**< Algorithm name byte 7.			*/
	RADIO__MSGCMST_ALGNONAME_7, /**< Algorithm name byte 8.			*/
	RADIO__MSGCMST_ALGNO_H,
	RADIO__MSGCMST_ALGNO_L,
	RADIO__MSGCMST_ALGNOVER_H,
	RADIO__MSGCMST_ALGNOVER_L,
	RADIO__MSGCMST_CAPACITY_H,
	RADIO__MSGCMST_CAPACITY_L,
	RADIO__MSGCMST_CABLERES_H,
	RADIO__MSGCMST_CABLERES_L,
	RADIO__MSGCMST_CELL_H,
	RADIO__MSGCMST_CELL_L,
	RADIO__MSGCMST_BASELOAD_H,
	RADIO__MSGCMST_BASELOAD_L,
	RADIO__MSGCMST_CHALGERROR_H,
	RADIO__MSGCMST_CHALGERROR_L,
	RADIO__MSGCMST_REGUERROR_H,
	RADIO__MSGCMST_REGUERROR_L,
	RADIO__MSGCMST_CHALGSTAT_H,
	RADIO__MSGCMST_CHALGSTAT_L,
	RADIO__MSGCMST_REGUSTAT_H,
	RADIO__MSGCMST_REGUSTAT_L,
//	RADIO__MSGCMST_PAC_H,
//	RADIO__MSGCMST_PAC_HM,
//	RADIO__MSGCMST_PAC_LM,
//	RADIO__MSGCMST_PAC_L,
//	RADIO__MSGCMST_UCHALG_H,
//	RADIO__MSGCMST_UCHALG_HM,
//	RADIO__MSGCMST_UCHALG_LM,
//	RADIO__MSGCMST_UCHALG_L,
//	RADIO__MSGCMST_ICHALG_H,
//	RADIO__MSGCMST_ICHALG_HM,
//	RADIO__MSGCMST_ICHALG_LM,
//	RADIO__MSGCMST_ICHALG_L,
	RADIO__MSGCMST_TBOARD_H,
	RADIO__MSGCMST_TBOARD_L,
	RADIO__MSGCMST_THS_H,
	RADIO__MSGCMST_THS_L,
//	RADIO__MSGCMST_HISTLOGIDX_H,
//	RADIO__MSGCMST_HISTLOGIDX_HM,
//	RADIO__MSGCMST_HISTLOGIDX_LM,
//	RADIO__MSGCMST_HISTLOGIDX_L,
//	RADIO__MSGCMST_EVTLOGIDX_H,
//	RADIO__MSGCMST_EVTLOGIDX_HM,
//	RADIO__MSGCMST_EVTLOGIDX_LM,
//	RADIO__MSGCMST_EVTLOGIDX_L,
//	RADIO__MSGCMST_INSTLOGIDX_H,
//	RADIO__MSGCMST_INSTLOGIDX_HM,
//	RADIO__MSGCMST_INSTLOGIDX_LM,
//	RADIO__MSGCMST_INSTLOGIDX_L,
	RADIO__MSGCMST_ACOVP_H,
	RADIO__MSGCMST_ACOVP_L,
	RADIO__MSGCMST_U32SPARE1_H,
	RADIO__MSGCMST_U32SPARE1_HM,
	RADIO__MSGCMST_U32SPARE1_LM,
	RADIO__MSGCMST_U32SPARE1_L,
	RADIO__MSGCMST_U32SPARE2_H,
	RADIO__MSGCMST_U32SPARE2_HM,
	RADIO__MSGCMST_U32SPARE2_LM,
	RADIO__MSGCMST_U32SPARE2_L,
	RADIO__MSGCMST_U16SPARE1_H,
	RADIO__MSGCMST_U16SPARE1_L,
	RADIO__MSGCMST_SIZE
/**< Size of CM Status message		*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol CM config message package 1 bytes.
 */
#define RADIO__MSGCMCFG_SEGMENTS	4	/**< number of segmentes */
enum radio__acMsgCMConfig1 { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMCFG1_PANID_H = 0, /**< PAN id							*/
	RADIO__MSGCMCFG1_PANID_L, /**< PAN id							*/
	RADIO__MSGCMCFG1_CHANNELID, /**< Channel id						*/
	RADIO__MSGCMCFG1_NWKID_H, /**< Network id (node id)			*/
	RADIO__MSGCMCFG1_NWKID_L, /**< Network id (node id)			*/
	RADIO__MSGCMCFG1_SERIALNO_H, /**< Hardware serial number			*/
	RADIO__MSGCMCFG1_SERIALNO_HM, /**< Hardware serial number			*/
	RADIO__MSGCMCFG1_SERIALNO_LM, /**< Hardware serial number			*/
	RADIO__MSGCMCFG1_SERIALNO_L, /**< Hardware serial number			*/
	RADIO__MSGCMCFG1_ENGINECODE_H, /**< Engine code					*/
	RADIO__MSGCMCFG1_ENGINECODE_HM, /**< Engine code					*/
	RADIO__MSGCMCFG1_ENGINECODE_LM, /**< Engine code					*/
	RADIO__MSGCMCFG1_ENGINECODE_L, /**< Engine code					*/
	RADIO__MSGCMCFG1_BATTERYTYPE, /**< Battery type					*/
	RADIO__MSGCMCFG1_ALGNO_H, /**< Default algorithm number		*/
	RADIO__MSGCMCFG1_ALGNO_L, /**< Default algorithm number		*/
	RADIO__MSGCMCFG1_CAPACITY_H, /**< Default rated capacity			*/
	RADIO__MSGCMCFG1_CAPACITY_L, /**< Default rated capacity			*/
	RADIO__MSGCMCFG1_CABLERES_H, /**< Default cable resistance		*/
	RADIO__MSGCMCFG1_CABLERES_L, /**< Default cable resistance		*/
	RADIO__MSGCMCFG1_CELL_H, /**< Default number of cells		*/
	RADIO__MSGCMCFG1_CELL_L, /**< Default number of cells		*/
	RADIO__MSGCMCFG1_BASELOAD_H, /**< Default base load				*/
	RADIO__MSGCMCFG1_BASELOAD_L, /**< Default base load				*/
	RADIO__MSGCMCFG1_POWERGROUP, /**< Group number					*/
	RADIO__MSGCMCFG1_IACLIMIT_H, /**< Mac AC current consumption		*/
	RADIO__MSGCMCFG1_IACLIMIT_HM, /**< Mac AC current consumption		*/
	RADIO__MSGCMCFG1_IACLIMIT_LM, /**< Mac AC current consumption		*/
	RADIO__MSGCMCFG1_IACLIMIT_L, /**< Mac AC current consumption		*/
	RADIO__MSGCMCFG1_PACLIMIT_H, /**< Max AC power consumption		*/
	RADIO__MSGCMCFG1_PACLIMIT_HM,
	RADIO__MSGCMCFG1_PACLIMIT_LM,
	RADIO__MSGCMCFG1_PACLIMIT_L,
	RADIO__MSGCMCFG1_IDCLIMIT_H,
	RADIO__MSGCMCFG1_IDCLIMIT_HM,
	RADIO__MSGCMCFG1_IDCLIMIT_LM,
	RADIO__MSGCMCFG1_IDCLIMIT_L,
/*
	RADIO__MSGCMCFG1_UADSLOPE_H,
	RADIO__MSGCMCFG1_UADSLOPE_HM,
	RADIO__MSGCMCFG1_UADSLOPE_LM,
	RADIO__MSGCMCFG1_UADSLOPE_L,
	RADIO__MSGCMCFG1_UADOFFSET_H,
	RADIO__MSGCMCFG1_UADOFFSET_HM,
	RADIO__MSGCMCFG1_UADOFFSET_LM,
	RADIO__MSGCMCFG1_UADOFFSET_L,
	RADIO__MSGCMCFG1_IADSLOPE_H,
	RADIO__MSGCMCFG1_IADSLOPE_HM,
	RADIO__MSGCMCFG1_IADSLOPE_LM,
	RADIO__MSGCMCFG1_IADSLOPE_L,
	RADIO__MSGCMCFG1_IADOFFSET_H,
	RADIO__MSGCMCFG1_IADOFFSET_HM,
	RADIO__MSGCMCFG1_IADOFFSET_LM,
	RADIO__MSGCMCFG1_IADOFFSET_L,
	RADIO__MSGCMCFG1_ISETSLOPE_H,
	RADIO__MSGCMCFG1_ISETSLOPE_HM,
	RADIO__MSGCMCFG1_ISETSLOPE_LM,
	RADIO__MSGCMCFG1_ISETSLOPE_L,
	RADIO__MSGCMCFG1_ISETOFFSET_H,
	RADIO__MSGCMCFG1_ISETOFFSET_HM,
	RADIO__MSGCMCFG1_ISETOFFSET_LM,
	RADIO__MSGCMCFG1_ISETOFFSET_L,
*/
	RADIO__MSGCMCFG1_U32SPARE5_H, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE5_HM, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE5_LM, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE5_L, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE6_H, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_U32SPARE6_HM, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_U32SPARE6_LM, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_U32SPARE6_L, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_U32SPARE7_H, /**< u32 spare3			*/
	RADIO__MSGCMCFG1_U32SPARE7_HM, /**< u32 spare3			*/
	RADIO__MSGCMCFG1_U32SPARE7_LM, /**< u32 spare3			*/
	RADIO__MSGCMCFG1_U32SPARE7_L, /**< u32 spare3			*/
	RADIO__MSGCMCFG1_U32SPARE8_H, /**< u32 spare4			*/
	RADIO__MSGCMCFG1_U32SPARE8_HM, /**< u32 spare4			*/
	RADIO__MSGCMCFG1_U32SPARE8_LM, /**< u32 spare4			*/
	RADIO__MSGCMCFG1_U32SPARE8_L, /**< u32 spare4			*/
	RADIO__MSGCMCFG1_U32SPARE9_H, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE9_HM, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE9_LM, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE9_L, /**< u32 spare1			*/
	RADIO__MSGCMCFG1_U32SPARE10_H, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_U32SPARE10_HM, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_U32SPARE10_LM, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_U32SPARE10_L, /**< u32 spare2			*/
	RADIO__MSGCMCFG1_DISPCONTRAST,
	RADIO__MSGCMCFG1_LEDBRIGHTMAX, /**< 		*/
	RADIO__MSGCMCFG1_LEDBRIGHTDIM, /**< 		*/
	RADIO__MSGCMCFG1_LANGUAGE, /**< 		*/
	RADIO__MSGCMCFG1_TIMEDATEFORMAT, /**< 		*/
	RADIO__MSGCMCFG1_BUTTONF1FUNC, /**< 		*/
	RADIO__MSGCMCFG1_BUTTONF2FUNC, /**< 		*/
	RADIO__MSGCMCFG1_REMOTEINFUNC, /**< 		*/
	RADIO__MSGCMCFG1_REMOTEOUTFUNC, /**< 		*/
	RADIO__MSGCMCFG1_REMOTEOUTALARMVAR,	/**< Remote out alarm variable	*/
	RADIO__MSGCMCFG1_REMOTEOUTPHASEVAR,	/**< Remote out phase variable	*/
	RADIO__MSGCMCFG1_WATERFUNC, /**< 		*/
	RADIO__MSGCMCFG1_WATERVAR, /**< 			*/
	RADIO__MSGCMCFG1_AIRPUMPVAR, /**< 		*/
	RADIO__MSGCMCFG1_AIRPUMPVAR2, /**< 		*/
	RADIO__MSGCMCFG1_PARALLELFUNC,
	RADIO__MSGCMCFG1_TIMERESTRICTION, /**< Time restriction status		*/
	RADIO__MSGCMCFG1_TIMETABLE_0, /**< Timetable array byte [0][0] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_1, /**< Timetable array byte [0][1] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_2, /**< Timetable array byte [0][2] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_3, /**< Timetable array byte [0][3] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_4, /**< Timetable array byte [0][4] 	*/

	RADIO__MSGCMCFG1_TIMETABLE_5, /**< Timetable array byte [1][0] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_6, /**< Timetable array byte [1][1] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_7, /**< Timetable array byte [1][2] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_8, /**< Timetable array byte [1][3] 	*/
	RADIO__MSGCMCFG1_TIMETABLE_9, /**< Timetable array byte [1][4] 	*/

	/** Total size of CM Config message package 1 (should be 88) */
	RADIO__MSGCMCFG1_SIZE,
};

/**
 * Access OTA protocol CM config message package 2 bytes.
 */

enum radio__acMsgCMConfig2 { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMCFG2_TIMETABLE_10 = 0, /**< Timetable array byte [2][0] 	*/
	RADIO__MSGCMCFG2_TIMETABLE_11, /**< Timetable array byte [2][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_12, /**< Timetable array byte [2][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_13, /**< Timetable array byte [2][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_14, /**< Timetable array byte [2][4]	*/

	RADIO__MSGCMCFG2_TIMETABLE_15, /**< Timetable array byte [3][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_16, /**< Timetable array byte [3][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_17, /**< Timetable array byte [3][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_18, /**< Timetable array byte [3][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_19, /**< Timetable array byte [3][4]	*/

	RADIO__MSGCMCFG2_TIMETABLE_20, /**< Timetable array byte [4][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_21, /**< Timetable array byte [4][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_22, /**< Timetable array byte [4][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_23, /**< Timetable array byte [4][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_24, /**< Timetable array byte [4][4]	*/

	RADIO__MSGCMCFG2_TIMETABLE_25, /**< Timetable array byte [5][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_26, /**< Timetable array byte [5][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_27, /**< Timetable array byte [5][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_28, /**< Timetable array byte [5][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_29, /**< Timetable array byte [5][4]	*/

	RADIO__MSGCMCFG2_TIMETABLE_30, /**< Timetable array byte [6][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_31, /**< Timetable array byte [6][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_32, /**< Timetable array byte [6][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_33, /**< Timetable array byte [6][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_34, /**< Timetable array byte [6][3]	*/

	RADIO__MSGCMCFG2_TIMETABLE_35, /**< Timetable array byte [7][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_36, /**< Timetable array byte [7][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_37, /**< Timetable array byte [7][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_38, /**< Timetable array byte [7][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_39, /**< Timetable array byte [7][3]	*/

	RADIO__MSGCMCFG2_TIMETABLE_40, /**< Timetable array byte [8][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_41, /**< Timetable array byte [8][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_42, /**< Timetable array byte [8][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_43, /**< Timetable array byte [8][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_44, /**< Timetable array byte [8][3]	*/

	RADIO__MSGCMCFG2_TIMETABLE_45, /**< Timetable array byte [9][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_46, /**< Timetable array byte [9][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_47, /**< Timetable array byte [9][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_48, /**< Timetable array byte [9][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_49, /**< Timetable array byte [9][2]	*/

	RADIO__MSGCMCFG2_TIMETABLE_50, /**< Timetable array byte [10][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_51, /**< Timetable array byte [10][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_52, /**< Timetable array byte [10][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_53, /**< Timetable array byte [10][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_54, /**< Timetable array byte [10][2]	*/

	RADIO__MSGCMCFG2_TIMETABLE_55, /**< Timetable array byte [11][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_56, /**< Timetable array byte [11][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_57, /**< Timetable array byte [11][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_58, /**< Timetable array byte [11][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_59, /**< Timetable array byte [11][2]	*/

	RADIO__MSGCMCFG2_TIMETABLE_60, /**< Timetable array byte [12][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_61, /**< Timetable array byte [12][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_62, /**< Timetable array byte [12][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_63, /**< Timetable array byte [12][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_64, /**< Timetable array byte [12][1]	*/

	RADIO__MSGCMCFG2_TIMETABLE_65, /**< Timetable array byte [13][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_66, /**< Timetable array byte [13][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_67, /**< Timetable array byte [13][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_68, /**< Timetable array byte [13][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_69, /**< Timetable array byte [13][1]	*/

	RADIO__MSGCMCFG2_TIMETABLE_70, /**< Timetable array byte [14][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_71, /**< Timetable array byte [14][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_72, /**< Timetable array byte [14][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_73, /**< Timetable array byte [14][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_74, /**< Timetable array byte [14][1]	*/

	RADIO__MSGCMCFG2_TIMETABLE_75, /**< Timetable array byte [15][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_76, /**< Timetable array byte [15][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_77, /**< Timetable array byte [15][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_78, /**< Timetable array byte [15][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_79, /**< Timetable array byte [15][0]	*/

	RADIO__MSGCMCFG2_TIMETABLE_80, /**< Timetable array byte [16][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_81, /**< Timetable array byte [16][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_82, /**< Timetable array byte [16][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_83, /**< Timetable array byte [16][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_84, /**< Timetable array byte [16][0]	*/

	RADIO__MSGCMCFG2_TIMETABLE_85, /**< Timetable array byte [17][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_86, /**< Timetable array byte [17][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_87, /**< Timetable array byte [17][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_88, /**< Timetable array byte [17][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_89, /**< Timetable array byte [17][0]	*/

	RADIO__MSGCMCFG2_TIMETABLE_90, /**< Timetable array byte [18][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_91, /**< Timetable array byte [18][2]	*/
	RADIO__MSGCMCFG2_TIMETABLE_92, /**< Timetable array byte [18][3]	*/
	RADIO__MSGCMCFG2_TIMETABLE_93, /**< Timetable array byte [18][4]	*/
	RADIO__MSGCMCFG2_TIMETABLE_94, /**< Timetable array byte [18][0]	*/

	RADIO__MSGCMCFG2_TIMETABLE_95, /**< Timetable array byte [19][0]	*/
	RADIO__MSGCMCFG2_TIMETABLE_96, /**< Timetable array byte [19][1]	*/
	RADIO__MSGCMCFG2_TIMETABLE_97, /**< Timetable array byte [19][2]	*/
	RADIO__MSGCMCFG2_SIZE
/**< Size of CM config package 2	*/
}; /* size should be 88 */

/**
 * Access OTA protocol CM config message package 3 bytes.
 */

enum radio__acMsgCMConfig3 {
	RADIO__MSGCMCFG3_TIMETABLE_98 = 0, /**< Timetable array byte [19][3]	*/
	RADIO__MSGCMCFG3_TIMETABLE_99, /**< Timetable array byte [19][4]	*/

	RADIO__MSGCMCFG3_TIMETABLE_100, /**< Timetable array byte [20][0]	*/
	RADIO__MSGCMCFG3_TIMETABLE_101, /**< Timetable array byte [20][1]	*/
	RADIO__MSGCMCFG3_TIMETABLE_102, /**< Timetable array byte [20][2]	*/
	RADIO__MSGCMCFG3_TIMETABLE_103, /**< Timetable array byte [20][3]	*/
	RADIO__MSGCMCFG3_TIMETABLE_104, /**< Timetable array byte [20][4]	*/

	RADIO__MSGCMCFG3_CANMODE, /**< 1=CAN is active				*/
	RADIO__MSGCMCFG3_CANNWKCTRL_H, /**< Each bit represents a can device.*/
	RADIO__MSGCMCFG3_CANNWKCTRL_L, /**< Each bit represents a can device.*/
	RADIO__MSGCMCFG3_PINOUTSELECT_0_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_0_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_0_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_0_L,
	RADIO__MSGCMCFG3_PINOUTSELECT_1_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_1_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_1_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_1_L,
	RADIO__MSGCMCFG3_PINOUTSELECT_2_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_2_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_2_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_2_L,
	RADIO__MSGCMCFG3_PINOUTSELECT_3_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_3_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_3_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_3_L,
	RADIO__MSGCMCFG3_PINOUTSELECT_4_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_4_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_4_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_4_L,
	RADIO__MSGCMCFG3_PINOUTSELECT_5_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_5_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_5_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_5_L,
	RADIO__MSGCMCFG3_PINOUTSELECT_6_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_6_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_6_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_6_L,
	RADIO__MSGCMCFG3_PINOUTSELECT_7_H,
	RADIO__MSGCMCFG3_PINOUTSELECT_7_HM,
	RADIO__MSGCMCFG3_PINOUTSELECT_7_LM,
	RADIO__MSGCMCFG3_PINOUTSELECT_7_L,
	RADIO__MSGCMCFG3_PININSELECT_0_H,
	RADIO__MSGCMCFG3_PININSELECT_0_HM,
	RADIO__MSGCMCFG3_PININSELECT_0_LM,
	RADIO__MSGCMCFG3_PININSELECT_0_L,
	RADIO__MSGCMCFG3_PININSELECT_1_H,
	RADIO__MSGCMCFG3_PININSELECT_1_HM,
	RADIO__MSGCMCFG3_PININSELECT_1_LM,
	RADIO__MSGCMCFG3_PININSELECT_1_L,
	RADIO__MSGCMCFG3_PININSELECT_2_H,
	RADIO__MSGCMCFG3_PININSELECT_2_HM,
	RADIO__MSGCMCFG3_PININSELECT_2_LM,
	RADIO__MSGCMCFG3_PININSELECT_2_L,
	RADIO__MSGCMCFG3_PININSELECT_3_H,
	RADIO__MSGCMCFG3_PININSELECT_3_HM,
	RADIO__MSGCMCFG3_PININSELECT_3_LM,
	RADIO__MSGCMCFG3_PININSELECT_3_L,
	RADIO__MSGCMCFG3_PININSELECT_4_H,
	RADIO__MSGCMCFG3_PININSELECT_4_HM,
	RADIO__MSGCMCFG3_PININSELECT_4_LM,
	RADIO__MSGCMCFG3_PININSELECT_4_L,
	RADIO__MSGCMCFG3_PININSELECT_5_H,
	RADIO__MSGCMCFG3_PININSELECT_5_HM,
	RADIO__MSGCMCFG3_PININSELECT_5_LM,
	RADIO__MSGCMCFG3_PININSELECT_5_L,
	RADIO__MSGCMCFG3_PININSELECT_6_H,
	RADIO__MSGCMCFG3_PININSELECT_6_HM,
	RADIO__MSGCMCFG3_PININSELECT_6_LM,
	RADIO__MSGCMCFG3_PININSELECT_6_L,
	RADIO__MSGCMCFG3_PININSELECT_7_H,
	RADIO__MSGCMCFG3_PININSELECT_7_HM,
	RADIO__MSGCMCFG3_PININSELECT_7_LM,
	RADIO__MSGCMCFG3_PININSELECT_7_L,
	RADIO__MSGCMCFG3_RADIOMODE, /**< 1=Radio active					*/
	RADIO__MSGCMCFG3_CHARGINGMODE,
	//RADIO__MSGCMCFG3_JOINENABLE,
	RADIO__MSGCMCFG3_NWKSETTINGS,
	RADIO__MSGCMCFG3_SECURITYCODE1_H,
	RADIO__MSGCMCFG3_SECURITYCODE1_HM,
	RADIO__MSGCMCFG3_SECURITYCODE1_LM,
	RADIO__MSGCMCFG3_SECURITYCODE1_L,
	RADIO__MSGCMCFG3_SECURITYCODE2_H,
	RADIO__MSGCMCFG3_SECURITYCODE2_HM,
	RADIO__MSGCMCFG3_SECURITYCODE2_LM,
	RADIO__MSGCMCFG3_SECURITYCODE2_L,
	RADIO__MSGCMCFG3_BLTIME, /**< Backlight time					*/
	RADIO__MSGCMCFG3_INSTLOGSAMPPER_H, /**< Instant log sample period		*/
	RADIO__MSGCMCFG3_INSTLOGSAMPPER_L, /**< Instant log sample period		*/
	RADIO__MSGCMCFG3_SIZE
/**< Size of CM config package 3	*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol CM config message package 4 bytes.
 */

enum radio__acMsgCMConfig4 {
	RADIO__MSGCMCFG4_CID_H,	/*CID Charger Id */
	RADIO__MSGCMCFG4_CID_HM,
	RADIO__MSGCMCFG4_CID_LM,
	RADIO__MSGCMCFG4_CID_L,
	RADIO__MSGCMCFG4_BBCFUNC, /**< 		*/
	RADIO__MSGCMCFG4_BBCVAR, /**< 		*/
	RADIO__MSGCMCFG4_EQUFUNC, /**< 		*/
	RADIO__MSGCMCFG4_EQUVAR, /**< 		*/
	RADIO__MSGCMCFG4_EQUVAR2, /**< 		*/
	RADIO__MSGCMCFG4_ROUTING, /**< 		*/
	RADIO__MSGCMCFG4_BITCONFIG, /**< 		*/
	RADIO__MSGCMCFG4_REMOTEOUTBBCVAR,	/**< Remote out alarm variable	*/
	RADIO__MSGCMCFG4_CANBPS, /**< 		*/
	RADIO__MSGCMCFG4_ECFUNC, /**< 		*/
	RADIO__MSGCMCFG4_ECVAR, /**< 		*/
	RADIO__MSGCMCFG4_ECVAR2, /**< 		*/
	RADIO__MSGCMCFG4_ECVAR3, /**< 		*/

	RADIO__MSGCMCFG4_HWTYPE_H, /**< u32 spare2			*/
	RADIO__MSGCMCFG4_HWTYPE_HM, /**< u32 spare2			*/
	RADIO__MSGCMCFG4_HWTYPE_LM, /**< u32 spare2			*/
	RADIO__MSGCMCFG4_HWTYPE_L, /**< u32 spare2			*/
	RADIO__MSGCMCFG4_HWVER_H, /**< u32 spare3			*/
	RADIO__MSGCMCFG4_HWVER_HM, /**< u32 spare3			*/
	RADIO__MSGCMCFG4_HWVER_LM, /**< u32 spare3			*/
	RADIO__MSGCMCFG4_HWVER_L, /**< u32 spare3			*/
	RADIO__MSGCMCFG4_DPLPACTOT_H, /**< u32 spare1			*/
	RADIO__MSGCMCFG4_DPLPACTOT_HM, /**< u32 spare1			*/
	RADIO__MSGCMCFG4_DPLPACTOT_LM, /**< u32 spare1			*/
	RADIO__MSGCMCFG4_DPLPACTOT_L, /**< u32 spare1			*/
	RADIO__MSGCMCFG4_U32SPARE4_H, /**< u32 spare4			*/
	RADIO__MSGCMCFG4_U32SPARE4_HM, /**< u32 spare4			*/
	RADIO__MSGCMCFG4_U32SPARE4_LM, /**< u32 spare4			*/
	RADIO__MSGCMCFG4_U32SPARE4_L, /**< u32 spare4			*/
	RADIO__MSGCMCFG4_BATTERYTEMP_H, /**< Battery temp (0,1 C/F)	*/
	RADIO__MSGCMCFG4_BATTERYTEMP_L, /**< 						*/
	RADIO__MSGCMCFG4_AIRPUMPLOW_H, /**< u16 spare2			*/
	RADIO__MSGCMCFG4_AIRPUMPLOW_L, /**< u16 spare2			*/
	RADIO__MSGCMCFG4_AIRPUMPHIGH_H, /**< u16 spare3			*/
	RADIO__MSGCMCFG4_AIRPUMPHIGH_L, /**< u16 spare3			*/
	RADIO__MSGCMCFG4_U16SPARE4_H, /**< u16 spare4			*/
	RADIO__MSGCMCFG4_U16SPARE4_L, /**< u16 spare4			*/
	RADIO__MSGCMCFG4_CANNODEID, /**< Can node id			*/
	RADIO__MSGCMCFG4_DPLFUNC, /**< u8 spare2			*/
	RADIO__MSGCMCFG4_DPLPRIO, /**< u8 spare3			*/
	RADIO__MSGCMCFG4_DPLPACDEF, /**< u8 spare4			*/
	//RADIO__MSGCMCFG4_ITHRESHOLDVAR,	/**< 	I threshold variable	*/ // sorabITR - not now
	//RADIO__MSGCMCFG4_ITHRESHOLDCOND,/**< 	I threshold condition	*/ // sorabITR - not now
	RADIO__MSGCMCFG4_SIZE
/**< Size of CM config package 4 = ??	*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol CMlog bytes.
 */

enum radio__acMsgCMlog { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMLOG_LOGTYPE = 0, /**< 0=Historical, 1=event, 2=instant*/
	RADIO__MSGCMLOG_LOGDATA
/**< Log data					 	*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#define RADIO__MSGCMHLOG_SEGMENTS	2	/**< number of segmentes */
/**< Historical Log data, segment 1						 		*/
enum radio__acMsgCMHistLog1 { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMHLOG1_LOGTYPE = 0, /**< 0=Historical, 1=event, 2=instant*/
	RADIO__MSGCMHLOG1_CHARGEIDX_H,
	RADIO__MSGCMHLOG1_CHARGEIDX_HM,
	RADIO__MSGCMHLOG1_CHARGEIDX_LM,
	RADIO__MSGCMHLOG1_CHARGEIDX_L,
	RADIO__MSGCMHLOG1_CHARGEIDXRESET_H,
	RADIO__MSGCMHLOG1_CHARGEIDXRESET_HM,
	RADIO__MSGCMHLOG1_CHARGEIDXRESET_LM,
	RADIO__MSGCMHLOG1_CHARGEIDXRESET_L,
	RADIO__MSGCMHLOG1_RECTYPE_H,
	RADIO__MSGCMHLOG1_RECTYPE_L,
	RADIO__MSGCMHLOG1_RECSIZE,
	RADIO__MSGCMHLOG1_CHARGESTARTTIME_H,
	RADIO__MSGCMHLOG1_CHARGESTARTTIME_HM,
	RADIO__MSGCMHLOG1_CHARGESTARTTIME_LM,
	RADIO__MSGCMHLOG1_CHARGESTARTTIME_L,
	RADIO__MSGCMHLOG1_CHARGEENDTIME_H,
	RADIO__MSGCMHLOG1_CHARGEENDTIME_HM,
	RADIO__MSGCMHLOG1_CHARGEENDTIME_LM,
	RADIO__MSGCMHLOG1_CHARGEENDTIME_L,
	RADIO__MSGCMHLOG1_ALGNO_H,
	RADIO__MSGCMHLOG1_ALGNO_L,
	RADIO__MSGCMHLOG1_CAPACITY_H,
	RADIO__MSGCMHLOG1_CAPACITY_L,
	RADIO__MSGCMHLOG1_CELLS_H,
	RADIO__MSGCMHLOG1_CELLS_L,
	RADIO__MSGCMHLOG1_CABLERES_H,
	RADIO__MSGCMHLOG1_CABLERES_L,
	RADIO__MSGCMHLOG1_BASELOAD_H,
	RADIO__MSGCMHLOG1_BASELOAD_L,
	RADIO__MSGCMHLOG1_THSSTART_H,
	RADIO__MSGCMHLOG1_THSSTART_L,
	RADIO__MSGCMHLOG1_THSEND_H,
	RADIO__MSGCMHLOG1_THSEND_L,
	RADIO__MSGCMHLOG1_THSMAX_H,
	RADIO__MSGCMHLOG1_THSMAX_L,
	RADIO__MSGCMHLOG1_STARTVOLTAGE_H,
	RADIO__MSGCMHLOG1_STARTVOLTAGE_HM,
	RADIO__MSGCMHLOG1_STARTVOLTAGE_LM,
	RADIO__MSGCMHLOG1_STARTVOLTAGE_L,
	RADIO__MSGCMHLOG1_ENDVOLTAGE_H,
	RADIO__MSGCMHLOG1_ENDVOLTAGE_HM,
	RADIO__MSGCMHLOG1_ENDTVOLTAGE_LM,
	RADIO__MSGCMHLOG1_ENDVOLTAGE_L,
	RADIO__MSGCMHLOG1_CHARGETIME_H,
	RADIO__MSGCMHLOG1_CHARGETIME_HM,
	RADIO__MSGCMHLOG1_CHARGETIME_LM,
	RADIO__MSGCMHLOG1_CHARGETIME_L,
	RADIO__MSGCMHLOG1_CHARGEDAH_H,
	RADIO__MSGCMHLOG1_CHARGEDAH_HM,
	RADIO__MSGCMHLOG1_CHARGEDAH_LM,
	RADIO__MSGCMHLOG1_CHARGEDAH_L,
	RADIO__MSGCMHLOG1_CHARGEDWH_H,
	RADIO__MSGCMHLOG1_CHARGEDWH_HM,
	RADIO__MSGCMHLOG1_CHARGEDWH_LM,
	RADIO__MSGCMHLOG1_CHARGEDWH_L,
	RADIO__MSGCMHLOG1_CHARGEDAHP_H,
	RADIO__MSGCMHLOG1_CHARGEDAHP_L,
	RADIO__MSGCMHLOG1_EQUTIME_H,
	RADIO__MSGCMHLOG1_EQUTIME_HM,
	RADIO__MSGCMHLOG1_EQTIME_LM,
	RADIO__MSGCMHLOG1_EQUTIME_L,
	RADIO__MSGCMHLOG1_EQUAH_H,
	RADIO__MSGCMHLOG1_EQUAH_HM,
	RADIO__MSGCMHLOG1_EQTAH_LM,
	RADIO__MSGCMHLOG1_EQUAH_L,
	RADIO__MSGCMHLOG1_EQUWH_H,
	RADIO__MSGCMHLOG1_EQUWH_HM,
	RADIO__MSGCMHLOG1_EQUWH_LM,
	RADIO__MSGCMHLOG1_EQUWH_L,
	RADIO__MSGCMHLOG1_CHALGERRORSUM_H,
	RADIO__MSGCMHLOG1_CHALGERRORSUM_L,
	RADIO__MSGCMHLOG1_REGUERRORSUM_H,
	RADIO__MSGCMHLOG1_REGUERRORSUM_L,
	RADIO__MSGCMHLOG1_EVTIDXSTART_H,
	RADIO__MSGCMHLOG1_EVTIDXSTART_HM,
	RADIO__MSGCMHLOG1_EVTIDXSTART_LM,
	RADIO__MSGCMHLOG1_EVTIDXSTART_L,
	RADIO__MSGCMHLOG1_EVTIDXSTOP_H,
	RADIO__MSGCMHLOG1_EVTIDXSTOP_HM,
	RADIO__MSGCMHLOG1_EVTIDXSTOP_LM,
	RADIO__MSGCMHLOG1_EVTIDXSTOP_L,
	RADIO__MSGCMHLOG1_BID_H,
	RADIO__MSGCMHLOG1_BID_HM,
	RADIO__MSGCMHLOG1_BID_LM,
	RADIO__MSGCMHLOG1_BID_L,
	RADIO__MSGCMHLOG1_BMFGID,
	RADIO__MSGCMHLOG1_BTYPE,
	RADIO__MSGCMHLOG1_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Historical Log data, segment 2, last					 	*/
enum radio__acMsgCMHistLog2 { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMHLOG2_BSN_H = 0,
	RADIO__MSGCMHLOG2_BSN_HM,
	RADIO__MSGCMHLOG2_BSN_LM,
	RADIO__MSGCMHLOG2_BSN_L,
	RADIO__MSGCMHLOG2_ENGINECODE_H,
	RADIO__MSGCMHLOG2_ENGINECODE_HM,
	RADIO__MSGCMHLOG2_ENGINECODE_LM,
	RADIO__MSGCMHLOG2_ENGINECODE_L,
	RADIO__MSGCMHLOG2_SERIALNO_H,
	RADIO__MSGCMHLOG2_SERIALNO_HM,
	RADIO__MSGCMHLOG2_SERIALNO_LM,
	RADIO__MSGCMHLOG2_SERIALNO_L,
	RADIO__MSGCMHLOG2_ALGNONAME_0, /**< Algorithm name byte 1.			*/
	RADIO__MSGCMHLOG2_ALGNONAME_1, /**< Algorithm name byte 2.			*/
	RADIO__MSGCMHLOG2_ALGNONAME_2, /**< Algorithm name byte 3.			*/
	RADIO__MSGCMHLOG2_ALGNONAME_3, /**< Algorithm name byte 4.			*/
	RADIO__MSGCMHLOG2_ALGNONAME_4, /**< Algorithm name byte 5.			*/
	RADIO__MSGCMHLOG2_ALGNONAME_5, /**< Algorithm name byte 6.			*/
	RADIO__MSGCMHLOG2_ALGNONAME_6, /**< Algorithm name byte 7.			*/
	RADIO__MSGCMHLOG2_ALGNONAME_7, /**< Algorithm name byte 8.			*/
//	RADIO__MSGCMHLOG2_BMID_HHH,
//	RADIO__MSGCMHLOG2_BMID_HH,
//	RADIO__MSGCMHLOG2_BMID_H,
//	RADIO__MSGCMHLOG2_BMID_HM,
//	RADIO__MSGCMHLOG2_BMID_LM,
//	RADIO__MSGCMHLOG2_BMID_L,
//	RADIO__MSGCMHLOG2_BMID_LL,
//	RADIO__MSGCMHLOG2_BMID_LLL,
	RADIO__MSGCMHLOG2_CHARGINGMODE,
	RADIO__MSGCMHLOG2_CHALGSTATSUM_H,
	RADIO__MSGCMHLOG2_CHALGSTATSUM_L,
	RADIO__MSGCMHLOG2_FID_H,
	RADIO__MSGCMHLOG2_FID_HM,
	RADIO__MSGCMHLOG2_FID_LM,
	RADIO__MSGCMHLOG2_FID_L,
	RADIO__MSGCMHLOG2_U32SPARE1_H, /**< u32 spare1			*/
	RADIO__MSGCMHLOG2_U32SPARE1_HM, /**< u32 spare1			*/
	RADIO__MSGCMHLOG2_U32SPARE1_LM, /**< u32 spare1			*/
	RADIO__MSGCMHLOG2_U32SPARE1_L, /**< u32 spare1			*/
	RADIO__MSGCMHLOG2_U32SPARE2_H, /**< u32 spare2			*/
	RADIO__MSGCMHLOG2_U32SPARE2_HM, /**< u32 spare2			*/
	RADIO__MSGCMHLOG2_U32SPARE2_LM, /**< u32 spare2			*/
	RADIO__MSGCMHLOG2_U32SPARE2_L, /**< u32 spare2			*/
	RADIO__MSGCMHLOG2_U16SPARE1_H, /**< u16 spare1			*/
	RADIO__MSGCMHLOG2_U16SPARE1_L, /**< u16 spare1			*/
	RADIO__MSGCMHLOG2_U16SPARE2_H, /**< u16 spare2			*/
	RADIO__MSGCMHLOG2_U16SPARE2_L, /**< u16 spare2			*/
	RADIO__MSGCMHLOG2_U8SPARE1, /**< u8 spare1			*/
	RADIO__MSGCMHLOG2_U8SPARE2, /**< u8 spare2			*/
	RADIO__MSGCMHLOG2_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data header					 	*/
enum radio__acMsgCMElog { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMELOG_INDEX_H = 0,
	RADIO__MSGCMELOG_INDEX_HM,
	RADIO__MSGCMELOG_INDEX_LM,
	RADIO__MSGCMELOG_INDEX_L,
	RADIO__MSGCMELOG_INDEXRESET_H,
	RADIO__MSGCMELOG_INDEXRESET_HM,
	RADIO__MSGCMELOG_INDEXRESET_LM,
	RADIO__MSGCMELOG_INDEXRESET_L,
	RADIO__MSGCMELOG_TIME_H,
	RADIO__MSGCMELOG_TIME_HM,
	RADIO__MSGCMELOG_TIME_LM,
	RADIO__MSGCMELOG_TIME_L,
	RADIO__MSGCMELOG_EVTID_H,
	RADIO__MSGCMELOG_EVTID_L,
	RADIO__MSGCMELOG_DATASIZE,
	RADIO__MSGCMELOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 1, TimeSet					 	*/
enum radio__acMsgCMEvtLogTimeSet { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME1LOG_OLDTIME_H = 0,
	RADIO__MSGCME1LOG_OLDTIME_HM,
	RADIO__MSGCME1LOG_OLDTIME_LM,
	RADIO__MSGCME1LOG_OLDTIME_L,
	RADIO__MSGCME1LOG_SOURCE,
	RADIO__MSGCME1LOG_U32SPARE1_H,
	RADIO__MSGCME1LOG_U32SPARE1_HM,
	RADIO__MSGCME1LOG_U32SPARE1_LM,
	RADIO__MSGCME1LOG_U32SPARE1_L,
	RADIO__MSGCME1LOG_U16SPARE1_H,
	RADIO__MSGCME1LOG_U16SPARE1_L,
	RADIO__MSGCME1LOG_U8SPARE1,
	RADIO__MSGCME1LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 2, ChargeStarted (CS)					 	*/
enum radio__acMsgCMEvtLogChargeStarted { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME2LOG_U32SPARE1_H = 0,
	RADIO__MSGCME2LOG_U32SPARE1_HM,
	RADIO__MSGCME2LOG_U32SPARE1_LM,
	RADIO__MSGCME2LOG_U32SPARE1_L,
	RADIO__MSGCME2LOG_U16SPARE1_H,
	RADIO__MSGCME2LOG_U16SPARE1_L,
	RADIO__MSGCME2LOG_U8SPARE1,
	RADIO__MSGCME2LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 3, ChargeEnded (CE)					 	*/
enum radio__acMsgCMEvtLogChargeEnded { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME3LOG_U32SPARE1_H = 0,
	RADIO__MSGCME3LOG_U32SPARE1_HM,
	RADIO__MSGCME3LOG_U32SPARE1_LM,
	RADIO__MSGCME3LOG_U32SPARE1_L,
	RADIO__MSGCME3LOG_U16SPARE1_H,
	RADIO__MSGCME3LOG_U16SPARE1_L,
	RADIO__MSGCME3LOG_U8SPARE1,
	RADIO__MSGCME3LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 4, ChalgError					 	*/
enum radio__acMsgCMEvtLogChalgError { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME4LOG_CHALGERROR_H = 0,
	RADIO__MSGCME4LOG_CHALGERROR_L,
	RADIO__MSGCME4LOG_ACTIVE_H,
	RADIO__MSGCME4LOG_ACTIVE_L,
	RADIO__MSGCME4LOG_U32SPARE1_H,
	RADIO__MSGCME4LOG_U32SPARE1_HM,
	RADIO__MSGCME4LOG_U32SPARE1_LM,
	RADIO__MSGCME4LOG_U32SPARE1_L,
	RADIO__MSGCME4LOG_U16SPARE1_H,
	RADIO__MSGCME4LOG_U16SPARE1_L,
	RADIO__MSGCME4LOG_U8SPARE1,
	RADIO__MSGCME4LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 5, ReguError					 	*/
enum radio__acMsgCMEvtLogReguError { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME5LOG_REGUERROR_H = 0,
	RADIO__MSGCME5LOG_REGUERROR_L,
	RADIO__MSGCME5LOG_ACTIVE_H,
	RADIO__MSGCME5LOG_ACTIVE_L,
	RADIO__MSGCME5LOG_U32SPARE1_H,
	RADIO__MSGCME5LOG_U32SPARE1_HM,
	RADIO__MSGCME5LOG_U32SPARE1_LM,
	RADIO__MSGCME5LOG_U32SPARE1_L,
	RADIO__MSGCME5LOG_U16SPARE1_H,
	RADIO__MSGCME5LOG_U16SPARE1_L,
	RADIO__MSGCME5LOG_U8SPARE1,
	RADIO__MSGCME5LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 6, CurveData					 	*/
enum radio__acMsgCMEvtLogCurveData { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME6LOG_ALGNO_NEW_H = 0,
	RADIO__MSGCME6LOG_ALGNO_NEW_L,
	RADIO__MSGCME6LOG_ALGNO_OLD_H,
	RADIO__MSGCME6LOG_ALGNO_OLD_L,
	RADIO__MSGCME6LOG_CAPACITY_NEW_H,
	RADIO__MSGCME6LOG_CAPACITY_NEW_L,
	RADIO__MSGCME6LOG_CAPACITY_OLD_H,
	RADIO__MSGCME6LOG_CAPACITY_OLD_L,
	RADIO__MSGCME6LOG_CELLS_NEW_H,
	RADIO__MSGCME6LOG_CELLS_NEW_L,
	RADIO__MSGCME6LOG_CELLS_OLD_H,
	RADIO__MSGCME6LOG_CELLS_OLD_L,
	RADIO__MSGCME6LOG_CABLERES_NEW_H,
	RADIO__MSGCME6LOG_CABLERES_NEW_L,
	RADIO__MSGCME6LOG_CABLERES_OLD_H,
	RADIO__MSGCME6LOG_CABLERES_OLD_L,
	RADIO__MSGCME6LOG_BASELOAD_NEW_H,
	RADIO__MSGCME6LOG_BASELOAD_NEW_L,
	RADIO__MSGCME6LOG_BASELOAD_OLD_H,
	RADIO__MSGCME6LOG_BASELOAD_OLD_L,
	RADIO__MSGCME6LOG_U32SPARE1_H,
	RADIO__MSGCME6LOG_U32SPARE1_HM,
	RADIO__MSGCME6LOG_U32SPARE1_LM,
	RADIO__MSGCME6LOG_U32SPARE1_L,
	RADIO__MSGCME6LOG_U16SPARE1_H,
	RADIO__MSGCME6LOG_U16SPARE1_L,
	RADIO__MSGCME6LOG_U8SPARE1,
	RADIO__MSGCME6LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 7, Vcc 					 	*/
enum radio__acMsgCMEvtLogVcc { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME7LOG_VCC = 0,
	RADIO__MSGCME7LOG_ACTIVE,
	RADIO__MSGCME7LOG_U32SPARE1_H,
	RADIO__MSGCME7LOG_U32SPARE1_HM,
	RADIO__MSGCME7LOG_U32SPARE1_LM,
	RADIO__MSGCME7LOG_U32SPARE1_L,
	RADIO__MSGCME7LOG_U16SPARE1_H,
	RADIO__MSGCME7LOG_U16SPARE1_L,
	RADIO__MSGCME7LOG_U8SPARE1,
	RADIO__MSGCME7LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 8, Voltage calibration					 	*/
enum radio__acMsgCMEvtLogVoltageCalib { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME8LOG_USLOPE_NEW_H = 0,
	RADIO__MSGCME8LOG_USLOPE_NEW_HM,
	RADIO__MSGCME8LOG_USLOPE_NEW_LM,
	RADIO__MSGCME8LOG_USLOPE_NEW_L,
	RADIO__MSGCME8LOG_USLOPE_OLD_H,
	RADIO__MSGCME8LOG_USLOPE_OLD_HM,
	RADIO__MSGCME8LOG_USLOPE_OLD_LM,
	RADIO__MSGCME8LOG_USLOPE_OLD_L,
	RADIO__MSGCME8LOG_U32SPARE1_H,
	RADIO__MSGCME8LOG_U32SPARE1_HM,
	RADIO__MSGCME8LOG_U32SPARE1_LM,
	RADIO__MSGCME8LOG_U32SPARE1_L,
	RADIO__MSGCME8LOG_U16SPARE1_H,
	RADIO__MSGCME8LOG_U16SPARE1_L,
	RADIO__MSGCME8LOG_U8SPARE1,
	RADIO__MSGCME8LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 9, Current calibration					 	*/
enum radio__acMsgCMEvtLogCurrentCalib { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME9LOG_ISLOPE_NEW_H = 0,
	RADIO__MSGCME9LOG_ISLOPE_NEW_HM,
	RADIO__MSGCME9LOG_ISLOPE_NEW_LM,
	RADIO__MSGCME9LOG_ISLOPE_NEW_L,
	RADIO__MSGCME9LOG_ISLOPE_OLD_H,
	RADIO__MSGCME9LOG_ISLOPE_OLD_HM,
	RADIO__MSGCME9LOG_ISLOPE_OLD_LM,
	RADIO__MSGCME9LOG_ISLOPE_OLD_L,
	RADIO__MSGCME9LOG_U32SPARE1_H,
	RADIO__MSGCME9LOG_U32SPARE1_HM,
	RADIO__MSGCME9LOG_U32SPARE1_LM,
	RADIO__MSGCME9LOG_U32SPARE1_L,
	RADIO__MSGCME9LOG_U16SPARE1_H,
	RADIO__MSGCME9LOG_U16SPARE1_L,
	RADIO__MSGCME9LOG_U8SPARE1,
	RADIO__MSGCME9LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 10, Start network 					 	*/
enum radio__acMsgCMEvtLogStartNwk { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME10LOG_CHANNEL = 0,
	RADIO__MSGCME10LOG_PANID_H,
	RADIO__MSGCME10LOG_PANID_L,
	RADIO__MSGCME10LOG_NODEID_H,
	RADIO__MSGCME10LOG_NODEID_L,
	RADIO__MSGCME10LOG_U32SPARE1_H,
	RADIO__MSGCME10LOG_U32SPARE1_HM,
	RADIO__MSGCME10LOG_U32SPARE1_LM,
	RADIO__MSGCME10LOG_U32SPARE1_L,
	RADIO__MSGCME10LOG_U16SPARE1_H,
	RADIO__MSGCME10LOG_U16SPARE1_L,
	RADIO__MSGCME10LOG_U8SPARE1,
	RADIO__MSGCME10LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 11, Join network 					 	*/
enum radio__acMsgCMEvtLogJoinNwk { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME11LOG_CHANNEL = 0,
	RADIO__MSGCME11LOG_PANID_H,
	RADIO__MSGCME11LOG_PANID_L,
	RADIO__MSGCME11LOG_NODEID_H,
	RADIO__MSGCME11LOG_NODEID_L,
	RADIO__MSGCME11LOG_U32SPARE1_H,
	RADIO__MSGCME11LOG_U32SPARE1_HM,
	RADIO__MSGCME11LOG_U32SPARE1_LM,
	RADIO__MSGCME11LOG_U32SPARE1_L,
	RADIO__MSGCME11LOG_U16SPARE1_H,
	RADIO__MSGCME11LOG_U16SPARE1_L,
	RADIO__MSGCME11LOG_U8SPARE1,
	RADIO__MSGCME11LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 12, Startup					 	*/
enum radio__acMsgCMEvtLogStartup { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME12LOG_MAINFWTYPE_H = 0,
	RADIO__MSGCME12LOG_MAINFWTYPE_HM,
	RADIO__MSGCME12LOG_MAINFWTYPE_LM,
	RADIO__MSGCME12LOG_MAINFWTYPE_L,
	RADIO__MSGCME12LOG_MAINFWVER_H,
	RADIO__MSGCME12LOG_MAINFWVER_HM,
	RADIO__MSGCME12LOG_MAINFWVER_LM,
	RADIO__MSGCME12LOG_MAINFWVER_L,
	RADIO__MSGCME12LOG_RADIOFWTYPE_H,
	RADIO__MSGCME12LOG_RADIOFWTYPE_HM,
	RADIO__MSGCME12LOG_RADIOFWTYPE_LM,
	RADIO__MSGCME12LOG_RADIOFWTYPE_L,
	RADIO__MSGCME12LOG_RADIOFWVER_H,
	RADIO__MSGCME12LOG_RADIOFWVER_HM,
	RADIO__MSGCME12LOG_RADIOFWVER_LM,
	RADIO__MSGCME12LOG_RADIOFWVER_L,
	RADIO__MSGCME12LOG_U32SPARE1_H,
	RADIO__MSGCME12LOG_U32SPARE1_HM,
	RADIO__MSGCME12LOG_U32SPARE1_LM,
	RADIO__MSGCME12LOG_U32SPARE1_L,
	RADIO__MSGCME12LOG_U16SPARE1_H,
	RADIO__MSGCME12LOG_U16SPARE1_L,
	RADIO__MSGCME12LOG_U8SPARE1,
	RADIO__MSGCME12LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 13, Settings					 	*/
enum radio__acMsgCMEvtLogSettings { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME13LOG_U32SPARE1_H = 0,
	RADIO__MSGCME13LOG_U32SPARE1_HM,
	RADIO__MSGCME13LOG_U32SPARE1_LM,
	RADIO__MSGCME13LOG_U32SPARE1_L,
	RADIO__MSGCME13LOG_U16SPARE1_H,
	RADIO__MSGCME13LOG_U16SPARE1_L,
	RADIO__MSGCME13LOG_U8SPARE1,
	RADIO__MSGCME13LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 14, Assert					 	*/
enum radio__acMsgCMEvtLogAssert { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME14LOG_ASSERTID = 0,
	RADIO__MSGCME14LOG_U32SPARE1_H,
	RADIO__MSGCME14LOG_U32SPARE1_HM,
	RADIO__MSGCME14LOG_U32SPARE1_LM,
	RADIO__MSGCME14LOG_U32SPARE1_L,
	RADIO__MSGCME14LOG_U16SPARE1_H,
	RADIO__MSGCME14LOG_U16SPARE1_L,
	RADIO__MSGCME14LOG_U8SPARE1,
	RADIO__MSGCME14LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 15, SupError					 	*/
enum radio__acMsgCMEvtLogSupError { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCME15LOG_SUPERROR_H = 0,
	RADIO__MSGCME15LOG_SUPERROR_L,
	RADIO__MSGCME15LOG_ACTIVE_H,
	RADIO__MSGCME15LOG_ACTIVE_L,
	RADIO__MSGCME15LOG_U32SPARE1_H,
	RADIO__MSGCME15LOG_U32SPARE1_HM,
	RADIO__MSGCME15LOG_U32SPARE1_LM,
	RADIO__MSGCME15LOG_U32SPARE1_L,
	RADIO__MSGCME15LOG_U16SPARE1_H,
	RADIO__MSGCME15LOG_U16SPARE1_L,
	RADIO__MSGCME15LOG_U8SPARE1,
	RADIO__MSGCME15LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Event Log data 16, Cable resistance event		 	*/
enum radio__acMsgCMEvtLogCableRiSlope { /*'''''''''''''''''''''''''''''''*/
	RADIO__MSGCME16LOG_RISLOPE_NEW_H = 0,
	RADIO__MSGCME16LOG_RISLOPE_NEW_HM,
	RADIO__MSGCME16LOG_RISLOPE_NEW_LM,
	RADIO__MSGCME16LOG_RISLOPE_NEW_L,
	RADIO__MSGCME16LOG_RISLOPE_OLD_H,
	RADIO__MSGCME16LOG_RISLOPE_OLD_HM,
	RADIO__MSGCME16LOG_RISLOPE_OLD_LM,
	RADIO__MSGCME16LOG_RISLOPE_OLD_L,
	RADIO__MSGCME16LOG_U32SPARE1_H,
	RADIO__MSGCME16LOG_U32SPARE1_HM,
	RADIO__MSGCME16LOG_U32SPARE1_LM,
	RADIO__MSGCME16LOG_U32SPARE1_L,
	RADIO__MSGCME16LOG_U16SPARE1_H,
	RADIO__MSGCME16LOG_U16SPARE1_L,
	RADIO__MSGCME16LOG_U8SPARE1,
	RADIO__MSGCME16LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Instant Log, time stamp record					 	*/
enum radio__acMsgCMInstLogHeader { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMILOG_INDEX_H = 0,
	RADIO__MSGCMILOG_INDEX_HM,
	RADIO__MSGCMILOG_INDEX_LM,
	RADIO__MSGCMILOG_INDEX_L,
	RADIO__MSGCMILOG_RECORDCOUNT,
	RADIO__MSGCMILOG_TIME_H,
	RADIO__MSGCMILOG_TIME_HM,
	RADIO__MSGCMILOG_TIME_LM,
	RADIO__MSGCMILOG_TIME_L,
	RADIO__MSGCMILOG_SAMPLEPERIOD_H,
	RADIO__MSGCMILOG_SAMPLEPERIOD_L,
	RADIO__MSGCMILOG_RECORDTYPE_H,
	RADIO__MSGCMILOG_RECORDTYPE_L,
	RADIO__MSGCMILOG_RECORDSIZE,
	RADIO__MSGCMILOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**< Instant Log, standard record					 	*/
enum radio__acMsgCMInstLogStandard { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMI1LOG_RECORDNO = 0,
	RADIO__MSGCMI1LOG_CHALGERROR_H,
	RADIO__MSGCMI1LOG_CHALGERROR_L,
	RADIO__MSGCMI1LOG_REGUERROR_H,
	RADIO__MSGCMI1LOG_REGUERROR_L,
	RADIO__MSGCMI1LOG_UCHALG_H,
	RADIO__MSGCMI1LOG_UCHALG_HM,
	RADIO__MSGCMI1LOG_UCHALG_LM,
	RADIO__MSGCMI1LOG_UCHALG_L,
	RADIO__MSGCMI1LOG_ICHALG_H,
	RADIO__MSGCMI1LOG_ICHALG_HM,
	RADIO__MSGCMI1LOG_ICHALG_LM,
	RADIO__MSGCMI1LOG_ICHALG_L,
	RADIO__MSGCMI1LOG_TBOARD_H,
	RADIO__MSGCMI1LOG_TBOARD_L,
	RADIO__MSGCMI1LOG_THS_H,
	RADIO__MSGCMI1LOG_THS_L,
	RADIO__MSGCMI1LOG_U32SPARE1_H, /**< u32 spare1			*/
	RADIO__MSGCMI1LOG_U32SPARE1_HM, /**< u32 spare1			*/
	RADIO__MSGCMI1LOG_U32SPARE1_LM, /**< u32 spare1			*/
	RADIO__MSGCMI1LOG_U32SPARE1_L, /**< u32 spare1			*/
	RADIO__MSGCMI1LOG_SIZE

}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access OTA protocol get log message bytes.
 */

enum radio__acMsgGetLog { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGGLOG_LOGTYPE = 0, /**< Log type						*/
	RADIO__MSGGLOG_RECORDNO_H, /**< Index of start record			*/
	RADIO__MSGGLOG_RECORDNO_HM, /**< Index of start record			*/
	RADIO__MSGGLOG_RECORDNO_LM, /**< Index of start record			*/
	RADIO__MSGGLOG_RECORDNO_L, /**< Index of start record			*/
	RADIO__MSGGLOG_NOOFRECORDS, /**< Number of records				*/

	RADIO__MSGGLOG_SIZE
/**< Size of get log message		*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*@{*/
#define RADIO__LOGTYPE_HISTORICAL	0	/**< Historical log type	*/
#define RADIO__LOGTYPE_EVENT		1	/**< Event log type			*/
#define RADIO__LOGTYPE_INSTANT		2	/**< Instant log type		*/
/*@}*/

/**
 * Access OTA protocol set mpl message bytes.
 */

enum radio__acMsgSetMpl { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGSMPL_PWRGRP = 0, /**< Group of chargers on same fuse	*/
	RADIO__MSGSMPL_PWRLIMIT_H, /**< Max AC power for each charger	*/
	RADIO__MSGSMPL_PWRLIMIT_L,
	RADIO__MSGSMPL_U32SPARE1_H, /**< u32 spare			*/
	RADIO__MSGSMPL_U32SPARE1_HM, /**< u32 spare			*/
	RADIO__MSGSMPL_U32SPARE1_LM, /**< u32 spare			*/
	RADIO__MSGSMPL_U32SPARE1_L, /**< u32 spare			*/
	RADIO__MSGSMPL_U16SPARE1_H, /**< u16 spare			*/
	RADIO__MSGSMPL_U16SPARE1_L, /**< u16 spare			*/
	RADIO__MSGSMPL_U8SPARE1, 	/**< u8 spare			*/
	RADIO__MSGSMPL_SIZE
/**< Max AC power for each charger	*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*
 SerialNo		Uint32	SerialNo		CM		Charger production serial number.
 HistLogIndex	Uint32	HistlogIndex	SUP		No of historical data records (index)
 HistLogDate		Uint32	chargeStartTime	SUP		The start date of the last charging stored in Historical log (from Historical log)
 EvtLogIndex		Uint32	EvtLogIndex		SUP		The latest Event log index
 EvtLogDate		Uint32	Time			SUP		The timestamp for the last Event log (from Event log)
 MomLogIndex		Uint32	MomLogIndex		SUP		The latest Momentary log index
 ChalgErrorSum	Uint16	ChalgErrorSum	CHALG	See description in [2]
 ReguErrorSum	Uint16	ReguErrorSum	REGU	See description in [2]
 */

/*
 * Access OTA protocol CmInfo message bytes.
 */

enum radio__acMsgCmInfo { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGCMINF_CID_H = 0,
	RADIO__MSGCMINF_CID_HM,
	RADIO__MSGCMINF_CID_LM,
	RADIO__MSGCMINF_CID_L,
	RADIO__MSGCMINF_HISTLOGINMEM_H,
	RADIO__MSGCMINF_HISTLOGINMEM_L,
	RADIO__MSGCMINF_HISTLOGIDX_H,
	RADIO__MSGCMINF_HISTLOGIDX_HM,
	RADIO__MSGCMINF_HISTLOGIDX_LM,
	RADIO__MSGCMINF_HISTLOGIDX_L,
	RADIO__MSGCMINF_CTTIME_H,
	RADIO__MSGCMINF_CTTIME_HM,
	RADIO__MSGCMINF_CTTIME_LM,
	RADIO__MSGCMINF_CTTIME_L,
	RADIO__MSGCMINF_EVTLOGINMEM_H,
	RADIO__MSGCMINF_EVTLOGINMEM_L,
	RADIO__MSGCMINF_EVTLOGIDX_H,
	RADIO__MSGCMINF_EVTLOGIDX_HM,
	RADIO__MSGCMINF_EVTLOGIDX_LM,
	RADIO__MSGCMINF_EVTLOGIDX_L,
	RADIO__MSGCMINF_EVTLOGTIME_H,
	RADIO__MSGCMINF_EVTLOGTIME_HM,
	RADIO__MSGCMINF_EVTLOGTIME_LM,
	RADIO__MSGCMINF_EVTLOGTIME_L,
	RADIO__MSGCMINF_MOMLOGINMEM_H,
	RADIO__MSGCMINF_MOMLOGINMEM_L,
	RADIO__MSGCMINF_MOMLOGIDX_H,
	RADIO__MSGCMINF_MOMLOGIDX_HM,
	RADIO__MSGCMINF_MOMLOGIDX_LM,
	RADIO__MSGCMINF_MOMLOGIDX_L,
	RADIO__MSGCMINF_CHALGERRSUM_H,
	RADIO__MSGCMINF_CHALGERRSUM_L,
	RADIO__MSGCMINF_REGUERRSUM_H,
	RADIO__MSGCMINF_REGUERRSUM_L,
	RADIO__MSGCMINF_CMID_HHH, /**< First byte of Cm ID (64bit)		*/
	RADIO__MSGCMINF_CMID_HH,
	RADIO__MSGCMINF_CMID_H,
	RADIO__MSGCMINF_CMID_HM,
	RADIO__MSGCMINF_CMID_LM,
	RADIO__MSGCMINF_CMID_L,
	RADIO__MSGCMINF_CMID_LL,
	RADIO__MSGCMINF_CMID_LLL,
	RADIO__MSGCMINF_FWVERSION_H, /**< FwVersion	*/
	RADIO__MSGCMINF_FWVERSION_HM, /**< 			*/
	RADIO__MSGCMINF_FWVERSION_LM, /**< 			*/
	RADIO__MSGCMINF_FWVERSION_L, /**< 			*/
	RADIO__MSGCMINF_U16SPARE1_H, /**< u16 spare			*/
	RADIO__MSGCMINF_U16SPARE1_L, /**< u16 spare			*/
	RADIO__MSGCMINF_U8SPARE1, /**< u8 spare			*/
	RADIO__MSGCMINF_SIZE
/**< Size of CmInfo message			*/
}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*
 * Access OTA protocol CmUserParams message bytes.
 */
//enum radio__acMsgCmUserParams { /*''''''''''''''''''''''''''''''''''*/ // sorabtest magnus J
	/*RADIO__MSGCMUSP_INDEX = 0,
	RADIO__MSGCMUSP_TYPE,
	RADIO__MSGCMUSP_VALUE_H,
	RADIO__MSGCMUSP_VALUE_HM,
	RADIO__MSGCMUSP_VALUE_LM,
	RADIO__MSGCMUSP_VALUE_L,
	RADIO__MSGCMUSP_MIN_H,
	RADIO__MSGCMUSP_MIN_HM,
	RADIO__MSGCMUSP_MIN_LM,
	RADIO__MSGCMUSP_MIN_L,
	RADIO__MSGCMUSP_MAX_H,
	RADIO__MSGCMUSP_MAX_HM,
	RADIO__MSGCMUSP_MAX_LM,
	RADIO__MSGCMUSP_MAX_L,
	RADIO__MSGCMUSP_DEFAULT_H,
	RADIO__MSGCMUSP_DEFAULT_HM,
	RADIO__MSGCMUSP_DEFAULT_LM,
	RADIO__MSGCMUSP_DEFAULT_L,
	RADIO__MSGCMUSP_NAMESIZE,
	RADIO__MSGCMUSP_NAME_I,
	RADIO__MSGCMUSP_SIZE*/

	/**< Size of CmUserParams message			*/
//}; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*
 * Access OTA protocol CmInfo message bytes.
 */

enum radio__acMsgSetIdentify { /*''''''''''''''''''''''''''''''''''*/
	RADIO__MSGSETIDENTIFY_MODE = 0,
	RADIO__MSGSETIDENTIFY_SIZE
};

/**
 * Broadcast address.
 */

#define	RADIO__MACADDR_BROADCAST 0xFFFF

/**
 * Maximum amount of bytes that can be stored in a message payload. This does
 * not include the Access protocol header bytes.
 */

#define RADIO__AC_MAXPAYLOADSIZE	88

/**
 * Maximum number of instant records that can be requested
 */
#define MAX_INST_RECORDS 100

#define RADIO_INSTLOG_PER_SEGMENT 4
#define RADIO_INSTLOG_HEADER_SEGMENT (RADIO_INSTLOG_PER_SEGMENT - 1)

/**
 * List of steps (substates) that are used in the send log state.
 */

enum radio__acStateLogSteps {
	RADIO__STEPLOG_BUILD, /**< Building segment				*/
	RADIO__STEPLOG_SEND
/**< Sending segment				*/
};

/*******************************************************************************
 ;
 ;	INTERNAL MACROS
 ;
 ;-----------------------------------------------------------------------------*/

/**
 * Copy a 32-bit array register element to the buffer.
 */

#define radio__acABuildU32(pBuffer_, param_, handle_, arrayIdx_) {			\
	Uint32 tempVar;															\
	reg_aGet(&tempVar, (handle_), (arrayIdx_));								\
	(pBuffer_)[param_##_H] = (BYTE) (tempVar >> 24);						\
	(pBuffer_)[param_##_HM] = (BYTE) (tempVar >> 16);						\
	(pBuffer_)[param_##_LM] = (BYTE) (tempVar >> 8);						\
	(pBuffer_)[param_##_L] = (BYTE) tempVar;								\
}

/**
 * Copy a 32-bit register to the buffer.
 */

#define radio__acBuildU32(pBuffer_, param_, handle_) {						\
	Uint32 tempVar;															\
	reg_get(&tempVar, (handle_));											\
	(pBuffer_)[param_##_H] = (BYTE) (tempVar >> 24);						\
	(pBuffer_)[param_##_HM] = (BYTE) (tempVar >> 16);						\
	(pBuffer_)[param_##_LM] = (BYTE) (tempVar >> 8);						\
	(pBuffer_)[param_##_L] = (BYTE) tempVar;								\
}

/**
 * Copy a 16-bit register to the buffer.
 */

#define radio__acBuildU16(pBuffer_, param_, handle_) {						\
	Uint16 tempVar;															\
	reg_get(&tempVar, (handle_));											\
	(pBuffer_)[param_##_H] = (BYTE) (tempVar >> 8);							\
	(pBuffer_)[param_##_L] = (BYTE) tempVar;								\
}

/*******************************************************************************
 ;
 ;	INTERNAL STRUCTURES AND DATA TYPES
 ;
 ;-----------------------------------------------------------------------------*/

struct radio__acInst;
typedef struct radio__acInst radio__AcInst;

/**
 * Type of processing functions that builds the next segment.
 */

typedef Boolean radio__AcProcFn(radio__AcInst *, Boolean);

/**
 *	State information for message handling.
 */
typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	radio__AcProcFn * pProcFn;			/**< Pointer to current proc func.	*/
	Uint16 dstAddr;						/**< Destination address for multi-
										 * package messages					*/
	Uint8 seqNum;						/**< Response sequence number		*/
} radio__AcMsgInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Access radio protocol specific variables.
 */

struct radio__acInst {					/*''''''''''''''''''''''''''''' RAM	*/
	radio__PnInst parent;				/**< Parent instance				*/
	cm_Request logRequest;				/**< Log read request.				*/
	radio__AcMsgInfo normMessage;		/**< Normal message info.			*/
	radio__AcMsgInfo segmentedMessage;	/**< Segmented message info.		*/
	radio__AcMsgInfo delayedMsg;		/**< Info about delayed resend msg.	*/
	radio__AcMsgInfo spontaneousMsg;	/**< Spontaneous message sending info.*/
	radio__AcMsgInfo * pLastMsg;		/**< Info about last sent message.	*/
	Uint32 logBuff[(sizeof(cm_HistRecStd) + 4) / 4];/**<		*/
	Uint16 byteCount;					/**< Number of message bytes sent
										 *  or received.					*/
	Uint8 recCount;						/**< Number of records to send		*/
	uint8_t recordOffset;				/**< Current record offset			*/
	uint32_t recordNumber;				/**< Start record number			*/
	Uint8 seqNum;						/**< Message sequence counter.		*/
	Uint8 testCmd;						/**< Handle as test command			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Data about one message. The data is used while processing the message.
 */

typedef struct { /*''''''''''''''''''''''''''''' RAM */
	BYTE * pData; /**< Pointer to message data		*/
	Uint8 size; /**< Size of the message			*/
	Uint8 control; /**< The control byte of the message*/
	Uint8 seqNum; /**< Sequence number			*/
	radio__PnPopNwkAddr address; /**< Source address. */
} radio__AcProc; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE radio__ErrCode radio__acProcAck(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcJoinInd(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcSetTime(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcBBC(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcBMCaPack(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcBmBatteryData(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcBmConfig(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcBmMeas(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcGetStatus(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcGetLog(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcSetMpl(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcSetConfig(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcGetInfo(radio__AcInst *, radio__AcProc *);
//PRIVATE radio__ErrCode radio__acProcGetUserParams(radio__AcInst *, radio__AcProc *); // sorabtest magnus J
//PRIVATE radio__ErrCode radio__acProcSetUserParams(radio__AcInst *, radio__AcProc *); // sorabtest magnus J
PRIVATE radio__ErrCode radio__acProcSetIdentify(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcCmDplMasterAll(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcCmDplMasterSlave(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcGetCmDplSlave(radio__AcInst *, radio__AcProc *);
PRIVATE radio__ErrCode radio__acProcCmDplSlave(radio__AcInst *, radio__AcProc *);

PRIVATE void radio__acBuildHeader(radio__AcInst *, Uint8, Uint8, Uint8);
PRIVATE void radio__acBuildAck(radio__AcInst *, Uint8, Uint8);
PRIVATE radio__ErrCode radio__acBuildConfig(radio__AcInst *, const uint8_t);
PRIVATE radio__ErrCode radio__acBuildHistLog(radio__AcInst *, const uint8_t);
PRIVATE radio__ErrCode radio__acBuildEventLog(radio__AcInst *, const uint8_t);
PRIVATE radio__ErrCode radio_acProcInstantLog(radio__AcInst *, const uint8_t);
PRIVATE radio__ErrCode radio__acBuildInstLog(radio__AcInst *);
PRIVATE radio__ErrCode radio__acBuildCmMeas(radio__AcInst *, Uint8, Uint8);
PRIVATE radio__ErrCode radio__acBuildGetBmMeas(radio__AcInst *, Uint8, Uint8);
PRIVATE radio__ErrCode radio__acBuildSetTime(radio__AcInst *, Uint8, Uint8);
/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

radio__MsgBmConfig sMsgConfig;
/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the Access OTA protocol handler.
*
*	\param		pInit	Pointer to FB init structure.
*
*	\return		Pointer to new allocated FB instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC radio__Inst * radio__acInit(radio_Init * pInit) {
	radio__AcInst * pInst;

	pInst = (radio__AcInst *) mem_reserve(&mem_normal, sizeof(radio__AcInst));

	pInst->byteCount = 0;
	pInst->seqNum = 0;

	pInst->normMessage.pProcFn = NULL;
	pInst->normMessage.dstAddr = 0;
	pInst->normMessage.seqNum = 0;

	pInst->segmentedMessage.pProcFn = NULL;
	pInst->segmentedMessage.dstAddr = 0;
	pInst->segmentedMessage.seqNum = 0;
	pInst->recordOffset = 0;
	pInst->recordNumber = 0;

	pInst->spontaneousMsg.pProcFn = NULL;

	pInst->pLastMsg = NULL;

	return ((radio__Inst *) pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcessMessage
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes the incoming packet in the buffer.
*
*	\param		pInstance	Pointer to FB instance.
*	\param		pMsg		Pointer to message.
*	\param		size		Size of message.
*	\param		address		The source network ID.
*
*	\retval		RADIO__ERR_ERROR	If something went wrong when processing the
*									message.
*	\retval		RADIO__ERR_PROCOK	If the message was processed successfully.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC radio__ErrCode radio__acProcessMessage(radio__Inst * pInstance,
		BYTE * pMsg, Uint8 size, radio__PnPopNwkAddr address) {
	radio__AcInst * pInst;
	radio__ErrCode retVal = RADIO__ERR_ERROR;
	radio__AcProc msgInfo;
	Uint8 command;

	pInst = (radio__AcInst *) pInstance;
	retVal = RADIO__ERR_PROCOK;

	msgInfo.address = address;
	msgInfo.seqNum = pMsg[RADIO__MSGH_MPSEQ];
	msgInfo.control = pMsg[RADIO__MSGH_CONTROL];
	msgInfo.pData = &pMsg[RADIO__MSGH_DATA];
	msgInfo.size = size - RADIO__MSGH_DATA;

	command = pMsg[RADIO__MSGH_CMD];

	/*
	 * Handle test command
	 */
	if(msgInfo.address == 0xFFFF)
		pInst->testCmd = 1;
	else
		pInst->testCmd = 0;

	/*
	 * Handle the actual message.
	 */

	switch (command) {
		case RADIO__ACCMD_GET_CONFIG:
		case RADIO__ACCMD_GET_LOG:
			/* These require segmented responses */
			pInst->segmentedMessage.seqNum = msgInfo.seqNum;
			pInst->segmentedMessage.dstAddr = msgInfo.address;
			break;

		default:
			pInst->normMessage.dstAddr = address;
			pInst->normMessage.seqNum = msgInfo.seqNum;
			break;
	}

	switch (command) {
	/* Common requests */
	case RADIO__ACCMD_GETSEG:
		// Read control byte here to allow test to inject segment reads
		retVal = RADIO__ERR_ERROR;

		if (msgInfo.size == RADIO__MSGGETSEG_SIZE) {
			uint8_t control = msgInfo.pData[RADIO__MSGGETSEG_CONTROL];

			retVal = radio__acProcGetSegment(pInstance, control);
		}
		break;

	case RADIO__ACCMD_SET_TIME:
		retVal = radio__acProcSetTime(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_BBC:
		retVal = radio__acProcBBC(pInst, &msgInfo);
		break;

		/* Common responses */
	case RADIO__ACCMD_ACK:
		retVal = radio__acProcAck(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_JOININD:
		retVal = radio__acProcJoinInd(pInst, &msgInfo);
		break;

		/* BM responses */
	case RADIO__ACCMD_BMMEAS:
		retVal = radio__acProcBmMeas(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_BMDATA:
		retVal = radio__acProcBmBatteryData(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_BMCONFIG:
		retVal = radio__acProcBmConfig(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_BMCAPACK:
		retVal = radio__acProcBMCaPack(pInst, &msgInfo);
		break;

		/* SM requests */
	case RADIO__ACCMD_GET_STATUS:
		retVal = radio__acProcGetStatus(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_GET_MEAS:
		retVal = radio__acBuildCmMeas(pInst, msgInfo.seqNum,
				RADIO__ACCMD_CMMEAS);
		break;

	case RADIO__ACCMD_GET_CONFIG:
		//	added to allow interruption of segmented messages
		pInst->segmentedMessage.pProcFn = &radio__acBuildConfig;
		retVal = radio__acBuildConfig(pInst, RADIO__MSGCMCFG_SEGMENTS);
		break;

	case RADIO__ACCMD_GET_LOG:
		retVal = radio__acProcGetLog(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_SET_MPL:
		retVal = radio__acProcSetMpl(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_SET_CMCONFIG:
		retVal = radio__acProcSetConfig(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_GET_INFO:
		retVal = radio__acProcGetInfo(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_SET_IDENTIFY:
		retVal = radio__acProcSetIdentify(pInst, &msgInfo);
		break;

	/* CM DPL request/response */
	case RADIO__ACCMD_CMDPLMASTERALL:
		retVal = radio__acProcCmDplMasterAll(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_CMDPLMASTERSLAVE:
		retVal = radio__acProcCmDplMasterSlave(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_GET_CMDPLSLAVE:
		retVal = radio__acProcGetCmDplSlave(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_CMDPLSLAVE:
		retVal = radio__acProcCmDplSlave(pInst, &msgInfo);
		break;

	/*case RADIO__ACCMD_GET_USERPARAMS: // sorabtest magnus J
		retVal = radio__acProcGetUserParams(pInst, &msgInfo);
		break;

	case RADIO__ACCMD_SET_USERPARAMS: // sorabtest magnus J
		retVal = radio__acProcSetUserParams(pInst, &msgInfo);
		break;*/
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acContinueLogProc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Should be called when log message processing can continue.
*
*	\param		pInstance	Pointer to FB instance.
*	\param		pDstAddr	Pointer to address to send response to if a response
*							should be sent.
*
*	\retval		RADIO__ERR_PROCOK		If no response should be sent.
*	\retval		RADIO__ERR_PROCOK_RESP	If a response should be sent.
*
*	\details	This function is called when records have been read from the
*				log and the message can be sent.
*
*	\note
*
*******************************************************************************/

PUBLIC radio__ErrCode radio__acContinueLogProc(
    radio__Inst *			pInstance,
    Uint16 *				pDstAddr
) {
    radio__AcInst *			pInst;
    radio__ErrCode			retVal;
	radio__AcMsgInfo *		pStateInfo;

    pInst = (radio__AcInst *) pInstance;
	retVal = RADIO__ERR_PROCOK;
	
	pInst->logRequest.flags &= ~(CM_REQFLG_READ | CM_REQFLG_READNEXT);
	pStateInfo = &pInst->segmentedMessage;

	if (pStateInfo->pProcFn != NULL)
	{
		retVal = pStateInfo->pProcFn(pInst, FALSE);
		*pDstAddr = pStateInfo->dstAddr;
	}

    return(retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acStoreForRebuild
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Store retransmission information about the last sent frame.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		-
*
*	\details	The information is stored so that other commands can be executed
*				while waiting for retransmission.
*
*	\note
*
*******************************************************************************/

PUBLIC Boolean radio__acStoreForRebuild(
    radio__Inst *			pInstance
) {
    radio__AcInst *			pInst;

    pInst = (radio__AcInst *) pInstance;

    if (pInst->pLastMsg == NULL)
    {
    	return FALSE;
    }

	pInst->delayedMsg.dstAddr = pInst->pLastMsg->dstAddr;
	pInst->delayedMsg.pProcFn = pInst->pLastMsg->pProcFn;
	pInst->delayedMsg.seqNum = pInst->pLastMsg->seqNum;

	return TRUE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendGetBMCaPack
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the charging algorithm from the battery module.
*
*	\param		pInst	Pointer to instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendGetBMCaPack(radio__Inst * pInst) {
	Uint16 BMNwkId;
	radio__AcInst * pAcInst;

	pAcInst = (radio__AcInst *) pInst;

	/*
	 * Only ask for the battery CaPack if the CaPack in RAM differs from the
	 * one in the battery.
	 */

	/*
	 * The battery module CaPack is different from the one in charger RAM.
	 */

	radio__acBuildHeader(pAcInst, RADIO__ACCMD_GET_BMCAPACK,
			pAcInst->seqNum, RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->instId);
	radio__pnSendMpNwkDataReq(pInst, BMNwkId);

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendGetBMBData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the charging algorithm from the battery module.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendGetBMBData(radio__Inst * pInst, volatile CurrentCombination_t * pCurrentCombination) {
	radio__AcInst * pAcInst;
	BYTE * pPayload;

	pAcInst = (radio__AcInst *) pInst;

	// Build data
	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	pPayload[RADIO__MSGGETBMBDATA_CURRLEVEL1] = pCurrentCombination->CurrentLevel1;
	pPayload[RADIO__MSGGETBMBDATA_CURRLEVEL2] = pCurrentCombination->CurrentLevel2;
	pPayload[RADIO__MSGGETBMBDATA_CURRLEVEL3] = pCurrentCombination->CurrentLevel3;

	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGGETBMBDATA_SIZE;

	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_GET_BMDATA, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);

	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	radio__pnSendMpNwkDataReq(pInst, RADIO__MACADDR_BROADCAST);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendGetBmMeas
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send Get_BmMeas message to BM.
*
*	\param		pInst	Pointer to instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendGetBmMeas(radio__Inst * pInst) {
	Uint16 BMNwkId;
	radio__AcInst * pAcInst;

	pAcInst = (radio__AcInst *) pInst;

	radio__acBuildGetBmMeas(pAcInst, pAcInst->seqNum, RADIO__ACCMD_GET_BMMEAS);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->instId);
	radio__pnSendMpNwkDataReq(pInst, BMNwkId);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendGetBMConfig
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the charging algorithm from the battery module.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendGetBMConfig(radio__Inst * pInst) {
	radio__AcInst * pAcInst;
	BYTE * pPayload;
	Uint16 BMNwkId;

	pAcInst = (radio__AcInst *) pInst;

	// Build data
	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGGETCONFIG_SIZE;

	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_GET_CONFIG, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);

	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	// Get Bm node Id
	reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->instId);
	// Send message
	radio__pnSendMpNwkDataReq(pInst, BMNwkId);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendSetBmuTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send SetTime message to BM.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendSetBmuTime(radio__Inst * pInst) {
	Uint16 BMNwkId;
	radio__AcInst * pAcInst;

	pAcInst = (radio__AcInst *) pInst;

	radio__acBuildSetTime(pAcInst, pAcInst->seqNum, RADIO__ACCMD_SET_TIME);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->instId);
	radio__pnSendMpNwkDataReq(pInst, BMNwkId);

}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendSetBMConfig
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set parameters for battery module.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendSetBMConfig(radio__Inst * pInst) {
	BYTE * pPayload;
	radio__AcInst * pAcInst;
	Uint16 BMNwkId;
	Uint32 u32TempVar;
	Uint16 u16TempVar;
	Uint8 u8TempVar;

	pAcInst = (radio__AcInst *) pInst;

	/*
	 * Get pointer to the payload part of the PopNet gateway protocol network
	 * data request. Step forward to the data part of the message.
	 */

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	/*
	 * Build the message.
	 */
	u16TempVar = sMsgConfig.panId;
	pPayload[RADIO_MSGBMCONFIG_PANID_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_PANID_L] = (Uint8) (u16TempVar);

	u8TempVar = sMsgConfig.channel;
	pPayload[RADIO_MSGBMCONFIG_CHANNEL] = u8TempVar;

	u16TempVar = sMsgConfig.nodeAddr;
	pPayload[RADIO_MSGBMCONFIG_NODEADDR_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_NODEADDR_L] = (Uint8) (u16TempVar);

	u8TempVar = sMsgConfig.nwkSettings;
	pPayload[RADIO_MSGBMCONFIG_NWKSETTINGS] = u8TempVar;

	u32TempVar = sMsgConfig.serialNo;
	pPayload[RADIO_MSGBMCONFIG_SERIALNO_H] = (Uint8) (u32TempVar >> 24);
	pPayload[RADIO_MSGBMCONFIG_SERIALNO_HM] = (Uint8) (u32TempVar >> 16);
	pPayload[RADIO_MSGBMCONFIG_SERIALNO_LM] = (Uint8) (u32TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_SERIALNO_L] = (Uint8) (u32TempVar);

	u8TempVar = sMsgConfig.bmType;
	pPayload[RADIO_MSGBMCONFIG_BMTYPE] = u8TempVar;

	u32TempVar = sMsgConfig.bid;
	pPayload[RADIO_MSGBMCONFIG_BID_H] = (Uint8) (u32TempVar >> 24);
	pPayload[RADIO_MSGBMCONFIG_BID_HM] = (Uint8) (u32TempVar >> 16);
	pPayload[RADIO_MSGBMCONFIG_BID_LM] = (Uint8) (u32TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_BID_L] = (Uint8) (u32TempVar);

	u8TempVar = sMsgConfig.equFunc;
	pPayload[RADIO_MSGBMCONFIG_EQUFUNC] = u8TempVar;

	u8TempVar = sMsgConfig.equParam;
	pPayload[RADIO_MSGBMCONFIG_EQUPARAM] = u8TempVar;

	u8TempVar = sMsgConfig.waterFunc;
	pPayload[RADIO_MSGBMCONFIG_WATERFUNC] = u8TempVar;

	u8TempVar = sMsgConfig.waterParam;
	pPayload[RADIO_MSGBMCONFIG_WATERPARAM] = u8TempVar;

	// Get battery manufacturer Id text string
	for(int i = 0; i < 8; i++) {
		pPayload[RADIO_MSGBMCONFIG_BMFGID_HHH + i] = sMsgConfig.bmfgId[i];
	}

	//u16TempVar = sMsgConfig.cells;
	reg_get(&u16TempVar, radio__hCell_bm);
	pPayload[RADIO_MSGBMCONFIG_CELLS_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_CELLS_L] = (Uint8) (u16TempVar);

	//u16TempVar = sMsgConfig.capacity;
	reg_get(&u16TempVar, radio__hCapacity_bm);
	u16TempVar = u16TempVar * 10;														// Capacity (in 0,1Ah for BMU)
	pPayload[RADIO_MSGBMCONFIG_CAPACITY_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_CAPACITY_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.cyclesAvailable;
	pPayload[RADIO_MSGBMCONFIG_CYCLESAVAILABLE_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_CYCLESAVAILABLE_L] = (Uint8) (u16TempVar);

	u32TempVar = sMsgConfig.ahAvailable;
	pPayload[RADIO_MSGBMCONFIG_AHAVAILABLE_H] = (Uint8) (u32TempVar >> 24);
	pPayload[RADIO_MSGBMCONFIG_AHAVAILABLE_HM] = (Uint8) (u32TempVar >> 16);
	pPayload[RADIO_MSGBMCONFIG_AHAVAILABLE_LM] = (Uint8) (u32TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_AHAVAILABLE_L] = (Uint8) (u32TempVar);

	u8TempVar = sMsgConfig.batteryType;
	pPayload[RADIO_MSGBMCONFIG_BATTERYTYPE] = u8TempVar;

	//u16TempVar = sMsgConfig.baseload;
	reg_get(&u16TempVar, radio__hBaseLoad_bm);
	pPayload[RADIO_MSGBMCONFIG_BASELOAD_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_BASELOAD_L] = (Uint8) (u16TempVar);

	//u16TempVar = sMsgConfig.cableRes;
	reg_get(&u16TempVar, radio__hCableRes_bm);
	pPayload[RADIO_MSGBMCONFIG_CABLERES_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_CABLERES_L] = (Uint8) (u16TempVar);

	//u16TempVar = sMsgConfig.algNo;
	reg_get(&u16TempVar, radio__hAlgNo_bm);
	pPayload[RADIO_MSGBMCONFIG_ALGNO_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_ALGNO_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.u16Spare5;
	pPayload[RADIO_MSGBMCONFIG_U16SPARE5_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_U16SPARE5_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.bbcGroup;
	pPayload[RADIO_MSGBMCONFIG_BBCGROUP_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_BBCGROUP_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.instPeriod;
	pPayload[RADIO_MSGBMCONFIG_INSTPERIOD_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_INSTPERIOD_L] = (Uint8) (u16TempVar);

	u8TempVar = sMsgConfig.cellTap;
	pPayload[RADIO_MSGBMCONFIG_CELLTAP] = u8TempVar;

	u8TempVar = sMsgConfig.voltageBalanceLimit;
	pPayload[RADIO_MSGBMCONFIG_VBL] = u8TempVar;

	u8TempVar = sMsgConfig.acidSensor;
	pPayload[RADIO_MSGBMCONFIG_ACIDSENSOR] = u8TempVar;

	u16TempVar = sMsgConfig.tMax;
	pPayload[RADIO_MSGBMCONFIG_TMAX_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_TMAX_L] = (Uint8) (u16TempVar);

	u8TempVar = sMsgConfig.remoteOutFunc;
	pPayload[RADIO_MSGBMCONFIG_ROUTFUNC] = u8TempVar;

	u16TempVar = sMsgConfig.dEL;
	pPayload[RADIO_MSGBMCONFIG_DEL_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_DEL_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.cEL;
	pPayload[RADIO_MSGBMCONFIG_CEL_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_CEL_L] = (Uint8) (u16TempVar);

	u8TempVar = sMsgConfig.cEfficiency;
	pPayload[RADIO_MSGBMCONFIG_CEFFICIENCY] = u8TempVar;

	u8TempVar = sMsgConfig.clearStatistics;
	pPayload[RADIO_MSGBMCONFIG_CLEARSTATISTICS] = u8TempVar;

	u32TempVar = sMsgConfig.u32Spare1;
	pPayload[RADIO_MSGBMCONFIG_U32SPARE1_H] = (Uint8) (u32TempVar >> 24);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE1_HM] = (Uint8) (u32TempVar >> 16);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE1_LM] = (Uint8) (u32TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE1_L] = (Uint8) (u32TempVar);

	u32TempVar = sMsgConfig.u32Spare2;
	pPayload[RADIO_MSGBMCONFIG_U32SPARE2_H] = (Uint8) (u32TempVar >> 24);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE2_HM] = (Uint8) (u32TempVar >> 16);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE2_LM] = (Uint8) (u32TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE2_L] = (Uint8) (u32TempVar);

	u32TempVar = sMsgConfig.u32Spare3;
	pPayload[RADIO_MSGBMCONFIG_U32SPARE3_H] = (Uint8) (u32TempVar >> 24);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE3_HM] = (Uint8) (u32TempVar >> 16);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE3_LM] = (Uint8) (u32TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_U32SPARE3_L] = (Uint8) (u32TempVar);

	u16TempVar = sMsgConfig.securityCode1;
	pPayload[RADIO_MSGBMCONFIG_SECURITYCODE1_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_SECURITYCODE1_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.securityCode2;
	pPayload[RADIO_MSGBMCONFIG_SECURITYCODE2_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_SECURITYCODE2_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.u16Spare3;
	pPayload[RADIO_MSGBMCONFIG_U16SPARE3_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_U16SPARE3_L] = (Uint8) (u16TempVar);

	u16TempVar = sMsgConfig.u16Spare4;
	pPayload[RADIO_MSGBMCONFIG_U16SPARE4_H] = (Uint8) (u16TempVar >> 8);
	pPayload[RADIO_MSGBMCONFIG_U16SPARE4_L] = (Uint8) (u16TempVar);

	u8TempVar = sMsgConfig.bmBitConfig;
	pPayload[RADIO_MSGBMCONFIG_BMBITCONFIG] = u8TempVar;

	u8TempVar = sMsgConfig.u8Spare2;
	pPayload[RADIO_MSGBMCONFIG_U8SPARE2] = u8TempVar;

	u8TempVar = sMsgConfig.u8Spare3;
	pPayload[RADIO_MSGBMCONFIG_U8SPARE3] = u8TempVar;

	u8TempVar = sMsgConfig.u8Spare4;
	pPayload[RADIO_MSGBMCONFIG_U8SPARE4] = u8TempVar;

	// Set length of data to send
	pInst->usedTxBuff = RADIO_MSGBMCONFIG_SIZE;
	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_SET_BMCONFIG, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	// Get Bm node Id
	reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->instId);
	// Send message
	radio__pnSendMpNwkDataReq(pInst, BMNwkId);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendBBC
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send best battery choice message.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendBBC(radio__Inst * pInst, Uint8 BBC) {
	BYTE * pPayload;
	radio__AcInst * pAcInst;
	Uint32 u32TempVar;
	Uint16 u16TempVar;
	Uint8 u8TempVar;

	pAcInst = (radio__AcInst *) pInst;

	/*
	 * Get pointer to the payload part of the PopNet gateway protocol network
	 * data request. Step forward to the data part of the message.
	 */

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	//reg_get(&pPayload[RADIO__MSGBBC_GROUP], radio__hBBC_var);
	// Get BBC group and modify(set MSB bit) if not BBC
	{
		reg_get(&u8TempVar, radio__hBBC_var);
		if(!BBC)
			u8TempVar |= 0x80;
		(pPayload)[RADIO__MSGBBC_GROUP] = (BYTE) u8TempVar;

	}
	reg_get(&pPayload[RADIO__MSGBBC_STATUS], radio__hBBCStatus);

	radio__acBuildU32(pPayload, RADIO__MSGBBC_TIME, radio__hBBCStatusTime);

	reg_getLocal(&u16TempVar, reg_i_BmTbatt, pInst->instId);
	(pPayload)[RADIO__MSGBBC_TBATT_H] = (BYTE) (u16TempVar >> 8);
	(pPayload)[RADIO__MSGBBC_TBATT_L] = (BYTE) u16TempVar;

	reg_getLocal(&u32TempVar, reg_i_ChargerId, pInst->instId);
	(pPayload)[RADIO__MSGBBC_CID_H] = (BYTE) (u32TempVar >> 24);
	(pPayload)[RADIO__MSGBBC_CID_HM] = (BYTE) (u32TempVar >> 16);
	(pPayload)[RADIO__MSGBBC_CID_LM] = (BYTE) (u32TempVar >> 8);
	(pPayload)[RADIO__MSGBBC_CID_L] = (BYTE) u32TempVar;

	// fill Spare registers with zero
	u32TempVar = 0;
	(pPayload)[RADIO__MSGBBC_U32SPARE1_H] = (BYTE) (u32TempVar >> 24);
	(pPayload)[RADIO__MSGBBC_U32SPARE1_HM] = (BYTE) (u32TempVar >> 16);
	(pPayload)[RADIO__MSGBBC_U32SPARE1_LM] = (BYTE) (u32TempVar >> 8);
	(pPayload)[RADIO__MSGBBC_U32SPARE1_L] = (BYTE) u32TempVar;

	u16TempVar = 0;
	(pPayload)[RADIO__MSGBBC_U16SPARE1_H] = (BYTE) (u16TempVar >> 8);
	(pPayload)[RADIO__MSGBBC_U16SPARE1_L] = (BYTE) u16TempVar;

	u8TempVar = 0;
	(pPayload)[RADIO__MSGBBC_U8SPARE1] = (BYTE) u8TempVar;

	pInst->usedTxBuff = RADIO__MSGBBC_SIZE;

	radio__acBuildHeader(pAcInst, RADIO__ACCMD_BBC, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	radio__pnSendMpNwkDataReq(pInst, RADIO__MACADDR_BROADCAST);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendBmCalibration
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send BmCalibration message to BM.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendBmCalibration(radio__Inst * pInst, Uint8 mesurePoint, Int32 measuredValue) {
	radio__AcInst * pAcInst;
	BYTE * pPayload;
	Uint16 BMNwkId;
	pAcInst = (radio__AcInst *) pInst;

	// Build data
	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	pPayload[RADIO__MSGSCA_TYPE] = 4;										// Type hardcoded to Current calibartion
	pPayload[RADIO__MSGSCA_MEASPOINT] = mesurePoint;						// MeasurePoint from input parameter (0=point1, 1=point2)

	(pPayload)[RADIO__MSGSCA_MEASVALUE_H] = (BYTE) (measuredValue >> 24);	// MeasuredValue from input parameter (in mV/mA)
	(pPayload)[RADIO__MSGSCA_MEASVALUE_HM] = (BYTE) (measuredValue >> 16);
	(pPayload)[RADIO__MSGSCA_MEASVALUE_LM] = (BYTE) (measuredValue >> 8);
	(pPayload)[RADIO__MSGSCA_MEASVALUE_L] = (BYTE) measuredValue;

	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGSCA_SIZE;

	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_SET_BMCALIBRATION, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);

	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->instId);
	radio__pnSendMpNwkDataReq(pInst, BMNwkId);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendCmEoc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send end of charge message to BM.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendCmEoc(radio__Inst * pInst) {
	BYTE * pPayload;
	radio__AcInst * pAcInst;
	Uint32 u32TempVar;
	Uint16 u16TempVar;
	Uint16 BMNwkId;
	Uint8 u8TempVar;

	pAcInst = (radio__AcInst *) pInst;

	/*
	 * Get pointer to the payload part of the PopNet gateway protocol network
	 * data request. Step forward to the data part of the message.
	 */

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	/*
	 * Build the message.
	 */
	// ChalgStatusSum
	radio__acBuildU16(pPayload, RADIO__MSGCMEOC_CHALGSTATUSSUM, radio__hChalgStatusSum);
	// ChargedWh
	//radio__acBuildU32(pPayload, RADIO__MSGCMEOC_CHARGEDAH, (Uint32)(radio__hChargedAh));
	reg_get(&u16TempVar, radio__hChargedAh);
	u32TempVar = u16TempVar;
	pPayload[RADIO__MSGCMEOC_CHARGEDAH_H] = (Uint8) (u32TempVar >> 24);
	pPayload[RADIO__MSGCMEOC_CHARGEDAH_HM] = (Uint8) (u32TempVar >> 16);
	pPayload[RADIO__MSGCMEOC_CHARGEDAH_LM] = (Uint8) (u32TempVar >> 8);
	pPayload[RADIO__MSGCMEOC_CHARGEDAH_L] = (Uint8) (u32TempVar);
	// ChargedWh
	radio__acBuildU32(pPayload, RADIO__MSGCMEOC_CHARGEDWH, radio__hChargedWh);

	// fill Spare registers with zero
	// U32Spare1
	radio__acBuildU32(pPayload, RADIO__MSGCMEOC_U32SPARE1, radio__hU32Spare);
	// U32Spare2
	radio__acBuildU32(pPayload, RADIO__MSGCMEOC_U32SPARE2, radio__hU32Spare);
	// U16Spare1
	radio__acBuildU16(pPayload, RADIO__MSGCMEOC_U16SPARE1, radio__hU16Spare);
	// U16Spare2
	radio__acBuildU16(pPayload, RADIO__MSGCMEOC_U16SPARE2, radio__hU16Spare);
	// U8Spare1
	u8TempVar = 0;
	(pPayload)[RADIO__MSGCMEOC_U8SPARE1] = (BYTE) u8TempVar;
	// U8Spare2
	(pPayload)[RADIO__MSGCMEOC_U8SPARE2] = (BYTE) u8TempVar;

	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGCMEOC_SIZE;
	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_CMEOC, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	// Get Bm node Id
	reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->instId);
	// Send message
	radio__pnSendMpNwkDataReq(pInst, BMNwkId);

}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendCmDplMasterAll
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send CmDplMAsterAll message.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendCmDplMasterAll(radio__Inst * pInst) {
	BYTE * pPayload;
	radio__AcInst * pAcInst;
	Uint8 powerGroup;
	Uint16 powerAllocated;
	Uint32 powerLimitTotal;
	Uint32 powerAllocatedTotal;
	Uint32 powerRequestedTotal;
	Uint32 powerConsumedTotal;
	Uint32 powerUnits;
	int index;


	pAcInst = (radio__AcInst *) pInst;

	/*
	 * Get pointer to the payload part of the PopNet gateway protocol network
	 * data request. Step forward to the data part of the message.
	 */

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	/*
	 * Build the message.
	 */
	// Power group
	powerGroup = dplGetPowerGroup();
	pPayload[RADIO__MSGCMDPLMS_PWRGROUP] = (Uint8) powerGroup;
	// Power limit total
	powerLimitTotal = dplGetDplPowerLimitTotal();
	pPayload[RADIO__MSGCMDPLMA_PWRLIMTOT_H] = (Uint8) (powerLimitTotal >> 24);
	pPayload[RADIO__MSGCMDPLMA_PWRLIMTOT_HM] = (Uint8) (powerLimitTotal >> 16);
	pPayload[RADIO__MSGCMDPLMA_PWRLIMTOT_LM] = (Uint8) (powerLimitTotal >> 8);
	pPayload[RADIO__MSGCMDPLMA_PWRLIMTOT_L] = (Uint8) (powerLimitTotal);
	// Power allocated total
	powerAllocatedTotal = dplGetPowerAllocatedTotal();
	pPayload[RADIO__MSGCMDPLMA_PWRALLOCTOT_H] = (Uint8) (powerAllocatedTotal >> 24);
	pPayload[RADIO__MSGCMDPLMA_PWRALLOCTOT_HM] = (Uint8) (powerAllocatedTotal >> 16);
	pPayload[RADIO__MSGCMDPLMA_PWRALLOCTOT_LM] = (Uint8) (powerAllocatedTotal >> 8);
	pPayload[RADIO__MSGCMDPLMA_PWRALLOCTOT_L] = (Uint8) (powerAllocatedTotal);
	// Power requested total
	powerRequestedTotal = dplGetPowerRequestedTotal();
	pPayload[RADIO__MSGCMDPLMA_PWRREQTOT_H] = (Uint8) (powerRequestedTotal >> 24);
	pPayload[RADIO__MSGCMDPLMA_PWRREQTOT_HM] = (Uint8) (powerRequestedTotal >> 16);
	pPayload[RADIO__MSGCMDPLMA_PWRREQTOT_LM] = (Uint8) (powerRequestedTotal >> 8);
	pPayload[RADIO__MSGCMDPLMA_PWRREQTOT_L] = (Uint8) (powerRequestedTotal);
	// Power units
	powerUnits = dplGetPowerUnitsCtrl();
	pPayload[RADIO__MSGCMDPLMA_PWRUNITS_H] = (Uint8) (powerUnits >> 24);
	pPayload[RADIO__MSGCMDPLMA_PWRUNITS_HM] = (Uint8) (powerUnits >> 16);
	pPayload[RADIO__MSGCMDPLMA_PWRUNITS_LM] = (Uint8) (powerUnits >> 8);
	pPayload[RADIO__MSGCMDPLMA_PWRUNITS_L] = (Uint8) (powerUnits);
	// Power allocation for master and slave 1-31
	for(index = 0; index < DPL_LIST_SIZE; index++){
		powerAllocated =  dplGetPowerAllocated(index);
		pPayload[RADIO__MSGCMDPLMA_PWRALLOCM_H + 2*index] = (Uint8) (powerAllocated >> 8);
		pPayload[RADIO__MSGCMDPLMA_PWRALLOCM_L + 2*index] = (Uint8) (powerAllocated);
	}
	// Power consumed total (VA)
	powerConsumedTotal = dplGetPowerConsumedTotal();
	pPayload[RADIO__MSGCMDPLMA_PWRCSUMDTOT_H] = (Uint8) (powerConsumedTotal >> 24);
	pPayload[RADIO__MSGCMDPLMA_PWRCSUMDTOT_HM] = (Uint8) (powerConsumedTotal >> 16);
	pPayload[RADIO__MSGCMDPLMA_PWRCSUMDTOT_LM] = (Uint8) (powerConsumedTotal >> 8);
	pPayload[RADIO__MSGCMDPLMA_PWRCSUMDTOT_L] = (Uint8) (powerConsumedTotal);

	// fill Spare registers with zero
	// U16Spare1
	radio__acBuildU16(pPayload, RADIO__MSGCMDPLMA_U16SPARE1, radio__hU16Spare);
	// U8Spare1
	reg_get(&pPayload[RADIO__MSGCMDPLMA_U8SPARE1], radio__hU8Spare);

	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGCMDPLMA_SIZE;
	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_CMDPLMASTERALL, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	// Send message broadcast
	radio__pnSendMpNwkDataReq(pInst, RADIO__MACADDR_BROADCAST);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendCmDplMasterSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send CmDplMAsterAll message.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendCmDplMasterSlave(radio__Inst * pInst, Uint16 slaveAddr, Uint8 pwrGrp, Uint8 pwrUnit, Uint16 pwrAlloc) {
	BYTE * pPayload;
	radio__AcInst * pAcInst;

	pAcInst = (radio__AcInst *) pInst;

	/*
	 * Get pointer to the payload part of the PopNet gateway protocol network
	 * data request. Step forward to the data part of the message.
	 */

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	/*
	 * Build the message.
	 */
	// Power group
	//reg_get(&pPayload[RADIO__MSGCMDPLMS_PWRGROUP], radio__hU8Spare);
	pPayload[RADIO__MSGCMDPLMS_PWRGROUP] = (Uint8) pwrGrp;
	// Power unit
	//reg_get(&pPayload[RADIO__MSGCMDPLMS_PWRUINT], radio__hU8Spare);
	pPayload[RADIO__MSGCMDPLMS_PWRUINT] = (Uint8) pwrUnit;
	// Power allocated
	//radio__acBuildU16(pPayload, RADIO__MSGCMDPLMS_PWRALLOC, radio__hU16Spare);
	pPayload[RADIO__MSGCMDPLMS_PWRALLOC_H] = (Uint8) (pwrAlloc >> 8);
	pPayload[RADIO__MSGCMDPLMS_PWRALLOC_L] = (Uint8) pwrAlloc;

	// fill Spare registers with zero
	// U32Spare1
	radio__acBuildU32(pPayload, RADIO__MSGCMDPLMS_U32SPARE1, radio__hU32Spare);
	// U16Spare1
	radio__acBuildU16(pPayload, RADIO__MSGCMDPLMS_U16SPARE1, radio__hU16Spare);
	// U8Spare1
	reg_get(&pPayload[RADIO__MSGCMDPLMS_U8SPARE1], radio__hU8Spare);

	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGCMDPLMS_SIZE;
	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_CMDPLMASTERSLAVE, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	// Send message to slave
	radio__pnSendMpNwkDataReq(pInst, slaveAddr);

}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendGetCmDplSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send radio__acSendGetCmDplSlave message.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendGetCmDplSlave(radio__Inst * pInst) {
	BYTE * pPayload;
	radio__AcInst * pAcInst;
	Uint16 dplMasterAllCntr;
	Uint8 powerGroupCfg;
	Uint8 powerUnit;

	pAcInst = (radio__AcInst *) pInst;

	/*
	 * Get pointer to the payload
	 */
	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	/*
	 * Build the message.
	 */
	// Power group
	powerGroupCfg = dplGetPowerGroup();
	pPayload[RADIO__MSGGETCMDPLS_PWRGROUP] = powerGroupCfg;
	// Power unit
	powerUnit = 0xFF;
	pPayload[RADIO__MSGGETCMDPLS_PWRUINT] = powerUnit;

	// fill Spare registers with zero
	// U32Spare1
	radio__acBuildU32(pPayload, RADIO__MSGGETCMDPLS_U32SPARE1, radio__hU32Spare);
	// U16Spare1
	radio__acBuildU16(pPayload, RADIO__MSGGETCMDPLS_U16SPARE1, radio__hU16Spare);
	// U8Spare1
	reg_get(&pPayload[RADIO__MSGGETCMDPLS_U8SPARE1], radio__hU8Spare);

	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGGETCMDPLS_SIZE;
	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_GET_CMDPLSLAVE, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	// Send message to slave
	radio__pnSendMpNwkDataReq(pInst, RADIO__MACADDR_BROADCAST);

	// Start timeout timer
	dplMasterAllCntr = (DPL_SLAVE_JITTER + 3) * TICKS_PER_SECOND;
	reg_putLocal(&dplMasterAllCntr, reg_i_dplMasterAllCntr, pInst->instId);
	pInst->dplMasterAllTimeoutCntr = dplMasterAllCntr;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendCmDplSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send radio__acSendCmDplSlave message.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendCmDplSlave(radio__Inst * pInst) {
	BYTE * pPayload;
	radio__AcInst * pAcInst;

	pAcInst = (radio__AcInst *) pInst;
	Uint16 dplMasterNwkAddr;
	Uint8 powerGroup;
	Uint8 dplId;
	Uint8 soc;
	Uint8 prio;
	Uint16 pwrReq;

	reg_getLocal(&dplMasterNwkAddr, reg_i_dplMasterNwkAddr, pInst->instId);
	powerGroup = dplGetPowerGroup();
	dplId = dplGetDplId();
	prio = dplGetDplPriorityFactor();
	pwrReq = dplGetPacRequest();
	soc = dplGetSoc();

	// Get pointer to payload
	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	// Set power group
	pPayload[RADIO__MSGCMDPLS_PWRGROUP] = powerGroup;
	// Set power unit/dplId
	pPayload[RADIO__MSGCMDPLS_PWRUINT] = dplId;
	// Set SOC
	pPayload[RADIO__MSGCMDPLS_SOC] = soc;
	// Set PRIO
	pPayload[RADIO__MSGCMDPLS_PRIO] = prio;
	// Set power request
	(pPayload)[RADIO__MSGCMDPLS_PWRREQ_H] = (BYTE) (pwrReq >> 8);
	(pPayload)[RADIO__MSGCMDPLS_PWRREQ_L] = (BYTE) pwrReq;
	// Power consumed
	radio__acBuildU32(pPayload, RADIO__MSGCMDPLS_PWRCSUMD, radio__Smeas1s);

	// fill Spare registers with zero
	// U16Spare1
	radio__acBuildU16(pPayload, RADIO__MSGCMDPLS_U16SPARE1, radio__hU16Spare);
	// U8Spare1
	reg_get(&pPayload[RADIO__MSGCMDPLS_U8SPARE1], radio__hU8Spare);


	// Set length of data to send
	pInst->usedTxBuff = RADIO__MSGCMDPLS_SIZE;
	// Build header
	radio__acBuildHeader(pAcInst, RADIO__ACCMD_CMDPLSLAVE, pAcInst->seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
	pAcInst->seqNum++;
	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	// Send message to slave
	radio__pnSendMpNwkDataReq(pInst, dplMasterNwkAddr);

}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acSendAcknowledge
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send acknowledge message.
*
*	\param		pInst	Pointer to instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__acSendAcknowledge(radio__Inst * pInst, Uint16 dstAddr, Uint8 seqNum, Uint8 reqCmd, Uint8 status) {
	radio__AcInst * pAcInst;
	BYTE * pPayload;

	pAcInst = (radio__AcInst *) pInst;

	pPayload = radio__acGetPayloadPointer(pInst);
	pPayload += RADIO__MSGH_DATA;

	pPayload[RADIO__MSGACK_REQCMD] = reqCmd;
	pPayload[RADIO__MSGACK_STATUS] = status;

	pInst->usedTxBuff = RADIO__MSGACK_SIZE;

	radio__acBuildHeader(pAcInst, RADIO__ACCMD_ACK, seqNum,
			RADIO__MSGHCTRL_SINGLE
	);

	pAcInst->pLastMsg = &pAcInst->spontaneousMsg;

	radio__pnSendMpNwkDataReq(pInst, dstAddr);

}

/*******************************************************************************
 *
 *	F U N C T I O N   D E S C R I P T I O N
 *
 *	radio__acProcAck
 *
 *--------------------------------------------------------------------------*//**
 *
 *	\brief		Process acknowledge message.
 *
 *	\param		pInst	Pointer to instance.
 *	\param		pMsgInfo	Pointer to message info. Contains information about
 *							the incoming message.
 *
 *	\return
 *
 *	\details
 *
 *	\note
 *
 *******************************************************************************/

PRIVATE radio__ErrCode radio__acProcAck(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGACK_SIZE){
		Uint8 cmd;

		// Get command that sent the request
		cmd = pMsgInfo->pData[RADIO__MSGACK_REQCMD];

		switch(cmd)
		{
			case RADIO__ACCMD_SET_BMCALIBRATION:
			{
				Uint8	radioAck = TRUE;

				reg_putLocal(&radioAck, reg_i_radioAck, pInst->parent.parent.instId); // message received ok
				break;
			}

			case RADIO__ACCMD_SET_BMDATA:
			case RADIO__ACCMD_SET_BMCONFIG:
			{
				Uint8					cmdStatus;

				/*
				 * Update status registers so that GUI can display the sending bmu parameters Ok
				 * text.
				 */
				cmdStatus = RADIO_CMDS_BMUPARAMOK;
				reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.parent.instId);
				pInst->parent.flags &= ~RADIO__PNFLAG_BMPARAM;

				break;
			}
		}
	}
	return (retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcJoinInd
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Join indication response from joining node.
*
*	\param		pInst	Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcJoinInd(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	radio__ErrCode retVal = RADIO__ERR_PROCOK;
//JJ removed for new joinEnable handling
/*
	if(pMsgInfo->size == RADIO__MSGJOININD_SIZE){
		// abort join enable
		Uint8 joinEnabled;
		joinEnabled = 0;
		reg_putLocal(&joinEnabled, reg_i_joinEnable, pInst->parent.parent.instId);
	}
*/
	return (retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcSetTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process set time message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*
*	\retval		RADIO__ERR_PROCOK_RESP	if the message was processed ok
*										and a response message has been built on
*										the buffer.
*	\retval		RADIO__ERR_PROCOK		if the message was processed ok.
*	\retval		RADIO__ERR_ERROR		If a error occurred while processing the
*										message.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcSetTime(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	Uint32 time;
	reg_Status status;
	Uint8 ackStatus;
	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGST_SIZE){
		/*
		 * Parse message and set the device time.
		 */

		time = pMsgInfo->pData[RADIO__MSGST_TIME_H] << 24;
		time |= pMsgInfo->pData[RADIO__MSGST_TIME_HM] << 16;
		time |= pMsgInfo->pData[RADIO__MSGST_TIME_LM] << 8;
		time |= pMsgInfo->pData[RADIO__MSGST_TIME_L];
		status = reg_put(&time, radio__hSetRTC);

		/*
		 * Build response message
		 */

		ackStatus = status == REG_OK ? 0 : 0xFF;

		radio__acBuildAck(pInst, RADIO__ACCMD_SET_TIME, ackStatus);

		retVal = RADIO__ERR_PROCOK_RESP;
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcBBC
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process BBC message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*
*	\retval		RADIO__ERR_PROCOK_RESP	if the message was processed ok
*										and a response message has been built on
*										the buffer.
*	\retval		RADIO__ERR_PROCOK		if the message was processed ok.
*	\retval		RADIO__ERR_ERROR		If a error occurred while processing the
*										message.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcBBC(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {

	radio__ErrCode retVal = RADIO__ERR_PROCOK;
	
	if(pMsgInfo->size == RADIO__MSGBBC_SIZE){
		/*
		 * Get active BBC status from network
		 */
		Uint32 BBCStatusTimeActive;
		Int16 TbattActive;
		Uint8 BBCStatusActive;
		Uint8 BBCGroupActive;
		Uint8 BBCGroupLocal;

		BBCStatusTimeActive = pMsgInfo->pData[RADIO__MSGBBC_TIME_H] << 24;
		BBCStatusTimeActive |= pMsgInfo->pData[RADIO__MSGBBC_TIME_HM] << 16;
		BBCStatusTimeActive |= pMsgInfo->pData[RADIO__MSGBBC_TIME_LM] << 8;
		BBCStatusTimeActive |= pMsgInfo->pData[RADIO__MSGBBC_TIME_L];

		TbattActive = pMsgInfo->pData[RADIO__MSGBBC_TBATT_H] << 8;
		TbattActive |= pMsgInfo->pData[RADIO__MSGBBC_TBATT_L];

		BBCStatusActive = pMsgInfo->pData[RADIO__MSGBBC_STATUS];

		BBCGroupActive = pMsgInfo->pData[RADIO__MSGBBC_GROUP];

		/*
		 * Check if same group
		 */
		
		reg_get(&BBCGroupLocal, radio__hBBC_var);

		if(BBCGroupActive == BBCGroupLocal)
		{
			/*
			 * Get local BBC status
			 */
			Uint32 BBCStatusTimeLocal;
			Int16 TbattLocal;
			Uint16 BBCPollCntr;
			Uint16 nodeIdLocal;
			Uint8 BBCStatusLocal;
			Uint8 ChargingMode;
			Uint8 BBC = FALSE;


			reg_get(&BBCStatusTimeLocal, radio__hBBCStatusTime);
			//reg_get(&TbattLocal, radio__hTbatt);
			reg_getLocal(&TbattLocal, reg_i_BmTbatt, pInst->parent.parent.instId);
			reg_get(&BBCStatusLocal, radio__hBBCStatus);
			reg_get(&ChargingMode, radio__currChargingMode);
			reg_getLocal(&nodeIdLocal, reg_i_NodeId, pInst->parent.parent.instId);

			/*
			 * compare BBCStatus.
			 */

			if(BBCStatusLocal > BBCStatusActive)
			{
				BBC = TRUE;	// this is new BBC
			}
			else if(BBCStatusLocal == BBCStatusActive)
			{
				if(BBCStatusTimeLocal > BBCStatusTimeActive)
				{
					BBC = TRUE; // This is new BBC
				}
				else if(BBCStatusTimeLocal == BBCStatusTimeActive)
				{
					if(nodeIdLocal < pMsgInfo->address)
					{
						// Node address used to differentiate.
						// Lowest node address considered as best.
						BBC = TRUE; // This is new BBC
					}
				}
			}

			/*
			 * update BBCPollCntr
			 */

			if(BBC)
			{
				BBCPollCntr = radio__acProcGetDelay();	// Get delay for sending out BBC message
				if(BBCPollCntr < pInst->parent.parent.BBCPollCntr){	// Only update if new value is less than present value
					reg_putLocal(&BBCPollCntr, reg_i_BBCPollCntr, pInst->parent.parent.instId);
				}
			}
			else
			{
				BBCPollCntr = TICKS_PER_SECOND * (4*BBC_POLL_INTERVAL + 30);/* 4 minutes + 30 seconds in 50ms resolution */
				reg_putLocal(&BBCPollCntr, reg_i_BBCPollCntr, pInst->parent.parent.instId);
				reg_put(&BBC, radio__hBBC);
			}
		}
	}
	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcGetDelay
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate Delay for BBC message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*	\retval		Delay
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 radio__acProcGetDelay(void) {

	/*
	 * Calculate delay time due to BBCStatus and BBCStatusTime.
	 * The Delay resolution is 50ms, which means that a delay value of 20 is 1 second (20*50ms)
	 *
	 * Delay due to BBCStatus is:
	 * Maintenance 	 >60h	1s -> Delay = 20
	 * Maintenance 60-30h	2s -> Delay = 40
	 * Maintenance 30-0h	3s -> Delay = 60
	 * Equalize 			6s -> Delay = 120
	 * Additional 			7s -> Delay = 140
	 * Main 				8s -> Delay = 160
	 * Pre 					9s -> Delay = 180
	 *
	 * Delay due to BBCStatusTime is calculated as follows:
	 * Maintenance	  >60h  0-1s	-> Delay 0-20 	(90min time slice)
	 * Maintenance 	60-30h 	0-1s 	-> Delay 0-20	(90min time slice)
	 * Maintenance 	30-0h 	0-3s 	-> Delay 0-60	(30min time slice)
	 * Equalize		10-0h 	0-1s	-> Delay 0-20	(30min time slice)
	 * Additional 	10-0h 	0-1s	-> Delay 0-20	(30min time slice)
	 * Main 		10-0h 	0-1s	-> Delay 0-20	(30min time slice)
	 * Pre 			10-0h 	0-1s	-> Delay 0-20	(30min time slice)
	 */

		Uint32 BBCStatusTimeLocal;
		Uint32 BBCStatusTimeNorm;
		Uint16 Delay;
		Uint16 BBCStatusDelayMax;
		Uint8 BBCStatusLocal;

		reg_get(&BBCStatusTimeLocal, radio__hBBCStatusTime);
		reg_get(&BBCStatusLocal, radio__hBBCStatus);

		if(BBCStatusLocal & BBC_STAT_MAINTENANCE){

			if(BBCStatusTimeLocal < 30*3600){									// 0-30h (in seconds)
				Delay = 60;														// 3 seconds delay because of status and time
				BBCStatusTimeNorm = BBCStatusTimeLocal / 1800;					// divide into 60 30 min slices (1800 seconds)
				BBCStatusDelayMax = 59;											// 0-2.950 seconds delay (0-59*50ms) for 30-0h

				if(BBCStatusTimeNorm > BBCStatusDelayMax)
				{
					BBCStatusTimeNorm = BBCStatusDelayMax;
				}
			}
			else if(BBCStatusTimeLocal < 60*3600){								// 30-60h (in seconds)
				Delay = 40;														// 2 second delay because of status and time
				BBCStatusTimeNorm = (BBCStatusTimeLocal - 30*3600) / 5400;		// divide into 30 90min slices (5400 seconds)
				BBCStatusDelayMax = 19;											// 0-0.950 seconds delay (0-19*50ms) for 60-30h

				if(BBCStatusTimeNorm > BBCStatusDelayMax)
				{
					BBCStatusTimeNorm = BBCStatusDelayMax;
				}
			}
			else {																// if more than than 60h (or equal)
				Delay = 20;														// 1 second delay because of status and time
				BBCStatusTimeNorm = (BBCStatusTimeLocal - 60*3600) / 5400;		// divide time above 60h into 90min slices (5400 seconds)
				BBCStatusDelayMax = 19;											// 0-0.950 second delay (0-19*50ms) for 90-60h

				if(BBCStatusTimeNorm > BBCStatusDelayMax)
				{
					BBCStatusTimeNorm = BBCStatusDelayMax;						// if more than 90h add no delay
				}
			}

		}
		else
		{
			if(BBCStatusLocal & BBC_STAT_EQUALIZE){
				Delay = 120;													// 6 seconds delay because of status
			}
			else if(BBCStatusLocal & BBC_STAT_ADDITIONAL){
				Delay = 140;													// 7 seconds delay because of status
			}
			else if(BBCStatusLocal & BBC_STAT_MAIN){
				Delay = 160;													// 8 seconds delay because of status
			}
			else{
				Delay = 180;													// 9 seconds delay because of status
			}

			BBCStatusTimeNorm = BBCStatusTimeLocal / 1800;						// divide 10h into 20 30min slices (1800 seconds)
			BBCStatusDelayMax = 19;												// 0-0.950 seconds delay (19*50ms) for 10-0h

			if(BBCStatusTimeNorm > BBCStatusDelayMax)
			{
				BBCStatusTimeNorm = BBCStatusDelayMax;
			}
		}

		Delay += (BBCStatusDelayMax - BBCStatusTimeNorm);						// high BBCStatusTimeNorm value gives short delay

	return (Delay);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcSetMpl
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process set mpl message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*
*	\retval		RADIO__ERR_PROCOK_RESP	if the message was processed ok
*										and a response message has been built on
*										the buffer.
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR		If a error occurred while processing the
*										message.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcSetMpl(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	reg_Status status = 0xFF;
	Uint8 ackStatus = 0xFF;
	Uint8 PowerGroupLocal;
	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGSMPL_SIZE){
		/*
		 * Parse message and store the values to REG if correct PowerGroup.
		 */
		reg_get(&PowerGroupLocal, radio__hPowerGroup);

		if (PowerGroupLocal == pMsgInfo->pData[RADIO__MSGSMPL_PWRGRP]) {
			Uint16 powerLimit;

			powerLimit = pMsgInfo->pData[RADIO__MSGSMPL_PWRLIMIT_H] << 8;
			powerLimit |= pMsgInfo->pData[RADIO__MSGSMPL_PWRLIMIT_L];
			status = reg_put(&powerLimit, radio__hIacLimit);

			/*
			 * Spare params
			 *
			RADIO__MSGSMPL_U32SPARE1
			RADIO__MSGSMPL_U16SPARE1
			RADIO__MSGSMPL_U8SPARE1
			*/

			ackStatus = status == REG_OK ? 0 : 0xFF;
		}

		/*
		 * Build response message
		 */
		radio__acBuildAck(pInst, RADIO__ACCMD_SET_MPL, ackStatus);

		retVal = RADIO__ERR_PROCOK_RESP;
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcSetConfig
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process set config message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsg		Pointer to message.
*	\param		size		Size of the message in bytes.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcSetConfig(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	Uint8 u8TempVar;
	Uint16 u16TempVar;
	Uint32 u32TempVar;
	Uint32 serialNo;
	Uint16 status = REG_OK;
	radio__ErrCode retVal = RADIO__ERR_PROCOK;
	static Boolean serialNoMatch = FALSE;

	if (pMsgInfo->control == RADIO__MSGHCTRL_CFG_1ST) {
		if(pMsgInfo->size == RADIO__MSGCMCFG1_SIZE){
			/*
			 * First config package.
			 */

			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_H]) << 24;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_HM]) << 16;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_LM]) << 8;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_L]);
			reg_get(&serialNo, radio__hSerialNo);

			if(u32TempVar == serialNo){
				/*
				 * Only write config if serialNo match
				 */
				serialNoMatch = TRUE;

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_PANID_H]) << 8;
				u16TempVar |= pMsgInfo->pData[RADIO__MSGCMCFG1_PANID_L];
				status = reg_putLocal(&u16TempVar, reg_i_setPanId,
						pInst->parent.parent.instId);

				u8TempVar = (Uint8) pMsgInfo->pData[RADIO__MSGCMCFG1_CHANNELID];
				// setChannel is in interval 0-15 (channel is in interval 11-26)
				status |= reg_putLocal(&u8TempVar,
						reg_i_setChannel, pInst->parent.parent.instId);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_NWKID_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_NWKID_L]);
				status |= reg_putLocal(&u16TempVar, reg_i_setNodeId,
						pInst->parent.parent.instId);

	//			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_H])
	//					<< 24;
	//			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_HM])
	//					<< 16;
	//			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_LM])
	//					<< 8;
	//			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_SERIALNO_L]);
	//			if(u32TempVar != 0)
	//			{
	//				status |= reg_put(&u32TempVar, radio__hSerialNo);
	//			}

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ENGINECODE_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ENGINECODE_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ENGINECODE_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ENGINECODE_L]);

				//Check condition before updating current used egine code and value stored to flash .
				project_storeEngineCode(u32TempVar);

				status |= reg_put(
						&pMsgInfo->pData[RADIO__MSGCMCFG1_BATTERYTYPE],
						radio__hDefBatteryType
				);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_ALGNO_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_ALGNO_L]);

				// Update engine index which is shown in menu, Alg no is updated in "StBattInfoFn"
				chalg_UpdateAlgIdx(u16TempVar);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_CAPACITY_H])
						<< 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_CAPACITY_L]);
				status |= reg_put(&u16TempVar, radio__hDefCapacity);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_CABLERES_H])
						<< 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_CABLERES_L]);
				status |= reg_put(&u16TempVar, radio__hDefCableRes);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_CELL_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_CELL_L]);
				status |= reg_put(&u16TempVar, radio__hDefCell);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_BASELOAD_H])
						<< 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG1_BASELOAD_L]);
				status |= reg_put(&u16TempVar, radio__hDefBaseLoad);

				//Store the default battery parameters to flash memory.
				pInst->parent.parent.pInit->pStBattInfoFn(TRUE);

				status |= reg_put(
						&pMsgInfo->pData[RADIO__MSGCMCFG1_POWERGROUP],
						radio__hPowerGroup
				);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IACLIMIT_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IACLIMIT_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IACLIMIT_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IACLIMIT_L]);
				status |= reg_put(&u32TempVar, radio__hIacLimit);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_PACLIMIT_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_PACLIMIT_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_PACLIMIT_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_PACLIMIT_L]);
				status |= reg_put(&u32TempVar, radio__hPacLimit);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IDCLIMIT_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IDCLIMIT_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IDCLIMIT_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IDCLIMIT_L]);
				status |= reg_put(&u32TempVar, radio__hIdcLimit);
		/*
				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADSLOPE_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADSLOPE_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADSLOPE_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADSLOPE_L]);
				status |= reg_put(&u32TempVar, radio__hUadSlope);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADOFFSET_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADOFFSET_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADOFFSET_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_UADOFFSET_L]);
				status |= reg_put(&u32TempVar, radio__hUadOffset);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADSLOPE_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADSLOPE_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADSLOPE_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADSLOPE_L]);
				status |= reg_put(&u32TempVar, radio__hIadSlope);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADOFFSET_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADOFFSET_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADOFFSET_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_IADOFFSET_L]);
				status |= reg_put(&u32TempVar, radio__hIadOffset);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETSLOPE_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETSLOPE_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETSLOPE_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETSLOPE_L]);
				status |= reg_put(&u32TempVar, radio__hIsetSlope);

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETOFFSET_H])
						<< 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETOFFSET_HM])
						<< 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETOFFSET_LM])
						<< 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG1_ISETOFFSET_L]);
				status |= reg_put(&u32TempVar, radio__hIsetOffset);
		*/
				/*
				 * Spare registers not handled
				 * RADIO__MSGCMCFG1_U32SPARE5
				 * RADIO__MSGCMCFG1_U32SPARE6
				 * RADIO__MSGCMCFG1_U32SPARE7
				 * RADIO__MSGCMCFG1_U32SPARE8
				 * RADIO__MSGCMCFG1_U32SPARE9
				 * RADIO__MSGCMCFG1_U32SPARE10
				 */

				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_DISPCONTRAST], radio__hDispContrast);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_LEDBRIGHTMAX], radio__hLedBrightMax);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_LEDBRIGHTDIM], radio__hLedBrightDim);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_LANGUAGE], radio__hLanguage);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMEDATEFORMAT], radio__hTimeDateFormat);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_BUTTONF1FUNC], radio__hButtonF1_func);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_BUTTONF2FUNC], radio__hButtonF2_func);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_REMOTEINFUNC], radio__hRemoteIn_func);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_REMOTEOUTFUNC], radio__hRemoteOut_func);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_REMOTEOUTALARMVAR], radio__hRemoteOutAlarm_var);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_REMOTEOUTPHASEVAR], radio__hRemoteOutPhase_var);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_WATERFUNC], radio__hWater_func);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_WATERVAR], radio__hWater_var);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_AIRPUMPVAR], radio__hAirPump_var);
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_AIRPUMPVAR2], radio__hAirPump_var2);
	#if PROJECT_USE_CAN == 1
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_PARALLELFUNC], radio__hParallelControl_func);
	#endif
				status
						|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMERESTRICTION], radio__hTimeRestriction);

				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_0],
						radio__httCtrl, 0);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_1],
						radio__httFromHour, 0);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_2],
						radio__httFromMin, 0);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_3],
						radio__httToHour, 0);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_4],
						radio__httToMin, 0);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_5],
						radio__httCtrl, 1);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_6],
						radio__httFromHour, 1);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_7],
						radio__httFromMin, 1);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_8],
						radio__httToHour, 1);
				status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG1_TIMETABLE_9],
						radio__httToMin, 1);

				/*
				 * Build a response message.
				 */
				radio__acBuildAck(pInst, RADIO__ACCMD_SET_CMCONFIG, pMsgInfo->control);
				retVal = RADIO__ERR_PROCOK_RESP;
			}
			else{
				/*
				 * Set flag for address conflict error
				 */
				msg_sendTry(
					MSG_LIST(statusRep),
					PROJECT_MSGT_STATUSREP,
					SUP_STAT_ADDRESS_CONFLICT
				);

				serialNoMatch = FALSE;
			}
		}
	} else if (pMsgInfo->control == RADIO__MSGHCTRL_CFG_2ND) {
		if(pMsgInfo->size == RADIO__MSGCMCFG2_SIZE && serialNoMatch == TRUE){
			/*
			 * Second config package.
			 */
			status = reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_10],
					radio__httCtrl, 2);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_11],
					radio__httFromHour, 2);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_12],
					radio__httFromMin, 2);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_13],
					radio__httToHour, 2);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_14],
					radio__httToMin, 2);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_15],
					radio__httCtrl, 3);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_16],
					radio__httFromHour, 3);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_17],
					radio__httFromMin, 3);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_18],
					radio__httToHour, 3);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_19],
					radio__httToMin, 3);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_20],
					radio__httCtrl, 4);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_21],
					radio__httFromHour, 4);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_22],
					radio__httFromMin, 4);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_23],
					radio__httToHour, 4);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_24],
					radio__httToMin, 4);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_25],
					radio__httCtrl, 5);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_26],
					radio__httFromHour, 5);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_27],
					radio__httFromMin, 5);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_28],
					radio__httToHour, 5);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_29],
					radio__httToMin, 5);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_30],
					radio__httCtrl, 6);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_31],
					radio__httFromHour, 6);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_32],
					radio__httFromMin, 6);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_33],
					radio__httToHour, 6);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_34],
					radio__httToMin, 6);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_35],
					radio__httCtrl, 7);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_36],
					radio__httFromHour, 7);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_37],
					radio__httFromMin, 7);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_38],
					radio__httToHour, 7);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_39],
					radio__httToMin, 7);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_40],
					radio__httCtrl, 8);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_41],
					radio__httFromHour, 8);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_42],
					radio__httFromMin, 8);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_43],
					radio__httToHour, 8);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_44],
					radio__httToMin, 8);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_45],
					radio__httCtrl, 9);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_46],
					radio__httFromHour, 9);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_47],
					radio__httFromMin, 9);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_48],
					radio__httToHour, 9);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_49],
					radio__httToMin, 9);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_50],
					radio__httCtrl, 10);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_51],
					radio__httFromHour, 10);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_52],
					radio__httFromMin, 10);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_53],
					radio__httToHour, 10);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_54],
					radio__httToMin, 10);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_55],
					radio__httCtrl, 11);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_56],
					radio__httFromHour, 11);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_57],
					radio__httFromMin, 11);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_58],
					radio__httToHour, 11);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_59],
					radio__httToMin, 11);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_60],
					radio__httCtrl, 12);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_61],
					radio__httFromHour, 12);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_62],
					radio__httFromMin, 12);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_63],
					radio__httToHour, 12);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_64],
					radio__httToMin, 12);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_65],
					radio__httCtrl, 13);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_66],
					radio__httFromHour, 13);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_67],
					radio__httFromMin, 13);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_68],
					radio__httToHour, 13);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_69],
					radio__httToMin, 13);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_70],
					radio__httCtrl, 14);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_71],
					radio__httFromHour, 14);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_72],
					radio__httFromMin, 14);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_73],
					radio__httToHour, 14);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_74],
					radio__httToMin, 14);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_75],
					radio__httCtrl, 15);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_76],
					radio__httFromHour, 15);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_77],
					radio__httFromMin, 15);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_78],
					radio__httToHour, 15);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_79],
					radio__httToMin, 15);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_80],
					radio__httCtrl, 16);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_81],
					radio__httFromHour, 16);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_82],
					radio__httFromMin, 16);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_83],
					radio__httToHour, 16);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_84],
					radio__httToMin, 16);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_85],
					radio__httCtrl, 17);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_86],
					radio__httFromHour, 17);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_87],
					radio__httFromMin, 17);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_88],
					radio__httToHour, 17);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_89],
					radio__httToMin, 17);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_90],
					radio__httCtrl, 18);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_91],
					radio__httFromHour, 18);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_92],
					radio__httFromMin, 18);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_93],
					radio__httToHour, 18);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_94],
					radio__httToMin, 18);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_95],
					radio__httCtrl, 19);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_96],
					radio__httFromHour, 19);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG2_TIMETABLE_97],
					radio__httFromMin, 19);

			/*
			 * Build a response message.
			 */
			radio__acBuildAck(pInst, RADIO__ACCMD_SET_CMCONFIG, pMsgInfo->control);
			retVal = RADIO__ERR_PROCOK_RESP;
		}
	} else if (pMsgInfo->control == RADIO__MSGHCTRL_CFG_3RD) {
		if(pMsgInfo->size == RADIO__MSGCMCFG3_SIZE  && serialNoMatch == TRUE){
			/*
			 * Third package.
			 */

			status = reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG3_TIMETABLE_98],
					radio__httToHour, 19);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG3_TIMETABLE_99],
					radio__httToMin, 19);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG3_TIMETABLE_100],
					radio__httCtrl, 20);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG3_TIMETABLE_101],
					radio__httFromHour, 20);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG3_TIMETABLE_102],
					radio__httFromMin, 20);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG3_TIMETABLE_103],
					radio__httToHour, 20);
			status |= reg_aPut(&pMsgInfo->pData[RADIO__MSGCMCFG3_TIMETABLE_104],
					radio__httToMin, 20);

			/*
			 * CAN Config
			 */
#if PROJECT_USE_CAN == 1
			status
					|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG3_CANMODE], radio__hCANMode);

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG3_CANNWKCTRL_H])
					<< 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG3_CANNWKCTRL_L]);
			status |= reg_put(&u16TempVar, radio__hCANNwkCtrl);
#endif
			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_0_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_0_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_0_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_0_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 0);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_1_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_1_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_1_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_1_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 1);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_2_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_2_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_2_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_2_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 2);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_3_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_3_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_3_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_3_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 3);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_4_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_4_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_4_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_4_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 4);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_5_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_5_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_5_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_5_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 5);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_6_H])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_6_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_6_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_6_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 6);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_7_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_7_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_7_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PINOUTSELECT_7_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinOutSelect, 7);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_0_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_0_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_0_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_0_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 0);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_1_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_1_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_1_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_1_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 1);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_2_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_2_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_2_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_2_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 2);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_3_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_3_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_3_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_3_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 3);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_4_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_4_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_4_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_4_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 4);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_5_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_5_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_5_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_5_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 5);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_6_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_6_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_6_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_6_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 6);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_7_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_7_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_7_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_PININSELECT_7_L]);
			status |= reg_aPut(&u32TempVar, radio__hPinInSelect, 7);

			/*
			 * RADIO Config
			 */

			status |= reg_putLocal(&pMsgInfo->pData[RADIO__MSGCMCFG3_RADIOMODE],
					reg_i_RadioMode, pInst->parent.parent.instId);

			status |= reg_put(
					&pMsgInfo->pData[RADIO__MSGCMCFG3_CHARGINGMODE],
					radio__DefChargingMode
			);

			//Store the default battery parameters to flash memory.
			pInst->parent.parent.pInit->pStBattInfoFn(TRUE);
	/*
			status |= reg_putLocal(&pMsgInfo->pData[RADIO__MSGCMCFG3_JOINENABLE],
					reg_i_joinEnable, pInst->parent.parent.instId);
	*/
			status |= reg_putLocal(&pMsgInfo->pData[RADIO__MSGCMCFG3_NWKSETTINGS],
					reg_i_NwkSettings, pInst->parent.parent.instId);

			/*
			 * Other stuff
			 */

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE1_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE1_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE1_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE1_L]);
			status |= reg_put(&u32TempVar, radio__hSecurityCode1);

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE2_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE2_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE2_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG3_SECURITYCODE2_L]);
			status |= reg_put(&u32TempVar, radio__hSecurityCode2);

			status |= reg_put(
					&pMsgInfo->pData[RADIO__MSGCMCFG3_BLTIME],
					radio__hBacklightTime
			);

			u16TempVar
					|= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG3_INSTLOGSAMPPER_H])
							<< 8;
			u16TempVar
					|= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG3_INSTLOGSAMPPER_L]);
			status |= reg_put(&u16TempVar, radio__hInstLogSPeriod);

			/*
			 * Build a response message.
			 */
			radio__acBuildAck(pInst, RADIO__ACCMD_SET_CMCONFIG, pMsgInfo->control);
			retVal = RADIO__ERR_PROCOK_RESP;
		}
	} else if (pMsgInfo->control == RADIO__MSGHCTRL_CFG_4TH) {
		if(pMsgInfo->size == RADIO__MSGCMCFG4_SIZE  && serialNoMatch == TRUE){
			/*
			 * Fourth package.
			 */
			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_CID_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_CID_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_CID_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_CID_L]);
			if(u32TempVar != 0)
			{
				status = reg_putLocal(&u32TempVar, reg_i_ChargerId,
						pInst->parent.parent.instId);
			}

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_BBCFUNC],
					radio__hBBC_func);
			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_BBCVAR],
					radio__hBBC_var);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_EQUFUNC],
					radio__hEqualize_func);
			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_EQUVAR],
					radio__hEqualize_var);
			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_EQUVAR2],
					radio__hEqualize_var2);

			status |= reg_putLocal(&pMsgInfo->pData[RADIO__MSGCMCFG4_ROUTING],
					reg_i_Routing, pInst->parent.parent.instId);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_BITCONFIG],
					radio__hBitConfig);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_REMOTEOUTBBCVAR],
					radio__hRemoteOutBBC_var);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_CANBPS],
					radio__hCANBps);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_ECFUNC],
					radio__hExtraCharge_func);
			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_ECVAR],
					radio__hExtraCharge_var);
			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_ECVAR2],
					radio__hExtraCharge_var2);
			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_ECVAR3],
					radio__hExtraCharge_var3);

			{
				Uint32 Time;
				Uint8 Hour;
				Uint8 Min;

				Hour = pMsgInfo->pData[RADIO__MSGCMCFG4_ECVAR2];
				Min = pMsgInfo->pData[RADIO__MSGCMCFG4_ECVAR3];

				// Convert hour and minutes to seconds (for posix comparison)
				Time = (Uint32)Hour * 3600UL + (Uint32)Min * 60UL;

				reg_put(&Time, radio__hECTime_write);
			}

			/*
			 * OBS! Only applicable for Access 30 (SW 11617017)
			 * RADIO__MSGCMCFG4_HWTYPE
			 * RADIO__MSGCMCFG4_HWVER
			 */

			u32TempVar
					= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_DPLPACTOT_H])
							<< 24;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_DPLPACTOT_HM])
							<< 16;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_DPLPACTOT_LM])
							<< 8;
			u32TempVar
					|= ((Uint32) pMsgInfo->pData[RADIO__MSGCMCFG4_DPLPACTOT_L]);
			status |= reg_put(&u32TempVar, radio__hDplPowerLimitTotal);

			/*
			 * Spare registers is not handled
			 * RADIO__MSGCMCFG4_U32SPARE4
			 */

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG4_BATTERYTEMP_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG4_BATTERYTEMP_L]);
			status |= reg_put(&u16TempVar, radio__hDefBatteryTemp);

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG4_AIRPUMPLOW_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG4_AIRPUMPLOW_L]);
			status |= reg_put(&u16TempVar, radio__AirPumpAlarmLow);

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG4_AIRPUMPHIGH_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMCFG4_AIRPUMPHIGH_L]);
			status |= reg_put(&u16TempVar, radio__AirPumpAlarmHigh);

			/*
			 * RADIO__MSGCMCFG4_U16SPARE4
			 */

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_CANNODEID],
					radio__hCANNodeId);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_DPLFUNC],
					radio__hPowerGroup_func);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_DPLPRIO],
					radio__hDplPriorityFactor);

			status |= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_DPLPACDEF],
					radio__hDplPacLimit_default);

			/*status
					|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_ITHRESHOLDVAR], radio__hIthreshold_var); 	// sorabITR - not now
			status
					|= reg_put(&pMsgInfo->pData[RADIO__MSGCMCFG4_ITHRESHOLDCOND], radio__hIthreshold_cond); // sorabITR - not now
			*/

			/*
			 * Build a response message.
			 */
			radio__acBuildAck(pInst, RADIO__ACCMD_SET_CMCONFIG, pMsgInfo->control);
			retVal = RADIO__ERR_PROCOK_RESP;

			/*
			 * Trigger delayd acknowledge with status = 0
			 */
			pInst->parent.parent.ackCntr = 2;
			pInst->parent.parent.ackDstAddr = pMsgInfo->address;
			pInst->parent.parent.ackSeqNum = pMsgInfo->seqNum;
			pInst->parent.parent.ackCmd = RADIO__ACCMD_SET_CMCONFIG;
			pInst->parent.parent.ackStatus = RADIO__MSGHCTRL_CFG_DONE;
		}
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcBMCaPack
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process CaPack message.
*
*	\param		pInst	Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message. Should be filled with
*							information about the response message.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcBMCaPack(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	reg_FileIo fileIo;

	if (pMsgInfo->control & RADIO__MSGHCTRL_FIRST) {
		/*
		 * First segment. Reset the byte counter.
		 */

		pInst->byteCount = 0;
	}

	/*
	 * Store the received segment to REG.
	 */

	fileIo.ioBuffer = &pMsgInfo->pData[RADIO__MSGBMCP_CAPACK];
	fileIo.offset = pInst->byteCount;
	fileIo.x.length = pMsgInfo->size;
	reg_put(&fileIo, radio__hCaPack);

	if (pMsgInfo->control == RADIO__MSGHCTRL_LAST) {
		Boolean sentOk;

		/*
		 * This is the last package. Send a CaPack received event.
		 */

		pInst->byteCount = 0;

		sentOk = msg_sendTry(MSG_LIST(statusRep), PROJECT_MSGT_STATUSREP,
				SUP_STAT_CHALG_RECEIVED);

		if(!sentOk)
		{
			Uint8 AssertId = 4;
			reg_put(&AssertId, radio__EvtAssert);
		}
		//deb_assert(sentOk == TRUE);
	} else {
		pInst->byteCount += pMsgInfo->size;
	}

	return (RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcBmMeas
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process BM measurements message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcBmMeas(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	Uint32 u32tmpVar;
	Uint16 u16tmpVar;
	Int16 i16tmpVar;
	Uint16 BMNwkId;
	Uint8 u8tmpVar;
	reg_Status status;
	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	status = REG_OK;

	if(pMsgInfo->size == RADIO__MSGBMM_SIZE){
		/*
		 * First just verifying that the message is sent from the battery that
		 * is connected to the charger.
		 */
		reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->parent.parent.instId);

		if (pMsgInfo->address != BMNwkId) {
			/*
			 * The message is from some other battery. Do not store
			 * the values to REG. Two chargers have same address.
			 * Set error for address conflict.
			 */
			msg_sendTry(
				MSG_LIST(statusRep),
				PROJECT_MSGT_STATUSREP,
				SUP_STAT_ADDRESS_CONFLICT
			);

			retVal = RADIO__ERR_PROCOK;
		}
		else
		{
			/*
			 * Read and store data from BMU
			 */
			u8tmpVar = ((Uint8) pMsgInfo->pData[RADIO__MSGBMM_UNITSCALE]);

			i16tmpVar = ((Int16) pMsgInfo->pData[RADIO__MSGBMM_TBATT_H]) << 8;
			i16tmpVar |= ((Int16) pMsgInfo->pData[RADIO__MSGBMM_TBATT_L]);
			if(u8tmpVar & 1<<0){	// Scale = Fahrenheit
				i16tmpVar = (5*(i16tmpVar - 320))/9;
			}
			status |= reg_putLocal(&i16tmpVar, reg_i_BmTbatt, pInst->parent.parent.instId);

			u32tmpVar = ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_IBATT_H]) << 24;
			u32tmpVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_IBATT_HM]) << 16;
			u32tmpVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_IBATT_LM]) << 8;
			u32tmpVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_IBATT_L]);
			status |= reg_putLocal(&u32tmpVar, reg_i_BmIbatt,
					pInst->parent.parent.instId);

			u32tmpVar = ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_UBATT_H]) << 24;
			u32tmpVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_UBATT_HM]) << 16;
			u32tmpVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_UBATT_LM]) << 8;
			u32tmpVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMM_UBATT_L]);
			status |= reg_putLocal(&u32tmpVar, reg_i_BmUbatt,
					pInst->parent.parent.instId);

			u8tmpVar = ((Uint8) pMsgInfo->pData[RADIO__MSGBMM_SOC]);
			status |= reg_putLocal(&u8tmpVar, reg_i_BmSOC,
					pInst->parent.parent.instId);

			u16tmpVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMM_ERROR_H]) << 8;
			u16tmpVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMM_ERROR_L]);
			status |= reg_putLocal(&u16tmpVar, reg_i_BmError,
					pInst->parent.parent.instId);

			if(u16tmpVar & RADIO_BMERROR_TIME)													// Trigger BMU set time message
				radio__triggerBmuSetTime(&pInst->parent.parent);

			if(u16tmpVar & RADIO_BMERROR_ADDRESS_CONFLICT){
				/*
				 * Two BMUs have same address.
				 * Set error for BMU address conflict.
				 */
				msg_sendTry(
					MSG_LIST(statusRep),
					PROJECT_MSGT_STATUSREP,
					SUP_STAT_BM_ADDRESS_CONFLICT
				);
			}
			//RADIO__MSGBMM_UCBATT_H
			//RADIO__MSGBMM_UCBATT_HM
			//RADIO__MSGBMM_UCBATT_LM
			//RADIO__MSGBMM_UCBATT_L


			u16tmpVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMM_STATUS_H]) << 8;
			u16tmpVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMM_STATUS_L]);
			status |= reg_putLocal(&u16tmpVar, reg_i_BmStatus,
					pInst->parent.parent.instId);

			//RADIO__MSGBMM_UELECTROLYTE_H,
			//RADIO__MSGBMM_UELECTROLYTE_HM,
			//RADIO__MSGBMM_UELECTROLYTE_LM,
			//RADIO__MSGBMM_UELECTROLYTE_L,
			//RADIO__MSGBMM_U32SPARE1_H,
			//RADIO__MSGBMM_U32SPARE1_HM,
			//RADIO__MSGBMM_U32SPARE1_LM,
			//RADIO__MSGBMM_U32SPARE1_L,
			//RADIO__MSGBMM_U16SPARE1_H,
			//RADIO__MSGBMM_U16SPARE1_L,
			//RADIO__MSGBMM_UNITSCALE,


			if (status != REG_OK) {
				retVal = RADIO__ERR_ERROR;
			} else {
				retVal = RADIO__ERR_PROCOK;
			}
		}
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcBMData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process BMBatteryData message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcBmBatteryData(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	radio__ErrCode retVal;
	reg_Status status;
	Uint32 u32TempVar;
	Uint16 u16TempVar;
	Uint16 BmNwkId;
	Uint16 currAlgNo;
	Uint8 BmInvalidParam;
	Uint8 SupState;
	Uint8 joinEnable;

	status = 0;
	retVal = RADIO__ERR_ERROR;
	BmInvalidParam = FALSE;

	if(pMsgInfo->size == RADIO__MSGBMDA_SIZE){
		reg_get(&SupState, radio__hSupState);
		reg_getLocal(&BmNwkId, reg_i_BMnwkId, pInst->parent.parent.instId);

		if(SupState < SUP_STATE_INIT_BM){
			/*
			 * The message is not intended for this charger.
			 * Two chargers have same address.
			 * Set error for address conflict.
			 */
			msg_sendTry(
				MSG_LIST(statusRep),
				PROJECT_MSGT_STATUSREP,
				SUP_STAT_ADDRESS_CONFLICT
			);

		}
		else if((SupState == SUP_STATE_INIT_BM) || (SupState == SUP_STATE_INIT_BM_2)){
			if(BmNwkId == 0){
				BmNwkId = pMsgInfo->address;
				reg_putLocal(&BmNwkId, reg_i_BMnwkId, pInst->parent.parent.instId);

				/*
				 * Process the message data.
				 */

				u32TempVar = ((Uint32) pMsgInfo->pData[RADIO__MSGBMDA_BID_H]) << 24;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMDA_BID_HM]) << 16;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMDA_BID_LM]) << 8;
				u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO__MSGBMDA_BID_L]);
				status |= reg_put(&u32TempVar, radio__hBID);


				{	// Get battery text string
					Uint8 BatteryText[9];	// First byte of the string is length
					int i;
					int len = 0;

					for(i = RADIO__MSGBMDA_BATTERYTEXT_0; i <= RADIO__MSGBMDA_BATTERYTEXT_7; i++) {
						if(pMsgInfo->pData[i] == '\0')
							break;
						else{
							len++;
							BatteryText[len] = pMsgInfo->pData[i];
						}
					}

					BatteryText[0] = len;	// Set length of text string

					status |= reg_put(&BatteryText, radio__hCurrBidTag);
				}

				status |= reg_put(&pMsgInfo->pData[RADIO__MSGBMDA_BATT_TYPE], radio__hcurrBatteryType);

				currAlgNo = pMsgInfo->pData[RADIO__MSGBMDA_ALGNO_H] << 8;
				currAlgNo |= pMsgInfo->pData[RADIO__MSGBMDA_ALGNO_L];
				status |= reg_put(&currAlgNo, radio__hCurrAlgNo);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_ALGNOVER_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_ALGNOVER_L]);
				status |= reg_put(&u16TempVar, radio__hcurrVersion);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_CAP_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_CAP_L]);
				status |= reg_put(&u16TempVar, radio__hCurrCapacity);
				if(u16TempVar < 1)
					BmInvalidParam = TRUE;	// trigger invalid param alarm

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_CELLS_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_CELLS_L]);
				status |= reg_put(&u16TempVar, radio__hCurrCell);
				if(u16TempVar < 1)
					BmInvalidParam = TRUE;	// trigger invalid param alarm

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_CABLERES_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_CALBERES_L]);
				status |= reg_put(&u16TempVar, radio__hCurrCableResBm);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_BASELOAD_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_BASELOAD_L]);
				status |= reg_put(&u16TempVar, radio__hCurrBaseLoad);

				u16TempVar = ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_STATUS_H]) << 8;
				u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO__MSGBMDA_STATUS_L]);
				status |= reg_putLocal(&u16TempVar, reg_i_BmStatus,
						pInst->parent.parent.instId);

				// Check if AlgNo present and if so set as active
				if(chalg_UpdateAlgIdxFromBm(currAlgNo) == FALSE)
					BmInvalidParam = TRUE;	// trigger invalid param alarm

				/* signal invalid param alarm or not */
				reg_putLocal(&BmInvalidParam, reg_i_BmInvalidParam, pInst->parent.parent.instId);

				// Turn off Join enable if enabled
				reg_getLocal(&joinEnable, reg_i_joinEnable, pInst->parent.parent.instId);
				if(joinEnable){
					joinEnable = 0;
					reg_putLocal(&joinEnable, reg_i_joinEnable, pInst->parent.parent.instId);
				}

				if (status == REG_OK) {
					msg_sendTry(MSG_LIST(statusRep), PROJECT_MSGT_STATUSREP,
								SUP_STAT_BDATA_RECEIVED);
								retVal = RADIO__ERR_PROCOK;
				}
			}
		}
		else{
			if(pMsgInfo->address != BmNwkId){
				/*
				 * The message is from some other battery.
				 * Two chargers have same address.
				 * Set error for address conflict.
				 */
				msg_sendTry(
					MSG_LIST(statusRep),
					PROJECT_MSGT_STATUSREP,
					SUP_STAT_ADDRESS_CONFLICT
				);
			}
		}
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcBMConfig
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process BMConfig message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info. Contains information about
*							the incoming message.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcBmConfig(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	radio__ErrCode retVal;
	Uint32 u32TempVar;
	Uint16 u16TempVar;
	Uint8 u8TempVar;
	Uint8 setBmData;

	retVal = RADIO__ERR_ERROR;

	if(pMsgInfo->size == RADIO_MSGBMCONFIG_SIZE)
	{
		Uint16 BMNwkId;

		// Get Bm node Id
		reg_getLocal(&BMNwkId, reg_i_BMnwkId, pInst->parent.parent.instId);

		// Check if message is from correct BMU
		if(pMsgInfo->address == BMNwkId)
		{
			/*
			 * Process the message data.
			 */

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_PANID_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_PANID_L]);
			sMsgConfig.panId = u16TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_CHANNEL]);
			sMsgConfig.channel = u8TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_NODEADDR_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_NODEADDR_L]);
			sMsgConfig.nodeAddr = u16TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_NWKSETTINGS]);
			sMsgConfig.nwkSettings = u8TempVar;

			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_SERIALNO_H]) << 24;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_SERIALNO_HM]) << 16;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_SERIALNO_LM]) << 8;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_SERIALNO_L]);
			sMsgConfig.serialNo = u32TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_BMTYPE]);
			sMsgConfig.bmType = u8TempVar;

			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_BID_H]) << 24;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_BID_HM]) << 16;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_BID_LM]) << 8;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_BID_L]);
			sMsgConfig.bid = u32TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_EQUFUNC]);
			sMsgConfig.equFunc = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_EQUPARAM]);
			sMsgConfig.equParam = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_WATERFUNC]);
			sMsgConfig.waterFunc = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_WATERPARAM]);
			sMsgConfig.waterParam = u8TempVar;

			// Get battery manufacturer Id text string
			for(int i = 0; i < 8; i++) {
				sMsgConfig.bmfgId[i] = pMsgInfo->pData[RADIO_MSGBMCONFIG_BMFGID_HHH + i];
			}

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CELLS_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CELLS_L]);
			sMsgConfig.cells = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CAPACITY_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CAPACITY_L]);
			sMsgConfig.capacity = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CYCLESAVAILABLE_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CYCLESAVAILABLE_L]);
			sMsgConfig.cyclesAvailable = u16TempVar;

			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_AHAVAILABLE_H]) << 24;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_AHAVAILABLE_HM]) << 16;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_AHAVAILABLE_LM]) << 8;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_AHAVAILABLE_L]);
			sMsgConfig.ahAvailable = u32TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_BATTERYTYPE]);
			sMsgConfig.batteryType = u8TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_BASELOAD_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_BASELOAD_L]);
			sMsgConfig.baseload = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CABLERES_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CABLERES_L]);
			sMsgConfig.cableRes = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_ALGNO_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_ALGNO_L]);
			sMsgConfig.algNo = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_U16SPARE5_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_U16SPARE5_L]);
			sMsgConfig.u16Spare5 = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_BBCGROUP_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_BBCGROUP_L]);
			sMsgConfig.bbcGroup = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_INSTPERIOD_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_INSTPERIOD_L]);
			sMsgConfig.instPeriod = u16TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_CELLTAP]);
			sMsgConfig.cellTap = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_VBL]);
			sMsgConfig.voltageBalanceLimit = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_ACIDSENSOR]);
			sMsgConfig.acidSensor = u8TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_TMAX_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_TMAX_L]);
			sMsgConfig.tMax = u16TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_ROUTFUNC]);
			sMsgConfig.remoteOutFunc = u8TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_DEL_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_DEL_L]);
			sMsgConfig.dEL = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CEL_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_CEL_L]);
			sMsgConfig.cEL = u16TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_CEFFICIENCY]);
			sMsgConfig.cEfficiency = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_CLEARSTATISTICS]);
			sMsgConfig.clearStatistics = u8TempVar;

			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE1_H]) << 24;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE1_HM]) << 16;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE1_LM]) << 8;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE1_L]);
			sMsgConfig.u32Spare1 = u32TempVar;

			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE2_H]) << 24;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE2_HM]) << 16;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE2_LM]) << 8;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE2_L]);
			sMsgConfig.u32Spare2 = u32TempVar;

			u32TempVar = ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE3_H]) << 24;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE3_HM]) << 16;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE3_LM]) << 8;
			u32TempVar |= ((Uint32) pMsgInfo->pData[RADIO_MSGBMCONFIG_U32SPARE3_L]);
			sMsgConfig.u32Spare3 = u32TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_SECURITYCODE1_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_SECURITYCODE1_L]);
			sMsgConfig.securityCode1 = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_SECURITYCODE2_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_SECURITYCODE2_L]);
			sMsgConfig.securityCode2 = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_U16SPARE3_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_U16SPARE3_L]);
			sMsgConfig.u16Spare3 = u16TempVar;

			u16TempVar = ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_U16SPARE4_H]) << 8;
			u16TempVar |= ((Uint16) pMsgInfo->pData[RADIO_MSGBMCONFIG_U16SPARE4_L]);
			sMsgConfig.u16Spare4 = u16TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_BMBITCONFIG]);
			sMsgConfig.bmBitConfig = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_U8SPARE2]);
			sMsgConfig.u8Spare2 = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_U8SPARE3]);
			sMsgConfig.u8Spare3 = u8TempVar;

			u8TempVar = ((Uint8) pMsgInfo->pData[RADIO_MSGBMCONFIG_U8SPARE4]);
			sMsgConfig.u8Spare4 = u8TempVar;

			// Trigger sending of Set_BmConfig
			setBmData = 2;
			reg_putLocal(&setBmData, reg_i_SetBmData, pInst->parent.parent.instId);
		}
		else{
			/*
			 * The message is from some other battery.
			 * Two chargers have same address.
			 * Set error for address conflict.
			 */
			msg_sendTry(
				MSG_LIST(statusRep),
				PROJECT_MSGT_STATUSREP,
				SUP_STAT_ADDRESS_CONFLICT
			);
		}
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcGetStatus
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process get status message.
*
*	\param		pInst	Pointer to instance.
*	\param		pMsg	Pointer to message.
*	\param		size	Size of the message in bytes.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function will build the CMStatus message.
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcGetStatus(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	BYTE * pPayload;
	uint64_t mui = project_getMui();

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	radio__acBuildU32(pPayload, RADIO__MSGCMST_CMTYPE, radio__hEngineCode);
	(pPayload)[RADIO__MSGCMST_CMID_HHH] = (uint8_t)(mui >> 56);
	(pPayload)[RADIO__MSGCMST_CMID_HH] = (uint8_t)(mui >> 48);
	(pPayload)[RADIO__MSGCMST_CMID_H] = (uint8_t)(mui >> 40);
	(pPayload)[RADIO__MSGCMST_CMID_HM] = (uint8_t)(mui >> 32);
	(pPayload)[RADIO__MSGCMST_CMID_LM] = (uint8_t)(mui >> 24);
	(pPayload)[RADIO__MSGCMST_CMID_L] = (uint8_t)(mui >> 16);
	(pPayload)[RADIO__MSGCMST_CMID_LL] = (uint8_t)(mui >> 8);
	(pPayload)[RADIO__MSGCMST_CMID_LLL] = (uint8_t)(mui & 0xff);
	radio__acBuildU32(pPayload, RADIO__MSGCMST_TIME, radio__hLocalTime);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_FID, radio__hFID);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_BSN, radio__hBSN);
//	reg_get(&pPayload[RADIO__MSGCMST_BMFGID], radio__hBMfgId);
//	reg_get(&pPayload[RADIO__MSGCMST_BATTERYTYPE], radio__hBatteryType);
	radio__acBuildU32(pPayload, RADIO__MSGCMST_CTIMETOTAL, radio__hChargeTimeTotal);
	radio__acBuildU32(pPayload, RADIO__MSGCMST_CAHTOTAL, radio__hChargedAhTotal);
	radio__acBuildU32(pPayload, RADIO__MSGCMST_ACWHTOTAL, radio__hAcWhTotal);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CYCLESUM1, radio__hCyclesSum1);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CYCLESUM2, radio__hCyclesSum2);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CYCLESUM3, radio__hCyclesSum3);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CYCLESUM4, radio__hCyclesSum4);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CYCLESUM5, radio__hCyclesSum5);
	reg_get(&pPayload[RADIO__MSGCMST_CHARGINGMODE], radio__currChargingMode);
	radio__acBuildU32(pPayload, RADIO__MSGCMST_BID, radio__hBID);
	
	{
		char algNoName[CHALG_ALG_NAME_LEN_MAX + 1];	// first byte is length
		int ii;
		for(ii = 0; ii < CHALG_ALG_NAME_LEN_MAX; ii++) {
			algNoName[ii] = '\0';
		}
		reg_get(algNoName,	radio__hCurrAlgName);

		pPayload[RADIO__MSGCMST_ALGNONAME_0] = (Uint8) (algNoName[1]);
		pPayload[RADIO__MSGCMST_ALGNONAME_1] = (Uint8) (algNoName[2]);
		pPayload[RADIO__MSGCMST_ALGNONAME_2] = (Uint8) (algNoName[3]);
		pPayload[RADIO__MSGCMST_ALGNONAME_3] = (Uint8) (algNoName[4]);
		pPayload[RADIO__MSGCMST_ALGNONAME_4] = (Uint8) (algNoName[5]);
		pPayload[RADIO__MSGCMST_ALGNONAME_5] = (Uint8) (algNoName[6]);
		pPayload[RADIO__MSGCMST_ALGNONAME_6] = (Uint8) (algNoName[7]);
		pPayload[RADIO__MSGCMST_ALGNONAME_7] = (Uint8) (algNoName[8]);
	}

	radio__acBuildU16(pPayload, RADIO__MSGCMST_ALGNO, radio__hCurrAlgNo);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_ALGNOVER, radio__hcurrVersion);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CAPACITY, radio__hCurrCapacity);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CABLERES, radio__hCurrCableRes);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CELL, radio__hCurrCell);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_BASELOAD, radio__hCurrBaseLoad);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CHALGERROR, radio__hChalgError);
	{
		ChargerStatusFields_type Status;

		reg_get(&Status.Sum, radio__hReguStatus);
		//radio__acBuildU16(pPayload, RADIO__MSGCMST_REGUERROR, radio__hReguError);
		pPayload[RADIO__MSGCMST_REGUERROR_H] = (BYTE)Status.Detail.Warning.Sum;
		pPayload[RADIO__MSGCMST_REGUERROR_L] = (BYTE)Status.Detail.Error.Sum;
		//radio__acBuildU16(pPayload, RADIO__MSGCMST_REGUSTAT, radio__hReguStatus);
		pPayload[RADIO__MSGCMST_REGUSTAT_H] = (BYTE)Status.Detail.Info.Sum;
		pPayload[RADIO__MSGCMST_REGUSTAT_L] = (BYTE)Status.Detail.Derate.Sum;
	}
	radio__acBuildU16(pPayload, RADIO__MSGCMST_CHALGSTAT, radio__hChalgStatus);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_PAC, radio__hPac);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_UCHALG, radio__hUchalg);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_ICHALG, radio__hIchalg);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_TBOARD, radio__hTboard);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_THS, radio__hThs);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_HISTLOGIDX, radio__hHistlogIndexReset);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_EVTLOGIDX, radio__hEvtlogIndexReset);
//	radio__acBuildU32(pPayload, RADIO__MSGCMST_INSTLOGIDX, radio__hInstlogIndexReset);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_ACOVP, radio__hOverVoltCnt);
	// Spare data
	radio__acBuildU32(pPayload, RADIO__MSGCMST_U32SPARE1, radio__hU32Spare);
	radio__acBuildU32(pPayload, RADIO__MSGCMST_U32SPARE2, radio__hU32Spare);
	radio__acBuildU16(pPayload, RADIO__MSGCMST_U16SPARE1, radio__hU16Spare);

	pInst->parent.parent.usedTxBuff = RADIO__MSGCMST_SIZE;

	radio__acBuildHeader(pInst, RADIO__ACCMD_CMSTATUS,
		pInst->normMessage.seqNum, RADIO__MSGHCTRL_SINGLE);
	pInst->pLastMsg = &pInst->normMessage;

	return (RADIO__ERR_PROCOK_RESP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcGetLog
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Build log message on the buffer.
*
*	\param		pInst	Pointer to instance.
*	\param		pMsg	Pointer to message.
*	\param		size	Size of the message in bytes.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcGetLog(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {

	radio__AcProcFn *ProcFn = NULL;
	Uint32 logIndexTot = 0;
	Boolean RecordIsValid = FALSE;
	radio__ErrCode retVal;

	retVal = RADIO__ERR_ERROR;

	/*
	 * record count has to be stored so that RADIO remembers to send all
	 * requested records.
	 */

	pInst->recCount = pMsgInfo->pData[RADIO__MSGGLOG_NOOFRECORDS];

	/*
	 * Setup log read request.
	 */

	pInst->logRequest.flags = CM_REQFLG_READ;
	pInst->logRequest.pDoneFn = &radio__logReqDoneFn;
	pInst->logRequest.pRecord = pInst->logBuff;
	pInst->logRequest.pUtil = pInst;
	pInst->logRequest.size = 0;	// set size of read record to zero
	pInst->parent.parent.usedSegBuff = 0;

	pInst->logRequest.logType = pMsgInfo->pData[RADIO__MSGGLOG_LOGTYPE];

	if (pInst->logRequest.logType <= RADIO__LOGTYPE_INSTANT) {
		pInst->logRequest.index
				= ((Uint32) pMsgInfo->pData[RADIO__MSGGLOG_RECORDNO_H]) << 24;
		pInst->logRequest.index
				|= ((Uint32) pMsgInfo->pData[RADIO__MSGGLOG_RECORDNO_HM]) << 16;
		pInst->logRequest.index
				|= ((Uint32) pMsgInfo->pData[RADIO__MSGGLOG_RECORDNO_LM]) << 8;
		pInst->logRequest.index
				|= ((Uint32) pMsgInfo->pData[RADIO__MSGGLOG_RECORDNO_L]);
		pInst->recordNumber = pInst->logRequest.index;
		pInst->recordOffset = 0;


		switch (pInst->logRequest.logType) {
			case RADIO__LOGTYPE_HISTORICAL:
				ProcFn = &radio__acBuildHistLog;
				reg_get(&logIndexTot, radio__hHistlogIndex );
				break;
			case RADIO__LOGTYPE_EVENT:
				ProcFn = &radio__acBuildEventLog;
				reg_get(&logIndexTot, radio__hEvtlogIndex );
				break;
			case RADIO__LOGTYPE_INSTANT:
				ProcFn = &radio_acProcInstantLog;
				reg_get(&logIndexTot, radio__hInstlogIndex );
				break;
		}

		/*
		 * Compare highest record index request with highest index in log
		 * No need continue process if incorrect record index request
		 */
		if (pInst->recCount && pInst->logRequest.index) {
			if ((pInst->logRequest.index + (pInst->recCount - 1)) <= logIndexTot) {
				RecordIsValid = TRUE;

				pInst->segmentedMessage.pProcFn = ProcFn;
				cm_processRequest(&pInst->logRequest);
				retVal = RADIO__ERR_PROCOK;
			}
		}
	}

//	else
	if (!RecordIsValid) {
		/*
		 * The logType or index could not be found.
		 */
		BYTE * pPayload;
		Uint8 control;

		// Get pointer to payload
		pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
		pPayload += RADIO__MSGH_DATA;
		// Set log type
		pPayload[RADIO__MSGCMLOG_LOGTYPE] = 0xFF;
		// set payload offset to point at first element of specific Event
		pPayload += RADIO__MSGCMLOG_LOGDATA;
		// increase payload size
		pInst->parent.parent.usedTxBuff = 1;

		control = RADIO__MSGHCTRL_SINGLE;
		retVal = RADIO__ERR_PROCOK_RESP;

		radio__acBuildHeader(pInst, RADIO__ACCMD_CMLOG,
			pInst->segmentedMessage.seqNum, control);
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->segmentedMessage;
	}

	return (retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcGetInfo
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process get info message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function will build the CmInfo message.
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcGetInfo(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	BYTE * pPayload;
	Uint32 u32TempVar;
	Uint16 u16TempVar;
	Uint8 u8TempVar;
	uint64_t mui = project_getMui();

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

//	radio__acBuildU32(pPayload, RADIO__MSGCMINF_SERIALNO, radio__hSerialNo);
	reg_getLocal(&u32TempVar, reg_i_ChargerId, pInst->parent.parent.instId);
	(pPayload)[RADIO__MSGCMINF_CID_H] = (BYTE) (u32TempVar >> 24);
	(pPayload)[RADIO__MSGCMINF_CID_HM] = (BYTE) (u32TempVar >> 16);
	(pPayload)[RADIO__MSGCMINF_CID_LM] = (BYTE) (u32TempVar >> 8);
	(pPayload)[RADIO__MSGCMINF_CID_L] = (BYTE) u32TempVar;
	radio__acBuildU16(pPayload, RADIO__MSGCMINF_HISTLOGINMEM, radio__hHistLogInMem);
	radio__acBuildU32(pPayload, RADIO__MSGCMINF_HISTLOGIDX, radio__hHistlogIndex);
	radio__acBuildU32(pPayload, RADIO__MSGCMINF_CTTIME, radio__hLatestHistLogTime);
	radio__acBuildU16(pPayload, RADIO__MSGCMINF_EVTLOGINMEM, radio__hEvtLogInMem);
	radio__acBuildU32(pPayload, RADIO__MSGCMINF_EVTLOGIDX, radio__hEvtlogIndex);
	radio__acBuildU32(pPayload, RADIO__MSGCMINF_EVTLOGTIME, radio__hLatestEvtLogTime);
	radio__acBuildU16(pPayload, RADIO__MSGCMINF_MOMLOGINMEM, radio__hInstLogInMem);
	radio__acBuildU32(pPayload, RADIO__MSGCMINF_MOMLOGIDX, radio__hInstlogIndex);
	radio__acBuildU16(pPayload, RADIO__MSGCMINF_CHALGERRSUM, radio__hChalgErrorSum);
	{
		ChargerStatusFields_type Status;

		reg_get(&Status.Sum, radio__hReguStatusSum);
		//radio__acBuildU16(pPayload, RADIO__MSGCMINF_REGUERRSUM, radio__hReguErrorSum);
		pPayload[RADIO__MSGCMINF_REGUERRSUM_H] = (BYTE)Status.Detail.Warning.Sum;
		pPayload[RADIO__MSGCMINF_REGUERRSUM_L] = (BYTE)Status.Detail.Error.Sum;
	}

	(pPayload)[RADIO__MSGCMINF_CMID_HHH] = (uint8_t)(mui >> 56);
	(pPayload)[RADIO__MSGCMINF_CMID_HH] = (uint8_t)(mui >> 48);
	(pPayload)[RADIO__MSGCMINF_CMID_H] = (uint8_t)(mui >> 40);
	(pPayload)[RADIO__MSGCMINF_CMID_HM] = (uint8_t)(mui >> 32);
	(pPayload)[RADIO__MSGCMINF_CMID_LM] = (uint8_t)(mui >> 24);
	(pPayload)[RADIO__MSGCMINF_CMID_L] = (uint8_t)(mui >> 16);
	(pPayload)[RADIO__MSGCMINF_CMID_LL] = (uint8_t)(mui >> 8);
	(pPayload)[RADIO__MSGCMINF_CMID_LLL] = (uint8_t)(mui & 0xff);

	reg_get(&u32TempVar, radio__FirmwareVerMain);
	(pPayload)[RADIO__MSGCMINF_FWVERSION_H] = (BYTE) (u32TempVar >> 24);
	(pPayload)[RADIO__MSGCMINF_FWVERSION_HM] = (BYTE) (u32TempVar >> 16);
	(pPayload)[RADIO__MSGCMINF_FWVERSION_LM] = (BYTE) (u32TempVar >> 8);
	(pPayload)[RADIO__MSGCMINF_FWVERSION_L] = (BYTE) u32TempVar;
	
	// fill Spare registers with zero
	u16TempVar = 0;
	(pPayload)[RADIO__MSGCMINF_U16SPARE1_H] = (BYTE) (u16TempVar >> 8);
	(pPayload)[RADIO__MSGCMINF_U16SPARE1_L] = (BYTE) u16TempVar;

	u8TempVar = 0;
	(pPayload)[RADIO__MSGCMINF_U8SPARE1] = (BYTE) u8TempVar;

	pInst->parent.parent.usedTxBuff = RADIO__MSGCMINF_SIZE;

	radio__acBuildHeader(pInst, RADIO__ACCMD_CMINFO, pInst->normMessage.seqNum,
			RADIO__MSGHCTRL_SINGLE
	);

	pInst->pLastMsg = &pInst->normMessage;
	
	return (RADIO__ERR_PROCOK_RESP);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcGetUserParams
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process get info message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function will build the UserParam message.
*
*	\note
*
*******************************************************************************/
/*PRIVATE radio__ErrCode radio__acProcGetUserParams(radio__AcInst * pInst, // sorabtest magnus J
		radio__AcProc * pMsgInfo) {
	BYTE * pPayload;
	Uint16 algId;
	bool validParams = true;
	BYTE nOfIndex=0;
	BYTE errorCode=0;


	enum CTRL {NoOFINDEX, GETPARAM, GETALG, ERROR};

	//Requested index
	uint32_t index = (uint32_t) pMsgInfo->pData[RADIO__MSGCMUSP_INDEX];
	//What to do in ctrl (Get number of index, Get parameters or Get current charging alg.)
	uint32_t ctrl = (uint32_t) pMsgInfo->pData[RADIO__MSGCMUSP_INDEX+1];

	AlgDef_ConstPar_Type* curveData;
//	Identity_ConstPar_Type* curveInfo;
	Mandatory_ConstPar_Type* mandatory;
	UserParam_ConstParam_Type* parameters;

	//Get charging curve Id
	reg_get(&algId, radio__algIdx_default);
	algId = chalg_GetAlgNo(algId);

	switch (ctrl){
		case NoOFINDEX :
			//Check if chalg has curve data
			if (!chalg_UserParamIsValid(algId))
			{
				nOfIndex=0;
				break;
			}
			//If any curve data, Get it
			if (!chalg_GetCurveData(algId, (void**)&curveData)) {
				nOfIndex=0;
			}
			else
			{
				mandatory = (Mandatory_ConstPar_Type*)curveData->Mandatory_ConstPar_Addr;
				nOfIndex = (BYTE)mandatory->UserParam.Size;
			}

			break;
		case GETPARAM :
			if (!chalg_UserParamIsValid(algId)) //Check if chalg has curve data
			{
				ctrl = ERROR;	//send error 0xFF
				errorCode = 0xFF;
				break;
			}
			//Any curve data?
		if (!chalg_GetCurveData(algId, (void**)&curveData)) {		//If any curve data
				//No curve data send error 0xFF
				ctrl = ERROR;
				errorCode = 0xFF;
				break;
			}

			//Index out of range?
			mandatory = (Mandatory_ConstPar_Type*)curveData->Mandatory_ConstPar_Addr;
			nOfIndex = (BYTE)mandatory->UserParam.Size;
			if (index > (uint32_t)nOfIndex-1)
			{
				ctrl = ERROR;
				errorCode = 0xFF;																//??? Unique error code for index to big ?
			}

			break;
		case GETALG :
			if (!validParams)
			{
				ctrl = ERROR;	//send error 0xFF
				errorCode = 0xFF;
			}
			break;
		default : //ctrl = unknown
			ctrl = ERROR;	//send error 0xFF
			errorCode = 0xFF;
			break;
	}


	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	if (ctrl == GETPARAM){
		mandatory = (Mandatory_ConstPar_Type*)curveData->Mandatory_ConstPar_Addr;
		parameters = (UserParam_ConstParam_Type*)mandatory->UserParam.Addr;

		 //Index
		 (pPayload)[RADIO__MSGCMUSP_INDEX] = (BYTE) index;

		 //Type
		 (pPayload)[RADIO__MSGCMUSP_TYPE] = (BYTE) parameters[index].Unit;


		 float					fTmp;
		 Uint8 *				pFloat;

		 //Value
		 reg_aGet(&fTmp, radio__UserParam, index);
		 pFloat = (void *)&fTmp;
		 (pPayload)[RADIO__MSGCMUSP_VALUE_H] = (BYTE) pFloat[3];
		 (pPayload)[RADIO__MSGCMUSP_VALUE_HM] = (BYTE) pFloat[2];
		 (pPayload)[RADIO__MSGCMUSP_VALUE_LM] = (BYTE) pFloat[1];
		 (pPayload)[RADIO__MSGCMUSP_VALUE_L] = (BYTE) pFloat[0];

		 //MinValue
		 fTmp = (float)parameters[index].Min;
		 pFloat = (void *)&fTmp;
		 (pPayload)[RADIO__MSGCMUSP_MIN_H] = (BYTE) pFloat[3];
		 (pPayload)[RADIO__MSGCMUSP_MIN_HM] = (BYTE) pFloat[2];
		 (pPayload)[RADIO__MSGCMUSP_MIN_LM] = (BYTE) pFloat[1];
		 (pPayload)[RADIO__MSGCMUSP_MIN_L] = (BYTE) pFloat[0];

		 //MaxValue
		 fTmp = (float)parameters[index].Max;
		 pFloat = (void *)&fTmp;
		 (pPayload)[RADIO__MSGCMUSP_MAX_H] = (BYTE) pFloat[3];
		 (pPayload)[RADIO__MSGCMUSP_MAX_HM] = (BYTE) pFloat[2];
		 (pPayload)[RADIO__MSGCMUSP_MAX_LM] = (BYTE) pFloat[1];
		 (pPayload)[RADIO__MSGCMUSP_MAX_L] = (BYTE) pFloat[0];

		 //Default Value
		 fTmp = (float)parameters[index].Default;
		 pFloat = (void *)&fTmp;
		 (pPayload)[RADIO__MSGCMUSP_DEFAULT_H] = (BYTE) pFloat[3];
		 (pPayload)[RADIO__MSGCMUSP_DEFAULT_HM] = (BYTE) pFloat[2];
		 (pPayload)[RADIO__MSGCMUSP_DEFAULT_LM] = (BYTE) pFloat[1];
		 (pPayload)[RADIO__MSGCMUSP_DEFAULT_L] = (BYTE) pFloat[0];

		 //Name Size
		 (pPayload)[RADIO__MSGCMUSP_NAMESIZE] = (BYTE) parameters[index].NameSize;

		 //Parameter Name
		 for (BYTE i=0; i < parameters[index].NameSize; i++)
		 {
			 (pPayload)[RADIO__MSGCMUSP_NAME_I+i] = parameters[index].Name[i];
		 }

		pInst->parent.parent.usedTxBuff = RADIO__MSGCMUSP_SIZE + parameters[index].NameSize-1;

	}
	else if (ctrl == GETALG){//Get current Alg Id
		(pPayload)[RADIO__MSGCMUSP_INDEX] = (BYTE)(algId >> 8);
		(pPayload)[RADIO__MSGCMUSP_INDEX+1] = (BYTE)algId;

		pInst->parent.parent.usedTxBuff = RADIO__MSGCMUSP_INDEX + 2;
	}
	else if (ctrl == NoOFINDEX){//Get number of index
		(pPayload)[RADIO__MSGCMUSP_INDEX] = nOfIndex;
		pInst->parent.parent.usedTxBuff = RADIO__MSGCMUSP_INDEX + 1;
	}
	else{ //ERROR
		(pPayload)[RADIO__MSGCMUSP_INDEX] = errorCode; //(BYTE)mandatory->UserParam.Size;
		pInst->parent.parent.usedTxBuff = RADIO__MSGCMUSP_INDEX + 1;
	}


	radio__acBuildHeader(pInst, RADIO__ACCMD_CMUSERPARAMS, pInst->normMessage.seqNum,
			RADIO__MSGHCTRL_SINGLE
	);

	pInst->pLastMsg = &pInst->normMessage;

	return (RADIO__ERR_PROCOK_RESP);
}*/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcSetUserParams
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process set user parameter message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function will build the UserParam message.
*
*	\note
*
*******************************************************************************/

/*PRIVATE radio__ErrCode radio__acProcSetUserParams(radio__AcInst * pInst, // sorabtest magnus J
		radio__AcProc * pMsgInfo) {
	BYTE * pPayload;

	BYTE indexAck=0;

	uint32_t index = (uint32_t) pMsgInfo->pData[RADIO__MSGCMUSP_INDEX];

	uint32_t value = ((Uint32) pMsgInfo->pData[RADIO__MSGCMUSP_INDEX+1]) << 24;
	value |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMUSP_INDEX+2]) << 16;
	value |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMUSP_INDEX+3]) << 8;
	value |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMUSP_INDEX+4]);

	//Check index
	Uint16 algId;
	BYTE nOfIndex=0;

	AlgDef_ConstPar_Type* curveData;
	Mandatory_ConstPar_Type* mandatory;

	//Get charging curve Id
	reg_get(&algId, radio__algIdx_default);
	algId = chalg_GetAlgNo(algId);

	if (!chalg_GetCurveData(algId, (void**)&curveData)) {
		//ERROR
		indexAck = 0xFF;
	}
	else
	{
		mandatory = (Mandatory_ConstPar_Type*)curveData->Mandatory_ConstPar_Addr;
		nOfIndex = (BYTE)mandatory->UserParam.Size;

		if (index > (uint32_t)nOfIndex-1)
		{
			//ERROR
			indexAck = 0xFF;
		}
	}


	 float					fTmp;
	 Uint8 *				pFloat;
	 Uint8 *				pInt;

	 pFloat = (void *)&fTmp;
	 pInt = (void *)&value;

	 pFloat[3] = pInt[3];
	 pFloat[2] = pInt[2];
	 pFloat[1] = pInt[1];
	 pFloat[0] = pInt[0];

	 reg_aPut(&fTmp, radio__UserParam, index);

	 pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	 pPayload += RADIO__MSGH_DATA;

	 project_storeUserParameter();

	 //Send ack (0 = ok, ff=failed)
	 (pPayload)[RADIO__MSGCMUSP_INDEX] = indexAck;

	 pInst->parent.parent.usedTxBuff = RADIO__MSGCMUSP_INDEX + 1;

	 radio__acBuildHeader(pInst, RADIO__ACCMD_CMUSERPARAMS, pInst->normMessage.seqNum,RADIO__MSGHCTRL_SINGLE);

	 pInst->pLastMsg = &pInst->normMessage;

	 return (RADIO__ERR_PROCOK_RESP);

}*/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcSetIdentify
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process set identify message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function start blinking LEDs.
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcSetIdentify(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {
	Uint8 u8TempVar;
	reg_Status status;
	Uint8 ackStatus;
	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGSETIDENTIFY_SIZE){

		u8TempVar = (Uint8) pMsgInfo->pData[RADIO__MSGSETIDENTIFY_MODE];
		status = reg_put(&u8TempVar, radio__IdentifyMode);

		if(pMsgInfo->address != RADIO__MACADDR_BROADCAST)
		{
			/*
			 * Build response message
			 */
			ackStatus = status == REG_OK ? 0 : 0xFF;

			radio__acBuildAck(pInst, RADIO__ACCMD_SET_IDENTIFY, ackStatus);

			retVal = RADIO__ERR_PROCOK_RESP;
		}

	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcCmDplMasterAll
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process CmDplMasterAll message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function handles CmDplMasterAll message with power limit to slaves (broadcast).
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcCmDplMasterAll(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {

	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGCMDPLMA_SIZE){
		Uint8 powerGroupCfg;
		Uint8 powerGroup;

		powerGroup = (Uint8) pMsgInfo->pData[RADIO__MSGCMDPLMA_PWRGROUP];
		powerGroupCfg = dplGetPowerGroup();

		if(powerGroup == powerGroupCfg){
			if(dplGetPowerGroupFunc() == DplSlave){
				Uint8 dplId;
				Uint16 dplMasterNwkAddr;
				Uint32 powerUnits;

				powerUnits = ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLMA_PWRUNITS_H]) << 24;
				powerUnits |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLMA_PWRUNITS_HM]) << 16;
				powerUnits |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLMA_PWRUNITS_LM]) << 8;
				powerUnits |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLMA_PWRUNITS_L]);

				dplId = dplGetDplId();

				reg_getLocal(&dplMasterNwkAddr, reg_i_dplMasterNwkAddr, pInst->parent.parent.instId);

				if(pMsgInfo->address == dplMasterNwkAddr){
					if((dplId != 0) && (powerUnits & (1 << dplId))){
						Uint16 dplPacLimit;
						Uint16 dplSlaveWatchdogCntr;

						// Set DPL power limit
						dplPacLimit = ((Uint16) pMsgInfo->pData[RADIO__MSGCMDPLMA_PWRALLOCM_H + (2 * dplId)]) << 8;
						dplPacLimit |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMDPLMA_PWRALLOCM_L + (2 * dplId)]);
						dplPutDplPacLimit(dplPacLimit);
						// set to normal mode
						dplPutMode(DplModeNormal);
						// Reset watchdog timer
						dplSlaveWatchdogCntr = TICKS_PER_SECOND * (DPL_MASTER_ALL_TIMEOUT + DPL_MSG_DELAY);
						reg_putLocal(&dplSlaveWatchdogCntr, reg_i_dplSlaveWatchdogCntr, pInst->parent.parent.instId);
						// Reset error if set
						dplNoMasterClear();
					}
					else{
						// Slave has no place in the system. Set output to zero.
						dplPutMode(DplModeOff);
					}
				}
			}
			else if(dplGetPowerGroupFunc() == DplMaster){
				// Trigger error for master conflict by writing to timeout timer
				Uint16 dplMasterConflictCntr;

				dplMasterConflictCntr = DPL_MASTER_ALL_TIMEOUT * TICKS_PER_SECOND;
				reg_putLocal(&dplMasterConflictCntr, reg_i_dplMasterConflictCntr, pInst->parent.parent.instId);
			}
		}
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcCmDplMasterSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process CmDplMasterSlave message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function handles CmDplMasterSlave message with power limit to single slave.
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcCmDplMasterSlave(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {

	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGCMDPLMS_SIZE){
		Uint8 powerGroupCfg;
		Uint8 powerGroup;

		powerGroup = (Uint8) pMsgInfo->pData[RADIO__MSGCMDPLMS_PWRGROUP];
		powerGroupCfg = dplGetPowerGroup();

		if(powerGroup == powerGroupCfg){
			if(dplGetPowerGroupFunc() == DplSlave){
				Uint8 dplIdCfg;
				Uint8 dplId;
				Uint8 ackStatus;

				// Get DPL Id
				dplId = (Uint8) pMsgInfo->pData[RADIO__MSGCMDPLMS_PWRUINT];
				dplIdCfg = dplGetDplId();

				if(dplId == 0 || (dplIdCfg == 0 && dplId > 0 && dplId < DPL_LIST_SIZE)){
					dplIdCfg = dplId;
					dplPutDplId(dplIdCfg);
				}

				if((dplId != 0) && (dplId == dplIdCfg)){
					Uint16 dplPacLimit;
					Uint16 dplSlaveWatchdogCntr;
					Uint16 dplMasterNwkAddr;

					// Set DPL power limit
					dplPacLimit = ((Uint16) pMsgInfo->pData[RADIO__MSGCMDPLMS_PWRALLOC_H]) << 8;
					dplPacLimit |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMDPLMS_PWRALLOC_L]);
					dplPutDplPacLimit(dplPacLimit);
					// Set to normal mode
					dplPutMode(DplModeNormal);
					// Reset watchdog timer
					dplSlaveWatchdogCntr = TICKS_PER_SECOND * (DPL_MASTER_ALL_TIMEOUT + DPL_MSG_DELAY);
					reg_putLocal(&dplSlaveWatchdogCntr, reg_i_dplSlaveWatchdogCntr, pInst->parent.parent.instId);
					// Set master address
					dplMasterNwkAddr = pMsgInfo->address;
					reg_putLocal(&dplMasterNwkAddr, reg_i_dplMasterNwkAddr, pInst->parent.parent.instId);
					// Reset error if set
					dplNoMasterClear();
				}
				else{
					// Slave has no place in the system. Set output to zero.
					dplPutMode(DplModeOff);
				}

				// build acknowledge as response
				ackStatus = 0;	// OK

				radio__acBuildAck(pInst, RADIO__ACCMD_CMDPLMASTERSLAVE, ackStatus);

				retVal = RADIO__ERR_PROCOK_RESP;
				pInst->pLastMsg = &pInst->normMessage;
			}
			else if(dplGetPowerGroupFunc() == DplMaster){
				// Trigger error for master conflict by writing to timeout timer
				Uint16 dplMasterConflictCntr;

				dplMasterConflictCntr = DPL_MASTER_ALL_TIMEOUT * TICKS_PER_SECOND;
				reg_putLocal(&dplMasterConflictCntr, reg_i_dplMasterConflictCntr, pInst->parent.parent.instId);
			}
		}
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcGetCmDplSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process GetCmDplSlave message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function handles GetCmDplSlave message, sent from master to trigger request of power from slave(s).
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcGetCmDplSlave(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {

	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGGETCMDPLS_SIZE){
		Uint8 powerGroupCfg;
		Uint8 powerGroup;

		powerGroup = (Uint8) pMsgInfo->pData[RADIO__MSGGETCMDPLS_PWRGROUP];
		powerGroupCfg = dplGetPowerGroup();

		if(powerGroup == powerGroupCfg){
			if(dplGetPowerGroupFunc() == DplSlave){
				Uint16 dplMasterNwkAddr;
				Uint8 dplIdCfg;
				Uint8 dplId;

				// Set master address
				dplMasterNwkAddr = pMsgInfo->address;
				reg_putLocal(&dplMasterNwkAddr, reg_i_dplMasterNwkAddr, pInst->parent.parent.instId);

				// Get Id for this node
				dplId = (Uint8) pMsgInfo->pData[RADIO__MSGGETCMDPLS_PWRUINT];
				dplIdCfg = dplGetDplId();

				if( dplId == 0xFF){
					// Reset dplId
					dplIdCfg = 0;
					dplPutDplId(dplIdCfg);
				}

				if(dplId == dplIdCfg || dplId == 0xFF){
					//Trigger sending of radio__acSendCmDplSlave() with random delay
					int randomNumber;
					Uint16 dplSlaveJitter;

					// Get random number
					randomNumber = mp_random();
					// Limit value to DPL_SLAVE_JITTER
					dplSlaveJitter = (Uint16)(randomNumber % (TICKS_PER_SECOND * DPL_SLAVE_JITTER)) + 1 * TICKS_PER_SECOND;

					reg_putLocal(&dplSlaveJitter, reg_i_dplSlaveJitterCntr, pInst->parent.parent.instId);
					pInst->parent.parent.dplSlaveTimeoutCntr = dplSlaveJitter;
				}
			}
		}
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcCmDplSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process CmDplSlave message.
*
*	\param		pInst		Pointer to instance.
*	\param		pMsgInfo	Pointer to message info struct.
*
*	\retval		RADIO__ERR_PROCOK	if the message was processed ok.
*	\retval		RADIO__ERR_ERROR	if there was a problem with the processing.
*
*	\details	This function handles CmDplSlave message sent from slave to request power.
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acProcCmDplSlave(radio__AcInst * pInst,
		radio__AcProc * pMsgInfo) {

	radio__ErrCode retVal = RADIO__ERR_PROCOK;

	if(pMsgInfo->size == RADIO__MSGCMDPLS_SIZE){
		Uint8 powerGroupCfg;
		Uint8 powerGroup;

		powerGroup = (Uint8) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRGROUP];
		powerGroupCfg = dplGetPowerGroup();

		if(powerGroup == powerGroupCfg){
			if(dplGetPowerGroupFunc() == DplMaster){
				Uint8 dplId;
				Uint8 pwrUnit;
				Uint8 soc;
				Uint8 priority;
				Uint16 pwrReq;
				Uint16 nodeId;
				Uint16 pwrAlloc;
				Uint32 pwrConsumed;

				pwrUnit = (Uint8) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRUINT];
				soc = (Uint8) pMsgInfo->pData[RADIO__MSGCMDPLS_SOC];
				priority = (Uint8) pMsgInfo->pData[RADIO__MSGCMDPLS_PRIO];
				pwrReq = ((Uint16) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRREQ_H]) << 8;
				pwrReq |= ((Uint16) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRREQ_L]);
				pwrConsumed = ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRCSUMD_H]) << 24;
				pwrConsumed |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRCSUMD_HM]) << 16;
				pwrConsumed |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRCSUMD_LM]) << 8;
				pwrConsumed |= ((Uint32) pMsgInfo->pData[RADIO__MSGCMDPLS_PWRCSUMD_L]);
				nodeId = pMsgInfo->address;

				dplId = dplUpdateSlave(nodeId, pwrUnit, soc, priority, pwrReq, pwrConsumed);

				if(pwrUnit == 0 || dplId == 0){
					if(dplId == 0){
						pwrAlloc = 0;
					}
					else{
						// Get power allocation
						pwrAlloc = dplGetPowerRequested(pwrReq);
					}

					// Send DplMasterSlave message with dplId (0 = fail 1-31 = success)
					BYTE * pPayload;

					// Get pointer to payload
					pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
					pPayload += RADIO__MSGH_DATA;
					// Set power group
					pPayload[RADIO__MSGCMDPLMS_PWRGROUP] = powerGroupCfg;
					// Set power unit/dplId
					pPayload[RADIO__MSGCMDPLMS_PWRUINT] = dplId;
					// Set power allocated
					(pPayload)[RADIO__MSGCMDPLMS_PWRALLOC_H] = (BYTE) (pwrAlloc >> 8);
					(pPayload)[RADIO__MSGCMDPLMS_PWRALLOC_L] = (BYTE) pwrAlloc;
					// fill Spare registers with zero
					// U32Spare1
					radio__acBuildU32(pPayload, RADIO__MSGCMDPLMS_U32SPARE1, radio__hU32Spare);
					// U16Spare1
					radio__acBuildU16(pPayload, RADIO__MSGCMDPLMS_U16SPARE1, radio__hU16Spare);
					// U8Spare1
					reg_get(&pPayload[RADIO__MSGCMDPLMS_U8SPARE1], radio__hU8Spare);

					// increase payload size
					pInst->parent.parent.usedTxBuff = RADIO__MSGCMDPLMS_SIZE;

					retVal = RADIO__ERR_PROCOK_RESP;

					radio__acBuildHeader(pInst, RADIO__ACCMD_CMDPLMASTERSLAVE, pInst->normMessage.seqNum,
							RADIO__MSGHCTRL_SINGLE
					);

					pInst->pLastMsg = &pInst->normMessage;
				}
				else{
					// build acknowledge as response
					Uint8 ackStatus = 0;	// OK

					radio__acBuildAck(pInst, RADIO__ACCMD_CMDPLSLAVE, ackStatus);

					retVal = RADIO__ERR_PROCOK_RESP;
					pInst->pLastMsg = &pInst->normMessage;
				}

				//trigger sending of DplMasterAll to slaves almost directly
				Uint16 dplMasterAllCntr = TICKS_PER_SECOND * 3;

				reg_putLocal(&dplMasterAllCntr, reg_i_dplMasterAllCntr, pInst->parent.parent.instId);
			}
		}

	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildHeader
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Build MPAccess OTA protocol header on buffer.
*
*	\param		pInst		Pointer to instance.
*	\param		cmd			Access OTA protocol command.
*	\param		sequence	Sequence number of the message.
*	\param		control		Control byte of the header.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void radio__acBuildHeader(radio__AcInst * pInst, Uint8 cmd,
		Uint8 sequence, Uint8 control) {
	BYTE * pPayload;

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);

	pPayload[RADIO__MSGH_CMD] = cmd;
	pPayload[RADIO__MSGH_MPSEQ] = sequence;
	pPayload[RADIO__MSGH_CONTROL] = control;

	pInst->parent.parent.usedTxBuff += RADIO__MSGH_DATA;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildCmMeas
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process get measurements message.
*
*	\param		pInst		Pointer to instance.
*	\param		seqNum		Sequence number used for the message.
*
*	\retval		RADIO__ERR_PROCOK		if the message was processed ok.
*	\retval		RADIO__ERR_ERROR		if there was a problem with the
*										processing.
*	\retval		RADIO__ERR_PROCOK_RESP	if the message was processed ok and a
*										response message has been build on the
*										buffer.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acBuildCmMeas(radio__AcInst * pInst,
		Uint8 seqNum, Uint8 command) {
	radio__ErrCode retVal;
	BYTE * pPayload;
	Uint32 u32tempVar;
	Uint16 u16tempVar;
	Uint8 u8tempVar;
	Uint8 status;

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	if(dplGetPowerGroupFunc() == DplMaster){
		u32tempVar = dplGetPowerConsumedTotal();
		status = REG_OK;
	}
	else{
		status = reg_get(&u32tempVar, radio__Smeas1s);
	}
	pPayload[RADIO__MSGCMMEAS_PAC_H] = (Uint8) (u32tempVar >> 24);
	pPayload[RADIO__MSGCMMEAS_PAC_HM] = (Uint8) (u32tempVar >> 16);
	pPayload[RADIO__MSGCMMEAS_PAC_HL] = (Uint8) (u32tempVar >> 8);
	pPayload[RADIO__MSGCMMEAS_PAC_L] = (Uint8) u32tempVar;

	if (status == REG_OK) {
		status = reg_get(&u32tempVar, radio__hUchalg);
		pPayload[RADIO__MSGCMMEAS_UCHALG_H] = (Uint8) (u32tempVar >> 24);
		pPayload[RADIO__MSGCMMEAS_UCHALG_HM] = (Uint8) (u32tempVar >> 16);
		pPayload[RADIO__MSGCMMEAS_UCHALG_LM] = (Uint8) (u32tempVar >> 8);
		pPayload[RADIO__MSGCMMEAS_UCHALG_L] = (Uint8) u32tempVar;
	}

	if (status == REG_OK) {
		status = reg_get(&u32tempVar, radio__hIchalg);
		pPayload[RADIO__MSGCMMEAS_ICHALG_H] = (Uint8) (u32tempVar >> 24);
		pPayload[RADIO__MSGCMMEAS_ICHALG_HM] = (Uint8) (u32tempVar >> 16);
		pPayload[RADIO__MSGCMMEAS_ICHALG_LM] = (Uint8) (u32tempVar >> 8);
		pPayload[RADIO__MSGCMMEAS_ICHALG_L] = (Uint8) u32tempVar;
	}

	if (status == REG_OK) {
		status = reg_get(&u16tempVar, radio__hTboard);
		pPayload[RADIO__MSGCMMEAS_TBOARD_H] = (Uint8) (u16tempVar >> 8);
		pPayload[RADIO__MSGCMMEAS_TBOARD_L] = (Uint8) u16tempVar;
	}

	if (status == REG_OK) {
		status = reg_get(&u16tempVar, radio__hThs);
		pPayload[RADIO__MSGCMMEAS_THS_H] = (Uint8) (u16tempVar >> 8);
		pPayload[RADIO__MSGCMMEAS_THS_L] = (Uint8) u16tempVar;
	}

	if (status == REG_OK) {
		u32tempVar = 0;
		pPayload[RADIO__MSGCMMEAS_U32SPARE1_H] = (Uint8) (u32tempVar >> 24);
		pPayload[RADIO__MSGCMMEAS_U32SPARE1_HM] = (Uint8) (u32tempVar >> 16);
		pPayload[RADIO__MSGCMMEAS_U32SPARE1_LM] = (Uint8) (u32tempVar >> 8);
		pPayload[RADIO__MSGCMMEAS_U32SPARE1_L] = (Uint8) u32tempVar;
	}

	if (status == REG_OK) {
		u16tempVar = 0;
		pPayload[RADIO__MSGCMMEAS_U16SPARE1_H] = (Uint8) (u16tempVar >> 8);
		pPayload[RADIO__MSGCMMEAS_U16SPARE1_L] = (Uint8) u16tempVar;
	}

	if (status == REG_OK) {
		u8tempVar = 0;
		pPayload[RADIO__MSGCMMEAS_U8SPARE1] = u8tempVar;
	}

	if (status == REG_OK) {
		pInst->parent.parent.usedTxBuff = RADIO__MSGCMMEAS_SIZE;

		radio__acBuildHeader(pInst, command, seqNum, RADIO__MSGHCTRL_SINGLE
		);

		retVal = RADIO__ERR_PROCOK_RESP;
	} else {
		retVal = RADIO__ERR_ERROR;
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildGetBmMeas
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process get measurements message.
*
*	\param		pInst		Pointer to instance.
*	\param		seqNum		Sequence number used for the message.
*
*	\retval		RADIO__ERR_PROCOK		if the message was processed ok.
*	\retval		RADIO__ERR_ERROR		if there was a problem with the
*										processing.
*	\retval		RADIO__ERR_PROCOK_RESP	if the message was processed ok and a
*										response message has been build on the
*										buffer.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acBuildGetBmMeas(radio__AcInst * pInst,
		Uint8 seqNum, Uint8 command) {
	radio__ErrCode retVal;
	BYTE * pPayload;
	Uint32 u32tempVar;
	Uint16 u16tempVar;
	Uint8 u8tempVar;
	Uint8 status;

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	status = reg_get(&u16tempVar, radio__hChalgStatus);
	pPayload[RADIO__MSGGETBMMEAS_STATUS_H] = (Uint8) (u16tempVar >> 8);
	pPayload[RADIO__MSGGETBMMEAS_STATUS_L] = (Uint8) u16tempVar;


	if (status == REG_OK) {
		status = reg_get(&u32tempVar, radio__hUchalg);
		pPayload[RADIO__MSGGETBMMEAS_UCHALG_H] = (Uint8) (u32tempVar >> 24);
		pPayload[RADIO__MSGGETBMMEAS_UCHALG_HM] = (Uint8) (u32tempVar >> 16);
		pPayload[RADIO__MSGGETBMMEAS_UCHALG_LM] = (Uint8) (u32tempVar >> 8);
		pPayload[RADIO__MSGGETBMMEAS_UCHALG_L] = (Uint8) u32tempVar;
	}

	if (status == REG_OK) {
		status = reg_get(&u32tempVar, radio__hIchalg);
		pPayload[RADIO__MSGGETBMMEAS_ICHALG_H] = (Uint8) (u32tempVar >> 24);
		pPayload[RADIO__MSGGETBMMEAS_ICHALG_HM] = (Uint8) (u32tempVar >> 16);
		pPayload[RADIO__MSGGETBMMEAS_ICHALG_LM] = (Uint8) (u32tempVar >> 8);
		pPayload[RADIO__MSGGETBMMEAS_ICHALG_L] = (Uint8) u32tempVar;
	}

	if (status == REG_OK) {
		u32tempVar = 0;
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE1_H] = (Uint8) (u32tempVar >> 24);
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE1_HM] = (Uint8) (u32tempVar >> 16);
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE1_LM] = (Uint8) (u32tempVar >> 8);
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE1_L] = (Uint8) u32tempVar;
	}

	if (status == REG_OK) {
		u32tempVar = 0;
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE2_H] = (Uint8) (u32tempVar >> 24);
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE2_HM] = (Uint8) (u32tempVar >> 16);
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE2_LM] = (Uint8) (u32tempVar >> 8);
		pPayload[RADIO__MSGGETBMMEAS_U32SPARE2_L] = (Uint8) u32tempVar;
	}

	if (status == REG_OK) {
		u16tempVar = 0;
		pPayload[RADIO__MSGGETBMMEAS_U16SPARE1_H] = (Uint8) (u16tempVar >> 8);
		pPayload[RADIO__MSGGETBMMEAS_U16SPARE1_L] = (Uint8) u16tempVar;
	}

	if (status == REG_OK) {
		u16tempVar = 0;
		pPayload[RADIO__MSGGETBMMEAS_U16SPARE2_H] = (Uint8) (u16tempVar >> 8);
		pPayload[RADIO__MSGGETBMMEAS_U16SPARE2_L] = (Uint8) u16tempVar;
	}

	if (status == REG_OK) {
		u8tempVar = 0;
		pPayload[RADIO__MSGGETBMMEAS_U8SPARE1] = u8tempVar;
	}

	if (status == REG_OK) {
		u8tempVar = 0;
		pPayload[RADIO__MSGGETBMMEAS_U8SPARE2] = u8tempVar;
	}

	if (status == REG_OK) {
		pInst->parent.parent.usedTxBuff = RADIO__MSGGETBMMEAS_SIZE;

		radio__acBuildHeader(pInst, command, seqNum, RADIO__MSGHCTRL_SINGLE
		);

		retVal = RADIO__ERR_PROCOK_RESP;
	} else {
		retVal = RADIO__ERR_ERROR;
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildSetTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set BMU time.
*
*	\param		pInst		Pointer to instance.
*	\param		seqNum		Sequence number used for the message.
*
*	\retval		RADIO__ERR_PROCOK		if the message was processed ok.
*	\retval		RADIO__ERR_ERROR		if there was a problem with the
*										processing.
*	\retval		RADIO__ERR_PROCOK_RESP	if the message was processed ok and a
*										response message has been build on the
*										buffer.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acBuildSetTime(radio__AcInst * pInst,
		Uint8 seqNum, Uint8 command) {
	radio__ErrCode retVal;
	BYTE * pPayload;
	Uint32 u32tempVar;
	Uint8 u8tempVar;
	Uint8 status;

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	/* Build message */
	u8tempVar = 1;														// Source = CM
	pPayload[RADIO__MSGST_SOURCE] = u8tempVar;

	status = reg_get(&u32tempVar, radio__hLocalTime);
	pPayload[RADIO__MSGST_TIME_H] = (Uint8) (u32tempVar >> 24);
	pPayload[RADIO__MSGST_TIME_HM] = (Uint8) (u32tempVar >> 16);
	pPayload[RADIO__MSGST_TIME_LM] = (Uint8) (u32tempVar >> 8);
	pPayload[RADIO__MSGST_TIME_L] = (Uint8) u32tempVar;


	if (status == REG_OK) {
		pInst->parent.parent.usedTxBuff = RADIO__MSGST_SIZE;

		radio__acBuildHeader(pInst, command, seqNum, RADIO__MSGHCTRL_SINGLE
		);

		retVal = RADIO__ERR_PROCOK_RESP;
	} else {
		retVal = RADIO__ERR_ERROR;
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->normMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildAck
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Build ACK message on buffer.
*
*	\param		pInst	Pointer to instance.
*	\param		reqCmd		The Cmd of the request message that this message is
*							responding to.
*	\param		status		Content of ACK status byte.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE void radio__acBuildAck(radio__AcInst * pInst, Uint8 reqCmd,
		Uint8 status) {
	BYTE * pPayload;

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	pPayload[RADIO__MSGACK_REQCMD] = reqCmd;
	pPayload[RADIO__MSGACK_STATUS] = status;

	pInst->parent.parent.usedTxBuff = RADIO__MSGACK_SIZE;

	radio__acBuildHeader(pInst, RADIO__ACCMD_ACK, pInst->normMessage.seqNum,
			RADIO__MSGHCTRL_SINGLE
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acProcGetSegment
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get next segment from segmented message.
*
*	\param		pInst	Pointer to instance.
*	\param		pMsgInfo	Message info.
*
*	\return		True if message to send.
*
*	\details
*
*	\note
*
*******************************************************************************/
radio__ErrCode radio__acProcGetSegment(radio__Inst * pInstance,
		const uint8_t control) {
	radio__AcInst* pInst;
    radio__ErrCode retVal = RADIO__ERR_PROCOK;

    pInst = (radio__AcInst *) pInstance;

	if (pInst->segmentedMessage.pProcFn != NULL)
	{
		retVal = pInst->segmentedMessage.pProcFn(pInst, control);
	}

	return retVal;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildConfig
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Build next CM config message segment on the buffer.
*
*	\param		pInst	Pointer to instance.
*	\param		segment	The segment to read.
*
*	\return		True if a new segment was copied to the buffer. False if there
*				is no more segments.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE radio__ErrCode radio__acBuildConfig(radio__AcInst * pInst,
		const uint8_t segment) {
	radio__ErrCode retVal;
	BYTE * pPayload;
	Uint8 u8TempVar;
	Uint16 u16TempVar;
	Uint32 u32TempVar;
	Uint8 control;

	/*
	 * Get pointer to the payload part of the PopNet gateway protocol network
	 * data request. Step forward to the data part of the message.
	 */

	pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
	pPayload += RADIO__MSGH_DATA;

	if (segment == 4) {
		/*
		 * Build segment 1 of 4
		 */

		reg_getLocal(&u16TempVar, reg_i_setPanId, pInst->parent.parent.instId);
		pPayload[RADIO__MSGCMCFG1_PANID_H] = (Uint8) (u16TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_PANID_L] = (Uint8) u16TempVar;

		reg_getLocal(&u8TempVar, reg_i_setChannel, pInst->parent.parent.instId);
		// setChannel is in interval 0-15 (channel is in interval 11-26)
		pPayload[RADIO__MSGCMCFG1_CHANNELID] = u8TempVar;

		reg_getLocal(&u16TempVar, reg_i_setNodeId, pInst->parent.parent.instId);
		pPayload[RADIO__MSGCMCFG1_NWKID_H] = (Uint8) (u16TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_NWKID_L] = (Uint8) (u16TempVar);

		reg_get(&u32TempVar, radio__hSerialNo);
		pPayload[RADIO__MSGCMCFG1_SERIALNO_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_SERIALNO_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_SERIALNO_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_SERIALNO_L] = (Uint8) u32TempVar;

		reg_get(&u32TempVar, radio__hEngineCode);
		pPayload[RADIO__MSGCMCFG1_ENGINECODE_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_ENGINECODE_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_ENGINECODE_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_ENGINECODE_L] = (Uint8) u32TempVar;

		reg_get(&u8TempVar, radio__hDefBatteryType);
		pPayload[RADIO__MSGCMCFG1_BATTERYTYPE] = u8TempVar;

		reg_get(&u16TempVar, radio__hDefAlgNo);
		pPayload[RADIO__MSGCMCFG1_ALGNO_H] = (Uint8) (u16TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_ALGNO_L] = (Uint8) u16TempVar;

		reg_get(&u16TempVar, radio__hDefCapacity);
		pPayload[RADIO__MSGCMCFG1_CAPACITY_H] = (Uint8) (u16TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_CAPACITY_L] = (Uint8) u16TempVar;

		reg_get(&u16TempVar, radio__hDefCableRes);
		pPayload[RADIO__MSGCMCFG1_CABLERES_H] = (Uint8) (u16TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_CABLERES_L] = (Uint8) u16TempVar;

		reg_get(&u16TempVar, radio__hDefCell);
		pPayload[RADIO__MSGCMCFG1_CELL_H] = (Uint8) (u16TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_CELL_L] = (Uint8) u16TempVar;

		reg_get(&u16TempVar, radio__hDefBaseLoad);
		pPayload[RADIO__MSGCMCFG1_BASELOAD_H] = (Uint8) (u16TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_BASELOAD_L] = (Uint8) u16TempVar;

		reg_get(&u8TempVar, radio__hPowerGroup);
		pPayload[RADIO__MSGCMCFG1_POWERGROUP] = u8TempVar;

		reg_get(&u32TempVar, radio__hIacLimit);
		pPayload[RADIO__MSGCMCFG1_IACLIMIT_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_IACLIMIT_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_IACLIMIT_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_IACLIMIT_L] = (Uint8) u32TempVar;

		reg_get(&u32TempVar, radio__hPacLimit);
		pPayload[RADIO__MSGCMCFG1_PACLIMIT_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_PACLIMIT_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_PACLIMIT_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_PACLIMIT_L] = (Uint8) u32TempVar;

		reg_get(&u32TempVar, radio__hIdcLimit);
		pPayload[RADIO__MSGCMCFG1_IDCLIMIT_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_IDCLIMIT_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_IDCLIMIT_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_IDCLIMIT_L] = (Uint8) u32TempVar;
/*
		reg_get(&u32TempVar, radio__hUadSlope);
		pPayload[RADIO__MSGCMCFG1_UADSLOPE_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_UADSLOPE_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_UADSLOPE_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_UADSLOPE_L] = (Uint8) u32TempVar;

		reg_get(&u32TempVar, radio__hUadOffset);
		pPayload[RADIO__MSGCMCFG1_UADOFFSET_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_UADOFFSET_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_UADOFFSET_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_UADOFFSET_L] = (Uint8) u32TempVar;

		reg_get(&u32TempVar, radio__hIadSlope);
		pPayload[RADIO__MSGCMCFG1_IADSLOPE_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_IADSLOPE_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_IADSLOPE_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_IADSLOPE_L] = (Uint8) u32TempVar;

		reg_get(&u32TempVar, radio__hIadOffset);
		pPayload[RADIO__MSGCMCFG1_IADOFFSET_H] = (Uint8) (u32TempVar >> 24);
		pPayload[RADIO__MSGCMCFG1_IADOFFSET_HM] = (Uint8) (u32TempVar >> 16);
		pPayload[RADIO__MSGCMCFG1_IADOFFSET_LM] = (Uint8) (u32TempVar >> 8);
		pPayload[RADIO__MSGCMCFG1_IADOFFSET_L] = (Uint8) u32TempVar;

		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_ISETSLOPE, radio__hIsetSlope);
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_ISETOFFSET, radio__hIsetOffset);
*/
		/* Spare data set to zero */
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_U32SPARE5, radio__hU32Spare);
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_U32SPARE6, radio__hU32Spare);
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_U32SPARE7, radio__hU32Spare);
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_U32SPARE8, radio__hU32Spare);
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_U32SPARE9, radio__hU32Spare);
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG1_U32SPARE10, radio__hU32Spare);

		reg_get(&pPayload[RADIO__MSGCMCFG1_DISPCONTRAST], radio__hDispContrast);
		reg_get(&pPayload[RADIO__MSGCMCFG1_LEDBRIGHTMAX], radio__hLedBrightMax);
		reg_get(&pPayload[RADIO__MSGCMCFG1_LEDBRIGHTDIM], radio__hLedBrightDim);

		reg_get(&pPayload[RADIO__MSGCMCFG1_LANGUAGE], radio__hLanguage);
		reg_get(&pPayload[RADIO__MSGCMCFG1_TIMEDATEFORMAT], radio__hTimeDateFormat);

		reg_get(&pPayload[RADIO__MSGCMCFG1_BUTTONF1FUNC], radio__hButtonF1_func);
		reg_get(&pPayload[RADIO__MSGCMCFG1_BUTTONF2FUNC], radio__hButtonF2_func);
		reg_get(&pPayload[RADIO__MSGCMCFG1_REMOTEINFUNC], radio__hRemoteIn_func);
		reg_get(&pPayload[RADIO__MSGCMCFG1_REMOTEOUTFUNC], radio__hRemoteOut_func);
		reg_get(&pPayload[RADIO__MSGCMCFG1_REMOTEOUTALARMVAR], radio__hRemoteOutAlarm_var);
		reg_get(&pPayload[RADIO__MSGCMCFG1_REMOTEOUTPHASEVAR], radio__hRemoteOutPhase_var);
		reg_get(&pPayload[RADIO__MSGCMCFG1_WATERFUNC], radio__hWater_func);
		reg_get(&pPayload[RADIO__MSGCMCFG1_WATERVAR], radio__hWater_var);
		reg_get(&pPayload[RADIO__MSGCMCFG1_AIRPUMPVAR], radio__hAirPump_var);
		reg_get(&pPayload[RADIO__MSGCMCFG1_AIRPUMPVAR2], radio__hAirPump_var2);
		/* Parallel control */
#if PROJECT_USE_CAN == 1
		reg_get(&pPayload[RADIO__MSGCMCFG1_PARALLELFUNC], radio__hParallelControl_func);
#endif
		reg_get(&pPayload[RADIO__MSGCMCFG1_TIMERESTRICTION], radio__hTimeRestriction);

		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_0], radio__httCtrl, 0);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_1], radio__httFromHour, 0);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_2], radio__httFromMin, 0);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_3], radio__httToHour, 0);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_4], radio__httToMin, 0);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_5], radio__httCtrl, 1);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_6], radio__httFromHour, 1);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_7], radio__httFromMin, 1);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_8], radio__httToHour, 1);
		reg_aGet(&pPayload[RADIO__MSGCMCFG1_TIMETABLE_9], radio__httToMin, 1);

		pInst->parent.parent.usedTxBuff = RADIO__MSGCMCFG1_SIZE;
		control = (RADIO__MSGHCTRL_FIRST + RADIO__MSGCMCFG_SEGMENTS);
		retVal = RADIO__ERR_PROCOK_RESP;
	} else if (segment == 3) {
		/*
		 * Build segment 2 of 4
		 */
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_10], radio__httCtrl, 2);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_11], radio__httFromHour,
				2);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_12], radio__httFromMin, 2);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_13], radio__httToHour, 2);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_14], radio__httToMin, 2);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_15], radio__httCtrl, 3);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_16], radio__httFromHour,
				3);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_17], radio__httFromMin, 3);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_18], radio__httToHour, 3);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_19], radio__httToMin, 3);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_20], radio__httCtrl, 4);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_21], radio__httFromHour,
				4);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_22], radio__httFromMin, 4);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_23], radio__httToHour, 4);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_24], radio__httToMin, 4);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_25], radio__httCtrl, 5);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_26], radio__httFromHour,
				5);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_27], radio__httFromMin, 5);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_28], radio__httToHour, 5);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_29], radio__httToMin, 5);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_30], radio__httCtrl, 6);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_31], radio__httFromHour,
				6);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_32], radio__httFromMin, 6);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_33], radio__httToHour, 6);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_34], radio__httToMin, 6);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_35], radio__httCtrl, 7);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_36], radio__httFromHour,
				7);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_37], radio__httFromMin, 7);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_38], radio__httToHour, 7);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_39], radio__httToMin, 7);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_40], radio__httCtrl, 8);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_41], radio__httFromHour,
				8);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_42], radio__httFromMin, 8);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_43], radio__httToHour, 8);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_44], radio__httToMin, 8);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_45], radio__httCtrl, 9);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_46], radio__httFromHour,
				9);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_47], radio__httFromMin, 9);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_48], radio__httToHour, 9);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_49], radio__httToMin, 9);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_50], radio__httCtrl, 10);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_51], radio__httFromHour,
				10);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_52], radio__httFromMin,
				10);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_53], radio__httToHour, 10);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_54], radio__httToMin, 10);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_55], radio__httCtrl, 11);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_56], radio__httFromHour,
				11);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_57], radio__httFromMin,
				11);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_58], radio__httToHour, 11);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_59], radio__httToMin, 11);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_60], radio__httCtrl, 12);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_61], radio__httFromHour,
				12);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_62], radio__httFromMin,
				12);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_63], radio__httToHour, 12);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_64], radio__httToMin, 12);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_65], radio__httCtrl, 13);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_66], radio__httFromHour,
				13);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_67], radio__httFromMin,
				13);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_68], radio__httToHour, 13);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_69], radio__httToMin, 13);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_70], radio__httCtrl, 14);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_71], radio__httFromHour,
				14);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_72], radio__httFromMin,
				14);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_73], radio__httToHour, 14);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_74], radio__httToMin, 14);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_75], radio__httCtrl, 15);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_76], radio__httFromHour,
				15);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_77], radio__httFromMin,
				15);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_78], radio__httToHour, 15);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_79], radio__httToMin, 15);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_80], radio__httCtrl, 16);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_81], radio__httFromHour,
				16);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_82], radio__httFromMin,
				16);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_83], radio__httToHour, 16);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_84], radio__httToMin, 16);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_85], radio__httCtrl, 17);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_86], radio__httFromHour,
				17);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_87], radio__httFromMin,
				17);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_88], radio__httToHour, 17);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_89], radio__httToMin, 17);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_90], radio__httCtrl, 18);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_91], radio__httFromHour,
				18);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_92], radio__httFromMin,
				18);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_93], radio__httToHour, 18);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_94], radio__httToMin, 18);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_95], radio__httCtrl, 19);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_96], radio__httFromHour,
				19);
		reg_aGet(&pPayload[RADIO__MSGCMCFG2_TIMETABLE_97], radio__httFromMin, 19);

		pInst->parent.parent.usedTxBuff = RADIO__MSGCMCFG2_SIZE;
		control = (RADIO__MSGCMCFG_SEGMENTS - 1);
		retVal = RADIO__ERR_PROCOK_RESP;
	} else if (segment == 2) {
		/*
		 * Build segment 3 of 4
		 */
		reg_aGet(&pPayload[RADIO__MSGCMCFG3_TIMETABLE_98], radio__httToHour, 19);
		reg_aGet(&pPayload[RADIO__MSGCMCFG3_TIMETABLE_99], radio__httToMin, 19);

		reg_aGet(&pPayload[RADIO__MSGCMCFG3_TIMETABLE_100], radio__httCtrl, 20);
		reg_aGet(&pPayload[RADIO__MSGCMCFG3_TIMETABLE_101], radio__httFromHour,
				20);
		reg_aGet(&pPayload[RADIO__MSGCMCFG3_TIMETABLE_102], radio__httFromMin,
				20);
		reg_aGet(&pPayload[RADIO__MSGCMCFG3_TIMETABLE_103], radio__httToHour,
				20);
		reg_aGet(&pPayload[RADIO__MSGCMCFG3_TIMETABLE_104], radio__httToMin, 20);

		/*
		 * CAN Config
		 */
#if PROJECT_USE_CAN == 1
		reg_get(&pPayload[RADIO__MSGCMCFG3_CANMODE], radio__hCANMode);

		radio__acBuildU16(pPayload, RADIO__MSGCMCFG3_CANNWKCTRL, radio__hCANNwkCtrl)
#endif
		/*
		 * Extra I/O Config
		 */
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_0, radio__hPinOutSelect, 0);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_1, radio__hPinOutSelect, 1);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_2, radio__hPinOutSelect, 2);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_3, radio__hPinOutSelect, 3);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_4, radio__hPinOutSelect, 4);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_5, radio__hPinOutSelect, 5);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_6, radio__hPinOutSelect, 6);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PINOUTSELECT_7, radio__hPinOutSelect, 7);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_0, radio__hPinInSelect, 0);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_1, radio__hPinInSelect, 1);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_2, radio__hPinInSelect, 2);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_3, radio__hPinInSelect, 3);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_4, radio__hPinInSelect, 4);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_5, radio__hPinInSelect, 5);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_6, radio__hPinInSelect, 6);
		radio__acABuildU32(pPayload, RADIO__MSGCMCFG3_PININSELECT_7, radio__hPinInSelect, 7);

		/*
		 * RADIO Config
		 */

		reg_getLocal(&pPayload[RADIO__MSGCMCFG3_RADIOMODE], reg_i_RadioMode,
				pInst->parent.parent.instId);
		reg_get(&pPayload[RADIO__MSGCMCFG3_CHARGINGMODE], radio__DefChargingMode);
/*
		reg_getLocal(&pPayload[RADIO__MSGCMCFG3_JOINENABLE], reg_i_joinEnable,
				pInst->parent.parent.instId);
*/
		reg_getLocal(&pPayload[RADIO__MSGCMCFG3_NWKSETTINGS], reg_i_NwkSettings,
				pInst->parent.parent.instId);

		/*
		 * Other stuff
		 */

		radio__acBuildU32(pPayload, RADIO__MSGCMCFG3_SECURITYCODE1, radio__hSecurityCode1);
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG3_SECURITYCODE2, radio__hSecurityCode2);

		reg_get(&pPayload[RADIO__MSGCMCFG3_BLTIME], radio__hBacklightTime);

		radio__acBuildU16(pPayload, RADIO__MSGCMCFG3_INSTLOGSAMPPER, radio__hInstLogSPeriod)

		pInst->parent.parent.usedTxBuff = RADIO__MSGCMCFG3_SIZE;
		control = (RADIO__MSGCMCFG_SEGMENTS - 2);
		retVal = RADIO__ERR_PROCOK_RESP;
	} else if (segment == 1) {
		/*
		 * Build segment 4 of 4
		 */

		/* Charger Id*/
		reg_getLocal(&u32TempVar, reg_i_ChargerId, pInst->parent.parent.instId);
		(pPayload)[RADIO__MSGCMCFG4_CID_H] = (BYTE) (u32TempVar >> 24);
		(pPayload)[RADIO__MSGCMCFG4_CID_HM] = (BYTE) (u32TempVar >> 16);
		(pPayload)[RADIO__MSGCMCFG4_CID_LM] = (BYTE) (u32TempVar >> 8);
		(pPayload)[RADIO__MSGCMCFG4_CID_L] = (BYTE) u32TempVar;

		/* BBC registers*/
		reg_get(&pPayload[RADIO__MSGCMCFG4_BBCFUNC], radio__hBBC_func);
		reg_get(&pPayload[RADIO__MSGCMCFG4_BBCVAR], radio__hBBC_var);

		/* Equalize registers*/
		reg_get(&pPayload[RADIO__MSGCMCFG4_EQUFUNC], radio__hEqualize_func);
		reg_get(&pPayload[RADIO__MSGCMCFG4_EQUVAR], radio__hEqualize_var);
		reg_get(&pPayload[RADIO__MSGCMCFG4_EQUVAR2], radio__hEqualize_var2);

		/* Routing */
		reg_getLocal(&pPayload[RADIO__MSGCMCFG4_ROUTING], reg_i_Routing, pInst->parent.parent.instId);

		/* Bit config */
		reg_get(&pPayload[RADIO__MSGCMCFG4_BITCONFIG], radio__hBitConfig);

		/* Remote out BBC register */
		reg_get(&pPayload[RADIO__MSGCMCFG4_REMOTEOUTBBCVAR], radio__hRemoteOutBBC_var);

		/* Can baudrate (kbps)*/
		reg_get(&pPayload[RADIO__MSGCMCFG4_CANBPS], radio__hCANBps);

		/* Extra charge registers*/
		reg_get(&pPayload[RADIO__MSGCMCFG4_ECFUNC], radio__hExtraCharge_func);
		reg_get(&pPayload[RADIO__MSGCMCFG4_ECVAR], radio__hExtraCharge_var);
		reg_get(&pPayload[RADIO__MSGCMCFG4_ECVAR2], radio__hExtraCharge_var2);
		reg_get(&pPayload[RADIO__MSGCMCFG4_ECVAR3], radio__hExtraCharge_var3);

		/* HW type OBS! Only applicable for Access 30 (SW 11617017) */
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG4_HWTYPE, radio__hU32Spare);

		/* HW version OBS! Only applicable for Access 30 (SW 11617017) */
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG4_HWVER, radio__hU32Spare);

		/* DPL Pac limit total*/
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG4_DPLPACTOT, radio__hDplPowerLimitTotal);

		/* u32Spare data set to zero */
		radio__acBuildU32(pPayload, RADIO__MSGCMCFG4_U32SPARE4, radio__hU32Spare);

		/* Static battery temp */
		radio__acBuildU16(pPayload, RADIO__MSGCMCFG4_BATTERYTEMP, radio__hDefBatteryTemp);

		/* Air pump high/low alarm level */
		radio__acBuildU16(pPayload, RADIO__MSGCMCFG4_AIRPUMPLOW, radio__AirPumpAlarmLow);
		radio__acBuildU16(pPayload, RADIO__MSGCMCFG4_AIRPUMPHIGH, radio__AirPumpAlarmHigh);

		/* u16Spare data set to zero */
		radio__acBuildU16(pPayload, RADIO__MSGCMCFG4_U16SPARE4, radio__hU16Spare);

		/* CAN node id */
		reg_get(&pPayload[RADIO__MSGCMCFG4_CANNODEID], radio__hCANNodeId);

		/* DPL func */
		reg_get(&pPayload[RADIO__MSGCMCFG4_DPLFUNC], radio__hPowerGroup_func);

		/* DPL prio */
		reg_get(&pPayload[RADIO__MSGCMCFG4_DPLPRIO], radio__hDplPriorityFactor);

		/* DPL Pac limit default */
		reg_get(&pPayload[RADIO__MSGCMCFG4_DPLPACDEF], radio__hDplPacLimit_default);

		//reg_get(&pPayload[RADIO__MSGCMCFG4_ITHRESHOLDVAR], radio__hIthreshold_var); 	// sorabITR - not now
		//reg_get(&pPayload[RADIO__MSGCMCFG4_ITHRESHOLDCOND], radio__hIthreshold_cond); // sorabITR - not now

		pInst->parent.parent.usedTxBuff = RADIO__MSGCMCFG4_SIZE;
		control = RADIO__MSGHCTRL_LAST;
		retVal = RADIO__ERR_PROCOK_RESP;
	} else {
		/*
		 * All CMconfig segments sent.
		 */

		retVal = RADIO__ERR_PROCOK;
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		radio__acBuildHeader(pInst, RADIO__ACCMD_CMCONFIG,
			pInst->segmentedMessage.seqNum, control);
		pInst->pLastMsg = &pInst->segmentedMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildHistLog
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue to process get log message.
*
*	\param		pInst	Pointer to instance.
*	\param		segment	Segment to read.
*
*	\retval		TRUE	If the get log message has been handled.
*	\retval		FALSE	If still handling the get log message.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acBuildHistLog(radio__AcInst * pInst,
		const uint8_t segment) {
	radio__ErrCode retVal;
	Uint8 control;

	uint8_t segmentTotal = pInst->recCount * RADIO__MSGCMHLOG_SEGMENTS;
	uint8_t segmentIndex = segmentTotal - segment;
	uint8_t recordOffset;

	if (segment == 0) {
		segmentIndex = pInst->recordOffset * RADIO__MSGCMHLOG_SEGMENTS;
		recordOffset = pInst->recordOffset;
	} else {
		recordOffset = segmentIndex / RADIO__MSGCMHLOG_SEGMENTS;

		if (pInst->recordOffset != recordOffset) {
			/* Read log */
			pInst->recordOffset = recordOffset;
			pInst->logRequest.index = pInst->recordNumber +
					pInst->recordOffset;
			pInst->logRequest.flags = CM_REQFLG_READ;
			cm_processRequest(&pInst->logRequest);

			return RADIO__ERR_PROCOK;
		}
	}

	if (pInst->recCount) {
		BYTE * pPayload;

		/* Get pointer to payload */
		pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
		pPayload += RADIO__MSGH_DATA;

		if (!(pInst->logRequest.flags & CM_REQFLG_SUCCESS)) {
			/*
			 * The record could not be found. Some records have already been
			 * found. Just send the found records.
			 */
			pPayload[RADIO__MSGCMLOG_LOGTYPE] = 0xFF;
			pInst->parent.parent.usedTxBuff = 1;
			pInst->recCount = 0;
			control = RADIO__MSGHCTRL_SINGLE;
			retVal = RADIO__ERR_PROCOK_RESP;
		} else {
			if (segmentIndex % RADIO__MSGCMHLOG_SEGMENTS == 0) {
				Uint32 u32TempVar;
				Uint16 u16TempVar;
				Uint8 u8TempVar;

				// get pointer to log buffer
				cm_HistRecStd *pBuffer = (cm_HistRecStd*) pInst->logBuff;

				// set log type
				pPayload[RADIO__MSGCMHLOG1_LOGTYPE] = RADIO__LOGTYPE_HISTORICAL;
				
				u32TempVar = pBuffer->header.ChargeIndex;
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDX_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDX_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDX_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDX_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->header.ChargeIndexReset;
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDXRESET_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDXRESET_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDXRESET_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGEIDXRESET_L] = (Uint8) (u32TempVar);

				u16TempVar = pBuffer->header.RecordType;
				pPayload[RADIO__MSGCMHLOG1_RECTYPE_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_RECTYPE_L] = (Uint8) (u16TempVar);

				u8TempVar = pBuffer->header.RecordSize;
				pPayload[RADIO__MSGCMHLOG1_RECSIZE] = (Uint8) (u8TempVar);

				u32TempVar = pBuffer->chargeStartTime;
				pPayload[RADIO__MSGCMHLOG1_CHARGESTARTTIME_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_CHARGESTARTTIME_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_CHARGESTARTTIME_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGESTARTTIME_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->chargeEndTime;
				pPayload[RADIO__MSGCMHLOG1_CHARGEENDTIME_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_CHARGEENDTIME_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_CHARGEENDTIME_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGEENDTIME_L] = (Uint8) (u32TempVar);

				u16TempVar = pBuffer->AlgNo;
				pPayload[RADIO__MSGCMHLOG1_ALGNO_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_ALGNO_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->Capacity;
				pPayload[RADIO__MSGCMHLOG1_CAPACITY_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CAPACITY_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->Cells;
				pPayload[RADIO__MSGCMHLOG1_CELLS_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CELLS_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->Ri;
				pPayload[RADIO__MSGCMHLOG1_CABLERES_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CABLERES_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->Baseload;
				pPayload[RADIO__MSGCMHLOG1_BASELOAD_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_BASELOAD_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->ThsStart;
				pPayload[RADIO__MSGCMHLOG1_THSSTART_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_THSSTART_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->ThsEnd;
				pPayload[RADIO__MSGCMHLOG1_THSEND_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_THSEND_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->ThsMax;
				pPayload[RADIO__MSGCMHLOG1_THSMAX_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_THSMAX_L] = (Uint8) (u16TempVar);

				u32TempVar = pBuffer->startVoltage;
				pPayload[RADIO__MSGCMHLOG1_STARTVOLTAGE_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_STARTVOLTAGE_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_STARTVOLTAGE_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_STARTVOLTAGE_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->endVoltage;
				pPayload[RADIO__MSGCMHLOG1_ENDVOLTAGE_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_ENDVOLTAGE_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_ENDTVOLTAGE_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_ENDVOLTAGE_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->ChargeTime;
				pPayload[RADIO__MSGCMHLOG1_CHARGETIME_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_CHARGETIME_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_CHARGETIME_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGETIME_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->chargedAh;
				pPayload[RADIO__MSGCMHLOG1_CHARGEDAH_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_CHARGEDAH_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_CHARGEDAH_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGEDAH_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->chargedWh;
				pPayload[RADIO__MSGCMHLOG1_CHARGEDWH_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_CHARGEDWH_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_CHARGEDWH_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGEDWH_L] = (Uint8) (u32TempVar);

				u16TempVar = pBuffer->chargedAhP;
				pPayload[RADIO__MSGCMHLOG1_CHARGEDAHP_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHARGEDAHP_L] = (Uint8) (u16TempVar);

				u32TempVar = pBuffer->equTime;
				pPayload[RADIO__MSGCMHLOG1_EQUTIME_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_EQUTIME_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_EQTIME_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_EQUTIME_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->equAh;
				pPayload[RADIO__MSGCMHLOG1_EQUAH_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_EQUAH_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_EQTAH_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_EQUAH_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->equWh;
				pPayload[RADIO__MSGCMHLOG1_EQUWH_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_EQUWH_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_EQUWH_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_EQUWH_L] = (Uint8) (u32TempVar);

				u16TempVar = pBuffer->ChalgErrorSum;
				pPayload[RADIO__MSGCMHLOG1_CHALGERRORSUM_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_CHALGERRORSUM_L] = (Uint8) (u16TempVar);

				u16TempVar = pBuffer->ReguErrorSum;
				pPayload[RADIO__MSGCMHLOG1_REGUERRORSUM_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_REGUERRORSUM_L] = (Uint8) (u16TempVar);

				u32TempVar = pBuffer->EvtIdxStart;
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTART_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTART_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTART_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTART_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->EvtIdxStop;
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTOP_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTOP_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTOP_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_EVTIDXSTOP_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->BID;
				pPayload[RADIO__MSGCMHLOG1_BID_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG1_BID_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG1_BID_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG1_BID_L] = (Uint8) (u32TempVar);

				u8TempVar = pBuffer->BMfgId;
				pPayload[RADIO__MSGCMHLOG1_BMFGID] = (Uint8) (u8TempVar);

				u8TempVar = pBuffer->BatteryType;
				pPayload[RADIO__MSGCMHLOG1_BTYPE] = (Uint8) (u8TempVar);

				/* Send first segment */
				pInst->parent.parent.usedTxBuff = RADIO__MSGCMHLOG1_SIZE;

			    control = segmentTotal - segmentIndex;

			    if (segmentIndex == 0 && segmentTotal > 1) {
			      control += RADIO__MSGHCTRL_FIRST;
			    }
			} else {
				Uint32 u32TempVar;
				Uint16 u16TempVar;
				Uint8 u8TempVar;
				char *pTempStr;

				// get pointer to log buffer
				cm_HistRecStd *pBuffer = (cm_HistRecStd*) pInst->logBuff;

				u32TempVar = pBuffer->BSN;
				pPayload[RADIO__MSGCMHLOG2_BSN_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG2_BSN_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG2_BSN_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG2_BSN_L] = (Uint8) (u32TempVar);

	 			u32TempVar = pBuffer->CMtype;
				pPayload[RADIO__MSGCMHLOG2_ENGINECODE_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG2_ENGINECODE_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG2_ENGINECODE_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG2_ENGINECODE_L] = (Uint8) (u32TempVar);

				u32TempVar = pBuffer->SerialNo;
				pPayload[RADIO__MSGCMHLOG2_SERIALNO_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG2_SERIALNO_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG2_SERIALNO_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG2_SERIALNO_L] = (Uint8) (u32TempVar);

				//pTempStr = &pBuffer->AlgNoName;
				pTempStr = (char*)pBuffer->AlgNoName;
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_0] = (Uint8) (pTempStr[0]);
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_1] = (Uint8) (pTempStr[1]);
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_2] = (Uint8) (pTempStr[2]);
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_3] = (Uint8) (pTempStr[3]);
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_4] = (Uint8) (pTempStr[4]);
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_5] = (Uint8) (pTempStr[5]);
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_6] = (Uint8) (pTempStr[6]);
				pPayload[RADIO__MSGCMHLOG2_ALGNONAME_7] = (Uint8) (pTempStr[7]);
				u8TempVar = pBuffer->ChargingMode;
				pPayload[RADIO__MSGCMHLOG2_CHARGINGMODE] = (Uint8) (u8TempVar);

				u16TempVar = pBuffer->ChalgStatusSum;
				pPayload[RADIO__MSGCMHLOG2_CHALGSTATSUM_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG2_CHALGSTATSUM_L] = (Uint8) (u16TempVar);

				u32TempVar = pBuffer->FID;
				pPayload[RADIO__MSGCMHLOG2_FID_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMHLOG2_FID_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMHLOG2_FID_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMHLOG2_FID_L] = (Uint8) (u32TempVar);

				/* Spare data set to zero */
				radio__acBuildU32(pPayload, RADIO__MSGCMHLOG2_U32SPARE1, radio__hU32Spare);
				radio__acBuildU32(pPayload, RADIO__MSGCMHLOG2_U32SPARE2, radio__hU32Spare);
				radio__acBuildU16(pPayload, RADIO__MSGCMHLOG2_U16SPARE1, radio__hU16Spare);
				radio__acBuildU16(pPayload, RADIO__MSGCMHLOG2_U16SPARE2, radio__hU16Spare);
				reg_get(&pPayload[RADIO__MSGCMHLOG2_U8SPARE1], radio__hU8Spare);
				reg_get(&pPayload[RADIO__MSGCMHLOG2_U8SPARE2], radio__hU8Spare);

				pInst->parent.parent.usedTxBuff = RADIO__MSGCMHLOG2_SIZE;
			    control = segmentTotal - segmentIndex;
			}

			radio__acBuildHeader(pInst, RADIO__ACCMD_CMLOG,
				pInst->segmentedMessage.seqNum, control);
			pInst->pLastMsg = &pInst->segmentedMessage;
			retVal = RADIO__ERR_PROCOK_RESP;
		}
	} else {
		/*
		 * No records to send
		 */
		retVal = RADIO__ERR_PROCOK;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildEventLog
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue to process get log message.
*
*	\param		pInst	Pointer to instance.
*	\param		segment	Segment to read.
*
*	\retval		TRUE	If the get log message has been handled.
*	\retval		FALSE	If still handling the get log message.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__acBuildEventLog(radio__AcInst * pInst,
		const uint8_t segment) {
	radio__ErrCode retVal;
	Uint8 control;
	static Uint16 EvtId;

	uint8_t segmentTotal = pInst->recCount;
	uint8_t segmentIndex = segmentTotal - segment;
	uint8_t recordOffset;

	if (segment == 0) {
		segmentIndex = pInst->recordOffset;
		recordOffset = pInst->recordOffset;
	} else {
		recordOffset = segmentIndex;

		if (pInst->recordOffset != recordOffset) {
			/* Read log */
			pInst->recordOffset = recordOffset;
			pInst->logRequest.index = pInst->recordNumber +
					pInst->recordOffset;
			pInst->logRequest.flags = CM_REQFLG_READ;
			cm_processRequest(&pInst->logRequest);

			return RADIO__ERR_PROCOK;
		}
	}

	if (pInst->recCount) {
		BYTE * pPayload;

		/* Get pointer to payload */
		pPayload = radio__acGetPayloadPointer((radio__Inst *) pInst);
		pPayload += RADIO__MSGH_DATA;

		if (!(pInst->logRequest.flags & CM_REQFLG_SUCCESS)) {
			/*
			 * The record could not be found.
			 * Set flag for sending error message.
			 */
			retVal = RADIO__ERR_PROCOK;
		} else { //log read from flash OK
			Uint32 u32TempVar;
			Uint16 u16TempVar;
			Uint8 u8TempVar;

			// get pointer to log buffer
			cm_EvtLogHdr *pHeader = (cm_EvtLogHdr*) pInst->logBuff;

			pPayload[RADIO__MSGCMLOG_LOGTYPE] = RADIO__LOGTYPE_EVENT;
			// set payload offset to point at first element of specific Event
			pPayload += RADIO__MSGCMLOG_LOGDATA;
			// increase payload size
			pInst->parent.parent.usedTxBuff = RADIO__MSGCMLOG_LOGDATA;

			u32TempVar = pHeader->Index;
			pPayload[RADIO__MSGCMELOG_INDEX_H] = (Uint8) (u32TempVar >> 24);
			pPayload[RADIO__MSGCMELOG_INDEX_HM] = (Uint8) (u32TempVar >> 16);
			pPayload[RADIO__MSGCMELOG_INDEX_LM] = (Uint8) (u32TempVar >> 8);
			pPayload[RADIO__MSGCMELOG_INDEX_L] = (Uint8) (u32TempVar);

			u32TempVar = pHeader->IndexReset;
			pPayload[RADIO__MSGCMELOG_INDEXRESET_H] = (Uint8) (u32TempVar >> 24);
			pPayload[RADIO__MSGCMELOG_INDEXRESET_HM] = (Uint8) (u32TempVar >> 16);
			pPayload[RADIO__MSGCMELOG_INDEXRESET_LM] = (Uint8) (u32TempVar >> 8);
			pPayload[RADIO__MSGCMELOG_INDEXRESET_L] = (Uint8) (u32TempVar);

			u32TempVar = pHeader->Time;
			pPayload[RADIO__MSGCMELOG_TIME_H] = (Uint8) (u32TempVar >> 24);
			pPayload[RADIO__MSGCMELOG_TIME_HM] = (Uint8) (u32TempVar >> 16);
			pPayload[RADIO__MSGCMELOG_TIME_LM] = (Uint8) (u32TempVar >> 8);
			pPayload[RADIO__MSGCMELOG_TIME_L] = (Uint8) (u32TempVar);

			EvtId = pHeader->EventId;
			pPayload[RADIO__MSGCMELOG_EVTID_H] = (Uint8) (EvtId >> 8);
			pPayload[RADIO__MSGCMELOG_EVTID_L] = (Uint8) (EvtId);

			u8TempVar = pHeader->DataSize;
			pPayload[RADIO__MSGCMELOG_DATASIZE] = (Uint8) (u8TempVar);

			// set payload offset to point at first element of specific Event
			pPayload += RADIO__MSGCMELOG_SIZE;
			// increase payload size
			pInst->parent.parent.usedTxBuff += RADIO__MSGCMELOG_SIZE;

			// Handle log depending on the Event
			switch (EvtId) {
				case CM_EVENT_TIMESET: {
					// get pointer to log buffer
					cm_EvtRecTimeSet *pEvt1Log = (cm_EvtRecTimeSet*) pInst->logBuff;
					u32TempVar = pEvt1Log->OldTime;
					pPayload[RADIO__MSGCME1LOG_OLDTIME_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME1LOG_OLDTIME_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME1LOG_OLDTIME_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME1LOG_OLDTIME_L] = (Uint8) (u32TempVar);

					u8TempVar = pEvt1Log->Source;
					pPayload[RADIO__MSGCME1LOG_SOURCE] = (Uint8) (u8TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME1LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME1LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME1LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME1LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_CHARGESTARTED: {
					pInst->parent.parent.usedTxBuff += RADIO__MSGCME2LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_CHARGEENDED: {
					pInst->parent.parent.usedTxBuff += RADIO__MSGCME3LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_CHALGERROR: {
					// get pointer to log buffer
					cm_EvtRecChalgError *pEvt4Log = (cm_EvtRecChalgError*) pInst->logBuff;
					u16TempVar = pEvt4Log->ChalgError;
					pPayload[RADIO__MSGCME4LOG_CHALGERROR_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME4LOG_CHALGERROR_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt4Log->Active;
					pPayload[RADIO__MSGCME4LOG_ACTIVE_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME4LOG_ACTIVE_L] = (Uint8) (u16TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME4LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME4LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME4LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME4LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_REGUERROR: {
					// get pointer to log buffer
					cm_EvtRecReguError *pEvt5Log = (cm_EvtRecReguError*) pInst->logBuff;
					u16TempVar = pEvt5Log->ReguError;
					pPayload[RADIO__MSGCME5LOG_REGUERROR_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME5LOG_REGUERROR_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt5Log->Active;
					pPayload[RADIO__MSGCME5LOG_ACTIVE_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME5LOG_ACTIVE_L] = (Uint8) (u16TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME5LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME5LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME5LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME5LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_CURVEDATA: {
					// get pointer to log buffer
					cm_EvtRecCurveData *pEvt6Log = (cm_EvtRecCurveData*) pInst->logBuff;

					u16TempVar = pEvt6Log->AlgNo_new;
					pPayload[RADIO__MSGCME6LOG_ALGNO_NEW_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_ALGNO_NEW_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->AlgNo_old;
					pPayload[RADIO__MSGCME6LOG_ALGNO_OLD_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_ALGNO_OLD_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Capacity_new;
					pPayload[RADIO__MSGCME6LOG_CAPACITY_NEW_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_CAPACITY_NEW_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Capacity_old;
					pPayload[RADIO__MSGCME6LOG_CAPACITY_OLD_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_CAPACITY_OLD_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Cells_new;
					pPayload[RADIO__MSGCME6LOG_CELLS_NEW_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_CELLS_NEW_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Cells_old;
					pPayload[RADIO__MSGCME6LOG_CELLS_OLD_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_CELLS_OLD_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Ri_new;
					pPayload[RADIO__MSGCME6LOG_CABLERES_NEW_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_CABLERES_NEW_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Ri_old;
					pPayload[RADIO__MSGCME6LOG_CABLERES_OLD_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_CABLERES_OLD_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Baseload_new;
					pPayload[RADIO__MSGCME6LOG_BASELOAD_NEW_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_BASELOAD_NEW_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt6Log->Baseload_old;
					pPayload[RADIO__MSGCME6LOG_BASELOAD_OLD_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME6LOG_BASELOAD_OLD_L] = (Uint8) (u16TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME6LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME6LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME6LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME6LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_VCC: {
					// get pointer to log buffer
					cm_EvtRecVcc *pEvt7Log = (cm_EvtRecVcc*) pInst->logBuff;

					u8TempVar = pEvt7Log->VccStatus;
					pPayload[RADIO__MSGCME7LOG_VCC] = (Uint8) (u8TempVar);

					u8TempVar = pEvt7Log->Active;
					pPayload[RADIO__MSGCME7LOG_ACTIVE] = (Uint8) (u8TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME7LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME7LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME7LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME7LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_VOLTAGECALIBRATION: {
					// get pointer to log buffer
					cm_EvtRecVolCalib *pEvt8Log = (cm_EvtRecVolCalib*) pInst->logBuff;

					u32TempVar = pEvt8Log->UslopeNew;
					pPayload[RADIO__MSGCME8LOG_USLOPE_NEW_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME8LOG_USLOPE_NEW_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME8LOG_USLOPE_NEW_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME8LOG_USLOPE_NEW_L] = (Uint8) (u32TempVar);

					u32TempVar = pEvt8Log->UslopeOld;
					pPayload[RADIO__MSGCME8LOG_USLOPE_OLD_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME8LOG_USLOPE_OLD_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME8LOG_USLOPE_OLD_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME8LOG_USLOPE_OLD_L] = (Uint8) (u32TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME8LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME8LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME8LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME8LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_CURRENTCALIBRATION: {
					// get pointer to log buffer
					cm_EvtRecCurrCalib *pEvt9Log = (cm_EvtRecCurrCalib*) pInst->logBuff;

					u32TempVar = pEvt9Log->IslopeNew;
					pPayload[RADIO__MSGCME9LOG_ISLOPE_NEW_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME9LOG_ISLOPE_NEW_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME9LOG_ISLOPE_NEW_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME9LOG_ISLOPE_NEW_L] = (Uint8) (u32TempVar);

					u32TempVar = pEvt9Log->IslopeOld;
					pPayload[RADIO__MSGCME9LOG_ISLOPE_OLD_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME9LOG_ISLOPE_OLD_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME9LOG_ISLOPE_OLD_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME9LOG_ISLOPE_OLD_L] = (Uint8) (u32TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME9LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME9LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME9LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME9LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_STARTNETWORK: {
					pInst->parent.parent.usedTxBuff += RADIO__MSGCME2LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_JOINNETWORK: {
					pInst->parent.parent.usedTxBuff += RADIO__MSGCME2LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_STARTUP: {
					// get pointer to log buffer
					cm_EvtRecStartUp *pEvt12Log = (cm_EvtRecStartUp*) pInst->logBuff;

					u32TempVar = pEvt12Log->MainFwType;
					pPayload[RADIO__MSGCME12LOG_MAINFWTYPE_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME12LOG_MAINFWTYPE_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME12LOG_MAINFWTYPE_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME12LOG_MAINFWTYPE_L] = (Uint8) (u32TempVar);

					u32TempVar = pEvt12Log->MainFwVer;
					pPayload[RADIO__MSGCME12LOG_MAINFWVER_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME12LOG_MAINFWVER_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME12LOG_MAINFWVER_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME12LOG_MAINFWVER_L] = (Uint8) (u32TempVar);

					u32TempVar = pEvt12Log->RadioFwType;
					pPayload[RADIO__MSGCME12LOG_RADIOFWTYPE_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME12LOG_RADIOFWTYPE_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME12LOG_RADIOFWTYPE_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME12LOG_RADIOFWTYPE_L] = (Uint8) (u32TempVar);

					u32TempVar = pEvt12Log->RadioFwVer;
					pPayload[RADIO__MSGCME12LOG_RADIOFWVER_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME12LOG_RADIOFWVER_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME12LOG_RADIOFWVER_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME12LOG_RADIOFWVER_L] = (Uint8) (u32TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME12LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME12LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME12LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME12LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_SETTINGS: {
					pInst->parent.parent.usedTxBuff += RADIO__MSGCME13LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_ASSERT: {
					// get pointer to log buffer
					cm_EvtRecAssert *pEvt14Log = (cm_EvtRecAssert*) pInst->logBuff;

					/* Get Asssert Id */
					u8TempVar = pEvt14Log->AssertId;
					pPayload[RADIO__MSGCME14LOG_ASSERTID] = (Uint8) (u8TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME14LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME14LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME14LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME14LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_SUPERROR: {
					// get pointer to log buffer
					cm_EvtRecSupError *pEvt15Log = (cm_EvtRecSupError*) pInst->logBuff;

					u16TempVar = pEvt15Log->SupError;
					pPayload[RADIO__MSGCME15LOG_SUPERROR_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME15LOG_SUPERROR_L] = (Uint8) (u16TempVar);

					u16TempVar = pEvt15Log->Active;
					pPayload[RADIO__MSGCME15LOG_ACTIVE_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCME15LOG_ACTIVE_L] = (Uint8) (u16TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME15LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME15LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME15LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME15LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				case CM_EVENT_CABLERISLOPE: {
					// get pointer to log buffer
					cm_EvtRecCableRiSlope *pEvt16Log = (cm_EvtRecCableRiSlope*) pInst->logBuff;

					u32TempVar = pEvt16Log->RiSlopeNew;
					pPayload[RADIO__MSGCME16LOG_RISLOPE_NEW_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME16LOG_RISLOPE_NEW_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME16LOG_RISLOPE_NEW_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME16LOG_RISLOPE_NEW_L] = (Uint8) (u32TempVar);

					u32TempVar = pEvt16Log->RiSlopeOld;
					pPayload[RADIO__MSGCME16LOG_RISLOPE_OLD_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCME16LOG_RISLOPE_OLD_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCME16LOG_RISLOPE_OLD_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCME16LOG_RISLOPE_OLD_L] = (Uint8) (u32TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCME16LOG_U32SPARE1, radio__hU32Spare);
					radio__acBuildU16(pPayload, RADIO__MSGCME16LOG_U16SPARE1, radio__hU16Spare);
					reg_get(&pPayload[RADIO__MSGCME16LOG_U8SPARE1], radio__hU8Spare);

					pInst->parent.parent.usedTxBuff += RADIO__MSGCME16LOG_SIZE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
				}
				default:
					retVal = RADIO__ERR_PROCOK;
					break;
			} // end switch
		}

		if (retVal == RADIO__ERR_PROCOK_RESP) {
			control = segmentTotal - segmentIndex;

			if (segmentTotal == 1) {
				control = RADIO__MSGHCTRL_SINGLE;
			} else if (segmentIndex == 0) {
				control += RADIO__MSGHCTRL_FIRST;
			}
		} else {
			/*
			 * Record or record type not found
			 * Build error message
			 */
			pPayload[RADIO__MSGCMLOG_LOGTYPE] = 0xFF;
			// set payload offset to point at first element of specific Event
			pPayload += RADIO__MSGCMLOG_LOGDATA;
			// increase payload size
			pInst->parent.parent.usedTxBuff = 1;
			pInst->recCount = 0;
			control = RADIO__MSGHCTRL_SINGLE;
			retVal = RADIO__ERR_PROCOK_RESP;
		}

		// Send message
		radio__acBuildHeader(pInst, RADIO__ACCMD_CMLOG,
			pInst->segmentedMessage.seqNum, control);
		pInst->pLastMsg = &pInst->segmentedMessage;
	} else {
		/*
		 * No records to send
		 */
		retVal = RADIO__ERR_PROCOK;
	}

	return retVal;
}

/*******************************************************************************
---------------- radio_acProcInstantLog ----------------------------------------
  Build Instant log message and call function to send it
--------------------------------------------------------------------------------
*******************************************************************************/
PRIVATE radio__ErrCode radio_acProcInstantLog(radio__AcInst * pInst,
		const uint8_t segment) {
	if (segment == 0) {
		return radio__acBuildInstLog(pInst);
	} else {
		uint8_t segmentTotal = 1 + (pInst->recCount / RADIO_INSTLOG_PER_SEGMENT);
		uint8_t segmentIndex = segmentTotal - segment;
		uint8_t recordOffset = RADIO_INSTLOG_HEADER_SEGMENT +
				((segmentIndex - 1) * RADIO_INSTLOG_PER_SEGMENT);

		// Validate data
		if (segment > segmentTotal) {
			return RADIO__ERR_PROCOK;
		}

		/* Read log */
		pInst->recordOffset = recordOffset;
		pInst->logRequest.index = pInst->recordNumber + pInst->recordOffset;
		pInst->logRequest.flags = CM_REQFLG_READ;
		cm_processRequest(&pInst->logRequest);
	}

	return RADIO__ERR_PROCOK;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acBuildInstLog
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue to process get log message.
*
*	\param		pInst	Pointer to instance.
*	\param		segment	Segment to read.
*
*	\retval		TRUE	If the get log message has been handled.
*	\retval		FALSE	If still handling the get log message.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE radio__ErrCode radio__acBuildInstLog(radio__AcInst * pInst) {
	static Uint16 InstLogRecordType = 0;
	radio__ErrCode retVal;
	Uint8 control = 0;

	if (pInst->recCount) {
		BYTE * pPayload;

		if (!(pInst->logRequest.flags & CM_REQFLG_SUCCESS)) {
			/*
			 * The record could not be found. Some records have already been
			 * found. Just send the found records.
			 */
			// Get pointer to payload
			pPayload = pInst->parent.parent.pTxSegBuff;
			// Set log type
			pPayload[RADIO__MSGCMLOG_LOGTYPE] = 0xFF;
			// set payload offset to point at first element of specific Event
			pPayload += RADIO__MSGCMLOG_LOGDATA;
			// increase payload size
			pInst->parent.parent.usedSegBuff = 1;
			pInst->recCount = 0;
			control = RADIO__MSGHCTRL_SINGLE;
			retVal = RADIO__ERR_PROCOK_RESP;
		} else { //log read from flash OK
			Uint32 InstLogHdrTime;
			Uint32 InstLogHdrIdx;
			Uint32 InstLogIdxOffset;
			Uint32 u32TempVar;
			Uint16 u16TempVar;
			Uint16 InstLogHdrSP;

			if (pInst->recordOffset == 0) {
				Uint8 InstLogRecordSize;

				// Get pointer to payload
				pPayload = pInst->parent.parent.pTxSegBuff;
				// Build header
				pPayload[RADIO__MSGCMLOG_LOGTYPE] = RADIO__LOGTYPE_INSTANT;
				// set payload offset to point at first element of specific Event
				pPayload += RADIO__MSGCMLOG_LOGDATA;
				// increase payload size
				pInst->parent.parent.usedSegBuff = RADIO__MSGCMLOG_LOGDATA;

				// use start index from request
				u32TempVar = pInst->logRequest.index;
				pPayload[RADIO__MSGCMILOG_INDEX_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMILOG_INDEX_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMILOG_INDEX_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMILOG_INDEX_L] = (Uint8) (u32TempVar);

				reg_get(&InstLogHdrTime,radio__hInstLogHdrTime);
				reg_get(&InstLogHdrIdx,radio__hInstLogHdrIndex);
				reg_get(&InstLogHdrSP,radio__hInstLogHdrSamplePer);

				InstLogIdxOffset = (pInst->logRequest.index - InstLogHdrIdx);
				u32TempVar = InstLogHdrTime + (Uint32)(InstLogIdxOffset * InstLogHdrSP);

				pPayload[RADIO__MSGCMILOG_TIME_H] = (Uint8) (u32TempVar >> 24);
				pPayload[RADIO__MSGCMILOG_TIME_HM] = (Uint8) (u32TempVar >> 16);
				pPayload[RADIO__MSGCMILOG_TIME_LM] = (Uint8) (u32TempVar >> 8);
				pPayload[RADIO__MSGCMILOG_TIME_L] = (Uint8) (u32TempVar);

				reg_get(&u16TempVar, radio__hInstLogHdrSamplePer);
				pPayload[RADIO__MSGCMILOG_SAMPLEPERIOD_H] = (Uint8) (u16TempVar >> 8);
				pPayload[RADIO__MSGCMILOG_SAMPLEPERIOD_L] = (Uint8) (u16TempVar);

				reg_get(&InstLogRecordType, radio__hInstLogHdrRecType);
				pPayload[RADIO__MSGCMILOG_RECORDTYPE_H] = (Uint8) (InstLogRecordType >> 8);
				pPayload[RADIO__MSGCMILOG_RECORDTYPE_L] = (Uint8) (InstLogRecordType);

				reg_get(&InstLogRecordSize, radio__hInstLogHdrRecSize);
				pPayload[RADIO__MSGCMILOG_RECORDSIZE] = (Uint8) (InstLogRecordSize);

				// Get number of records to send
				if (pInst->recCount > MAX_INST_RECORDS) {
					pInst->recCount = MAX_INST_RECORDS;
				}

				pPayload[RADIO__MSGCMILOG_RECORDCOUNT] = (Uint8) (pInst->recCount);

				// increase payload size
				pInst->parent.parent.usedSegBuff += RADIO__MSGCMILOG_SIZE;
				retVal = RADIO__ERR_PROCOK;
			}

			// Get pointer to payload
			pPayload = pInst->parent.parent.pTxSegBuff;

			// Get log data depending on record type
			switch (InstLogRecordType) {
				case CM_INSTREC_STANDARD: {
					// get pointer to log buffer
					cm_InstRecStd *pInstLogStd = (cm_InstRecStd*) pInst->logBuff;

					// Fault handling, abort message
					if (pInst->parent.parent.usedSegBuff > 63) {
						pInst->parent.parent.usedSegBuff = 0;
						pInst->segmentedMessage.pProcFn = NULL;
						retVal = RADIO__ERR_PROCOK;
						pInst->recCount = 0;
						break;
					}

					// Get offset
					pPayload += pInst->parent.parent.usedSegBuff;
					pPayload[RADIO__MSGCMI1LOG_RECORDNO] = pInst->recordOffset;

					u16TempVar = pInstLogStd->ChalgError;
					pPayload[RADIO__MSGCMI1LOG_CHALGERROR_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCMI1LOG_CHALGERROR_L] = (Uint8) (u16TempVar);

					u16TempVar = pInstLogStd->ReguError.Sum;
					pPayload[RADIO__MSGCMI1LOG_REGUERROR_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCMI1LOG_REGUERROR_L] = (Uint8) (u16TempVar);

					u32TempVar = pInstLogStd->Uchalg;
					pPayload[RADIO__MSGCMI1LOG_UCHALG_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCMI1LOG_UCHALG_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCMI1LOG_UCHALG_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCMI1LOG_UCHALG_L] = (Uint8) (u32TempVar);

					u32TempVar = pInstLogStd->Ichalg;
					pPayload[RADIO__MSGCMI1LOG_ICHALG_H] = (Uint8) (u32TempVar >> 24);
					pPayload[RADIO__MSGCMI1LOG_ICHALG_HM] = (Uint8) (u32TempVar >> 16);
					pPayload[RADIO__MSGCMI1LOG_ICHALG_LM] = (Uint8) (u32TempVar >> 8);
					pPayload[RADIO__MSGCMI1LOG_ICHALG_L] = (Uint8) (u32TempVar);

					u16TempVar = pInstLogStd->Tboard;
					pPayload[RADIO__MSGCMI1LOG_TBOARD_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCMI1LOG_TBOARD_L] = (Uint8) (u16TempVar);

					u16TempVar = pInstLogStd->Ths;
					pPayload[RADIO__MSGCMI1LOG_THS_H] = (Uint8) (u16TempVar >> 8);
					pPayload[RADIO__MSGCMI1LOG_THS_L] = (Uint8) (u16TempVar);

					/* Spare data set to zero */
					radio__acBuildU32(pPayload, RADIO__MSGCMI1LOG_U32SPARE1, radio__hU32Spare);

					// set payload offset to point at first element of specific Event
					pPayload += RADIO__MSGCMI1LOG_SIZE;

					// increase payload size
					pInst->parent.parent.usedSegBuff += RADIO__MSGCMI1LOG_SIZE;
					pInst->recordOffset++;
					retVal = RADIO__ERR_PROCOK_RESP;

					// All logs sent
					if (pInst->recordOffset == pInst->recCount) {
						control = 1;
					} else if (pInst->recordOffset == RADIO_INSTLOG_HEADER_SEGMENT) { // Header packet full
						control = 1 + (pInst->recCount / RADIO_INSTLOG_PER_SEGMENT);

						if (pInst->recCount > RADIO_INSTLOG_HEADER_SEGMENT) {
							control += RADIO__MSGHCTRL_FIRST;
						}
					} else if ((pInst->recordOffset > RADIO_INSTLOG_HEADER_SEGMENT) &&
							((pInst->recordOffset - RADIO_INSTLOG_HEADER_SEGMENT) %
									RADIO_INSTLOG_PER_SEGMENT == 0)) { // Segment full
						uint8_t logStartRecord = pInst->recordOffset - RADIO_INSTLOG_PER_SEGMENT;
						control = (RADIO_INSTLOG_HEADER_SEGMENT + pInst->recCount - logStartRecord) / RADIO_INSTLOG_PER_SEGMENT;
					} else {
						if (pInst->recordOffset != pInst->recCount) {
							//Fetch the next record.
							pInst->logRequest.flags = CM_REQFLG_READNEXT;
							cm_processRequest(&pInst->logRequest);
						}
						return RADIO__ERR_PROCOK;
					}

					if (pInst->recCount <= RADIO_INSTLOG_HEADER_SEGMENT) {
						control = RADIO__MSGHCTRL_SINGLE;
					}
					break;
				}
				default:
					/*
					 * The recordType could not be found.
					 */
					pPayload[RADIO__MSGCMLOG_LOGTYPE] = 0xFF;
					// set payload offset to point at first element of specific Event
					pPayload += RADIO__MSGCMLOG_LOGDATA;
					// increase payload size
					pInst->parent.parent.usedSegBuff += RADIO__MSGCMLOG_LOGDATA;
					pInst->recCount = 0;
					control = RADIO__MSGHCTRL_SINGLE;
					retVal = RADIO__ERR_PROCOK_RESP;
					break;
			} // end switch
		}

		if (retVal == RADIO__ERR_PROCOK_RESP) {
			BYTE* pTxBuffer = radio__acGetPayloadPointer((radio__Inst *) pInst);
			pTxBuffer += RADIO__MSGH_DATA;

			memcpy(pTxBuffer, pInst->parent.parent.pTxSegBuff,
					pInst->parent.parent.usedSegBuff);
			pInst->parent.parent.usedTxBuff = pInst->parent.parent.usedSegBuff;
			pInst->parent.parent.usedSegBuff = 0;

			radio__acBuildHeader(pInst, RADIO__ACCMD_CMLOG,
					pInst->segmentedMessage.seqNum, control);
		} else {
			//Fetch the next record.
			pInst->logRequest.flags = CM_REQFLG_READNEXT;
			cm_processRequest(&pInst->logRequest);
		}
	} else {
		/*
		 * No records to send
		 */
		retVal = RADIO__ERR_PROCOK;
	}

	if (retVal == RADIO__ERR_PROCOK_RESP) {
		pInst->pLastMsg = &pInst->segmentedMessage;
	}

	return (retVal);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__acGetPayloadPointer
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns a pointer to the payload part of the TX buffer.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		Pointer to payload buffer for a data request.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC BYTE * radio__acGetPayloadPointer(
	radio__Inst *			pInst
) {
	radio__AcInst *pAcInst = (radio__AcInst*)pInst;
	if(pAcInst->testCmd)
		return(&pInst->pTxBuffer[RADIO__PCK_DATA]);
	else
		return(&pInst->pTxBuffer[RADIO__PCK_DATA + RADIO__PCKMPDR_PAYLOAD]);
}
