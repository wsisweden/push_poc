/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		radio_pn.h
*
*	\ingroup	RADIO
*
*	\brief		
*
*	\details
*
*	\note
*
*	\version	\$Rev: 521 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef RADIO_PN_H_INCLUDED
#define RADIO_PN_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "sys.h"
#include "tstamp.h"
#include "rtc.h"

#include "Radio.h"
#include "init.h"
#include "local.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * \name	PopNet instance flags
 *
 * \brief	Flags used in the flags field of the PopNet gateway instance struct.
 */

/*@{*/
#define RADIO__PNFLAG_RESEND	(1<<0)	/**< Resend the message				*/
#define RADIO__PNFLAG_JOINENA	(1<<1)	/**< Join enabled					*/
#define RADIO__PNFLAG_BMPARAM	(1<<2)	/**< SetBmParam sent				*/
#define RADIO__PNFLAG_KEYMON	(1<<3)	/**< Keyboard monitoring enabled	*/
/*@{*/

#define RADIO__BYTE_MPTEST		0x54	/**< MP test group byte				*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/**
 * Information needed by the production test module.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	tstamp_Stamp			testTime;	/**< Timestamp used by test code	*/
	Uint16					pressedKeys;/**< Keyboard monitor flags			*/
	Uint16					oldKeys;	/**< Previous pressed key flags.	*/
	Uint16					testTimeout;/**< Timeout time for active test	*/
	Uint8					activeTest;	/**< Active test ID					*/
	Uint8					segment;	/**< Test segment number			*/
} radio__TestData;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway specific variables
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	radio__Inst				parent;		/**< Parent instance				*/
	radio__TestData			testData;	/**< Test module data.				*/
	tstamp_Stamp			sentTime;	/**< Time when request was sent.	*/
	//tstamp_Stamp			joinEna;	/**< Time when join enable was started*/
	Posix_Time				joinEna;	/**< Time when join enable was started*/
	//Uint16					dstAddr;	/**< Last used destination address.	*/
	Uint8					checksum;	/**< Checksum counter.				*/
	Uint8					command;	/**< Active command					*/
	Uint8					flags;		/**< State flags					*/
	Uint8					retryCntr;	/**< Retry counter					*/
} radio__PnInst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/	

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
