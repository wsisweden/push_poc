
#include "tools.h"

#include "../radio_pn.h"
#include "../local.h"








/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateGetConfigPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Builds a get config request packet on the rx buffer.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		-
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateGetConfigPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

#if 0
		/** STX character. The start of each serial gateway packet. Always 0x50. */
		RADIO__PCK_STX = 0,

		/** PopNet Group. Always 0x50. */
		RADIO__PCK_PROTGRP,	

		/** Event ID. */
		RADIO__PCK_EVTID,

		/** Lenght of data, depending on Event ID. Length 0x00...0xFF. */
		RADIO__PCK_LEN,

		RADIO__PCKMPDI_SRCADDR_H,	/* Node source address high byte	*/
		RADIO__PCKMPDI_SRCADDR_L,	/* Node source address low byte	*/
		RADIO__PCKMPDI_LEN,			/* Length of payload to come.		*/

		/**
		 * A command byte to identify the data to come.
		 */
		RADIO__MSGH_CMD = 0,

		/**
		 * If the message is a request (trigged from application) the MpSequence
		 * number is taken from the local MpSequence counter. But if the message is
		 * a response then the MpSequence number is taken from the incoming
		 * request message MpSequence number.
		 */
		RADIO__MSGH_MPSEQ,

		/**
		 * Control byte which indicated if message is segmented or not.
		 * 0 = Single message
		 * 1 = segmented, first message
		 * 2 = segmented, consecutive message
		 * 3 = segmented last message
		 */
		RADIO__MSGH_CONTROL,
#endif

	pRxBuffer = pInst->pRxBuffer;
	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x50;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__EVTID_DATAIND;
	pRxBuffer[RADIO__PCK_LEN] = 0x0C;

	pRxBuffer[RADIO__PCK_DATA] = 0x00;		/* src addr */
	pRxBuffer[RADIO__PCK_DATA+1] = 0x00;	/* src addr */
	pRxBuffer[RADIO__PCK_DATA+2] = 0x03;	/* payload length */

	pRxBuffer[RADIO__PCK_DATA+3] = 0x62; /* Access OTA command. 0x62= GET_CONFIG */
	pRxBuffer[RADIO__PCK_DATA+4] = 0x01; /* Sequence number */
	pRxBuffer[RADIO__PCK_DATA+5] = 0x00; /* Control byte */

	pRxBuffer[RADIO__PCK_DATA+6] = 0xB6;	/* checksum			*/

	pInst->usedBuff = 11;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateMpNwkDataRspPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Builds a get MpNwkDataRsp packet on the rx buffer.
*
*	\param		pInst		Pointer to FB instance.
*	\param		status		Frame status field.
*
*	\return		
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateMpNwkDataRspPacket(
	radio__Inst *			pInst,
	Uint8					status
) {
	BYTE *					pRxBuffer;

#if 0
		/** STX character. The start of each serial gateway packet. Always 0x50. */
		RADIO__PCK_STX = 0,

		/** PopNet Group. Always 0x50. */
		RADIO__PCK_PROTGRP,	

		/** Event ID. */
		RADIO__PCK_EVTID,

		/** Lenght of data, depending on Event ID. Length 0x00...0xFF. */
		RADIO__PCK_LEN
#endif

	pRxBuffer = pInst->pRxBuffer;
	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x50;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__EVTID_NWKDATARSP;
	pRxBuffer[RADIO__PCK_LEN] = 0x01;

	pRxBuffer[RADIO__PCK_DATA] = status;		/* src addr			*/

	pRxBuffer[RADIO__PCK_DATA+1] = 0xD4 ^ status;/* checksum			*/

	pInst->usedBuff = 6;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateLogRequestPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Builds a log request packet on the rx/tx buffer.
*
*	\param		pInst		Pointer to FB instance.
*	\param		index		Log record index.
*	\param		count		Number of records.
*	\param		logType		Selects which log should be used.
*							- 0: historical log
*							- 1: Event log
*							- 2: Instant log
*
*	\return		
*
*	\details	This function can be used to simulate a historical log request.
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateLogRequestPacket(
	radio__Inst *			pInst,
	Uint32					index,
	Uint8					count,
	Uint8					logType
) {
	BYTE *					pRxBuffer;

#if 0
		/** STX character. The start of each serial gateway packet. Always 0x50. */
		RADIO__PCK_STX = 0,

		/** PopNet Group. Always 0x50. */
		RADIO__PCK_PROTGRP,	

		/** Event ID. */
		RADIO__PCK_EVTID,

		/** Lenght of data, depending on Event ID. Length 0x00...0xFF. */
		RADIO__PCK_LEN,

		RADIO__PCKMPDI_SRCADDR_H,	/* Node source address high byte	*/
		RADIO__PCKMPDI_SRCADDR_L,	/* Node source address low byte	*/
		RADIO__PCKMPDI_LEN,			/* Length of payload to come.		*/

		/**
		 * A command byte to identify the data to come.
		 */
		RADIO__MSGH_CMD = 0,

		/**
		 * If the message is a request (trigged from application) the MpSequence
		 * number is taken from the local MpSequence counter. But if the message is
		 * a response then the MpSequence number is taken from the incoming
		 * request message MpSequence number.
		 */
		RADIO__MSGH_MPSEQ,

		/**
		 * Control byte which indicated if message is segmented or not.
		 * 0 = Single message
		 * 1 = segmented, first message
		 * 2 = segmented, consecutive message
		 * 3 = segmented last message
		 */
		RADIO__MSGH_CONTROL,

		RADIO__MSGGLOG_LOGTYPE = 0, /**< Log type						*/
		RADIO__MSGGLOG_RECORDNO_H,	/**< Index of start record			*/
		RADIO__MSGGLOG_RECORDNO_HM, /**< Index of start record			*/
		RADIO__MSGGLOG_RECORDNO_LM, /**< Index of start record			*/
		RADIO__MSGGLOG_RECORDNO_L,	/**< Index of start record			*/
		RADIO__MSGGLOG_NOOFRECORDS, /**< Number of records				*/

		/** XOR sum of all bytes in the packet. */
		RADIO__PCK_CHKSUM = 
#endif

	pRxBuffer = pInst->pRxBuffer;
	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x50;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__EVTID_DATAIND;
	pRxBuffer[RADIO__PCK_LEN] = 0x0C;

	pRxBuffer[RADIO__PCK_DATA] = 0x00;		/* src addr */
	pRxBuffer[RADIO__PCK_DATA+1] = 0x00;	/* src addr */
	pRxBuffer[RADIO__PCK_DATA+2] = 0x09;	/* payload length */

	pRxBuffer[RADIO__PCK_DATA+3] = 0x63; /* Access OTA command. 0x63 = GET_LOG */
	pRxBuffer[RADIO__PCK_DATA+4] = 0x01; /* Sequence number */
	pRxBuffer[RADIO__PCK_DATA+5] = 0x00; /* Control byte */

	pRxBuffer[RADIO__PCK_DATA+6] = logType; 
	pRxBuffer[RADIO__PCK_DATA+7] = (Uint8) (index >> 24);
	pRxBuffer[RADIO__PCK_DATA+8] = (Uint8) (index >> 16);
	pRxBuffer[RADIO__PCK_DATA+9] = (Uint8) (index >> 8);
	pRxBuffer[RADIO__PCK_DATA+10] = (Uint8) (index);
	pRxBuffer[RADIO__PCK_DATA+11] = count;

	pRxBuffer[RADIO__PCK_DATA+12] = 0x69;	/* checksum			*/

	pInst->usedBuff = 17;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateGetStatusPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Builds a GetStatus request packet on the rx buffer.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		-
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateGetStatusPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

#if 0
		/** STX character. The start of each serial gateway packet. Always 0x50. */
		RADIO__PCK_STX = 0,

		/** PopNet Group. Always 0x50. */
		RADIO__PCK_PROTGRP,	

		/** Event ID. */
		RADIO__PCK_EVTID,

		/** Lenght of data, depending on Event ID. Length 0x00...0xFF. */
		RADIO__PCK_LEN,

		RADIO__PCKMPDI_SRCADDR_H,	/* Node source address high byte	*/
		RADIO__PCKMPDI_SRCADDR_L,	/* Node source address low byte	*/
		RADIO__PCKMPDI_LEN,			/* Length of payload to come.		*/

		/**
		 * A command byte to identify the data to come.
		 */
		RADIO__MSGH_CMD = 0,

		/**
		 * If the message is a request (trigged from application) the MpSequence
		 * number is taken from the local MpSequence counter. But if the message is
		 * a response then the MpSequence number is taken from the incoming
		 * request message MpSequence number.
		 */
		RADIO__MSGH_MPSEQ,

		/**
		 * Control byte which indicated if message is segmented or not.
		 * 0 = Single message
		 * 1 = segmented, first message
		 * 2 = segmented, consecutive message
		 * 3 = segmented last message
		 */
		RADIO__MSGH_CONTROL,
#endif

	pRxBuffer = pInst->pRxBuffer;
	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x50;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__EVTID_DATAIND;
	pRxBuffer[RADIO__PCK_LEN] = 0x0C;

	pRxBuffer[RADIO__PCK_DATA] = 0x00;		/* src addr			*/
	pRxBuffer[RADIO__PCK_DATA+1] = 0x00;	/* src addr			*/
	pRxBuffer[RADIO__PCK_DATA+2] = 0x03;	/* payload length	*/

	pRxBuffer[RADIO__PCK_DATA+3] = 0x60; /* Access OTA command. 0x60= GET_STATUS */
	pRxBuffer[RADIO__PCK_DATA+4] = 0x01;	/* Sequence number	*/
	pRxBuffer[RADIO__PCK_DATA+5] = 0x00;	/* Control byte		*/

	pRxBuffer[RADIO__PCK_DATA+6] = 0x00;	/* checksum			*/

	pInst->usedBuff = 11;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateTestSwitchesPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Builds a log request packet on the rx/tx buffer.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	This function can be used to simulate a historical log request.
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateTestSwitchesPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

	pRxBuffer = pInst->pRxBuffer;

	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x54;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__MPTEST_SWITCHES;
	pRxBuffer[RADIO__PCK_LEN] = 0x02;

	/*
	 *	Set 4096ms timeout.
	 */

	pRxBuffer[RADIO__PCK_DATA] = 0x10;		/* timeout high byte */
	pRxBuffer[RADIO__PCK_DATA+1] = 0x00;	/* timeout low byte */

	pRxBuffer[RADIO__PCK_DATA+3] = 0x00;	/* checksum			*/

	pInst->usedBuff = 7;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateTestLedsPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Builds a test leds packet on the rx/tx buffer.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateTestLedsPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

	pRxBuffer = pInst->pRxBuffer;

	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x54;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__MPTEST_LEDTEST;
	pRxBuffer[RADIO__PCK_LEN] = 0x02;

	/*
	 *	Set 4096ms timeout.
	 */

	pRxBuffer[RADIO__PCK_DATA] = 0xaa;		/* timeout high byte */
	pRxBuffer[RADIO__PCK_DATA+1] = 0xaa;	/* timeout low byte */
	pRxBuffer[RADIO__PCK_DATA+3] = 0x42;	/* checksum			*/

	pInst->usedBuff = 7;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateStartTestPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Builds a log request packet on the rx/tx buffer.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	This function can be used to simulate a historical log request.
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateStartTestPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

	pRxBuffer = pInst->pRxBuffer;

	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x54;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__MPTEST_START;
	pRxBuffer[RADIO__PCK_LEN] = 0x01;
	pRxBuffer[RADIO__PCK_DATA] = 0x01;		/* Enable test */
	pRxBuffer[RADIO__PCK_DATA+1] = 0x00;	/* checksum			*/

	pInst->usedBuff = 6;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateTestGetConfigPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	This function can be used to simulate a historical log request.
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateTestGetConfigPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

	pRxBuffer = pInst->pRxBuffer;

	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x54;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__MPTEST_GETCONF;
	pRxBuffer[RADIO__PCK_LEN] = 0x03;
	pRxBuffer[RADIO__PCK_DATA] = 0x62;		/* Get config command	*/
	pRxBuffer[RADIO__PCK_DATA+1] = 0x00;	/* sequence number		*/
	pRxBuffer[RADIO__PCK_DATA+2] = 0x00;	/* Single message		*/
	pRxBuffer[RADIO__PCK_DATA+3] = 0x00;	/* Checksum				*/
	pInst->usedBuff = 8;

#if 0
	RADIO__MSGH_CMD = 0,

	/**
	 * If the message is a request (trigged from application) the MpSequence
	 * number is taken from the local MpSequence counter. But if the message is
	 * a response then the MpSequence number is taken from the incoming
	 * request message MpSequence number.
	 */
	RADIO__MSGH_MPSEQ,

	/**
	 * Control byte which indicated if message is segmented or not.
	 * 0 = Single message
	 * 1 = segmented, first message
	 * 2 = segmented, consecutive message
	 * 3 = segmented last message
	 */
	RADIO__MSGH_CONTROL,
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateTestGetHsTempPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	This function can be used to simulate a historical log request.
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateTestGetHsTempPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

	pRxBuffer = pInst->pRxBuffer;

	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x54;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__MPTEST_HSTEMP;
	pRxBuffer[RADIO__PCK_LEN] = 0x00;
	pRxBuffer[RADIO__PCK_DATA] = 0x75;	/* Checksum				*/
	pInst->usedBuff = 5;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__simulateTestUmeasPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details	This function can be used to simulate a historical log request.
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__simulateTestUmeasPacket(
	radio__Inst *			pInst
) {
	BYTE *					pRxBuffer;

	pRxBuffer = pInst->pRxBuffer;

	/* $02$54$1D$04$40$00$00$00$0d */

	pRxBuffer[RADIO__PCK_STX] = 0x02;
	pRxBuffer[RADIO__PCK_PROTGRP] = 0x54;
	pRxBuffer[RADIO__PCK_EVTID] = RADIO__MPTEST_UMEAS;
	pRxBuffer[RADIO__PCK_LEN] = 0x04;
	pRxBuffer[RADIO__PCK_DATA] = 0x40;		/* float gain	2.0			*/
	pRxBuffer[RADIO__PCK_DATA+1] = 0x00;	/* float gain	2.0			*/
	pRxBuffer[RADIO__PCK_DATA+2] = 0x00;	/* float gain	2.0			*/
	pRxBuffer[RADIO__PCK_DATA+3] = 0x00;	/* float gain	2.0			*/
	pRxBuffer[RADIO__PCK_DATA+4] = 0x0d;	/* Checksum					*/
	pInst->usedBuff = 9;
}