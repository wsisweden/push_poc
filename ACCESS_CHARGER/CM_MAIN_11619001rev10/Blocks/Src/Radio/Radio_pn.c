/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Radio_pn.c
*
*	\ingroup	RADIO
*
*	\brief		PopNet Radio Gateway protocol implementation.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 522 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "mem.h"
#include "deb.h"
#include "sys.h"
#include "reg.h"
#include "tstamp.h"

#include "Sup.h"
#include "Radio.h"

#include "local.h"
#include "radio_pn.h"
/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * PopNet gateway start network request bytes.
 */

enum radio__pnStartNetReq {				/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKSNR_CHANNEL = 0,			/**< Channel						*/
	RADIO__PCKSNR_PANID_H,				/**< Panid high byte				*/
	RADIO__PCKSNR_PANID_L,				/**< Panid low byte					*/

	RADIO__PCKSNR_SIZE					/**< Request size					*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway start network response bytes.
 */

enum radio__pnStartNetRsp {				/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKSNET_STATUS,				/**< If command succeeded or not.	*/
	RADIO__PCKSNET_CHANNEL,				/**< Channel. Value in range 11...26*/
	RADIO__PCKSNET_PANID_H,				/**< PanId. high byte				*/
	RADIO__PCKSNET_PANID_L,				/**< PanId. low byte				*/
	RADIO__PCKSNET_NWKADDR_H,			/**< Network address of starter.	*/
	RADIO__PCKSNET_NWKADDR_L			/**< Network address of starter.	*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
* PopNet gateway join network request bytes.
*/

enum radio__pnJoinNetReq {				/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKJNR_CHANNEL = 0,			/**< Channel						*/
	RADIO__PCKJNR_PANID_H,				/**< Panid high byte				*/
	RADIO__PCKJNR_PANID_L,				/**< Panid low byte					*/

	RADIO__PCKJNR_SIZE					/**< Request size					*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway join network response bytes.
 */

enum radio__pnJoinNetRsp {				/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKJNET_STATUS,				/**< Join ok or not					*/
	RADIO__PCKJNET_CHANNEL,				/**< Channel. Value in range 11...26*/
	RADIO__PCKJNET_PANID_H,				/**< PanId. high byte				*/
	RADIO__PCKJNET_PANID_L,				/**< PanId. low byte				*/
	RADIO__PCKJNET_NWKID_H,				/**< Given network address.			*/
	RADIO__PCKJNET_NWKID_L,				/**< Given network address.			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway leave network response bytes.
 */

enum radio__pnMpLeaveNetworkRsp {		/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPLNRSP_STATUS			/**< 0=OK (left network).			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway scan network request bytes.
 */

enum radio__pnMpScanNetworkReq {		/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKSNRRQ_CHANNELLIST_H,		/**< Channel bitmask				*/
	RADIO__PCKSNRRQ_CHANNELLIST_L,		/**< Channel bitmask				*/

	RADIO__PCKSNRRQ_SIZE				/**< Size of scan network command	*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway scan network response bytes.
 */

enum radio__pnMpScanNetworkRsp {		/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKSNRRSP_CHANNEL,			/**< Channel bitmask				*/
	RADIO__PCKSNRRSP_PANID_H,			/**< Channel bitmask				*/
	RADIO__PCKSNRRSP_PANID_L,			/**< Channel bitmask				*/
	RADIO__PCKSNRRSP_NWKIDARRAY			/**< Array with found network
										 * addresses						*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway set radio param request bytes.
 */

enum radio__pnMpSetRadioParamReq {		/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPSRDOPAR_CHANNEL,		/**< Channel (11...26). 0 = auto	*/
	RADIO__PCKMPSRDOPAR_PANID_H,		/**< PAN ID (0x0000...0xFFF0)
										 * 0xFFFF = auto					*/
	RADIO__PCKMPSRDOPAR_PANID_L,		/**< PAN ID low byte				*/
	RADIO__PCKMPSRDOPAR_NWKADDR_H,		/**< Net address (0x0000-0xFFF0)	*/
	RADIO__PCKMPSRDOPAR_NWKADDR_L,		/**< Net address low byte			*/

	RADIO__PCKMPSRDOPAR_SIZE			/**< Size of the request			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway get radio data request bytes.
 */

enum radio__pnGetRdoDataReq {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKGRD_REQ_CHARGER_ID_H,		/**< Charger ID						*/
	RADIO__PCKGRD_REQ_CHARGER_ID_HM,	/**< Charger ID						*/
	RADIO__PCKGRD_REQ_CHARGER_ID_LM,	/**< Charger ID						*/
	RADIO__PCKGRD_REQ_CHARGER_ID_L,		/**< Charger ID						*/

	RADIO__PCKGRD_SIZE					/**< Size of the request			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway get radio data response bytes.
 */

enum radio__pnGetRdoDataRsp {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKGRD_FWTYPE_H,				/**< Radio firmware type			*/
	RADIO__PCKGRD_FWTYPE_HM,			/**< Radio firmware type			*/
	RADIO__PCKGRD_FWTYPE_LM,			/**< Radio firmware type			*/
	RADIO__PCKGRD_FWTYPE_L,				/**< Radio firmware type			*/
	RADIO__PCKGRD_FWVER_H,				/**< Radio firmware version			*/
	RADIO__PCKGRD_FWVER_HM,				/**< Radio firmware version			*/
	RADIO__PCKGRD_FWVER_LM,				/**< Radio firmware version			*/
	RADIO__PCKGRD_FWVER_L,				/**< Radio firmware version			*/
	RADIO__PCKGRD_CHKSUM_H,				/**< Radio firmware checksum		*/
	RADIO__PCKGRD_CHKSUM_HM,			/**< Radio firmware checksum		*/
	RADIO__PCKGRD_CHKSUM_LM,			/**< Radio firmware checksum		*/
	RADIO__PCKGRD_CHKSUM_L,				/**< Radio firmware checksum		*/
	RADIO__PCKGRD_CMID_0,				/**< Radio MAC address				*/
	RADIO__PCKGRD_CMID_1,				/**< Radio MAC address				*/
	RADIO__PCKGRD_CMID_2,				/**< Radio MAC address				*/
	RADIO__PCKGRD_CMID_3,				/**< Radio MAC address				*/
	RADIO__PCKGRD_CMID_4,				/**< Radio MAC address				*/
	RADIO__PCKGRD_CMID_5,				/**< Radio MAC address				*/
	RADIO__PCKGRD_CMID_6,				/**< Radio MAC address				*/
	RADIO__PCKGRD_CMID_7,				/**< Radio MAC address				*/
	RADIO__PCKGRD_MFGID_H,				/**< Manufacturer ID				*/
	RADIO__PCKGRD_MFGID_L,				/**< Manufacturer ID				*/
	RADIO__PCKGRD_APPID_H,				/**< Application ID					*/
	RADIO__PCKGRD_APPID_L,				/**< Application ID					*/
	RADIO__PCKGRD_CHANNEL,				/**< Channel number					*/
	RADIO__PCKGRD_PANID_H,				/**< PAN ID							*/
	RADIO__PCKGRD_PANID_L,				/**< PAN ID							*/
	RADIO__PCKGRD_NWKID_H,				/**< Network address				*/
	RADIO__PCKGRD_NWKID_L,				/**< Network address				*/
	RADIO__PCKGRD_ADDRPOOL_H,			/**< Address pool high byte			*/
	RADIO__PCKGRD_ADDRPOOL_L			/**< Address pool low byte			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway join network enabled request bytes.
 */

enum radio__pnMpJoinEnableReq {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPJNEREQ_ENABLE,			/**< 0=join not allowed, 1=allowed	*/
	RADIO__PCKMPJNEREQ_NETWIDE,			/**< 0=this node only, 1=broadcast	*/

	RADIO__PCKMPJNEREQ_SIZE
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway join network enabled response bytes.
 */

enum radio__pnMpJoinEnableRsp {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPJNERSP_ENABLE			/**< 0=join not allowed, 1=allowed	*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Sent from MC13224 as a response to MpNwkDataReq.
 */

enum radio__pnMpNwkDataRsp {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPDC_STATUS				/**< Message buffer status. This is 
										 * sent from MC13224 as a response to
										 * PopNwkDataRequest. The value of
										 * Status is interpreted as number of
										 * spaces left in the buffer. Zero (0)
										 * means that there are no free buffer
										 * space.							*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway data indication bytes.
 */

enum radio__pnMpDataIndication {		/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPDI_SRCADDR_H = 0,		/**< Node source address high byte	*/
	RADIO__PCKMPDI_SRCADDR_L,			/**< Node source address low byte	*/
	RADIO__PCKMPDI_LEN,					/**< Length of payload to come.		*/
	RADIO__PCKMPDI_PAYLOAD				/**< Payload sent OTA. This is where
										 * the Access project specific PopNet
										 * data from CM is sent				*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway node placement response bytes.
 */

enum radio__pnMpNodePlacementRsp {		/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKNPRSP_STATUS				/**< Signal strength				*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway heartbeat request bytes.
 */

enum radio__pnMpHeartbeatReq {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPHBREQ_STATUS,			/**< Status							*/
	RADIO__PCKMPHBREQ_SIZE
};

/**
 * PopNet gateway heartbeat response bytes.
 */

enum radio__pnMpHeartbeatRsp {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPHB_STATUS				/**< Status							*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet routing disable/enable request bytes.
 */

enum radio__pnMpRoutingReq {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPROUREQ_ENABLE,			/**< 0=disabled, 1=enabled	*/
	RADIO__PCKMPROUREQ_SIZE
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * PopNet gateway join network enabled response bytes.
 */

enum radio__pnMpRoutingRsp {			/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPROURSP_ENABLE			/**< 0=disabled, 1=enabled	*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
/**
 * PopNet gateway debug message request bytes.
 */

enum radio__pnMpRadioDebugReq {		/*''''''''''''''''''''''''''''''''''*/
	RADIO__PCKMPRDEBUG_FUNCTION,		/**< 0=Off/1=Unicast/2=Broadcast	*/
	RADIO__PCKMPRDEBUG_INT_H,			/**< interval time in seconds		*/
	RADIO__PCKMPRDEBUG_INT_L,			/**< 								*/
	RADIO__PCKMPRDEBUG_SIZE				/**< Size of the request			*/
};

#define RADIO__BYTE_STX			0x02	/**< STX byte						*/

#define RADIO__DEFAULT_RADIUS	10

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef radio__ErrCode radio__pnProcFn(radio__PnInst *,BYTE *,Uint8);

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* Incoming command processing functions */
PRIVATE radio__ErrCode radio__pnProcMpStartNetworkRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpJoinNetworkRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpLeaveNetworkRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpScanNetworkRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpSetRadioParamRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpGetRadioDataRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpJoinEnableRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpNwkDataRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpDataIndication(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpNodePlacementRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpHeartbeatRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpRoutingRsp(radio__PnInst *,BYTE *,Uint8);

PRIVATE radio__ErrCode radio__pnProcMpTestSerialRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpSetMuiRsp(radio__PnInst *,BYTE *,Uint8);
PRIVATE radio__ErrCode radio__pnProcMpAssertSerialRsp(radio__PnInst *,BYTE *,Uint8);


/* Output command building functions */
PRIVATE void radio__pnBuildMpNwkDataReq(radio__PnInst *,radio__PnPopNwkAddr);
PRIVATE void radio__pnBuildHeader(radio__PnInst *,Uint8,Uint8);

/* Helper functions */
PRIVATE void radio__pnUpdateRadioRegs(radio__PnInst *,Uint8,Uint16,Uint16);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the PopNet gateway protocol layer.
*
*	\param		pInit	Pointer to FB initialization parameters.
*
*	\return		The new allocated FB instance.
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC radio__Inst * radio__pnInit(
	radio_Init *			pInit
) {
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) radio__acInit(pInit);

	pInst->parent.pRxBuffer = mem_reserve(&mem_normal, 0x100);
	pInst->parent.pTxBuffer = mem_reserve(&mem_normal, 0x100);
	pInst->parent.pTxSegBuff = mem_reserve(&mem_normal, 0x100);

	//pInst->dstAddr = 0;
	pInst->checksum = 0;
	pInst->command = RADIO__EVTID_NA;
	pInst->flags = 0;
	pInst->retryCntr = 0;

	return((radio__Inst *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnCheckPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks that the received packet is ok.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		
*
*	\details	This function should be called for each received byte.
*
*	\note		
*
*******************************************************************************/

PUBLIC radio__ErrCode radio__pnCheckPacket(
	radio__Inst *			pInstance
) {
	radio__PnInst *			pInst;
	radio__ErrCode			retVal;
	uint8_t					lastByteIdx;

	pInst = (radio__PnInst *) pInstance;
	retVal = RADIO__ERR_CHECKOK;
	lastByteIdx = pInst->parent.usedRxBuff - 1;

	if(lastByteIdx != RADIO__PCK_STX)
	{
		pInst->checksum ^= pInst->parent.pRxBuffer[lastByteIdx];
	}
	/*
	 * Perform a check depending on which packet byte was received.
	 */

	switch(lastByteIdx)
	{
		case RADIO__PCK_STX:
			if (pInst->parent.pRxBuffer[RADIO__PCK_STX] != RADIO__BYTE_STX)
			{
				retVal = RADIO__ERR_ERROR;
			}	
			break;

		case RADIO__PCK_PROTGRP:
			if (pInst->parent.pRxBuffer[RADIO__PCK_PROTGRP] != RADIO__BYTE_POPNET)
			{
				if (pInst->parent.pRxBuffer[RADIO__PCK_PROTGRP] != RADIO__BYTE_MPTEST)
				{
					retVal = RADIO__ERR_ERROR;
				}
			}	
			break;

		case RADIO__PCK_EVTID:
			/*
			 * This byte should contain the command id. Check that the id is
			 * valid.
			 */
			if (pInst->parent.pRxBuffer[RADIO__PCK_PROTGRP] == RADIO__BYTE_POPNET)
			{
				switch(pInst->parent.pRxBuffer[RADIO__PCK_EVTID])
				{
					case RADIO__EVTID_STRTNETRSP:
					case RADIO__EVTID_JOINNETRSP:
					case RADIO__EVTID_LEAVNETRSP:
					case RADIO__EVTID_SCANNETRSP:
					case RADIO__EVTID_SETRPARRSP:
					case RADIO__EVTID_GETRDATRSP:
					case RADIO__EVTID_JOINENARSP:
					case RADIO__EVTID_NWKDATARSP:
					case RADIO__EVTID_DATAIND:
					case RADIO__EVTID_NOPLACERSP:
					case RADIO__EVTID_HEARTBRSP:
					case RADIO__EVTID_ROUTINGRSP:
					case RADIO__EVTID_ASSERTSERIALRSP:
					case RADIO__EVTID_TESTSERIALRSP:
					case RADIO__EVTID_SETMUIRSP:

						break;

					default:
						retVal = RADIO__ERR_ERROR;
						break;
				}
			}
			else if (pInst->parent.pRxBuffer[RADIO__PCK_PROTGRP] == RADIO__BYTE_MPTEST)
			{
/*
				switch(pInst->parent.pRxBuffer[RADIO__PCK_EVTID])
				{
					case RADIO__MPTEST_ID33:
						break;

					default:
						retVal = RADIO__ERR_ERROR;
						break;
				}
*/
				;
			}
			break;

		case RADIO__PCK_LEN:
			if (pInst->parent.pRxBuffer[RADIO__PCK_LEN] > 128)
			{
				retVal = RADIO__ERR_ERROR;
			}	
			break;

		default:
			if (pInst->parent.usedRxBuff == RADIO__PCK_HEADERSIZE + pInst->parent.pRxBuffer[RADIO__PCK_LEN])
			{
				/*
				 * The whole packet has been received. Check checksum.
				 */

				if (pInst->checksum != 0)
				{
					retVal = RADIO__ERR_ERROR;
				}
				else
				{
					retVal = RADIO__ERR_READY;
				}
			}
			break;
	}

	if (retVal == RADIO__ERR_ERROR)
	{
		pInst->checksum = 0;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcessPacket
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes the incoming packet in the buffer.
*
*	\param		pInstance		Pointer to FB instance.
*	\param		pPopNetHeader	Pointer to the PopNet gateway packet header.
*	\param		size			Size of the packet.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC radio__ErrCode radio__pnProcessPacket(
	radio__Inst *			pInstance,
	BYTE *					pPopNetHeader,
	Uint16					size
) {
	radio__ErrCode			retVal = RADIO__ERR_PROCOK;
	radio__PnInst *			pInst;
	BYTE *					pCommand;
	Uint8					dataSize;
	radio__pnProcFn *		pProcFn = NULL;
  uint8_t fallTroughCommand;

	pInst = (radio__PnInst *) pInstance;
	pCommand = pPopNetHeader + RADIO__PCK_DATA;
	dataSize = pPopNetHeader[RADIO__PCK_LEN];
	
	/*
	 * Select processing function
	 */
	if(pPopNetHeader[RADIO__PCK_PROTGRP] == RADIO__BYTE_POPNET)
	{
		switch(pPopNetHeader[RADIO__PCK_EVTID])
		{
			case RADIO__EVTID_STRTNETRSP:
				pProcFn = &radio__pnProcMpStartNetworkRsp;
				break;

			case RADIO__EVTID_JOINNETRSP:
				pProcFn = &radio__pnProcMpJoinNetworkRsp;
				break;

			case RADIO__EVTID_LEAVNETRSP:
				pProcFn = &radio__pnProcMpLeaveNetworkRsp;
				break;

			case RADIO__EVTID_SCANNETRSP:
				pProcFn = &radio__pnProcMpScanNetworkRsp;
				break;

			case RADIO__EVTID_SETRPARRSP:
				pProcFn = &radio__pnProcMpSetRadioParamRsp;
				break;

			case RADIO__EVTID_GETRDATRSP:
				pProcFn = &radio__pnProcMpGetRadioDataRsp;
				// Trigger sending of routingRequest message
				radio__triggerRoutingReq(pInstance);
				break;

			case RADIO__EVTID_JOINENARSP:
				pProcFn = &radio__pnProcMpJoinEnableRsp;
				break;

			case RADIO__EVTID_NWKDATARSP:
				pProcFn = &radio__pnProcMpNwkDataRsp;
				break;

			case RADIO__EVTID_DATAIND:
				pProcFn = &radio__pnProcMpDataIndication;
				break;

			case RADIO__EVTID_NOPLACERSP:
				pProcFn = &radio__pnProcMpNodePlacementRsp;
				break;

			case RADIO__EVTID_HEARTBRSP:
				pProcFn = &radio__pnProcMpHeartbeatRsp;
				break;

			case RADIO__EVTID_ROUTINGRSP:
				pProcFn = &radio__pnProcMpRoutingRsp;
				break;

			case RADIO__EVTID_ASSERTSERIALRSP:
				pProcFn = &radio__pnProcMpAssertSerialRsp;
				break;

			case RADIO__EVTID_TESTSERIALRSP:
				pProcFn = &radio__pnProcMpTestSerialRsp;
				break;

			case RADIO__EVTID_SETMUIRSP:
			  fallTroughCommand = RADIO__MPTEST_SET_MUI;
			  pProcFn = &radio__pnProcMpSetMuiRsp;
	      break;

			default:
				retVal = RADIO__ERR_ERROR;
		}

		/*
		 * Call processing function.
		 */
		if(pProcFn != NULL)
		{
			retVal = pProcFn(pInst, pCommand, dataSize);	// 100824 Jakob added retVal as return code

			if (retVal == RADIO__ERR_PROCOK_RESP)
			{
				radio__pnBuildHeader(pInst, RADIO__EVTID_NWKDATAREQ, RADIO__BYTE_POPNET);
				radio__sendData(pInstance, RADIO__UART_POPNET);
			}
			else if (retVal == RADIO__ERR_PROCOK_FALL_TROUGH)
			{
			  radio__pnBuildHeader(pInst, fallTroughCommand, RADIO__BYTE_MPTEST);
			  radio__sendData(pInstance, RADIO__UART_MPTEST);
			}
		}
	}
	else if(pPopNetHeader[RADIO__PCK_PROTGRP] == RADIO__BYTE_MPTEST)
	{
		retVal = radio__testProcess(
			pInstance,
			pCommand,
			dataSize,
			pPopNetHeader[RADIO__PCK_EVTID],
			&fallTroughCommand
		);

		if (retVal == RADIO__ERR_PROCOK_RESP)
		{
			radio__pnBuildHeader(pInst, pPopNetHeader[RADIO__PCK_EVTID], RADIO__BYTE_MPTEST);
			radio__sendData(pInstance, RADIO__UART_MPTEST);
		}
		else if (retVal == RADIO__ERR_PROCOK_FALL_TROUGH)
		{
      radio__pnBuildHeader(pInst, fallTroughCommand, RADIO__BYTE_POPNET);
      radio__sendData(pInstance, RADIO__UART_POPNET);
		}
	}

#if TARGET_SELECTED & TARGET_W32
	if (retVal == RADIO__ERR_PROCOK) {
		static Uint8 counter = 1;
		if (counter) {
			/* Simulate a GetStatus package while waiting for log data to be
			 * read from flash memory. */
			radio__simulateGetStatusPacket((radio__Inst *) pInst);
			pInst->parent.triggers |= (1<<3);
			counter--;
		}
	}
#endif

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpStartNetworkRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes start network response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode	radio__pnProcMpStartNetworkRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	Uint8					status;
	Uint8					cmdStatus;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	status = pCommand[RADIO__PCKSNET_STATUS];
	cmdStatus = RADIO_CMDS_STARTFAILED;

//	if (status == 0)
	if ((status == gPopStatusFormSuccess_c) ||
		(status ==	gPopStatusSilentStart_c) ||
		(status ==	gPopStatusAlreadyOnTheNetwork_c))
	{
		Uint16	panId;
		Uint16	nwkId;

	/*
		 * Start network was successful. Store channel, pan id and network id
		 * to REG.
	 */
		
		panId = pCommand[RADIO__PCKSNET_PANID_H] << 8;
		panId |= pCommand[RADIO__PCKSNET_PANID_L];

		nwkId = pCommand[RADIO__PCKSNET_NWKADDR_H] << 8;
		nwkId |= pCommand[RADIO__PCKSNET_NWKADDR_L];

		radio__pnUpdateRadioRegs(
			pInst,
			pCommand[RADIO__PCKSNET_CHANNEL],
			panId,
			nwkId
		);

		cmdStatus = RADIO_CMDS_STARTOK;
	}

	if(status != gPopErrBusy_c)
	{
		reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);
	}

	// send getRadioDataReq
	radio__pnSendMpGetRadioDataReq(&pInst->parent);

	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpJoinNetworkRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes join network response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode	radio__pnProcMpJoinNetworkRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	Uint8					status;
	Uint8					cmdStatus;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	status = pCommand[RADIO__PCKJNET_STATUS];
	cmdStatus = RADIO_CMDS_JOINFAILED;

	if ((status == gPopStatusJoinSuccess_c) ||
		(status ==	gPopStatusSilentStart_c) ||
		(status ==	gPopStatusAlreadyOnTheNetwork_c))
	{
		Uint16	panId;
		Uint16	nwkId;

	/*
		 * Join network was successful. Store channel, pan id and network id
		 * to REG.
	 */

		panId = pCommand[RADIO__PCKJNET_PANID_H] << 8;
		panId |= pCommand[RADIO__PCKJNET_PANID_L];

		nwkId = pCommand[RADIO__PCKJNET_NWKID_H] << 8;
		nwkId |= pCommand[RADIO__PCKJNET_NWKID_L];

		radio__pnUpdateRadioRegs(
			pInst,
			pCommand[RADIO__PCKJNET_CHANNEL],
			panId,
			nwkId
		);

		cmdStatus = RADIO_CMDS_JOINOK;
	}

	if(status != gPopErrBusy_c)
	{
		reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);
	}
	// send getRadioDataReq
//	radio__pnSendMpGetRadioDataReq(&pInst->parent);	//removed 100204 not needed

	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpLeaveNetworkRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes leave network response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode	radio__pnProcMpLeaveNetworkRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	Uint8					status;
	Uint8					cmdStatus;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	status = pCommand[RADIO__PCKMPLNRSP_STATUS];
	cmdStatus = RADIO_CMDS_LEAVEFAILED;

	if (status == 0)
	{
		Uint8 nwkSettings;
		Uint8 RadioMode;

		reg_getLocal(&nwkSettings, reg_i_NwkSettings, pInst->parent.instId);
		reg_getLocal(&RadioMode, reg_i_RadioMode, pInst->parent.instId);

		if(nwkSettings == RADIO_NWKSETT_DEFAULT && RadioMode == RADIO_MODE_ENABLED){
			Uint8 startDefaultNetwork = 1;

			reg_putLocal(&startDefaultNetwork, reg_i_startDefaultNetwork, pInst->parent.instId);
		}
		else
		{
			Uint8 	nwkStatus;

			/*
			 * Charger has left the network.
			 */

			cmdStatus = RADIO_CMDS_LEAVEOK;
			nwkStatus = RADIO__NWKSTATUS_NOCONN;

			reg_putLocal(&nwkStatus, reg_i_nwkStatus, pInst->parent.instId);

			/*
			 * Write the command status register so that a message is shown on the
			 * display.
			 */

			reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);

			// send getRadioDataReq
			radio__pnSendMpGetRadioDataReq(&pInst->parent);
		}
	}

	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpScanNetworkRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes scan network response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpScanNetworkRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	//Uint8					channel;
	//Uint16					panId;

	/*
	 * Clear the active command.
	 */

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;



	//channel = pCommand[RADIO__PCKSNRRSP_CHANNEL];
	//panId = ((Uint16)pCommand[RADIO__PCKSNRRSP_PANID_H]) << 8;
	//panId |= (Uint16)pCommand[RADIO__PCKSNRRSP_PANID_L];
	
	//size -= RADIO__PCKSNRRSP_NWKIDARRAY;
	//size /= sizeof(Uint16);

	/* todo:	process the response data
	 */

	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpSetRadioParamRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes set radio parameters response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpSetRadioParamRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	Uint8					status;
	Uint8					cmdStatus;
	Uint16					panIdSet;
	Uint16					nodeIdSet;
	Uint8					channelSet;

	/*
	 * Clear the active command.
	 */

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	/*
	 * process the status code if needed.
	 */

	status = pCommand[RADIO__PCKMPLNRSP_STATUS];

	if (status == 0)
	{
		cmdStatus = RADIO_CMDS_STARTOK;

		/* Update actual network parameters from setChannel, setPanId and setNodeId */

		reg_getLocal(&panIdSet, reg_i_setPanId, pInst->parent.instId);
		reg_getLocal(&channelSet, reg_i_setChannel, pInst->parent.instId);
		channelSet += 11;
		reg_getLocal(&nodeIdSet, reg_i_setNodeId, pInst->parent.instId);

		radio__pnUpdateRadioRegs(
			pInst,
			channelSet,
			panIdSet,
			nodeIdSet
		);
	}
	else{
		cmdStatus = RADIO_CMDS_STARTFAILED;
	}

	/*
	 * Write the command status register so that a message is shown on the
	 * display.
	 */

	reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);

	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpGetRadioDataRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes get radio data response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode	radio__pnProcMpGetRadioDataRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	radio__ErrCode			retVal = RADIO__ERR_PROCOK;
	Uint64					mui;
	Uint32					u32TmpVar1;
	Uint32					u32TmpVar2;
	Uint16					u16TmpVar1;
	Uint16					u16TmpVar2;
	Uint16					nwkAddr;
	Uint16					panId;
	Uint8					channel;
	Uint8					nwkSettings;
	Uint8 					RadioMode;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	u32TmpVar1 = ((Uint32) pCommand[RADIO__PCKGRD_FWTYPE_H]) << 24;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_FWTYPE_HM]) << 16;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_FWTYPE_LM]) << 8;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_FWTYPE_L]);
	reg_getLocal(&u32TmpVar2, reg_i_FirmwareTypeRF, pInst->parent.instId);
	if (u32TmpVar1 != u32TmpVar2)
	{
		reg_putLocal(&u32TmpVar1, reg_i_FirmwareTypeRF, pInst->parent.instId);
	}

	u32TmpVar1 = ((Uint32) pCommand[RADIO__PCKGRD_FWVER_H]) << 24;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_FWVER_HM]) << 16;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_FWVER_LM]) << 8;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_FWVER_L]);
	reg_getLocal(&u32TmpVar2, reg_i_FirmwareVerRF, pInst->parent.instId);
	if (u32TmpVar1 != u32TmpVar2)
	{
		reg_putLocal(&u32TmpVar1, reg_i_FirmwareVerRF, pInst->parent.instId);
	}

	u32TmpVar1 = ((Uint32) pCommand[RADIO__PCKGRD_CHKSUM_H]) << 24;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CHKSUM_HM]) << 16;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CHKSUM_LM]) << 8;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CHKSUM_L]);
	reg_getLocal(&u32TmpVar2, reg_i_ChecksumRF, pInst->parent.instId);
	if (u32TmpVar1 != u32TmpVar2)
	{
		reg_putLocal(&u32TmpVar1, reg_i_ChecksumRF, pInst->parent.instId);
	}

	u32TmpVar1 = ((Uint32) pCommand[RADIO__PCKGRD_CMID_0]) << 24;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CMID_1]) << 16;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CMID_2]) << 8;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CMID_3]);
	reg_getLocal(&u32TmpVar2, reg_i_CmIdH, pInst->parent.instId);
	if (u32TmpVar1 != u32TmpVar2)
	{
		reg_putLocal(&u32TmpVar1, reg_i_CmIdH, pInst->parent.instId);
	}
	mui = ((Uint64)u32TmpVar1) << 32;

	u32TmpVar1 = ((Uint32) pCommand[RADIO__PCKGRD_CMID_4]) << 24;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CMID_5]) << 16;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CMID_6]) << 8;
	u32TmpVar1 |= ((Uint32) pCommand[RADIO__PCKGRD_CMID_7]);
	reg_getLocal(&u32TmpVar2, reg_i_CmIdL, pInst->parent.instId);
	if (u32TmpVar1 != u32TmpVar2)
	{
		reg_putLocal(&u32TmpVar1, reg_i_CmIdL, pInst->parent.instId);
	}
	mui |= (Uint64)u32TmpVar1;

	u16TmpVar1 = ((Uint16) pCommand[RADIO__PCKGRD_MFGID_H]) << 8;
	u16TmpVar1 |= ((Uint16) pCommand[RADIO__PCKGRD_MFGID_L]);
	reg_getLocal(&u16TmpVar2, reg_i_MfgId, pInst->parent.instId);
	if (u16TmpVar1 != u16TmpVar2)
	{
		reg_putLocal(&u16TmpVar1, reg_i_MfgId, pInst->parent.instId);
	}

	u16TmpVar1 = ((Uint16) pCommand[RADIO__PCKGRD_APPID_H]) << 8;
	u16TmpVar1 |= ((Uint16) pCommand[RADIO__PCKGRD_APPID_L]);
	reg_getLocal(&u16TmpVar2, reg_i_AppId, pInst->parent.instId);
	if (u16TmpVar1 != u16TmpVar2)
	{
		reg_putLocal(&u16TmpVar1, reg_i_AppId, pInst->parent.instId);
	}

	u16TmpVar1 = ((Uint16) pCommand[RADIO__PCKGRD_ADDRPOOL_H]) << 8;
	u16TmpVar1 |= ((Uint16) pCommand[RADIO__PCKGRD_ADDRPOOL_L]);
	reg_putLocal(&u16TmpVar1, reg_i_AddrPool, pInst->parent.instId);

	channel = pCommand[RADIO__PCKGRD_CHANNEL];

	panId = ((Uint16) pCommand[RADIO__PCKGRD_PANID_H]) << 8;
	panId |= ((Uint16) pCommand[RADIO__PCKGRD_PANID_L]);

	nwkAddr = ((Uint16) pCommand[RADIO__PCKGRD_NWKID_H]) << 8;
	nwkAddr |= ((Uint16) pCommand[RADIO__PCKGRD_NWKID_L]);

	radio__pnUpdateRadioRegs(
		pInst,
		channel,
		panId,
		nwkAddr
	);

	/* Handle default network connection */
	reg_getLocal(&nwkSettings, reg_i_NwkSettings, pInst->parent.instId);
	reg_getLocal(&RadioMode, reg_i_RadioMode, pInst->parent.instId);
	if(nwkSettings == RADIO_NWKSETT_DEFAULT && RadioMode == RADIO_MODE_ENABLED)
	{
		if(pInst->parent.startNwkCntr){
			radio__triggerStartDefaultNetwork(&pInst->parent);
			pInst->parent.startNwkCntr--;
		}
	}

	radio__testProcCmId(&pInst->parent);

	/* Update SerialNo, lowest 9 digits of MUI*/
	u32TmpVar1 = (Uint32)((mui & 0xFFFFFFFFFFFFUL) % 1000000000);
	reg_get(&u32TmpVar2, radio__hSerialNo);
	if(u32TmpVar1 != u32TmpVar2)
	{
		reg_put(&u32TmpVar1, radio__hSerialNo);
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpJoinEnableRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes join enable response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpJoinEnableRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	Uint8					enable;
	Uint8					cmdStatus;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	enable = pCommand[RADIO__PCKMPJNERSP_ENABLE];


	if(pInst->flags & RADIO__PNFLAG_JOINENA)
	{
		if(!enable){
			/*
			 * Join enable failed because of empty address pool
			 */
			pInst->flags &= ~RADIO__PNFLAG_JOINENA;
			cmdStatus = RADIO_CMDS_JOINENAFAILED;
			reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);
		}
	}
	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpNwkDataRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes data confirmation command.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details	Sent from CM13224 as a response to PopNwkDataRequest.
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpNwkDataRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	radio__ErrCode			retVal = RADIO__ERR_PROCOK;
	Uint8					status;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	status = pCommand[RADIO__PCKMPDC_STATUS];

	if (status != 0)
	{
		/*
		 * Buffer is full in the radio chip. The data must be sent again after
		 * a short while.
		 * 
		 * Just give up if a previous message is waiting for resend.
		 */

		if not(pInst->flags & RADIO__PNFLAG_RESEND)
		{
			if (radio__acStoreForRebuild((radio__Inst *) pInst))
			{
				pInst->flags |= RADIO__PNFLAG_RESEND;
			}
		}
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpDataIndication
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes data indication command.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the received PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpDataIndication(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	radio__ErrCode			retVal = RADIO__ERR_PROCOK;
	BYTE *					pPayload;
	Uint8					len;
	radio__PnPopNwkAddr		srcAddr;

	/*
	 * Copy data from buffer.
	 */

	srcAddr = pCommand[RADIO__PCKMPDI_SRCADDR_H] << 8;
	srcAddr |= pCommand[RADIO__PCKMPDI_SRCADDR_L];

	len = pCommand[RADIO__PCKMPDI_LEN];

	/*
	 * Process the payload.
	 */

	pPayload = pCommand + RADIO__PCKMPDI_PAYLOAD;

	/*
	 * Forward message to the Access OTA protocol layer.
	 */

	retVal = radio__acProcessMessage(
		(radio__Inst *) pInst,
		pPayload,
		len,
		srcAddr
	);

	if (retVal == RADIO__ERR_PROCOK_RESP)
	{	
		/*
		 * A response message has been built on the buffer. Build the 
		 * network data request bytes on the buffer.
		 */

		radio__pnBuildMpNwkDataReq(pInst, srcAddr);
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpNodePlacementRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes node placement response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpNodePlacementRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	char					text[4];

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	text[0] = 3;
	text[1] = pCommand[RADIO__PCKNPRSP_STATUS] + '0';
	text[2] = '/';
	text[3] = '4';

	reg_putLocal(&text, reg_i_signalStrength, pInst->parent.instId);

	return(RADIO__ERR_PROCOK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpHeartbeatRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes heartbeat response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpHeartbeatRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	//Uint8 Status;
	Uint8 GwError;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	//Status = pCommand[RADIO__PCKMPHB_STATUS];

	// Update watchdog timer
	pInst->parent.WatchdogCntr = TICKS_PER_SECOND * WATCHDOG_POLL_INTERVAL;

	GwError = FALSE;
	reg_putLocal(&GwError, reg_i_GwError, pInst->parent.instId);

	msg_sendTry(
		MSG_LIST(statusRep),
		PROJECT_MSGT_STATUSREP,
		SUP_STAT_RADIO_ERROR
	);

	return(RADIO__ERR_PROCOK);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnProcMpRoutingRsp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes routing response.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pCommand	Pointer to the PopNet gateway command.
*	\param		size		Size of the command.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE radio__ErrCode radio__pnProcMpRoutingRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	Uint8					enable;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	enable = pCommand[RADIO__PCKMPROURSP_ENABLE];

	pInst->parent.routing = enable;

	return(RADIO__ERR_PROCOK);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnIsBusy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if PopNet gateway layer is busy.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		TRUE if the PopNet gateway layer of RADIO FB is busy processing
*				a previous request.
*				FALSE if the PopNet gateway layer of RADIO FB is free to take on
*				a new request.
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC Boolean radio__pnIsBusy(
	radio__Inst *			pInstance
) {
	radio__PnInst *			pInst;
	Boolean					retVal;

	pInst = (radio__PnInst *) pInstance;
	retVal = TRUE;

	if (
		pInst->command == RADIO__EVTID_NA
		&& not(pInst->flags & RADIO__PNFLAG_RESEND)
	) {
		retVal = FALSE;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnBuildHeader
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Append PopNet gateway header and checksum to the buffer.
*
*	\param		pInstance	Pointer to FB instance.
*	\param		command		Event ID.
*	\param		protocolId	Protocol group ID
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE void radio__pnBuildHeader(
	radio__PnInst *			pInst,
	Uint8					command,
	Uint8					protocolId
) {
	uint8_t					ii;
	uint8_t					checksum = 0;

	/*
	 * Append command header bytes.
	 */

	pInst->parent.pTxBuffer[RADIO__PCK_STX] = RADIO__BYTE_STX;
	pInst->parent.pTxBuffer[RADIO__PCK_PROTGRP] = protocolId;
	pInst->parent.pTxBuffer[RADIO__PCK_EVTID] = command;
	pInst->parent.pTxBuffer[RADIO__PCK_LEN] = pInst->parent.usedTxBuff;

	pInst->parent.usedTxBuff += RADIO__PCK_DATA;

	/*
	 * Calculate and append checksum to command.
	 */

		for (ii = 1; ii < pInst->parent.usedTxBuff; ii++)
		{
			checksum ^= pInst->parent.pTxBuffer[ii];
		}

		pInst->parent.pTxBuffer[pInst->parent.usedTxBuff] = checksum;
		pInst->parent.usedTxBuff++;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnBuildMpNwkDataReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Build network data request on the buffer.
*
*	\param		pInst		Pointer to FB instance.
*	\param		dstAddr		Destination address.
*
*	\return		
*
*	\details	Payload size must be stored in usedTxBuff before calling this
*				function.
*
*	\note		
*
*******************************************************************************/

PRIVATE void radio__pnBuildMpNwkDataReq(
	radio__PnInst *			pInst,
	radio__PnPopNwkAddr		dstAddr
) {
	BYTE *					pNwkReq;
	
	//pInst->dstAddr = dstAddr;

	/*
	 * Store a timestamp so that timeout can be checked.
	 */
	
	pInst->command = RADIO__EVTID_NWKDATAREQ;
	tstamp_getStamp(&pInst->sentTime);

	/*
	 * Build the network data request and send it.
	 */

	pNwkReq = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	pNwkReq[RADIO__PCKMPDR_DSTADDR_H] = dstAddr >> 8;
	pNwkReq[RADIO__PCKMPDR_DSTADDR_L] = dstAddr & 0xFF;
	pNwkReq[RADIO__PCKMPDR_PAYLENGHT] = pInst->parent.usedTxBuff;

	pInst->parent.usedTxBuff += RADIO__PCKMPDR_PAYLOAD;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpStartNetworkReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send start network request.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendMpStartNetworkReq(
	radio__Inst *			pInstance
) {
	BYTE *					pNwkReq;
	Uint8					channel;
	Uint16					panId;
	Uint8					cmdStatus;
	Uint8 					nwkSettings;
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	/*
	 * Store a timestamp so that timeout can be checked.
	 */
	
	pInst->command = RADIO__EVTID_STRTNETREQ;
	tstamp_getStamp(&pInst->sentTime);

	/*
	 * Update status registers so that GUI can display the correct information.
	 */

	cmdStatus = RADIO_CMDS_STARTNET;
	reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);

	/*
	 * Build and send the packet.
	 */


	reg_getLocal(&nwkSettings, reg_i_NwkSettings, pInst->parent.instId);

	if (nwkSettings == RADIO_NWKSETT_AUTO)
	{
		channel = gPopNwkUseChannelList_c;
		panId = gPopNwkAnyPanId_c;
	}
	else if(nwkSettings == RADIO_NWKSETT_DEFAULT){
		channel = gPopNwkInvalidChannel_c;
		panId = gPopNwkInvalidPan_c;
	}
	else
	{
		reg_getLocal(&channel, reg_i_setChannel, pInst->parent.instId);
		channel += 11;
		reg_getLocal(&panId, reg_i_setPanId, pInst->parent.instId);
	}

	pNwkReq = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	pNwkReq[RADIO__PCKSNR_CHANNEL] = channel;
	pNwkReq[RADIO__PCKSNR_PANID_H] = panId >> 8;
	pNwkReq[RADIO__PCKSNR_PANID_L] = (Uint8) (panId & 0xFF);

	pInst->parent.usedTxBuff += RADIO__PCKSNR_SIZE;

	radio__pnBuildHeader(pInst, RADIO__EVTID_STRTNETREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);

#if TARGET_SELECTED & TARGET_W32
	{
		/*
		 * Simulate that start was successful.
		 */

		radio__pnUpdateRadioRegs(
			pInst,
			channel,
			panId,
			50
		);
	}
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpJoinNetworkReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send join network request.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendMpJoinNetworkReq(
	radio__Inst *			pInstance
) {
	BYTE *					pNwkReq;
	Uint8					channel;
	Uint16					panId;
	Uint8					cmdStatus;
	Uint8 					nwkSettings;
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	/*
	 * Store a timestamp so that timeout can be checked.
	 */

	pInst->command = RADIO__EVTID_JOINNETREQ;
	tstamp_getStamp(&pInst->sentTime);

	/*
	 * Update status registers so that GUI can display the joining network
	 * text.
	 */

	cmdStatus = RADIO_CMDS_JOINNET;
	reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);

	/*
	 * Build and send the packet.
	 */
	reg_getLocal(&nwkSettings, reg_i_NwkSettings, pInst->parent.instId);

	if (nwkSettings == RADIO_NWKSETT_AUTO)
	{
		channel = gPopNwkUseChannelList_c;
		panId = gPopNwkAnyPanId_c;
	}
	else
	{
		reg_getLocal(&channel, reg_i_setChannel, pInst->parent.instId);
		channel += 11;
		reg_getLocal(&panId, reg_i_setPanId, pInst->parent.instId);
	}

	pNwkReq = pInst->parent.pTxBuffer + RADIO__PCK_DATA;

	pNwkReq[RADIO__PCKJNR_CHANNEL] = channel;
	pNwkReq[RADIO__PCKJNR_PANID_H] = panId >> 8;
	pNwkReq[RADIO__PCKJNR_PANID_L] = (Uint8) (panId & 0xFF);

	pInst->parent.usedTxBuff += RADIO__PCKJNR_SIZE;

	radio__pnBuildHeader(pInst, RADIO__EVTID_JOINNETREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);

#if TARGET_SELECTED & TARGET_W32
	{
		/*
		 * Simulate that join was successful.
		 */

		radio__pnUpdateRadioRegs(
			pInst,
			15,
			100,
			50
		);
	}
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpLeaveNetworkReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send leave network request.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__pnSendMpLeaveNetworkReq(
	radio__Inst *			pInstance
) {
	Uint8					cmdStatus;
	Uint8 					nwkSettings;
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	/*
	 * Store a timestamp and command so that timeout can be checked.
	 */

	pInst->command = RADIO__EVTID_LEAVNETREQ;
	tstamp_getStamp(&pInst->sentTime);

	/*
	 * Update status registers so that GUI can display the correct information.
	 */
	reg_getLocal(&nwkSettings, reg_i_NwkSettings, pInst->parent.instId);

	if(nwkSettings != RADIO_NWKSETT_DEFAULT){
		cmdStatus = RADIO_CMDS_LEAVENET;
		reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);
	}

	/*
	 * Build and send the packet.
	 */
	radio__pnBuildHeader(pInst, RADIO__EVTID_LEAVNETREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);

#if TARGET_SELECTED & TARGET_W32
	{
		Uint8 	nwkStatus;
		Uint16 	nwkId;

		/*
		 * Charger has left the network.
		 */

		nwkStatus = RADIO__NWKSTATUS_NOCONN;
		nwkId = 0;

	//	reg_putLocal(&nwkStatus, reg_i_nwkStatus, pInst->parent.instId);
		reg_putLocal(&nwkId, reg_i_NodeId, pInst->parent.instId);
	}
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpScanNetworkReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send scan network request.
*
*	\param		pInstance	Pointer to FB instance.
*	\param		channelList	Channel bitmask. Bit 0 = channel 11,
*							bit 15 = channel 26. Set 0xFFFF to scan all.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendMpScanNetworkReq(
	radio__Inst *			pInstance,
	Uint16					channelList
) {
	BYTE *					pNwkReq;
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	/*
	 * Store a timestamp and command so that timeout can be checked.
	 */

	pInst->command = RADIO__EVTID_SCANNETREQ;
	tstamp_getStamp(&pInst->sentTime);

	pNwkReq = pInst->parent.pTxBuffer + RADIO__PCK_DATA;
	pNwkReq[RADIO__PCKSNRRQ_CHANNELLIST_H] = (Uint8) (channelList >> 8);
	pNwkReq[RADIO__PCKSNRRQ_CHANNELLIST_L] = (Uint8) channelList;

	pInst->parent.usedTxBuff += RADIO__PCKSNRRQ_SIZE;

	radio__pnBuildHeader(pInst, RADIO__EVTID_SCANNETREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpSetRadioParamReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send set radio parameters command.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendMpSetRadioParamReq(
	radio__Inst *			pInstance,
	Uint16					panId,
	Uint16					nodeId,
	Uint8					channel
) {
	Uint8					cmdStatus;
	BYTE *					pNwkReq;
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	/*
	 * Store a timestamp and command so that timeout can be checked.
	 */

	pInst->command = RADIO__EVTID_SETRPARREQ;
	tstamp_getStamp(&pInst->sentTime);

	/*
	 * Update status registers so that GUI can display the correct information.
	 */

	cmdStatus = RADIO_CMDS_STARTNET;
	reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->parent.instId);

	/*
	 * Build and send the command
	 */

	pNwkReq = pInstance->pTxBuffer + RADIO__PCK_DATA;
	pNwkReq[RADIO__PCKMPSRDOPAR_CHANNEL] = channel;
	pNwkReq[RADIO__PCKMPSRDOPAR_PANID_H] = (Uint8) (panId >> 8);
	pNwkReq[RADIO__PCKMPSRDOPAR_PANID_L] = (Uint8) panId;
	pNwkReq[RADIO__PCKMPSRDOPAR_NWKADDR_H] = (Uint8) (nodeId >> 8);
	pNwkReq[RADIO__PCKMPSRDOPAR_NWKADDR_L] = (Uint8) nodeId;
	
	pInstance->usedTxBuff += RADIO__PCKMPSRDOPAR_SIZE;

	radio__pnBuildHeader(pInst, RADIO__EVTID_SETRPARREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpGetRadioDataReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send a MpGetRadioDataReq command to radio module.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		
*
*	\details
*
*	\note		This function should not be called from this module.
*
*******************************************************************************/

PUBLIC void radio__pnSendMpGetRadioDataReq(
	radio__Inst *			pInstance
) {
	BYTE *					pReq;
	radio__PnInst *			pInst;
	uint32_t chargerId;

	pInst = (radio__PnInst *) pInstance;

	deb_log("RADIO: Requesting radio data");

	/*
	 * Store a timestamp of when the command is sent.
	 */

	tstamp_getStamp(&pInst->sentTime);
	pInst->command = RADIO__EVTID_GETRDATREQ;

	/*
	 * Build and send the command
	 */

	reg_getLocal(&chargerId, reg_i_ChargerId, pInst->parent.instId);
	pReq = pInstance->pTxBuffer + RADIO__PCK_DATA;
	pReq[RADIO__PCKGRD_REQ_CHARGER_ID_H] = (uint8_t)(chargerId >> 24);
	pReq[RADIO__PCKGRD_REQ_CHARGER_ID_HM] = (uint8_t)(chargerId >> 16);
	pReq[RADIO__PCKGRD_REQ_CHARGER_ID_LM] = (uint8_t)(chargerId >> 8);
	pReq[RADIO__PCKGRD_REQ_CHARGER_ID_L] = (uint8_t)chargerId;

	pInstance->usedTxBuff += RADIO__PCKGRD_SIZE;

	radio__pnBuildHeader(pInst, RADIO__EVTID_GETRDATREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpJoinEnableReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send join network enable request.
*
*	\param		pInst		Pointer to FB instance.
*	\param		enable		0=Join not allowed, 1=Join allowed.
*	\param		netWide		0=Only this node, 1=Send broadcast to whole network.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendMpJoinEnableReq(
	radio__Inst *			pInst,
	Uint8					enable,
	Uint8					netWide
) {
	BYTE *					pNwkReq;
	Uint8					cmdStatus;
	radio__PnInst *			pPnInst;

	pPnInst = (radio__PnInst *) pInst;

	/*
	 * Store a timestamp so that timeout can be checked.
	 */
	pPnInst->command = RADIO__EVTID_JOINENAREQ;
	tstamp_getStamp(&pPnInst->sentTime);

	/*
	 * Update status registers so that GUI can display the join enable text.
	 */
	if (enable == 1) {
		/*
		 * Join enable has been turned on. It should only be enabled for
		 * 30 seconds. Store a timestamp that can be used to check how long
		 * join enable has been enabled.
		 */
		rtc_hwGetAsPosix(&pPnInst->joinEna);
		pPnInst->flags |= RADIO__PNFLAG_JOINENA;
		cmdStatus = RADIO_CMDS_JOINENAWAIT;
	}
	else{
		pPnInst->flags &= ~RADIO__PNFLAG_JOINENA;
		if(enable == 0xFF){
			cmdStatus = RADIO_CMDS_JOINENATIMEOUT;
			enable = 0;
		}
		else{
			cmdStatus = RADIO_CMDS_JOINENAOK;
		}
	}
	reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->instId);

	/*
	 * Build and send the request command.
	 */
	pNwkReq = pInst->pTxBuffer + RADIO__PCK_DATA;

	pNwkReq[RADIO__PCKMPJNEREQ_ENABLE] = enable;
	pNwkReq[RADIO__PCKMPJNEREQ_NETWIDE] = netWide;

	pInst->usedTxBuff += RADIO__PCKMPJNEREQ_SIZE;

	radio__pnBuildHeader(pPnInst, RADIO__EVTID_JOINENAREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInst, RADIO__UART_POPNET);

#if TARGET_SELECTED & TARGET_W32
	/*
	 * Simulate that the request did get a response.
	 */
	if (enable)
	{
		pPnInst->flags |= RADIO__PNFLAG_JOINENA;
		//tstamp_getStamp(&pPnInst->joinEna);
		rtc_hwGetAsPosix(&pPnInst->joinEna);
		pPnInst->command = 0;
	}
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpNwkDataReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send network data request.
*
*	\param		pInst		Pointer to FB instance.
*	\param		dstAddr		Destination address.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendMpNwkDataReq(
	radio__Inst *			pInst,
	Uint16					dstAddr
) {
	radio__pnBuildMpNwkDataReq((radio__PnInst *) pInst, dstAddr);
	radio__pnBuildHeader((radio__PnInst *) pInst, RADIO__EVTID_NWKDATAREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInst, RADIO__UART_POPNET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendTestResponse
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send test protocol data on the rx/tx buffer.
*
*	\param		pInst		Pointer to FB instance.
*	\param		testId		Test case ID.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendTestResponse(
	radio__Inst *			pInst,
	Uint8					testId
) {
	radio__pnBuildHeader((radio__PnInst *) pInst, testId, RADIO__BYTE_MPTEST);
	radio__sendData(pInst, RADIO__UART_MPTEST);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpNodePlacementReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send node placement request.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PUBLIC void radio__pnSendMpNodePlacementReq(
	radio__Inst *			pInst
) {
	radio__PnInst *			pPnInst;

	pPnInst = (radio__PnInst *) pInst;

	/*
	 * Store a timestamp so that timeout can be checked.
	 */

	pPnInst->command = RADIO__EVTID_NOPLACEREQ;
	tstamp_getStamp(&pPnInst->sentTime);

	deb_log("RADIO: Sending MpNodePlacementReq");
	radio__pnBuildHeader(pPnInst, RADIO__EVTID_NOPLACEREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInst, RADIO__UART_POPNET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpHeartbeatReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send a MpHeartbeatReq command to radio module.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note		This function should not be called from this module.
*
*******************************************************************************/

PUBLIC void radio__pnSendMpHeartbeatReq(
	radio__Inst *			pInstance
) {
	BYTE *					pNwkReq;
	radio__PnInst *			pInst;

	Uint8 bbcFunction = 0;
	Uint8 status = 0;

	pInst = (radio__PnInst *) pInstance;

	deb_log("RADIO: Sending heartbeat");

	/*
	 * Store a timestamp of when the command is sent.
	 */

	tstamp_getStamp(&pInst->sentTime);
	pInst->command = RADIO__EVTID_HEARTBREQ;

	/*
	 * Build and send the command
	 */

	pNwkReq = pInstance->pTxBuffer + RADIO__PCK_DATA;
	reg_get(&bbcFunction, radio__hBBC_func);
	if(bbcFunction){
		status |= RADIO_HBSTATUS_BBC;
	}
	else{
		status &=~ RADIO_HBSTATUS_BBC;
	}
	pNwkReq[RADIO__PCKMPHBREQ_STATUS] = status;

	pInstance->usedTxBuff += RADIO__PCKMPHBREQ_SIZE;

	radio__pnBuildHeader(pInst, RADIO__EVTID_HEARTBREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpRoutingReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send a MpRoutingReq command to radio module.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note		This function should not be called from this module.
*
*******************************************************************************/

PUBLIC void radio__pnSendMpRoutingReq(
	radio__Inst *			pInst
) {
	radio__PnInst *			pnInst;
	BYTE *					pNwkReq;
	Uint8					enable;

	pnInst = (radio__PnInst *) pInst;

	deb_log("RADIO: Sending routing");

	/*
	 * Store a timestamp of when the command is sent.
	 */

	tstamp_getStamp(&pnInst->sentTime);
	pnInst->command = RADIO__EVTID_ROUTINGREQ;


	/*
	 * Build and send the request command.
	 */
	reg_getLocal(&enable, reg_i_Routing, pInst->instId);

	pNwkReq = pInst->pTxBuffer + RADIO__PCK_DATA;

	pNwkReq[RADIO__PCKMPROUREQ_ENABLE] = enable;

	pInst->usedTxBuff += RADIO__PCKMPROUREQ_SIZE;

	radio__pnBuildHeader(pnInst, RADIO__EVTID_ROUTINGREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInst, RADIO__UART_POPNET);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpSetBmParamReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send a MpNwkDataReq with Get_BmConfig command to battery module.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note		This function should not be called from this module.
*
*******************************************************************************/

PUBLIC void radio__pnSendMpSetBmParamReq(
	radio__Inst *			pInst
) {
	radio__PnInst *			pnInst;
	Uint8					cmdStatus;

	pnInst = (radio__PnInst *) pInst;

	deb_log("RADIO: Sending Get_BmConfig");

	/*
	 * Store a timestamp so that timeout can be checked.
	 */

	pnInst->flags |= RADIO__PNFLAG_BMPARAM;
	tstamp_getStamp(&pnInst->sentTime);

	/*
	 * Update status registers so that GUI can display the sending bmu parameters
	 * text.
	 */
	cmdStatus = RADIO_CMDS_BMUPARAMWAIT;
	reg_putLocal(&cmdStatus, reg_i_cmdStatus, pInst->instId);


	/*
	 * Build and send the request command.
	 */
	radio__acSendGetBMConfig(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpAssertOTAReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send radio assert OTA message request.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__pnSendMpAssertOTAReq(
	radio__Inst *			pInstance
) {
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	/*
	 * Store a timestamp and command so that timeout can be checked.
	 */

	pInst->command = RADIO__EVTID_ASSERTOTAREQ;
	tstamp_getStamp(&pInst->sentTime);

	radio__pnBuildHeader(pInst, RADIO__EVTID_ASSERTOTAREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSendMpDebugOTAReq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send radio debug OTA message request.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__pnSendMpDebugOTAReq(
	radio__Inst *			pInstance
) {
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	/*
	 * Store a timestamp and command so that timeout can be checked.
	 */

	pInst->command = RADIO__EVTID_DEBUGOTAREQ;
	tstamp_getStamp(&pInst->sentTime);

	radio__pnBuildHeader(pInst, RADIO__EVTID_DEBUGOTAREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInstance, RADIO__UART_POPNET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnSetCfgRadioParam
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set radio parameters.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__pnSetCfgRadioParam(
	radio__Inst *			pInstance,
	Uint16					panId,
	Uint16					nodeId,
	Uint8					channel
) {
	radio__PnInst *			pInst;

	pInst = (radio__PnInst *) pInstance;

	reg_putLocal(&channel, reg_i_CfgChannel, pInst->parent.instId);
	reg_putLocal(&panId, reg_i_CfgPanId, pInst->parent.instId);
	reg_putLocal(&nodeId, reg_i_CfgNodeId, pInst->parent.instId);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks command timeout.
*
*	\param		pInstance	Pointer to FB instance.
*
*	\return		TRUE if PopNet gateway layer is working fine. FALSE if a fatal
*				error has been detected.
*
*	\details	This function should be called at least at each 50ms to
*				check if the last sent command has timed out or if a command
*				should be resent.
*
*	\note		
*
*******************************************************************************/

PUBLIC Boolean radio__pnTick(
	radio__Inst *			pInstance
) {
	radio__PnInst *			pInst;
	Boolean					retVal;

	pInst = (radio__PnInst *) pInstance;
	retVal = TRUE;

	{
		radio__ErrCode retCode;

		retCode = radio__testTick(pInstance);
		if (retCode == RADIO__ERR_PROCOK_RESP)
		{
			radio__pnBuildHeader(pInst, pInst->testData.activeTest, RADIO__BYTE_MPTEST);
			radio__sendData(pInstance, RADIO__UART_MPTEST);
			pInst->testData.activeTest = RADIO__MPTEST_NA;
		}
	}

	/***************************************************************************
	 * Check if command processing is active
	 */

	if (pInst->command == RADIO__EVTID_NA)
	{
		if (pInst->flags & RADIO__PNFLAG_RESEND)
		{
			Uint16 elapsed;

			tstamp_getDiffCurr(&pInst->sentTime, &elapsed);

			if (elapsed >= 5000)
			{
				pInst->flags &= ~RADIO__PNFLAG_RESEND;
			}
		}

		/***********************************************************************
		 * Check if join is enabled.
		 */

		if (pInst->flags & RADIO__PNFLAG_JOINENA)
		{
			//Uint16 elapsed;
			Uint32 elapsed;
			Posix_Time now;
			Uint8 BacklightTime;

			//tstamp_getDiffCurr(&pInst->joinEna, &elapsed);

			rtc_hwGetAsPosix(&now);
			reg_get(&BacklightTime, radio__hBacklightTimeP);

			elapsed = now - pInst->joinEna;

			//if (elapsed >= 30000)
			if (elapsed >= (Uint32)(BacklightTime * 60))
			{
				/*
				 * Join has been enabled for backlight time. Disable it.
				 */

				radio__pnSendMpJoinEnableReq((radio__Inst *) pInst, 0xFF, 0);
			}
		}

		/***********************************************************************
		 * Check if BmRadioParams have been sent.
		 */
		if (pInst->flags & RADIO__PNFLAG_BMPARAM)
		{
			Uint16 elapsed;

			tstamp_getDiffCurr(&pInst->sentTime, &elapsed);

			if(elapsed  >= 10000)
			{
				Uint8 status;
				status = RADIO_CMDS_BMUPARAMFAILED;
				reg_putLocal(&status, reg_i_cmdStatus, pInst->parent.instId);
				pInst->flags &= ~RADIO__PNFLAG_BMPARAM;
			}
		}
	}
	else
	{
		Uint16 elapsed;

		tstamp_getDiffCurr(&pInst->sentTime, &elapsed);

		if (elapsed >= 300)
		{
			/*
			 * 300ms has elapsed after request was sent. No response has been
			 * received yet. Some messages should retry at this point.
			 */

			if (pInst->command == RADIO__EVTID_GETRDATREQ)
			{
				pInst->retryCntr++;

				if (pInst->retryCntr < 3)
				{
					radio__pnSendMpGetRadioDataReq(pInstance);
				}
				else
				{
					deb_log("RADIO: Command timeout.");
					pInst->command = RADIO__EVTID_NA;
					pInst->retryCntr = 0;	//debug
#if !(TARGET_SELECTED & TARGET_W32)
					retVal = FALSE;
#endif
				}
			}

			if (pInst->command == RADIO__EVTID_HEARTBREQ)
			{
				pInst->retryCntr++;

				if (pInst->retryCntr < 3)
				{
					radio__pnSendMpHeartbeatReq(pInstance);
				}
				else
				{
					deb_log("RADIO: Command timeout.");
					pInst->command = RADIO__EVTID_NA;
					pInst->retryCntr = 0;	//debug
				}
			}

			if (pInst->command == RADIO__EVTID_SETRPARREQ)
			{
				pInst->retryCntr++;

				if (pInst->retryCntr < 3)
				{
					Uint16	panIdSet;
					Uint16	nodeIdSet;
					Uint8	channelSet;

					reg_getLocal(&panIdSet, reg_i_setPanId, pInstance->instId);
					reg_getLocal(&channelSet, reg_i_setChannel, pInstance->instId);
					channelSet += 11;
					reg_getLocal(&nodeIdSet, reg_i_setNodeId, pInstance->instId);

					/* Start network silent with new parameters */
					radio__pnSendMpSetRadioParamReq(pInstance, panIdSet, nodeIdSet, channelSet);
				}
				else
				{
					Uint8 status;
					Uint16	panId;
					Uint16	nodeId;
					Uint8	channel;

					status = RADIO_CMDS_STARTFAILED;
					reg_putLocal(&status, reg_i_cmdStatus, pInstance->instId);

					/* Reset configuration variables to actual varuables (latest received from radio module */
					reg_getLocal(&panId, reg_i_PanId, pInstance->instId);
					reg_getLocal(&channel, reg_i_Channel, pInstance->instId);
					reg_getLocal(&nodeId, reg_i_NodeId, pInstance->instId);

					reg_putLocal(&panId, reg_i_setPanId, pInstance->instId);
					channel -= 11;
					reg_putLocal(&channel, reg_i_setChannel, pInstance->instId);
					reg_putLocal(&nodeId, reg_i_setNodeId, pInstance->instId);

					deb_log("RADIO: Command timeout.");
					pInst->command = RADIO__EVTID_NA;
					pInst->retryCntr = 0;
				}
			}
		}

		if (elapsed >= 10000)
		{
			/*
			 * 10 seconds has elapsed and no response has been received.
			 */

			deb_log("RADIO: Command timeout.");

			if (pInst->command == RADIO__EVTID_JOINNETREQ)
			{
				Uint8 status;
				status = RADIO_CMDS_JOINFAILED;
				reg_putLocal(&status, reg_i_cmdStatus, pInst->parent.instId);
			}
			else if (pInst->command == RADIO__EVTID_STRTNETREQ)
			{
				Uint8 status;
				status = RADIO_CMDS_STARTFAILED;
				reg_putLocal(&status, reg_i_cmdStatus, pInst->parent.instId);
			}
			else if (pInst->command == RADIO__EVTID_LEAVNETREQ)
			{
				Uint8 status;
				Uint16 nodeId;

				status = RADIO__NWKSTATUS_NOCONN;
				nodeId = 0xFFFE;

				reg_putLocal(&status, reg_i_nwkStatus, pInst->parent.instId);
				reg_putLocal(&nodeId, reg_i_NodeId, pInst->parent.instId);

				status = RADIO_CMDS_LEAVEFAILED;
				reg_putLocal(&status, reg_i_cmdStatus, pInst->parent.instId);
			}
			else if (pInst->command == RADIO__EVTID_JOINENAREQ)
			{
				Uint8 status;
				Uint8 joinEnabled;

				reg_getLocal(&joinEnabled, reg_i_joinEnable, pInst->parent.instId);
				if (joinEnabled != 0) {
					status = RADIO_CMDS_JOINENATIMEOUT;
					reg_putLocal(&status, reg_i_cmdStatus, pInst->parent.instId);
				}
			}
			pInst->command = RADIO__EVTID_NA;
		}
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnUpdateRadioRegs
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Update channel, panId and nwkId registers.
*
*	\param		pInst		Pointer to FB instance.
*	\param		newChannel	New channel number.
*	\param		newPanId	New personal area network id.
*	\param		newNwkId	New network id.
*
*	\return
*
*	\details	
*
*	\note		
*
*******************************************************************************/
PRIVATE void radio__pnUpdateRadioRegs(
	radio__PnInst *			pInst,
	Uint8					newChannel,
	Uint16					newPanId,
	Uint16					newNwkId
) {
	Uint8					u8oldVal;
	Uint16					u16oldVal;
	Boolean					newParam = FALSE;

	reg_getLocal(&u8oldVal, reg_i_Channel, pInst->parent.instId);
	if (newChannel != u8oldVal)
	{
		Uint8 setChannel;

		reg_putLocal(&newChannel, reg_i_Channel, pInst->parent.instId);
		setChannel = newChannel - 11;
		reg_putLocal(&setChannel, reg_i_setChannel, pInst->parent.instId);
		reg_putLocal(&newChannel, reg_i_CfgChannel, pInst->parent.instId);
		newParam = TRUE;
	}

	reg_getLocal(&u16oldVal, reg_i_PanId, pInst->parent.instId);
	if (newPanId != u16oldVal)
	{
		reg_putLocal(&newPanId, reg_i_PanId, pInst->parent.instId);
		reg_putLocal(&newPanId, reg_i_setPanId, pInst->parent.instId);
		reg_putLocal(&newPanId, reg_i_CfgPanId, pInst->parent.instId);
		newParam = TRUE;
	}

	reg_getLocal(&u16oldVal, reg_i_NodeId, pInst->parent.instId);
	if (newNwkId != u16oldVal)
	{
		reg_putLocal(&newNwkId, reg_i_NodeId, pInst->parent.instId);
		reg_putLocal(&newNwkId, reg_i_setNodeId, pInst->parent.instId);
		reg_putLocal(&newNwkId, reg_i_CfgNodeId, pInst->parent.instId);
		newParam = TRUE;
	}

	reg_getLocal(&u8oldVal, reg_i_nwkStatus, pInst->parent.instId);
	if (
		(newPanId < gPopNwkReservedPan_c)
		&& ((newChannel >= 11) && (newChannel <= 26))
		&& (newNwkId < gPopNwkReservedAddr_c)
	) {
		/*
		 * Charger is now connected to a network.
		 */

		if (u8oldVal == RADIO__NWKSTATUS_NOCONN)
		{
			u8oldVal = RADIO__NWKSTATUS_CONNECTED;
			reg_putLocal(&u8oldVal, reg_i_nwkStatus, pInst->parent.instId);
		}
	}
	else
	{
		/*
		 * Charger is not connected to a network.
		 */

		if (u8oldVal == RADIO__NWKSTATUS_CONNECTED)
		{
			u8oldVal = RADIO__NWKSTATUS_NOCONN;
			reg_putLocal(&u8oldVal, reg_i_nwkStatus, pInst->parent.instId);
		}
	}

	if(newParam == TRUE)
	{
		/*
		 * Trigger reset of address conflict
		 */
		msg_sendTry(
			MSG_LIST(statusRep),
			PROJECT_MSGT_STATUSREP,
			SUP_STAT_ADDRESS_CONFLICT_CLEAR
		);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	radio__pnContinueLogProc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue processing log message.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details	This function is called when processing a log request message
*				and a new record has been read from the log.
*
*	\note
*
*******************************************************************************/

PUBLIC void radio__pnContinueLogProc(
	radio__Inst *			pInst
) {
	radio__ErrCode			retCode;
	Uint16					dstAddr;

	retCode = radio__acContinueLogProc(pInst, &dstAddr);

	if (retCode == RADIO__ERR_PROCOK_RESP)
	{
		radio__pnBuildMpNwkDataReq((radio__PnInst *) pInst, dstAddr);
		radio__pnBuildHeader((radio__PnInst *) pInst, RADIO__EVTID_NWKDATAREQ, RADIO__BYTE_POPNET);
		radio__sendData(pInst, RADIO__UART_POPNET);
	}
}

PUBLIC void radio__pnSendMpTestSerialReq(
	radio__Inst *			pInst
) {
	BYTE *					pNwkReq;
	radio__PnInst *			pPnInst;
	Uint32					SerialTxCnt;
	pPnInst = (radio__PnInst *) pInst;

	/*
	 * Store a timestamp so that timeout can be checked.
	 */

	pPnInst->command = RADIO__EVTID_TESTSERIALREQ;
	tstamp_getStamp(&pPnInst->sentTime);

	/*
	 * Build and send the request command.
	 */

	pNwkReq = pInst->pTxBuffer + RADIO__PCK_DATA;

	SerialTxCnt = pInst->serialTxCnt;
	pNwkReq[0] = (Uint8) (SerialTxCnt >> 24);
	pNwkReq[1] = (Uint8) (SerialTxCnt >> 16);
	pNwkReq[2] = (Uint8) (SerialTxCnt >> 8);
	pNwkReq[3] = (Uint8) SerialTxCnt;

	pInst->usedTxBuff += 4;


	deb_log("RADIO: Sending MpTestSerialReq");
	radio__pnBuildHeader(pPnInst, RADIO__EVTID_TESTSERIALREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInst, RADIO__UART_POPNET);
}

PRIVATE radio__ErrCode radio__pnProcMpTestSerialRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {
	Uint32 SerialRxCnt;

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	SerialRxCnt = pCommand[0] << 24;
	SerialRxCnt |= pCommand[1] << 16;
	SerialRxCnt |= pCommand[2] << 8;
	SerialRxCnt |= pCommand[3];

	// Update SerialRxCnt
	pInst->parent.serialRxCnt = SerialRxCnt;

	return(RADIO__ERR_PROCOK);
}

PRIVATE radio__ErrCode radio__pnProcMpSetMuiRsp(
  radio__PnInst *     pInst,
  BYTE *          pCommand,
  Uint8         size
) {
	Uint8 rdoPollCntr = 1;
	uint8_t* dataRsp = pInst->parent.pTxBuffer + RADIO__PCK_DATA;
    dataRsp[0] = 0;
    pInst->parent.usedTxBuff += 1;

	// Trigger update of radio data
    reg_putLocal(&rdoPollCntr, reg_i_rdoPollCntr, pInst->parent.instId);

    return RADIO__ERR_PROCOK_FALL_TROUGH;
}

PUBLIC void radio__pnSendMpAssertSerialReq(
	radio__Inst *			pInst
) {
	radio__PnInst *			pPnInst;
	pPnInst = (radio__PnInst *) pInst;

	/*
	 * Store a timestamp so that timeout can be checked.
	 */

	pPnInst->command = RADIO__EVTID_ASSERTSERIALREQ;
	tstamp_getStamp(&pPnInst->sentTime);

	deb_log("RADIO: Sending MpAssertSerialReq");
	radio__pnBuildHeader(pPnInst, RADIO__EVTID_ASSERTSERIALREQ, RADIO__BYTE_POPNET);
	radio__sendData(pInst, RADIO__UART_POPNET);
}

PRIVATE radio__ErrCode radio__pnProcMpAssertSerialRsp(
	radio__PnInst *			pInst,
	BYTE *					pCommand,
	Uint8					size
) {

	pInst->command = RADIO__EVTID_NA;
	pInst->retryCntr = 0;

	pInst->parent.assertStackOverRun = pCommand[0];
	pInst->parent.assertHeapCompromised = pCommand[1];
	pInst->parent.assertOutOfEvents = pCommand[2];
	pInst->parent.assertBadReceivePtr = pCommand[3];
	pInst->parent.assertNwkWatchdog = pCommand[4];
	pInst->parent.assertGwWatchdog = pCommand[5];

	return(RADIO__ERR_PROCOK);
}
