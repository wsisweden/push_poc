@rem test to run make and if it could not be found set the path
@mingw32-make --help --quiet 1>NUL 2>&1
@if ERRORLEVEL 1 path = %path%;C:\Octave\3.2.4_gcc-4.4.0\mingw32\bin\;C:\Octave\3.2.4_gcc-4.4.0\bin\

@rem observe both paths above

@mingw32-make --no-print-directory --quiet -C %~dp0/src/chalg/testChalg/obj
@if ERRORLEVEL 1 goto CompilationError

@octave --q --eval "cd %~dp0/Src/chalg/testChalg, printf(\"Simulation done press enter to exit\n\"), testCharg(), pause"
goto End

:CompilationError
@echo If compilation did not start and command could not be found check path of Octave in the file "testChalg.bat"
pause
geto end

:End
