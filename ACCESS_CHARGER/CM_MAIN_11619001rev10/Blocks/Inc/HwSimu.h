/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		HwSimu.h
*
*	\ingroup	HWSIMU
*
*	\brief		Public declarations for the windows hardware simulation.
*
*	\details
*
*	\note
*
*	\version	28-10-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef HWSIMU_LOCAL_H_INCLUDED
#define HWSIMU_LOCAL_H_INCLUDED

/******************************************************//** \cond priv_decl *//*
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DLL_TEST_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DLL_TEST_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DLL_TEST_EXPORTS
#define DLL_TEST_API __declspec(dllexport)
#else
#define DLL_TEST_API __declspec(dllimport)
#endif

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * Simulated HW parameters.
 */

enum hwsimu__params
{
	HWSIMU_P_CANLED,							/**< CAN led				*/
	HWSIMU_P_F1LED,								/**< F1 led					*/
	HWSIMU_P_F2LED,
	HWSIMU_P_BACKLIGHT,							/**< Display backlight		*/
	HWSIMU_P_STANDBYLED,						
	HWSIMU_P_ALARMLED,							
	HWSIMU_P_CHARGINGLED,						
	HWSIMU_P_CHARGEREADYLED,
	HWSIMU_P_REMOTEOUT
};

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void		hwsimu_setParameter(int,int);
extern void		hwsimu_debugLog(Uint16);
extern void		hwsimu_setPixel(int x, int y, int on);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
