/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Cm.h
*
*	\ingroup	CM
*
*	\brief		Public declarations for the CM FB.
*
*	\note		
*
*	\version	?-?-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef CM_H_INCLUDED
#define CM_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "cai_cc.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * List of possible log types.
 */

enum cm_logType {
	CM_LOGTYPE_HISTORICAL = 0,			/**< Historical log					*/
	CM_LOGTYPE_EVENT,					/**< Event log						*/
	CM_LOGTYPE_INSTANT					/**< Instant log					*/
};

/**
 * List of possible historical record types.
 */

enum cm_histRecType {
	CM_HISTREC_STANDARD = 1				/**< Standard record type			*/
};

/**
 * List of possible event record types.
 */

enum cm_eventRecType {
	CM_EVENT__MIN,						/**< Lower limit					*/

	CM_EVENT_TIMESET,					/**< Time has been set				*/
	CM_EVENT_CHARGESTARTED,				/**< Charging started				*/
	CM_EVENT_CHARGEENDED,				/**< Charging ended					*/
	CM_EVENT_CHALGERROR,				/**< CHALG error occurred			*/
	CM_EVENT_REGUERROR,					/**< REGU error	occurred			*/
	CM_EVENT_CURVEDATA,					/**< New curve data set				*/
	CM_EVENT_VCC,						/**< Mains state changed			*/
	CM_EVENT_VOLTAGECALIBRATION,		/**< Voltage calibration done		*/
	CM_EVENT_CURRENTCALIBRATION,		/**< Current calibration done		*/
	CM_EVENT_STARTNETWORK,				/**< Network started				*/
	CM_EVENT_JOINNETWORK,				/**< Network joined					*/
	CM_EVENT_STARTUP,					/**< Device started					*/
	CM_EVENT_SETTINGS,					/**< Settings changed				*/
	CM_EVENT_ASSERT,					/**< Assert has occurred			*/
	CM_EVENT_SUPERROR,					/**< CHALG error occurred			*/
	CM_EVENT_CABLERISLOPE,				/**< Cable Resistance slope raise 	*/

	CM_EVENT__COUNT						/**< Upper limit					*/
};

/**
 * List of possible instant log record types.
 */

enum cm_instRecType {
	CM_INSTREC_STANDARD = 1				/**< Standard record type			*/
};

/**
 * \name	Log request flags
 * 
 * \name	The following flags can be used in the flags field of the CM 
 *			request struct.
 */

/*@{*/
#define CM_REQFLG_READ		(1<<0)		/**< Perform read operation.		*/
#define CM_REQFLG_WRITE		(1<<1)		/**< Perform write operation.		*/
#define CM_REQFLG_READNEXT	(1<<2)		/**< Perform read next operation.	*/
#define CM_REQFLG_READPREV	(1<<3)		/**< Perform read previous operation.*/
#define CM_REQFLG_SUCCESS	(1<<4)		/**< Record was read successfully.	*/
#define CM_REQFLG_CLEAR		(1<<5)		/**< Clear all logs					*/
#define CM_REQFLG_CLEARINST	(1<<6)		/**< Clear Inst log					*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Log area description. This type contains information about a flash memory
 * area that can be used for log storing.
 */


typedef union{
	struct{
		ChargerError_type	Error;
		ChargerWarning_type	Warning;
	};
	Uint16 Sum;
} chargerAlarm_type;

typedef struct cm_sectorInfo {			/*''''''''''''''''''''''''''' CONST */
	Uint32					startAddr;	/**< Address of first byte.			*/
	Uint32					sectorSize;	/**< Size of the sectors.			*/
	Uint32					count;		/**< Number of sectors.				*/
} cm_LogAreaInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Historical data log header.
 */
#pragma pack(1)
typedef struct cm_histLogHdr {		/*''''''''''''''''''''''''''''''''''''''''*/
	Uint32			ChargeIndex;	/**< Number of charges since production */
	Uint32			ChargeIndexReset;	/**< Number of charges since log reset */
	Uint16			RecordType;		/**< Type of the historical log record	*/
	Uint8			RecordSize;		/**< The size of the record in bytes.	*/
} cm_HistLogHdr;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()

/**
 * Historical data log standard record.
 */
#pragma pack(1)
typedef struct cm_histRecStd {		/*''''''''''''''''''''''''''''''''''''''*/
	cm_HistLogHdr	header;			/**< The record header.					*/
	Uint32			chargeStartTime;/**< Time stamp. (At start of charge, status= charging). Seconds since 1970 (POSIX) */
	Uint32			chargeEndTime;	/**< Time stamp. (At end of charge, status= charging completed). Seconds since 1970 (POSIX) */
	Uint16			AlgNo;			/**< Algorithm number.					*/
	Uint16			Capacity;		/**< Rated capacity.					*/
	Uint16			Cells;			/**< Number of cells.					*/
	Uint16			Ri;				/**< Cable resistance.					*/
	Uint16			Baseload;		/**< Base load.							*/
	Uint16			ThsStart; 		/**< Measured Ths, at CST.				*/
	Uint16			ThsEnd;			/**< Measured Ths, at CET.				*/
	Uint16			ThsMax;			/**< Maximum Ths between CST and CET.	*/
	Uint32			startVoltage;	/**< Measured Battery cell voltage, 1s before CST. */
	Uint32			endVoltage;		/**< Measured Battery cell voltage, 1s before CET. */
	Uint32			ChargeTime;		/**< charging time = time in state CHARGING between chargeStartTime and chargeEndTime */
	Uint32			chargedAh;		/**< Sum of Ichalg second by second during CT */
	Uint32			chargedWh;		/**< Sum of Pchalg second by second during CT */
	Uint16			chargedAhP;		/**< Charged Ah in % of total capacity in main stage. */
	Uint32			equTime;		/**< Sum of time Calculation formula TBD. */
	Uint32			equAh;			/**< Sum of Ichalg second by second during EQT */
	Uint32			equWh;			/**< Sum of Pchalg second by second during EQT */
	Uint16			ChalgErrorSum;	/**< Error summary from CHALG			*/
	Uint16			ReguErrorSum;	/**< Error summary from REGU			*/
	Uint32			EvtIdxStart;	/**< The values of event index at start of charging */
	Uint32			EvtIdxStop;		/**< The values of event index at end of charging */
	Uint32			FID;			/**< Forklift Id if available. Else set to 0x00000000. */
	Uint32			BID;			/**< Unique battery identifier. If available else set to 0x00000000 */
	Uint32			BSN;			/**< Battery serial number. If available else set to 0x00000000. */
	Uint8			BMfgId;			/**< Battery Manufacturing Id i.e. Exide, Enersys. If available else set to 0x0000. */
	Uint8			BatteryType;	/**< Actual (default or BM) Battery type i.e. open led. */
	Uint32			CMtype;			/**< Charger type						*/
	Uint32			SerialNo;		/**< Serial number						*/
	char			AlgNoName[8];	/**< Algorithm name						*/
	Uint8			ChargingMode;	/**< Charging mode. User def or BM params*/
	Uint16			ChalgStatusSum;	/**< Status summary from CHALG			*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint32			u32spare2;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint16			u16spare2;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
	Uint8			u8spare2;		/**< u8 spare.							*/
} cm_HistRecStd;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log header.
 */
#pragma pack(1)
typedef struct cm_evtLogHdr {		/*''''''''''''''''''''''''''''''''''''''*/
	Uint32			Index;			/* Number of events since production	*/
	Uint32			IndexReset;			/* Number of events since reset 	*/
	Uint32			Time;			/* Time when event occurred.			*/
	Uint16			EventId;		/* Unique identity.						*/
	Uint8			DataSize;		/* Number of data corresponding to EventId.*/
} cm_EvtLogHdr;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()

/**
 * Event data log time set record.
 */
#pragma pack(1)
typedef struct cm_evtRecTimeSet {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint32			OldTime;		/**< Time before setting				*/
	Uint8			Source;			/**< 1 = SM, 2 = CM						*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecTimeSet;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log ChargeStarted record.
 */
#pragma pack(1)
typedef struct cm_evtRecChargeStarted {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;				/**< Record header						*/
	Uint32			u32spare1;			/**< u32 spare.							*/
	Uint16			u16spare1;			/**< u16 spare.							*/
	Uint8			u8spare1;			/**< u8 spare.							*/
} cm_EvtRecChargeStarted;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log ChargeEnded record.
 */
#pragma pack(1)
typedef struct cm_evtRecChargeEnded {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;				/**< Record header						*/
	Uint32			u32spare1;			/**< u32 spare.							*/
	Uint16			u16spare1;			/**< u16 spare.							*/
	Uint8			u8spare1;			/**< u8 spare.							*/
} cm_EvtRecChargeEnded;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log ChalgError record.
 */
#pragma pack(1)
typedef struct cm_evtRecChalgError {/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint16			ChalgError;		/**< Error position, bitmask			*/
	Uint16			Active;			/**< Error set(1) or reset(0), bitmask	*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecChalgError;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log ReguError record.
 */
#pragma pack(1)
typedef struct cm_evtRecReguError {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint16			ReguError;		/**< Error position, bitmask			*/
	Uint16			Active;			/**< Error set(1) or reset(0), bitmask	*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecReguError;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log CurveData record.
 */
#pragma pack(1)
typedef struct cm_evtRecCurveData {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint16			AlgNo_new;		/**< New value							*/
	Uint16			AlgNo_old;		/**< Old value							*/
	Uint16			Capacity_new;	/**< New value							*/
	Uint16			Capacity_old;	/**< Old value							*/
	Uint16			Cells_new;		/**< New value							*/
	Uint16			Cells_old;		/**< Old value							*/
	Uint16			Ri_new;			/**< New value							*/
	Uint16			Ri_old;			/**< Old value							*/
	Uint16			Baseload_new;	/**< New value							*/
	Uint16			Baseload_old;	/**< Old value							*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecCurveData;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log Vcc record.
 */
#pragma pack(1)
typedef struct cm_evtRecVcc {		/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint8			VccStatus;		/**< Bit0=Mains, Bit1=Battery, status	*/
	Uint8			Active;			/**< Bit0=Mains, Bit1=Battery, active	*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecVcc;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()

/**
 * Event data log VoltageCalibration record.
 */
#pragma pack(1)
typedef struct cm_evtRecVolCalib {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint32			UslopeNew;		/**< New slope.							*/
	Uint32			UslopeOld;		/**< Old slope.							*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecVolCalib;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log CurrentCalibration record.
 */
#pragma pack(1)
typedef struct cm_evtRecCurrCalib {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint32			IslopeNew;		/**< New slope.							*/
	Uint32			IslopeOld;		/**< Old slope.							*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecCurrCalib;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log StartNetwork record.
 */
#pragma pack(1)
typedef struct cm_evtRecStartNet {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint8			Channel;		/**< Channel 11-26						*/
	Uint16			PanID;			/**< PAN ID								*/
	Uint16			NwkID;			/**< Network node ID					*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecStartNet;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log JoinNetwork record.
 */
#pragma pack(1)
typedef struct cm_evtRecJoinNet {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint8			Channel;		/**< Channel 11-26						*/
	Uint16			PanID;			/**< PAN ID								*/
	Uint16			NwkID;			/**< Network node ID					*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecJoinNet;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log StartUp record.
 */
#pragma pack(1)
typedef struct cm_evtRecStartUp {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint32			MainFwVer;		/**< Main firmware version				*/
	Uint32			RadioFwVer;		/**< Radio module firmware version		*/
	Uint32			MainFwType;		/**< Main firmware type					*/
	Uint32			RadioFwType;	/**< Radio firmware type				*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecStartUp;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Event data log ChargeEnded record.
 */
#pragma pack(1)
typedef struct cm_evtRecSettings {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecSettings;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()

/**
 * Event data log Assert record.
 */
#pragma pack(1)
typedef struct cm_evtRecAssert {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint8			AssertId;		/**< Id off assert for traceability.	*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecAssert;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()

/**
 * Event data log SupError record.
 */
#pragma pack(1)
typedef struct cm_evtRecSupError {	/*''''''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;			/**< Record header						*/
	Uint16			SupError;		/**< Error position, bitmask			*/
	Uint16			Active;			/**< Error set(1) or reset(0), bitmask	*/
	Uint32			u32spare1;		/**< u32 spare.							*/
	Uint16			u16spare1;		/**< u16 spare.							*/
	Uint8			u8spare1;		/**< u8 spare.							*/
} cm_EvtRecSupError;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()

/**
 * Event data log CableRiSlope record.
 */
#pragma pack(1)
typedef struct cm_evtRecCableRiSlope {	/*''''''''''''''''''''''''''''''''''*/
	cm_EvtLogHdr	header;				/**< Record header					*/
	Uint32			RiSlopeNew;			/**< New slope.						*/
	Uint32			RiSlopeOld;			/**< Old slope.						*/
	Uint32			u32spare1;			/**< u32 spare.						*/
	Uint16			u16spare1;			/**< u16 spare.						*/
	Uint8			u8spare1;			/**< u8 spare.						*/
} cm_EvtRecCableRiSlope;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()

/**
 * Instant data log timestamp record.
 */
#pragma pack(1)
typedef struct cm_instLogTstamp {	/*''''''''''''''''''''''''''''''''''''''*/
	Uint8			RecordSync;		/**< Bit7 always one. All other zero	*/
	Uint32			Index;			/**< Number of Instant log records		*/
	Uint32			Time;			/**< Unique identity.					*/
	Uint16			SamplePeriod;	/**< Time between every record in seconds*/
	Uint16			RecordType;		/**< Type of following records			*/
	Uint8			RecordSize;		/**< Size of following records			*/
} cm_InstLogTstamp;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Instant data log standard record.
 */
#pragma pack(1)
typedef struct cm_instRecStd {		/*''''''''''''''''''''''''''''''''''''''*/
	Uint8			RecordSync;		/**< Bit7 always zero. All other one	*/
	Uint16			ChalgError;		/**< 	*/
	chargerAlarm_type	ReguError;		/**<	*/
	Uint32			Uchalg;			/**<	*/		
//	Int32			Ichalg;			/**<	*/
//	Int16			Tboard;			/**<	*/
//	Int16			Ths;			/**<	*/
	Uint32			Ichalg;			/**<	*/
	Uint16			Tboard;			/**<	*/
	Uint16			Ths;			/**<	*/
	Uint32			u32spare1;		/**< u32 spare.							*/
} cm_InstRecStd;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#pragma pack()
/**
 * Union type with all event record types. Can be used to get the largest 
 * possible event record size.
 */
typedef union {
	cm_EvtRecTimeSet		timeSet;
	cm_EvtRecChargeStarted	chargeStarted;
	cm_EvtRecChargeEnded	chargeEnded;
	cm_EvtRecChalgError		chalgError;
	cm_EvtRecReguError		reguError;
	cm_EvtRecCurveData		curveData;
	cm_EvtRecVcc			vcc;
	cm_EvtRecVolCalib		volCalib;
	cm_EvtRecCurrCalib		currCalib;
	cm_EvtRecStartNet		startNet;
	cm_EvtRecJoinNet		joinNet;
	cm_EvtRecStartUp		startup;
	cm_EvtRecSettings		settings;
	cm_EvtRecAssert			assert;
	cm_EvtRecSupError		supError;
	cm_EvtRecCableRiSlope	cableRiSlope;
} cm_EvtRecUnion;

/**
 * Union type with all instant log types. Can be used to get the largest
 * possible instant record size.
 */

typedef union {
	cm_InstLogTstamp		tstamp;
	cm_InstRecStd			rec;
} cm_InstRecUnion;

struct cm_request;
typedef struct cm_request	cm_Request;	

/**
 * \brief	Request callback function type.
 *
 * \param	pRequest	Pointer to the request.
 */

typedef void				cm_ReqDoneFn(cm_Request * pRequest);

/**
 * \brief	Log write/read request.
 *
 * \details	The request should be filled before sending it to CM. CM will then
 *			handle the request and modify the request in some cases.
 *
 *			The request sender may not modify the request before the callback 
 *			function is called.
 *
 * \sa		Log request flags
 */

struct cm_request {

	/** Pointer to next request. This is used internally by CM and should not
	 *	be modified by any other FB */
	cm_Request *			pNext;		

	/** Pointer to buffer for the log record */
	void *					pRecord;	

	/** Pointer to callback function that will be called when the request has
	 *	been handled
	 */
	cm_ReqDoneFn *			pDoneFn;

	/** Utility pointer e.g. can be used to store instance pointer.
	 *	The pointer can then be accessed in the callback function when the 
	 *	request has been handled.
	 */
	void *					pUtil;

	/** Record index. This should be filled for read requests before sending it
	 *	to CM.
	 */
	Uint32					index;		

	/** Address to current record. This is filled by CM when a record has been
	 *	read from the log. It should not be modified by any other FB.
	 */
	Uint32					currAddr;	

	/** Temp index used for instant log. This is only used by CM internally.
	 *	it should not be modified in any other FB.
	 */
	Uint32					tmpIdx;		

	/** Size of the record. Should be set to a write request so that CM knows
	 *	how many bytes should be written.
	 *
	 *	When reading records CM will store the size of the read record to this
	 *	field. The size is used by CM if a read next request is sent.
	 */
	Uint8					size;

	/** Option flags. Specifies the request type. */
	Uint8					flags;		
	
	/** The log type. Should be set to the request before sending it to CM.
	 *	CM will not modify this field.
	 */
	Uint8					logType;
};

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void					cm_processRequest(cm_Request *);
extern void 				cm__TestEraseFlashStart();
extern int 					cm__TestEraseFlashDone();
extern void 				cm__TestClearStatStart();
extern int 					cm__TestClearStatDone();

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif


