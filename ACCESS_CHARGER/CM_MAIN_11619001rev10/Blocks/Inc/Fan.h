/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Fan.h
*
*	\ingroup	FAN
*
*	\brief		Public declarations for the FAN FB.
*
*	\note		
*
*	\version	\$Rev: $ \n
*				\$Date: $ \n
*				\$Author: $
*
*******************************************************************************/

#ifndef FAN_H_INCLUDED
#define FAN_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif 

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "sys.h"
#include "Cm3_Arm7.h"
/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define FAN_DELAY_COUNT	30

/**
 *	\name	Initialization option flags.
 */
/*@{*/
#define FAN_OPT_STARTON		(1<<0)		/**< Start with fan turned on.		*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/
#define FAN_ENGINE_ON !(FIO2PIN & (1<<13))
/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef struct fan__inst fan_Inst;

/**
 *	Type for new duty cycle callback.
 */

typedef void fan_NewDutyFn(Uint32 dutyCycle);

/**
 *	Type used for initialization parameters to the FAN FB.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint32					maxCycle;	/**< Maximum duty cycle.			*/
	Uint32					minCycle;	/**< Duty cycle at min temperature.	*/
	fan_NewDutyFn *			pNewDutyFn;	/**< Pointer to new duty cycle func.*/
	Int16					tMax;		/**< Max duty cycle temperature.	*/
	Int16					tMin;		/**< Min duty cycle temperature.	*/
	sys_RegHandle			temp_h;		/**< Temperature register handle.	*/
	Uint8					delay;		/**< Speed reduction delay in
										 *	seconds.						*/
	Uint8					options;	/**< Option flags.					*/
} fan_Init;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern fan_Inst	*			fan_init(fan_Init const_P *);
extern void					fan_on(fan_Inst *);
extern void					fan_off(fan_Inst *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#ifdef __cplusplus
}
#endif

#endif
