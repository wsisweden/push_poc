/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		rtc.h
*
*	\ingroup	RTC
*
*	\brief		RTC FB public declarations.
*
*	\details	
*
*	\note		
*
*	\version	09-12-2009 / Antero Rintam�ki
*
*******************************************************************************/

#ifndef RTC_H_INCLUDED
#define RTC_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * 	\name	Posix time related defines
 */

/*@{*/
#define RTC_SECS_PER_MIN		60						/**< Seconds per minute	*/
#define RTC_SECS_PER_HOUR		(RTC_SECS_PER_MIN * 60)	/**< Seconds per hour	*/
#define RTC_SECS_PER_DAY		(24 * RTC_SECS_PER_HOUR)/**< Seconds per day	*/
#define RTC_SECS_PER_WEEK		(7 * RTC_SECS_PER_DAY)	/**< Seconds per week	*/
/*@}*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Type used for variables holding POSIX time.
 */

typedef Uint32				Posix_Time;

/**
 * Type used to hold RTC clock internal values.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint16	tm_sec;						/**< Seconds 0-59					*/
	Uint16	tm_min;						/**< Minutes 0-59					*/
	Uint16	tm_hour;					/**< Hours 0-23						*/
	Uint16	tm_mday;					/**< Days of Month 1-(from 29 to 31)*/
	Uint16	tm_mon;						/**< Month 0 - 11					*/
	Uint16	tm_year;					/**< Year 0-4095					*/
	Uint16	tm_wday;					/**< Day of Week 0-6				*/
	Uint16	tm_yday;					/**< Day of Year 1-365 (leapy. 366)	*/
} RTC_Time;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void rtc_hwInit(void);
extern void rtc_hwStart(void);
extern void rtc_hwStop(void);

extern void rtc_hwEnableIrq(void);
extern void rtc_hwDisableIrq(void);

extern void rtc_hwSet(RTC_Time *);
extern void rtc_hwSetDate(RTC_Time *);
extern void rtc_hwSetTime(RTC_Time *);
extern void rtc_hwSetAsPosix(Posix_Time *);

extern void rtc_hwGet(RTC_Time *);
extern void rtc_hwGetAsPosix(Posix_Time *);

extern Boolean rtc_convertToPosix32(RTC_Time *, Posix_Time *);
extern Boolean rtc_convertToTime32(Posix_Time, RTC_Time *);

extern Uint8 rtc_posixToWeekday(Posix_Time);
extern void rtc_hwSetCalibration(Uint32);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */
#endif
