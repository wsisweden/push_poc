/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Radio.h
*
*	\ingroup	RADIO
*
*	\brief		Public declarations of RADIO FB.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 496 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef RADIO_H_INCLUDED
#define RADIO_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/
// Generic Radio gateway status byte interpretation
#define gPopErrNone_c             0x00  // no error
#define gPopErrFailed_c           0x01  // generic failure
#define gPopErrNoMem_c            0x02  // out of memory
#define gPopErrBadTask_c          0x03  // bad task
#define gPopErrBadParm_c          0x04  // invalid parameter
#define gPopErrNotFound_c         0x05  // not found
#define gPopErrFull_c             0x06  // queue or buffer full
#define gPopErrEmpty_c            0x07  // queue or buffer is empty
#define gPopErrBusy_c             0x08  // subsystem full
#define gPopErrAlreadyThere_c     0x09  // attempting to add to a table that already contains the entry
// Start/Join network special status interpretation
#define gPopStatusFormSuccess_c         0x10 // successful formation of the network!
#define gPopStatusJoinSuccess_c         0x11 // successful joining of the network
#define gPopStatusSilentStart_c         0x12 // silently joined or formed (parms already valid)
#define gPopStatusAlreadyOnTheNetwork_c 0x13 // The node is on running mode.
#define gPopStatusPanAlreadyExists_c    0x14 // can't form as PAN ID already exists
#define gPopStatusJoinFailure_c         0x15 // join timed out
#define gPopStatusJoinDisabled_c        0x16 // all potential parents have join disabled
#define gPopStatusWrongMfgId_c          0x17 // all potential parents have different MfgId (use 0xffff to mean any)
#define gPopStatusNoParent_c            0x18 // no parent to join
// PopNet network parameter definitions
#define gPopNwkReservedAddr_c		0xFFF1
#define gPopNwkInvalidAddr_c		0xFFFE
#define gPopNwkReservedPan_c		0xFFF1
#define gPopNwkInvalidPan_c			0xFFFE
#define gPopNwkAnyPanId_c			0xFFFF
#define gPopNwkUseChannelList_c		0x00
#define gPopNwkInvalidChannel_c		0xFE

/**
 * Radio status codes. GUI determines what status message to display by reading
 * these status codes.
 */

enum radio_cmdStatus {					/*''''''''''''''''''''''''''''''''''*/
	RADIO_CMDS_STARTNET,				/**< Starting network				*/
	RADIO_CMDS_STARTFAILED,				/**< Could not start network		*/
	RADIO_CMDS_STARTOK,					/**< Network was started			*/
	RADIO_CMDS_JOINNET,					/**< Joining network searching		*/
	RADIO_CMDS_JOINFAILED,				/**< Could not join network			*/
	RADIO_CMDS_JOINOK,					/**< Network was joined				*/
	RADIO_CMDS_LEAVENET,				/**< Leaving network				*/
	RADIO_CMDS_LEAVEFAILED,				/**< Leave network failed			*/
	RADIO_CMDS_LEAVEOK,					/**< Charger has left the network	*/
	RADIO_CMDS_JOINENAWAIT,				/**< Join enabled waiting			*/
	RADIO_CMDS_JOINENATIMEOUT,			/**< Join enabled timeout			*/
	RADIO_CMDS_JOINENAFAILED,			/**< Join enabled fail				*/
	RADIO_CMDS_JOINENAOK,				/**< Join enabled end, node successfully joined	*/
	RADIO_CMDS_BMUPARAMWAIT,			/**< Sending BMU param waiting		*/
	RADIO_CMDS_BMUPARAMFAILED,			/**< Sending BMU param fail			*/
	RADIO_CMDS_BMUPARAMOK,				/**< Sending BMU param Ok			*/
	RADIO_CMDS_COUNT					/**< Number of status codes			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Network status codes.
 */

enum radio__nwkStatus {					/*''''''''''''''''''''''''''''''''''*/
	RADIO__NWKSTATUS_NOCONN = 0,		/**< Not connected					*/
	RADIO__NWKSTATUS_CONNECTED			/**< Connected						*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Radio mode codes.
 */

enum radio__modeCode {					/*''''''''''''''''''''''''''''''''''*/
	RADIO_MODE_DISABLED = 0,			/**< Radio disabled					*/
	RADIO_MODE_ENABLED					/**< Radio enabled					*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

enum radio__nwkSetting {
	RADIO_NWKSETT_AUTO = 0,				/**< Automatic network configuration */
	RADIO_NWKSETT_USERCONF,				/**< Manual network configuration	 */
	RADIO_NWKSETT_DEFAULT				/**< Default network configuration	 */
};

enum radio__bmCalibration {
	RADIO_BMCALIB_POINT1 = 0,			/**< Calibration point 1 			*/
	RADIO_BMCALIB_POINT2, 				/**< Calibration point 1			*/
	RADIO_BMCALIB_END 					/**< End of calibration				*/
};

enum radio__debugCodes {
	RADIO__DEB_TESTSER,					/**< serial send/receive test.		*/
	RADIO__DEB_ASSERTSER,				/**< Assert counters via serial.	*/
	RADIO__DEB_ASSERTOTA,				/**< Assert counters sent OTA.		*/
	RADIO__DEB_DEBUGOTA,				/**< Debug counters sent OTA.		*/
};

/**
 * 	\name	Charging mode values.
 *
 *	\brief	Register ChargingMode can have following values.
 */
/*@{*/
#define RADIO_USER_DEF	0
#define RADIO_BM	1
/*@}*/

/* BmStatus bit description*/
#define RADIO_BMSTATUS_DISCHARGING      (1<<0)    // status discharging
#define RADIO_BMSTATUS_CHARGING         (1<<1)    // status charging
#define RADIO_BMSTATUS_RADIO_CONNECTED  (1<<2)    // status radio connected
#define RADIO_BMSTATUS_RADIO_JOIN       (1<<3)    // status radio joining
#define RADIO_BMSTATUS_RADIO_START      (1<<4)    // status radio starting
#define RADIO_BMSTATUS_RADIO_JOINENABLE (1<<5)    // status radio join enable
#define RADIO_BMSTATUS_FUNC_EQUALIZE	(1<<6)	  // Function equalize	on/off
#define RADIO_BMSTATUS_NEED_EQUALIZE    (1<<7)    // Need equalize
#define RADIO_BMSTATUS_FUNC_WATER		(1<<8)	  // Function water	on/off
#define RADIO_BMSTATUS_NEED_WATER       (1<<9)    // Need water
#define RADIO_BMSTATUS_CM_CONNECTED     (1<<10)   // Connected to charger
#define RADIO_BMSTATUS_NEED_CALIBRATION (1<<11)   // Need calibration

/* BmError bit decsription */
#define RADIO_BMERROR_HIGH_BATTERY_TEMP (1<<0)    // error high battery temp
#define RADIO_BMERROR_LOW_ACID_LEVEL    (1<<1)    // error low electrolyte level
#define RADIO_BMERROR_VOLT_BALANCE_ERR  (1<<2)    // error voltage balancing error
#define RADIO_BMERROR_TIME              (1<<3)    // Time not set
#define RADIO_BMERROR_LOW_SOC           (1<<4)    // SOC is low
#define RADIO_BMERROR_ADDRESS_CONFLICT  (1<<5)    // Radio network address conflict
//#define RADIO_BMERROR_HW_SW_ERR         (1<<15)   // error HW or SW

/* Heartbeat status bit decsription */
#define RADIO_HBSTATUS_BBC	 			(1<<0)    // BBC function active
/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/**
 * BmConfig data structure.
 */

typedef struct { /*''''''''''''''''''''''''''''' RAM */
	Uint16 panId;
	Uint8 channel;
	Uint16 nodeAddr;
	Uint8 nwkSettings;
	Uint32 serialNo;
	Uint8 bmType;
	Uint32 bid;
	Uint8 equFunc;
	Uint8 equParam;
	Uint8 waterFunc;
	Uint8 waterParam;
	Uint8 bmfgId[8];
	Uint16 cells;
	Uint16 capacity;
	Uint16 cyclesAvailable;
	Uint32 ahAvailable;
	Uint8 batteryType;
	Uint16 baseload;
	Uint16 cableRes;
	Uint16 algNo;
	Uint16 u16Spare5;
	Uint16 bbcGroup;
	Uint16 instPeriod;
	Uint8 cellTap;
	Uint8 voltageBalanceLimit;
	Uint8 acidSensor;
	Uint16 tMax;
	Uint8 remoteOutFunc;
	Uint16 dEL;
	Uint16 cEL;
	Uint8 cEfficiency;
	Uint8 clearStatistics;
	Uint32 u32Spare1;
	Uint32 u32Spare2;
	Uint32 u32Spare3;
	Uint16 securityCode1;
	Uint16 securityCode2;
	Uint16 u16Spare3;
	Uint16 u16Spare4;
	Uint8 bmBitConfig;
	Uint8 u8Spare2;
	Uint8 u8Spare3;
	Uint8 u8Spare4;
} radio__MsgBmConfig; /*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void					radio_dataSent(void *);
extern void					radio_dataReceived(Uint8,void *);
extern void					radio_dataReceived2(Uint8,void *);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
