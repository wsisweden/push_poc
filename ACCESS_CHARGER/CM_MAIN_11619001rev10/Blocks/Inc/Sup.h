/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Sup.h
*
*	\ingroup	SUP
*
*	\brief		Public Self Supervision FB defines.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 1663 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef SUP_H_INCLUDED
#define SUP_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/
/**
 * 	\name	Voltage sense values.
 *
 *	\brief	Voltage sense limits.
 */
/*@{*/
#define SUP_VSENSE_DELAYTIME	10		// Seconds
#define SUP_VSENSE_DELTALIMIT	10		// mV/cell per minute
#define SUP_VSENSE_MINLIMIT	1900		// mV/cell
#define SUP_VSENSE_MAXLIMIT	2300		// mV/cell
/*@}*/
/**
 * Status codes that are used as data in status report events.
 */

enum sup_statusCodes {
	SUP_STAT_NA = 0,					/**< Invalid status code			*/
	SUP_STAT_IDSENTOK,					/**< Radio ID sent via UART			*/
	SUP_STAT_IDSENDFAILED,				/**< Radio ID send failed			*/
	SUP_STAT_BDATA_RECEIVED,			/**< Battery data received.			*/
	SUP_STAT_CHALG_RECEIVED,			/**< Charging algorithm received ok	*/
	SUP_STAT_CHALG_VALUES,				/**< New CHALG values to process.	*/
	SUP_STAT_REGU_VALUES,				/**< New REGU values to process.	*/
	SUP_STAT_RADIO_ERROR,				/**< Fatal error in radio gateway	*/
	SUP_STAT_START_CHARGING,			/**< Start/resume charging			*/
	SUP_STAT_STOP_CHARGING,				/**< pause charging					*/
	SUP_STAT_BM_INIT2,					/**< Enter BmInit2 state			*/
	SUP_STAT_BM_CALOK,					/**< Calibration done				*/
	SUP_STAT_VSENSE,					/**< Enter voltage sense state		*/
	SUP_STAT_BM_INIT2_FAIL,				/**< Exit BmInit2 state 			*/
	SUP_STAT_CHALG_ACTIVATE,			/**< Run chalg_activate function.	*/
	SUP_STAT_ADDRESS_CONFLICT,			/**< Charger address conflict.		*/
	SUP_STAT_BM_ADDRESS_CONFLICT,		/**< BMU address conflict.			*/
	SUP_STAT_DPL_NO_MASTER,				/**< DPL no master connection.		*/
	SUP_STAT_DPL_MASTER,				/**< DPL master connection.			*/
	SUP_STAT_DPL_MASTER_CONFLICT_SET,	/**< DPL no master connection.		*/
	SUP_STAT_DPL_MASTER_CONFLICT_CLEAR,	/**< DPL master connection.			*/
	SUP_STAT_ADDRESS_CONFLICT_CLEAR		/**< Clear address conflict.		*/
};

/**
 * List of possible states SUP can be in.
 */

enum sup__states {
	SUP_STATE_STARTUP = 0,				/**< Startup from reset				*/
	SUP_STATE_SLEEP,					/**< Sleep (no mains connected)		*/
	SUP_STATE_WAKEUP,					/**< Wakeup from sleep state		*/
	SUP_STATE_IDLE,						/**< Idle state						*/
	SUP_STATE_INIT_CHARGING,			/**< Initialize charging			*/
	SUP_STATE_INIT_BM,					/**< Initialize BMU 				*/
	SUP_STATE_INIT_BM_2,				/**< Initialize BMU alt. method		*/
	SUP_STATE_RETRIEVE_BM_DATA,			/**< Retrieve battery module data	*/
	SUP_STATE_HANDLE_EXCEPTION,			/**< Handle BM exception			*/
	SUP_STATE_INIT_BM_3,				/**< Get start voltage				*/
	SUP_STATE_INIT_BM_4,				/**< Calibrate BMU 					*/
	SUP_STATE_INIT_BM_5,				/**< Calibrate BMU success			*/
	SUP_STATE_INIT_BM_6,				/**< Calibrate BMU fail				*/
	SUP_STATE_VOLTAGE_SENSE,			/**< Sense voltage for dual mode	*/
	SUP_STATE_CHARGING,					/**< Charging battery				*/
	SUP_STATE_PAUSE,					/**< Charging is paused				*/
	SUP_STATE_REMOTERESTRICTION,		/**< Remote in restriction active	*/
	SUP_STATE_TIMERESTRICTION,			/**< Time restriction active		*/
	SUP_STATE_CHARGE_ERROR,				/**< Charging error					*/
	SUP_STATE_FINALIZE_CHARGING,		/**< Finalize charging				*/
	SUP_STATE_FW_ERROR,					/**< Firmware malfunction			*/
	SUP_STATE_RADIO_ERROR,				/**< Radio gateway error			*/
	SUP_STATE_CAN_CONTROLLED			/**< CAN controlled					*/
};

/**
 * \name	Status flags
 *
 * \brief	The following flags are used in the statusFlags REG register.
 */

/*@{*/
#define SUP_STATUS_TIMERESTRICT	(1<<0)	/**< Charging restricted			*/
#define SUP_STATUS_REMOTERESTR	(1<<1)	/**< Waiting for remote start		*/
#define SUP_STATUS_REMOTERESTR2	(1<<2)	/**< Charging blocked remotely		*/
#define SUP_STATUS_EXTRACHARGE	(1<<3)	/**< Extra charge activated			*/
#define SUP_STATUS_BM_INIT2		(1<<4)	/**< BM initialization 2 started	*/
#define SUP_STATUS_VSENSE_LOW	(1<<5)	/**< Voltage sense to low			*/
#define SUP_STATUS_VSENSE_HIGH	(1<<6)	/**< Voltage sense to high			*/
#define SUP_STATUS_VSENSE_ERR	(1<<7)	/**< Voltage sense unspecified voltage	*/

#define SUP_STATUS_CORRUPTED	(1<<8)	/**< Internal flash is corrupted	*/
#define SUP_STATUS_RADIO_ERROR	(1<<9)	/**< Radio Gateway error			*/
#define SUP_STATUS_BM_FAILED	(1<<10)	/**< BM initialization failed		*/
#define SUP_STATUS_BM_ALG_ERR	(1<<11)	/**< Incorrect algorithm from BM	*/
#define SUP_STATUS_ADDR_CONFLICT	(1<<12)	/**< Charger address conflict	*/
#define SUP_STATUS_BM_ADDR_CONFLICT	(1<<13)	/**< BMU address conflict		*/
#define SUP_STATUS_DPL_NO_MASTER	(1<<14)	/**< DPL no master connection	*/
#define SUP_STATUS_DPL_MASTER_CONFLICT	(1<<15)	/**< DPL no master connection	*/
/*@}*/

#define SUP_EVENT_MASK		(SUP_STATUS_BM_INIT2|SUP_STATUS_CORRUPTED|SUP_STATUS_RADIO_ERROR|SUP_STATUS_BM_FAILED|SUP_STATUS_BM_ALG_ERR|SUP_STATUS_ADDR_CONFLICT|SUP_STATUS_BM_ADDR_CONFLICT|SUP_STATUS_DPL_NO_MASTER|SUP_STATUS_DPL_MASTER_CONFLICT)
#define SUP_ERROR_MASK		(SUP_STATUS_VSENSE_LOW|SUP_STATUS_VSENSE_HIGH|SUP_STATUS_VSENSE_ERR|SUP_STATUS_CORRUPTED|SUP_STATUS_RADIO_ERROR|SUP_STATUS_BM_FAILED|SUP_STATUS_BM_ALG_ERR|SUP_STATUS_ADDR_CONFLICT|SUP_STATUS_BM_ADDR_CONFLICT|SUP_STATUS_DPL_NO_MASTER|SUP_STATUS_DPL_MASTER_CONFLICT)

/**
 * Possible remote input functions. The values are used in the SUP register
 * RemoteIn_func.
 */

enum sup_remoteInFunc {
	SUP_RIFUNC_NO_FUNC = 0,
	SUP_RIFUNC_START_STOP,
	SUP_RIFUNC_STOP,
	SUP_RIFUNC_HIGHLOW
};

/**
 * Possible remote output functions. The values are used in the SUP register
 * RemoteOut_function.
 */

enum sup_remoteOutFunc {
	SUP_ROFUNC_NO_FUNC = 0,
	SUP_ROFUNC_ERROR,
	SUP_ROFUNC_PHASE,
	SUP_ROFUNC_BBC,
	SUP_ROFUNC_WATER,
	SUP_ROFUNC_AIR,
	SUP_ROFUNC_NO_ERROR,
	SUP_ROFUNC_CHARGING = 254,
	SUP_ROFUNC_RELAY_TOGGLE = 253//,
	//SUP_ROFUNC_ITHRESHOLD	// sorabITR - not now
};

/**
 * Remote out BBC enum
 */
enum sup_remoteOutFuncBBC {
	SUP_ROFUNC_BBC_BBC = 0,
	SUP_ROFUNC_BBC_BBC_READY
};

/**
 * Remote out ITR enum // sorabITR - not now
 */
/*enum sup_remoteOutFuncITR {
	SUP_ROFUNC_ITHRESHOLD_CURRENTH = 0,
	SUP_ROFUNC_ITHRESHOLD_CURRENTL
};*/

/**
 * Possible function button functions.
 */

enum sup_funcButtFunc {
	SUP_FFUNC_NO_FUNC = 0,
	SUP_FFUNC_EQUALIZE,
	SUP_FFUNC_REMOTEOUT,
	SUP_FFUNC_AIRPUMP,
	SUP_FFUNC_WATER
};

enum sup_ExtraChargeFunc {
	SUP_ECFUNC_DISABLED = 0,
	SUP_ECFUNC_ENABLED
};

/** 
 * Status rep codes. These codes are stored to the statusRep register. GUI
 * listens to changes in the register and displays messages accordingly.
 */

enum sup_statusRepCodes {
	SUP_REP_NONE,						/**< Nothing to report				*/
	SUP_REP_BM_INIT_SUCCESS,			/**< BM init successful				*/
	SUP_REP_BM_INIT_FAIL,				/**< BM init failed					*/
	SUP_REP_EQUALIZE_ENABLED,			/**< Equalize enabled				*/
	SUP_REP_EQUALIZE_DISABLED,			/**< Equalize disabled				*/
	SUP_REP_WATER_ENABLED,				/**< Water enabled					*/
	SUP_REP_WATER_DISABLED,				/**< Water disabled					*/
	SUP_REP_AIR_ENABLED,				/**< Air pump enabled				*/
	SUP_REP_AIR_DISABLED,				/**< Air pump disabled				*/
	SUP_REP_REMOTEOUT_ENABLED,			/**< Air pump enabled				*/
	SUP_REP_REMOTEOUT_DISABLED,			/**< Air pump disabled				*/
	SUP_REP_WATER_MANUAL_ENABLED,		/**< Manual water enabled			*/
	SUP_REP_WATER_MANUAL_DISABLED,		/**< Manual water disabled			*/
};

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/
#define SUP_CHARGING_ON_DO_OFF() {FIO2SET = (1<<13);}
#define SUP_ENGINE_ON !(FIO2PIN & (1<<13))
/* Debug RAM data (uninitialized) */
#define SUP_MASK_HARDFAULT 	0x48415244					/**< "HARD" in ASCII characters 	*/
#define SUP_MASK_SWRESET 	0x54575253					/**< "SWRS" in ASCII characters 	*/
#define SUP_RESET_TIMEOUT 	(60*60*12)					/**< 12h timeout 					*/
#define SUP_RESET_SOFTWARE	(*((Uint32*)0x10007FE0))	/**< Flag for software reset 		*/
#define SUP_RESET_STATUS	(*((Uint32*)0x10007FE4))	/**< Status at hardfault		  	*/
#define SUP_RESET_TIMER		(*((Uint32*)0x10007FE8))	/**< Timeout for hardfault	 		*/
//#define SUP_REG_CFSR	(*((Uint32*)0x10007FEC))		/**< Configurable status register 	*/
//#define SUP_REG_HFSR	(*((Uint32*)0x10007FF0))		/**< Hardfault status register 		*/
//#define SUP_REG_MMAR	(*((Uint32*)0x10007FF4))		/**< Memory management register 	*/
//#define SUP_REG_BFAR	(*((Uint32*)0x10007FF8))		/**< Bus fault register	 			*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/
extern Uint8		sup_enpackEnum(Uint8,BYTE *,Uint8 *);
extern Uint8		sup_enpackEnumCode(Uint8,BYTE *,Uint8 *);
extern void 		sup_testClearStat();
/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
