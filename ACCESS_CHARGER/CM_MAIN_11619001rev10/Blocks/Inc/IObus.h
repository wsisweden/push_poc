#ifndef IO_BUS_H
#define IO_BUS_H

#include <stdint.h>

#define IO_CARD_INPUTS 4
#define IO_CARD_OUTPUTS 4

uint8_t IObusGetChainLen(void);
uint32_t IObusGetTimer(uint8_t pin);
void IObusResetTimer(uint8_t pin);

#endif
