/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		can.h
*
*	\ingroup	CAN
*
*	\brief		Global declarations of CAN FB.
*
*	\details
*
*	\note
*
*	\version	22-10-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef CAN_H_INCLUDED
#define CAN_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/
#include "tools.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/
#define SDO_TIMEOUT 300 // in (ms)
#define LSS_TIMEOUT 300 // in (ms)

#define IO_MODULE_NODE_ID 0x7F
#define CAN_LIMIT_SERIAL_REQUEST_LIMIT 1000

#define CAN_BPS_20		20
#define CAN_BPS_50		50
#define CAN_BPS_125 	125
#define CAN_BPS_250 	250
#define CAN_BPS_500 	500
#define CAN_BPS_800 	800
#define CAN_BPS_1000 	1000

/**
 * Can profile codes. The codes are used in the CAN_CommProfile register.
 */

enum can_CommProfile_enum {
	CAN_COMM_PROFILE_DISABLED = 0,
	CAN_COMM_PROFILE_MASTER = 1,
	CAN_COMM_PROFILE_SLAVE = 2,
	CAN_COMM_PROFILE_MASTER_CAN_INPUT = 255,
	CAN_COMM_PROFILE_STATUS = 4
};

/**
 * Can baudrate codes. The codes are used in the CanBps register.
 */

enum can_CanBps_enum {
	CAN_20_BPS = 0,
	CAN_50_BPS,
	CAN_125_BPS,
	CAN_250_BPS,
	CAN_500_BPS,
	CAN_800_BPS,
	CAN_1000_BPS
};

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/
PUBLIC void can_resetCanInput();
/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
