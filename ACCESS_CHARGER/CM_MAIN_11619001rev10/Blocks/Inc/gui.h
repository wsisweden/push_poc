/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		gui.h
*
*	\ingroup	GUI
*
*	\brief		Global declarations of GUI FB.
*
*	\details
*
*	\note
*
*	\version	22-10-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * 	\name	Possible values of Language register.
 */
/*@{*/
enum gui_Languages
{
	gui_lang_english,					/**< English UK						*/
	gui_lang_swedish,					/**< Swedish						*/
	gui_lang_english_us,				/**< English US						*/
	gui_lang_german,					/**< German							*/
	gui_lang_italian,					/**< Italian						*/
	gui_lang_spanish,					/**< Spanish						*/
	gui_lang_portuguese,				/**< portuguese						*/
};
/*@}*/

/**
 * 	\name	Possible values of TimeDateFormat register.
 */
/*@{*/
enum gui_TimeFormat
{
	gui_time_iso,						/**< ISO time format				*/
	gui_time_eur,						/**< European time format			*/
	gui_time_usa,						/**< USA time format				*/
};
/*@}*/


/**
 * 	\name	Menu table flags.
 */
/*@{*/
#define GUI_TBL_TYPE_NORM	0			/**< Normal menu table				*/
#define GUI_TBL_TYPE_ROOT	1			/**< Root menu						*/
#define GUI_TBL_TYPE_PROC	2			/**< Process menu					*/
#define GUI_TBL_TYPE_TIME	3			/**< Time table						*/
#define GUI_TBL_TYPE_ELOG	4			/**< Error log						*/
#define GUI_TBL_TYPE_CS		5			/**< Charger selection				*/
#define GUI_TBL_TYPE_SCP	6			/**< Charging parameter editing		*/
#define GUI_TBL_TYPE_SRO	7			/**< IO bus output select editing	*/
#define GUI_TBL_TYPE_CAN	8			/**< CAN							*/
#define GUI_TBL_TYPE_SRI	9			/**< IO bus input select editing	*/
#define GUI_TBL_TYPE_DPL	10			/**< DPL, Normal menu table			*/
#define GUI_TBL_TYPE_MASK	0x0F		/**< Type mask						*/
#define GUI_TBL_NO_HDR		(1<<4)		/**< Do not show automatic header	*/
/*@}*/

/**
 * 	\name	Security access levels of menu tables.
 */
/*@{*/
#define GUI_ACCESS_LEVEL0	0			/**< Access level 0 (not logged in)	*/
#define GUI_ACCESS_LEVEL1	1			/**< Access level 1					*/
#define GUI_ACCESS_LEVEL2	2			/**< Access level 2					*/
#define GUI_ACCESS_MASK		0x03		/**< Access level mask				*/
/*@}*/

/**
 * 	\name	Table enter / exit flags.
 */
/*@{*/
#define GUI_F_ENTER_OFFSET	2			/**< Enter index offset				*/
#define GUI_F_ENTER_MASK	(0x0F << GUI_F_ENTER_OFFSET)/**< Index bit mask	*/
#define GUI_F_ENTER_NONE	(0<<GUI_F_ENTER_OFFSET)	/**< No enter function	*/
#define GUI_F_ENTER_F0		(1<<GUI_F_ENTER_OFFSET)	/**< Enter func index 0	*/
#define GUI_F_ENTER_F1		(2<<GUI_F_ENTER_OFFSET)	/**< Enter func index 1	*/
#define GUI_F_ENTER_F2		(3<<GUI_F_ENTER_OFFSET)	/**< Enter func index 2	*/
#define GUI_F_ENTER_F3		(4<<GUI_F_ENTER_OFFSET)	/**< Enter func index 3	*/
#define GUI_F_ENTER_F4		(5<<GUI_F_ENTER_OFFSET)	/**< Enter func index 4	*/
#define GUI_F_ENTER_F5		(6<<GUI_F_ENTER_OFFSET)	/**< Enter func index 5	*/
#define GUI_F_ENTER_F6		(7<<GUI_F_ENTER_OFFSET)	/**< Enter func index 6	*/
#define GUI_F_ENTER_F7		(8<<GUI_F_ENTER_OFFSET)	/**< Enter func index 7	*/
#define GUI_F_ENTER_F8		(9<<GUI_F_ENTER_OFFSET)	/**< Enter func index 8	*/
#define GUI_F_ENTER_F9		(0x0A<<GUI_F_ENTER_OFFSET)	/**< Enter func index 9	*/
#define GUI_F_ENTER_F10		(0x0B<<GUI_F_ENTER_OFFSET)	/**< Enter func index 10*/
#define GUI_F_ENTER_F11		(0x0C<<GUI_F_ENTER_OFFSET)	/**< Enter func index 11*/
#define GUI_F_ENTER_F12		(0x0D<<GUI_F_ENTER_OFFSET)	/**< Enter func index 12*/
#define GUI_F_ENTER_F13		(0x0E<<GUI_F_ENTER_OFFSET)	/**< Enter func index 13*/
#define GUI_F_EXIT_OFFSET	6			/**< Exit index offset				*/
#define GUI_F_EXIT_MASK		(0x0F << GUI_F_EXIT_OFFSET)	/**< Index bit mask	*/
#define GUI_F_EXIT_NONE		(0<<GUI_F_EXIT_OFFSET)	/**< No exit function	*/
#define GUI_F_EXIT_F0		(1<<GUI_F_EXIT_OFFSET)	/**< Exit func index 0	*/
#define GUI_F_EXIT_F1		(2<<GUI_F_EXIT_OFFSET)	/**< Exit func index 1	*/
#define GUI_F_EXIT_F2		(3<<GUI_F_EXIT_OFFSET)	/**< Exit func index 2	*/
#define GUI_F_EXIT_F3		(4<<GUI_F_EXIT_OFFSET)	/**< Exit func index 3	*/
#define GUI_F_EXIT_F4		(5<<GUI_F_EXIT_OFFSET)	/**< Exit func index 4	*/
#define GUI_F_EXIT_F5		(6<<GUI_F_EXIT_OFFSET)	/**< Exit func index 5	*/
#define GUI_F_EXIT_F6		(7<<GUI_F_EXIT_OFFSET)	/**< Exit func index 6	*/
#define GUI_F_EXIT_F7		(8<<GUI_F_EXIT_OFFSET)	/**< Exit func index 7	*/
/*@}*/

/**
 * 	\name	Time table types.
 */
/*@{*/
#define GUI_TIME_OFFSET		2
#define GUI_TIME_MON		(0<<GUI_TIME_OFFSET)/**< Monday					*/
#define GUI_TIME_TUE		(1<<GUI_TIME_OFFSET)/**< Tuesday				*/
#define GUI_TIME_WED		(2<<GUI_TIME_OFFSET)/**< Wednesday				*/
#define GUI_TIME_THU		(3<<GUI_TIME_OFFSET)/**< Thursday				*/
#define GUI_TIME_FRI		(4<<GUI_TIME_OFFSET)/**< Friday					*/
#define GUI_TIME_SAT		(5<<GUI_TIME_OFFSET)/**< Saturday				*/
#define GUI_TIME_SUN		(6<<GUI_TIME_OFFSET)/**< Sunday					*/
#define GUI_TIME_MASK		0xFC				/**< Time mask				*/
/*@}*/


/**
 * 	\name	GUI menu row type.
 */
/*@{*/
#define GUI_ROW_SUBMENU		(0|GUI_ROW_NAV)/**< Sub menu link				*/
#define GUI_ROW_TEXT		(1)			/**< Text row						*/
#define GUI_ROW_PARAM		(2|GUI_ROW_NAV)/**<	Parameter row				*/
#define GUI_ROW_NAV			(1<<7)		/**< Row navigable flag				*/
#define GUI_ROW_MASK		(0x003F)	/**< Row type mask					*/
/*@}*/

/**
 * 	\name	Menu item: parameter flags.
 */
/*@{*/
#define GUI_PRM__F_BITS		24
#define GUI_PRM__F_MASK		((1<<GUI_PRM__F_BITS)-1)
#define GUI_PRM_F_NONE		0			/**< No flags						*/
#define GUI_PRM_MASKED		(1<<0)		/**< Password like editing			*/
#define GUI_PRM_SINGLE		(1<<1)		/**< Push button like param			*/
#define GUI_PRM_DBL_REG		(1<<2)		/**< Different read / write regs	*/
#define GUI_PRM_ENUM_TXT	(1<<3)		/**< Push button with enum text		*/
#define GUI_PRM_SLIDE		(1<<4)		/**< Whole value is modifed when editing*/
#define GUI_PRM_ACCEL		(1<<5)		/**< GUI_PRM_SLIDE with acceleration*/
#define GUI_PRM_BITMASK		(1<<6)		/**< Bitmask type					*/
#define GUI_PRM_DYN_ENUM	(1<<7)		/**< Dynamic enum					*/
#define GUI_PRM_ARR_IDX		(1<<8)		/**< Register has array index		*/
#define GUI_PRM_TEXT		(1<<9)		/**< Register has diffrent text		*/
#define GUI_FACT_DEFAULT	(1<<10)		/**< Writing to this should trigger	
												factory default confirmation*/
#define GUI_PRM_IMMEDIATE	(1<<11)		/**< Register change is written		
												immediately as it changes	*/
#define GUI_PRM_CHKBOX		(1<<12)		/**< Show checkbox with PRM_SINGLE	*/
#define GUI_CLEAR_STAT		(1<<13)		/**< Writing to this should trigger	
												clear statistics query		*/
#define GUI_REG_VALUE		(1<<14)		/**< Show centrered register value	*/
#define GUI_REG_MAX			(1<<15)		/**< Register max value is taken from
												a register when editing		*/
#define GUI_REG_LOOP		(1<<16)		/**< Editing loops from max to min	
												if GUI_PRM_SLIDE is on		*/
#define GUI_FACT_SET		(1<<17)		/**< Writing to this should trigger
												set factory default confirmation*/
/*@}*/	

/**
 * 	\name	Menu item: text flags.
 */
/*@{*/
#define GUI_TEXT_F_NONE		0			/**< No text flags					*/
#define GUI_TEXT_AL_RIGHT	(1<<0)		/**< Right aligned text				*/
#define GUI_TEXT_AL_CENTER	(1<<1)		/**< Center aligned text			*/
#define GUI_TEXT_BOLD		(1<<2)		/**< Bold text						*/
/*@}*/

/**
 * 	\name	Menu item: sub menu flags.
 */
/*@{*/
#define GUI_MENU_F_NONE		0			/**< No flags						*/
#define GUI_MENU_LANG		(1<<0)		/**< Show language name				*/
#define GUI_MENU_REG		(1<<1)		/**< Show register value with link	*/
/*@}*/

/**
 * 	\name	Menu item: flags common to all.
 */
/*@{*/
#define GUI_FLG_PAGE_BREAK	(1<<23)		/**< Page break position			*/
/*@}*/

/**
 * 	\name	Menu row showing conditions.
 */
/*@{*/
#define GUI_CND_NONE		(0<<8)		/**< Shown unconditionally			*/
#define GUI_CND_LOGGED		(1<<8)		/**< Shown if logged in				*/
#define GUI_CND_JOINED		(2<<8)		/**< Shown if joined to network		*/
#define GUI_CND_RO_PHASE	(3<<8)		/**< Shown if remote out = phase	*/
#define GUI_CND_CHALG		(4<<8)		/**< Shown if CHALG is running		*/
#define GUI_CND_RO_WATER	(5<<8)		/**< Shown if remote out = water	*/
#define GUI_CND_F1DUR		(6<<8)		/**< Shown if f1 func != nop func	*/
#define GUI_CND_F2DUR		(7<<8)		/**< Shown if f2 func != nop func	*/
#define GUI_CND_STOPDUR		(8<<8)		/**< Shown if STOP func != nop func	*/
#define GUI_CND_ACHAN		(9<<8)		/**< Shown if network conf = auto	*/
#define GUI_CND_CYCLE_LOG	(10<<8)		/**< Shown if cycle log is not empty*/
#define GUI_CND_ALARMS		(11<<8)		/**< Shown if cycle log has alarms	*/
#define GUI_CND_BM_CON		(12<<8)		/**< Shown if battery module connected		*/
#define GUI_CND_CHARGE_STAT	(13<<8)		/**< Shown if charging				*/
#define GUI_CND_REMIN_PAUSE	(14<<8)		/**< Shown if remote in func=pause  */
#define GUI_CND_CUR_ALARMS	(15<<8)		/**< Shown if current charge has alarms*/
#define GUI_CND_RADIO_FW_VER_AVAILABLE (16<<8)		/**< Shown if current charge has alarms*/
#define GUI_CND_CUR_TIMERES	(17<<8)		/**< Shown if time restrictions are on*/
#define GUI_CND_RADIO_OFF	(18<<8)		/**< Shown if radio is disabled		*/
#define GUI_CND_NOT_JOINED	(19<<8)		/**< Shown if radio is on & not joined*/
#define GUI_CND_CHMODE_BM	(20<<8)		/**< Shown if CharingMode == BM		*/
#define GUI_CND_LCHMODE_BM	(21<<8)		/**< Shown if Log CharingMode == BM	*/
#define GUI_CND_PARALLEL	(22<<8)		/**< Shown if Parallel control = TRUE*/
#define GUI_CND_RO_AIR		(23<<8)		/**< Shown if remote out = air pump	*/
#define GUI_CND_CHARGE_COMP	(24<<8)		/**< Shown if chalgstatus = charging completed	*/
#define GUI_CND_CYCLE_LOG_MEM	(25<<8)	/**< Shown if cycle log present in flash	*/
#define GUI_CND_EQU_DISABLED	(26<<8)	/**< Shown if Equalize_func = disabled	*/
#define GUI_CND_EQU_WEEKDAY	(27<<8)		/**< Shown if Equalize_func = Weekday	*/
#define GUI_CND_EQU_CYCLIC	(28<<8)		/**< Shown if Equalize_func = cyclic	*/
#define GUI_CND_DEB_TESTSER	(29<<8)		/**< Shown if Debug_func = TestSerial	*/
#define GUI_CND_DEB_ASSERTSER	(30<<8)	/**< Shown if Debug_func = AssertSerial	*/
#define GUI_CND_DEB_ASSERTOTA	(31<<8)	/**< Shown if Debug_func = AssertOTA	*/
#define GUI_CND_DEB_DEBUGOTA	(32<<8)	/**< Shown if Debug_func = DebugOTA		*/
#define GUI_CND_CAN_ON		(33<<8)		/**< Shown if CAN_Profile = on	*/
#define GUI_CND_CAN_MASTER	(34<<8)		/**< Shown if CAN_Profile = master	*/
#define GUI_CND_CAN_ID_AUTO	(35<<8)		/**< Shown if CAN_Profile = master or slave	*/
#define GUI_CND_CAN_ID_SET	(36<<8)		/**< Shown if CAN_Profile = master external	*/
#define GUI_CND_FW_MODE_USA	(37<<8)		/**< Shown if FW Mode = USA	*/
#define GUI_CND_CHALG_USA	(38<<8)		/**< Shown if Chalg enabled and FW Mode != USA	*/
#define GUI_CND_CHALG_JOINED	(39<<8)		/**< Shown if Chalg enabled and joined to network	*/
#define GUI_CND_CHMODE_BM_CON	(40<<8)		/**< Shown if battery module NOT connected		*/
#define GUI_CND_CHMODE_BM_NOCON	(41<<8)		/**< Shown if CharingMode == DUAL		*/
#define GUI_CND_CHMODE_DEF_DUAL	(42<<8)		/**< Shown if CharingMode == USER_DEF or DUAL */
#define GUI_CND_CHMODE_MULTI	(43<<8)		/**< Shown if CharingMode == MULTI */
#define GUI_CND_BATT_CELL_FUNC	(44<<8)		/**< Shown if Battery XX cells function enabled */
#define GUI_CND_LOGGED2		(45<<8)		/**< Shown if logged in	level 2			*/
#define GUI_CND_USERPARAM	(46<<8)		/**< Shown if User parameters are valid		*/
#define GUI_CND_CONNECTED	(47<<8)		/**< Shown if connected to network		*/
#define GUI_CND_CAN_SLAVE	(48<<8)		/**< Shown if CAN_Profile = slave	*/
#define GUI_CND_CAN_MASTER_EXT	(49<<8)		/**< Shown if CAN_Profile = Master external	*/
#define GUI_CND_RO_BBC		(50<<8)		/**< Shown if remote out = BBC	*/
#define GUI_CND_DPL_MASTER		(51<<8)		/**< Shown if Powergroup_func = Master	*/
#define GUI_CND_DPL_SLAVE		(52<<8)		/**< Shown if Powergroup_func = Slave	*/
#define GUI_CND_DPL_DISPLAY		(53<<8)		/**< Shown if connected to network and CAN_Profile != slave	*/
#define GUI_CND_LOGGED2CHALG	(54<<8)		/**< Shown if logged in	level 2 and Chalg enabled	*/
#define GUI_CND_LOGGED2NOCHALG	(55<<8)		/**< Shown if logged in	level 2 and Chalg enabled == FALSE	*/
#define GUI_CND_CHMODE_DUAL	(56<<8)		/**< Shown if CharingMode == DUAL */
//#define GUI_CND_RO_ITHRESHOLD	(51<<8)		/**< Shown if remote out = I threshold	*/ // sorabITR - not now

#define GUI_CND_INVERT		(0x8000)	/**< Inverts condition				*/
#define GUI_CND_IDX_MASK	(0x7F00)	/**< Condition index mask			*/
#define GUI_CND_MASK		(0xFF00)	/**< Condition bit mask				*/
/*@}*/

#define GUI_NO_TEXT			SYS_BAD_TEXT


/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/** 
 *	\brief	Text row.
 *
 *	\param	c_	visibility condition
 *	\param	h_	text handle
 *	\param	f_	text flags
 */
#define GUI_TEXT_ROW(c_, h_, f_)											\
	{																		\
		GUI_ROW_TEXT|c_,													\
		f_,																	\
		0,																	\
		{ h_ },																\
		0,																	\
		{ NULL },															\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief	Text row with two texts. Texts are space so that text h1_ is 
 *			aligned left and h2_ is aligned right.
 *
 *	\param	c_	visibility condition
 *	\param	h1_	text handle
 *	\param	h2_	text handle
 *	\param	f_	text flags
 */
#define GUI_TEXT2_ROW(c_, h1_, h2_, f_)										\
	{																		\
		GUI_ROW_TEXT|c_,													\
		f_,																	\
		0,																	\
		{ h1_ },															\
		0,																	\
		{ NULL },															\
		{ h2_ }																\
	}


/** 
 * \brief		Parameter row.
 *	
 *	\param		c_	visibility condition
 *	\param		r_	register handle
 *	\param		f_	parameter flags
 *	\param		e_	enum index (on GUI_PRM_SINGLE) or array index (GUI_PRM_ARR_IDX)
 *
 *	\details	A parameter row can be used to display a REG register value on
 *				a menu row.
 */
#define GUI_PARAM_ROW(c_, r_, f_, e_)										\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_,																	\
		r_,																	\
		{ 0 },																\
		e_,																	\
		{NULL },															\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief		Extended parameter row.
 *	
 *	\param		c_	visibility condition
 *	\param		r_	register handle
 *	\param		f_	parameter flags
 *	\param		e_	enum index (on GUI_PRM_SINGLE) or array index (GUI_PRM_ARR_IDX)
 *	\param		t_	text handle of the parameter text
 *
 *	\details	A parameter row can be used to display a REG register value on
 *				a menu row. This version allows using a different text than
 *				the register name.
 *
 *	\notice		This won't work with menu items that have different read
 *				and write registers.
 */
#define GUI_PARAM_ROW_EX(c_, r_, f_, e_, t_)								\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_,																	\
		r_,																	\
		{ t_ },																\
		e_,																	\
		{NULL },															\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief	Parameter row with decimal scaling.
 *
 *	\param	c_	visibility condition
 *	\param	r_	register handle
 *	\param	f_	parameter flags
 *	\param	e_	enum index (on GUI_PRM_SINGLE)
 *	\param	d_	number of decimals to show
 */
#define GUI_PARAM_ROW_DEC(c_, r_, f_, e_, d_)								\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_,																	\
		r_,																	\
		{ 0 },																\
		e_,																	\
		{NULL },															\
		{ d_ }																\
	}

/** 
 *	\brief	Parameter row with decimal scaling and extra text.
 *
 *	\param	c_	visibility condition
 *	\param	r_	register handle
 *	\param	f_	parameter flags
 *	\param	e_	enum index (on GUI_PRM_SINGLE)
 *	\param	d_	number of decimals to show
 *	\param	t_	text handle
 */
#define GUI_PARAM_ROW_DEC_EX(c_, r_, f_, e_, d_, t_)						\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_,																	\
		r_,																	\
		{ t_ },																\
		e_,																	\
		{NULL },															\
		{ d_ }																\
	}

/** 
 *	\brief	Parameter row with different read and write registers.
 *
 *	\param	c_	visibility condition
 *	\param	r_	read register handle
 *	\param	r2_	write register handle
 *	\param	f_	parameter flags
 *	\param	e_	enum index (on GUI_PRM_SINGLE)
 */
#define GUI_PARAM2_ROW(c_, r_, r2_, f_, e_)									\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_|GUI_PRM_DBL_REG,													\
		r_,																	\
		{ r2_ },															\
		e_,																	\
		{ NULL },															\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief	Parameter row with different read and write registers and decimal
 *			scaling.
 *
 *	\param	c_	visibility condition
 *	\param	r_	read register handle
 *	\param	r2_	write register handle
 *	\param	f_	parameter flags
 *	\param	e_	enum index (on GUI_PRM_SINGLE)
 *	\param	d_	number of decimals to show
 */
#define GUI_PARAM2_ROW_DEC(c_, r_, r2_, f_, e_, d_)							\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_|GUI_PRM_DBL_REG,													\
		r_,																	\
		{ r2_ },															\
		e_,																	\
		{ NULL },															\
		{ d_ }																\
	}

/** 
 *	\brief	Parameter bitmask row.
 *
 *	\param	c_	visibility condition
 *	\param	r_	register handle
 *	\param	t_	parameter 'header' text
 *	\param	f_	parameter flags
 *	\param	e_	bit index 
 *	\param	te_	bit text handle
 */
#define GUI_PARAM_BM_ROW(c_, r_, t_, f_, e_, te_)							\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_|GUI_PRM_BITMASK,													\
		r_,																	\
		{ t_ },																\
		e_,																	\
		{ NULL },															\
		{ te_ }																\
	}

/** 
 *	\brief	Dynamic enum row.
 *
 *	\param	c_	visibility condition
 *	\param	r_	register handle
 *	\param	f_	parameter flags
 *	\param	p_	dymanic enum function (type gui_fnDynamicEnum)
 */
#define GUI_PARAM_DE_ROW(c_, r_, f_, p_)									\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_|GUI_PRM_DYN_ENUM,												\
		r_,																	\
		{ 0 },																\
		0,																	\
		{ (gui_MenuTable const_P *)p_ },									\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief	Sub menu table row.
 *
 *	\param	c_	visibility condition
 *	\param	p_	sub menu ptr
 *	\param	f_	sub menu flags
 */
#define GUI_SUBMENU(c_, p_, f_)												\
	{																		\
		GUI_ROW_SUBMENU|c_,													\
		f_,																	\
		0,																	\
		{ 0 },																\
		0,																	\
		{ p_} ,																\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief	Sub menu table row with register value.
 *
 *	\param	c_	visibility condition
 *	\param	p_	sub menu ptr
 *	\param	f_	sub menu flags
 *	\param	r_	register whos value should be shown
 */
#define GUI_SUBMENU_REG(c_, p_, f_, r_)										\
	{																		\
		GUI_ROW_SUBMENU|c_,													\
		f_|GUI_MENU_REG,													\
		r_,																	\
		{ 0 },																\
		0,																	\
		{ p_} ,																\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief	Parameter row where the editing max value is limited to a value
 *			of given register.
 *
 *	\param	c_	visibility condition
 *	\param	r_	register handle
 *	\param	r2_	limit register handle
 *	\param	f_	parameter flags
 */
#define GUI_PARAM_REG_LIM(c_, r_, r2_, f_)									\
	{																		\
		GUI_ROW_PARAM|c_,													\
		f_|GUI_REG_MAX,														\
		r_,																	\
		{ r2_ },															\
		0,																	\
		{ NULL },															\
		{ GUI_NO_TEXT }														\
	}

/** 
 *	\brief	Menu table.
 *
 *	\param	n_	menu table name
 *	\param	t_	table type
 *	\param	x_	menu header text handle
 *	\param	a_	security access level
 *	\param	c_	enter / exit functions
 */
#define GUI_MENU_TABLE(n_, t_, x_, a_, c_)									\
	PUBLIC const_P gui_MenuTable n_ =										\
	{ x_, t_, a_ | c_, dim(n_##Items), n_##Items };


/** Casts pointer into gui_MenuItem */
#define gui__itm(i_)			((gui_MenuItem *)(i_))

/** Casts pointer into gui_MenuTable */
#define gui__tbl(i_)			((gui_MenuTable *)(i_))

/** Gets table type */
#define gui__tblType(t_)		(gui__tbl(t_)->type & GUI_TBL_TYPE_MASK)

/** Gives row type index from menu item ptr */
#define gui_rowTypeToIdx(i_)	(gui__itm(i_)->type & GUI_ROW_MASK)

/** Gives row condition index from menu item ptr */
#define gui_rowCondition(i_)	((gui__itm(i_)->type & GUI_CND_MASK) >> 8)

/** Gives row condition index from menu item ptr */
#define gui_rowConditionIdx(i_)	((gui__itm(i_)->type & GUI_CND_IDX_MASK) >> 8)

/** Gives row condition inversion from menu item ptr */
#define gui_rowConditionInv(i_)	((gui__itm(i_)->type & GUI_CND_INVERT) ? 1 : 0)

/** Gives menu leaving function index from menu table ptr */
#define gui_tblExit(t_)			((gui__tbl(t_)->access & GUI_F_EXIT_MASK) >> GUI_F_EXIT_OFFSET)

/** Gives menu extering function index from menu table ptr */
#define gui_tblEnter(t_)		((gui__tbl(t_)->access & GUI_F_ENTER_MASK) >> GUI_F_ENTER_OFFSET)

/** Gives menu day index from menu table ptr */
#define gui_tblDay(t_)			((gui__tbl(t_)->access & GUI_TIME_MASK) >> GUI_TIME_OFFSET)

/** Gives menu access level from menu table ptr */
#define gui_tblAccess(t_)		((gui__tbl(t_)->access & GUI_ACCESS_MASK))

/** Gives TRUE if menu item is parameter row */
#define gui_isParamRow(p_)		(((p_)->type & GUI_ROW_MASK) == (GUI_ROW_PARAM & GUI_ROW_MASK))

/** Gives TRUE if menu item is text row */
#define gui_isTextRow(p_)		(((p_)->type & GUI_ROW_MASK) == (GUI_ROW_TEXT & GUI_ROW_MASK))

/** Gives TRUE if menu item is sub menu link row */
#define gui_isMenuRow(p_)		(((p_)->type & GUI_ROW_MASK) == (GUI_ROW_SUBMENU & GUI_ROW_MASK))

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/** 
 *	Function type for GUI visibility condition checks.
 */
typedef Boolean (*			gui_fnCondition)(void);


/** 
 *	Dynamic enum value retrieval function.
 *	Returns number of bytes written.
 *	Given buffer may be NULL.
 */
typedef Uint8 (*			gui_fnDynamicEnum)(Uint8 idx, BYTE * pBuf, Uint8 * pCnt);

/** 
 *	Menu table enter / exit function.
 *
 *	If argument 'validate' is TRUE when function is called it is a request
 *	to allow exiting the menu table. If returned value is GUI_NO_TEXT GUI may
 *	navigate out of the table. If returned value is a valid text handle it is
 *	shown as an error message and leaving the menu is prevented.
 *
 *	If argument 'validate' is FALSE the call is just an indication about 
 *	entering or exiting a menu table. Return value is ignored in thsi case.
 *	
 */
typedef sys_TxtHandle (*	gui_fnTableEnterExit)(Boolean validate);

typedef struct gui_MenuTable gui_MenuTable;

/** 
 *	Condition functions info.
 */
typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	gui_fnCondition *		func;		/**< Ptr to function pointers		*/
	Uint8					count;		/**< Number of Functions			*/
} gui_ConditionTable;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Menu item structure.
 */
typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	Uint16					type;		/**< Item type / condition			*/
	Uint32					flags;		/**< Item flags						*/
	sys_RegHandle			reg;		/**< Register handle for params		*/
	union								/*									*/
	{									/*									*/
		sys_TxtHandle		text;		/**< Text handle for text rows		*/
		sys_RegHandle		reg2;		/**< Write reg  handle for params	*/
	};									/*									*/
	Uint8					enumval;	/**< Enum index for 'push buttons'	*/
	union								/*									*/
	{									/*									*/
		gui_MenuTable const_P *subitem;	/**< Ptr to sub menu table			*/
		gui_fnDynamicEnum	pEnumFn;	/**< Dynamic enum handler			*/
	};									/*									*/
	union								/*									*/
	{									/*									*/
		Uint16				decims;		/**< Visible number of decims		*/
		sys_TxtHandle		text2;		/**< Secondary text handle for bits	*/
	};									/*									*/
} gui_MenuItem;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Menu table structure.
 */
struct gui_MenuTable					/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	sys_TxtHandle			header;		/**< Table header text				*/
	Uint8					type;		/**< Table type / flags				*/
	Uint16					access;		/**< Access / conf level			*/
	Uint8					count;		/**< Table item count				*/
	gui_MenuItem const_P *	pItems;		/**< Ptr to table items				*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/
extern volatile int gUserParamsEdited;
/*****************************************************************************/

#endif
