/* 30-10-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		pwm.h
*
*	\ingroup	PWM
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	30-10-2009 / Antero Rintam�ki
*
*******************************************************************************/

#ifndef PWM_H_INCLUDED
#define PWM_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/
// PWM-cycle definitions
#define PWM_CYCLE_FULL				((Uint32)(48e6/2.5e3))//1200					/* TODO give the regu-cycle */


#define PWM_CYCLE_TUI_NORMAL		PWM_CYCLE_FULL
#define PWM_CYCLE_TUI_EYE_SAVE		PWM_CYCLE_FULL/4

// Timer Control Register
#define TCR_CNT_EN				0x00000001
#define TCR_CNT_RESET			0x00000002
#define TCR_PWM_EN				0x00000008

// PWM latch register
#define LER0_EN			(1<<0)
#define LER1_EN			(1<<1)
#define LER2_EN			(1<<2)
#define LER3_EN			(1<<3)
#define LER4_EN			(1<<4)
#define LER5_EN			(1<<5)
#define LER6_EN			(1<<6)

// PWM Control registers for enabling outputs
#define PWMENA1			(1<<9)
#define PWMENA2			(1<<10)
#define PWMENA3			(1<<11)
#define PWMENA4			(1<<12)
#define PWMENA5			(1<<13)
#define PWMENA6			(1<<14)


// PWM outputs

/**
 * \brief	List of PWM outputs.
 *
 * \details	The following values can be used with the PWM functions to select
 *			the output.
 */

enum pwm__outputs
{
	PWM_OUTPUT_ALARM = 0,				/**< Alarm LED						*/
	PWM_OUTPUT_CHARG,					/**< Charging LED					*/			
	PWM_OUTPUT_CHARGRDY,				/**< Charging ready LED				*/
	PWM_OUTPUT_STANDBY,					/**< Standby LED					*/
	PWM_OUTPUT_CHARGE_CONTROL,			/**< Charge current control			*/
	PWM_OUTPUT_FAN,						/**< Fan control.					*/
	
	PWM_OUTPUT_COUNT					/**< Number of PWM outputs			*/
};

// PWM-cycle definitions

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void		pwm_init(void);
extern void		pwm_start(void);	
extern void		pwm_stop(void);

extern Uint32	pwm_getOutputCycle(Uint32);
PUBLIC void pwm_setOutputCycle(
	Uint32 				output,
	Uint32 				cycle
);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
