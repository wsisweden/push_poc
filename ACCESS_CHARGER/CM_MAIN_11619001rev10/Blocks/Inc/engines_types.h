/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		engines_type.h
*
*	\ingroup	MPACCESS
*
*	\brief		Type definition of Engine parameters
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef ENGINES_TYPES_H_INCLUDED
#define ENGINES_TYPES_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "chalg.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/
#define ENGINE_MP_HF1_STANDARD_POWERFACTOR (0.95)
/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/* TempSensor_No :
 *   return an invalid value
 *
 * TempSensor_5V_Ntc :
 *   1/T = A + B*ln(R_NTC) + C*ln(R_NTC)*ln(R_NTC) Steinhart-Hart equation
 *   measured is R_NTC/R insert this into equation and compensate
 *   1/T = A + B*(ln(R_NTC/R) + ln(R)) + C*(ln(R_NTC/R) + ln(R))^2 = A + B*(ln(R_NTC) + ln(R))* + C*(ln(R_NTC)^2 + 2*ln(R)*ln(R_NTC) + ln(R)*ln(R)) =
 *   (A + B*ln(R) + C*ln(R)*ln(R)) + (B + 2*C*ln(R))*ln(R_NTC) + C*ln(R_NTC)*ln(R_NTC)
 *
 *   It should possible to fit this to any combination of R and NTC thermistor as long as the voltage is 5 volt
 */
typedef enum {
	TempSensor_No,
	TempSensor_3_3V_Ntc,
	TempSensor_5V_Ntc,
	TempSensor_5V_Ptc,
	TempSensor_MTM_HF2_NTC,
	TempSensor_MTM_HF3_NTC,
	TempSensor_Robust_Int,
	TempSensor_Robust_Batt
} TempSense_enum;

typedef enum {
	TempSensorDigital_No,
	TempSensorDigital_High,
} TempSenseDigital_enum;

typedef struct {
	TempSense_enum			Type;				//    \   Coefficients used
	float					A;					//     \  for calculation of
	float					B;					//      } temperature from
	float					C;					//     /  input voltage.
	float					D;					//    /
	float					ShortCircuit;		// Short circuit assumed below this ADC value or temperature sensor bottom out
	float					OpenCircuit;		// Open circuit assumed below this ADC value or temperature sensor bottom out
	float					Derate;				// used for temperature control of heat sink
	float					Overtemperature;	// trip over temperature error.
} Temperature_type;

// Scale factor is use in formula
// BitToNorm: Normalized = Slope*<Bit> + Offset
// NormToBit: Bits = Slope*<Normalized> + Offset
typedef struct {
	struct {
		float					UadSlope;		// bit/Volt
		float					UadOffset;		// bit
		float					IadSlope;		// bit/Ampere
		float					IadOffset;		// bit
		float					IpwmSlope;		// bit/Ampere
		float					IpwmOffset;		// bit
	} NormToBit;
	struct {
		float					UadSlope;		// Volt/bit
		float					UadOffset;		// Volt
		float					IadSlope;		// Ampere/bit
		float					IadOffset;		// Ampere
		float					IpwmSlope;		// Ampere/bit
		float					IpwmOffset;		// Ampere
	} BitToNorm;
} Scalefactor_type;

typedef struct {
	float					KpU;			// Proportional Gain voltage limit control
	float					KIU;			// Integral Gain voltage limit control
	float					KchooseU;		// Weight constant multiplied by current error for choosing voltage limit control
	float					KpI;			// Proportional gain current control
	float					KII;			// Integral gain current control
	float					KchooseI;		// Weight constant multiplied by current error for choosing current control
	float					KpP;			// Proportional gain power limit control
	float					KIP;			// Integral gain power limit control
	float					KchooseP;		// Weight constant multiplied by current error for choosing power limit control
	float					KpT;			// Proportional gain temperature control
	float					KIT;			// Integral gain temperature control
	float					KchooseT;		// Weight constant multiplied by current error for choosing temperature limit control
} EngineRegulator_type;

typedef struct {
	float					Volt;		// Volt
	float					Curr;		// Ampere
} EngineUIchar_type;

typedef struct {
	float					offset;		// Volt
	float					gain;		// Volt
} EngineAccuracy_type;

typedef struct {
	float					Low;		// Ampere
	float					High;		// Ampere
	int						StartTime;	// Time to wait before data is sent
} EngineBitCurrent_type;


typedef struct {
	Uint32							Code;          // Engine code
	float							Ri;
	float							ConversionfactorPowerInToDc;		// Idc = P*n/Udc, P power, n efficiency, Udc battery voltage
	float							ConversionfactorPhaseCurrentToDc;	// Idc = P*n/Udc = sqrt(3)U*Iphase*k*n/Udc = Iphase*(sqrt(3)*U*k*n)/Udc, P power, n efficiency, Udc battery voltage, k power factor
	const EngineUIchar_type *		UIchar_p;
	int								UIcharLength;
	const EngineAccuracy_type*		Uaccuracy_p;
	const EngineAccuracy_type*		Iaccuracy_p;
	EngineRegulator_type			Regulator;
	Scalefactor_type				ScalefactorNom;
	const Temperature_type*			Theatsink;			// parameters for heat sink temperature sensor, this used for temperature control
	TempSenseDigital_enum			Ttrafo;				// parameters for transformator temperature sensor
	EngineBitCurrent_type			BitCurrent;			// logic levels for signals sent via battery cable
} Engine_type;

typedef enum {
	EngineType_NA,
	EngineType_MP_HF1,
	EngineType_MP_HF2,
	EngineType_MP_HF3,
	EngineType_PF_HF1,
	EngineType_PF_HF2
} EngineType_enum;

#define ENGINE_LEN 101
#define CEC_ENGINE_LEN 30							// Length of CEC engine restriction table
/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/
int	VoltageAboveEngineMax(const float voltage);		// Return 1 if voltage is above engine max, 0 otherwise
float engineVoltageMax();							// Return maximum voltage in (V)
float engineCurrentMax(float voltage);				// Return maximum allowed current in (A) for a given voltage in (V)
float enginePowerMax();							// Return maximum allowed power in (W)
float engineNominalCurrent();						// Return nominal current in (A)
const Engine_type *engineGetParameters();			// Return pointer to selected engine parameters
Uint32 engineGetEngineType();						// Return Engine type
void engineCalibrateUadslope(float gain);			// Calibrate voltage gain
void engineCalibrateIadslope(float gain);			// Calibrate current gain
void engineCalibrateIadoffset(float offset);		// Calibrate current offset
void engineCalibrateIpwmslope(float gain);			// Calibrate current PWM gain
void engineCalibrateIpwmoffset(float offset);		// Calibrate current PWM offset
uint32_t engineIadslopeCompensation(uint32_t Ipwm);	// Compensate Ipwm for IadSlope calibration
Uint32 engineGetCode(int index);
int engineGetLen();
void engineUpdate();								// Should be called then type of engine should be updated
void engineSupportedBatteryVoltage(void);			// Set supported battery voltages for current engine
int engineValidateParamsCEC(chalg_BattInfoType battInfo);	// Check for capacity and cell restrictions i CEC mode
/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/



/*****************************************************************************/

#endif
