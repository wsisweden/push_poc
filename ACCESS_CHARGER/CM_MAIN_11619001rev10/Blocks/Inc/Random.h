#ifndef RANDOM_H_
#define RANDOM_H_

#include "tools.h"

void  mp_srandom (unsigned int seed);			/* Initialize random function with seed */
int  mp_random ();								/* Random function */

#endif /* RANDOM_H_ */
