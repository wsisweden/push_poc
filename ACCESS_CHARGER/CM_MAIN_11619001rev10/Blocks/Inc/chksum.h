/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		chksum.h
*
*	\ingroup	CHKSUM
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 335 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef CHKSUM_H_INCLUDED
#define CHKSUM_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Flash area description for checksum checking.
 */
typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	Uint32					start;		/**< Flash area start address		*/
	Uint32					size;		/**< Flash area byte size			*/
} checksum_ChkArea;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Checksum checking data.
 */
typedef struct							/*''''''''''''''''''''''''''''''''''*/	
{										/*									*/
	checksum_ChkArea const_P *pArea;	/**< Current checksum check area	*/
	Uint32					bytes;		/**< Number of bytes left in area	*/
	BYTE *					pByteToChk;	/**< Ptr to current byte to check	*/
	Uint32					checksum;	/**< Cumulative checksum			*/
} checksum_ChkData;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/**
 * \brief	Selects the flash area to be checked.
 *
 * \param	pData_		Pointer to the checksum information in RAM.
 * \param	pArea_		Pointer to the constant flash area information.
 *
 * \return	-
 */

#define chksum_setArea(pData_, pArea_) (pData_)->pArea = (pArea_)


/**
 * \brief		Returns the calculated checksum.
 *
 * \param		pData_		Pointer to the checksum information in RAM.
 *
 * \details		The checkStep function must be called until it returns true 
 *				before this macro can be used. Only then will this macro
 *				return a proper checksum.
 *
 * \return		The calculated checksum as an Uint32.
 */

#define chksum_getChecksum(pData_) (pData_)->checksum

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern void		chksum_checkReset(checksum_ChkData *);
extern Boolean	chksum_checkStep(checksum_ChkData *, Uint32);
extern Boolean	chksum_checkExecute(checksum_ChkData *, Uint32);

/***************************************************************//** \endcond */
#endif
