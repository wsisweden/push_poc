/*
 $Revision: 235 $
 $Date: 2010-05-21 08:55:45 +0200 (fr, 21 maj 2010) $
 $Author: bsi $
 $HeadURL: https://subversion.mpi.local/svn/access/CM/Main/trunk/CM/access/SW/Inc/cairegu.h $
 */

/**************************************************************************
 Project:     ACCESS:CM
 Module:
 Contains:    Types for data used by charging algorithm, cluster control and regulator.
 Environment: Compiled and tested with GNU GCC.
 Copyright:   Micropower ED
 Version:
 ---------------------------------------------------------------------------
 History:
 Rev      Date      Sign  Description
 bsi   First version.
 ***************************************************************************/

/***************************************************************************
 Definition and Type
 ***************************************************************************/
#ifndef CAI_CC_H
#define CAI_CC_H

#include <stdint.h>

typedef enum {
	ReguMode_PowerSupply,		// Control as a power supply
	ReguMode_Off,				// Turn off
	ReguMode_On,				// Turn on if battery detected otherwise off
	ReguMode_Auto				// Equivalent to "ReguMode_On" above
} ReguMode_enum;

typedef struct {
  float Low;					// Lower limit in (V)
  float High;					// Higher limit in (V)
} Interval_type;				// Voltage interval

typedef struct {
	float U;         			// Voltage in (V)
	float I;         			// Current in (A)
	float P;         			// Power in (W)
} ReguValue_type;

typedef enum {LimitNone, LimitUset, LimitIset, LimitPset, LimitT, LimitUengine, LimitIengine, LimitPengine, LimitIdc, LimitIac, LimitPac, LimitPhase} limiter_type;

typedef union {
	struct {
		uint8_t VoltageIsNearSetValue:1;	// Should be true if and only if voltage is close to or above set reference value
		uint8_t CurrentIsNearSetValue:1;	// Should be true if and only if current is close to or above set reference value
		uint8_t PowerIsNearSetValue:1;		// Should be true if and only if power is close to or above set reference value
		limiter_type limit:4;				// Currently active limit used by regulator, actual value may or may not be close to this limit
	} Detail;
	uint8_t Sum;							// Info
} ChargerInfo_type;

typedef union {
	struct {
		uint8_t Phase:1;					// Fuse probably need to be changed but the charger still work at low output current
		uint8_t UserParam:1;				// De-rated because of a limit set by user in software for example: phase current, input power, output current
		uint8_t Engine:1;					// Limited by engine parameters
		uint8_t Temperature:1;				// Limited by charger temperature
		uint8_t NoLoad:1;					// No load equal to battery disconnected, note logic level because charger is de-rated to zero then no load is connected
		uint8_t LowVoltage:1;				// Battery is considered as connected but voltage is below UactOff->Low
		uint8_t HighVoltage:1;				// Battery is considered as connected but voltage is above UactOff->High
	} Detail;
	uint8_t Sum;							// De-rate, note set value limits should not be included here since engine will always be limited by something
} ChargerDerate_type;

typedef union {
	struct {
		uint8_t Phase:1;					// Fuse need to be changed, charger could still deliver some output
		uint8_t Regulator:1;				// Output to engine do not agree with measured value
		uint8_t LowChargerTemperature:1;	// Charger temperature low
		uint8_t HighChargerTemperature:1;	// Charger temperature high and charger turned off, if derate but turned on this should not be true
		uint8_t LowBoardTemperature:1;		// Board temperature low
		uint8_t HighBoardTemperature:1;		// Board temperature high
		uint8_t HighTrafoTemperature:1;		// Trafo temperature high, power unit shut off
		uint8_t Watchdog:1;					// Charger disappeared or communication lost, at least one of the expected messages did not arrive in time
	} Detail;
	uint8_t Sum;							// Warning, different from zero if and only if a faulty component is found
} ChargerWarning_type;

typedef union {
	struct {
		uint8_t Phase:1;					// Fuse need to be changed and the charger can not deliver output, observe if charger could deliver output this should be zero
		uint8_t Regulator:1;				// Output to engine do not agree with measured value
		uint8_t LowChargerTemperature:1;	// Charger temperature low
		uint8_t HighChargerTemperature:1;	// Charger temperature high and charger turned off, if derate but turned on this should not be true
		uint8_t LowBoardTemperature:1;		// Board temperature low
		uint8_t HighBoardTemperature:1;		// Board temperature high
		uint8_t HighTrafoTemperature:1;		// Trafo temperature high, power unit shut off
		uint8_t Watchdog:1;					// Charger disappeared, it is not possible to communicate with a charger which is supposed to be there
	} Detail;
	uint8_t Sum;							// Error, different from zero if and only if the charger stop. This may or may not be because of a faulty
} ChargerError_type;						// component.

typedef union {
	struct {
		ChargerInfo_type		Info;		// Information
		ChargerDerate_type		Derate;		// Should be different from zero if and only if the charger is limited by something else than the set reference values
		ChargerWarning_type		Warning;	// Should be different from zero if and only if a faulty component is detected
		ChargerError_type		Error;		// Should be different from zero if and only if the charger stop and can not deliver output
	} Detail;
	uint32_t Sum;
} ChargerStatusFields_type;

typedef enum {
	BatterydetectCond_NotUsed,				// Do use this condition to test for a battery
	BatterydetectCond_False,				// Battery detection condition should be false to detect a battery
	BatterydetectCond_True					// Battery detection condition should be true to detect a battery
} BatterydetectCond_enum;					// Battery detection conditions

union ReguOnAndOff_type {					// Battery disconnect and connect condition
	struct
	{
		BatterydetectCond_enum off:4;		// Battery disconnect condition
		BatterydetectCond_enum on:4;		// Battery connect condition
	};
	uint8_t both;
};

typedef struct {
	ReguValue_type				Set;		// Set reference values
	ReguMode_enum           	Mode;		// Regulator mode
	union ReguOnAndOff_type		Uact;		// Voltage interval battery detection condition, used voltage intervals are also required if selected to be used
	union ReguOnAndOff_type		RemoteIn;	// Remote in battery detection condition
	uint8_t						soc;		// State of charge received from external source
} ChargerSet_type;

typedef struct {
	ReguValue_type				Meas;		// Measurements
	ChargerStatusFields_type	Status;		// Status fields
} ChargerMeas_type;

#endif  // #ifndef CAI_CC_H
/*******************************************************************************
END OF FILE
*******************************************************************************/
