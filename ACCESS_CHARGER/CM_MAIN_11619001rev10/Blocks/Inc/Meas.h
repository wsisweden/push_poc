/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Meas.h
*
*	\ingroup	MEAS
*
*	\brief		Global declarations of MEAS FB.
*
*	\details
*
*	\note
*
*	\version	22-10-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef MEAS_H_INCLUDED
#define MEAS_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_W32
#include "protif.h"
#endif
#include "tools.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * \name	Keypad button masks
 *
 * \brief	The following flags can be used in the 16-bit parameter of the 
 *			keybChanged event.
 */

/*@{*/
#define MEAS_KEY_STOP		(1<<0)
#define MEAS_KEY_F1			(1<<1)
#define MEAS_KEY_F2			(1<<2)
#define MEAS_KEY_ESC		(1<<3)
#define MEAS_KEY_OK			(1<<4)
#define MEAS_KEY_LEFT		(1<<5)
#define MEAS_KEY_RIGHT		(1<<6)
#define MEAS_KEY_UP			(1<<7)
#define MEAS_KEY_DOWN		(1<<8)
/*@}*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/
Uint32 current2pwm(
	float 				   Current
);
float ADC2Ampere(
	Int32 					AdValue
);
Uint32 Volt2ADC(
	float 					Voltage
);
Uint32 Ampere2ADC(
	float 					Current
);
float AdValueToVolt(
	Uint32					sampleCount,
	Int32 					AdValue
);
float AdValueToAmpere(
	Uint32					sampleCount,
	Int32 					AdValue
);
float AdValueToPower(
	Uint32					sampleCount,
	Int32 					AdValue
);
Uint32 Power2ADC(
	float 					Power
);

extern void					meas_handleAdcValues(void *);
extern Boolean				meas_getFilteredMains(void *);
PUBLIC int 					meas_getPhaseError(void);
PUBLIC void 				meas_endChargeVoltage(void);
PUBLIC uint16_t				meas_getIoBoardSum(void);
PUBLIC int	 				meas_GetDeltaVoltageOff();
PUBLIC Int32 				meas_GetDeltaVoltage();
PUBLIC Boolean 				meas_GetDeltaVoltageLimits(Int32 *deltaLimitOff, Int32 *deltaLimitOn, Int32 *voltageLimit);

#if TARGET_SELECTED & TARGET_W32
extern Uint8				meas__tiltSimuProc(protif_Request *);
#endif

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
