#ifndef DPL_H
#define DPL_H

#define DPL_LIST_SIZE	32
#define DPL_MASTER_WATCHDOG_CNT	3
#define DPL_SLAVE_WATCHDOG_CNT	3
#define DPL_ALL 0xFF

typedef struct{
	Uint8	dplId;
	Uint16 	nodeId;
	Uint8	soc;
	Uint8	priorityFactor;
	Uint32	priority;
	Uint16	powerRequested;
	Uint16	powerAllocated;
	Uint16	powerRemaining;
	Uint32	powerConsumed;
	Uint8	watchdog;
}dplList_t;

typedef enum{
	DplDisabled,
	DplMaster,
	DplSlave
}dplPwrGrpFunc_enum;

typedef enum{
	DplPriolow,
	DplPrioNormal,
	DplPrioHigh
}dplPrioFactor_enum;

typedef enum{
	DplModeOff,
	DplModeNormal,
	DplModeSafe
}dplMode_enum;

typedef enum{
	DplPowerOff,
	DplPowerOn
}dplPower_enum;

void dplMasterWatchdogReset();
void dplMasterWatchdogRun();
void dplMasterConflictSet();
void dplMasterConflictClear();
void dplNoMasterSet();
void dplNoMasterClear();
//void dplSlaveWatchdogReset();
int dplSlaveWatchdogRun();
void dplInit();
Uint8 dplUpdateSlave(Uint16 nodeId, Uint8 powerUnit, Uint8 soc, Uint8 priorityFactor, Uint16 powerRequest, Uint32 powerConsumed);
void dplUpdateMaster();
void dplPowerCalculation();

/* Local variable get/put functions */
Uint32 dplGetPowerUnitsCtrl();
Uint32 dplGetPowerRequestedTotal();
Uint32 dplGetPowerAllocatedTotal();
Uint32 dplGetPowerConsumedTotal();
Uint16 dplGetPowerAllocated(Uint8 dplId);
Uint16 dplGetPowerRequested(Uint16 pwrReq);
Uint32 dplGetPowerConsumed();
Uint32 dplGetDplPacLimit();
Uint16 dplGetPacRequest();
Uint8 dplGetSoc();
Uint8 dplGetDplId();
Uint32 dplGetPset();

void dplPutMode(Uint8 mode);
void dplPutDplPacLimit(Uint32 pacLimit);
void dplPutDplId(Uint8 dplId);

/* NVM parameter get/put functions */
Uint8 dplGetPowerGroup();
Uint8 dplGetPowerGroupFunc();
Uint8 dplGetDplPriorityFactor();
Uint8 dplGetDplPacLimit_default();
Uint32 dplGetDplPowerLimitTotal();
void dplPutPacLimit(Uint32 pacLimit);
void dplPutPowerGroup(Uint8 powerGroup);
void dplPutPowerGroupFunc(Uint8 powerGroupFunc);
void dplPutDplPriorityFactor(Uint8 priorityFactor);
void dplPutDplPacLimitDefault(Uint8 pacLimit_default);
void dplPutDplPowerLimitTotal(Uint32 powerLimitTotal);

#endif /* DPL_H */
