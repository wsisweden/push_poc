/* 30-10-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Chalg.h
*
*	\ingroup	CHALG
*
*	\brief		Public declarations of CHALG FB.
*
*	\details	
*
*	\note		
*
*	\version	
*
*******************************************************************************/

#ifndef CHALG_H_INCLUDED
#define CHALG_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/
#include "cai_cc.h"
#include "Cm3_Arm7.h"
#include <stdbool.h>
/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * 	\name	ChalgStatus register bits
 *	
 *	\brief	The following flags can be set in the chalgStatus register of
 *			CHALG FB.
 */

/*@{*/
#define CHALG_STAT_MAINS_CONN 			(1<<0)	/**< Main connected			*/
#define CHALG_STAT_BATTRY_CONN 			(1<<1)	/**< Battery connected		*/
#define CHALG_STAT_BM_CONNTECTED		(1<<2)	/**< BM connected			*/
#define CHALG_STAT_MAIN_CHARG			(1<<3)	/**< Main charging			*/
#define CHALG_STAT_ADDITIONAL_CHARG		(1<<4)	/**< Additional charging	*/
#define CHALG_STAT_EQUALIZE_CHARG		(1<<5)	/**< Equalize charging		*/
#define CHALG_STAT_MAINTENANCE_CHARG	(1<<6)	/**< Maintenance charging	*/
#define CHALG_STAT_CHARG_COMPLETED		(1<<7)	/**< Charging complete		*/
#define CHALG_STAT_PAUSE				(1<<8)	/**< Pause					*/
#define CHALG_STAT_PRE_CHARG			(1<<9)	/**< Pre-charging stage		*/
#define CHALG_STAT_FORCE				(1<<10)	/**< Force start			*/
#define CHALG_STAT_BBC					(1<<11)	/**< BBC status flag		*/
#define CHALG_STAT_TBATTLOW				(1<<12)	/**< Low battery temp		*/
#define CHALG_STAT_TBATTHIGH			(1<<13)	/**< High battery temp		*/
#define CHALG_STAT_RESTRICTED			(1<<14)	/**< Charging restricted	*/
#define CHALG_STAT_DOWN					(1<<15)	/**< CHALG is down			*/
/*@}*/

/**
 * 	\name	BBCStatus register bits
 *
 *	\brief	The following flags can be set in the BBCStatus register of
 *			CHALG FB.
 */

/*@{*/
#define BBC_STAT_PRE			(1<<0)	/**< Pre-charging stage		*/
#define BBC_STAT_MAIN			(1<<1)	/**< Main charging			*/
#define BBC_STAT_ADDITIONAL		(1<<2)	/**< Additional charging	*/
#define BBC_STAT_EQUALIZE		(1<<3)	/**< Equalize charging		*/
#define BBC_STAT_MAINTENANCE	(1<<4)	/**< Maintenance charging	*/
#define BBC_STAT_COMPLETED		(1<<5)	/**< Charging complete		*/
/*@}*/

/**
 * 	\name	ChalgError register bits
 *
 *	\brief	The following flags can be set in the chalgError register of
 *			CHALG FB.
 */

/*@{*/
#define CHALG_ERR_LOW_BATTERY_VOL		(1<<0)	/**< Low battery start voltage	*/
#define CHALG_ERR_HIGH_BATTERY_VOL		(1<<1)	/**< High battery start voltage	*/
#define CHALG_ERR_CHARGE_TIME_LIMIT		(1<<2)	/**< Charge time limit			*/
#define CHALG_ERR_CHARGE_AH_LIMIT		(1<<3)	/**< Charge Ah limit			*/
#define CHALG_ERR_INCORRECT_ALGORITHM	(1<<4)	/**< Incorrect algorithm		*/
#define CHALG_ERR_INCORRECT_MOUNT		(1<<5)	/**< Tilt sensor error			*/
#define CHALG_ERR_HIGH_BATTERY_LIMIT	(1<<6)	/**< High battery voltage limit	*/
#define CHALG_ERR_BATTERY_ERROR			(1<<7)	/**< Battery error (Delta I/U/P)*/
#define CHALG_ERR_BM_TEMP				(1<<8)	/**< BM high battery temp		*/
#define CHALG_ERR_BM_ACID				(1<<9)	/**< BM low acid level			*/
#define CHALG_ERR_BM_BALANCE			(1<<10)	/**< BM voltage balance error	*/
#define CHALG_ERR_LOW_AIRPUMP_PRESSURE	(1<<11)	/**< Low AirPump pressure		*/
#define CHALG_ERR_HIGH_AIRPUMP_PRESSURE	(1<<12)	/**< High AirPump pressure		*/
#define CHALG_ERR_BM_LOW_SOC			(1<<13)	/**< BM low State Of Charge		*/
/*@}*/

/**
 * 	\name	Charging mode values.
 *
 *	\brief	Register ChargingMode can have following values.
 */
/*@{*/
#define CHALG_USER_DEF	0
#define CHALG_BM	1
#define CHALG_DUAL	2
#define CHALG_MULTI	3
/*@}*/

/**
 * 	\name	Equalize_active values.
 *
 *	\brief	Register Equalize_active can have following values.
 */
/*@{*/
#define CHALG_EQUALIZE_DISABLED	0
#define CHALG_EQUALIZE_ENABLED	1
/*@}*/

/**
 * 	\name	Equalize_func values.
 *
 *	\brief	Register Equalize_func can have following values.
 */
/*@{*/
#define CHALG_EQU_FUNC_DISABLED	0
#define CHALG_EQU_FUNC_CYCLIC	1
#define CHALG_EQU_FUNC_WEEKDAY	2
/*@}*/

#define CHALG_ENGINE_ON !(FIO2PIN & (1<<13))

/**
 *	Defines nominal battery voltages
 */
#define BATTERY_NO_EDIT		0
#define BATTERY_6_CELLS		1
#define BATTERY_12_CELLS	2
#define BATTERY_18_CELLS	3
#define BATTERY_24_CELLS	4
#define BATTERY_40_CELLS	5

#define BATTERY_NO_ERROR			0
#define BATTERY_INVALID_CELLS		1
#define BATTERY_INVALID_CAPACITY	2

/**
 *	Defines maximum number of characters for algorithm name arrays
 */
#define CHALG_ALG_NAME_LEN_MAX 12

/**
 *	Defines The bit config byte
 */
#define CHALG_BITCFG_CEC		(1<<0)	/**< CEC mode	*/
#define CHALG_BITCFG_USER_P		(1<<1)	/**< User param	mode */
#define CHALG_BITCFG_QUIET_DERATE	(1<<2)	/**< No temperature alarm until 50% current derate	*/
#define CHALG_BITCFG_DPL		(1<<3)	/**< DPL function activated	*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Type for battery information register. This is used to store default battery
 * information to flash memory.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint16					algNo;		/**< Algorithm number				*/
	Uint16					capacity;	/**< Battery capacity				*/
	Uint16					cableRes;	/**< Cable resistance				*/
	Uint16					cell;		/**< Number of cells				*/
	Uint16					baseLoad;	/**< Base current consumption		*/
} chalg_BattInfoType;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8					suported;	/**< Supported by selected engine	*/
	Uint8					enabled;	/**< parameters in use if TRUE		*/
	Uint16					algNo;		/**< Algorithm number				*/
	Uint16					capacity;	/**< Battery capacity				*/
	Uint16					cableRes;	/**< Cable resistance				*/
	Uint16					cell;		/**< Number of cells				*/
	Uint16					baseLoad;	/**< Base current consumption		*/
	Int16					batteryTemp; /**< Battery temperature			*/
	Int16					batteryTempF; /**< Battery temperature Fahrenheit	*/
} chalg_BattInfoMultiType;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * CaPack register handle type. Used by the CaPack register.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	BYTE * const			pCaPack;	/**< Pointer to CaPack data			*/
} chalg_CaPackHandle;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

//extern Uint8		chalg_storeBatteryInfo(void);

extern reg_Status	chalg_caPackWrite(void const_P *,reg_FileIo *);
extern reg_Status	chalg_caPackRead(void const_P *, reg_FileIo *);
extern reg_Status	chalg_caPackControl(void const_P *,void *,void const_P *);

extern void			chalg_activate(void *pInstance);
void chalg_Chalg(ChargerMeas_type Measurements, ChargerSet_type* ChalgOutput, Interval_type* UactOffInterval, Interval_type* UactOnInterval);
extern Uint8		chalg_capackEnum(Uint8,BYTE *,Uint8 *);
extern Uint8		chalg_UserParam(Uint8,BYTE *,Uint8 *);
extern Uint16 		chalg_GetAlgNo(Uint16 AlgIdx);
extern Uint16 		chalg_GetUserParamAlgNo(Uint16 AlgIdx);
extern bool 		chalg_GetCurveData(Uint16 idNumber, void** curveData);
extern Uint16 		chalg_GetAlgIdx(Uint16 AlgNo);
extern uint16_t 	chalg_GetAlgorithmCount(void);
extern uint16_t 	chalg_GetUserParameterAlgorithmCount(void);
extern Uint8 		chalg_UpdateAlgIdx(Uint16 AlgNo);
extern Uint8 		chalg_UpdateAlgIdxFromBm(Uint16 AlgNo);
extern Uint8 		chalg_UserParamIsValid(Uint16 AlgNo);
extern Boolean 		chalg_GetAirPumpFailed(void); 	// Return error flag
extern Boolean 		chalg_GetAirPumpOutput(void); 	// Return Airpump function from Chalg
extern Boolean 		chalg_GetWaterPumpOutput(void); // Return Water function from Chalg
extern void 		chalg_resetStoredParameters(const int algID, const int algVer);
extern void			chalg_setStoredUserParameters(int len);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern sys_TypeTools				chalg_BattInfoTools;
extern sys_TypeTools				chalg_BattInfoMultiTools;
extern chalg_CaPackHandle const_P	chalg_caPackHandle;
extern volatile int 				nfcUserParamsEdited;
extern volatile int 				nfcStoredUserParamsEdited;

/***************************************************************//** \endcond */

#endif
