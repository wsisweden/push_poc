/* 30-10-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Regu.h
*
*	\ingroup	REGU
*
*	\brief		REGU FB public declarations.
*
*	\details	
*
*	\note		
*
*	\version	
*
*******************************************************************************/

#ifndef REGU_H_INCLUDED
#define REGU_H_INCLUDED

#include "Cm3_Arm7.h"

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define PINMODE_OD2    (*(volatile unsigned long *)(PINCON_BASE_ADDR + 0x70))
#define PINCON_BASE_ADDR 0x4002C000
#define REGU_CHARGING_ON_DO_OFF() {FIO2SET = (1<<13);}
#define REGU_CHARGING_ON_DO_ON() {FIO2CLR = (1<<13);}
#define REGU_CHARGING_ON_DO_INIT() {PINMODE_OD2 |= (1<<13); FIO2SET = (1<<13); FIO2DIR |= (1<<13);}
#define REGU_ENGINE_ON !(FIO2PIN & (1<<13))

/**
 * \name	Regulator error bit definition
 * 			For detailed description see cai_cc.h
 */
#define REGU_ERR_PHASE_ERROR				(1<<0)
#define REGU_ERR_REGULATOR_ERROR			(1<<1)
#define REGU_ERR_LOW_CHARGER_TEMPERATURE	(1<<2)
#define REGU_ERR_HIGH_CHARGER_TEMPERATURE	(1<<3)
#define REGU_ERR_LOW_BOARD_TEMPERATURE		(1<<4)
#define REGU_ERR_HIGH_BOARD_TEMPERATURE		(1<<5)
#define REGU_ERR_HIGH_TRAFO_TEMPERATURE		(1<<6)
#define REGU_ERR_WATCHDOG					(1<<7)

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/
#include "cai_cc.h"

extern void regu_activate(void *pInstance);
extern ChargerStatusFields_type regu_Regulate();

extern void regu_setInput(ChargerSet_type* input);
extern void regu_setUactDisconnect(Interval_type* interval);
extern void regu_setUactConnect(Interval_type* interval);
extern void regu_getUactOnOffLimits(Interval_type* UactOff, Interval_type* UactOn);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
