Export and import text:

Run set_env from ..\Exe folder
Run textExport from ..\Blocks folder
A file texts.txt will be created in Blocks folder.

Open Excel and import data from text/CSV
Choose Unicode (UTF-8) format.

Save file as texts.txt (Unicode-text)
Remove first row with (Column1 Column2 etc)
Save again.

Run textImport
If all goes well the text in INIT.h files will be same as before (if not edited).
