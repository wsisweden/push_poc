function testClusterControl()
  printf("Compilation of cluster control test bench started\n\n");

  if(system("mingw32-make --no-print-directory -C obj"))
    pause
    return
  endif
  
  printf("\nSimulation of cluster control started\n");
  cd generated
  
  len = 800;
  Uref = linspace(48.0, 48.0, len)';
  Iref = [linspace(30, 30, len/4) linspace(550, 550, len/4) linspace(180, 180, len/8) linspace(180, 180, len/8) linspace(350, 350, len/8) linspace(180, 180, len/8)]';
  Pref = linspace(8e3, 8e3, len)';
  
  meas = systemWrapper(Uref, Iref, Pref);
  t = (1:len)';

  h = figure;
  plot(t, meas.SetI(:,1), t, meas.MeasI(:,1));
  legend("I_{set}", "I_{meas}")
  xlabel("t")
  ylabel("Current in (A)")
  title("Set currents 1")
  
  h = figure;
  plot(t, meas.SetI(:,2), t, meas.MeasI(:,2));
  legend("I_{set}", "I_{meas}")
  xlabel("t")
  ylabel("Current in (A)")
  title("Set currents 2")
  
  h = figure;
  plot(t, meas.SetI(:,3), t, meas.MeasI(:,3));
  legend("I_{set}", "I_{meas}")
  xlabel("t")
  ylabel("Current in (A)")
  title("Set currents 3")
  
  h = figure;
  plot(t, meas.SetI(:,4), t, meas.MeasI(:,4));
  legend("I_{set}", "I_{meas}")
  xlabel("t")
  ylabel("Current in (A)")
  title("Set currents 4")
  
  h = figure;
  plot(t, meas.SetI(:,5), t, meas.MeasI(:,5));
  legend("I_{set}", "I_{meas}")
  xlabel("t")
  ylabel("Current in (A)")
  title("Set currents 5")
  
  h = figure;
  plot(t, meas.SetI(:,6), t, meas.MeasI(:,6));
  legend("I_{set}", "I_{meas}")
  xlabel("t")
  ylabel("Current in (A)")
  title("Set currents 6")

  
  h = figure;
  plot(t, Uref, t, 0*sum(meas.MeasU, 2)+1.0, t, meas.measSumU)
  legend("U_{ref}", "0*U_{sum}", "U_{meas}")
  xlabel("t")
  ylabel("Voltage in (V)")
  title("Voltages")
  
  h = figure;
  plot(t, Iref, t, sum(meas.MeasI, 2)+1.0, t, meas.measSumI)
  legend("I_{ref}", "I_{sum} + 1.0", "I_{meas}")
  xlabel("t")
  ylabel("Current in (A)")
  title("Sum of currents for cluster")
  
  printf("Simulation done press enter to exit\n")
  pause
endfunction
