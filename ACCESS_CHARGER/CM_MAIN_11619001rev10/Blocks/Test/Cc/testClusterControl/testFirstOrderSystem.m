function testFirstOrderSystem()
  printf("Compilation of cluster control test bench started\n\n");

  if(system("make --no-print-directory -C obj"))
    pause
    return
  endif
  
  printf("\nSimulation of cluster control started\n");
  cd generated
  
  len = 8*80;
  Uref = linspace(48.0, 48.0, len);
  Iref = [linspace(0, 380, len/2) linspace(180, 180, len/8) linspace(180, 180, len/8) linspace(350, 350, len/8) linspace(180, 180, len/8)];
  Pref = linspace(8e3, 8e3, len);
  
  meas = systemWrapper(Uref, Iref, Pref);
  t = 1:len;

  figure
  plot(t, meas.measSumI)
  legend("Isum")
  xlabel("t")
  ylabel("current in (A)")
  title("Measured current sum")
  
  figure
  plot(t, meas.Set1I, t, meas.Set2I-1, t, meas.Set3I-2)
  legend("I1", "I2", "I3")
  xlabel("t")
  ylabel("current in (A)")
  title("Set currents")
  
  printf("Simulation done press enter to exit\n")
  pause
endfunction
