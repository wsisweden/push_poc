#ifndef LOAD_H
#define LOAD_H

#include "cai_cc.h"

ReguValue_type load(ReguValue_type set);			// Return measured output for the applied input

#endif
