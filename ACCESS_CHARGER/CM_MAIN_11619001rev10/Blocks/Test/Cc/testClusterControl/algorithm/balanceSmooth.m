function balanceSmooth()
	len = 3*60;
	chargers = 5;
	t = (1:len)';
	outs0 = rand(1,chargers);
	outs(len, chargers) = NaN;
	outs(1, :) = outs0;
	ref.I = [linspace(2.8, 2.8, len/3) linspace(3.8, 3.8, len/3) linspace(2.4, 2.4, len/3)]';
	
	for n = 1:len-1
		sum.I = 0;
		for charger = 1:chargers											% Figure out direction
			if(not(isnan(outs(n, charger))))
				sum.I = sum.I + outs(n, charger);
			endif
		endfor
		
		d = 1/60;
		remainder.I = ref.I(n);
		average.I = remainder.I/chargers;									% Average static value
		
		dists = outs(n, :) - average.I
		indexes = 1:chargers;
		indexesLen = chargers;
		
		for k = 1:chargers													% For as many times as there are chargers
			posTry = 1;
			absDist = abs(dists(indexes(1)));								% Try this first
			for pos = 2:indexesLen											% For all others
				if(abs(dists(indexes(pos))) < absDist)						% Found smaller distance ?
					absDist = abs(dists(indexes(pos)));
					posTry = pos;
				else
				endif
			endfor
			absDist															% Smallest distance
			indexes(posTry)
			if(absDist < d)
				%remainder.I = remainder.I + dists(indexes(posTry));
				outs(n+1, indexes(posTry)) = average.I;
			else
				if(dists(indexes(posTry)) < 0)
					%remainder.I = remainder.I - d;
					outs(n+1, indexes(posTry)) = outs(n, indexes(posTry)) + d;
				else
					%remainder.I = remainder.I - d;
					outs(n+1, indexes(posTry)) = outs(n, indexes(posTry)) - d;
				endif
			endif
			
			indexesLen = indexesLen - 1;
			indexes = [indexes(1:posTry - 1) indexes(posTry + 1:end)];
		endfor
		outs(n,:)
	endfor

	h = figure;
	plot(t, ref.I)

	h = figure;
	plot(t, outs)
endfunction
