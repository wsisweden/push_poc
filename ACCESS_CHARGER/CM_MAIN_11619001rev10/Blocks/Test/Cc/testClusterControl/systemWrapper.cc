#include <octave/oct.h>
#include <octave/ov-struct.h>
#include <octave/parse.h>
#include <inttypes.h>
#include <stdio.h>

extern "C"
{
#include "ClusterControl.h"
#include <math.h>
#include "load.h"
}

FILE* fp;

DEFUN_DLD(systemWrapper, args, nargout, "systemWrapper"){
  int nargin = args.length();
  octave_value_list retval;

  fp = fopen("../output/debug.out", "w");
  if(!fp){
    printf("Error could not open debug.out\n");
  }

  /* Check number of arguments */
  if (nargin != 3 || nargout != 1){
    print_usage ("meas = systemWrapper(Uref, Iref, Pref)");
    return retval;
  }

  NDArray Uref = args(0).array_value();
  NDArray Iref = args(1).array_value();
  NDArray Pref = args(2).array_value();

  if (!error_state){    
    const dim_vector dv = Iref.dims();
    const dim_vector dvCluster(dv.elem(0), CC_CHARGERS_LEN*dv.elem(1));
    octave_idx_type len = Iref.length();

    NDArray measSumU(dv);
    NDArray measSumI(dv);
    NDArray measSumP(dv);
    NDArray measSumStatus(dv);

    NDArray SetU(dvCluster);
    NDArray SetI(dvCluster);
    NDArray SetP(dvCluster);

    NDArray MeasU(dvCluster);
    NDArray MeasI(dvCluster);
    NDArray MeasP(dvCluster);

    uint32_t Chargers[] = {1, 2, 4, 3, 5, 0};
    //CcSetSerialNumberThis(15);

    for(int n = 0; n < len; n++){
        CcChargersUpdate(Chargers, 6);
    	ChargerMeas_type sum = CcMasterSum();
    	measSumU(n) = sum.Meas.U;
    	measSumI(n) = sum.Meas.I;
    	measSumP(n) = sum.Meas.P;
    	measSumStatus(n) = (double)sum.Status.Sum;

    	ChargerSet_type input;
    	input.Mode = ReguMode_PowerSupply;
    	input.Set.U = Uref(n);
    	input.Set.I = Iref(n);
    	input.Set.P = Pref(n);

    	CcMasterSet(&input, &sum, 150.0, 8e3);

    	for(int charger = 0; charger < CC_CHARGERS_LEN; charger++){
        	volatile const ChargerSet_type* Set;
        	const uint32_t SerialNr = CcSet(charger, &Set);
        	ChargerStatusFields_type status;
        	status.Info.Sum = 0;
        	status.Derate.Sum = 0;
        	status.Warning.Sum = 0;
        	status.Error.Sum = 0;

        	if(Set){
        		ReguValue_type chargerOutput;
        		chargerOutput.U = Set->Set.U;
        		chargerOutput.I = Set->Set.I;
        		chargerOutput.P = Set->Set.P;
            	SetU(n, charger) = Set->Set.U;
            	SetI(n, charger) = Set->Set.I;
            	SetP(n, charger) = Set->Set.P;
            	if(__isnanf(chargerOutput.I)){
            		chargerOutput.I = 0.0;
            	}
            	else if(chargerOutput.I > 160.0){
            		chargerOutput.I = 160.0;
            		status.Derate.Sum = 1;
            	}
            	const ReguValue_type meas = load(chargerOutput);
        		CcMeasUI(SerialNr, meas.U, meas.I);
            	CcMeasPStatus(SerialNr, meas.P, status);
            	MeasU(n, charger) = meas.U;
            	MeasI(n, charger) = meas.I;
            	MeasP(n, charger) = meas.P;
        	}
    	}
    }
    
    Octave_map meas;
    meas.assign("measSumU", measSumU);
    meas.assign("measSumI", measSumI);
    meas.assign("measSumP", measSumP);
    meas.assign("measSumStatus", measSumStatus);

    meas.assign("SetU", SetU);
    meas.assign("SetI", SetI);
    meas.assign("SetP", SetP);
    meas.assign("MeasU", MeasU);
    meas.assign("MeasI", MeasI);
    meas.assign("MeasP", MeasP);
    retval.append(octave_value(meas));
  }
  
  if(fp)
    fclose(fp);

  return retval;
}
