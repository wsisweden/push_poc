@echo off

echo Starting Octave
path = %path%;C:\Octave\3.2.4_gcc-4.4.0\bin\
octave --q --eval "cd %~dp0, testClusterControl()"
if ERRORLEVEL 1 goto OctaveRunError
goto End

:OctaveRunError
echo:
echo If Octave could not be found check path of Octave in the file "testClusterControl.bat"
echo Note "panic: Segmentation violation" is usually because data is read or written outside allocated region by program run by Octave
echo Check also for compilation warnings 
echo:
pause
goto end

:End
@echo on
