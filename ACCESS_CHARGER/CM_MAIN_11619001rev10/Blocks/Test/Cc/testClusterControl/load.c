#include "load.h"
#include "math.h"

const double U = 46.0;
const double I = INFINITY;
const double P = INFINITY;
const double R = 17e-3;
const enum{selectU, selectI, selectP, selectR} select = selectU;

ReguValue_type load(ReguValue_type set){
	ReguValue_type measured = set;

	switch(select){
	case selectU :
		if(U + R*set.I > set.U){
			measured.U = set.U;
			measured.I = (set.U - U)/R;
		}
		else{
			measured.U = U + R*set.I;
			measured.I = set.I;
		}
		measured.P = measured.U*measured.I;
		break;
	case selectI :
		if(set.I < I){
			measured.U = 0.0;
			measured.I = set.I;
		}
		else{
			measured.U = U;
			measured.I = I;
		}
		measured.P = measured.I*measured.P;
		break;
	case selectP :
		measured.U = 0.0;
		measured.I = 0.0;
		measured.P = 0.0;
		break;
	case selectR :
		measured.U = 0.0;
		measured.I = 0.0;
		measured.P = 0.0;
		break;
	}

	if(measured.I < 0.0){
		measured.I = 0.0;
	}

	return measured;
}
