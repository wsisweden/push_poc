#ifndef CHALG_CALC_ERRWIN_H
#define CHALG_CALC_ERRWIN_H

#include "cai_cc.h"
#include "cai_constparam.h"
#include "caimain.h"

void chalg_CheckErrWin(const ChalgToCc_Type* Set, ChargerMeas_type* measSum); // check the error windows which are used by the charging algorithm

#endif /* CHALG_STATUSCALCULATIONS_H_ */
