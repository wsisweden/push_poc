function testCharg()
  time = 60*60*25;
  saveGraphics = 0;

  cd obj;

  
  t = linspace(0, time, time);

  [measVoltage measCurrent setVoltage setCurrent battCurrent status phase Cai_Tick] = chargWrapper(t);
  voltage(1:5) = nan;
  
  t = Cai_Tick;%t/60;

  figure
  plot(t, measVoltage)
  a = axis;
  a(3:4) = [0 3];
  axis(a)
  title("Simulated charge")
  xlabel("t in (min)")
  legend("Measured voltage in (V)")
  if(saveGraphics)
    print("Simulated charge Meas voltage.png", "-dpng")
  endif

  figure
  plot(t, measCurrent)
  a = axis;
  a(3:4) = [0 150];
  axis(a)
  title("Simulated charge")
  xlabel("t in (min)")
  legend("Measured current in (A)")
  if(saveGraphics)
    print("Simulated charge Meas current.png", "-dpng")
  endif

  figure
  plot(t, phase)
  a = axis;
  a(3:4) = [0 110];
  axis(a)
  title("Simulated charge")
  xlabel("t in (min)")
  legend("phase")
  grid minor
  if(saveGraphics)
    print("Simulated phase.png", "-dpng")
  endif
return

  figure
  plot(t, battCurrent)
  a = axis;
  a(3:4) = [0 150];
  axis(a)
  title("Simulated charge")
  xlabel("t in (min)")
  legend("Battery current in (A)")
  if(saveGraphics)
    print("Simulated battery current.png", "-dpng")
  endif

  figure
  plot(t, setVoltage)
  a = axis;
  a(3:4) = [0 150];
  axis(a)
  title("Simulated charge")
  xlabel("t in (min)")
  legend("Set voltage in (V)")
  if(saveGraphics)
    print("Simulated set voltage.png", "-dpng")
  endif

  figure
  plot(t, setCurrent)
  a = axis;
  a(3:4) = [0 150];
  axis(a)
  title("Simulated charge")
  xlabel("t in (min)")
  legend("Set current in (A)")
  if(saveGraphics)
    print("Simulated set current.png", "-dpng")
  endif
endfunction
