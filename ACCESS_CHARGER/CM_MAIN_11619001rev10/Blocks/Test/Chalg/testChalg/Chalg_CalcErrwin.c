#include "Chalg_CalcErrwin.h"
#include "cai_cc.h"
#include "math.h"

#warning defines below not good
#define REGUSTATUS_BIT_VOLTAGE_IS_NEAR_THE_SET_VALUE (1<<0)
#define REGUSTATUS_BIT_CURRENT_IS_NEAR_THE_SET_VALUE (1<<1)
#define REGUSTATUS_BIT_POWER_IS_NEAR_THE_SET_VALUE (1<<2)

static void cc_UpdateStatus(int true, unsigned int* cnt, uint8_t* statusRegister, uint8_t StatusBit);

void chalg_CheckErrWin(const ChalgToCc_Type* Set, ChargerMeas_type* measSum)
{
	/* Decide if we considered to have small Err */
	/* ---------------------------------------- */

	/* Check if Uerr is inside window */
	{
		static unsigned int UerrWinCnt = 0;
		float Uerr;
		Uerr = Set->Set.U - measSum->Meas.U;
		cc_UpdateStatus(Uerr < Set->ErrWin.U, &UerrWinCnt, &measSum->Status.Info.Sum, REGUSTATUS_BIT_VOLTAGE_IS_NEAR_THE_SET_VALUE);
	}

	/* Check if Ierr is inside window */
	{
		static unsigned int IerrWinCnt = 0;
		float Ierr;
		Ierr = Set->Set.I - measSum->Meas.I;
		cc_UpdateStatus(Ierr < Set->ErrWin.I, &IerrWinCnt, &measSum->Status.Info.Sum, REGUSTATUS_BIT_CURRENT_IS_NEAR_THE_SET_VALUE);
	}

	/* Check if Perr is inside window */
	{
		static unsigned int PerrWinCnt = 0;
		float Perr;
		Perr = Set->Set.P - measSum->Meas.P;
		cc_UpdateStatus(Perr < Set->ErrWin.P, &PerrWinCnt, &measSum->Status.Info.Sum, REGUSTATUS_BIT_POWER_IS_NEAR_THE_SET_VALUE);
	}
}

static void cc_UpdateStatus(int true, unsigned int* cnt, uint8_t* statusRegister, uint8_t StatusBit)
{
#define PerrWinCnt_Max 9
#define PerrWinCnt_HystHigh 6
#define PerrWinCnt_HystLow 3
	if(true && *cnt < PerrWinCnt_Max)
		(*cnt)++;
	else if (!true && *cnt)
			(*cnt)--;

	if (*cnt > PerrWinCnt_HystHigh)
	{
		*statusRegister |= StatusBit;
	}
	else if (*cnt < PerrWinCnt_HystLow)
		*statusRegister &= ~StatusBit;
}
