@path = %path%;C:\Octave\3.2.4_gcc-4.4.0\bin\

@echo Starting Octave with script "testChalg.m"
@octave --q --eval "cd %~dp0, testChalg()"
@if ERRORLEVEL 1 goto OctaveRunError
@goto End

:OctaveRunError
@echo:
@echo If Octave could not be found check path of Octave in the file "testClusterControl.bat"
@echo Note "panic: Segmentation violation" is usually because data is read or written outside allocated region by program run by Octave
@echo Check especially the files generated from the spreadsheet in the directory "testChalg/generated". A common problem is that array in OutputPlot.c is to small.
@echo Check also for compilation warnings 
@echo:
@echo:
@pause
@goto end

:End
