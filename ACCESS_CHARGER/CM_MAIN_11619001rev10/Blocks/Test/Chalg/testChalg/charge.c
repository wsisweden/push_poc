#include "charge.h"
#include "cai_cc.h"
#include "cai_constparam.h"
#include "battery.h"
#include "OutputPlot.h"
#include <stdio.h>

static CaiInput_type  CaiInput;
static CaiOutput_type CaiOutput;
static struct battery_type batt;

void chargeInit(){
	batteryInit(&batt);

	CaiInput.Algorithm.AlgDef_p = OutputPlot_GetChalg();
	CaiInput.Algorithm.BaseLoad = 0.0;
	CaiInput.Algorithm.CableRes = 0.0;
	CaiInput.Algorithm.Capacity = batt.capacity/(60*60);
	CaiInput.Algorithm.Cells = 1;
	CaiInput.CcStatus.Sum = 0;
	CaiInput.Control = CAI_INPUT_CONTROL_CHARG;// | CAI_INPUT_CONTROL_FORCE;
	CaiInput.Input.Sum = 0;
	CaiInput.Measure.Iact = 0.0;
	CaiInput.Measure.Pact = 0.0;
	CaiInput.Measure.Tbatt = 25.0;
	CaiInput.Measure.Uact = 2.0;
	CaiInput.UserParam.Valid = 0;

	CaiOutput.ChalgToCc.Set.U = 0.0;
	CaiOutput.ChalgToCc.Set.I = 0.0;
}

void charge(){
    const int SamplesLen = OutputPlot_GetSamplesLen();

    int n;
    for (n = 0; n < SamplesLen; n++){
    	if(batt.MeasureT){
        	CaiMain(&CaiInput, &CaiOutput);												// run charging algorithm
    	}
    	else{
    		const float tmp = CaiInput.Measure.Tbatt;

    		CaiInput.Measure.Tbatt = 25.0f;
        	CaiMain(&CaiInput, &CaiOutput);												// run charging algorithm
    		CaiInput.Measure.Tbatt = tmp;
    	}

    	OutputPlotSaveData(&CaiInput, &CaiOutput);									// log
    	const ChargerSet_type const Set = {.Set = CaiOutput.ChalgToCc.Set, .Mode = CaiOutput.ChalgToCc.Mode};

    	CaiInput.CcStatus = batteryCharge(&batt, &CaiInput.Measure, &Set);			// run software battery
    }
}
