#ifndef BATTERY_H
#define BATTERY_H

#include "cai_cc.h"
#include "caimain.h"

struct battery_type{
	enum {batteryLeadAcid_enum, batteryLiFePO4_enum} type;
	double chargedPercent;
	double capacity;
	double cells;
	double leak;
	double chargeEfficiency;
	double T;
	int MeasureT;
};

void batteryInit(struct battery_type* state);

ChargerStatusFields_type batteryCharge(struct battery_type* state, Measure_type* out, const ChargerSet_type* set);

ChargerStatusFields_type batteryChargeLeadAcid(struct battery_type* state, Measure_type* out, const ChargerSet_type* set);
ChargerStatusFields_type batteryChargeLiFePh(struct battery_type* state, Measure_type* out, const ChargerSet_type* set);

#endif
