#ifndef CHARGE_H_
#define CHARGE_H_

#include <inttypes.h>
#include "cai_cc.h"

void chargeInit();

void charge();

int charge_GetValuesSize();

double* OutputPlot_GetVariable(int n);

#endif /* CHARGE_H_ */
