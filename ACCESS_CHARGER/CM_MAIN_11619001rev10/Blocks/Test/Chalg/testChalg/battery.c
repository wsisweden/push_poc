#include "battery.h"

/* The selection of battery must in some way be passed from the excel sheet to the C code. Currently this is achieved by setting the battery type in the
 * same structure used to pass the parameters. This works very well since then all parameters go into the same structure.
 * Other possible solutions had been:
 *   1. Remove this selector and link to the used battery.
 *   1. Use C++ and a virtual class.
 * An advantage with this solution is that all battery types are compiled into the software each time so a compilation error will be discovered at
 * then introduced instead of the first time the battery is used.
 */

ChargerStatusFields_type batteryCharge(struct battery_type* state, Measure_type* out, const ChargerSet_type* set){
	switch(state->type){
	case batteryLeadAcid_enum :
		return batteryChargeLeadAcid(state, out, set);
		break;
	case batteryLiFePO4_enum :
		return batteryChargeLiFePh(state, out, set);
		break;
	}														// Note, no default will give a warning in case of missing enumeration value

	{
		ChargerStatusFields_type tmp = {.Sum = 0};

		return (ChargerStatusFields_type)tmp;
	}
}
