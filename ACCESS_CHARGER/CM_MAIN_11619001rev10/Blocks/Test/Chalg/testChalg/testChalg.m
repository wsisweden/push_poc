function testChalg()
  printf("Compilation of charging algorithm test bench started\n\n");

  if(system("mingw32-make --no-print-directory -C obj"))
    pause
    return
  endif
  
  printf("\nSimulation of charging started by calling \"generated/OutputPlot()\"\n");
  
  cd generated
  OutputPlot() 
  
  printf("Simulation done press enter to exit\n")
  pause
endfunction
