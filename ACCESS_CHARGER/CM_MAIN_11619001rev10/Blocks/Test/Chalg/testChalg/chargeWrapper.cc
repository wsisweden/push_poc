#include <octave/oct.h>
#include <inttypes.h>
#include <stdio.h>

extern "C"
{
#include "charge.h"
int OutputPlot_GetSamplesLen();
}

FILE *fp;

DEFUN_DLD(chargeWrapper, args, nargout, "chargeWrapper"){
  int nargin = args.length ();
  octave_value_list retval;
  
  fp = fopen("../output/debug.txt", "w");
  if(fp == NULL)
	printf("Could not open debug output file\n");
  fprintf(fp, "Used to dump debug information\n");

  /* Check number of input arguments */
  if (nargin != 0){
    print_usage ("outdata = chargeWrapper()");
    return retval;
  }

  if (!error_state){    

    chargeInit();

    charge();

    int SamplesLen = OutputPlot_GetSamplesLen();
    dim_vector size(SamplesLen, 1);
    NDArray values(size);

    int n;
    for(n = 0; n < charge_GetValuesSize(); n++){
		double* result = OutputPlot_GetVariable(n);
    	int k;
    	for(k = 0; k < SamplesLen-1; k++){
    		values(k) = (double)result[k];
    	}
    	retval.append(values);
    }

  }

  if(fp != NULL)
	  fclose(fp);

  return retval;
}
