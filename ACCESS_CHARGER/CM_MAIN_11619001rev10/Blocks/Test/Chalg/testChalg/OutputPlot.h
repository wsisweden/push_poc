#ifndef FILE_OUTPUT_PLOT_H
#define FILE_OUTPUT_PLOT_H

#include "cai_constparam.h"
#include "caimain.h"

void OutputPlotSaveData(const CaiInput_type *CaiInput_p, const CaiOutput_type *CaiOutput_p);

int OutputPlot_GetSamplesLen();
const AlgDef_ConstPar_Type* OutputPlot_GetChalg();

#endif
