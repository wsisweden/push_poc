#include "regulator.h"

void regulatorInit(struct regulator_type* This){
  This->limit = 0;

  This->regulator.KpOld = 0;
  This->regulator.Isum = 0;
  This->regulator.min = 0;
  This->regulator.max = (int32_t)(150*65536);
  
  This->Limits[regulator_limit_U].Kp = (uint32_t)(0.05*65536);
  This->Limits[regulator_limit_U].Ki = (uint32_t)(1*0.01*65536);
  This->Limits[regulator_limit_U].Choose = 65536*5;

  This->Limits[regulator_limit_I].Kp = (uint32_t)(0.05*65536);
  This->Limits[regulator_limit_I].Ki = (uint32_t)(2.5*0.01*65536);
  This->Limits[regulator_limit_I].Choose = 65536*1;

  This->Limits[regulator_limit_P].Kp = (uint32_t)(0.08*65536);
  This->Limits[regulator_limit_P].Ki = (uint32_t)(8*0.01*65536);
  This->Limits[regulator_limit_P].Choose = 65536*1;

  This->Limits[regulator_limit_T].Kp = (uint32_t)(100.0*65536);
  This->Limits[regulator_limit_T].Ki = (uint32_t)(0.3*0.01*65536);
  This->Limits[regulator_limit_T].Choose = 65536*400;
}

int32_t regulator(struct regulator_type* This, const struct regulatorIn_type* in){
  int64_t minValue = INT64_MAX;
  int min = 0;

  int n;
  for(n = 0; n < 3; n++){
    int64_t e = (int64_t)This->Limits[n].Choose*(in->values[n].ref - in->values[n].actual);
    if(e < minValue){
      minValue = e;
      min = n;
    }
  }
  
  This->limit = min;

  return PI(&This->regulator, This->Limits[min].Kp, This->Limits[min].Ki, in->values[min].ref - in->values[min].actual);
}
