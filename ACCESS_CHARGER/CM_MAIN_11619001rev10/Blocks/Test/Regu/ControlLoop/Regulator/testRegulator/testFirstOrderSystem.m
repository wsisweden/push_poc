function testFirstOrderSystem()
  if(system("make --no-print-directory -C obj"))
    error("")
    return
  endif
  
  cd generated
  
  % Set reference signal
  r.u(1:500) = 28.8;
  r.u(501:800) = 28.8;

  r.i(601:800) = 60;
  r.i(401:600) = 60;
  r.i(201:400) = 60;
  r.i(101:200) = 60;
  r.i(1:200) = 60;

  r.P(1:800) = 4200;
  r.T(1:800) = 100;
  
  enumUsedRegulator.names = {"U", "I", "P", "T"};
  enumUsedRegulator.n = [0 1 2 3];

  [voltage current power temperature usedGain] = systemWrapper(r.u, r.i, r.P, r.T);

  t = linspace(1, length(current), length(current));
  
  xZoom = [1 length(current)];

  figure
  plot(t, r.u, t, voltage)
  a = axis;
  a(1:2) = xZoom;
  axis(a)
  legend("u_r", "u")
  xlabel("t")
  ylabel("amplitude")
  title("Test of first order system with limited input signal and PI-regulator")

  figure
  plot(t, r.i, t, current)
  title("Currents in (A)")
  a = axis;
  a(1:2) = xZoom;
  axis(a)
  legend("i_r", "i")

  figure
  plot(t, r.P, t, power)
  title("Power in (W)")
  a = axis;
  a(1:2) = xZoom;
  axis(a)
  legend("P_r", "P")

  figure
  plot(t, r.T, t, temperature)
  title("Temperature in (degreee Celsius)")
  a = axis;
  a(1:2) = xZoom;
  axis(a)
  legend("i_r", "i", "used_u")

  figure
  title("Used regulator")
  h = plot(t, usedGain);
  ax = get(h, "parent");
  set(ax, "ytick", enumUsedRegulator.n)
  set(ax, "YTickLabel", enumUsedRegulator.names)
  a = axis(ax);
  a(3:4) = [-0.5 3.5];
  a(1:2) = xZoom;
  axis(ax, a)
  legend("used")
endfunction
