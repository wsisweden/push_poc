#include <octave/oct.h>
#include <octave/ov-struct.h>
#include <octave/parse.h>
#include <inttypes.h>
#include <stdio.h>

extern "C"
{
#include "regulator.h"
#include "firstOrderSystem.h"
}

FILE* fp;

DEFUN_DLD(systemWrapper, args, nargout, "Test"){
  int nargin = args.length();
  octave_value_list retval;

  fp = fopen("../output/debug.out", "w");
  if(!fp)
    printf("Error could not open debug.out\n");

  /* Check number of input arguments */
  if (nargin != 4){
    print_usage ("[regulator_type u] = regulator(regulator_type, regulatorIn_type)");
    return retval;
  }

  NDArray Uref = args(0).array_value();
  NDArray Iref = args(1).array_value();
  NDArray Pref = args(2).array_value();
  NDArray Tref = args(3).array_value();

  if (!error_state){    
    // input matrix x is 3D. Define const so we don't make a copy later
    dim_vector dv = Iref.dims();
    octave_idx_type len = Iref.length();
    
    NDArray u(dv);
    NDArray y(dv);
    NDArray power(dv);
    NDArray T(dv);
    NDArray usedGain(dv);

    struct regulator_type reg;
    struct regulatorIn_type in;

    //in.U
    //in.I
    //in.P
    in.values[regulator_limit_T].actual = 300;//T(0);

    regulatorInit(&reg);


    const struct firstOrderSystem_type batteryVoltage = {0.5, 0.1, 0.01};
    int n = 0;
    u(0) = 0.0;
    y(0) = 0.0;
    for(n = 0; n < len-1; n++){
      in.values[regulator_limit_U].ref = (int32_t)(Uref(n)*65536);
      in.values[regulator_limit_I].ref = (int32_t)(Iref(n)*65536);
      in.values[regulator_limit_P].ref = (int32_t)(Pref(n)*65536);
      in.values[regulator_limit_T].ref = (int32_t)(Tref(n)*65536);

      in.values[regulator_limit_U].actual = (int32_t)(y(n)*65536);
      in.values[regulator_limit_I].actual = (int32_t)(u(n)*65536);
      in.values[regulator_limit_P].actual = (int32_t)(y(n)*u(n)*65536);
      in.values[regulator_limit_T].actual = (int32_t)25*65536;

      power(n) = y(n)*u(n);
      T(n) = 25;

      u(n+1) = (double)regulator(&reg, &in)/65536;
      y(n+1) = firstOrderSystem(&batteryVoltage, y(n), (float)u(n+1));
      usedGain(n+1) = reg.limit;
    }
    
    retval.append(octave_value(y));
    retval.append(octave_value(u));
    retval.append(octave_value(power));
    retval.append(octave_value(T));
    retval.append(octave_value(usedGain));
  }
  
  if(fp)
    fclose(fp);

  return retval;
}
