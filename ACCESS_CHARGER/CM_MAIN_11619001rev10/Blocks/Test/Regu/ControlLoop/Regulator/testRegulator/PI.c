/*
 * PI.c
 *
 *  Created on: May 14, 2010
 *      Author: nicka
 */
#include "pi.h"

int32_t PI(struct PI_type* regulator, const int32_t Kp, const int32_t Ki, const int32_t e){
	/* Länk till blockdiagram nedan:
	 * http://www.20sim.com/webhelp/library/signal/control/pid_control/antiwindup.htm
	 *
	 * Diskret överföringsfunktion integrator Fd(z) = Y(z)/U(z) = hz^(-1)/(1 - z^(-1)) <=>
	 * Y(z) = Y(z)z^(-1) + hU(z)z^(-1)
	 *
	 * Konstanter:
	 *   Kp   Proportionell förstärkning
	 *   Ki   Integral förstärkning
	 *   e    Differensen mellan börvärde och ärvärde
	 *   u'   Obegränsad utsignal
	 *   u    Begränsad utsignal
	 * Antiwindup parallel form kontinuerlig u' = Kp*e + Ki/s*(e + u - u')
	 * Diskret u' = Kp*e + z^(-1)(u_prim - Kp*e + Ki*h(e + u - u_prim))
	 *
	 */


 	int64_t u; // limited output signal
	int64_t Kpe; // Kp at least 16 bits + e at least 17 bits = at least more than 33 bits
	int64_t u_prim; // unlimited output signa at least 33 bits

	Kpe = (int64_t)Kp*e;
	u_prim = Kpe;
	u_prim += regulator->KpOld;
	u_prim += regulator->Isum;
	//u_prim += (int64_t)Ki*regulator->Isum >> 16;
  
	/* Saturate output */
	if((u_prim >> 16) < regulator->min)
	    u = (int64_t)regulator->min << 16;
	else if((u_prim >> 16) > regulator->max)
		u = (int64_t)regulator->max << 16;
	else
	u = u_prim;

	regulator->KpOld = u_prim - Kpe;
	regulator->Isum = (int64_t)Ki*e + ((int64_t)Ki*(u - u_prim) >> 16); // antiwindup
	//regulator->Isum = e + u - u_prim; // antiwindup

	u = u >> 16;

	return u;
}
