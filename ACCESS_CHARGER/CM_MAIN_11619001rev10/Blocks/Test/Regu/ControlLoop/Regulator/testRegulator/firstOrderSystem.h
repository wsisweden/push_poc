#ifndef FIRST_ORDER_SYSTEM_H
#define FIRST_ORDER_SYSTEM_H

/*Kontinuerlig överföringsfunktion G(s) = Y(s)/U(s) = K/(1 + ST)
 * Stigtid                   t_r = 2.20T
 * Insvängningstid           t_5% = 3.00T
 * Ekvivalent tidskonstant   T_63% = T
 * Bandbredd                 omega_b = 1/T
 *
 * Discrete transfer function Gd(z) = Y(z)/U(z) = K*(1 - e^(-ah))z^(-1)/(1 - e^(-ah)z^(-1)) <=>
 * 1 - e^(-ah)z^(-1)Y(z) = a/K*(1 - e^(-ah))z^(-1)U(z) <=>
 * Y(z) = e^(-ah)Y(z)z^(-1) + K*(1 - e^(-ah))z^(-1)U(z)
 */

struct firstOrderSystem_type{
  double K;
  double T;
  double h;
};

double firstOrderSystem(const struct firstOrderSystem_type* This, double y_m1, double u_m1);

#endif
