@echo off

path = C:\Octave\Octave-4.4.1\x86_64-w64-mingw32\bin\;C:\Octave\Octave-4.4.1\bin;%path%

echo Compilation and linking of regulator and testbench started running make in "testRegulator/obj" directory:
echo:
make --no-print-directory -C %~dp0/obj
if ERRORLEVEL 1 goto CompilationError

octave --q --eval "printf(\"\nSimulation started in "testRegulator" directory by Octave:\n\"), cd %~dp0, testFirstOrderSystem(), printf(\"\nSimulation done press enter to exit\n\"), pause"
if ERRORLEVEL 1 goto OctaveRunError
goto End

:CompilationError
echo:
echo If compilation did not start and command could not be found check path of Octave in the file "testRegu.bat"
echo:
pause
goto end

:OctaveRunError
echo:
echo Note "panic: Segmentation violation" is usually because data is read or written outside allocated region by program run by Octave
echo Check also for compilation warnings 
echo:
pause
goto end

:End
@echo on