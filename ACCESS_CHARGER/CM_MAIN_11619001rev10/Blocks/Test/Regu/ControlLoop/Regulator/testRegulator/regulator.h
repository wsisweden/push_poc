#ifndef REGULATOR_H
#define REGULATOR_H

#include <inttypes.h>
#include "pi.h"

enum regulator_limits_enum{regulator_limit_U, regulator_limit_I, regulator_limit_P, regulator_limit_T, regulator_limits_size};

struct regulator_type{
  enum regulator_limits_enum limit;
  struct PI_type regulator;

  struct{
    uint32_t Kp;
    uint32_t Ki;
    uint32_t Choose;
  } Limits[regulator_limits_size];
};

struct regulatorIn_type{
  struct{
    int32_t ref;
    int32_t actual;}
  values[regulator_limits_size];
};

/* Initialize */
void regulatorInit(struct regulator_type* This);

int32_t regulator(struct regulator_type* This, const struct regulatorIn_type* in);

#endif
