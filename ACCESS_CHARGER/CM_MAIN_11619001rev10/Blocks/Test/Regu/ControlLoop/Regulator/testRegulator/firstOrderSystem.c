#include "firstOrderSystem.h"
#include "math.h"

double firstOrderSystem(const struct firstOrderSystem_type* This, double y_m1, double u_m1){
  double a = 1/This->T;

  /* y = exp(-a*h)*y_m1 + K*(1 - exp(-a*h))*u_m1; */
  double exp_ah = exp(-a*This->h/50);
  double y = y_m1;
  int n;
  
  for(n = 0; n < 50; n++){
    y = exp_ah*y + This->K*(1 - exp_ah)*u_m1;
  }
  
  return y;
  //return exp_ah*y_m1 + This->K*(1 - exp_ah)*u_m1;
}
