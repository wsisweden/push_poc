/*
 * PI.h
 *
 *  Created on: May 14, 2010
 *      Author: nicka
 *
 *  Implement:
 *  	A standard discrete PI-regulator with antiwindup. It is implemented on something called parallel form.
 */

#ifndef PI_H_
#define PI_H_

#include <inttypes.h>

struct PI_type{
  int64_t KpOld;
  int64_t Isum;
  int32_t min;
  int32_t max;
};

/* This is an implementation of an ordinary discrete PI-regulator with antiwindup. IT Should be called with a
 * pointer to a struct defining the parameters and the error. It return the output. The reason to include the
 * gain parameters is that the parameters depend on error used. */
int32_t PI(struct PI_type* regulator, const int32_t Kp, const int32_t Ki, const int32_t e);
#endif /* PI_H_ */

