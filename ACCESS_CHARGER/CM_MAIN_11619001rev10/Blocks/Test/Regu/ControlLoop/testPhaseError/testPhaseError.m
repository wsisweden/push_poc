function testPhaseError()
  printf("Compilation of phase error detection test bench started\n\n");

  if(system("make --no-print-directory -C obj"))
    pause
    return
  endif
  
  printf("\nSimulation of phase error detection started\n");
  cd generated
  
  len = 1000;
  dt = 1e-3;
  f = 50;
  t = linspace(dt, len*dt, len)';

  i = 2000*[abs(sin(t(1:len)*2*pi*f));abs(sin(3*t(1:len)*2*pi*f));abs(sin(t(1:len)*2*pi*f))];
  t = linspace(dt, 3*len*dt, 3*len)';

  Perr = phaseErrorWrapper(i);

  figure
  plot(t, i);
  title("Test of phase error")
  xlabel("t in (s)")
  ylabel("i in (bit)")

  h = figure;
  plot(t, Perr);
  set(get(h, "currentaxes"), "ylim", [-0.5 1.5])
  title("Test of phase error")
  xlabel("t in (s)")
  ylabel("Error")

  printf("Simulation done press enter to exit\n")
  pause
endfunction
