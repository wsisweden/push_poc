#include <octave/oct.h>
#include <octave/ov-struct.h>
#include <octave/parse.h>
#include <inttypes.h>
#include <stdio.h>

extern "C"
{
#include "PhaseError.h"
}

FILE* fp;

DEFUN_DLD(phaseErrorWrapper, args, nargout, "phaseErrorWrapper"){
  int nargin = args.length();
  octave_value_list retval;

  fp = fopen("../output/debug.out", "w");
  if(!fp)
    printf("Error could not open debug.out\n");

  /* Check number of input arguments */
  if (nargin != 1){
    print_usage ("ChargerError_type = CalculateErrorWrapper(i)");
    return retval;
  }

  NDArray i = args(0).array_value();

  if (!error_state){    
    // input matrix x is 3D. Define const so we don't make a copy later
    dim_vector dv = i.dims();
    octave_idx_type len = i.length();

    NDArray Perr(dv);

	int16_t samples[10];
	int k = 0;
	int oldError = 0;

    int n = 0;
    for(n = 0; n < len; n++){

    	samples[k] = (int16_t)i(n);

    	if(k < 9){
    		k++;
    	}
    	else{
        	oldError = regu_DetectMissingPhase(samples, k + 1);
    		k = 0;
    	}

    	Perr(n) = (double)oldError;
    }
    
    retval.append(octave_value(Perr));

  }
  
  if(fp)
    fclose(fp);

  return retval;
}
