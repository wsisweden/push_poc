function CheckMacros_EXE_USE(macros, name)
																										% Find all storage types
	StorageTypes = {};
	for n = 1:length(macros.EXE_REG)																	% For all EXE_REG macros
		StorageType = macros.EXE_REG{n}(9:10);															% Read storage type for this variable
		found = 0;
		for k = 1:length(StorageTypes)																	% For all known storage types in this init.h file
			if(strcmp(StorageType, StorageTypes{k}))													% Storage type already known in this init.h file ?
				found = 1;
				break
			endif
		endfor
		if(not(found))																					% Storage not known in this init.h file ?
			StorageTypes{length(StorageTypes) + 1} = StorageType;										% Add storage type to list of known storage types for this init.h file
		endif
	endfor

																										% Check storage type
	if(length(macros.EXE_USE) != length(StorageTypes))													% Do the number of storage types agree ?
		error(["The number of storages types in " name " do not agree with the EXE_REG macros"])
	endif
	
	for n = 1:length(StorageTypes)																		% For each known storage type
		StorageOk = 0;
		for k = 1:length(macros.EXE_USE)																% For each declarared storage type
			if(strcmp(StorageTypes{n}, macros.EXE_USE{k}(9:10)))										% The needed storage type declared
				StorageOk = 1;
			endif
		endfor
		if(not(StorageOk))																				% Storage not declared ?
			error(["Storage type " StorageTypes{n} " not declared in " name])
		endif
	endfor
endfunction
