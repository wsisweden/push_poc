function CheckRegister()
	InitFiles = InitFilesFind("../Src");							% Find all init files
	
	for n = 1:length(InitFiles)										% For all Init.h files
		macros{n}.macros = GetMacros(InitFiles(n).file);			% Read macros from files
	endfor

	disp("Checking EXE_REG_ and EXE_MMI macros. For every EXE_REG_ macro there should be exactly one EXE_MMI_ macro. Sort should not matter but the script check if they are sorted in the same order.")
	disp("\nChecking Init.h files:")
	for n = 1:length(InitFiles)										% For Init.h files
		disp(InitFiles(n).name)										% Display information
		CheckMacros_EXE_REG_to_EXE_MMI(macros{n}.macros)			% Check macros
	endfor

	disp("\nCheck EXE_IND_ME macros for all found Init.h files:")
	warning("Some problems are still missed in one of the functions")
	for n = 1:length(InitFiles)										% For Init.h files
		disp(InitFiles(n).name)										% Display information
		CheckMacros_EXE_MMI_sorting(macros{n}.macros, tolower(InitFiles(n).name))	% Check macros
	endfor

	disp("\nCheck EXE_USE macros for all found Init.h files:")
	warning("Some problems are still missed in one of the functions")
	for n = 1:length(InitFiles)										% For Init.h files
		disp(InitFiles(n).name)										% Display information
		CheckMacros_EXE_USE(macros{n}.macros, tolower(InitFiles(n).name))	% Check macros
	endfor

	CheckFlashTag(macros)
	
endfunction
