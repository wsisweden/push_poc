################################################################################
#
#	Makefile for NXP LPC23xx
#
#	This makefile is used when compiling the blocks folder. Blocks library 
# 	specific overrides can be made here.
#
################################################################################

! INCLUDE $(TLLIB_L)\mak\SRC_$(TLLIB_T).MAK

clean : platclean
# 	Add library specific cleanup commands here
