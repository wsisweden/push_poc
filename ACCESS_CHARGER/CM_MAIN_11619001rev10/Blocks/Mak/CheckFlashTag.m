function CheckFlashTag(InitFiles)
	NNpos = 1;
	NTpos = 1;

	for n = 1:length(InitFiles)												% For all Init.h files
		%disp(InitFiles{n}.macros.EXE_REG)
		for k = 1:length(InitFiles{n}.macros.EXE_REG)						% For all macros
			if(strncmp(InitFiles{n}.macros.EXE_REG{k}, "EXE_REG_NN", 10))
				NN{NNpos} = InitFiles{n}.macros.EXE_REG{k};					% Display information
				NNpos = NNpos + 1;
			endif
			if(strncmp(InitFiles{n}.macros.EXE_REG{k}, "EXE_REG_NT", 10))
				NT{NTpos} = InitFiles{n}.macros.EXE_REG{k};					% Display information
				NTpos = NTpos + 1;
			endif
		endfor
	endfor
	
	FlashTags = GetFlashtagEntries("../Exe/MPAccess/flashtag.h")
	
	return
	[FID MSG] = fopen(FlashTags, "r");
	if(FID == -1)
		error(MSG)
	endif

	Found_offsetTagMap = 0;
	textLine = fgets(FID);
	while textLine != -1
		textLine = strtrim(textLine);
	
		if(Found_offsetTagMap)
			printf("Var\n");
		elif(strcmp(textLine, "PUBLIC const_P flmem_TagOffset flmem_offsetTagMap[] = {"))
			printf("Found var\n");
			Found_offsetTagMap = 1;
		else
			printf("No var\n");
		endif

		textLine = fgets(FID);
	endwhile

	fclose(FID);

	PrintNN(NN, NT)	
endfunction

function PrintNN(NN, NT)
	[FID MSG] = fopen("EXE_REG_NN", "w");
	if(FID == -1)
		error(MSG)
	endif
	
	for n = 1:length(NN)
		fprintf(FID, "%s\n", NN{n})
	endfor

	fprintf(FID, "\n")
	
	for n = 1:length(NT)
		fprintf(FID, "%s\n", NT{n})
	endfor
	
	fclose(FID);
endfunction
