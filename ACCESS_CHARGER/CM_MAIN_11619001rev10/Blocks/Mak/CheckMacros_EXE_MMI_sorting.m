function CheckMacros_EXE_MMI_sorting(macros, name)
	EXE_IND_len = length(macros.EXE_IND);																% Total number of macros
	oldInstance = [];																					% Set old instance to no instance

																										% Check instance sorting
	for n = 1:EXE_IND_len																				% For all EXE_IND_ME macros
		[Instance err] = GetParam(macros.EXE_IND{n}, 3);												% Get instance
		if(err)																							% No instance ?
			Instance = name;																			% Set to this Instance
		endif
		
		for k = 1:min(length(oldInstance), length(Instance))											% For all characters
			if(Instance(k) > oldInstance(k))															% Character for new instance larger than instance before ?
				break																					% Continue with next macro
			endif
		
			if(Instance(k) < oldInstance(k))															% Character for new instance smaller than instance before ?
				error(["Instance name sorting error for EXE_MMI_ macro in " name " variable " GetParam(macros.EXE_IND{n}, 1)])
			endif
		endfor

		oldInstance = Instance;
	endfor
																										% Check that order of EXE_IND macros are the same as EXE_REG macros
	VariablePos = 0;
	for n = 1:EXE_IND_len																				% For all EXE_IND macros
		[Variable err] = GetParam(macros.EXE_IND{n}, 1);												% Get macro EXE_IND variable
		if(err)
			error(["No variable " variable " in EXE_REG macro in " name])
		endif
																										% Search for position of macro EXE_REG for variable 
		for k = 1:length(macros.EXE_REG)																% For all EXE_REG macros
			if(strcmp(GetParam(macros.EXE_REG{k}, 1), Variable))										% Variable found ?
				if(k <= VariablePos)																	% This position smaller or equal ?
					error(["Variable sorting error for EXE_MMI macro in " name " variable " Variable])
				endif
				VariablePos = k;
			endif
		endfor
	endfor
endfunction
