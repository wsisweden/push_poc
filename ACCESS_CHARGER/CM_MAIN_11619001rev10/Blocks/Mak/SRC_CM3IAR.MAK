################################################################################
#
#	This makefile is used when compiling the blocks folder. Blocks library 
# 	specific overrides can be made here.
#
#	Target:		CM3 (LPC17xx)
#	Compiler:	IAR
#	Make:		Microsoft NMAKE
#
################################################################################

! INCLUDE $(TLLIB_L)\mak\SRC_$(TLLIB_T).MAK

clean : platclean
# 	Add library specific cleanup commands here

