function FlashTags = GetFlashtagEntries(filename)
	[FID MSG] = fopen(filename, "r");
	if(FID == -1)
		error([MSG ": " filename]);
	endif
	
	FlashTagsPos = 1;
	
	textLine = fgets(FID);
	while textLine != -1
		commentPos = strfind(textLine, "//");
		offsetofPos = strfind(textLine, "offsetof");
		if(!length(offsetofPos))
			textLine = fgets(FID);
			continue
		elseif(commentPos > offsetofPos)
			textLine = fgets(FID);
			continue
		else
			FlashTags{FlashTagsPos} = textLine(offsetofPos:end);
			FlashTagsPos = FlashTagsPos + 1;
			textLine = fgets(FID);
		endif

	endwhile

	warning("Function no done")
%	for n = 1:length(FlashTags)
%		exe__nnPos = strfind(textLine, "offsetof");
%		FlashTags{n}	
%		GetParam(FlashTags{n}, 2);
%	endfor

	fclose(FID);

endfunction
