function macros = GetMacros(filename)
	[FID MSG] = fopen(filename, "r");
	if(FID == -1)
		error([MSG ": " filename]);
	endif

	macros.EXE_REG = {};
	macros.EXE_MMI = {};
	macros.EXE_IND = {};
	macros.EXE_USE = {};
	
	EXE_REG_pos = 0;
	EXE_MMI_pos = 0;
	EXE_IND_pos = 0;
	EXE_USE_pos = 0;
	
	textLine = fgets(FID);
	while textLine != -1
		textLine = strtrim(textLine);
	
		if(strncmp(textLine, "EXE_REG_", 8) && !strncmp(textLine, "EXE_REG_EX", 10))			% EXE_REG_ macro ?
			EXE_REG_pos = EXE_REG_pos + 1;
			macros.EXE_REG{EXE_REG_pos} = textLine;
		endif

		if(strncmp(textLine, "EXE_MMI_", 8))													% EXE_MMI_ macro ?
			EXE_MMI_pos = EXE_MMI_pos + 1;
			macros.EXE_MMI{EXE_MMI_pos} = textLine;
		endif
		
		if(strncmp(textLine, "EXE_IND_", 8))													% EXE_IND_ macro ?
			EXE_IND_pos = EXE_IND_pos + 1;
			macros.EXE_IND{EXE_IND_pos} = textLine;
		endif
		
		if(strncmp(textLine, "#define", 7))														% #define ?
			index = strfind(textLine, "EXE_USE_");
			if(index)
				EXE_USE_pos = EXE_USE_pos + 1;
				macros.EXE_USE{EXE_USE_pos} = textLine(index:index + 9);
			endif
		endif
		
		textLine = fgets(FID);
	endwhile

	fclose(FID);
endfunction
