function CheckMacros_EXE_REG_to_EXE_MMI(macros)
	EXE_REG_len = length(macros.EXE_REG);
	EXE_MMI_len = length(macros.EXE_MMI);
	
	if(EXE_REG_len != EXE_MMI_len)
		for n = 1:EXE_REG_len
			disp([num2str(n) " " macros.EXE_REG{n}])
		endfor
		disp("")
		for n = 1:EXE_MMI_len
			disp([num2str(n) " " macros.EXE_MMI{n}])
		endfor

		error("Number of EXER_REG_ macros different from number of EXE_MMI_ macros")
	endif
	
	if 0																								% enable disable print during check
		for n = 1:EXE_REG_len
			disp(["""" GetParam(macros.EXE_REG{n}, 1) """ == """ GetParam(macros.EXE_MMI{n}, 1) """"])
			if(!strcmp(GetParam(macros.EXE_REG{n}, 1), GetParam(macros.EXE_MMI{n}, 1)))
				error("EXE_REG_ do not agree with EXE_MMI")
			endif
		endfor
	endif
endfunction
