function InitFiles = InitFilesFind(FunctionBlocksPath)
	FunctionBlocksDirs = dir(FunctionBlocksPath);
	k = 1;
	for n = 1:length(FunctionBlocksDirs)
		filename = [FunctionBlocksPath "/" FunctionBlocksDirs(n).name "/Init.h"];
		if(length(glob(filename)))
			InitFiles(k).name = FunctionBlocksDirs(n).name;
			InitFiles(k).file = filename;
			k = k + 1;
		endif
	endfor
endfunction
