function [Param err] = GetParam(str, arg)
	Param = [];
	err = 1;
	
	indexComment = strfind(str," //");								% Search for comment
	if(length(indexComment))
		str = str(1:indexComment-1);								% Remove comment
	endif

	indexParamStart = strfind(str, "(");							% Start of parameters "("

	indexParamSeparator = strfind(str, ",");						% Parameter separator ","
	indexParamEnd = strfind(str, ")");								% End of parameters ")"
	
	if(length(indexParamStart) != 1)								% Exactly one start of parameters
		return
	endif
	if(length(indexParamStart) != 1)								% Exactly one end of parameters
		return
	endif
	if(indexParamStart > indexParamEnd)								% Start of parameters before end of parameters
		return
	endif
	for n = 1:length(indexParamSeparator)							% For all parameter separators
		if(indexParamSeparator(n) < indexParamStart)				% Before start ?
			return
		endif
		if(indexParamSeparator(n) > indexParamEnd)					% After end
			return
		endif
	endfor
	
	% There should now be exactly one start and one end and
	% the start should come before the end and if there are
	% any parameters they should be between start and end
	
	indexParamSeparator = [indexParamStart indexParamSeparator indexParamEnd];		% Now all indexes for parameters should be in one list
	if(arg + 1 > length(indexParamSeparator))						% Check if parameter number n available
		return
	endif

	err = 0;														% Now there should be no error

	Param = strtrim(str(indexParamSeparator(arg) + 1 : indexParamSeparator(arg + 1) - 1));
endfunction
