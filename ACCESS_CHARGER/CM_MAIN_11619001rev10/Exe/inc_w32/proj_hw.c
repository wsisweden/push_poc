/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[X]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_w32/proj_hw.c
*
*	\ingroup	HWSIMU
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Ari suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <windows.h>
#include <assert.h>

#include "tools.h"
#include "HwSimu.h"
#include "protif.h"
#include "iicmstr.h"

#include "global.h"
#include "Meas.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void main(void);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_runMain
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Used to start the program from the hardware simulation.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project_runMain(
	void
) {
	main();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_spiSimu
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Simulated SPI bus slaves.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project_spiSimu(
	Uint8					busNum,
	protif_Request *		pRequest
) {
	if (busNum == 1)
	{

	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_iicmstrSimu
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Simulated i2c bus slaves.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC Uint8 project_iicmstrSimu(
	protif_Request *		pReq,
	Uint8					busNum
) {
	Uint8					retCode;

	if (
		busNum == 0 && 
		((iicmstr_SlaveInfo *) pReq->pTargetInfo)->address == 0x4C
	) {
		/*
		 * This request is for the tilt sensor.
		 */

		retCode = meas__tiltSimuProc(pReq);
	}

	return(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__enterSleepMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enters sleep mode.
*
*	\return		-
*
*	\details	The program will return from this function when the device
*				wakes up. The maximum sleep time is 1s.
*
*	\note
*
*******************************************************************************/

PUBLIC void project__enterSleepMode()
{
	//Sleep(1000);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__disablePLL
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Disable PLL
*
*	\return		-
*
*	\details	This will do nothing in windows.
*
*	\note
*
*******************************************************************************/

PUBLIC void project__disablePLL()
{

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__configurePLL
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure PLL
*
*	\return		-
*
*	\details	This will do nothing in windows.
*
*	\note
*
*******************************************************************************/

PUBLIC void project__configurePLL()
{

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__startSystemTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start system tick timer.
*
*	\return		-
*
*	\details	This will do nothing in windows.
*
*	\note
*
*******************************************************************************/

PUBLIC void project__startSystemTick()
{

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__stopSystemTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Stop system tick timer.
*
*	\return		-
*
*	\details	This will do nothing in windows.
*
*	\note
*
*******************************************************************************/

PUBLIC void project__stopSystemTick()
{

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__errorBlinker
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Signals program error to the user.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__errorBlinker(
	Uint8					nCycle
) {
	assert(FALSE);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_assert
*
*--------------------------------------------------------------------------*//**
*
*	\brief		project__displayDataCommand
*
*	\param		command		
*
*	\return		-
*
*	\details	Sets command / data selection of the display.
*
*	\note
*
*******************************************************************************/

PUBLIC void project__displayDataCommand(
	Boolean					command
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__displayBacklight
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets display backlight on /off.
*
*	\param		on		If TRUE backlight will be set on else off.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void project__displayBacklight(
	Boolean					on
) {
	hwsimu_setParameter(HWSIMU_P_BACKLIGHT, on ? 1 : 0);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_log
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Output debug message character.
*
*	\param		character	The character to output.
*
*	\return		-
*
*	\details	This function is called by deb_log(). It can be configured to
*				write the character to UART.
*
*	\note
*
*******************************************************************************/

PUBLIC void project_log(
	Uint16					character
) {
	hwsimu_debugLog(character);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_setWpState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets or clears flash memory write protection pin.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__setWpState(
	Boolean					wpHigh
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_checkBootButtons
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check if boot action buttons are pressed during boot.
*
*	\return		-
*
*	\details	This function should be called before OSA is started. It checks
*				if a boot action button is pressed during boot.
*
*				One boot action could be that settings should be reset to 
*				factory defaults.
*
*	\note
*
*******************************************************************************/

PUBLIC void project_checkBootButtons(
	void
) {

}
