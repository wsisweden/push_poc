/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		HwSimu.c
*
*	\ingroup	HWSIMU
*
*	\brief		Windows hardware simulation DLL interface functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	HWSIMU		HWSIMU
*
*	\ingroup	MPACCESS
*
*	\brief		Hardware simulation in windows.
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "stdafx.h"

#include "tools.h"
#include "HwSimu.h"

#ifdef _MANAGED
#pragma managed(push, off)
#endif

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Type for callback function that reports HW parameter changes to C# code.
 */

typedef void	CSharpCbFn(int paramNum, int newValue);

/**
 * Type for callback function that sends debug log messages to C# code.
 */

typedef void	CSharpLogCbFn(int character);


typedef void	CSharpPixelCbFn(int x, int y, int on);

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*
 * Declaring functions extern here so that they do not have to be mentioned
 * in any header.
 */

extern void		project_runMain(void);
extern void		meas__setUAdValue(Uint16);
extern void		meas__setIAdValue(Uint16);
extern void		meas__setTbAdValue(Uint16);
extern void		meas__setThAdValue(Uint16);
extern void		meas__setMainsAdValue(Uint16);
extern void		meas__updateDigiStates(Uint16);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * This variable is used to stop all interface calls when the C# application
 * is closing. When this variable is set to 1 then no calls will be made to the
 * C# code.
 */

PUBLIC int					hwsimu__closing = 0;

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/** 
 * Pointer to function that will report a parameter change to the C# code.
 */

PRIVATE CSharpCbFn *		hwsimu__pCbFunc = NULL;

/** 
 * Pointer to function that will send a debug log character to the C# code.
 */

PRIVATE CSharpLogCbFn *		hwsimu__pLogCbFunc = NULL;

/** 
 * Pointer to function that will draw a pixel on the C# display simulation
 * bitmap.
 */

PRIVATE CSharpPixelCbFn *	hwsimu__pPixelCbFunc = NULL;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	DllMain
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Main function of the DLL.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

BOOL APIENTRY DllMain(
	HMODULE					hModule,
	DWORD					ul_reason_for_call,
	LPVOID					lpReserved
) {

	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}

    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	DoMain
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This will call the main function of the project.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

DLL_TEST_API void DoMain(
	void
) {
	project_runMain();
}

DLL_TEST_API void UScroll(int adValue)
{
	meas__setUAdValue(adValue);
}

DLL_TEST_API void IScroll(int adValue)
{
	meas__setIAdValue(adValue);
}

DLL_TEST_API void TbScroll(int adValue)
{
	meas__setTbAdValue(adValue);
}

DLL_TEST_API void ThScroll(int adValue)
{
	meas__setThAdValue(adValue);
}

DLL_TEST_API void MainsScroll(int adValue)
{
	meas__setMainsAdValue(adValue);
}

DLL_TEST_API void UpdateDigiStates(int states)
{
	meas__updateDigiStates(states);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	RegCallback
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Registers the parameter change callback so that the C code can
*				report parameter changes to C# code.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

DLL_TEST_API void RegCallback(
	CSharpCbFn *			pCbFunc
) {
	hwsimu__pCbFunc = pCbFunc;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	RegPixelCallback
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Registers the draw pixel function so that C code can draw pixels
*				on the C# simulated display.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

DLL_TEST_API void RegPixelCallback(
	CSharpPixelCbFn *		pCbFunc
) {
	hwsimu__pPixelCbFunc = pCbFunc;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	RegLogCallback
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Registers the log write function so that C code send debug log
*				messages to C# code.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

DLL_TEST_API void RegLogCallback(
	CSharpLogCbFn *			pCbFunc
) {
	hwsimu__pLogCbFunc = pCbFunc;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	Closing
*
*--------------------------------------------------------------------------*//**
*
*	\brief		After this function is called C code will no longer call C# 
*				functions.
*
*	\return		-
*
*	\details	This will be called from C# code when the application thread is
*				closing and can no longer receive function calls from C DLL
*				threads.
*
*	\note
*
*******************************************************************************/

DLL_TEST_API void Closing(
	void
) {
	hwsimu__closing = 1;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	hwsimu_setParameter
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Can be called from the C project to report parameter change to
*				the C# application.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void hwsimu_setParameter(
	int						paramNum,
	int						newValue
) {
	if (hwsimu__closing == 0)
	{
		hwsimu__pCbFunc(paramNum, newValue);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	hwsimu_debugLog
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Can be called from the C project to write debug log to the C#
*				application.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void hwsimu_debugLog(
	Uint16					character
) {
	if (hwsimu__closing == 0)
	{
		hwsimu__pLogCbFunc(character);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	hwsimu_setPixel
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Can be called from the C project to draw a pixel in the C#
*				application display simulation.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void hwsimu_setPixel(
	int						x,
	int						y,
	int						on
) {
	if (hwsimu__closing == 0)
	{
		hwsimu__pPixelCbFunc(x, y, on);
	}
}
