################################################################################
#
#	Override makefile
#
#	Overrides made in this file will override settings in all libraries of the
#	project.
#
#	Target:		S4F4 (ST STM32F4xx)
#	Compiler:	GCC
#	Make:		Microsoft NMAKE
#
################################################################################

#
#	C-flags override
#
#c_flags= 

#
#	Assembler flags override
#
#a_flags= 

#
#	Linker flags override
#
#lnk_flags= 

#
#	Optimization flags override
#
#debug_flags=


#
#	Linker library list override
#
#lnk_libs= 
