################################################################################
#
#	Override makefile
#
#	Overrides made in this file will override settings in all libraries of the
#	project.
#
#	Target:		CM3 (NXP LPC17xx)
#	Compiler:	GCC
#	Make:		Microsoft NMAKE
#
################################################################################

#
#	Compiler warning settings
#
WARNINGS   =-Wall -Wshadow -Wpointer-arith -Wbad-function-cast -Wcast-align -Wsign-compare \
 -Wunused

#
#	Defines for C compiler and assembler override
#
#comp_defines =
 
#
#	C-flags override
#
#c_flags= 

#
#	Assembler flags override
#
#a_flags= 

#
#	Linker flags override
#
#lnk_flags= 

#
#	Optimization flags override
#
!ifndef debug_flags
! if "$(TLLIB_B)" == "DEBUG"
debug_flags = -Os -fno-strict-aliasing
! elseif "$(TLLIB_B)" == "RELEASE"
debug_flags = -DNDEBUG -Os -fno-strict-aliasing
! endif
!endif

#
#	Linker library list override
#
#lnk_libs= 
