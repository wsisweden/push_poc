################################################################################
#
#	This makefile is used when compiling the EXE folder from the command line.
#	Project specific overrides can be made here.
#
#	Target:		SM4 (ST STM32F3xx)
#	Compiler:	IAR
#	Make:		Microsoft NMAKE
#
################################################################################

! INCLUDE $(TLLIB_L)\mak\SRC_$(TLLIB_T).MAK

clean : platclean
# Add project specific cleanup commands here
