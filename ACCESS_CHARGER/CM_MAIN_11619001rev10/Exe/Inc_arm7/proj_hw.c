/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[X]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_arm7/proj_hw.c
*
*	\ingroup	MPACCESS_ARM7
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Ari suomi
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MPACCESS_ARM7		ARM7
*
*	\ingroup	MPACCESS
*
*	\brief		ARM7 specific project code.
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"

#include "global.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 *	PLL settings.
 *	Fcck = 36Mhz, Fosc = 288Mhz, and USB 48Mhz 
 */

/*@{*/
#define PLL_MValue					11	/**< M Value - 1					*/
#define PLL_NValue					0	/**< N Value - 1					*/
#define CCLKDivValue				7	/**< Div Value						*/
#define USBCLKDivValue				6	/**< USB Div Value					*/
/*@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_configHW
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configures HW settings at startup
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__configHW(
	void
) {

#ifndef NDEBUG
	/*
	 * The program must wait a while for debugging to work.
	 */

	project__delay(0x00020000);

#endif

	/*
	* Enable fast IO on port0 and port1
	*/

	SCS |= 1;

	/*
	 *  Ethernet block power must be enabled if ethernet RAM is to be used
	 *	(as per errdata).
	 */

	PCONP |= (1<<30);

	/*	
	 *  Configure PLL
	 */
	project__configurePLL();

	/*	
	 *  Configure timer to to generate 1ms pulses, match register 1 used
	 */

	PCLKSEL0 = (PCLKSEL0 & (~(0x3<<2))) | (0x01 << 2);
	T0TCR  = 2;         /* Stop and reset the timer */
	T0CTCR = 0;         /* Timer mode               */

	/* Timer prescaler */
	T0PR = 0;

	/* Calc match value for 1ms */
	T0MR1 = (PROJECT_CPU_HZ / 1000) - 1;

	/* Reset timer on match and generate interrupt */
	T0MCR  = (3 << 3);  

	/* Setup the VIC for the timer. */
	VICIntEnable = 0x00000010;
	VICVectAddr4 = ( long ) project_swTimerIsr;
	VICVectCntl4 = 1;


	/*
	 * Memory Acceleration Module (MAM) settings. MAMTIM is set differently
	 * depending on the crystal frequency. Mam function is disabled while
	 * changeing MAMTIM. Mam is only enabled partially (Faster will not work).
	 */

	#if PROJECT_MAM_ENABLE == 1
	MAMCR = 0;

	#if PROJECT_CPU_HZ < 20000000
	MAMTIM = 1;
	#elif PROJECT_CPU_HZ < 40000000
	MAMTIM = 2;
	#else
	MAMTIM = 3;
	#endif

	MAMCR = 1;
	#endif

	/* Display backlight pin */
	PINSEL0 &= ~((1<<16)|(1<<17));
	FIO2DIR |= (1<<8);

	/* Display command / data pin */
	PINSEL4 &= ~((1<<13)|(1<<12));
	FIO2DIR |= (1<<6);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__enterSleepMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enter sleep mode.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__enterSleepMode()
{
	DISABLE;

	/* todo:	RTC must be configured to give a interrupt each second.
	 */

//	PCON = ((1<<0)|(1<<7));				/* Enter sleep mode					*/
	project__delay(0x00020000);

	ENABLE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__disablePLL
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Disconnect and disable PLL.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__disablePLL(
	void
) {
	/*
	* Disconnect PLL
	*/

	if (PLLSTAT & (1 << 25))
	{
		PLLCON = 1;
		PLLFEED = 0xaa;
		PLLFEED = 0x55;
	}

	/*
	* Turn PLL off
	*/

	PLLCON = 0;
	PLLFEED = 0xaa;
	PLLFEED = 0x55;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__configurePLL
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configures main clock
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__configurePLL(
	void
) {
	Uint32					MValue;
	Uint32					NValue;

	project__disablePLL();

	SCS |= 0x20;						/* Enable main OSC					*/
	while(!(SCS & 0x40));				/* Wait until main OSC is usable	*/

	CLKSRCSEL = 0x1;					/* Select main OSC, 12MHz, as the PLL 
										 * clock source						*/

	PLLCFG = PLL_MValue | (PLL_NValue << 16);
	PLLFEED = 0xaa;
	PLLFEED = 0x55;

	PLLCON = 1;							/* Enable PLL (still disconnected)	*/
	PLLFEED = 0xaa;
	PLLFEED = 0x55;

	CCLKCFG = CCLKDivValue;				/* Set clock divider				*/

	while (((PLLSTAT & (1 << 26)) == 0));	/* Check lock bit status		*/

	MValue = PLLSTAT & 0x00007FFF;
	NValue = (PLLSTAT & 0x00FF0000) >> 16;

	while ((MValue != PLL_MValue) && ( NValue != PLL_NValue) );

	PLLCON = 3;							/* Connect PLL						*/
	PLLFEED = 0xaa;
	PLLFEED = 0x55;

	while ( ((PLLSTAT & (1 << 25)) == 0) );	/* Check connect bit status		*/
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	Undef_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Undefined instruction handler
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void Undef_Handler(void)
{
	/*
	*	[ 1 ]
	*
	*	Undefined Instruction
	*/
#ifndef NDEBUG
	project__errorBlinker(1);
#else
	FOREVER
	{

	}
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	PAbt_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Prefetch Abort (instruction fetch memory fault)
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void PAbt_Handler(void)
{
	/*
	*	[ 2 ]
	*
	*	Prefetch Abort (instruction fetch memory fault)
	*/

#ifndef NDEBUG
	project__errorBlinker(2);
#else
	FOREVER
	{

	}
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	DAbt_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Data Abort (data access memory fault)
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void DAbt_Handler(void)
{
	/*
	*	[ 3 ]
	*
	*	Data Abort (data access memory fault)
	*/

#ifndef NDEBUG
	project__errorBlinker(3);
#else
	FOREVER
	{

	}
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	FIQ_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Fast Interrupt reQuest handler
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void FIQ_Handler(void)
{
#ifndef NDEBUG
	project__errorBlinker(4);
#else
	FOREVER
	{

	}
#endif
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__errorBlinker
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Blinks degug led to indicate error type.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__errorBlinker(
	Uint8					nCycle
) {
	Uint32					i;

	FIO1DIR |= (1<<21);			/* Define LED-Pin as output			*/

	FIO1DIR |= (1<<26);
	FIO1CLR = (1<<26);

	while (1)
	{
		FIO1CLR = (1<<21);
		project__delay(0x00120000);

		for (i = 0; i < nCycle; i++)
		{
			FIO1SET = (1<<21);
			project__delay(0x00040000);

			FIO1CLR = (1<<21);
			project__delay(0x00040000);
		}

		project__delay(0x00070000);
	}

}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__displayDataCommand
*
*--------------------------------------------------------------------------*//**
*
*	\brief		project__displayDataCommand
*
*	\param		command		
*
*	\return		-
*
*	\details	Sets command / data selection of the display.
*
*	\note
*
*******************************************************************************/
PUBLIC void project__displayDataCommand(
	Boolean					command
) {
	if (command)
	{
		FIO2CLR = (1<<6);
	}
	else
	{
		FIO2SET = (1<<6);
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__displayBacklight
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets display backlight on /off.
*
*	\param		on		If TRUE backlight will be set on else off.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void project__displayBacklight(
	Boolean					on
) {
	if (on)
	{
		FIO2SET = (1<<8);
	}
	else
	{
		FIO2CLR = (1<<8);
	}
}

PUBLIC void project_log(
	Uint16					character
) {

}

PUBLIC void project__startSystemTick()
{
	T0TCR = 0x01;
}