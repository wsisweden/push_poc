/*
 * Revision.h
 *
 *  Created on: Aug 23, 2010
 *      Author: nicka
 */

#ifndef REVISION_H_
#define REVISION_H_

#warning below should be automatically updated by hook script
#define SVN_REVISION_NUMBER 232783

#endif /* REVISION_H_ */
