/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		global.h
*
*	\ingroup	PROJECT
*
*	\brief		Projectwide defines etc.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 1667 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef GLOBAL_H_INCLUDED
#define GLOBAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "tstamp.h"
#include "sys.h"
#include "osa.h"
#include "msg.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * \name	Configuration.
 */
/*@{*/
#define PROJECT_ENABLE_TEST	1				/**< Enables test functions		*/
/*@}*/

#if (TARGET_SELECTED & TARGET_CM3) && defined(NDEBUG)
#define PROJECT_USE_WDOG	1				/**< Use the watchdog			*/
#define PROJECT_WDOG_TO		60000			/**< Watchdog timeout in ms		*/
#else
#define PROJECT_USE_WDOG	0				/**< Watchdog disabled			*/
#define PROJECT_WDOG_TO		60000			/**< Watchdog timeout in ms		*/
#endif

/**
 * \name	Hardware configuration.
 */
/*@{*/
#define PROJECT_CPU_HZ	48000000UL			/**< Main clock frequency in Hz	*/
#define PROJECT_MAM_ENABLE	1				/**< Flash accelerator			*/
/*@}*/


/**
 * \name	Possible hardware variations
 */

/*@{*/
#define PROJECT_HW_TLPROTO	1			/**< Tietolaite prototype.			*/
#define PROJECT_HW_MPPROTO	2			/**< Micropower prototype.			*/
/*@}*/

/* todo:	Checksum should be stored at 0x0007 FFF8 according to CM specs.
 */
#define PROJECT_CHKSUM_ADDR	0x0007FFF0	/**< Checksum address				*/

/**
 * The selected hardware variation
 */

#define PROJECT_SELECTED_HW			PROJECT_HW_MPPROTO


#if PROJECT_SELECTED_HW == PROJECT_HW_TLPROTO

/* Tietolaite prototype defines */
#define PROJECT_PIN_DISP_BACKLIGHT	8
#define PROJECT_PIN_DISP_COMMAND	6
#define PROJECT_PIN_DISP_CS			7
#define PROJECT_PORT_DISP_CS		2
#define PROJECT_REG_DIR_DISP_BL		FIO2DIR
#define PROJECT_REG_CLR_DISP_BL		FIO2CLR
#define PROJECT_REG_SET_DISP_BL		FIO2SET
#define PROJECT_REG_DIR_DISP_COMM	FIO2DIR
#define PROJECT_REG_CLR_DISP_COMM	FIO2CLR
#define PROJECT_REG_SET_DISP_COMM	FIO2SET


#elif PROJECT_SELECTED_HW == PROJECT_HW_MPPROTO

/* Micropower prototype defines */
#define PROJECT_PIN_DISP_BACKLIGHT	8
#define PROJECT_PIN_DISP_COMMAND	8
#define PROJECT_PIN_DISP_CS			6
#define PROJECT_PORT_DISP_CS		0
#define PROJECT_REG_DIR_DISP_BL		FIO2DIR
#define PROJECT_REG_CLR_DISP_BL		FIO2CLR
#define PROJECT_REG_SET_DISP_BL		FIO2SET
#define PROJECT_REG_DIR_DISP_COMM	FIO0DIR
#define PROJECT_REG_CLR_DISP_COMM	FIO0CLR
#define PROJECT_REG_SET_DISP_COMM	FIO0SET
#define PROJECT_PIN_ALL_DEFINED		0x0201C713 // Mask with all pin defined in project
#define PROJECT_PIN_RESET_DEFAULTS	0x0201C413 // OK + ESC pins set(logic 0) and all other logic 1
#define PROJECT_PIN_CLEAR_ALL_NVM	0x0201C701 // F1 + F2 pins set(logic 0) and all other logic 1

#else
#error Unknown hardware type

#endif

/**
 * Define firmware mode.
 */
//#define NORMAL		0
//#define USA 			1

//#define FW_MODE 		USA

/**
 * Return codes for store functions.
 */

enum project_storeRetVal
{
	PROJECT_STORERET_OK,				/**< Values store successfully		*/
	PROJECT_STORERET_CAPRANGE,			/**< Capacity out of range			*/
	PROJECT_STORERET_CELLSRANGE,		/**< Number of cells out of range	*/
	PROJECT_STORERET_RESRANGE,			/**< Resistance out of range		*/
	PROJECT_STORERET_BLRANGE,			/**< Base load out of range			*/
	PROJECT_STORERET_ALGNORANGE,		/**< Unknown algorithm number		*/
	PROJECT_STORERET_UBATTTYPE,			/**< Unknown battery type			*/
	PROJECT_STORERET_MISMATCH			/**< Values does not match			*/
};

/**
 * Pin numbers for digital inputs on port1.
 */

enum project_port1Pins
{
	PROJECT_HWP1_STOP = 0,				/**< Key, STOP, DI					*/
	PROJECT_HWP1_F1 = 1,				/**< Key, F1, DI					*/
	PROJECT_HWP1_F2 = 4,				/**< Key, F2, DI					*/
	PROJECT_HWP1_ESC = 8,				/**< Key, ESC, DI					*/
	PROJECT_HWP1_OK = 9,				/**< Key, OK, DI					*/
	PROJECT_HWP1_LEFT = 10,				/**< Key, Left, DI					*/
	PROJECT_HWP1_RIGHT = 14,			/**< Key, Right, DI					*/
	PROJECT_HWP1_UP = 15,				/**< Key, UP, DI					*/
	PROJECT_HWP1_DOWN = 16,				/**< Key, Down, DI					*/
	PROJECT_HWP1_CHALGTEST = 25			/**< CHALG Test, DI					*/
};

/**
 * Pin numbers for digital inputs on port2.
 */

enum project_port2Pins
{
	PROJECT_HWP2_REMOTEINA = 2,			/**< Remote IN-A, DI				*/
	PROJECT_HWP2_REMOTEINB = 3			/**< Remote IN-B, DI				*/
};

/**
 * Pin numbers for digital inputs on port4.
 */

enum project_port4Pins
{
	PROJECT_HWP4_OVERTEMP = 29			/**< Engine Overtemp, DI			*/
};

/**
 * List of all keypad digital inputs
 */

enum project_keys
{
	PROJECT_KEY_STOP,
	PROJECT_KEY_F1,
	PROJECT_KEY_F2,
	PROJECT_KEY_ESC,
	PROJECT_KEY_OK,
	PROJECT_KEY_LEFT,
	PROJECT_KEY_RIGHT,
	PROJECT_KEY_UP,
	PROJECT_KEY_DOWN,

	PROJECT_KEY_COUNT						/**< Total number of keys		*/
};



#if TARGET_SELECTED & TARGET_CM3

#define PROJECT_AHB_BANK0_ADDR 	0x2007C000	/**< Address of AHB SRAM Bank 0	*/
#define PROJECT_AHB_BANK0_SIZE	(16 * 1024)	/**< Size of AHB SRAM Bank 0	*/

#define PROJECT_AHB_BANK1_ADDR	0x20080000	/**< Address of AHB SRAM Bank 1	*/
#define PROJECT_AHB_BANK1_SIZE	(16 * 1024)	/**< Size of AHB SRAM Bank 1	*/

#endif



#ifndef NDEBUG

#if (TARGET_SELECTED & TARGET_W32) == 0

#define PROJECT_USE_STACK_WATCH		1		/**< Stack watch should be	
											 *	enabled in debug mode 		*/
#else

/*
 * No stack watch for Windows.
 */

#define PROJECT_USE_STACK_WATCH		0

#endif			

#else
#define PROJECT_USE_STACK_WATCH		0		/**< Stack watch is optional in	
											 *	release mode 				*/
#endif

/**
 * \name	Error handling constants.
 */

/*@{*/
#define PROJECT_ERROR_TEST		0			/**< Error from test functions	*/
#define PROJECT_ERROR_CHKSUM	1			/**< Error from checksum		*/
#define PROJECT_ERROR_EXCEPTION	2			/**< Error exception (vectors)	*/
/*@}*/


/**
 * Event types used in this project.
 */

enum project_msgType {
	PROJECT_MSGT_STATUSREP = MSG_T_1ST_LOCAL,/**< Status report to sup		*/

	PROJECT_MSGT_COUNT						/**< Number of event types.		*/
};

/**
 * This will turn on external flash memory dump if set to TRUE. External flash
 * memory contents will be dumped to UART0.
 */

#define PROJECT__USE_MEMREAD		FALSE	//TRUE	//FALSE

/**
 *	Sector size for FLMEM
 */
#define PROJECT_FLMEM_SECT_SIZE		FLMEM_SECT_SIZE_TO_8

/**
 * This define can be set to zero if FLMEM should be excluded from the build.
 * If FLMEM is excluded then all non-volatile registers will be volatile.
 */

#define PROJECT_USE_FLMEM	1

/**
 *	Defines if CAN is used
 */
#define PROJECT_USE_CAN		1

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

#if TARGET_SELECTED == TARGET_W32

#define global_print				printf

#define global_dbgLedInit()
#define global_dbgLedOn()
#define global_dbgLedOff()

#define global_dbgBtnInit()
#define global_dbgBtn1()			0
#define global_dbgBtn2()			0

#else

#define global_print(x_)

#define global_dbgLedInit()			IODIR0 |= (1<<PROJECT_KIT_LED)
#define global_dbgLedOn()			IOSET0 = (1<<PROJECT_KIT_LED)
#define global_dbgLedOff()			IOCLR0 = (1<<PROJECT_KIT_LED)

#define global_dbgBtnInit()			IODIR0 &= ~((1<<PROJECT_KIT_BTN1)|(1<<PROJECT_KIT_BTN2))
#define global_dbgBtn1()			(IOPIN0 & (1<<PROJECT_KIT_BTN1) ? 0 : 1)
#define global_dbgBtn2()			(IOPIN0 & (1<<PROJECT_KIT_BTN2) ? 0 : 1)

#endif

#define project_incrMsgCounter()												\
	atomic(																		\
		project_eventCount++;													\
		project_sentEvents++;													\
		project_maxEventCount = MAX(project_eventCount, project_maxEventCount); \
		deb_assert(project_eventCount <= 20);									\
	)

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

extern void					project__errorHandler(Uint8);
extern sys_TxtHandle		project_storeBatteryInfo(Boolean);
extern Boolean				project_storeEngineCode(Uint32	engineCodeNew);
extern void           		project_setStoredUserParamReq(int len);

extern void					project_debugLog(char *);

uint64_t project_getMui(void);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern Uint32 project_stateMachSync;

extern sys_FBInstId const_P MSG_LIST(keybChanged)[];
extern sys_FBInstId const_P MSG_LIST(statusRep)[];

extern Uint32 project_eventCount;
extern Uint32 project_maxEventCount;
extern Uint32 project_sentEvents;

extern Uint8 project_wdogTimeout;

/*****************************************************************************/

#endif
