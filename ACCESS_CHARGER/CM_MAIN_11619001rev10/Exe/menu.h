/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		menu.h
*
*	\ingroup	MPACCESS
*
*	\brief		Menu structure
*
*	\details
*
*	\note
*
*	\version	\$Rev: 455 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************//** \cond priv_decl *//*
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
*	Root->Charging Status->Charging param.
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatusChargerItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_currChargingMode_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BM_CON, reg_i_BID_chalg1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_BM_CON|GUI_CND_INVERT, exe_t_tBID_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_currAlgName_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_currCapacity_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_currCells_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_currCableRes_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_currBaseLoad_chalg1, GUI_PRM_F_NONE, 0),
};


GUI_MENU_TABLE(
	project_menuStatusCharger, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tChargerParam_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Charging Status->Instant data
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatusInstDatItems[] =
{
	GUI_PARAM_ROW(GUI_CND_BM_CON, reg_i_BmSOC_radio1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_BM_CON|GUI_CND_INVERT, exe_t_tBmSOC_radio, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_chalgPhase_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_PmeasSum_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_UVc_chalg1, GUI_PRM_F_NONE, 0, 2),
	GUI_PARAM_ROW_DEC_EX(GUI_CND_NONE, reg_i_ImeasSum_Real_chalg1, GUI_PRM_TEXT, 0, 1, exe_t_tIA_gui),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Ths_meas1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_Tboard_meas1,	GUI_PRM_F_NONE, 0, 1),
//	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_TbattExt_meas1,	GUI_PRM_F_NONE, 0, 1),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_limiterGui_regu1, GUI_PRM_F_NONE, 0),
};


GUI_MENU_TABLE(
	project_menuStatusInstDat, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tInstDat_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Charging Status->Alarms
*---------------------------------------------------------------------------*/

/*
 *	Error log does not have normal menu table but used regu and chalg error sum
 *	registers are defined here.
 */
PRIVATE const_P gui_MenuItem project_menuChargingELogItems[] =
{
	//GUI_PARAM_ROW(GUI_CND_NONE, reg_i_StatusSum_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ReguErrorSum_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChalgErrorSum_chalg1, GUI_PRM_F_NONE, 0),
};


GUI_MENU_TABLE(
	project_menuChargingELog, 
	GUI_TBL_TYPE_ELOG, 
	exe_t_tAlarms_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);


/*-----------------------------------------------------------------------------
*	Root->Charging Status
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatusItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_chargeStartDate_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_chargeStartTime_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_startVoltage_meas1, GUI_PRM_F_NONE, 0, 2),
	GUI_PARAM_ROW_DEC(GUI_CND_CHARGE_COMP, reg_i_endVoltage_meas1, GUI_PRM_F_NONE, 0, 2),
	GUI_TEXT2_ROW(GUI_CND_CHARGE_COMP|GUI_CND_INVERT, exe_t_tEndVoltage_meas, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_chargedAh_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_chargedAhPercent_chalg1, GUI_PRM_F_NONE, 0),
	GUI_SUBMENU_REG(GUI_CND_CUR_ALARMS, &project_menuChargingELog, GUI_MENU_F_NONE, reg_i_AlarmCount_chalg1),
	GUI_TEXT2_ROW(GUI_CND_CUR_ALARMS|GUI_CND_INVERT, exe_t_tAlarms_gui, exe_t_tNone_cm, GUI_TEXT_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatusCharger, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatusInstDat, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuStatus, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tChargingStatus_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Statistics->Charging Cycles->Cycle log->Charger param
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatCyclesParamItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChargingModeTemp_cm1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_LCHMODE_BM, reg_i_BIDTemp_cm1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_LCHMODE_BM|GUI_CND_INVERT, exe_t_tBID_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_AlgNoNameTemp_cm1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_capacityTemp_cm1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_cellsTemp_cm1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_cableRiTemp_cm1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_baseloadTemp_cm1, GUI_PRM_F_NONE, 0),
};


GUI_MENU_TABLE(
	project_menuStatCyclesParam, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tChargerParam_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_F3|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Statistics->Charging Cycles->Cycle log->Alarms
*---------------------------------------------------------------------------*/

/*
 *	Error log does not have normal menu table but used regu and chalg error sum
 *	registers are defined here.
 */
PRIVATE const_P gui_MenuItem project_menuStatCyclesELogItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ReguErrorSum_cm1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChalgErrorSum_cm1, GUI_PRM_F_NONE, 0),
};


GUI_MENU_TABLE(
	project_menuStatCyclesELog, 
	GUI_TBL_TYPE_ELOG, 
	exe_t_tAlarms_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_F2|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Statistics->Charging Cycles->Cycle log
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatCyclesLogItems[] =
{
	GUI_PARAM_REG_LIM(GUI_CND_CYCLE_LOG, reg_i_cycleNo_cm1, reg_i_histLogIndexReset_cm1, GUI_PRM_SLIDE|GUI_PRM_IMMEDIATE|GUI_REG_LOOP),
	GUI_TEXT2_ROW(GUI_CND_CYCLE_LOG|GUI_CND_INVERT, exe_t_tCycleNo_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_CYCLE_LOG_MEM, reg_i_startDate_cm1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_CYCLE_LOG_MEM|GUI_CND_INVERT, exe_t_tDate_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_CYCLE_LOG_MEM, reg_i_startTime_cm1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_CYCLE_LOG_MEM|GUI_CND_INVERT, exe_t_tTime_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW_DEC(GUI_CND_CYCLE_LOG_MEM, reg_i_startVoltage_cm1, GUI_PRM_F_NONE, 0, 2),
	GUI_TEXT2_ROW(GUI_CND_CYCLE_LOG_MEM|GUI_CND_INVERT, exe_t_tStartVoltage_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW_DEC(GUI_CND_CYCLE_LOG_MEM, reg_i_endVoltage_cm1, GUI_PRM_F_NONE, 0, 2),
	GUI_TEXT2_ROW(GUI_CND_CYCLE_LOG_MEM|GUI_CND_INVERT, exe_t_tEndVoltage_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_CYCLE_LOG_MEM, reg_i_chargedAh_cm1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_CYCLE_LOG_MEM|GUI_CND_INVERT, exe_t_tChargedAh_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_CYCLE_LOG_MEM, reg_i_chargedAhP_cm1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_CYCLE_LOG_MEM|GUI_CND_INVERT, exe_t_tChargedAhP_cm, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_SUBMENU_REG(GUI_CND_ALARMS, &project_menuStatCyclesELog, GUI_MENU_F_NONE, reg_i_AlarmCount_cm1),
	GUI_TEXT2_ROW(GUI_CND_ALARMS|GUI_CND_INVERT, exe_t_tAlarms_gui, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_SUBMENU(GUI_CND_CYCLE_LOG_MEM, &project_menuStatCyclesParam, GUI_MENU_F_NONE),
	GUI_TEXT_ROW(GUI_CND_CYCLE_LOG_MEM|GUI_CND_INVERT, exe_t_tChargerParam_gui, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuStatCyclesLog, 
	GUI_TBL_TYPE_NORM, /* todo */
	exe_t_tCycleLog_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_F1|GUI_F_EXIT_F2
);


/*-----------------------------------------------------------------------------
*	Root->Statistics->Charging Cycles->Cycles summary
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatCyclesSumItems[] =
{
	GUI_TEXT2_ROW(GUI_CND_NONE, exe_t_tCycLeft_sup, exe_t_tCycRight_sup, GUI_TEXT_F_NONE),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_CyclesSumBelow25_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_CyclesSum26to50_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_CyclesSum51to80_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_CyclesSum81to90_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_CyclesSumAbove90_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_CyclesSumTotal_sup1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuStatCyclesSum, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tCyclesSummary_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Statistics->Charger
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatChargerItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChargeTimeTotal_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChargedAhTotal_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChargedWhTotalP_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_OverVoltCnt_meas1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuStatCharger, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tCharger_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Statistics->Charging Cycles
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatCyclesItems[] =
{
	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatCyclesSum, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatCyclesLog, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuStatCycles, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tChargingCycles_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);


/*-----------------------------------------------------------------------------
*	Root->Statistics
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuStatisticsItems[] =
{
	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatCycles, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatCharger, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuStatistics, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tStatistics_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Keyboard->F2 button
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuKeyboardF2Items[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ButtonF2_func_sup1,	GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuKeyboardF2, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tF2button_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Keyboard->F1 button
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuKeyboardF1Items[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ButtonF1_func_sup1,	GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuKeyboardF1, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tF1button_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);


/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable->Sunday
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeSundayItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, GUI_NO_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeSunday, 
	GUI_TBL_TYPE_TIME, 
	exe_t_tSunday_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_TIME_SUN
);


/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable->Saturday
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeSaturdayItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, GUI_NO_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeSaturday, 
	GUI_TBL_TYPE_TIME, 
	exe_t_tSaturday_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_TIME_SAT
);

/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable->Friday
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeFridayItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, GUI_NO_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeFriday, 
	GUI_TBL_TYPE_TIME, 
	exe_t_tFriday_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_TIME_FRI
);

/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable->Thursday
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeThursdayItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, GUI_NO_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeThursday, 
	GUI_TBL_TYPE_TIME, 
	exe_t_tThursday_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_TIME_THU
);
/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable->Wednesday
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeWednesdayItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, GUI_NO_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeWednesday, 
	GUI_TBL_TYPE_TIME, 
	exe_t_tWednesday_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_TIME_WED
);

/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable->Tuesday
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeTuesdayItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, GUI_NO_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeTuesday, 
	GUI_TBL_TYPE_TIME, 
	exe_t_tTuesday_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_TIME_TUE
);
/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable->Monday
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeMondayItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, GUI_NO_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeMonday, 
	GUI_TBL_TYPE_TIME, 
	exe_t_tMonday_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_TIME_MON
);

/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions->Timetable
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeTableItems[] =
{
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetTimeMonday, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetTimeTuesday, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetTimeWednesday, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetTimeThursday, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetTimeFriday, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetTimeSaturday, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetTimeSunday, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeTable, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tTimeTable_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Extra charge
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuExtraChargeItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ExtraCharge_func_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ExtraCharge_active_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ExtraCharge_var_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM2_ROW(GUI_CND_NONE, reg_i_ECTime_read_sup1, reg_i_ECTime_write_sup1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuExtraCharge,
	GUI_TBL_TYPE_NORM,
	exe_t_tExtraCharge_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Factory settings->Factory defaults
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuFactoryDefaultsItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_FactorySet_sup1,	GUI_FACT_SET|GUI_PRM_SINGLE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_FactoryDefaults_sup1,	GUI_FACT_DEFAULT|GUI_PRM_SINGLE, 0),
};

GUI_MENU_TABLE(
	project_menuFactoryDefaults,
	GUI_TBL_TYPE_NORM,
	exe_t_tFactoryDefault_sup,
	GUI_ACCESS_LEVEL2,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Factory settings
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuFactoryItems[] =
{
	GUI_PARAM_DE_ROW(GUI_CND_NONE, reg_i_engineIdx_default_sup1,	GUI_PRM_F_NONE, sup_enpackEnumCode),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_IdcLimit_regu1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_IacLimit_regu1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_PacLimit_regu1,	GUI_PRM_F_NONE, 0),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuFactoryDefaults, GUI_MENU_F_NONE),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ClearStatistics_sup1,	GUI_CLEAR_STAT|GUI_PRM_SINGLE, 0),
};

GUI_MENU_TABLE(
	project_menuFactory, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tFactorySettings_gui, 
	GUI_ACCESS_LEVEL2,
	GUI_F_ENTER_NONE|GUI_F_EXIT_F3
);

/*-----------------------------------------------------------------------------
*	Root->Service->Calibration
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuCalibItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, exe_t_tVoltCal_meas, GUI_TEXT_F_NONE),
	GUI_PARAM2_ROW_DEC(GUI_CND_NONE, reg_i_Umeas1s_meas1, reg_i_Ucalib_meas1, GUI_PRM_SLIDE|GUI_PRM_ACCEL, 0, 2),
	GUI_TEXT_ROW(GUI_CND_NONE, exe_t_tCurrCal_meas, GUI_TEXT_F_NONE),
	GUI_PARAM2_ROW_DEC(GUI_CND_NONE, reg_i_Imeas1s_meas1, reg_i_Icalib_meas1, GUI_PRM_SLIDE|GUI_PRM_ACCEL, 0, 2),
};

GUI_MENU_TABLE(
	project_menuCalib, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tCalibration_gui, 
	GUI_ACCESS_LEVEL2,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);


/*-----------------------------------------------------------------------------
*	Root->Service->CAN->Network Status
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuCANStatusItems[0];

GUI_MENU_TABLE(
	project_menuCANStatus, 
	GUI_TBL_TYPE_CAN,
	exe_t_tCANNetwork_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->CAN->Network info->Derate
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuCanDerateItems[] =
{
	/* Derate bits */
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_DerateP_cc1, exe_t_tDeratePhase_cc, GUI_PRM_F_NONE, 0, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_DerateP_cc1, exe_t_tDerateUdef_cc, GUI_PRM_F_NONE, 1, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_DerateP_cc1, exe_t_tDerateUIchar_cc, GUI_PRM_F_NONE, 2, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_DerateP_cc1, exe_t_tDerateTemp_cc, GUI_PRM_F_NONE, 3, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_DerateP_cc1, exe_t_tDerateNoLoad_cc, GUI_PRM_F_NONE, 4, exe_t_tOffOn_sup),
};

GUI_MENU_TABLE(
	project_menuCanDerate,
	GUI_TBL_TYPE_NORM,
	exe_t_tDerateP_cc,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->CAN->Network info->Warning
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuCanWarningItems[] =
{
	/* Warning bits */
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_WarningP_cc1, exe_t_tErrorPhase_cc, GUI_PRM_F_NONE, 0, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_WarningP_cc1, exe_t_tErrorRegu_cc, GUI_PRM_F_NONE, 1, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_WarningP_cc1, exe_t_tErrorTempL_cc, GUI_PRM_F_NONE, 2, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_WarningP_cc1, exe_t_tErrorTempH_cc, GUI_PRM_F_NONE, 3, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_WarningP_cc1, exe_t_tErrorTempHW_cc, GUI_PRM_F_NONE, 6, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_WarningP_cc1, exe_t_tErrorCAN_cc, GUI_PRM_F_NONE, 7, exe_t_tOffOn_sup),
};

GUI_MENU_TABLE(
	project_menuCanWarning,
	GUI_TBL_TYPE_NORM,
	exe_t_tWarningP_cc,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->CAN->Network info->Error
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuCanErrorItems[] =
{
	/* Error bits */
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_ErrorP_cc1, exe_t_tErrorPhase_cc, GUI_PRM_F_NONE, 0, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_ErrorP_cc1, exe_t_tErrorRegu_cc, GUI_PRM_F_NONE, 1, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_ErrorP_cc1, exe_t_tErrorTempL_cc, GUI_PRM_F_NONE, 2, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_ErrorP_cc1, exe_t_tErrorTempH_cc, GUI_PRM_F_NONE, 3, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_ErrorP_cc1, exe_t_tErrorTempHW_cc, GUI_PRM_F_NONE, 6, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_ErrorP_cc1, exe_t_tErrorCAN_cc, GUI_PRM_F_NONE, 7, exe_t_tOffOn_sup),
};

GUI_MENU_TABLE(
	project_menuCanError,
	GUI_TBL_TYPE_NORM,
	exe_t_tErrorP_cc,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->CAN->Network info
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuCanNetInfoItems[] =
{
	//GUI_PARAM_ROW(GUI_CND_NONE, reg_i_SyncP_cc1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_SyncP_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_UsetP_cc1, GUI_PRM_F_NONE, 0, 2),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_IsetP_cc1, GUI_PRM_F_NONE, 0, 2),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_PsetP_cc1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CAN_MASTER_EXT, reg_i_SocP_cc1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_UmeasP_cc1, GUI_PRM_F_NONE, 0, 2),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_ImeasP_cc1, GUI_PRM_F_NONE, 0, 2),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_PmeasP_cc1, GUI_PRM_F_NONE, 0),
	GUI_SUBMENU_REG(GUI_CND_NONE, &project_menuCanDerate, GUI_MENU_F_NONE, reg_i_DerateDisp_cc1),
	GUI_SUBMENU_REG(GUI_CND_NONE, &project_menuCanWarning, GUI_MENU_F_NONE, reg_i_WarningDisp_cc1),
	GUI_SUBMENU_REG(GUI_CND_NONE, &project_menuCanError, GUI_MENU_F_NONE, reg_i_ErrorDisp_cc1),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RemoteP_cc1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuCanNetInfo,
	GUI_TBL_TYPE_NORM,
	exe_t_tNetInfo_cc,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->CAN
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuCANItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_CAN_CommProfile_can1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CAN_ON, reg_i_State_can1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CAN_ON, reg_i_CanBps_can1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CAN_ID_AUTO, reg_i_NodeId_can1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CAN_ID_SET, reg_i_NodeIdFixed_can1, GUI_PRM_F_NONE, 0),
	GUI_SUBMENU(GUI_CND_CAN_MASTER, &project_menuCanNetInfo, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CAN_SLAVE, &project_menuCanNetInfo, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CAN_MASTER, &project_menuCANStatus, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuCAN,
	GUI_TBL_TYPE_NORM,
	exe_t_tCAN_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Radio->Network settings
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuRadioNetSettingsItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_NwkSettings_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChargerId_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_ACHAN|GUI_CND_INVERT, reg_i_setChannel_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_ACHAN|GUI_CND_INVERT, reg_i_setPanId_radio1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_ACHAN, exe_t_tChannel_radio, exe_t_tNa_gui, 0),
	GUI_TEXT2_ROW(GUI_CND_ACHAN, exe_t_tPanID_radio, exe_t_tNa_gui, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_setNodeId_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM2_ROW(GUI_CND_NONE, reg_i_RoutingP_radio1, reg_i_Routing_radio1, GUI_PRM_F_NONE, 0),
	GUI_TEXT_ROW(GUI_CND_NONE, SYS_BAD_TEXT, GUI_TEXT_F_NONE),
};

GUI_MENU_TABLE(
	project_menuRadioNetSettings, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tRadioNetwork_radio, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F11|GUI_F_EXIT_F6
);

/*-----------------------------------------------------------------------------
*	Root->Service->Radio->Network info
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuRadioNetInfoItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChargerIdP_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Channel_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_PanId_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_NodeId_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_signalStrength_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_AddrPool_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RoutingP_radio1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuRadioNetInfo, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tNetSett_radio, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F0 | GUI_F_EXIT_F1
);

/*-----------------------------------------------------------------------------
*	Root->Service->Radio
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuRadioItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RadioMode_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_RADIO_OFF|GUI_CND_INVERT, reg_i_nwkStatus_radio1, GUI_PRM_F_NONE, 0),
	GUI_SUBMENU(GUI_CND_RADIO_OFF|GUI_CND_INVERT, &project_menuRadioNetSettings, GUI_MENU_F_NONE),

	GUI_PARAM_ROW(GUI_CND_NOT_JOINED, reg_i_startNetwork_radio1, GUI_PRM_SINGLE, 1),
	GUI_PARAM_ROW(GUI_CND_NOT_JOINED, reg_i_joinNetwork_radio1, GUI_PRM_SINGLE, 1),

	GUI_SUBMENU(GUI_CND_CONNECTED, &project_menuRadioNetInfo, GUI_MENU_F_NONE),
	GUI_PARAM_ROW(GUI_CND_JOINED, reg_i_joinEnable_radio1, GUI_PRM_SINGLE, 1),
	GUI_PARAM_ROW(GUI_CND_JOINED, reg_i_leaveNetwork_radio1, GUI_PRM_SINGLE, 1),

};

GUI_MENU_TABLE(
	project_menuRadio, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tRadio_radio, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Display
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuDisplayItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_DisplayContrast_gui1, GUI_PRM_SLIDE|GUI_PRM_IMMEDIATE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_backlight_delay_gui1, GUI_PRM_SLIDE, 0),
};

GUI_MENU_TABLE(
	project_menuDisplay, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tDisplay_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);


/*-----------------------------------------------------------------------------
*	Root->Service->Date and time
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuDateAndTimeItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_TimeDateFormat_gui1, GUI_PRM_F_NONE, 0),
	GUI_PARAM2_ROW(GUI_CND_NONE, reg_i_date_read_sup1, reg_i_date_write_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM2_ROW(GUI_CND_NONE, reg_i_time_read_sup1, reg_i_time_write_sup1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuDateAndTime, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tDateAndTime_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Remote output->Output schedule
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetRemoteOutScheItems[] =
{
	GUI_TEXT2_ROW(GUI_CND_NONE, exe_t_tPhase_gui, exe_t_tOutput_gui, GUI_TEXT_F_NONE),

	/* Phase bits */
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_RemoteOutPhase_var_sup1, exe_t_tChargePre_gui, GUI_PRM_F_NONE, 0, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_RemoteOutPhase_var_sup1, exe_t_tChargeMain_gui, GUI_PRM_F_NONE, 1, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_RemoteOutPhase_var_sup1, exe_t_tChargeAdditional_gui, GUI_PRM_F_NONE, 2, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_RemoteOutPhase_var_sup1, exe_t_tChargeReady_gui, GUI_PRM_F_NONE, 3, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_RemoteOutPhase_var_sup1, exe_t_tChargeEqualize_gui, GUI_PRM_F_NONE, 4, exe_t_tOffOn_sup),
	GUI_PARAM_BM_ROW(GUI_CND_NONE, reg_i_RemoteOutPhase_var_sup1, exe_t_tChargeIdle_gui, GUI_PRM_F_NONE, 5, exe_t_tOffOn_sup),
};

GUI_MENU_TABLE(
	project_menuSetRemoteOutSche, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tOutputSchedul_sup, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);
/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->Water
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetWaterOutputItems[] =
{
//	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Water_func_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Water_var_sup1,	GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuSetWaterOutput,
	GUI_TBL_TYPE_NORM, 
	exe_t_tSettings_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);
/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->Air
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetAirOutputItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_AirPump_var_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_AirPump_var2_sup1,GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_AirPumpAlarmLow_IObus1,GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_AirPumpAlarmHigh_IObus1,GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_AirPumpPressureP_IObus1,GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuSetAirOutput,
	GUI_TBL_TYPE_NORM, 
	exe_t_tSettings_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);
/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->BBC
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetBBCOutputItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RemoteOutBBC_var_sup1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RemoteOutBBC_var_sup1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 1),
};

GUI_MENU_TABLE(
	project_menuSetBBCOutput,
	GUI_TBL_TYPE_NORM,
	exe_t_tSettings_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);
/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->I threshold // sorabITR - not now
*---------------------------------------------------------------------------*/

/*PRIVATE const_P gui_MenuItem project_menuSetITROutputItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Ithreshold_var_sup1, GUI_PRM_F_NONE, 0),

	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Ithreshold_cond_sup1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Ithreshold_cond_sup1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 1),
};

GUI_MENU_TABLE(
	project_menuSetITROutput,
	GUI_TBL_TYPE_NORM,
	exe_t_tSettings_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);*/
/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->Remote output
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetRemoteOutputItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RemoteOut_func_sup1, GUI_PRM_F_NONE, 0),
	GUI_SUBMENU(GUI_CND_RO_PHASE, &project_menuSetRemoteOutSche, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_RO_WATER, &project_menuSetWaterOutput, GUI_MENU_F_NONE),	// Time setting function added
	GUI_SUBMENU(GUI_CND_RO_AIR, &project_menuSetAirOutput, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_RO_BBC, &project_menuSetBBCOutput, GUI_MENU_F_NONE),
	//GUI_SUBMENU(GUI_CND_RO_ITHRESHOLD, &project_menuSetITROutput, GUI_MENU_F_NONE), // sorabITR - not now
};

GUI_MENU_TABLE(
	project_menuSetRemoteOutput,
	GUI_TBL_TYPE_NORM, 
	exe_t_tRemoteOutput_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->Extra inputs
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetIoBusInputItems[] = {};

GUI_MENU_TABLE(
	project_menuSetIoBusInput,
	GUI_TBL_TYPE_SRI,
	exe_t_tIoBusInput_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->Extra outputs
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetIoBusOutputItems[] = {};

GUI_MENU_TABLE(
	project_menuSetIoBusOutput,
	GUI_TBL_TYPE_SRO,
	exe_t_tIoBusOutput_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->I/O Control->Remote input
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetRemoteInputItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RemoteIn_func_sup1, GUI_PRM_F_NONE, 0),
//	GUI_PARAM_ROW(GUI_CND_REMIN_PAUSE, reg_i_RemoteIn_var_sup1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuSetRemoteInput,
	GUI_TBL_TYPE_NORM, 
	exe_t_tRemoteInput_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->BBC
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuBBCItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_BBC_func_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_BBC_var_sup1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuBBC,
	GUI_TBL_TYPE_NORM,
	exe_t_tBBC_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->DPL
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuDPLItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_PowerGroup_func_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_PowerGroup_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_DplPriorityFactor_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DPL_SLAVE, reg_i_DplPacLimit_default_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DPL_MASTER, reg_i_DplPowerLimitTotal_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_DplPacLimit_regu1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_DplId_regu1, GUI_PRM_F_NONE, 0),

};

GUI_MENU_TABLE(
	project_menuDPL,
	GUI_TBL_TYPE_DPL,
	exe_t_tDPL_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F13|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Equalize->Settings
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuEquSettingsItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Equalize_var3_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Equalize_var4_sup1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Equalize_var5_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Equalize_var6_sup1,	GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuEquSettings,
	GUI_TBL_TYPE_NORM,
	exe_t_tSettings_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);
/*-----------------------------------------------------------------------------
*	Root->Service->Equalize
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuEquItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Equalize_func_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Equalize_active_sup1, GUI_PRM_F_NONE, 0),
//	GUI_SUBMENU(GUI_CND_NONE, &project_menuEquSettings, GUI_MENU_F_NONE),
	GUI_PARAM_ROW(GUI_CND_EQU_CYCLIC, reg_i_Equalize_var_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_EQU_CYCLIC, reg_i_EquCycleCount_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_EQU_WEEKDAY, reg_i_Equalize_var2_sup1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuEqu,
	GUI_TBL_TYPE_NORM,
	exe_t_tEqu_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Time restrictions
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetTimeRestItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_TimeRestriction_sup1, GUI_PRM_F_NONE, 0),
	GUI_SUBMENU(GUI_CND_CUR_TIMERES, &project_menuSetTimeTable, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuSetTimeRest, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tTimeRestrictions_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Charger param-> Battery 6c
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuBattery6Items[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_enabled_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_DE_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_algIdx_tmp_chalg1, GUI_PRM_F_NONE, chalg_capackEnum),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_capacity_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cells_tmp_chalg1,		GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cableRes_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_baseLoad_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_BATT_CELL_FUNC, reg_i_batteryTemp_tmp_chalg1, GUI_PRM_SLIDE, 0, 1),
};

GUI_MENU_TABLE(
	project_menuBattery6,
	GUI_TBL_TYPE_NORM,
	exe_t_tBattery6_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F12|GUI_F_EXIT_F0
);

/*-----------------------------------------------------------------------------
*	Root->Service->Charger param-> Battery 12c
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuBattery12Items[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_enabled_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_DE_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_algIdx_tmp_chalg1, GUI_PRM_F_NONE, chalg_capackEnum),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_capacity_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cells_tmp_chalg1,		GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cableRes_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_baseLoad_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_BATT_CELL_FUNC, reg_i_batteryTemp_tmp_chalg1, GUI_PRM_SLIDE, 0, 1),
};

GUI_MENU_TABLE(
	project_menuBattery12,
	GUI_TBL_TYPE_NORM,
	exe_t_tBattery12_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F7|GUI_F_EXIT_F0
);

/*-----------------------------------------------------------------------------
*	Root->Service->Charger param-> Battery 18c
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuBattery18Items[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_enabled_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_DE_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_algIdx_tmp_chalg1, GUI_PRM_F_NONE, chalg_capackEnum),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_capacity_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cells_tmp_chalg1,		GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cableRes_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_baseLoad_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_BATT_CELL_FUNC, reg_i_batteryTemp_tmp_chalg1, GUI_PRM_SLIDE, 0, 1),
};

GUI_MENU_TABLE(
	project_menuBattery18,
	GUI_TBL_TYPE_NORM,
	exe_t_tBattery18_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F8|GUI_F_EXIT_F0
);
/*-----------------------------------------------------------------------------
*	Root->Service->Charger param-> Battery 24c
*---------------------------------------------------------------------------*/
PRIVATE const_P gui_MenuItem project_menuBattery24Items[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_enabled_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_DE_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_algIdx_tmp_chalg1, GUI_PRM_F_NONE, chalg_capackEnum),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_capacity_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cells_tmp_chalg1,		GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cableRes_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_baseLoad_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_BATT_CELL_FUNC, reg_i_batteryTemp_tmp_chalg1, GUI_PRM_SLIDE, 0, 1),
};

GUI_MENU_TABLE(
	project_menuBattery24,
	GUI_TBL_TYPE_NORM,
	exe_t_tBattery24_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F9|GUI_F_EXIT_F0
);
/*-----------------------------------------------------------------------------
*	Root->Service->Charger param-> Battery 40c
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuBattery40Items[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_enabled_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_DE_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_algIdx_tmp_chalg1, GUI_PRM_F_NONE, chalg_capackEnum),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_capacity_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cells_tmp_chalg1,		GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_cableRes_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_BATT_CELL_FUNC, reg_i_baseLoad_tmp_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_BATT_CELL_FUNC, reg_i_batteryTemp_tmp_chalg1, GUI_PRM_SLIDE, 0, 1),
};

GUI_MENU_TABLE(
	project_menuBattery40,
	GUI_TBL_TYPE_NORM,
	exe_t_tBattery40_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F10|GUI_F_EXIT_F0
);
/*-----------------------------------------------------------------------------
*	Root->Service->Charger param->User params
*---------------------------------------------------------------------------*/
PRIVATE const_P gui_MenuItem project_menuSetChargingParamItems[0];

GUI_MENU_TABLE(
	project_menuSetChargingParam,
	GUI_TBL_TYPE_SCP,
	exe_t_tSetParameters_gui,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_F5
);
/*-----------------------------------------------------------------------------
*	Root->Service->Charger param.
*---------------------------------------------------------------------------*/
PRIVATE const_P gui_MenuItem project_menuSetChargerParamItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ChargingMode_default_chalg1,	GUI_PRM_F_NONE, 0),
	// User def or Dual
	GUI_PARAM_DE_ROW(GUI_CND_CHMODE_DEF_DUAL, reg_i_algIdx_default_chalg1, GUI_PRM_F_NONE, chalg_capackEnum),
	GUI_PARAM_ROW(GUI_CND_CHMODE_DEF_DUAL, reg_i_capacity_default_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CHMODE_DEF_DUAL, reg_i_cells_default_chalg1,		GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CHMODE_DEF_DUAL, reg_i_cableRes_default_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CHMODE_DEF_DUAL, reg_i_baseLoad_default_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_CHMODE_DEF_DUAL, reg_i_BatteryTemp_default_chalg1, GUI_PRM_SLIDE, 0, 1),

	// BMU when connected
	GUI_PARAM_DE_ROW(GUI_CND_CHMODE_BM_CON, reg_i_algIdx_bm_chalg1, GUI_PRM_F_NONE, chalg_capackEnum),
	GUI_PARAM_ROW(GUI_CND_CHMODE_BM_CON, reg_i_capacity_bm_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CHMODE_BM_CON, reg_i_cells_bm_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CHMODE_BM_CON, reg_i_cableRes_bm_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_CHMODE_BM_CON, reg_i_baseLoad_bm_chalg1,	GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_CHMODE_BM_CON, exe_t_tBatteryTemp_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	// BMU when not connected
	GUI_TEXT2_ROW(GUI_CND_CHMODE_BM_NOCON, exe_t_tAlgName_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_TEXT2_ROW(GUI_CND_CHMODE_BM_NOCON, exe_t_tCapacity_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_TEXT2_ROW(GUI_CND_CHMODE_BM_NOCON, exe_t_tCells_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_TEXT2_ROW(GUI_CND_CHMODE_BM_NOCON, exe_t_tCableRes_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_TEXT2_ROW(GUI_CND_CHMODE_BM_NOCON, exe_t_tBaseload_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),
	GUI_TEXT2_ROW(GUI_CND_CHMODE_BM_NOCON, exe_t_tBatteryTemp_chalg, exe_t_tNa_gui, GUI_TEXT_F_NONE),

	// User def or Dual or BMU and connected
	GUI_SUBMENU(GUI_CND_USERPARAM, &project_menuSetChargingParam, GUI_MENU_F_NONE),

	// Multi mode
	GUI_SUBMENU(GUI_CND_CHMODE_MULTI, &project_menuBattery6, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHMODE_MULTI, &project_menuBattery12, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHMODE_MULTI, &project_menuBattery18, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHMODE_MULTI, &project_menuBattery24, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHMODE_MULTI, &project_menuBattery40, GUI_MENU_F_NONE),

};

GUI_MENU_TABLE(
	project_menuSetChargerParam, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tChargerParam_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F6|GUI_F_EXIT_F0
);

/*-----------------------------------------------------------------------------
*	Root->Service->Parallel Control->Charger Selection
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSetChargerSelItems[0];

GUI_MENU_TABLE(
	project_menuSetChargerSel, 
	GUI_TBL_TYPE_CS, 
	exe_t_tChargerSelection_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Parallel Control
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuParallelItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_ParallelControl_func_cc1,	GUI_PRM_F_NONE, 0),
	GUI_SUBMENU(GUI_CND_PARALLEL, &project_menuSetChargerSel, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuParallel, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tParallelControl_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Advanced->Instant log
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuInstLogItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_InstLogSamplePeriod_sup1, GUI_PRM_F_NONE, 0),
};

GUI_MENU_TABLE(
	project_menuInstLog,
	GUI_TBL_TYPE_NORM,
	exe_t_tInstLog_gui,
	GUI_ACCESS_LEVEL2,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Debug menu
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuDebugItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_serialTxCnt_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DEB_TESTSER, reg_i_serialRxCnt_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DEB_ASSERTSER, reg_i_assertStackOverRun_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DEB_ASSERTSER, reg_i_assertHeapCompromised_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DEB_ASSERTSER, reg_i_assertOutOfEvents_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DEB_ASSERTSER, reg_i_assertBadReceivePtr_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DEB_ASSERTSER, reg_i_assertNwkWatchdog_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_DEB_ASSERTSER, reg_i_assertGwWatchdog_radio1, GUI_PRM_F_NONE, 0),

};


GUI_MENU_TABLE(
	project_menuDebug,
	GUI_TBL_TYPE_NORM,
	exe_t_tDebug_gui,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_F4|GUI_F_EXIT_F4
);

/*-----------------------------------------------------------------------------
*	Root->Service->Advanced->RadioDebug
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuRadioDebugItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RadioDebugFunc_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_RadioDebugInt_radio1, GUI_PRM_F_NONE, 0),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuDebug, GUI_PRM_F_NONE),
};

GUI_MENU_TABLE(
	project_menuRadioDebug,
	GUI_TBL_TYPE_NORM,
	exe_t_tRadioDebug_radio,
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Advanced
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuAdvancedItems[] =
{
	GUI_PARAM_ROW(GUI_CND_CHALG, reg_i_ForceStart_chalg1, GUI_PRM_SINGLE, 1),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuInstLog, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatusInstDat, GUI_MENU_F_NONE),
	// Page 2 if logged in level 2 and charging algorithm controlled
	GUI_PARAM_BM_ROW(GUI_CND_LOGGED2CHALG, reg_i_BitConfig_chalg1, exe_t_tCECMode_chalg, GUI_FLG_PAGE_BREAK, 0, exe_t_tFuncOnOff_sup),
	GUI_PARAM_BM_ROW(GUI_CND_LOGGED2CHALG, reg_i_BitConfig_chalg1, exe_t_tUserParamMode_chalg, GUI_PRM_F_NONE, 1, exe_t_tFuncOnOff_sup),
	GUI_PARAM_BM_ROW(GUI_CND_LOGGED2CHALG, reg_i_BitConfig_chalg1, exe_t_tQuietDerate_chalg, GUI_PRM_F_NONE, 2, exe_t_tFuncOnOff_sup),
	// Page 2 if logged in level 2 and BMS controlled (or Slave)
	GUI_PARAM_BM_ROW(GUI_CND_LOGGED2NOCHALG, reg_i_BitConfig_chalg1, exe_t_tQuietDerate_chalg, GUI_FLG_PAGE_BREAK, 2, exe_t_tFuncOnOff_sup),
};

GUI_MENU_TABLE(
	project_menuAdvanced, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tAdvanced_gui, 
	GUI_ACCESS_LEVEL2,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);
/*-----------------------------------------------------------------------------
*	Root->Service->I/O control
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuIOControlItems[] =
{
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetRemoteInput, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetRemoteOutput, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetIoBusInput, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSetIoBusOutput, GUI_MENU_F_NONE),
};

GUI_MENU_TABLE(
	project_menuIOControl, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tIOControl_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service->Charger Info
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuChargerInfoItems[] =
{
	GUI_PARAM_ROW(GUI_CND_RADIO_FW_VER_AVAILABLE, reg_i_SerialNo_sup1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_RADIO_FW_VER_AVAILABLE|GUI_CND_INVERT, exe_t_tSerialNo_sup, exe_t_tNa_gui, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_FirmwareTypeMain_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_FirmwareVerMain_sup1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_RADIO_FW_VER_AVAILABLE, reg_i_FirmwareTypeRF_radio1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_RADIO_FW_VER_AVAILABLE|GUI_CND_INVERT, exe_t_tFirmwareTypeRF_radio, exe_t_tNa_gui, 0),
	GUI_PARAM_ROW(GUI_CND_RADIO_FW_VER_AVAILABLE, reg_i_FirmwareVerRF_radio1, GUI_PRM_F_NONE, 0),
	GUI_TEXT2_ROW(GUI_CND_RADIO_FW_VER_AVAILABLE|GUI_CND_INVERT, exe_t_tFirmwareVerRF_radio, exe_t_tNa_gui, 0)
};

GUI_MENU_TABLE(
	project_menuChargerInfo, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tChargerInfo_gui, 
	GUI_ACCESS_LEVEL1,
	GUI_F_ENTER_F5|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Service
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuSettingsItems[] =
{
	GUI_SUBMENU(GUI_CND_NONE, &project_menuChargerInfo, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHALG, &project_menuSetChargerParam, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHALG, &project_menuSetTimeRest, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHALG, &project_menuExtraCharge, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHALG, &project_menuEqu, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHALG_JOINED, &project_menuBBC, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuDPL, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuIOControl, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CAN_MASTER, &project_menuParallel, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_CHALG_USA, &project_menuKeyboardF1, GUI_MENU_F_NONE), //only show if FW_MODE != USA
	GUI_SUBMENU(GUI_CND_CHALG, &project_menuKeyboardF2, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuDateAndTime, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuDisplay, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuRadio, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuCAN, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuCalib, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuFactory, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuAdvanced, GUI_MENU_F_NONE),
};


GUI_MENU_TABLE(
	project_menuSettings, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tService_gui,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);


/*-----------------------------------------------------------------------------
*	Root->Language
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuLangSelItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Language_gui1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Language_gui1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 2),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Language_gui1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 1),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Language_gui1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 3),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Language_gui1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 4),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Language_gui1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 5),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_Language_gui1, GUI_PRM_ENUM_TXT|GUI_PRM_SINGLE|GUI_PRM_CHKBOX, 6),
};

GUI_MENU_TABLE(
	project_menuLangSel, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tLanguage_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);


/*-----------------------------------------------------------------------------
 *	Root->Login
 *---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuLoginItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_login_gui1, GUI_PRM_MASKED, 0),
};

GUI_MENU_TABLE(
	project_menuLogin, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tLogMenu_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
 *	Root->Function Activation
 *---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuActivationItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_login_gui1, GUI_PRM_MASKED, 0),
};

GUI_MENU_TABLE(
	project_menuActivation,
	GUI_TBL_TYPE_NORM,
	exe_t_tActivationMenu_gui,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root->Battery Status
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuBatteryStatusItems[] =
{
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_BID_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_currBidTag_chalg1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_BmSOC_radio1, GUI_PRM_F_NONE, 0),
	GUI_PARAM_ROW_DEC(GUI_CND_NONE, reg_i_BmTbattP_radio1, GUI_PRM_F_NONE, 0, 1),
};


GUI_MENU_TABLE(
	project_menuBatteryStatus, 
	GUI_TBL_TYPE_NORM, 
	exe_t_tBatteryStatus_gui, 
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/*-----------------------------------------------------------------------------
*	Root (accessed from process menu)
*---------------------------------------------------------------------------*/

PRIVATE const_P gui_MenuItem project_menuRootItems[] =
{
	GUI_TEXT_ROW(GUI_CND_NONE, exe_t_tMenuHeader_gui, GUI_TEXT_AL_CENTER|GUI_TEXT_BOLD),
	GUI_PARAM_ROW(GUI_CND_NONE, reg_i_EngineType_sup1, GUI_REG_VALUE, 0),

	GUI_SUBMENU(GUI_CND_BM_CON, &project_menuBatteryStatus, GUI_MENU_F_NONE),
//	GUI_TEXT_ROW(GUI_CND_BM_CON|GUI_CND_INVERT, exe_t_tBatteryStatus_gui, GUI_TEXT_F_NONE),

	GUI_SUBMENU(GUI_CND_CHARGE_STAT, &project_menuStatus, GUI_MENU_F_NONE),
//	GUI_TEXT_ROW(GUI_CND_CHARGE_STAT|GUI_CND_INVERT, exe_t_tChargingStatus_gui, GUI_TEXT_F_NONE),

	GUI_SUBMENU(GUI_CND_NONE, &project_menuStatistics, GUI_MENU_F_NONE),
	GUI_SUBMENU(GUI_CND_NONE, &project_menuSettings, GUI_MENU_F_NONE),

	GUI_SUBMENU(GUI_CND_LOGGED|GUI_CND_INVERT, &project_menuLogin, GUI_MENU_F_NONE),
	GUI_PARAM_ROW(GUI_CND_LOGGED, reg_i_logout_gui1, GUI_PRM_SINGLE, 1),

	GUI_SUBMENU(GUI_CND_NONE, &project_menuLangSel, GUI_MENU_LANG),
};

GUI_MENU_TABLE(
	project_menuRoot, 
	GUI_TBL_TYPE_ROOT|GUI_TBL_NO_HDR, 
	exe_t_tMenuHeader_gui,
	GUI_ACCESS_LEVEL0,
	GUI_F_ENTER_NONE|GUI_F_EXIT_NONE
);

/* End of declaration module ***********************************//** \endcond */
