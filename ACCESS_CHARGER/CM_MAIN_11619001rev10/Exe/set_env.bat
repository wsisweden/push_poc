@echo off
REM ****************************************************************************
REM * This batch will set the environment variables needed to compile a project
REM * using the platform tools. It is meant to be sent with releases of projects
REM * that are built on T-Plat.E. It sets the same variables as c.bat.
REM *
REM * The first environment variables should be edited to match the project
REM * target and the compiler location. 
REM *
REM ****************************************************************************

REM Specify compiler home directory
SET TLLIB_H="C:\Program Files (x86)\GNU Tools ARM Embedded\5.4 2016q3"
REM SET TLLIB_H="C:\nxp\ARM_GCC_5_4-2016q3"

REM Specify the target
SET TLLIB_T=cm3
SET TLLIB_T_HW=cm3
SET TLLIB_T_COMP=gnu
SET TLLIB_T_OS=FRE09M3
SET TLLIB_HW=LPC1768
SET TLLIB_F=LPC17xx

REM Specify the build type (DEBUG or RELEASE)
set TLLIB_B=RELEASE

rem Specify the output format
rem The cm3 target can have the following output formats:
rem cm3 	= Compile using Yagarto toolchain (arm-elf)
rem aeabi 	= Compile using LPCXpresso toolchain (arm-none-eabi)
SET TLLIB_OF=aeabi
SET TLLIB_O=o_aeabi

set INC_DIRS=-I%CD%\..\blocks\Src\Lib -I%CD%\..\blocks\Src\Lib\NanoPB

REM * The variables below does not need to be altered **************************

echo.
echo --------------------------------------------------------------------------------

if not exist project.c (
	REM Not in exe directory directory
	echo ERROR: The batch should be run from the exe directory.
	goto end
)

ECHO Setting up environment
ECHO.

REM Target dependant environment variables
SET path=%TLLIB_H%\bin;%CD%\..\Platform\Tools;%CD%\Bin;%path%

REM The rest is common to all targets

ECHO INFO: Compiler dir:     %TLLIB_H%
ECHO       Target:           %TLLIB_T% (%TLLIB_HW%)
ECHO       Build:            %TLLIB_B%

:end
ECHO --------------------------------------------------------------------------------
