/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		MPAccess\local.h
*
*	\ingroup	MPACCESS
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	02-11-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef PROJ_LOCAL_H_INCLUDED
#define PROJ_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/**
 *	Generates a delay with volatile loop. To be used with debug functions only.
 */

#define project__delay(c_)												\
{																		\
	volatile Uint32 cnt = c_;											\
	while(cnt--);														\
}

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void				project__disablePLL(void);
extern void				project__configurePLL(void);
extern void				project__enterSleepMode(void);
extern void				project__configHW(void);
extern void				project__errorBlinker(Uint8);
extern void				project__startSystemTick(void);
extern void				project__stopSystemTick(void);

extern void				project__displayDataCommand(Boolean);
extern void				project__displayBacklight(Boolean);

extern void				project__setWpState(Boolean);
extern void 			project__addMainStackwatch();

extern void				project_checkBootButtons(void);

extern					osa_isrOSDecl(project_swTimerIsr);


extern void				project_log(Uint16);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif


