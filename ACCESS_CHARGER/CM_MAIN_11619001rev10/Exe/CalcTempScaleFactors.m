## Copyright (C) 2010 nicka
## 
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <http://www.gnu.org/licenses/>.

## KTY83_120

## Author: nicka <nicka@MICROPOWER41>
## Created: 2010-11-09

function CalcTempScaleFactors(type="NTC10k")

switch type
case "KTY83_120"
	temperatures = 273.15 + [-55 -50 -40 -30 -20 -10 0 10 20 25 30 40 50 60 70 80 90 100 110 120 125 130 140 150 160 170 175];
	R_PTC = [500 525 577 632 691 754 820 889 962 1000 1039 1118 1202 1288 1379 1472 1569 1670 1774 1882 1937 1993 2107 2225 2346 2471 2535];

	R = 10e3;
	P = polyfit(R_PTC(5:end-5)/R, temperatures(5:end-5), 2);

	A = P(3);
	B = P(2);
	C = P(1);

	figure
	plot(R_PTC, temperatures - polyval(P, R_PTC/R))
	title("Error between datasheet and calculated")
	legend("temperatures - calculated")
	xlabel("log(R_{PTC})")
	ylabel("Temperature degree Celsius")

	figure
	plot(R_PTC, temperatures-273.15, [50 R_PTC 3000 3500 4000 5000], polyval(P, [50 R_PTC 3000 3500 4000 5000]/R)-273.15)
	title("Calculated as in software")
	legend("temperatures", "calculated")
	xlabel("R_{PTC}")
	ylabel("Temperature degree Celsius")
	text(600, -50, sprintf("R = 10kohm\nT = %f + %f*ln(R_{PTC}/R) + %f*ln(R_{PTC}/R)*ln(R_{PTC}/R)", A, B, C))
case "NTC10k"
	temperatures = 273.15 + [-40 -35 -30 -25 -20 -15 -10 -5 0 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 105 110 115 120 125];
	R_NTC = [336450 242660 176960 130410 97072 72951 55326 42326 32650 25391 19899 15711 12492 10000 8056.8 6531.4 5326.4 4368.4 3602.3 2986.1 2487.8 2082.7 1751.6 1479.3 1255.2 1069.7 915.43 786.63 678.6 587.58 510.59 445.24 389.56 341.92];

	R_NTC_log = log(R_NTC);
	P = polyfit(R_NTC_log, 1./temperatures, 2);

	figure
	plot(R_NTC./10e3, temperatures-273.15, R_NTC./10e3, 1./polyval(P, R_NTC_log)-273.15)
	title("Comparison between from datasheet and calculated")
	legend("temperatures", "calculated")
	xlabel("R_{NTC}")
	ylabel("Temperature degree Celsius")
	text(1000, 0, sprintf("1/T = %f*(log(R_NTC))^2 + %f*(log(R_NTC)) + %f", P(1), P(2), P(3)))

	figure
	plot(R_NTC_log, temperatures-273.15, R_NTC_log, 1./polyval(P, R_NTC_log)-273.15)
	title("Logarithm of R_{NTC}")
	legend("temperatures", "calculated")
	xlabel("log(R_{NTC})")
	ylabel("Temperature degree Celsius")

	figure
	plot(R_NTC, temperatures - 1./polyval(P, R_NTC_log))
	title("Error between datasheet and calculated")
	legend("temperatures - calculated")
	xlabel("log(R_{NTC})")
	ylabel("Temperature degree Celsius")


	% Transformation so that different resistors could be used
	%
	% TempSensor_5V_Ntc :
	%   1/T = A + B*ln(R_NTC) + C*ln(R_NTC)*ln(R_NTC) Steinhart-Hart equation
	%   measured is R_NTC/R insert this into equation and compensate
	%   1/T = A + B*(ln(R_NTC/R) + ln(R)) + C*(ln(R_NTC/R) + ln(R))^2 = A + B*(ln(R_NTC) + ln(R))* + C*(ln(R_NTC)^2 + 2*ln(R)*ln(R_NTC) + ln(R)*ln(R)) =
	%   (A + B*ln(R) + C*ln(R)*ln(R)) + (B + 2*C*ln(R))*ln(R_NTC) + C*ln(R_NTC)*ln(R_NTC)
	%
	%   It should possible to fit this to any combination of R and NTC thermistor as long as the voltage is 5 volt
	R = 10e3;
	A = P(3);
	B = P(2);
	C = P(1);
	lnR = log(R);

	A_prim = (A + B*lnR + C*lnR*lnR);
	B_prim = (B + 2*C*lnR);
	measured = log([R_NTC 300 250 200 150 100 50 25]/R);
	temp_software = 1./(A_prim + B_prim*measured + C*measured.^2);
	figure
	plot(R_NTC, temperatures-273.15, [R_NTC 300 250 200 150 100 50 25], temp_software-273.15)
	title("Calculated as in software")
	legend("temperatures", "calculated")
	xlabel("R_{NTC}")
	ylabel("Temperature degree Celsius")
	text(50000, 80, sprintf("1/T = (A + B*ln(R) - C*ln(R)*ln(R)) + (B + 2*C*ln(R))*ln(R_{NTC}/R) + C*ln(R_{NTC}/R)*ln(R_{NTC}/R) =\n(%f + %f*ln(R) - %f*ln(R)*ln(R)) + (%f + 2*%f*ln(R))*ln(R_{NTC}/R) + %f*ln(R_{NTC}/R)*ln(R_{NTC}/R) =\n%f + %f*ln(R_{NTC}/R) + %f*ln(R_{NTC}/R)*ln(R_{NTC}/R)", A, B, C, B, C, C, A_prim, B_prim, C))
case "Board_NTC10k"
	temperatures = 273.15 + [-40 -35 -30 -25 -20 -15 -10 -5 0 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100 105 110 115 120 125];
	R_NTC = [336450 242660 176960 130410 97072 72951 55326 42326 32650 25391 19899 15711 12492 10000 8056.8 6531.4 5326.4 4368.4 3602.3 2986.1 2487.8 2082.7 1751.6 1479.3 1255.2 1069.7 915.43 786.63 678.6 587.58 510.59 445.24 389.56 341.92];

	R_NTC_log = log(R_NTC);
	P = polyfit(R_NTC_log, 1./temperatures, 2);

	figure
	plot(R_NTC, temperatures-273.15, R_NTC, 1./polyval(P, R_NTC_log)-273.15)
	title("Comparison between from datasheet and calculated")
	legend("temperatures", "calculated")
	xlabel("R_{NTC}")
	ylabel("Temperature degree Celsius")
	text(1000, 0, sprintf("1/T = %f*(log(R_NTC))^2 + %f*(log(R_NTC)) + %f", P(1), P(2), P(3)))

	figure
	plot(R_NTC_log, temperatures-273.15, R_NTC_log, 1./polyval(P, R_NTC_log)-273.15)
	title("Logarithm of R_{NTC}")
	legend("temperatures", "calculated")
	xlabel("log(R_{NTC})")
	ylabel("Temperature degree Celsius")

	figure
	plot(R_NTC, temperatures - 1./polyval(P, R_NTC_log))
	title("Error between datasheet and calculated")
	legend("temperatures - calculated")
	xlabel("log(R_{NTC})")
	ylabel("Temperature degree Celsius")


	% Transformation so that different resistors could be used
	%
	% TempSensor_5V_Ntc :
	%   1/T = A + B*ln(R_NTC) + C*ln(R_NTC)*ln(R_NTC) Steinhart-Hart equation
	%   measured is R_NTC/R insert this into equation and compensate
	%   1/T = A + B*(ln(R_NTC/R) + ln(R)) + C*(ln(R_NTC/R) + ln(R))^2 = A + B*(ln(R_NTC) + ln(R))* + C*(ln(R_NTC)^2 + 2*ln(R)*ln(R_NTC) + ln(R)*ln(R)) =
	%   (A + B*ln(R) + C*ln(R)*ln(R)) + (B + 2*C*ln(R))*ln(R_NTC) + C*ln(R_NTC)*ln(R_NTC)
	%
	%   It should possible to fit this to any combination of R and NTC thermistor as long as the voltage is 5 volt
	R = 10e3;
	A = P(3);
	B = P(2);
	C = P(1);
	lnR = log(R);

	A_prim = (A + B*lnR + C*lnR*lnR);
	B_prim = (B + 2*C*lnR);
	measured = log([R_NTC 300 250 200 150 100 50 25]/R);
	temp_software = 1./(A_prim + B_prim*measured + C*measured.^2);
	figure
	plot(R_NTC, temperatures-273.15, [R_NTC 300 250 200 150 100 50 25], temp_software-273.15)
	title("Calculated as in software")
	legend("temperatures", "calculated")
	xlabel("R_{NTC}")
	ylabel("Temperature degree Celsius")
	text(50000, 80, sprintf("1/T = (A + B*ln(R) - C*ln(R)*ln(R)) + (B + 2*C*ln(R))*ln(R_{NTC}/R) + C*ln(R_{NTC}/R)*ln(R_{NTC}/R) =\n(%f + %f*ln(R) - %f*ln(R)*ln(R)) + (%f + 2*%f*ln(R))*ln(R_{NTC}/R) + %f*ln(R_{NTC}/R)*ln(R_{NTC}/R) =\n%f + %f*ln(R_{NTC}/R) + %f*ln(R_{NTC}/R)*ln(R_{NTC}/R)", A, B, C, B, C, C, A_prim, B_prim, C))
otherwise
	error("Usage: KTY83_120(""KTY83_120""|""NTC10k""|""Board_NTC10k"")")
endswitch

endfunction
