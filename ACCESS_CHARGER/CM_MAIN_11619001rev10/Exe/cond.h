/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		cond.h
*
*	\ingroup	MPACCESS
*
*	\brief		Menu visibility condition handlers.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 439 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "CAN.h"
#include "Chalg.h"
#include "dpl.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Boolean project__cndNone(void);
PRIVATE Boolean project__cndLoggedIn(void);

PRIVATE Boolean project__cndJoinedNetwork(void);

PRIVATE Boolean project__cndRemoteOutPhase(void);
PRIVATE Boolean project__cndRemoteInPause(void);

PRIVATE Boolean project__cndChalgEnabled(void);
PRIVATE Boolean project__cndCANOn(void);
PRIVATE Boolean project__cndCANMaster(void);
PRIVATE Boolean project__cndCANSlave(void);
PRIVATE Boolean project__cndCANMasterExt(void);
PRIVATE Boolean project__cndCANNodeIdAuto(void);
PRIVATE Boolean project__cndCANNodeIdSet(void);

PRIVATE Boolean project__cndRemoteOutWater(void);
PRIVATE Boolean project__cndF1Duration(void);
PRIVATE Boolean project__cndF2Duration(void);
PRIVATE Boolean project__cndSTOPDuration(void);

PRIVATE Boolean project__cndNetworkConfAuto(void);

PRIVATE Boolean project__cndCycleLogNotEmpty(void);
PRIVATE Boolean project__cndCycleLogHasAlarms(void);
PRIVATE Boolean project__cndCurrentChargeHasAlarms(void);

PRIVATE Boolean project__cndBmConnected(void);
PRIVATE Boolean project__cndCharging(void);

PRIVATE Boolean project__cndFirmwareVersionAvailable(void);
PRIVATE Boolean project__cndTimeRestriction(void);

PRIVATE Boolean project__cndRadioOnAndNotJoined(void);
PRIVATE Boolean project__cndRadioDisabled(void);

PRIVATE Boolean project__cndChargingModeBm(void);
PRIVATE Boolean project__cndLogBmConnected(void);
PRIVATE Boolean project__cndParallelControl(void);
PRIVATE Boolean project__cndRemoteOutAir(void);
PRIVATE Boolean project__cndChargingComplete(void);
PRIVATE Boolean project__cndCycleLogInMem(void);

PRIVATE Boolean project__cndEquDisabled(void);
PRIVATE Boolean project__cndEquWeekDay(void);
PRIVATE Boolean project__cndEquCyclic(void);

PRIVATE Boolean project__cndTestSerial(void);
PRIVATE Boolean project__cndAssertSerial(void);
PRIVATE Boolean project__cndAssertOTA(void);
PRIVATE Boolean project__cndDebugOTA(void);
PRIVATE Boolean project__cndFwModeUsa(void);
PRIVATE Boolean project__cndChalgEnabledUsa(void);
PRIVATE Boolean project__cndChalgEnabledJoined(void);
PRIVATE Boolean project__cndChargingModeBmConnected(void);
PRIVATE Boolean project__cndChargingModeBmNotConnected(void);
PRIVATE Boolean project__cndChargingModeDefDual(void);
PRIVATE Boolean project__cndChargingModeMulti(void);
PRIVATE Boolean project__cndBatteryCellFunc(void);
PRIVATE Boolean project__cndLoggedIn2(void);
PRIVATE Boolean project__cndUserParameters(void);
PRIVATE Boolean project__cndNwkConnected(void);
PRIVATE Boolean project__cndRemoteOutBBC(void);
PRIVATE Boolean project__cndDplMaster(void);
PRIVATE Boolean project__cndDplSlave(void);
PRIVATE Boolean project__cndDplDisplay(void);
PRIVATE Boolean project__cndLoggedIn2Chalg(void);
PRIVATE Boolean project__cndLoggedIn2NoChalg(void);
PRIVATE Boolean project__cndChargingModeDual(void);
//PRIVATE Boolean project__cndRemoteOutIthreshold(void); 	// sorabITR - not now
/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************//** \cond priv_decl *//*
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Condition functions. Order and count must match the GUI_CND_xxx defines
 *	in gui.h.
 */
PRIVATE gui_fnCondition project__navConditions[] =
{
	project__cndNone,
	project__cndLoggedIn,
	project__cndJoinedNetwork,
	project__cndRemoteOutPhase,
	project__cndChalgEnabled,
	project__cndRemoteOutWater,
	project__cndF1Duration,
	project__cndF2Duration,
	project__cndSTOPDuration,
	project__cndNetworkConfAuto,
	project__cndCycleLogNotEmpty,
	project__cndCycleLogHasAlarms,
	project__cndBmConnected,
	project__cndCharging,
	project__cndRemoteInPause,
	project__cndCurrentChargeHasAlarms,
	project__cndFirmwareVersionAvailable,
	project__cndTimeRestriction,
	project__cndRadioDisabled,
	project__cndRadioOnAndNotJoined,
	project__cndChargingModeBm,
	project__cndLogBmConnected,
	project__cndParallelControl,
	project__cndRemoteOutAir,
	project__cndChargingComplete,
	project__cndCycleLogInMem,
	project__cndEquDisabled,
	project__cndEquWeekDay,
	project__cndEquCyclic,
	project__cndTestSerial,
	project__cndAssertSerial,
	project__cndAssertOTA,
	project__cndDebugOTA,
	project__cndCANOn,
	project__cndCANMaster,
	project__cndCANNodeIdAuto,
	project__cndCANNodeIdSet,
	project__cndFwModeUsa,
	project__cndChalgEnabledUsa,
	project__cndChalgEnabledJoined,
	project__cndChargingModeBmConnected,
	project__cndChargingModeBmNotConnected,
	project__cndChargingModeDefDual,
	project__cndChargingModeMulti,
	project__cndBatteryCellFunc,
	project__cndLoggedIn2,
	project__cndUserParameters,
	project__cndNwkConnected,
	project__cndCANSlave,
	project__cndCANMasterExt,
	project__cndRemoteOutBBC,
	project__cndDplMaster,
	project__cndDplSlave,
	project__cndDplDisplay,
	project__cndLoggedIn2Chalg,
	project__cndLoggedIn2NoChalg,
	project__cndChargingModeDual,
	//project__cndRemoteOutIthreshold, 	// sorabITR - not now
};

/** 
 *	Condition functions info.
 */
PUBLIC const_P  gui_ConditionTable project__conditions =
{
	project__navConditions,
	dim(project__navConditions)
};


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Dummy condition, always return TRUE.
*
*	\return		Always TRUE.
*
*	\details		
*
*	\note					
*
******************************************************************************/

PRIVATE Boolean project__cndNone(void) 
{
	return TRUE;
}	

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if user is logged in.
*
*	\return		TRUE if user is logged in.
*
*	\details		
*
*	\note			
*
******************************************************************************/

PRIVATE Boolean project__cndLoggedIn(void) 
{
	Uint8					login;

	reg_get(&login, reg_i_LoginLevel_gui1);

	return (login != GUI_ACCESS_LEVEL0);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if radio network is joined.
*
*	\return		TRUE if radio network is joined.
*
*	\details		
*
*	\note				
*
******************************************************************************/

PRIVATE Boolean project__cndJoinedNetwork(
	void
) {
	Uint8					status;
	Uint8					nwkSettings;

	reg_get(&status, reg_i_nwkStatus_radio1);
	reg_get(&nwkSettings, reg_i_NwkSettings_radio1);

	return (status == RADIO__NWKSTATUS_CONNECTED && nwkSettings != RADIO_NWKSETT_DEFAULT);
}
/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__cndRadioOnAndNotJoined
*
*-------------------------------------------------------------------------*//**
*
*	\brief		True if radio is enabled but no network has been joined.
*
*	\return		TRUE if radio is enabled but no network has been joined.
*
*	\details		
*
*	\note				
*
******************************************************************************/

PRIVATE Boolean project__cndRadioOnAndNotJoined(
	void
) {
	Uint8					mode;
	Uint8					status;
	Uint8					nwkSettings;

	reg_get(&mode, reg_i_RadioMode_radio1);
	reg_get(&status, reg_i_nwkStatus_radio1);
	reg_get(&nwkSettings, reg_i_NwkSettings_radio1);
	
	return (mode != 0 && status == RADIO__NWKSTATUS_NOCONN && nwkSettings != RADIO_NWKSETT_DEFAULT);
}
/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__cndRadioDisabled
*
*-------------------------------------------------------------------------*//**
*
*	\brief		True if radio is disabled.
*
*	\return		TRUE if radio is disabled.
*
*	\details		
*
*	\note				
*
******************************************************************************/

PRIVATE Boolean project__cndRadioDisabled(
	void
) {
	Uint8					mode;

	reg_get(&mode, reg_i_RadioMode_radio1);
	
	return (mode == 0);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if remote out function is 
*				phase.
*
*	\return		TRUE if remote out function is phase.
*
*	\details		
*
*	\note				
*
*******************************************************************************/

PRIVATE Boolean project__cndRemoteOutPhase(void) 
{
	Uint8					status;

	reg_get(&status, reg_i_RemoteOut_func_sup1);

	return (status == SUP_ROFUNC_PHASE);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if CAN communication profile is master.
*
*	\return		TRUE if CAN is in master mode.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndChalgEnabled(void)
{
	Uint8					mode;

	reg_get(&mode, reg_i_CAN_CommProfile_can1);

	return (mode == CAN_COMM_PROFILE_MASTER || mode == CAN_COMM_PROFILE_DISABLED || mode == CAN_COMM_PROFILE_STATUS);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if CAN communication profile is on
*
*	\return		TRUE if any CAN communication profile is on.
*
*	\details		
*
*	\note		
*
*******************************************************************************/
PRIVATE Boolean project__cndCANOn(void)
{
	Uint8					CommunicationProfile;

	reg_get(&CommunicationProfile, reg_i_CAN_CommProfile_can1);

	return (CommunicationProfile != CAN_COMM_PROFILE_DISABLED);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if CAN communication profile is master or master external
*
*	\return		TRUE if CAN is in master mode.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndCANMaster(void)
{
	Uint8					CommunicationProfile;

	reg_get(&CommunicationProfile, reg_i_CAN_CommProfile_can1);

	return (CommunicationProfile == CAN_COMM_PROFILE_MASTER || CommunicationProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if CAN communication profile is slave
*
*	\return		TRUE if CAN is in slave mode.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndCANSlave(void)
{
	Uint8					CommunicationProfile;

	reg_get(&CommunicationProfile, reg_i_CAN_CommProfile_can1);

	return (CommunicationProfile == CAN_COMM_PROFILE_SLAVE);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if CAN communication profile is slave
*
*	\return		TRUE if CAN is in slave mode.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndCANMasterExt(void)
{
	Uint8					CommunicationProfile;

	reg_get(&CommunicationProfile, reg_i_CAN_CommProfile_can1);

	return (CommunicationProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if CAN node ID is set automatically
*
*	\return		TRUE if CAN is in master or slave mode.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndCANNodeIdAuto(void)
{
	Uint8					CommunicationProfile;

	reg_get(&CommunicationProfile, reg_i_CAN_CommProfile_can1);

	return (CommunicationProfile == CAN_COMM_PROFILE_MASTER || CommunicationProfile == CAN_COMM_PROFILE_SLAVE);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if CAN node ID set
*
*	\return		TRUE if CAN is in master external mode.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndCANNodeIdSet(void)
{
	Uint8					CommunicationProfile;

	reg_get(&CommunicationProfile, reg_i_CAN_CommProfile_can1);

	return (CommunicationProfile == CAN_COMM_PROFILE_MASTER_CAN_INPUT || CommunicationProfile == CAN_COMM_PROFILE_STATUS);
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if remote out function is not
*				'No Function'.
*
*	\return		TRUE if Remote out function is water.
*
*	\details		
*
*	\note		
*
*******************************************************************************/
PRIVATE Boolean project__cndRemoteOutWater(void)
{
	Uint8					status;

	reg_get(&status, reg_i_RemoteOut_func_sup1);

	return (status == SUP_ROFUNC_WATER);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if F1 button function is not
*				'No Function'.
*
*	\return		TRUE if CAN is in master mode.
*
*	\details		
*
*	\note		
*
*******************************************************************************/
PRIVATE Boolean project__cndF1Duration(void) 
{
	Uint8					status;

	reg_get(&status, reg_i_ButtonF1_func_sup1);

	return (status != SUP_FFUNC_NO_FUNC);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if F2 button function is not
*				'No Function'.
*
*	\return		TRUE if CAN is in master mode.
*
*	\details		
*
*	\note		
*
*******************************************************************************/
PRIVATE Boolean project__cndF2Duration(void) 
{
	Uint8					status;

	reg_get(&status, reg_i_ButtonF2_func_sup1);

	return (status != SUP_FFUNC_NO_FUNC);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if STOP button function is not
*				'No Function'.
*
*	\return		TRUE if CAN is in master mode.
*
*	\details		
*
*	\note		
*
*******************************************************************************/
PRIVATE Boolean project__cndSTOPDuration(void) 
{
	return TRUE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if radio network configuration
*				is 'auto'.
*
*	\return		TRUE if network config is auto.
*
*	\details		
*
*	\note		
*
*******************************************************************************/
PRIVATE Boolean project__cndNetworkConfAuto(void) 
{
	Uint8					status;

	reg_get(&status, reg_i_NwkSettings_radio1);

	return(
		(status == RADIO_NWKSETT_AUTO) ||
		(status == RADIO_NWKSETT_DEFAULT)
	);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if cycle log is not empty.
*
*	\return		TRUE if cycle log is not empty.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndCycleLogNotEmpty(void) 
{
	Uint32					indexLogEmpty;

//	reg_get(&indexLogEmpty, reg_i_chargeIndexReset_sup1);
	reg_get(&indexLogEmpty, reg_i_histLogIndexReset_cm1);

	return(indexLogEmpty > 0);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if cycle log entry has alarms.
*
*	\return		TRUE if cycle log entry has alarms.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndCycleLogHasAlarms(void) 
{
	Uint8					alarms;

	reg_get(&alarms, reg_i_AlarmCount_cm1);

	return(alarms > 0);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if battery module is connected.
*
*	\return		TRUE if battery module is connected.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndBmConnected(void)
{
	Boolean					Connected;
	Uint16					chalg_status;

	reg_get(&chalg_status, reg_i_ChalgStatus_chalg1);

	Connected = chalg_status & CHALG_STAT_BM_CONNTECTED ? 1 : 0;

	return(Connected == TRUE);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if charging.
*
*	\return		TRUE if charging.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndCharging(void) 
{
	Uint8					supervision_state;

	reg_get(&supervision_state, reg_i_supervisionState_sup1);

	return(
		(supervision_state == SUP_STATE_CHARGING) ||
		(supervision_state == SUP_STATE_CHARGE_ERROR) ||
		(supervision_state == SUP_STATE_PAUSE)
	);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if remote in function = pause.
*
*	\return		TRUE if remote in function = pause.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndRemoteInPause(void) 
{
	Uint8					func;

	reg_get(&func, reg_i_RemoteIn_func_sup1);

	return(func == SUP_RIFUNC_START_STOP);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if current charge has alarms.
*
*	\return		TRUE if current charge has alarms.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndCurrentChargeHasAlarms(void) 
{
	Uint8					alarms;

	reg_get(&alarms, reg_i_AlarmCount_chalg1);

	return(alarms > 0);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Radio firmware is available.
*
*	\return		TRUE if Radio firmware is available.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndFirmwareVersionAvailable(void) 
{
	//Uint8					RadioFwVer;
	// Ari: Register is 32-bit. Using 8-bit variable will corrupt the stack.
	Uint32					RadioFwVer;
	
	reg_get(&RadioFwVer, reg_i_FirmwareVerRF_radio1);

	return(RadioFwVer > 0);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if time restrictions are
*				enabled.
*
*	\return		TRUE if time restrictions are enabled.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean project__cndTimeRestriction(void) 
{
	Uint8					val;

	reg_get(&val, reg_i_TimeRestriction_sup1);

	return(val > 0);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if the charging mode is BM.
*
*	\return		TRUE if the charging mode is BM. FALSE if it is not.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PRIVATE Boolean project__cndChargingModeBm(
	void
) {
	Uint8					val;

	reg_get(&val, reg_i_ChargingMode_default_chalg1);

	return(val == CHALG_BM);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if the charging mode is BM in
*				the log data.
*
*	\return		TRUE if the charging mode is BM in the log data. FALSE if it is
*				"User def".
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PRIVATE Boolean project__cndLogBmConnected(
	void
) {
	Boolean					Connected;
	Uint16					chalg_status;

	reg_get(&chalg_status, reg_i_ChalgStatusSum_cm1);

	Connected = chalg_status & CHALG_STAT_BM_CONNTECTED ? 1 : 0;

	return(Connected == TRUE);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Parallel control is enabled.
*
*	\return		TRUE if CAN is in master mode.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndParallelControl(void)
{
	Uint8					mode;

	reg_get(&mode, reg_i_ParallelControl_func_cc1);

	return (mode ? TRUE : FALSE);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if remote out function is not
*				'No Function'.
*
*	\return		TRUE if Remote out function is air pump.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndRemoteOutAir(void)
{
	Uint8					status;

	reg_get(&status, reg_i_RemoteOut_func_sup1);

	return (status == SUP_ROFUNC_AIR);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if charging completed.
*
*	\return		TRUE if charging completed.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChargingComplete(void)
{
	Uint16 chalgStatus;

	reg_get(&chalgStatus, sup__chalgStatus);

	return(chalgStatus & CHALG_STAT_CHARG_COMPLETED ? 1 : 0);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if cycle log is present in memory.
*
*	\return		TRUE if cycle log is still in flash (not deleted due to cyclic memory).
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndCycleLogInMem(void)
{
	Uint32					ChargeIndexReset;
	Uint16					HistLogInMem;
	Uint32					CycleNo;

//	reg_get(&ChargeIndexReset, reg_i_chargeIndexReset_sup1);
	reg_get(&ChargeIndexReset, reg_i_histLogIndexReset_cm1);
//	reg_get(&HistLogInMem, sup__HistLogInMem);
	reg_get(&HistLogInMem, reg_i_histLogCount_cm1);
	reg_get(&CycleNo, reg_i_cycleNo_cm1);

	return((CycleNo > (ChargeIndexReset - HistLogInMem)) && (ChargeIndexReset > 0));
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Equalize_func = Disabled.
*
*	\return		TRUE if Equalize_func = Disabled.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndEquDisabled(void)
{
	Uint8					Equalize_func;

	reg_get(&Equalize_func, reg_i_Equalize_func_sup1);

	return(Equalize_func == CHALG_EQU_FUNC_DISABLED);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Equalize_func = Cyclic.
*
*	\return		TRUE if Equalize_func = Cyclic.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndEquCyclic(void)
{
	Uint8					Equalize_func;

	reg_get(&Equalize_func, reg_i_Equalize_func_sup1);

	return(Equalize_func == CHALG_EQU_FUNC_CYCLIC);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Equalize_func = Weekday.
*
*	\return		TRUE if Equalize_func = Weekday.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndEquWeekDay(void)
{
	Uint8					Equalize_func;

	reg_get(&Equalize_func, reg_i_Equalize_func_sup1);

	return(Equalize_func == CHALG_EQU_FUNC_WEEKDAY);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Debug_func = TestSerial.
*
*	\return		TRUE if Debug_func = TestSerial.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndTestSerial(void)
{
	Uint8					Debug_func;

	reg_get(&Debug_func, reg_i_RadioDebugFunc_radio1);

	return(Debug_func == RADIO__DEB_TESTSER);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Debug_func = AssertSerial.
*
*	\return		TRUE if Debug_func = AssertSerial.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndAssertSerial(void)
{
	Uint8					Debug_func;

	reg_get(&Debug_func, reg_i_RadioDebugFunc_radio1);

	return(Debug_func == RADIO__DEB_ASSERTSER);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Debug_func = AssertOTA.
*
*	\return		TRUE if Debug_func = AssertOTA.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndAssertOTA(void)
{
	Uint8					Debug_func;

	reg_get(&Debug_func, reg_i_RadioDebugFunc_radio1);

	return(Debug_func == RADIO__DEB_ASSERTOTA);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Debug_func = DebugOTA.
*
*	\return		TRUE if Debug_func = DebugOTA.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndDebugOTA(void)
{
	Uint8					Debug_func;

	reg_get(&Debug_func, reg_i_RadioDebugFunc_radio1);

	return(Debug_func == RADIO__DEB_DEBUGOTA);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if FW_MODE = USA.
*
*	\return		TRUE if Debug_func = DebugOTA.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndFwModeUsa(void)
{
	Uint8					language;

	reg_get(&language, reg_i_LanguageP_gui1);

	return(language == gui_lang_english_us);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Chalg is used and if FW_MODE != USA.
*
*	\return		TRUE if Debug_func = DebugOTA.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChalgEnabledUsa(void)
{
	Boolean					FwModeUSA;
	Boolean 				ChalgEnabled;

	FwModeUSA = project__cndFwModeUsa();
	ChalgEnabled = project__cndChalgEnabled();

	return((ChalgEnabled == TRUE) && (FwModeUSA != TRUE));
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Chalg is used and connected to radio network.
*
*	\return		TRUE if Debug_func = DebugOTA.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChalgEnabledJoined(void)
{
	Uint8					status;
	Boolean					Joined;
	Boolean 				ChalgEnabled;

	reg_get(&status, reg_i_nwkStatus_radio1);

	Joined = (status == RADIO__NWKSTATUS_CONNECTED) ? TRUE:FALSE;
	ChalgEnabled = project__cndChalgEnabled();

	return((ChalgEnabled == TRUE) && (Joined == TRUE));
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if battery module is connected.
*
*	\return		TRUE if battery module is connected.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChargingModeBmConnected(void)
{
	Boolean					Connected;
	Boolean 				ChargingModeBm;
	Uint16					chalg_status;

	reg_get(&chalg_status, reg_i_ChalgStatus_chalg1);

	Connected = chalg_status & CHALG_STAT_BM_CONNTECTED ? 1 : 0;
	ChargingModeBm = project__cndChargingModeBm();
	ChargingModeBm |= project__cndChargingModeDual();

	return((ChargingModeBm == TRUE) && (Connected == TRUE));
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if battery module is NOT connected.
*
*	\return		TRUE if battery module is connected.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChargingModeBmNotConnected(void)
{
	Boolean					Connected;
	Boolean 				ChargingModeBm;
	Uint16					chalg_status;

	reg_get(&chalg_status, reg_i_ChalgStatus_chalg1);

	Connected = chalg_status & CHALG_STAT_BM_CONNTECTED ? 1 : 0;
	ChargingModeBm = project__cndChargingModeBm();

	return((ChargingModeBm == TRUE) && (Connected != TRUE));
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if charging mode == Def or Dual.
*
*	\return		TRUE if charging mode == Def or Dual.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChargingModeDefDual(void)
{
	Uint8					val;
	Uint16					chalg_status;
	Boolean					Connected;

	reg_get(&val, reg_i_ChargingMode_default_chalg1);
	reg_get(&chalg_status, reg_i_ChalgStatus_chalg1);

	Connected = chalg_status & CHALG_STAT_BM_CONNTECTED ? 1 : 0;

	return(val == CHALG_USER_DEF || (val == CHALG_DUAL && Connected == FALSE));
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if charging mode == Multi.
*
*	\return		RUE if charging mode == Multi.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChargingModeMulti(void)
{
	Uint8					val;

	reg_get(&val, reg_i_ChargingMode_default_chalg1);

	return(val == CHALG_MULTI);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if Battery XX cells function is enabled.
*
*	\return		TRUE if Battery XX cells function is enabled.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndBatteryCellFunc(void)
{
	Uint8					function;

	reg_get(&function, reg_i_enabled_tmp_chalg1);

	return (function ? TRUE : FALSE);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if user is logged in level 2.
*
*	\return		TRUE if user is logged in level 2.
*
*	\details
*
*	\note
*
******************************************************************************/

PRIVATE Boolean project__cndLoggedIn2(void)
{
	Uint8					login;

	reg_get(&login, reg_i_LoginLevel_gui1);

	return (login == GUI_ACCESS_LEVEL2);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if
*	\brief		charging mode == Def and
*	\brief		Curve (AlgIdx_default) has valid User parameters.
*
*	\return		See description above.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndUserParameters(void)
{
	Uint8					bitConfig;

	reg_get(&bitConfig, reg_i_BitConfig_chalg1);

	if(bitConfig & CHALG_BITCFG_USER_P)
	{
		Uint16					algNo;
		Uint16					algIdx;
		Uint8					chargingMode;
		Uint8					userParamIsValid = FALSE;
		Uint8					chargingModeIsValid = FALSE;

		reg_get(&chargingMode, reg_i_ChargingMode_default_chalg1);

		if(chargingMode == CHALG_USER_DEF){
			reg_get(&algIdx, reg_i_algIdx_default_chalg1);
			chargingModeIsValid = TRUE;
		}
		else if(chargingMode == CHALG_BM){
			if( project__cndChargingModeBmConnected()){
				reg_get(&algIdx, reg_i_algIdx_bm_chalg1);
				chargingModeIsValid = TRUE;
			}
		}
		else if(chargingMode == CHALG_DUAL){
			if( project__cndChargingModeBmConnected()){
				reg_get(&algIdx, reg_i_algIdx_bm_chalg1);
			}
			else{
				reg_get(&algIdx, reg_i_algIdx_default_chalg1);
			}
			chargingModeIsValid = TRUE;
		}

		if(chargingModeIsValid == TRUE){
			algNo = chalg_GetAlgNo(algIdx);
			userParamIsValid = chalg_UserParamIsValid(algNo);
		}

		return(userParamIsValid == TRUE);
	}
	else
	{
		return FALSE;
	}
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		True if connected to network.
*
*	\return		TRUE if connected to network.
*
*	\details
*
*	\note
*
******************************************************************************/

PRIVATE Boolean project__cndNwkConnected(
	void
) {
	Uint8					status;

	reg_get(&status, reg_i_nwkStatus_radio1);

	return (status == RADIO__NWKSTATUS_CONNECTED);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if remote out function is not
*				'No Function'.
*
*	\return		TRUE if Remote out function is BBC.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndRemoteOutBBC(void)
{
	Uint8					status;

	reg_get(&status, reg_i_RemoteOut_func_sup1);

	return (status == SUP_ROFUNC_BBC);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if PowerGroup_func == Master.
*
*	\return		TRUE if PowerGroup_func == Master.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndDplMaster(void)
{
	Uint8					function;

	reg_get(&function, reg_i_PowerGroup_func_regu1);

	return (function == DplMaster);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if PowerGroup_func == Slave.
*
*	\return		TRUE if PowerGroup_func == Slave.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndDplSlave(void)
{
	Uint8					function;

	reg_get(&function, reg_i_PowerGroup_func_regu1);

	return (function == DplSlave);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if connected to network and CAN_CommProfile != Slave.
*
*	\return		if connected to network and CAN_CommProfile != Slave.
*
*	\details
*
*	\note
*
*******************************************************************************/
PRIVATE Boolean project__cndDplDisplay(void)
{
	Uint8 bitConfig;
	Boolean dplMode;
	Boolean connected;
	Boolean canSlave;

	reg_get(&bitConfig,	chalg__BitConfig);

	dplMode = (bitConfig & CHALG_BITCFG_DPL) ? TRUE : FALSE;
	connected = project__cndNwkConnected();
	canSlave = project__cndCANSlave();

	return (dplMode && connected && !canSlave);
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if user is logged in level 2
*				and source is charging algorithm.
*
*	\return		TRUE if user is logged in level 2 and ChalgEnabled.
*
*	\details
*
*	\note
*
******************************************************************************/

PRIVATE Boolean project__cndLoggedIn2Chalg(void)
{
	Uint8					login;
	Boolean 				ChalgEnabled;

	reg_get(&login, reg_i_LoginLevel_gui1);
	ChalgEnabled = project__cndChalgEnabled();

	return ((login == GUI_ACCESS_LEVEL2) && (ChalgEnabled == TRUE));
}

/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if user is logged in level 2
*				and source is NOT charging algorithm.
*
*	\return		TRUE if user is logged in level 2 and ChalgEnabled == FALSE.
*
*	\details
*
*	\note
*
******************************************************************************/

PRIVATE Boolean project__cndLoggedIn2NoChalg(void)
{
	Uint8					login;
	Boolean 				ChalgEnabled;

	reg_get(&login, reg_i_LoginLevel_gui1);
	ChalgEnabled = project__cndChalgEnabled();

	return ((login == GUI_ACCESS_LEVEL2) && (ChalgEnabled == FALSE));
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if charging mode == Dual.
*
*	\return		TRUE if charging mode == Dual.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/
PRIVATE Boolean project__cndChargingModeDual(void)
{
	Uint8					val;

	reg_get(&val, reg_i_ChargingMode_default_chalg1);

	return(val == CHALG_DUAL);
}

/******************************************************************************* sorabITR - not now
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Condition that evaluates to TRUE if remote out function is not
*				'No Function'.
*
*	\return		TRUE if Remote out function is I threshold.
*
*	\details
*
*	\note
*
*******************************************************************************/
/*PRIVATE Boolean project__cndRemoteOutIthreshold(void) // sorabITR - not now
{
	Uint8					status;

	reg_get(&status, reg_i_RemoteOut_func_sup1);

	return (status == SUP_ROFUNC_ITHRESHOLD);
}*/

