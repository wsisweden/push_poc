/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[X]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/proj_hw.c
*
*	\ingroup	MPACCESS_CM3
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	\$Rev: 1701 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MPACCESS_CM3		Cortex-M3
*
*	\ingroup	MPACCESS
*
*	\brief		LPC1765 specific project code.
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "rtc.h"
#include "hw.h"
#include "nxp.h"
#include "flmem.h"
#include "uart.h"
#include "deb.h"
#include "Regu.h"
#include "pwm.h"
#include "osa.h"

#include "global.h"

#include "a25lx.h"

#include "local.h"

#include "Trace/trcRecorder.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#if PROJECT_CPU_HZ == 36000000UL
/**
 *	PLL settings.
 *	Fcck = 36Mhz, Fosc = 288Mhz, and USB 48Mhz 
 */

/*@{*/
#define PLL_MValue					11	/**< M Value - 1					*/
#define PLL_NValue					0	/**< N Value - 1					*/
#define CCLKDivValue				7	/**< Div Value	7 = 36MHz, 5 = 48MHz	*/
#define USBCLKDivValue				6	/**< USB Div Value					*/
/*@}*/

#elif PROJECT_CPU_HZ == 48000000UL
/**
 *	PLL settings.
 *	Fcck = 36Mhz, Fosc = 288Mhz, and USB 48Mhz
 */

/*@{*/
#define PLL_MValue					11	/**< M Value - 1					*/
#define PLL_NValue					0	/**< N Value - 1					*/
#define CCLKDivValue				5	/**< Div Value	7 = 36MHz, 5 = 48MHz	*/
#define USBCLKDivValue				6	/**< USB Div Value					*/
/*@}*/


#else

#error "Recalculate PLL values!"

#endif

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_configHW
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configures HW settings at startup
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__configHW(
	void
) {
	/*	
	 *  Configure PLL
	 */

	project__configurePLL();

#ifndef NDEBUG
	/*
	 * The program must wait a while for debugging to work.
	 */

	//project__delay(0x00400000);

#endif

	/*
	 *  Ethernet block power must be enabled if ethernet RAM is to be used
	 *	(as per errdata).
	 */

	SC->PCONP |= (1<<30);

	/*	
	 *  Configure timer to to generate 1ms interrupts.
	 *	The SysTick interrupt priority must be the same as all other interrupts.
	 */

	SysTick->CTRL = 0;
	NVIC_SetPriority(SysTick_IRQn, configMAX_SYSCALL_INTERRUPT_PRIORITY);
	SysTick->LOAD = (PROJECT_CPU_HZ / 1000UL) - 1;
	SysTick->CTRL = (1<<1)|(1<<2);

#if PROJECT_MAM_ENABLE == 1
	/*
	 *	Flash accelerator
	 */
	#if PROJECT_CPU_HZ <= 20000000UL
	
		SC->FLASHCFG = 0x00000000;
		
	#elif PROJECT_CPU_HZ <= 40000000UL
	
		SC->FLASHCFG = (1<<12);	
		
	#elif PROJECT_CPU_HZ <= 60000000UL
	
		SC->FLASHCFG = (1<<13);		
		
	#elif PROJECT_CPU_HZ <= 80000000UL
	
		SC->FLASHCFG = (1<<12)|(1<<13);				
		
	#else
	
		SC->FLASHCFG = (1<<14);					
		
	#endif
#else

	SC->FLASHCFG = (1<<12)|(1<<14);					
	
#endif

	/*	
	 *  Configure debug message UART
	 */


	PINCON->PINSEL0 |= (1<<20) | (1<<22);		/* RxD2 and TxD2					*/
	SC->PCONP |= (1<<24);					/* Enable power to UART2			*/
	U2IER = 0x00;						/* disable all interrupts			*/
	U2LCR = (1<<0) | (1<<1) | (1<<7);	/* 8 data, enable latch				*/
	U2DLL = (Uint8)59;					/* set for baud low byte			*/
	U2DLM = (Uint8)0;					/* set for baud high byte			*/
	U2LCR &= ~(1<<7);					/* Disable latch					*/
	U2FCR = 1;							/* Enable FIFO						*/

	/* Display backlight pin */
	PINCON->PINSEL0 &= ~((1<<16)|(1<<17));
	FIO2SET |= (1<<8);
	FIO2DIR |= (1<<8);

	//project_monUartInst_p = uart2_interface.init(&project_uart2Config, NULL);

	/* Display backlight pin */
	PROJECT_REG_DIR_DISP_BL |= (1<<PROJECT_PIN_DISP_BACKLIGHT);

	/* Display command / data pin */
	PROJECT_REG_DIR_DISP_COMM |= (1<<PROJECT_PIN_DISP_COMMAND);
	
	/* Initialize UART */
	PINCON->PINSEL0 |= (1<<4) | (1<<6);

	/* Configuring I2C pins for NFC */
	LPC_PINCON->PINSEL1 |= (1<<6)|(1<<7)|(1<<8)|(1<<9);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__enterSleepMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enter sleep mode.
*
*	\return		-
*
*	\details	This function will set the microcontroller in deep-sleep mode 
*				and return when the next RTC clock interrupt occurs.
*
*	\note		
*
*******************************************************************************/

PUBLIC void project__enterSleepMode()
{
	SCB->SCR = SCR_SLEEPDEEP;
	SC->PCON = ((0<<1)|(1<<0));
	__WFI();							/* Enter sleep */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__disablePLL
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Disconnect and disable PLL.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__disablePLL(
	void
) {

	if (PLLSTAT & (1<<25))
	{
		/*
		 * Disconnect PLL
		 */

		PLLCON = 0;
		PLLFEED = 0xaa;
		PLLFEED = 0x55;

		/* Wait for PLL to disconnect */
		while (PLLSTAT & (1<<25));
	}

	if (0)//PLLSTAT & (1<<24))
	{
		/*
		 * Turn PLL off
		 */

		PLLCON = 0;
		PLLFEED = 0xaa;
		PLLFEED = 0x55;

		/* Wait for PLL to shut down */
		while (PLLSTAT & (1<<24));
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__configurePLL
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configures main clock
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__configurePLL(
	void
) {
	Uint32					MValue;
	Uint32					NValue;

	project__disablePLL();

	/* Enable main OSC and wait until it is usable */
	SC->SCS |= 0x20;
	while(!(SC->SCS & 0x40));

	/* Select main OSC, 12MHz, as the PLL clock source */
	SC->CLKSRCSEL = 0x1;

	PLLCFG = PLL_MValue | (PLL_NValue << 16);
	PLLFEED = 0xaa;
	PLLFEED = 0x55;

	/* Enable PLL (still disconnected) */
	PLLCON = 1;							
	PLLFEED = 0xaa;
	PLLFEED = 0x55;

	/* Set clock divider */
	SC->CCLKCFG = CCLKDivValue;

	/* Wait until PLL is locked onto the requested frequency */
	while (((PLLSTAT & (1 << 26)) == 0));

	MValue = PLLSTAT & 0x00007FFF;
	NValue = (PLLSTAT & 0x00FF0000) >> 16;

	while ((MValue != PLL_MValue) && ( NValue != PLL_NValue) );

	/* Connect PLL */
	PLLCON = 3;							
	PLLFEED = 0xaa;
	PLLFEED = 0x55;

	/* Wait until PLL is connected */
	while ( ((PLLSTAT & (1 << 25)) == 0) );	
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__errorBlinker
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Halt application and blink the red LED.
*
*	\param		nCycle		Number of times the LED should blink.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC inline void project__errorBlinker(
	Uint8					nCycle
) {
	Uint32					i;

	vTraceStop();

	/*
	 *	Disabling all interrupts here to make sure no other parts of the code
	 *	are executed. This makes sure the power output is not enabled after it
	 *	has been turned off here.
	 */

	DISABLE;

	REGU_CHARGING_ON_DO_INIT();
	REGU_CHARGING_ON_DO_OFF();
 	pwm_setOutputCycle(PWM_OUTPUT_CHARGE_CONTROL, 0);

	PINCON->PINSEL3 &= ~((1<<10) | (1<<11));

	FIO1DIR |= (1<<21);			/* Define LED-Pin as output			*/

	while (1)
	{
#if PROJECT_USE_WDOG != 0
		nxp_wdogKick();
#endif	

		FIO1CLR = (1<<21);
		project__delay(0x00120000);

		for (i = 0; i < nCycle; i++)
		{
			FIO1SET = (1<<21);
			project__delay(0x00040000);

			FIO1CLR = (1<<21);
			project__delay(0x00080000);
		}

		project__delay(0x00070000);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__displayDataCommand
*
*--------------------------------------------------------------------------*//**
*
*	\brief		project__displayDataCommand
*
*	\param		command		
*
*	\return		-
*
*	\details	Sets command / data selection of the display.
*
*	\note
*
*******************************************************************************/

PUBLIC void project__displayDataCommand(
	Boolean					command
) {
	if (command)
	{
		PROJECT_REG_CLR_DISP_COMM = (1<<PROJECT_PIN_DISP_COMMAND);
	}
	else
	{
		PROJECT_REG_SET_DISP_COMM = (1<<PROJECT_PIN_DISP_COMMAND);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__displayBacklight
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets display backlight on /off.
*
*	\param		on		If TRUE backlight will be set on else off.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void project__displayBacklight(
	Boolean					on
) {
	if (on)
	{
		PROJECT_REG_SET_DISP_BL = (1<<PROJECT_PIN_DISP_BACKLIGHT);
	}
	else
	{
		PROJECT_REG_CLR_DISP_BL = (1<<PROJECT_PIN_DISP_BACKLIGHT);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_log
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Output debug message character.
*
*	\param		character	The character to output.
*
*	\return		-
*
*	\details	This function is called by deb_log(). It can be configured to
*				write the character to UART.
*
*	\note
*
*******************************************************************************/

PUBLIC void project_log(
	Uint16					character
) {

	if (character & DEB_LOG_LAST)
	{
		/*
		 * Last character in a message. Send row change characters.
		 */

		while (!(U2LSR & (1<<5)));
		U2THR = '\r';
		while (!(U2LSR & (1<<5)));
		U2THR = '\n';
	}
	else if (character & DEB_LOG_FIRST)
	{

	}
	else 
	{
		/*	
		 *  Wait for previous byte to be sent and then queue the next byte.
		 */

		while (!(U2LSR & (1<<5)));
		U2THR = (Uint8) character;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__startSystemTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start systick timer.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__startSystemTick()
{
	SysTick->CTRL |= (1<<0);	
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__stopSystemTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Stops systick timer.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__stopSystemTick()
{
	SysTick->CTRL &= ~(1<<0);	
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_setWpState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets or clears flash memory write protection pin.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__setWpState(
	Boolean					wpHigh
) {
	if (wpHigh)
	{
		FIO0SET = (1 << 21);
	}
	else
	{
		FIO0CLR = (1 << 21);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project_checkBootButtons
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check if boot action buttons are pressed during boot.
*
*	\return		-
*
*	\details	This function should be called before OSA is started. It checks
*				if a boot action button is pressed during boot.
*
*				One boot action could be that settings should be reset to 
*				factory defaults.
*
*	\note
*
*******************************************************************************/

PUBLIC void project_checkBootButtons(
	void
) {
	if not(FIO1PIN & (1<<PROJECT_HWP1_F1))
	{
		/*
		 * F1 button pressed.
		 */
		
//		flmem_setFlashFormat(1);
	}


}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	project__addMainStackwatch
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Add the main stack to the stack watch.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void project__addMainStackwatch(
	void
) {
	/*
	 * _vMainStackEnd is defined in the linker script. 0x200 is reserved for
	 * main stack in the linker script.
	 */

	extern unsigned long _vMainStackEnd;

	osa_diagAddStackWatch(NULL, &_vMainStackEnd, 0x200);
}
