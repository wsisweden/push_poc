/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		startup.C
*
*	\ingroup	MPACCESS_CM3
*
*	\brief		Startup code and exception handlers.
*
*	\details
*
*	\note
*
*	\version	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "..\local.h"
#include "global.h"
#include "nxp.h"
#include "sup.h"
/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#define WEAK __attribute__ ((weak))
#define ALIAS(f) __attribute__ ((weak, alias (#f)))

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void stackOverflowHook(sys_FatalErrorType errorType,void * pErrData);

void			Reset_Handler(void);
PRIVATE void	NMI_Handler(void);
PRIVATE void	HardFault_Handler( void ) __attribute__( ( naked ) );
PRIVATE void	MemManage_Handler(void);
PRIVATE void	BusFault_Handler(void);
PRIVATE void	UsageFault_Handler(void);
PRIVATE void	DebugMon_Handler(void);

void WDT_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER0_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER1_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER2_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER3_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART0_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART1_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART2_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART3_IRQHandler(void) ALIAS(IntDefaultHandler);
void PWM1_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C0_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C1_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C2_IRQHandler(void) ALIAS(IntDefaultHandler);
void SPI_IRQHandler(void) ALIAS(IntDefaultHandler);
void SSP0_IRQHandler(void) ALIAS(IntDefaultHandler);
void SSP1_IRQHandler(void) ALIAS(IntDefaultHandler);
void PLL0_IRQHandler(void) ALIAS(IntDefaultHandler);
void RTC_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT0_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT1_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT2_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT3_IRQHandler(void) ALIAS(IntDefaultHandler);
void ADC_IRQHandler(void) ALIAS(IntDefaultHandler);
void BOD_IRQHandler(void) ALIAS(IntDefaultHandler);
void USB_IRQHandler(void) ALIAS(IntDefaultHandler);
void CAN_IRQHandler(void) ALIAS(IntDefaultHandler);
void DMA_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2S_IRQHandler(void) ALIAS(IntDefaultHandler);
void ENET_IRQHandler(void) ALIAS(IntDefaultHandler);
void RIT_IRQHandler(void) ALIAS(IntDefaultHandler);
void MCPWM_IRQHandler(void) ALIAS(IntDefaultHandler);
void QEI_IRQHandler(void) ALIAS(IntDefaultHandler);
void PLL1_IRQHandler(void) ALIAS(IntDefaultHandler);
void SVC_IRQHandler(void) ALIAS(IntDefaultHandler);
void PENDSV_IRQHandler(void) ALIAS(IntDefaultHandler);
void SysTick_IRQHandler(void) ALIAS(IntDefaultHandler);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

extern void xPortSysTickHandler(void);
extern void xPortPendSVHandler(void);
extern void vPortSVCHandler( void );
extern void vEMAC_ISR( void );

extern WEAK void __main(void);
extern WEAK void main(void);

extern void* _vStackTop;
extern unsigned long _vMainStackEnd;

extern unsigned long _etext;
extern unsigned long _data;
extern unsigned long _edata;
extern unsigned long _bss;
extern unsigned long _ebss;

extern void project__configHW(void);

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

static osa_TaskType * bad_task_handle;
static char const_P * bad_task_name;

/** 
 *	Vector table
 */
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) =
{
	/* CM3 core */
	(void *)&_vStackTop,					/* The initial stack pointer	*/
	Reset_Handler,							/* The reset handler			*/
	NMI_Handler,							/* The NMI handler				*/
	HardFault_Handler,						/* The hard fault handler		*/
	MemManage_Handler,						/* The MPU fault handler		*/
	BusFault_Handler,						/* The bus fault handler		*/
	UsageFault_Handler,						/* The usage fault handler		*/
	0,										/* Reserved						*/
	0,										/* Reserved						*/
	0,										/* Reserved						*/
	0,										/* Reserved						*/
	vPortSVCHandler,                        /* SVCall handler (SVC_IRQHandler)	*/
	DebugMon_Handler,						/* Debug monitor handler		*/
	0,										/* Reserved						*/
	xPortPendSVHandler,                     /* The PendSV handler (PENDSV_IRQHandler)	*/
	project_swTimerIsr,						/* The SysTick handler (SysTick_IRQHandler)	*/

	/* LPC17xx  */
	WDT_IRQHandler,							/* 16, WDT						*/
	TIMER0_IRQHandler,						/* 17, TIMER0					*/
	TIMER1_IRQHandler,						/* 18, TIMER1					*/
	TIMER2_IRQHandler,						/* 19, TIMER2					*/
	TIMER3_IRQHandler,						/* 20, TIMER3					*/
	UART0_IRQHandler,						/* 21, UART0					*/
	UART1_IRQHandler,						/* 22, UART1					*/
	UART2_IRQHandler,						/* 23, UART2					*/
	UART3_IRQHandler,						/* 24, UART3					*/
	PWM1_IRQHandler,						/* 25, PWM1						*/
	I2C0_IRQHandler,						/* 26, I2C0						*/
	I2C1_IRQHandler,						/* 27, I2C1						*/
	I2C2_IRQHandler,						/* 28, I2C2						*/
	SPI_IRQHandler,							/* 29, SPI						*/
	SSP0_IRQHandler,						/* 30, SSP0						*/
	SSP1_IRQHandler,						/* 31, SSP1						*/
	PLL0_IRQHandler,						/* 32, PLL0						*/
	RTC_IRQHandler,	 						/* 33, RTC						*/
	EINT0_IRQHandler,						/* 34, EINT0					*/
	EINT1_IRQHandler,						/* 35, EINT1					*/
	EINT2_IRQHandler,						/* 36, EINT2					*/
	EINT3_IRQHandler,						/* 37, EINT3					*/
	ADC_IRQHandler,							/* 38, ADC						*/
	BOD_IRQHandler,							/* 39, BOD						*/
	USB_IRQHandler,							/* 40, USB						*/
	CAN_IRQHandler,							/* 41, CAN						*/
	DMA_IRQHandler,							/* 42, GP DMA					*/
	I2S_IRQHandler,							/* 43, I2S						*/
	ENET_IRQHandler,                      	/* 44, Ethernet.				*/
	RIT_IRQHandler,							/* 45, RITINT					*/
	MCPWM_IRQHandler,						/* 46, Motor Control PWM		*/
	QEI_IRQHandler,							/* 47, Quadrature Encoder		*/
	PLL1_IRQHandler,						/* 48, PLL1 (USB PLL)			*/
};

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	Reset_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset entry point. Performs startup initialization.
*
*	\return		-
*
*	\details	Reset is invoked on power up or a warm reset. The exception
*				model treats reset as a special form of exception. When reset is
*				asserted, the operation of the processor stops, potentially at
*				any point in an instruction. When reset is deasserted, execution
*				restarts from the address provided by the reset entry in the
*				vector table. Execution restarts as privileged execution in
*				Thread mode.
*
*	\note
*
*******************************************************************************/

void Reset_Handler(
	void
) {
	unsigned long * 		pulMainStack;
	unsigned long *			pulSrc;
	unsigned long *			pulDest;
	Ufast8					bytesRemaining;

	/*
	 *  Fill the end of the main stack area so that we can check the stack
	 *  usage later and check for stack overflow. We cannot fill the whole main
	 *  stack here because the stack is currently in use.
	 */

	pulMainStack = &_vMainStackEnd;
	for (bytesRemaining = 100; bytesRemaining != 0; bytesRemaining--)
	{
		*pulMainStack++ = 0xA5A5A5A5U;
	}

    /*
     * Copy the data segment initializers from flash to SRAM.
	 */

    pulSrc = &_etext;
    for(pulDest = &_data; pulDest < &_edata; )
    {
        *pulDest++ = *pulSrc++;
    }

    /*
     * Zero fill the bss segment.  This is done with inline assembly since this
     * will clear the value of pulDest if it is not kept in a register.
     */

    __asm("    ldr     r0, =_bss\n"
          "    ldr     r1, =_ebss\n"
          "    mov     r2, #0\n"
          "    .thumb_func\n"
          "zero_loop:\n"
          "        cmp     r0, r1\n"
          "        it      lt\n"
          "        strlt   r2, [r0], #4\n"
          "        blt     zero_loop");

	/*
	 * Configure hardware. This will enable the PLL and memory acceleration.
	 */

	project__configHW();

	sys_registerFatalErrorCb(stackOverflowHook);

	/*
	 * Call the application's entry point.
	 */

	main();

	/*
	 * main() shouldn't return, but if it does, we'll just enter an infinite
	 * loop. 
	 */

	FOREVER {
		;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	NMI_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		NMI interrupt handler.
*
*	\return		-
*
*	\details	A NonMaskable Interrupt (NMI) can be signaled by a peripheral
*				or triggered by software. This is the highest priority exception
*				other than reset. It is permanently enabled and has a fixed
*				priority of -2. NMIs cannot be:
*				- masked or prevented from activation by any other exception
*				- preempted by any exception other than Reset.
*
*	\note		
*
*******************************************************************************/

PRIVATE void NMI_Handler(
	void
) {
	project__errorBlinker(1);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HardFault_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Hard fault interrupt handler.
*
*	\return		-
*
*	\details	A hard fault is an exception that occurs because of an error
*				during exception processing, or because an exception cannot be
*				managed by any other exception mechanism. Hard faults have a
*				fixed priority of -1, meaning they have higher priority than any
*				exception with configurable priority.
*
*	\note
*
*******************************************************************************/

void prvGetRegistersFromStack( uint32_t *pulFaultStackAddress )
{
	Uint32* pSoftwareReset = &SUP_RESET_SOFTWARE;
	Uint32* pStatusReset = &SUP_RESET_STATUS;
	Uint32* pTimerReset = &SUP_RESET_TIMER;
	extern volatile int BatteryConnected;
	extern volatile Uint32 ResetTimer;

	/* These are volatile to try and prevent the compiler/linker optimising them
	away as the variables never actually get used.  If the debugger won't show the
	values of the variables, make them global my moving their declaration outside
	of this function. */
	volatile uint32_t r0;
	volatile uint32_t r1;
	volatile uint32_t r2;
	volatile uint32_t r3;
	volatile uint32_t r12;
	volatile uint32_t lr; /* Link register. */
	volatile uint32_t pc; /* Program counter. */
	volatile uint32_t psr;/* Program status register. */
	volatile unsigned long _CFSR ;
	volatile unsigned long _HFSR ;
	volatile unsigned long _DFSR ;
	volatile unsigned long _AFSR ;
	volatile unsigned long _BFAR ;
	volatile unsigned long _MMAR ;

    r0 = pulFaultStackAddress[ 0 ];
    r1 = pulFaultStackAddress[ 1 ];
    r2 = pulFaultStackAddress[ 2 ];
    r3 = pulFaultStackAddress[ 3 ];

    r12 = pulFaultStackAddress[ 4 ];
    lr = pulFaultStackAddress[ 5 ];
    pc = pulFaultStackAddress[ 6 ];
    psr = pulFaultStackAddress[ 7 ];

    // Configurable Fault Status Register
    // Consists of MMSR, BFSR and UFSR
    _CFSR = (*((volatile unsigned long *)(0xE000ED28))) ;

    // Hard Fault Status Register
    _HFSR = (*((volatile unsigned long *)(0xE000ED2C))) ;

    // Debug Fault Status Register
    _DFSR = (*((volatile unsigned long *)(0xE000ED30))) ;

    // Auxiliary Fault Status Register
    _AFSR = (*((volatile unsigned long *)(0xE000ED3C))) ;

    // Read the Fault Address Registers. These may not contain valid values.
    // Check BFARVALID/MMARVALID to see if they are valid values
    // MemManage Fault Address Register
    _MMAR = (*((volatile unsigned long *)(0xE000ED34))) ;
    // Bus Fault Address Register
    _BFAR = (*((volatile unsigned long *)(0xE000ED38))) ;

    (void)r0;
    (void)r1;
    (void)r2;
    (void)r3;
    (void)r12;
    (void)lr;
    (void)pc;
    (void)psr;
    (void)_CFSR;
    (void)_HFSR;
    (void)_DFSR;
    (void)_AFSR;
    (void)_MMAR;
    (void)_BFAR;

	/* When the following line is hit, the variables contain the register values. */

    /* Store data, trigger action at startup and reset application */
    *pSoftwareReset = SUP_MASK_HARDFAULT;
    *pStatusReset = (Uint32)BatteryConnected;
    *pTimerReset = ResetTimer;

#if PROJECT_USE_WDOG == 0
	/*
	 *	Initialize watchdog.
	 */
	nxp_wdogInit(
		NXP_WDOG_RESET,
		((((PROJECT_CPU_HZ / 1000) / 4) / 4) * PROJECT_WDOG_TO) - 1,
		NXP_WDOG_CLK_APB
	);
#endif

	nxp_wdogGenerateReset();
}

PRIVATE void HardFault_Handler(void)
{
    __asm volatile
    (
        " tst lr, #4                                                \n"
        " ite eq                                                    \n"
        " mrseq r0, msp                                             \n"
        " mrsne r0, psp                                             \n"
        " ldr r1, [r0, #24]                                         \n"
        " ldr r2, handler2_address_const                            \n"
        " bx r2                                                     \n"
        " handler2_address_const: .word prvGetRegistersFromStack    \n"
    );
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	MemManage_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Mem interrupt handler.
*
*	\return		-
*
*	\details	A memory management fault is an exception that occurs because of
*				a memory protection related fault. The MPU or the fixed memory
*				protection constraints determines this fault, for both
*				instruction and data memory transactions. This fault is used to
*				abort instruction accesses to Execute Never (XN) memory regions,
*				even if the MPU is disabled.
*
*	\note			
*
*******************************************************************************/

PRIVATE void MemManage_Handler(
	void
) {
	project__errorBlinker(3);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	BusFault_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Bus fault interrupt handler.
*
*	\return		-
*
*	\details	A bus fault is an exception that occurs because of a memory
*				related fault for an instruction or data memory transaction.
*				This might be from an error detected on a bus in the memory
*				system.
*
*	\note			
*
*******************************************************************************/

PRIVATE void BusFault_Handler(
	void
) {
	project__errorBlinker(4);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	UsageFault_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Usage fault interrupt handler.
*
*	\return		-
*
*	\details	A usage fault is an exception that occurs because of a fault
*				related to instruction execution. This includes:
*				- an undefined instruction
*				- an illegal unaligned access
*				- invalid state on instruction execution
*				- an error on exception return.
*
*				The following can cause a usage fault when the core is
*				The configured to report them:
*				- an unaligned address on word and halfword memory access
*				- division by zero.
*
*	\note			
*
*******************************************************************************/

PRIVATE void UsageFault_Handler(
	void
) {
	project__errorBlinker(5);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	DebugMon_Handler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Debug monitor interrupt handler.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void DebugMon_Handler(
	void
) {
	project__errorBlinker(6);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	IntDefaultHandler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Default interrupt handler.
*
*	\return		
*
*	\details
*
*	\note			
*
*******************************************************************************/

PRIVATE void IntDefaultHandler(
	void
) {
	/*
	 * A interrupt is enabled without a interrupt handler. This is the default
	 * handler.
	 */
	project__errorBlinker(7);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	stackOverflowHook
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Hanlde stack overflow.
*
*	\details	Called when stack overflow has been detected.
*
*	\note		
*
*******************************************************************************/

PRIVATE void stackOverflowHook(
	sys_FatalErrorType		errorType,
	void * 					pErrData
) {
	if (errorType == SYS_FATAL_STACK_OVERFLOW) {
		bad_task_handle = pErrData;
		bad_task_name = bad_task_handle->pName;
	}

	project__errorBlinker(10);
}
