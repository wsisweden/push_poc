# Microsoft Developer Studio Project File - Name="MSVC" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=MSVC - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "MSVC.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "MSVC.mak" CFG="MSVC - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MSVC - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "MSVC - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "MSVC - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "..\inc" /I "..\inc\inc_w32" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x40b /d "NDEBUG"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy Release\MSVC.lib ..\lib\lib_TjkW32.lib
# End Special Build Tool

!ELSEIF  "$(CFG)" == "MSVC - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\inc\inc_w32" /I "..\inc" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x40b /d "_DEBUG"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Debug\MSVCD.lib"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy Debug\MSVCD.lib ..\lib\lib_TjkW32D.lib
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "MSVC - Win32 Release"
# Name "MSVC - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "DEB"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\DEB\DEB.C
# End Source File
# Begin Source File

SOURCE=..\Src\DEB\DEB_LOG.C
# End Source File
# Begin Source File

SOURCE=..\Src\DEB\DEB_LOG0.C
# End Source File
# Begin Source File

SOURCE=..\Src\DEB\DEB_LOGN.C
# End Source File
# Begin Source File

SOURCE=..\Src\DEB\LOCAL.H
# End Source File
# End Group
# Begin Group "DS1305"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\DS1305\ds1305.c
# End Source File
# Begin Source File

SOURCE=..\Src\DS1305\ds1305_convert.c
# End Source File
# Begin Source File

SOURCE=..\Src\DS1305\fn_macros.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\DS1305\LOCAL.H
# End Source File
# End Group
# Begin Group "DS1820"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\DS1820\ds1820.c
# End Source File
# Begin Source File

SOURCE=..\Src\DS1820\ds1820_alarm.c
# End Source File
# Begin Source File

SOURCE=..\Src\DS1820\fn_macros.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\DS1820\LOCAL.H
# End Source File
# End Group
# Begin Group "EEPR"

# PROP Default_Filter ""
# Begin Group "inc_m16c"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\EEPR\inc_m16c\EEPR2.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\inc_m16c\EEPR_REG2.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\inc_m16c\init.h
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\inc_m16c\LOCAL2.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\inc_m16c\M95256.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\inc_m16c\M95256.h
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\EEPR\EEPR.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\EEPR_REG.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\INIT.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPR\LOCAL.H
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "EEPRM16"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\EEPRM16\EEPRM16.C

!IF  "$(CFG)" == "MSVC - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "MSVC - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Src\EEPRM16\EEPRM16_REG.C

!IF  "$(CFG)" == "MSVC - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "MSVC - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Src\EEPRM16\file.C

!IF  "$(CFG)" == "MSVC - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "MSVC - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Src\EEPRM16\INIT.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPRM16\LOCAL.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPRM16\M95256.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\EEPRM16\M95256.h
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "ERR"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\ERR\ERR.C
# End Source File
# End Group
# Begin Group "FIFOC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\fifoc\fifoc.c
# End Source File
# End Group
# Begin Group "LIB"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\LIB\lib_atoi.c
# End Source File
# Begin Source File

SOURCE=..\Src\LIB\LIB_FCUR.C
# End Source File
# Begin Source File

SOURCE=..\Src\LIB\LIB_FIFO.C
# End Source File
# Begin Source File

SOURCE=..\Src\LIB\lib_itoa.c
# End Source File
# End Group
# Begin Group "MEM"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\Mem\MEM.C
# End Source File
# End Group
# Begin Group "NOV"

# PROP Default_Filter ""
# Begin Group "inc_avr"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\NOV\Inc_avr\local.h
# End Source File
# Begin Source File

SOURCE=..\Src\NOV\Inc_avr\novavr.c
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\NOV\nov.c
# End Source File
# Begin Source File

SOURCE=..\Src\NOV\nov01.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\NOV\NOV_IF.C
# End Source File
# End Group
# Begin Group "ONEW"

# PROP Default_Filter ""
# Begin Group "inc_avr No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\ONEW\Inc_avr\_isr.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\ONEW\Inc_avr\ONEW_CONFIG.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\ONEW\Inc_avr\onew_eeprom.c
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\ONEW\fn_macros.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\ONEW\isr.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\ONEW\LOCAL.H
# End Source File
# Begin Source File

SOURCE=..\Src\ONEW\onew_prot.c
# End Source File
# Begin Source File

SOURCE=..\Src\ONEW\onew_rom.c
# End Source File
# End Group
# Begin Group "OSA"

# PROP Default_Filter ""
# Begin Group "inc_avr No. 2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\OSA\inc_avr\_OSA.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_avr\isr.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_avr\LOCAL.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_avr\OSAAVR.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_avr\OSAAVREX.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_avr\test.c
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_avricc"

# PROP Default_Filter ""
# End Group
# Begin Group "inc_m16c No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\OSA\inc_m16c\_OSA.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_m16c\OS_CFG.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_m16c\OS_CPU.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_m16c\UCOS.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_m16c\UCOS.H
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_w16"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\OSA\inc_w16\_OSA.C
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_w32 No. 1"

# PROP Default_Filter ""
# Begin Group "Old"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OLD\_OSA.C
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OLD\OSAW32.C
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\_OSA.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\INCLUDES.H
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_CFG.h
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_CORE.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\os_cpu.h
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\os_cpu_c.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_MBOX.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_MEM.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_Q.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_SEM.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_TASK.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OS_TIME.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\OSAW32.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\pc.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\pc.h
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\UCOS_II.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\inc_w32\UCOS_II.H
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\OSA\OSA.C
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\OSA_COTA.C
# End Source File
# Begin Source File

SOURCE=..\Src\OSA\OSA_YIEL.C
# End Source File
# End Group
# Begin Group "PORTDRV"

# PROP Default_Filter ""
# Begin Group "inc_avr No. 3"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\PORTDRV\Inc_avr\TWI.C
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_m16c No. 2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\PORTDRV\Inc_m16c\PORT2.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\PORTDRV\Inc_m16c\PORT2b.C
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_w32 No. 2"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\PORTDRV\Inc_w32\portw32.c
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\PORTDRV\INIT.H
# End Source File
# Begin Source File

SOURCE=..\Src\PORTDRV\LOCAL.H
# End Source File
# Begin Source File

SOURCE=..\Src\PORTDRV\PORTDRV.C
# End Source File
# End Group
# Begin Group "REAL"

# PROP Default_Filter ""
# Begin Group "inc_avr No. 4"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\REAL\inc_avr\real_mul.s90
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_avricc No. 1"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\REAL\inc_avricc\real_mul.asm
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_m16c No. 3"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\REAL\inc_m16c\real_div.s34
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\REAL\inc_m16c\real_mul.s34
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Group "inc_w32 No. 3"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\REAL\inc_w32\real_mat.c
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\REAL\real.c
# End Source File
# End Group
# Begin Group "REG"

# PROP Default_Filter ""
# Begin Group "inc_m16c No. 4"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\Reg\Inc_m16c\REG_STR2.S34
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=..\Src\Reg\INIT.H
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\LOCAL.H
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_BUF.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_CNV1.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_DEB.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_FILE.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_I16.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_I32.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_I8.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_MMI.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_STR.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_U16.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_U32.C
# End Source File
# Begin Source File

SOURCE=..\Src\Reg\REG_U8.C
# End Source File
# End Group
# Begin Group "SPI"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\SPI\fn_macros.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\SPI\SPI.C
# End Source File
# End Group
# Begin Group "SWTIMER"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\SWTimer\isr.C
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\Src\SWTimer\local.h
# End Source File
# Begin Source File

SOURCE=..\Src\SWTimer\swtimer.C
# End Source File
# End Group
# Begin Group "SYS"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\SYS\local.h
# End Source File
# Begin Source File

SOURCE=..\Src\SYS\SYS.C
# End Source File
# Begin Source File

SOURCE=..\Src\SYS\sys_ctrl.c
# End Source File
# Begin Source File

SOURCE=..\Src\SYS\SYS_ITOD.C
# End Source File
# Begin Source File

SOURCE=..\Src\SYS\sys_iToN.c
# End Source File
# Begin Source File

SOURCE=..\Src\SYS\sys_nToI.c
# End Source File
# End Group
# Begin Group "T6963"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\T6963\t6963.c
# End Source File
# End Group
# Begin Group "TIMER"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\Timer\timer.c
# End Source File
# End Group
# Begin Group "TODO"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\TODO\todo.c
# End Source File
# End Group
# Begin Group "TXT"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\TXT\TXT.C
# End Source File
# Begin Source File

SOURCE=..\Src\TXT\TXT_CPY.C
# End Source File
# End Group
# Begin Group "USB"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\USB\INIT.H
# End Source File
# Begin Source File

SOURCE=..\Src\USB\local.h
# End Source File
# Begin Source File

SOURCE=..\Src\USB\usb.c
# End Source File
# End Group
# Begin Group "UTIL"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\UTIL\atoi16.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\atoi32.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\atoi8.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\atou16.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\atou32.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\atou8.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\i16toa.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\i32toa.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\i8toa.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\u16toa.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\u32toa.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\u8toa.c
# End Source File
# Begin Source File

SOURCE=..\Src\UTIL\util_str.c
# End Source File
# End Group
# Begin Group "FLWR"

# PROP Default_Filter ""
# Begin Group "INC_W32 No. 4"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\FLWR\inc_w32\flwr_hw.c
# End Source File
# End Group
# Begin Group "INC_LPC2XXX"

# PROP Default_Filter ""
# End Group
# Begin Source File

SOURCE=..\Src\FLWR\flwr.c
# End Source File
# Begin Source File

SOURCE=..\Src\FLWR\flwr_hw.h
# End Source File
# Begin Source File

SOURCE=..\Src\FLWR\flwr_reg.c
# End Source File
# Begin Source File

SOURCE=..\Src\FLWR\INIT.H
# End Source File
# Begin Source File

SOURCE=..\Src\FLWR\LOCAL.H
# End Source File
# End Group
# Begin Group "DUMMY"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Src\DUMMY\DUMMY.C
# End Source File
# Begin Source File

SOURCE=..\Src\DUMMY\INIT.H
# End Source File
# End Group
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Group "Inc_w32"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Inc\Inc_w32\_OSA.H
# End Source File
# Begin Source File

SOURCE=..\Inc\Inc_w32\_portdrv.h
# End Source File
# Begin Source File

SOURCE=..\Inc\Inc_w32\INCLUDES.H
# End Source File
# Begin Source File

SOURCE=..\Inc\Inc_w32\OS_CFG.h
# End Source File
# Begin Source File

SOURCE=..\Inc\Inc_w32\os_cpu.h
# End Source File
# Begin Source File

SOURCE=..\Inc\Inc_w32\pc.h
# End Source File
# Begin Source File

SOURCE=..\Inc\Inc_w32\UCOS_II.H
# End Source File
# End Group
# Begin Source File

SOURCE=..\Inc\AVRIO.H
# End Source File
# Begin Source File

SOURCE=..\Inc\AVRUART.H
# End Source File
# Begin Source File

SOURCE=..\Inc\DEB.H
# End Source File
# Begin Source File

SOURCE=..\Inc\DS1305.H
# End Source File
# Begin Source File

SOURCE=..\Inc\DS1820.H
# End Source File
# Begin Source File

SOURCE=..\Inc\dummy.h
# End Source File
# Begin Source File

SOURCE=..\Inc\eeprm16.h
# End Source File
# Begin Source File

SOURCE=..\Inc\ERR.H
# End Source File
# Begin Source File

SOURCE=..\Inc\EXE.H
# End Source File
# Begin Source File

SOURCE=..\Inc\fifoc.h
# End Source File
# Begin Source File

SOURCE=..\Inc\flwr.h
# End Source File
# Begin Source File

SOURCE=..\Inc\HW.H
# End Source File
# Begin Source File

SOURCE=..\Inc\ISR.H
# End Source File
# Begin Source File

SOURCE=..\Inc\LIB.H
# End Source File
# Begin Source File

SOURCE=..\Inc\M95.H
# End Source File
# Begin Source File

SOURCE=..\Inc\MEM.H
# End Source File
# Begin Source File

SOURCE=..\Inc\NOV.H
# End Source File
# Begin Source File

SOURCE=..\Inc\ONEW.H
# End Source File
# Begin Source File

SOURCE=..\Inc\OSA.H
# End Source File
# Begin Source File

SOURCE=..\Inc\PORTDRV.H
# End Source File
# Begin Source File

SOURCE=..\Inc\REAL.H
# End Source File
# Begin Source File

SOURCE=..\Inc\REG.H
# End Source File
# Begin Source File

SOURCE=..\Inc\SPI.H
# End Source File
# Begin Source File

SOURCE=..\Inc\swtimer.h
# End Source File
# Begin Source File

SOURCE=..\Inc\SYS.H
# End Source File
# Begin Source File

SOURCE=..\Inc\t6963.h
# End Source File
# Begin Source File

SOURCE=..\Inc\timer.h
# End Source File
# Begin Source File

SOURCE=..\Inc\todo.h
# End Source File
# Begin Source File

SOURCE=..\Inc\TOOLS.H
# End Source File
# Begin Source File

SOURCE=..\Inc\TXT.H
# End Source File
# Begin Source File

SOURCE=..\Inc\usb.h
# End Source File
# Begin Source File

SOURCE=..\Inc\util.h
# End Source File
# End Group
# End Target
# End Project
