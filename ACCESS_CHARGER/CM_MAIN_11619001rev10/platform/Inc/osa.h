/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		osa.h
*
*	\ingroup	OSA
*
*	\brief		
*
*	\note		
*
*	\version	04-08-02 / Tero Kankaanpää
*
*******************************************************************************/

#ifndef OSA_H_INCLUDED
#define OSA_H_INCLUDED

#ifndef TOOLS_H_INCLUDED
#error Must include TOOLS.H before OSA.H
#endif

#include "sys.h"

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Default priority codes
 * \details	These may be redef'd by the implementation specific part (.OSA).
 */

/**@{*/
#define OSA_PRIORITY_HIGH	8			
#define OSA_PRIORITY_NORMAL	5			
#define OSA_PRIORITY_LOW	2			
/**@}*/

/**
 *	\name 	Sync flags
 */

/**@{*/
#define OSA__SYNC_NVMEM		(1<<0)		/**< NVMEM ready flag.				*/
#define OSA__SYNC_REG		(1<<1)		/**< REG ready flag. Non-volatile
										 *	register memory is ready.		*/
#define OSA__SYNC_ILEXEC_CONF (1<<2)	/**< ILEXEC Config ready flag.		*/
#define OSA__SYNC_1USR_MASK (1<<3)		/**< First user flag.				*/
/**@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/**
 * Number of milliseconds for each OSA tick.
 */

#define	osa_tickIntervalMs	8U

/**
 * Converts milliseconds to OSA ticks.
 */

#define osa_msToTicks(ms_)	(((ms_)+osa_tickIntervalMs/2)/osa_tickIntervalMs)

#define osa_getTaskCount()	(osa_nTaskCount)

#if defined NDEBUG || (TARGET_SELECTED & TARGET_AVR)

# define osa_newCoTask(obj_,name_,prior_,fn_,stack_) {	\
	(obj_)->fn = fn_;									\
	osa__newCoTask(obj_,prior_,stack_);					\
}

#else

# define osa_newCoTask(obj_,name_,prior_,fn_,stack_) {	\
	(obj_)->name = name_;								\
	(obj_)->fn = fn_;									\
	osa__newCoTask(obj_,prior_,stack_);					\
}

#endif

#define osa_diagFirstStackInfo()	osa__diagStackList
#define osa_diagNextStackInfo(x_)	(x_)->pNext

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*
 *	Declare sync flag type depending on target size.
 */

#if SYS__TARGET_SIZE <= 100
typedef Uint8 osa_syncFlagType;			/**< Type for sync flags.			*/
#elif SYS__TARGET_SIZE <= 200
typedef Uint16 osa_syncFlagType;		/**< Type for sync flags.			*/
#else
typedef Uint32 osa_syncFlagType;		/**< Type for sync flags.			*/
#endif

/**
 * 	Function pointer type used for OSA post init callback.
 */

typedef void (*				osa_fnPostInit)(void);

/*
 *	There is no actual idle task on avr implementation...
 */

#if !(TARGET_SELECTED & TARGET_AVR)

typedef struct osa__idleType {			/*''''''''''''''''''''''''''''''''''*/
	void (*					fn)(void *); /**< Function to invoke by idle-task*/
	void *					pUtil;		/**< Argument to the function       */
	struct osa__idleType *	link;		/**< Internal to OSA                */
} osa_IdleType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif

/**
 *	Type used for registering sync flag callbacks.
 */

typedef struct osa__sCallType {			/*''''''''''''''''''''''''''''''''''*/
	osa_syncFlagType		flags;		/**< Synchronization flags to wait  */
	void (*					fn)(void *);/**< Function to invoke by SyncSet	*/
	void *					pUtil;		/**< Argument to the function       */
	struct osa__sCallType *	link;		/**< Internal to OSA                */
} osa_SyncCallType;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type used for CoTasks.
 */

typedef struct osa__coTaskType {		/*''''''''''''''''''''''''''''''''''*/
	struct osa__coTaskType *link;		/**< Internal to OSA                */
#if !defined(NDEBUG) && !(TARGET_SELECTED & TARGET_AVR)
	char const_P *			name;		/* Name of the co-task / debug only	*/
#endif									/*									*/
	void (*					fn)(struct osa__coTaskType *); /**< CoTask fn   */
} osa_CoTaskType;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#ifdef TIMER_H_INCLUDED

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	osa_CoTaskType			coTask;
} osa_CyclicCoTaskType;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif

/******************************************************************************
;
;	I M P L E M E N T A T I O N   S P E C I F I C   P A R T
;
******************************************************************************/

# include "_osa.h"

/**
 *  Declared after the inclusion of target independent header so that 
 *	osa_TaskType is defined. Using "struct osa_TaskType" causes warnings in
 *	gcc.
 */
 
typedef struct osa__diagTag {				/*''''''''''''''''''''''''''''''*/
	struct osa__diagTag *	pNext;			/**< Ptr to next stack info     */
	osa_TaskType *			pTask;			/**< Ptr to task struct         */
	void *					pStack;			/**< Ptr to stack               */
	Uint16					nStackSize;		/**< Size of stack in bytes     */
	Uint16					nStackUsage;	/**< Max detected stack usage   */
} osa_DiagStackInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

#ifdef __cplusplus 
extern "C" {
#endif

void						osa__newCoTask(osa_CoTaskType *,Uint8,Uint16);
void						osa_coTaskRun(osa_CoTaskType *);
void						osa_coTaskRunIsr(osa_CoTaskType *);

void						osa_execIdle(osa_IdleType *);

void						osa_run(void);
void						osa_regPostInitFn(osa_fnPostInit);

void						osa_semaSetFn(osa_SemaType *);
void						osa_signalSendFn(osa_TaskType *);

void						osa_taskResumeFn(osa_TaskType *);

void						osa_yield(void);

osa_syncFlagType			osa_newSyncFlag(void);
void						osa_syncCall(osa_SyncCallType *);
void						osa_syncGet(osa_syncFlagType);
void						osa_syncSet(osa_syncFlagType);
Boolean						osa_syncPeek(osa_syncFlagType);
osa_syncFlagType			osa_syncWait(osa_syncFlagType, Uint16);

void						osa_diagAddStackWatch(void *, void *, Uint16);
Uint16						osa_diagCheckStack(osa_DiagStackInfo *);
Uint16						osa_diagStepStackScan(osa_DiagStackInfo **);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern Boolean				osa_isRunning;
extern Uint8				osa_nTaskCount;

/* extern osa_fnPostInit		osa_fnPostInitPtr; */

#ifdef __cplusplus 
}
#endif

/***************************************************************//** \endcond */

#endif
