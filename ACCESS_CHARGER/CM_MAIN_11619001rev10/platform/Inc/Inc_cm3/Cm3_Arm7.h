/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	Name:	Cm3_Arm7.h
*
*--------------------------------------------------------------------------*//**
*
*	\file
*
*	\ingroup	OSA_CM3
*
*	\brief		LPC17xx register defines
*
*	\details	This file does only rename the register defines so that the same
*				name can be used as for the LPC23xx series controllers.
*
*	\note
*
*	\version	\$Rev: 3795 $ \n
*				\$Date: 2019-08-14 10:30:22 +0300 (ke, 14 elo 2019) $ \n
*				\$Author: tlvian $
*
*******************************************************************************/

#ifndef CM3_ARM7_REMAP_H_INCLUDED
#define CM3_ARM7_REMAP_H_INCLUDED

/*******************************************************************************
 * System Control registers
 */

 /*
  *	SCR bits
  */
#define SCR_SLEEP			0x00
#define SCR_SLEEPONEXIT		(1<<1)
#define SCR_SLEEPDEEP		(1<<2)
#define SCR_SEVONPEND		(1<<4)
 
/*
 *	PLL registers
 */
#define PLLCON				LPC_SC->PLL0CON
#define PLLFEED				LPC_SC->PLL0FEED
//#define SCS				LPC_SC->SCS			Use new format
//#define CLKSRCSEL			LPC_SC->CLKSRCSEL	Use new format
#define PLLCFG				LPC_SC->PLL0CFG
//#define CCLKCFG			LPC_SC->CCLKCFG		Use new format
#define PLLSTAT				LPC_SC->PLL0STAT

//#define PCONP				LPC_SC->PCONP		Use new format
//#define PCLKSEL0			LPC_SC->PCLKSEL0	Use new format
//#define PCLKSEL1			LPC_SC->PCLKSEL1	Use new format

/*******************************************************************************
 * General Purpose Input/Output (GPIO)
 */

#define FIO0DIR				LPC_GPIO0->FIODIR
#define FIO0CLR				LPC_GPIO0->FIOCLR
#define	FIO0SET				LPC_GPIO0->FIOSET
#define	FIO0MASK			LPC_GPIO0->FIOMASK
#define	FIO0PIN				LPC_GPIO0->FIOPIN

#define FIO1DIR				LPC_GPIO1->FIODIR
#define FIO1CLR				LPC_GPIO1->FIOCLR
#define	FIO1SET				LPC_GPIO1->FIOSET
#define	FIO1MASK			LPC_GPIO1->FIOMASK
#define	FIO1PIN				LPC_GPIO1->FIOPIN

#define FIO2DIR				LPC_GPIO2->FIODIR
#define FIO2CLR				LPC_GPIO2->FIOCLR
#define	FIO2SET				LPC_GPIO2->FIOSET
#define	FIO2MASK			LPC_GPIO2->FIOMASK
#define	FIO2PIN				LPC_GPIO2->FIOPIN

#define FIO3DIR				LPC_GPIO3->FIODIR
#define FIO3CLR				LPC_GPIO3->FIOCLR
#define	FIO3SET				LPC_GPIO3->FIOSET
#define	FIO3MASK			LPC_GPIO3->FIOMASK
#define	FIO3PIN				LPC_GPIO3->FIOPIN

#define FIO4DIR				LPC_GPIO4->FIODIR
#define FIO4CLR				LPC_GPIO4->FIOCLR
#define	FIO4SET				LPC_GPIO4->FIOSET
#define	FIO4MASK			LPC_GPIO4->FIOMASK
#define	FIO4PIN				LPC_GPIO4->FIOPIN

#define IO_INT_STAT			LPC_GPIOINT->IntStatus
#define IO0_INT_STAT_R		LPC_GPIOINT->IO0IntStatR
#define	IO0_INT_STAT_F		LPC_GPIOINT->IO0IntStatF
#define	IO0_INT_CLR			LPC_GPIOINT->IO0IntClr
#define	IO0_INT_EN_R		LPC_GPIOINT->IO0IntEnR
#define IO0_INT_EN_F		LPC_GPIOINT->IO0IntEnF
#define IO2_INT_STAT_R		LPC_GPIOINT->IO2IntStatR
#define	IO2_INT_STAT_F		LPC_GPIOINT->IO2IntStatF
#define	IO2_INT_CLR			LPC_GPIOINT->IO2IntClr
#define	IO2_INT_EN_R		LPC_GPIOINT->IO2IntEnR
#define	IO2_INT_EN_F		LPC_GPIOINT->IO2IntEnF

/*******************************************************************************
 * Pin Connect Block (PINCON)
 */
/*
#define PINSEL0				LPC_PINCON->PINSEL0		Use new format
#define PINSEL1				LPC_PINCON->PINSEL1
#define PINSEL2				LPC_PINCON->PINSEL2
#define PINSEL3				LPC_PINCON->PINSEL3
#define PINSEL4				LPC_PINCON->PINSEL4
#define PINSEL5				LPC_PINCON->PINSEL5
#define PINSEL6				LPC_PINCON->PINSEL6
#define PINSEL7				LPC_PINCON->PINSEL7
#define PINSEL8				LPC_PINCON->PINSEL8
#define PINSEL9				LPC_PINCON->PINSEL9
#define PINSEL10			LPC_PINCON->PINSEL10

#define PINMODE0			LPC_PINCON->PINMODE0	Use new format
#define PINMODE1    		LPC_PINCON->PINMODE1
#define PINMODE2    		LPC_PINCON->PINMODE2
#define PINMODE3    		LPC_PINCON->PINMODE3
#define PINMODE4    		LPC_PINCON->PINMODE4
#define PINMODE5    		LPC_PINCON->PINMODE5
#define PINMODE6    		LPC_PINCON->PINMODE6
#define PINMODE7    		LPC_PINCON->PINMODE7
#define PINMODE8    		LPC_PINCON->PINMODE8
#define PINMODE9    		LPC_PINCON->PINMODE9
*/
/*******************************************************************************
 * Analog-to-Digital Converter (ADC)
 */
 
#define AD0CR				LPC_ADC->ADCR
#define AD0GDR				LPC_ADC->ADGDR
#define AD0INTEN			LPC_ADC->ADINTEN
#define AD0DR0				LPC_ADC->ADDR0
#define AD0DR1				LPC_ADC->ADDR1
#define AD0DR2				LPC_ADC->ADDR2
#define AD0DR3				LPC_ADC->ADDR3
#define AD0DR4				LPC_ADC->ADDR4
#define AD0DR5				LPC_ADC->ADDR5
#define AD0DR6				LPC_ADC->ADDR6
#define AD0DR7				LPC_ADC->ADDR7
#define AD0STAT				LPC_ADC->ADSTAT
#define AD0TRM				LPC_ADC->ADTRM
	        
/*******************************************************************************
 * Serial Peripheral Interface (SPI)
 */

#define S0SPCR				LPC_SPI->SPCR
#define S0SPSR				LPC_SPI->SPSR
#define S0SPDR 				LPC_SPI->SPDR
#define S0SPCCR 			LPC_SPI->SPCCR
#define S0SPINT				LPC_SPI->SPINT

/*******************************************************************************
 * Synchronous Serial Communication (SSP)
 */

#define SSP0CR0				LPC_SSP0->CR0
#define SSP0CR1				LPC_SSP0->CR1
#define SSP0DR				LPC_SSP0->DR
#define SSP0SR				LPC_SSP0->SR
#define SSP0CPSR	    	LPC_SSP0->CPSR
#define SSP0IMSC			LPC_SSP0->IMSC
#define SSP0RIS				LPC_SSP0->RIS
#define SSP0MIS				LPC_SSP0->MIS
#define SSP0ICR				LPC_SSP0->ICR
#define SSP0DMACR			LPC_SSP0->DMACR
        
#define SSP1CR0				LPC_SSP1->CR0
#define SSP1CR1				LPC_SSP1->CR1
#define SSP1DR				LPC_SSP1->DR
#define SSP1SR				LPC_SSP1->SR
#define SSP1CPSR	    	LPC_SSP1->CPSR
#define SSP1IMSC			LPC_SSP1->IMSC
#define SSP1RIS				LPC_SSP1->RIS
#define SSP1MIS				LPC_SSP1->MIS
#define SSP1ICR				LPC_SSP1->ICR
#define SSP1DMACR			LPC_SSP1->DMACR
	
/*******************************************************************************
 * Pulse-Width Modulation (PWM)
 */	

#define PWM1IR				LPC_PWM1->IR
#define PWM1TCR				LPC_PWM1->TCR
#define PWM1TC				LPC_PWM1->TC
#define PWM1PR				LPC_PWM1->PR
#define PWM1PC				LPC_PWM1->PC
#define PWM1MCR				LPC_PWM1->MCR
#define PWM1MR0				LPC_PWM1->MR0
#define PWM1MR1				LPC_PWM1->MR1
#define PWM1MR2				LPC_PWM1->MR2
#define PWM1MR3				LPC_PWM1->MR3
#define PWM1CCR				LPC_PWM1->CCR
#define PWM1CR0				LPC_PWM1->CR0
#define PWM1CR1				LPC_PWM1->CR1
#define PWM1CR2				LPC_PWM1->CR2
#define PWM1CR3				LPC_PWM1->CR3
#define PWM1MR4				LPC_PWM1->MR4
#define PWM1MR5				LPC_PWM1->MR5
#define PWM1MR6				LPC_PWM1->MR6
#define PWM1PCR				LPC_PWM1->PCR
#define PWM1LER				LPC_PWM1->LER
#define PWM1CTCR			LPC_PWM1->CTCR
		
/*******************************************************************************
 * Watchdog Timer (WDT)
 */	
/*
#define WDMOD				LPC_WDT->WDMOD		Use new format
#define WDTC				LPC_WDT->WDTC
#define WDFEED				LPC_WDT->WDFEED
#define WDTV				LPC_WDT->WDTV
#define WDCLKSEL			LPC_WDT->WDCLKSEL
*/
/*******************************************************************************
 * UART
 */		
#define U0IER				LPC_UART0->IER
#define U0LCR				LPC_UART0->LCR
#define U0DLL				LPC_UART0->DLL
#define U0DLM				LPC_UART0->DLM
#define U0FDR				LPC_UART0->FDR
#define U0FCR				LPC_UART0->FCR
#define	U0THR				LPC_UART0->THR
#define U0IIR				LPC_UART0->IIR
#define U0LSR				LPC_UART0->LSR
#define U0RBR				LPC_UART0->RBR

#define U1IER				LPC_UART1->IER
#define U1LCR				LPC_UART1->LCR
#define U1DLL				LPC_UART1->DLL
#define U1DLM				LPC_UART1->DLM
#define U1FDR				LPC_UART1->FDR
#define U1FCR				LPC_UART1->FCR
#define	U1THR				LPC_UART1->THR
#define U1IIR				LPC_UART1->IIR
#define U1LSR				LPC_UART1->LSR
#define U1RBR				LPC_UART1->RBR

#define U2IER				LPC_UART2->IER
#define U2LCR				LPC_UART2->LCR
#define U2DLL				LPC_UART2->DLL
#define U2DLM				LPC_UART2->DLM
#define U2FDR				LPC_UART2->FDR
#define U2FCR				LPC_UART2->FCR
#define	U2THR				LPC_UART2->THR
#define U2IIR				LPC_UART2->IIR
#define U2LSR				LPC_UART2->LSR
#define U2RBR				LPC_UART2->RBR

#define U3IER				LPC_UART3->IER
#define U3LCR				LPC_UART3->LCR
#define U3DLL				LPC_UART3->DLL
#define U3DLM				LPC_UART3->DLM
#define U3FDR				LPC_UART3->FDR
#define U3FCR				LPC_UART3->FCR
#define	U3THR				LPC_UART3->THR
#define U3IIR				LPC_UART3->IIR
#define U3LSR				LPC_UART3->LSR
#define U3RBR				LPC_UART3->RBR

/*******************************************************************************
 * RTC
 */		
#define RTC_CCR				LPC_RTC->CCR
#define RTC_CIIR			LPC_RTC->CIIR
#define RTC_ILR				LPC_RTC->ILR
#define RTC_CAL				LPC_RTC->CALIBRATION
//#define RTC_AUX			LPC_RTC->RTC_AUX		Use new format
#define RTC_AMR				LPC_RTC->AMR
#define RTC_SEC				LPC_RTC->SEC
//#define RTC_MIN			LPC_RTC->MIN			Use new format
#define RTC_HOUR			LPC_RTC->HOUR
#define RTC_DOM				LPC_RTC->DOM
#define RTC_DOW				LPC_RTC->DOW
#define RTC_DOY				LPC_RTC->DOY
#define RTC_MONTH			LPC_RTC->MONTH
#define RTC_YEAR			LPC_RTC->YEAR
#define RTC_GPREG0			LPC_RTC->GPREG0
#define RTC_GPREG1			LPC_RTC->GPREG1
#define RTC_GPREG2			LPC_RTC->GPREG2
#define RTC_GPREG3			LPC_RTC->GPREG3
#define RTC_GPREG4			LPC_RTC->GPREG4
#define RTC_CTIME0			LPC_RTC->CTIME0
#define RTC_CTIME1			LPC_RTC->CTIME1
#define RTC_CTIME2			LPC_RTC->CTIME2
#define RTC_ALSEC			LPC_RTC->ALSEC
		
/*******************************************************************************
 * I2C
 */	
#define I20CONSET			LPC_I2C0->I2CONSET
#define I20CONCLR			LPC_I2C0->I2CONCLR
#define I20STAT				LPC_I2C0->I2STAT
#define I20DAT				LPC_I2C0->I2DAT
#define I20ADR				LPC_I2C0->I2ADR0
#define I20SCLL				LPC_I2C0->I2SCLL
#define I20SCLH				LPC_I2C0->I2SCLH

#define I21CONSET			LPC_I2C1->I2CONSET
#define I21CONCLR			LPC_I2C1->I2CONCLR
#define I21STAT				LPC_I2C1->I2STAT
#define I21DAT				LPC_I2C1->I2DAT
#define I21ADR				LPC_I2C1->I2ADR0
#define I21SCLL				LPC_I2C1->I2SCLL
#define I21SCLH				LPC_I2C1->I2SCLH

#define I22CONSET			LPC_I2C2->I2CONSET
#define I22CONCLR			LPC_I2C2->I2CONCLR
#define I22STAT				LPC_I2C2->I2STAT
#define I22DAT				LPC_I2C2->I2DAT
#define I22ADR				LPC_I2C2->I2ADR0
#define I22SCLL				LPC_I2C2->I2SCLL
#define I22SCLH				LPC_I2C2->I2SCLH

/*******************************************************************************
 * GPDMA
 */	
#define GPDMA_INT_STAT         LPC_GPDMA->DMACIntStat
#define GPDMA_INT_TCSTAT       LPC_GPDMA->DMACIntTCStat
#define GPDMA_INT_TCCLR        LPC_GPDMA->DMACIntTCClear
#define GPDMA_INT_ERR_STAT     LPC_GPDMA->DMACIntErrStat
#define GPDMA_INT_ERR_CLR      LPC_GPDMA->DMACIntErrClr
#define GPDMA_RAW_INT_TCSTAT   LPC_GPDMA->DMACRawIntTCStat
#define GPDMA_RAW_INT_ERR_STAT LPC_GPDMA->DMACRawIntErrStat
#define GPDMA_ENABLED_CHNS     LPC_GPDMA->DMACEnbldChns
#define GPDMA_SOFT_BREQ        LPC_GPDMA->DMACSoftBReq
#define GPDMA_SOFT_SREQ        LPC_GPDMA->DMACSoftSReq
#define GPDMA_SOFT_LBREQ       LPC_GPDMA->DMACSoftLBReq
#define GPDMA_SOFT_LSREQ       LPC_GPDMA->DMACSoftLSReq
#define GPDMA_CONFIG           LPC_GPDMA->DMACConfig
#define GPDMA_SYNC             LPC_GPDMA->DMACSync

/* DMA channel 0 registers */
#define GPDMA_CH0_SRC		LPC_GPDMACH0->DMACCSrcAddr
#define GPDMA_CH0_DEST		LPC_GPDMACH0->DMACCDestAddr
#define GPDMA_CH0_LLI		LPC_GPDMACH0->DMACCLLI
#define GPDMA_CH0_CTRL		LPC_GPDMACH0->DMACCControl
#define GPDMA_CH0_CFG		LPC_GPDMACH0->DMACCConfig

/* DMA channel 1 registers */
#define GPDMA_CH1_SRC		LPC_GPDMACH1->DMACCSrcAddr
#define GPDMA_CH1_DEST		LPC_GPDMACH1->DMACCDestAddr
#define GPDMA_CH1_LLI		LPC_GPDMACH1->DMACCLLI
#define GPDMA_CH1_CTRL		LPC_GPDMACH1->DMACCControl
#define GPDMA_CH1_CFG		LPC_GPDMACH1->DMACCConfig

/* DMA channel 2 registers */
#define GPDMA_CH2_SRC		LPC_GPDMACH2->DMACCSrcAddr
#define GPDMA_CH2_DEST		LPC_GPDMACH2->DMACCDestAddr
#define GPDMA_CH2_LLI		LPC_GPDMACH2->DMACCLLI
#define GPDMA_CH2_CTRL		LPC_GPDMACH2->DMACCControl
#define GPDMA_CH2_CFG		LPC_GPDMACH2->DMACCConfig

/* DMA channel 3 registers */
#define GPDMA_CH3_SRC		LPC_GPDMACH3->DMACCSrcAddr
#define GPDMA_CH3_DEST		LPC_GPDMACH3->DMACCDestAddr
#define GPDMA_CH3_LLI		LPC_GPDMACH3->DMACCLLI
#define GPDMA_CH3_CTRL		LPC_GPDMACH3->DMACCControl
#define GPDMA_CH3_CFG		LPC_GPDMACH3->DMACCConfig

/* DMA channel 4 registers */
#define GPDMA_CH4_SRC		LPC_GPDMACH4->DMACCSrcAddr
#define GPDMA_CH4_DEST		LPC_GPDMACH4->DMACCDestAddr
#define GPDMA_CH4_LLI		LPC_GPDMACH4->DMACCLLI
#define GPDMA_CH4_CTRL		LPC_GPDMACH4->DMACCControl
#define GPDMA_CH4_CFG		LPC_GPDMACH4->DMACCConfig

/* DMA channel 5 registers */
#define GPDMA_CH5_SRC		LPC_GPDMACH5->DMACCSrcAddr
#define GPDMA_CH5_DEST		LPC_GPDMACH5->DMACCDestAddr
#define GPDMA_CH5_LLI		LPC_GPDMACH5->DMACCLLI
#define GPDMA_CH5_CTRL		LPC_GPDMACH5->DMACCControl
#define GPDMA_CH5_CFG		LPC_GPDMACH5->DMACCConfig

/* DMA channel 6 registers */
#define GPDMA_CH6_SRC		LPC_GPDMACH6->DMACCSrcAddr
#define GPDMA_CH6_DEST		LPC_GPDMACH6->DMACCDestAddr
#define GPDMA_CH6_LLI		LPC_GPDMACH6->DMACCLLI
#define GPDMA_CH6_CTRL		LPC_GPDMACH6->DMACCControl
#define GPDMA_CH6_CFG		LPC_GPDMACH6->DMACCConfig

/* DMA channel 7 registers */
#define GPDMA_CH7_SRC		LPC_GPDMACH7->DMACCSrcAddr
#define GPDMA_CH7_DEST		LPC_GPDMACH7->DMACCDestAddr
#define GPDMA_CH7_LLI		LPC_GPDMACH7->DMACCLLI
#define GPDMA_CH7_CTRL		LPC_GPDMACH7->DMACCControl
#define GPDMA_CH7_CFG		LPC_GPDMACH7->DMACCConfig

/*******************************************************************************
 * CAN
 */

/* Acceptance Filter Registers */
#define CAN_AFMR			LPC_CANAF->AFMR
#define CAN_SFF_SA			LPC_CANAF->SFF_sa
#define CAN_SFF_GRP_SA		LPC_CANAF->SFF_GRP_sa
#define CAN_EFF_SA			LPC_CANAF->EFF_sa
#define CAN_EFF_GRP_SA		LPC_CANAF->EFF_GRP_sa
#define CAN_EOT				LPC_CANAF->ENDofTable
#define CAN_LUTerrAd		LPC_CANAF->LUTerrAd
#define CAN_LUTerr			LPC_CANAF->LUTerr

/* Controller Registers */
#define CAN1MOD				LPC_CAN1->MOD
#define CAN1CMR				LPC_CAN1->CMR
#define CAN1GSR				LPC_CAN1->GSR
#define CAN1ICR				LPC_CAN1->ICR
#define CAN1IER				LPC_CAN1->IER
#define CAN1BTR				LPC_CAN1->BTR
#define CAN1EWL				LPC_CAN1->EWL
#define CAN1SR				LPC_CAN1->SR
#define CAN1RFS				LPC_CAN1->RFS
#define CAN1RID				LPC_CAN1->RID
#define CAN1RDA				LPC_CAN1->RDA
#define CAN1RDB				LPC_CAN1->RDB
#define CAN1TFI1			LPC_CAN1->TFI1
#define CAN1TID1			LPC_CAN1->TID1
#define CAN1TDA1			LPC_CAN1->TDA1
#define CAN1TDB1			LPC_CAN1->TDB1
#define CAN1TFI2			LPC_CAN1->TFI2
#define CAN1TID2			LPC_CAN1->TID2
#define CAN1TDA2			LPC_CAN1->TDA2
#define CAN1TDB2			LPC_CAN1->TDB2
#define CAN1TFI3			LPC_CAN1->TFI3
#define CAN1TID3			LPC_CAN1->TID3
#define CAN1TDA3			LPC_CAN1->TDA3
#define CAN1TDB3			LPC_CAN1->TDB3

/*******************************************************************************
 * TIMERS
 */

#define T0IR				LPC_TIM0->IR
#define T0TCR				LPC_TIM0->TCR
#define T0TC				LPC_TIM0->TC
#define T0PR				LPC_TIM0->PR
#define T0PC				LPC_TIM0->PC
#define T0MCR				LPC_TIM0->MCR
#define T0MR0				LPC_TIM0->MR0
#define T0MR1				LPC_TIM0->MR1
#define T0MR2				LPC_TIM0->MR2
#define T0MR3				LPC_TIM0->MR3
#define T0CCR				LPC_TIM0->CCR
#define T0CR0				LPC_TIM0->CR0
#define T0CR1				LPC_TIM0->CR1
#define T0EMR				LPC_TIM0->EMR
#define T0CTCR				LPC_TIM0->CTCR

#define T1IR				LPC_TIM1->IR
#define T1TCR				LPC_TIM1->TCR
#define T1TC				LPC_TIM1->TC
#define T1PR				LPC_TIM1->PR
#define T1PC				LPC_TIM1->PC
#define T1MCR				LPC_TIM1->MCR
#define T1MR0				LPC_TIM1->MR0
#define T1MR1				LPC_TIM1->MR1
#define T1MR2				LPC_TIM1->MR2
#define T1MR3				LPC_TIM1->MR3
#define T1CCR				LPC_TIM1->CCR
#define T1CR0				LPC_TIM1->CR0
#define T1CR1				LPC_TIM1->CR1
#define T1EMR				LPC_TIM1->EMR
#define T1CTCR				LPC_TIM1->CTCR

#define T2IR				LPC_TIM2->IR
#define T2TCR				LPC_TIM2->TCR
#define T2TC				LPC_TIM2->TC
#define T2PR				LPC_TIM2->PR
#define T2PC				LPC_TIM2->PC
#define T2MCR				LPC_TIM2->MCR
#define T2MR0				LPC_TIM2->MR0
#define T2MR1				LPC_TIM2->MR1
#define T2MR2				LPC_TIM2->MR2
#define T2MR3				LPC_TIM2->MR3
#define T2CCR				LPC_TIM2->CCR
#define T2CR0				LPC_TIM2->CR0
#define T2CR1				LPC_TIM2->CR1
#define T2EMR				LPC_TIM2->EMR
#define T2CTCR				LPC_TIM2->CTCR

#define T3IR				LPC_TIM3->IR
#define T3TCR				LPC_TIM3->TCR
#define T3TC				LPC_TIM3->TC
#define T3PR				LPC_TIM3->PR
#define T3PC				LPC_TIM3->PC
#define T3MCR				LPC_TIM3->MCR
#define T3MR0				LPC_TIM3->MR0
#define T3MR1				LPC_TIM3->MR1
#define T3MR2				LPC_TIM3->MR2
#define T3MR3				LPC_TIM3->MR3
#define T3CCR				LPC_TIM3->CCR
#define T3CR0				LPC_TIM3->CR0
#define T3CR1				LPC_TIM3->CR1
#define T3EMR				LPC_TIM3->EMR
#define T3CTCR				LPC_TIM3->CTCR

/*******************************************************************************
 * Motor control PWM
 */

//#define MCCON				LPC_MCPWM->MCCON			Use new format
//#define MCCON_SET			LPC_MCPWM->MCCON_SET
//#define MCCON_CLR			LPC_MCPWM->MCCON_CLR
//#define MCCAPCON			LPC_MCPWM->MCCAPCON
//#define MCCAPCON_SET		LPC_MCPWM->MCCAPCON_SET
//#define MCCAPCON_CLR		LPC_MCPWM->MCCAPCON_CLR		Use new format
#define MCTC0				LPC_MCPWM->MCTIM0
#define MCTC1				LPC_MCPWM->MCTIM1
#define MCTC2				LPC_MCPWM->MCTIM2
#define MCLIM0				LPC_MCPWM->MCPER0
#define MCLIM1				LPC_MCPWM->MCPER1
#define MCLIM2				LPC_MCPWM->MCPER2
#define MCMAT0				LPC_MCPWM->MCPW0
#define MCMAT1				LPC_MCPWM->MCPW1
#define MCMAT2				LPC_MCPWM->MCPW2
#define MCDT				LPC_MCPWM->MCDEADTIME
#define MCCP				LPC_MCPWM->MCCCP
#define MCCAP0				LPC_MCPWM->MCCR0
#define MCCAP1				LPC_MCPWM->MCCR1
#define MCCAP2				LPC_MCPWM->MCCR2
//#define MCINTEN			LPC_MCPWM->MCINTEN			Use new format
//#define MCINTEN_SET		LPC_MCPWM->MCINTEN_SET		
//#define MCINTEN_CLR		LPC_MCPWM->MCINTEN_CLR
//#define MCCNTCON			LPC_MCPWM->MCCNTCON
//#define MCCNTCON_SET		LPC_MCPWM->MCCNTCON_SET
//#define MCCNTCON_CLR		LPC_MCPWM->MCCNTCON_CLR		Use new format
#define MCINTF				LPC_MCPWM->MCINTFLAG
#define MCINTF_SET			LPC_MCPWM->MCINTFLAG_SET
#define MCINTF_CLR			LPC_MCPWM->MCINTFLAG_CLR
//#define MCCAP_CLR			LPC_MCPWM->MCCAP_CLR		Use new format


#endif
