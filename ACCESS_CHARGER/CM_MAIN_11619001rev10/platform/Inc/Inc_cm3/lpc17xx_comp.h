/* 30-10-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		lpc17xx_comp.h
*
*	\ingroup	HW
*
*	\brief		Compatibility remapping.
*
*	\details	This file makes old projects using old lpc17xx.h file compatible
*				with new lpc17xx.h files.
*
*				Newer NXP LPC17xx.h files have prefixed each peripheral name 
*				with "LPC_". This file defines the old names and maps them
*				to the new names.
*
*				The old defines should not be used in new software. They are
*				only declared here so that old software will compile with new
*				NXP lpc17xx.h headers.
*
*	\note		This file should be included in the _hw.h header as long
*				as the compatibility is needed.
*
*	\version	
*
*******************************************************************************/

/**
 *	\name		Compatibility remapping
 */
/*@{*/
#define SC				LPC_SC       
#define GPIO0           LPC_GPIO0     
#define GPIO1           LPC_GPIO1     
#define GPIO2           LPC_GPIO2     
#define GPIO3           LPC_GPIO3     
#define GPIO4           LPC_GPIO4     
#define WDT             LPC_WDT       
#define TIM0            LPC_TIM0      
#define TIM1            LPC_TIM1      
#define TIM2            LPC_TIM2      
#define TIM3            LPC_TIM3      
#define RIT             LPC_RIT       
#define UART0           LPC_UART0     
#define UART1           LPC_UART1     
#define UART2           LPC_UART2     
#define UART3           LPC_UART3     
#define PWM1            LPC_PWM1      
#define I2C0            LPC_I2C0      
#define I2C1            LPC_I2C1      
#define I2C2            LPC_I2C2      
#define I2S             LPC_I2S       
#define SPI             LPC_SPI       
#define RTC             LPC_RTC       
#define GPIOINT         LPC_GPIOINT   
#define PINCON          LPC_PINCON    
#define SSP0            LPC_SSP0      
#define SSP1            LPC_SSP1      
#define ADC             LPC_ADC       
#define DAC             LPC_DAC       
#define CANAF_RAM       LPC_CANAF_RAM 
#define CANAF           LPC_CANAF     
#define CANCR           LPC_CANCR     
#define CAN1            LPC_CAN1      
#define CAN2            LPC_CAN2      
#define MCPWM           LPC_MCPWM     
#define QEI             LPC_QEI       
#define EMAC            LPC_EMAC      
#define GPDMA           LPC_GPDMA     
#define GPDMACH0        LPC_GPDMACH0  
#define GPDMACH1        LPC_GPDMACH1  
#define GPDMACH2        LPC_GPDMACH2  
#define GPDMACH3        LPC_GPDMACH3  
#define GPDMACH4        LPC_GPDMACH4  
#define GPDMACH5        LPC_GPDMACH5  
#define GPDMACH6        LPC_GPDMACH6  
#define GPDMACH7        LPC_GPDMACH7  
#define USB             LPC_USB       
/*@}*/
