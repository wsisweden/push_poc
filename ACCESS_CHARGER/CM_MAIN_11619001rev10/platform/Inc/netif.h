/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	Name: netif.h
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	NETIF
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	23-12-2008 / Ari Suomi
*
*******************************************************************************/

#ifndef NETIF_H_INCLUDED
#define NETIF_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "ndif.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 *	\name	Status bits
 *
 *	\brief	The following status bits are used in the status register.
 */
/**@{*/									/*''''''''''''''''''''''''''''''''''*/
#define NETIF_STAT_ENABLED	(1<<0)		/**< Network interface is enabled.	*/
#define NETIF_STAT_LINKUP	(1<<1)		/**< Network link is up.			*/
#define NETIF_STAT_REGREADY	(1<<2)		/**< REG is ready.					*/
/**@}*/									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Possible values in the dhcpMode register.
 */

enum netif_dhcpMode {					/*''''''''''''''''''''''''''''''''''*/
	NETIF_DHCPMODE_AUTO,				/**< Automatic DHCP client.			*/
	NETIF_DHCPMODE_ENABLED				/**< DHCP client enabled.			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	\name	IP-address status
 *
 *	\brief	The following status bits are used in the ipStatus register.
 */
/**@{*/									/*''''''''''''''''''''''''''''''''''*/
#define NETIF_IPSTAT_LINKLOCAL	(1<<0)	/**< Link-local IP-address in use.	*/
#define NETIF_IPSTAT_DHCP		(1<<1)	/**< IP-address from DHCP server in
										 * use.								*/
#define NETIF_IPSTAT_DHCPMODE	(1<<2)	/**< 1 = DHCP client is enabled.	*/
/**@}*/									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

struct netif__inst;
typedef struct netif__inst netif__Inst;

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PUBLIC void netif_useDhcp(netif__Inst *);
PUBLIC void netif_useAutoip(netif__Inst *);
PUBLIC void netif_useDhcpAndAutoip(netif__Inst *);
PUBLIC void netif_useStatic(netif__Inst *);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern ndif_RxIf const_P	netif_rxIf;
extern ndif_BuffHanIf const_P netif_memHandler;

/******************************************************//** \endcond pub_decl */

#endif
