/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DRV1.H
*
*	\ingroup	DRV1
*
*	\brief		Interface for low level communication driver.
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	DRV1		DRV1
*
*	\brief		Interface for low level communication driver e.g. UART.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

#ifndef DRV1_H_INCLUDED
#define DRV1_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/** 
 * \brief	Type for communication driver init function.
 *
 * \param	pConfig		Pointer to initialization data.
 * \param	pUtil		Utility pointer that will be used as argument for the
 *						the functions called by the communication driver.
 *
 * \return	Pointer to the communication driver instance.
 *
 * \details	The init function of a low level communication driver should be of 
 * 			this type. The function can only be called when OSA is running.
 */
 
typedef void * (*		drv1_fnInit)(void const_P * pConfig, void * pUtil);

/** 
 * \brief	Type for communication driver set function.
 *
 * \details	The set function of a low level communication driver should be of 
 * 			this type. The function is used to set driver settings. The 
 *			function can only be called when OSA is running.
 */
 
typedef void (*			drv1_fnSet)(void);

/** 
 * \brief	Type for communication driver write function.
 *
 * \param	pInst	Pointer to the communication driver instance.
 * \param	pData	Pointer to the data to be written.
 * \param	len		Lenght of the data to be written in BYTES.
 *
 * \details	The write function of a low level communication driver should be 
 * 			of thistype. The function is called when data needs to be sent.
 */

typedef void (*			drv1_fnWrite)(void * pInst,Uint8 * pData,Uint16 len);

/**
 * Interface type. All communication drivers that uses the DRV1 interface should
 * have a public interface of this type.
 */
 
typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	drv1_fnInit				init;		/**< Ptr to init function           */
	drv1_fnSet				set;		/**< Ptr to settings function       */
	drv1_fnWrite			write;		/**< Ptr to write function          */
} drv1_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/



#endif
