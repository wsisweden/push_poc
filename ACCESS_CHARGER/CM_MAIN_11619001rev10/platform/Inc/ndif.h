/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		ndif.h
*
*	\ingroup	NDIF
*
*	\brief		Network Driver Interface.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4475 $ \n
*				\$Date: 2021-04-01 16:28:01 +0300 (to, 01 huhti 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	NDIF	NDIF
*
*	\brief		Network Driver Interface
*
********************************************************************************
*
*	\details	The network driver interface is used by all Ethernet MAC
*				drivers. The network driver takes care of the Physical OSI
*				layer and part of the data link layer.
*
*******************************************************************************/

#ifndef NDIF_H_INCLUDED
#define NDIF_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * \name	EUI-48 address bits
 */

/**@{*/
#define NDIF_EUI48BIT_MULTICAST	(1U<<0)		/**< Multicast address.			*/
#define NDIF_EUI48BIT_LOCAL		(1U<<1)		/**< Locally administered.		*/
/**@}*/

/**
 * \name	Ethernet frame types
 */

/**@{*/
#define NDIF_TYPE_INVALID	0x0000		/**< Invalid EtherType value.		*/
#define NDIF_TYPE_IP		0x0800		/**< IPv4 frame.					*/
#define NDIF_TYPE_ARP       0x0806		/**< ARP frame.						*/
#define NDIF_TYPE_VLAN      0x8100		/**< Tagged VLAN frame.				*/
#define NDIF_TYPE_IPV6		0x86DD		/**< IPv6 frame.					*/
#define NDIF_TYPE_PPPOEDISC 0x8863  	/**< PPP Over Ethernet Discovery Stage.*/
#define NDIF_TYPE_PPPOE     0x8864  	/**< PPP Over Ethernet Session Stage.*/
#define NDIF_TYPE_GOOSE		0x88B8		/**< GOOSE frame.					*/
/**@}*/

/**
 * \name	Status change flags.
 */

/**@{*/
#define	NDIF_STAT_LINKUP	(1<<0)		/**< Link is established			*/
#define NDIF_STAT_LINKDOWN	(1<<1)		/**< Link is lost 					*/
/**@}*/

/**
 * \name	Device capability flags
 */

/**@{*/
#define	NDIF_CAP_BROADCAST	(1<<0)		/**< Device can do broadcast		*/
#define NDIF_OPT_ARP		(1<<1)		/**< Device uses ARP				*/
#define	NDIF_CAP_IGMP		(1<<2)		/**< Device can do IGMP				*/
#define NDIF_OPT_DEFAULT	(1<<3)		/**< This is the default device		*/
/**@}*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

typedef struct ndif__txIf ndif_TxIf;

/**
 * Ethernet device status change type. Used by a parameter to the
 * ndif_statusFn function type.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8					flags;		/**< Status change flags			*/
} ndif_Status;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Init values for FB using the MAC driver.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8 					macAddr[6];	/**< Array containing MAC address	*/
	Uint16					mtu;		/**< Maximum transmission unit		*/
	Uint8					flags;		/**< Driver capability/option flags	*/
	void *					pMacInst;	/**< Pointer to MAC driver instance.*/
	ndif_TxIf const_P *		pTxIf;		/**< Pointer to output interface.	*/
} ndif_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief		Initialization function.
 *
 * \param		pInst	Pointer to the FB instance.
 * \param		pInit	Pointer to initialization values.
 *
 * \details		A FB that is using a network driver should implement a function
 * 				of this type. The function is called from the network driver
 * 				task during initialization.
 */

typedef void  				ndif_InitFn(void * pInst,ndif_Init const_D * pInit);

/**
 *	A chain-buffer element. An array of this type is called a chain-buffer.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	BYTE *					pData;		/**< Pointer to buffer block.		*/
	void *					pBlockInfo;	/**< Pointer to block information.	*/
	Uint16					size;		/**< Size of block.					*/
} ndif_Cbuff;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief		Parameter type for input function.
 *
 * \details		A pointer to an object of this type is provided to the Ethernet
 * 				frame input function. The frame data should include the Ethernet
 * 				header. The preamble should not be included. The frame check
 * 				sequence should not be included in the data.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	ndif_Cbuff *			pData;		/**< CBuff with the received frame.	*/
	Uint16					type;		/**< Frame EtherType				*/
	Uint8					count;		/**< Number of CBuff elements.		*/
} ndif_InData;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief		Ethernet frame input function type.
 *
 * \param		pInst	Pointer to instance of handling FB.
 * \param		pInfo	Pointer to information about RX data.
 *
 * \retval		TRUE	The frame was accepted by the protocol handler and it
 *						will free the buffer when the frame has been processed.
 * \retval		FALSE	The Ethertype is not handled by the protocol handler and
 *						it will not free the buffer.
 * 
 * \details		A function of this type should be implemented in all handler
 *				FBs. The MAC driver should call the function for each received
 *				frame. The MAC driver does not know which EtherTypes the
 *				protocol handlers expects. It should call the input function of
 *				all the protocol handlers until one of them accepts the frame.
 *				
 *				The data should include the whole Ethernet frame header.
 *				
 *				The protocol handler must check the EtherType of the received
 *				frame. Multiple protocol handlers must not accept the same
 *				EtherType. There can be only one protocol handler per project
 *				for each EtherType. The protocol handler documentation should
 *				state which EtherTypes it accepts.
 */

typedef Boolean ndif_InputFn(void * pInst,ndif_InData const_D * pInfo);

/**
 * \brief		Handler activation function.
 *
 * \param		pInst	Pointer to instance of handling FB.
 * \param		pStatus	Link status.
 *
 * \details		This function is called when the Ethernet link status changes 
 *				or when activation has been requested by the handler with the
 *				"get activation" function. A status change is usually the
 * 				network cable being plugged or unplugged.
 */

typedef void ndif_ActivateFn(void * pInst,ndif_Status const_D * pStatus);

/**
 * \brief		Network driver input interface constant type.
 *
 * \details		Each FB that should receive data from a network driver should
 * 				declare a public constant of this type.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	ndif_InitFn * 			pInitFn;	/**< Ptr to init function.			*/
	ndif_InputFn * 			pInputFn;	/**< Ptr to frame input function.	*/
	ndif_ActivateFn *		pStatusFn;	/**< Ptr to status change function.	*/
} ndif_RxIf;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief	Frees memory when data has been transmitted.
 *
 * \param	pCBuffer	Pointer to chain-buffer containing information about
 *						the data that has been sent.
 */

typedef void ndif_FrameSentFn(ndif_Cbuff const_D * pCBuffer);

/**
 * \brief		Output request type.
 *
 * \details		The type is used by the parameter to the output function.
 * 
 *				The pCBuffer should contain the frame to be sent. It should 
 *				include the Ethernet frame header with the source and
 *				destination address. It does not have to include the frame
 *				check sequence. The FCS should be calculated and added by the
 *				MAC driver, preferably with hardware acceleration.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	ndif_Cbuff const_D *	pCBuffer;	/**< Ptr to chain-buffer.			*/
	ndif_FrameSentFn *		pFrameSentFn;/**< Ptr to data sent callback.	*/
	void *					pUtil;		/**< Utility pointer sent to callback*/
	Uint16					count;		/**< Number of buffer elements.		*/
} ndif_Request;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief		Type of the TX activation functions.
 *
 * \param		pInst		Pointer to Ethernet MAC driver instance.
 * \param		pRequest	Pointer to transmit request.
 *
 * \details		Each Ethernet driver should implement a function of this type.
 * 				The function enqueues data for transmission.
 *		
 *				The function may block if the transmit queue is full.
 *				
 */

typedef void	ndif_OutputFn(void * pInst,ndif_Request const_D * pRequest);

/**
 * \brief		Type of activation request functions.
 *
 * \param		pInst		Pointer to Ethernet MAC driver instance.
 *
 * \details		Each Ethernet driver should implement a function of this type.
 * 				The function triggers an activation call back to the caller FB.
 *
 *				The function may be called with tasking disabled but not with
 *				interrupts disabled. It must not be called from an ISR.
 *				
 *				The MAC driver may call the activation function of all protocol
 *				handlers when this function is called.
 */

typedef void	ndif_GetActFn(void * pInst);

/**
 * \brief		Type of multicast frame filter update function.
 *
 * \param		pInst		Pointer to Ethernet MAC driver instance.
 * \param		pMacAddr	Pointer to the multicast MAD address that should be
 *							added or removed from the filter.
 * \param		addAddress	TRUE if the address should be added to the filter.
 *							FALSE if the address should be removed from the
 *							filter.
 * 
 * \retval		TRUE	if the address was added successfully.
 * \retval		FALSE	if the address could not be added. This could mean that 
 *						the list is full.
 * 
 * \details		Adding an MAC address to the filter means that frames sent to
 *				that group address will be passed to the input function.
 *				
 *				The MAC driver does not need to support this function. It may
 *				pass all multicast frames. The address still needs to be
 *				checked on higher layers.
 *
 *				The purpose of the function is to make it possible for the
 *				MAC driver to use hardware filtering when possible.
 */

typedef Boolean	ndif_MultiFiltFn(void * pInst,Uint8 const_D * pMacAddr,Boolean addAddress);

/**
 * \brief		Allocate buffers function.
 *
 * \param		pInst	Pointer to instance data of the buffer handler.
 * \param		pInfo	Pointer to chain buffer array.
 * \param		count	Size of the supplied chain buffer array.
 *
 * \return		Number of buffers that could be allocated.
 *
 * \details		The MAC driver will call a function of this type when it 
 *				allocates buffers to receive data into. Function blocks that 
 *				send data may also call a function of this type to allocate 
 *				buffers for sending data.
 *
 *				If the return value is smaller than the count parameter then
 *				the function may be called again after a while when more memory
 *				might be available.
 *				
 *				The function may allocate memory dynamically from the heap or 
 *				from a statically allocated memory pool.
 *				
 *				The size of the allocated buffer blocks depends on the memory
 *				handler. This makes it possible to use memory pools.
 */

typedef Uint8 ndif_AllocBuffFn(void * pInst,ndif_Cbuff * pData,Uint8 count);

/**
 * \brief		Free buffer.
 *
 * \param		pInst		Pointer to instance data of the buffer handler or
 *							MAC driver instance.
 * \param		pBlockInfo	Pointer to information needed to free the memory. 
 *							This is the pBlockInfo pointer returned by the
 *							allocation function.
 *
 * \return		-
 *
 * \details		The MAC driver will call a function of this type when it needs
 *				to free memory. This might happen if the received frame does not
 *				pass a multicast frame filter implemented in software.
 *				
 *				If a function block that sends data through the MAC driver has
 *				allocated the memory with a ndif_AllocBuffFn type function then
 *				the memory needs to be freed by calling a function of this type.
 */

typedef void ndif_FreeBuffFn(void * pInst, void * pBlockInfo);

/**
 * \brief		Ethernet output interface constant type.
 *
 * \details		Each Ethernet driver should declare a public constant of this
 * 				type. A pointer to the constant is given as an initialization
 * 				parameter to all FBs that will use the Ethernet driver for
 * 				output.
 */

struct ndif__txIf {						/*''''''''''''''''''''''''''' CONST */
	ndif_OutputFn * 	pOutputFn;		/**< Pointer to output function.	*/
	ndif_GetActFn *		pGetActFn;		/**< Pointer to get activation function.*/
	ndif_MultiFiltFn *	pMultiFiltFn;	/**< Pointer to multicast filter
										 * update function.					*/
	ndif_AllocBuffFn *	pAllocBuffFn;	/**< Pointer to memory allocation
										 * function.						*/
	ndif_FreeBuffFn *	pFreeBuffFn;	/**< Poitner to free buffer function. */
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief		Initialize buffer handler function type.
 *
 * \param		blockSize	Size of blocks that the hander allocates.
 * \param		blockCount	Maximum number of blocks that can be allocated.
 *
 * \return		Pointer to the buffer handler instance data.
 *
 * \details		The buffer handler does not have to limit the block count to
 *				the value supplied to this function.
 */

typedef void * ndif_InitBuffFn(Uint16 blockSize,Uint8 blockCount);

/**
 * Type used by buffer handler interface constants. Each buffer handler will
 * declare a public constant of this type. The MAC driver will allocate and
 * free buffers through this interface.
 */

typedef struct {						/*'''''''''''''''''''''''''' CONST */
	ndif_InitBuffFn *		pInitBuffFn;/**< Pointer to init function.		*/
	ndif_AllocBuffFn *		pAllocBuffFn;/**< Pointer to allocation function.*/
	ndif_FreeBuffFn *		pFreeBuffFn;/**< Pointer to free function.		*/
} ndif_BuffHanIf;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern ndif_BuffHanIf const_P ndif_dynamicMemHandler;
extern ndif_BuffHanIf const_P ndif_poolMemHandler;

/******************************************************//** \endcond pub_decl */

#endif
