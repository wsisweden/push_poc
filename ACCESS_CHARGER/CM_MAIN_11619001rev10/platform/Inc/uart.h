/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc/UART.H
*
*	\ingroup	UART
*
*	\brief		Public declarations of the UART module
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4653 $ \n
*				\$Date: 2021-09-10 12:26:57 +0300 (pe, 10 syys 2021) $ \n
*				\$Author: tlmari $
*
*******************************************************************************/

#ifndef UART_H_INCLUDED
#define UART_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "drv1.h"
#include "sys.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * Baud rate ids. These id numbers are used to store the baud rate in a 
 * 8-bit register. Using a 8-bit register restricts the possible
 * baud rate options.
 */

enum uart_baudRate {
	UART_BAUDRATE_1200 = 0,				/**< 1200 baud						*/
	UART_BAUDRATE_2400,					/**< 2400 baud						*/
	UART_BAUDRATE_4800,					/**< 4800 baud						*/
	UART_BAUDRATE_9600,					/**< 9600 baud						*/
	UART_BAUDRATE_19200,				/**< 19200 baud						*/
	UART_BAUDRATE_38400,				/**< 38400 baud						*/
	UART_BAUDRATE_57600,				/**< 57600 baud						*/
	UART_BAUDRATE_115200,				/**< 115200 baud					*/

	UART_BAUDRATE_COUNT					/**< Number of possible baud rates 	*/
};

/**
 * List of possible data bit number options. These values should be used in the
 * config struct data bits field.
 */

enum uart_dataBits {
	UART_DATABITS_5 = 5,				/**< Five data bits					*/
	UART_DATABITS_6,					/**< Six data bits					*/
	UART_DATABITS_7,					/**< Seven data bits				*/
	UART_DATABITS_8						/**< Eight data bits				*/
};

/**
 * List of possible parity modes. These values should be used in the config
 * struct parity field.
 */

enum uart_parity {
	UART_PARITY_EVEN = 0,				/**< Even parity mode				*/
	UART_PARITY_ODD,					/**< Odd parity mode				*/
	UART_PARITY_NONE					/**< No parity bit					*/
};

/**
 * List of possible stop bit options. These values should be used in the
 * config struct stopbits field.
 */

enum uart_stopBits {
	UART_STOPBITS_1 = 1,				/**< One stop bit					*/
	UART_STOPBITS_2						/**< Two stop bits					*/
};

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_W32

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	char *					port;		/**< Port string	("COMN")		*/
										/**					("\\\\.\\com16")*/
	Uint32					baudRate;	/**< Baud rate						*/
	Uint8					dataBits;	/**< Data bits						*/
	char *					parity;		/**< Parity			("N" = None)	*/
	Uint8					stopBits;	/**< Stop bits						*/
	Boolean					silentFail;	/**< Continue even if port fails.	*/
	Uint8					cbIndex;	/**< Callback array index.			*/
} uart_Config;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#else

/**
 * \brief	UART configuration structure.
 *
 * \details	This structure should be used to set the configuration for a UART
 *			instance. It is usually used in project.c and given as an init
 *			parameter to the function block that uses the UART driver.
 *			
 *			The register handles can be set to SYS_BAD_REGISTER if no registers
 *			are used. Then the config must be given in the baudRate, dataBits,
 *			parity and stopBits fields.
 *
 *			The configuration struct is different for w32 target.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	sys_RegHandle			baudRateHandle;/**< Register handle for baud rate*/
	sys_RegHandle			dataBitsHandle;/**< Register handle for data bits*/
	sys_RegHandle			parityHandle;/**< Register handle for parity mode*/
	sys_RegHandle			stopBitsHandle;/**< Register handle for stop bits*/
	Uint32					baudRate;	/**< Baud rate (e.g. 9600, 19200)	*/
	Uint8					dataBits;	/**< Data bits                      */
	Uint8					parity;		/**< Parity                         */
	Uint8					stopBits;	/**< Stop bits                      */
	Uint32					pllHz;		/**< PLL output frequency.			*/
	Uint8					clkDiv;		/**< Peripheral clock divider.		*/
	Uint8					cbIndex;	/**< Callback array index.			*/
} uart_Config;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif

/**
 * \brief	Called when one data byte has been received.
 *
 * \param	data	The received data byte.
 * \param	pUtil	Utility pointer. Usually it contains the instance pointer
 *					of the function block that is using the driver.
 *
 * \details	The function block that is using the UART driver must implement
 *			this function.
 *
 * \note	This function is called from ISR.
 */

typedef void (*uart_fnDataReceived)(Uint8 data, void * pUtil);

/**
 * \brief	Called when all data has been copied to the UART TX buffer.
 *
 * \param	pUtil	Utility pointer. Usually it contains the instance pointer
 *					of the function block that is using the driver.
 *
 * \details	The function block that is using the UART driver must implement
 *			this function.
 *
 *			The buffer that was sent to the send function can be used again
 *			after this function is called.
 *
 * \note	This function is called from ISR.
 */

typedef void (*uart_fnDataSent)(void * pUtil);

/**
 * Type used to configure callback functions for UART. The configuration is
 * usually done in project.c.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	uart_fnDataReceived		fnDataReceived;/**< Callback for data receive   */
	uart_fnDataSent			fnDataSent; /**< Callback for all data sent     */
} uart_Callback;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Only for backward compatibility. The uart_Interface should not be used in
 * new projects. drv1_Interface should be used instead.
 */

typedef drv1_Interface		uart_Interface;


typedef struct uart__inst uart_Inst;

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_ARM7

extern const_P drv1_Interface	uart0_interface;
extern const_P drv1_Interface	uart1_interface;
extern const_P drv1_Interface	uart2_interface;
extern const_P drv1_Interface	uart3_interface;

void * uart_init0(void const_P *, void *);
void uart_set0(void);
void uart_write0(void *, Uint8 *, Uint16);

void * uart_init1(void const_P *, void *);
void uart_set1(void);
void uart_write1(void *, Uint8 *, Uint16);

void * uart_init2(void const_P *, void *);
void uart_set2(void);
void uart_write2(void *, Uint8 *, Uint16);

void * uart_init3(void const_P *, void *);
void uart_set3(void);
void uart_write3(void *, Uint8 *, Uint16);

#elif TARGET_SELECTED & TARGET_CM0

extern const_P drv1_Interface	uart0_interface;
extern const_P drv1_Interface	uart1_interface;
extern const_P drv1_Interface	uart1_interface2;
extern const_P drv1_Interface	uart2_interface;
extern const_P drv1_Interface	uart3_interface;

#elif TARGET_SELECTED & TARGET_CM3

extern const_P drv1_Interface	uart0_interface;
extern const_P drv1_Interface	uart1_interface;
extern const_P drv1_Interface	uart1_interface2;
extern const_P drv1_Interface	uart2_interface;
extern const_P drv1_Interface	uart3_interface;

#elif TARGET_SELECTED & TARGET_CM4

extern const_P drv1_Interface	uart0_interface;
extern const_P drv1_Interface	uart1_interface;
extern const_P drv1_Interface	uart1_interface2;
extern const_P drv1_Interface	uart2_interface;
extern const_P drv1_Interface	uart3_interface;

#elif TARGET_SELECTED & TARGET_S4F4

extern const_P drv1_Interface	uart_if1TxPB6;
extern const_P drv1_Interface	uart_if1NoInit;
extern const_P drv1_Interface	uart_if2NoInit;

#elif TARGET_SELECTED & TARGET_S3F2

extern const_P drv1_Interface	uart_if1TxPB6;
extern const_P drv1_Interface	uart_if1NoInit;
extern const_P drv1_Interface	uart_if2NoInit;
extern const_P drv1_Interface	uart_if3NoInit;

#elif TARGET_SELECTED & TARGET_W32

/* Define for backward compatibility */
#define uart_interface			uart0_interface;

extern const_P drv1_Interface	uart0_interface;
extern const_P drv1_Interface	uart1_interface;
extern const_P drv1_Interface	uart1_interface2;
extern const_P drv1_Interface	uart2_interface;
extern const_P drv1_Interface	uart3_interface;

/*
 *  The following functions are only for backward compatibility.
 */

extern void * 					uart_init(void const_P *, void *);
extern void 					uart_set(void);
extern void 					uart_write(void *, Uint8 *, Uint16);

#endif

/***************************************************************//** \endcond */

#endif /* UART_H_INCLUDED */
