/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	Name: MEM.H
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	MEM
*
*	\brief		Public declarations of MEM.
*
*	\note		
*
*	\version	
*
*******************************************************************************/

#ifndef MEM_H_INCLUDED
#define MEM_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#ifndef	TOOLS_H_INCLUDED
# error	"TOOLS.H must be included before MEM.H"
#endif

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	This can be switched to 1 when debugging with the W32 target to enabled 
 *	additional runtime checks. All memory allocations will then be mapped to
 *	standard library malloc.
 */

#if TARGET_SELECTED == TARGET_W32
#define MEM__USE_STDLIB		0
#else
#define MEM__USE_STDLIB		0
#endif

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void			mem_add(void **,void *,size_t);

#if MEM__USE_STDLIB == 0
void *			mem_alloc(void **,size_t);
void *			mem_reserve(void **,size_t);
void *			mem_realloc(void *,size_t);
void			mem_free(void *);
#else
# include <stdlib.h>
# define		mem_alloc(ppCat_, size_)		malloc(size_)
# define		mem_reserve(ppCat_, size_)		malloc(size_)
# define		mem_realloc(pData_, newSize_)	realloc((pData_), (newSize_))
# define		mem_free(pData_)				free(pData_)
#endif

void *			mem_catSave(void **);
Boolean 		mem_catLoad(void **,void *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern void *	mem_fast;
extern void *	mem_normal;

/***************************************************************//** \endcond */

#endif
