/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\brief		Public defines for the MDIO bus.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 2779 $ \n
*				\$Date: 2015-05-07 11:33:05 +0300 (to, 07 touko 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef MDIO_H_INCLUDED
#define MDIO_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * Standard MDIO registers.
 *
 * \see		IEEE 802.3 22.2.4.
 */

enum {
	MDIO_REG_BMCR = 0,				/**< Basic control register.			*/
	MDIO_REG_BMSR,					/**< Basic status register.				*/
	MDIO_REG_PHYIDR1,				/**< PHY Identifier byte 1.				*/
	MDIO_REG_PHYIDR2,				/**< PHY Identifier byte 2.				*/
	MDIO_REG_ANAR,					/**< Auto-Negotiation Advertisement.	*/
	MDIO_REG_ANLPAR,				/**<
									 * Auto-Negotiation Link Partner Base 
									 * Page Ability.						*/
	MDIO_REG_ANER,					/**< Auto-Negotiation Expansion.		*/
	MDIO_REG_ANNPTR,				/**< Auto-Negotiation Next Page Transmit.*/
	MDIO_REG_ANLPRNP,				/**<
									 * Auto-Negotiation Link Partner Received
									 * Next Page.							*/
	MDIO_REG_MSCTRL,				/**< MASTER-SLAVE Control Register.		*/
	MDIO_REG_MSSTAT,				/**< MASTER-SLAVE Status Register.		*/
	MDIO_REG_PSECTRL,				/**< PSE Control register.				*/
	MDIO_REG_PSESTAT,				/**< PSE Status register.				*/
	MDIO_REG_MMDCTRL,				/**< MMD Access Control Register.		*/
	MDIO_REG_MMDADDR,				/**< MMD Access Address Data Register.	*/
	MDIO_REG_EXTSTAT,				/**< Extended Status.					*/
};

/**
 * \name	Basic control register bits
 *
 * \see		IEEE 802.3 22.2.4.1.
 */

/*@{*/
#define MDIO_BMCR_SP1000 	(1<<6)		/**< MSB of Speed (1000).			*/
#define MDIO_BMCR_CTST 		(1<<7)		/**< Collision test.				*/
#define MDIO_BMCR_DUPLEX	(1<<8)		/**< 1=Full duplex. 0=Half duplex.	*/
#define MDIO_BMCR_RE_AN		(1<<9)		/**< 1=Restart auto-negotiation.	*/
#define MDIO_BMCR_ISOLATE	(1<<10)		/**< 1=Electric isolation of PHY.	*/
#define MDIO_BMCR_PD		(1<<11)		/**< 1=Power down mode.				*/
#define MDIO_BMCR_AN		(1<<12)		/**< 1=Auto-Nego enabled.			*/
#define MDIO_BMCR_SP100		(1<<13)		/**< Speed select. 1=100Mbps 0=10Mbps.*/
#define MDIO_BMCR_LB		(1<<14)		/**< 1=Loopback mode. 0=Normal mode.*/
#define MDIO_BMCR_RESET		(1<<15)		/**< 1=Software reset, Self clearing.*/
/*@}*/

/**
 * \name	Basic status register bits
 *
 * \see		IEEE 802.3 22.2.4.2.
 */

/*@{*/
#define MDIO_BMSR_ERCAP			(1<<0)  /**< Extended-reg capability.		*/
#define MDIO_BMSR_JCD			(1<<1)  /**< Jabber detected.				*/
#define MDIO_BMSR_LINK_ESTAB	(1<<2)	/**< Link is up.					*/
#define MDIO_BMSR_NO_AUTO		(1<<3)	/**< Able to do auto-negotiation.	*/
#define MDIO_BMSR_REMOTEFAULT	(1<<4)	/**< Remote fault detected.			*/
#define MDIO_BMSR_AUTO_DONE		(1<<5)	/**< Auto-negotiation complete.		*/
#define MDIO_BMSR_PRESUPPRESS	(1<<6)	/**<
										 * PHY will accept management frames
										 * with preamble suppressed. 		*/
#define MDIO_BMSR_UNIDIR		(1<<7)	/**<
										 * PHY able to transmit from media
										 * independent interface regardless of
										 * whether the PHY has determined that
										 * a valid link has been established.*/
#define MDIO_BMSR_ESTATEN		(1<<8)  /**< Extended Status in R15			*/
#define MDIO_BMSR_100HALF2  	(1<<9)  /**< Can do 100BASE-T2 HDX			*/
#define MDIO_BMSR_100FULL2 		(1<<10) /**< Can do 100BASE-T2 FDX			*/
#define MDIO_BMSR_10BE_HALF		(1<<11)	/**< Can do 10mbps, half-duplex		*/
#define MDIO_BMSR_10BE_FULL		(1<<12)	/**< Can do 10mbps, full-duplex		*/
#define MDIO_BMSR_100TX_HALF	(1<<13)	/**< Can do 100mbps, half-duplex	*/
#define MDIO_BMSR_100TX_FULL	(1<<14)	/**< Can do 100mbps, full-duplex	*/
#define MDIO_BMSR_100BE_T4		(1<<15)	/**< Can do 100mbps, 4k packets		*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
