/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		lib.h
*
*	\ingroup	LIB
*
*	\brief		Public declarations for commonly used functions.
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	LIB		LIB
*
*	\brief		Collection of common functions.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

#ifndef LIB_H_INCLUDED
#define LIB_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED != TARGET_AVR
#include <string.h> /* Needed for memcpy() and memcmp() */
#endif

#include "ptrs.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	Return status values for Ring Buffer functions.
 */

/*@{*/									/*''''''''''''''''''''''''''''''''''*/
#define	LIB_RBUF_OK		((lib_RBufStat)1)/**< Function call succeeded.		*/
#define LIB_RBUF_FULL	((lib_RBufStat)2)/**< Buffer is full at the time 
										  *	when it was tested.				*/
#define LIB_RBUF_OFLOW	((lib_RBufStat)3)/**< Value can't be added. Buffer 
										 * is full.							*/
#define LIB_RBUF_EMPTY	((lib_RBufStat)4)/**< Last value from buffer returned*/
#define LIB_RBUF_UFLOW	((lib_RBufStat)5)/**< Buffer is empty at the time 
										  *	when it was tested.				*/
/*@}*/									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Values that are unknown to the byte stream conversion functions.
 */

/*@{*/									/*''''''''''''''''''''''''''''''''''*/
#define LIB_SFE_RESERVED_L	0x55		/* The two characters that must not	*/
#define LIB_SFE_RESERVED_H	0xAA		/*	occur in 254-packed data		*/
/*@}*/									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#define LIB_FIFO_EMPTY		{ NULL, NULL }

/**
 * \brief	Initialize FIFO.
 * 
 * \param	pHdr	Ptr to the FIFO header element.
 */
#define lib_fifoInit(pHdr)	(pHdr)->pNewest = (pHdr)->pOldest = NULL

/**
 *	\brief		Append a new element in the specified FIFO.
 *
 *	\param		pHdr_ 		Pointer to the FIFO header element.
 *	\param		pPutThis_ 	Pointer to the data element (its uninitialized
 *							link pointer) to place in FIFO.
 *
 *	\note		This is NOT an atomic operation! Calling application must take
 *				care of protecting the FIFO e.g. against a simultaneous
 *				lib_figoGet() call.
 */
#define lib_fifoPutNonProt(pHdr_, pPutThis_) do {					\
	*(pPutThis_) = NULL;											\
	if ((pHdr_)->pNewest != NULL)									\
		*(void **) (pHdr_)->pNewest = (void *) (pPutThis_);			\
	(pHdr_)->pNewest = (void *) (pPutThis_);						\
	if ((pHdr_)->pOldest == NULL)									\
		(pHdr_)->pOldest = (void *) (pPutThis_);					\
} while (FALSE)

/**
 *	\brief		Get the oldest element from the specified FIFO.
 *
 *	\param		pHdr_ 		Pointer to the FIFO header element, previously
 *							initialized either to LIB_FIFO_EMPTY (static) or
 *							using the lib_fifoInit() macro (runtime).
 *	\param		ppElement_	Pointer to pointer where the macro will store the
 *							address of the oldest element in FIFO, now removed,
 *							or NULL if the FIFO was empty.
 *
 *	\note		This is NOT an atomic operation! Calling application must take
 *				care of protecting the FIFO e.g. against a simultaneous
 *				lib_figoGet() call.
 */
#define lib_fifoGetNonProt(pHdr_, ppElement_)	do {				\
	*(void ***)(ppElement_) = (void **) (pHdr_)->pOldest;			\
	if (*(void ***)(ppElement_) != NULL) {							\
		if(((pHdr_)->pOldest = **(void ***)(ppElement_)) == NULL)	\
			(pHdr_)->pNewest = NULL;								\
		**(void ***)(ppElement_) = NULL;							\
	}																\
} while (FALSE)


#define lib_Uint8ToAscii(n_,p_,m_)	lib_Uint16ToAscii(n_,p_,m_)
#define lib_Int8ToAscii(n_,p_,m_)	lib_Int16ToAscii(n_,p_,m_)

/*
 *	Target specific tools to operate with const_P type data.
 */

#if TARGET_SELECTED == TARGET_AVR
# define lib_pMemcmp(pC_,pC_P_,nSize_)	lib__pMemcmp(pC_,pC_P_,nSize_)
# define lib_pMemcpy(pC_,pC_P_,nSize_)	lib__pMemcpy(pC_,pC_P_,nSize_)
/* TODO: */
# define lib_pStrcmp(pC_,pC_P_)			lib__pStrcmp(pC_,pC_P_)
# define lib_pStrcpy(pC_,pC_P_)			lib__pStrcpy(pC_,pC_P_)
# define lib_pStrerror(nErrNum_)		lib__pStrerror(nErrNum_)
# define lib_pStrlen(pC_P_)				lib__pStrlen(pC_P_)
# define lib_pStrncmp(pC_,pC_P_,nSize_)	lib__pStrncmp(pC_,pC_P_,nSize_)
# define lib_pStrncpy(pC_,pC_P_,nSize_)	lib__pStrncpy(pC_,pC_P_,nSize_)
#else
# define lib_pMemcmp(pC_,pC_P_,nSize_)	memcmp(pC_,pC_P_,nSize_)
# define lib_pMemcpy(pC_,pC_P_,nSize_)	memcpy(pC_,pC_P_,nSize_)
# define lib_pStrcmp(pC_,pC_P_)			strcmp(pC_,pC_P_)
# define lib_pStrcpy(pC_,pC_P_)			strcpy(pC_,pC_P_)
# define lib_pStrerror(nErrNum_)		strerror(nErrNum_)
# define lib_pStrlen(pC_P_)				strlen(pC_P_)
# define lib_pStrncmp(pC_,pC_P_,nSize_)	strncmp(pC_,pC_P_,nSize_)
# define lib_pStrncpy(pC_,pC_P_,nSize_)	strncpy(pC_,pC_P_,nSize_)
#endif

/*
 *	Repeating macros; "C" stand for comma and "S" for space separated values.
 *	Repeat count is to be appended with the token pasting operator (##).
 */

#define LIB_REPC1(n)	n
#define LIB_REPC2(n)	n,n
#define LIB_REPC3(n)	n,n,n
#define LIB_REPC4(n)	n,n,n,n
#define LIB_REPC5(n)	n,n,n,n,n
#define LIB_REPC6(n)	n,n,n,n,n,n
#define LIB_REPC7(n)	n,n,n,n,n,n,n
#define LIB_REPC8(n)	n,n,n,n,n,n,n,n
#define LIB_REPC9(n)	n,n,n,n,n,n,n,n,n
#define LIB_REPC10(n)	n,n,n,n,n,n,n,n,n,n
#define LIB_REPC11(n)	n,n,n,n,n,n,n,n,n,n,n
#define LIB_REPC12(n)	n,n,n,n,n,n,n,n,n,n,n,n
#define LIB_REPC13(n)	n,n,n,n,n,n,n,n,n,n,n,n,n
#define LIB_REPC14(n)	n,n,n,n,n,n,n,n,n,n,n,n,n,n
#define LIB_REPC15(n)	n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
#define LIB_REPC16(n)	n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n

#define LIB_REPS1(n)	n
#define LIB_REPS2(n)	n n
#define LIB_REPS3(n)	n n n
#define LIB_REPS4(n)	n n n n
#define LIB_REPS5(n)	n n n n n
#define LIB_REPS6(n)	n n n n n n
#define LIB_REPS7(n)	n n n n n n n
#define LIB_REPS8(n)	n n n n n n n n
#define LIB_REPS9(n)	n n n n n n n n n
#define LIB_REPS10(n)	n n n n n n n n n n
#define LIB_REPS11(n)	n n n n n n n n n n n
#define LIB_REPS12(n)	n n n n n n n n n n n n
#define LIB_REPS13(n)	n n n n n n n n n n n n n
#define LIB_REPS14(n)	n n n n n n n n n n n n n n
#define LIB_REPS15(n)	n n n n n n n n n n n n n n n
#define LIB_REPS16(n)	n n n n n n n n n n n n n n n n

#if TARGET_MEM_ALIGN > 1
# define lib_memAlign(p_,s_) lib__memAlign(p_,s_)
#else
# define lib_memAlign(p_,s_) ((void *)(p_))
#endif

/**
 *	\brief		Initializes a 32-bit Ethernet CRC.
 *
 * 	\return		Starting value for the CRC.
 *
 *	\details	This macro should be used instead of calling the functions
 *				directly. When this macro is used it is easy to switch
 *				between the fast, small and hardware versions.
 */
#define lib_crc32EthernetInit() lib_crc32EthIf_p->pInitFn()

/**
 *	\brief		Appends bytes to a 32-bit Ethernet CRC.
 *	
 *	\param		crc_	Starting value or previously returned value.
 *	\param		pData_	Pointer to data to calculate CRC from.
 *	\param		size_	Size of data to calculate CRC from.
 *
 *	\details	This macro should be used instead of calling the functions
 *				directly. When this macro is used it is easy to switch
 *				between the fast, small and hardware versions.
 */
#define lib_crc32Ethernet(crc_, pData_, size_) \
	lib_crc32EthIf_p->pUpdFn(crc_, pData_, size_)

/**
 *	\brief		Returns the final value of a 32-bit Ethernet CRC.
 *
 * 	\param		crc_	The value returned by the CRC update function.
 * 
 * 	\return		Final value for the CRC.
 *
 *	\details	This macro should be used instead of calling the functions
 *				directly. When this macro is used it is easy to switch
 *				between the fast, small and hardware versions.
 */
#define lib_crc32EthernetGet(crc_) lib_crc32EthIf_p->pGetFn(crc_)

/**
 *	\brief		Calculates a 8bit Dallas/Maxim "Dow" CRC.
 *	
 *	\param		crc_	Starting value.
 *	\param		pData_	Pointer to data to calculate CRC from.
 *	\param		size_	Size of data to calculate CRC from.
 *
 *	\details	This macro should be used instead of calling the functions
 *				directly. When this macro is used it is easy to switch
 *				between the fast and small function versions.
 */
#define lib_crc8DallasMaxim(crc_, pData_, size_) \
	lib_crc8MaximIf_p->pUpdFn(crc_, pData_, size_);

/**
 *	\brief		Calculates a 16bit Kermit CRC.
 *	
 *	\param		crc_	Starting value.
 *	\param		pData_	Pointer to data to calculate CRC from.
 *	\param		size_	Size of data to calculate CRC from.
 *
 *	\details	This macro should be used instead of calling the functions
 *				directly. When this macro is used it is easy to switch
 *				between the fast and small function versions.
 */
#define lib_crc16Kermit(crc_, pData_, size_) \
	lib_crc16Ker mitIf_p->pUpdFn(crc_, pData_, size_);

/**
 *	\brief		Allocate a memory block from the pool.
 *
 *	\param		pPool_	Pointer to the pool structure.
 *
 *	\return		Pointer to the allocated block. NULL if the pool is empty.
 */

#define lib_poolAlloc(pPool_)												\
	(void *) lib_fifoGet(pPool_)

/**
 *	\brief		Put the supplied memory block back into the pool.
 *
 *	\param		pPool_		Pointer to the pool structure.
 *	\param		pBlock_		Pointer to the block that should be put back into
 *							the pool.
 *
 *	\details
 */

#define lib_poolFree(pPool_, pBlock_)										\
	lib_fifoPut((pPool_), (void **) (void *) (pBlock_))


#if (TARGET_SELECTED & TARGET_CM3) == TARGET_CM3
# define lib_reverse32(pIn_,pOut_) *(pOut_) = __REV(*(pIn_))
# define lib_reverse16(pIn_,pOut_) *(pOut_) = __REV16(*(pIn_))

#elif (TARGET_SELECTED & TARGET_CM4) == TARGET_CM4
# define lib_reverse32(pIn_,pOut_) *(pOut_) = __REV(*(pIn_))
# define lib_reverse16(pIn_,pOut_) *(pOut_) = __REV16(*(pIn_))

#elif (TARGET_SELECTED & TARGET_CM0) == TARGET_CM0
# define lib_reverse32(pIn_,pOut_) *(pOut_) = __REV(*(pIn_))
# define lib_reverse16(pIn_,pOut_) *(pOut_) = __REV16(*(pIn_))

#elif (TARGET_SELECTED & TARGET_ARM7) == TARGET_ARM7
# define lib_reverse32(pIn_,pOut_)					\
	*(pOut_) =										\
        (*(pIn_) >> 8 | *(pIn_) << 24) ^ (			\
            ((*(pIn_) >> 16 | *(pIn_) << 16) ^		\
            *(pIn_)) & (Uint32) 0xFF00FFFFUL		\
        ) >> 8

# define lib_reverse16(pIn_,pOut_)					\
	*(pOut_) =										\
		((Uint16)(*(pIn_)) << 8U					\
        | (Uint16)(*(pIn_)) >> 8U)

#else
/**
 *	\brief			Reverse byte order of a 32-bit value.
 *	
 *	\param [in]		pIn_	Pointer to 32-bit variable containing the value that
 *							should be reversed.
 *	\param [out]	pOut_	Pointer to 32-bit variable where the reversed value 
 *							will be stored.
 *	
 *	\detail			The source and destination variable may be the same
 *					variable.
 */
# define lib_reverse32(pIn_,pOut_) {				\
	Uint8 lib__temp = ((Uint8 *) (pIn_))[0];		\
    ((Uint8 *) (pOut_))[0] = ((Uint8 *) (pIn_))[3];	\
    ((Uint8 *) (pOut_))[3] = lib__temp;				\
    lib__temp = ((Uint8 *) (pIn_))[1];				\
    ((Uint8 *) (pOut_))[1] = ((Uint8 *) (pIn_))[2];	\
    ((Uint8 *) (pOut_))[2] = lib__temp;				\
}

/**
 *	\brief			Reverse byte order of a 16-bit value.
 *	
 *	\param [in]		pIn_	Pointer to 16-bit variable containing the value that
 *							should be reversed.
 *	\param [out]	pOut_	Pointer to 16-bit variable where the reversed value 
 *							will be stored.
 *	
 *	\detail			The source and destination variable may be the same
 *					variable.
 */
# define lib_reverse16(pIn_,pOut_)					\
	*(pOut_) =										\
        *(Uint16 *)(pIn_) << 8U						\
        | *(Uint16 *)(pIn_) >> 8U
#endif 


#if TARGET_ENDIANNESS == TARGET_LITTLE_ENDIAN

/**
 *	\brief			Convert 32-bit big-endian value to native endianness.
 *	
 *	\param [in]		pIn_	Pointer to 32-bit variable with big-endian data.
 *	\param [out]	pOut_	Pointer to 32-bit destination variable.
 */
#define lib_endianFromBig32(pIn_,pOut_)	lib_reverse32(pIn_,pOut_)

/**
 *	\brief			Convert 16-bit big-endian value to native endianness.
 *	
 *	\param [in]		pIn_	Pointer to 16-bit variable with big-endian data.
 *	\param [out]	pOut_	Pointer to 16-bit destination variable.
 */
#define lib_endianFromBig16(pIn_,pOut_)	lib_reverse16(pIn_,pOut_)

/**
 *	\brief			Convert 32-bit native endianness value to big-endian format.
 *	
 *	\param [in]		pIn_	Pointer to 32-bit variable with data in native 
 *							endianness.
 *	\param [out]	pOut_	Pointer to 32-bit destination variable.
 */
#define lib_endianToBig32(pIn_,pOut_)		lib_reverse32(pIn_,pOut_)

/**
 *	\brief			Convert 16-bit native endianness value to big-endian format.
 *	
 *	\param [in]		pIn_	Pointer to 16-bit variable with data in native 
 *							endianness.
 *	\param [out]	pOut_	Pointer to 16-bit destination variable.
 */
#define lib_endianToBig16(pIn_,pOut_)		lib_reverse16(pIn_,pOut_)

/**
 *	\brief			Convert 32-bit little-endian value to native endianness.
 *	
 *	\param [in]		pIn_	Pointer to 32-bit variable with little-endian data.
 *	\param [out]	pOut_	Pointer to 32-bit destination variable.
 */
#define lib_endianFromLittle32(pIn_,pOut_)	*(pOut_) = *(pIn_)

/**
 *	\brief			Convert 16-bit little-endian value to native endianness.
 *	
 *	\param [in]		pIn_	Pointer to 16-bit variable with little-endian data.
 *	\param [out]	pOut_	Pointer to 16-bit destination variable.
 */
#define lib_endianFromLittle16(pIn_,pOut_)	*(pOut_) = *(pIn_)

/**
 *	\brief			Convert 32-bit native endianness value to little-endian
 *					format.
 *	
 *	\param [in]		pIn_	Pointer to 32-bit variable with data in native 
 *							endianness.
 *	\param [out]	pOut_	Pointer to 32-bit destination variable.
 */
#define lib_endianToLittle32(pIn_,pOut_)	*(pOut_) = *(pIn_)

/**
 *	\brief			Convert 16-bit native endianness value to little-endian
 *					format.
 *	
 *	\param [in]		pIn_	Pointer to 16-bit variable with data in native 
 *							endianness.
 *	\param [out]	pOut_	Pointer to 16-bit destination variable.
 */
#define lib_endianToLittle16(pIn_,pOut_)	*(pOut_) = *(pIn_)

#else /* Some kind of middle-endianness can also be handled here */

# define lib_endianFromBig32(pIn_,pOut_)	*(pOut_) = *(pIn_)
# define lib_endianFromBig16(pIn_,pOut_)	*(pOut_) = *(pIn_)
# define lib_endianToBig32(pIn_,pOut_)		*(pOut_) = *(pIn_)
# define lib_endianToBig16(pIn_,pOut_)		*(pOut_) = *(pIn_)

# define lib_endianFromLittle32(pIn_,pOut_)	lib_reverse32(pIn_,pOut_)
# define lib_endianFromLittle16(pIn_,pOut_)	lib_reverse16(pIn_,pOut_)
# define lib_endianToLittle32(pIn_,pOut_)	lib_reverse32(pIn_,pOut_)
# define lib_endianToLittle16(pIn_,pOut_)	lib_reverse16(pIn_,pOut_)

#endif

#if TARGET_SELECTED & TARGET_CM3 \
	|| TARGET_SELECTED & TARGET_CM4 \
	|| TARGET_SELECTED & TARGET_SM4 \
	|| TARGET_SELECTED & TARGET_S3F2 \
	|| TARGET_SELECTED & TARGET_S4F4
# define lib_clz8(inVal_) (__CLZ(inVal_) - 24)
# define lib_clz16(inVal_) (__CLZ(inVal_) - 16)
# define lib_clz32(inVal_) __CLZ(inVal_)

#else

extern Uint8 const_P lib__clzBitTable[256];

/**
 *	\brief			Count leading zeros from the supplied Uint8 variable.
 *	
 *	\param [in] 	inVal_	Uint8 variable containing the value. May not be
 *							zero.
 *	
 *	\detail			Counts the number of leading zeros in inVal_ and returns the
 *					result. The result is 7 if only bit 0 is set in the
 *					supplied value, and zero if bit 7 is set.
 *
 */

#define lib_clz8(inVal_) (lib__clzBitTable[inVal_] & 0x07)

/**
 *	\brief			Count leading zeros from the supplied Uint16 variable.
 *	
 *	\param		 	inVal	Uint16 variable containing the value. May not be
 *							zero.
 *	
 *	\detail			Counts the number of leading zeros in inVal and returns the
 *					result. The result is 15 if only bit 0 is set in the
 *					supplied value, and zero if bit 15 is set.
 */

INLINE Uint8 lib_clz16(
	Uint16					inVal
) {
	Uint8 leadingZeros;
	Uint8 tableIndex;
	
	leadingZeros = 0;
	tableIndex = (Uint8) (inVal >> 8);
	if (tableIndex == 0) {
		tableIndex = (Uint8) inVal;
		leadingZeros = 8;
	}

	return leadingZeros + lib_clz8(tableIndex);
}

/**
 *	\brief			Count leading zeros from the supplied Uint32 variable.
 *	
 *	\param		 	inVal	Uint16 variable containing the value. May not be
 *							zero.
 *	
 *	\detail			Counts the number of leading zeros in inVal and returns the
 *					result. The result is 31 if only bit 0 is set in the
 *					supplied value, and zero if bit 31 is set.
 */

INLINE Uint8 lib_clz32(
	Uint32					inVal
) {
	Uint8 leadingZeros;
	Uint8 tableIndex;
	Uint16 tempVal;
	
	tempVal = (Uint16) (inVal >> 16);
	if (tempVal == 0) {
		leadingZeros = 16;	
		tableIndex = (Uint8) (inVal >> 8);
		
		if (tableIndex == 0) {
			tableIndex = (Uint8) inVal;
			leadingZeros = 24;
		}
		
	} else {
		leadingZeros = 0;
		tableIndex = (Uint8) (inVal >> 24);
		
		if (tableIndex == 0) {
			tableIndex = (Uint8) tempVal;
			leadingZeros = 8;
		}
	}
	
	return leadingZeros + lib_clz8(tableIndex);
}
#endif

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Generic fifo. Can store any datatype.
 */
 
typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	void *					pNewest;	/**< The most recent element        */
	void *					pOldest;	/**< The least recent element       */
} lib_FifoType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Fifo cursor. This type is used to store the state while traversing the FIFO.
 */
 
typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	void *					pCur;		/**<                                */
} lib_FifoCursor;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Types for Ring Buffer size information and indexing.
 */

typedef Uint8				lib_RBufSDim;
typedef Uint16				lib_RBufMDim;

/**
 *	Type for status return values of Ring Buffer functions.
 */

typedef Uint8				lib_RBufStat;

/**
 *	Tool set for Ring Buffer operations.
 */

typedef struct lib_rBufToolSet {		/*''''''''''''''''''''''''''''''''''*/
	lib_RBufStat (*			lib_rBPut)(void *,void const_D *);
	lib_RBufStat (*			lib_rBGet)(void *,void *);
} lib_RBufToolSet;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Information on a byte stream.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8 *					bufPtr;		/* Ptr to the next (free) buf byte	*/
	Uint8					bufLen;		/* Number of bytes (space) left		*/
	Uint8					bufByte;	/* The byte in process and			*/
	Uint8					bufUsed;	/*	the range (0..253) used so far	*/
} lib_SFeInfo;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#if 0
/**
 *	Types for the filtering functions.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8					prevIn;		/**< Previous input status			*/
	Uint8					edges[1];	/**< Status change information		*/
} lib_Filt8Type;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint16					prevIn;		/**< Previous input status			*/
	Uint16					edges[1];	/**< Status change information		*/
} lib_Filt16Type;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint32					prevIn;		/**< Previous input status			*/
	Uint32					edges[1];	/**< Status change information		*/
} lib_Filt32Type;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#endif

/**
 * \brief	Type for 8-bit CRC init functions.
 * \return	The CRC value to start from.
 */
typedef Uint8 lib_Crc8InitFn(void);
/**
 * \brief 	Type for 8-bit CRC update functions.
 * \param	crc		The crc value to coninue from.
 * \param	pData	Pointer to data.
 * \param	size	Number of data bytes.
 * \return	The updated CRC value.
 */
typedef Uint8 lib_Crc8UpdFn(Uint8 crc, BYTE const_D * pData, Uint16 size);
typedef Uint8 lib_Crc8GetFn(Uint8);

typedef Uint16 lib_Crc16InitFn(void);
typedef Uint16 lib_Crc16UpdFn(Uint16 crc, BYTE const_D * pData, Uint16 size);
typedef Uint16 lib_Crc16GetFn(Uint16);

typedef Uint32 lib_Crc32InitFn(void);
typedef Uint32 lib_Crc32UpdFn(Uint32 crc, BYTE const_D * pData, Uint16 size);
typedef Uint32 lib_Crc32GetFn(Uint32);

/**
 * 	Interface for 8-bit CRC calculation implementations.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	lib_Crc8InitFn *		pInitFn;	/**< Pointer to CRC init function.	*/
	lib_Crc8UpdFn *			pUpdFn;		/**< Pointer to CRC update function.*/
	lib_Crc8GetFn *			pGetFn;		/**< Pointer to CRC get function.	*/
} lib_Crc8If;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * 	Interface for 16-bit CRC calculation implementations.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	lib_Crc16InitFn *		pInitFn;	/**< Pointer to CRC init function.	*/
	lib_Crc16UpdFn *		pUpdFn;		/**< Pointer to CRC update function.*/
	lib_Crc16GetFn *		pGetFn;		/**< Pointer to CRC get function.	*/
} lib_Crc16If;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * 	Interface for 32-bit CRC calculation implementations.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	lib_Crc32InitFn *		pInitFn;	/**< Pointer to CRC init function.	*/
	lib_Crc32UpdFn *		pUpdFn;		/**< Pointer to CRC update function.*/
	lib_Crc32GetFn *		pGetFn;		/**< Pointer to CRC get function.	*/
} lib_Crc32If;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type for memory pool information.
 */
 
typedef lib_FifoType lib_MemPool;

/**
 * \brief	Type for memory pool block init functions.
 * 
 * \param	pBlock		Pointer to the block that should be initialized.
 */

typedef void (* lib_PoolCbFn)(void * pBlock);

/**
 * Type for memory pool initialization values.
 */

typedef struct {						/*'''''''''''''''''''''''''''''' RAM */
	void **					ppCategory;	/**< Used memory category.			 */
	size_t					blockSize;	/**< The desired block size.		 */
	Uint8					blockCount;	/**< The desired number of blocks.	 */
	lib_PoolCbFn 			pInitCbFn;	/**< Pointer to block init function. */
} lib_PoolInit;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Simple moving average filter state information.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16					maxCount;	/**< Number of elements in array.	*/
	Uint16					nextIdx;	/**< Index for the next value.		*/
	Uint16 *				pValues;	/**< Pointer to value array.		*/
	Uint32					sum;		/**< Sum of all values in average.	*/
} lib_FsmaInfo;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Simple moving average filter state information for 32-bit values.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16					maxCount;	/**< Number of elements in array.	*/
	Uint16					nextIdx;	/**< Index for the next value.		*/
	Uint32 *				pValues;	/**< Pointer to value array.		*/
	Uint32					sum;		/**< Sum of all values in average.	*/
} lib_FsmaInfo32;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * State information for average filter.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16					maxCount;	/**< Number of samples for average.	*/
	Uint16					counter;	/**< Sample counter.				*/
	Uint32					currSum;	/**< Sum of current samples.		*/
	Uint32					sum;		/**< Latest calculated sum.			*/
} lib_FavgInfo;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * State infomation for combined average and SMA filter.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	lib_FavgInfo			stage1;		/**< State info for stage 1.		*/
	lib_FsmaInfo32			stage2;		/**< State info for stage 2.		*/
} lib_FcombInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void lib_fifoPut(lib_FifoType *,void **);
void **lib_fifoGet(lib_FifoType *);

void **lib_fifoBegin(lib_FifoType *,lib_FifoCursor *);
void **lib_fifoNext(lib_FifoType *,lib_FifoCursor *);
void **lib_fifoErase(lib_FifoType *,lib_FifoCursor *);

//void lib_fifoErase(lib_FifoType *,void **);

Uint8 lib_Uint16ToAscii(Uint16,char *,Uint16);
Uint8 lib_Uint32ToAscii(Uint32,char *,Uint16);
Uint8 lib_Int16ToAscii(Int16,char *,Uint16);
Uint8 lib_Int32ToAscii(Int32,char *,Uint16);

int lib__pMemcmp(void const_D *,void const_P *,size_t);
void *lib__pMemcpy(void *,void const_P *,size_t);

void lib_moveUp(BYTE*, BYTE*, size_t);
void lib_moveDown(BYTE*, BYTE*, size_t);

/* LIB_MEMA.C */

void *lib__memAlign(void const_D *,Uint8);

/* LIB_RBS.C */

void lib_rBufSReset(void *);
lib_RBufSDim lib_rBufSInfo(void *,lib_RBufSDim *);

/* LIB_RBS1.C */

void *lib__rBufS1Init(lib_RBufSDim,Uint8);
lib_RBufStat lib_rBufS1Put(void *,void const_D *);
lib_RBufStat lib_rBufS1Get(void *,void *);
#define lib_rBufS1Init(s_)		lib__rBufS1Init(s_,0)
#define lib_rBufS1Reset(p_)		lib_rBufSReset(p_)
#define lib_rBufS1Info(p_,d_)	lib_rBufSInfo(p_,d_)

/* LIB_RBS2.C */

void *lib__rBufS2Init(lib_RBufSDim,Uint8);
lib_RBufStat lib_rBufS2Put(void *,void const_D *);
lib_RBufStat lib_rBufS2Get(void *,void *);
#define lib_rBufS2Init(s_)		lib__rBufS2Init(s_,0)
#define lib_rBufS2Reset(p_)		lib_rBufSReset(p_)
#define lib_rBufS2Info(p_,d_)	lib_rBufSInfo(p_,d_)

/* LIB_RBS4.C */

void *lib__rBufS4Init(lib_RBufSDim,Uint8);
lib_RBufStat lib_rBufS4Put(void *,void const_D *);
lib_RBufStat lib_rBufS4Get(void *,void *);
#define lib_rBufS4Init(s_)		lib__rBufS4Init(s_,0)
#define lib_rBufS4Reset(p_)		lib_rBufSReset(p_)
#define lib_rBufS4Info(p_,d_)	lib_rBufSInfo(p_,d_)

/* LIB_RBSX.C */

void *lib_rBufSXInit(lib_RBufSDim,Uint8);
lib_RBufStat lib_rBufSXPut(void *,void const_D *);
lib_RBufStat lib_rBufSXGet(void *,void *);
#define lib_rBufSXReset(p_)		lib_rBufSReset(p_)
#define lib_rBufSXInfo(p_,d_)	lib_rBufSInfo(p_,d_)

/* LIB_RBM.C */

void lib_rBufMReset(void *);
lib_RBufMDim lib_rBufMInfo(void *,lib_RBufMDim *);

/* LIB_RBM1.C */

void *lib__rBufM1Init(lib_RBufMDim,Uint8);
lib_RBufStat lib_rBufM1Put(void *,void const_D *);
lib_RBufStat lib_rBufM1Get(void *,void *);
#define lib_rBufM1Init(s_)		lib__rBufM1Init(s_,0)
#define lib_rBufM1Reset(p_)		lib_rBufMReset(p_)
#define lib_rBufM1Info(p_,d_)	lib_rBufMInfo(p_,d_)

/* LIB_RBM2.C */

void *lib__rBufM2Init(lib_RBufMDim,Uint8);
lib_RBufStat lib_rBufM2Put(void *,void const_D *);
lib_RBufStat lib_rBufM2Get(void *,void *);
#define lib_rBufM2Init(s_)		lib__rBufM2Init(s_,0)
#define lib_rBufM2Reset(p_)		lib_rBufMReset(p_)
#define lib_rBufM2Info(p_,d_)	lib_rBufMInfo(p_,d_)

/* LIB_RBM4.C */

void *lib__rBufM4Init(lib_RBufMDim,Uint8);
lib_RBufStat lib_rBufM4Put(void *,void const_D *);
lib_RBufStat lib_rBufM4Get(void *,void *);
#define lib_rBufM4Init(s_)		lib__rBufM4Init(s_,0)
#define lib_rBufM4Reset(p_)		lib_rBufMReset(p_)
#define lib_rBufM4Info(p_,d_)	lib_rBufMInfo(p_,d_)

/* LIB_RBMX.C */

void *lib_rBufMXInit(lib_RBufMDim,Uint8);
lib_RBufStat lib_rBufMXPut(void *,void const_D *);
lib_RBufStat lib_rBufMXGet(void *,void *);
#define lib_rBufMXReset(p_)		lib_rBufMReset(p_)
#define lib_rBufMXInfo(p_,d_)	lib_rBufMInfo(p_,d_)

/* LIB_CRES.c */
Uint32 lib_crc32EthernetSmall(Uint32 ,BYTE const_D *,Uint16);

/* LIB_CRDS.c */
Uint32 lib_crc8DallasMaximSmall(Uint32 ,BYTE const_D *,Uint16);

/* LIB_CPM8.c */
Uint8 lib_memCmpCpy8(ptrs_RegData_bp pTarget,ptrs_RegData_bcDp pSource,Uint8 bytes);

/* LIB_POOL.C */
void lib_poolInit(lib_MemPool *,void ** ppCategory,size_t blockSize,Uint8 blockCount);
void lib_poolInitCb(lib_MemPool * pPool,lib_PoolInit * pInit);

/* LIB_FSMA.c */
void lib_fsmaInit(lib_FsmaInfo *,Uint16 initValue,Uint16 * pArray,Uint16 valueCount);
Boolean lib_fsmaAddValue(lib_FsmaInfo *,Uint16 newValue);
Uint16 lib_fsmaGet(lib_FsmaInfo *);
#define lib_fsmaGetSum(pFilter_) ((pFilter_)->sum)
#define lib_fsmaGetCount(pFilter_) ((pFilter_)->maxCount)

/* LIB_FMA3.c */
void lib_fsmaInit32(lib_FsmaInfo32 *,Uint32 initValue,Uint32 * pArray,Uint16 valueCount);
Boolean lib_fsmaAddValue32(lib_FsmaInfo32 *,Uint32 newValue);
Uint32 lib_fsmaGet32(lib_FsmaInfo32 *);
#define lib_fsmaGetSum32(pFilter_) ((pFilter_)->sum)
#define lib_fsmaGetCount32(pFilter_) ((pFilter_)->maxCount)

/* LIB_FAVG.c */
void lib_favgInit32(lib_FavgInfo *,Uint32 initValue,Uint16 valueCount);
Boolean lib_favgAddValue32(lib_FavgInfo *,Uint32 newValue);
Uint32 lib_favgGet32(lib_FavgInfo *);
#define lib_favgGetSum32(pFilter_) ((pFilter_)->sum)
#define lib_favgGetCount32(pFilter_) ((pFilter_)->maxCount)

/* LIB_FC32.c */
void lib_fcombInit32(lib_FcombInfo *,Uint32 initValue,Uint32 * pArray,Uint16 valueCount1,Uint16 valueCount2);
Boolean lib_fcombAddValue32(lib_FcombInfo *,Uint32 newValue);
Uint32 lib_fcombGet32(lib_FcombInfo *);
#define lib_fcombGetSum32(pFilter_) ((pFilter_)->stage2.sum)
#define lib_fcombGetCount32(pFilter_) ((pFilter_)->stage1.maxCount * (pFilter_)->stage2.maxCount)

/* LIB_SFE?.C */

void lib_sFeInit(lib_SFeInfo *,Uint8 *,Uint8);
Boolean lib_sFeWrite8(lib_SFeInfo *,Uint8,Uint8);
Boolean lib_sFeRead8(lib_SFeInfo *,Uint8 *,Uint8);
Boolean lib_sFeWrite16(lib_SFeInfo *,Uint16,Uint16);
Boolean lib_sFeRead16(lib_SFeInfo *,Uint16 *,Uint16);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/* LIB_RB??.C */

extern lib_RBufToolSet const_P lib_rBufM1Tools;
extern lib_RBufToolSet const_P lib_rBufM2Tools;
extern lib_RBufToolSet const_P lib_rBufM4Tools;
extern lib_RBufToolSet const_P lib_rBufMXTools;
extern lib_RBufToolSet const_P lib_rBufS1Tools;
extern lib_RBufToolSet const_P lib_rBufS2Tools;
extern lib_RBufToolSet const_P lib_rBufS4Tools;
extern lib_RBufToolSet const_P lib_rBufSXTools;

extern lib_Crc8If const_P * const_P lib_crc8MaximIf_p;
extern lib_Crc16If const_P * const_P lib_crc16KermitIf_p;
extern lib_Crc32If const_P * const_P lib_crc32EthIf_p;

extern lib_Crc32If const_P lib_crc32EthFastIf;
extern lib_Crc32If const_P lib_crc32EthSmallIf;
extern lib_Crc8If const_P lib_crc8MaximFastIf;
extern lib_Crc8If const_P lib_crc8MaximSmallIf;
extern lib_Crc16If const_P lib_crc16KermitFastIf;
extern lib_Crc16If const_P lib_crc16KermitSmallIf;
extern lib_Crc16If const_P lib_crc16Mcrf4FastIf;
extern lib_Crc16If const_P lib_crc16Mcrf4SmallIf;
extern lib_Crc16If const_P lib_crc16Mcrf4HwIf;

/***************************************************************//** \endcond */

#endif
