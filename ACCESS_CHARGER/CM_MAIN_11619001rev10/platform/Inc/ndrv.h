/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		ndrv.h
*
*	\ingroup	NDRV
*
*	\brief		Public declarations of the network driver.
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef NDRV_H_INCLUDED
#define NDRV_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "miimif.h"
#include "ndif.h"
#include "phyif.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Network driver option flags.
 */

/*@{*/
#define NDRV_OPT_RMII			(1<<0)	/**< Enable RMII mode.				*/
#define NDRV_OPT_ACC_UNICAST	(1<<1)	/**< Accept all unicast frames.		*/
#define NDRV_OPT_ACC_BROADCAST	(1<<2)	/**< Accept broadcast frames.		*/
#define NDRV_OPT_ACC_MULTICAST	(1<<3)	/**< Accept all multicast frames.	*/
#define NDRV_OPT_ACC_MATCH		(1<<4)	/**< Accept frames with matching MAC
										 * address.							*/
#define NDRV_OPT_WOL			(1<<5)	/**< Enable wake on LAN.			*/
#define NDRV_OPT_DEFAULT		(1<<6)	/**< This is the default Ethernet
										 * MAC 								*/
#define NDRV_OPT_POLLPHY		(1<<7)	/**< Enable PHY status polling.		*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

struct ndrv__emacIf;
typedef struct ndrv__emacIf ndrv_EmacIf;

struct ndrv__inst;
typedef struct ndrv__inst ndrv__Inst;

/**
 * Type of PHY status polling init function.
 */

typedef void 				ndrv_InitPollFn(ndrv__Inst *);

/**
 * Type of multicast frame filter initialization function.
 */

typedef void ndrv__InitFiltFn(ndrv__Inst *);

/**
 * Type used to store function pointers to the multicast filter function.
 */

typedef Boolean ndrv__MultiFiltFn(ndrv__Inst *,Uint8 const_D *,Boolean);

/**
 * Type of multicast frame filter interface.
 */

typedef struct {
	ndrv__InitFiltFn *		pInitFiltFn;
	ndrv__MultiFiltFn *		pMultiFiltFn;
} ndrv__MultiFiltIf;

/**
 *	Protocol handler information.
 */

typedef struct {
	ndif_RxIf const_P *		pRxIf;		/**< Ptr to RX interface.			*/
	void **					ppUtil;		/**< Pointer to utility pointer.	*/
} ndrv_ProtHandler;

/**
 * Initialization type for the network driver.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	Uint8 					fltAddrCount;/**< Number of multicast addresses
										 * that can be configured to the
										 * filter.							*/
	Uint8					blockCount;	/**< Number of buffer blocks that should
										 * be allocated to buffer pool.		*/
	Uint8					priority;	/**< NDRV task priority.			*/
	Uint16					mtu;		/**< Maximum transmission unit.		*/
	Uint16					options;	/**< Options.						*/
	Uint16					blockSize;	/**< Size of the buffer blocks that will
										 * be allocated.					*/
	sys_RegHandle			macAddrReg;/**< Register handle of a Uint32 array
										 * register with 3 elements containing
										 * the MAC address.	SYS_BAD_REGISTER
										 * if no register is used.			*/
	ndif_BuffHanIf const_P * pBuffIf;	/**< Buffer handler interface.		*/
	ndrv_ProtHandler const_P * pProtHan;/**< List of protocol handlers.		*/
	phyif_Interface const_P * pPhyIf;	/**< Ptr to PHY driver interface.	*/
	phyif_Init const_P * 	pPhyInit;	/**< Ptr to PHY init parameters.	*/
	ndrv_EmacIf const_P *	pEmacIf;	/**< Ptr to EMAC drv implem.		*/
	Uint8 const_P *			pMacAddr;	/**< Ptr to MAC address. NULL if REG
										 * is used to store the address.	*/
	ndrv_InitPollFn *		pInitPollFn;/**< Ptr to init polling function	*/
	ndrv__MultiFiltIf const_P * pMultiFiltIf;/**< Pointer to multicast filter
										 * interface. May be set to NULL if
										 * no multicast filter is used.		*/
	void **					pCategory;	/**< Pointer to memory category used
										 * for Ethernet data.				*/
	sys_RegHandle const_P *	pMacAddReg;	/**< Pointer to MAC address register
										 * handle.							*/
} ndrv_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* EMAC driver */
void * 	ndrv_init(ndrv_Init const_P *);
void 	ndrv_initPolling(ndrv__Inst *);
void 	ndrv_handleInterrupt(void *);
Uint8	ndrv_readClearRxDone(ndrv__Inst *);

/* SME drivers */
void * ndrv_amdioInit(void);
void * ndrv_mdioInit(void);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_ARM7
extern ndrv_EmacIf const_P 		ndrv_emac0;
extern miimif_Interface const_P ndrv_miimIf0;

#elif TARGET_SELECTED & (TARGET_CM3 | TARGET_CM4)
extern ndrv_EmacIf const_P 		ndrv_emac0;
extern miimif_Interface const_P ndrv_miimIf0;
extern amif_Interface const_P	ndrv_amdioIf;

#elif TARGET_SELECTED & TARGET_W32
extern ndrv_EmacIf const_P 		ndrv_emac0;
extern ndrv_EmacIf const_P 		ndrv_emac1;
extern ndrv_EmacIf const_P 		ndrv_emac2;
extern ndrv_EmacIf const_P 		ndrv_emac3;
extern ndrv_EmacIf const_P 		ndrv_emac4;
extern miimif_Interface const_P ndrv_miimIf0;
extern amif_Interface const_P	ndrv_amdioIf;

#elif TARGET_SELECTED & TARGET_S4F4
extern ndrv_EmacIf const_P 		ndrv_emac0;
extern ndrv_EmacIf const_P		ndrv_emac0NoPinConf;
extern amif_Interface const_P	ndrv_amdioIf;

#elif TARGET_SELECTED & TARGET_S3F2
extern ndrv_EmacIf const_P 		ndrv_emac0;
extern ndrv_EmacIf const_P      ndrv_emac0AltPins;
extern ndrv_EmacIf const_P		ndrv_emac0NoPinConf;
extern amif_Interface const_P	ndrv_amdioIf;

#elif TARGET_SELECTED & TARGET_DOXY
extern ndrv_EmacIf const_P 		ndrv_emac0;
extern miimif_Interface const_P ndrv_miimIf0;
extern amif_Interface const_P	ndrv_amdioIf;

#endif

extern ndrv__MultiFiltIf const_P ndrv_MultiFilt;

/***************************************************************//** \endcond */

#endif
