/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		phyif.h
*
*	\ingroup	PHYIF
*
*	\brief		Synchronous PHY driver interface.
*
*	\details	Declares types and constants used in the PHYIF interface.
*
*	\note
*
*	\version	\$Rev: 3654 $ \n
*				\$Date: 2019-03-22 15:16:11 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	PHYIF	PHYIF
*
*	\brief		Synchronous PHY driver interface.
*
********************************************************************************
*
*	\details	The interface is used by EMAC drivers to communicate with a
*				PHY driver. The interface provides a common way for controlling
*				any PHY. The PHY driver internally takes care of all PHY chip
*				specific operations.
*
*				This interface adds synchronous operations to the APIF
*				interface. The APIF PHY driver option flags and the Link status
*				flags should be used with this interface.
*	<pre>
*				                  PHYIF
*				                    |
*				+-------------+     |     +--------------+
*				| EMAC driver |     |     |  PHY driver  |
*				|             |     |     |              |
*				|             |-----|-->init()           |
*				|             |-----|-->warmInit()       |
*				|             |-----|-->poll()           |
*				|             |-----|-->doOp()           |
*				|             |     |     |              |
*				+-------------+     |     +--------------+
*				                    |
*	</pre>
*
*******************************************************************************/

#ifndef PHYIF_H_INCLUDED
#define PHYIF_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "miimif.h"
#include "apif.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	Type for storing operation codes. All APIF operation codes can be used with
 *	PHYIF.
 *
 *	\see	apif_OpCode
 */

typedef apif_OpCode phyif_OpCode;

/**
 *	Return code type for PHYIF functions. The return code may be a GAI return
 *	code, APIF return code or a PHY driver specific return code.
 *
 *	\see	gai_retCode apif_retCode
 */

typedef Uint8 phyif_RetCode;

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * \brief		Initialization value type for PHY drivers.
 *
 * \details		The PHY driver initialization constant is usually defined in
 *				project.c. A pointer to the constant is usually provided to the
 *				EMAC driver FB as an initialization parameter. The EMAC driver
 *				will then provide the pointer to the PHY driver initialization
 *				function.
 *				
 *				The init parameters are based on the APIF init parameters. PHYIF
 *				only adds the MIIMIF interface constant pointer in case the PHY
 *				driver uses the synchronous MIIM bus master interface.
 *
 * \see			apif_Init
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	apif_Init				base;		/**< Base init parameters.			*/
	miimif_Interface const_P * pMiiIf;	/**< 
										 * Pointer to MIIMIF interface
										 * constant. May be NULL if an
										 * asynchronous STA driver is used.	*/
} phyif_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief		Type used for PHY driver instances externally.
 *
 * \details		Internally the driver has its own type.
 */

typedef void phyif_PhyInst;

/**
 * \brief 		Functions of this type initializes the PHY driver instance.
 *	
 * \param		pInit	Pointer to the initialization values for the PHY driver.
 *
 * \return		A pointer to the new PHY driver instance.
 *
 * \details		The function is called during FB initialization before OSA is
 * 				running.
 */

typedef phyif_PhyInst * phyif_InitFn(phyif_Init const_P * pInit);

/**
 * \brief		Functions of this type initializes the PHY chip.
 *
 * \param		pInst	Pointer to the PHY driver instance data.
 * \param		options	Option flags.
 *
 * \return		PHYIF return code.
 *
 * \details		The function is called from a FB task after OSA has been
 * 				started. It is assumed that the calling task can be blocked
 * 				while the PHY is being initialized.
 *	
 * \see 		phyif_RetCode
 */

typedef phyif_RetCode phyif_WarmInitFn(phyif_PhyInst * pInst, Uint8 options);

/**
 * \brief		This function reads the PHY link status.
 *
 * \param		pInst		Pointer to the PHY driver instance data.
 * \param		pStatus		Pointer to Uint8 value where the link status change
 * 				  			information will be stored.
 *
 * \return		PHYIF return code.
 *
 * \details		The function should be called when a PHY link change interrupt
 * 				is generated. If the PHY does not support link change interrupts
 * 				then the function should be called at constant intervals. An
 * 				appropriate interval might be 1s.
 *
 * 				The most common type of link change is when the network cable
 * 				is plugged or unplugged.
 *
 * \see 		phyif_RetCode
 */

typedef phyif_RetCode phyif_PollFn(phyif_PhyInst * pInst,Uint8 * pStatus);

/**
 * \brief		Do operation function.
 *
 * \param		pInst	Pointer to PHY driver instance.
 * \param		op		Operation code.
 *
 * \return		PHYIF return code.
 *
 * \details		This function is used to request a operation to be performed.
 * 				Some operations are not supported by all PHY chips.
 *
 * \see 		phyif_RetCode
 */

typedef phyif_RetCode phyif_DoOpFn(phyif_PhyInst * pInst,phyif_OpCode op);

/**
 * \brief		Type for PHY driver public interface constants.
 *
 * \details		A public constant of this type is defined in each PHY driver
 *				that implements synchronous interface (PHYIF). A pointer to the
 *				constant is sent as an initialization parameter to the FB that
 *				will be using the PHY driver.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	phyif_InitFn * 			pInitFn;	/**< Ptr to initialization function.*/
	phyif_WarmInitFn * 		pWarmInitFn;/**< Ptr to warm init function. 	*/
	phyif_PollFn *			pPollFn;	/**< Ptr to link status poll func.	*/
	phyif_DoOpFn *			pDoOpFn;	/**< Ptr to do operation function.	*/
} phyif_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* Asynchronous interface wrapper */
phyif_PhyInst * phyif_asyncInit(apif_Interface const_P * pApif,void * pAsyncInst);
phyif_RetCode phyif_asyncWarmInit(phyif_PhyInst * pVoidInst,Uint8 options);
phyif_RetCode phyif_asyncPoll(phyif_PhyInst * pVoidInst,Uint8 * pStatus);
phyif_RetCode phyif_asyncDoOp(phyif_PhyInst * pVoidInst,phyif_OpCode op);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
