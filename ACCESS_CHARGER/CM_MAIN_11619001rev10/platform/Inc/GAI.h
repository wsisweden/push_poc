/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		GAI.h
*
*	\ingroup	GAI
*
*	\brief		Generic Asynchronous Interface.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 3657 $ \n
*				\$Date: 2019-03-22 15:21:40 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	GAI	GAI
*
*	\brief		Generic Asynchronous Interface.
*
********************************************************************************
*
*	\details	The GAI interface provides base types that can be used by all
*				asynchronous interfaces. Asynchronous interfaces are usually
*				needed when a single task needs to perform multiple operations
*				simultaneously. Allocating an own task for every simultaneous
*				operation would often require too much RAM, which is a very
*				limited resource in embedded systems.
*
*				While the GAI interface may contain all functionality needed for
*				a specific purpose, it is not intended to be used directly by
*				applications. A derived interface should be created when an
*				interface is needed.
*
*				The reason for this interface is to provide a common way of
*				implementing asynchronous interfaces. It is easier to understand
*				an interface if it is based on a known interface. The interface
*				also comes with a generic request queue, which can be used by
*				all function blocks that implement an interface based on GAI.
*
*	\par		Request done callback
*				The request structure contains a function pointer to a function
*				which should be called when the request has been handled.
*
*******************************************************************************/

#ifndef GAI_H_INCLUDED
#define GAI_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * Generic return codes. The codes in this enum can be extended by creating a 
 * new enum and assigning GAI_RET_1ST_IF to the first value.
 */
 
enum gai_retCode {						/*''''''''''''''''''''''''''''''''''*/
	GAI_RET_OK,							/**< Operation was successful.		*/
	GAI_RET_ERROR,						/**< Unspecified error.				*/
	GAI_RET_BUSY,						/**< Driver busy. This is only
										 * returned if the driver does not
										 * support request queuing.			*/
	GAI_RET_NOT_SUPPORTED,				/**< Operation is not supported.	*/
	GAI_RET_1ST_IF						/**< First interface specific.		*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

struct gai_req;

/**
 * Type used for GAI return codes.
 */
 
typedef Uint8 				gai_RetCode;

/**
 * \brief	Type of callback function called when a request has been handled.
 *
 * \param	pReq		Pointer to the request that was handled.
 * \param	retCode		The return code that specifies the result of the 
 *						operation.
 *
 * \details	The callback function must be called from a task or a CoTask
 *			with interrupts and tasking enabled. It must not be called from
 *			an ISR.
 *	\par
 *			It should be possible to call the access function again from the
 *			callback function.
 */
 
typedef void gai_DoneFn(struct gai_req * pReq, gai_RetCode retCode);

/**
 * \brief	Generic asynchronous request type.
 *
 * \details	This type can be used as a base for other asynchronous interfaces.
 * \par		Example
 * \code	
 *			typedef struct {
 *				gai_Req		base;
 *				myIf_Op		operation;
 *				Uint16		data;
 *			} myIf_Req;
 * \endcode
 */

typedef struct gai_req {				/*''''''''''''''''''''''''''''' RAM */
	struct gai_req *		pNext;		/**< Pointer to the next request.
										 * Can be used to queue requests. May
										 * be modified by the interface
										 * implementing FB.					*/
	gai_DoneFn *			pDoneFn;	/**< Pointer to function called when
										 * the request has been handled.
										 * The FB that implements the interface
										 * should not modify this pointer.	*/
	void *					pUtil;		/**< Utility pointer. Can be used
										 * to store information needed in
										 * the done callback function.
										 * The FB that implements the interface
										 * should not modify this pointer.	*/
} gai_Req;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief	Type of function called to request an operation.
 *
 * \param	pDrvInst	Pointer to the driver instance.
 * \param	pReq		Pointer to the request that should be handled.
 *
 * \return	The function returns GAI_RET_OK if the request was accepted for 
 *			handling. The "done" callback function will always be called when 
 *			the request has been accepted.
 * \par
 *			If anything else than GAI_RET_OK is returned then the request was
 *			not accepted. In that case the "done" callback function will not be
 *			called
 *
 * \details	
 */
 
typedef gai_RetCode gai_AccessFn(void * pDrvInst, gai_Req * pReq);

/**
 * \brief	Generic type for interface constants.
 *
 * \details	If the interface only needs the access function then this type can
 *			be used. It is however recommended to make a typedef of this type so
 *			that it is easier to differentiate two separate interfaces based on
 *			GAI.
 *
 *			If the interface needs additional functions then this type can be
 *			ignored. An equivalent structure with multiple function pointers is
 *			then declared.
 *
 *	\par	Example
 *	\code		
 *			typedef gai_Interface myIf_Interface;
 *	\endcode
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	gai_AccessFn *			pAccessFn;	/**< Pointer to access function.	*/
} gai_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* GAI request FIFO handling */
void *		gai_fifoInit(gai_Interface const_P * pImplIf,void * pImplInst);
gai_RetCode gai_fifoAccess(void * pDrvInst,gai_Req * pReq);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern gai_Interface const_P gai_fifoIf;

/***************************************************************//** \endcond */


#endif
