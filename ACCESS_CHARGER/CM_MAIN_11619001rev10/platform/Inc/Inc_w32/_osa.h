/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_w32/_osa.h
*
*	\ingroup	OSA_W32
*
*	\brief		Target specific OSA declarations.
*
*	\details	Implementation specific part for the following:
*				- PC platform
*				- Microsoft MSVC (32-bit)
*				- Native 32-bit Windows
*
*	\note		
*
*	\version	\$Rev: 4298 $ \n
*				\$Date: 2020-11-10 13:23:42 +0200 (ti, 10 marras 2020) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/******************************************************************************
;
;	HEADER AND INCLUDE FILES
;
******************************************************************************/

/******************************************************************************
;
;	CONSTANTS AND DEFINITIONS
;
******************************************************************************/

/******************************************************************************
;
;	DATA TYPES AND STRUCTURES
;
******************************************************************************/

typedef void (*				osa_InterruptFnPtr)(void);
typedef Uint8				osa_Status;
typedef void *				osa__W32Handle;
typedef void (*				osa__DebLogHandlerFn)(char *);
#define OSA_TASK
typedef Uint32				osa_TickType;

/*
 *	OSA objects.
 */

typedef struct {						/*''''''''''''''''''''''' SEMAPHORE	*/
	osa__W32Handle			sema;		/**< Internal to OSA                */
} osa_SemaType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct osa__task {				/*'''''''''''''''''''''''''''' TASK	*/
	link2_Node				link;		/**< Internal to OSA                */
	const char *			pName;		/**< Name of the task               */
	OSA_TASK void (*		fn)(void);	/**< Task's main function           */
	void *					pUtil;		/**< Utility ptr to the task        */
	osa__W32Handle			w32Thread;	/**< Win thread (internal to OSA)   */
	osa__W32Handle			w32Signal;	/**< Used in signaling (intrl to OSA)*/
	Uint8					priority;	/**< Priority of the task           */
} osa_TaskType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	DATA
;
******************************************************************************/

extern int osa__taskingLockCount;

/******************************************************************************
;
;	MACROS
;
******************************************************************************/

#define osa_fastIsrDecl(name_)		void name_(void) 
#define osa_osIsrDecl(name_)		void name_(void) 

#define osa_isrFastDecl(name_)		void name_(void) 
#define osa_isrOSDecl(name_)		void name_(void) 

#define osa_isrFastFn(isr_,name_)	void name_(void)
#define osa_isrOSFn(isr_,name_)		void name_(void) { osa_interruptsDisable();

#define osa_taskingIsDisabled()		osa__taskingLockCount

#define osa_isrEnter()				
#define osa_isrLeave()				
#define osa_endOSIsr				osa_interruptsEnable(); }

/******************************************************************************
;
;	FUNCTION PROTOTYPES
;
******************************************************************************/

void osa_init(void);

void osa_newSemaSet(osa_SemaType *);
void osa_newSemaTaken(osa_SemaType *);


void osa_semaGet(osa_SemaType *);
void osa_semaSetIsr(osa_SemaType *);
void osa_semaWait(osa_SemaType *,Uint16,osa_Status *);
#define osa_semaSet(pSema)		osa_semaSetFn(pSema);

void osa_signalGet(void);
void osa_signalWait(Uint16,osa_Status *);
#define osa_signalSend(pTask)	osa_signalSendFn(pTask);

void osa_taskingDisable(void);
void osa_taskingEnable(void);
void osa_interruptsDisable(void);
void osa_interruptsEnable(void);
tools_IsrState osa_interruptsDisableIntr(void);
void osa_interruptsEnableIntr(tools_IsrState *);

void osa_newTask(osa_TaskType *,const_P char *,Uint8,void (OSA_TASK *)(void),Uint16);
osa_TaskType * osa_taskCurrent(void);
void osa_taskWait(Uint16);
void osa_taskSuspend(void);
#define osa_taskResume(pTask)	osa_taskResumeFn(pTask);
#define osa_getTaskName(p_)		((p_)->pName)

void osa_tick(void);
osa_TickType osa_getTick(void);

void osa__isrEnter(void);
void osa__isrLeave(void);

/*void osa_logMessage(char const_P *,...);*/
