/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	File:	nan.h
*	
*--------------------------------------------------------------------*//** \file		
*
*	\ingroup	NAN
*
*	\brief		NAN constant definition.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4007 $ \n
*				\$Date: 2019-11-19 17:41:25 +0200 (ti, 19 marras 2019) $ \n
*				\$Author: tldati $
*
*******************************************************************************/

#ifndef NAN_H_INCLUDED
#define NAN_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include <math.h>
#include <float.h>

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/* Visual studio 2005 does not support NAN or INFINIY directly */

#ifndef INFINITY
# define INFINITY			(FLT_MAX + FLT_MAX)
#endif

#ifndef NAN
# define NAN				(INFINITY - INFINITY)
#endif

#ifndef isnan
# define isnan(f_)			_isnan(f_)
#endif

#if 0
# define isnan(floatValue_) ((*((Uint32 *) &(floatValue_))) == 0xFFFFFFFFUL)
#endif

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/******************************************************//** \endcond pub_decl */

#endif
