/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_w32/_portdrv.h
*
*	\ingroup	PORTDRV_W32
*
*	\brief		Serial Port Driver
*
*	\details	
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef _PORTDRV_H_INCLUDED
#define _PORTDRV_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define PORTDRV_DIV0	0 /* Clock source = f1 */
#define PORTDRV_DIV2	0 /* Clock source = f1 / 2 */
#define PORTDRV_DIV8	1 /* Clock source = f1 / 8 */

#define PORTDRV_IIC_ADDRMATCH_W		1
#define PORTDRV_IIC_ADDRMATCH_R		2
#define PORTDRV_IIC_DATATRAN_ACK	3
#define PORTDRV_IIC_DATATRAN_NACK	4
#define PORTDRV_IIC_DATAREC			5

#define PORTDRV_IIC_DATATRAN_LB_ACK	6
#define PORTDRV_IIC_DATAREC_ACK		7

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/* 
 * tools for IIC
 * -------------
 */
#define portdrv_setStartCond(_i, _n)		
#define portdrv_clearStartCond(_i, _n)	
	
#define portdrv_setReStartCond(_i, _n)		
#define portdrv_clearReStartCond(_i, _n)	

#define portdrv_setStopCond(_i, _n)		
#define portdrv_clearStopCond(_i, _n)	

#define portdrv_setAck(pB, n)			
#define portdrv_clearAck(_n)		

#define portdrv_setNack(_i, _n)			
#define portdrv_clearNack(_i, _n)		

/* 
 * General tools
 * -------------
 */
#define portdrv_txIsrFn(_i, _n)

#define portdrv_rxIsrFn(_i, _n)



/**
 * portdrv_getBRG			Get value of baud rate register(s) 
 *	_br = requested baud rate i.e. 9600
 * _xtal = frequency of main clock i.e 24000000 = 24Mhz
 * _pP = pointer to parameter structure
 */
#define portdrv_getBRG(_br, _xtal, _pP)

#define portdrv_sendByteIsr(_pB, _pD)			
#define portdrv_getStatusIsr(pB, n, pS)	*(pS) = 0;

#define portdrv_getDataIsr(pB, n, pD)


#define portdrv_setClockWait(pB, n)		
#define portdrv_setClockEnable(pB, n)	

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PUBLIC void portdrv_initw32(void *,void const_P *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif

