/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NXP.H
*
*	\ingroup	NXP
*
*	\brief		Public declarations for the NXP function block.
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef NXP_H_INCLUDED
#define NXP_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/*	
 *  IAP defines.
 */
/*@{*/
#define NXP_IAP_PAGE_SIZE	256
/*@}*/

#if TARGET_SELECTED & TARGET_ARM7
# define NXP_IAP_LOCATION		0x7FFFFFF1	/**< IAP location in flash      */
#elif (TARGET_SELECTED & (TARGET_CM0 | TARGET_CM3 | TARGET_CM4)) != 0
# define NXP_IAP_LOCATION		0x1FFF1FF1	/**< IAP location in flash      */
#elif TARGET_SELECTED & TARGET_W32
extern void nxp__w32FnIAP(Uint32 [],Uint32 []);
# define NXP_IAP_LOCATION		&nxp__w32FnIAP
#endif


#define NXP_FLASH_INVALID_SECTOR		0xFFFFFFFFUL

/**
 *  ISP/IAP return codes.
 */
enum {									/*''''''''''''''''''''''''''''''''''*/
	NXP_FLASH_RET_SUCCESS = 0,			/**<		*/
	NXP_FLASH_RET_INV_CMD,				/**<		*/
	NXP_FLASH_RET_SRC_ADDR,				/**<		*/
	NXP_FLASH_RET_DST_ADDR,				/**<		*/
	NXP_FLASH_RET_SRC_NOT_MAPPED,		/**<		*/
	NXP_FLASH_RET_DST_NOT_MAPPED,		/**<		*/
	NXP_FLASH_RET_COUNT_ERR,			/**<		*/
	NXP_FLASH_RET_INV_SECTOR,			/**<		*/
	NXP_FLASH_RET_NOT_BLANK,			/**<		*/
	NXP_FLASH_RET_NOT_PREPARED,			/**<		*/
	NXP_FLASH_RET_CMP_ERROR,			/**<		*/
	NXP_FLASH_RET_BUSY,					/**<		*/
	NXP_FLASH_RET_PARAM_ERR,			/**<		*/
	NXP_FLASH_RET_ADDR_ERROR,			/**<		*/
	NXP_FLASH_RET_ADDR_NOT_MAPPED,		/**<		*/
	NXP_FLASH_RET_CMD_LOCKED,			/**<		*/
	NXP_FLASH_RET_INV_CODE,				/**<		*/
	NXP_FLASH_RET_INV_BAUD,				/**<		*/
	NXP_FLASH_RET_INV_STOP,				/**<		*/
	NXP_FLASH_RET_READ_PROTECTION		/**<		*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *  \name	Watchdog modes
 */
/*@{*/
#define NXP_WDOG_ISR		0x02		/**< Generate interrupt             */
#define NXP_WDOG_RESET		0x03		/**< Reset                          */
/*@}*/

/**
 *  \name	Watchdog clock sources
 */
/*@{*/
#define NXP_WDOG_CLK_RC		0x00		/**< Internal RC oscillator         */
#define NXP_WDOG_CLK_APB	0x01		/**< APB peripheral clock           */
#define NXP_WDOG_CLK_RTC	0x02		/**< RTC oscillator                 */
/*@}*/

/**
 * 	\name	DMA endianness
 */
/*@{*/
#define NXP_DMA_BIG_END		(1<<1)		/**< Big endian						*/
#define NXP_DMA_LITTLE_END	(0<<1)		/**< Little endian					*/
/*@}*/

#if defined(TARGETFAMILY_LPC178x_7x) || (TARGET_SELECTED & TARGET_CM4) != 0

/**
 * 	\name	DMA request selection
 */
/*@{*/
#define NXP_DMA_8_TMR0_M0	(1<<0)		/**< Timer 0 match 0				*/
#define NXP_DMA_9_SDCARD	(0<<1)		/**< SD card interface				*/
#define NXP_DMA_9_TMR0_M1	(1<<1)		/**< Timer 0 match 1				*/
#define NXP_DMA_10_SSP0_TX	(0<<2)		/**< SSP0 TX						*/
#define NXP_DMA_10_TMR1_M0	(1<<2)		/**< Timer 1 match 0				*/
#define NXP_DMA_10_SSP0_RX	(0<<3)		/**< SSP0 RX						*/
#define NXP_DMA_11_TMR1_M1	(1<<3)		/**< Timer 1 match 1				*/
#define NXP_DMA_10_SSP1_TX	(0<<4)		/**< SSP1 TX						*/
#define NXP_DMA_12_TMR2_M0	(1<<4)		/**< Timer 2 match 0				*/
#define NXP_DMA_10_SSP1_RX	(0<<5)		/**< SSP1 RX						*/
#define NXP_DMA_13_TMR2_M1	(1<<5)		/**< Timer 2 match 1				*/
#define NXP_DMA_10_SSP2_TX	(0<<6)		/**< SSP2 TX						*/
#define NXP_DMA_14_I2C_0	(1<<6)		/**< I2S channel 0					*/
#define NXP_DMA_10_SSP2_RX	(0<<7)		/**< SSP2 RX						*/
#define NXP_DMA_14_I2C_1	(1<<7)		/**< I2S channel 1					*/
#define NXP_DMA_10_UART0_TX	(0<<10)		/**< UART0 TX						*/
#define NXP_DMA_10_UART3_TX	(1<<10)		/**< UART3 TX						*/
#define NXP_DMA_11_UART0_RX	(0<<11)		/**< UART0 RX						*/
#define NXP_DMA_11_UART3_RX	(1<<11)		/**< UART3 RX						*/
#define NXP_DMA_12_UART1_TX	(0<<12)		/**< UART1 TX						*/
#define NXP_DMA_12_UART4_TX	(1<<12)		/**< UART2 TX						*/
#define NXP_DMA_13_UART1_RX	(0<<13)		/**< UART1 RX						*/
#define NXP_DMA_13_UART4_RX	(1<<13)		/**< UART4 RX						*/
#define NXP_DMA_14_UART2_TX	(0<<14)		/**< UART2 TX						*/
#define NXP_DMA_14_TMR3_M0	(1<<14)		/**< Timer 3 match 0				*/
#define NXP_DMA_15_UART2_RX	(0<<15)		/**< UART2 RX						*/
#define NXP_DMA_15_TMR3_M1	(1<<15)		/**< Timer 3 match 1				*/
/*@}*/

/**
 * 	\name	DMA channel connections.
 */
/*@{*/
#define NXP_DMA_MEMORY		0			/**< Memory							*/
#define NXP_DMA_SD			1			/**< SD card						*/
#define NXP_DMA_SSP0_TX		2			/**< SSP0 TX						*/
#define NXP_DMA_SSP0_RX		3			/**< SSP0 RX						*/
#define NXP_DMA_SSP1_TX		4			/**< SSP1 TX						*/
#define NXP_DMA_SSP1_RX		5			/**< SSP1 RX						*/
#define NXP_DMA_SSP2_TX		6			/**< SSP2 TX						*/
#define NXP_DMA_SSP2_RX		7			/**< SSP2 RX						*/
#define NXP_DMA_ADC			8			/**< ADC							*/
#define NXP_DMA_DAC			9			/**< DAC							*/
#define NXP_DMA_10			10			/**< UART0 Tx / UART3 Tx.			*/
#define NXP_DMA_11			11			/**< UART0 Rx / UART3 Rx.			*/
#define NXP_DMA_12			12			/**< UART1 Tx / UART4 Tx.			*/
#define NXP_DMA_13			13			/**< UART1 Rx / UART4 Rx.			*/
#define NXP_DMA_14			14			/**< UART2 Tx / T3_MAT[0].			*/
#define NXP_DMA_15			15			/**< UART2 Rx / T3_MAT[1].			*/
/*@}*/

#else

/**
 * 	\name	DMA request selection
 */
/*@{*/
#define NXP_DMA_8_UART0_TX	(0<<0)		/**< UART0 TX						*/
#define NXP_DMA_8_TMR0_M0	(1<<0)		/**< Timer 0 match 0				*/
#define NXP_DMA_9_UART0_RX	(0<<1)		/**< UART0 RX						*/
#define NXP_DMA_9_TMR0_M1	(1<<1)		/**< Timer 0 match 1				*/
#define NXP_DMA_10_UART1_TX	(0<<2)		/**< UART1 TX						*/
#define NXP_DMA_10_TMR1_M0	(1<<2)		/**< Timer 1 match 0				*/
#define NXP_DMA_11_UART1_RX	(0<<3)		/**< UART1 RX						*/
#define NXP_DMA_11_TMR1_M1	(1<<3)		/**< Timer 1 match 1				*/
#define NXP_DMA_12_UART2_TX	(0<<4)		/**< UART2 TX						*/
#define NXP_DMA_12_TMR2_M0	(1<<4)		/**< Timer 2 match 0				*/
#define NXP_DMA_13_UART2_RX	(0<<5)		/**< UART2 RX						*/
#define NXP_DMA_13_TMR2_M1	(1<<5)		/**< Timer 2 match 1				*/
#define NXP_DMA_14_UART3_TX	(0<<6)		/**< UART3 TX						*/
#define NXP_DMA_14_TMR3_M0	(1<<6)		/**< Timer 3 match 0				*/
#define NXP_DMA_15_UART3_RX	(0<<7)		/**< UART3 RX						*/
#define NXP_DMA_15_TMR3_M1	(1<<7)		/**< Timer 3 match 1				*/
/*@}*/

/**
 * 	\name	DMA channel connections.
 */
/*@{*/
#define NXP_DMA_MEMORY		0			/**< Memory							*/
#define NXP_DMA_SSP0_TX		0			/**< SSP0 TX						*/
#define NXP_DMA_SSP0_RX		1			/**< SSP0 RX						*/
#define NXP_DMA_SSP1_TX		2			/**< SSP1 TX						*/
#define NXP_DMA_SSP1_RX		3			/**< SSP1 RX						*/
#define NXP_DMA_ADC			4			/**< ADC							*/
#define NXP_DMA_I2S_0		5			/**< I2S channel 0					*/
#define NXP_DMA_I2S_1		6			/**< I2S channel 1					*/
#define NXP_DMA_DAC			7			/**< DAC							*/
#define NXP_DMA_8			8			/**< UART0 Tx / MAT0.0				*/
#define NXP_DMA_9			9			/**< UART0 Rx / MAT0.1				*/
#define NXP_DMA_10			10			/**< UART1 Tx / MAT1.0 				*/
#define NXP_DMA_11			11			/**< UART1 Rx / MAT1.1				*/
#define NXP_DMA_12			12			/**< UART2 Tx / MAT2.0 				*/
#define NXP_DMA_13			13			/**< UART2 Rx / MAT2.1				*/
#define NXP_DMA_14			14			/**< UART3 Tx / MAT3.0				*/
#define NXP_DMA_15			15			/**< UART3 Rx / MAT3.1				*/
/*@}*/

#endif

/**
 * 	\name	DMA transfer types.
 */
/*@{*/
#define NXP_DMA_MEM_TO_MEM	0			/**< Memory to memory				*/
#define NXP_DMA_MEM_TO_PER	1			/**< Memory to peripheral			*/
#define NXP_DMA_PER_TO_MEM	2			/**< Peripheral to memory			*/
#define NXP_DMA_PER_TO_PER	3			/**< Peripheral to peripheral		*/
/*@}*/

/**
 * 	\name	DMA transfer completion codes.
 */
/*@{*/
#define NXP_DMA_OK			1			/**< DMA transfer succesfull		*/
#define NXP_DMA_ERROR		0			/**< DMA transfer error				*/
/*@}*/

/**
 * 	\name	DMA definitions.
 */
/*@{*/
#define NXP_DMA_INV_CHNL	0xFF		/**< Invalid DMA channel			*/
/*@}*/

/**
 * 	\name	NXP controllers flash sectors counts.
 */
/*@{*/
#define NXP_IAP_SECTOR_COUNT_LPC11U68	(29)
/*@}*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/**
 *	Kick the watchdog
 */
#define nxp_wdogKick()				nxp_wdogKickIsr()

/**
 *	Generate reset through wrong watchdog feed sequence
 */
#define nxp_wdogGenerateReset()		nxp_wdogGenerateResetIsr()

/** 
 *
 *	\param	s_		source peripheral
 *	\param	d_		destination peripheral
 *	\param	t_		transfer type
 */
#define nxp_dmaConfig(s_, d_, t_)											\
	(((s_) << 1) | ((d_) << 6) | ((t_) << 11))

/** 
 *
 *	\param	ts_		transfer size	
 *	\param	ss_		source burst size (0=1byte)
 *	\param	ds_		destination burst size (0=1byte)
 *	\param	sw_		source transfer width (0=8-bit)
 *	\param	dw_		destination transfer width (0=8-bit)
 *	\param	si_		source increment (0, 1)
 *	\param	di_		destination icnrements (0, 1)
 *	\param	l_		last block (0, 1)
 */
#define nxp_dmaChnlConfig(ts_,ss_,ds_,sw_,dw_,si_,di_,l_)				\
	((ts_)|((ss_)<<12)|((ds_)<<15)|((sw_)<<18)|((dw_)<<21)|((si_)<<26)|((di_)<<27)|((l_)<<31))

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

typedef void (*				nxp_dmaCallback)(void *, Uint8);

typedef Uint8				nxp_DmaChannel;

/**
 * 	DMA channel config type.
 */
typedef struct							/*''''''''''''''''''''''''''''' RAM */
{										/*									*/
	nxp_dmaCallback			cb;			/**< Completion callback			*/
	void *					pData;		/**< Callback data ptr				*/
	Uint32					ctrl;		/**< DMA control register bits		*/
} nxp_DMAReserveInfo;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * 	DMA request type.
 */
typedef struct							/*''''''''''''''''''''''''''''' RAM */
{										/*									*/
	Uint32					src;		/**< Source address					*/
	Uint32					dst;		/**< Destination address			*/
	Uint32					next;		/**< Address of next linked request	*/
	Uint32					ctrl;		/**< Control bits					*/
} nxp_DMARequest;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/**
 * 	IAP flash sector define.
 */
typedef struct							/*''''''''''''''''''''''''''''' RAM */
{										/*									*/
	Uint32					start;		/**< Start address					*/
	Uint32					end;		/**< End address					*/
} nxp_FlashSector;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type used to store UART settings in HW register value format.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8					fdr;		/**< Fractional divider register.	*/
	Uint8					dlm;		/**< Divisor latch MSB register.	*/
	Uint8					dll;		/**< Divisor latch LSB register.	*/
} nxp_UartCfg;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*	
 *  Flash handling functions
 */
extern Uint32				nxp_flashPrepareSectors(Uint32, Uint32);
extern Uint32				nxp_flashCopyToFlash(Uint32,Uint32,Uint32,Uint32);
extern Uint32				nxp_flashErase(Uint32, Uint32, Uint32);
extern Uint32				nxp_flashBlankCheck(Uint32, Uint32);
extern Uint32				nxp_flashReadId(void);
extern Uint32				nxp_flashReadBootVersion(void);
extern Uint32				nxp_flashCompare(Uint32, Uint32, Uint32);
extern void					nxp_flashReinvokeISP(void);

extern void					nxp_flashInit(void);
extern Uint32				nxp_flashWrite(Uint32, void *, Uint32, Uint32);
extern Uint32				nxp_flashRead(void *, Uint32, Uint32);

extern Uint32				nxp_flashSectorFromAddress(Uint32);

/*	
 *  Watchdog functions
 */
extern void					nxp_wdogInit(Uint32, Uint32, Uint32);
extern void					nxp_wdogKickIsr(void);
extern void					nxp_wdogGenerateResetIsr(void);
extern Uint8				nxp_wdogGetTimeoutFlag(void);

/*
 *	DMA functions.
 */
extern void					nxp_dmaConfigure(Uint32, Uint32, Uint32);
extern void					nxp_dmaEnable(void);
extern void					nxp_dmaDisable(void);
extern void					nxp_dmaChannelConfigure(
								nxp_DmaChannel, 
								nxp_DMAReserveInfo *
							);
extern void					nxp_dmaChannelRequest(nxp_DmaChannel, nxp_DMARequest *);
extern void 				nxp_dmaChannelDisable(nxp_DmaChannel);

/*
 *	IAP functions.
 */
extern void					nxp_iapInit(nxp_FlashSector const_P *, Uint8, Uint32);
extern Uint32				nxp_iapPrepareSectors(Uint32, Uint32);
extern Uint32				nxp_iapEraseSectors(Uint32, Uint32);
extern Uint32				nxp_iapWritePages(Uint32, BYTE *, Uint32);
extern Boolean				nxp_iapWrite(Uint32, BYTE *, Uint32);
extern Boolean				nxp_iapReadUniqueID(Uint32 *);

/*
 *	UART functions.
 */

void nxp_uartCalcBaudrate(Uint32 pclk,Uint32 baudrate,nxp_UartCfg * pSettings);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern const_P nxp_FlashSector nxp_lpc11u68FlashSectors[];

/***************************************************************//** \endcond */

#endif

