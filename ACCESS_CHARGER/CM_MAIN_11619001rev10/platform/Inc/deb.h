/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DEB.H
*
*	\ingroup	DEB
*
*	\brief		Debugging functions header.
*
*	\details
*
*	\note
*
*	\version	02-08-07 / Tommi Nakkila / Added deb_assert and deb_verify
*				macros.
*
*******************************************************************************/

#ifndef DEB_H_INCLUDED
#define	DEB_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <stdarg.h>
#include <assert.h>
#include "global.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#ifndef DEB_ENABLE_LOGGING
#define DEB_ENABLE_LOGGING	0				/**< Logging enable off by dflt	*/
#endif

/**
 *	\name	Logging flags
 */

/*@{*/
#define DEB_LOG_OSA			0x01			/**< OSA debug logging flag     */
#define DEB_LOG_MEM			0x02			/**< MEM debug logging flag     */
#define DEB_LOG_SYS			0x04			/**< SYS debug logging flag     */
#define DEB_LOG_APPL		0x80			/**< Applications debug logging
												flag                        */
/*@}*/

/**
 *	\name	Log callback flags
 */

/*@{*/
#define DEB_LOG_LAST		0x8000			/**< Last char in output		*/
#define DEB_LOG_FIRST		0x4000			/**< First char in output		*/
#define DEB_LOG_ISR			0x2000			/**< Isr output					*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*
 *  Macro for debugging. Expressions inside DEBUG call are enabled only in
 *	debug build.
 */

#ifndef NDEBUG

/**
 *	Assert that returns zero. The return value is needed in some macros.
 *	This define is only intended to be used by the platform.
 */

#define deb__assertRet(expr_) \
	((expr_) ? (void)0 : deb__assertInt(SYS_FILE_NAME, (Uint16) __LINE__), 0)

/**
 *	\brief		Calls assertion handler if expression evaluates to false.
 *
 *	\param		expr_	Expression to be evaluated.
 *
 *	\details	In debug build (NDEBUG not defined) evaluates an expression and
 *				when the result is FALSE, calls user defined assertion handler.
 *
 *				In release build (NDEBUG defined) the macro evaluates to an
 *				empty macro and thus any code inside the asserted expression
 *				disappears too.
 *
 *				Requires header file deb.h. Assertion handler function
 *				project_assert must exist in the project and will be called if
 *				the assert fails.
 *
 *	\note		Macro requires a const char variable named sys__filename to be
 *				present in the file. Macro SYS_DEFINE_FILE_NAME creates this
 *				variable automatically.
 */

# define deb_assert(expr_) \
	((void) deb__assertRet(expr_))

/**
 *	\brief		In debug mode evaluates an expression and when the result is
 *				FALSE, calls user defined assertion handler. In release mode
 *				expression is excuted normally.
 *
 *	\param		expr_	Expression to be evaluated.
 *
 *	\details	In debug build (NDEBUG not defined) evaluates an expression and
 *				when the result is FALSE, calls user defined assertion handler
 *				just like deb_assert.
 *
 *				In release build (NDEBUG defined) the code is executed normally.
 *
 *				Requires header file deb.h.
 *
 *				Assertion handler function project_assert must exist in the
 *				project and will be called if the assert fails.
 *
 *	\note		Macro requires a const char variable named sys__filename to be
 *				present in the file. Macro SYS_DEFINE_FILE_NAME creates this
 *				variable automatically (see example below).
 */

# define deb_verify(expr_)	deb_assert(expr_)

/**
 *	\brief		Code to be executed only in debug build.
 *
 *	\param		exp		Code to be executed only in debug build.
 *
 *	\details	The assert macro expands to nothing when building release build.
 */

# define DEB_DBG(exp)		exp

#else

# define deb__assertRet(expr_) 0
# define deb_assert(expr_)
# define deb_verify(expr_)	expr_
# define DEB_DBG(exp)

#endif

/**
 * \brief	Static assertion that causes compilation error if the expression
 *			evaluates to false.
 *
 * \param	expr_	Expression to be evaluated. Must be a constant expression
 *					that can be evaluated during compilation.
 * \param	name_	Name of the assertion check. The name must be unique within
 *					the scope deb_staticAssert is used in.
 *
 * \details			Static assert should be preferred over run-time asserts when
 *					possible.
 *
 *					A comment with the purpose of the check should be added 
 *					before the usage of this macro. The evaluated expression is
 *					not always enough to indicate why the assertion has been 
 *					made.
 *			
 *					This macro uses the standard static assert macro if it is
 *					available in the compiler.
 */

#if defined(static_assert)
# define deb_staticAssert(expr_, name_) static_assert((expr_), #name_ ": " #expr_)
#else
# define deb_staticAssert(expr_, name_) typedef Uint8 (name_)[		\
	(expr_) ? 1 : -1												\
]																		
#endif

#if DEB_ENABLE_LOGGING == 1


#define DEB_MSG_INIT()			deb_logOsaInit()
#define DEB_MSG_REGISTER(f_)	deb_logRegLogger(f_)
#define DEB_MSG(x_)				deb_log0(x_, FALSE)
#define DEB_MSG_ISR(x_)			deb_log0(x_, TRUE)
#define DEB_MSG_ARGS(x_)		deb_logN x_		
#define DEB_MSG_ARGS_ISR(x_)	deb_logNIsr x_	

/*
 *	Deprecated, do not use these macros any more
 */
#define deb_log(x_)				deb_log0(x_, FALSE)
#define deb_logIsr(x_)			deb_log0(x_, TRUE)
#define deb_logNList(x_,y_)		deb_logNXList(x_, FALSE, y_)
#define deb_logNListIsr(x_,y_)	deb_logNXList(x_, TRUE, y_)

#else

/**
 * \brief		Initializes debug logger. To be called after osa_init has
 *				been called.
 */
 
#define DEB_MSG_INIT()

/**
 * \brief		Registers logger function.
 *
 * \param		f_		Pointer to the logger function.
 *
 * \details		This macro should be used before OSA is started. Before this 
 *				debug logging will not work.
 */
 
#define DEB_MSG_REGISTER(f_)

/**
 * \brief		Write debug message without arguments.
 *
 * \param		x_		Debug message
 *
 * \note		Must not be called from an ISR!
 */
 
#define DEB_MSG(x_)

/**
 * \brief		Write debug message without arguments from ISR.
 *
 * \param		x_		Debug message
 */
 
#define DEB_MSG_ISR(x_)

/**
 * \brief		Write debug message with args.
 *
 * \param		x_		Format string and arguments
 *
 *	\details	Usage example: DEB_LOG_ARGS(("Foo = %d", 1))
 */
 
#define DEB_MSG_ARGS(x_)

/**
 * \brief		Write debug message with args from isr.
 *
 * \param		x_		Format string and arguments
 *
 *	\details	Usage example: DEB_LOG_ARGS_ISR(("Foo = %d", 1))
 */
 
#define DEB_MSG_ARGS_ISR(x_)

/*
 *	Deprecated, do not use these macros any more
 */
#define deb_log(x_)			
#define deb_logIsr(x_)		
#define deb_logNList(x_,y_)		
#define deb_logNListIsr(x_,y_)	

#endif

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	\brief		Type for debug logging function implemented for a project.
 *
 *	\param		code	A code containing an ASCII character and/or status
 *						flags. The lower 8-bits contain an ASCII character. The
 *						upper 8-bits contain flags.
 *	
 *	\details	The user must implement a function of this type for debug
 *				logging to work. The DEB FB will call the function for each
 *				character in a debug message.
 */
 
typedef void (*				deb_FnLog)(Uint16 code);

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PUBLIC void					deb__assertInt(char const_P *, Uint16);

/* Debug logging */
PUBLIC void					deb_logOsaInit(void);
PUBLIC void					deb_logRegLogger(deb_FnLog);
PUBLIC void					deb_log0(char const_P *, Boolean);
PUBLIC void					deb_logN(char const_P *, ...);
PUBLIC void					deb_logNIsr(char const_P *, ...);
PUBLIC void					deb_logNXList(char const_P *, Boolean, va_list);

/* Assert storage */
void						deb_initAssertStorage(void *);
Uint16						deb_getAssertRow(void);
void						deb_getAssertFile(char *);
void						deb_clearAssertStorage(void);
void						deb_storeAssert(char const_P *,Uint16);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG
extern Uint8				deb_logMask;	/* Debug logging mask			*/
#endif

/***************************************************************//** \endcond */

#endif
