/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NOV.H
*
*	\ingroup	NOV
*
*	\brief		Declares the interface to the non-volatile memory manager. The
*				implementation may vary from a project to another thus here are
*				no implementation specific definitions.
*
*	\details		
*
*	\note		
*
*	\version	xx-xx-xxxx / xx
*
*******************************************************************************/

#ifndef NOV_H_INCLUDED
#define NOV_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#ifndef	TOOLS_H_INCLUDED
# error	"TOOLS.H must be included before NOV.H"
#endif

#ifndef	SYS_H_INCLUDED
# error	"SYS.H must be included before NOV.H"
#endif

#ifndef	HW_H_INCLUDED
#include "hw.h"
#endif

#ifndef LIB_TODO_H_INCLUDED
#include "todo.h"
#endif

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*
 *	Service codes for the interface ('nov_Interface'). Internal use only.
 */

#define NOV__RESERVE		((Uint8)0)
#define NOV__PUT			((Uint8)1)
#define NOV__GET			((Uint8)2)
#define NOV__STORE			((Uint8)3)

/*
 *	Return codes.
 */

#define NOV_OK				((Uint8)0)	/**< OK / found that data (reserve) */
#define NOV_NEW				((Uint8)1)	/**< Data not found, created, uninit!*/
#define NOV__ERR			((Uint8)2)	/**< Error occurred, no service     */
#define NOV_DOWN			((Uint8)3)	/**< NOV not operational at the moment*/

/*_______________________________________________________
 *
 * Obsolete definitions - must not be used in new design
 *_______________________________________________________
 */

#if (TARGET_SELECTED & TARGET_AVR)
#define NOV_FLASH_READ		1
#else
#define NOV_FLASH_READ		1
#endif

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*_______________________________________________________
 *
 * Obsolete definitions - must not be used in new design
 *_______________________________________________________
 */

#define nov_plug_avr()		(nov_table_ptr = &nov_avr_impl)

/*
 *	Initializes NOV info structure.
 *	s_ = ptr to the info structure
 *	c_ = callback function
 */
#define nov_initInfo(s_, c_)												\
	{																		\
		(s_)->todo.pNext		= NULL;										\
		(s_)->todo.pCallback	= (c_);										\
		(s_)->nSize				= 0;										\
	}

/*
 *	Macros for calling implementation dependent functions.
 */
#define nov_init()			(nov_table_ptr->Init())
#define nov_read(a_)		(nov_table_ptr->Read(a_))
#define nov_write(a_)		(nov_table_ptr->Write(a_))
#define nov_enableWrite(b_)	(nov_table_ptr->EnableWrite(b_))

#if NOV_FLASH_READ == 1

#undef nov_write
#define nov_write(a_)														\
	{																		\
		(a_)->nFlags = 0;													\
		(nov_table_ptr->Write(a_));											\
	}

#define nov_writeConst(a_)													\
	{																		\
		(a_)->nFlags = 1;													\
		(nov_table_ptr->Write(a_));											\
	}
#endif

/*
 *	Check success of read / write operation. Can be used only after the
 *	callback has been called.
 *	a_ = ptr into the used info structure
 */
#define nov_succeeded(a_)	((a_)->nSize == 0)

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*
 *	Define the data types:
 *
 *	 nov_AddrType	=Type of logical NOV address. This should cover (only one
 *					 of) the data area(s) in NOV.
 *	 nov_SizeType	=Type of the size of an element in the NOV memory. This
 *					 should cover (only one of) the data area(s) in NOV.
 *	 nov_MemAddr	=Type of physical NOV address. This should cover entire
 *					 NOV memory.
 *
 *	Types 'nov_AddrType' and 'nov_SizeType' are practically the same unless
 *	the former is actually an address i.e. a pointer definition.
 */

#if (TARGET_SELECTED & TARGET_AVR)
# if HW_EEPROM <= 512
typedef Uint8				nov_AddrType;
typedef Uint8				nov_SizeType;
#  if HW_EEPROM <= 256
typedef Uint8				nov_MemAddr;
#  else
typedef Uint16				nov_MemAddr;
#  endif
# else
typedef Uint16				nov_AddrType;
typedef Uint16				nov_SizeType;
typedef Uint16				nov_MemAddr;
# endif

#elif (TARGET_SELECTED & TARGET_W32)
typedef Uint16				nov_AddrType;
typedef Uint16				nov_SizeType;
typedef Uint16				nov_MemAddr;

#elif (TARGET_SELECTED == TARGET_M16C)
typedef Uint16				nov_AddrType;
typedef Uint16				nov_SizeType;
typedef Uint16				nov_MemAddr;

#else
typedef Uint32				nov_AddrType;
typedef Uint32				nov_SizeType;
typedef Uint32				nov_MemAddr;

#endif

/**
 * Alloc info type
 */

typedef struct nov__allocInfo {			/*''''''''''''''''''''''''''''' RAM	*/
	struct nov__allocInfo *	link;		/**< Must be the first element      */
	sys_FBInstId			iid;		/**< Identification of the instance */
	Uint8					hNov;		/**< NOV-handle (internal use only) */
	void *					data;		/**< Source/target buffer in RAM    */
	nov_SizeType			size;		/**< Number of BYTEs to deal with   */
	nov_SizeType			offs;		/**< BYTE offset to the NOV-r/w area*/
	void (*					done)(void *,struct nov__allocInfo *);
} nov_AllocInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * NOV interface type
 */
 
typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8 (*				novFn)(Uint8,nov_AllocInfo *);
} nov_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*_______________________________________________________
 *
 * Obsolete definitions - must not be used in new design
 *_______________________________________________________
 */

#if (TARGET_SELECTED & TARGET_AVR)

# if HW_EEPROM <= 512

typedef Uint8				nov_AddrSize;
typedef Uint8				nov_WriteSize;

# else

typedef Uint16				nov_AddrSize;
typedef Uint16				nov_WriteSize;

# endif

#elif (TARGET_SELECTED & TARGET_W32)

typedef Uint16				nov_AddrSize;
typedef Uint16				nov_WriteSize;

#elif (TARGET_SELECTED == TARGET_M16C)

typedef Uint16				nov_AddrSize;
typedef Uint16				nov_WriteSize;

#else
typedef Uint32				nov_AddrSize;
typedef Uint32				nov_WriteSize;
#endif

/*
 *	Struct for NOV read and write calls.
 */

typedef struct {
	todo_Link_st			todo;		/**< Todo struct for linking        */

#if NOV_FLASH_READ == 1
	union {
		BYTE *				pRamData;
		BYTE const_P *		pConstData;
	} data;
	Uint8					nFlags;
#else
	BYTE *					pData;		/* Ptr to beginning of the data		*/
#endif

	nov_AddrSize			nAddr;		/* Offset of the data				*/
	nov_WriteSize			nSize;		/* Size of the data					*/
} nov_Info_st;

/*
 *	Struct for functions pointers of NOV.
 */

typedef struct {
	void (*					Init)(void);
	void (*					Read)(nov_Info_st *);
	void (*					Write)(nov_Info_st *);
	void (*					EnableWrite)(Boolean);
} nov_Impl_st;

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

Uint8 nov_reserve(nov_AllocInfo *);
Uint8 nov_put(nov_AllocInfo *);
Uint8 nov_get(nov_AllocInfo *);
Uint8 nov_store(void);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern nov_Interface nov_if;

/*_______________________________________________________
 *
 * Obsolete definitions - must not be used in new design
 *_______________________________________________________
 */

#if (TARGET_SELECTED & TARGET_AVR)
extern const nov_Impl_st	nov_avr_impl;

#elif (TARGET_SELECTED & TARGET_W32)
extern const nov_Impl_st	nov_avr_impl;

#elif (TARGET_SELECTED == TARGET_M16C)
extern const nov_Impl_st	nov_avr_impl;

#endif

extern const nov_Impl_st *	nov_table_ptr;

/***************************************************************//** \endcond */

#endif
