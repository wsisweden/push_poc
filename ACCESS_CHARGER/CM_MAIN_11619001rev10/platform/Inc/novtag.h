/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		novtag.h
*
*	\brief		Macros and types for creating register tag tables.
*
*	\details	Register tags can be used when storing register data to
*				non-volatile memory. Each register is then given an unique tag.
*				The tag is stored to the non-volatile memory with the data. The 
*				tag is used to identify the register when loading data from the
*				non-volatile memory.
*
*				The register NOV image offset or the global register handle
*				could also be used to identify the memory but both may change
*				when adding new non-volatile registers to a project. The tag for
*				a register will remain the same as long as the settings of the
*				register is unmodified.
*
*				Using tags makes it possible to add and remove non-volatile 
*				registers without loosing compatibility with values stored in
*				the non-volatile memory.
*
*				A register must be given a new tag if the register settings are
*				modified so that the old data is not compatible with the new 
*				data. Any of the following modifications will do that:
*				- The register data type is changed e.g. from Uint8 to Uint16
*				- The register type is changed e.g. from NN to NS
*				- The register max or min value is changed
*				- The length of a string register is reduced
*
*				The unique register tags may also be used for other purposes
*				where an unique identifier is needed across software versions.
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef NOVTAG_H_INCLUDED
#define NOVTAG_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/**
 *	\brief		Macro for declaring registers in the tag/offset tables.
 *	
 *	\param		type_	Register type.
 *	\param		name_	Register name.
 *	\param		inst_	Instance name.
 *	\param		tag_	Register tag that will be used when storing the register
 *						to non-volatile memory.
 *						
 *	\details	This macro can be used to declare registers in the tag/offset
 *				tables.
 */
 
#define NOVTAG__REG(type_,name_,inst_,tag_)									\
	{																		\
		(Uint16) offsetof(													\
			exe__ImageRAM,													\
			exe__##type_.exe_i_##name_##_##inst_							\
		),																	\
		tag_																\
	}
	
/**
 *	\brief		Macro for declaring array registers in the tag/offset tables.
 *	
 *	\param		type_	Register type.
 *	\param		name_	Register name.
 *	\param		arrIdx_	Register array index.
 *	\param		inst_	Instance name.
 *	\param		tag_	Register tag that will be used when storing the register
 *						to non-volatile memory.
 *						
 *	\details	This macro can be used to declare array registers in the 
 *				tag/offset tables.
 */
 
#define NOVTAG__AREG(type_,name_,arrIdx_,inst_,tag_)						\
	{																		\
		(Uint16) offsetof(													\
			exe__ImageRAM,													\
			exe__##type_.exe_i_##name_##_##inst_[arrIdx_]					\
		),																	\
		tag_																\
	}
	
/**
 *	\brief		Macro for declaring NS registers in the tag/offset tables.
 *	
 *	\param		name_	Register name.
 *	\param		inst_	Instance name.
 *	\param		tag_	Register tag that will be used when storing the register
 *						to non-volatile memory.
 *						
 *	\details	This macro can be used to declare registers in the tag/offset
 *				tables.
 */
 
#define NOVTAG_REG_NS(name_,inst_,tag_)	NOVTAG__REG(ns, name_, inst_, tag_)
	
/**
 *	\brief		Macro for declaring NN registers in the tag/offset tables.
 *	
 *	\param		name_	Register name.
 *	\param		inst_	Instance name.
 *	\param		tag_	Register tag that will be used when storing the register
 *						to non-volatile memory.
 *						
 *	\details	This macro can be used to declare registers in the tag/offset
 *				tables.
 */
 
#define NOVTAG_REG_NN(name_,inst_,tag_)	NOVTAG__REG(nn, name_, inst_, tag_)

/**
 *	\brief		Macro for declaring array NN registers in the tag/offset tables.
 *	
 *	\param		name_	Register name.
 *	\param		arrIdx_	Register array index.
 *	\param		inst_	Instance name.
 *	\param		tag_	Register tag that will be used when storing the register
 *						to non-volatile memory.
 *						
 *	\details	This macro can be used to declare array registers in the 
 *				tag/offset tables
 */
 
#define NOVTAG_AREG_NN(name_,arrIdx_,inst_,tag_)	\
	NOVTAG__AREG(nn, name_, arrIdx_, inst_, tag_)
	
/**
 *	\brief		Macro for declaring NT registers in the tag/offset tables.
 *	
 *	\param		name_	Register name.
 *	\param		inst_	Instance name.
 *	\param		tag_	Register tag that will be used when storing the register
 *						to non-volatile memory.
 *						
 *	\details	This macro can be used to declare registers in the tag/offset
 *				tables.
 */
 
#define NOVTAG_REG_NT(name_,inst_,tag_)	NOVTAG__REG(nt, name_, inst_, tag_)

/**
 *	\brief		Macro for declaring NT array registers in the tag/offset tables.
 *	
 *	\param		name_	Register name.
 *	\param		arrIdx_	Register array index.
 *	\param		inst_	Instance name.
 *	\param		tag_	Register tag that will be used when storing the register
 *						to non-volatile memory.
 *						
 *	\details	This macro can be used to declare registers in the tag/offset
 *				tables.
 */

#define NOVTAG_AREG_NT(name_,arrIdx_,inst_,tag_)	\
	NOVTAG__AREG(nt, name_, arrIdx_, inst_, tag_)

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type for storing paired NOV image offset and unique register tag. The tag
 *	can be set to anything as long as no other register has the same tag.
 * 
 *	The NOVTAG_REG_* macros can be used when defining an array of this type.
 */
 
typedef struct novtag__otPair {			/*''''''''''''''''''''''''''' CONST */
	Uint16 					offset;		/**< NOV image offset.				*/
	Uint16 					tag;		/**< Register tag.					*/
} novtag_OtPair;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type used to store information about non-volatile tag tables.
 */

typedef struct novtag__tagInfo {		/*''''''''''''''''''''''''''' CONST */
	Uint16					tagCount;	/**< Number of tags.				*/
	novtag_OtPair const_P * pTagsByOffs;/**< Ptr to table in offset order.	*/
	novtag_OtPair const_P * pTagsByTag;	/**< Ptr to table in tag order.		*/
} novtag_TagInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
