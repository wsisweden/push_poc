/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		IICMSTR.H
*
*	\ingroup	IICMSTR
*
*	\brief		IICMSTR header.
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef IICMSTR_H_INCLUDED
#define IICMSTR_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "protif.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

#define IICMSTR_INV_ADDR	0xFFFF

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

#if (TARGET_SELECTED & (TARGET_ARM7 | (TARGET_CM0 | TARGET_CM3 | TARGET_CM4))) != 0

/**
 *	Calculates CLLL and CLLH (assuming they are the same).
 *	<pre>
 *                PCLK
 *	bit freq = -----------
 *             CLLL + CLLH
 *
 *	c_ = main clock Hz
 *	s_ = desired bit frequency
 * 	</pre>
 */

# define iicmstr_speed(c_,s_)	(c_ /  (2 * s_))

#endif

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

typedef struct iicmstr__Inst iicmstr_Inst;		/*!< Instance type			*/


#if (TARGET_SELECTED & (TARGET_ARM7 | (TARGET_CM0 | TARGET_CM3 | TARGET_CM4))) != 0

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint32					CLLL;		/**<                                */
	Uint32					CLLH;		/**<                                */
} iicmstr_Settings;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#elif TARGET_SELECTED & TARGET_W32

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint32					CLLL;		/*!<								*/
	Uint32					CLLH;		/*!<								*/
} iicmstr_Settings;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#elif TARGET_SELECTED & TARGET_S4F4

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint32					busClock;	/*!< The bus clock frequency in Hz.	*/
	Uint16 					maxCombSize;/**< Maximum transfer size for blocks
										 * comined with the continue flag.	*/
} iicmstr_Settings;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#else
/* 
 *	Target not supported. Not using #error directive here because this header
 *	may be included in other function blocks even when IICMSTR is not used
 *	in the project. That should be allowed.
 */
typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8					dummy;		/**< Dummy member.					*/
} iicmstr_Settings;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif

/**
 *	I2C master interface
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	void (*					fnInit)(iicmstr_Inst *);	/**< Init function  */
	void (*					fnProcess)(void);	/**< Read / write handler   */
} iicmstr__If;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	I2C slave information
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8					address;	/**< Slave address                  */
} iicmstr_SlaveInfo;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

struct iicmstr_aardData;

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

Uint8 project_iicmstrSimu(protif_Request *, Uint8);

/* Windows I2C with Aardvark */
struct iicmstr_aardData * iicmstr_aardvarkInit(int,int,Uint32);
Uint8 iicmstr_aardvark(struct iicmstr_aardData *,protif_Request *);
int iicmstr_aardvarkGpioDir(struct iicmstr_aardData * pAardData,Uint8 dirMask);
int iicmstr_aardvarkGpioSet(struct iicmstr_aardData * pAardData,Uint8 outMask);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern const_P protif_Interface iicmstr_interface;

extern const_P iicmstr__If	iicmstr_if0;
extern const_P iicmstr__If	iicmstr_if1NoPinInit;
extern const_P iicmstr__If	iicmstr_if0NoInit;
extern const_P iicmstr__If	iicmstr_if1;
extern const_P iicmstr__If	iicmstr_if1NoPinInit;
extern const_P iicmstr__If	iicmstr_if1NoInit;
extern const_P iicmstr__If	iicmstr_if2;
extern const_P iicmstr__If	iicmstr_if2NoPinInit;
extern const_P iicmstr__If	iicmstr_if2NoInit;
extern const_P iicmstr__If	iicmstr_if3;
extern const_P iicmstr__If	iicmstr_if4;
extern const_P iicmstr__If	iicmstr_if5;

#if TARGET_SELECTED & TARGET_S4F4
extern const_P iicmstr__If	iicmstr_if3SclPa8;
extern const_P iicmstr__If	iicmstr_if3NoPinInit;
#endif

/***************************************************************//** \endcond */

#endif
