/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_arm7/_OSA.H
*
*	\ingroup	OSA_ARM7
*
*	\brief		Target specific OSA declarations
*
*	\details		
*
*	\note 		Should be read by the OSA.H header only.
*
*	\version
*
*******************************************************************************/

/******************************************************************************
;
;	HEADER AND INCLUDE FILES
;
******************************************************************************/

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

/******************************************************************************
;
;	CONSTANTS AND DEFINITIONS
;
******************************************************************************/

/*
 *	Signal flags for nSignal field of osa_TaskType
 */
 
#define OSA_SIGNAL_WAIT		0x01		/**< Task is waiting signal         */
#define OSA_SIGNAL_SET		0x02		/**< Task has been signaled         */

/******************************************************************************
;
;	DATA TYPES AND STRUCTURES
;
******************************************************************************/

typedef void (*				osa_InterruptFnPtr)(void);
typedef Uint8				osa_Status;
typedef void (*				osa__DebLogHandlerFn)(char *);
#define OSA_TASK			
typedef portTickType 		osa_TickType;

/*
 *	OSA objects.
 */

typedef struct {						/*''''''''''''''''''''''' SEMAPHORE	*/
	xSemaphoreHandle		sema;		/**< Internal to OSA                */
} osa_SemaType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct osa__task {				/*'''''''''''''''''''''''''''' TASK	*/
	tskTCB					task;		/**< FreeRTOS task (must be first)  */
	Uint32					nSignal;	/**< Flags for signaling            */
	void *					pUtil;		/**< Utility ptr to the task        */
	char const_P *			pName;		/**< Name of the task               */
} osa_TaskType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	DATA
;
******************************************************************************/


/******************************************************************************
;
;	MACROS
;
******************************************************************************/

#define osa_isrFastDecl(name_)		void name_(void) __attribute__((interrupt ("IRQ")))
#define osa_isrOSDecl(name_)		void name_(void) __attribute__((naked))

#define osa_isrFastFn(isr_,name_)	void name_(void) 
#define osa_isrOSFn(isr_,name_)		void name_(void) { portENTER_SWITCHING_ISR()


#define osa_init()					
#define osa_isrEnter()				
#define osa_isrLeave()				
#define osa_endOSIsr				portEXIT_SWITCHING_ISR( TRUE ) }

#define osa_newSemaSet(pS_)			vSemaphoreCreateBinary((pS_)->sema)
#define osa_newSemaTaken(pS_)												\
	{																		\
		vSemaphoreCreateBinary((pS_)->sema);								\
		osa_semaGet(pS_); /* todo: check this */					\
	}
#define osa_semaGet(pS_)			xSemaphoreTake((pS_)->sema, portMAX_DELAY)
#define osa_semaSet(pS_)			xSemaphoreGive((pS_)->sema)
#define osa_semaSetIsr(pS_)			xSemaphoreGiveFromISR((pS_)->sema, pdFALSE)
#define osa_semaWait(pS_,t_,pE_)											\
	{																		\
		if (xSemaphoreTake((pS_)->sema, (portTickType) (t_)) == pdTRUE)		\
		{																	\
			*(pE_) = 0;														\
		} else {															\
			*(pE_) = 1;														\
		}																	\
	}

/**
 *  Signal get / set are implmented using flags and task suspending and 
 *	resuming. 
 *	NOTICE! vTaskSuspend enables interrupts internally so there is no ENABLE in 
 *	else branch.
 */
#define osa_signalGet()														\
	{																		\
		osa_TaskType * pTask_ = osa_taskCurrent();							\
		DISABLE;															\
		if (pTask_->nSignal & OSA_SIGNAL_SET) {								\
			pTask_->nSignal &= ~OSA_SIGNAL_SET;								\
			ENABLE;															\
		} else {															\
			pTask_->nSignal |= OSA_SIGNAL_WAIT;								\
			vTaskSuspend((xTaskHandle)pTask_);								\
		}																	\
	}

#define osa_signalSend(pT_)													\
	{																		\
		DISABLE;															\
		if (((osa_TaskType*)(pT_))->nSignal & OSA_SIGNAL_WAIT) {			\
			((osa_TaskType*)(pT_))->nSignal &= ~OSA_SIGNAL_WAIT;			\
			ENABLE;															\
			vTaskResume((xTaskHandle)(pT_));								\
		} else {															\
			((osa_TaskType*)(pT_))->nSignal |= OSA_SIGNAL_SET;				\
			ENABLE;															\
		}																	\
	}

#define osa_signalWait(t_,pE_)		deb_assert(0) // Not implemented yet

#define osa_taskingDisable()		vTaskSuspendAll()
#define osa_taskingEnable()			xTaskResumeAll()

/* TODO:	xTaskGetSchedulerState is not available in the FreeRTOS version used
 *			for the ARM7 target. A newer FreeRTOS version should have the 
 *			function.
 *
 * #define osa_taskingIsDisabled()		(xTaskGetSchedulerState() == taskSCHEDULER_SUSPENDED)
 */

#define osa_taskCurrent()			((osa_TaskType *)xTaskGetCurrentTaskHandle())
#define osa_taskResume(pT_)			vTaskResume((xTaskHandle)(pT_))
#define osa_taskSuspend()			vTaskSuspend((xTaskHandle)osa_taskCurrent())
#define osa_taskWait(t_)			vTaskDelay((portTickType)t_)

#define osa_tick					vPreemptiveTick
#define	osa_getTick()				xTaskGetTickCount()

#define osa_getTaskName(p_)			((p_)->pName)

/******************************************************************************
;
;	FUNCTION PROTOTYPES
;
******************************************************************************/

void osa_newTask(osa_TaskType *,char const_P *,Uint8,void (OSA_TASK *)(void),Uint16);

/* FreeRTOS function in portISR.c */
extern void ( vPreemptiveTick )( void );
