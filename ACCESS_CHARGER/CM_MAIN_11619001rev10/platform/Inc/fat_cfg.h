/*
 * 	Dummy FAT configuration include file.
 *
 *	This file will not be included if a file with the same name is put in the
 *	project include directory.
 *
 *	The file in the project include directory can then override default FAT
 *  configuration.
 *
 *	The default configuration is in the fatfsconf.h file in the FAT function
 *	block.
 */
 