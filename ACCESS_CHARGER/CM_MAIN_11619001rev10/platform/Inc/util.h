/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		UTIL.H
*
*	\ingroup	UTIL
*
*	\brief		Collection of commonly used functions.
*
*	\details
*
*	\note
*
*	\version	3.8.2004   8:41
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	UTIL	UTIL
*
*	\brief		Collection of commonly used functions.
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#define util_ultoaLHex(a_,b_,c_)	util__ultoaHex(a_,b_,c_,'a')
#define util_ultoaUHex(a_,b_,c_)	util__ultoaHex(a_,b_,c_,'A')

#define util_isDecDigit(n_)			(((n_) >= '0') && ((n_) <= '9'))

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*
 *	util_str.c
 */

Boolean	util_cstrstr(char *, char const_P *);

Boolean	util_cstrcmp(char *, char const_P*);
Boolean	util_strcmp(char *, char *);
Boolean	util_strcmpNoCase(char *, char *);

Boolean	util_isdigit(char);
Boolean	util_isnumeric(char);
char	util_toupper(char);

Int32	util_atol(char *, char **);
Int32	util_atolConst(char const_P *, char const_P **);
char *	util_ltoaEx(char *, char *, Int32);
char *	util_ultoaEx(char *, char *, Uint32);

char *	util_findFirst(char *, char);
char *	util_findFirstOf(char *, char *);

char *	util__ultoaHex(char *, char *, Uint32, char);

/*
 *  Number to string conversions
 */

char *	util_i32toa(Int32 nVal,char * pBuf,Uint8 buffSize,Uint8 nBase);
char * 	util_i32toadec(Int32 nVal,char * pBuf,Uint8 nSize);
char *	util_u32toa(Uint32 nVal,char * pBuf,Uint8 buffSize,Uint8 nBase);
char *	util_u32toadec(Uint32 nVal,char * pBuf,char * pEnd);
char *	util_u32toahex(Uint32 nVal,char * pBuf,char * pEnd);

char *	util_i16toa(Int16 nVal,char * pBuf,Uint8 buffSize, Uint8 nBase);
char *	util_u16toa(Uint16 nVal,char * pBuf,Uint8 buffSize, Uint8 nBase);
char *	util_u16toadec(Uint16 nVal,char * pBuf,char * pEnd);
char *	util_u16toahex(Uint16 nVal,char * pBuf,char * pEnd);

char *	util_i8toa(Int8 nVal,char * pBuf,Uint8 buffSize,Uint8 nBase);
char *	util_u8toa(Uint8 nVal,char * pBuf,Uint8 buffSize,Uint8 nBase);
char *	util_u8toadec(Uint8 nVal,char * pBuf,char * pEnd);
char *	util_u8toahex(Uint8 nVal,char * pBuf,char * pEnd);

/*
 *  String to number conversions
 */

Int8	util_atoi8(char * pStr,char ** pEnd,Uint8 nBase);
Int16	util_atoi16(char * pStr,char ** pEnd,Uint8 nBase);
Int32	util_atoi32(char * pStr,char ** pEnd,Uint8 nBase);
Uint8	util_atou8(char * pStr,char ** pEnd,Uint8 nBase);
Uint16	util_atou16(char * pStr,char ** pEnd,Uint8 nBase);
Uint32	util_atou32(char * pStr,char ** pEnd,Uint8 nBase);

Int16	util_atoi16dec(char const_D * pPtr,Uint16 len,char const_D **ppEnd);
Uint16	util_atou16dec(char const_D * pPtr,Uint16 len,char const_D **ppEnd);
Int32	util_atoi32dec(char const_D * pPtr,Uint16 len,char const_D **ppEnd);
Uint32	util_atou32dec(char const_D * pPtr,Uint16 len,char const_D **ppEnd);

Uint32	util_atoip(char const_D * pString,Uint16 strLen,char const_D ** ppEnd);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */
