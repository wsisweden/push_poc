/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SECTORW32.h
*
*	\ingroup	SECTORW32
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef SECTORW32_H_INCLUDED
#define SECTORW32_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

//#include "nvmem.h"
#include "memdrv.h"
#include "flash.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type used for initialization parameters to sectorw32.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	char const_P *		file;			/**< Memory image file name			*/
	flash_SectorList const_P *
						sectorList;		/**< Sector definition list			*/
	Uint8 const_P *		sectorSmallestSize;	/**< Size of smallest sector	*/
	Uint32				sectorCount;	/**< Number of sectors				*/
	Uint16				sectorSize;		/**< Alternative sector size		*/
	Uint8				noOpenCloseFile;/**< If 1, then file is not opened
											 and closed for each operation	*/
} sectorw32_Init;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void * sectorw32_initialize(sectorw32_Init const_P *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern memdrv_Interface		sectorw32_fnIf;

/***************************************************************//** \endcond */

#endif

