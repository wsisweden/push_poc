/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		miimif.h
*
*	\ingroup	MIIMIF
*
*	\brief		Public declarations of the MII Management bus Interface.
*
*	\details	This file contains all public types of the MIIMIF interface.
*
*	\note
*
*	\version	\$Rev: 3657 $ \n
*				\$Date: 2019-03-22 15:21:40 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MIIMIF	MIIMIF
*
*	\brief		MII Management bus (MDIO) Interface.
*
********************************************************************************
*
*	\details	The interface is used by PHY chip drivers to communicate with
*				Station Management Entity (STA) drivers. The STA acts as a bus
*				master for the MDIO bus and this interface provides a common way
*				for controlling any STA driver.
*
*				The AMIF interface should be preferred over this interface for
*				new implementations.
*
*******************************************************************************/

#ifndef MIIMIF_H_INCLUDED
#define MIIMIF_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "MDIO.h"

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * \brief		PHY read register function type.
 *
 * \param		phyAddr		The PHY chip management bus address.
 * \param		regNum		The register number.
 *
 * \return		The register contents.
 *
 * \details		The STA driver should implement a function of this type. The
 *				function reads one register from an external PHY chip. The
 *				function blocks until the data is read.
 */

typedef Uint16 miimif_ReadFn(Uint8 phyAddr,Uint8 regNum);

/**
 * \brief		PHY write register function type.
 *
 * \param		phyAddr		The PHY chip MDIO bus address.
 * \param		regNum		The register number. The standard registers declared
 *							in MDIO.H may be used. PHY drivers might also define
 *							additional non-standard registers.
 * \param		data		The data to be written.
 *
 * \details		The STA driver should implement a function of this type. The
 *				function writes data to one PHY register. The function should
 *				block until the data has been written.
 */

typedef void miimif_WriteFn(Uint8 phyAddr,Uint8 regNum,Uint16 data);

/**
 *	STA driver interface type. STA drivers should define a public constant of
 *	this type, if they implement the MIIMIF interface. A pointer to the constant
 *	is supplied as an initialization parameter to every FB using the STA driver.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	miimif_ReadFn * 		pReadFn;	/**< Pointer to read function.		*/
	miimif_WriteFn *		pWriteFn;	/**< Pointer to write function.		*/
} miimif_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
