/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	EXE
*
*	\brief		Undefine all EXE macros.
*
*	\details	This file should only be included in T-Plat.E header files.
*				it is used to undefine all EXE_... macros.
*	
*	\note		
*
*	\version
*
*******************************************************************************/

/* Extern register declarations */
#undef EXE_REG_EX
#undef EXE_REG_EXPORT
#undef EXE_REG_IMPORT

/* Indication macros */
#undef EXE_IND_ME
#undef EXE_IND_LOC
#undef EXE_IND_EX
#undef EXE_IND_EXT

/* Register macros */
#undef EXE_REG_NS
#undef EXE_REG_NN
#undef EXE_BUF_NN
#undef EXE_REG_NT
#undef EXE_BUF_NT
#undef EXE_REG_VN
#undef EXE_BUF_VN
#undef EXE_REG_VT
#undef EXE_BUF_VT
#undef EXE_REG_VS
#undef EXE_REG_PN
#undef EXE_REG_PT
#undef EXE_REG_PS
#undef EXE_REG_BL

/* MMI macros */
#undef EXE_MMI_NONE
#undef EXE_MMI_INT
#undef EXE_MMI_REAL
#undef EXE_MMI_MASK
#undef EXE_MMI_STR
#undef EXE_MMI_ENUM
#undef EXE_MMI_DATE
#undef EXE_MMI_TIME

/* Register help text macros */
#undef EXE_HELP_EN

/* Text macros */
#undef EXE_TXT

/* ERR macros */
#undef EXE_ERR_CLASS
#undef EXE_ERR_TYPE
