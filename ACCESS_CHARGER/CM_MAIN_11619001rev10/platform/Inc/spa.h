/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPA.H
*
*	\ingroup	SPA
*
*	\brief		Public declarations of the SPA module
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef SPA_H_INCLUDED
#define SPA_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define SPA_EXT_CATEGORIES	5

/*	
 *  SPA register flags
 */
#define SPA_F_READ			(1<<0)			/**< Read allwed                */
#define SPA_F_WRITE			(1<<1)			/**< Write allowed              */

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef Uint8				spa_Flags;

typedef struct {							/*''''''''''''''''''''''''''''''*/
	Uint16					nChannel;		/**< SPA channel                */
	Uint32					nData;			/**< SPA data                   */
	sys_RegHandle			nHandle;		/**< Register handle            */
	sys_ArrIndex			nArrIndex;		/**< Register array index       */
	spa_Flags				nFlags;			/**< SPA flags                  */
} spa_CategoryPOD;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {							/*''''''''''''''''''''''''''''''*/
	const_P spa_CategoryPOD *	maps;			/**< Ptr to array of SPA
											mappings                        */
	const_P Uint16				size;			/**< Size of the array      */
} spa_CategoryInfo;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {							/*''''''''''''''''''''''''''''''*/
	spa_CategoryInfo		categories[SPA_EXT_CATEGORIES];	/**<            */
} spa_POD;									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/* todo: types */
typedef OSA_TASK void (*	spa_TaskFn)(void);
typedef void * (*			spa_InitFn)(void const_P *);
typedef Uint8 (*			spa_ParseFn)(void *, void *);
typedef Uint8 (*			spa_ReadFn)(void *, void *, void *);
typedef Uint8 (*			spa_WriteFn)(void *, void *, void *);
typedef Uint8 (*			spa_ClockFn)(void *, void *, void *);
typedef Uint8 (*			spa_FtrFn)(void *, void *, void *);
typedef Uint8 (*			spa_EventFn)(void *, void *, void *);
typedef Uint8 (*			spa_AlarmFn)(void *, void *, void *);

typedef struct {							/*''''''''''''''''''''''''''''''*/
	spa_InitFn				fnInit;			/**< Init function              */
	spa_TaskFn				fnTask;			/**< Task function              */
	spa_ParseFn				fnParse;		/**< Parser function            */
	spa_ReadFn				fnRead;			/**< Read function              */
	spa_WriteFn				fnWrite;		/**< Write function             */
	spa_ClockFn				fnClock;		/**< Clock handler fuction      */
	spa_FtrFn				fnFtr;			/**< FTR handler function       */
	spa_EventFn				fnEvent;		/**< Event read function        */
	spa_AlarmFn				fnAlarm;		/**< Alarm read function        */
} spa_Interface;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/****************************************************************************/
/* SPA POD generation structures											*/
/****************************************************************************/

typedef struct								/*''''''''''''''''''''''''''''''*/
{											/* SPA POD entry structure		*/
	sys_RegHandle			nHandle;		/**< Register handle            */
	sys_ArrIndex			nArrIndex;		/**< Array index                */
	Uint8					nFlags;			/**< Access flags               */
} spa_PodEntry;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void *				spa_dummyInit(void const_P *);
extern Uint8				spa_dummyParse(void *, void *);
extern Uint8				spa_dummyRead(void *, void *, void *);
extern Uint8				spa_dummyWrite(void *, void *, void *);
extern Uint8				spa_dummyClock(void *, void *, void *);
extern Uint8				spa_dummyFtr(void *, void *, void *);
extern Uint8				spa_dummyEvent(void *, void *, void *);
extern Uint8				spa_dummyAlarm(void *, void *, void *);


extern OSA_TASK void		spa_taskSingleMsg(void);
extern Uint8				spa__basicRead(void *, void *, void *);
extern Uint8				spa__basicWrite(void *, void *, void *);
extern Uint8				spa__parseBasicFrame(void *, void*);

extern Uint8				spa__podBasicRead(void *, void *, void *);
extern Uint8				spa__podBasicWrite(void *, void *, void *);
extern Uint8				spa__podParseBasicFrame(void *, void*);


/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif /* SPA_H_INCLUDED */
