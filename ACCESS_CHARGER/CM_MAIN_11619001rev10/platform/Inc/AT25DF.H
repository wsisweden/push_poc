/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		AT25DF.H
*
*	\ingroup	AT25DF
*
*	\brief		Public declarations for the AT25DFxxx flash memory driver.
*
*	\note		
*
*	\version	\$Rev: 4614 $ \n
*				\$UtcDate: $ \n
*				\$Author: tlmari $
*
*******************************************************************************/

#ifndef AT25DF_H_INCLUDED
#define AT25DF_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "protif.h"
#include "memdrv.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif 

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type of function called to control write protection pin.
 */

typedef void at25df_SetWpStateFn(Boolean);

/**
 *	Type for driver initialization values.
 */

typedef struct at25df__init {			/*''''''''''''''''''''''''''' CONST */
	void const_P *			pTargetInfo;/**< Pointer to chip select pin
										 * information. 					*/
	protif_Interface const_P *pProtIf;	/**< Ptr to Protocol handler interface*/
	void **					ppHandler;	/**< Inst ptr of the protocol handler*/
	at25df_SetWpStateFn *	pWpFn;		/**< Ptr to write protection func.	*/
	memdrv_MemInfo *		pMemInfo;	/**< Pointer to place for memory info.*/
} at25df_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Type used for Windows flash memory emulation initialization values.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	char const_P *		file;			/**< Memory image file name			*/
	Uint32				sectorCount;	/**< Number of sectors				*/
	Uint16				sectorSize;		/**< Alternative sector size		*/
} at25df_EmuInit;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void * at25df_init(at25df_Init const_P *);

/* Emulation functions for windows */
void * at25df_emuInit(at25df_EmuInit const_P *);
Uint8 at25df_emulate(void *,protif_Request *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern memdrv_Interface const_P at25df_driverIf;

/***************************************************************//** \endcond */

#ifdef __cplusplus
}
#endif
#endif
