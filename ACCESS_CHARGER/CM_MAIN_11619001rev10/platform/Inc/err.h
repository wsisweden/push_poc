/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	Name:	err.h
*
*--------------------------------------------------------------------------*//**
*
*	\file
*
*	\ingroup	ERR
*
*	\brief		Public declarations of the ERR FB.
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef ERR_H_INCLUDED
#define ERR_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/
#include "TSTAMP.H"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	\name		err_getError() status flags
 *
 *	\details	The following flags are used in the return value of 
 *				err_getError().
 */
 
/*@{*/									/*''''''''''''''''''''''''''''''''''*/
#define ERR_GET_OK			0x01		/**< Unread error successfully read */
#define ERR_OVERFLOW		0x02		/**< Overflow occurred prior the read*/
/*@}*/									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/**
*	
*	\brief		Report error to ERR FB.
*	
*	\param		fbId_		Identification of the FB instance in which the error
*							occurred, or SYS_BAD_FB_INST_ID if none available.
*	\param		localErrId_	Unique invocation id, e.g. enumerated error code,
*							private to the application.
*	\param		errType_	Name of error type defined by EXE_ERR_TYPE in the
*							INIT.H file of the FB.
*
*	\details	The err_error() macro is used to report an error to the ERR FB,
*				which stores the report for another application to read.
*/

#define err_error(fbId_,localErrId_,errType_) {				\
	extern sys_ErrorType const_P exe__et_##errType_;		\
	static const_P err_DescrType err__description = {		\
		(err_CodeType) (localErrId_),						\
		&exe__et_##errType_									\
	};														\
	err__putError(fbId_, &err__description, 0);				\
}

/**
*	
*	\brief		Report error with parameter to ERR FB.
*	
*	\param		fbId_		Identification of the FB instance in which the error
*							occurred, or SYS_BAD_FB_INST_ID if none available.
*	\param		localErrId_	Unique invocation id, e.g. enumerated error code,
*							private to the application.
*	\param		errType_	Name of error type defined by EXE_ERR_TYPE in the
*							INIT.H file of the FB.
*	\param		param_		Additional parameter stored with the reported error.
*
*	\details	The err_error() macro is used to report an error to the ERR FB,
*				which stores the report for another application to read.
*/

#define err_errorEx(fbId_,localErrId_,errType_,param_) {	\
	extern sys_ErrorType const_P exe__et_##errType_;		\
	static const_P err_DescrType err__description = {		\
		(err_CodeType) (localErrId_),						\
		&exe__et_##errType_									\
	};														\
	err__putError(fbId_, &err__description, param_);		\
}


/**
*	
*	\brief		Compare error classes.
*
*	\param		result_		Name of the Boolean variable, where the result of
*							the comparison is stored into.
*	\param		cbInfo_		Ptr to the error type info i.e. the 2nd argument
*							given to the callback function.
*	\param		classId_	The class ID to be compared.
*
*	\details	The err_class() macro compares the given class (classId_)
*				against classes of the specified error type information
*				(cbInfo_) passed to a call-back function. As a result, TRUE is 
*				returned in variable (result_) to indicate that the specified
*				error type belongs to the given error class.
*
*	\note		The 1st argument (result_) is the name of the result variable,
*				not its address.
*/

#define err_class(result_,cbInfo_,classId_) {				\
	extern Uint8 const_P exe_c_##classId_;					\
	result_ = exe__errClassMatch(cbInfo_,exe_c_##classId_);	\
}

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	err_CodeType is provided to the applications to hold an identification
 *	of an error. See err_error(). The preferred us8e is to define an enumerated
 *	list of error identification codes and to supply one of the codes to
 *	each invocation of err_error().
 */

typedef Uint8				err_CodeType;

/**
 * The err_DescrType is used in the err_QueueElem structure. It contains a 
 * description of the error. The nCode contains a FB specific enum value which
 * indicates exactly the occurred error. The typePtr points to a sys_ErrorType
 * struct but also to a exe_ErrorType struct. 
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	err_CodeType			nCode;		/**< Enumerated error code          */
	sys_ErrorType const_P *	typePtr;	/**< Error type                     */
} err_DescrType;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Contains all the information of one error. ERR FB contains a queue with
 * elements of this type.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	sys_FBInstId			instID;		/**< Instance / SYS_BAD_FB_INST_ID  */
	tstamp_Stamp			putTime;	/**< Lowest bits of the time stamp.	*/
	Uint16					param;		/**< Parameter.						*/
	err_DescrType const_P *	pDescr;		/**< Ptr to description of the error,
										 *	NULL if no error available yet	*/
} err_QueueElem;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Callback node. ERR stores a list of callback nodes. The function fn will
 * be called for each reported error. When supplying a callback node to the 
 * err_callBack() function only the fn field has to be initialized, the two
 * other fields are only used internally by ERR.
 */

typedef struct err__callBackNode {		/*''''''''''''''''''''''''''''' RAM	*/
	void (*					fn)(struct err__callBackNode *,void const_P *);
	struct err__callBackNode *next;		/**< Next call-back node / NULL     */
	Uint8					errIndex;	/**< Index to the requested error in
										 *	the queue, 0=oldest available	*/
	Uint8					errCount;	/**< Number of queued errors.		*/
} err_CallBackNode;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void err_init(Uint8);
void err_callBack(err_CallBackNode *);
void err__putError(sys_FBInstId,err_DescrType const_P *,Uint16);
Uint8 err_getError(err_CallBackNode *,err_QueueElem *);

/* EXE.H */

Boolean exe__errClassMatch(void const_P *,Uint8);
Uint16 exe__errIndex(sys_ErrorType const_P *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
