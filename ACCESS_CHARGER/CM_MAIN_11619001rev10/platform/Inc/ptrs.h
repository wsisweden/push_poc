/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	PTRS
*
*	\brief		Target dependent pointer types for a specific purpose within
*				the platform.
*
*	\details	This header includes a target dependent header that contains
*				types declared for each target. Some targets have multiple
*				memory sections that behaves differently. A pointer pointing
*				to such a section may have to be declared in a target specific
*				way.
*				
*				The target specific type definitions are gathered to this header
*				to avoid scattering compiler specific keywords across platform
*				code.
*				
*	\note
*
*	\version	\$Rev: 4504 $ \n
*				\$Date: 2021-05-24 12:20:58 +0300 (ma, 24 touko 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	PTRS	PTRS
*
*	\brief		Platform pointer types with target specific keywords.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

#ifndef PTRS_H_INCLUDED
#define PTRS_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "_ptrs.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/*
 *	Types for pointers pointing to REG register data.
 *	
 *	PTRS__REGQUAL Should be defined in the target specific header if register
 *	data pointers require a target specific type qualifier.
 *	
 *	PTRS__REGTYPEDEF should be defined in the target specific header if the
 *	default register data typedefs has to be overridden for a target. This is
 *	only necessary if a type qualifier is not enough. The pointer types in this
 *	header may still be used.
 *	
 *	PTRS__REGPTRDEF should be defined in the target specific header if the
 *	default register data pointer types has to be overridden for a target.
 *	In that case all the typedefs in this header for register data must be
 *	defined in the target specific header.
 */

#ifndef PTRS__REGQUAL
# define PTRS__REGQUAL
#endif

#ifndef PTRS__REGTYPEDEF

typedef void PTRS__REGQUAL		ptrs__RegData;

typedef Uint8 PTRS__REGQUAL		ptrs__RegData_u8;
typedef Uint16 PTRS__REGQUAL	ptrs__RegData_u16;
typedef Uint32 PTRS__REGQUAL	ptrs__RegData_u32;
typedef Uint64 PTRS__REGQUAL	ptrs__RegData_u64;

typedef Int8 PTRS__REGQUAL		ptrs__RegData_i8;
typedef Int16 PTRS__REGQUAL		ptrs__RegData_i16;
typedef Int32 PTRS__REGQUAL		ptrs__RegData_i32;
typedef Int64 PTRS__REGQUAL		ptrs__RegData_i64;

typedef Float PTRS__REGQUAL		ptrs__RegData_f;
typedef Double PTRS__REGQUAL	ptrs__RegData_d;

typedef BYTE PTRS__REGQUAL		ptrs__RegData_b;

# ifndef PTRS__REGPTRDEF
/*
 *	The type name postfix describes the pointer type. Type postfixes are added
 *	in the same order as they appear in the type definition.
 *	
 *	cD	= const_D
 *	u8	= Uint8
 *	u16 = Uint16
 *	u32 = Uint32
 *	i8  = Int8
 *	i16 = Int16
 *	i32 = Int32
 *	p	= pointer (*)
 */

typedef ptrs__RegData *			ptrs_RegData_p;
typedef ptrs__RegData const_D *	ptrs_RegData_cDp;

typedef ptrs__RegData_u8 *		ptrs_RegData_u8p;
typedef ptrs__RegData_u8 const_D * ptrs_RegData_u8cDp;

typedef ptrs__RegData_u16 *		ptrs_RegData_u16p;
typedef ptrs__RegData_u16 const_D * ptrs_RegData_u16cDp;

typedef ptrs__RegData_u32 *		ptrs_RegData_u32p;
typedef ptrs__RegData_u32 const_D * ptrs_RegData_u32cDp;

typedef ptrs__RegData_u64 *		ptrs_RegData_u64p;
typedef ptrs__RegData_u64 const_D * ptrs_RegData_u64cDp;

typedef ptrs__RegData_i8 *		ptrs_RegData_i8p;
typedef ptrs__RegData_i8 const_D * ptrs_RegData_i8cDp;

typedef ptrs__RegData_i16 *		ptrs_RegData_i16p;
typedef ptrs__RegData_i16 const_D * ptrs_RegData_i16cDp;

typedef ptrs__RegData_i32 *		ptrs_RegData_i32p;
typedef ptrs__RegData_i32 const_D * ptrs_RegData_i32cDp;

typedef ptrs__RegData_i64 *		ptrs_RegData_i64p;
typedef ptrs__RegData_i64 const_D * ptrs_RegData_i64cDp;

typedef ptrs__RegData_f *		ptrs_RegData_fp;
typedef ptrs__RegData_f const_D * ptrs_RegData_fcDp;

typedef ptrs__RegData_d *		ptrs_RegData_dp;
typedef ptrs__RegData_d const_D * ptrs_RegData_dcDp;

typedef ptrs__RegData_b *		ptrs_RegData_bp;
typedef ptrs__RegData_b const_D * ptrs_RegData_bcDp;

# endif
#endif

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/******************************************************//** \endcond pub_decl */

#endif
