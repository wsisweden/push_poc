/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DUMMY.H
*
*	\ingroup	DUMMY
*
*	\brief		Dummy functions
*
*	\details		
*
*	\note		
*
*	\version	31.01.2005 / Jukka Rankaviita / Orginal version.
*
*******************************************************************************/

#ifndef DUMMY_H_INCLUDED
#define DUMMY_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PUBLIC void *				dummy_init(sys_FBInstId, void const_P *);
PUBLIC void					dummy_reset(void *, void const_P *);
PUBLIC void					dummy_down(void *, sys_DownType);
PUBLIC sys_IndStatus		dummy_read(void *,sys_IndIndex,sys_ArrIndex,void *);
PUBLIC sys_IndStatus		dummy_write(void *, sys_IndIndex, sys_ArrIndex);
PUBLIC void					dummy_ctrl(void *, sys_CtrlType *);
PUBLIC Uint16				dummy_test(void *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif /*DUMMY_H_INCLUDED*/
