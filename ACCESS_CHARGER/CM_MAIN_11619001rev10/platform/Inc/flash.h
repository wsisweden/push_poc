/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2010, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flash.h
*
*	\ingroup	FLASH
*
*	\brief		Declares types that can be used to describe the sectors of a
*				flash memory.
*
*	\details	The types that are declared here are used in several function
*				blocks.
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef FLASH_H_INCLUDED
#define FLASH_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Sector-definition for flash-sectors.
 */
typedef struct {						/*''''''''''''''''''''''''''' CONST */
	Uint8 			sectorSizeFactor;	/**< Sector size def by as 2^ exponent.
										 * e.g. 12 equals 4096 bytes.		*/	
	Uint16			sectorOffset;		/**< Sector offset divided by  	
										 * smallest sector size. Offset 
										 * value is kilobytes.				*/
} flash_SectorList;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * This type is used for the variable that holds the sector count. 
 */
typedef Uint8				flash_SectorIndexSizeType;



/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/


/***************************************************************//** \endcond */

#endif

