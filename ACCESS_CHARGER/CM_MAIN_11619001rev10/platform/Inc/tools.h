/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		TOOLS.H
*
*	\brief		Common tools and target selection.
*
*	\details
*
*	\note
*
*	\version	08-04-04 / Juha Ylinen
*
*	\version	09-08-04 / Juha Ylinen\n
*				Added target: TARGET_AVR ( IAR AVR -compiler )
*
*	\version	22-10-04 / Sami Niemi\n
*				Added target: TARGET_M16C ( IAR AVR -compiler )
*				Added define C_task for TARGET_W32
*
*	\version	25-01-05 / Jukka Rankaviita\n
*				Added target dependent macro DUMMY_VAR
*
*	\version	01-02-05 / Jukka Rankaviita\n
*				Added tools for double linked list (link2_Node)
*
*	\version	24-03-05 / TJK\n
*				Should use 'dim', not 'DIM'.
*
*	\version	06-04-05 / Juha Ylinen\n
*				New macro: atomic_(expr_) . This saves and restores the
*				I-flag status. Atomic macro now operates like it should,
*				it does not save/restore I-flag
*
*	\version	18-04-05 / Jukka Rankaviita\n
*				New macros for _mem_P and _str_P functions.
*
*	\version	25-04-05 / Jukka Rankaviita\n
*				TARGET_M16C selection changed for new compiler.
*				intrinsics.h icluded instead of intrm16c.h
*
*	\version	31-03-07 / TJK\n
*				Added TARGET_MEM_ALIGN. Fixes and cleanup.
*
*	\version	02-08-07 / Tommi Nakkila\n
*				Added macros SYS_DEFINE_FILE_NAME and SYS_FILE_NAME.
*				Removed definition for macro atomic if target is not
*				AVR or M16C.
*
*	\version	17-08-07 / Tommi Nakkila, TJK\n
*				Added macro TOOLS_MESSAGE_PRAGMA (was TOOLS_NO_PRAGMAMSG)
*				for target ARM7. This can be used to detect that
*				the compiler does not support message pragmas.
*				Fixed the include path of FreeRTOS port header file.
*
*	\version	18-06-08 / Ari Suomi\n
*				Moved all str..._P, str..._G, mem..._P and mem..._G macros
*				to LIB.H.
*
*	\version	10-07-08 / Ari Suomi\n
*				Changed the tools_structPtr() macro to remove a compiler
* 				warning.
*
* 	\version	28-10-08 / Ari Suomi\n
* 				Added atomicintr() macro.
*
*	\version	26-11-09 / Tommi Nakkila\n
*				/ Added Cortex-M3 target. Added data type range defines.
*
*	\version	30-09-10 / Tommi Nakkila
*				Changed CM3 isr enabling / disabling macros.
*
*	\version	\$Rev: 4653 $ \n
*				\$Date: 2021-09-10 12:26:57 +0300 (pe, 10 syys 2021) $ \n
*				\$Author: tlmari $
*
*******************************************************************************/

#ifndef	TOOLS_H_INCLUDED
#define	TOOLS_H_INCLUDED

/*******************************************************************************
;
;	H E A D E R    F I L E S
;
*******************************************************************************/

#include <stddef.h>

/**
 *	\name	Masks for targets based Platform.
 */

/*@{*/									/*''''''''''''''''''''''''''''''''''*/
#define	TARGET_AVR				0x8000	/**< Microchip Atmel ATmega (AVR)	*/
#define	TARGET_M16C				0x4000	/**< Renesas (Mitsubishi)           */
#define TARGET_W32				0x2000	/**< 32-bit Windows / NT            */
#define	TARGET_PC				0x1000	/**< MS-DOS / 16-bit Windows        */
#define	TARGET_ARM7				0x0800	/**< NXP LPC23xx (Arm7)       		*/
#define	TARGET_UNIX				0x0400	/**< Linux / Unix                   */
#define	TARGET_CM3				0x0200	/**< NXP LPC17xx (Cortex-M3)		*/
#define	TARGET_CM0				0x0100	/**< NXP LPC11xx (Cortex-M0)		*/
#define	TARGET_CM4			   0x10000	/**< NXP LPC40xx (Cortex-M4)		*/
#define	TARGET_MM7			   0x20000	/**< MC Atmel SAME7 (Cortex-M7)		*/
#define TARGET_SM4			   0x40000	/**< ST STM32F3xx (Cortex-M4)		*/
#define TARGET_S4F4			   0x80000	/**< ST STM32F4xx (Cortex-M4)		*/
#define TARGET_S3F2			   0x100000	/**< ST STM32F2xx (Cortex-M3)		*/
/*@}*/									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	\name	Masks for targets based Compiler.
 */

/*@{*/									/*''''''''''''''''''''''''''''''''''*/
#define	TARGET_GNU				0x0001	/**< GNU                            */
#define	TARGET_ICC				0x0002	/**< IMAGECRAFT                     */
#define	TARGET_IAR				0x0004	/**< IAR SYSTEMS                    */
#define TARGET_DOXY				0x0008	/**< Doxygen						*/
/*@}*/									/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	\name 	Targets based the Compiler and Platform together.
 */

/*@{*/
/** Atmel AVR 8-bit with GCC compiler. */
#define	TARGET_AVRGNU			(TARGET_AVR | TARGET_GNU)
/** Atmel AVR 8-bit with ICC compiler. */
#define	TARGET_AVRICC			(TARGET_AVR | TARGET_ICC)
/** NXP LPC23xx series (ARM7) */
#define	TARGET_ARM7GNU			(TARGET_ARM7 | TARGET_GNU)
/** NXP LPC17xx/18xx series (Cortex-M3) */
#define	TARGET_CM3GNU			(TARGET_CM3 | TARGET_GNU)
/** NXP LPC11xx series (Cortex-M0+) */
#define	TARGET_CM0GNU			(TARGET_CM0 | TARGET_GNU)
/** NXP LPC40xx series (Cortex-M4) */
#define	TARGET_CM4GNU			(TARGET_CM4 | TARGET_GNU)
/** Microchip Cortex-M7 */
#define	TARGET_MM7GNU			(TARGET_MM7 | TARGET_GNU)
/** ST STM32 Cortex-M4 with IAR compiler */
#define TARGET_SM4IAR			(TARGET_SM4 | TARGET_IAR)
/** NXP LPC17xx/18xx series (Cortex-M3) */
#define TARGET_CM3IAR			(TARGET_CM3 | TARGET_IAR)
/** ST STM32 Cortex-M4 with GCC compiler */
#define TARGET_SM4GNU			(TARGET_SM4 | TARGET_GNU)
/** W32 with MinGW GCC/G++ GCC compiler */
#define TARGET_W32GNU			(TARGET_W32 | TARGET_GNU)
/** ST STM32F4 Cortex-M4 with GCC compiler */
#define TARGET_S4F4GNU			(TARGET_S4F4 | TARGET_GNU)
/** ST STM32F4xx Cortex-M4 with IAR compiler */
#define TARGET_S4F4IAR			(TARGET_S4F4 | TARGET_IAR)
/** ST STM32F2 Cortex-M3 with GCC compiler */
#define TARGET_S3F2GNU			(TARGET_S3F2 | TARGET_GNU)
/*@}*/

/*******************************************************************************
;
;	TARGET_SELECTED from compiler predefinions
;
*******************************************************************************/

/*************** GCC compiler *************************/
#if	defined		__GNUC__

# if defined		AVR
#  define	TARGET_SELECTED				TARGET_AVRGNU
# elif defined	__linux
#  define	TARGET_SELECTED				TARGET_UNIX
# elif defined	__cortex_m3__ 	/* Defined in the MAKEFILE! 	*/
#  define	TARGET_SELECTED				TARGET_CM3GNU
# elif defined	__cortex_m0__ 	/* Defined in the MAKEFILE! 	*/
#  define	TARGET_SELECTED				TARGET_CM0GNU
# elif defined	__cortex_m4__ 	/* Defined in the MAKEFILE! 	*/
#  define	TARGET_SELECTED				TARGET_CM4GNU
# elif defined __st_cortex_m4__	/* Defined in the MAKEFILE! 	*/
#  define	TARGET_SELECTED				TARGET_SM4GNU
# elif defined __st_cortex_m4_f4__ /* Defined in the MAKEFILE! 	*/
#  define	TARGET_SELECTED				TARGET_S4F4GNU
# elif defined __st_cortex_m3_f2__ /* Defined in the MAKEFILE! 	*/
#  define	TARGET_SELECTED				TARGET_S3F2GNU
# elif defined	__mc_cortex_m7__ /* Defined in the MAKEFILE! 	*/
#  define	TARGET_SELECTED				TARGET_MM7GNU
# elif defined	__arm__ 		/* Defined by GNU ARM compiler 	*/
#  define	TARGET_SELECTED				TARGET_ARM7GNU
# elif defined __MINGW32__		/* Defined by MinGW compiler	*/
#  define	TARGET_SELECTED				TARGET_W32GNU
# endif

/*************** Imagecraft compiler ******************/
#elif defined	__IMAGECRAFT__

# define	TARGET_SELECTED				TARGET_AVRICC

/*************** IAR compiler *************************/
#elif defined __IAR_SYSTEMS_ICC__

# if  defined __ICCAVR__
#  define	TARGET_SELECTED				TARGET_AVR
# elif defined __ICCM16C__
#  define	TARGET_SELECTED				TARGET_M16C
# elif defined __ICCARM__
#  if defined __st_cortex_m4__		/* Defined in the MAKEFILE! 	*/
#   define	TARGET_SELECTED				TARGET_SM4IAR
#  elif defined __st_cortex_m4_f4__	/* Defined in the MAKEFILE! 	*/
#   define	TARGET_SELECTED				TARGET_S4F4IAR
#  elif defined __cortex_m3__
#	define  TARGET_SELECTED				TARGET_CM3IAR
#  endif
# endif

/*************** Windows ******************************/
#elif defined	_WIN32

# define TARGET_SELECTED				TARGET_W32

/*************** Microsoft compiler *******************/
#elif defined	_MSC_VER
# if _MSC_VER < 900
#  define TARGET_SELECTED				TARGET_PC
# else
#  define TARGET_SELECTED				TARGET_W32
# endif

/*************** Doxygen preprocessor *****************/
#elif defined	__DOXYGEN__
#  define TARGET_SELECTED				TARGET_DOXY
#endif

#ifndef TARGET_SELECTED
# error	"The compiler used cannot be recognized, check that the tl-podco.ini has correct target defines"
#endif

/*******************************************************************************
;
;	Compiler independent macros
;
*******************************************************************************/

#define	PRIVATE				static

#if TARGET_SELECTED == TARGET_W32
#define PUBLIC
//#define PUBLIC	__declspec(dllexport)
#else
#define PUBLIC
#endif

#define FOREVER				for(;;)		/**< Infinite loop					*/
#define until(e_)			while(!(e_))

#if	TARGET_SELECTED != TARGET_W32GNU
#define	not(e_)				(!(e_))		/**< Reverses boolean 				*/
#endif

/**
 * Returns the larger value of the two supplied values.
 */
#define MAX(a_,b_)			((a_)>(b_)?(a_):(b_))

/**
 * Returns the smaller value of the two supplied values.
 */
#define	MIN(a_,b_)			((a_)<(b_)?(a_):(b_))

/**
 * Returns the absolute value of the supplied value.
 */
#define ABS(v_)				(((v_) >= 0) ? (v_) : -(v_))
#define DIM(a_)				(sizeof(a_)/sizeof(*(a_))) /**< TJK: do not use!*/

/**
 * Returns the size of an array.
 */
#define dim(a_)				(sizeof(a_)/sizeof(*(a_)))


/* TJK: do not use LBIT, SETBIT and CLSBIT with masks of >16 bits! */

#define LBIT(s_)			(1U << (s_))
#define SETBIT(m_,b_)		((m_) |= LBIT(b_))
#define CLSBIT(m_,b_)		((m_) &= ~LBIT(b_))

/**
 * This keeps the compiler happy when function call parameters are not used.
 */
#define DUMMY_VAR(s_)		((s_) = (s_))

/**
 *	\brief 	Converts address of structure element to address of the structure.
 *
 *	\param	type_		Type of the structure.
 *	\param	membName_	Name of the structure member.
 *	\param	pMember_	Pointer to member membName_.
 *
 * 	\return	Pointer to the structure of type type_.
 */

#define tools_structPtr(type_,membName_,pMember_) (							\
	(type_ *)(																\
		(void *)(															\
			(BYTE *)(pMember_) - offsetof(type_, membName_) / sizeof(BYTE)	\
		)																	\
	)																		\
)

/*
 *	Double Linked List tools.
 */
#define link2_next(pp)			((*(pp)) = (*(pp))->next)
#define link2_prev(pp)			((*(pp)) = (*(pp))->prev)
#define link2_init(p)			((p)->next = (p)->prev = (p))
#define link2_delete(p)					\
	((p)->prev)->next = (p)->next;		\
	((p)->next)->prev = (p)->prev;
#define link2_insert(p,pn)				\
	(pn)->prev = (p);					\
	(pn)->next = (p)->next;		   		\
	((p)->next)->prev = (pn);			\
	(p)->next = (pn);

#define link2_getNext(p)		((p)->next)
#define link2_getPrev(p)		((p)->prev)
#define link2_parent(p,type,name)  	\
	((type *) ((BYTE *) (p) - offsetof(type, name)))

#ifndef NULL
# define NULL				0
#endif
#ifndef FALSE
# define FALSE				0
#endif
#ifndef TRUE
# define TRUE				1
#endif

/*
 *  DEBUG macros
 */

#ifndef NDEBUG

/**
 *  File name macro, creates a local string variable that contains file name.
 *	Must be used before calling deb_assert.
 */

# define SYS_DEFINE_FILE_NAME	PRIVATE char const_P sys__fileName[] = __FILE__;
# define SYS_FILE_NAME			sys__fileName

#else

# define SYS_DEFINE_FILE_NAME
# define SYS_FILE_NAME			NULL

#endif

/*******************************************************************************
;
;	Target dependent macros and data types
;
*******************************************************************************/

#if	( TARGET_SELECTED & TARGET_AVR )

# define input_io(addr_)		(*(volatile BYTE *)(addr_))
# define output_io(addr_,byte_)	(*(volatile BYTE *)(addr_)=(byte_))

#elif ( TARGET_SELECTED & TARGET_M16C )

# define input_io(addr_)		(*(volatile WORD *)(addr_))
# define output_io(addr_,byte_)	(*(volatile WORD *)(addr_)=(byte_))

#else

# define input_io(addr_)		0
# define output_io(addr_,byte_)

#endif

/*
 *	Data type ranges
 */
#define Int8_MIN			-128
#define Int8_MAX			127
#define Int16_MIN			(-Int16_MAX-1)
#define Int16_MAX			32767
#define Int32_MIN			(-Int32_MAX-1)
#define Int32_MAX			2147483647
#define Int64_MIN			(-Int64_MAX-1)
#define Int64_MAX			9223372036854775807
#define Uint8_MIN			0
#define Uint8_MAX			255U
#define Uint16_MIN			0
#define Uint16_MAX			65535U
#define Uint32_MIN			0
#define Uint32_MAX			4294967295U
#define Uint64_MIN			0
#define Uint64_MAX			18446744073709551615U

/* pragma message supported by default */
#define TOOLS_MESSAGE_PRAGMA

/*------------------------------------------------------------------------------
;
;		T A R G E T   A V R I C C
;
;*----------------------------------------------------------------------------*/

#if	TARGET_SELECTED		== TARGET_AVRICC

 typedef unsigned char		BYTE;
 typedef unsigned short		WORD;
 typedef unsigned char		Uint8;
 typedef unsigned short		Uint16;
 typedef unsigned long 		Uint32;
 typedef signed char		Int8;
 typedef signed short		Int16;
 typedef signed long		Int32;
 typedef unsigned char		Boolean;
 typedef float				Float;
 typedef double				Double;

 typedef unsigned char		Ufast8;
 typedef unsigned short		Ufast16;
 typedef unsigned long		Ufast32;
 typedef signed char		Fast8;
 typedef signed short		Fast16;
 typedef signed long		Fast32;

# define const_P			const
# define const_D			const
# define DISABLE_ATOM		asm("in r0, 0x3F\npush r0\ncli")
# define ENABLE_ATOM		asm("pop r0\nout 0x3F, r0")
# define DISABLE			asm("cli")
# define ENABLE				asm("sei")
# define atomic(e_)			{ DISABLE; e_ ENABLE;	}

/* Depricated. Do not use in new projects. Use atomicintr() instead */
# define atomic_(expr_)		{												\
								Uint8 tools__IntFlags;						\
								tools__IntFlags = input_io(0x3F);			\
								DISABLE;									\
								{ expr_ }									\
								output_io(0x3F, tools__IntFlags);			\
							}

#define atomicintr(e_) 		atomic_(e_)


 /*------------------------------------------------------------------------------
;
;		T A R G E T   A V R G N U
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED	==	TARGET_AVRGNU

 typedef unsigned char		BYTE;
 typedef unsigned short		WORD;
 typedef unsigned char		Uint8;
 typedef unsigned short		Uint16;
 typedef unsigned long 		Uint32;
 typedef signed char		Int8;
 typedef signed short		Int16;
 typedef signed long		Int32;
 typedef unsigned char		Boolean;
 typedef float				Float;
 typedef double				Double;

 typedef unsigned char		Ufast8;
 typedef unsigned short		Ufast16;
 typedef unsigned long		Ufast32;
 typedef signed char		Fast8;
 typedef signed short		Fast16;
 typedef signed long		Fast32;

# define const_P			const		/**< const types goes to SRAM!      */
# define const_D			const
# define DISABLE			asm("cli" ::)
# define ENABLE				asm("sei" ::)
# define asm				__asm__ __volatile__


 /*------------------------------------------------------------------------------
;
;		T A R G E T   A V R		( I A R )
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED	==	TARGET_AVR

 typedef unsigned char		BYTE;
 typedef unsigned short		WORD;
 typedef unsigned char		Uint8;
 typedef unsigned short		Uint16;
 typedef unsigned long 		Uint32;
 typedef signed char		Int8;
 typedef signed short		Int16;
 typedef signed long		Int32;
 typedef unsigned char		Boolean;
 typedef float				Float;
 typedef double				Double;

 typedef unsigned char		Ufast8;
 typedef unsigned short		Ufast16;
 typedef unsigned long		Ufast32;
 typedef signed char		Fast8;
 typedef signed short		Fast16;
 typedef signed long		Fast32;

#define	const_P				__flash
#define	const_D				const
#define INLINE				inline

#define	DISABLE_ATOM												\
	{																\
		asm("push r0");												\
		asm("in r0, 0x3F");											\
		asm("push r0");												\
		asm("cli");													\
	}
#define	ENABLE_ATOM													\
	{																\
		asm("pop r0");												\
		asm("out 0x3F, r0");										\
		asm("pop r0");												\
	}

#define DISABLE				asm("cli")
#define ENABLE				asm("sei")
#define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }
/*#define atomic(e_)			e_*/

 /* Depricated. Do not use in new projects. Use atomicintr() instead */
#define atomic_(expr_)		{												\
								Uint8		tools__IntFlags;				\
								tools__IntFlags = input_io(0x5F);			\
								DISABLE;									\
								{ expr_ }									\
								output_io(0x5F, tools__IntFlags);			\
							}

#define atomicintr(e_) 		atomic_(e_)

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1


/*------------------------------------------------------------------------------
;
;		T A R G E T   M 1 6 C
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_M16C

 typedef unsigned char		BYTE;
 typedef unsigned short		WORD;
 typedef unsigned char		Uint8;
 typedef unsigned short		Uint16;
 typedef unsigned long 		Uint32;
 typedef signed char		Int8;
 typedef short				Int16;
 typedef long				Int32;
 typedef unsigned char		Boolean;
 typedef float				Float;
 typedef double				Double;

 typedef unsigned char		Ufast8; /* M16c supports 8-bit ops well. */
 typedef unsigned short		Ufast16;
 typedef unsigned long		Ufast32;
 typedef signed char		Fast8;	/* M16c supports 8-bit ops well. */
 typedef signed short		Fast16;
 typedef signed long		Fast32;

# define const_P			const /* __data20 */
# define const_D			const
#define INLINE				inline

# include "intrinsics.h"
# define DISABLE			__disable_interrupt()
# define ENABLE				__enable_interrupt()
# define DISABLE_ATOM		__disable_interrupt()
# define ENABLE_ATOM		__enable_interrupt()
# define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

 typedef __istate_t			tools_IsrState;

/** 
 *	\brief			Disables interrupts and returns variable containing current 
 *					interrupt enabled status.
 *	\param	p_		Pointer to variable of type ::InterruptState which will 
 *					receive the current interrupt status.
 */
# define DISABLEINTR(p_)				\
	*(p_) = __get_interrupt_state();	\
	__disable_interrupt()

/** 
 *	\brief			Restores interrupt enabled state to state they were in
 *					prior to ::DISABLEINTR call.
 *	\param	p_		Pointer to variable containing interrupt enabled state 
 *					previously read with ::DISABLEINTR;
 */
# define ENABLEINTR(p_)		__set_interrupt_state(*(p_))

/** 
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration and then restoring interrupt enabled state to what
 *				it was before the call.
 *	\param		e_	Operation(s) to perform atomically.
 */
# define atomicintr(e_)	{ 													\
	tools_IsrState tools__primask;											\
	DISABLEINTR(&tools__primask);											\
	{ e_ }																	\
	ENABLEINTR(&tools__primask);											\
}

# define TARGET_MEM_ALIGN	2

#define near_t				__data16
#define far_t				__data20

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1

/*------------------------------------------------------------------------------
;
;		T A R G E T   W 3 2
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_W32 || TARGET_SELECTED == TARGET_W32GNU

 typedef unsigned char		BYTE;
 typedef unsigned short		WORD;
 typedef unsigned char		Uint8;
 typedef unsigned short		Uint16;
 typedef unsigned long 		Uint32;
 typedef signed char		Int8;
 typedef signed short		Int16;
 typedef signed long		Int32;

#if TARGET_SELECTED == TARGET_W32
 typedef __int64			Int64;
 typedef unsigned __int64	Uint64;
#else
 typedef signed long long	Int64;
 typedef unsigned long long	Uint64;
#endif

 typedef unsigned char		Boolean;
 typedef float				Float;
 typedef double				Double;

 typedef unsigned long		tools_IsrState;

 typedef unsigned long		Ufast8;
 typedef unsigned long		Ufast16;
 typedef unsigned long		Ufast32;
 typedef long				Fast8;
 typedef long				Fast16;
 typedef long				Fast32;

#if TARGET_SELECTED == TARGET_W32
# define DISABLE			osa_interruptsDisable()
# define ENABLE				osa_interruptsEnable()
# define DISABLEINTR(p_)	*(p_) = osa_interruptsDisableIntr()
# define ENABLEINTR(p_)		osa_interruptsEnableIntr(p_)
#else
# define DISABLE	
# define ENABLE	
# define DISABLEINTR(p_)	 *(p_) = 0; DISABLE
# define ENABLEINTR(p_)		ENABLE
#endif

# define const_P			const
# define const_D			const
# define C_task
# define INLINE				__inline
# define atomic(e_)			{ DISABLE; e_ ENABLE;	}
#define atomicintr(e_)	{ 													\
	tools_IsrState tools__intState;											\
	DISABLEINTR(&tools__intState);											\
	{ e_ }																	\
	ENABLEINTR(&tools__intState);											\
}

# define TARGET_MEM_ALIGN	4

# define near_t
# define far_t

/*
 *	Stack growth direction. Doesn't matter on this target.
 */
# define TOOLS_STACK_GROWTH	-1

/* Visual studio 2005 does not define the NAN constant */
#if TARGET_SELECTED == TARGET_W32
# include "vs\nan.h"
#endif

/* MinGW does not support pragma message */
#if TARGET_SELECTED == TARGET_W32GNU
/* pragma message NOT supported */
# undef TOOLS_MESSAGE_PRAGMA
#endif

/*------------------------------------------------------------------------------
;
;		T A R G E T   P C
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_PC

 typedef unsigned char		BYTE;
 typedef unsigned short		WORD;
 typedef unsigned char		Uint8;
 typedef unsigned short		Uint16;
 typedef unsigned long 		Uint32;
 typedef signed char		Int8;
 typedef signed short		Int16;
 typedef signed long		Int32;
 typedef unsigned char		Boolean;
 typedef float				Float;
 typedef double				Double;

 typedef unsigned long		Ufast8;
 typedef unsigned long		Ufast16;
 typedef unsigned long		Ufast32;
 typedef long				Fast8;
 typedef long				Fast16;
 typedef long				Fast32;

# define DISABLE			osa_isrEnter()
# define ENABLE				osa_isrLeave()
# define const_P			const
# define const_D			const
# define C_task
# define INLINE				__inline

# define TARGET_MEM_ALIGN	2

/*
 *	Stack growth direction. Doesn't matter on this target.
 */
#define TOOLS_STACK_GROWTH	-1

/*------------------------------------------------------------------------------
;
;		T A R G E T   A R M / G N U
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_ARM7GNU

typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
typedef unsigned long 		Uint32;
typedef signed char			Int8;
typedef short				Int16;
typedef long				Int32;
typedef long long			Int64;
typedef unsigned long long	Uint64;
typedef unsigned char		Boolean;
typedef float				Float;
typedef double				Double;

typedef unsigned long		Ufast8;
typedef unsigned long		Ufast16;
typedef unsigned long		Ufast32;
typedef long				Fast8;
typedef long				Fast16;
typedef long				Fast32;

# define const_P			const
# define const_D			const
# define C_task
# define INLINE				__inline__

/*
 *  Include FreeRTOS arm7 port file here, it includes macros for disabling
 *	and enabling interrupts etc.
 */
#include "portmacro.h"

# define DISABLE			portDISABLE_INTERRUPTS()
# define ENABLE				portENABLE_INTERRUPTS()

# define DISABLE_ATOM		DISABLE
# define ENABLE_ATOM		ENABLE

# define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

#define atomicintr(e_) 							\
{												\
	Uint32 status;								\
	asm("MRS %0,cpsr" : "=r" (status));			\
	DISABLE_ATOM;								\
	e_											\
	asm("MSR cpsr_c,%0" : : "r" (status));		\
}

/*
 *  4 byte align required
 */
# define TARGET_MEM_ALIGN	4

#define near_t
#define far_t

/* pragma message NOT supported */
#undef TOOLS_MESSAGE_PRAGMA

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1

/*------------------------------------------------------------------------------
;
;		T A R G E T   C O R T E X M 3 / G N U / I A R
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_CM3GNU || TARGET_SELECTED == TARGET_CM3IAR

typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
#if TARGET_SELECTED == TARGET_CM3IAR
typedef unsigned int		Uint32; /* Declared to match the uint32_t type */
#else
typedef unsigned long		Uint32; /* Declared to match the uint32_t type */
#endif
typedef signed char			Int8;
typedef short				Int16;
typedef long				Int32;
typedef long long			Int64;
typedef unsigned long long	Uint64;
typedef unsigned char		Boolean;
typedef float				Float;
typedef double				Double;

typedef unsigned long		Ufast8;
typedef unsigned long		Ufast16;
typedef unsigned long		Ufast32;
typedef long				Fast8;
typedef long				Fast16;
typedef long				Fast32;

/** 
*	Interrupts enabled / disabled state variable type. To be used with 
*	::DISABLEINTR and ::ENABLEINTR.
 */
typedef unsigned long		tools_IsrState;

#define const_P				const
#define const_D				const
#define C_task
#if TARGET_SELECTED == TARGET_CM3IAR
#define INLINE				inline
#else
#define INLINE				__inline__
#endif

/*
 *  Include Cortex M3 core header.
 */
#include "hw.h"

/** 
 *	Disables all interrupts.
 */
#define DISABLE				hw_isrDisable()

/** 
 *	Enables all interrupts.
 */
#define ENABLE				hw_isrEnable()

#define DISABLE_ATOM		DISABLE
#define ENABLE_ATOM			ENABLE

/** 
 *	\brief			Disables interrupts and returns variable containing current 
 *					interrupt enabled status.
 *	\param	p_		Pointer to variable of type ::InterruptState which will 
 *					receive the current interrupt status.
 */
#define DISABLEINTR(p_)		*(p_) = hw_isrDisableIntr()

/** 
 *	\brief			Restores interrupt enabled state to state they were in
 *					prior to ::DISABLEINTR call.
 *	\param	p_		Pointer to variable containing interrupt enabled state 
 *					previously read with ::DISABLEINTR;
 */
#define ENABLEINTR(p_)		hw_isrEnableIntr(*(p_))

/**
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration. 
 *	\note		Interrupts are unconditionally enabled once the operation is 
 *				done.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

/** 
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration and then restoring interrupt enabled state to what
 *				it was before the call.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomicintr(e_)	{ 													\
	tools_IsrState tools__primask;											\
	DISABLEINTR(&tools__primask);											\
	{ e_ }																	\
	ENABLEINTR(&tools__primask);											\
}

/*
 *  4 byte align required
 */
# define TARGET_MEM_ALIGN	4

#define near_t
#define far_t

/* pragma message NOT supported */
#undef TOOLS_MESSAGE_PRAGMA

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1

/*------------------------------------------------------------------------------
;
;		T A R G E T   N X P   C O R T E X - M 0 / G N U
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_CM0GNU

typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
typedef unsigned long 		Uint32;
typedef signed char			Int8;
typedef short				Int16;
typedef long				Int32;
typedef long long			Int64;
typedef unsigned long long	Uint64;
typedef unsigned char		Boolean;
typedef float				Float;
typedef double				Double;

typedef unsigned long		Ufast8;
typedef unsigned long		Ufast16;
typedef unsigned long		Ufast32;
typedef long				Fast8;
typedef long				Fast16;
typedef long				Fast32;

/** 
*	Interrupts enabled / disabled state variable type. To be used with 
*	::DISABLEINTR and ::ENABLEINTR.
 */
typedef unsigned long		tools_IsrState;

#define const_P				const
#define const_D				const
#define C_task
//#define INLINE				__inline__

/*
 *  Include Cortex M0 core header.
 */
#include "hw.h"

/** 
 *	Disables all interrupts.
 */
#define DISABLE				__disable_irq()

/** 
 *	Enables all interrupts.
 */
#define ENABLE				__enable_irq()

#define DISABLE_ATOM		DISABLE
#define ENABLE_ATOM			ENABLE

/** 
 *	\brief			Disables interrupts and returns variable containing current 
 *					interrupt enabled status.
 *	\param	p_		Pointer to variable of type ::InterruptState which will 
 *					receive the current interrupt status.
 */
#define DISABLEINTR(p_)		*(p_) = hw__isrDisableIntr()

/** 
 *	\brief			Restores interrupt enabled state to state they were in
 *					prior to ::DISABLEINTR call.
 *	\param	p_		Pointer to variable containing interrupt enabled state 
 *					previously read with ::DISABLEINTR;
 */
#define ENABLEINTR(p_)		hw__isrEnableIntr(*(p_))

/**
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration. 
 *	\note		Interrupts are unconditionally enabled once the operation is 
 *				done.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

/** 
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration and then restoring interrupt enabled state to what
 *				it was before the call.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomicintr(e_)	{ 													\
	tools_IsrState tools__primask;											\
	DISABLEINTR(&tools__primask);											\
	{ e_ }																	\
	ENABLEINTR(&tools__primask);											\
}

/*
 *  4 byte align required
 */
# define TARGET_MEM_ALIGN	4

#define near_t
#define far_t

/* pragma message NOT supported */
#undef TOOLS_MESSAGE_PRAGMA

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1


/*------------------------------------------------------------------------------
;
;		T A R G E T   N X P   C O R T E X - M 4 / G N U
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_CM4GNU

typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
typedef unsigned long 		Uint32;
typedef signed char			Int8;
typedef short				Int16;
typedef long				Int32;
typedef long long			Int64;
typedef unsigned long long	Uint64;
typedef unsigned char		Boolean;
typedef float				Float;
typedef double				Double;

typedef unsigned long		Ufast8;
typedef unsigned long		Ufast16;
typedef unsigned long		Ufast32;
typedef long				Fast8;
typedef long				Fast16;
typedef long				Fast32;

/** 
*	Interrupts enabled / disabled state variable type. To be used with 
*	::DISABLEINTR and ::ENABLEINTR.
 */
typedef unsigned long		tools_IsrState;

#define const_P				const
#define const_D				const
#define C_task

/*
 *  Include Cortex M4 core header.
 */
#include "hw.h"

/** 
 *	Disables all interrupts.
 */
#define DISABLE				__disable_irq()

/** 
 *	Enables all interrupts.
 */
#define ENABLE				__enable_irq()

#define DISABLE_ATOM		DISABLE
#define ENABLE_ATOM			ENABLE

/** 
 *	\brief			Disables interrupts and returns variable containing current 
 *					interrupt enabled status.
 *	\param	p_		Pointer to variable of type ::InterruptState which will 
 *					receive the current interrupt status.
 */
#define DISABLEINTR(p_)		*(p_) = hw__isrDisableIntr()

/** 
 *	\brief			Restores interrupt enabled state to state they were in
 *					prior to ::DISABLEINTR call.
 *	\param	p_		Pointer to variable containing interrupt enabled state 
 *					previously read with ::DISABLEINTR;
 */
#define ENABLEINTR(p_)		hw__isrEnableIntr(*(p_))

/**
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration. 
 *	\note		Interrupts are unconditionally enabled once the operation is 
 *				done.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

/** 
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration and then restoring interrupt enabled state to what
 *				it was before the call.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomicintr(e_)	{ 													\
	tools_IsrState tools__primask;											\
	DISABLEINTR(&tools__primask);											\
	{ e_ }																	\
	ENABLEINTR(&tools__primask);											\
}

/*
 *  4 byte align required
 */
# define TARGET_MEM_ALIGN	4

#define near_t
#define far_t

/* pragma message NOT supported */
#undef TOOLS_MESSAGE_PRAGMA

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1

/*------------------------------------------------------------------------------
;
;		T A R G E T   M i c r o c h i p   C O R T E X - M 7 / G N U
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_MM7GNU

typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
typedef unsigned long 		Uint32;
typedef signed char			Int8;
typedef short				Int16;
typedef long				Int32;
typedef long long			Int64;
typedef unsigned long long	Uint64;
typedef unsigned char		Boolean;
typedef float				Float;
typedef double				Double;

typedef unsigned long		Ufast8;
typedef unsigned long		Ufast16;
typedef unsigned long		Ufast32;
typedef long				Fast8;
typedef long				Fast16;
typedef long				Fast32;

/** 
*	Interrupts enabled / disabled state variable type. To be used with 
*	::DISABLEINTR and ::ENABLEINTR.
 */
typedef unsigned long		tools_IsrState;

#define const_P				const
#define const_D				const
#define C_task

/*
 *  Include Cortex-M7 core header.
 */
#include "hw.h"

/** 
 *	Disables all interrupts.
 */
#define DISABLE				__disable_irq()

/** 
 *	Enables all interrupts.
 */
#define ENABLE				__enable_irq()

#define DISABLE_ATOM		DISABLE
#define ENABLE_ATOM			ENABLE

/** 
 *	\brief			Disables interrupts and returns variable containing current 
 *					interrupt enabled status.
 *	\param	p_		Pointer to variable of type ::InterruptState which will 
 *					receive the current interrupt status.
 */
#define DISABLEINTR(p_)		*(p_) = hw__isrDisableIntr()

/** 
 *	\brief			Restores interrupt enabled state to state they were in
 *					prior to ::DISABLEINTR call.
 *	\param	p_		Pointer to variable containing interrupt enabled state 
 *					previously read with ::DISABLEINTR;
 */
#define ENABLEINTR(p_)		hw__isrEnableIntr(*(p_))

/**
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration. 
 *	\note		Interrupts are unconditionally enabled once the operation is 
 *				done.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

/** 
 *	\brief		Performs atomic operation by disabling interupts for the 
 *				duration and then restoring interrupt enabled state to what
 *				it was before the call.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomicintr(e_)	{ 													\
	tools_IsrState tools__primask;											\
	DISABLEINTR(&tools__primask);											\
	{ e_ }																	\
	ENABLEINTR(&tools__primask);											\
}

/*
 *  4 byte align required
 */
# define TARGET_MEM_ALIGN	4

#define near_t
#define far_t

/* pragma message NOT supported */
#undef TOOLS_MESSAGE_PRAGMA

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1


/*------------------------------------------------------------------------------
;
;		T A R G E T   S T   C o r t e x - M 4 / G N U / IAR
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_SM4IAR || TARGET_SELECTED == TARGET_SM4GNU \
	|| TARGET_SELECTED == TARGET_S4F4IAR || TARGET_SELECTED == TARGET_S4F4GNU

typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
#if TARGET_SELECTED == TARGET_SM4IAR || TARGET_SELECTED == TARGET_S4F4IAR
typedef unsigned int		Uint32; /* Declared to match the uint32_t type */
#else
typedef unsigned long		Uint32; /* Declared to match the uint32_t type */
#endif
typedef signed char			Int8;
typedef short				Int16;
#if TARGET_SELECTED == TARGET_SM4IAR || TARGET_SELECTED == TARGET_S4F4IAR
typedef int					Int32;	/* Declared to match the uint32_t type */
#else
typedef long				Int32;	/* Declared to match the uint32_t type */
#endif
typedef long long			Int64;
typedef unsigned long long	Uint64;
typedef unsigned char		Boolean;
typedef float				Float;
typedef double				Double;

typedef Uint32				Ufast8;
typedef Uint32				Ufast16;
typedef Uint32				Ufast32;
typedef Int32				Fast8;
typedef Int32				Fast16;
typedef Int32				Fast32;

/** 
 *	Interrupts enabled / disabled state variable type. To be used with 
 *	::DISABLEINTR and ::ENABLEINTR.
 */
typedef unsigned long		tools_IsrState;

#define const_P				const
#define const_D				const
#define C_task
#define INLINE				inline

/*
 *  Include Cortex-M4 core header.
 */
#include "hw.h"

/** 
 *	Disables all interrupts.
 */
#define DISABLE				__disable_irq()
//#define DISABLE				hw_disableLowPrioInterrupts()

/** 
 *	Enables all interrupts.
 */
#define ENABLE				__enable_irq()
//#define ENABLE				hw_enableLowPrioInterrupts()

#define DISABLE_ATOM		DISABLE
#define ENABLE_ATOM			ENABLE

/** 
 *	\brief			Disables interrupts and returns variable containing current 
 *					interrupt enabled status.
 *	\param	p_		Pointer to variable of type ::InterruptState which will 
 *					receive the current interrupt status.
 */
#define DISABLEINTR(p_)		*(p_) = hw__isrDisableIntr()

/** 
 *	\brief			Restores interrupt enabled state to state they were in
 *					prior to ::DISABLEINTR call.
 *	\param	p_		Pointer to variable containing interrupt enabled state 
 *					previously read with ::DISABLEINTR;
 */
#define ENABLEINTR(p_)		hw__isrEnableIntr(*(p_))

/**
 *	\brief		Performs atomic operation by disabling interrupts for the 
 *				duration. 
 *	\note		Interrupts are unconditionally enabled once the operation is 
 *				done.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

/** 
 *	\brief		Performs atomic operation by disabling interrupts for the 
 *				duration and then restoring interrupt enabled state to what
 *				it was before the call.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomicintr(e_)	{ 													\
	tools_IsrState tools__primask;											\
	DISABLEINTR(&tools__primask);											\
	{ e_ }																	\
	ENABLEINTR(&tools__primask);											\
}

/*
 *  4 byte align required
 */
# define TARGET_MEM_ALIGN	4

#define near_t
#define far_t

/* pragma message NOT supported */
#undef TOOLS_MESSAGE_PRAGMA

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1


/*------------------------------------------------------------------------------
;
;		T A R G E T   S T   C o r t e x - M 4 / G N U / IAR
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED == TARGET_S3F2GNU

typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned char		Uint8;
typedef unsigned short		Uint16;
typedef unsigned long		Uint32; /* Declared to match the uint32_t type */

typedef signed char			Int8;
typedef short				Int16;
typedef long				Int32;	/* Declared to match the uint32_t type */

typedef long long			Int64;
typedef unsigned long long	Uint64;
typedef unsigned char		Boolean;
typedef float				Float;
typedef double				Double;

typedef Uint32				Ufast8;
typedef Uint32				Ufast16;
typedef Uint32				Ufast32;
typedef Int32				Fast8;
typedef Int32				Fast16;
typedef Int32				Fast32;

/** 
 *	Interrupts enabled / disabled state variable type. To be used with 
 *	::DISABLEINTR and ::ENABLEINTR.
 */
typedef unsigned long		tools_IsrState;

#define const_P				const
#define const_D				const
#define C_task
#define INLINE				inline

/*
 *  Include Cortex-M3 core header.
 */
#include "hw.h"

/** 
 *	Disables all interrupts.
 */
#define DISABLE				__disable_irq()
//#define DISABLE				hw_disableLowPrioInterrupts()

/** 
 *	Enables all interrupts.
 */
#define ENABLE				__enable_irq()
//#define ENABLE				hw_enableLowPrioInterrupts()

#define DISABLE_ATOM		DISABLE
#define ENABLE_ATOM			ENABLE

/** 
 *	\brief			Disables interrupts and returns variable containing current 
 *					interrupt enabled status.
 *	\param	p_		Pointer to variable of type ::InterruptState which will 
 *					receive the current interrupt status.
 */
#define DISABLEINTR(p_)		*(p_) = hw__isrDisableIntr()

/** 
 *	\brief			Restores interrupt enabled state to state they were in
 *					prior to ::DISABLEINTR call.
 *	\param	p_		Pointer to variable containing interrupt enabled state 
 *					previously read with ::DISABLEINTR;
 */
#define ENABLEINTR(p_)		hw__isrEnableIntr(*(p_))

/**
 *	\brief		Performs atomic operation by disabling interrupts for the 
 *				duration. 
 *	\note		Interrupts are unconditionally enabled once the operation is 
 *				done.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomic(e_)			{ DISABLE_ATOM; e_  ENABLE_ATOM; }

/** 
 *	\brief		Performs atomic operation by disabling interrupts for the 
 *				duration and then restoring interrupt enabled state to what
 *				it was before the call.
 *	\param		e_	Operation(s) to perform atomically.
 */
#define atomicintr(e_)	{ 													\
	tools_IsrState tools__primask;											\
	DISABLEINTR(&tools__primask);											\
	{ e_ }																	\
	ENABLEINTR(&tools__primask);											\
}

/*
 *  4 byte align required
 */
# define TARGET_MEM_ALIGN	4

#define near_t
#define far_t

/* pragma message NOT supported */
#undef TOOLS_MESSAGE_PRAGMA

/*
 *	Stack growth direction. Positive means growing towards higher addresses and
 *	negative means growing towards smaller addresses.
 */
#define TOOLS_STACK_GROWTH	-1

/*------------------------------------------------------------------------------
;
;		T A R G E T   D O X Y G E N
;
;*----------------------------------------------------------------------------*/

#elif TARGET_SELECTED	==	TARGET_DOXY

/** Constant data stored in program memory 	*/
# define const_P			const_P

/** Constant data stored in ram				*/
# define const_D			const_D

/**
 * 	\brief	Everything put within the parentheses will be executed with
 * 			interrupts (except NMI) disabled.
 *
 * 	\param	e_	Code to be executed atomically.
 *
 * 	\note	This macro cannot be used inside an ISR. It will always enable
 * 			interrupts.
 */

#define atomic(e_)

/**
 * 	\brief		Same as atomic() but safe inside ISRs.
 *
 * 	\param		e_		Code to be executed atomically.
 *
 * 	\details	The macro will save the interrupt state before executing the
 * 				code. After the code is executed the macro restores the previous
 * 				interrupt state.
 *
 * 	\note		It is safe to use this macro inside ISRs.
 */

#define atomicintr(e_)

/**
 *	TARGET_MEM_ALIGN can be specified for any target but if not, the default is
 *	one which is that the memory (RAM) is BYTE-oriented. TARGET_MEM_ALIGN merely
 *	indicates how many BYTEs to include in testing the alignment, not
 *	the alignment directly. It is safe to use a larger value than required,
 *	probably just some additional code is generated but no RAM is wasted.
 *	Valid values: 1, 2, 4, 8 and 16.
 */

#define TARGET_MEM_ALIGN	1

/*------------------------------------------------------------------------------
;
;	End of target dependent macros and data types
;
;*----------------------------------------------------------------------------*/

#endif

/*******************************************************************************
;
;		T A R G E T   I N D E P E N D E N T
;
*******************************************************************************/

#ifndef INLINE
# define INLINE
#endif

/**
 *	Doubly Linked List structure.
 */

typedef struct link2__node {
	struct link2__node *	next;
	struct link2__node *	prev;
} link2_Node;

#if TARGET_SELECTED == TARGET_W32
/* Must include osa.h here because DISABLE and ENABLE uses OSA functions */
# include "osa.h"
#endif

#ifndef TARGET_MEM_ALIGN
# define TARGET_MEM_ALIGN	1
#endif

#endif

