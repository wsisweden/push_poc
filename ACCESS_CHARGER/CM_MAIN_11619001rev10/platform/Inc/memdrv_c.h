/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		MEMDRV_C.H
*
*	\ingroup	MEMDRV
*
*	\brief		Mem driver cache interface.
*
*	\note		
*
*	\version	\$Rev: 2778 $ \n
*				\$Date: 2015-05-07 10:12:28 +0300 (to, 07 touko 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef MEMDRV_C_H_INCLUDED
#define MEMDRV_C_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "memdrv.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef Uint16				memdrv__Sector;

typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	void **					pDriver;	/**< Pointer to driver instance ptr.*/
	memdrv_Interface const_P * pInterface;	/**< Pointer to driver interface.*/
	Uint16					cacheSize;	/**< RAM cache size.				*/
	Uint16					sectorSize;	/**< Reported block size.			*/
} memdrv_CacheConfig;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void *						memdrv__cacheInit(memdrv_CacheConfig const_P *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern memdrv_Interface const_P memdrv_cacheInterface;

/***************************************************************//** \endcond */

#endif
