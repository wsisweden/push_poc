/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		MEMDRV.H
*
*	\ingroup	MEMDRV
*
*	\brief		Mem driver interface
*
*	\note		
*
*	\version	\$Rev: 4434 $ \n
*				\$Date: 2021-02-04 15:38:21 +0200 (to, 04 helmi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MEMDRV	MEMDRV
*
*	\brief		Memory driver interface
*
********************************************************************************
*
*	\details	The memory driver interface is used by memory drivers like 
*				AT25000 and M24XXX. The interface can be used by many kinds of 
*				memory drivers. Most commonly some sort of non-volatile memory.
*
*				Here is a common configuration:
*	<pre>
*				                   MEMDRV if
*				                       |                      Connection to some
*				+-----+    +-------+   |   +---------------+  sort of memory
*				| REG |----| NVMEM |---|---| Memory driver |----... ..
*				+-----+    +-------+   |   +---------------+
*				                                  
*	</pre>
*	\par		EEPROM and flash differences
*				Both EEPROM and NOR flash memories are supported by the
*				interface. It is however important to know that EEPROM and
*				flash memories must be handled differently when writing data to
*				them. The function block that uses a memory driver must know
*				if it's a flash memory driver and handle it accordingly. It is
*				possible to read the memory type through the interface.
* 	\par
*				Reading a EEPROM memory and a NOR flash memory is done exactly
*				the same way.
*	\par
*				If a FB is made only for EEPROM memory drivers then a flash
*				memory driver cannot be used even if both drivers uses the
*				MEMDRV interface. For example NVMEM assumes that the memory
*				doesn't have to be erased before it is written and therefore
*				only works with EEPROM memories. It may however be possible to
*				make a function block support both types.
*	\par
*				It is possible to make a EEPROM memory driver that simulates
*				the flash memory erase operations. Such a driver can in some
*				cases be used to replace a real flash memory driver.
*
*	\par		Block and byte level addressing
*				Some memories only support block level addressing. The
*				MEMDRV_OP_GET_INFO operation can be used to ask the addressing
*				mode of the memory. The addressing mode will usually not change
*				at run-time but drivers for removable memories may need to do
*				that.
*	\par
*				A driver for a byte level addressing memory can be made to use
*				block level addressing but a driver for a block level addressing
*				memory can not easily be made to use byte level addressing. That
*				would require a read-modify-write operation for each byte level
*				write.
*
*				Block level addressing memories are often so large that a byte
*				address would not fit into 32 bits e.g. a SD memory card.
*
*	\par		Removable memory
*				Some memories can be removed at run-time. The 
*				MEMDRV_OP_MEDIA_PRESENT operation can be used to check if a
*				memory is connected. The application using this kind of memory
*				must be made to support this.
*	\par
*				Each time a new memory is connected the MEMDRV_OP_GET_INFO
*				operation may return different information.
*	
*	\par		Driver specific operations
*				Some drivers may support operations that are not specified in
*				this interface. The operations are accessed through the doOp
*				interface function. The code for all such operations should be
*				MEMDRV_OP_1ST_DRIVER or above.
*
*******************************************************************************/

#ifndef MEMDRV_H_INCLUDED
#define MEMDRV_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * Memory driver return codes. The memory driver supplies one of the following
 * codes to the MEMDRV request callback function. The code is put in the count
 * field of the request.
 */

typedef enum memdrv_retCode {			/*''''''''''''''''''''''''''''''''''*/
	MEMDRV_OK,							/**< OK (read / write request)      */
	MEMDRV_ERROR,						/**< Fatal error                    */
	MEMDRV_BUSY,						/**< Driver is busy (busy request)  */
	MEMDRV_FREE,						/**< Driver is free (busy request)  */
	MEMDRV_NOT_SUPPORTED,				/**< Operation not supported		*/
	MEMDRV_ABORTED,						/**< Operation has been aborted		*/
	MEMDRV_PARAM_ERROR,					/**< Request parameter error		*/
	MEMDRV_MEDIA_IS_PRESENT,			/**< Media is connected.			*/
	MEMDRV_MEDIA_NOT_PRESENT			/**< Media is not connected.		*/
} memdrv_RetCode;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Operation types. One of the following operations are put into the op
 * field of the request when the doOp function is called.
 *
 * These operations will not be supported by all memory drivers e.g. the erase 
 * operations are used in FLASH memory drivers but are not needed in EEPROM
 * memory drivers.
 *
 * The PUT, GET and BUSY operations should not be used with the doOp function.
 * They are included in this list so that the memory driver can use them
 * internally.
 */

enum memdrv_operation {					/*''''''''''''''''''''''''''''''''''*/
	MEMDRV_OP_PUT = 0,					/**< Write data.					*/
	MEMDRV_OP_GET,						/**< Read data.						*/
	MEMDRV_OP_BUSY,						/**< Check if busy.					*/
	MEMDRV_OP_SLEEP,					/**< Set memory to sleep mode. This op
										 * request can be given even if the 
										 * memory is already sleeping. 		*/
	MEMDRV_OP_WAKEUP,					/**< Wakeup memory from sleep mode.
										 * This op request can be given even
										 * if the memory isn't sleeping.	*/
	MEMDRV_OP_ERASE_SECTOR,				/**< Erase memory sector.			*/
	MEMDRV_OP_ERASE_CHIP,				/**< Erase whole memory.			*/
	MEMDRV_OP_ERASE_BLOCK,				/**< Erase memory block.			*/
	MEMDRV_OP_GET_INFO,					/**< Get memory info.				*/
	MEMDRV_OP_INIT_MEM,					/**< Initialize memory.				*/
	MEMDRV_OP_MEDIA_PRESENT,			/**< Check if media is present.		*/
	MEMDRV_OP_FLUSH,					/**< Flush data to media.			*/
	MEMDRV_OP_GET_EXT_INFO,				/**< Get extended device information.*/
	MEMDRV_OP_ERASE_RANGE,				/**< Erase memory range.			*/
										/*									*/
	MEMDRV_OP_COUNT,					/**< Number of operations.			*/
										/*									*/
	MEMDRV_OP_1ST_DRIVER = 200			/**< First driver specific op.		*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \name	Memory info flags
 *
 * Flags that are used in the flags field of the memdrv_MemInfo structure.
 */

/*@{*/
#define MEMDRV_FLG_BLOCK	(1<<0)		/**< Memory uses block addresses.	*/
#define MEMDRV_FLG_ERASE	(1<<1)		/**< Must be erased before writing.	*/
#define MEMDRV_FLG_FLUSH	(1<<2)		/**< Requires flushing.				*/
#define MEMDRV_FLG_WRNIBBLE	(1<<3)		/**< Multiple writes to the
										 * same nibble is not allowed.		*/
/*@}*/
							 
/**
 * \name	Driver description flags
 *
 * Flags that are used to define how the memdrv implementation being called
 * functions. (for example is it synchronous or asynchronous).
 */
/*@{*/
#define MEMDRV_FUNC_SYNCHRONOUS (1<<0)	/**< Driver is synchronous.			*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

struct memdrv__alloc;

/**
 * Memory info structure. The MEMDRV_OP_GET_INFO operation will copy memory info
 * to the pData buffer using this structure. The memory driver must be able to
 * cast the pData buffer to this type i.e. it must be properly aligned.
 *
 * The memory size can be calculated by multiplying sectorCount with sectorSize.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8					flags;		/**< Information flags.				*/
	Uint16					sectorSize;	/**< Erase block size in bytes. 1=The
	 	 	 	 	 	 	 	 	 	 *	memory does not have erase blocks.*/
	Uint16					jedecId;	/**< JEDEC manufacturer ID.			*/
	Uint8					deviceId[4];/**< Vendor specific data.		*/
	Uint32					sectorCount;/**< Erase block count. 0=unknown. 	*/
} memdrv_MemInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Memory addresses are stored with this type.
 */

typedef Uint32				memdrv_AddrType;

/**
 * Data size is stored with this type.
 */

typedef Uint16				memdrv_SizeType;

/**
 * Additional parameters for range erasing. The pData field of the alloc struct
 * should point to a variable of this type when requesting a
 * MEMDRV_OP_ERASE_RANGE operation.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	memdrv_AddrType			endAddr;	/**< The end address of the erase.	*/
} memdrv_RngErase;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief	Type for callback function pointers.
 * 
 * \param	pReq	Pointer to the completed memory driver request.
 */

typedef void (* 			memdrv_DoneFn)(struct memdrv__alloc * pReq);

/**
 * Memory driver request. The type is used when calling a memory driver 
 * interface function. When a request has been sent to a driver it must not be
 * modified before the callback function is called.
 *
 * After the request has been handled the memory driver provides the request
 * pointer to the callback function. The return code has to be put in the count
 * field before calling the function. 
 */
 
typedef struct memdrv__alloc {			/*''''''''''''''''''''''''''''' RAM */
	memdrv_AddrType			address;	/**< Address                        */
	memdrv_SizeType			count;		/**< Size of data in bytes			*/
	union {								/*									*/
		BYTE *				pData;		/**< Ptr to data					*/
		BYTE const_D *		pConstData;	/**< Ptr to constant data			*/
	};									/*									*/
	memdrv_DoneFn 			fnDone; 	/**< The callback function.			*/
	struct memdrv__alloc *	pNext;		/**< Pointer to next request.		*/
	Uint8					op;			/**< Specifies the wanted Operation. 
										 * Does not need to be set for put, get
										 * or busy calls.					*/
	void *					pUtil;		/**< Utility pointer set by the FB
										 *	calling the driver.				*/
} memdrv_Alloc;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * This type is used by all memory driver interface functions. The function
 * type takes the following parameters:
 * - Pointer to the memory driver instance.
 * - Pointer to the MEMDRV request.
 */
 
typedef void (*				memdrv_fnAccess)(void *, memdrv_Alloc *);

/**
 * Memory driver interface type. All memory drivers should have a public 
 * interface of this type. A pointer to the interface is supplied to all FBs
 * that will be using the memory driver. The FBs can then access the memory 
 * driver through the interface.
 */
 
typedef struct {						/*''''''''''''''''''''''''''' CONST */
	memdrv_fnAccess			put;		/**< Writes non const data to mem   */
	memdrv_fnAccess			get;		/**< Function to read from memory   */
	memdrv_fnAccess			busy;		/**< Function to check memory status*/
	memdrv_fnAccess			doOp;		/**< Function to do other operation	*/
	Uint8					flags;		/**< Operation flags				*/
} memdrv_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
