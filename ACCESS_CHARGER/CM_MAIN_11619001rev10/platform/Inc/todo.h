/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		TODO.H
*
*	\ingroup	TODO
*
*	\brief		Todo public header.
*
*	\details		
*
*	\note		
*
*	\version	11.6.2004   12:47
*
*******************************************************************************/

#ifndef LIB_TODO_H_INCLUDED
#define LIB_TODO_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
 
/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Todo list callback function definition and list structure.
 */
 
typedef void (*				todo_Callback_f)(void *);

/**
 *	Todo structure.
 */
 
typedef struct {
	void *					pNext;			/**< Ptr to next todo           */
	todo_Callback_f			pCallback;		/**< Callback function          */
} todo_Link_st;

typedef struct {
	todo_Link_st *			pNext;
} todo_Base_st;

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*
 *	Macros for adding a todo into the todo list.
 *	todo_add can be used when interrupts are not disabled
 *	todo_add_isr can be used when interrupts are disabled (for example from an
 *	isr function).
 */
#define	todo_init()

#if 0

#define todo_add(p_)		atomic(todo__add(p_))
#define todo_addIsr(p_)		todo__add(p_)

#else
#define todo_add(p_)		todo_addIsr(p_)
#define todo_addIsr(p_)														\
	{																		\
		atomicintr(															\
			(p_)->pNext = todo__instance.pNext;								\
			todo__instance.pNext = (p_);									\
		)																	\
	}
#endif

#define todo_next(p_)		(((todo_Link_st *)(p_))->pNext)

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

//PUBLIC void					todo__add(todo_Link_st *);
PUBLIC Boolean				todo_process(void);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern todo_Base_st		todo__instance;

/***************************************************************//** \endcond */

#endif
