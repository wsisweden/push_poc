/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_SYS_H__
#define __LWIP_SYS_H__

#include "ntcpip/opt.h"

#ifdef __cplusplus
extern "C" {
#endif

#if NTCPIP__NO_SYS

/* For a totally minimal and standalone system, we provide null
   definitions of the sys_ functions. */
typedef Uint8 ntcpip__SysSema;
typedef Uint8 ntcpip__SysMbox;
struct ntcpip__sysTimeo {Uint8 dummy;};

#define ntcpip__sysInit()
#define ntcpip__sysTimeout(m,h,a)
#define ntcpip__sysUntimeout(m,a)
#define ntcpip__sysSemNew(c) c
#define ntcpip__sysSemSignal(s)
#define ntcpip__sysSemWait(s)
#define ntcpip__sysSemWaitTimeout(s,t)
#define ntcpip__sysArchSemWait(s,t)
#define ntcpip__sysSemFree(s)
#define ntcpip__sysMboxNew(s) 0
#define ntcpip__sysMboxFetch(m,d)
#define ntcpip__sysMboxTryFetch(m,d)
#define ntcpip__sysMboxPost(m,d)
#define ntcpip__sysMboxTrypost(m,d)
#define ntcpip__sysMboxFree(m)

#define ntcpip__sysThreadNew(n,t,a,s,p)

#else /* NTCPIP__NO_SYS */

/** Return code for timeouts from ntcpip__sysArchMboxFetch and ntcpip__sysArchSemWait */
#define NTCPIP__SYS_ARCH_TIMEOUT 0xffffffffUL

/* ntcpip__sysMboxTryFetch returns NTCPIP__SYS_MBOX_EMPTY if appropriate.
 * For now we use the same magic value, but we allow this to change in future.
 */
#define NTCPIP__SYS_MBOX_EMPTY NTCPIP__SYS_ARCH_TIMEOUT 

#include "ntcpip/err.h"
#include "ntcpip/arch/sys_arch.h"

typedef void (* ntcpip__sysTimeoutHandler)(void *arg);

struct ntcpip__sysTimeo {
  struct ntcpip__sysTimeo *next;
  Uint32 time;
  ntcpip__sysTimeoutHandler h;
  void *arg;
};

struct ntcpip__sysTimeouts {
  struct ntcpip__sysTimeo *next;
};

/* ntcpip__sysInit() must be called before anthing else. */
void ntcpip__sysInit(void);

/*
 * ntcpip__sysTimeout():
 *
 * Schedule a timeout a specified amount of milliseconds in the
 * future. When the timeout occurs, the specified timeout handler will
 * be called. The handler will be passed the "arg" argument when
 * called.
 *
 */
void ntcpip__sysTimeout(Uint32 msecs, ntcpip__sysTimeoutHandler h, void *arg);
void ntcpip__sysUntimeout(ntcpip__sysTimeoutHandler h, void *arg);
struct ntcpip__sysTimeouts *ntcpip__sysArchTimeouts(void);

/* Semaphore functions. */
ntcpip__SysSema ntcpip__sysSemNew(Uint8 count);
void ntcpip__sysSemSignal(ntcpip__SysSema sem);
Uint32 ntcpip__sysArchSemWait(ntcpip__SysSema sem, Uint32 timeout);
void ntcpip__sysSemFree(ntcpip__SysSema sem);
void ntcpip__sysSemWait(ntcpip__SysSema sem);
int ntcpip__sysSemWaitTimeout(ntcpip__SysSema sem, Uint32 timeout);

/* Time functions. */
#ifndef ntcpip__sysMSleep
void ntcpip__sysMSleep(Uint32 ms); /* only has a (close to) 1 jiffy resolution. */
#endif
#ifndef ntcpip__sysJiffies
Uint32 ntcpip__sysJiffies(void); /* since power up. */
#endif

/* Mailbox functions. */
ntcpip__SysMbox ntcpip__sysMboxNew(int size);
void ntcpip__sysMboxPost(ntcpip__SysMbox mbox, void *msg);
ntcpip_Err ntcpip__sysMboxTrypost(ntcpip__SysMbox mbox, void *msg);
Uint32 ntcpip__sysArchMboxFetch(ntcpip__SysMbox mbox, void **msg, Uint32 timeout);
#ifndef ntcpip__sysArchMboxTryFetch /* Allow port to override with a macro */
Uint32 ntcpip__sysArchMboxTryFetch(ntcpip__SysMbox mbox, void **msg);
#endif
/* For now, we map straight to sys_arch implementation. */
#define ntcpip__sysMboxTryFetch(mbox, msg) ntcpip__sysArchMboxTryFetch(mbox, msg)
void ntcpip__sysMboxFree(ntcpip__SysMbox mbox);
void ntcpip__sysMboxFetch(ntcpip__SysMbox mbox, void **msg);

/* Thread functions. */
ntcpip__SysThread ntcpip__sysThreadNew(char *name, void (* thread)(void *arg), void *arg, int stacksize, int prio);

#endif /* NTCPIP__NO_SYS */

/** Returns the current time in milliseconds. */
Uint32 ntcpip__sysNow(void);

/* Critical Region Protection */
/* These functions must be implemented in the sys_arch.c file.
   In some implementations they can provide a more light-weight protection
   mechanism than using semaphores. Otherwise semaphores can be used for
   implementation */
#ifndef NTCPIP__SYS_ARCH_PROTECT
/** NTCPIP__SYS_LIGHTWEIGHT_PROT
 * define NTCPIP__SYS_LIGHTWEIGHT_PROT in lwipopts.h if you want inter-task protection
 * for certain critical regions during buffer allocation, deallocation and memory
 * allocation and deallocation.
 */
#if NTCPIP__SYS_LIGHTWEIGHT_PROT

/** NTCPIP__SYS_ARCH_DECL_PROTECT
 * declare a protection variable. This macro will default to defining a variable of
 * type ntcpip__SysProt. If a particular port needs a different implementation, then
 * this macro may be defined in sys_arch.h.
 */
#define NTCPIP__SYS_ARCH_DECL_PROTECT(lev) ntcpip__SysProt lev
/** NTCPIP__SYS_ARCH_PROTECT
 * Perform a "fast" protect. This could be implemented by
 * disabling interrupts for an embedded system or by using a semaphore or
 * mutex. The implementation should allow calling NTCPIP__SYS_ARCH_PROTECT when
 * already protected. The old protection level is returned in the variable
 * "lev". This macro will default to calling the ntcpip__sysArchProtect() function
 * which should be implemented in sys_arch.c. If a particular port needs a
 * different implementation, then this macro may be defined in sys_arch.h
 */
#define NTCPIP__SYS_ARCH_PROTECT(lev) lev = ntcpip__sysArchProtect()
/** NTCPIP__SYS_ARCH_UNPROTECT
 * Perform a "fast" set of the protection level to "lev". This could be
 * implemented by setting the interrupt level to "lev" within the MACRO or by
 * using a semaphore or mutex.  This macro will default to calling the
 * ntcpip__sysArchUnprotect() function which should be implemented in
 * sys_arch.c. If a particular port needs a different implementation, then
 * this macro may be defined in sys_arch.h
 */
#define NTCPIP__SYS_ARCH_UNPROTECT(lev) ntcpip__sysArchUnprotect(lev)
ntcpip__SysProt ntcpip__sysArchProtect(void);
void ntcpip__sysArchUnprotect(ntcpip__SysProt pval);

#else

#define NTCPIP__SYS_ARCH_DECL_PROTECT(lev)
#define NTCPIP__SYS_ARCH_PROTECT(lev)
#define NTCPIP__SYS_ARCH_UNPROTECT(lev)

#endif /* NTCPIP__SYS_LIGHTWEIGHT_PROT */

#endif /* NTCPIP__SYS_ARCH_PROTECT */

/*
 * Macros to set/get and increase/decrease variables in a thread-safe way.
 * Use these for accessing variable that are used from more than one thread.
 */

#ifndef NTCPIP__SYS_ARCH_INC
#define NTCPIP__SYS_ARCH_INC(var, val) do { \
                                NTCPIP__SYS_ARCH_DECL_PROTECT(old_level); \
                                NTCPIP__SYS_ARCH_PROTECT(old_level); \
                                var += val; \
                                NTCPIP__SYS_ARCH_UNPROTECT(old_level); \
                              } while(0)
#endif /* NTCPIP__SYS_ARCH_INC */

#ifndef NTCPIP__SYS_ARCH_DEC
#define NTCPIP__SYS_ARCH_DEC(var, val) do { \
                                NTCPIP__SYS_ARCH_DECL_PROTECT(old_level); \
                                NTCPIP__SYS_ARCH_PROTECT(old_level); \
                                var -= val; \
                                NTCPIP__SYS_ARCH_UNPROTECT(old_level); \
                              } while(0)
#endif /* NTCPIP__SYS_ARCH_DEC */

#ifndef NTCPIP__SYS_ARCH_GET
#define NTCPIP__SYS_ARCH_GET(var, ret) do { \
                                NTCPIP__SYS_ARCH_DECL_PROTECT(old_level); \
                                NTCPIP__SYS_ARCH_PROTECT(old_level); \
                                ret = var; \
                                NTCPIP__SYS_ARCH_UNPROTECT(old_level); \
                              } while(0)
#endif /* NTCPIP__SYS_ARCH_GET */

#ifndef NTCPIP__SYS_ARCH_SET
#define NTCPIP__SYS_ARCH_SET(var, val) do { \
                                NTCPIP__SYS_ARCH_DECL_PROTECT(old_level); \
                                NTCPIP__SYS_ARCH_PROTECT(old_level); \
                                var = val; \
                                NTCPIP__SYS_ARCH_UNPROTECT(old_level); \
                              } while(0)
#endif /* NTCPIP__SYS_ARCH_SET */


#ifdef __cplusplus
}
#endif

#endif /* __LWIP_SYS_H__ */

