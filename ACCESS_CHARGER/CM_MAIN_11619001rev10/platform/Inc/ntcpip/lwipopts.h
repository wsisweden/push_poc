/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		lwipopts.h
*
*	\ingroup	NTCPIP
*
*	\brief		LwIP settings.
*
*	\details	Should be as generic as possible so that ntcpip does not need
*				to be recompiled for each project.
*
*	\note
*
*	\version	09-03-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef LWIPOPTS_H_INCLUDED
#define LWIPOPTS_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"

/*
 * The following file can be created in the global include directory. It should
 * contain project specific defines to configure LwIP. Defines made here can
 * be overridden in that file.
 *
 * A dummy version of the file is in the platform inc folder. The dummy version
 * will be included if the file doesn't exist in the global_inc folder.
 *
 * Using the <> style instead of "" because of how Visual Studio handles "".
 */

#include <ntcpip_p.h>

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * NTCPIP__SYS_LIGHTWEIGHT_PROT==1: if you want inter-task protection for certain
 * critical regions during buffer allocation, deallocation and memory
 * allocation and deallocation.
 */

#define NTCPIP__SYS_LIGHTWEIGHT_PROT		1

/*
	------------------------------------
	---------- Memory options ----------
	------------------------------------
*/

/**
 * NTCPIP__MEM_ALIGNMENT: should be set to the alignment of the CPU
 *    4 byte alignment -> #define NTCPIP__MEM_ALIGNMENT 4
 *    2 byte alignment -> #define NTCPIP__MEM_ALIGNMENT 2
 */

#define NTCPIP__MEM_ALIGNMENT				TARGET_MEM_ALIGN


/**
 * NTCPIP__MEM_SIZE: the size of the heap memory. If the application will send
 * a lot of data that needs to be copied, this should be set high.
 *
 * tlarsu: This is not needed when the platform memory manager is used.
 */
#ifndef NTCPIP__MEM_SIZE
#define NTCPIP__MEM_SIZE					0
#endif

/**
* MEMP_MEM_MALLOC==1: Use ntcpip__memMalloc/ntcpip__memFree instead of the lwip
* pool allocator. Especially useful with NTCPIP__MEM_LIBC_MALLOC but handle with
* care regarding execution speed and usage from interrupts!
*/
#ifndef MEMP_MEM_MALLOC
#define MEMP_MEM_MALLOC						1
#endif

/**
 * NTCPIP__LWIP_NETIF_LINK_CALLBACK==1: Support a callback function from an interface
 * whenever the link changes (i.e., link down)
 *
 * tlarsu:	There seems to be a bug in LwIP so that the link callback must be
 * 			enabled so that NETIF can report link status changes.
 * 			The callback is not used in this implementation but NETIF reports
 * 			link changes to the stack.
 *
 *			The newest CVS version has fixed this. So next release will probably
 *			not have to enable the link callback.
 */

#define NTCPIP__LWIP_NETIF_LINK_CALLBACK	1


/*
	------------------------------------------------
	---------- Internal Memory Pool Sizes ----------
	------------------------------------------------
*/

/**
 *	NTCPIP__MEMP_NUM_TCP_SEG: the number of simultaneously queued TCP segments.
 *	(requires the NTCPIP__LWIP_TCP option)
 *
 *	Should be at least as big as NTCPIP__TCP_SND_QUEUELEN
 *
 *	This is configured to a really high value so that it will pass the check in
 *	lwip_sanity_check(). The value does not matter because we do not use memory
 *	pools. T-Plat.E uses dynamic memory instead.
 */
#ifndef NTCPIP__MEMP_NUM_TCP_SEG
#define NTCPIP__MEMP_NUM_TCP_SEG	Uint16_MAX
#endif

/**
 * NTCPIP__MEMP_NUM_SYS_TIMEOUT: the number of simultaneously active timeouts.
 * (requires NTCPIP__NO_SYS==0)
 */

#ifndef NTCPIP__MEMP_NUM_SYS_TIMEOUT
#define NTCPIP__MEMP_NUM_SYS_TIMEOUT	(\
	NTCPIP__LWIP_TCP\
	+ NTCPIP__IP_REASSEMBLY\
	+ NTCPIP__LWIP_ARP\
	+ (2*NTCPIP__LWIP_DHCP)\
	+ NTCPIP__LWIP_AUTOIP\
	+ NTCPIP__LWIP_IGMP\
	+ NTCPIP__LWIP_DNS\
	+ NTCPIP__PPP_SUPPORT)
#endif

/*
	---------------------------------
	---------- API options ----------
	---------------------------------
*/
/**
 * NTCPIP__LWIP_RAW==1: Enable application layer to hook into the IP layer
 * itself.
 */
#ifndef NTCPIP__LWIP_RAW
#define NTCPIP__LWIP_RAW			0
#endif

/**
 * NTCPIP__LWIP_NETIF_API==1: Support netif api (in netifapi.c)
 */
#ifndef NTCPIP__LWIP_NETIF_API
#define NTCPIP__LWIP_NETIF_API		1
#endif

/**
 * NTCPIP__LWIP_NETCONN==1: Enable Netconn API (require to use api_lib.c)
 */
#ifndef NTCPIP__LWIP_NETCONN
#define NTCPIP__LWIP_NETCONN		1
#endif

/**
 * NTCPIP__LWIP_SOCKET==1: Enable Socket API (require to use sockets.c)
 */
#ifndef NTCPIP__LWIP_SOCKET
#define NTCPIP__LWIP_SOCKET			0
#endif


/**
 *	NTCPIP__LWIP_NETIF_STATUS_CALLBACK==1: Support a callback function whenever
 *	an interface changes its up/down status (i.e., due to DHCP IP acquistion)
 */
#ifndef NTCPIP__LWIP_NETIF_STATUS_CALLBACK
#define NTCPIP__LWIP_NETIF_STATUS_CALLBACK	1
#endif


/*
	----------------------------------
	-------- Protocol options --------
	----------------------------------
*/
/**
 * NTCPIP__LWIP_DHCP==1: Enable DHCP module.
 */

#ifndef NTCPIP__LWIP_DHCP
#define NTCPIP__LWIP_DHCP			1
#endif

/**
 * NTCPIP__LWIP_AUTOIP==1: Enable AUTOIP module.
 */

#ifndef NTCPIP__LWIP_AUTOIP
#define NTCPIP__LWIP_AUTOIP			1
#endif

/**
 * NTCPIP__LWIP_DHCP_AUTOIP_COOP==1: Allow DHCP and AUTOIP to be both enabled on
 * the same interface at the same time.
 */

#ifndef NTCPIP__LWIP_DHCP_AUTOIP_COOP
#define NTCPIP__LWIP_DHCP_AUTOIP_COOP	1
#endif

/**
 * NTCPIP__LWIP_SNMP==1: Turn on SNMP module. UDP must be available for SNMP
 * transport.
 */
#ifndef NTCPIP__LWIP_SNMP
#define NTCPIP__LWIP_SNMP			0
#endif

/**
 * NTCPIP__LWIP_IGMP==1: Turn on IGMP module.
 */

#ifndef NTCPIP__LWIP_IGMP
#define NTCPIP__LWIP_IGMP			0
#endif

/**
 * NTCPIP__LWIP_DNS==1: Turn on DNS module. UDP must be available for DNS
 * transport.
 */

#ifndef NTCPIP__LWIP_DNS
#define NTCPIP__LWIP_DNS			0
#endif

/**
 * NTCPIP__LWIP_UDP==1: Turn on UDP.
 */

#ifndef NTCPIP__LWIP_UDP
#define NTCPIP__LWIP_UDP			1
#endif

/**
 * NTCPIP__LWIP_TCP==1: Turn on TCP.
 */

#ifndef NTCPIP__LWIP_TCP
#define NTCPIP__LWIP_TCP			1
#endif

/**
 * NTCPIP__PPP_SUPPORT==1: Enable PPP.
 */

#ifndef NTCPIP__PPP_SUPPORT
#define NTCPIP__PPP_SUPPORT			0
#endif

/**
 * NTCPIP__PPPOE_SUPPORT==1: Enable PPP Over Ethernet
 */

#ifndef NTCPIP__PPPOE_SUPPORT
#define NTCPIP__PPPOE_SUPPORT		0
#endif

/**
 * NTCPIP__PPPOS_SUPPORT==1: Enable PPP Over Serial
 */

#ifndef NTCPIP__PPPOS_SUPPORT
#define NTCPIP__PPPOS_SUPPORT		NTCPIP__PPP_SUPPORT
#endif


/*******************************************************************************
 * TCP options
 ******************************************************************************/

/**
 *	NTCPIP__TCP_WND: The size of a TCP window.
 *
 *	The TCP window size can be adjusted by changing the define TCP_WND. However,
 *	do keep in mind that this should be at least twice the size of TCP_MSS. If
 *	memory allows it, set this as high as possible (16-bit, so 0xFFFF is the
 *	highest value), but keep in mind that for every active connection, the full
 *	window may have to be buffered until it is acknowledged by the remote side
 *	(although this buffer size can still be controlled by TCP_SND_BUF and
 *	TCP_SND_QUEUELEN). The reason for "twice" are both the nagle algorithm and
 *	delayed ACK from the remote peer.
 *	
 *	tlarsu:	Should be at least 4*NTCPIP__TCP_MSS
 *
 *			Too big window size will result in RX overrun in the Ethernet 
 *			driver when there is no more RAM available for incoming packets.
 *			The packets will then be dropped.
 *			
 *			There should be enough heap memory for a full window size for each
 *			TCP connection.
 */

#ifndef NTCPIP__TCP_WND
#define NTCPIP__TCP_WND				(4*NTCPIP__TCP_MSS)
#endif

/**
 * NTCPIP__TCP_MSS: TCP Maximum segment size. (default is 128, a *very*
 * conservative default.)
 * For the receive side, this MSS is advertised to the remote side
 * when opening a connection. For the transmit size, this MSS sets
 * an upper limit on the MSS advertised by the remote host.
 * 
 * For maximum throughput, set this as high as possible for your network.
 * 
 * Maximum for Ethernet is 1460.
 */
#ifndef NTCPIP__TCP_MSS
#define NTCPIP__TCP_MSS				786
#endif

/**
 * NTCPIP__TCP_SND_BUF: TCP sender buffer space (bytes).
 * 
 * This limits the sender buffer space (in bytes): tcp_write only allows a
 * limited amount of bytes to be buffered (until acknowledged). For maximum
 * throughput, set this to the same value as TCP_WND (effectively disabling
 * the extra-check).
 * 
 * ATTENTION: keep in mind that every active connection might buffer this amount
 * of data, so make sure you have enough RAM or limit the number of concurrently
 * active connections! 
 */
#ifndef NTCPIP__TCP_SND_BUF
#define NTCPIP__TCP_SND_BUF			(4*1024)
#endif

/**
 * NTCPIP__TCP_SND_QUEUELEN: TCP sender buffer space (pbufs).
 * 
 * This limits the number of pbufs in the send-buffer: Every segment needs at
 * least one pbuf (when passing TCP_WRITE_FLAG_COPY to tcp_write) or up to
 * 1 + number of tcp_write-calls per segment (when not passing
 * TCP_WRITE_FLAG_COPY to tcp_write).
 * 
 * If you want to effectively disable this check, set it to
 * TCP_SNDQUEUELEN_OVERFLOW, but make sure you don't run out of pbufs then.
 *
 * This must be at least as much as (2 * NTCPIP__TCP_SND_BUF/NTCPIP__TCP_MSS)
 * for things to work.
 */
#ifndef NTCPIP__TCP_SND_QUEUELEN
#define NTCPIP__TCP_SND_QUEUELEN	(2 * (NTCPIP__TCP_SND_BUF/NTCPIP__TCP_MSS))
#endif

/**
 * NTCPIP__PBUF_POOL_BUFSIZE: the size of each pbuf in the pbuf pool. The 
 * default is designed to accommodate single full size TCP frame in one pbuf, 
 * including NTCPIP__TCP_MSS, IP header, and link header.
 */
#ifndef NTCPIP__PBUF_POOL_BUFSIZE
#define NTCPIP__PBUF_POOL_BUFSIZE LWIP_MEM_ALIGN_SIZE(NTCPIP__TCP_MSS+40+NTCPIP__PBUF_LINK_HLEN)
#endif

/*******************************************************************************
 * UDP options
 ******************************************************************************/

/**
 * NTCPIP__NETBUF_RECVINFO==1: append destination addr and port to every netbuf.
 */
#ifndef NTCPIP__NETBUF_RECVINFO
#define NTCPIP__NETBUF_RECVINFO				1
#endif

/*******************************************************************************
 * SNMP options
 ******************************************************************************/

/**
 * NTCPIP__SNMP_PRIVATE_MIB:
 */

#ifndef NTCPIP__SNMP_PRIVATE_MIB
#define NTCPIP__SNMP_PRIVATE_MIB			0
#endif

/*******************************************************************************
 * DHCP options
 ******************************************************************************/

/**
 * LWIP_DHCP_AUTOIP_COOP_TRIES: Set to the number of DHCP DISCOVER probes
 * that should be sent before falling back on AUTOIP. This can be set
 * as low as 1 to get an AutoIP address very quickly, but you should
 * be prepared to handle a changing IP address when DHCP overrides
 * AutoIP.
 */

#ifndef LWIP_DHCP_AUTOIP_COOP_TRIES
#define LWIP_DHCP_AUTOIP_COOP_TRIES			4
#endif


/*
	------------------------------------
	---------- Thread options ----------
	------------------------------------
*/

/**
 * NTCPIP__TCPIP_THREAD_NAME: The name assigned to the main tcpip thread.
 */

#define NTCPIP__TCPIP_THREAD_NAME			"ntcpip__task"


/**
 * NTCPIP__TCPIP_THREAD_STACKSIZE: The stack size used by the main tcpip thread.
 * The stack size value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__TCPIP_THREAD_STACKSIZE
#define NTCPIP__TCPIP_THREAD_STACKSIZE		1792
#endif

/**
 * NTCPIP__TCPIP_THREAD_PRIO: The priority assigned to the main tcpip thread.
 * The priority value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */

#define NTCPIP__TCPIP_THREAD_PRIO			OSA_PRIORITY_NORMAL

/*
 * 	Checksum algorithm
 */
#define LWIP_CHKSUM_ALGORITHM 				3

/*******************************************************************************
 * Debug message settings
 */

#ifndef LWIP_DEBUG
#define LWIP_DEBUG							0
#endif

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif

