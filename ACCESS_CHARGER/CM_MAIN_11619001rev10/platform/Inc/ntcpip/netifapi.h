/*
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 */
 
#ifndef __LWIP_NETIFAPI_H__
#define __LWIP_NETIFAPI_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_NETIF_API /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/sys.h"
#include "ntcpip/netif.h"
/* #include "ntcpip/dhcp.h" removed by tlarsu */
/* #include "ntcpip/autoip.h" removed by tlarsu */

#ifdef __cplusplus
extern "C" {
#endif

struct ntcpip__netifapiMsgMsg {
#if !NTCPIP__LWIP_TCPIP_CORE_LOCKING
  ntcpip__SysSema sem;
#endif /* !NTCPIP__LWIP_TCPIP_CORE_LOCKING */
  ntcpip_Err err;
  struct ntcpip__netif *netif;
  union {
    struct {
      struct ntcpip_ipAddr *ipaddr;
      struct ntcpip_ipAddr *netmask;
      struct ntcpip_ipAddr *gw;
      void *state;
      ntcpip_Err (* init) (struct ntcpip__netif *netif);
      ntcpip_Err (* input)(struct ntcpip_pbuf *p, struct ntcpip__netif *netif);
    } add;
    struct {
      void  (* voidfunc)(struct ntcpip__netif *netif);
      ntcpip_Err (* errtfunc)(struct ntcpip__netif *netif);
    } common;
  } msg;
};

struct ntcpip__netifapiMsg {
  void (* function)(struct ntcpip__netifapiMsgMsg *msg);
  struct ntcpip__netifapiMsgMsg msg;
};


/* API for application */
ntcpip_Err ntcpip_netifapiNetifAdd(struct ntcpip__netif *netif,
                                 struct ntcpip_ipAddr *ipaddr,
                                 struct ntcpip_ipAddr *netmask,
                                 struct ntcpip_ipAddr *gw,
                                 void *state,
                                 ntcpip_Err (* init)(struct ntcpip__netif *netif),
                                 ntcpip_Err (* input)(struct ntcpip_pbuf *p, struct ntcpip__netif *netif) );

ntcpip_Err ntcpip_netifapiNetifSetAddr( struct ntcpip__netif *netif,
                                 struct ntcpip_ipAddr *ipaddr,
                                 struct ntcpip_ipAddr *netmask,
                                 struct ntcpip_ipAddr *gw );

ntcpip_Err ntcpip_netifapiNetifCommon( struct ntcpip__netif *netif,
                                 void  (* voidfunc)(struct ntcpip__netif *netif),
                                 ntcpip_Err (* errtfunc)(struct ntcpip__netif *netif) );

#define ntcpip_netifapiNetifRemove(n) 	ntcpip_netifapiNetifCommon(n, ntcpip__netifRemove, NULL)
#define ntcpip_netifapiNetifSetUp(n)  	ntcpip_netifapiNetifCommon(n, ntcpip__netifSetUp, NULL)
#define ntcpip_netifapiNetifSetDown(n)	ntcpip_netifapiNetifCommon(n, ntcpip__netifSetDown, NULL)
#define ntcpip_netifapiNetifSetDefault(n) ntcpip_netifapiNetifCommon(n, ntcpip__netifSetDefault, NULL)
#define ntcpip_netifapiDhcpStart(n)  	ntcpip_netifapiNetifCommon(n, NULL, ntcpip__dhcpStart)
#define ntcpip_netifapiDhcpStop(n) 		ntcpip_netifapiNetifCommon(n, ntcpip__dhcpStop, NULL)
#define ntcpip_netifapiAutoipStart(n)	ntcpip_netifapiNetifCommon(n, NULL, ntcpip__autoipStart)
#define ntcpip_netifapiAutoipStop(n)	ntcpip_netifapiNetifCommon(n, NULL, ntcpip__autoipStop)

/* The following two are added by tlarsu */
#define ntcpip_netifapiLinkUp(n)		ntcpip_netifapiNetifCommon(n, ntcpip__netifSetLinkUp, NULL)
#define ntcpip_netifapiLinkDown(n)		ntcpip_netifapiNetifCommon(n, ntcpip__netifSetLinkDown, NULL)

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_NETIF_API */

#endif /* __LWIP_NETIFAPI_H__ */

