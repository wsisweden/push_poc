/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_IP_ADDR_H__
#define __LWIP_IP_ADDR_H__

#include "ntcpip/opt.h"

#include "ntcpip/inet.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct ntcpip_ipAddr {
  NTCPIP__PACK_STRUCT_FIELD(Uint32 addr);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

/*
 * struct ipaddr2 is used in the definition of the ARP packet format in
 * order to support compilers that don't have structure packing.
 */
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct ntcpip__ipAddr2 {
  NTCPIP__PACK_STRUCT_FIELD(Uint16 addrw[2]);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

struct ntcpip__netif;

extern const struct ntcpip_ipAddr ntcpip__ipaddrAny;
extern const struct ntcpip_ipAddr ntcpip__ipaddrBroadcast;

/** IP_ADDR_ can be used as a fixed IP address
 *  for the wildcard and the broadcast address
 */

#if 0
#define NTCPIP_IP_ADDR_ANY         ((struct ntcpip_ipAddr *) ((void *) &ntcpip__ipaddrAny))
#define NTCPIP_IP_ADDR_BROADCAST   ((struct ntcpip_ipAddr *)&ntcpip__ipaddrBroadcast)

#else
/* tlarsu: Removed the const qualifier removing cast. */
#define NTCPIP_IP_ADDR_ANY         &ntcpip__ipaddrAny
#define NTCPIP_IP_ADDR_BROADCAST   &ntcpip__ipaddrBroadcast

#endif

/* Definitions of the bits in an Internet address integer.

   On subnets, host and network parts are found according to
   the subnet mask, not these masks.  */

#define NTCPIP_IN_CLASSA(a)        ((((Uint32)(a)) & 0x80000000UL) == 0)
#define NTCPIP_IN_CLASSA_NET       0xff000000
#define NTCPIP_IN_CLASSA_NSHIFT    24
#define NTCPIP_IN_CLASSA_HOST      (0xffffffff & ~NTCPIP_IN_CLASSA_NET)
#define NTCPIP_IN_CLASSA_MAX       128

#define NTCPIP_IN_CLASSB(a)        ((((Uint32)(a)) & 0xc0000000UL) == 0x80000000UL)
#define NTCPIP_IN_CLASSB_NET       0xffff0000
#define NTCPIP_IN_CLASSB_NSHIFT    16
#define NTCPIP_IN_CLASSB_HOST      (0xffffffff & ~NTCPIP_IN_CLASSB_NET)
#define NTCPIP_IN_CLASSB_MAX       65536

#define NTCPIP_IN_CLASSC(a)        ((((Uint32)(a)) & 0xe0000000UL) == 0xc0000000UL)
#define NTCPIP_IN_CLASSC_NET       0xffffff00
#define NTCPIP_IN_CLASSC_NSHIFT    8
#define NTCPIP_IN_CLASSC_HOST      (0xffffffff & ~NTCPIP_IN_CLASSC_NET)

#define NTCPIP_IN_CLASSD(a)        (((Uint32)(a) & 0xf0000000UL) == 0xe0000000UL)
#define NTCPIP_IN_CLASSD_NET       0xf0000000          /* These ones aren't really */
#define NTCPIP_IN_CLASSD_NSHIFT    28                  /*   net and host fields, but */
#define NTCPIP_IN_CLASSD_HOST      0x0fffffff          /*   routing needn't know. */
#define NTCPIP_IN_MULTICAST(a)     NTCPIP_IN_CLASSD(a)

#define NTCPIP_IN_EXPERIMENTAL(a)  (((Uint32)(a) & 0xf0000000UL) == 0xf0000000UL)
#define NTCPIP_IN_BADCLASS(a)      (((Uint32)(a) & 0xf0000000UL) == 0xf0000000UL)

#define NTCPIP_IN_LOOPBACKNET      127                 /* official! */

#define NTCPIP_IP4_ADDR(ipaddr, a,b,c,d) \
        (ipaddr)->addr = ntcpip_htonl(((Uint32)((a) & 0xff) << 24) | \
                               ((Uint32)((b) & 0xff) << 16) | \
                               ((Uint32)((c) & 0xff) << 8) | \
                                (Uint32)((d) & 0xff))

#define ntcpip_ipaddrSet(dest, src) (dest)->addr = \
                               ((void *)(src) == NULL? 0:\
                               (src)->addr)
/**
 * Determine if two address are on the same network.
 *
 * @arg addr1 IP address 1
 * @arg addr2 IP address 2
 * @arg mask network identifier mask
 * @return !0 if the network identifiers of both address match
 */
#define ntcpip_ipaddrNetCmp(addr1, addr2, mask) (((addr1)->addr & \
                                              (mask)->addr) == \
                                             ((addr2)->addr & \
                                              (mask)->addr))
#define ntcpip_ipaddrCmp(addr1, addr2) ((addr1)->addr == (addr2)->addr)

#define ntcpip_ipaddrIsAny(addr1) ((addr1) == NULL || (addr1)->addr == 0)

Uint8 ntcpip__ipaddrIsBroadcast(const struct ntcpip_ipAddr *, struct ntcpip__netif *);

#define ntcpip_ipaddrIsMulticast(addr1) (((addr1)->addr & ntcpip_ntohl(0xf0000000UL)) == ntcpip_ntohl(0xe0000000UL))

#define ntcpip_ipaddrIsLinkLocal(addr1) (((addr1)->addr & ntcpip_ntohl(0xffff0000UL)) == ntcpip_ntohl(0xa9fe0000UL))

#define ntcpip_ipaddrDebugPrint(debug, ipaddr) \
  NTCPIP__LWIP_DEBUGF(debug, ("%"U16_F".%"U16_F".%"U16_F".%"U16_F,              \
                      (void *)(ipaddr) != NULL ?                                  \
                      (Uint16)(ntcpip_ntohl((ipaddr)->addr) >> 24) & 0xff : 0,  \
                      (void *)(ipaddr) != NULL ?                                  \
                      (Uint16)(ntcpip_ntohl((ipaddr)->addr) >> 16) & 0xff : 0,  \
                      (void *)(ipaddr) != NULL ?                                  \
                      (Uint16)(ntcpip_ntohl((ipaddr)->addr) >> 8) & 0xff : 0,   \
                      (void *)(ipaddr) != NULL ?                                  \
                      (Uint16)ntcpip_ntohl((ipaddr)->addr) & 0xff : 0))

/* These are cast to Uint16, with the intent that they are often arguments
 * to printf using the U16_F format from cc.h. */
#define ntcpip_ipaddr1(ipaddr) ((Uint16)(ntcpip_ntohl((ipaddr)->addr) >> 24) & 0xff)
#define ntcpip_ipaddr2(ipaddr) ((Uint16)(ntcpip_ntohl((ipaddr)->addr) >> 16) & 0xff)
#define ntcpip_ipaddr3(ipaddr) ((Uint16)(ntcpip_ntohl((ipaddr)->addr) >> 8) & 0xff)
#define ntcpip_ipaddr4(ipaddr) ((Uint16)(ntcpip_ntohl((ipaddr)->addr)) & 0xff)

/**
 * Same as ntcpip__inetNtoa() but takes a struct ntcpip_ipAddr*
 */
#define ntcpip_ipNtoa(addr)  ((addr != NULL) ? ntcpip__inetNtoa(*((struct ntcpip__inAddr*)(addr))) : "NULL")

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_IP_ADDR_H__ */

