/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_ARCH_H__
#define __LWIP_ARCH_H__

#ifndef NTCPIP__LITTLE_ENDIAN
#define NTCPIP__LITTLE_ENDIAN 1234
#endif

#ifndef NTCPIP__BIG_ENDIAN
#define NTCPIP__BIG_ENDIAN 4321
#endif

#include "ntcpip/arch/cc.h"

/** Temporary: define format string for size_t if not defined in cc.h */
#ifndef SZT_F
#define SZT_F U32_F
#endif /* SZT_F */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NTCPIP__PACK_STRUCT_BEGIN
#define NTCPIP__PACK_STRUCT_BEGIN
#endif /* NTCPIP__PACK_STRUCT_BEGIN */

#ifndef NTCPIP__PACK_STRUCT_END
#define NTCPIP__PACK_STRUCT_END
#endif /* NTCPIP__PACK_STRUCT_END */

#ifndef NTCPIP__PACK_STRUCT_FIELD
#define NTCPIP__PACK_STRUCT_FIELD(x) x
#endif /* NTCPIP__PACK_STRUCT_FIELD */


#ifndef NTCPIP__LWIP_UNUSED_ARG
#define NTCPIP__LWIP_UNUSED_ARG(x) (void)x
#endif /* NTCPIP__LWIP_UNUSED_ARG */ 


#ifdef LWIP_PROVIDE_ERRNO

#define  NTCPIP__EPERM     1  /* Operation not permitted */
#define  NTCPIP__ENOENT     2  /* No such file or directory */
#define  NTCPIP__ESRCH     3  /* No such process */
#define  NTCPIP__EINTR     4  /* Interrupted system call */
#define  NTCPIP__EIO     5  /* I/O error */
#define  NTCPIP__ENXIO     6  /* No such device or address */
#define  NTCPIP__E2BIG     7  /* Arg list too long */
#define  NTCPIP__ENOEXEC     8  /* Exec format error */
#define  NTCPIP__EBADF     9  /* Bad file number */
#define  NTCPIP__ECHILD    10  /* No child processes */
#define  NTCPIP__EAGAIN    11  /* Try again */
#define  NTCPIP__ENOMEM    12  /* Out of memory */
#define  NTCPIP__EACCES    13  /* Permission denied */
#define  NTCPIP__EFAULT    14  /* Bad address */
#define  NTCPIP__ENOTBLK    15  /* Block device required */
#define  NTCPIP__EBUSY    16  /* Device or resource busy */
#define  NTCPIP__EEXIST    17  /* File exists */
#define  NTCPIP__EXDEV    18  /* Cross-device link */
#define  NTCPIP__ENODEV    19  /* No such device */
#define  NTCPIP__ENOTDIR    20  /* Not a directory */
#define  NTCPIP__EISDIR    21  /* Is a directory */
#define  NTCPIP__EINVAL    22  /* Invalid argument */
#define  NTCPIP__ENFILE    23  /* File table overflow */
#define  NTCPIP__EMFILE    24  /* Too many open files */
#define  NTCPIP__ENOTTY    25  /* Not a typewriter */
#define  NTCPIP__ETXTBSY    26  /* Text file busy */
#define  NTCPIP__EFBIG    27  /* File too large */
#define  NTCPIP__ENOSPC    28  /* No space left on device */
#define  NTCPIP__ESPIPE    29  /* Illegal seek */
#define  NTCPIP__EROFS    30  /* Read-only file system */
#define  NTCPIP__EMLINK    31  /* Too many links */
#define  NTCPIP__EPIPE    32  /* Broken pipe */
#define  NTCPIP__EDOM    33  /* Math argument out of domain of func */
#define  NTCPIP__ERANGE    34  /* Math result not representable */
#define  NTCPIP__EDEADLK    35  /* Resource deadlock would occur */
#define  NTCPIP__ENAMETOOLONG  36  /* File name too long */
#define  NTCPIP__ENOLCK    37  /* No record locks available */
#define  NTCPIP__ENOSYS    38  /* Function not implemented */
#define  NTCPIP__ENOTEMPTY  39  /* Directory not empty */
#define  NTCPIP__ELOOP    40  /* Too many symbolic links encountered */
#define  NTCPIP__EWOULDBLOCK  NTCPIP__EAGAIN  /* Operation would block */
#define  NTCPIP__ENOMSG    42  /* No message of desired type */
#define  NTCPIP__EIDRM    43  /* Identifier removed */
#define  NTCPIP__ECHRNG    44  /* Channel number out of range */
#define  NTCPIP__EL2NSYNC  45  /* Level 2 not synchronized */
#define  NTCPIP__EL3HLT    46  /* Level 3 halted */
#define  NTCPIP__EL3RST    47  /* Level 3 reset */
#define  NTCPIP__ELNRNG    48  /* Link number out of range */
#define  NTCPIP__EUNATCH    49  /* Protocol driver not attached */
#define  NTCPIP__ENOCSI    50  /* No CSI structure available */
#define  NTCPIP__EL2HLT    51  /* Level 2 halted */
#define  NTCPIP__EBADE    52  /* Invalid exchange */
#define  NTCPIP__EBADR    53  /* Invalid request descriptor */
#define  NTCPIP__EXFULL    54  /* Exchange full */
#define  NTCPIP__ENOANO    55  /* No anode */
#define  NTCPIP__EBADRQC    56  /* Invalid request code */
#define  NTCPIP__EBADSLT    57  /* Invalid slot */

#define  NTCPIP__EDEADLOCK  NTCPIP__EDEADLK

#define  NTCPIP__EBFONT    59  /* Bad font file format */
#define  NTCPIP__ENOSTR    60  /* Device not a stream */
#define  NTCPIP__ENODATA    61  /* No data available */
#define  NTCPIP__ETIME    62  /* Timer expired */
#define  NTCPIP__ENOSR    63  /* Out of streams resources */
#define  NTCPIP__ENONET    64  /* Machine is not on the network */
#define  NTCPIP__ENOPKG    65  /* Package not installed */
#define  NTCPIP__EREMOTE    66  /* Object is remote */
#define  NTCPIP__ENOLINK    67  /* Link has been severed */
#define  NTCPIP__EADV    68  /* Advertise error */
#define  NTCPIP__ESRMNT    69  /* Srmount error */
#define  NTCPIP__ECOMM    70  /* Communication error on send */
#define  NTCPIP__EPROTO    71  /* Protocol error */
#define  NTCPIP__EMULTIHOP  72  /* Multihop attempted */
#define  NTCPIP__EDOTDOT    73  /* RFS specific error */
#define  NTCPIP__EBADMSG    74  /* Not a data message */
#define  NTCPIP__EOVERFLOW  75  /* Value too large for defined data type */
#define  NTCPIP__ENOTUNIQ  76  /* Name not unique on network */
#define  NTCPIP__EBADFD    77  /* File descriptor in bad state */
#define  NTCPIP__EREMCHG    78  /* Remote address changed */
#define  NTCPIP__ELIBACC    79  /* Can not access a needed shared library */
#define  NTCPIP__ELIBBAD    80  /* Accessing a corrupted shared library */
#define  NTCPIP__ELIBSCN    81  /* .lib section in a.out corrupted */
#define  NTCPIP__ELIBMAX    82  /* Attempting to link in too many shared libraries */
#define  NTCPIP__ELIBEXEC  83  /* Cannot exec a shared library directly */
#define  NTCPIP__EILSEQ    84  /* Illegal byte sequence */
#define  NTCPIP__ERESTART  85  /* Interrupted system call should be restarted */
#define  NTCPIP__ESTRPIPE  86  /* Streams pipe error */
#define  NTCPIP__EUSERS    87  /* Too many users */
#define  NTCPIP__ENOTSOCK  88  /* Socket operation on non-socket */
#define  NTCPIP__EDESTADDRREQ  89  /* Destination address required */
#define  NTCPIP__EMSGSIZE  90  /* Message too long */
#define  NTCPIP__EPROTOTYPE  91  /* Protocol wrong type for socket */
#define  NTCPIP__ENOPROTOOPT  92  /* Protocol not available */
#define  NTCPIP__EPROTONOSUPPORT  93  /* Protocol not supported */
#define  NTCPIP__ESOCKTNOSUPPORT  94  /* Socket type not supported */
#define  NTCPIP__EOPNOTSUPP  95  /* Operation not supported on transport endpoint */
#define  NTCPIP__EPFNOSUPPORT  96  /* Protocol family not supported */
#define  NTCPIP__EAFNOSUPPORT  97  /* Address family not supported by protocol */
#define  NTCPIP__EADDRINUSE  98  /* Address already in use */
#define  NTCPIP__EADDRNOTAVAIL  99  /* Cannot assign requested address */
#define  NTCPIP__ENETDOWN  100  /* Network is down */
#define  NTCPIP__ENETUNREACH  101  /* Network is unreachable */
#define  NTCPIP__ENETRESET  102  /* Network dropped connection because of reset */
#define  NTCPIP__ECONNABORTED  103  /* Software caused connection abort */
#define  NTCPIP__ECONNRESET  104  /* Connection reset by peer */
#define  NTCPIP__ENOBUFS    105  /* No buffer space available */
#define  NTCPIP__EISCONN    106  /* Transport endpoint is already connected */
#define  NTCPIP__ENOTCONN  107  /* Transport endpoint is not connected */
#define  NTCPIP__ESHUTDOWN  108  /* Cannot send after transport endpoint shutdown */
#define  NTCPIP__ETOOMANYREFS  109  /* Too many references: cannot splice */
#define  NTCPIP__ETIMEDOUT  110  /* Connection timed out */
#define  NTCPIP__ECONNREFUSED  111  /* Connection refused */
#define  NTCPIP__EHOSTDOWN  112  /* Host is down */
#define  NTCPIP__EHOSTUNREACH  113  /* No route to host */
#define  NTCPIP__EALREADY  114  /* Operation already in progress */
#define  NTCPIP__EINPROGRESS  115  /* Operation now in progress */
#define  NTCPIP__ESTALE    116  /* Stale NFS file handle */
#define  NTCPIP__EUCLEAN    117  /* Structure needs cleaning */
#define  NTCPIP__ENOTNAM    118  /* Not a XENIX named type file */
#define  NTCPIP__ENAVAIL    119  /* No XENIX semaphores available */
#define  NTCPIP__EISNAM    120  /* Is a named type file */
#define  NTCPIP__EREMOTEIO  121  /* Remote I/O error */
#define  NTCPIP__EDQUOT    122  /* Quota exceeded */

#define  NTCPIP__ENOMEDIUM  123  /* No medium found */
#define  NTCPIP__EMEDIUMTYPE  124  /* Wrong medium type */


#define NTCPIP__ENSROK    0 /* DNS server returned answer with no data */
#define NTCPIP__ENSRNODATA  160 /* DNS server returned answer with no data */
#define NTCPIP__ENSRFORMERR 161 /* DNS server claims query was misformatted */
#define NTCPIP__ENSRSERVFAIL 162  /* DNS server returned general failure */
#define NTCPIP__ENSRNOTFOUND 163  /* Domain name not found */
#define NTCPIP__ENSRNOTIMP  164 /* DNS server does not implement requested operation */
#define NTCPIP__ENSRREFUSED 165 /* DNS server refused query */
#define NTCPIP__ENSRBADQUERY 166  /* Misformatted DNS query */
#define NTCPIP__ENSRBADNAME 167 /* Misformatted domain name */
#define NTCPIP__ENSRBADFAMILY 168 /* Unsupported address family */
#define NTCPIP__ENSRBADRESP 169 /* Misformatted DNS reply */
#define NTCPIP__ENSRCONNREFUSED 170 /* Could not contact DNS servers */
#define NTCPIP__ENSRTIMEOUT 171 /* Timeout while contacting DNS servers */
#define NTCPIP__ENSROF    172 /* End of file */
#define NTCPIP__ENSRFILE  173 /* Error reading file */
#define NTCPIP__ENSRNOMEM 174 /* Out of memory */
#define NTCPIP__ENSRDESTRUCTION 175 /* Application terminated lookup */
#define NTCPIP__ENSRQUERYDOMAINTOOLONG  176 /* Domain name is too long */
#define NTCPIP__ENSRCNAMELOOP 177 /* Domain name is too long */

#ifndef errno
extern int errno;
#endif

#endif /* LWIP_PROVIDE_ERRNO */

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_ARCH_H__ */

