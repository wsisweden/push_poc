/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#ifndef __LWIP_PBUF_H__
#define __LWIP_PBUF_H__

#include "ntcpip/opt.h"
#include "ntcpip/err.h"

#ifdef __cplusplus
extern "C" {
#endif

#define NTCPIP__PBUF_TRANSPORT_HLEN 20
#define NTCPIP__PBUF_IP_HLEN        20

typedef enum {
  NTCPIP_PBUF_TRANSPORT,
  NTCPIP_PBUF_IP,
  NTCPIP_PBUF_LINK,
  NTCPIP_PBUF_RAW
} ntcpip__PbufLayer;

typedef enum {
  NTCPIP_PBUF_RAM, /* pbuf data is stored in RAM */
  NTCPIP_PBUF_ROM, /* pbuf data is stored in ROM */
  NTCPIP_PBUF_REF, /* pbuf comes from the pbuf pool */
  NTCPIP_PBUF_POOL /* pbuf payload refers to RAM */
} ntcpip__PbufType;


/** indicates this packet's data should be immediately passed to the application */
#define NTCPIP__PBUF_FLAG_PUSH 0x01U

struct ntcpip_pbuf {
  /** next pbuf in singly linked pbuf chain */
  struct ntcpip_pbuf *next;

  /** pointer to the actual data in the buffer */
  void *payload;
  
  /**
   * total length of this buffer and all next buffers in chain
   * belonging to the same packet.
   *
   * For non-queue packet chains this is the invariant:
   * p->tot_len == p->len + (p->next? p->next->tot_len: 0)
   */
  Uint16 tot_len;
  
  /** length of this buffer */
  Uint16 len;  

  /** ntcpip__PbufType as Uint8 instead of enum to save space */
  Uint8 /*ntcpip__PbufType*/ type;

  /** misc flags */
  Uint8 flags;

  /**
   * the reference count always equals the number of pointers
   * that refer to this pbuf. This can be pointers from an application,
   * the stack itself, or pbuf->next pointers from a chain.
   */
  Uint16 ref;
  
};

/* Initializes the pbuf module. This call is empty for now, but may not be in future. */
#define ntcpip__pbufInit()

struct ntcpip_pbuf *ntcpip_pbufAlloc(ntcpip__PbufLayer l, Uint16 size, ntcpip__PbufType type);
void 		ntcpip_pbufRealloc(struct ntcpip_pbuf *p, Uint16 size);
Uint8 		ntcpip__pbufHeader(struct ntcpip_pbuf *p, Int16 header_size);
void 		ntcpip_pbufRef(struct ntcpip_pbuf *p);
void 		ntcpip__pbufRefChain(struct ntcpip_pbuf *p);
Uint8 		ntcpip_pbufFree(struct ntcpip_pbuf *p);
Uint8 		ntcpip__pbufClen(struct ntcpip_pbuf *p);
void 		ntcpip_pbufCat(struct ntcpip_pbuf *head, struct ntcpip_pbuf *tail);
void 		ntcpip__pbufChain(struct ntcpip_pbuf *head, struct ntcpip_pbuf *tail);
struct ntcpip_pbuf *ntcpip__pbufDechain(struct ntcpip_pbuf *p);
ntcpip_Err 	ntcpip_pbufCopy(struct ntcpip_pbuf *p_to, struct ntcpip_pbuf *p_from);
Uint16 		ntcpip_pbufCopyPartial(struct ntcpip_pbuf *p, void *dataptr, Uint16 len, Uint16 offset);
ntcpip_Err 	ntpcip__pbufTake(struct ntcpip_pbuf *buf, const void *dataptr, Uint16 len);
struct ntcpip_pbuf *ntcpip__pbufCoalesce(struct ntcpip_pbuf *p, ntcpip__PbufLayer layer);

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_PBUF_H__ */

