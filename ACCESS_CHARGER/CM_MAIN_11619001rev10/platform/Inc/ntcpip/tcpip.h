/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_TCPIP_H__
#define __LWIP_TCPIP_H__

#include "ntcpip/opt.h"

#if !NTCPIP__NO_SYS /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/api_msg.h"
#include "ntcpip/netifapi.h"
#include "ntcpip/pbuf.h"
#include "ntcpip/api.h"
#include "ntcpip/sys.h"
#include "ntcpip/netif.h"

#ifdef __cplusplus
extern "C" {
#endif

#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
/** The global semaphore to lock the stack. */
extern ntcpip__SysSema lock_tcpip_core;
#define NTCPIP__LOCK_CORE()     ntcpip__sysSemWait(lock_tcpip_core)
#define NTCPIP__UNLOCK_CORE()   ntcpip__sysSemSignal(lock_tcpip_core)
#define NTCPIP__APIMSG(m)       tcpip_apimsg_lock(m)
#define NTCPIP__APIMSG_ACK(m)
#define NTCPIP__NETIFAPI(m)     tcpip_netifapi_lock(m)
#define NTCPIP__NETIFAPI_ACK(m)
#else
#define NTCPIP__LOCK_CORE()
#define NTCPIP__UNLOCK_CORE()
#define NTCPIP__APIMSG(m)       ntcpip__tcpipApiMsg(m)
#define NTCPIP__APIMSG_ACK(m)   ntcpip__sysSemSignal(m->conn->op_completed)
#define NTCPIP__NETIFAPI(m)     ntcpip__tcpipNetifApi(m)
#define NTCPIP__NETIFAPI_ACK(m) ntcpip__sysSemSignal(m->sem)
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */

void ntcpip__tcpipInit(void (* tcpip_init_done)(void *), void *arg);

#if NTCPIP__LWIP_NETCONN
ntcpip_Err ntcpip__tcpipApiMsg(struct ntcpip__apiMsg *apimsg);
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
ntcpip_Err tcpip_apimsg_lock(struct ntcpip__apiMsg *apimsg);
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */
#endif /* NTCPIP__LWIP_NETCONN */

ntcpip_Err ntcpip_tcpipInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp);

#if NTCPIP__LWIP_NETIF_API
ntcpip_Err ntcpip__tcpipNetifApi(struct ntcpip__netifapiMsg *netifapimsg);
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
ntcpip_Err tcpip_netifapi_lock(struct ntcpip__netifapiMsg *netifapimsg);
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */
#endif /* NTCPIP__LWIP_NETIF_API */

ntcpip_Err ntcpip_tcpipCallbackWithBlock(void (*f)(void *ctx), void *ctx, Uint8 block);
#define ntcpip__tcpipCb(f, ctx)              ntcpip_tcpipCallbackWithBlock(f, ctx, 1)

/* free pbufs or heap memory from another context without blocking */
ntcpip_Err ntcpip_pbufFreeCallback(struct ntcpip_pbuf *p);
ntcpip_Err mem_free_callback(void *m);

ntcpip_Err ntcpip__tcpipTimeout(Uint32 msecs, ntcpip__sysTimeoutHandler h, void *arg);
ntcpip_Err ntcpip__tcpipUntimeout(ntcpip__sysTimeoutHandler h, void *arg);

enum ntcpip__msgType {
#if NTCPIP__LWIP_NETCONN
  NTCPIP__MSG_API,
#endif /* NTCPIP__LWIP_NETCONN */
  NTCPIP__MSG_INPKT,
#if NTCPIP__LWIP_NETIF_API
  NTCPIP__MSG_NETIFAPI,
#endif /* NTCPIP__LWIP_NETIF_API */
  NTCPIP__MSG_CALLBACK,
  NTCPIP__MSG_TIMEOUT,
  NTCPIP__MSG_UNTIMEOUT
};

struct ntcpip__msg {
  enum ntcpip__msgType type;
  ntcpip__SysSema *sem;
  union {
#if NTCPIP__LWIP_NETCONN
    struct ntcpip__apiMsg *apimsg;
#endif /* NTCPIP__LWIP_NETCONN */
#if NTCPIP__LWIP_NETIF_API
    struct ntcpip__netifapiMsg *netifapimsg;
#endif /* NTCPIP__LWIP_NETIF_API */
    struct {
      struct ntcpip_pbuf *p;
      struct ntcpip__netif *netif;
    } inp;
    struct {
      void (*f)(void *ctx);
      void *ctx;
    } cb;
    struct {
      Uint32 msecs;
      ntcpip__sysTimeoutHandler h;
      void *arg;
    } tmo;
  } msg;
};

#ifdef __cplusplus
}
#endif

#endif /* !NTCPIP__NO_SYS */

#endif /* __LWIP_TCPIP_H__ */

