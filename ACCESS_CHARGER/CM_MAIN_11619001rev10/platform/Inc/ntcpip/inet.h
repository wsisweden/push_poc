/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_INET_H__
#define __LWIP_INET_H__

#include "ntcpip/opt.h"

#ifdef __cplusplus
extern "C" {
#endif

/* For compatibility with BSD code */
struct ntcpip__inAddr {
  Uint32 s_addr;
};

#define NTCPIP_INADDR_NONE         ((Uint32)0xffffffffUL)  /* 255.255.255.255 */
#define NTCPIP_INADDR_LOOPBACK     ((Uint32)0x7f000001UL)  /* 127.0.0.1 */
#define NTCPIP_INADDR_ANY          ((Uint32)0x00000000UL)  /* 0.0.0.0 */
#define NTCPIP_INADDR_BROADCAST    ((Uint32)0xffffffffUL)  /* 255.255.255.255 */

Uint32 ntcpip__inetAddr(const char *cp);
int ntcpip__inetAton(const char *cp, struct ntcpip__inAddr *addr);
char *ntcpip__inetNtoa(struct ntcpip__inAddr addr); /* returns ptr to static buffer; not reentrant! */

#ifdef ntcpip_htons
#undef ntcpip_htons
#endif /* ntcpip_htons */
#ifdef ntcpip_htonl
#undef ntcpip_htonl
#endif /* ntcpip_htonl */
#ifdef ntcpip_ntohs
#undef ntcpip_ntohs
#endif /* ntcpip_ntohs */
#ifdef ntcpip_ntohl
#undef ntcpip_ntohl
#endif /* ntcpip_ntohl */

#ifndef NTCPIP__PLATFORM_BYTESWAP
#define NTCPIP__PLATFORM_BYTESWAP 0
#endif

#if NTCPIP__BYTE_ORDER == NTCPIP__BIG_ENDIAN
#define ntcpip_htons(x) (x)
#define ntcpip_ntohs(x) (x)
#define ntcpip_htonl(x) (x)
#define ntcpip_ntohl(x) (x)
#else /* NTCPIP__BYTE_ORDER != NTCPIP__BIG_ENDIAN */
#ifdef LWIP_PREFIX_BYTEORDER_FUNCS
/* workaround for naming collisions on some platforms */
#define ntcpip_htons lwip_htons
#define ntcpip_ntohs lwip_ntohs
#define ntcpip_htonl lwip_htonl
#define ntcpip_ntohl lwip_ntohl
#endif /* LWIP_PREFIX_BYTEORDER_FUNCS */
#if NTCPIP__PLATFORM_BYTESWAP
#define ntcpip_htons(x) NTCPIP__PLATFORM_HTONS(x)
#define ntcpip_ntohs(x) NTCPIP__PLATFORM_HTONS(x)
#define ntcpip_htonl(x) NTCPIP__PLATFORM_HTONL(x)
#define ntcpip_ntohl(x) NTCPIP__PLATFORM_HTONL(x)
#else /* NTCPIP__PLATFORM_BYTESWAP */
Uint16 ntcpip_htons(Uint16 x);
Uint16 ntcpip_ntohs(Uint16 x);
Uint32 ntcpip_htonl(Uint32 x);
Uint32 ntcpip_ntohl(Uint32 x);
#endif /* NTCPIP__PLATFORM_BYTESWAP */

#endif /* NTCPIP__BYTE_ORDER == NTCPIP__BIG_ENDIAN */

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_INET_H__ */

