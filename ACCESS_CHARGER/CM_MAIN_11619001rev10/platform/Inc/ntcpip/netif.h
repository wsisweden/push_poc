/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_NETIF_H__
#define __LWIP_NETIF_H__

#include "ntcpip/opt.h"

#define ENABLE_LOOPBACK (LWIP_NETIF_LOOPBACK || NTCPIP__LWIP_HAVE_LOOPIF)

#include "ntcpip/err.h"

#include "ntcpip/ip_addr.h"

#include "ntcpip/inet.h"
#include "ntcpip/pbuf.h"
#if NTCPIP__LWIP_DHCP
struct dhcp;
#endif
#if NTCPIP__LWIP_AUTOIP
struct autoip;
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Throughout this file, IP addresses are expected to be in
 * the same byte order as in IP_PCB. */

/** must be the maximum of all used hardware address lengths
    across all types of interfaces in use */
#define NTCPIP_NETIF_MAX_HWADDR_LEN 6U

/** TODO: define the use (where, when, whom) of netif flags */

/** whether the network interface is 'up'. this is
 * a software flag used to control whether this network
 * interface is enabled and processes traffic.
 */
#define NTCPIP_NETIF_UP           0x01U
/** if set, the netif has broadcast capability */
#define NTCPIP_NETIF_BROADCAST    0x02U
/** if set, the netif is one end of a point-to-point connection */
#define NTCPIP_NETIF_POINTTOPOINT 0x04U
/** if set, the interface is configured using DHCP */
#define NTCPIP_NETIF_DHCP         0x08U
/** if set, the interface has an active link
 *  (set by the network interface driver) */
#define NTCPIP_NETIF_LINK_UP      0x10U
/** if set, the netif is an device using ARP */
#define NTCPIP_NETIF_ETHARP       0x20U
/** if set, the netif has IGMP capability */
#define NTCPIP_NETIF_IGMP         0x40U

/** Generic data structure used for all lwIP network interfaces.
 *  The following fields should be filled in by the initialization
 *  function for the device driver: hwaddr_len, hwaddr[], mtu, flags */

struct ntcpip__netif {
  /** pointer to next in linked list */
  struct ntcpip__netif *next;

  /** IP address configuration in network byte order */
  struct ntcpip_ipAddr ip_addr;
  struct ntcpip_ipAddr netmask;
  struct ntcpip_ipAddr gw;

  /** This function is called by the network device driver
   *  to pass a packet up the TCP/IP stack. */
  ntcpip_Err (* input)(struct ntcpip_pbuf *p, struct ntcpip__netif *inp);
  /** This function is called by the IP module when it wants
   *  to send a packet on the interface. This function typically
   *  first resolves the hardware address, then sends the packet. */
  ntcpip_Err (* output)(struct ntcpip__netif *netif, struct ntcpip_pbuf *p,
       const struct ntcpip_ipAddr *ipaddr);
  /** This function is called by the ARP module when it wants
   *  to send a packet on the interface. This function outputs
   *  the pbuf as-is on the link medium. */
  ntcpip_Err (* linkoutput)(struct ntcpip__netif *netif, struct ntcpip_pbuf *p);
#if NTCPIP__LWIP_NETIF_STATUS_CALLBACK
  /** This function is called when the netif state is set to up or down
   */
  void (* status_callback)(struct ntcpip__netif *netif);
#endif /* NTCPIP__LWIP_NETIF_STATUS_CALLBACK */
#if NTCPIP__LWIP_NETIF_LINK_CALLBACK
  /** This function is called when the netif link is set to up or down
   */
  void (* link_callback)(struct ntcpip__netif *netif);
#endif /* NTCPIP__LWIP_NETIF_LINK_CALLBACK */
  /** This field can be set by the device driver and could point
   *  to state information for the device. */
  void *state;
#if NTCPIP__LWIP_DHCP
  /** the DHCP client state information for this netif */
  struct dhcp *dhcp;
#endif /* NTCPIP__LWIP_DHCP */
#if NTCPIP__LWIP_AUTOIP
  /** the AutoIP client state information for this netif */
  struct autoip *autoip;
#endif
#if NTCPIP__LWIP_NETIF_HOSTNAME
  /* the hostname for this netif, NULL is a valid value */
  char*  hostname;
#endif /* NTCPIP__LWIP_NETIF_HOSTNAME */
  /** maximum transfer unit (in bytes) */
  Uint16 mtu;
  /** number of bytes used in hwaddr */
  Uint8 hwaddr_len;
  /** link level hardware address of this interface */
  Uint8 hwaddr[NTCPIP_NETIF_MAX_HWADDR_LEN];
  /** flags (see NETIF_FLAG_ above) */
  Uint8 flags;
  /** descriptive abbreviation */
  char name[2];
  /** number of this interface */
  Uint8 num;
#if NTCPIP__LWIP_SNMP
  /** link type (from "snmp_ifType" enum from snmp.h) */
  Uint8 link_type;
  /** (estimate) link speed */
  Uint32 link_speed;
  /** timestamp at last change made (up/down) */
  Uint32 ts;
  /** counters */
  Uint32 ifinoctets;
  Uint32 ifinucastpkts;
  Uint32 ifinnucastpkts;
  Uint32 ifindiscards;
  Uint32 ifoutoctets;
  Uint32 ifoutucastpkts;
  Uint32 ifoutnucastpkts;
  Uint32 ifoutdiscards;
#endif /* NTCPIP__LWIP_SNMP */
#if NTCPIP__LWIP_IGMP
  /* This function could be called to add or delete a entry in the multicast filter table of the ethernet MAC.*/
  ntcpip_Err (*igmp_mac_filter)( struct ntcpip__netif *netif, const struct ntcpip_ipAddr *group, Uint8 action);
#endif /* NTCPIP__LWIP_IGMP */
#if NTCPIP__LWIP_NETIF_HWADDRHINT
  Uint8 *addr_hint;
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT */
#if ENABLE_LOOPBACK
  /* List of packets to be queued for ourselves. */
  struct ntcpip_pbuf *loop_first;
  struct ntcpip_pbuf *loop_last;
#if LWIP_LOOPBACK_MAX_PBUFS
  Uint16 loop_cnt_current;
#endif /* LWIP_LOOPBACK_MAX_PBUFS */
#endif /* ENABLE_LOOPBACK */
};

#if NTCPIP__LWIP_SNMP
#define NTCPIP_NETIF_INIT_SNMP(netif, type, speed) \
  /* use "snmp_ifType" enum from snmp.h for "type", snmp_ifType_ethernet_csmacd by example */ \
  netif->link_type = type;    \
  /* your link speed here (units: bits per second) */  \
  netif->link_speed = speed;  \
  netif->ts = 0;              \
  netif->ifinoctets = 0;      \
  netif->ifinucastpkts = 0;   \
  netif->ifinnucastpkts = 0;  \
  netif->ifindiscards = 0;    \
  netif->ifoutoctets = 0;     \
  netif->ifoutucastpkts = 0;  \
  netif->ifoutnucastpkts = 0; \
  netif->ifoutdiscards = 0
#else /* NTCPIP__LWIP_SNMP */
#define NTCPIP_NETIF_INIT_SNMP(netif, type, speed)
#endif /* NTCPIP__LWIP_SNMP */


/** The list of network interfaces. */
extern struct ntcpip__netif *ntcpip__netifList;
/** The default network interface. */
extern struct ntcpip__netif *ntcpip__netifDefault;

#define ntcpip__netifInit() /* Compatibility define, not init needed. */

struct ntcpip__netif *ntcpip__netifAdd(struct ntcpip__netif *netif, struct ntcpip_ipAddr *ipaddr, struct ntcpip_ipAddr *netmask,
      struct ntcpip_ipAddr *gw,
      void *state,
      ntcpip_Err (* init)(struct ntcpip__netif *netif),
      ntcpip_Err (* input)(struct ntcpip_pbuf *p, struct ntcpip__netif *netif));

void
ntcpip__netifSetAddr(struct ntcpip__netif *netif,struct ntcpip_ipAddr *ipaddr, struct ntcpip_ipAddr *netmask,
    struct ntcpip_ipAddr *gw);
void ntcpip__netifRemove(struct ntcpip__netif * netif);

/* Returns a network interface given its name. The name is of the form
   "et0", where the first two letters are the "name" field in the
   netif structure, and the digit is in the num field in the same
   structure. */
struct ntcpip__netif *ntcpip__netifFind(char *name);

void ntcpip__netifSetDefault(struct ntcpip__netif *netif);

/* tlarsu: added const qualifier to the ip address */
void ntcpip__netifSetIpaddr(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *ipaddr);
void ntcpip__netifSetNetmask(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *netmask);
void ntcpip__netifSetGw(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *gw);

void ntcpip__netifSetUp(struct ntcpip__netif *netif);
void ntcpip__netifSetDown(struct ntcpip__netif *netif);
Uint8 ntcpip__netifIsUp(struct ntcpip__netif *netif);

#if NTCPIP__LWIP_NETIF_STATUS_CALLBACK
/*
 * Set callback to be called when interface is brought up/down
 */
void netif_set_status_callback(struct ntcpip__netif *netif, void (* status_callback)(struct ntcpip__netif *netif));
#endif /* NTCPIP__LWIP_NETIF_STATUS_CALLBACK */

void ntcpip__netifSetLinkUp(struct ntcpip__netif *netif);
void ntcpip__netifSetLinkDown(struct ntcpip__netif *netif);
Uint8 ntcpip__netifIsLinkUp(struct ntcpip__netif *netif);

/*
 * Set callback to be called when link is brought up/down
 */

#if NTCPIP__LWIP_NETIF_LINK_CALLBACK
void ntcpip__netifSetLinkCallback(struct ntcpip__netif *netif, void (* link_callback)(struct ntcpip__netif *netif));
#endif /* NTCPIP__LWIP_NETIF_LINK_CALLBACK */

#ifdef __cplusplus
}
#endif

#if ENABLE_LOOPBACK
ntcpip_Err netif_loop_output(struct ntcpip__netif *netif, struct ntcpip_pbuf *p, struct ntcpip_ipAddr *dest_ip);
void netif_poll(struct ntcpip__netif *netif);
#if !LWIP_NETIF_LOOPBACK_MULTITHREADING
void netif_poll_all(void);
#endif /* !LWIP_NETIF_LOOPBACK_MULTITHREADING */
#endif /* ENABLE_LOOPBACK */

#endif /* __LWIP_NETIF_H__ */

