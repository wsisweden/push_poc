/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		sys_arch.h
*
*	\ingroup	NTCPIP
*
*	\brief		Public declarations of the OS emulation layer.
*
*	\details
*
*	\note
*
*	\version	09-03-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef SYSARCH_H_INCLUDED
#define SYSARCH_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define NTCPIP__SYS_MBOX_NULL	NULL	/**< Invalid mailbox pointer.		*/
#define NTCPIP__SYS_SEM_NULL	NULL	/**< Invalid semaphore pointer.		*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*
 * The semaphore, mailbox and thread types does not need to be understood
 * outside of ntcpip_plat.c. The structs are only forward declared here.
 */

struct ntcpip__sysSema;
struct ntcpip__sysMbox;
struct ntcpip__sysThread;

typedef	struct ntcpip__sysSema *	ntcpip__SysSema;
typedef	struct ntcpip__sysMbox *	ntcpip__SysMbox;
typedef	struct ntcpip__sysThread *	ntcpip__SysThread;

/**
 * Pool status information.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8					freeCount;	/**< Number of free elements.		*/
	Uint8 					lowestCount;/**< Lowest number of free elements.*/
	Uint8					totalCount;	/**< Total number of elements.		*/
} ntcpip_SysPoolStat;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Mailbox space information.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8 					mboxSize;	/**< Size of smallest mailbox.		*/
	Uint8 					mboxSpace;	/**< Free space in the mbox with
										 * the least free space.			*/
	Uint8 					mboxLowSpace;/**< The least space there has been
										 * in any mbox.						*/
} ntcpip_SysMboxStat;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * NTCPIP sys arch layer status.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	ntcpip_SysMboxStat		mboxStatus;	/**< Mailbox free space info.		*/
	ntcpip_SysPoolStat		mboxPool;	/**< Mailbox pool status.			*/
	ntcpip_SysPoolStat		semaPool;	/**< Semaphore pool status.			*/
} ntcpip_SysStatus;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
