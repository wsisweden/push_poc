/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		sys_arch.h
*
*	\ingroup	NTCPIP
*
*	\brief		Target specific declarations for LwIP.
*
*	\details
*
*	\note
*
*	\version	09-03-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef CC_H_INCLUDED
#define CC_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "mem.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define NTCPIP__USE_PLAT_MEM	1

/**
 * Specifies the target endiannes.
 */

#if (TARGET_SELECTED & TARGET_ARM7) == TARGET_ARM7
#define NTCPIP__BYTE_ORDER 				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & TARGET_DOXY) == TARGET_DOXY
#define NTCPIP__BYTE_ORDER				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & TARGET_W32) == TARGET_W32
#define NTCPIP__BYTE_ORDER				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & TARGET_AVR) == TARGET_AVR
#define NTCPIP__BYTE_ORDER 				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & (TARGET_CM0 | TARGET_CM3 | TARGET_CM4)) != 0
#define NTCPIP__BYTE_ORDER 				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & TARGET_MM7) == TARGET_MM7
#define NTCPIP__BYTE_ORDER 				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & TARGET_SM4) == TARGET_SM4
#define NTCPIP__BYTE_ORDER 				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & TARGET_S4F4) == TARGET_S4F4
#define NTCPIP__BYTE_ORDER 				NTCPIP__LITTLE_ENDIAN

#elif (TARGET_SELECTED & TARGET_S3F2) == TARGET_S3F2
#define NTCPIP__BYTE_ORDER 				NTCPIP__LITTLE_ENDIAN

#else
#error Target specific LwIP settings has not been made for this target.

#endif

/**
 * \name	deb_logN formatters
 *
 * \brief	Selects which deb_logN format specifier should be used for each
 * 			variable type.
 */

/*@{*/
#define U16_F "hu"						/**< Uint16 format specifier		*/
#define S16_F "hd"						/**< Int16 format specifier			*/
#define X16_F "hx"						/**< ?								*/
#define U32_F "lu"						/**< Uint32	format specifier		*/
#define S32_F "ld"						/**< Int32 format specifier			*/
#define X32_F "lx"						/**< ?								*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/


/**
 * This macro is used as assert in LwIP. It will call the project_assert
 * directly. The file and line number is not provided, instead a description
 * string is provided.
 */

#ifndef NDEBUG
#define NTCPIP__ASSERT(x_)				project_assert(x_, 0)
#else
#define NTCPIP__ASSERT(x_)
#endif

/**
 * This macro is used by LwIP to print out debug information. What kind of
 * information will be written can be selected in lwipopts.h.
 */

#define NTCPIP__PLATFORM_DIAG(x)	\
{									\
	deb_logN x;						\
}

/**
 * \name	Struct packing macros
 *
 * \brief	The following macros are used by LwIP in each struct that must be
 * 			packed.
 */

/*@{*/
#if TARGET_SELECTED & TARGET_GNU
#define NTCPIP__PACK_STRUCT_STRUCT		__attribute__((packed))

#elif TARGET_SELECTED & TARGET_IAR
#define NTCPIP__PACK_STRUCT_BEGIN		_Pragma("pack(1)")
#define NTCPIP__PACK_STRUCT_STRUCT
#define NTCPIP__PACK_STRUCT_END			_Pragma("pack()")

#elif (TARGET_SELECTED & TARGET_W32) == TARGET_W32
#define NTCPIP__PACK_STRUCT_STRUCT		

#elif (TARGET_SELECTED & TARGET_DOXY) == TARGET_DOXY
#define NTCPIP__PACK_STRUCT_STRUCT		

#elif (TARGET_SELECTED & TARGET_AVR) == TARGET_AVR
#define NTCPIP__PACK_STRUCT_STRUCT

#else
#error Struct packing defines have not been declared for this target.
#endif
/*@}*/

#if NTCPIP__USE_PLAT_MEM == 1
/**
 * \name	Memory handler mapping
 *
 * \brief	All LwIP memory handling is mapped to the platform MEM.
 */

/*@{*/
#define ntcpip__memFree(x) 				mem_free(x)
#define ntcpip__memMalloc(x) 			mem_alloc(ntcpip__memCat, x)
#define ntcpip__memCalloc(x, y) 		NotImplemented
#define ntcpip__memRealloc(x, size) 	mem_realloc(x, size)
/*@}*/
#endif

/**
 * \name	Byte swap optimizations
 */

/*@{*/
#if (TARGET_SELECTED & TARGET_CM3)
#define NTCPIP__PLATFORM_BYTESWAP 		1
#define NTCPIP__PLATFORM_HTONS(x) 		__REV16(x)
#define NTCPIP__PLATFORM_HTONL(x) 		__REV(x)

#elif (TARGET_SELECTED & TARGET_SM4)
#define NTCPIP__PLATFORM_BYTESWAP 		1
#define NTCPIP__PLATFORM_HTONS(x) 		__REV16(x)
#define NTCPIP__PLATFORM_HTONL(x) 		__REV(x)

#else
#define NTCPIP__PLATFORM_BYTESWAP 		0
/*
 *	TODO:	It would be great to use the lib_reverse32 and lib_reverse16 
 *			macros here. They do not return the modified value so it might not
 *			work. Wrapping them in a function might defeat the purpose.
 */
#endif
/*@}*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef Uint16				ntcpip__SysProt;	/**< Protection level type.	*/
typedef Uint32				ntcpip__MemPtr;		/**< Mem pointer type.		*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG
extern void					project_assert(char const_P *, Uint16);
#endif

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern void ** ntcpip__memCat;

/***************************************************************//** \endcond */

#endif
