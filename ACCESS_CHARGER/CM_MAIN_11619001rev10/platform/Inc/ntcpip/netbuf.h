/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_NETBUF_H__
#define __LWIP_NETBUF_H__

#include "ntcpip/opt.h"
#include "ntcpip/pbuf.h"
#include "ntcpip/ip_addr.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ntcpip__netbuf {
  struct ntcpip_pbuf *p, *ptr;
  const struct ntcpip_ipAddr *addr;
  Uint16 port;
#if NTCPIP__NETBUF_RECVINFO
  struct ntcpip_ipAddr *toaddr;
  Uint16 toport;
#endif /* NTCPIP__NETBUF_RECVINFO */
};

/* Network buffer functions: */
struct ntcpip__netbuf * ntcpip_netbufNew(void);
void              ntcpip_netbufDelete(struct ntcpip__netbuf *buf);
void *            ntcpip_netbufAlloc(struct ntcpip__netbuf *buf, Uint16 size);
void              ntcpip_netbufFree(struct ntcpip__netbuf *buf);
ntcpip_Err        ntcpip__netbufRef(struct ntcpip__netbuf *buf,
           const void *dataptr, Uint16 size);
void              ntcpip__netbufChain(struct ntcpip__netbuf *head,
           struct ntcpip__netbuf *tail);

Uint16            ntcpip__netbufLen(struct ntcpip__netbuf *buf);
ntcpip_Err        ntcpip_netbufData(struct ntcpip__netbuf *buf,
           void **dataptr, Uint16 *len);
Int8              ntcpip_netbufNext(struct ntcpip__netbuf *buf);
void              ntcpip__netbufFirst(struct ntcpip__netbuf *buf);


#define ntcpip_netbufCopyPartial(buf, dataptr, len, offset) \
  ntcpip_pbufCopyPartial((buf)->p, (dataptr), (len), (offset))
#define ntcpip_netbufCopy(buf,dataptr,len) ntcpip_netbufCopyPartial(buf, dataptr, len, 0)
#define ntcpip_netbufTake(buf, dataptr, len) ntpcip__pbufTake((buf)->p, dataptr, len)
#define ntcpip_netbufLen(buf)              ((buf)->p->tot_len)
#define ntcpip_netbufFromAddr(buf)         ((buf)->addr)
#define ntcpip_netbufFromPort(buf)         ((buf)->port)
#if NTCPIP__NETBUF_RECVINFO
#define netbuf_destaddr(buf)         ((buf)->toaddr)
#define netbuf_destport(buf)         ((buf)->toport)
#endif /* NTCPIP__NETBUF_RECVINFO */

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_NETBUF_H__ */

