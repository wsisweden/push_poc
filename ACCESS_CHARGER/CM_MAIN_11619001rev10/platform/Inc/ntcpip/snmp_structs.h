/**
 * @file
 * Generic MIB tree structures.
 *
 * @todo namespace prefixes
 */

/*
 * Copyright (c) 2006 Axon Digital Design B.V., The Netherlands.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Christiaan Simons <christiaan.simons@axon.tv>
 */

#ifndef __LWIP_SNMP_STRUCTS_H__
#define __LWIP_SNMP_STRUCTS_H__

#include "ntcpip/opt.h"


#include "ntcpip/snmp.h"

#if NTCPIP__SNMP_PRIVATE_MIB
#include "private_mib.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* MIB object instance */
#define MIB_OBJECT_NONE 	0

/**
 * tlarsu:	This seems to mean scalar object.
 */
#define MIB_OBJECT_SCALAR 	1

/**
 * tlarsu:	This seems to mean tabular object.
 */
#define MIB_OBJECT_TAB 		2

/* MIB object access */
#define MIB_OBJECT_READ_ONLY 		0
#define MIB_OBJECT_READ_WRITE 		1
#define MIB_OBJECT_WRITE_ONLY 		2
#define MIB_OBJECT_NOT_ACCESSIBLE	3

/** object definition returned by (get_object_def)() */
struct obj_def
{
  /** MIB_OBJECT_NONE (0), MIB_OBJECT_SCALAR (1), MIB_OBJECT_TAB (2) */
  Uint8 instance;

  /** 0 read-only, 1 read-write, 2 write-only, 3 not-accessible */
  Uint8 access;

  /** ASN type for this object */
  Uint8 asn_type;

  /** value length (host length) */
  Uint16 v_len;

  /** length of instance part of supplied object identifier */
  Uint8  id_inst_len;

  /** instance part of supplied object identifier */
  Int32 *id_inst_ptr;
};

struct snmp_name_ptr
{
  Uint8 					ident_len;
  Int32 *					ident;
};

/** MIB const scalar (.0) node */
#define MIB_NODE_SC 0x01
/** MIB const array node */
#define MIB_NODE_AR 0x02
/** MIB array node (mem_malloced from RAM) */
#define MIB_NODE_RA 0x03
/** MIB list root node (mem_malloced from RAM) */
#define MIB_NODE_LR 0x04
/** MIB node for external objects */
#define MIB_NODE_EX 0x05

/**
 * node "base class" layout, the mandatory fields for a node
 */
struct mib_node {
  /** returns struct obj_def for the given object identifier */
  void (*					get_object_def)(Uint8 ident_len, Int32 *ident, struct obj_def *od);
  /** returns object value for the given object identifier,
     @note the caller must allocate at least len bytes for the value */
  void (*					get_value)(struct obj_def *od, Uint16 len, void *value);
  /** tests length and/or range BEFORE setting */
  Uint8 (*					set_test)(struct obj_def *od, Uint16 len, void *value);
  /** sets object value, only to be called when set_test()  */
  void (*					set_value)(struct obj_def *od, Uint16 len, void *value);
  /** One out of MIB_NODE_AR, MIB_NODE_LR or MIB_NODE_EX */
  const Uint8 				node_type;
  /* array or max list length */
  const Uint16 				maxlength;
};

/**
 * derived node for scalars .0 index
 */
typedef struct mib_node mib_scalar_node;

/**
 * derived node, points to a fixed size const array
 * of sub-identifiers plus a 'child' pointer
 */
struct mib_array_node {
	/* inherited "base class" members */
	void (* const 			get_object_def)(Uint8 ident_len, Int32 *ident, struct obj_def *od);
	void (* const			get_value)(struct obj_def *od, Uint16 len, void *value);
	Uint8 (*				set_test)(struct obj_def *od, Uint16 len, void *value);
	void (*					set_value)(struct obj_def *od, Uint16 len, void *value);

	const Uint8 			node_type;
	const Uint16 			maxlength;

	/* Additional struct members */
	Int32 const_P *	objid;
	struct mib_node const_P * const *nptr;
};

/**
 * derived node, points to a fixed size mem_malloced array of sub-identifiers
 * plus a 'child' pointer
 */
struct mib_ram_array_node
{
  /* inherited "base class" members */
  void (*					get_object_def)(Uint8 ident_len, Int32 *ident, struct obj_def *od);
  void (*					get_value)(struct obj_def *od, Uint16 len, void *value);
  Uint8 (*					set_test)(struct obj_def *od, Uint16 len, void *value);
  void (*					set_value)(struct obj_def *od, Uint16 len, void *value);

  Uint8 					node_type;
  Uint16 					maxlength;

  /* Additional struct members */
  Int32 *					objid;
  struct mib_node **		nptr;
};



struct mib_list_node
{
  struct mib_list_node *	prev;
  struct mib_list_node *	next;
  Int32 					objid;
  struct mib_node *			nptr;
};


/**
 * derived node, points to a doubly linked list of sub-identifiers plus a
 * 'child' pointer
 */
struct mib_list_rootnode
{
  /* inherited "base class" members */
  void (*					get_object_def)(Uint8 ident_len, Int32 *ident, struct obj_def *od);
  void (*					get_value)(struct obj_def *od, Uint16 len, void *value);
  Uint8 (*					set_test)(struct obj_def *od, Uint16 len, void *value);
  void (*					set_value)(struct obj_def *od, Uint16 len, void *value);

  Uint8 					node_type;
  Uint16 					maxlength;

  /* Additional struct members */
  struct mib_list_node *	head;
  struct mib_list_node *	tail;
  /* counts list nodes in list  */
  Uint16 					count;
};


/**
 * derived node, has access functions for mib object in external memory or
 * device using 'tree_level' and 'idx', with a range 0 .. (level_length() - 1)
 */
struct mib_external_node
{
  /* inherited "base class" members */
  void (*		get_object_def)(Uint8 ident_len, Int32 *ident, struct obj_def *od);
  void (*		get_value)(struct obj_def *od, Uint16 len, void *value);
  Uint8 (*		set_test)(struct obj_def *od, Uint16 len, void *value);
  void (*		set_value)(struct obj_def *od, Uint16 len, void *value);

  Uint8			node_type;
  Uint16 		maxlength;

  /* Additional struct members */

  /** points to an external (in memory) record of some sort of addressing
      information, passed to and interpreted by the functions below */
  void const_P * addr_inf;
  /** tree levels under this node */
  Uint8 		tree_levels;
  /** number of objects at this level */
  Uint16 (*		level_length)(void* addr_inf, Uint8 level);
  /** compares object sub identifier with external id
      return zero when equal, nonzero when unequal */
  Int32 (*		ident_cmp)(void* addr_inf, Uint8 level, Uint16 idx, Int32 sub_id);
  void (*		get_objid)(void* addr_inf,Uint8 level,Uint16 idx,Int32 * sub_id);

  /* async Questions */
  void (*		get_object_def_q)(void* addr_inf, Uint8 rid, Uint8 ident_len, Int32 *ident);
  void (*		get_value_q)(Uint8 rid, struct obj_def *od);
  void (*		set_test_q)(Uint8 rid, struct obj_def *od);
  void (*		set_value_q)(Uint8 rid, struct obj_def *od, Uint16 len, void *value);

  /* async Answers */
  void (*		get_object_def_a)(Uint8 rid, Uint8 ident_len, Int32 *ident, struct obj_def *od);
  void (*		get_value_a)(Uint8 rid, struct obj_def *od, Uint16 len, void *value);
  Uint8 (*		set_test_a)(Uint8 rid, struct obj_def *od, Uint16 len, void *value);
  void (*		set_value_a)(Uint8 rid, struct obj_def *od, Uint16 len, void *value);

  /*
   * async Panic Close (agent returns error reply,
   * e.g. used for external transaction cleanup)
   */
  void (*		get_object_def_pc)(Uint8 rid, Uint8 ident_len, Int32 *ident);
  void (*		get_value_pc)(Uint8 rid, struct obj_def *od);
  void (*		set_test_pc)(Uint8 rid, struct obj_def *od);
  void (*		set_value_pc)(Uint8 rid, struct obj_def *od);
};

/*
 * export MIB tree from mib2.c
 */
extern const struct mib_array_node internet;

/* dummy function pointers for non-leaf MIB nodes from mib2.c */
void 	ntcpip_snmpNoleafsGetObjectDef(Uint8 ident_len, Int32 *ident, struct obj_def *od);
void 	ntcpip_snmpNoleafsGetValue(struct obj_def *od, Uint16 len, void *value);
Uint8 	ntcpip_snmpNoleafsSetTest(struct obj_def *od, Uint16 len, void *value);
void 	ntcpip_snmpNoleafsSetValue(struct obj_def *od, Uint16 len, void *value);

void 	ntcpip__snmpOidtoip(Int32 *ident, struct ntcpip_ipAddr *ip);
void 	ntcpip__snmpIptooid(struct ntcpip_ipAddr *ip, Int32 *ident);
void 	ntcpip__snmpIfindextonetif(Int32 ifindex, struct ntcpip__netif **netif);
void 	ntcpip__snmpNetiftoifindex(struct ntcpip__netif *netif, Int32 *ifidx);

struct mib_list_node*	ntcpip__snmpMibLnAlloc(Int32 id);
void 	ntcpip__snmpMibLnFree(struct mib_list_node *ln);
struct mib_list_rootnode*	ntcpip__snmpMibLrnAlloc(void);
void 	ntcpip__snmpMibLrnFree(struct mib_list_rootnode *lrn);

Int8 	ntcpip__snmpMibNodeInsert(struct mib_list_rootnode *rn, Int32 objid, struct mib_list_node **insn);
Int8 	ntcpip__snmpMibNodeFind(struct mib_list_rootnode *rn, Int32 objid, struct mib_list_node **fn);
struct mib_list_rootnode *	ntcpip__snmpMibNodeDelete(struct mib_list_rootnode *rn, struct mib_list_node *n);

struct mib_node*	ntcpip__snmpSearchTree(struct mib_node *node, Uint8 ident_len, Int32 *ident, struct snmp_name_ptr *np);
struct mib_node*	ntcpip__snmpExpandTree(struct mib_node *node, Uint8 ident_len, Int32 *ident, struct snmp_obj_id *oidret);
Uint8 	ntcpip__snmpIsoPrefixTst(Uint8 ident_len, Int32 *ident);
Uint8 	ntcpip__snmpIsoPrefixExpand(Uint8 ident_len, Int32 *ident, struct snmp_obj_id *oidret);

#ifdef __cplusplus
}
#endif

//#endif /* NTCPIP__LWIP_SNMP */

#endif /* __LWIP_SNMP_STRUCTS_H__ */

