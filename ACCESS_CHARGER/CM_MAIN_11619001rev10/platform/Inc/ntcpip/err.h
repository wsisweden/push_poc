/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_ERR_H__
#define __LWIP_ERR_H__

#include "ntcpip/opt.h"
#include "ntcpip/arch.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Define LWIP_ERR_T in cc.h if you want to use
 *  a different type for your platform (must be signed). */
#ifdef LWIP_ERR_T
typedef LWIP_ERR_T ntcpip_Err;
#else /* LWIP_ERR_T */
 typedef Int8 ntcpip_Err;
#endif /* LWIP_ERR_T*/

/* Definitions for error constants. */

#define NTCPIP_ERR_OK          0    /* No error, everything OK. */
#define NTCPIP_ERR_MEM        -1    /* Out of memory error.     */
#define NTCPIP_ERR_BUF        -2    /* Buffer error.            */
#define NTCPIP_ERR_TIMEOUT    -3    /* Timeout.                 */
#define NTCPIP_ERR_RTE        -4    /* Routing problem.         */

#define NTCPIP_ERR_IS_FATAL(e) ((e) < NTCPIP_ERR_RTE)

#define NTCPIP_ERR_ABRT       -5    /* Connection aborted.      */
#define NTCPIP_ERR_RST        -6    /* Connection reset.        */
#define NTCPIP_ERR_CLSD       -7    /* Connection closed.       */
#define NTCPIP_ERR_CONN       -8    /* Not connected.           */

#define NTCPIP_ERR_VAL        -9    /* Illegal value.           */

#define NTCPIP_ERR_ARG        -10   /* Illegal argument.        */

#define NTCPIP_ERR_USE        -11   /* Address in use.          */

#define NTCPIP_ERR_IF         -12   /* Low-level netif error    */
#define NTCPIP_ERR_ISCONN     -13   /* Already connected.       */

#define NTCPIP_ERR_INPROGRESS -14   /* Operation in progress    */


#ifdef LWIP_DEBUG
extern const char *ntcpip__lwipStrErr(ntcpip_Err err);
#else
#define ntcpip__lwipStrErr(x) ""
#endif /* LWIP_DEBUG */

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_ERR_H__ */

