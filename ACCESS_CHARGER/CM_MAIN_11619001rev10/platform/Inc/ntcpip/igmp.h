/*
 * Copyright (c) 2002 CITEL Technologies Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met: 
 * 1. Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 3. Neither the name of CITEL Technologies Ltd nor the names of its contributors 
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY CITEL TECHNOLOGIES AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED.  IN NO EVENT SHALL CITEL TECHNOLOGIES OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. 
 *
 * This file is a contribution to the lwIP TCP/IP stack.
 * The Swedish Institute of Computer Science and Adam Dunkels
 * are specifically granted permission to redistribute this
 * source code.
*/

#ifndef __LWIP_IGMP_H__
#define __LWIP_IGMP_H__

#include "ntcpip/opt.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "ntcpip/pbuf.h"

#if NTCPIP__LWIP_IGMP /* don't build if not configured for use in lwipopts.h */

#ifdef __cplusplus
extern "C" {
#endif

/* 
 * IGMP constants
 */
#define NTCPIP__IP_PROTO_IGMP                  2
#define NTCPIP__IGMP_TTL                       1
#define NTCPIP__IGMP_MINLEN                    8
#define NTCPIP__ROUTER_ALERT                   0x9404
#define NTCPIP__ROUTER_ALERTLEN                4

/*
 * IGMP message types, including version number.
 */
#define NTCPIP__IGMP_MEMB_QUERY				0x11 /* Membership query         */
#define NTCPIP__IGMP_V1_MEMB_REPORT			0x12 /* Ver. 1 membership report */
#define NTCPIP__IGMP_V2_MEMB_REPORT			0x16 /* Ver. 2 membership report */
#define NTCPIP__IGMP_LEAVE_GROUP			0x17 /* Leave-group message      */

/* IGMP timer */
#define NTCPIP__IGMP_TMR_INTERVAL              100 /* Milliseconds */
#define NTCPIP__IGMP_V1_DLY_MEMBER_TMR   (1000/NTCPIP__IGMP_TMR_INTERVAL)
#define NTCPIP__IGMP_JOIN_DLY_MEMBER_TMR (500 /NTCPIP__IGMP_TMR_INTERVAL)

/* MAC Filter Actions */
#define NTCPIP_IGMP_DEL_MAC				0
#define NTCPIP_IGMP_ADD_MAC				1

/* Group  membership states */
#define NTCPIP__IGMP_GROUP_NON_MEMBER	0
#define NTCPIP__IGMP_GROUP_DLY_MEMBER	1
#define NTCPIP__IGMP_GROUP_IDLE_MEMBER	2

/*
 * IGMP packet format.
 */
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct ntcpip__igmpMsg {
 NTCPIP__PACK_STRUCT_FIELD(Uint8           igmp_msgtype);
 NTCPIP__PACK_STRUCT_FIELD(Uint8           igmp_maxresp);
 NTCPIP__PACK_STRUCT_FIELD(Uint16          igmp_checksum);
 NTCPIP__PACK_STRUCT_FIELD(struct ntcpip_ipAddr igmp_group_address);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

/* 
 * now a group structure - there is
 * a list of groups for each interface
 * these should really be linked from the interface, but
 * if we keep them separate we will not affect the lwip original code
 * too much
 * 
 * There will be a group for the all systems group address but this 
 * will not run the state machine as it is used to kick off reports
 * from all the other groups
 */

struct ntcpip__igmpGroup {
  struct ntcpip__igmpGroup *next;
  struct ntcpip__netif      *interface;
  struct ntcpip_ipAddr     group_address;
  Uint8               last_reporter_flag; /* signifies we were the last person to report */
  Uint8               group_state;
  Uint16              timer;
  Uint8               use; /* counter of simultaneous uses */
};


/*  Prototypes */
void   ntcpip__igmpInit(void);

ntcpip_Err  ntcpip__igmpStart( struct ntcpip__netif *netif);

ntcpip_Err  ntcpip__igmpStop( struct ntcpip__netif *netif);

void   ntcpip__igmpReportGroups( struct ntcpip__netif *netif);

struct ntcpip__igmpGroup *ntcpip__igmpLookforGroup( struct ntcpip__netif *ifp, const struct ntcpip_ipAddr *addr);

struct ntcpip__igmpGroup *ntcpip__igmpLookupGroup( struct ntcpip__netif *ifp, const struct ntcpip_ipAddr *addr);

ntcpip_Err  ntcpip__igmpRemoveGroup( struct ntcpip__igmpGroup *group);

void   ntcpip__igmpInput( struct ntcpip_pbuf *p, struct ntcpip__netif *inp, struct ntcpip_ipAddr *dest);

ntcpip_Err  ntcpip_igmpJoingroup(const struct ntcpip_ipAddr *ifaddr, const struct ntcpip_ipAddr *groupaddr);

ntcpip_Err  ntcpip__igmpLeavegroup( struct ntcpip_ipAddr *ifaddr, struct ntcpip_ipAddr *groupaddr);

void   ntcpip__igmpTmr(void);

void   ntcpip__igmpTimeout( struct ntcpip__igmpGroup *group);

void   ntcpip__igmpStartTimer( struct ntcpip__igmpGroup *group, Uint8 max_time);

void   ntcpip__igmpStopTimer( struct ntcpip__igmpGroup *group);

void   ntcpip__igmpDelayingMember( struct ntcpip__igmpGroup *group, Uint8 maxresp);

ntcpip_Err  ntcpip__igmpIpOutputIf( struct ntcpip_pbuf *p, struct ntcpip_ipAddr *src, struct ntcpip_ipAddr *dest, Uint8 ttl, Uint8 proto, struct ntcpip__netif *netif);

void   ntcpip__igmpSend( struct ntcpip__igmpGroup *group, Uint8 type);

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_IGMP */

#endif /* __LWIP_IGMP_H__ */

