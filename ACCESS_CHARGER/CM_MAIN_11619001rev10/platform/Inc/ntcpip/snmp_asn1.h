/**
 * @file
 * Abstract Syntax Notation One (ISO 8824, 8825) codec.
 */
 
/*
 * Copyright (c) 2006 Axon Digital Design B.V., The Netherlands.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Christiaan Simons <christiaan.simons@axon.tv>
 */

#ifndef __LWIP_SNMP_ASN1_H__
#define __LWIP_SNMP_ASN1_H__

#include "ntcpip/opt.h"
#include "ntcpip/err.h"
#include "ntcpip/pbuf.h"
#include "ntcpip/snmp.h"

//#if NTCPIP__LWIP_SNMP

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Universal, for types whose meaning is the same in all applications; these
 * types are only defined in X.208.
 */
#define SNMP_ASN1_UNIV   0

/**
 * Application, for types whose meaning is specific to an application, such as
 * X.500 directory services; types in two different applications may have the
 * same application-specific tag and different meanings.
 */
#define SNMP_ASN1_APPLIC 0x40

/**
 * Context-specific, for types whose meaning is specific to a given structured
 * type; context-specific tags are used to distinguish between component types
 * with the same underlying tag within the context of a given structured type,
 * and component types in two different structured types may have the same tag
 * and different meanings.
 */
#define SNMP_ASN1_CONTXT 0x80

#define SNMP_ASN1_CONSTR 0x20
#define SNMP_ASN1_PRIMIT 0

/* universal tags */
#define SNMP_ASN1_INTEG  2
#define SNMP_ASN1_OC_STR 4
#define SNMP_ASN1_NUL    5
#define SNMP_ASN1_OBJ_ID 6
#define SNMP_ASN1_SEQ    16

/* application specific (SNMP) tags */
#define SNMP_ASN1_IPADDR 0    /* octet string size(4) */
#define SNMP_ASN1_COUNTER 1   /* Uint32 */
#define SNMP_ASN1_GAUGE 2     /* Uint32 */
#define SNMP_ASN1_TIMETICKS 3 /* Uint32 */
#define SNMP_ASN1_OPAQUE 4    /* octet string */

/* context specific (SNMP) tags */
#define SNMP_ASN1_PDU_GET_REQ 0
#define SNMP_ASN1_PDU_GET_NEXT_REQ 1
#define SNMP_ASN1_PDU_GET_RESP 2
#define SNMP_ASN1_PDU_SET_REQ 3
#define SNMP_ASN1_PDU_TRAP 4

ntcpip_Err ntcpip__snmpAsn1DecType(struct ntcpip_pbuf *p, Uint16 ofs, Uint8 *type);
ntcpip_Err ntcpip__snmpAsn1DecLength(struct ntcpip_pbuf *p, Uint16 ofs, Uint8 *octets_used, Uint16 *length);
ntcpip_Err ntcpip__snmpAsn1DecU32t(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 len, Uint32 *value);
ntcpip_Err ntcpip__snmpAsn1DecS32t(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 len, Int32 *value);
ntcpip_Err ntcpip__snmpAsn1DecOid(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 len, struct snmp_obj_id *oid);
ntcpip_Err ntcpip__snmpAsn1DecRaw(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 len, Uint16 raw_len, Uint8 *raw);

void ntcpip__snmpAsn1EncLengthCnt(Uint16 length, Uint8 *octets_needed);
void ntcpip__snmpAsn1EncU32tCnt(Uint32 value, Uint16 *octets_needed);
void ntcpip__snmpAsn1EncS32tCnt(Int32 value, Uint16 *octets_needed);
void ntcpip__snmpAsn1EncOidCnt(Uint8 ident_len, Int32 *ident, Uint16 *octets_needed);
ntcpip_Err ntcpip__snmpAsn1EncType(struct ntcpip_pbuf *p, Uint16 ofs, Uint8 type);
ntcpip_Err ntcpip__snmpAsn1EncLength(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 length);
ntcpip_Err ntcpip__snmpAsn1EncU32t(struct ntcpip_pbuf *p, Uint16 ofs, Uint8 octets_needed, Uint32 value);
ntcpip_Err ntcpip__snmpAsn1EncS32t(struct ntcpip_pbuf *p, Uint16 ofs, Uint8 octets_needed, Int32 value);
ntcpip_Err ntcpip__snmpAsn1EncOid(struct ntcpip_pbuf *p, Uint16 ofs, Uint8 ident_len, Int32 *ident);
ntcpip_Err ntcpip__snmpAsn1EncRaw(struct ntcpip_pbuf *p, Uint16 ofs, Uint8 raw_len, Uint8 *raw);

#ifdef __cplusplus
}
#endif

//#endif /* NTCPIP__LWIP_SNMP */

#endif /* __LWIP_SNMP_ASN1_H__ */


