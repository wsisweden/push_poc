/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_API_H__
#define __LWIP_API_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_NETCONN /* don't build if not configured for use in lwipopts.h */

#include <stddef.h> /* for size_t */

#include "ntcpip/netbuf.h"
#include "ntcpip/sys.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/err.h"

#ifdef __cplusplus
extern "C" {
#endif


/* Throughout this file, IP addresses and port numbers are expected to be in
 * the same byte order as in the corresponding pcb.
 */

/* Flags for ntcpip_netconnWrite */
#define NTCPIP_NETCONN_NOFLAG 0x00
#define NTCPIP_NETCONN_NOCOPY 0x00 /* Only for source code compatibility */
#define NTCPIP_NETCONN_COPY   0x01
#define NTCPIP_NETCONN_MORE   0x02

/* Helpers to process several netconn_types by the same code */
#define NTCPIP__NETCONNTYPE_GROUP(t)    (t&0xF0)
#define NTCPIP__NETCONNTYPE_DATAGRAM(t) (t&0xE0)

enum ntcpip_netconnType {
  NTCPIP_NETCONN_INVALID    = 0,
  /* NTCPIP_NETCONN_TCP Group */
  NTCPIP_NETCONN_TCP        = 0x10,
  /* NTCPIP_NETCONN_UDP Group */
  NTCPIP_NETCONN_UDP        = 0x20,
  NTCPIP_NETCONN_UDPLITE    = 0x21,
  NTCPIP_NETCONN_UDPNOCHKSUM= 0x22,
  /* NTCPIP_NETCONN_RAW Group */
  NTCPIP_NETCONN_RAW        = 0x40
};

enum ntcpip__netconnState {
  NTCPIP__NETCONN_NONE,
  NTCPIP__NETCONN_WRITE,
  NTCPIP__NETCONN_LISTEN,
  NTCPIP__NETCONN_CONNECT,
  NTCPIP__NETCONN_CLOSE
};

enum ntcpip_netconnEvt {
  NTCPIP_NETCONN_EVT_RCVPLUS,
  NTCPIP_NETCONN_EVT_RCVMINUS,
  NTCPIP_NETCONN_EVT_SENDPLUS,
  NTCPIP_NETCONN_EVT_SENDMINUS
};

#if NTCPIP__LWIP_IGMP
enum ntcpip__netconnIgmp {
  NTCPIP_NETCONN_JOIN,
  NTCPIP_NETCONN_LEAVE
};
#endif /* NTCPIP__LWIP_IGMP */

/* forward-declare some structs to avoid to include their headers */
struct ntcpip__ipPcb;
struct ntcpip__tcpPcb;
struct ntcpip__udpPcb;
struct ntcpip__rawPcb;
struct ntcpip__netconn;

/** A callback prototype to inform about events for a netconn */
typedef void (* ntcpip__NetconnCallback)(struct ntcpip__netconn *, enum ntcpip_netconnEvt, Uint16 len);

/** A netconn descriptor */
struct ntcpip__netconn {
  /** type of the netconn (TCP, UDP or RAW) */
  enum ntcpip_netconnType type;
  /** current state of the netconn */
  enum ntcpip__netconnState state;
  /** the lwIP internal protocol control block */
  union {
    struct ntcpip__ipPcb  *ip;
    struct ntcpip__tcpPcb *tcp;
    struct ntcpip__udpPcb *udp;
    struct ntcpip__rawPcb *raw;
  } pcb;
  /** the last error this netconn had */
  ntcpip_Err err;
  /** sem that is used to synchroneously execute functions in the core context */
  ntcpip__SysSema op_completed;
  /** mbox where received packets are stored until they are fetched
      by the netconn application thread (can grow quite big) */
  ntcpip__SysMbox recvmbox;
  /** mbox where new connections are stored until processed
      by the application thread */
  ntcpip__SysMbox acceptmbox;
  /** only used for socket layer */
  int socket;
#if NTCPIP__LWIP_SO_RCVTIMEO
  /** timeout to wait for new data to be received
      (or connections to arrive for listening netconns) */
  int recv_timeout;
#endif /* NTCPIP__LWIP_SO_RCVTIMEO */
#if NTCPIP__LWIP_SO_RCVBUF
  /** maximum amount of bytes queued in recvmbox */
  int recv_bufsize;
#endif /* NTCPIP__LWIP_SO_RCVBUF */
  Int16 recv_avail;
#if NTCPIP__LWIP_TCP
  /** TCP: when data passed to ntcpip_netconnWrite doesn't fit into the send buffer,
      this temporarily stores the message. */
  struct ntcpip__apiMsgMsg *write_msg;
  /** TCP: when data passed to ntcpip_netconnWrite doesn't fit into the send buffer,
      this temporarily stores how much is already sent. */
  size_t write_offset;
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
  /** TCP: when data passed to ntcpip_netconnWrite doesn't fit into the send buffer,
      this temporarily stores whether to wake up the original application task
      if data couldn't be sent in the first try. */
  Uint8 write_delayed;
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */
#endif /* NTCPIP__LWIP_TCP */
  /** A callback function that is informed about events for this netconn */
  ntcpip__NetconnCallback callback;
  /** Utility pointer. This pointer is not used by lwIP. May be used to move
      information to the netconn callback function. (Added by Tietolaite) */
  void * pUtil;
};

/* Register an Network connection event */
#define NTCPIP__APIEVENT(c,e,l) if (c->callback) {         \
                           (*c->callback)(c, e, l); \
                         }

/* Network connection functions: */
#define ntcpip_netconnNew(t)                  ntcpip_netconnNewWithProtoAndCallback(t, 0, NULL)
#define ntcpip_netconnNewWithCallback(t, c) ntcpip_netconnNewWithProtoAndCallback(t, 0, c)
struct
ntcpip__netconn *ntcpip_netconnNewWithProtoAndCallback(enum ntcpip_netconnType t, Uint8 proto,
                                   ntcpip__NetconnCallback callback);
ntcpip_Err             ntcpip_netconnDelete  (struct ntcpip__netconn *conn);
/** Get the type of a netconn (as enum ntcpip_netconnType). */
#define ntcpip_netconnType(conn) (conn->type)

ntcpip_Err             ntcpip_netconnGetAddr (struct ntcpip__netconn *conn,
                                   struct ntcpip_ipAddr *addr,
                                   Uint16 *port,
                                   Uint8 local);
#define ntcpip_netconnPeer(c,i,p) ntcpip_netconnGetAddr(c,i,p,0)
#define ntcpip_netconnAddr(c,i,p) ntcpip_netconnGetAddr(c,i,p,1)

ntcpip_Err             ntcpip_netconnBind(struct ntcpip__netconn *conn,
                                   const struct ntcpip_ipAddr *addr,
                                   Uint16 port);
ntcpip_Err             ntcpip_netconnConnect(struct ntcpip__netconn *conn,
                                   struct ntcpip_ipAddr *addr,
                                   Uint16 port);
ntcpip_Err             ntcpip_netconnDisconnect (struct ntcpip__netconn *conn);
ntcpip_Err             ntcpip_netconnlistenWithBacklog(struct ntcpip__netconn *conn, Uint8 backlog);
#define ntcpip_netconnListen(conn) ntcpip_netconnlistenWithBacklog(conn, NTCPIP__TCP_DEFAULT_LISTEN_BACKLOG)
struct ntcpip__netconn *  ntcpip_netconnAccept  (struct ntcpip__netconn *conn);
struct ntcpip__netbuf *   ntcpip_netconnRecv    (struct ntcpip__netconn *conn);
ntcpip_Err             ntcpip_netconnSendTo(struct ntcpip__netconn *conn,
                                   struct ntcpip__netbuf *buf, const struct ntcpip_ipAddr *addr, Uint16 port);
ntcpip_Err             ntcpip_netconnSend    (struct ntcpip__netconn *conn,
                                   struct ntcpip__netbuf *buf);
ntcpip_Err             ntcpip_netconnWrite   (struct ntcpip__netconn *conn,
                                   const void *dataptr, size_t size,
                                   Uint8 apiflags);
ntcpip_Err             ntcpip_netconnClose   (struct ntcpip__netconn *conn);

#if NTCPIP__LWIP_IGMP
ntcpip_Err             ntcpip_netconnjoinLeaveGroup (struct ntcpip__netconn *conn,
                                            struct ntcpip_ipAddr *multiaddr,
                                            struct ntcpip_ipAddr *interface,
                                            enum ntcpip__netconnIgmp join_or_leave);
#endif /* NTCPIP__LWIP_IGMP */
#if NTCPIP__LWIP_DNS
ntcpip_Err             ntcpip_netconnGetHostByName(const char *name, struct ntcpip_ipAddr *addr);
#endif /* NTCPIP__LWIP_DNS */

#define ntcpip_netconnErr(conn)          ((conn)->err)
#define ntcpip_netconnRecvBufSize(conn) ((conn)->recv_bufsize)

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_NETCONN */

#endif /* __LWIP_API_H__ */

