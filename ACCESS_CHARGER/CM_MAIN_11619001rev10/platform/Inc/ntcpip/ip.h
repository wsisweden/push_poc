/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_IP_H__
#define __LWIP_IP_H__

#include "ntcpip/opt.h"

//#include "lwip/def.h"		/* removed by tlarsu */
#include "ntcpip/pbuf.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/err.h"
#include "ntcpip/netif.h"

#ifdef __cplusplus
extern "C" {
#endif

/** Currently, the function ip_output_if_opt() is only used with IGMP */
#define IP_OPTIONS_SEND   NTCPIP__LWIP_IGMP

#define NTCPIP__IP_HLEN 20

#define IP_PROTO_ICMP    1
#define IP_PROTO_UDP     17
#define IP_PROTO_UDPLITE 136
#define IP_PROTO_TCP     6

/* This is passed as the destination address to ntcpip__ipOutputIf (not
   to ntcpip__ipOutput), meaning that an IP header already is constructed
   in the pbuf. This is used when TCP retransmits. */
#ifdef IP_HDRINCL
#undef IP_HDRINCL
#endif /* IP_HDRINCL */
#define IP_HDRINCL  NULL

#if NTCPIP__LWIP_NETIF_HWADDRHINT
#define IP_PCB_ADDRHINT ;Uint8 addr_hint
#else
#define IP_PCB_ADDRHINT
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT */

/* This is the common part of all PCB types. It needs to be at the
   beginning of a PCB type definition. It is located here so that
   changes to this common part are made in one location instead of
   having to change all PCB structs. */
#define NTCPIP__IP_PCB \
  /* ip addresses in network byte order */ \
  struct ntcpip_ipAddr local_ip; \
  struct ntcpip_ipAddr remote_ip; \
   /* Socket options */  \
  Uint16 so_options;      \
   /* Type Of Service */ \
  Uint8 tos;              \
  /* Time To Live */     \
  Uint8 ttl               \
  /* link layer address resolution hint */ \
  IP_PCB_ADDRHINT

struct ntcpip__ipPcb {
/* Common members of all PCB types */
  NTCPIP__IP_PCB;
};

/*
 * Option flags per-socket. These are the same like SO_XXX.
 */
#define NTCPIP_SOF_DEBUG       (Uint16)0x0001U    /* turn on debugging info recording */
#define NTCPIP_SOF_ACCEPTCONN  (Uint16)0x0002U    /* socket has had listen() */
#define NTCPIP_SOF_REUSEADDR   (Uint16)0x0004U    /* allow local address reuse */
#define NTCPIP_SOF_KEEPALIVE   (Uint16)0x0008U    /* keep connections alive */
#define NTCPIP_SOF_DONTROUTE   (Uint16)0x0010U    /* just use interface addresses */
#define NTCPIP_SOF_BROADCAST   (Uint16)0x0020U    /* permit to send and to receive broadcast messages (see IP_SOF_BROADCAST option) */
#define NTCPIP_SOF_USELOOPBACK (Uint16)0x0040U    /* bypass hardware when possible */
#define NTCPIP_SOF_LINGER      (Uint16)0x0080U    /* linger on close if data present */
#define NTCPIP_SOF_OOBINLINE   (Uint16)0x0100U    /* leave received OOB data in line */
#define NTCPIP_SOF_REUSEPORT   (Uint16)0x0200U    /* allow local address & port reuse */


#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct ip_hdr {
  /* version / header length / type of service */
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _v_hl_tos);
  /* total length */
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _len);
  /* identification */
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _id);
  /* fragment offset field */
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _offset);
#define IP_RF 0x8000        /* reserved fragment flag */
#define IP_DF 0x4000        /* dont fragment flag */
#define IP_MF 0x2000        /* more fragments flag */
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
  /* time to live / protocol*/
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _ttl_proto);
  /* checksum */
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _chksum);
  /* source and destination IP addresses */
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip_ipAddr src);
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip_ipAddr dest); 
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

#define IPH_V(hdr)  (ntcpip_ntohs((hdr)->_v_hl_tos) >> 12)
#define IPH_HL(hdr) ((ntcpip_ntohs((hdr)->_v_hl_tos) >> 8) & 0x0f)
#define IPH_TOS(hdr) (ntcpip_ntohs((hdr)->_v_hl_tos) & 0xff)
#define IPH_LEN(hdr) ((hdr)->_len)
#define IPH_ID(hdr) ((hdr)->_id)
#define IPH_OFFSET(hdr) ((hdr)->_offset)
#define IPH_TTL(hdr) (ntcpip_ntohs((hdr)->_ttl_proto) >> 8)
#define IPH_PROTO(hdr) (ntcpip_ntohs((hdr)->_ttl_proto) & 0xff)
#define IPH_CHKSUM(hdr) ((hdr)->_chksum)

#define IPH_VHLTOS_SET(hdr, v, hl, tos) (hdr)->_v_hl_tos = (ntcpip_htons(((v) << 12) | ((hl) << 8) | (tos)))
#define IPH_LEN_SET(hdr, len) (hdr)->_len = (len)
#define IPH_ID_SET(hdr, id) (hdr)->_id = (id)
#define IPH_OFFSET_SET(hdr, off) (hdr)->_offset = (off)
#define IPH_TTL_SET(hdr, ttl) (hdr)->_ttl_proto = (ntcpip_htons(IPH_PROTO(hdr) | ((Uint16)(ttl) << 8)))
#define IPH_PROTO_SET(hdr, proto) (hdr)->_ttl_proto = (ntcpip_htons((proto) | (IPH_TTL(hdr) << 8)))
#define IPH_CHKSUM_SET(hdr, chksum) (hdr)->_chksum = (chksum)

/** The interface that provided the packet for the current callback invocation. */
extern struct ntcpip__netif *ntcpip__currentNetif;
/** Header of the input packet currently being processed. */
extern const struct ip_hdr *ntcpip__currentHeader;

#define ip_init() /* Compatibility define, not init needed. */
struct ntcpip__netif *ntcpip__ipRoute(const struct ntcpip_ipAddr *dest);
ntcpip_Err ntcpip__ipInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp);
ntcpip_Err ntcpip__ipOutput(struct ntcpip_pbuf *p, struct ntcpip_ipAddr *src, struct ntcpip_ipAddr *dest,
       Uint8 ttl, Uint8 tos, Uint8 proto);
ntcpip_Err ntcpip__ipOutputIf(struct ntcpip_pbuf *p, const struct ntcpip_ipAddr *src, const struct ntcpip_ipAddr *dest,
       Uint8 ttl, Uint8 tos, Uint8 proto,
       struct ntcpip__netif *netif);
#if NTCPIP__LWIP_NETIF_HWADDRHINT
ntcpip_Err ip_output_hinted(struct ntcpip_pbuf *p, struct ntcpip_ipAddr *src, struct ntcpip_ipAddr *dest,
       Uint8 ttl, Uint8 tos, Uint8 proto, Uint8 *addr_hint);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT */
#if IP_OPTIONS_SEND
ntcpip_Err ntcpip__ipOutputIfOpt(struct ntcpip_pbuf *p, const struct ntcpip_ipAddr *src, const struct ntcpip_ipAddr *dest,
       Uint8 ttl, Uint8 tos, Uint8 proto, struct ntcpip__netif *netif, void *ip_options,
       Uint16 optlen);
#endif /* IP_OPTIONS_SEND */
/** Get the interface that received the current packet.
 * This function must only be called from a receive callback (ntcpip__udpRecv,
 * raw_recv, ntcpip__tcpAccept). It will return NULL otherwise. */
#define ntcpip_ipCurrentNetif()  (ntcpip__currentNetif)
/** Get the IP header of the current packet.
 * This function must only be called from a receive callback (ntcpip__udpRecv,
 * raw_recv, ntcpip__tcpAccept). It will return NULL otherwise. */
#define ntcpip_ipCurrentHeader() (ntcpip__currentHeader)
#if NTCPIP__IP_DEBUG
void ip_debug_print(struct ntcpip_pbuf *p);
#else
#define ip_debug_print(p)
#endif /* NTCPIP__IP_DEBUG */

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_IP_H__ */



