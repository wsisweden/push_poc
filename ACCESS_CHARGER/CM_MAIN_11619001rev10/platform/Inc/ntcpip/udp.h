/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_UDP_H__
#define __LWIP_UDP_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_UDP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/pbuf.h"
#include "ntcpip/netif.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/ip.h"

#ifdef __cplusplus
extern "C" {
#endif

#define UDP_HLEN 8

/* Fields are (of course) in network byte order. */
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct udp_hdr {
  NTCPIP__PACK_STRUCT_FIELD(Uint16 src);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 dest);  /* src/dest UDP ports */
  NTCPIP__PACK_STRUCT_FIELD(Uint16 len);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 chksum);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

#define UDP_FLAGS_NOCHKSUM 0x01U
#define UDP_FLAGS_UDPLITE  0x02U
#define UDP_FLAGS_CONNECTED  0x04U

struct ntcpip__udpPcb {
/* Common members of all PCB types */
  NTCPIP__IP_PCB;

/* Protocol specific PCB members */

  struct ntcpip__udpPcb *next;

  Uint8 flags;
  /* ports are in host byte order */
  Uint16 local_port, remote_port;

#if NTCPIP__LWIP_IGMP
  /* outgoing network interface for multicast packets */
  struct ntcpip_ipAddr multicast_ip;
#endif /* NTCPIP__LWIP_IGMP */

#if NTCPIP__LWIP_UDPLITE
  /* used for UDP_LITE only */
  Uint16 chksum_len_rx, chksum_len_tx;
#endif /* NTCPIP__LWIP_UDPLITE */

  /* receive callback function
   * addr and port are in same byte order as in the pcb
   * The callback is responsible for freeing the pbuf
   * if it's not used any more.
   *
   * ATTENTION: Be aware that 'addr' points into the pbuf 'p' so freeing this pbuf
   *            makes 'addr' invalid, too.
   *
   * @param arg user supplied argument (udp_pcb.recv_arg)
   * @param pcb the udp_pcb which received data
   * @param p the packet buffer that was received
   * @param addr the remote IP address from which the packet was received
   * @param port the remote port from which the packet was received
   */
  void (* recv)(void *arg, struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p,
    struct ntcpip_ipAddr *addr, Uint16 port);
  /* user-supplied argument for the recv callback */
  void *recv_arg;  
};
/* ntcpip__udpPcbs export for exernal reference (e.g. SNMP agent) */
extern struct ntcpip__udpPcb *ntcpip__udpPcbs;

/* The following functions is the application layer interface to the
   UDP code. */
struct ntcpip__udpPcb * ntcpip_udpNew(void);
void             ntcpip_udpRemove(struct ntcpip__udpPcb *pcb);
ntcpip_Err       ntcpip_udpBind(struct ntcpip__udpPcb *pcb, const struct ntcpip_ipAddr *ipaddr,
                 Uint16 port);
ntcpip_Err       ntcpip_udpConnect(struct ntcpip__udpPcb *pcb, const struct ntcpip_ipAddr *ipaddr,
                 Uint16 port);
void             ntcpip_udpDisconnect(struct ntcpip__udpPcb *pcb);
void             ntcpip_udpRecv(struct ntcpip__udpPcb *pcb,
         void (* recv)(void *arg, struct ntcpip__udpPcb *upcb,
                 struct ntcpip_pbuf *p,
                 struct ntcpip_ipAddr *addr,
                 Uint16 port),
         void *recv_arg);
ntcpip_Err       ntcpip_udpSendToIf(struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p, const struct ntcpip_ipAddr *dst_ip, Uint16 dst_port, struct ntcpip__netif *netif);
ntcpip_Err       ntcpip_udpSendTo(struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p, const struct ntcpip_ipAddr *dst_ip, Uint16 dst_port);
ntcpip_Err       ntcpip_udpSend(struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p);

#define          udp_flags(pcb)  ((pcb)->flags)
#define          udp_setflags(pcb, f)  ((pcb)->flags = (f))

/* The following functions are the lower layer interface to UDP. */
void             ntcpip__udpInput      (struct ntcpip_pbuf *p, struct ntcpip__netif *inp);

#define udp_init() /* Compatibility define, not init needed. */

#if NTCPIP__UDP_DEBUG
void udp_debug_print(struct udp_hdr *udphdr);
#else
#define udp_debug_print(udphdr)
#endif

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_UDP */

#endif /* __LWIP_UDP_H__ */

