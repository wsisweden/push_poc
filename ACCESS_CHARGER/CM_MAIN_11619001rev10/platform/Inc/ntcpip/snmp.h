/*
 * Copyright (c) 2001, 2002 Leon Woestenberg <leon.woestenberg@axon.tv>
 * Copyright (c) 2001, 2002 Axon Digital Design B.V., The Netherlands.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Leon Woestenberg <leon.woestenberg@axon.tv>
 *
 */
#ifndef __LWIP_SNMP_H__
#define __LWIP_SNMP_H__

#include "ntcpip/opt.h"
#include "ntcpip/netif.h"
//#include "lwip/udp.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ntcpip__udpPcb;

/**
 * @see RFC1213, "MIB-II, 6. Definitions"
 */
enum snmp_ifType {
  snmp_ifType_other=1,                /* none of the following */
  snmp_ifType_regular1822,
  snmp_ifType_hdh1822,
  snmp_ifType_ddn_x25,
  snmp_ifType_rfc877_x25,
  snmp_ifType_ethernet_csmacd,
  snmp_ifType_iso88023_csmacd,
  snmp_ifType_iso88024_tokenBus,
  snmp_ifType_iso88025_tokenRing,
  snmp_ifType_iso88026_man,
  snmp_ifType_starLan,
  snmp_ifType_proteon_10Mbit,
  snmp_ifType_proteon_80Mbit,
  snmp_ifType_hyperchannel,
  snmp_ifType_fddi,
  snmp_ifType_lapb,
  snmp_ifType_sdlc,
  snmp_ifType_ds1,                    /* T-1 */
  snmp_ifType_e1,                     /* european equiv. of T-1 */
  snmp_ifType_basicISDN,
  snmp_ifType_primaryISDN,            /* proprietary serial */
  snmp_ifType_propPointToPointSerial,
  snmp_ifType_ppp,
  snmp_ifType_softwareLoopback,
  snmp_ifType_eon,                    /* CLNP over IP [11] */
  snmp_ifType_ethernet_3Mbit,
  snmp_ifType_nsip,                   /* XNS over IP */
  snmp_ifType_slip,                   /* generic SLIP */
  snmp_ifType_ultra,                  /* ULTRA technologies */
  snmp_ifType_ds3,                    /* T-3 */
  snmp_ifType_sip,                    /* SMDS */
  snmp_ifType_frame_relay
};


/** SNMP "sysuptime" Interval */
#define SNMP_SYSUPTIME_INTERVAL 10

/** fixed maximum length for object identifier type */
#define LWIP_SNMP_OBJ_ID_LEN 32

/** internal object identifier representation */
struct snmp_obj_id
{
  Uint8 len;
  Int32 id[LWIP_SNMP_OBJ_ID_LEN];
};

#if NTCPIP__LWIP_SNMP /* don't build if not configured for use in lwipopts.h */

/* system */
void ntcpip_snmpSetSysdesr(Uint8 *str, Uint8 *len);
void ntcpip_snmpSetSysobjid(struct snmp_obj_id *oid);
void ntcpip__snmpGetSysobjid_ptr(struct snmp_obj_id **oid);
void ntcpip__snmpIncSysuptime(void);
void ntcpip_snmpAddSysuptime(Uint32 value);
void ntcpip__snmpGetSysuptime(Uint32 *value);
void ntcpip_snmpSetSyscontact(Uint8 *ocstr,Uint8 *ocstrlen);
void ntcpip_snmpSetSysname(Uint8 *ocstr,Uint8 *ocstrlen);
void ntcpip_snmpSetSyslocation(Uint8 *ocstr,Uint8 *ocstrlen);

/* network interface */
void ntcpip_snmpAddIfinoctets(struct ntcpip__netif *ni, Uint32 value);
void ntcpip_snmpIncIfinucastpkts(struct ntcpip__netif *ni);
void ntcpip_snmpIncIfinnucastpkts(struct ntcpip__netif *ni);
void ntcpip_snmpIncIfindiscards(struct ntcpip__netif *ni);
void ntcpip_snmpAddIfoutoctets(struct ntcpip__netif *ni, Uint32 value);
void ntcpip_snmpIncIfoutucastpkts(struct ntcpip__netif *ni);
void ntcpip_snmpIncIfoutnucastpkts(struct ntcpip__netif *ni);
void ntcpip_snmpIncIfoutdiscards(struct ntcpip__netif *ni);
void ntcpip__snmpIncIflist(void);
void ntcpip__snmpDecIflist(void);

/* ARP (for atTable and ipNetToMediaTable) */
void ntcpip__snmpInsertArpidxTree(struct ntcpip__netif *ni, struct ntcpip_ipAddr *ip);
void ntcpip__snmpDeleteArpidxTree(struct ntcpip__netif *ni, struct ntcpip_ipAddr *ip);

/* IP */
void ntcpip__snmpIncIpinreceives(void);
void ntcpip__snmpIncIpinhdrerrors(void);
void ntcpip__snmpIncIpinaddrerrors(void);
void ntcpip__snmpIncIpforwdatagrams(void);
void ntcpip__snmpIncIpinunknownprotos(void);
void ntcpip__snmpIncIpindiscards(void);
void ntcpip__snmpIncIpindelivers(void);
void ntcpip__snmpIncIpoutrequests(void);
void ntcpip__snmpIncIpoutdiscards(void);
void ntcpip__snmpIncIpoutnoroutes(void);
void ntcpip__snmpIncIpreasmreqds(void);
void ntcpip__snmpIncIpreasmoks(void);
void ntcpip__snmpIncIpreasmfails(void);
void ntcpip__snmpIncIpfragoks(void);
void ntcpip__snmpIncIpfragfails(void);
void ntcpip__snmpIncIpfragcreates(void);
void ntcpip__snmpIncIproutingdiscards(void);
void ntcpip__snmpInsertIpaddridxTree(struct ntcpip__netif *ni);
void ntcpip__snmpDeleteIpaddridxTree(struct ntcpip__netif *ni);
void ntcpip__snmpInsertIprteidxTree(Uint8 dflt, struct ntcpip__netif *ni);
void ntcpip__snmpDeleteIprteidxTree(Uint8 dflt, struct ntcpip__netif *ni);

/* ICMP */
void ntcpip__snmpIncIcmpinmsgs(void);
void ntcpip__snmpIncIcmpinerrors(void);
void ntcpip__snmpIncIcmpindestunreachs(void);
void ntcpip__snmpIncIcmpintimeexcds(void);
void ntcpip__snmpIncIcmpinparmprobs(void);
void ntcpip__snmpIncIcmpinsrcquenchs(void);
void ntcpip__snmpIncIcmpinredirects(void);
void ntcpip__snmpIncIcmpinechos(void);
void ntcpip__snmpIncIcmpinechoreps(void);
void ntcpip__snmpIncIcmpintimestamps(void);
void ntcpip__snmpIncIcmpintimestampreps(void);
void ntcpip__snmpIncIcmpinaddrmasks(void);
void ntcpip__snmpIncIcmpinaddrmaskreps(void);
void ntcpip__snmpIncIcmpoutmsgs(void);
void ntcpip__snmpIncIcmpouterrors(void);
void ntcpip__snmpIncIcmpoutdestunreachs(void);
void ntcpip__snmpIncIcmpouttimeexcds(void);
void ntcpip__snmpIncIcmpoutparmprobs(void);
void ntcpip__snmpIncIcmpoutsrcquenchs(void);
void ntcpip__snmpIncIcmpoutredirects(void); 
void ntcpip__snmpIncIcmpoutechos(void);
void ntcpip__snmpIncIcmpoutechoreps(void);
void ntcpip__snmpIncIcmpouttimestamps(void);
void ntcpip__snmpIncIcmpouttimestampreps(void);
void ntcpip__snmpIncIcmpoutaddrmasks(void);
void ntcpip__snmpIncIcmpoutaddrmaskreps(void);

/* TCP */
void ntcpip__snmpIncTcpactiveopens(void);
void ntcpip__snmpIncTcppassiveopens(void);
void ntcpip__snmpIncTcpattemptfails(void);
void ntcpip__snmpIncTcpestabresets(void);
void ntcpip__snmpIncTcpinsegs(void);
void ntcpip__snmpIncTcpoutsegs(void);
void ntcpip__snmpIncTcpretranssegs(void);
void ntcpip__snmpIncTcpinerrs(void);
void ntcpip__snmpIncTcpoutrsts(void);

/* UDP */
void ntcpip__snmpInc_udpindatagrams(void);
void ntcpip__snmpIncUdpnoports(void);
void ntcpip__snmpIncUdpinerrors(void);
void ntcpip__snmpIncUdpoutdatagrams(void);
void ntcpip__snmpInsertUdpidxTree(struct ntcpip__udpPcb *pcb);
void ntcpip__snmpDeleteUdpidxTree(struct ntcpip__udpPcb *pcb);

/* SNMP */
void ntcpip__snmpIncSnmpinpkts(void);
void ntcpip__snmpIncSnmpoutpkts(void);
void ntcpip__snmpIncSnmpinbadversions(void);
void ntcpip__snmpIncSnmpinbadcommunitynames(void);
void ntcpip__snmpIncSnmpinbadcommunityuses(void);
void ntcpip__snmpIncSnmpinasnparseerrs(void);
void ntcpip__snmpIncSnmpintoobigs(void);
void ntcpip__snmpIncSnmpinnosuchnames(void);
void ntcpip__snmpIncSnmpinbadvalues(void);
void ntcpip__snmpIncSnmpinreadonlys(void);
void ntcpip__snmpIncSnmpingenerrs(void);
void ntcpip__snmpAddSnmpintotalreqvars(Uint8 value);
void ntcpip__snmpAddSnmpintotalsetvars(Uint8 value);
void ntcpip__snmpIncSnmpingetrequests(void);
void ntcpip__snmpIncSnmpingetnexts(void);
void ntcpip__snmpIncSnmpinsetrequests(void);
void ntcpip__snmpIncSnmpingetresponses(void);
void ntcpip__snmpIncSnmpintraps(void);
void ntcpip__snmpIncSnmpouttoobigs(void);
void ntcpip__snmpIncSnmpoutnosuchnames(void);
void ntcpip__snmpIncSnmpoutbadvalues(void);
void ntcpip__snmpIncSnmpoutgenerrs(void);
void ntcpip__snmpIncSnmpoutgetrequests(void);
void ntcpip__snmpIncSnmpoutgetnexts(void);
void ntcpip__snmpIncSnmpoutsetrequests(void);
void ntcpip__snmpIncSnmpoutgetresponses(void);
void ntcpip__snmpIncSnmpouttraps(void);
void ntcpip__snmpGetSnmpgrpid_ptr(struct snmp_obj_id **oid);
void ntcpip__snmpSetSnmpenableauthentraps(Uint8 *value);
void ntcpip__snmpGetSnmpenableauthentraps(Uint8 *value);

/* NTCPIP__LWIP_SNMP support not available */
/* define everything to be empty */
#else

/* system */
#define ntcpip__snmpSetSysdesr(str, len)
#define ntcpip__snmpSetSysobjid(oid);
#define ntcpip__snmpGetSysobjid_ptr(oid)
#define ntcpip__snmpIncSysuptime()
#define ntcpip__snmpAddSysuptime(value)
#define ntcpip__snmpGetSysuptime(value)
#define ntcpip__snmpSetSyscontact(ocstr, ocstrlen);
#define ntcpip__snmpSetSysname(ocstr, ocstrlen);
#define ntcpip__snmpSetSyslocation(ocstr, ocstrlen);

/* network interface */
#define ntcpip__snmpAddIfinoctets(ni,value) 
#define ntcpip__snmpIncIfinucastpkts(ni)
#define ntcpip__snmpIncIfinnucastpkts(ni)
#define ntcpip__snmpIncIfindiscards(ni)
#define ntcpip__snmpAddIfoutoctets(ni,value)
#define ntcpip__snmpIncIfoutucastpkts(ni)
#define ntcpip__snmpIncIfoutnucastpkts(ni)
#define ntcpip__snmpIncIfoutdiscards(ni)
#define ntcpip__snmpIncIflist()
#define ntcpip__snmpDecIflist()

/* ARP */
#define ntcpip__snmpInsertArpidxTree(ni,ip)
#define ntcpip__snmpDeleteArpidxTree(ni,ip)

/* IP */
#define ntcpip__snmpIncIpinreceives()
#define ntcpip__snmpIncIpinhdrerrors()
#define ntcpip__snmpIncIpinaddrerrors()
#define ntcpip__snmpIncIpforwdatagrams()
#define ntcpip__snmpIncIpinunknownprotos()
#define ntcpip__snmpIncIpindiscards()
#define ntcpip__snmpIncIpindelivers()
#define ntcpip__snmpIncIpoutrequests()
#define ntcpip__snmpIncIpoutdiscards()
#define ntcpip__snmpIncIpoutnoroutes()
#define ntcpip__snmpIncIpreasmreqds()
#define ntcpip__snmpIncIpreasmoks()
#define ntcpip__snmpIncIpreasmfails()
#define ntcpip__snmpIncIpfragoks()
#define ntcpip__snmpIncIpfragfails()
#define ntcpip__snmpIncIpfragcreates()
#define ntcpip__snmpIncIproutingdiscards()
#define ntcpip__snmpInsertIpaddridxTree(ni)
#define ntcpip__snmpDeleteIpaddridxTree(ni)
#define ntcpip__snmpInsertIprteidxTree(dflt, ni)
#define ntcpip__snmpDeleteIprteidxTree(dflt, ni)

/* ICMP */
#define ntcpip__snmpIncIcmpinmsgs()
#define ntcpip__snmpIncIcmpinerrors() 
#define ntcpip__snmpIncIcmpindestunreachs() 
#define ntcpip__snmpIncIcmpintimeexcds()
#define ntcpip__snmpIncIcmpinparmprobs() 
#define ntcpip__snmpIncIcmpinsrcquenchs() 
#define ntcpip__snmpIncIcmpinredirects() 
#define ntcpip__snmpIncIcmpinechos() 
#define ntcpip__snmpIncIcmpinechoreps()
#define ntcpip__snmpIncIcmpintimestamps() 
#define ntcpip__snmpIncIcmpintimestampreps()
#define ntcpip__snmpIncIcmpinaddrmasks()
#define ntcpip__snmpIncIcmpinaddrmaskreps()
#define ntcpip__snmpIncIcmpoutmsgs()
#define ntcpip__snmpIncIcmpouterrors()
#define ntcpip__snmpIncIcmpoutdestunreachs() 
#define ntcpip__snmpIncIcmpouttimeexcds() 
#define ntcpip__snmpIncIcmpoutparmprobs()
#define ntcpip__snmpIncIcmpoutsrcquenchs()
#define ntcpip__snmpIncIcmpoutredirects() 
#define ntcpip__snmpIncIcmpoutechos() 
#define ntcpip__snmpIncIcmpoutechoreps()
#define ntcpip__snmpIncIcmpouttimestamps()
#define ntcpip__snmpIncIcmpouttimestampreps()
#define ntcpip__snmpIncIcmpoutaddrmasks()
#define ntcpip__snmpIncIcmpoutaddrmaskreps()
/* TCP */
#define ntcpip__snmpIncTcpactiveopens()
#define ntcpip__snmpIncTcppassiveopens()
#define ntcpip__snmpIncTcpattemptfails()
#define ntcpip__snmpIncTcpestabresets()
#define ntcpip__snmpIncTcpinsegs()
#define ntcpip__snmpIncTcpoutsegs()
#define ntcpip__snmpIncTcpretranssegs()
#define ntcpip__snmpIncTcpinerrs()
#define ntcpip__snmpIncTcpoutrsts()

/* UDP */
#define ntcpip__snmpInc_udpindatagrams()
#define ntcpip__snmpIncUdpnoports()
#define ntcpip__snmpIncUdpinerrors()
#define ntcpip__snmpIncUdpoutdatagrams()
#define ntcpip__snmpInsertUdpidxTree(pcb)
#define ntcpip__snmpDeleteUdpidxTree(pcb)

/* SNMP */
#define ntcpip__snmpIncSnmpinpkts()
#define ntcpip__snmpIncSnmpoutpkts()
#define ntcpip__snmpIncSnmpinbadversions()
#define ntcpip__snmpIncSnmpinbadcommunitynames()
#define ntcpip__snmpIncSnmpinbadcommunityuses()
#define ntcpip__snmpIncSnmpinasnparseerrs()
#define ntcpip__snmpIncSnmpintoobigs()
#define ntcpip__snmpIncSnmpinnosuchnames()
#define ntcpip__snmpIncSnmpinbadvalues()
#define ntcpip__snmpIncSnmpinreadonlys()
#define ntcpip__snmpIncSnmpingenerrs()
#define ntcpip__snmpAddSnmpintotalreqvars(value)
#define ntcpip__snmpAddSnmpintotalsetvars(value)
#define ntcpip__snmpIncSnmpingetrequests()
#define ntcpip__snmpIncSnmpingetnexts()
#define ntcpip__snmpIncSnmpinsetrequests()
#define ntcpip__snmpIncSnmpingetresponses()
#define ntcpip__snmpIncSnmpintraps()
#define ntcpip__snmpIncSnmpouttoobigs()
#define ntcpip__snmpIncSnmpoutnosuchnames()
#define ntcpip__snmpIncSnmpoutbadvalues()
#define ntcpip__snmpIncSnmpoutgenerrs()
#define ntcpip__snmpIncSnmpoutgetrequests()
#define ntcpip__snmpIncSnmpoutgetnexts()
#define ntcpip__snmpIncSnmpoutsetrequests()
#define ntcpip__snmpIncSnmpoutgetresponses()
#define ntcpip__snmpIncSnmpouttraps()
#define ntcpip__snmpGetSnmpgrpid_ptr(oid)
#define ntcpip__snmpSetSnmpenableauthentraps(value)
#define ntcpip__snmpGetSnmpenableauthentraps(value)

#endif /* NTCPIP__LWIP_SNMP */

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_SNMP_H__ */


