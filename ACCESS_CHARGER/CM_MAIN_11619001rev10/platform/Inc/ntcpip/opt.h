/**
 * @file
 *
 * lwIP Options Configuration
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_OPT_H__
#define __LWIP_OPT_H__

/*
 * Include user defined options first. Anything not defined in these files
 * will be set to standard values. Override anything you dont like!
 */
#include "ntcpip/lwipopts.h"
#include "ntcpip/debug.h"

/*
   -----------------------------------------------
   ---------- Platform specific locking ----------
   -----------------------------------------------
*/

/**
 * NTCPIP__SYS_LIGHTWEIGHT_PROT==1: if you want inter-task protection for certain
 * critical regions during buffer allocation, deallocation and memory
 * allocation and deallocation.
 */
#ifndef NTCPIP__SYS_LIGHTWEIGHT_PROT
#define NTCPIP__SYS_LIGHTWEIGHT_PROT            0
#endif

/** 
 * NTCPIP__NO_SYS==1: Provides VERY minimal functionality. Otherwise,
 * use lwIP facilities.
 */
#ifndef NTCPIP__NO_SYS
#define NTCPIP__NO_SYS                          0
#endif

/**
 * NTCPIP__MEMCPY: override this if you have a faster implementation at hand than the
 * one included in your C library
 */
#ifndef NTCPIP__MEMCPY
#define NTCPIP__MEMCPY(dst,src,len)             memcpy(dst,src,len)
#endif

/**
 * NTCPIP__SMEMCPY: override this with care! Some compilers (e.g. gcc) can inline a
 * call to memcpy() if the length is known at compile time and is small.
 */
#ifndef NTCPIP__SMEMCPY
#define NTCPIP__SMEMCPY(dst,src,len)            memcpy(dst,src,len)
#endif

/*
   ------------------------------------
   ---------- Memory options ----------
   ------------------------------------
*/
/**
 * NTCPIP__MEM_LIBC_MALLOC==1: Use malloc/free/realloc provided by your C-library
 * instead of the lwip internal allocator. Can save code size if you
 * already use it.
 */
#ifndef NTCPIP__MEM_LIBC_MALLOC
#define NTCPIP__MEM_LIBC_MALLOC                 0
#endif

/**
* MEMP_MEM_MALLOC==1: Use ntcpip__memMalloc/ntcpip__memFree instead of the lwip pool allocator.
* Especially useful with NTCPIP__MEM_LIBC_MALLOC but handle with care regarding execution
* speed and usage from interrupts!
*/
#ifndef MEMP_MEM_MALLOC
#define MEMP_MEM_MALLOC                 0
#endif

/**
 * NTCPIP__MEM_ALIGNMENT: should be set to the alignment of the CPU
 *    4 byte alignment -> #define NTCPIP__MEM_ALIGNMENT 4
 *    2 byte alignment -> #define NTCPIP__MEM_ALIGNMENT 2
 */
#ifndef NTCPIP__MEM_ALIGNMENT
#define NTCPIP__MEM_ALIGNMENT                   1
#endif

/**
 * NTCPIP__MEM_SIZE: the size of the heap memory. If the application will send
 * a lot of data that needs to be copied, this should be set high.
 */
#ifndef NTCPIP__MEM_SIZE
#define NTCPIP__MEM_SIZE                        1600
#endif

/**
 * NTCPIP__MEMP_OVERFLOW_CHECK: memp overflow protection reserves a configurable
 * amount of bytes before and after each memp element in every pool and fills
 * it with a prominent default value.
 *    NTCPIP__MEMP_OVERFLOW_CHECK == 0 no checking
 *    NTCPIP__MEMP_OVERFLOW_CHECK == 1 checks each element when it is freed
 *    NTCPIP__MEMP_OVERFLOW_CHECK >= 2 checks each element in every pool every time
 *      ntcpip__mempMalloc() or ntcpip__mempFree() is called (useful but slow!)
 */
#ifndef NTCPIP__MEMP_OVERFLOW_CHECK
#define NTCPIP__MEMP_OVERFLOW_CHECK             0
#endif

/**
 * NTCPIP__MEMP_SANITY_CHECK==1: run a sanity check after each ntcpip__mempFree() to make
 * sure that there are no cycles in the linked lists.
 */
#ifndef NTCPIP__MEMP_SANITY_CHECK
#define NTCPIP__MEMP_SANITY_CHECK               0
#endif

/**
 * NTCPIP__MEM_USE_POOLS==1: Use an alternative to malloc() by allocating from a set
 * of memory pools of various sizes. When ntcpip__memMalloc is called, an element of
 * the smallest pool that can provide the length needed is returned.
 * To use this, NTCPIP__MEMP_USE_CUSTOM_POOLS also has to be enabled.
 */
#ifndef NTCPIP__MEM_USE_POOLS
#define NTCPIP__MEM_USE_POOLS                   0
#endif

/**
 * MEM_USE_POOLS_TRY_BIGGER_POOL==1: if one malloc-pool is empty, try the next
 * bigger pool - WARNING: THIS MIGHT WASTE MEMORY but it can make a system more
 * reliable. */
#ifndef MEM_USE_POOLS_TRY_BIGGER_POOL
#define MEM_USE_POOLS_TRY_BIGGER_POOL   0
#endif

/**
 * NTCPIP__MEMP_USE_CUSTOM_POOLS==1: whether to include a user file lwippools.h
 * that defines additional pools beyond the "standard" ones required
 * by lwIP. If you set this to 1, you must have lwippools.h in your 
 * inlude path somewhere. 
 */
#ifndef NTCPIP__MEMP_USE_CUSTOM_POOLS
#define NTCPIP__MEMP_USE_CUSTOM_POOLS           0
#endif

/**
 * Set this to 1 if you want to free NTCPIP_PBUF_RAM pbufs (or call ntcpip__memFree()) from
 * interrupt context (or another context that doesn't allow waiting for a
 * semaphore).
 * If set to 1, ntcpip__memMalloc will be protected by a semaphore and NTCPIP__SYS_ARCH_PROTECT,
 * while ntcpip__memFree will only use NTCPIP__SYS_ARCH_PROTECT. ntcpip__memMalloc SYS_ARCH_UNPROTECTs
 * with each loop so that ntcpip__memFree can run.
 *
 * ATTENTION: As you can see from the above description, this leads to dis-/
 * enabling interrupts often, which can be slow! Also, on low memory, ntcpip__memMalloc
 * can need longer.
 *
 * If you don't want that, at least for NTCPIP__NO_SYS=0, you can still use the following
 * functions to enqueue a deallocation call which then runs in the tcpip_thread
 * context:
 * - ntcpip_pbufFreeCallback(p);
 * - mem_free_callback(m);
 */
#ifndef LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT
#define LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT 0
#endif

/*
   ------------------------------------------------
   ---------- Internal Memory Pool Sizes ----------
   ------------------------------------------------
*/
/**
 * NTCPIP__MEMP_NUM_PBUF: the number of memp struct pbufs (used for NTCPIP_PBUF_ROM and NTCPIP_PBUF_REF).
 * If the application sends a lot of data out of ROM (or other static memory),
 * this should be set high.
 */
#ifndef NTCPIP__MEMP_NUM_PBUF
#define NTCPIP__MEMP_NUM_PBUF                   16
#endif

/**
 * NTCPIP__MEMP_NUM_RAW_PCB: Number of raw connection PCBs
 * (requires the NTCPIP__LWIP_RAW option)
 */
#ifndef NTCPIP__MEMP_NUM_RAW_PCB
#define NTCPIP__MEMP_NUM_RAW_PCB                4
#endif

/**
 * NTCPIP__MEMP_NUM_UDP_PCB: the number of UDP protocol control blocks. One
 * per active UDP "connection".
 * (requires the NTCPIP__LWIP_UDP option)
 */
#ifndef NTCPIP__MEMP_NUM_UDP_PCB
#define NTCPIP__MEMP_NUM_UDP_PCB                4
#endif

/**
 * NTCPIP__MEMP_NUM_TCP_PCB: the number of simulatenously active TCP connections.
 * (requires the NTCPIP__LWIP_TCP option)
 */
#ifndef NTCPIP__MEMP_NUM_TCP_PCB
#define NTCPIP__MEMP_NUM_TCP_PCB                5
#endif

/**
 * NTCPIP__MEMP_NUM_TCP_PCB_LISTEN: the number of listening TCP connections.
 * (requires the NTCPIP__LWIP_TCP option)
 */
#ifndef NTCPIP__MEMP_NUM_TCP_PCB_LISTEN
#define NTCPIP__MEMP_NUM_TCP_PCB_LISTEN         8
#endif

/**
 * NTCPIP__MEMP_NUM_TCP_SEG: the number of simultaneously queued TCP segments.
 * (requires the NTCPIP__LWIP_TCP option)
 */
#ifndef NTCPIP__MEMP_NUM_TCP_SEG
#define NTCPIP__MEMP_NUM_TCP_SEG                16
#endif

/**
 * NTCPIP__MEMP_NUM_REASSDATA: the number of simultaneously IP packets queued for
 * reassembly (whole packets, not fragments!)
 */
#ifndef NTCPIP__MEMP_NUM_REASSDATA
#define NTCPIP__MEMP_NUM_REASSDATA              5
#endif

/**
 * NTCPIP__MEMP_NUM_ARP_QUEUE: the number of simulateously queued outgoing
 * packets (pbufs) that are waiting for an ARP request (to resolve
 * their destination address) to finish.
 * (requires the NTCPIP__ARP_QUEUEING option)
 */
#ifndef NTCPIP__MEMP_NUM_ARP_QUEUE
#define NTCPIP__MEMP_NUM_ARP_QUEUE              30
#endif

/**
 * NTCPIP__MEMP_NUM_IGMP_GROUP: The number of multicast groups whose network interfaces
 * can be members et the same time (one per netif - allsystems group -, plus one
 * per netif membership).
 * (requires the NTCPIP__LWIP_IGMP option)
 */
#ifndef NTCPIP__MEMP_NUM_IGMP_GROUP
#define NTCPIP__MEMP_NUM_IGMP_GROUP             8
#endif

/**
 * NTCPIP__MEMP_NUM_SYS_TIMEOUT: the number of simulateously active timeouts.
 * (requires NTCPIP__NO_SYS==0)
 */
#ifndef NTCPIP__MEMP_NUM_SYS_TIMEOUT
#define NTCPIP__MEMP_NUM_SYS_TIMEOUT            3
#endif

/**
 * NTCPIP__MEMP_NUM_NETBUF: the number of struct netbufs.
 * (only needed if you use the sequential API, like api_lib.c)
 */
#ifndef NTCPIP__MEMP_NUM_NETBUF
#define NTCPIP__MEMP_NUM_NETBUF                 2
#endif

/**
 * NTCPIP__MEMP_NUM_NETCONN: the number of struct netconns.
 * (only needed if you use the sequential API, like api_lib.c)
 */
#ifndef NTCPIP__MEMP_NUM_NETCONN
#define NTCPIP__MEMP_NUM_NETCONN                4
#endif

/**
 * NTCPIP__MEMP_NUM_TCPIP_MSG_API: the number of struct tcpip_msg, which are used
 * for callback/timeout API communication. 
 * (only needed if you use tcpip.c)
 */
#ifndef NTCPIP__MEMP_NUM_TCPIP_MSG_API
#define NTCPIP__MEMP_NUM_TCPIP_MSG_API          8
#endif

/**
 * NTCPIP__MEMP_NUM_TCPIP_MSG_INPKT: the number of struct tcpip_msg, which are used
 * for incoming packets. 
 * (only needed if you use tcpip.c)
 */
#ifndef NTCPIP__MEMP_NUM_TCPIP_MSG_INPKT
#define NTCPIP__MEMP_NUM_TCPIP_MSG_INPKT        8
#endif

/**
 * NTCPIP__PBUF_POOL_SIZE: the number of buffers in the pbuf pool. 
 */
#ifndef NTCPIP__PBUF_POOL_SIZE
#define NTCPIP__PBUF_POOL_SIZE                  16
#endif

/*
   ---------------------------------
   ---------- ARP options ----------
   ---------------------------------
*/
/**
 * NTCPIP__LWIP_ARP==1: Enable ARP functionality.
 */
#ifndef NTCPIP__LWIP_ARP
#define NTCPIP__LWIP_ARP                        1
#endif

/**
 * NTCPIP__ARP_TABLE_SIZE: Number of active MAC-IP address pairs cached.
 */
#ifndef NTCPIP__ARP_TABLE_SIZE
#define NTCPIP__ARP_TABLE_SIZE                  10
#endif

/**
 * NTCPIP__ARP_QUEUEING==1: Outgoing packets are queued during hardware address
 * resolution.
 */
#ifndef NTCPIP__ARP_QUEUEING
#define NTCPIP__ARP_QUEUEING                    1
#endif

/**
 * NTCPIP__ETHARP_TRUST_IP_MAC==1: Incoming IP packets cause the ARP table to be
 * updated with the source MAC and IP addresses supplied in the packet.
 * You may want to disable this if you do not trust LAN peers to have the
 * correct addresses, or as a limited approach to attempt to handle
 * spoofing. If disabled, lwIP will need to make a new ARP request if
 * the peer is not already in the ARP table, adding a little latency.
 */
#ifndef NTCPIP__ETHARP_TRUST_IP_MAC
#define NTCPIP__ETHARP_TRUST_IP_MAC             1
#endif

/**
 * ETHARP_SUPPORT_VLAN==1: support receiving ethernet packets with VLAN header.
 * Additionally, you can define ETHARP_VLAN_CHECK to an Uint16 VLAN ID to check.
 * If ETHARP_VLAN_CHECK is defined, only VLAN-traffic for this VLAN is accepted.
 * If ETHARP_VLAN_CHECK is not defined, all traffic is accepted.
 */
#ifndef ETHARP_SUPPORT_VLAN
#define ETHARP_SUPPORT_VLAN             		0
#endif

/*
   --------------------------------
   ---------- IP options ----------
   --------------------------------
*/
/**
 * NTCPIP__IP_FORWARD==1: Enables the ability to forward IP packets across
 * network interfaces. If you are going to run lwIP on a device with only one
 * network interface, define this to 0.
 */
#ifndef NTCPIP__IP_FORWARD
#define NTCPIP__IP_FORWARD                      0
#endif

/**
 * NTCPIP__IP_OPTIONS_ALLOWED: Defines the behavior for IP options.
 *      NTCPIP__IP_OPTIONS_ALLOWED==0: All packets with IP options are dropped.
 *      NTCPIP__IP_OPTIONS_ALLOWED==1: IP options are allowed (but not parsed).
 */
#ifndef NTCPIP__IP_OPTIONS_ALLOWED
#define NTCPIP__IP_OPTIONS_ALLOWED              1
#endif

/**
 * NTCPIP__IP_REASSEMBLY==1: Reassemble incoming fragmented IP packets. Note that
 * this option does not affect outgoing packet sizes, which can be controlled
 * via NTCPIP__IP_FRAG.
 */
#ifndef NTCPIP__IP_REASSEMBLY
#define NTCPIP__IP_REASSEMBLY                   1
#endif

/**
 * NTCPIP__IP_FRAG==1: Fragment outgoing IP packets if their size exceeds MTU. Note
 * that this option does not affect incoming packet sizes, which can be
 * controlled via NTCPIP__IP_REASSEMBLY.
 */
#ifndef NTCPIP__IP_FRAG
#define NTCPIP__IP_FRAG                         1
#endif

/**
 * NTCPIP__IP_REASS_MAXAGE: Maximum time (in multiples of IP_TMR_INTERVAL - so seconds, normally)
 * a fragmented IP packet waits for all fragments to arrive. If not all fragments arrived
 * in this time, the whole packet is discarded.
 */
#ifndef NTCPIP__IP_REASS_MAXAGE
#define NTCPIP__IP_REASS_MAXAGE                 3
#endif

/**
 * NTCPIP__IP_REASS_MAX_PBUFS: Total maximum amount of pbufs waiting to be reassembled.
 * Since the received pbufs are enqueued, be sure to configure
 * NTCPIP__PBUF_POOL_SIZE > NTCPIP__IP_REASS_MAX_PBUFS so that the stack is still able to receive
 * packets even if the maximum amount of fragments is enqueued for reassembly!
 */
#ifndef NTCPIP__IP_REASS_MAX_PBUFS
#define NTCPIP__IP_REASS_MAX_PBUFS              10
#endif

/**
 * NTCPIP__IP_FRAG_USES_STATIC_BUF==1: Use a static MTU-sized buffer for IP
 * fragmentation. Otherwise pbufs are allocated and reference the original
 * packet data to be fragmented.
 */
#ifndef NTCPIP__IP_FRAG_USES_STATIC_BUF
#define NTCPIP__IP_FRAG_USES_STATIC_BUF         1
#endif

/**
 * NTCPIP__IP_FRAG_MAX_MTU: Assumed max MTU on any interface for IP frag buffer
 * (requires NTCPIP__IP_FRAG_USES_STATIC_BUF==1)
 */
#if NTCPIP__IP_FRAG_USES_STATIC_BUF && !defined(NTCPIP__IP_FRAG_MAX_MTU)
#define NTCPIP__IP_FRAG_MAX_MTU                 1500
#endif

/**
 * NTCPIP__IP_DEFAULT_TTL: Default value for Time-To-Live used by transport layers.
 */
#ifndef NTCPIP__IP_DEFAULT_TTL
#define NTCPIP__IP_DEFAULT_TTL                  255
#endif

/**
 * IP_SOF_BROADCAST=1: Use the SOF_BROADCAST field to enable broadcast
 * filter per pcb on udp and raw send operations. To enable broadcast filter
 * on recv operations, you also have to set IP_SOF_BROADCAST_RECV=1.
 */
#ifndef IP_SOF_BROADCAST
#define IP_SOF_BROADCAST                0
#endif

/**
 * IP_SOF_BROADCAST_RECV (requires IP_SOF_BROADCAST=1) enable the broadcast
 * filter on recv operations.
 */
#ifndef IP_SOF_BROADCAST_RECV
#define IP_SOF_BROADCAST_RECV           0
#endif

/*
   ----------------------------------
   ---------- ICMP options ----------
   ----------------------------------
*/
/**
 * NTCPIP__LWIP_ICMP==1: Enable ICMP module inside the IP stack.
 * Be careful, disable that make your product non-compliant to RFC1122
 */
#ifndef NTCPIP__LWIP_ICMP
#define NTCPIP__LWIP_ICMP                       1
#endif

/**
 * NTCPIP__ICMP_TTL: Default value for Time-To-Live used by ICMP packets.
 */
#ifndef NTCPIP__ICMP_TTL
#define NTCPIP__ICMP_TTL                       (NTCPIP__IP_DEFAULT_TTL)
#endif

/**
 * LWIP_BROADCAST_PING==1: respond to broadcast pings (default is unicast only)
 */
#ifndef LWIP_BROADCAST_PING
#define LWIP_BROADCAST_PING             0
#endif

/**
 * LWIP_MULTICAST_PING==1: respond to multicast pings (default is unicast only)
 */
#ifndef LWIP_MULTICAST_PING
#define LWIP_MULTICAST_PING             0
#endif

/*
   ---------------------------------
   ---------- RAW options ----------
   ---------------------------------
*/
/**
 * NTCPIP__LWIP_RAW==1: Enable application layer to hook into the IP layer itself.
 */
#ifndef NTCPIP__LWIP_RAW
#define NTCPIP__LWIP_RAW                        1
#endif

/**
 * NTCPIP__LWIP_RAW==1: Enable application layer to hook into the IP layer itself.
 */
#ifndef NTCPIP__RAW_TTL
#define NTCPIP__RAW_TTL                        (NTCPIP__IP_DEFAULT_TTL)
#endif

/*
   ----------------------------------
   ---------- DHCP options ----------
   ----------------------------------
*/
/**
 * NTCPIP__LWIP_DHCP==1: Enable DHCP module.
 */
#ifndef NTCPIP__LWIP_DHCP
#define NTCPIP__LWIP_DHCP                       0
#endif

/**
 * NTCPIP__DHCP_DOES_ARP_CHECK==1: Do an ARP check on the offered address.
 */
#ifndef NTCPIP__DHCP_DOES_ARP_CHECK
#define NTCPIP__DHCP_DOES_ARP_CHECK             ((NTCPIP__LWIP_DHCP) && (NTCPIP__LWIP_ARP))
#endif

/*
   ------------------------------------
   ---------- AUTOIP options ----------
   ------------------------------------
*/
/**
 * NTCPIP__LWIP_AUTOIP==1: Enable AUTOIP module.
 */
#ifndef NTCPIP__LWIP_AUTOIP
#define NTCPIP__LWIP_AUTOIP                     0
#endif

/**
 * NTCPIP__LWIP_DHCP_AUTOIP_COOP==1: Allow DHCP and AUTOIP to be both enabled on
 * the same interface at the same time.
 */
#ifndef NTCPIP__LWIP_DHCP_AUTOIP_COOP
#define NTCPIP__LWIP_DHCP_AUTOIP_COOP           0
#endif

/**
 * LWIP_DHCP_AUTOIP_COOP_TRIES: Set to the number of DHCP DISCOVER probes
 * that should be sent before falling back on AUTOIP. This can be set
 * as low as 1 to get an AutoIP address very quickly, but you should
 * be prepared to handle a changing IP address when DHCP overrides
 * AutoIP.
 */
#ifndef LWIP_DHCP_AUTOIP_COOP_TRIES
#define LWIP_DHCP_AUTOIP_COOP_TRIES     9
#endif

/*
   ----------------------------------
   ---------- SNMP options ----------
   ----------------------------------
*/
/**
 * NTCPIP__LWIP_SNMP==1: Turn on SNMP module. UDP must be available for SNMP
 * transport.
 */
#ifndef NTCPIP__LWIP_SNMP
#define NTCPIP__LWIP_SNMP                       0
#endif

/**
 * NTCPIP__SNMP_CONCURRENT_REQUESTS: Number of concurrent requests the module will
 * allow. At least one request buffer is required. 
 */
#ifndef NTCPIP__SNMP_CONCURRENT_REQUESTS
#define NTCPIP__SNMP_CONCURRENT_REQUESTS        1
#endif

/**
 * NTCPIP__SNMP_TRAP_DESTINATIONS: Number of trap destinations. At least one trap
 * destination is required
 */
#ifndef NTCPIP__SNMP_TRAP_DESTINATIONS
#define NTCPIP__SNMP_TRAP_DESTINATIONS          1
#endif

/**
 * NTCPIP__SNMP_PRIVATE_MIB: 
 */
#ifndef NTCPIP__SNMP_PRIVATE_MIB
#define NTCPIP__SNMP_PRIVATE_MIB                0
#endif

/**
 * Only allow SNMP write actions that are 'safe' (e.g. disabeling netifs is not
 * a safe action and disabled when NTCPIP__SNMP_SAFE_REQUESTS = 1).
 * Unsafe requests are disabled by default!
 */
#ifndef NTCPIP__SNMP_SAFE_REQUESTS
#define NTCPIP__SNMP_SAFE_REQUESTS              1
#endif

/*
   ----------------------------------
   ---------- IGMP options ----------
   ----------------------------------
*/
/**
 * NTCPIP__LWIP_IGMP==1: Turn on IGMP module. 
 */
#ifndef NTCPIP__LWIP_IGMP
#define NTCPIP__LWIP_IGMP                       0
#endif

/*
   ----------------------------------
   ---------- DNS options -----------
   ----------------------------------
*/
/**
 * NTCPIP__LWIP_DNS==1: Turn on DNS module. UDP must be available for DNS
 * transport.
 */
#ifndef NTCPIP__LWIP_DNS
#define NTCPIP__LWIP_DNS                        0
#endif

/** DNS maximum number of entries to maintain locally. */
#ifndef NTCPIP__DNS_TABLE_SIZE
#define NTCPIP__DNS_TABLE_SIZE                  4
#endif

/** DNS maximum host name length supported in the name table. */
#ifndef NTCPIP__DNS_MAX_NAME_LENGTH
#define NTCPIP__DNS_MAX_NAME_LENGTH             256
#endif

/** The maximum of DNS servers */
#ifndef NTCPIP__DNS_MAX_SERVERS
#define NTCPIP__DNS_MAX_SERVERS                 2
#endif

/** DNS do a name checking between the query and the response. */
#ifndef NTCPIP__DNS_DOES_NAME_CHECK
#define NTCPIP__DNS_DOES_NAME_CHECK             1
#endif

/** DNS use a local buffer if NTCPIP__DNS_USES_STATIC_BUF=0, a static one if
    NTCPIP__DNS_USES_STATIC_BUF=1, or a dynamic one if NTCPIP__DNS_USES_STATIC_BUF=2.
    The buffer will be of size NTCPIP__DNS_MSG_SIZE */
#ifndef NTCPIP__DNS_USES_STATIC_BUF
#define NTCPIP__DNS_USES_STATIC_BUF             1
#endif

/** DNS message max. size. Default value is RFC compliant. */
#ifndef NTCPIP__DNS_MSG_SIZE
#define NTCPIP__DNS_MSG_SIZE                    512
#endif

/** DNS_LOCAL_HOSTLIST: Implements a local host-to-address list. If enabled,
 *  you have to define
 *    #define DNS_LOCAL_HOSTLIST_INIT {{"host1", 0x123}, {"host2", 0x234}}
 *  (an array of structs name/address, where address is an Uint32 in network
 *  byte order).
 *
 *  Instead, you can also use an external function:
 *  #define DNS_LOOKUP_LOCAL_EXTERN(x) extern Uint32 my_lookup_function(const char *name)
 *  that returns the IP address or NTCPIP_INADDR_NONE if not found.
 */
#ifndef DNS_LOCAL_HOSTLIST
#define DNS_LOCAL_HOSTLIST              0
#endif /* DNS_LOCAL_HOSTLIST */

/** If this is turned on, the local host-list can be dynamically changed
 *  at runtime. */
#ifndef DNS_LOCAL_HOSTLIST_IS_DYNAMIC
#define DNS_LOCAL_HOSTLIST_IS_DYNAMIC   0
#endif /* DNS_LOCAL_HOSTLIST_IS_DYNAMIC */

/*
   ---------------------------------
   ---------- UDP options ----------
   ---------------------------------
*/
/**
 * NTCPIP__LWIP_UDP==1: Turn on UDP.
 */
#ifndef NTCPIP__LWIP_UDP
#define NTCPIP__LWIP_UDP                        1
#endif

/**
 * NTCPIP__LWIP_UDPLITE==1: Turn on UDP-Lite. (Requires NTCPIP__LWIP_UDP)
 */
#ifndef NTCPIP__LWIP_UDPLITE
#define NTCPIP__LWIP_UDPLITE                    0
#endif

/**
 * NTCPIP__UDP_TTL: Default Time-To-Live value.
 */
#ifndef NTCPIP__UDP_TTL
#define NTCPIP__UDP_TTL                         (NTCPIP__IP_DEFAULT_TTL)
#endif

/**
 * NTCPIP__NETBUF_RECVINFO==1: append destination addr and port to every netbuf.
 */
#ifndef NTCPIP__NETBUF_RECVINFO
#define NTCPIP__NETBUF_RECVINFO            0
#endif

/*
   ---------------------------------
   ---------- TCP options ----------
   ---------------------------------
*/
/**
 * NTCPIP__LWIP_TCP==1: Turn on TCP.
 */
#ifndef NTCPIP__LWIP_TCP
#define NTCPIP__LWIP_TCP                        1
#endif

/**
 * NTCPIP__TCP_TTL: Default Time-To-Live value.
 */
#ifndef NTCPIP__TCP_TTL
#define NTCPIP__TCP_TTL                         (NTCPIP__IP_DEFAULT_TTL)
#endif

/**
 * NTCPIP__TCP_WND: The size of a TCP window.  This must be at least 
 * (2 * NTCPIP__TCP_MSS) for things to work well
 */
#ifndef NTCPIP__TCP_WND
#define NTCPIP__TCP_WND                         (4 * NTCPIP__TCP_MSS)
#endif 

/**
 * NTCPIP__TCP_MAXRTX: Maximum number of retransmissions of data segments.
 */
#ifndef NTCPIP__TCP_MAXRTX
#define NTCPIP__TCP_MAXRTX                      12
#endif

/**
 * NTCPIP__TCP_SYNMAXRTX: Maximum number of retransmissions of SYN segments.
 */
#ifndef NTCPIP__TCP_SYNMAXRTX
#define NTCPIP__TCP_SYNMAXRTX                   6
#endif

/**
 * NTCPIP__TCP_QUEUE_OOSEQ==1: TCP will queue segments that arrive out of order.
 * Define to 0 if your device is low on memory.
 */
#ifndef NTCPIP__TCP_QUEUE_OOSEQ
#define NTCPIP__TCP_QUEUE_OOSEQ                 (NTCPIP__LWIP_TCP)
#endif

/**
 * NTCPIP__TCP_MSS: TCP Maximum segment size. (default is 536, a conservative default,
 * you might want to increase this.)
 * For the receive side, this MSS is advertised to the remote side
 * when opening a connection. For the transmit size, this MSS sets
 * an upper limit on the MSS advertised by the remote host.
 */
#ifndef NTCPIP__TCP_MSS
#define NTCPIP__TCP_MSS                         536
#endif

/**
 * NTCPIP__TCP_CALCULATE_EFF_SEND_MSS: "The maximum size of a segment that TCP really
 * sends, the 'effective send MSS,' MUST be the smaller of the send MSS (which
 * reflects the available reassembly buffer size at the remote host) and the
 * largest size permitted by the IP layer" (RFC 1122)
 * Setting this to 1 enables code that checks NTCPIP__TCP_MSS against the MTU of the
 * netif used for a connection and limits the MSS if it would be too big otherwise.
 */
#ifndef NTCPIP__TCP_CALCULATE_EFF_SEND_MSS
#define NTCPIP__TCP_CALCULATE_EFF_SEND_MSS      1
#endif


/**
 * NTCPIP__TCP_SND_BUF: TCP sender buffer space (bytes). 
 */
#ifndef NTCPIP__TCP_SND_BUF
#define NTCPIP__TCP_SND_BUF                     256
#endif

/**
 * NTCPIP__TCP_SND_QUEUELEN: TCP sender buffer space (pbufs). This must be at least
 * as much as (2 * NTCPIP__TCP_SND_BUF/NTCPIP__TCP_MSS) for things to work.
 */
#ifndef NTCPIP__TCP_SND_QUEUELEN
#define NTCPIP__TCP_SND_QUEUELEN                (4 * (NTCPIP__TCP_SND_BUF)/(NTCPIP__TCP_MSS))
#endif

/**
 * NTCPIP__TCP_SNDLOWAT: TCP writable space (bytes). This must be less than or equal
 * to NTCPIP__TCP_SND_BUF. It is the amount of space which must be available in the
 * TCP snd_buf for select to return writable.
 */
#ifndef NTCPIP__TCP_SNDLOWAT
#define NTCPIP__TCP_SNDLOWAT                    ((NTCPIP__TCP_SND_BUF)/2)
#endif

/**
 * NTCPIP__TCP_LISTEN_BACKLOG: Enable the backlog option for tcp listen pcb.
 */
#ifndef NTCPIP__TCP_LISTEN_BACKLOG
#define NTCPIP__TCP_LISTEN_BACKLOG              0
#endif

/**
 * The maximum allowed backlog for TCP listen netconns.
 * This backlog is used unless another is explicitly specified.
 * 0xff is the maximum (Uint8).
 */
#ifndef NTCPIP__TCP_DEFAULT_LISTEN_BACKLOG
#define NTCPIP__TCP_DEFAULT_LISTEN_BACKLOG      0xff
#endif

/**
 * LWIP_TCP_TIMESTAMPS==1: support the TCP timestamp option.
 */
#ifndef LWIP_TCP_TIMESTAMPS
#define LWIP_TCP_TIMESTAMPS             0
#endif

/**
 * TCP_WND_UPDATE_THRESHOLD: difference in window to trigger an
 * explicit window update
 */
#ifndef TCP_WND_UPDATE_THRESHOLD
#define TCP_WND_UPDATE_THRESHOLD   (NTCPIP__TCP_WND / 4)
#endif

/**
 * NTCPIP__LWIP_EVENT_API and NTCPIP__LWIP_CALLBACK_API: Only one of these should be set to 1.
 *     NTCPIP__LWIP_EVENT_API==1: The user defines lwip_tcp_event() to receive all
 *         events (accept, sent, etc) that happen in the system.
 *     NTCPIP__LWIP_CALLBACK_API==1: The PCB callback function is called directly
 *         for the event.
 */
#ifndef NTCPIP__LWIP_EVENT_API
#define NTCPIP__LWIP_EVENT_API                  0
#define NTCPIP__LWIP_CALLBACK_API               1
#else 
#define NTCPIP__LWIP_EVENT_API                  1
#define NTCPIP__LWIP_CALLBACK_API               0
#endif


/*
   ----------------------------------
   ---------- Pbuf options ----------
   ----------------------------------
*/
/**
 * NTCPIP__PBUF_LINK_HLEN: the number of bytes that should be allocated for a
 * link level header. The default is 14, the standard value for
 * Ethernet.
 */
#ifndef NTCPIP__PBUF_LINK_HLEN
#define NTCPIP__PBUF_LINK_HLEN                  14
#endif

/**
 * NTCPIP__PBUF_POOL_BUFSIZE: the size of each pbuf in the pbuf pool. The default is
 * designed to accomodate single full size TCP frame in one pbuf, including
 * NTCPIP__TCP_MSS, IP header, and link header.
 */
#ifndef NTCPIP__PBUF_POOL_BUFSIZE
#define NTCPIP__PBUF_POOL_BUFSIZE               LWIP_MEM_ALIGN_SIZE(NTCPIP__TCP_MSS+40+NTCPIP__PBUF_LINK_HLEN)
#endif

/*
   ------------------------------------------------
   ---------- Network Interfaces options ----------
   ------------------------------------------------
*/
/**
 * NTCPIP__LWIP_NETIF_HOSTNAME==1: use DHCP_OPTION_HOSTNAME with netif's hostname
 * field.
 */
#ifndef NTCPIP__LWIP_NETIF_HOSTNAME
#define NTCPIP__LWIP_NETIF_HOSTNAME             0
#endif

/**
 * NTCPIP__LWIP_NETIF_API==1: Support netif api (in netifapi.c)
 */
#ifndef NTCPIP__LWIP_NETIF_API
#define NTCPIP__LWIP_NETIF_API                  0
#endif

/**
 * NTCPIP__LWIP_NETIF_STATUS_CALLBACK==1: Support a callback function whenever an interface
 * changes its up/down status (i.e., due to DHCP IP acquistion)
 */
#ifndef NTCPIP__LWIP_NETIF_STATUS_CALLBACK
#define NTCPIP__LWIP_NETIF_STATUS_CALLBACK      0
#endif

/**
 * NTCPIP__LWIP_NETIF_LINK_CALLBACK==1: Support a callback function from an interface
 * whenever the link changes (i.e., link down)
 */
#ifndef NTCPIP__LWIP_NETIF_LINK_CALLBACK
#define NTCPIP__LWIP_NETIF_LINK_CALLBACK        0
#endif

/**
 * NTCPIP__LWIP_NETIF_HWADDRHINT==1: Cache link-layer-address hints (e.g. table
 * indices) in struct ntcpip__netif. TCP and UDP can make use of this to prevent
 * scanning the ARP table for every sent packet. While this is faster for big
 * ARP tables or many concurrent connections, it might be counterproductive
 * if you have a tiny ARP table or if there never are concurrent connections.
 */
#ifndef NTCPIP__LWIP_NETIF_HWADDRHINT
#define NTCPIP__LWIP_NETIF_HWADDRHINT           0
#endif

/**
 * LWIP_NETIF_LOOPBACK==1: Support sending packets with a destination IP
 * address equal to the netif IP address, looping them back up the stack.
 */
#ifndef LWIP_NETIF_LOOPBACK
#define LWIP_NETIF_LOOPBACK             0
#endif

/**
 * LWIP_LOOPBACK_MAX_PBUFS: Maximum number of pbufs on queue for loopback
 * sending for each netif (0 = disabled)
 */
#ifndef LWIP_LOOPBACK_MAX_PBUFS
#define LWIP_LOOPBACK_MAX_PBUFS         0
#endif

/**
 * LWIP_NETIF_LOOPBACK_MULTITHREADING: Indicates whether threading is enabled in
 * the system, as netifs must change how they behave depending on this setting
 * for the LWIP_NETIF_LOOPBACK option to work.
 * Setting this is needed to avoid reentering non-reentrant functions like
 * ntcpip__tcpInput().
 *    LWIP_NETIF_LOOPBACK_MULTITHREADING==1: Indicates that the user is using a
 *       multithreaded environment like tcpip.c. In this case, netif->input()
 *       is called directly.
 *    LWIP_NETIF_LOOPBACK_MULTITHREADING==0: Indicates a polling (or NTCPIP__NO_SYS) setup.
 *       The packets are put on a list and netif_poll() must be called in
 *       the main application loop.
 */
#ifndef LWIP_NETIF_LOOPBACK_MULTITHREADING
#define LWIP_NETIF_LOOPBACK_MULTITHREADING    (!NTCPIP__NO_SYS)
#endif

/**
 * LWIP_NETIF_TX_SINGLE_PBUF: if this is set to 1, lwIP tries to put all data
 * to be sent into one single pbuf. This is for compatibility with DMA-enabled
 * MACs that do not support scatter-gather.
 * Beware that this might involve CPU-memcpy before transmitting that would not
 * be needed without this flag! Use this only if you need to!
 *
 * @todo: TCP and IP-frag do not work with this, yet:
 */
#ifndef LWIP_NETIF_TX_SINGLE_PBUF
#define LWIP_NETIF_TX_SINGLE_PBUF             0
#endif /* LWIP_NETIF_TX_SINGLE_PBUF */

/*
   ------------------------------------
   ---------- LOOPIF options ----------
   ------------------------------------
*/
/**
 * NTCPIP__LWIP_HAVE_LOOPIF==1: Support loop interface (127.0.0.1) and loopif.c
 */
#ifndef NTCPIP__LWIP_HAVE_LOOPIF
#define NTCPIP__LWIP_HAVE_LOOPIF                0
#endif

/*
   ------------------------------------
   ---------- SLIPIF options ----------
   ------------------------------------
*/
/**
 * LWIP_HAVE_SLIPIF==1: Support slip interface and slipif.c
 */
#ifndef LWIP_HAVE_SLIPIF
#define LWIP_HAVE_SLIPIF                0
#endif

/*
   ------------------------------------
   ---------- Thread options ----------
   ------------------------------------
*/
/**
 * NTCPIP__TCPIP_THREAD_NAME: The name assigned to the main tcpip thread.
 */
#ifndef NTCPIP__TCPIP_THREAD_NAME
#define NTCPIP__TCPIP_THREAD_NAME              "tcpip_thread"
#endif

/**
 * NTCPIP__TCPIP_THREAD_STACKSIZE: The stack size used by the main tcpip thread.
 * The stack size value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__TCPIP_THREAD_STACKSIZE
#define NTCPIP__TCPIP_THREAD_STACKSIZE          0
#endif

/**
 * NTCPIP__TCPIP_THREAD_PRIO: The priority assigned to the main tcpip thread.
 * The priority value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__TCPIP_THREAD_PRIO
#define NTCPIP__TCPIP_THREAD_PRIO               1
#endif

/**
 * NTCPIP__TCPIP_MBOX_SIZE: The mailbox size for the tcpip thread messages
 * The queue size value itself is platform-dependent, but is passed to
 * ntcpip__sysMboxNew() when ntcpip__tcpipInit is called.
 */
#ifndef NTCPIP__TCPIP_MBOX_SIZE
#define NTCPIP__TCPIP_MBOX_SIZE                 0
#endif

/**
 * NTCPIP__SLIPIF_THREAD_NAME: The name assigned to the slipif_loop thread.
 */
#ifndef NTCPIP__SLIPIF_THREAD_NAME
#define NTCPIP__SLIPIF_THREAD_NAME             "slipif_loop"
#endif

/**
 * SLIP_THREAD_STACKSIZE: The stack size used by the slipif_loop thread.
 * The stack size value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__SLIPIF_THREAD_STACKSIZE
#define NTCPIP__SLIPIF_THREAD_STACKSIZE         0
#endif

/**
 * NTCPIP__SLIPIF_THREAD_PRIO: The priority assigned to the slipif_loop thread.
 * The priority value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__SLIPIF_THREAD_PRIO
#define NTCPIP__SLIPIF_THREAD_PRIO              1
#endif

/**
 * NTCPIP__PPP_THREAD_NAME: The name assigned to the pppMain thread.
 */
#ifndef NTCPIP__PPP_THREAD_NAME
#define NTCPIP__PPP_THREAD_NAME                "pppMain"
#endif

/**
 * NTCPIP__PPP_THREAD_STACKSIZE: The stack size used by the pppMain thread.
 * The stack size value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__PPP_THREAD_STACKSIZE
#define NTCPIP__PPP_THREAD_STACKSIZE            0
#endif

/**
 * NTCPIP__PPP_THREAD_PRIO: The priority assigned to the pppMain thread.
 * The priority value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__PPP_THREAD_PRIO
#define NTCPIP__PPP_THREAD_PRIO                 1
#endif

/**
 * NTCPIP__DEFAULT_THREAD_NAME: The name assigned to any other lwIP thread.
 */
#ifndef NTCPIP__DEFAULT_THREAD_NAME
#define NTCPIP__DEFAULT_THREAD_NAME            "lwIP"
#endif

/**
 * NTCPIP__DEFAULT_THREAD_STACKSIZE: The stack size used by any other lwIP thread.
 * The stack size value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__DEFAULT_THREAD_STACKSIZE
#define NTCPIP__DEFAULT_THREAD_STACKSIZE        0
#endif

/**
 * NTCPIP__DEFAULT_THREAD_PRIO: The priority assigned to any other lwIP thread.
 * The priority value itself is platform-dependent, but is passed to
 * ntcpip__sysThreadNew() when the thread is created.
 */
#ifndef NTCPIP__DEFAULT_THREAD_PRIO
#define NTCPIP__DEFAULT_THREAD_PRIO             1
#endif

/**
 * NTCPIP__DEFAULT_RAW_RECVMBOX_SIZE: The mailbox size for the incoming packets on a
 * NTCPIP_NETCONN_RAW. The queue size value itself is platform-dependent, but is passed
 * to ntcpip__sysMboxNew() when the recvmbox is created.
 */
#ifndef NTCPIP__DEFAULT_RAW_RECVMBOX_SIZE
#define NTCPIP__DEFAULT_RAW_RECVMBOX_SIZE       0
#endif

/**
 * NTCPIP__DEFAULT_UDP_RECVMBOX_SIZE: The mailbox size for the incoming packets on a
 * NTCPIP_NETCONN_UDP. The queue size value itself is platform-dependent, but is passed
 * to ntcpip__sysMboxNew() when the recvmbox is created.
 */
#ifndef NTCPIP__DEFAULT_UDP_RECVMBOX_SIZE
#define NTCPIP__DEFAULT_UDP_RECVMBOX_SIZE       0
#endif

/**
 * NTCPIP__DEFAULT_TCP_RECVMBOX_SIZE: The mailbox size for the incoming packets on a
 * NTCPIP_NETCONN_TCP. The queue size value itself is platform-dependent, but is passed
 * to ntcpip__sysMboxNew() when the recvmbox is created.
 */
#ifndef NTCPIP__DEFAULT_TCP_RECVMBOX_SIZE
#define NTCPIP__DEFAULT_TCP_RECVMBOX_SIZE       0
#endif

/**
 * NTCPIP__DEFAULT_ACCEPTMBOX_SIZE: The mailbox size for the incoming connections.
 * The queue size value itself is platform-dependent, but is passed to
 * ntcpip__sysMboxNew() when the acceptmbox is created.
 */
#ifndef NTCPIP__DEFAULT_ACCEPTMBOX_SIZE
#define NTCPIP__DEFAULT_ACCEPTMBOX_SIZE         0
#endif

/*
   ----------------------------------------------
   ---------- Sequential layer options ----------
   ----------------------------------------------
*/
/**
 * NTCPIP__LWIP_TCPIP_CORE_LOCKING: (EXPERIMENTAL!)
 * Don't use it if you're not an active lwIP project member
 */
#ifndef NTCPIP__LWIP_TCPIP_CORE_LOCKING
#define NTCPIP__LWIP_TCPIP_CORE_LOCKING         0
#endif

/**
 * NTCPIP__LWIP_NETCONN==1: Enable Netconn API (require to use api_lib.c)
 */
#ifndef NTCPIP__LWIP_NETCONN
#define NTCPIP__LWIP_NETCONN                    1
#endif

/*
   ------------------------------------
   ---------- Socket options ----------
   ------------------------------------
*/
/**
 * NTCPIP__LWIP_SOCKET==1: Enable Socket API (require to use sockets.c)
 */
#ifndef NTCPIP__LWIP_SOCKET
#define NTCPIP__LWIP_SOCKET                     1
#endif

/**
 * NTCPIP__LWIP_COMPAT_SOCKETS==1: Enable BSD-style sockets functions names.
 * (only used if you use sockets.c)
 */
#ifndef NTCPIP__LWIP_COMPAT_SOCKETS
#define NTCPIP__LWIP_COMPAT_SOCKETS             1
#endif

/**
 * NTCPIP__LWIP_POSIX_SOCKETS_IO_NAMES==1: Enable POSIX-style sockets functions names.
 * Disable this option if you use a POSIX operating system that uses the same
 * names (read, write & close). (only used if you use sockets.c)
 */
#ifndef NTCPIP__LWIP_POSIX_SOCKETS_IO_NAMES
#define NTCPIP__LWIP_POSIX_SOCKETS_IO_NAMES     1
#endif

/**
 * NTCPIP__LWIP_TCP_KEEPALIVE==1: Enable TCP_KEEPIDLE, TCP_KEEPINTVL and TCP_KEEPCNT
 * options processing. Note that TCP_KEEPIDLE and TCP_KEEPINTVL have to be set
 * in seconds. (does not require sockets.c, and will affect tcp.c)
 */
#ifndef NTCPIP__LWIP_TCP_KEEPALIVE
#define NTCPIP__LWIP_TCP_KEEPALIVE              0
#endif

/**
 * NTCPIP__LWIP_SO_RCVTIMEO==1: Enable SO_RCVTIMEO processing.
 */
#ifndef NTCPIP__LWIP_SO_RCVTIMEO
#define NTCPIP__LWIP_SO_RCVTIMEO                0
#endif

/**
 * NTCPIP__LWIP_SO_RCVBUF==1: Enable SO_RCVBUF processing.
 */
#ifndef NTCPIP__LWIP_SO_RCVBUF
#define NTCPIP__LWIP_SO_RCVBUF                  0
#endif

/**
 * If NTCPIP__LWIP_SO_RCVBUF is used, this is the default value for recv_bufsize.
 */
#ifndef RECV_BUFSIZE_DEFAULT
#define RECV_BUFSIZE_DEFAULT            INT_MAX
#endif

/**
 * NTCPIP__SO_REUSE==1: Enable SO_REUSEADDR and SO_REUSEPORT options. DO NOT USE!
 */
#ifndef NTCPIP__SO_REUSE
#define NTCPIP__SO_REUSE                        0
#endif

/*
   ----------------------------------------
   ---------- Statistics options ----------
   ----------------------------------------
*/
/**
 * NTCPIP__LWIP_STATS==1: Enable statistics collection in lwip_stats.
 */
#ifndef NTCPIP__LWIP_STATS
#define NTCPIP__LWIP_STATS                      1
#endif

#if NTCPIP__LWIP_STATS

/**
 * NTCPIP__LWIP_STATS_DISPLAY==1: Compile in the statistics output functions.
 */
#ifndef NTCPIP__LWIP_STATS_DISPLAY
#define NTCPIP__LWIP_STATS_DISPLAY              0
#endif

/**
 * NTCPIP__LINK_STATS==1: Enable link stats.
 */
#ifndef NTCPIP__LINK_STATS
#define NTCPIP__LINK_STATS                      1
#endif

/**
 * NTCPIP__ETHARP_STATS==1: Enable etharp stats.
 */
#ifndef NTCPIP__ETHARP_STATS
#define NTCPIP__ETHARP_STATS                    (NTCPIP__LWIP_ARP)
#endif

/**
 * NTCPIP__IP_STATS==1: Enable IP stats.
 */
#ifndef NTCPIP__IP_STATS
#define NTCPIP__IP_STATS                        1
#endif

/**
 * NTCPIP__IPFRAG_STATS==1: Enable IP fragmentation stats. Default is
 * on if using either frag or reass.
 */
#ifndef NTCPIP__IPFRAG_STATS
#define NTCPIP__IPFRAG_STATS                    (NTCPIP__IP_REASSEMBLY || NTCPIP__IP_FRAG)
#endif

/**
 * NTCPIP__ICMP_STATS==1: Enable ICMP stats.
 */
#ifndef NTCPIP__ICMP_STATS
#define NTCPIP__ICMP_STATS                      1
#endif

/**
 * NTCPIP__IGMP_STATS==1: Enable IGMP stats.
 */
#ifndef NTCPIP__IGMP_STATS
#define NTCPIP__IGMP_STATS                      (NTCPIP__LWIP_IGMP)
#endif

/**
 * NTCPIP__UDP_STATS==1: Enable UDP stats. Default is on if
 * UDP enabled, otherwise off.
 */
#ifndef NTCPIP__UDP_STATS
#define NTCPIP__UDP_STATS                       (NTCPIP__LWIP_UDP)
#endif

/**
 * NTCPIP__TCP_STATS==1: Enable TCP stats. Default is on if TCP
 * enabled, otherwise off.
 */
#ifndef NTCPIP__TCP_STATS
#define NTCPIP__TCP_STATS                       (NTCPIP__LWIP_TCP)
#endif

/**
 * NTCPIP__MEM_STATS==1: Enable mem.c stats.
 */
#ifndef NTCPIP__MEM_STATS
#define NTCPIP__MEM_STATS                       ((NTCPIP__MEM_LIBC_MALLOC == 0) && (NTCPIP__MEM_USE_POOLS == 0))
#endif

/**
 * NTCPIP__MEMP_STATS==1: Enable memp.c pool stats.
 */
#ifndef NTCPIP__MEMP_STATS
#define NTCPIP__MEMP_STATS                      (MEMP_MEM_MALLOC == 0)
#endif

/**
 * NTCPIP__SYS_STATS==1: Enable system stats (sem and mbox counts, etc).
 */
#ifndef NTCPIP__SYS_STATS
#define NTCPIP__SYS_STATS                       (NTCPIP__NO_SYS == 0)
#endif

#else

#define NTCPIP__LINK_STATS                      0
#define NTCPIP__IP_STATS                        0
#define NTCPIP__IPFRAG_STATS                    0
#define NTCPIP__ICMP_STATS                      0
#define NTCPIP__IGMP_STATS                      0
#define NTCPIP__UDP_STATS                       0
#define NTCPIP__TCP_STATS                       0
#define NTCPIP__MEM_STATS                       0
#define NTCPIP__MEMP_STATS                      0
#define NTCPIP__SYS_STATS                       0
#define NTCPIP__LWIP_STATS_DISPLAY              0

#endif /* NTCPIP__LWIP_STATS */

/*
   ---------------------------------
   ---------- PPP options ----------
   ---------------------------------
*/
/**
 * NTCPIP__PPP_SUPPORT==1: Enable PPP.
 */
#ifndef NTCPIP__PPP_SUPPORT
#define NTCPIP__PPP_SUPPORT                     0
#endif

/**
 * NTCPIP__PPPOE_SUPPORT==1: Enable PPP Over Ethernet
 */
#ifndef NTCPIP__PPPOE_SUPPORT
#define NTCPIP__PPPOE_SUPPORT                   0
#endif

/**
 * NTCPIP__PPPOS_SUPPORT==1: Enable PPP Over Serial
 */
#ifndef NTCPIP__PPPOS_SUPPORT
#define NTCPIP__PPPOS_SUPPORT                   NTCPIP__PPP_SUPPORT
#endif

#if NTCPIP__PPP_SUPPORT

/**
 * NTCPIP__NUM_PPP: Max PPP sessions.
 */
#ifndef NTCPIP__NUM_PPP
#define NTCPIP__NUM_PPP                         1
#endif

/**
 * NTCPIP__PAP_SUPPORT==1: Support PAP.
 */
#ifndef NTCPIP__PAP_SUPPORT
#define NTCPIP__PAP_SUPPORT                     0
#endif

/**
 * NTCPIP__CHAP_SUPPORT==1: Support CHAP.
 */
#ifndef NTCPIP__CHAP_SUPPORT
#define NTCPIP__CHAP_SUPPORT                    0
#endif

/**
 * NTCPIP__MSCHAP_SUPPORT==1: Support MSCHAP. CURRENTLY NOT SUPPORTED! DO NOT SET!
 */
#ifndef NTCPIP__MSCHAP_SUPPORT
#define NTCPIP__MSCHAP_SUPPORT                  0
#endif

/**
 * NTCPIP__CBCP_SUPPORT==1: Support CBCP. CURRENTLY NOT SUPPORTED! DO NOT SET!
 */
#ifndef NTCPIP__CBCP_SUPPORT
#define NTCPIP__CBCP_SUPPORT                    0
#endif

/**
 * NTCPIP__CCP_SUPPORT==1: Support CCP. CURRENTLY NOT SUPPORTED! DO NOT SET!
 */
#ifndef NTCPIP__CCP_SUPPORT
#define NTCPIP__CCP_SUPPORT                     0
#endif

/**
 * NTCPIP__VJ_SUPPORT==1: Support VJ header compression.
 */
#ifndef NTCPIP__VJ_SUPPORT
#define NTCPIP__VJ_SUPPORT                      0
#endif

/**
 * NTCPIP__MD5_SUPPORT==1: Support MD5 (see also CHAP).
 */
#ifndef NTCPIP__MD5_SUPPORT
#define NTCPIP__MD5_SUPPORT                     0
#endif

/*
 * Timeouts
 */
#ifndef NTCPIP__FSM_DEFTIMEOUT
#define NTCPIP__FSM_DEFTIMEOUT                  6       /* Timeout time in seconds */
#endif

#ifndef NTCPIP__FSM_DEFMAXTERMREQS
#define NTCPIP__FSM_DEFMAXTERMREQS              2       /* Maximum Terminate-Request transmissions */
#endif

#ifndef NTCPIP__FSM_DEFMAXCONFREQS
#define NTCPIP__FSM_DEFMAXCONFREQS              10      /* Maximum Configure-Request transmissions */
#endif

#ifndef NTCPIP__FSM_DEFMAXNAKLOOPS
#define NTCPIP__FSM_DEFMAXNAKLOOPS              5       /* Maximum number of nak loops */
#endif

#ifndef NTCPIP__UPAP_DEFTIMEOUT
#define NTCPIP__UPAP_DEFTIMEOUT                 6       /* Timeout (seconds) for retransmitting req */
#endif

#ifndef NTCPIP__UPAP_DEFREQTIME
#define NTCPIP__UPAP_DEFREQTIME                 30      /* Time to wait for auth-req from peer */
#endif

#ifndef NTCPIP__CHAP_DEFTIMEOUT
#define NTCPIP__CHAP_DEFTIMEOUT                 6       /* Timeout time in seconds */
#endif

#ifndef NTCPIP__CHAP_DEFTRANSMITS
#define NTCPIP__CHAP_DEFTRANSMITS               10      /* max # times to send challenge */
#endif

/* Interval in seconds between keepalive echo requests, 0 to disable. */
#ifndef NTCPIP__LCP_ECHOINTERVAL
#define NTCPIP__LCP_ECHOINTERVAL                0
#endif

/* Number of unanswered echo requests before failure. */
#ifndef NTCPIP__LCP_MAXECHOFAILS
#define NTCPIP__LCP_MAXECHOFAILS                3
#endif

/* Max Xmit idle time (in jiffies) before resend flag char. */
#ifndef NTCPIP__PPP_MAXIDLEFLAG
#define NTCPIP__PPP_MAXIDLEFLAG                 100
#endif

/*
 * Packet sizes
 *
 * Note - lcp shouldn't be allowed to negotiate stuff outside these
 *    limits.  See lcp.h in the pppd directory.
 * (XXX - these constants should simply be shared by lcp.c instead
 *    of living in lcp.h)
 */
#define NTCPIP__PPP_MTU                         1500     /* Default MTU (size of Info field) */
#ifndef NTCPIP__PPP_MAXMTU
/* #define NTCPIP__PPP_MAXMTU  65535 - (PPP_HDRLEN + PPP_FCSLEN) */
#define NTCPIP__PPP_MAXMTU                      1500 /* Largest MTU we allow */
#endif
#define NTCPIP__PPP_MINMTU                      64
#define NTCPIP__PPP_MRU                         1500     /* default MRU = max length of info field */
#define NTCPIP__PPP_MAXMRU                      1500     /* Largest MRU we allow */
#ifndef NTCPIP__PPP_DEFMRU
#define NTCPIP__PPP_DEFMRU                      296             /* Try for this */
#endif
#define NTCPIP__PPP_MINMRU                      128             /* No MRUs below this */

#ifndef NTCPIP__MAXNAMELEN
#define NTCPIP__MAXNAMELEN                      256     /* max length of hostname or name for auth */
#endif
#ifndef NTCPIP__MAXSECRETLEN
#define NTCPIP__MAXSECRETLEN                    256     /* max length of password or secret */
#endif

#endif /* NTCPIP__PPP_SUPPORT */

/*
   --------------------------------------
   ---------- Checksum options ----------
   --------------------------------------
*/
/**
 * NTCPIP__CHECKSUM_GEN_IP==1: Generate checksums in software for outgoing IP packets.
 */
#ifndef NTCPIP__CHECKSUM_GEN_IP
#define NTCPIP__CHECKSUM_GEN_IP                 1
#endif
 
/**
 * NTCPIP__CHECKSUM_GEN_UDP==1: Generate checksums in software for outgoing UDP packets.
 */
#ifndef NTCPIP__CHECKSUM_GEN_UDP
#define NTCPIP__CHECKSUM_GEN_UDP                1
#endif
 
/**
 * NTCPIP__CHECKSUM_GEN_TCP==1: Generate checksums in software for outgoing TCP packets.
 */
#ifndef NTCPIP__CHECKSUM_GEN_TCP
#define NTCPIP__CHECKSUM_GEN_TCP                1
#endif
 
/**
 * NTCPIP__CHECKSUM_CHECK_IP==1: Check checksums in software for incoming IP packets.
 */
#ifndef NTCPIP__CHECKSUM_CHECK_IP
#define NTCPIP__CHECKSUM_CHECK_IP               1
#endif
 
/**
 * NTCPIP__CHECKSUM_CHECK_UDP==1: Check checksums in software for incoming UDP packets.
 */
#ifndef NTCPIP__CHECKSUM_CHECK_UDP
#define NTCPIP__CHECKSUM_CHECK_UDP              1
#endif

/**
 * NTCPIP__CHECKSUM_CHECK_TCP==1: Check checksums in software for incoming TCP packets.
 */
#ifndef NTCPIP__CHECKSUM_CHECK_TCP
#define NTCPIP__CHECKSUM_CHECK_TCP              1
#endif

/*
   ---------------------------------------
   ---------- Debugging options ----------
   ---------------------------------------
*/
/**
 * NTCPIP__LWIP_DBG_MIN_LEVEL: After masking, the value of the debug is
 * compared against this value. If it is smaller, then debugging
 * messages are written.
 */
#ifndef NTCPIP__LWIP_DBG_MIN_LEVEL
#define NTCPIP__LWIP_DBG_MIN_LEVEL              LWIP_DBG_LEVEL_ALL
#endif

/**
 * NTCPIP__LWIP_DBG_TYPES_ON: A mask that can be used to globally enable/disable
 * debug messages of certain types.
 */
#ifndef NTCPIP__LWIP_DBG_TYPES_ON
#define NTCPIP__LWIP_DBG_TYPES_ON               NTCPIP__LWIP_DBG_ON
#endif

/**
 * NTCPIP__ETHARP_DEBUG: Enable debugging in etharp.c.
 */
#ifndef NTCPIP__ETHARP_DEBUG
#define NTCPIP__ETHARP_DEBUG                    NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__NETIF_DEBUG: Enable debugging in netif.c.
 */
#ifndef NTCPIP__NETIF_DEBUG
#define NTCPIP__NETIF_DEBUG                     NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__PBUF_DEBUG: Enable debugging in pbuf.c.
 */
#ifndef NTCPIP__PBUF_DEBUG
#define NTCPIP__PBUF_DEBUG                      NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__API_LIB_DEBUG: Enable debugging in api_lib.c.
 */
#ifndef NTCPIP__API_LIB_DEBUG
#define NTCPIP__API_LIB_DEBUG                   NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__API_MSG_DEBUG: Enable debugging in api_msg.c.
 */
#ifndef NTCPIP__API_MSG_DEBUG
#define NTCPIP__API_MSG_DEBUG                   NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__SOCKETS_DEBUG: Enable debugging in sockets.c.
 */
#ifndef NTCPIP__SOCKETS_DEBUG
#define NTCPIP__SOCKETS_DEBUG                   NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__ICMP_DEBUG: Enable debugging in icmp.c.
 */
#ifndef NTCPIP__ICMP_DEBUG
#define NTCPIP__ICMP_DEBUG                      NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__IGMP_DEBUG: Enable debugging in igmp.c.
 */
#ifndef NTCPIP__IGMP_DEBUG
#define NTCPIP__IGMP_DEBUG                      NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__INET_DEBUG: Enable debugging in inet.c.
 */
#ifndef NTCPIP__INET_DEBUG
#define NTCPIP__INET_DEBUG                      NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__IP_DEBUG: Enable debugging for IP.
 */
#ifndef NTCPIP__IP_DEBUG
#define NTCPIP__IP_DEBUG                        NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__IP_REASS_DEBUG: Enable debugging in ntcpip__ipFrag.c for both frag & reass.
 */
#ifndef NTCPIP__IP_REASS_DEBUG
#define NTCPIP__IP_REASS_DEBUG                  NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__RAW_DEBUG: Enable debugging in raw.c.
 */
#ifndef NTCPIP__RAW_DEBUG
#define NTCPIP__RAW_DEBUG                       NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__MEM_DEBUG: Enable debugging in mem.c.
 */
#ifndef NTCPIP__MEM_DEBUG
#define NTCPIP__MEM_DEBUG                       NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__MEMP_DEBUG: Enable debugging in memp.c.
 */
#ifndef NTCPIP__MEMP_DEBUG
#define NTCPIP__MEMP_DEBUG                      NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__SYS_DEBUG: Enable debugging in sys.c.
 */
#ifndef NTCPIP__SYS_DEBUG
#define NTCPIP__SYS_DEBUG                       NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_DEBUG: Enable debugging for TCP.
 */
#ifndef NTCPIP__TCP_DEBUG
#define NTCPIP__TCP_DEBUG                       NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_INPUT_DEBUG: Enable debugging in tcp_in.c for incoming debug.
 */
#ifndef NTCPIP__TCP_INPUT_DEBUG
#define NTCPIP__TCP_INPUT_DEBUG                 NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_FR_DEBUG: Enable debugging in tcp_in.c for fast retransmit.
 */
#ifndef NTCPIP__TCP_FR_DEBUG
#define NTCPIP__TCP_FR_DEBUG                    NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_RTO_DEBUG: Enable debugging in TCP for retransmit
 * timeout.
 */
#ifndef NTCPIP__TCP_RTO_DEBUG
#define NTCPIP__TCP_RTO_DEBUG                   NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_CWND_DEBUG: Enable debugging for TCP congestion window.
 */
#ifndef NTCPIP__TCP_CWND_DEBUG
#define NTCPIP__TCP_CWND_DEBUG                  NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_WND_DEBUG: Enable debugging in tcp_in.c for window updating.
 */
#ifndef NTCPIP__TCP_WND_DEBUG
#define NTCPIP__TCP_WND_DEBUG                   NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_OUTPUT_DEBUG: Enable debugging in tcp_out.c output functions.
 */
#ifndef NTCPIP__TCP_OUTPUT_DEBUG
#define NTCPIP__TCP_OUTPUT_DEBUG                NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_RST_DEBUG: Enable debugging for TCP with the RST message.
 */
#ifndef NTCPIP__TCP_RST_DEBUG
#define NTCPIP__TCP_RST_DEBUG                   NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCP_QLEN_DEBUG: Enable debugging for TCP queue lengths.
 */
#ifndef NTCPIP__TCP_QLEN_DEBUG
#define NTCPIP__TCP_QLEN_DEBUG                  NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__UDP_DEBUG: Enable debugging in UDP.
 */
#ifndef NTCPIP__UDP_DEBUG
#define NTCPIP__UDP_DEBUG                       NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__TCPIP_DEBUG: Enable debugging in tcpip.c.
 */
#ifndef NTCPIP__TCPIP_DEBUG
#define NTCPIP__TCPIP_DEBUG                     NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__PPP_DEBUG: Enable debugging for PPP.
 */
#ifndef NTCPIP__PPP_DEBUG
#define NTCPIP__PPP_DEBUG                       NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__SLIP_DEBUG: Enable debugging in slipif.c.
 */
#ifndef NTCPIP__SLIP_DEBUG
#define NTCPIP__SLIP_DEBUG                      NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__DHCP_DEBUG: Enable debugging in dhcp.c.
 */
#ifndef NTCPIP__DHCP_DEBUG
#define NTCPIP__DHCP_DEBUG                      NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__AUTOIP_DEBUG: Enable debugging in autoip.c.
 */
#ifndef NTCPIP__AUTOIP_DEBUG
#define NTCPIP__AUTOIP_DEBUG                    NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__SNMP_MSG_DEBUG: Enable debugging for SNMP messages.
 */
#ifndef NTCPIP__SNMP_MSG_DEBUG
#define NTCPIP__SNMP_MSG_DEBUG                  NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__SNMP_MIB_DEBUG: Enable debugging for SNMP MIBs.
 */
#ifndef NTCPIP__SNMP_MIB_DEBUG
#define NTCPIP__SNMP_MIB_DEBUG                  NTCPIP__LWIP_DBG_OFF
#endif

/**
 * NTCPIP__DNS_DEBUG: Enable debugging for DNS.
 */
#ifndef NTCPIP__DNS_DEBUG
#define NTCPIP__DNS_DEBUG                       NTCPIP__LWIP_DBG_OFF
#endif

#endif /* __LWIP_OPT_H__ */

