/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	APIF
*
*	\brief		Asynchronous PHY driver interface.
*
*	\details	Declares types and constants used in the APIF interface.
*
*	\note
*
*	\version	\$Rev: 3657 $ \n
*				\$Date: 2019-03-22 15:21:40 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	APIF	APIF
*
*	\brief		Asynchronous PHY driver interface.
*
********************************************************************************
*
*	\details	The interface is used by EMAC drivers to communicate with a
*				PHY driver. The interface provides a common way for controlling
*				any PHY. The PHY driver internally takes care of all PHY chip
*				specific operations.
*
*				PHYIF is a synchronous version of this interface. This
*				asynchronous version should be preferred in new implementations.
*
*				The interface is based on the generic asynchronous interface
*				(GAI).
*	<pre>
*				                  APIF
*				                    :
*				+-------------+     :     +--------------+
*				| EMAC driver |     :     |  PHY driver  |
*				|             |     :     |              |
*				|             |-----:-->init()           |
*				|             |-----:-->access()         |
*				|             |     :     |              |
*				|      callback()<--:-----|              |
*				|             |     :     |              |
*				+-------------+     :     +--------------+
*				                    :
*	</pre>
*
*******************************************************************************/

#ifndef APIF_H_INCLUDED
#define APIF_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "amif.h"
#include "GAI.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	PHY driver option flags
 *
 * \details	The following flags can be used in the data field when performing
 *			the warm init operation.
 */

/*@{*/
#define APIF_OPT_SPEED_100	(1<<0)		/**<
										 * Set link speed to 100Mbps. If this
										 * flag is not set then the link speed
										 * will be set to 10Mbps.			*/
#define APIF_OPT_FULL_DUP	(1<<1)		/**<
										 * Set link mode to full duplex. If
										 * this flag is not set then the mode
										 * will be set to half duplex.		*/
#define APIF_OPT_AUTO_NEGO	(1<<2)		/**<
										 * Auto-negotiate link speed and
										 * duplex if PHY supports
										 * auto-negotiation.				*/
#define APIF_OPT_PHYINT		(1<<3)		/**<
										 * Enable PHY link state interrupt if
										 * PHY chip supports it. PHY interrupt
										 * pin will have to be connected to
										 * an MCU pin with external interrupt
										 * enabled.							*/
/*@}*/

/**
 * \name	Link status flags
 *
 * \details	The PHY driver link status poll function will use the following
 * 			flags in the link status value.
 */

/*@{*/
#define APIF_STAT_LINKCHNG	(1<<0)		/**< Link status has changed.		*/
#define APIF_STAT_LINKUP	(1<<1)		/**< Link is up.					*/
#define APIF_STAT_SPEED		(1<<3)		/**< Link speed has changed.		*/
#define APIF_STAT_SPEED100	(1<<4)		/**<
										 * Ethernet link speed 100Mbps. if
										 * this flag is not set then the speed
										 * is 10Mbps.						*/
#define APIF_STAT_DUPFULL	(1<<5)		/**<
										 * Ethernet link in full duplex mode.
										 * If this flag is not set then the link
										 * is in half duplex mode.			*/
/*@}*/

/**
 * 	This constant can be used instead of the PHY address in the driver init
 *	parameters. The PHY chip address is detected automatically if this value is
 *	used. The driver will scan trough all possible addresses and use the first
 *	PHY with the right ID. This can be used when there is only one PHY connected
 *	to the MDIO bus.
 */

#define APIF_ADDR_AUTO		0xFF

/**
 * This type is used to store operation codes.
 */

typedef enum apif_opCode {
	APIF_OP_PWRSAVE = 0,				/**< Put PHY into power save state.	*/
	APIF_OP_WAKEUP,						/**< Turn power saving off.			*/

	/**
	 * The operation should be requested after OSA has been started.
	 */
	APIF_OP_WARMINIT,

	/**
	 * The operation should be requested when a PHY link change interrupt has
	 * been generated. If the PHY does not support link change interrupts then
	 * the operation should be requested at constant intervals. An appropriate
	 * interval might be 1s.
	 *
	 * The most common type of link change is when the network cable is plugged
	 * or unplugged.
	 */
	APIF_OP_POLL,

	/**
	 *	Read raw value from a PHY register. The register number should be stored
	 *	to the reg field of the request structure. The data field will contain
	 *	the register value after the operation has completed.
	 */
	APIF_OP_READ,

	/**
	 *	Write raw value to a PHY register. The register number should be stored
	 *	to the reg field of the request structure and the data should be stored
	 *	to the data field.
	 */
	APIF_OP_WRITE
} apif_OpCode;

/**
 * APIF return codes. The codes in this enum can be extended by creating a 
 * new enum and assigning APIF_RET_1ST_DRV to the first value.
 */
 
enum apif_retCode {						/*''''''''''''''''''''''''''''''''''*/
	APIF_RET_NOT_FOUND = GAI_RET_1ST_IF,/**<
										 * Device not found. Could be wrong PHY
										 * address, faulty bus or another PHY
										 * model.							*/
	APIF_RET_BUS_ERROR,					/**< MDIO bus error.				*/
	APIF_RET_1ST_DRV					/**<
										 * This enumerator should not be used
										 * directly. The first driver specific
										 * return code should be assigned this
										 * value.							*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * 	Return code type for access function. The return code may be a GAI return
 *	code, APIF return code or a PHY driver specific return code.
 */

typedef gai_RetCode apif_RetCode;

/**
 * \brief		Type used for PHY driver instances externally.
 *
 * \details		Internally the driver has its own type.
 */

typedef void apif_PhyInst;

/**
 * \brief		Initialization value type for PHY drivers.
 *
 * \details		The PHY driver initialization constant is usually statically
 * 				allocated in project.c. A pointer to the constant is provided
 * 				to the EMAC driver FB as an initialization parameter. The EMAC
 * 				driver will then provide the pointer to the PHY driver
 * 				initialization function.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	Uint8 					phyAddr;	/**< 
										 * PHY chip address on the management
										 * bus.								*/
	amif_Interface const_P * pAmif;		/**<
										 * Pointer to AMIF interface constant.
										 * May be NULL if the PHY driver
										 * doesn’t support asynchronous
										 * operation. In that case the init
										 * parmeters must be defined with the
										 * phyif_Init type.					*/
	void **					ppSmeInst;	/**<
										 * Pointer to station management
										 * entity driver instance pointer.	*/
} apif_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief	Request type for the asynchronous interface.
 *
 * \extends	gai_Req
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	gai_Req					baseReq;	/**< Base request.					*/
	apif_OpCode				op;			/**< Requested operation.			*/
	Uint8					reg;		/**< Register number for read/write.*/
	Uint16					data;		/**< Operation parameter.			*/
} apif_Req;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief 		Functions of this type initializes the PHY driver instance.
 *	
 * \param		pInit	Pointer to the initialization values for the PHY driver.
 *
 * \return		A pointer to the new PHY driver instance.
 *
 * \details		The function must be called during FB initialization before OSA
 *				is running.
 */

typedef apif_PhyInst * apif_InitFn(apif_Init const_P * pInit);

/**
 * \brief		Type for PHY driver public asynchronous interface constants.
 *
 * \details		A constant of this type is made for each asynchronous PHY
 *				driver. A pointer to the constant is sent as a initialization
 *				parameter to the FB that will be using the PHY driver.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	gai_AccessFn *			pAccessFn;	/**< Access function.				*/
	apif_InitFn *			pInitFn;	/**< Low level init function.		*/
} apif_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
