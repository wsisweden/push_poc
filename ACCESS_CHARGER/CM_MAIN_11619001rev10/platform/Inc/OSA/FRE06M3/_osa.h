/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/_osa.h
*
*	\ingroup	OSA_CM3
*
*	\brief		Target specific OSA declarations
*
*	\details		
*
*	\note 		Should be read by the OSA.H header only.
*
*	\version
*
*******************************************************************************/

/******************************************************************************
;
;	HEADER AND INCLUDE FILES
;
******************************************************************************/

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

/******************************************************************************
;
;	CONSTANTS AND DEFINITIONS
;
******************************************************************************/

/*
 *	Signal flags for nSignal field of osa_TaskType
 */
 
#define OSA_SIGNAL_WAIT		0x01		/**< Task is waiting signal         */
#define OSA_SIGNAL_SET		0x02		/**< Task has been signaled         */

//#define OSA_DEBUG_SEMA

/******************************************************************************
;
;	DATA TYPES AND STRUCTURES
;
******************************************************************************/

typedef void (*				osa_InterruptFnPtr)(void);
typedef Uint8				osa_Status;
typedef void (*				osa__DebLogHandlerFn)(char *);
#define OSA_TASK			
typedef portTickType 		osa_TickType;

/*
 *	OSA objects.
 */

typedef struct {						/*''''''''''''''''''''''' SEMAPHORE	*/
	xSemaphoreHandle		sema;		/**< Internal to OSA                */
#ifdef OSA_DEBUG_SEMA					/*									*/
	char const_P *			pFileName;	/**< Ptr to filename				*/
	Uint32					lineNum;	/**< Line number where sema was
		 	 	 	 	 	 	 	 	 	 	 granted.					*/
	char const_P *			pTaskName;	/**< Name of task the sema was
											 granted to.					*/
#endif									/*									*/
} osa_SemaType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct osa__task {				/*'''''''''''''''''''''''''''' TASK	*/
	tskTCB					task;		/**< FreeRTOS task (must be first)  */
	Uint32					nSignal;	/**< Flags for signaling            */
	void *					pUtil;		/**< Utility ptr to the task        */
	char const_P *			pName;		/**< Name of the task               */
#ifdef OSA_DEBUG_SEMA					/*  Last blocking semaSet			*/
	char const_P *			pFileName;	/**< Ptr to filename				*/
	Uint32					lineNum;	/**< Line number where sema was
	 	 	 	 	 	 	 	 	 	 	 requested.						*/
	Uint32					linkReg;	/**< Link register value where
											 sema was requested.			*/
#endif									/*									*/
} osa_TaskType;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	DATA
;
******************************************************************************/


/******************************************************************************
;
;	MACROS
;
******************************************************************************/

#define osa_isrFastDecl(name_)		void name_(void) 
#define osa_isrOSDecl(name_)		void name_(void) 

#define osa_isrFastFn(isr_,name_)	void name_(void) 
/* #define osa_isrOSFn(isr_,name_)		void name_(void) { DISABLE; */
#define osa_isrOSFn(isr_,name_)		void name_(void) {						\
	deb_assert(NVIC_GetPriority((SCB->ICSR & 0x1FF) - 16) >= (configMAX_SYSCALL_INTERRUPT_PRIORITY >> (8 - __NVIC_PRIO_BITS))); {


#define osa_init()					
#define osa_isrEnter()				
#define osa_isrLeave()				
/* #define osa_endOSIsr				ENABLE; portEND_SWITCHING_ISR ( TRUE ); } */
#define osa_endOSIsr				} portEND_SWITCHING_ISR ( TRUE ); }

#define osa_newSemaSet(pS_)			vSemaphoreCreateBinary((pS_)->sema)
#define osa_newSemaTaken(pS_) do {											\
		vSemaphoreCreateBinary((pS_)->sema);								\
		osa_semaGet(pS_); 													\
	} while (FALSE)
#define osa_semaGet(pS_)			xSemaphoreTake((pS_)->sema, portMAX_DELAY)

/*
 * Redefine semaGet when debugging blocking tasks.
 * Set filename pointer and line number when requesting semaphore.
 * Clear file name pointer and line number after semaphore has been received.
 *
 * Note that osa_semaGet may be called before OSA is running.
 */
#ifdef OSA_DEBUG_SEMA
#undef osa_semaGet
#define osa_semaGet(pS_)													\
	{																		\
		if (osa_taskCurrent()) {											\
			register long lr asm("r14");									\
			osa_taskCurrent()->pFileName = SYS_FILE_NAME;					\
			osa_taskCurrent()->lineNum = __LINE__;							\
			osa_taskCurrent()->linkReg = lr;								\
		}																	\
																			\
		xSemaphoreTake((pS_)->sema, portMAX_DELAY);							\
																			\
		if (osa_taskCurrent()) {											\
			osa_taskCurrent()->pFileName = NULL;							\
			osa_taskCurrent()->lineNum = 0;									\
			(pS_)->pFileName = SYS_FILE_NAME;								\
			(pS_)->lineNum = __LINE__;										\
			(pS_)->pTaskName = osa_taskCurrent()->pName;					\
		}																	\
	}
#endif

#define osa_semaSet(pS_)			xSemaphoreGive((pS_)->sema)
#define osa_semaSetIsr(pS_)			{ long lHigherPriorityTaskWoken = pdFALSE; xSemaphoreGiveFromISR((pS_)->sema, &lHigherPriorityTaskWoken); } 
#define osa_semaWait(pS_,t_,pE_)											\
	{																		\
		if (xSemaphoreTake((pS_)->sema, (portTickType) (t_)) == pdTRUE)		\
		{																	\
			*(pE_) = 0;														\
		} else {															\
			*(pE_) = 1;														\
		}																	\
	}

/**
 *  Signal get / set are implemented using flags and task suspending and 
 *	resuming. 
 */
#define osa_signalGet()														\
	{																		\
		osa_TaskType * pTask_ = osa_taskCurrent();							\
		portENTER_CRITICAL();												\
		if (pTask_->nSignal & OSA_SIGNAL_SET) {								\
			pTask_->nSignal &= ~OSA_SIGNAL_SET;								\
		} else {															\
			pTask_->nSignal |= OSA_SIGNAL_WAIT;								\
			vTaskSuspend((xTaskHandle)pTask_);								\
		}																	\
		portEXIT_CRITICAL();												\
	}

#define osa_signalSend(pT_)													\
	{																		\
		portENTER_CRITICAL();												\
		if (((osa_TaskType*)(pT_))->nSignal & OSA_SIGNAL_WAIT) {			\
			((osa_TaskType*)(pT_))->nSignal &= ~OSA_SIGNAL_WAIT;			\
			vTaskResume((xTaskHandle)(pT_));								\
		} else {															\
			((osa_TaskType*)(pT_))->nSignal |= OSA_SIGNAL_SET;				\
		}																	\
		portEXIT_CRITICAL();												\
	}

#define osa_signalWait(t_,pE_)		deb_assert(0) /* Not implemented yet */

#define osa_taskingDisable()		vTaskSuspendAll()
#define osa_taskingEnable()			xTaskResumeAll()
#define osa_taskingIsDisabled()		(xTaskGetSchedulerState() == taskSCHEDULER_SUSPENDED)
#define osa_taskCurrent()			((osa_TaskType *)xTaskGetCurrentTaskHandle())
#define osa_taskResume(pT_)			vTaskResume((xTaskHandle)(pT_))
#define osa_taskSuspend()			vTaskSuspend((xTaskHandle)osa_taskCurrent())
#define osa_taskWait(t_)			vTaskDelay((portTickType)t_)

#define osa_tick					xPortSysTickHandler
#define	osa_getTick()				xTaskGetTickCount()

#define osa_getTaskName(p_)			((p_)->pName)

/******************************************************************************
;
;	FUNCTION PROTOTYPES
;
******************************************************************************/

void osa_newTask(osa_TaskType *,char const_P *,Uint8,void (OSA_TASK *)(void),Uint16);

/* FreeRTOS function */
extern void xPortSysTickHandler( void );

