/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		memdump.h
*
*	\ingroup	MEMDUMP
*
*	\brief		Public declarations of the MEMDUMP FB.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 2778 $ \n
*				\$Date: 2015-05-07 10:12:28 +0300 (to, 07 touko 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef MEMDUMP_H_INCLUDED
#define MEMDUMP_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "memdrv.h"
#include "drv1.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * \brief	MEMDUMP initialization parameters.
 *
 * \details	A variable of this type should be defined in project.c. A pointer
 *			to that variable should then be passed to memdump_init.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	Uint32				memSize;		/**< Memory size in bytes.			*/
	memdrv_Interface const_P * drvIf;	/**< Ptr to memory driver interface.*/
	void const_P *		pUartCfg;		/**< Port driver config parameters.	*/
	drv1_Interface const_P * uartIf;	/**< Pointer to port driver interface.*/
	sys_FBInstId		drvInstId;		/**< Memory driver instance id if it
										 * has one.							*/
	Uint16				regBuffSize;	/**< Size of REG buffer.			*/
	Uint32				startAddr;		/**< Start address.					*/
} memdump_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
 
/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void					memdump_init(memdump_Init const_P *,void *);

/* UART interfacing functions */
extern void					memdump_dataSent(void *);
extern void					memdump_dataReceived(Uint8,void *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
