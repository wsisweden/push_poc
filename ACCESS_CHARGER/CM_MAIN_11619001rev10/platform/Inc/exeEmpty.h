/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	EXE
*
*	\brief		Declare empty EXE macros for all undefined EXE macros.
*
*	\details	This file should only be included in T-Plat.E header files.
*				it is used to declare empty EXE_... macros for all undefined
*				EXE macros.
*	
*	\note		
*
*	\version
*
*******************************************************************************/

/* Extern register references */
#ifndef EXE_REG_EX
# define EXE_REG_EX(e,n,i)
#endif
#ifndef EXE_REG_EXPORT
# define EXE_REG_EXPORT(e,n)
#endif
#ifndef EXE_REG_IMPORT
#define EXE_REG_IMPORT(e,i)
#endif

/* Indication macros */
#ifndef EXE_IND_ME
#define EXE_IND_ME(e,n)
#endif
#ifndef EXE_IND_LOC
#define EXE_IND_LOC(e,n,f)
#endif
#ifndef EXE_IND_EX
#define EXE_IND_EX(e,n,i)
#endif
#ifndef EXE_IND_EXT
#define EXE_IND_EXT(e,f)
#endif

/* Register macros */
#ifndef EXE_REG_NS
#define EXE_REG_NS(e,f,s,d)
#endif
#ifndef EXE_REG_NN
#define EXE_REG_NN(e,f,a,t,d,l,u)
#endif
#ifndef EXE_BUF_NN
#define EXE_BUF_NN(e,f,a,t,d,l,u)
#endif
#ifndef EXE_REG_NT
#define EXE_REG_NT(e,f,a,t,k)
#endif
#ifndef EXE_BUF_NT
#define EXE_BUF_NT(e,f,a,t,k)
#endif
#ifndef EXE_REG_VN
#define EXE_REG_VN(e,f,a,t,d,l,u)
#endif
#ifndef EXE_BUF_VN
#define EXE_BUF_VN(e,f,a,t,d,l,u)
#endif
#ifndef EXE_REG_VT
#define EXE_REG_VT(e,f,a,t,k)
#endif
#ifndef EXE_BUF_VT
#define EXE_BUF_VT(e,f,a,t,k)
#endif
#ifndef EXE_REG_VS
#define EXE_REG_VS(e,f,s,d)
#endif
#ifndef EXE_REG_PN
#define EXE_REG_PN(e,f,a,t,l,u)
#endif
#ifndef EXE_REG_PT
#define EXE_REG_PT(e,f,a,t)
#endif
#ifndef EXE_REG_PS
#define EXE_REG_PS(e,f,s)
#endif
#ifndef EXE_REG_BL
#define EXE_REG_BL(e,f,h,w,r,c)
#endif

/* MMI attribute macros */
#ifndef EXE_MMI_NONE
#define EXE_MMI_NONE(e)
#endif
#ifndef EXE_MMI_INT
#define EXE_MMI_INT(e,z,m,f,q)
#endif
#ifndef EXE_MMI_REAL
#define EXE_MMI_REAL(e,z,m,f,l,u,p,q)
#endif
#ifndef EXE_MMI_MASK
#define EXE_MMI_MASK(e,m,f,b,q)
#endif
#ifndef EXE_MMI_STR
#define EXE_MMI_STR(e,m,f)
#endif
#ifndef EXE_MMI_ENUM
# define EXE_MMI_ENUM(e,z,m,f,v)
#endif
#ifndef EXE_MMI_DATE
# define EXE_MMI_DATE(e,z,m,f,q)
#endif
#ifndef EXE_MMI_TIME
# define EXE_MMI_TIME(e,z,m,f,q)
#endif

/* Register help text macros */
#ifndef EXE_HELP_EN
# define EXE_HELP_EN(e,j,z,xS,xL)
#endif

/* Text macros */
#ifndef EXE_TXT
# define EXE_TXT(e,v)
#endif

/* ERR macros */
#ifndef EXE_ERR_CLASS
# define EXE_ERR_CLASS(e)
#endif
#ifndef EXE_ERR_TYPE
# define EXE_ERR_TYPE(e,x,y,f)
#endif

/* MSG macros */
#ifndef MSG_JOIN
# define MSG_JOIN(name_)
#endif
