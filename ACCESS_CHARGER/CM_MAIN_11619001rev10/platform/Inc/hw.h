/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2004, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		HW.H
*
*	\brief		Hardware header for controllers.
*
*	\details	This header includes target specific hardware related headers.
*
*	\note 		This file should be included instead of directly including 
*				target specific _hw.h.
*
* 	\version	8.4.2004 / Juha Ylinen	
*
*******************************************************************************/

#ifndef	HW_H_INCLUDED
#define HW_H_INCLUDED

#ifndef	TOOLS_H_INCLUDED
# error	"TOOLS.H must be included before HW.H"
#endif

#include "_HW.H"

#endif
