/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2004, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		FIFOC.H
*
*	\ingroup	FIFOC
*
*	\brief		Fifo
*
*	\details	
*
*	\note		
*
*	\version	
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#define fifoc_getSize(f_)	((f_)->nLenTotal)
#define fifoc_clear(f_)		((f_)->nLen = (f_)->nLenTotal = 0)

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef Uint8				fifoc_CounterType;

/**
 * Fifo type
 */
 
typedef struct {
	Uint8					nItems;
	Uint16					nItemSize;

	Uint8					nFirst;
	Uint8					nLen;
	Uint16					nLenTotal;
} fifoc_Type;

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PUBLIC fifoc_Type *			fifoc_create(Uint8, Uint16);
PUBLIC Boolean				fifoc_put(fifoc_Type *, const_D void *);
PUBLIC Boolean				fifoc_get(fifoc_Type *, void *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */
