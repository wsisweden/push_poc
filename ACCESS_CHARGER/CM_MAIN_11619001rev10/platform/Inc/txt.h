/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		TXT.H
*
*	\ingroup	TXT
*
*	\brief		Public declarations of the language dependent text support.
*
*	\details		
*
*	
*	\note		The header named INIT.H (the definition of e.g. the contents of
*				the register file) must be present. If not used, a dummy INIT.H
*				should be created.
*	\version
*
*******************************************************************************/

#ifndef TXT_H_INCLUDED
#define TXT_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#ifndef SYS_H_INCLUDED
# error SYS.H must be included before TXT.H
#endif

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef unsigned char		txt_LenType;

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

Uint8 txt_getLanguage(void);
Uint8 txt_setLanguage(Uint8);
sys_TxtHandle txt_localHandle(sys_FBInstId,sys_TxtIndex);
unsigned char const_P *txt_localText(sys_FBInstId,sys_TxtIndex);
unsigned char const_P *txt_text(sys_TxtHandle);
txt_LenType txt_localLen(sys_FBInstId,sys_TxtIndex);
txt_LenType txt_len(sys_TxtHandle);
txt_LenType txt_localCpy(void *,sys_FBInstId,sys_TxtIndex);
txt_LenType txt_cpy(void *,sys_TxtHandle);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/


/***************************************************************//** \endcond */

/*******************************************************************************
;
;	BUILD THE ENUMERATED LIST OF LOCAL TEXT HANDLES
;
;-----------------------------------------------------------------------------*/

#ifndef EXE_H_INCLUDED /* Only when EXE.H not included, i.e. non-PROJECT.C */
#ifndef TXT_SKIP_INIT_H

#define EXE_BEGIN					enum {
#define EXE_END						txt_i__dim };

#define EXE_TXT(e,v)				txt_i_##e,

#include "exeEmpty.h" 		/* Define default empty exe macros for the rest */

#include "init.h" 			/* The once-only declarations */

/*
 *	Enumerate local texts.
 */

#define EXE_MODE 0

#include "init.h"			/* Enumerate the texts of the FB (txt_i_...) */

#undef EXE_END
#define EXE_END						txt_index__dim };
#undef EXE_TXT
#define EXE_TXT(e,v)				txt_index_##e,

#include "init.h" 			/* Enumerate the texts of the FB (txt_index_...) */

#undef EXE_BEGIN
#undef EXE_END

#include "exeUndef.h"		/* Undefine all exe macros */

#undef EXE_MODE

#endif /* TXT_SKIP_INIT_H */
#endif /* EXE_H_INCLUDED */

/******************************************************************************/

#endif /* TXT_H_INCLUDED */
