/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		PROTIF.H
*
*	\ingroup	PROTIF
*
*	\brief		Protocol handler interface
*
*	\note		
*
*	\version	\$Rev: 3657 $ \n
*				\$Date: 2019-03-22 15:21:40 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	PROTIF	PROTIF
*
*	\brief		Protocol handler interface.
*
********************************************************************************
*
*	\details	The protocol handler interface is used by bus masters like 
*				IICMSTR and	SPIM. The interface can also be used by other FBs
*				that handles some kind of low level protocol.
*
*				The protocol handler is usually called from a driver that
*				controls an external device. The protocol handler takes care of
*				the low-level protocol, such as SPI and I2C bus. The driver
*				handles a higher level protocol to communicate with the slave.
*	
*				The interface makes it possible for several different types of
*				drivers to communicate with the same protocol handler. It is
*				also possible to use different protocol handlers for the same
*				driver.
*	<pre>
*				                 PROTIF                        
*				                   |                           
*				+-------------+    |    +------------------+
*				|  FB         |    |    | Protocol handler |
*				|             |    |    |                  |   Communication
*				|             |----|--------> access()     |   through some
*				|             |    |    |                  |   protocol
*				|             |    |    |                  |*****************
*				|             |    |    |                  |
*				| callback() <-----|----|                  |
*				|             |    |    |                  |
*				|             |    |    |                  |
*				+-------------+    |    +------------------+
*	</pre>
*		
*******************************************************************************/

#ifndef PROTIF_H_INCLUDED
#define PROTIF_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	\name	Request flags
 *
 * 	The data element structure has a flags field where the following flags can
 *	be used. It is possible to combine multiple flags by using the OR-operator.
 *
 *	The bits 7:5 (three most significant bits) of the flags field is reserved
 *	for protocol handler specific flags. Each protocol handler may specify own
 *	meanings for these bits. Protocol handler specific flags should be avoided
 *	if possible.
 * 
 *	\{
 */
#define PROTIF_FLAG_READ	(1<<0)		/**< Data should be read to the location
										 * specified by the pData pointer.	*/
#define PROTIF_FLAG_WRITE	(1<<1)		/**< Data should be written from the
										 * location specified by the pData
										 * pointer.							*/
#define PROTIF_FLAG_CONT	(1<<2)		/**< This data should be handled as if
										 * it was part of the previous element.
										 * This may have a meaning for some
										 * protocol handlers e.g. IICMSTR FB.*/
#define PROTIF_FLAG_RES		(1<<3)		/**< Indicates that the request comes
										 * from the driver that has reserved the
										 * protocol handler. This flag may only
										 * be used by the driver that has
										 * reserved the protocol handler.	*/
/**
 *	Indicates that a callback function is used instead of the pNext pointer in
 *	the data element.
 *
 *	When the protocol handler is handling the request and moves to the next data
 *	element then the callback function will be called to get the next data
 *	element.
 *
 *	This is useful if the data that should be sent depends on the received data.
 *	The data element callback function may be called from an interrupt handler.
 *	In all cases interrupts are disabled when the data callback function is
 *	called.
 */

#define PROTIF_FLAG_CB		(1<<4)		
#define PROTIF_FLG_BITS		5			/**< Protif flag mask bit count		*/
#define PROTIF_FLG_MSK		0x1F		/**< Protif request flag mask		*/
#define PROTIF_USER_FLG_MSK	0xE0		/**< Master specific flags mask		*/
/**\}*/

/**
 * 	\name	Special size values
 *
 *	\brief	Used in the size field when the pData field is NULL.
 *
 *	\{
 */
#define PROTIF_REQ_RESERVE	0			/**< Reserve bus.					*/
#define PROTIF_REQ_RELEASE	1			/**< Release bus.					*/
/**\}*/

/**
 * Return codes that the protocol handler can send to the callback function.
 */
 
enum protif_retCodes {					/*''''''''''''''''''''''''''''''''''*/
	PROTIF_OK,							/**< Request completed successfully.*/
	PROTIF_ERROR,						/**< The request could not be handled.
										 * This may be a temporary error. The
										 * driver could try to resend the
										 * request.							*/
	PROTIF_NACK,						/**< The slave returned a NACK. This
										 * is only returned from some protocol
										 * handlers e.g. IICMSTR FB.		*/
	PROTIF_TIMEOUT,						/**< Timeout error.					*/
	PROTIF_INVALID_RESULT = 0xFF		/**< This value is never returned by
										 * a protocol handler. It may be used
										 * internally in the driver.		*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef struct protif__Data protif_Data;
typedef struct protif__Request protif_Request;

/**
 * \brief	Function to get the next data block.
 *
 * \param	pReq	Pointer to request.
 * \param	pData	Pointer to current data block.
 *
 * \return 	Pointer to the next data block.
 *
 * \details	A function of this type can be used to make the protocol handler
 *			call a function to get the next data block. When using this feature
 *			a function pointer is set to the protif_Data.callback field.
 */
 
typedef protif_Data * (*protif_dataCallback)(protif_Request *, protif_Data *);

/** 
 * Read / write request data. Each request contains a linked list of data 
 * structures. The protocol handler handles the data structures in the list 
 * order. The data will be read from or written to the location pointed to by
 * the pData pointer. pNext of the last data structure in a request should be 
 * set to NULL.
 */
 
struct protif__Data	{					/*''''''''''''''''''''''''''''' RAM */
	union {								/*									*/
		protif_Data *		pNext;		/**< Ptr to next data structure.	*/
		protif_dataCallback	callback;	/**< Data block handled callback.	*/
	};
	BYTE *					pData;		/**< Ptr to data.					*/
	Uint16					size;		/**< Data size in bytes.			*/
	Uint8					flags;		/**< Read / write etc.				*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief	Function type used when a request has been handled.
 *	
 * \param	retCode		Result code containing the result of the operation.
 * \param	pRequest	Pointer to the request that has been handled.
 *
 * \details	The callback function can immediately call the protocol handler
 *			access function again if needed. This reduces the need for an own
 *			task in the driver.
 */

typedef void protif__FnCallback(Uint8 retCode, struct protif__Request * pRequest);

/** 
 * 	This type is used to create request objects for the protocol handler. The
 *	structure contains all information that is needed for a protocol operation.
 *
 *	To perform an operation on a protocol the object is filled with request
 *	information and a pointer to the object is supplied to the access function
 *	of the protocol handler. The callback function will be called when the
 *	operation has been handled.
 *
 *	The request can be allocated in any way as long as it stays allocated until
 *	the callback function is called. Usually one request is allocated during
 *	startup in each function block that uses the PROTIF interface.
 *
 *	After the access function has been called the request may only be modified
 *	by the protocol handler until the callback function is called.
 */
 
struct protif__Request {				/*''''''''''''''''''''''''''''' RAM */
	protif_Request *		pNext;		/**< Pointer to the next request in
										 * queue. Used by protocol handlers
										 * to queue requests.         		*/
	void const_P *			pTargetInfo;/**< Destination information. Usually
										 * some sort of address. The type of
										 * this information depends on which
										 * protocol handler is used. With this
										 * information the protocol handler is
										 * able to address the correct slave.*/
	protif_Data				data;		/**< First data element of the request.
										 * May contain a link to the next data
										 * element if there is one.   		*/
	protif__FnCallback *	fnCallback;	/**< Pointer to callback function that
										 * the protocol handler calls when the
										 * request is handled.				*/
	void *					pUtil;		/**< Pointer that can be used to supply
										 * data to the callback function. The
										 * protocol handler does not use this
										 * pointer. It can be set to point to
										 * any data.						*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/** 
 * \brief	Function pointer type used to send requests to the protocol handler.
 *	
 * \param	pInst		Pointer to the protocol handler instance.
 * \param	pRequest	Pointer to a PROTIF request.
 *
 * \details	Each protocol handler must implement a function of this type. The
 *			function is called when a request has to be handled. The request is
 *			not necessarily handled instantly. The protocol handler may queue
 *			the received requests.
 */

typedef void (* protif_fnAccess)(void * pInst, protif_Request * pRequest);	

/** 
 * 	Type used to define a public interface constant in the protocol handler. The
 *	structure contains a function pointer, which should point to the access
 *	function of the protocol handler.
 *
 *	The protocol hander is accessed through the public constant of this type.
 */
 
typedef struct {						/*''''''''''''''''''''''''''' CONST */
	protif_fnAccess			fnAccess;	/**< Ptr to the access function.	*/
} protif_Interface;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
