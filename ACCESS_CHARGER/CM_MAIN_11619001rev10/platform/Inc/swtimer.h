/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SWTIMER.H
*
*	\ingroup	SWTIMER
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef SWTIMER_H_INCLUDED
#define SWTIMER_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED == TARGET_W32
#define SWTIMER_TICK		1
#else
#define SWTIMER_TICK		1
#endif

/** 
 *	\brief		Sets timer delay and reload timers.
 *	
 *	\param		t_		Pointer to the timer object.
 *	\param		d_		New timer delay time (milliseconds). This is the initial
 *						delay time after which the timer expires for the first
 *						time.
 *	\param		r_		New timer reload time (milliseconds). This value will be
 *						reloaded to the \a nDelay variable of the timer 
 *						structure after timer expires and the timer is started 
 *						again. Use value 0 for one shot timers.
 *	
 *	\details	Only call this on timers that are not running and have been
 *				initialized with swtimer_new.
 */

#define swtimer_setTimes(t_,d_,r_) do {										\
		(t_)->nDelay	= (d_) / SWTIMER_TICK;								\
		(t_)->nReload	= (r_) / SWTIMER_TICK;								\
} while (0)

/**
 * \brief	Set the utility pointer.
 *
 * \param	pTimer_		Pointer to the timer structure.
 * \param	pUtil_		The utility pointer.
 *
 * \details	The utility pointer is not used by the swtimer. It may be used to
 *			store any pointer that needs to be accessed in the timer expiration
 *			function.
 */

#define swtimer_setUtilPtr(pTimer_, pUtil_) (pTimer_)->pUtil = (pUtil_)

/**
 * \brief	Get the utility pointer.
 *
 * \param	pTimer_		Pointer to the timer structure.
 *
 * \return	The utility pointer that has been stored to the swtimer.
 */

#define swtimer_getUtilPtr(pTimer_) ((pTimer_)->pUtil)

/** 
 *	Maximum allowed delay value for SWTIMER.
 */
#define SWTIMER_MAX_DELAY	(~((timer_CountType)0))

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef Uint32				swtimer_CountType;

struct swtimer__Struct;

/** 
 *	\brief	Software timer expiration function type.
 *	\param	pTimer	Pointer to the software timer that expired.
 */
typedef void (*				swtimer_Exp_f)(struct swtimer__Struct * pTimer);

/** 
 *	Timer link type.
 */
typedef struct stSwTimerLinkTag {			/*''''''''''''''''''''''''''''''*/
	struct stSwTimerLinkTag *	pNext;		/**< Link to next timer.		*/
} swtimer_Link_st;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/** 
 *	\brief		SWTIMER timer type. 
 *	\details	One instance of this structure is allocated for one timer.
 *	\note		Only field swtimer_Timer_st::pUtil should be manually set, other
 *				fields should be modified using swtimer_new or swtimer_setTimes.
 */
typedef struct swtimer__Struct {			/*''''''''''''''''''''''''''''''*/
	swtimer_Link_st			link;			/**< Link to next timer.		*/
	swtimer_CountType		nDelay;			/**< Delay of this timer.		*/
	swtimer_CountType		nReload;		/**< Reload value.				*/
	swtimer_Exp_f			fn;				/**< Callback function.			*/
	/** 
	 *	Utility pointer not used by the SWTIMER block itself. This can be used 
	 *	to, for example, gain access to the function block instance pointer in 
	 *	the timer expiration callback function.
	 */
	void *					pUtil;			
} swtimer_Timer_st;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* API functions */
void swtimer_init(void);
void swtimer_start(swtimer_Timer_st * pTimer);
void swtimer_stop(swtimer_Timer_st * pTimer);
void swtimer_new(swtimer_Timer_st * pTimer, swtimer_Exp_f pExpFn, swtimer_CountType delay, swtimer_CountType reload);

/* Implementation functions */
void swtimer_tick(void);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
