/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		REAL.H
*
*	\ingroup	REAL
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	1.7.2004   14:25
*
*******************************************************************************/

#ifndef REAL_H_INCLUDED
#define REAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#ifndef TOOLS_H_INCLUDED
# error TOOLS.H must be included before REAL.H
#endif

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define	REAL__FIX				10U
#define REAL__FRACT_MASK		0x3FFU
#define	REAL__SIGN_MASK			0x80000000UL
#define REAL__BITS				(sizeof(Real) * 8)

/*
 *		sign(1), digit-part(7), decimal-point(1), decimal-part(3), NULL(1)
 *			= 13
 */	

#define	REAL_MAXSTR				13	

#define REAL_MAX_DIGITS			3 

#define REAL_MAX				0x7FFFFFFFL
#define REAL_MIN				-0x7FFFFFFFL

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef	Uint32					Real;

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*
 *	Internal
 */

#define	real___new(int_, dec_)												\
	( ((Uint32)(int_)<<REAL__FIX) + ((((dec_)*1024UL)+500UL)/1000UL) )	

#define real_getDec(pR_)													\
	((((*(pR_) & REAL__FRACT_MASK) * 1000) + 500) / 1024)

/*
 *	Public
 *	Target independent.
 */

/** 
 *	\brief	Creates a positive constant Real value.
 *	\param	int_	Integer part.
 *	\param	dec_	Decimal part (0 - 999).
 *	\return	Created constant value.
 */
#define	real_constP(int_, dec_)	((Real)((Int32)real___new((int_), (dec_))) )

/** 
 *	\brief	Creates a negative constant Real value.
 *	\param	int_	Integer part.
 *	\param	dec_	Decimal part (0 - 999).
 *	\return	Created constant value.
 */
#define	real_constN(int_, dec_)	((Real)( -(Int32)real___new((int_), (dec_))) )

/** 
 *	\brief	Creates a positive Real value.
 *	\param	int_	Pointer to integer part.
 *	\param	dec_	Pointer to decimal part (0 - 999).
 *	\return	Created Real value.
 */
#define	real_newP(int_, dec_)	( real__new((int_),(dec_), FALSE) )

/** 
 *	\brief	Creates a negative Real value.
 *	\param	int_	Pointer to integer part.
 *	\param	dec_	Pointer to decimal part (0 - 999).
 *	\return	Created Real value.
 */
#define	real_newN(int_, dec_)	( real__new((int_),(dec_), TRUE) )

/** 
 *	\brief	Increments Real value by one.
 *	\param	pR_		Pointer to Real value to increment.
 */
#define	real_inc(pR_)			( *(pR_) += 1UL << REAL__FIX )

/** 
 *	\brief	Decrements Real value by one.
 *	\param	pR_		Pointer Real value to decrement.
 */
#define	real_dec(pR_)			( *(pR_) -= 1UL << REAL__FIX )

/** 
 *	\brief	Adds Real value to another.
 *	\param	pR_		Pointer to Real value where the pR2_ is added to.
 *	\param	pR2_	Pointer to Real value to add to pR_.
 */
#define	real_add(pR_,pR2_)		( *(pR_) += *(pR2_) )

/** 
 *	\brief	Substracts Real value from another.
 *	\param	pR_		Pointer to Real value where the pR2_ is substracted from.
 *	\param	pR2_	Pointer to Real value to substract from pR_.
 */
#define	real_sub(pR_,pR2_)		( *(pR_) -= *(pR2_) )

/** 
 *	\brief	Copies value of Real value to another.
 *	\param	pR_		Pointer to Real value where the pR2_ will be copied to.
 *	\param	pR2_	Pointer to Real value to copy.
 */
#define	real_cpy(pR_,pR2_)		( *(pR_) = *(pR2_) )

/** 
 *	\brief	Greater comparison or two Real values.
 *	\param	pR_		Pointer to Real value 1.
 *	\param	pR2_	Pointer to Real value 1.
 *	\retval	TRUE	pR1_ > pR2_ 	
 *	\retval	FALSE	pR1_ <= pR2_
 */
#define	real_isMore(pR_,pR2_)	( (Int32)*(pR_)>(Int32)*(pR2_) ? TRUE : FALSE )

/** 
 *	\brief	Less comparison or two Real values.
 *	\param	pR_		Pointer to Real value 1.
 *	\param	pR2_	Pointer to Real value 1.
 *	\retval	TRUE	pR1_ < pR2_ 	
 *	\retval	FALSE	pR1_ >= pR2_
 */
#define	real_isLess(pR_,pR2_)	( (Int32)*(pR_)<(Int32)*(pR2_) ? TRUE : FALSE )

/** 
 *	\brief	Greater or equal comparison or two Real values.
 *	\param	pR_		Pointer to Real value 1.
 *	\param	pR2_	Pointer to Real value 1.
 *	\retval	TRUE	pR1_ >= pR2_ 	
 *	\retval	FALSE	pR1_ < pR2_
 */
#define	real_isMoreEq(pR_,pR2_)	( (Int32)*(pR_)>=(Int32)*(pR2_) ? TRUE : FALSE )

/** 
 *	\brief	Less or equal comparison or two Real values.
 *	\param	pR_		Pointer to Real value 1.
 *	\param	pR2_	Pointer to Real value 1.
 *	\retval	TRUE	pR1_ <= pR2_ 	
 *	\retval	FALSE	pR1_ > pR2_
 */
#define	real_isLessEq(pR_,pR2_)	( (Int32)*(pR_)<=(Int32)*(pR2_) ? TRUE : FALSE )

/** 
 *	\brief	Converts Real value to positive value.
 *	\param	pR_		Pointer to Real value to convert.
 */
#define	real_toPos(pR_)			( real__toPos(pR_) )

/** 
 *	\brief	Multiplies Real value by an integer (pR_ = pR_ * int_).
 *	\param	pR_		Pointer to Real value to multiply. Value will be modified.
 *	\param	int_	Integer value used to multiply pR_.
 */
#define	real_mulInt(pR_, int_)	( *(pR_) *= (int_) )

/** 
 *	\brief	Divides Real value by an integer (pR_ = pR_ / int_).
 *	\param	pR_		Pointer to Real value to divide. Value will be modified.
 *	\param	int_	Integer value used to divide pR_.
 */
#define	real_divInt(pR_, int_)	( *(pR_) /= (int_) )

/** 
 *	\brief	Adds integer to Real value (pR_ = pR_ + int_).
 *	\param	pR_		Pointer to Real value. Value will be modified.
 *	\param	int_	Integer value added to pR_.
 */
#define real_addInt(pR_, int_)	((*pR_) += ((Int32)(int_) << REAL__FIX))

/** 
 *	\brief	Substract integer from Real value (pR_ = pR_ - int_).
 *	\param	pR_		Pointer to Real value. Value will be modified.
 *	\param	int_	Integer value substracted from pR_.
 */
#define real_subInt(pR_, int_)	((*pR_) -= ((Int32)(int_) << REAL__FIX))

/** 
 *	\brief	Cheks if Real value is negative.
 *	\param	pR_		Pointer to Real value.
 *	\retval	TRUE	pR1_ < 0 	
 *	\retval	FALSE	pR1_ >= 0
 */
#define real_isNeg(pR_)			!!(*(pR_) & 0x80000000UL)

/*	TODO: real <-> int conversions only works with positive values!	*/
#define real_toInt(pR_)			((Int32)((*pR_) >> REAL__FIX))
#define real_fromInt(pR_, int_)	((*pR_) = ((Int32)(int_) << REAL__FIX))

/** 
 *	\brief	Conerts string to Real.
 *	\param	pS_		Pointer to string containing the Real value. Spaces at the
 *					beginning are skipped.
 *	\param	pR_		Pointer to resulting Real value.
 *	\return	Pointer to character that stopped the parsing.
 */
#define	real_fromStr(pS_, pR_)	( real__fromStr(pS_, pR_) )

/*	
 *  Target dependent.
 */

#if TARGET_SELECTED & TARGET_AVR /* TODO: these shouldn't be here */

#define	real_mul(pR_,pR2_)		( real__mulAvr((pR_), (pR2_)) )
#define	real_div(pR_,pR2_)		( real__divAvr((pR_), (pR2_)) )

#elif TARGET_SELECTED & TARGET_M16C

#define	real_mul(pR_,pR2_)		(*(pR_) = real__mul(*(pR_), *(pR2_)))
#define	real_div(pR_,pR2_)		(*(pR_) = real__div(*(pR_), *(pR2_)))

#else

#define	real_mul(pR_,pR2_)		real__mul(pR_,pR2_)
#define	real_div(pR_,pR2_)		real__div(pR_,pR2_)

#endif

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*
 *	real.c
 */

Real						real__new(Uint32*, Uint16*, Boolean);
void						real__toPos(Real*);

//char *		real__toStr(Real *, char *, Uint8);
char *						real__toStrEx(Real *, char *, char *, Uint8);
char *						real__fromStr(char *, Real *);

void						real__mul(Real*, Real*);
void						real__div(Real*, Real*);


#if TARGET_SELECTED & TARGET_AVR 
/*
 *	Assembly routines
 */
void						real__mulAvr(Real*, Real*);
void						real__divAvr(Real*, Real*);
#endif

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
