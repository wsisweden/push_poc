@echo off
rem ****************************************************************************
rem *	
rem *	This batch Runs doxygen and proccesses the output to a easily readable
rem *	format like RTF or HTML.
rem *
rem * 	Copyright (c) 2005, TJK Tietolaite Oy.
rem *
rem *	Orginal:	10-10-2008 / Ari Suomi
rem *	
rem *	Versions:
rem *				28-08-2009 / Ari Suomi
rem *					The icodeDoc.chm now gets copied to the doc directory
rem *					as it should when generating internal HTML 
rem *					documentation.
rem *
rem ****************************************************************************

rem Default values
set KDLEAVE=false
set KDHTML=false
set KDINT=false
set KDLIB=
set doxyVersion=
set KDPATH=%~dp0

rem Parse command line parameters. Goto start when parsing is done
:ParseParams
	if "%1"=="" goto start

	if "%1"=="-?" goto usage
	if "%1"=="/?" goto usage
	if "%1"=="--?" goto usage
	if "%1"=="-l" set KDLEAVE=true
	if "%1"=="-h" set KDHTML=true
	if "%1"=="-p" set KDINT=true
	
	SHIFT
	
GOTO ParseParams

rem ****************************************************************************
rem *  
rem * 	Echo usage informaton
rem *
rem ****************************************************************************
:usage
	echo KD batch file generates code documentation from T-Plat.E code. The 
	echo batch can be run from project root or from a FB directory.
	echo.
	echo   Usage: TL-KD [-l] [-h] [-p] [-?]
	echo.
	echo    -l      Leave XML/HTML output undeleted
	echo    -?      This help
	echo    -h      Generate HTML (default is RTF)
	echo    -p      Include all private declarations and definitions in the 
	echo            documentation. (default is public only)
	echo.
	echo Troubleshoot: 
	echo.
	echo   When chm-file don't show properly you can consider following.
	echo.
	echo   If you trust all the computers on your LAN or intranet (and the people 
	echo   using them), you can lower the restrictions on the Local Intranet zone 
	echo   to allow CHM files to be displayed with the following steps:
	echo.
	echo   1. Click Start, click Run, type regedit, and then click OK.
	echo   2. Locate and then click the following subkey:
	echo   HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\HTMLHelp\1.x\ItssRestrictions
	echo   (if this registry subkey does not exist, create it).
	echo   3. Right-click the ItssRestrictions subkey, point to New, and then 
	echo   click DWORD Value.
	echo   4. Type MaxAllowedZone, and then press Enter.
	echo   5. Right-click the MaxAllowedZone value, and then click Modify.
	echo   6. In the Value data box, type 1, and then click OK.
	echo   7. Quit Registry Editor. 

	goto done


rem ****************************************************************************
rem *  
rem * 	Check if the batch is run from a project root directory. Project and
rem *	FB documentation uses different doxyfiles and XSL files.
rem *
rem	*	If a .svn directory is found then the revision of each file will be 
rem *	asked from the version control system. version.bat outputs the current
rem *	revision and the number of the revision in which the file was
rem *	last changed.
rem *
rem ****************************************************************************
:start
	if exist .svn set doxyVersion=%KDPATH%kd\version.bat
	if exist exe goto runProject
	if exist src goto runLibrary
	goto runFB




rem ****************************************************************************
rem *  
rem * 	This section is executed if the KD batch is not run from project root.
rem * 	It is assumed that it is run from a function block directory.
rem *
rem ****************************************************************************
:runFB
	echo Building FB documentation
	
	set KDLIB=..\..\..\platform
	
	rem There might be some easier way to get the current directory, but at 
	rem least this works. Directory name is the FB name.
	cd > "%tmp%\tmp.bat"
	for /F "usebackq delims=\ tokens=1-10" %%a in ("%tmp%\tmp.bat") do (
		if not "%%a"=="" set doxyFBName=%%a
		if not "%%b"=="" set doxyFBName=%%b
		if not "%%c"=="" set doxyFBName=%%c
		if not "%%d"=="" set doxyFBName=%%d
		if not "%%e"=="" set doxyFBName=%%e
		if not "%%f"=="" set doxyFBName=%%f
		if not "%%g"=="" set doxyFBName=%%g
		if not "%%h"=="" set doxyFBName=%%h
		if not "%%i"=="" set doxyFBName=%%i
		if not "%%j"=="" set doxyFBName=%%j
	)
	set doxyProjName=%doxyFBName% FB
	if "%KDHTML%"=="true" goto FBHTML
	rem otherwize RTF FB documentation is generated

rem ****************************************************************************
rem Generates FB documentation in RTF format.
:FBRTF
	set doxySections=
	set doxyGenHTML=NO
	set doxyGenXML=YES
	if "%KDINT%"=="true" goto privFBRTF
	rem otherwize public RTF is generated

rem ----------------------------------------------------------------------------
rem Generate FB documentation of public symbols in RTF format.
:pubFBRTF
	echo Making backup of old codeDoc.rtf
	copy codeDoc.rtf codeDoc.bak /Y > NUL
	echo Reading code documentation
	
	if exist .\publicFB (
		doxygen .\publicFB
	) else (
		doxygen %KDPATH%kd\publicFB
	)
	
	echo Processing code documentation
	xsltproc xml\combine.xslt xml\index.xml > xml\all.xml
	if ERRORLEVEL 1 goto errorFBRTF
	echo Generating codeDoc.rtf
	xsltproc %KDPATH%kd\publicFBRTF.xsl xml\all.xml > codeDoc.rtf
	if ERRORLEVEL 1 goto errorFBRTF
	echo Code documentation has been generated to codeDoc.rtf.
goto cleanFBRTF

rem Some error occured
:errorFBRTF
	echo Code documentation generation failed.

rem Remove any temporary files if -l flag is not set.
:cleanFBRTF
	if "%KDLEAVE%"=="true" goto done
	del xml /Q
	rmdir xml
goto done

rem ----------------------------------------------------------------------------
rem Generate FB documentation with public and private symbols in RTF format.
:privFBRTF
	echo Error: RTF generation of private symbols is not yet implemented.
	rem private RTF generation would require an own doxyfile
goto done
	
rem ****************************************************************************	
rem Generates FB documentation in HTML format.
:FBHTML
	set doxyGenHTML=YES
	set doxyGenXML=NO
	if "%KDINT%"=="true" goto privFBHTML
	rem otherwize public html is generated

	
rem Generates FB documentation of public symbols in HTML format.
:pubFBHTML
	echo Reading code documentation and generating HTML documentation of public symbols.
	set doxySections=pub_decl
	set doxyProjName=%doxyProjName% public
	set doxyHtmlHeader=%KDPATH%kd/head.html
	
	if exist .\publicFB (
		doxygen .\publicFB
	) else (
		doxygen %KDPATH%kd\publicFB
	)
	
	copy html\codeDoc.chm
	echo Code documentation has been generated to codeDoc.chm.
	if "%KDLEAVE%"=="true" goto done
	del html /Q
	rmdir html
goto done

rem Generates FB documentation of public and private symbols in HTML format.
:privFBHTML
	echo Reading code documentation and generating HTML documentation of private symbols.
	set doxySections=priv_decl pub_decl
	set doxyProjName=%doxyProjName% internal
	set doxyHtmlHeader=%KDPATH%kd/head.html
		
	if exist .\internalFB (
		doxygen .\internalFB
	) else (
		doxygen %KDPATH%kd\internalFB
	)
	
	copy ihtml\icodeDoc.chm
	echo Code documentation has been generated to icodeDoc.chm.
	if "%KDLEAVE%"=="true" goto done
	del ihtml /Q
	rmdir ihtml
goto done







rem ****************************************************************************
rem *  
rem * 	This section is executed if the KD batch is run from library root.
rem * 	Make a backup of the old codeDoc.rtf before running anything.
rem *	Run Doxygen and combine all the .xml files to one big .xml file.
rem *
rem ****************************************************************************
:runLibrary
	echo Building library documentation
	
	set KDLIB=..\platform
	if "%KDHTML%"=="true" goto LibHTML
	rem otherwise RTF documentation is generated
		
rem ****************************************************************************
rem Generate library documentation in RTF format.
:LibRTF
	set doxyGenHTML=NO
	set doxyGenXML=YES
	set doxySections=pub_decl
	
	echo Making backup of old codeDoc.rtf
	copy codeDoc.rtf codeDoc.bak /Y > NUL
	echo Reading code documentation
	
	if exist .\publicLib (
		doxygen .\publicLib
	) else (
		doxygen %KDPATH%kd\publicLib
	)
	
	echo Processing code documentation
	xsltproc xml\combine.xslt xml\index.xml > xml\all.xml
	if ERRORLEVEL 1 goto errorCustProjRTF
	echo Generating codeDoc.rtf
	xsltproc %KDPATH%kd\publicProjRTF.xsl xml\all.xml >codeDoc.rtf
	if ERRORLEVEL 1 goto errorCustProjRTF
	echo Code documentation has been generated to codeDoc.rtf.
goto cleanProj


rem ****************************************************************************
rem Generates library documentation in HTML format.
:LibHTML
	echo Reading code documentation and generating HTML documentation of public symbols.
	
	set doxyGenHTML=YES
	set doxyGenXML=NO
	set doxySections=pub_decl
	
	if exist .\publicLib (
		doxygen .\publicLib
	) else (
		doxygen %KDPATH%kd\publicLib
	)
	
	copy html\codeDoc.chm
	echo Code documentation has been generated to codeDoc.chm.
	if "%KDLEAVE%"=="true" goto done
	del html /Q
	rmdir html
goto done

	
	
	
	
	
	
	
	
rem ****************************************************************************
rem *  
rem * 	This section is executed if the KD batch is run from project root.
rem * 	Make a backup of the old codeDoc.rtf before running anything.
rem *	Run Doxygen and combine all the .xml files to one big .xml file.
rem *
rem ****************************************************************************
:runProject
	echo Building project documentation
	
	set KDLIB=.\platform
	if "%KDHTML%"=="true" goto ProjHTML
	rem otherwize RTF documentation is generated

rem ****************************************************************************
rem Generate project documentation in RTF format.
:ProjRTF
	set doxySections=
	set doxyGenHTML=NO
	set doxyGenXML=YES
	if "%KDINT%"=="true" goto privProjRTF
	rem otherwize public RTF is generated

rem ----------------------------------------------------------------------------
rem Generate project documentation of public symbols in RTF format.
:pubProjRTF
	echo Making backup of old codeDoc.rtf
	copy codeDoc.rtf codeDoc.bak /Y > NUL
	echo Reading code documentation
	
	if exist .\publicProj (
		doxygen .\publicProj
	) else (
		doxygen %KDPATH%kd\publicProj
	)
	
	echo Processing code documentation
	xsltproc xml\combine.xslt xml\index.xml > xml\all.xml
	if ERRORLEVEL 1 goto errorCustProjRTF
	echo Generating codeDoc.rtf
	xsltproc %KDPATH%kd\publicProjRTF.xsl xml\all.xml >codeDoc.rtf
	if ERRORLEVEL 1 goto errorCustProjRTF
	echo Code documentation has been generated to codeDoc.rtf.
goto cleanProj

rem Error when generating public project RTF documentation
:errorCustProjRTF
	echo Code documentation generation failed.
	rem	Continue to cleanProj
	
rem Remove any temporary files if -l flag is not set.
:cleanProj
	if "%KDLEAVE%"=="true" goto done
	del xml /Q
	rmdir xml
goto done	

rem ----------------------------------------------------------------------------
rem	Generate project documentation with public and private symbols in RTF format.
:privProjRTF
	echo Error: RTF generation of private symbols is not yet implemented.
goto done


rem ****************************************************************************
rem Generates project documentation in HTML format.
:ProjHTML
	set doxyGenHTML=YES
	set doxyGenXML=NO
	if "%KDINT%"=="true" goto privProjHTML
	rem otherwize public html is generated

rem ----------------------------------------------------------------------------
rem Generates project documentation of public symbols in HTML format.
:pubProjHTML
	set doxySections=pub_decl
	set doxyHtmlHeader=%KDPATH%kd/head.html
	echo Reading code documentation and generating HTML documentation of public symbols.
		
	if exist .\publicProj (
		doxygen .\publicProj
	) else (
		doxygen %KDPATH%kd\publicProj
	)
	
	copy html\codeDoc.chm
	echo Code documentation has been generated to codeDoc.chm.
	if "%KDLEAVE%"=="true" goto done
	del html /Q
	rmdir html
goto done

rem ----------------------------------------------------------------------------
rem Generates project documentation of public and private symbols in HTML format.
:privProjHTML
	set doxySections=pub_decl priv_decl
	set doxyHtmlHeader=%KDPATH%kd/head.html
	echo Reading code documentation and generating HTML documentation of private symbols.
		
	if exist .\internalProj (
		doxygen .\internalProj
	) else (
		doxygen %KDPATH%kd\internalProj
	)
	
	copy ihtml\icodeDoc.chm 
	echo Code documentation has been generated to icodeDoc.chm.
	if "%KDLEAVE%"=="true" goto done
	del ihtml /Q
	rmdir ihtml	
goto done
	
	
	
rem ****************************************************************************
rem *  
rem * 	The batch file is done.
rem *
rem ****************************************************************************
:done
set KDLEAVE=
set KDHTML=
set KDINT=
set KDSVN=
set KDLIB=
set KDPATH=
set doxyFBName=
set doxyProjName=
set doxyGenHTML=
set doxyGenXML=
set doxySections=
set doxyVersion=
set doxyHtmlHeader=