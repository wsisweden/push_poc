/* 02-11-2009 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	name@upc
*
*	\brief		Initialization types for name@upc FB.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

#ifndef name@upc_INIT_H_INCLUDED
#define name@upc_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

//#define name@lwc_reset	dummy_reset
//#define name@lwc_down		dummy_down
//#define name@lwc_read		dummy_read
//#define name@lwc_write	dummy_write
#define name@lwc_ctrl		dummy_ctrl
#define name@lwc_test		dummy_test

/** 
 *	Initialization structure for name@upc function block.
 */
typedef struct {						/*''''''''''''''''''''''''''' CONST */
	Uint8					dummy;		/**< Dummy.							*/
} name@lwc_Init;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_name@upc
#elif defined(EXE_GEN_name@upc)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_name@upc
#endif

#define EXE_APPL(n)			n##name@lwc

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB. Uncomment the types that are
 * used in this file. */

#define EXE_USE_VN /* Volatile numeric      */
//#define EXE_USE_NN /* Non-volatile numeric  */
//#define EXE_USE_VS /* Volatile string       */
//#define EXE_USE_NS /* Non-volatile string   */
//#define EXE_USE_VT /* Volatile typedef      */
//#define EXE_USE_NT /* Non-volatile typedef  */
//#define EXE_USE_PN /* Process-point numeric */
//#define EXE_USE_PT /* Process-point numeric */
//#define EXE_USE_PS /* Process-point string  */
//#define EXE_USE_BL /* Block of byte-data    */

/* *****************************************************************************
 *
 *	Register declarations:
 *
 *		EXE_REG_VN(	rName, flags, dim, dataType, 	def,	min, max 		)
 *		EXE_BUF_VN(	rName, flags, dim, dataType, 	def, 	min, max 		)
 *		EXE_REG_NN(	rName, flags, dim, dataType, 	def,	min, max 		)
 *		EXE_BUF_NN(	rName, flags, dim, dataType, 	def, 	min, max 		)
 *		EXE_REG_PN(	rName, flags, dim, dataType, 			min, max 		)
 *		EXE_REG_VT(	rName, flags, dim, dataType, 					toolDef )
 *		EXE_BUF_VT(	rName, flags, dim, dataType, 					toolDef )
 *		EXE_REG_NT(	rName, flags, dim, dataType, 					toolDef )
 *		EXE_BUF_NT(	rName, flags, dim, dataType, 					toolDef )
 *		EXE_REG_PT(	rName, flags, dim, dataType								)
 *		EXE_REG_VS(	rName, flags, dim, 				def 					)
 *		EXE_REG_NS(	rName, flags, dim,				def 					)
 *		EXE_REG_PS(	rName, flags, dim										)
 *		EXE_REG_BL( rName, flags, 			h, w, r, c						)
 *
 *			rName		Register name.
 *			regFlags (can be ORed together):
 *						SYS_REG_CHANGE_DET	Change detection supported
 *						SYS_REG_DEFAULT		None of above flags apply
 *			dim			Dimension. Number of array elements. Size of a string
 *						register.
 *			dataType	The register data type.
 *			def			Register default value.
 *			min			Minimum register value.
 *			max			Maximum register value.
 *
 *	MMI attribute declaration:
 *
 *		EXE_MMI_NONE( rName )
 *		EXE_MMI_INT(  rName, use, tHandle, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle, mmiF, tEnum                   )
 *		EXE_MMI_DATE( rName, use, tHandle, mmiF,                   qEnum )
 *		EXE_MMI_TIME( rName, use, tHandle, mmiF,                   qEnum )
 *		EXE_MMI_DT(   rName, use, tHandle, mmiF,                   qEnum )
 *
 *			rName 	Register name as given to the EXE_REG_..() macro.
 *			use 	Purpose/use of the register, one of enum SYS_MMI_U_...
 *			tHandle Handle of a descriptive name/text of the register.'
 *			tEnum	Handle of text with all enumerators.
 *
 *			mmiF 	flags (can be ORed together):
 *					SYS_MMI_READ	MMI can only read the register
 *					SYS_MMI_WRITE	MMI can only write the register
 *					SYS_MMI_RW		MMI can both read and write the register
 *					SYS_MMI_RT_ADJ	Should be adjusted real-time, not only at
 *									completion of editing (the default)
 *
 *			qEnum	Quantity string enumerator (SYS_Q_...)
 *
 *	Help text declarations:
 *		
 *		Help texts are long descriptive texts that are usually not stored in
 *		the embedded device. They are specified here for each register so that
 *		they can be extracted by using PodComposer. They can then be used in
 *		protocol profile files.
 *
 *		EXE_HELP_EN( rName,	aIndex,	tag, shortHelp, longHelp)
 *
 *			rName		Register name as given to the EXE_REG_..() macro.
 *			aIndex		The array index the help text is for.
 *			tag			A unique tag that may be used in PC software to identify
 *						the register.
 *			shortHelp	A short description string literal.
 *			longHelp	A long description string literal.
 *
 * *****************************************************************************
 *	        	Name		Flags			Dim	Type	Def	Min		Max
 *          	-----------	---------------	---	-----	---	-------	-------	  */

EXE_REG_VN(	dummyReg,	SYS_REG_DEFAULT, 1,	Uint32,	0,	0,		Uint32_MAX )
//EXE_MMI_NONE(	dummyReg )
//EXE_HELP_EN(	dummyReg,	0,	"TAG", "Short help", "Long help.")


/* *****************************************************************************
 *	
 *	References to external registers
 */

//EXE_REG_EX( myRName8, thatRName, thatInstanceName )



/* *****************************************************************************
 *
 *	Register indication declarations:
 *		
 *		EXE_IND_ME(	 iName, rName )
 *			Indication declaration without attributes for a local register.
 *
 *		EXE_IND_LOC( iName,	rName,	(indAttrib))
 *			Indication declaration with attributes for a local register.
 *
 *		EXE_IND_EXT( iName,			(indAttrib))
 *			Indication declaration with attributes for a external register.
 *
 *		Parameter description:
 *		iName 		Indication name
 *		rName 		Register name as given to the EXE_REG_..() macro.
 *		indAttrib	Indication attributes matching the fb__IndAttr structure.
 */
 
//EXE_IND_ME( myIndName3, 	dummyReg 		)
//EXE_IND_LOC( myIndName3, 	dummyReg, 	(0) )
//EXE_IND_EXT( myIndName3, 				(0) )

/* *****************************************************************************
 *
 * Language-dependent texts
 *
 * Note that: 
 *      - tExampleTxtHandle1 is reserved handlename and it's discarded by 
 *		text-tools.
 *      - you can add txthandle comment at the end of the texthandle line, 
 *		and this is parsed by text-tools.
 *
 * If you make changes to texts here you should use octal-codes for bytes with
 * value below 32 and above 127. This rule goes also for following chars:
 *      char: "   octal-code: \042
 *      char: (   octal-code: \050
 *	    char: )   octal-code: \051
 *      char: ,   octal-code: \054
 */

//EXE_TXT( tExampleTxtHandle1, EXE_T_EN("Example") EXE_T_FI("Esimerkki") "") /* txthandle comment */

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
