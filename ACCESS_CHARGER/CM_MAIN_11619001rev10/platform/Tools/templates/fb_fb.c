/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	name@upc
*
*	\brief		Function block interface for function block name@upc.
*
*	\details	.
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	name@upc	name@upc
*
*	\brief		name@upc function block.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E headers */
#include "tools.h"
#include "mem.h"
#include "deb.h"
#include "sys.h"
#include "reg.h"
#include "osa.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Template function block instance type
 */
 
typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	osa_TaskType			task;		/**< OSA Task						*/
	name@lwc_Init const_P * pInit;		/**< Initialization data			*/
} name@lwc__Inst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE OSA_TASK void name@lwc__task(void);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	name@lwc_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid 	An index (0..) that is accepted by various functions to
*						identify the instance. Should be kept for later use, or
*						may be discarded if not required by the FB.
*	\param		pArgs 	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * name@lwc_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	/* TODO: initialize function block. */

	name@lwc__Inst *			pInst;

	pInst = (name@lwc__Inst *) mem_reserve(&mem_normal, sizeof(name@lwc__Inst));

	pInst->pInit = (name@lwc_Init const_P *) pArgs;

	osa_newTask(
		&pInst->task,
		"name@lwc__task",
		OSA_PRIORITY_NORMAL,
		name@lwc__task,
		256
	);

	pInst->task.pUtil = pInst;			/* Set ptr to instance for the task	*/

	sys_mainInitCount(1);

	return((void *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	name@lwc_reset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reset the instance.
*
*	\param		pVoidInst	Pointer to instance.
*	\param		pArgs 		Pointer to the initialization arguments.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void name@lwc_reset(
	void *					pVoidInst,
	void const_P *			pArgs
) {
	DUMMY_VAR(pArgs);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	name@lwc_down
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power down indication.
*
*	\param		pVoidInst	Pointer to instance.
*	\param		nType		Down type.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void name@lwc_down(
	void *					pVoidInst, 
	sys_DownType			nType
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	name@lwc_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read the
*				value of a parameter owned by this FB.
*
*	\param		pVoidInst 	Pointer to the data of the instance.
*	\param		indIdx 		Indication index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex 		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue 		Pointer to the storage for the value.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC sys_IndStatus name@lwc_read(
	void *					pVoidInst,
	sys_IndIndex			indIdx,
	sys_ArrIndex			aIndex,
	void *					pValue
) {
	return REG_CONF_ERR;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	name@lwc_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		pVoidInst	Pointer to the data of the instance.
*	\param		indIdx		Indication index (0..).
*	\param		aIndex		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus name@lwc_write(
	void *					pVoidInst, 
	sys_IndIndex			indIdx,
	sys_ArrIndex			aIndex
) {
	return REG_CONF_ERR;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	name@lwc__task
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE OSA_TASK void name@lwc__task(
	void
) {
	name@lwc__Inst * pInst;

	pInst = (name@lwc__Inst *) osa_taskCurrent()->pUtil;

	/* Step 2: Complete the cold-start initializations */
										/* Execute these as necessary:		*/
	//osa_syncGet(...);					/* - Wait till services are avail	*/
	//osa_syncSet(...);					/* - Announce new services avail	*/

	sys_mainInitReady();				/* Tell SYS that init completed		*/
	
	FOREVER {

		/* Wait for signal from the xx__reset() function using OSA's service */

		/* Step 3: Warm-start initializations */
		
		FOREVER {

			/*
			 *  This is the main loop: Execute the tasks to do and then
			 *  enter an OSA function to wait a while (or vice versa).
			 *  At some point of the loop there should be a test for a signal
			 *  possibly coming from the xx__down() function.
			 */
	
			osa_taskWait(osa_msToTicks(1000));
		}
	}
}
