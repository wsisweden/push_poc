@echo off
rem ****************************************************************************
rem *
rem *	Copyright (c) 2005-2019, TJK Tietolaite Oy.
rem *	
rem	*	T-Plat.E project compilation batch file.
rem *
rem *	Version:	$Rev: 3986 $
rem *				$Date: 2019-11-07 16:25:05 +0200 (to, 07 marras 2019) $
rem *				$Author: tlarsu $
rem *
rem ****************************************************************************

rem Show usage help if no arguments were supplied
IF "%1" == "" GOTO usage

REM Check which choice version is used (FreeDOS, MS DOS or Windows Vista/7)
REM The following lines will produce error level 1 on Win Vista/7 but error
REM level zero on DOS version of choice.
SET TLC_CHOICE_VER=DOS
CHOICE /T 0 /D Y > NUL
if ERRORLEVEL 1 set TLC_CHOICE_VER=Vista

REM Jump to specified label if :: is supplied as the first argument
if .%1==.:: (
	REM ECHO Jumping to: %2
	goto %2
)

REM ****************************************************************************
REM * Parse the supplied arguments
REM ****************************************************************************
SET TLC_ARGS=
SET TLC_FOLDER=
SET TLLIB_ROOT=%CD%
SET TLC_ALL=FALSE
SET TLC_PREV=FALSE
SET TLC_IGNORE=FALSE

:ParseParams
	IF "%~1"=="" GOTO start
	IF "%1"=="-?" GOTO usage
	IF "%1"=="-all" (
		SET TLC_ALL=TRUE
		GOTO contArgParse
	)
	IF "%1"=="-prev" (
		SET TLC_PREV=TRUE
		GOTO contArgParse
	)
	IF "%1"=="-ignore" (
		SET TLC_IGNORE=TRUE
		GOTO contArgParse
	)
	REM The first argument not checked above is the folder name. The rest of the
	REM arguments are added to TLC_ARGS and later supplied to make.
	IF "%TLC_FOLDER%"=="" (SET TLC_FOLDER=%1) ELSE (SET TLC_ARGS=%TLC_ARGS% %1)

:contArgParse
	SHIFT
	GOTO ParseParams

rem ****************************************************************************
rem *
rem *	Parameters have been parsed, determine which type of compilation should
rem *	be done. 	
rem *
rem ****************************************************************************	
:start
	IF %TLC_FOLDER%=="" GOTO usage	
	IF /I "%TLC_FOLDER%"=="Exe" goto process_exe
	GOTO process_lib

rem ****************************************************************************
rem *
rem * 	Echo usage informaton
rem *
rem ****************************************************************************	
:usage
	echo.
	echo TL-C (c) TJK Tietolaite Oy, 2005-2019.
	echo.
	echo Batch file for compiling T-Plat.E libraries and executables.
	echo.
	echo Usage: TL-C folder [-?] [-prev] [-all] [make flags]
	echo.
	echo    folder       Library or executable folder
	echo    -?           Show this help
	echo    -prev        Compile the previously selected function blocks
	echo    -all         Compile all function blocks in the library
	echo    -ignore      Ignore all errors and continue compilation.
	echo    make flags   Flags passed on to MAKE
	echo.
	GOTO end_no_cleanup

rem ****************************************************************************
rem *
rem *  Compiles code in the EXE folder.
rem *
rem ****************************************************************************	
:process_exe
	SET TLLIB_E=PROJECT
	SET TLLIB_L=%TLLIB_ROOT%\platform
	SET TLLIB_R=%CD%\%TLC_FOLDER%
	
	rem /*
	rem  *	Collect the optional arguments from the command line
	rem  */
	set TLLIB_A=%TLC_ARGS%
	
	REM *
	REM *	Do a simple sanity check before continuing.
	REM *
	IF NOT EXIST %TLLIB_ROOT%\EXE\NUL (
		set TLLIB_S=Could not locate EXE from the current directory!
		GOTO end
	)
	
	REM *
	REM *	Now proceed with compiling the project and build a binary from it
	REM *	If TLLIB_M is not set we are using NMAKE else use whatever is in the
	REM *	TLLIB_M (GNU MAKE). Two stage makefiles are also assumed (first one
	REM *	generates the second makefile with dependencies).
	REM *
	cd EXE

	IF "%TLLIB_M%"=="" (
		
		nmake.exe /NOLOGO /R /C /D %TLLIB_A%
	
	) ELSE (
	
		%TLLIB_M% -f %TLLIB_L%\MAK\CREATE_MAKE_GNU.MAK create_make %TLLIB_A%	
		%TLLIB_M% -f Obj_%TLLIB_T%/makefile %TLLIB_A%	
	)
	
	set TLLIB_S=Success!
	if errorlevel 1 set TLLIB_S=Errors during compilation or linking
	
	GOTO exit

rem ****************************************************************************
rem *
rem *  Compiles code in the platform or blocks folder.
rem *
rem ****************************************************************************	
:process_lib
	SET TLLIB_L=%CD%\platform
	SET TLLIB_R=%CD%\%TLC_FOLDER%

	if "%TEMP%"=="" goto need_temp
	if .%TLLIB_O%==. goto no_target
	if .%TLLIB_T%==. goto no_target
	if not exist %TLLIB_R%\src\*.* goto root_err
	if exist %TLLIB_R%\Mak\*.* goto begin
	echo Problems with project's 'Mak' subdirectory, which is assumed to be under
	echo the current directory. The Mak subdirectory contains some project-wide
	echo configuration files for e.g. the MAKE utility.
	set TLLIB_S=Error
	goto exit

:no_target
	set TLLIB_S=No valid target selection. First use the set_env.bat batch file to setup target environment variables.
	goto exit

:root_err
	set TLLIB_S=Problems with the project's root. Check the currect working directory.
	goto exit

:need_temp
	set TLLIB_S=The TEMP environment variable is assumed to be set. Please set this variable to a temporary directory in the workstation.
	goto exit
	
:err_project
	set TLLIB_S=Problems with the directory %TLLIB_P_FLDR%. Please manually create the directory and restart the batch.
	goto exit

:begin
	set TLLIB_A=
	set TLLIB_P=
	set TLLIB_S=Success!
	set TLLIB_Z=call
	
	rem /*
	rem  *	Collect the optional arguments from the command line
	rem  */
	set TLLIB_A=%TLC_ARGS%

	rem /*
	rem  *	Set the directory where log and file list will be output. If there is 
	rem  *  a file named kscfg.bat it is executed and assumed to set TLLIB_KSP
	rem  *  to the directory. If there isn't the default dir is used.
	rem  */
	set TLLIB_KSP=%TLLIB_R%\..\Exe\Log
	if exist %TLLIB_R%\..\Exe\kscfg.bat call %TLLIB_R%\..\Exe\kscfg.bat
	set TLLIB_P_FLDR=%TLLIB_KSP%

	if not exist %TLLIB_P_FLDR% md %TLLIB_P_FLDR%
	if not exist %TLLIB_P_FLDR% goto err_project

	rem /*
	rem  *	Build the "project name" from the path, by replacing the reserved
	rem  *	characters semicolon (:), dot (.) and backslash (\).
	rem  */
	for /F "delims=: tokens=1*" %%a in ("%TLLIB_R%") do call tl-c :: build_proj %%a %%b

	IF "%TLC_ALL%"=="TRUE" goto compile_all
	IF "%TLC_PREV%"=="TRUE" goto compile_previous
	
	rem Let the user select which function blocks should be compiled
	echo Starting user interface..
	SET KSTOOL_BAT=tl-c
	tl-kstool %TLLIB_R%\SRC\*.* %TLLIB_P_FLDR%\%TLLIB_P%.KS "%TEMP%\tllibtmp.bat"
	goto listed_subset
	
:compile_all
	ECHO Compiling all function blocks in %TLC_FOLDER%
	SET KSTOOL_BAT=tl-c
	tl-kstool %TLLIB_R%\SRC\*.* %TLLIB_P_FLDR%\%TLLIB_P%.KS "%TEMP%\tllibtmp.bat" -a
	goto listed_subset

:compile_previous
	ECHO Compiling previously selected function blocks
	SET KSTOOL_BAT=tl-c
	tl-kstool %TLLIB_R%\SRC\*.* %TLLIB_P_FLDR%\%TLLIB_P%.KS "%TEMP%\tllibtmp.bat" -p

:listed_subset
	echo _______________________________________________________________________________>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	echo TL-C started %DATE% %TIME%>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	echo.>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	
	REM perform the compilation for each selected function block
	call "%TEMP%\tllibtmp.bat"

	goto exit

REM ****************************************************************************

rem /*
rem  *	Building the project file name, TLLIB_P
rem  */
:build_proj
	if .%3==. goto cont_build_proj
	if not .%TLLIB_P%==. set TLLIB_P=%TLLIB_P%~
	set TLLIB_P=%TLLIB_P%%3
	
:cont_build_proj
	if not .%4==. for /F "delims=.\ tokens=1*" %%a in ("%4") do call tl-c :: build_proj %%a %%b
	goto end_no_cleanup

REM ****************************************************************************

rem /*
rem  *	Entry point from the temp batch (tllibtmp.bat) to build one module.
rem  */
:make_src
	echo.
	echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %3
	cd %TLLIB_R%\Src\%3
	cd >>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	
	IF "%TLLIB_M%"=="" (
	
		if exist MAKEFILE. goto ok1_make_src
		if exist Inc_%TLLIB_T%\MAKEFILE goto ok1_make_src
		echo No MAKEFILE in this directory -- ignored.
		echo No MAKEFILE in this directory -- ignored.>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	
	) ELSE (
	
		if exist contents.xml. goto ok1_make_src
		echo No contents.xml in this directory -- ignored
		echo No contents.xml in this directory -- ignored.>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG		
	)
	
	goto end_no_cleanup

REM *
REM *	Compile source folder.
REM *	If TLLIB_M is not set we are using NMAKE else use whatever is in the
REM *	TLLIB_M (GNU MAKE). Two stage makefiles are also assumed (first one
REM *	generates the second makefile with dependencies).
REM *
:ok1_make_src
	echo.>recent.err
	set TLLIB_D=%TLLIB_R%\Src\%3
	
	IF "%TLLIB_M%"=="" (
		
		nmake.exe /NOLOGO %TLLIB_A% >tllib.$1
	
	) ELSE (
	
		%TLLIB_M% -f %TLLIB_L%\MAK\CREATE_MAKE_GNU.MAK create_make %TLLIB_A% >tllib.$1
		%TLLIB_M% -f Obj_%TLLIB_T%/makefile %TLLIB_A% >tllib.$1
	)
	
	if errorlevel 1 goto fail_make_src
	echo ok.
	copy tllib.$1+recent.err tllib.$2>nul
	del tllib.$1
	del recent.err
	if exist tllib.$1 goto fatal_make_src
	if exist tllib.err goto fatal_make_src
	ren tllib.$2 recent.err
	if exist tllib.$2 goto fatal_make_src
	type recent.err>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	goto end_no_cleanup

:fail_make_src
	echo.
	echo Failed MAKE. See %TLLIB_P_FLDR%\%TLLIB_P%.LOG.
	copy tllib.$1+recent.err tllib.$2>nul
	del tllib.$1
	del recent.err
	if exist tllib.$1 goto fatal_make_src
	if exist recent.err goto fatal_make_src
	ren tllib.$2 recent.err
	if exist tllib.$2 goto fatal_make_src
	type recent.err>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	goto cont_after_err

:fatal_make_src
	echo An unexpected error occurred in creating/accessing temporary files in current
	echo directory. Check the rights to the files in this directory and make sure there
	echo is free disk space. Also check %TLLIB_P_FLDR%\%TLLIB_P%.LOG.
	if exist tllib.$1 type tllib.$1>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	if exist recent.err type recent.err>>%TLLIB_P_FLDR%\%TLLIB_P%.LOG
	if exist tllib.$1 del tllib.$1
	if exist tllib.$2 del tllib.$2

:cont_after_err
	IF "%TLC_IGNORE%"=="TRUE" goto err_ignore
	echo.
	echo Press [R] to retry with the current directory
	echo       [I] to ignore the problem and proceed to next FB
	echo       [F] to abort TL-C and stay in the current FB directory
	echo       [A] to abort TL-C and return to the project's root directory
	echo       [T] to type the RECENT.ERR file that gives details of the problem
	IF "%TLC_CHOICE_VER%"=="Vista" (
		choice /C AFIRT /N /M "?"
	) ELSE (
		choice /CAFIRT /N "?"
	)
	if errorlevel 5 goto err_type
	if errorlevel 4 goto make_src
	if errorlevel 3 goto err_ignore
	if errorlevel 2 set TLLIB_ROOT=%TLLIB_D%
	set TLLIB_Z=rem
	set TLLIB_S=Aborted after an error or by the user.
	goto end_no_cleanup

:err_type
	echo -------------------------------------------------------------------------------
	type recent.err|more
	goto cont_after_err

:err_ignore
	set TLLIB_S=Completed with some ignored problems...
	goto end_no_cleanup

	
REM ****************************************************************************

:exit
	CD %TLLIB_ROOT%

:end
	echo.
	echo TL-C: %TLLIB_S%
	echo.
	SET TLC_ARGS=
	SET TLC_FOLDER=
	SET TLC_CHOICE_VER=
	SET TLLIB_A=
	SET TLLIB_C=
	SET TLLIB_D=
	SET TLLIB_KSP=
	SET TLLIB_L=
	SET TLLIB_P=
	SET TLLIB_P_FLDR=
	SET TLLIB_R=
	SET TLLIB_ROOT=
	SET TLLIB_S=
	SET TLLIB_Z=
	SET TLLIB_E=
	SET TLC_ALL=
	SET TLC_PREV=
	SET TLC_IGNORE=
	SET KSTOOL_BAT=

:end_no_cleanup
