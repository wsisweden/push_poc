<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<!-- ***************************************************************************
*	
*	S T Y L E S H E E T   D E S C R I P T I O N	
*
********************************************************************************
*
*	This stylesheet can be used to transform doxygen xml output to a RTF 
*	document that uses the Tietolaite template.
*
*	This file is not a complete stylesheet. It only contains common templates
*	that are used in several different stylesheets.
*
*	ORGINAL
*		30.9.2008 / Ari Suomi
*
*	VERSION
*		\$Rev: 2831 $ \n
*		\$Date: 2015-06-10 14:17:52 +0300 (ke, 10 kesä 2015) $ \n
*		\$Author: tlarsu $
*
**************************************************************************** -->

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Space character 
*
**************************************************************************** -->
<xsl:template match="sp">
	<xsl:text> </xsl:text>
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Bold text.
*
**************************************************************************** -->
<xsl:template match="bold">
	<xsl:text>{\b </xsl:text>
	
	<xsl:apply-templates />
	
	<xsl:text>}</xsl:text>
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Item list. (bullet list)
*
**************************************************************************** -->
<xsl:template match="itemizedlist">
		
	<xsl:for-each select="listitem">
		<xsl:text>{\pard\plain \s23\fi-360\li1080\nowidctlpar\jclisttab\tx1080{\*\pn \pnlvlblt\ilvl0\ls3\pnrnot0\pnf3\pnstart1\pnindent1209\pnhang{\pntxtb \'b7}}\ls3\adjustright \fs22\cgrid &#x0D;</xsl:text>			
		<xsl:value-of select="para" />
		<xsl:text>\par}&#x0D;</xsl:text>
	</xsl:for-each>
	
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Simple documentation section.
*	These sections are created witht the \par doxygen command.
*
**************************************************************************** -->
<xsl:template match="simplesect[@kind='par']">

	<!-- Display section title if there is one -->
	<xsl:if test="title != ''">
		<xsl:text>{\pard\plain \s34\li567\sb120\keep\keepn\nowidctlpar\adjustright \b\fs22\cgrid </xsl:text>
		<xsl:value-of select="title"/>
		<xsl:text>\par}&#x0D;</xsl:text>
	</xsl:if>
	
	<!-- Apply templates to the para element -->
	<xsl:apply-templates select="para" />
	
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Template for see also section.
*
**************************************************************************** -->
<xsl:template match="simplesect[@kind='see']">

	<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid {&#x0D;</xsl:text>
	
	<xsl:text>{\b See also:}</xsl:text>
	<xsl:for-each select ="para">
		<xsl:text>\line </xsl:text>
		<xsl:apply-templates />
	</xsl:for-each>
	
	<xsl:text>\par}&#x0D;</xsl:text>
			
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	List of parameters used in function documentation.
*
**************************************************************************** -->
<xsl:template match="parameterlist[@kind='param']">

	<xsl:for-each select="parameteritem">
				  
		<xsl:text>{\pard\plain \s31\li567\sb120\keep\keepn\nowidctlpar\adjustright \i\fs22\cgrid </xsl:text>
		<xsl:value-of select="parameternamelist/parametername" />
		<xsl:text>\par}&#x0D;</xsl:text>
			
		<xsl:apply-templates select="parameterdescription/para" />
		
	</xsl:for-each>

</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	List of return values used in function documentation.
*
**************************************************************************** -->
<xsl:template match="parameterlist[@kind='retval']">

	<!--
		TODO: 	This section could use the "Norm Tbl Header" and
				"Norm Tbl Text" styles to make a table-like list.
	-->
	<xsl:for-each select="parameteritem">
				  
		<xsl:text>{\pard\plain \s31\li567\sb120\keep\keepn\nowidctlpar\adjustright \i\fs22\cgrid </xsl:text>
		<xsl:value-of select="parameternamelist/parametername" />
		<xsl:text>\par}&#x0D;</xsl:text>
		
		<xsl:apply-templates select="parameterdescription/para" />
		
	</xsl:for-each>

</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Brief description
*
**************************************************************************** -->
<xsl:template match="briefdescription">
	<xsl:apply-templates select="para" />
</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Breif:		Detailed description
*
*	Details:	This template is used for all detailed description sections
*				in the document.
*
**************************************************************************** -->
<xsl:template match="detaileddescription">
	<xsl:apply-templates select="para"/>
</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Brief:		paragraph. Used in multiple places.
*
**************************************************************************** -->
<xsl:template match="para">

	<!--
		Para elements contain mixed content (elements and text). Text nodes
		should be shown in an own paragraph while templates should be applied
		to elements.
		
		It is possible that multiple consecutive text nodes should be in the 
		same paragraph in some situations. This will have to be modified to
		support that. This will happen if some words are bolded in the middle of
		a paragraph.
	-->

	<xsl:for-each select="node()">
		<xsl:choose>
			<xsl:when test="self::text()">
				<!--
					This is a text node. Make a paragraph for the text with
					"Normal indent" style. Remove unnccesary spaces from the 
					text.
				-->
				<xsl:text>{\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;</xsl:text>
				<xsl:value-of select="normalize-space(.)"/>
				<xsl:text>\par}&#x0D;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<!-- This is an element node. Just apply templates -->
				<xsl:apply-templates select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>

</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Brief:		Programlisting section.
*
*	Details:	Programlisting is displayed in a gray box with monospaced font.
*
**************************************************************************** -->
<xsl:template match="programlisting">

	<!-- Apply FnExample style to the whole programlisting paragraph -->
	<xsl:text>{\pard\plain \s20\li567\sb120\keep\nowidctlpar\box\brdrs\brdrw15\brsp80\brdrcf8 \tx3799\tx5188\tqr\tx9809\adjustright \shading500 \f2\fs20\expndtw-4\cf9\cgrid </xsl:text>

	<!-- Handle all code lines -->
	<xsl:for-each select="codeline">
		
		<!-- 	
			The highlight element contains mixed content (text nodes and element
			nodes). Loop through all nodes.
		-->

		<xsl:for-each select="highlight/node()">
		
			<xsl:choose>
			
				<xsl:when test="self::text()">
					<!-- 
						This is a text node. All curly brackets must escaped in
						text nodes because RTF uses them as a control character.
					-->

					<xsl:variable name="newtext">
						<xsl:call-template name="string-replace-all">
							<xsl:with-param name="text" select="." />
							<xsl:with-param name="replace" select="'{'" />
							<xsl:with-param name="by" select="'\{'" />
						</xsl:call-template>
					</xsl:variable>

					<xsl:variable name="finaltext">
						<xsl:call-template name="string-replace-all">
							<xsl:with-param name="text" select="$newtext" />
							<xsl:with-param name="replace" select="'}'" />
							<xsl:with-param name="by" select="'\}'" />
						</xsl:call-template>
					</xsl:variable>
					
					<xsl:value-of select="$finaltext"/>
					
				</xsl:when>
				<xsl:otherwise>
					<!-- This is an element. Just apply templates. -->
					<xsl:apply-templates select="." />
				</xsl:otherwise>
				
			</xsl:choose>

		</xsl:for-each>
			
		<!-- Add a newline to all codelines except the last one -->
		<xsl:if test="position() != last()">
			<xsl:text>\line&#x0D;</xsl:text>
		</xsl:if>

	</xsl:for-each>
	
	<!-- A new paragraph is always started after a code listing -->
	<xsl:text>\par}</xsl:text>

</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Breif:		Preformatted text
*
*	Details:	Preformatted text is displayed with monospaced font and all
*				newline characters are preserved.
*
**************************************************************************** -->
<xsl:template match="preformatted">

	<xsl:text>\par</xsl:text>
	<xsl:text>{\pard\plain \s52\li567\nowidctlpar\adjustright \f2\fs20\expnd-1\expndtw-8\cgrid </xsl:text>
	
	<!-- Call the text formatting template -->
	<xsl:call-template name="split-text">
		<xsl:with-param name="arg1">
			<xsl:value-of select="."/>
		</xsl:with-param>
	</xsl:call-template>

	<xsl:text>\par}&#x0D;</xsl:text>

</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Function descrtiption section.
*
**************************************************************************** -->
<xsl:template match="sectiondef[@kind='func']">

	<!-- one function at a time -->
	<xsl:for-each select="memberdef">
		
		<!-- Heading with bookmark -->
		<xsl:text>{\pard\plain \s3\li113\ri113\sb360\sa120\keep\keepn\widctlpar\box\brdrs\brdrw60\brsp60\brdrcf15 \tx567\outlinelevel2\adjustright \shading10000\cfpat15 \b\fs36\cf8\lang1024\cgrid {\*\bkmkstart kd_</xsl:text> 
		<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
		<xsl:text>}</xsl:text>
		<xsl:value-of select="name"/>
		<xsl:text>{\*\bkmkend kd_</xsl:text>
		<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
		<xsl:text>}\par}&#x0D;</xsl:text>
		
		<!-- Brief function description -->
		<xsl:apply-templates select="briefdescription" />
		
		<!-- Function declaration -->
		<xsl:text>{\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;</xsl:text>
		<xsl:value-of select="definition" />
		<xsl:text>({\i </xsl:text>
		<xsl:for-each select="param">
			<xsl:if test="position() != 1"><xsl:text>, </xsl:text></xsl:if>
			<xsl:value-of select="declname" />
		</xsl:for-each>
		<xsl:text>});</xsl:text>
		<xsl:text>\par}&#x0D;</xsl:text>
		
		<!-- Parameter types -->
		<xsl:text>{\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;</xsl:text>
		<xsl:for-each select="param">
			<xsl:value-of select="type" /><xsl:text> {\i </xsl:text><xsl:value-of select="declname" /><xsl:text>};</xsl:text>
			<xsl:if test="position() != last()">
				<xsl:text>\line&#x0D;</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>\par}&#x0D;</xsl:text>
		
		<!-- Parameter descriptions -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Arguments\par}&#x0D;</xsl:text>
		<xsl:apply-templates select="detaileddescription/para/parameterlist[@kind='param']" />
		
		<!-- Return value -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Return value\par}&#x0D;</xsl:text>
		<xsl:choose>
			<xsl:when test="detaileddescription/para/simplesect[@kind='return']/node() | detaileddescription/para/parameterlist[@kind='retval']/node()">
				<xsl:apply-templates select="detaileddescription/para/simplesect[@kind='return']" />
				<xsl:apply-templates select="detaileddescription/para/parameterlist[@kind='retval']" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>{\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D; -\par}&#x0D;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		
		<!-- Description -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Description\par}&#x0D;</xsl:text>
		<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;{</xsl:text>
		<xsl:value-of select="detaileddescription/para[1]/text()" />
		<xsl:text>\par}&#x0D;</xsl:text>
		<xsl:apply-templates select="detaileddescription/para[position() != 1 and not(simplesect)]" />
		
		<!-- Notice -->
		<xsl:if test="detaileddescription/para/simplesect[@kind='note']/node()">
			<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Notice\par}&#x0D;</xsl:text>
			<xsl:apply-templates select="detaileddescription/para/simplesect[@kind='note']" />
		</xsl:if>
		
		<!-- See -->
		<xsl:if test="detaileddescription/para/simplesect[@kind='see']/node()">
			<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{See also\par}&#x0D;</xsl:text>

			<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid {&#x0D;</xsl:text>

			<xsl:for-each select ="detaileddescription/para/simplesect[@kind='see']/para">
				<xsl:if test="position() != 1"><xsl:text>\line </xsl:text></xsl:if>
				<xsl:apply-templates />
			</xsl:for-each>
			
			<xsl:text>\par}</xsl:text>
		</xsl:if>
		
	</xsl:for-each>

</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Function macro.
*
**************************************************************************** -->
<xsl:template match="memberdef[@kind='define' and param/defname]">

		<!-- Heading with bookmark -->
		<xsl:text>{\pard\plain \s3\li113\ri113\sb360\sa120\keep\keepn\widctlpar\box\brdrs\brdrw60\brsp60\brdrcf15 \tx567\outlinelevel2\adjustright \shading10000\cfpat15 \b\fs36\cf8\lang1024\cgrid {\*\bkmkstart kd_</xsl:text>
		<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
		<xsl:text>}</xsl:text>
		<xsl:value-of select="name"/>
		<xsl:text>{\*\bkmkend kd_</xsl:text>
		<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
		<xsl:text>}\par}&#x0D;</xsl:text>
		
		<!-- Brief function description -->
		<xsl:apply-templates select="briefdescription" />
		
		<!-- Function declaration -->
		<xsl:text>{\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid </xsl:text>
		<xsl:value-of select="name" />
		<xsl:text>({\i </xsl:text>
		<xsl:for-each select="param">
			<xsl:if test="position() != 1"><xsl:text>, </xsl:text></xsl:if>
			<xsl:value-of select="defname" />
		</xsl:for-each>
		<xsl:text>});</xsl:text>
		<xsl:text>\par}&#x0D;</xsl:text>
		
		<!-- Parameters -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Arguments\par}&#x0D;</xsl:text>
		<xsl:apply-templates select="detaileddescription/para/parameterlist[@kind='param']" />
		
		<!-- Return value -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Return value\par}&#x0D;</xsl:text>
		<xsl:apply-templates select="detaileddescription/para/simplesect[@kind='return']" />
		<xsl:apply-templates select="detaileddescription/para/parameterlist[@kind='retval']" />
				
		<!-- Description -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Description\par}&#x0D;</xsl:text>
		<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;{</xsl:text>
		<xsl:value-of select="detaileddescription/para[1]/text()[1]" />
		<xsl:value-of select="detaileddescription/para[1]/text()[2]" />
		<xsl:value-of select="detaileddescription/para[1]/text()[3]" />
		<xsl:value-of select="detaileddescription/para[1]/text()[4]" />
		<xsl:value-of select="detaileddescription/para[1]/text()[5]" />
		<xsl:text>\par}&#x0D;</xsl:text>
		<xsl:apply-templates select="detaileddescription/para[position() != 1 and not(simplesect)]" />
		
		<!-- Notice -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Notice\par}&#x0D;</xsl:text>
		<xsl:apply-templates select="detaileddescription/para/simplesect[@kind='note']" />

		<!-- See -->
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{See also\par}&#x0D;</xsl:text>
		<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid {&#x0D;</xsl:text>

		<xsl:for-each select ="detaileddescription/para/simplesect[@kind='see']/para">
			<xsl:if test="position() != 1"><xsl:text>\line </xsl:text></xsl:if>
			<xsl:apply-templates />
		</xsl:for-each>
		
		<xsl:text>\par}</xsl:text>
		
</xsl:template>



<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Variable descrtiption section.
*
**************************************************************************** -->
<xsl:template match="sectiondef[@kind='var']">
	<xsl:apply-templates select="memberdef" />
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Define description section.
*
**************************************************************************** -->
<xsl:template match="sectiondef[@kind='define']">
	<xsl:apply-templates select="memberdef" />
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Enum section
*
**************************************************************************** -->
<xsl:template match="sectiondef[@kind='enum']">
	<xsl:apply-templates select="memberdef" />
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	User defined section
*
**************************************************************************** -->
<xsl:template match="sectiondef[@kind='user-defined']">
	
	<xsl:apply-templates select="description" />
 
	<xsl:choose>
		<xsl:when test="memberdef[@kind='define']">
		
			<!-- 
				A group of defines. Usually some flags. These will be displayed
				in a table
			-->
			
			<!-- Table title -->
			<xsl:text>\pard\plain \ltrpar\s40\ql \li567\ri0\sb240\sa60\keep\keepn\widctlpar\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin567\itap0\pararsid2754120 \rtlch\fcs1 \af0\afs20\alang1025 \ltrch\fcs0
\b\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2754120 </xsl:text>
			<xsl:value-of select="header"/>
			<xsl:text>\par \ltrrow}&#x0D;</xsl:text>
			
			<!-- Table heading -->
			<xsl:text>\trowd \ltrrow\ts11\trgaph107\trleft460\trkeep\trhdr\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3\tblrsid5057317\tblind567\tblindtype3
\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl \clshdng2000\cltxlrtb\clftsWidth3\clwWidth3510\clshdngraw2000 \cellx3970\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl
\clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl \clshdng2000\cltxlrtb\clftsWidth3\clwWidth6061\clshdngraw2000 \cellx10031
\pard\plain \ltrpar\s57\ql \li0\ri0\sb60\sa60\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5057317 \rtlch\fcs1 \af0\afs20\alang1025 \ltrch\fcs0 
\b\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2754120 Define\cell Description\cell }
\pard\plain \ltrpar \ql \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af0\afs20\alang1025 \ltrch\fcs0 \fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2754120 \trowd \ltrrow \ts11\trgaph107\trleft460\trkeep\trhdr\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15
\trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3\tblrsid5057317\tblind567\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl
\clshdng2000\cltxlrtb\clftsWidth3\clwWidth3510\clshdngraw2000 \cellx3970\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl
\clshdng2000\cltxlrtb\clftsWidth3\clwWidth6061\clshdngraw2000 \cellx10031\row \ltrrow}
\trowd \ltrrow\ts11\trgaph107\trleft460\trkeep\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3\tblrsid5057317\tblind567\tblindtype3
\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl \cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx3970\clvertalt \clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15
\clbrdrr\brdrtbl \cltxlrtb\clftsWidth3\clwWidth6061\clshdrawnil \cellx10031
\pard\plain \ltrpar \s56\ql \li0\ri0\sb60\sa60\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5057317 \rtlch\fcs1 \af0\afs20\alang1025 \ltrch\fcs0
\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 &#x0D;</xsl:text>

			<!-- Add one table row for each define in the group -->
			<xsl:for-each select="memberdef">
				<xsl:text>{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2754120 </xsl:text>
				<xsl:value-of select="name"/>
				<xsl:text>\cell </xsl:text>
				<xsl:value-of select="briefdescription" />
				<xsl:value-of select="detaileddescription/para" />
				<xsl:text>\cell \row}&#x0D;</xsl:text>
			</xsl:for-each>
			
		</xsl:when>
		<xsl:otherwise>
			<!-- For groups that does not contain defines -->
			<xsl:apply-templates select="memberdef" />

		</xsl:otherwise>
    </xsl:choose>
	
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Class, struct or union.
*
**************************************************************************** -->
<xsl:template match="innerclass">

	<xsl:variable name="currentid" select="@refid" />
		
	<xsl:apply-templates select="/doxygen/compounddef[@id=$currentid]" />

</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Variable description.
*
**************************************************************************** -->
<xsl:template match="memberdef[@kind='variable']">

	<!-- Test if briefdescription or detaileddescription nodes have any child nodes -->
	<xsl:if test="briefdescription/node() | detaileddescription/node()">
		<!-- Variable name -->
		<xsl:text>\pard\plain \s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel3\adjustright \i\f1\fs28\lang1024\cgrid {&#x0D;</xsl:text>
		<xsl:value-of select="name" />
		<xsl:text>\par}&#x0D;</xsl:text>
		
		<!-- Type -->
		<xsl:text>\pard\plain \s15\li567\sb120\keepn\nowidctlpar\adjustright \fs22\cgrid {Type: </xsl:text>
		<xsl:apply-templates select="type" />
		<xsl:text>\par}&#x0D;</xsl:text>
		
		<!-- Brief and detailed description -->
		<xsl:apply-templates select="briefdescription" />
		<xsl:apply-templates select="detaileddescription" />
	</xsl:if>
</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Define description. This template is only applied to non-macro defines.
*
**************************************************************************** -->
<xsl:template match="memberdef[@kind='define' and not(param)]">

	<!-- define name -->
	<xsl:text>\pard\plain \s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel3\adjustright \i\f1\fs28\lang1024\cgrid {</xsl:text>
	<xsl:value-of select="name" />
	<xsl:text>\par}&#x0D;</xsl:text>
	
	<!-- Brief and detailed description -->
	<xsl:apply-templates select="briefdescription" />
	<xsl:apply-templates  select="detaileddescription" />

</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Type description.
*
**************************************************************************** -->
<xsl:template match="memberdef[@kind='typedef']">

	<!-- Typedef name -->
	<xsl:text>\pard\plain \s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel3\adjustright \i\f1\fs28\lang1024\cgrid {{\*\bkmkstart kd_</xsl:text>
	<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
	<xsl:text>}</xsl:text>
	<xsl:value-of select="name" />
	<xsl:text>{\*\bkmkend kd_</xsl:text>
	<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
	<xsl:text>}\par}&#x0D;</xsl:text>
	
	<!-- Brief and detailed description -->
	<xsl:apply-templates select="briefdescription" />
	<xsl:apply-templates select="detaileddescription" />
	
</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Function pointer type description.
*
**************************************************************************** -->
<xsl:template match="memberdef[@kind='typedef' and argsstring != '']">

	<!-- Heading with bookmark -->
	<xsl:text>{\pard\plain \s3\li113\ri113\sb360\sa120\keep\keepn\widctlpar\box\brdrs\brdrw60\brsp60\brdrcf15 \tx567\outlinelevel2\adjustright \shading10000\cfpat15 \b\fs36\cf8\lang1024\cgrid {\*\bkmkstart kd_</xsl:text> 
	<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
	<xsl:text>}</xsl:text>
	<xsl:value-of select="name"/>
	<xsl:text>{\*\bkmkend kd_</xsl:text>
	<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
	<xsl:text>}\par}&#x0D;</xsl:text>
	
	<!-- Brief function description -->
	<xsl:apply-templates select="briefdescription" />
	
	<!-- 
		Function declaration 
		The parameter types are not listed so that they can be easily
		enumerated. Just displaying all the types on the declaration line.
	-->
	<xsl:text>{\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;</xsl:text>
	<xsl:value-of select="type" /><xsl:text> </xsl:text><xsl:value-of select="name" />
	<xsl:text>{\i </xsl:text><xsl:value-of select="argsstring" /><xsl:text>}</xsl:text><xsl:text>;</xsl:text>
	<xsl:text>\par}&#x0D;</xsl:text>
		
	<!-- Parameter descriptions -->
	<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Arguments\par}&#x0D;</xsl:text>
	<xsl:apply-templates select="detaileddescription/para/parameterlist[@kind='param']" />
	
	<!-- Return value -->
	<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Return value\par}&#x0D;</xsl:text>
	<xsl:choose>
		<xsl:when test="detaileddescription/para/simplesect[@kind='return']/node() | detaileddescription/para/parameterlist[@kind='retval']/node()">
			<xsl:apply-templates select="detaileddescription/para/simplesect[@kind='return']" />
			<xsl:apply-templates select="detaileddescription/para/parameterlist[@kind='retval']" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>{\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D; -\par}&#x0D;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	
	<!-- Description -->
	<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Description\par}&#x0D;</xsl:text>
	<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;{</xsl:text>
	<xsl:value-of select="detaileddescription/para[1]/text()" />
	<xsl:text>\par}&#x0D;</xsl:text>
	<xsl:apply-templates select="detaileddescription/para[position() != 1 and not(simplesect)]" />
	
	<!-- Notice -->
	<xsl:if test="detaileddescription/para/simplesect[@kind='note']/node()">
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{Notice\par}&#x0D;</xsl:text>
		<xsl:apply-templates select="detaileddescription/para/simplesect[@kind='note']" />
	</xsl:if>
	
	<!-- See -->
	<xsl:if test="detaileddescription/para/simplesect[@kind='see']/node()">
		<xsl:text>\pard\plain \s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid &#x0D;{See also\par}&#x0D;</xsl:text>

		<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid {&#x0D;</xsl:text>

		<xsl:for-each select ="detaileddescription/para/simplesect[@kind='see']/para">
			<xsl:if test="position() != 1"><xsl:text>\line </xsl:text></xsl:if>
			<xsl:apply-templates />
		</xsl:for-each>
		
		<xsl:text>\par}&#x0D;</xsl:text>
	</xsl:if>

</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Template for Enum documentation.
*
**************************************************************************** -->
<xsl:template match="memberdef[@kind='enum']">

	<!-- Heading -->
	<xsl:text>\pard\plain \s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel3\adjustright \i\f1\fs28\lang1024\cgrid {{\*\bkmkstart kd_</xsl:text>
	<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
	<xsl:text>}</xsl:text>
	<xsl:choose>
		<xsl:when test="starts-with(name, '@')">
			<xsl:text>Unnamed enum</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="name" />
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text>{\*\bkmkend kd_</xsl:text>
	<xsl:value-of select="name" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />	
	<xsl:text>}\par}&#x0D;</xsl:text>

	<!-- Description text -->
	<xsl:apply-templates select="briefdescription" />
	<xsl:apply-templates select="detaileddescription" />

	<!-- Table header with field descriptions -->
	<xsl:text>\pard\plain \s35\li567\sb240\sa60\keep\keepn\nowidctlpar\adjustright \b\fs22\cgrid {{\field{\*\fldinst SYMBOL 110 \\f "Wingdings" \\s 11}{\fldrslt\f14\fs22}}}&#x0D;{ </xsl:text>
	<xsl:choose>
		<xsl:when test="starts-with(name, '@')">
			<xsl:text>Unnamed enum</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="name" />
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text>\par}&#x0D;</xsl:text>
			<xsl:text>\pard\plain\trowd \ltrrow\ts11\trgaph107\trleft460\trkeep\trhdr\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3\tblrsid5057317\tblind567\tblindtype3
\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl \clshdng2000\cltxlrtb\clftsWidth3\clwWidth3510\clshdngraw2000 \cellx3970\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl
\clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl \clshdng2000\cltxlrtb\clftsWidth3\clwWidth6061\clshdngraw2000 \cellx10031
\pard\plain \ltrpar\s57\ql \li0\ri0\sb60\sa60\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5057317 \rtlch\fcs1 \af0\afs20\alang1025 \ltrch\fcs0 
\b\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2754120 Enumerator\cell Description\cell }
\pard\plain \ltrpar \ql \li0\ri0\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \rtlch\fcs1 \af0\afs20\alang1025 \ltrch\fcs0 \fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033
{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2754120 \trowd \ltrrow \ts11\trgaph107\trleft460\trkeep\trhdr\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15
\trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3\tblrsid5057317\tblind567\tblindtype3 \clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl
\clshdng2000\cltxlrtb\clftsWidth3\clwWidth3510\clshdngraw2000 \cellx3970\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl
\clshdng2000\cltxlrtb\clftsWidth3\clwWidth6061\clshdngraw2000 \cellx10031\row \ltrrow}
\trowd \ltrrow\ts11\trgaph107\trleft460\trkeep\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3\tblrsid5057317\tblind567\tblindtype3
\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrtbl \cltxlrtb\clftsWidth3\clwWidth3510\clshdrawnil \cellx3970\clvertalt \clbrdrt\brdrs\brdrw15 \clbrdrl\brdrtbl \clbrdrb\brdrs\brdrw15
\clbrdrr\brdrtbl \cltxlrtb\clftsWidth3\clwWidth6061\clshdrawnil \cellx10031
\pard\plain \ltrpar \s56\ql \li0\ri0\sb60\sa60\widctlpar\intbl\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\pararsid5057317 \rtlch\fcs1 \af0\afs20\alang1025 \ltrch\fcs0
\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033</xsl:text>

	<xsl:for-each select="enumvalue">
		<xsl:text>{\rtlch\fcs1 \af0 \ltrch\fcs0 \insrsid2754120 </xsl:text>
		<xsl:value-of select="name"/>
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="briefdescription" />
		<xsl:value-of select="detaileddescription/para" />
		<xsl:text>\cell \row}&#x0D;</xsl:text>
	</xsl:for-each>
	
</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Struct. The struct can have a brief and detailed description.
*
*	All struct fields are also displayed in a table.
*
**************************************************************************** -->
<xsl:template match="compounddef[@kind='struct'] | compounddef[@kind='union']">

	<!-- Struct name -->
	<xsl:text>\pard\plain \s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel3\adjustright \i\f1\fs28\lang1024\cgrid {{\*\bkmkstart kd_</xsl:text>
	<xsl:value-of select="compoundname" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
	<xsl:text>}</xsl:text>
	<xsl:value-of select="compoundname" />
	<xsl:text>{\*\bkmkend kd_</xsl:text>
	<xsl:value-of select="compoundname" /><xsl:text>_</xsl:text><xsl:value-of select="substring(@id,10,16)" />
	<xsl:text>}\par}&#x0D;</xsl:text>

	<!-- Description text -->
	<xsl:apply-templates select="briefdescription" />
	<xsl:apply-templates select="detaileddescription" />
	
	<!-- Table title with "Normal TableTitle" style -->
	<xsl:text>\pard\plain \s35\li567\sb240\sa60\keep\keepn\nowidctlpar\adjustright \b\fs22\cgrid {{\field{\*\fldinst SYMBOL 110 \\f "Wingdings" \\s 11}{\fldrslt\f14\fs22}}}&#x0D;{ </xsl:text>
	<xsl:choose>
		<xsl:when test="starts-with(compoundname, '@')">
			<xsl:text>Unnamed struct</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="compoundname" />
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text>\par}&#x0D;</xsl:text>	
	
	<!-- Table header with field descriptions -->
	<xsl:text>\trowd \trgaph107\trleft460\trkeep\trhdr\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3 \clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone
\clbrdrb\brdrs\brdrw15 \clbrdrr \brdrnone \clshdng2000\cltxlrtb\clftsWidth3\clwWidth1667 \cellx2127\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone
\clshdng2000\cltxlrtb\clftsWidth3\clwWidth2835 \cellx4962\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone \clshdng2000\cltxlrtb\clftsWidth3\clwWidth5068 \cellx10030
\pard\plain \s57\ql \li0\ri0\sb60\sa60\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \b\fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{Field name\cell Type\cell Description\cell }
\pard\plain \ql \li0\ri0\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\trowd \trgaph107\trleft460\trkeep\trhdr\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3 \clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone
\clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone \clshdng2000\cltxlrtb\clftsWidth3\clwWidth1667 \cellx2127\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone
\clshdng2000\cltxlrtb\clftsWidth3\clwWidth2835 \cellx4962\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl \brdrnone \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone \clshdng2000\cltxlrtb\clftsWidth3\clwWidth5068 \cellx10030
\row }
\trowd \trgaph107\trleft460\trkeep\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3 \clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone
\clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1667 \cellx2127\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone \clbrdrb \brdrs\brdrw15 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth2835
\cellx4962\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth5068 \cellx10030

</xsl:text>
	
	<xsl:for-each select="sectiondef/memberdef">
		<xsl:text>\pard\plain \s56\ql \li0\ri0\sb60\sa60\widctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \fs22\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 &#x0D;</xsl:text>
		<xsl:text>{</xsl:text>
		<xsl:value-of select="name"/>
		<xsl:text>\cell </xsl:text>
		<xsl:apply-templates select="type" />
		<xsl:text>\cell </xsl:text>
		<xsl:value-of select="briefdescription" />
		<xsl:value-of select="normalize-space(detaileddescription/para)" />
		<xsl:text>\cell}
\pard\plain \ql \li0\ri0\nowidctlpar\intbl\aspalpha\aspnum\faauto\adjustright\rin0\lin0 \fs20\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 
{\trowd \trgaph107\trleft460\trkeep\trbrdrt\brdrs\brdrw15 \trbrdrb\brdrs\brdrw15 \trbrdrh\brdrs\brdrw15 \trftsWidth1\trpaddl107\trpaddr107\trpaddfl3\trpaddfr3 \clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone
\clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth1667 \cellx2127\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone \clbrdrb \brdrs\brdrw15 \clbrdrr\brdrnone
\cltxlrtb\clftsWidth3\clwWidth2835 \cellx4962\clvertalt\clbrdrt\brdrs\brdrw15 \clbrdrl\brdrnone \clbrdrb\brdrs\brdrw15 \clbrdrr\brdrnone \cltxlrtb\clftsWidth3\clwWidth5068 \cellx10030
\row }&#x0D;</xsl:text>
	</xsl:for-each>
	
</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Template for a reference.
*
**************************************************************************** -->
<xsl:template match="ref">
	<!-- 	Cannot use the whole id generated by Doxygen here because it is too
			long for Word. Combining the link text and 16 characters
			from the id to get a quite good unique id.
	-->
		
	<xsl:text>{\field{\*\fldinst { HYPERLINK \\l "kd_</xsl:text>
	<!-- Excluding the parenthesis from links to functions or macros -->
	<xsl:choose>
		<xsl:when test="substring(text(), string-length(text())-1) = '()'">
			<!-- This is a link to a function or a macro. Remove parenthesis -->
			<xsl:value-of select="substring-before(text(), '()')" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="text()" />
		</xsl:otherwise>	
	</xsl:choose>
	<xsl:text>_</xsl:text><xsl:value-of select="substring(@refid,10,16)" />
	<xsl:text>" }}{\fldrslt {\cs50\ul\cf2 </xsl:text>
	<xsl:value-of select="text()" />
	<xsl:text>}}}</xsl:text>
</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Brief:		Inserts characters between lines in the supplied string.
*
*	Arguments:	arg1	String containing multiline text.
*
*	Details:	The template always processes the text before the first new 
*				line character. The rest of the string is supplied to the
*				template when it is called again.
*	
*	Note:		This is a recursive template
*
**************************************************************************** -->
<xsl:template name="split-text">
	<xsl:param name="arg1"/>
	
	<xsl:choose>
		<xsl:when test="contains($arg1,'&#10;')">
			<xsl:value-of select="substring-before($arg1,'&#10;')"/>
				
			<xsl:text>&#x0D;\par</xsl:text>
			<xsl:call-template name="split-text">
				<xsl:with-param name="arg1">
					<xsl:value-of select="substring-after($arg1,'&#10;')"/>
				</xsl:with-param>
			</xsl:call-template>
			
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$arg1"/>
			
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Template that can be used for string find and replace. XSLT 1.0 does not
*	have the replace() function which was introduced in XSLT 2.0. This
*	Template can be used on all XSLT versions.
*
**************************************************************************** -->
<xsl:template name="string-replace-all">
	<xsl:param name="text" />
	<xsl:param name="replace" />
	<xsl:param name="by" />
	<xsl:choose>
		<xsl:when test="$text = '' or $replace = ''or not($replace)" >
			<!-- Prevent this routine from hanging -->
			<xsl:value-of select="$text" />
		</xsl:when>
		<xsl:when test="contains($text, $replace)">
			<xsl:value-of select="substring-before($text,$replace)" />
			<xsl:value-of select="$by" />
			<xsl:call-template name="string-replace-all">
				<xsl:with-param name="text" select="substring-after($text,$replace)" />
				<xsl:with-param name="replace" select="$replace" />
				<xsl:with-param name="by" select="$by" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$text" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


</xsl:stylesheet>
