<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:strip-space elements="*"/>

<xsl:output method="text" version="1.0" encoding="UTF-8" indent="no" />

<xsl:include href="commonRTF.xsl" />

<!-- ***************************************************************************
*	
*	S T Y L E S H E E T   D E S C R I P T I O N	
*
********************************************************************************
*
*	This stylesheet can be used to transform doxygen xml output to a RTF 
*	document that uses the Tietolaite template.
*
*	This stylesheet outputs FB documentation that can be released to 
*	customers. This is actually the same as the projectRTF.xsl now. A separate
*	file has been used because there will probably be changes in this file
*	later.
*
*	ORGINAL
*		18.9.2008 / Ari Suomi
*
**************************************************************************** -->


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Brief:		Inner group
*
*	Details:	Displays target specific groups of a function block.
*		
*				In project documentation the FB subgroups are displayed under
*				the parent group (the FB).
*
**************************************************************************** -->
<xsl:template match="innergroup">

	<xsl:variable name="currentid" select="@refid" />
		
	<xsl:if test="/doxygen/compounddef[@id=$currentid]/briefdescription != ''">
	
		<!-- Group title -->
		<xsl:text>\pard\plain \s2\sb240\keep\keepn\nowidctlpar\outlinelevel1\adjustright \b\f1\fs28\expnd1\expndtw5\lang1024\cgrid &#x0D;{</xsl:text>
		<xsl:value-of select="/doxygen/compounddef[@id=$currentid]/title" />
		<xsl:text> \par}&#x0D;</xsl:text>
		
		<!-- Brief description -->
		<xsl:apply-templates select="/doxygen/compounddef[@id=$currentid]/briefdescription" />
		<xsl:apply-templates select="/doxygen/compounddef[@id=$currentid]/detaileddescription" />
		
		<!-- Typedefs and structs -->
		<xsl:if test="/doxygen/compounddef[@id=$currentid]/sectiondef[@kind='typedef'] | /doxygen/compounddef[@id=$currentid]/innerclass[@refid] !=''">
			<xsl:text>\pard\plain \s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel3\adjustright \i\f1\fs28\lang1024\cgrid &#x0D;{</xsl:text>
			<xsl:text>Types</xsl:text>
			<xsl:text> \par}&#x0D;</xsl:text>

			<xsl:apply-templates select="/doxygen/compounddef[@id=$currentid]/sectiondef[@kind='typedef']/memberdef" />
			<xsl:apply-templates select="/doxygen/compounddef[@id=$currentid]/innerclass" />
		</xsl:if>
		
		<!-- Public variables. The public variables header in inner groups uses a 
		different font than ordinary public variables headers. The sectiondef 
		template can not be used because it outputs the ordinary public variables
		header. -->
		<xsl:if test="/doxygen/compounddef[@id=$currentid]/sectiondef[@kind='var']">
			<xsl:text>\pard\plain \s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel3\adjustright \i\f1\fs28\lang1024\cgrid &#x0D;{</xsl:text>
			<xsl:text>Public variables</xsl:text>
			<xsl:text> \par}&#x0D;</xsl:text>
			
			<xsl:apply-templates select="/doxygen/compounddef[@id=$currentid]/sectiondef[@kind='var']/memberdef" />
		</xsl:if>
			
		<!-- enum -->
		<xsl:apply-templates select="/doxygen/compounddef[@id=$currentid]/sectiondef[@kind='enum']" />

	</xsl:if>
	
</xsl:template>


<!-- ***************************************************************************
*	
*	T E M P L A T E   D E S C R I P T I O N	
*
********************************************************************************
*
*	Breif:		The root template
*
*	Details:	This template defines all styles that will be used in the RTF 
*				document. Then it displays all module documentation.
*
**************************************************************************** -->
<xsl:template match="/">{\rtf1\ansi\ansicpg1252\uc1 \deff0\deflang1033\deflangfe1033{\fonttbl{\f0\froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}{\f1\fswiss\fcharset0\fprq2{\*\panose 020b0604020202020204}Arial;}
{\f2\fmodern\fcharset0\fprq1{\*\panose 02070309020205020404}Courier New;}{\f3\froman\fcharset2\fprq2{\*\panose 05050102010706020507}Symbol;}{\f14\fnil\fcharset2\fprq2{\*\panose 05000000000000000000}Wingdings;}
{\f15\fswiss\fcharset0\fprq2{\*\panose 020b0604030504040204}Tahoma;}{\f16\froman\fcharset238\fprq2 Times New Roman CE;}{\f17\froman\fcharset204\fprq2 Times New Roman Cyr;}{\f19\froman\fcharset161\fprq2 Times New Roman Greek;}
{\f20\froman\fcharset162\fprq2 Times New Roman Tur;}{\f21\froman\fcharset186\fprq2 Times New Roman Baltic;}{\f22\fswiss\fcharset238\fprq2 Arial CE;}{\f23\fswiss\fcharset204\fprq2 Arial Cyr;}{\f25\fswiss\fcharset161\fprq2 Arial Greek;}
{\f26\fswiss\fcharset162\fprq2 Arial Tur;}{\f27\fswiss\fcharset186\fprq2 Arial Baltic;}{\f28\fmodern\fcharset238\fprq1 Courier New CE;}{\f29\fmodern\fcharset204\fprq1 Courier New Cyr;}{\f31\fmodern\fcharset161\fprq1 Courier New Greek;}
{\f32\fmodern\fcharset162\fprq1 Courier New Tur;}{\f33\fmodern\fcharset186\fprq1 Courier New Baltic;}{\f106\fswiss\fcharset238\fprq2 Tahoma CE;}{\f107\fswiss\fcharset204\fprq2 Tahoma Cyr;}{\f109\fswiss\fcharset161\fprq2 Tahoma Greek;}
{\f110\fswiss\fcharset162\fprq2 Tahoma Tur;}{\f111\fswiss\fcharset186\fprq2 Tahoma Baltic;}}{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;
\red255\green255\blue255;\red0\green0\blue128;\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}{\stylesheet{\nowidctlpar\adjustright 
\fs20\cgrid \snext0 Normal;}{\s1\sb960\keep\keepn\nowidctlpar\adjustright \b\caps\f1\fs28\expnd2\expndtw10\lang1024\cgrid \sbasedon0 \snext15 heading 1;}{\s2\sb240\keep\keepn\nowidctlpar\adjustright \b\f1\fs28\expnd1\expndtw5\lang1024\cgrid 
\sbasedon0 \snext15 heading 2;}{\s3\li113\ri113\sb360\sa120\keep\keepn\widctlpar\box\brdrs\brdrw60\brsp60\brdrcf15 \tx567\adjustright \shading10000\cfpat15 \b\fs36\cf8\lang1024\cgrid \sbasedon0 \snext15 heading 3;}{
\s4\li567\sb180\keep\keepn\nowidctlpar\outlinelevel1\adjustright \i\f1\fs28\lang1024\cgrid \sbasedon2 \snext15 heading 4;}{\s5\keepn\nowidctlpar\adjustright \b\fs20\lang3081\cgrid \sbasedon0 \snext0 heading 5;}{\*\cs10 \additive Default Paragraph Font;}{
\s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid \sbasedon0 \snext15 Normal Indent;}{\s16\sb240\keep\keepn\nowidctlpar\adjustright \b\f1\fs28\expnd1\expndtw5\lang1024\cgrid \sbasedon2 \snext15 NoTOC Heading 2;}{
\s17\li113\ri113\sb360\sa120\keep\keepn\widctlpar\box\brdrs\brdrw60\brsp60\brdrcf15 \tx567\adjustright \shading10000\cfpat15 \b\fs36\cf8\lang1024\cgrid \sbasedon3 \snext15 FnHeading;}{\s18\sb120\keep\keepn\nowidctlpar\adjustright \b\f1\fs22\cgrid 
\sbasedon15 \snext15 FnSubHeading;}{\s19\sa964\nowidctlpar\tqc\tx4252\tx7939\tqr\tx8504\adjustright \fs20\cgrid \sbasedon0 \snext19 footer;}{\s20\li567\sb120\keep\nowidctlpar\box\brdrs\brdrw15\brsp80\brdrcf8 \tx3799\tx5188\tqr\tx9809\adjustright 
\shading500 \f2\fs20\expndtw-4\cf9\cgrid \sbasedon15 \snext15 FnExample;}{\s21\li720\nowidctlpar\adjustright \shading1000\cbpat8 \f2\fs16\cgrid \sbasedon0 \snext0 FormattedText;}{\s22\fi-2268\li2835\sb80\sa40\nowidctlpar\tqr\tx9923\adjustright 
\fs22\cgrid \sbasedon15 \snext22 Norm Tbl Text;}{\s23\fi-360\li1080\nowidctlpar\jclisttab\tx1080{\*\pn \pnlvlbody\ilvl0\ls3\pnrnot0\pndec }\ls3\adjustright \fs22\cgrid \sbasedon0 \snext0 \sautoupd Bullet;}{\*\cs24 \additive \sbasedon10 page number;}{
\s25\li567\sb120\sa120\nowidctlpar\adjustright \b\fs22\cgrid \sbasedon15 \snext15 _Fig Nr;}{\s26\li567\sb360\keepn\nowidctlpar\adjustright \fs22\cgrid \sbasedon15 \snext25 _Figure;}{\s27\nowidctlpar\adjustright \b\fs20\lang3081\cgrid \sbasedon0 \snext27 
Body Text;}{\*\cs28 \additive \fs16 \sbasedon10 annotation reference;}{\s29\nowidctlpar\adjustright \fs20\cgrid \sbasedon0 \snext29 annotation text;}{\s30\nowidctlpar\adjustright \cbpat9 \f15\fs20\cgrid \sbasedon0 \snext30 Document Map;}{
\s31\li567\sb120\keep\keepn\nowidctlpar\adjustright \i\fs22\cgrid \sbasedon15 \snext15 FnArgument;}{\s32\li426\ri2953\sb120\nowidctlpar\box\brdrs\brdrw15\brsp20 \adjustright \i\f1\fs16\lang1024\cgrid \sbasedon0 \snext32 Help;}{
\s33\li567\sb120\keep\keepn\nowidctlpar\brdrb\brdrs\brdrw15\brsp60 \tx2835\tqr\tx9923\adjustright \b\fs22\cgrid \sbasedon15 \snext0 Norm Tbl Header;}{\s34\li567\sb120\keep\keepn\nowidctlpar\adjustright \b\fs22\cgrid \sbasedon15 \snext15 Normal Subtitle;}{
\s35\li567\sb240\sa60\keep\keepn\nowidctlpar\adjustright \b\fs22\cgrid \sbasedon15 \snext35 Normal TableTitle;}{\s36\li567\sb120\widctlpar\brdrt\brdrs\brdrw15\brsp60 \brdrl\brdrs\brdrw15\brsp80 \brdrb\brdrs\brdrw15\brsp60 \brdrr\brdrs\brdrw15\brsp80 
\adjustright \fs22\cgrid \sbasedon15 \snext36 Note Box;}{\s37\li567\sb120\sa60\keep\keepn\nowidctlpar\brdrt\brdrs\brdrw15\brsp60 \brdrl\brdrs\brdrw15\brsp80 \brdrb\brdrs\brdrw15\brsp60 \brdrr\brdrs\brdrw15\brsp80 \adjustright \shading2000 \b\fs22\cgrid 
\sbasedon15 \snext36 Note Header;}{\s38\sb960\keep\keepn\nowidctlpar\adjustright \b\caps\f1\fs28\expnd2\expndtw10\lang1024\cgrid \sbasedon1 \snext15 NoTOC Heading 1;}{\s39\widctlpar\adjustright \f2\fs20\cgrid \sbasedon0 \snext39 Plain Text;}{\*\cs40 
\additive \b \sbasedon10 Strong;}{\s41\li567\sb120\nowidctlpar\tqr\tldot\tx9922\adjustright \caps\fs22\cgrid \sbasedon0 \snext0 \sautoupd toc 1;}{\s42\li567\nowidctlpar\tqr\tldot\tx9922\adjustright \fs22\cgrid \sbasedon0 \snext0 \sautoupd toc 2;}{
\s43\li1247\nowidctlpar\tqr\tldot\tx9922\adjustright \fs22\cgrid \sbasedon0 \snext0 \sautoupd toc 3;}{\s44\li1701\nowidctlpar\tqr\tldot\tx9922\adjustright \fs22\cgrid \sbasedon43 \snext15 \sautoupd toc 4;}{\s45\li800\nowidctlpar
\tqr\tldot\tx9922\adjustright \fs20\cgrid \sbasedon0 \snext0 \sautoupd toc 5;}{\s46\li1000\nowidctlpar\tqr\tldot\tx9922\adjustright \fs20\cgrid \sbasedon0 \snext0 \sautoupd toc 6;}{\s47\li1200\nowidctlpar\tqr\tldot\tx9922\adjustright \fs20\cgrid 
\sbasedon0 \snext0 \sautoupd toc 7;}{\s48\li1400\nowidctlpar\tqr\tldot\tx9922\adjustright \fs20\cgrid \sbasedon0 \snext0 \sautoupd toc 8;}{\s49\li1600\nowidctlpar\tqr\tldot\tx9922\adjustright \fs20\cgrid \sbasedon0 \snext0 \sautoupd toc 9;}{\*\cs50 
\additive \ul\cf2 \sbasedon10 Hyperlink;}}{\*\listtable{\list\listtemplateid-1556304406\listsimple{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1492\jclisttab\tx1492 }
{\listname ;}\listid-132}{\list\listtemplateid-616431248\listsimple{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1209\jclisttab\tx1209 }{\listname ;}\listid-131}
{\list\listtemplateid722348458\listsimple{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li926\jclisttab\tx926 }{\listname ;}\listid-130}{\list\listtemplateid-1628914372
\listsimple{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li643\jclisttab\tx643 }{\listname ;}\listid-129}{\list\listtemplateid-67489074\listsimple{\listlevel\levelnfc23
\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1492\jclisttab\tx1492 }{\listname ;}\listid-128}{\list\listtemplateid176321332\listsimple{\listlevel\levelnfc23\leveljc0
\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1209\jclisttab\tx1209 }{\listname ;}\listid-127}{\list\listtemplateid1089909588\listsimple{\listlevel\levelnfc23\leveljc0\levelfollow0
\levelstartat1\levelspace0\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li926\jclisttab\tx926 }{\listname ;}\listid-126}{\list\listtemplateid-1993995174\listsimple{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1
\levelspace0\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li643\jclisttab\tx643 }{\listname ;}\listid-125}{\list\listtemplateid1391239308\listsimple{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0
\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li360\jclisttab\tx360 }{\listname ;}\listid-120}{\list\listtemplateid1085672414\listsimple{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li360\jclisttab\tx360 }{\listname ;}\listid-119}{\list\listtemplateid-1479121124{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}
\fbias0 \fi-360\li927\jclisttab\tx927 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li1647\jclisttab\tx1647 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1
\levelspace0\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2367\jclisttab\tx2367 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3087
\jclisttab\tx3087 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li3807\jclisttab\tx3807 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0
\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4527\jclisttab\tx4527 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5247\jclisttab\tx5247 }
{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li5967\jclisttab\tx5967 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'08.;}{\levelnumbers\'01;}\fi-180\li6687\jclisttab\tx6687 }{\listname ;}\listid16125845}{\list\listtemplateid1295573036{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00);}{\levelnumbers\'01;}
\fbias0 \fi-420\li987\jclisttab\tx987 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li1647\jclisttab\tx1647 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1
\levelspace0\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2367\jclisttab\tx2367 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3087
\jclisttab\tx3087 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li3807\jclisttab\tx3807 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0
\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4527\jclisttab\tx4527 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5247\jclisttab\tx5247 }
{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li5967\jclisttab\tx5967 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'08.;}{\levelnumbers\'01;}\fi-180\li6687\jclisttab\tx6687 }{\listname ;}\listid16515121}{\list\listtemplateid1282465878{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}
\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc23\leveljc0\levelfollow0
\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li2727\jclisttab\tx2727 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}
\f3\fbias0 \fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc23\leveljc0\levelfollow0
\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li4887\jclisttab\tx4887 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}
\f3\fbias0 \fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc23\leveljc0\levelfollow0
\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li7047\jclisttab\tx7047 }{\listname ;}\listid113646247}{\list\listtemplateid-1301520214{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 
\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li2727\jclisttab\tx2727 }{\listlevel\levelnfc23\leveljc0\levelfollow0
\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}
\f2\fbias0 \fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li4887\jclisttab\tx4887 }{\listlevel\levelnfc23\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li7047\jclisttab\tx7047 }{\listname 
;}\listid271516146}{\list\listtemplateid10809828{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc23\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li2727\jclisttab\tx2727 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li3447\jclisttab\tx3447 }
{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li4887\jclisttab\tx4887 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li5607
\jclisttab\tx5607 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li7047\jclisttab\tx7047 }{\listname ;}\listid331227567}{\list\listtemplateid-191833370{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }
{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}
\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid385956681}{\list\listtemplateid1716309626{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2
\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}
\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid460538651}{\list\listtemplateid1026614372{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li2007\jclisttab\tx2007 }
{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li2727\jclisttab\tx2727 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li4167
\jclisttab\tx4167 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li4887\jclisttab\tx4887 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 
\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li7047\jclisttab\tx7047 }{\listname ;}\listid515391779}
{\list\listtemplateid1229349106{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727
\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }
{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid692221466}
{\list\listtemplateid-1889923064{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727
\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }
{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid817723855}
{\list\listtemplateid-1103466212{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727
\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }
{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid833837180}
{\list\listtemplateid1017526196{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727
\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }
{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid883252113}
{\list\listtemplateid-1536797616{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727
\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }
{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid931165603}
{\list\listtemplateid201916431\listsimple{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li360\jclisttab\tx360 }{\listname ;}\listid989746054}{\list\listtemplateid63618580
{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fbias0 \fi-360\li927\jclisttab\tx927 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0
{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li1647\jclisttab\tx1647 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2367\jclisttab\tx2367 }{\listlevel\levelnfc0
\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3087\jclisttab\tx3087 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'04.;}{\levelnumbers
\'01;}\fi-360\li3807\jclisttab\tx3807 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4527\jclisttab\tx4527 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1
\levelspace0\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5247\jclisttab\tx5247 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li5967
\jclisttab\tx5967 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li6687\jclisttab\tx6687 }{\listname ;}\listid1141653316}{\list\listtemplateid-946831868{\listlevel
\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li2727\jclisttab\tx2727 }
{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li4887
\jclisttab\tx4887 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 
\fi-360\li7047\jclisttab\tx7047 }{\listname ;}\listid1145585473}{\list\listtemplateid-274843848{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00);}{\levelnumbers\'01;}\fbias0 \fi-420\li987
\jclisttab\tx987 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li1647\jclisttab\tx1647 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0
\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2367\jclisttab\tx2367 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3087\jclisttab\tx3087 }
{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li3807\jclisttab\tx3807 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4527\jclisttab\tx4527 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5247\jclisttab\tx5247 }{\listlevel\levelnfc4\leveljc0
\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li5967\jclisttab\tx5967 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}
\fi-180\li6687\jclisttab\tx6687 }{\listname ;}\listid1288242151}{\list\listtemplateid-538946142{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1647\jclisttab\tx1647 }
{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2367\jclisttab\tx2367 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'02.;}{\levelnumbers\'01;}\fi-180\li3087\jclisttab\tx3087 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3807\jclisttab\tx3807 }{\listlevel\levelnfc4\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4527\jclisttab\tx4527 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}
\fi-180\li5247\jclisttab\tx5247 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5967\jclisttab\tx5967 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6687\jclisttab\tx6687 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7407
\jclisttab\tx7407 }{\listname ;}\listid1409309414}{\list\listtemplateid938260832{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel
\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}
\fi-180\li4887\jclisttab\tx4887 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047
\jclisttab\tx7047 }{\listname ;}\listid1451437919}{\list\listtemplateid732055448{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }
{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li2727\jclisttab\tx2727 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li3447
\jclisttab\tx3447 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li4887\jclisttab\tx4887 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li5607
\jclisttab\tx5607 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li7047\jclisttab\tx7047 }{\listname ;}\listid1497262661}{\list\listtemplateid-446378864{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2
\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727\jclisttab\tx2727 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat3\levelspace0\levelindent0{\leveltext\'01-;}{\levelnumbers;}
\fbias0 \fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0
\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}
\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid1519082647}{\list\listtemplateid956312620{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0
{\leveltext\'01\'00;}{\levelnumbers\'01;}\fi-432\li432\jclisttab\tx432 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'03\'00.\'01;}{\levelnumbers\'01\'03;}\fi-576\li576\jclisttab\tx576 }{\listlevel
\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'05\'00.\'01.\'02;}{\levelnumbers\'01\'03\'05;}\fi-720\li720\jclisttab\tx720 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext
\'07\'00.\'01.\'02.\'03;}{\levelnumbers\'01\'03\'05\'07;}\fi-864\li864\jclisttab\tx864 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'09\'00.\'01.\'02.\'03.\'04;}{\levelnumbers\'01\'03\'05\'07\'09;}
\fi-1008\li1008\jclisttab\tx1008 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'0b\'00.\'01.\'02.\'03.\'04.\'05;}{\levelnumbers\'01\'03\'05\'07\'09\'0b;}\fi-1152\li1152\jclisttab\tx1152 }{\listlevel
\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'0d\'00.\'01.\'02.\'03.\'04.\'05.\'06;}{\levelnumbers\'01\'03\'05\'07\'09\'0b\'0d;}\fi-1296\li1296\jclisttab\tx1296 }{\listlevel\levelnfc0\leveljc0\levelfollow0
\levelstartat1\levelspace0\levelindent0{\leveltext\'0f\'00.\'01.\'02.\'03.\'04.\'05.\'06.\'07;}{\levelnumbers\'01\'03\'05\'07\'09\'0b\'0d\'0f;}\fi-1440\li1440\jclisttab\tx1440 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0
\levelindent0{\leveltext\'11\'00.\'01.\'02.\'03.\'04.\'05.\'06.\'07.\'08;}{\levelnumbers\'01\'03\'05\'07\'09\'0b\'0d\'0f\'11;}\fi-1584\li1584\jclisttab\tx1584 }{\listname ;}\listid1525552812}{\list\listtemplateid-391477560{\listlevel\levelnfc0\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}
\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}\fi-180\li2727\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167
\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887\jclisttab\tx4887 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }
{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }{\listname ;}\listid1603100874}{\list\listtemplateid-1430718678{\listlevel\levelnfc23\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext
\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li2727\jclisttab\tx2727 }{\listlevel
\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0
{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li4887\jclisttab\tx4887 }
{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3913 ?;}{\levelnumbers;}\f3\fbias0 \fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'01o;}{\levelnumbers;}\f2\fbias0 \fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc23\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'01\u-3929 ?;}{\levelnumbers;}\f14\fbias0 \fi-360\li7047
\jclisttab\tx7047 }{\listname ;}\listid1681084833}{\list\listtemplateid201916431\listsimple{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace0\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li360\jclisttab\tx360 }
{\listname ;}\listid1722631477}{\list\listtemplateid-191833370{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}
\fi-180\li2727\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887
\jclisttab\tx4887 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }
{\listname ;}\listid1910965607}{\list\listtemplateid1163531560{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'00.;}{\levelnumbers\'01;}\fi-360\li1287\jclisttab\tx1287 }{\listlevel\levelnfc4\leveljc0
\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'01.;}{\levelnumbers\'01;}\fi-360\li2007\jclisttab\tx2007 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'02.;}{\levelnumbers\'01;}
\fi-180\li2727\jclisttab\tx2727 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'03.;}{\levelnumbers\'01;}\fi-360\li3447\jclisttab\tx3447 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1
\levelspace360\levelindent0{\leveltext\'02\'04.;}{\levelnumbers\'01;}\fi-360\li4167\jclisttab\tx4167 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'05.;}{\levelnumbers\'01;}\fi-180\li4887
\jclisttab\tx4887 }{\listlevel\levelnfc0\leveljc0\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'06.;}{\levelnumbers\'01;}\fi-360\li5607\jclisttab\tx5607 }{\listlevel\levelnfc4\leveljc0\levelfollow0\levelstartat1\levelspace360
\levelindent0{\leveltext\'02\'07.;}{\levelnumbers\'01;}\fi-360\li6327\jclisttab\tx6327 }{\listlevel\levelnfc2\leveljc2\levelfollow0\levelstartat1\levelspace360\levelindent0{\leveltext\'02\'08.;}{\levelnumbers\'01;}\fi-180\li7047\jclisttab\tx7047 }
{\listname ;}\listid2037924622}}{\*\listoverridetable{\listoverride\listid-125\listoverridecount0\ls1}{\listoverride\listid-126\listoverridecount0\ls2}{\listoverride\listid-127\listoverridecount0\ls3}{\listoverride\listid-128\listoverridecount0\ls4}
{\listoverride\listid1525552812\listoverridecount0\ls5}{\listoverride\listid16125845\listoverridecount0\ls6}{\listoverride\listid1141653316\listoverridecount0\ls7}{\listoverride\listid16515121\listoverridecount0\ls8}{\listoverride\listid1288242151
\listoverridecount0\ls9}{\listoverride\listid989746054\listoverridecount0\ls10}{\listoverride\listid1722631477\listoverridecount0\ls11}{\listoverride\listid-119\listoverridecount0\ls12}{\listoverride\listid-120\listoverridecount0\ls13}
{\listoverride\listid-129\listoverridecount0\ls14}{\listoverride\listid-130\listoverridecount0\ls15}{\listoverride\listid-131\listoverridecount0\ls16}{\listoverride\listid-132\listoverridecount0\ls17}{\listoverride\listid817723855\listoverridecount0\ls18}
{\listoverride\listid883252113\listoverridecount0\ls19}{\listoverride\listid931165603\listoverridecount0\ls20}{\listoverride\listid460538651\listoverridecount0\ls21}{\listoverride\listid1519082647\listoverridecount0\ls22}{\listoverride\listid1409309414
\listoverridecount0\ls23}{\listoverride\listid1603100874\listoverridecount0\ls24}{\listoverride\listid833837180\listoverridecount0\ls25}{\listoverride\listid1451437919\listoverridecount0\ls26}{\listoverride\listid2037924622\listoverridecount0\ls27}
{\listoverride\listid1910965607\listoverridecount0\ls28}{\listoverride\listid385956681\listoverridecount0\ls29}{\listoverride\listid692221466\listoverridecount0\ls30}{\listoverride\listid1145585473\listoverridecount0\ls31}{\listoverride\listid1681084833
\listoverridecount0\ls32}{\listoverride\listid113646247\listoverridecount0\ls33}{\listoverride\listid271516146\listoverridecount0\ls34}{\listoverride\listid515391779\listoverridecount0\ls35}{\listoverride\listid331227567\listoverridecount0\ls36}
{\listoverride\listid1497262661\listoverridecount0\ls37}}{\info{\title DoxyFilter test}{\author tlarsu}{\operator tlarsu}{\creatim\yr2008\mo9\dy17\hr14\min51}{\revtim\yr2008\mo9\dy18\hr9\min32}{\version4}{\edmins40}{\nofpages10}{\nofwords2401}
{\nofchars13687}{\*\company TJK Tietolaite Oy}{\nofcharsws16808}{\vern59}}\margl1418\margr567\margt1134\margb794 \widowctrl\ftnbj\aenddoc\hyphcaps0\viewkind1\viewscale100 \fet0\sectd \sbknone\linex0\headery510\footery510\colsx709\sectdefaultcl {\header 
\pard\plain \nowidctlpar\brdrb\brdrs\brdrw15\brsp80 \tqr\tx10206\adjustright \fs20\cgrid {\field{\*\fldinst {\fs22  FILENAME \\p }}{\fldrslt {\fs22\lang1024 S:\\TL\\Doxyfilt\\testProg\\doc\\refman.rtf}}}{\fs22  (rev. }{\field{\*\fldinst {\fs22  REVNUM }
}{\fldrslt {\fs22\lang1024 3}}}{\fs22\lang1035 , }{\field{\*\fldinst {\fs22  PRINTDATE \\@ "d.M.yyyy" }}{\fldrslt {\fs22\lang1024 0.0.0000}}}{\fs22\lang1035 )\~\~\'a9 }{\field{\*\fldinst {\fs22  SAVEDATE \\@ "yyyy" }}{\fldrslt {\fs22\lang1024 2008}}}{
\fs22\lang1035 \~\~TJK Tietolaite Oy\tab }{\field{\*\fldinst {\cs24\fs22\lang1035  PAGE }}{\fldrslt {\cs24\fs22\lang1024 10}}}{\cs24\fs22\lang1035  (}{\field{\*\fldinst {\cs24\fs22\lang1035  NUMPAGES }}{\fldrslt {\cs24\fs22\lang1024 1}}}{
\cs24\fs22\lang1035 )}{\fs22\lang1035 
\par }\pard \nowidctlpar\adjustright {
\par }}{\footer \pard\plain \s19\sa964\nowidctlpar\tx1276\tqr\tx8504\adjustright \fs20\cgrid {
\par }}{\*\pnseclvl1\pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}
{\*\pnseclvl5\pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8
\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}

<xsl:variable name="innerGroupIds" select="/doxygen/compounddef/innergroup/@refid" />

<!-- Select all compounddef elements are not sub elements of other coumpoundef elements. -->
<xsl:for-each select="/doxygen/compounddef[not(@id=$innerGroupIds)]">

	<!-- ********************* Function block descriptions ********************* -->
	<xsl:if test="@kind = 'group'">
	
		<!-- Function block name -->
		<xsl:text>\pard\plain \s1\sb960\keep\keepn\nowidctlpar\outlinelevel0\adjustright \b\caps\f1\fs28\expnd2\expndtw10\lang1024\cgrid &#x0D;{</xsl:text>
		<xsl:value-of select="title"/>
		<xsl:text>\par}&#x0D;</xsl:text>

		<!-- Brief description -->
		<xsl:for-each select="briefdescription">
			<xsl:text>\pard\plain \s15\li567\sb120\nowidctlpar\adjustright \fs22\cgrid &#x0D;{</xsl:text>
			<xsl:value-of select="para"/>
			<xsl:text>}&#x0D;</xsl:text>
		</xsl:for-each>
		
		<!-- Detailed description -->
		<xsl:apply-templates select="detaileddescription" />
		
		<!-- Typedef and struct documentation sections -->
		<xsl:if test="sectiondef[@kind='typedef'] | innerclass[@refid] !=''">
			<xsl:text>\pard\plain \s2\sb240\keep\keepn\nowidctlpar\outlinelevel1\adjustright \b\f1\fs28\expnd1\expndtw5\lang1024\cgrid &#x0D;{</xsl:text>
			<xsl:text>Types</xsl:text>
			<xsl:text> \par}&#x0D;</xsl:text>
			
			<xsl:apply-templates select="sectiondef[@kind='typedef']" />
			<xsl:apply-templates select="innerclass" />
		</xsl:if>
		
		<!-- Function and variable documentation sections -->
		<xsl:apply-templates select="sectiondef[@kind!='typedef']" />

		<!-- Target specific sub groups of the FB -->
		<xsl:apply-templates select="innergroup" />
		
	</xsl:if>
	
</xsl:for-each>



\pard\plain \nowidctlpar\adjustright \fs20\cgrid {\fs24\cgrid0 \sect }\sectd \sbknone\linex0\headery510\footery510\colsx709\sectdefaultcl \pard\plain \s2\sb240\keep\keepn\nowidctlpar\outlinelevel1\adjustright \b\f1\fs28\expnd1\expndtw5\lang1024\cgrid {

\par }}
</xsl:template>
</xsl:stylesheet>
