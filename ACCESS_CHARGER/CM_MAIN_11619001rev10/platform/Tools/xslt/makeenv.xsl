<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- XSL file to create environment setting batch file.					-->
	<!-- Sets all variables set by old set_env.bat.							-->
	<!-- Input parameters:													-->	
	<!-- build	= build name in project.xml									-->
	
	<xsl:output method="text" version="1.0" encoding="UTF-8" indent="no" />
	<xsl:strip-space elements="*"/>
		
	<xsl:template match="project">
	
		<xsl:text>REM &#xA;</xsl:text>
		<xsl:text>REM Automatically generated batch for project environment.&#xA;</xsl:text>
		<xsl:text>REM &#xA;</xsl:text>
		<xsl:text>REM DO NOT MODIFY THIS FILE MANUALLY! Instead configure build in contents.xml&#xA;</xsl:text>		
		<xsl:text>REM and use tl-configure to create this file.&#xA;</xsl:text>		
		<xsl:text>REM &#xA;</xsl:text>	
		<xsl:text>@echo off&#xA;&#xA;</xsl:text>
		
		<xsl:text>ECHO Setting up environment&#xA;</xsl:text>
		<xsl:text>ECHO.&#xA;</xsl:text>
		
		<xsl:text>&#xA;</xsl:text>		
		<xsl:text>if not exist project.c (&#xA;</xsl:text>
		<xsl:text>	REM Not in exe directory directory&#xA;</xsl:text>
		<xsl:text>	echo ERROR: The batch should be run from the exe directory ^(project^.c not found^)&#xA;</xsl:text>
		<xsl:text>	goto end&#xA;</xsl:text>
		<xsl:text>)&#xA;</xsl:text>

		<!-- Get the hardware part of the target name -->
		<xsl:variable name="targetHw">
			<xsl:choose>
				<xsl:when test="project-information/builds/build[name=$build]/target-hw">
					<xsl:value-of select="project-information/builds/build[name=$build]/target-hw" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="project-information/builds/build[name=$build]/target" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>	

		<!-- Get the compiler part of the target name -->
		<xsl:variable name="targetCompiler">
			<xsl:choose>
				<xsl:when test="project-information/builds/build[name=$build]/target-hw">
					<xsl:value-of select="project-information/builds/build[name=$build]/target-compiler" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>gnu</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		
		<xsl:choose>
		
			<!-- Handle case where the user has specified an non-existent build configuration -->
			<xsl:when test="$targetHw=''">
				<xsl:text>&#xA;</xsl:text>		
				<xsl:text>echo Could not find the selected build configuration: </xsl:text><xsl:value-of select="$build"/><xsl:text>&#xA;</xsl:text>
				<xsl:text>echo. &#xA;</xsl:text>
				<xsl:text>echo The following build configurations are available:&#xA;</xsl:text>
				<xsl:for-each select="project-information/builds/build">
					<xsl:text>echo  </xsl:text><xsl:value-of select="name"/><xsl:text>&#xA;</xsl:text>
				</xsl:for-each>
				<xsl:text>echo. &#xA;</xsl:text>
				<xsl:text>echo Example: set_env_gnumake.bat </xsl:text><xsl:value-of select="project-information/builds/build[1]/name"/><xsl:text>&#xA;</xsl:text>
				<xsl:text>goto end&#xA;</xsl:text>
			</xsl:when>
			 
			<!-- Normal operation -->
			<xsl:otherwise>
				<xsl:text>&#xA;</xsl:text>			
				<xsl:text>REM &#xA;</xsl:text>		
				<xsl:text>REM Build specific settings&#xA;</xsl:text>				
				<xsl:text>REM &#xA;</xsl:text>				
				<xsl:apply-templates select="project-information/builds/build[name=$build]">
					<xsl:with-param name="targetHw" select="$targetHw" />
					<xsl:with-param name="targetCompiler" select="$targetCompiler" />	
				</xsl:apply-templates>

				<xsl:text>SET TLLIB_O=%TLLIB_B%_o_%TLLIB_OF%&#xA;</xsl:text>

				<xsl:text>&#xA;</xsl:text>					
				<xsl:text>REM &#xA;</xsl:text>		
				<xsl:text>REM General settings&#xA;</xsl:text>				
				<xsl:text>REM &#xA;</xsl:text>						
				<xsl:text>SET TLLIB_M=make.exe&#xA;</xsl:text>
				<xsl:text>SET TLLIB_ROOT=%CD%\..&#xA;</xsl:text>		
				<xsl:text>SET TLLIB_TOOLS=%TLLIB_ROOT%\platform\tools&#xA;</xsl:text>
				
				<xsl:text>&#xA;</xsl:text>		
				<xsl:text>SET path=%TLLIB_H%\bin;%TLLIB_TOOLS%;%path%&#xA;</xsl:text>

				<xsl:text>&#xA;</xsl:text>					
				<xsl:text>REM &#xA;</xsl:text>		
				<xsl:text>REM Include paths&#xA;</xsl:text>				
				<xsl:text>REM &#xA;</xsl:text>						
				<xsl:text>SET TLLIB_INC=-I. -I%CD% -I%CD%\inc</xsl:text>
				<xsl:text> -I%CD%\inc\inc_</xsl:text><xsl:value-of select="$targetHw" /><xsl:text>\</xsl:text><xsl:value-of select="$targetCompiler" />
				<xsl:text> -I%CD%\inc\inc_</xsl:text><xsl:value-of select="$targetHw" />
				<xsl:if test="project-information/builds/build[name=$build]/target-os">
					<xsl:text> -I%TLLIB_ROOT%\platform\inc\osa\%TLLIB_T_OS%</xsl:text>
				</xsl:if>
				<xsl:apply-templates select="libraries/*">
					<xsl:with-param name="targetHw" select="$targetHw" />
					<xsl:with-param name="targetCompiler" select="$targetCompiler" />	
				</xsl:apply-templates>
				
				<xsl:text>&#xA;&#xA;</xsl:text>
				<xsl:text>ECHO INFO: Compiler dir:     %TLLIB_H%&#xA;</xsl:text>
				<xsl:text>ECHO       Target:           %TLLIB_T% (%TLLIB_HW%)&#xA;</xsl:text>
				<xsl:text>ECHO       Build:            %TLLIB_B%&#xA;</xsl:text>

			</xsl:otherwise>
		</xsl:choose>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>:end</xsl:text>

	</xsl:template>
	
	<xsl:template match="build">
		<xsl:param name="targetHw" select="foo"/>
		<xsl:param name="targetCompiler" select="foo"/>		
		<xsl:text>SET TLLIB_H=</xsl:text><xsl:value-of select="compiler" /><xsl:text>&#xA;</xsl:text>
		<xsl:text>SET TLLIB_T=</xsl:text><xsl:value-of select="target" /><xsl:text>&#xA;</xsl:text>
		<xsl:text>SET TLLIB_T_HW=</xsl:text><xsl:value-of select="$targetHw" /><xsl:text>&#xA;</xsl:text>
		<xsl:text>SET TLLIB_T_COMP=</xsl:text><xsl:value-of select="$targetCompiler" /><xsl:text>&#xA;</xsl:text>
		<xsl:if test="target-os">
			<xsl:text>SET TLLIB_T_OS=</xsl:text><xsl:value-of select="target-os" /><xsl:text>&#xA;</xsl:text>
		</xsl:if>
		<xsl:text>SET TLLIB_HW=</xsl:text><xsl:value-of select="hw" /><xsl:text>&#xA;</xsl:text>
		<xsl:text>SET TLLIB_B=</xsl:text><xsl:value-of select="build-type" /><xsl:text>&#xA;</xsl:text>
		<xsl:text>SET TLLIB_OF=</xsl:text><xsl:value-of select="output-format" /><xsl:text>&#xA;</xsl:text>	
	</xsl:template>

	<xsl:template match="library">
		<xsl:param name="targetHw" select="foo"/>
		<xsl:param name="targetCompiler" select="foo"/>		
		<xsl:text> -I%TLLIB_ROOT%\</xsl:text><xsl:value-of select="location/filesystem-root" />
		<xsl:text> -I%TLLIB_ROOT%\</xsl:text><xsl:value-of select="location/filesystem-root" /><xsl:text>\inc\inc_</xsl:text><xsl:value-of select="$targetHw" /><xsl:text>\</xsl:text><xsl:value-of select="$targetCompiler" />
		<xsl:text> -I%TLLIB_ROOT%\</xsl:text><xsl:value-of select="location/filesystem-root" /><xsl:text>\inc\inc_</xsl:text><xsl:value-of select="$targetHw" />
		<xsl:text> -I%TLLIB_ROOT%\</xsl:text><xsl:value-of select="location/filesystem-root" /><xsl:text>\inc</xsl:text>
	</xsl:template>
	
</xsl:stylesheet>