<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- Returns the filename part of a path.  Equivalent to Unix 'basename'
		Examples:
		'index.html'  ->  'index.html' 
		'foo/bar/'  ->  '' 
		'foo/bar/index.html'  ->  'index.html' 
	-->
	<xsl:template name="filename">
		<xsl:param name="path"/>
  
		<xsl:choose>
			<xsl:when test="contains($path, '/')">
				<xsl:call-template name="filename">
					<xsl:with-param name="path" select="substring-after($path, '/')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$path"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- Returns the last extension of a filename in a path.
		Examples:
		'index.html'  ->  '.html' 
		'index.dtdx.html'  ->  '.html' 
		'foo/bar/'  ->  '' 
		'foo/bar/index.html'  ->  '.html' 
		'foo/bar/index'  ->  '' 
	-->	
	<xsl:template name="ext">
		<xsl:param name="path"/>
	  
		<xsl:param name="subflag"/> <!-- Outermost call? -->
		<xsl:choose>
			<xsl:when test="contains($path, '.')">
				<xsl:call-template name="ext">
					<xsl:with-param name="path" select="substring-after($path, '.')"/>
					<xsl:with-param name="subflag" select="'sub'"/>
				</xsl:call-template>
			</xsl:when>
		
			<!-- Handle extension-less filenames by returning '' -->
			<xsl:when test="not($subflag) and not(contains($path, '.'))">
				<xsl:text/>
			</xsl:when>
			
			<xsl:otherwise>
				<xsl:value-of select="concat('.', $path)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Returns the last extension of a filename in a path without the colon.
		Examples:
		'index.html'  ->  'html' 
		'index.dtdx.html'  ->  'html' 
		'foo/bar/'  ->  '' 
		'foo/bar/index.html'  ->  'html' 
		'foo/bar/index'  ->  '' 
	-->	
	<xsl:template name="onlyext">
		<xsl:param name="path"/>
	  
		<xsl:param name="subflag"/> <!-- Outermost call? -->
		<xsl:choose>
			<xsl:when test="contains($path, '.')">
				<xsl:call-template name="onlyext">
					<xsl:with-param name="path" select="substring-after($path, '.')"/>
					<xsl:with-param name="subflag" select="'sub'"/>
				</xsl:call-template>
			</xsl:when>
		
			<!-- Handle extension-less filenames by returning '' -->
			<xsl:when test="not($subflag) and not(contains($path, '.'))">
				<xsl:text/>
			</xsl:when>
			
			<xsl:otherwise>
				<xsl:value-of select="$path"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	
	<!-- Returns a filename of a path stripped of its last extension.
		Examples:
		'foo/bar/index.dtdx.html' -> 'index.dtdx'
	-->	
	<xsl:template name="filename-noext">
		<xsl:param name="path"/>
		
		<xsl:variable name="filename">
			<xsl:call-template name="filename">
				<xsl:with-param name="path" select="$path"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name="ext">
			<xsl:call-template name="ext">
			<xsl:with-param name="path" select="$filename"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:value-of select="substring($filename, 1, string-length($filename) - string-length($ext))"/>
	</xsl:template>	

	<!-- translation for lowercase-->
	<xsl:variable name="lcletters" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:variable name="ucletters" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

	<xsl:template name="str-tolower">
		<xsl:param name="str"/>
		<xsl:value-of select="translate($str, $ucletters, $lcletters)"/>
	</xsl:template>	
	
</xsl:stylesheet>
