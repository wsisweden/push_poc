<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- XSL file which will generate a GNU makefile when used to transform	-->
	<!-- a function block or project xml.									-->
	<!-- Input parameters:													-->
	<!-- target			= output target (cm3 etc)							-->
	<!-- targethw		= output target hardware (cm3 etc)					-->
	<!-- targetos		= target OS (FRE09M3 etc)							-->
	<!-- targetcompiler	= target compiler (gnu or iar)						-->
	<!-- includes		= compiler include paths 							-->
	<!-- defines		= compiler defines									-->
		
	<xsl:output method="text" version="1.0" encoding="UTF-8" indent="no" />
	<xsl:strip-space elements="*"/>
	
	<xsl:include href="utils.xsl" />

	<!-- Object file path (for example 'Obj_cm3\') -->
	<xsl:variable name="objectpath"><xsl:text>Obj_</xsl:text><xsl:value-of select="$target" /><xsl:text>/</xsl:text></xsl:variable>
	
	<!-- Template for main level -->
	<xsl:template match="function-block | project">

		<xsl:text># Automatically generated makefile. DO NOT MODIFY THIS FILE.&#xA;&#xA;</xsl:text>
				
		<!-- Generate bject list -->
		<xsl:text># Object files&#xA;</xsl:text>
		<xsl:text>objects:=</xsl:text>
		<xsl:apply-templates select="files/file[
			type='source'
			and (not(target) or target='all' or target=$target)
            and (not(target-hw) or target-hw=$targethw)
			and (not(target-os) or target-os=$targetos)
			and (not(target-compiler) or target-compiler=$targetcompiler)
		]" mode="objects"/>
		
		<!-- Include main makefile -->
		<xsl:text>&#xA;&#xA;# Include main make file&#xA;</xsl:text>
		<xsl:text>include $(TLLIB_L)\MAK\SRC_$(TLLIB_T)_GNU.MAK</xsl:text>	
		
		<!-- Generate object depenencies-->
		<xsl:text>&#xA;&#xA;# File specific rules&#xA;</xsl:text>
		<xsl:apply-templates select="files/file[
			type='source'
			and (not(target) or target='all' or target=$target)
            and (not(target-hw) or target-hw=$targethw)
			and (not(target-os) or target-os=$targetos)
			and (not(target-compiler) or target-compiler=$targetcompiler)
		]" mode="dependencies"/>
		
	</xsl:template>
	
	<!-- Template for object list. Applied to each accepted file in source file list. -->
	<xsl:template match="file" mode="objects">
		<xsl:variable name="basename">
			<xsl:call-template name="filename-noext">
				<xsl:with-param name="path" select="path"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:value-of select="$objectpath" /><xsl:value-of select="$basename" /><xsl:text>.$(TLLIB_O) </xsl:text>	
	</xsl:template>

	<!-- Template for dependencies list generation. Applied to each accepted file in source file list. -->
	<xsl:template match="file" mode="dependencies">
	
		<!-- Get file base name (no path or extension) -->
		<xsl:variable name="basename">
			<xsl:call-template name="filename-noext">
				<xsl:with-param name="path" select="path"/>
			</xsl:call-template>
		</xsl:variable>

		<!-- Get file extension -->
		<xsl:variable name="extension">
			<xsl:call-template name="onlyext">
				<xsl:with-param name="path" select="path"/>
			</xsl:call-template>
		</xsl:variable>

		<!-- Lower case extension -->
		<xsl:variable name="lowerextension">
			<xsl:call-template name="str-tolower">
				<xsl:with-param name="str" select="$extension"/>
			</xsl:call-template>
		</xsl:variable>
		
		<!-- Object file name -->
		<xsl:variable name="objectname"><xsl:value-of select="$objectpath" /><xsl:value-of select="$basename" /><xsl:text>.$(TLLIB_O)</xsl:text></xsl:variable>
			
		<!-- Object file and dependency to c file -->
		<xsl:value-of select="$objectname" /><xsl:text> : </xsl:text><xsl:value-of select="path" />

		<!-- Dependencies from depenency file (the second argument to document makes path relative to xml instead of xsl) -->		
		<xsl:variable name="dependencypath">Dep_<xsl:value-of select="$target" />/<xsl:value-of select="$basename" />.xml</xsl:variable>
		<xsl:apply-templates select="document($dependencypath,/)/dependencies"/>


		<xsl:choose>
			<xsl:when test="path='project.c'">
				<xsl:text>&#xA;	@call preBuild.bat </xsl:text><xsl:value-of select="$includes" /><xsl:text> </xsl:text><xsl:value-of select="$defines" />
			<!--
				<xsl:text>&#xA;	@tl-podco.exe /b /i /t /L /g /d /p..\\ </xsl:text><xsl:value-of select="$includes" /><xsl:text> </xsl:text><xsl:value-of select="$defines" /><xsl:text> /f</xsl:text><xsl:value-of select="path" />
				-->
			</xsl:when>
		</xsl:choose>
		
		<xsl:text>&#xA;	@tl-podco.exe /e /d /p..\..\..\\ </xsl:text><xsl:value-of select="$includes" /><xsl:text> </xsl:text><xsl:value-of select="$defines" /><xsl:text> /f</xsl:text><xsl:value-of select="path" />
		<xsl:text>&#xA;	$(call compile_</xsl:text><xsl:value-of select="$lowerextension" /><xsl:text>)</xsl:text>			
		
		<xsl:text>&#xA;&#xA;</xsl:text>			
	</xsl:template>

	<!-- Template for dependency list output from external xml. -->
	<xsl:template match="dependencies">
		<xsl:for-each select="dependency">
			<xsl:text> </xsl:text><xsl:value-of select="path" />
		</xsl:for-each>		
	</xsl:template>
	
</xsl:stylesheet>
