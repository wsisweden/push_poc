<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!-- XSL file which will generate a NMAKE makefile when used to transform	-->
	<!-- a function block or project xml.										-->
	<!-- Input parameters:														-->
	<!-- target	= output target (cm3 etc)										-->

	<xsl:output method="text" version="1.0" encoding="UTF-8" indent="no" />
	<xsl:strip-space elements="*"/>
	
	<xsl:include href="utils.xsl" />

	<!-- Object file path (for example 'Obj_cm3\') -->
	<xsl:variable name="objectpath"><xsl:text>Obj_</xsl:text><xsl:value-of select="$target" /><xsl:text>\</xsl:text></xsl:variable>
	
	<!-- Template for main level -->
	<xsl:template match="function-block | project">
	
		<!-- Generate bject list -->
		<xsl:text>objects=</xsl:text>
		<xsl:apply-templates select="files/file[type='source' and (target='all' or target=$target)]" mode="objects"/>
		
		<!-- Include main makefile -->
		<xsl:text>&#xA;&#xA;!include $(TLLIB_R)\MAK\SRC_$(TLLIB_T).MAK&#xA;&#xA;</xsl:text>	

		<!-- Generate object depenencies-->
		<xsl:apply-templates select="files/file[type='source' and (target='all' or target=$target)]" mode="dependencies"/>
			
	</xsl:template>
	
	<!-- Template for object list. Applied to each accepted file in source file list. -->
	<xsl:template match="file" mode="objects">
		<xsl:variable name="basename">
			<xsl:call-template name="filename-noext">
				<xsl:with-param name="path" select="path"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:value-of select="$objectpath" /><xsl:value-of select="$basename" /><xsl:text>.$(TLLIB_O) </xsl:text>	
	</xsl:template>

	<!-- Template for dependencies list generation. Applied to each accepted file in source file list. -->
	<xsl:template match="file" mode="dependencies">
		<xsl:variable name="basename">
			<xsl:call-template name="filename-noext">
				<xsl:with-param name="path" select="path"/>
			</xsl:call-template>
		</xsl:variable>
		
		<!-- Object file and dependency to c file -->
		<xsl:value-of select="$objectpath" /><xsl:value-of select="$basename" /><xsl:text>.$(TLLIB_O) : </xsl:text><xsl:value-of select="path" />

		<!-- Dependencies from depenency file (the second argument to document makes path relative to xml instead of xsl) -->		
		<xsl:variable name="dependencypath">Dep_<xsl:value-of select="$target" />/<xsl:value-of select="$basename" />.xml</xsl:variable>
		<xsl:apply-templates select="document($dependencypath,/)/dependencies"/>
		
		<xsl:text>&#xA;</xsl:text>			
	</xsl:template>

	<!-- Template for dependency list output from external xml. -->
	<xsl:template match="dependencies">
		<xsl:for-each select="dependency">
			<xsl:text> </xsl:text><xsl:value-of select="path" />
		</xsl:for-each>		
	</xsl:template>
	
</xsl:stylesheet>
