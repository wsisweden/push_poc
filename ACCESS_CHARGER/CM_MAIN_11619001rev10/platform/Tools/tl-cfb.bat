::
:: Batch file for function block generation.
::
:: Adds function block folder and basic template files (.c, init.h and .h files).
::
@echo off
SETLOCAL EnableDelayedExpansion

:: Check number of arguments
if [%1]==[] goto usage
if [%2]==[] goto usage

:: Check we are in project root folder
if not exist .\EXE (
	echo This batch file must be executed in the root folder of the project.
	GOTO:EOF
)

set TMPL_ROOT=%cd%\platform\Tools\templates
set LIB_FLDR=%cd%\%1
set TRG_FLDR=%LIB_FLDR%\Src\%2
set INC_FLDR=%LIB_FLDR%\Inc

:: Create target forlder if it does not exist, prompt user for overwrite if it does.
if not exist %TRG_FLDR% (
	echo Creating folder %TRG_FLDR%	
	md %TRG_FLDR%
	set continue=y
) else (
	echo WARNING! Target folder already exists. Data may be overwritten.
	set /P continue=Continue [y/n]? 
)

:: Check if we are to continue or cancel
if /i {%continue%}=={y} (goto :replace)
if /i {%continue%}=={yes} (goto :replace)	
goto :cancel

::
:: Do the template copy / replace
::
:replace
	echo Copying file %TRG_FLDR%\MAKEFILE
	call TextReplace.exe "%TMPL_ROOT%/fb_MAKEFILE" -r/name@lwc/%2/l -r/name@upc/%2/u "-o%TRG_FLDR%\MAKEFILE" -s
	if ERRORLEVEL 1 goto error

	echo Copying file %TRG_FLDR%\%2.c
	call TextReplace.exe "%TMPL_ROOT%/fb_fb.c" -r/name@lwc/%2/l -r/name@upc/%2/u "-o%TRG_FLDR%\%2.c" -s
	if ERRORLEVEL 1 goto error

	echo Copying file %TRG_FLDR%\init.h
	call TextReplace.exe "%TMPL_ROOT%/fb_init.h" -r/name@lwc/%2/l -r/name@upc/%2/u "-o%TRG_FLDR%\init.h" -s
	if ERRORLEVEL 1 goto error

	echo Copying file %INC_FLDR%\%2.h
	call TextReplace.exe "%TMPL_ROOT%/fb_fb.h" -r/name@lwc/%2/l -r/name@upc/%2/u "-o%INC_FLDR%\%2.h" -s
	if ERRORLEVEL 1 goto error
	
	echo Copying file %INC_FLDR%\contents.xml
	call TextReplace.exe "%TMPL_ROOT%/fb_contents.xml" -r/name@lwc/%2/l -r/name@upc/%2/u "-o%TRG_FLDR%\contents.xml" -s
	if ERRORLEVEL 1 goto error
	
	echo Function block created.
	GOTO:EOF

::
:: Show usage
::
:usage
	echo Batch file for creating function blocks.
	echo.
	echo Execute in projects root folder.
	echo.
	echo Usage: tl-cfb ^<library^> ^<name^> 
	echo. 
	echo     ^<library^>      Library name	
	echo     ^<name^>         Function block name
	echo.
	GOTO:EOF

::
:: Error exit
::
:error
	echo Function block creation failed.
	GOTO:EOF
	
::
:: Cancelled exit
::
:cancel
	echo Function block creation cancelled.
	GOTO:EOF