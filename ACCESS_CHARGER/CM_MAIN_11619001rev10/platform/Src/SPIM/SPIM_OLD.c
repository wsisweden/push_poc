/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPIM_OLD.C
*
*	\ingroup	SPIM
*
*	\brief		SPI master old access function.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 3631 $ \n
*				\$Date: 2019-01-28 09:34:51 +0200 (ma, 28 tammi 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "deb.h"
#include "lib.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void spim__access(void *,protif_Request *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * This is the public interface object. A pointer to this object should be sent
 * to all FBs that will be using SPIM. The type of the interface object is
 * declared by the PROTIF interface.
 */

PUBLIC protif_Interface	const_P	spim_interface = {
	&spim__access
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__access
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Queue request for handling.
*
*	\param		pVoidInst 	Pointer to the FB instance
*	\param		pRequest 	Pointer to the request
*
*	\details	Adds the supplied request pointer to the request FIFO and signal
*				the task. SPIM will call the request callback function when the
*				request has been handled.
*
*				Using the spim__fastAccess function will use a bit more stack
*				but it is faster than this function.
*
*	\note		This function is called by another FB trough the spim_interface.
*
*******************************************************************************/

PRIVATE void spim__access(
	void *					pVoidInst,
	protif_Request *		pRequest
) {
	spim__Inst * pInst;

	pInst = (spim__Inst *) pVoidInst;

	deb_assert(pRequest != NULL);

	/*
	 *	SPIM could be made to work without OSA running. It would then work
	 *	by bypassing the fifo. This access function could then call the low
	 *	processing function directly. The callback would then have to be called
	 *	from the ISR which could cause problems.
	 *
	 *	Currently SPIM does not support running without OSA.
	 */

	deb_assert(osa_isRunning);

	DISABLE;
	if (pInst->reserved) {
		/*
		 *	The bus has been reserved by some FB. The requests from that FB will
		 *	be executed directly bypassing the fifo entirely.
		 *	Requests from other FBs are put into the fifo and will be handled 
		 *	after the bus has been released.
		 */

		if (pRequest->data.flags & PROTIF_FLAG_RES) {
			/*
			 *	This request should be from the FB that has reserved the bus.
			 */

			deb_assert(pInst->pCurrReq == NULL);

			pInst->pCurrReq = pRequest;
			pInst->flags |= SPIM__FLAG_NEWREQ;	
			ENABLE;
			osa_coTaskRun(&pInst->coTask);

		} else {
			/*
			 *	This request is from an FB that has not reserved the bus. 
			 */
			lib_fifoPutNonProt(&pInst->reqFifo, (void **) &pRequest->pNext);
			ENABLE;
		}

	} else {
		if (pInst->pCurrReq == NULL) {
			/*
			 *	SPIM is idle at the moment. We bypass the FIFO at this point and
			 *	put the request directly as current request. This  
			 *	implementation will always trigger the CoTask and start handling
			 *	the request from there.
			 */

			pInst->pCurrReq = pRequest;
			pInst->flags |= SPIM__FLAG_NEWREQ;	
			ENABLE;
			osa_coTaskRun(&pInst->coTask);

		} else {
			/*
			 *	SPIM is currently handling another request. Put the received
			 *	request on the FIFO.
			 */
			lib_fifoPutNonProt(&pInst->reqFifo, (void **) &pRequest->pNext);
			ENABLE;
		}
	}
}
