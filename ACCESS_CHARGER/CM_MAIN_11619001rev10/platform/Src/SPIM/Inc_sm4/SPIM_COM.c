/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_SM4
*
*	\brief		Helper functions for STM32F3xx SPI peripheral driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 3918 $ \n
*				\$Date: 2019-09-20 11:49:04 +0300 (pe, 20 syys 2019) $ \n
*				\$Author: tldati $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	SPIM_SM4	Cortex-M4
*
*	\ingroup	SPIM
*
*	\brief
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "osa.h"
#include "hw.h"

#include "../local.h"
#include "spim_com.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__comInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize SSP bus to SPI master mode.
*
*	\param		pInst 	Pointer to SPIM instance.
*	\param		pSsp	Pointer to SSP peripheral base.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void spim__comInit(
	spim__Inst *			pInst,
	SPI_TypeDef *			pSsp
) {
	Uint8					dummy;

	/*
	 * Clear the RX fifo by reading data register eight times.
	 */
	
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;

	dummy = dummy; /* Just to prevent compiler warning */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__comBusSettings
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set bus settings.
*
*	\param		pInst 	Pointer to SPIM instance.
*	\param		pSsp	Pointer to SSP peripheral base.
*	\param		pSlave	Pointer to slave info containing the bus settings.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void spim__comBusSettings(
	spim__Inst *			pInst,
	SPI_TypeDef *			pSsp,
	spim_SlaveInfo const_P *pSlave
) {
	Uint32					leadingZeros;
	Uint32					cr2Value;

	/*
	 * Disable SPI before changing CPOL/CPHA bits
	 * => master transmit mode, disable the SPI when TXE = 1 and BSY = 0
	 */
	pSsp->CR1 = 0;

	/*
	 * Set master mode.
	 */
	pSsp->CR1 |= SPI_CR1_MSTR;
	pSsp->CR1 |= SPI_CR1_SSM;
	pSsp->CR1 |= SPI_CR1_SSI;

	/*
	 * Set the clock settings (polarity and phase).
	 * Need to configure pull-up/pull-down for SCK.
	 */
	if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
		pSsp->CR1 |= SPI_CR1_CPOL;

		if (pSlave->flags & SPIM_PHASE_RISING) {
			pSsp->CR1 |= SPI_CR1_CPHA;
		}
		
	} else {
		if ((pSlave->flags & SPIM_PHASE_RISING) == 0) {
			pSsp->CR1 |= SPI_CR1_CPHA;
		}
	}

	/*
	 * Select the SPI bus clock.
	 */
	leadingZeros = lib_clz8(pSlave->clkDiv);
	leadingZeros &= 0x7;

	pSsp->CR1 |= leadingZeros<<SPI_CR1_BR_Pos;	

	/*
	 * CR2 register modifications
	 *	Set SSP frame length. Only 4...16 bits are supported on this bus.
	 *	Enable error interrupt
	 */
	deb_assert(pSlave->frameBits >= 4);
	deb_assert(pSlave->frameBits <= 16);

	cr2Value = 
		((pSlave->frameBits - 1) << SPI_CR2_DS_Pos)
		|	SPI_CR2_ERRIE;

	/*
	 * Configure RX FIFO threshold for 8-bit accesses
	 */
	if (pSlave->frameBits <= 8) {
		cr2Value |= SPI_CR2_FRXTH;
	}

	pSsp->CR2 = cr2Value;

	/*
	 * Enable SPI after changing CPOL/CPHA bits
	 */
	pSsp->CR1 |= SPI_CR1_SPE;
}
