/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_SM4
*
*	\brief		SPI1 driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4431 $ \n
*				\$Date: 2021-02-02 17:56:44 +0200 (ti, 02 helmi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "hw.h"

#include "../local.h"
#include "SPIM_P1.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE spim__Inst * 		spim__lowInit(spim_Init const_P *);
PRIVATE void 				spim__lowProcess(spim__Inst *);
PRIVATE void 				spim__lowUp(spim__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for physical bus SPI1 with SCK on PA5, MISO on PA6 and MOSI on PA7.
 */

PUBLIC spim__Tools const_P	spim__tools1pap5 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/**
 * Tools for physical bus SPI1 without any pin configuration.
 */

PUBLIC spim__Tools const_P	spim__tools1NoConf = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwSetupPinsAndClock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure clock and setup pins for SSP1 peripheral.
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pSetupPinsAndClock(
	spim_Init const_P *			pInit
) {
	/*
	 * Enable clock for SPI1
	 */
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SPI1);

	if (pInit->pTools == &spim__tools1pap5) {

		GPIO_TypeDef *			GPIO;
		LL_GPIO_InitTypeDef		pinInit;

		GPIO = (GPIO_TypeDef *)GPIOA;

		/*
		 * SCK = PORTA, pin 5
		 */
		pinInit.Pin = 1<<5;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_5;
		LL_GPIO_Init(GPIO, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);

		/*
		 * MISO = PORTA, pin 6
		 */
		pinInit.Pin = 1<<6;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_5;
		LL_GPIO_Init(GPIO, &pinInit);

		/*
		 * MOSI = PORTA, pin 7
		 */
		pinInit.Pin = 1<<7;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_5;
		LL_GPIO_Init(GPIO, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__pConfigPinsForSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure pins for current slave before SPI request
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pConfigPinsForSlave(
	spim_Init const_P *			pInit,
	spim_SlaveInfo const_P *	pSlave
) {
	GPIO_TypeDef *			GPIO;

	/*
	 * Change pull-up/pull-down for SCK depending in slave
	 */
	if (pInit->pTools == &spim__tools1pap5) {

		GPIO = (GPIO_TypeDef *)GPIOA;

		if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
			LL_GPIO_SetPinPull(GPIO, 1<<5, LL_GPIO_PULL_UP);
		} else {
			LL_GPIO_SetPinPull(GPIO, 1<<5, LL_GPIO_PULL_DOWN);
		}
	}
}

/* Include the generic SSP driver */
#include "SPIM_SSP.c"
