/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_SM4
*
*	\brief		SPI3 driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4449 $ \n
*				\$Date: 2021-03-10 13:19:10 +0200 (ke, 10 maalis 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "hw.h"

#include "../local.h"
#include "SPIM_P3.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE spim__Inst * 		spim__lowInit(spim_Init const_P *);
PRIVATE void 				spim__lowProcess(spim__Inst *);
PRIVATE void 				spim__lowUp(spim__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for physical bus SPI3 with SCK on port B pin 3
 */

PUBLIC spim__Tools const_P	spim__tools3pbp3 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/**
 * Tools for physical bus SPI3 with SCK on port C pin 10
 */

PUBLIC spim__Tools const_P	spim__tools3pcp10 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwSetupPinsAndClock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure clock and setup pins for SSP1 peripheral.
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pSetupPinsAndClock(
	spim_Init const_P *			pInit
) {
	LL_GPIO_InitTypeDef			pinInit;

	/*
	 * Enable clock for SPI2
	 */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI3);

	if (pInit->pTools == &spim__tools3pbp3) {

		GPIO_TypeDef *			GPIO;

		GPIO = (GPIO_TypeDef *)GPIOB;

#if 1
		/*
		 * SCK = PORTB, pin 3
		 * Comment this out, if need to use PB3 as SWO pin for profiling
		 */
		pinInit.Pin = 1<<3;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_6;
		LL_GPIO_Init(GPIO, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);
#endif
		/*
		 * MISO = PORTB, pin 4
		 */
		pinInit.Pin = 1<<4;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_6;
		LL_GPIO_Init(GPIO, &pinInit);

		/*
		 * MOSI = PORTB, pin 5
		 */
		pinInit.Pin = 1<<5;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_6;
		LL_GPIO_Init(GPIO, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);

	} else if (pInit->pTools == &spim__tools3pcp10) {

		/*
		 * SCK = PORTC, pin 10
		 */
		pinInit.Pin = LL_GPIO_PIN_10;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_6;
		LL_GPIO_Init(GPIOC, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);

		/*
		 * MISO = PORTB, pin 4
		 */
		pinInit.Pin = LL_GPIO_PIN_11;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_6;
		LL_GPIO_Init(GPIOC, &pinInit);

		/*
		 * MOSI = PORTC, pin 12
		 */
		pinInit.Pin = LL_GPIO_PIN_12;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_6;
		LL_GPIO_Init(GPIOC, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__pConfigPinsForSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure pins for current slave before SPI request
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pConfigPinsForSlave(
	spim_Init const_P *			pInit,
	spim_SlaveInfo const_P *	pSlave
) {
	GPIO_TypeDef *			GPIO;

	/*
	 * Change pull-up/pull-down for SCK depending in slave
	 */
	if (pInit->pTools == &spim__tools3pbp3) {

		GPIO = (GPIO_TypeDef *)GPIOB;

		if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
			LL_GPIO_SetPinPull(GPIO, 1<<3, LL_GPIO_PULL_UP);
		} else {
			LL_GPIO_SetPinPull(GPIO, 1<<3, LL_GPIO_PULL_DOWN);
		}

	} else if (pInit->pTools == &spim__tools3pcp10) {

		GPIO = (GPIO_TypeDef *)GPIOC;

		if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
			LL_GPIO_SetPinPull(GPIO, 1<<10, LL_GPIO_PULL_UP);
		} else {
			LL_GPIO_SetPinPull(GPIO, 1<<10, LL_GPIO_PULL_DOWN);
		}
	}
}

/* Include the generic SSP driver */
#include "SPIM_SSP.c"
