/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_arm7/SPIM_COM.H
*
*	\ingroup	SPIM_ARM7
*
*	\brief		Common declarations for all SPIM ARM7 files
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_COM_H_INCLUDED
#define SPIM_COM_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/* bits */
#define SPIM__SSPNCR0_DSS	0			/**< Data size select               */
#define SPIM__SSPNCR0_FRF	4			/**< Frame format                   */
#define SPIM__SSPNCR0_CPOL	6			/**< Clock Out Polarity             */
#define SPIM__SSPNCR0_CPHA	7			/**< Clock Out Phase                */
#define SPIM__SSPNCR0_SCR 	8			/**< Serial Clock Rate              */

#define SPIM__SSPNCR1_MS	0			/**< Master/Slave mode (0=master)   */
#define SPIM__SSPNCR1_SSE	1			/**< SSP Enable (1=enabled)         */

#define SPIM__SSPNIMSC_RORIM	(1<<0)	/**< Receive overrun                */
#define SPIM__SSPNIMSC_RTIM	(1<<1)		/**< Receive timeout                */
#define SPIM__SSPNIMSC_RXIM	(1<<2)		/**< RX FIFO is at least half full  */
#define SPIM__SSPNIMSC_TXIM	(1<<3)		/**< TX FIFO is at least half empty */

#define SPIM__SSPNSR_TFE 	(1<<0)		/**< 1=Transmit FIFO Empty          */
#define SPIM__SSPNSR_TNF 	(1<<1)		/**< 1=Transmit FIFO Not Full.      */
#define SPIM__SSPNSR_RNE 	(1<<2)		/**< 1=Receive FIFO Not Empty.      */
#define SPIM__SSPNSR_RFF 	(1<<3)		/**< 1=Receive FIFO Full.           */
#define SPIM__SSPNSR_BSY 	(1<<4)		/**< 1=Busy                         */

#define SPIM__SSPNMIS_RO 	(1<<0)		/**< Receive overrun                */
#define SPIM__SSPNMIS_RT 	(1<<1)		/**< Receive timeout                */
#define SPIM__SSPNMIS_RX	(1<<2)		/**< RX FIFO is at least half full  */
#define SPIM__SSPNMIS_TX	(1<<3)		/**< TX FIFO is at least half empty */

/* values */
#define SPIM__FRF_SPI		0			/**< SPI mode                       */
#define SPIM__MS_MASTER		0			/**< Master mode                    */
#define SPIM__PCLKSEL_DIV1	1			/**< CCLK divided by 1              */

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/**
 *	The instance type used by SPIM instances on lpc17xx MCUs without DMA.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	spim__Inst				parent;		/**< The common instance part		*/
	protif_Data * 			pCurrData;	/**< Ptr to data element being handled*/
	Uint16 					rxIndex;	/**< Data buffer index for RX		*/
	Uint16					txIndex;	/**< Data buffer index for TX		*/
	Uint16 					rxOverFlCnt;/**< RX FIFO overflow counter		*/
} spim__HwInst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
