/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_arm7/SPIM_0.H
*
*	\ingroup	SPIM_ARM7
*
*	\brief
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_0_H_INCLUDED
#define SPIM_0_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "spim_com.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

#ifndef SPIM__PINSEL_FUNC

/* values */
#define SPIM__PINSEL_FUNC	3			/**< Pin function                   */

/* bits */
#define SPIM__PCLKSEL_SPI	16			/**< SPI clock select bits          */

#define SPIM__PINSEL_SCK	30			/**< SPI clock bits                 */
#define SPIM__PINSEL_MISO	2			/**< SPI MISO bits                  */
#define SPIM__PINSEL_MOSI	4			/**< SPI MOSI bits                  */

#define SPIM__PCONP_SPI		8			/**< Power Control bit              */

#define SPIM__S0SPCR_BITS	8			/**< Frame size bits	            */

/* registers */
#define SPIM__REG_PCLKSEL	PCLKSEL0	/**< Peripheral Clock Selection reg */

/* variables */
#define SPIM__VAR_TOOLS		spim__tools0
#define SPIM__VAR_PINST		spim__pInst0

#endif


/* bits */
#define SPIM__S0SPCR_BENA	(1<<2)		/**< Bit enable. 0=8Bit frames      */
#define SPIM__S0SPCR_SPIE	(1<<7)		/**< Interrupt enable bit           */
#define SPIM__S0SPCR_CPHA	(1<<3)		/**< Clock phase                    */
#define SPIM__S0SPCR_CPOL	(1<<4)		/**< Clock polarity                 */
#define SPIM__S0SPCR_MSTR	(1<<5)		/**< 1=Master mode                  */

#define SPIM__S0SPSR_SPIF	(1<<7)		/**< Transfer complete flag         */

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

#define spim__enableVicInt()				\
{											\
	VICIntEnable |= 1 << 10;				\
	VICVectAddr10 = (long) spim__isr01;		\
	VICVectCntl10 = 1;						\
}

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

PUBLIC osa_isrOSDecl(spim__isr01);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern spim__HwInst * 		spim__pInst0;

/*****************************************************************************/

#endif
