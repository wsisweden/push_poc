/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_arm7/SPIM_2.H
*
*	\ingroup	SPIM_ARM7
*
*	\brief		SPIM register and variable defines
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_2_H_INCLUDED
#define SPIM_2_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "spim_com.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

#ifndef SPIM__PINSEL_FUNC

/* values */
#define SPIM__PINSEL_FUNC	2			/**< Pin function                   */

/* bits */
#define SPIM__PCLKSEL_SPI	20			/**< SSP clock select bits          */

#define SPIM__PINSEL_SCK	14			/**< SSP clock bits                 */
#define SPIM__PINSEL_MISO	16			/**< SSP MISO bits                  */
#define SPIM__PINSEL_MOSI	18			/**< SSP MOSI bits                  */

#define SPIM__PCONP_SPI		10			/**< Power Control bit              */

/* registers */
#define SPIM__REG_PINSEL	PINSEL0		/**< Used pinsel register           */
#define SPIM__REG_PCLKSEL	PCLKSEL0	/**< Peripheral Clock Selection reg */

/* variables */
#define SPIM__VAR_TOOLS		spim__tools2/**< Instance hardware tools        */
#define SPIM__VAR_PINST		spim__pInst2

#endif

/* registers */
#define SPIM__REG_SSPNCR0	SSP1CR0		/**< SSPn Control Register 0        */
#define SPIM__REG_SSPNCR1	SSP1CR1		/**< SSPn Control Register 1        */
#define SPIM__REG_SSPNIMSC 	SSP1IMSC	/**< SSPn Int. Mask Set/Clear Register*/
#define SPIM__REG_SSPNDR	SSP1DR		/**< SSPn Data register             */
#define SPIM__REG_SSPNSR	SSP1SR		/**< Status register                */
#define SPIM__REG_SSPNICR	SSP1ICR		/**< SSPn Interrupt Clear Register  */
#define SPIM__REG_SSPNMIS	SSP1MIS		/**< SSPn Masked Int. Status register*/
#define SPIM__REG_SSPNCPSR	SSP1CPSR	/**< SSPn Clock Prescale Register   */

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

#define spim__enableVicInt()				\
{											\
	VICIntEnable |= 1 << 11;				\
	VICVectAddr11 = (long) spim__isr2;		\
	VICVectCntl11 = 1;						\
}

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

PUBLIC osa_isrOSDecl(spim__isr2);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern spim__HwInst * 		spim__pInst2;

/*****************************************************************************/

#endif
