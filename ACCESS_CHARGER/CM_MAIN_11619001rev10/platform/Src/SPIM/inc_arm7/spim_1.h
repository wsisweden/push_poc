/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_arm7/SPIM_1.H
*
*	\ingroup	SPIM_ARM7
*
*	\brief		SPIM register and variable defines
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_1_H_INCLUDED
#define SPIM_1_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "spim_com.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

#ifndef SPIM__PINSEL_FUNC

/* values */
#define SPIM__PINSEL_FUNC	3			/**< Pin function                   */

/* bits */
#define SPIM__PCLKSEL_SPI	10			/**< SPI clock select bits          */

#define SPIM__PINSEL_SCK	8			/**< SPI clock bits                 */
#define SPIM__PINSEL_MISO	14			/**< SPI MISO bits                  */
#define SPIM__PINSEL_MOSI	16			/**< SPI MOSI bits                  */

#define SPIM__PCONP_SPI		21			/**< Power Control bit              */

/* registers */
#define SPIM__REG_PINSEL	PINSEL3		/**< Used pinsel register           */
#define SPIM__REG_PCLKSEL	PCLKSEL1	/**< Peripheral Clock Selection reg */

/* variables */
#define SPIM__VAR_TOOLS		spim__tools1/**< Instance hardware tools        */
#define SPIM__VAR_PINST		spim__pInst1

#endif

/* registers */
#define SPIM__REG_SSPNCR0	SSP0CR0		/**< SSPn Control Register 0        */
#define SPIM__REG_SSPNCR1	SSP0CR1		/**< SSPn Control Register 1        */
#define SPIM__REG_SSPNIMSC 	SSP0IMSC	/**< SSPn Int. Mask Set/Clear Register*/
#define SPIM__REG_SSPNDR	SSP0DR		/**< SSPn Data register             */
#define SPIM__REG_SSPNSR	SSP0SR		/**< Status register                */
#define SPIM__REG_SSPNICR	SSP0ICR		/**< SSPn Interrupt Clear Register  */
#define SPIM__REG_SSPNMIS	SSP0MIS		/**< SSPn Masked Int. Status register*/
#define SPIM__REG_SSPNCPSR	SSP0CPSR	/**< SSPn Clock Prescale Register   */

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

#define spim__enableVicInt()				\
{											\
	VICIntEnable |= 1 << 10;				\
	VICVectAddr10 = (long) spim__isr01;		\
	VICVectCntl10 = 1;						\
}

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

PUBLIC osa_isrOSDecl(spim__isr01);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern spim__HwInst * 		spim__pInst1;

/*****************************************************************************/

#endif
