/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_SM4
*
*	\brief		SPI2 register and variable defines
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_2_H_INCLUDED
#define SPIM_2_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/* variables */
#define SPIM__VAR_PINST		spim__pInst2

/* interrupt */
#define SPIM__IRQN			SPI2_IRQn		/**< IRQ number.				*/
#define SPIM__FN_ISR		SPI2_IRQHandler	/**< ISR name.					*/

/* peripheral and registers */
#define SPIM__PERIPH		SPI2			/**< Peripheral pointer.		*/
#define SPIM__REG_SR		SPI2->SR		/**< Status register            */
#define SPIM__REG_DR8		((volatile Uint8 *)&SPI2->DR)/**< Data register	*/
#define SPIM__REG_DR16		((volatile Uint16 *)&SPI2->DR)/**< Data register*/
#define SPIM__REG_CR2		SPI2->CR2		/**< Control Register 2			*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
