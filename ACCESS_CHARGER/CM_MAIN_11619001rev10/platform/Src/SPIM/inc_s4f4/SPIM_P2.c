/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_S4F4
*
*	\brief		SPI2 driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4449 $ \n
*				\$Date: 2021-03-10 13:19:10 +0200 (ke, 10 maalis 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "hw.h"

#include "../local.h"
#include "SPIM_P2.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE spim__Inst * 		spim__lowInit(spim_Init const_P *);
PRIVATE void 				spim__lowProcess(spim__Inst *);
PRIVATE void 				spim__lowUp(spim__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for physical bus SPI2 with SCK on PB13, MISO on PB14 and MOSI on PB15.
 */

PUBLIC spim__Tools const_P	spim__tools2pbp13 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwSetupPinsAndClock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure clock and setup pins for SSP1 peripheral.
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pSetupPinsAndClock(
	spim_Init const_P *			pInit
) {
	/*
	 * Enable clock for SPI2
	 */
	//LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);
	__HAL_RCC_SPI2_CLK_ENABLE();

	if (pInit->pTools == &spim__tools2pbp13) {
		GPIO_InitTypeDef gpioInit;

		/*
		 * SCK = PORTB, pin 13
		 */
		gpioInit.Pin       = GPIO_PIN_13;
		gpioInit.Mode      = GPIO_MODE_AF_PP;
		gpioInit.Pull      = GPIO_NOPULL;
		gpioInit.Speed     = GPIO_SPEED_FREQ_HIGH;
		gpioInit.Alternate = GPIO_AF5_SPI2;
		HAL_GPIO_Init(GPIOB, &gpioInit);

		/*
		 * MISO = PORTB, pin 14
		 */
		gpioInit.Pin       = GPIO_PIN_14;
		gpioInit.Mode      = GPIO_MODE_AF_PP;
		gpioInit.Pull      = GPIO_PULLUP;
		gpioInit.Speed     = GPIO_SPEED_FREQ_HIGH;
		gpioInit.Alternate = GPIO_AF5_SPI2;
		HAL_GPIO_Init(GPIOB, &gpioInit);

		/*
		 * MOSI = PORTB, pin 15
		 */
		gpioInit.Pin       = GPIO_PIN_15;
		gpioInit.Mode      = GPIO_MODE_AF_PP;
		gpioInit.Pull      = GPIO_NOPULL;
		gpioInit.Speed     = GPIO_SPEED_FREQ_HIGH;
		gpioInit.Alternate = GPIO_AF5_SPI2;
		HAL_GPIO_Init(GPIOB, &gpioInit);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__pConfigPinsForSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure pins for current slave before SPI request
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pConfigPinsForSlave(
	spim_Init const_P *			pInit,
	spim_SlaveInfo const_P *	pSlave
) {
	GPIO_TypeDef *			GPIO;

	/*
	 * Change pull-up/pull-down for SCK depending in slave
	 */
	if (pInit->pTools == &spim__tools2pbp13) {

		GPIO = (GPIO_TypeDef *)GPIOB;

		if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
			LL_GPIO_SetPinPull(GPIO, 1<<13, LL_GPIO_PULL_UP);
		} else {
			LL_GPIO_SetPinPull(GPIO, 1<<13, LL_GPIO_PULL_DOWN);
		}
	}
}

/* Include the generic SSP driver */
#include "SPIM_SSP.c"
