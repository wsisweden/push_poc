/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPIM\LOCAL.H
*
*	\ingroup	SPIM
*
*	\brief		Local declarations for the SPI bus master
*
*	\details
*
*	\note
*
*	\version	25-07-2008 / Ari Suomi
*
*******************************************************************************/

#ifndef SPIM_LOCAL_H_INCLUDED
#define SPIM_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "lib.h"
#include "spim.h"

#include "init.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	\name	Task signaling flags
 *
 *	One of the following flags has to be set before signaling the task.
 */

/*@{*/
#define SPIM__FLAG_DONE		(1<<0)		/**< Request has been completed.	*/
#define SPIM__FLAG_NEWREQ	(1<<1)		/**< Start handling request.		*/
#define SPIM__FLAG_UP		(1<<2)		/**< Call the up-function.			*/
#define SPIM__FLAG_ERROR	(1<<3)		/**< Error while processing, abort! */
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type for storing task triggering reason flags.
 */

typedef Uint8				spim__Triggers;

/**
 * SPIM instance type. Used by all SPIM instances.
 */

struct spim__inst {						/*''''''''''''''''''''''''''''' RAM */
	volatile Boolean		reserved;	/**< The bus has been reserved.		*/		
	volatile spim__Triggers	flags;		/**< Task triggering reason flags.	*/
	spim_Init const_P * 	pInit;		/**< Initialization data            */
	protif_Request * 		pCurrReq;	/**< Ptr to the active request		*/
	protif_Request * 		pReserved;	/**< Ptr to the next reserved req.	*/
	lib_FifoType			reqFifo;	/**< FIFO containing requests       */
	osa_CoTaskType			coTask;		/**< Module task                    */
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void 	spim__hwInitCSPins(spim_SlaveInfo const_P *,Uint8);
void	spim__hwChipsel(spim__Inst *,Boolean,spim_SlaveInfo const_P *);
void	spim__handleReq(spim__Inst *,Boolean fromCoTask);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
