/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/SPIM_2.H
*
*	\ingroup	SPIM_CM3
*
*	\brief		SPIM register and variable defines
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_2_H_INCLUDED
#define SPIM_2_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/* variables */
#define SPIM__VAR_PINST		spim__pInst2

/* registers */
#define SPIM__REG_SSPNCR0	SSP1CR0		/**< SSPn Control Register 0        */
#define SPIM__REG_SSPNCR1	SSP1CR1		/**< SSPn Control Register 1        */
#define SPIM__REG_SSPNIMSC 	SSP1IMSC	/**< SSPn Int. Mask Set/Clear		*/
#define SPIM__REG_SSPNDR	SSP1DR		/**< SSPn Data register             */
#define SPIM__REG_SSPNSR	SSP1SR		/**< Status register                */
#define SPIM__REG_SSPNICR	SSP1ICR		/**< SSPn Interrupt Clear			*/
#define SPIM__REG_SSPNMIS	SSP1MIS		/**< SSPn Masked Int. Status		*/
#define SPIM__REG_SSPNCPSR	SSP1CPSR	/**< SSPn Clock prescaler			*/

#define SPIM__PERIPH		LPC_SSP1	/**< Peripheral pointer.			*/
#define SPIM__FN_ISR		SSP1_IRQHandler	/**< ISR name.					*/
#define SPIM__IRQN			SSP1_IRQn	/**< IRQ number.					*/

/* DMA channel connections */
#define SPIM__DMA_RX		NXP_DMA_SSP1_RX
#define SPIM__DMA_TX		NXP_DMA_SSP1_TX

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
