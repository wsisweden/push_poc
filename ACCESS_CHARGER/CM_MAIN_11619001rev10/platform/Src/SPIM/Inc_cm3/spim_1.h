/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/SPIM_1.H
*
*	\ingroup	SPIM_CM3
*
*	\brief		SPIM register and variable defines
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_1_H_INCLUDED
#define SPIM_1_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/* variables */
#define SPIM__VAR_PINST		spim__pInst1

/* registers */
#define SPIM__REG_SSPNCR0	SSP0CR0		/**< SSPn Control Register 0        */
#define SPIM__REG_SSPNCR1	SSP0CR1		/**< SSPn Control Register 1        */
#define SPIM__REG_SSPNIMSC 	SSP0IMSC	/**< SSPn Int. Mask Set/Clear Register*/
#define SPIM__REG_SSPNDR	SSP0DR		/**< SSPn Data register             */
#define SPIM__REG_SSPNSR	SSP0SR		/**< Status register                */
#define SPIM__REG_SSPNICR	SSP0ICR		/**< SSPn Interrupt Clear Register  */
#define SPIM__REG_SSPNMIS	SSP0MIS		/**< SSPn Masked Int. Status register*/
#define SPIM__REG_SSPNCPSR	SSP0CPSR	/**< SSPn Clock Prescale Register   */

#define SPIM__PERIPH		LPC_SSP0	/**< Peripheral pointer.			*/
#define SPIM__FN_ISR		SSP0_IRQHandler	/**< ISR name.					*/
#define SPIM__IRQN			SSP0_IRQn	/**< IRQ number.					*/

/* DMA channel connections */
#define SPIM__DMA_RX		NXP_DMA_SSP0_RX
#define SPIM__DMA_TX		NXP_DMA_SSP0_TX

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
