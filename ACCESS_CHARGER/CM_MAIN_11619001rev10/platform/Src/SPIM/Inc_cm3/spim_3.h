/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/SPIM_1.H
*
*	\ingroup	SPIM_CM3
*
*	\brief		SPIM register and variable defines
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_3_H_INCLUDED
#define SPIM_3_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "spim_com.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

#ifndef SPIM__PINSEL_FUNC

#define SPIM__PCONP_SPI		20			/**< Power Control bit              */

#define SPIM__VAR_PINST		spim__pInst3

#endif

/* registers */
#define SPIM__REG_SSPNCR0	LPC_SSP2->CR0	/**< SSPn Control Register 0        */
#define SPIM__REG_SSPNCR1	LPC_SSP2->CR1	/**< SSPn Control Register 1        */
#define SPIM__REG_SSPNIMSC 	LPC_SSP2->IMSC	/**< SSPn Int. Mask Set/Clear Register*/
#define SPIM__REG_SSPNDR	LPC_SSP2->DR	/**< SSPn Data register             */
#define SPIM__REG_SSPNSR	LPC_SSP2->SR	/**< Status register                */
#define SPIM__REG_SSPNICR	LPC_SSP2->ICR	/**< SSPn Interrupt Clear Register  */
#define SPIM__REG_SSPNMIS	LPC_SSP2->MIS	/**< SSPn Masked Int. Status register*/
#define SPIM__REG_SSPNCPSR	LPC_SSP2->CPSR	/**< SSPn Clock Prescale Register   */

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
