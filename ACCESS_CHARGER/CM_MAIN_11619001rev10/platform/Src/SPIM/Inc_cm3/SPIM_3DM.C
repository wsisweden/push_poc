/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* Can only be compiled with controller with SSP2. */
#ifdef TARGETFAMILY_LPC178x_7x 
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_CM3
*
*	\brief		SSP2 driver using DMA.
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$		
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "hw.h"

#include "../local.h"
#include "spim_dma.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define SPIM__VAR_PINST		spim__pInst3

/* registers */
#define SPIM__REG_SSPNCR0	LPC_SSP2->CR0	/**< SSPn Control Register 0	*/
#define SPIM__REG_SSPNCR1	LPC_SSP2->CR1	/**< SSPn Control Register 1	*/
#define SPIM__REG_SSPNIMSC 	LPC_SSP2->IMSC	/**< SSPn Int. Mask Set/Clear	*/
#define SPIM__REG_SSPNDR	LPC_SSP2->DR	/**< SSPn Data register			*/
#define SPIM__REG_SSPNSR	LPC_SSP2->SR	/**< Status register			*/
#define SPIM__REG_SSPNICR	LPC_SSP2->ICR	/**< SSPn Interrupt Clear		*/
#define SPIM__REG_SSPNMIS	LPC_SSP2->MIS	/**< SSPn Masked Int. Status	*/
#define SPIM__REG_SSPNCPSR	LPC_SSP2->CPSR	/**< SSPn Clock prescaler		*/

#define SPIM__DMA_SSP		LPC_SSP2		/**< Peripheral pointer.		*/
#define SPIM__FN_ISR		SSP2_IRQHandler	/**< ISR name.					*/
#define SPIM__IRQN			SSP2_IRQn		/**< IRQ number.				*/

/* DMA channel connections */
#define SPIM__DMA_RX		NXP_DMA_SSP2_RX
#define SPIM__DMA_TX		NXP_DMA_SSP2_TX

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void				spim__hwSetupPinsAndClock(spim_Init const_P *);

PRIVATE spim__Inst *		spim__lowInit(spim_Init const_P *);
PRIVATE void 				spim__lowProcess(spim__Inst *);
PRIVATE void 				spim__lowUp(spim__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for physical bus SSP2 with SCK on port 1 pin 0
 */

PUBLIC spim__Tools const_P	spim__dmaTools3p1p0 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwSetupPinsAndClock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure clock for SSP2 peripheral and setup pins.
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__hwSetupPinsAndClock(
	spim_Init const_P *			pInit
) {
	/*
	 *	Enable SSP2
	 */

	LPC_SC->PCONP |= 1 << 20;

	/*
	 *	spim__dmaTools3p1p0 is currently the only pin option for SSP2.
	 */

	deb_assert(pInit->pTools == &spim__dmaTools3p1p0);

	/*
	 *	Type D pins
	 *
	 *	The MISO-line should not need the pull-up, but it is left enabled
	 *	to ensure full compatibility with older projects.
	 */

	LPC_IOCON->P1_0 = 0x04; 				/* SSP2_SCK */
	LPC_IOCON->P1_4 = 0x04 | HW_PIN_PULLUP;	/* SSP2_MISO */
	LPC_IOCON->P1_1 = 0x04;					/* SSP2_MOSI */
}

/* Include generic SSP DMA driver code */
#include "SPIM_SDD.c"

#endif /* TARGETFAMILY_LPC178x_7x */
