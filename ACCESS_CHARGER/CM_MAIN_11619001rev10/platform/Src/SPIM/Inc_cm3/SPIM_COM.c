/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_CM3
*
*	\brief		Helper functions for the ARM PrimeCell Synchronous Serial Port
*				driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 3062 $ \n
*				\$Date: 2016-10-10 15:38:17 +0300 (ma, 10 loka 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	SPIM_CM3	Cortex-M3
*
*	\ingroup	SPIM
*
*	\brief
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "osa.h"
#include "hw.h"

#include "../local.h"
#include "spim_com.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

#if (TARGET_SELECTED & TARGET_CM4) == TARGET_CM4

/* The LPC11U6x headers uses a different struct name */
typedef LPC_SSP_T		LPC_SSP_TypeDef;

#endif

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize SSP bus to SPI master mode.
*
*	\param		pInst 	Pointer to SPIM instance.
*	\param		pSsp	Pointer to SSP peripheral base.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void spim__hwInit(
	spim__Inst *			pInst,
	volatile LPC_SSP_TypeDef * pSsp
) {
	Uint16					dummy;

	/*
	 *	Setting clock pre-scaler. Minimum value is 2. Only even values are
	 *	allowed. Bus clock frequency is PCLK / (CPSR * [SCR+1]).
	 */

	pSsp->CPSR = 2; 

	/*
	 * Set SSP mode to master.
	 */

	pSsp->CR1 = 0;

	/*
	 * Clear the RX fifo by reading data register eight times.
	 */
	
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;
	dummy = pSsp->DR;

	dummy = dummy; /* Just to prevent compiler warning */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwBusSettings
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set bus settings.
*
*	\param		pInst 	Pointer to SPIM instance.
*	\param		pSsp	Pointer to SSP peripheral base.
*	\param		pSlave	Pointer to slave info containing the bus settings.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void spim__hwBusSettings(
	spim__Inst *			pInst,
	LPC_SSP_TypeDef *		pSsp,
	spim_SlaveInfo const_P *pSlave
) {
	/*
	 * Set SSP mode to SPI.
	 */

	pSsp->CR0 = SPIM__FRF_SPI << SPIM__SSPNCR0_FRF;

	/*
	 * Set SSP frame length. Only 4...16 bits are supported on this bus.
	 */

	deb_assert(pSlave->frameBits >= 4);
	deb_assert(pSlave->frameBits <= 16);
	pSsp->CR0 |= pSlave->frameBits - 1;

	/*
	 * Set the clock settings (polarity and phase).
	 */

	if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
		pSsp->CR0 |= 1 << SPIM__SSPNCR0_CPOL;

		if (pSlave->flags & SPIM_PHASE_RISING) {
			pSsp->CR0 |= 1 << SPIM__SSPNCR0_CPHA;
		}
		
	} else {
		if ((pSlave->flags & SPIM_PHASE_RISING) == 0) {
			pSsp->CR0 |= 1 << SPIM__SSPNCR0_CPHA;
		}
	}

	/*
	 * Select the SPI bus clock. Bus clock frequency is PCLK / (CPSR * [SCR+1]).
	 */

	pSsp->CR0 |= pSlave->clkDiv << SPIM__SSPNCR0_SCR;
}
