/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_CM3
*
*	\brief		SSP1 driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 2778 $ \n
*				\$Date: 2015-05-07 10:12:28 +0300 (to, 07 touko 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "hw.h"

#include "../local.h"
#include "spim_2.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE spim__Inst * 		spim__lowInit(spim_Init const_P *);
PRIVATE void 				spim__lowProcess(spim__Inst *);
PRIVATE void 				spim__lowUp(spim__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for physical bus SSP1 without pin configuration
 */

PUBLIC spim__Tools const_P	spim__tools2NoConf = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/**
 * Tools for physical bus SSP1 with SCK on port 0 pin 7
 */

PUBLIC spim__Tools const_P	spim__tools2p0p7 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

#ifdef TARGETFAMILY_LPC178x_7x 

/**
 * Tools for physical bus SSP1 with SCK on port 1 pin 19
 */

PUBLIC spim__Tools const_P	spim__tools2p1p19 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/**
 * Tools for physical bus SSP1 with SCK on port 4 pin 20
 */

PUBLIC spim__Tools const_P	spim__tools2p4p20 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/**
 * Tools for physical bus SSP1 with SCK on port 1 pin 31
 */

PUBLIC spim__Tools const_P	spim__tools2p1p31 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwSetupPinsAndClock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure clock for SSP1 peripheral and setup pins.
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__hwSetupPinsAndClock(
	spim_Init const_P *			pInit
) {
	/* Enable SSP1 */
	LPC_SC->PCONP |= 1 << 10;

#ifdef TARGETFAMILY_LPC178x_7x 

	/*
	 *	The MISO-line should not need the pull-up, but it is left enabled
	 *	to ensure full compatibility with older projects.
	 */
	
	if (pInit->pTools == &spim__tools2NoConf) {
		/*
 		 * Pin configuration must be done by the user during startup.
		 */
		
	} else if (pInit->pTools == &spim__tools2p0p7) {
		/*
		 * 	Type W pins.
		 *
		 *	Digital mode is enabled and input filter disabled.
		 */

		LPC_IOCON->P0_7 =
			0x02
			| HW_PIN_DIGITAL
			| HW_PIN_FILT_DIS;				/* SSP1_SCK */
		LPC_IOCON->P0_8 =
			0x02
			| HW_PIN_PULLUP
			| HW_PIN_DIGITAL
			| HW_PIN_FILT_DIS;				/* SSP1_MISO */
		LPC_IOCON->P0_9 =
			0x02
			| HW_PIN_DIGITAL
			| HW_PIN_FILT_DIS;				/* SSP1_MOSI */

	} else if (pInit->pTools == &spim__tools2p1p19) {
		/*
		 * 	Type D pins.
		 */

		LPC_IOCON->P1_19 = 0x05;					/* SSP1_SCK */
		LPC_IOCON->P1_18 = 0x05 | HW_PIN_PULLUP;	/* SSP1_MISO */
		LPC_IOCON->P1_22 = 0x05;					/* SSP1_MOSI */

	} else if (pInit->pTools == &spim__tools2p4p20) {
		/*
		 * 	Type D pins.
		 */

		LPC_IOCON->P4_20 = 0x03;					/* SSP1_SCK */
		LPC_IOCON->P4_22 = 0x03 | HW_PIN_PULLUP;	/* SSP1_MISO */
		LPC_IOCON->P4_23 = 0x03;					/* SSP1_MOSI */

	} else {
		deb_assert(pInit->pTools == &spim__tools2p1p31);

		/*
		 * 	Type A pins.
		 *
		 *	Digital mode is enabled and input filter disabled.
		 */

		LPC_IOCON->P1_31 =
			0x02
			| HW_PIN_DIGITAL
			| HW_PIN_FILT_DIS;				/* SSP1_SCK */
		LPC_IOCON->P0_12 =
			0x02
			| HW_PIN_PULLUP
			| HW_PIN_DIGITAL
			| HW_PIN_FILT_DIS;				/* SSP1_MISO */
		LPC_IOCON->P0_13 =
			0x02
			| HW_PIN_DIGITAL
			| HW_PIN_FILT_DIS;				/* SSP1_MOSI */
	}

#else

	/*
	 * Select pin functions for SSP
	 */
	
	if (pInit->pTools == &spim__tools2NoConf) {
		/*
 		 * Pin and clock configuration must be done by the user during startup.
		 */
		
	} else if (pInit->pTools == &spim__tools2p0p7) {
		/*
		 * Set the SSP clock.
		 * 36MHz / 1 = 36Mhz
		 * 36MHz / 2 = 18MHz
		 */

		LPC_SC->PCLKSEL0 |= 1 << 20;
		
		LPC_PINCON->PINSEL0 |= 2U << 14;	/* P0.7 SCK1 */
		LPC_PINCON->PINSEL0 |= 2U << 16;	/* P0.8 MISO1 */
		LPC_PINCON->PINSEL0 |= 2U << 18;	/* P0.9 MOSI1 */
		
	} else {
		/* Unknown pin configuration */
		deb_assert(FALSE);
	}

#endif
}

/* Include generic SSP driver code */
#include "../Inc_cm3/SPIM_SSP.c"
