/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/SPIM_DMA.H
*
*	\ingroup	SPIM_CM3
*
*	\brief		DMA SPIM header.
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_DMA_H_INCLUDED
#define SPIM_DMA_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "hw.h"

#include "nxp.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_CM4
typedef LPC_SSP_T		LPC_SSP_TypeDef;
#endif

/**
 * SPIM DMA instance.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	spim__Inst				base;		/**< SPIM base						*/
	nxp_DMARequest			dma_tx_req;	/**< DMA TX request					*/
	nxp_DMARequest			dma_rx_req;	/**< DMA RX request					*/
	protif_Data *			pSpiReq;	/**< Current request				*/
	Uint32					data_addr;	/**< SPI data register address		*/
	nxp_DMAReserveInfo		dma_rx;		/**< DMA rx channel config			*/
	nxp_DMAReserveInfo		dma_tx;		/**< DMA tx channel config			*/
	volatile LPC_SSP_TypeDef * pSsp;	/**< SSP register ptr				*/
	Uint32					offset;
} spim__DMAInst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void		spim__dmaInit(spim__DMAInst *, spim_Init const_P *, Uint32, Uint32);
void 		spim__dmaStartTransfer(spim__DMAInst *, protif_Data *);
void 		spim__dmaChipsel(spim__Inst *, Boolean);

void 		spim__dmaRxCallback(void *, Uint8);
void 		spim__dmaTxCallback(void *, Uint8);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
