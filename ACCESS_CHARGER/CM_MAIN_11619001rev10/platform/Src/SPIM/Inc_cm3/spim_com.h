/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_CM3
*
*	\brief		Common declarations for all SPIM LPC17xx (Cortex-M3) files
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SPIM_COM_H_INCLUDED
#define SPIM_COM_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/* bits */
#define SPIM__SSPNCR0_DSS	0			/**< Data size select               */
#define SPIM__SSPNCR0_FRF	4			/**< Frame format                   */
#define SPIM__SSPNCR0_CPOL	6			/**< Clock Out Polarity             */
#define SPIM__SSPNCR0_CPHA	7			/**< Clock Out Phase                */
#define SPIM__SSPNCR0_SCR 	8			/**< Serial Clock Rate              */

#define SPIM__SSPNCR1_MS	2			/**< Master/Slave mode (0=master)   */
#define SPIM__SSPNCR1_SSE	1			/**< SSP Enable (1=enabled)         */

#define SPIM__SSPNIMSC_RORIM	(1<<0)	/**< Receive overrun                */
#define SPIM__SSPNIMSC_RTIM	(1<<1)		/**< Receive timeout                */
#define SPIM__SSPNIMSC_RXIM	(1<<2)		/**< RX FIFO is at least half full  */
#define SPIM__SSPNIMSC_TXIM	(1<<3)		/**< TX FIFO is at least half empty */

#define SPIM__SSPNSR_TFE 	(1<<0)		/**< 1=Transmit FIFO Empty          */
#define SPIM__SSPNSR_TNF 	(1<<1)		/**< 1=Transmit FIFO Not Full.      */
#define SPIM__SSPNSR_RNE 	(1<<2)		/**< 1=Receive FIFO Not Empty.      */
#define SPIM__SSPNSR_RFF 	(1<<3)		/**< 1=Receive FIFO Full.           */
#define SPIM__SSPNSR_BSY 	(1<<4)		/**< 1=Busy                         */

#define SPIM__SSPNMIS_RO 	(1<<0)		/**< Receive overrun                */
#define SPIM__SSPNMIS_RT 	(1<<1)		/**< Receive timeout                */
#define SPIM__SSPNMIS_RX	(1<<2)		/**< RX FIFO is at least half full  */
#define SPIM__SSPNMIS_TX	(1<<3)		/**< TX FIFO is at least half empty */

/* values */
#define SPIM__FRF_SPI		0			/**< SPI mode                       */
#define SPIM__MS_MASTER		0			/**< Master mode                    */

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/**
 *	Enable TX interrupt.
 */

#define spim__hwEnableTx(pPherip_)	(pPherip_)->IMSC |= SPIM__SSPNIMSC_TXIM

/**
 *	Disable TX interrupt.
 */

#define spim__hwDisableTx(pPherip_)	(pPherip_)->IMSC &= ~SPIM__SSPNIMSC_TXIM

/**
 *	Enable RX interrupt.
 */

#define spim__hwEnableRx(pPherip_)	(pPherip_)->IMSC |= SPIM__SSPNIMSC_RORIM |\
	SPIM__SSPNIMSC_RTIM | SPIM__SSPNIMSC_RXIM;

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

#if TARGET_SELECTED & (TARGET_CM4|TARGET_CM0)
/* The LPCopen headers uses a different struct name */
typedef LPC_SSP_T		LPC_SSP_TypeDef;
#endif

/**
 *	The instance type used by SPIM instances on lpc17xx MCUs without DMA.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	spim__Inst				parent;		/**< The common instance part		*/
	protif_Data * 			pCurrData;	/**< Ptr to data element being handled*/
	Uint16 					rxIndex;	/**< Data buffer index for RX		*/
	Uint16					txIndex;	/**< Data buffer index for TX		*/
	Uint16 					rxOverFlCnt;/**< RX FIFO overflow counter		*/
} spim__HwInst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

extern void	spim__hwInit(spim__Inst *,volatile LPC_SSP_TypeDef *);
extern void	spim__hwBusSettings(spim__Inst *,LPC_SSP_TypeDef *,spim_SlaveInfo const_P *);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
