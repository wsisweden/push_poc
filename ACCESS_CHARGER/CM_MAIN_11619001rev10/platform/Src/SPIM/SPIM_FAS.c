/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPIM_FAS.c
*
*	\ingroup	SPIM
*
*	\brief		SPI master direct access function.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 3631 $ \n
*				\$Date: 2019-01-28 09:34:51 +0200 (ma, 28 tammi 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "lib.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void spim__fastAccess(void *,protif_Request *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * This is the fast public interface object. This interface starts handling
 * new requests without starting the CoTask. This improves the response time
 * slightly compared to the spim_interface. This interface will however use
 * a bit more stack in the access function.
 */

PUBLIC protif_Interface	const_P	spim_fastIf = {
	&spim__fastAccess
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__fastAccess
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle supplied request.
*
*	\param		pVoidInst 	Pointer to the FB instance
*	\param		pRequest 	Pointer to the request
*
*	\details	Starts handling of the supplied request. The request is queued
*				if SPIM is busy handling a previous request.
*
*	\note		This function is called by another FB trough the spim_interface.
*
*******************************************************************************/

PRIVATE void spim__fastAccess(
	void *					pVoidInst,
	protif_Request *		pRequest
) {
	spim__Inst * pInst;

	pInst = (spim__Inst *) pVoidInst;

	deb_assert(pInst != NULL);
	deb_assert(pRequest != NULL);

	/*
	 *	SPIM could be made to work without OSA running. It would then work
	 *	by bypassing the FOFO. This access function could then call the low
	 *	processing function directly. The callback would then have to be called
	 *	from the ISR which could cause problems.
	 *
	 *	Currently SPIM does not support running without OSA.
	 */

	deb_assert(osa_isRunning);

	DISABLE;
	if (pInst->reserved) {
		/*
		 *	The bus has been reserved by some FB. The requests from that FB will
		 *	be executed directly bypassing the FIFO entirely.
		 *	Requests from other FBs are put into the FIFO and will be handled 
		 *	after the bus has been released.
		 */

		if (pRequest->data.flags & PROTIF_FLAG_RES) {
			/*
			 *	This request is from the FB that has reserved the bus.
			 */

			deb_assert(pInst->pCurrReq == NULL);

			pInst->pCurrReq = pRequest;
			ENABLE;
			spim__handleReq(pInst, FALSE);

		} else {
			/*
			 *	This request is from an FB that has not reserved the bus. 
			 */
			lib_fifoPutNonProt(&pInst->reqFifo, (void **) &pRequest->pNext);
			ENABLE;
		}

	} else {
		if (pInst->pCurrReq == NULL) {
			/*
			 *	SPIM is idle at the moment. Start handling the new request 
			 *	immediately.
			 */

			pInst->pCurrReq = pRequest;
			ENABLE;
			spim__handleReq(pInst, FALSE);

		} else {
			/*
			 *	SPIM is currently handling another request. Put the received
			 *	request on the FIFO.
			 */
			lib_fifoPutNonProt(&pInst->reqFifo, (void **) &pRequest->pNext);
			ENABLE;
		}
	}
}
