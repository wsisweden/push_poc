/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GAI
*
*	\brief		Generic GAI request FIFO handling.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 2779 $ \n
*				\$Date: 2015-05-07 11:33:05 +0300 (to, 07 touko 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "mem.h"
#include "gai.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/**
 * \brief	Start handling the supplied request.
 *
 * \param	pInst_	Pointer to instance data.
 * \param	pReq_	Pointer to request that should be handled.
 */

#define gai__fifoHandleReq(pInst_, pReq_) do {								\
	(pInst_)->pCurrDoneFn = (pReq_)->pDoneFn;								\
	(pInst_)->pCurrUtil = (pReq_)->pUtil;									\
	(pReq_)->pUtil = (pInst_);												\
	(pReq_)->pDoneFn = &gai__fifoDone;										\
	(pInst_)->pIf->pAccessFn((pInst_)->pImplInst, (pReq_));					\
} while (FALSE)

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type for GAI FIFO handler instance data.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	gai_Req *				pHead;		/**< Pointer to FIFO head.			*/
	gai_Req *				pTail;		/**< Pointer to FIFO tail.			*/
	gai_Req *				pCurrReq;	/**< Pointer to current request.	*/
	void *					pCurrUtil;	/**< Utility pointer of current req.*/
	gai_DoneFn *			pCurrDoneFn;/**< Pointer to current done func.	*/
	void *					pImplInst;	/**< Pointer to interface implementing 
										 * FB instance.						*/
	gai_Interface const_P *	pIf;		/**< Pointer to interface implementing
										 * FB interface constant.			*/
} gai__FifoInst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void gai__fifoDone(gai_Req * pReq,gai_RetCode retCode);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface constant for GAI request queuing. GAI FIFO handling can be
 * added to any function block that implements an interface based on GAI.
 *
 * This is useful if the interface implementing FB does not support request
 * queuing. This constant can be used instead of using the interface constant 
 * from the implementing FB. A pointer to the implementing FB interface constant
 * should then be provided to the GAI FIFO handler init function.
 */
 
PUBLIC gai_Interface const_P gai_fifoIf = {
	&gai_fifoAccess
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* File name for assert */
SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gai_fifoInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize GAI request FIFO.
*
*	\param		pImplIf		Pointer to GAI interface constant of the interface
*							implementing FB.
*	\param		pImplInst	Pointer to implementing FB instance.
*
*	\return		Pointer to the instance i.e. to its private data.
*
*	\details	The request fifo is not a mandatory feature of the GAI
*				interface. It can optionally be used to queue GAI requests. 
*				This function is only needed when the request fifo feature is
*				used.
*
*				The FIFO handler sends one request at a time to a FB that 
*				implements an interface based on GAI. Requests will be queued
*				if the access function is called while request handling is in
*				progress. The next request on the FIFO will be handled when the
*				current request has been handled.
*
*	\note		Should be called before OSA is started.
*
*******************************************************************************/

PUBLIC void * gai_fifoInit(
	gai_Interface const_P *	pImplIf,
	void * 					pImplInst
) {
	gai__FifoInst *			pInst;
	
	deb_assert(pImplIf);
	deb_assert(pImplInst);

	pInst = (gai__FifoInst *) mem_reserve(&mem_normal, sizeof(gai__FifoInst));
	deb_assert(pInst);

	pInst->pHead = NULL;
	pInst->pTail = NULL;

	pInst->pCurrReq = NULL;
	pInst->pCurrUtil = NULL;
	pInst->pCurrDoneFn = NULL;

	pInst->pIf = pImplIf;
	pInst->pImplInst = pImplInst;

	return pInst;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gai_fifoAccess
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Request GAI operation through the FIFO handler.
*
*	\param		pDrvInst	Pointer to GAI FIFO handler instance.
*	\param		pReq		Pointer to request that should be handled.
*
*	\details	
*
*	\return		The function always returns GAI_RET_OK. The request will be 
*				queued if the FIFO handler is busy handling a previously
*				received request.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC gai_RetCode gai_fifoAccess(
	void *					pDrvInst,
	gai_Req *				pReq
) {
	gai__FifoInst *			pInst;
	
	deb_assert(pDrvInst);
	deb_assert(pReq);

	pInst = (gai__FifoInst *) pDrvInst;

	DISABLE;
	if (pInst->pCurrReq == NULL) {
		/*
		 *	FIFO handler is idle. Start handling the recieved request
		 *	immediately.
		 */

		pInst->pCurrReq = pReq;
		ENABLE;
		
		gai__fifoHandleReq(pInst, pReq);
		
	} else {
		/*
		 *	Request handling in progress. Push the received request to the 
		 *	FIFO.
		 */

		pReq->pNext = NULL;

		if (pInst->pTail != NULL) {
			/* FIFO is not empty */
			pInst->pTail->pNext = pReq;
		} else {
			/* FIFO is empty */
			pInst->pHead = pReq;
		}

		pInst->pTail = pReq;
		ENABLE;
	}

	return(GAI_RET_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	gai__fifoDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reports to the state machine that the MDIO request has been 
*				handled.
*
*	\param		pReq		Pointer to the MDIO request.
*	\param		retCode		Result of the operation.
*
*	\details
*
*	\note		Should be called from a coTask or a task.
*
*	\sa
*
*******************************************************************************/

PRIVATE void gai__fifoDone(
	gai_Req *				pReq,
	gai_RetCode				retCode
) {
	gai__FifoInst *			pInst;
	gai_Req *				pNextReq;

	pInst = (gai__FifoInst *) pReq->pUtil;

	/*
	 *	Restore the original utility pointer and done function pointer.
	 */

	pReq->pUtil = pInst->pCurrUtil;
	pReq->pDoneFn = pInst->pCurrDoneFn;

	/*
	 *	Pop the next request from the FIFO.
	 */

	DISABLE;
	pNextReq = pInst->pHead;
	if (pNextReq != NULL) {
		pInst->pHead = pNextReq->pNext;
		if (pInst->pHead == NULL) {
			pInst->pTail = NULL;
		}
	}

	pInst->pCurrReq = pNextReq;
	ENABLE;

	/*
	 *	Call the callback function to report that the request has been handled.
	 *	The callback function may call the gai__fifoAccess function here.
	 */

	pReq->pDoneFn(pReq, retCode);

	/*
	 *	Start handling the next request.
	 */

	if (pNextReq != NULL) {
		gai__fifoHandleReq(pInst, pNextReq);
	}
}
