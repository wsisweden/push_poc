/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		FLWR\LOCAL.H
*
*	\ingroup	FLWR
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	The steps in writing data into EEPROM.
 */

enum {									/*''''''''''''''''''''''''''''''''''*/
	FLWR__STEP_IDLE = 0,				/**< No writing data under progress */
	FLWR__STEP_ENTER,					/**< Changed the status, writes begun*/
	FLWR__STEP_WRITING,					/**< Writing of data in progress    */
	FLWR__STEP_LEAVE,					/**< Changed the status, writes done!*/
	FLWR__STEP_ERROR					/**< Error occurred, stop at assert */
} ;										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


#define FLWR_MAX_READ_SIZE	256		/**< Max temporary space needed         */

#define FLWR__F_WAIT_RESET	(1<<0)	/**< Waiting for reset                  */
#define FLWR__F_DOWN_REQ	(1<<1)	/**< Going down                         */
#define FLWR__F_DOWN		(1<<2)	/**< Is down                            */
#define FLWR__F_STORE		(1<<3)	/**< Store to flash                     */

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef struct {
	nov_MemAddr				nAddress;
	nov_MemAddr				nBankStart;
	nov_MemAddr				nBankEnd;
	Uint8					nCycle;
	Uint8					nHeaders;
} flwr_Bank;

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	Uint8					numHeaders;	/**< Number of header entries in use*/
										/*-------------------- runtime init	*/
	Uint8					status;		/**< Current Bank status byte / index*/
	osa_SemaType			memSema;	/**<                                */
	nov_MemAddr				nBankSize;	/**< Bank size in bytes             */
	flwr_Bank				banks[2];	/**< Bank addresses                 */
	Uint8					nUsedBank;	/**<                                */
	BYTE *					pNov;		/**< Ptr to RAM image               */
	Uint32					nClockFreq;	/**< Main clock frequency           */
	Uint8					nFlags;		/**<                                */
} flwr__Inst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern Uint8				flwr__if(Uint8,nov_AllocInfo *);
extern Uint8				flwr__reserve(nov_AllocInfo *);
extern void					flwr__get(flwr__Inst *,nov_AllocInfo *);
extern void					flwr__put(flwr__Inst *,nov_AllocInfo *);
extern void					flwr__proceedWrite(flwr__Inst *);

extern void					flwr__novInit(void);
extern void					flwr__writeBytesFromRAM(nov_MemAddr, BYTE *, nov_SizeType);
extern void					flwr__writeBytesFromRAMEx(nov_MemAddr, BYTE *, nov_SizeType);
extern void					flwr__writeBytesFromFlash(nov_MemAddr, BYTE const_P *, nov_SizeType);
extern void					flwr__readBytesFromFlash(BYTE *, nov_MemAddr, nov_SizeType);
extern void					flwr__prepareWrite(nov_MemAddr, nov_SizeType);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern flwr__Inst		flwr__onlyInstance;

/******************************************************************************/
