/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		FLWR\INIT.H
*
*	\ingroup	FLWR
*
*	\brief		Flwr initialization declarations.
*
*	\note		
*
*	\version
*
*******************************************************************************/
 
#ifndef FLWR_INIT_H_INCLUDED
#define FLWR_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

/*
#define flwr_reset		dummy_reset
#define flwr_down		dummy_down
*/
#define flwr_read		dummy_read
#define flwr_write		dummy_write
#define flwr_ctrl		dummy_ctrl
#define flwr_test		dummy_test

/**
 * Init data type
 */
 
typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	Uint8					numWrites;	/**< Max number of simultaneous     */
} flwr_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_FLWR
#elif defined(EXE_GEN_FLWR)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_FLWR
#endif

#define EXE_APPL(n)			n##flwr

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

/* The registers */

/* References to external registers */

/* Write-indications */

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
