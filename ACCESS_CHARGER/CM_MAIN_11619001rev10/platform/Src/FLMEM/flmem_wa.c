/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_wa.c
*
*	\ingroup	FLMEM
*
*	\brief		Wait all function handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"

/* local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 *	This implies if FLMEM is powering down and releasing all waiting tasks.
 */
#define FLMEM__FLG_RELEASING_WAIT_ALL	(1<<5)

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8				blockedTaskCount;/** Tells how many task are blocked
										 * in waitAll-function				*/
	osa_SemaType		waitAllSema;	/** This semaphore is used to
										 * implement the waitAll 
										 * reg_put-implementation			*/
	Uint8				flags;			/**< Wait all mode state flags		*/
} flmem__WaitAllData;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PUBLIC void	flmem__waitAllInit(flmem__Inst *);
PUBLIC void	flmem__waitAllDone(flmem__Inst *,flmem__MemoryIndex *);
PUBLIC void	flmem__waitAllRelease(flmem__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE flmem__WaitAllData	flmem__waitAllData;

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/

/*
 * TODO:	The wait all mode is not working as it is. The code should be
 *			checked and fixed.
 */

PUBLIC void flmem__waitAllInit(
	flmem__Inst *			pInst
) {
	flmem__waitAllData.blockedTaskCount = 0;
	flmem__waitAllData.flags = 0;

	osa_newSemaTaken(&flmem__waitAllData.waitAllSema);
}

PUBLIC void flmem__waitAllDone(
	flmem__Inst *			pInst,
	flmem__MemoryIndex *	tagInfo
) {
	Boolean					setWaitAllSema;

	setWaitAllSema = FALSE;

	/*	
	 *	Check if "waitAll" tasks should be released
	 *	(see flmem_waitAllWritten) 
	 *
	 *	TODO:	Check this!
	 */

	DISABLE;

	if (
		flmem__waitAllData.blockedTaskCount > 0 &&
		!(flmem__waitAllData.flags & FLMEM__FLG_RELEASING_WAIT_ALL)
	) {
		flmem__waitAllData.flags |= FLMEM__FLG_RELEASING_WAIT_ALL;
		setWaitAllSema = TRUE;
	}

	ENABLE;

	/*
	 * Release "waitAll" tasks (see flmem_waitAllWritten)
	 */

	if (setWaitAllSema) {
		osa_semaSet(&flmem__waitAllData.waitAllSema);
	}
}

PUBLIC void flmem__waitAllRelease(
	flmem__Inst *			pInst
) {
	if (
		flmem__waitAllData.blockedTaskCount > 0 &&
		not(flmem__waitAllData.flags & FLMEM__FLG_RELEASING_WAIT_ALL)
	) {
		atomic(
			flmem__waitAllData.flags |= FLMEM__FLG_RELEASING_WAIT_ALL;
		);

		osa_semaSet(&flmem__waitAllData.waitAllSema);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem_waitAllWritten
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function sets calling task to be halted as long as
*				FLMEM FIFO has some elements.
*
*	\returns	If all OK returns 0, otherwise non-zero returncode.
*
*	\details	Function assumes that FIFO will get empty. If that's not the
*				case this call won't work properly and some tasks could stay
*				locked forever.
*
*	\note
*
*******************************************************************************/

PUBLIC Uint8 flmem_waitAllWritten(
	void
) {
	flmem__Inst *			pInst;
	Boolean					callNextSemaSet;

	pInst = &flmem__instance;
	callNextSemaSet = FALSE;

	/* This function is currently not working. It needs to be fixed before it
	 * can be used. */
	deb_assert(FALSE);

	if (pInst->err > 0) {
		return FLMEM_SERVICE_FATAL_ERROR;
	}

	if (flmem__fifoFront(pInst) == FLMEM__MEMORYINDEX_INDEX_FIFOEND) {
		/*
		 *	FIFO is empty. No reason to wait!
		 */
		return FLMEM_SERVICE_OK;
	}

	/* If "release wait all" -flag is up, don't go and mix things up */
	while (flmem__waitAllData.flags & FLMEM__FLG_RELEASING_WAIT_ALL) {
		osa_yield();
		if (pInst->err > 0) {
			return FLMEM_SERVICE_FATAL_ERROR;
		}
	}

	if (flmem__fifoFront(pInst) == FLMEM__MEMORYINDEX_INDEX_FIFOEND) {
		/*
		 *	FIFO is empty. No reason to wait!
		 */
		return FLMEM_SERVICE_OK;
	}

	atomic(
		/* Write-requests denied! */
		if (pInst->sFlags & FLMEM__SFLG_POWER_DOWN_DENY_WRITE ||
			pInst->sFlags & FLMEM__SFLG_POWER_DOWN_ALLOW_LAST) {
			return FLMEM_SERVICE_UNAVAILABLE;
		}
	);

	if (pInst->err > 0) {
		return FLMEM_SERVICE_FATAL_ERROR;
	}

	/*
	 * Next part takes care locking and releasing tasks. Tasks are released
	 * when FLMEM FIFO is gets empty.
	 */

	{
		atomic(
			flmem__waitAllData.blockedTaskCount += 1;
		);

		/* Set current task to wait FIFO to get empty. */
		osa_semaGet(&flmem__waitAllData.waitAllSema);

		/* --> current task realeased! */

		/* FIFO got empty, release all task that are waiting. */
		DISABLE;
		{
			flmem__waitAllData.blockedTaskCount -= 1;

			if (flmem__waitAllData.blockedTaskCount > 0 &&
				flmem__waitAllData.flags & FLMEM__FLG_RELEASING_WAIT_ALL
			) {
				callNextSemaSet = TRUE;

			} else {
				flmem__waitAllData.flags &= ~FLMEM__FLG_RELEASING_WAIT_ALL;
				ENABLE;
				return FLMEM_SERVICE_OK;
			}
		}
		ENABLE;

		/* --> release next task! */
		if (callNextSemaSet) {
			osa_semaSet(&flmem__waitAllData.waitAllSema);
		}
	}

	return FLMEM_SERVICE_OK;
}
