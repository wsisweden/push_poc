/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_me.c
*
*	\ingroup	FLMEM
*
*	\brief		Memory driver handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"

/* local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void flmem__memdrvDone(memdrv_Alloc *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__memdrvInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the memory driver handling module.
*
*	\param		pInst			Pointer to FB instance
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void flmem__memdrvInit(
	flmem__Inst *			pInst
) {
	/* some memdrv-variables (these will stay the same..) */
	pInst->memdrvAlloc.fnDone = flmem__memdrvDone;
	pInst->memdrvAlloc.pData = pInst->pMemdrvData;

	/* initialize memory driver waiting semaphore */
	osa_newSemaTaken(&pInst->memdrvSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__setMemdrvValues
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function that will set some default variables before calling
*				specific memdrv-function.
*
*	\param		pInst			Pointer to FB instance
*	\param		idx				Sector index.
*	\param		offset			Offset from start of sector.
*	\param		count			Byte count that will tell how many bytes are
*								read or written.
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void flmem__setMemdrvValues(
	flmem__Inst *			pInst,
	flmem__SectorIdx		idx,
	Uint32					offset,
	memdrv_SizeType			count
) {
	pInst->memdrvAlloc.address =
		pInst->pInit->sectorList[idx].sectorOffset *
			(*pInst->pInit->sectorSmallestSize) * 1024
				+ offset;

	pInst->memdrvAlloc.count = count;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__callMemdrvPut
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function will call memdriver-function put
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void flmem__callMemdrvPut(
	flmem__Inst * 			pInst
) {
	Uint8 stat;

	stat = flmem__memdrvAccess(
		pInst->pInit->fnDriver->put,
		&pInst->memdrvAlloc,
		pInst
	);

	if (stat == MEMDRV_ERROR) {
	
		flmem__errorInformLocal(
			pInst,
			FLMEM__ERR_WRITE_1,
			flmemWriteErr
		);
	
	} else if (stat == MEMDRV_ABORTED) {

		atomic(
			pInst->sFlags |= FLMEM__SFLG_MEMDRV_ABORT;
		);

		longjmp(pInst->jmpBuf, 1);
	} 
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__callMemdrvGet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function will call memdrv-function get
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void flmem__callMemdrvGet(
	flmem__Inst * 			pInst
) {
	Uint8 stat;

	stat = flmem__memdrvAccess(
		pInst->pInit->fnDriver->get,
		&pInst->memdrvAlloc,
		pInst
	);

	if (stat == MEMDRV_ERROR) {

		flmem__errorInformLocal(
			pInst,
			FLMEM__ERR_READ_1,
			flmemReadErr
		);

	} else if (stat == MEMDRV_ABORTED) {

		atomic(
			pInst->sFlags |= FLMEM__SFLG_MEMDRV_ABORT;
		);

		longjmp(pInst->jmpBuf, 1);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__memdrvDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function is executed when memdrv-call is done.
*
*	\param		pAlloc		Pointer to memdrv-allocation structure.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__memdrvDone(
	memdrv_Alloc *			pAlloc
) {
	flmem__Inst * pInst;

	pInst = tools_structPtr(flmem__Inst, memdrvAlloc, pAlloc);
	osa_semaSet(&pInst->memdrvSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__memdrvAwake
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send wakeup command to memory.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void flmem__memdrvAwake(
	flmem__Inst *			pInst
) {
	FLMEM__MSG("19");

	pInst->memdrvAlloc.address = 0;
	pInst->memdrvAlloc.op = MEMDRV_OP_WAKEUP;

	if (
		flmem__memdrvAccess(
			pInst->pInit->fnDriver->doOp,
			&pInst->memdrvAlloc,
			pInst
		) == MEMDRV_ERROR
	) {
		flmem__errorInform(
			pInst,
			FLMEM__ERR_DO_OP_1,
			flmemDoOpErr
		);
	}
}
#ifndef NDEBUG
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__memdrvCheckMem
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check that FLMEM is compatible with the configured memory.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void flmem__memdrvCheckMem(
	flmem__Inst *			pInst
) {
	memdrv_MemInfo			memInfo;
	Uint8					memStatus;

	/*
	 *	Get information about the memory.
	 */

	pInst->memdrvAlloc.address = 0;
	pInst->memdrvAlloc.op = MEMDRV_OP_GET_INFO;
	pInst->memdrvAlloc.pData = (BYTE *) &memInfo;
	memStatus = flmem__memdrvAccess(
		pInst->pInit->fnDriver->doOp,
		&pInst->memdrvAlloc,
		pInst
	);

	if (memStatus == MEMDRV_ERROR) {
		/* There is something wrong with the memory or the connection to the 
		 * memory. */
		flmem__errorInform(
			pInst,
			FLMEM__ERR_DRV_1,
			flmemDoOpErr
		);

	} else if (memStatus == MEMDRV_OK) {
		Ufast8 count;

		/* 
		 * The "Get info" operation succeeded. Check that the memory is 
		 * compatible.
		 */

		/* 
		 * FLMEM does not support block addressed memories. If the following
		 * assert fails then the configured memory uses block level addressing.
		 */
		deb_assert(not(memInfo.flags & MEMDRV_FLG_BLOCK));

		/*
		 *	FLMEM is made for memories that requires erase before write. If the
		 *	following assert fails then some other type of memory has been
		 *	configured.
		 */
		deb_assert(memInfo.flags & MEMDRV_FLG_ERASE);

		count = *pInst->pInit->sectorCount;

		/*
		 *	If the following assert fails then the memory has fewer sectors than
		 *	what has been configured for FLMEM.
		 */
		if (memInfo.sectorCount != 0) {
			deb_assert(count <= memInfo.sectorCount);
		}

		if (memInfo.sectorSize != 0) {
			flash_SectorList const_P * pSectorList;

			pSectorList = pInst->pInit->sectorList;
			do {
				Uint16 size;
				count--;

				size = FLMEM__EXP2(pSectorList[count].sectorSizeFactor);

				/* 
				 * The configured sector sizes must match with the sector size
				 * of the memory. If the following assert fails then the sector
				 * size has been configured wrong.
				 */
				deb_assert(size == memInfo.sectorSize);
			} while (count);
		}
	}

	pInst->memdrvAlloc.pData = pInst->pMemdrvData;
}
#endif
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__memdrvSleep
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Put memory in sleep mode.
*
*	\param		pInst		Pointer to FB instance.
*
*	\returns
*	\details
*	\note
*
*******************************************************************************/

PUBLIC void flmem__memdrvSleep(
	flmem__Inst *			pInst
) {
	FLMEM__MSG("20");

	pInst->memdrvAlloc.address = 0;
	pInst->memdrvAlloc.op = MEMDRV_OP_SLEEP;

	if (
		flmem__memdrvAccess(
			pInst->pInit->fnDriver->doOp,
			&pInst->memdrvAlloc,
			pInst
		) == MEMDRV_ERROR
	) {
		flmem__errorInform(
			pInst,
			FLMEM__ERR_DO_OP_2,
			flmemDoOpErr
		);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__memdrvAccess
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function will execute given function call with given
*				parameters. Function will wait until memdrv-call is done.
*
*	\param		memDrvCall	Interface function call that should be executed
*	\param		pAlloc		Pointer to memdrv-allocation structure.
*	\param		pInst		Pointer to FB instance.
*
*	\details
*
*	\return		0 if all OK, otherwise some memdrv returncode.
*
*	\note
*
*******************************************************************************/

PUBLIC Uint8 flmem__memdrvAccess(
	memdrv_fnAccess			memDrvCall,
	memdrv_Alloc *			pAlloc,
	flmem__Inst * 			pInst
) {
	Uint8					retVal;

	/*
	 *	Call memory driver and wait until the operation is completed.
	 */

	memDrvCall(*pInst->pInit->ppDrvInst, pAlloc);
	osa_semaGet( &pInst->memdrvSema);

	/*
	 *	The return code is stored in the count field.
	 */

	atomic(
		if (pInst->err == 0) {
			retVal = (Uint8)pAlloc->count;

		} else {
			retVal = MEMDRV_ERROR;
		}
	);

	/* return code (0==ALL OK) */
	return retVal;
}
