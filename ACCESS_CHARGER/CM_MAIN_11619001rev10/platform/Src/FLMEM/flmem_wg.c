/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_wg.c
*
*	\ingroup	FLMEM
*
*	\brief		Wait group mode handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"
#include "sys.h"
#include "reg.h"

/* local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void	flmem__waitGroupInit(flmem__Inst *);
PRIVATE void	flmem__waitGroupDone(flmem__Inst *,flmem__MemoryIndex *);
PRIVATE void	flmem__waitGroupRelease(flmem__Inst *);
PRIVATE void	flmem__waitGroupRegPut(Uint16,ptrs_RegData_cDp,Uint8);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

PUBLIC flmem_ModeIf const_P flmem_waitGroupMode = {
	&flmem__waitGroupInit,
	&flmem__waitGroupDone,
	&flmem__waitGroupRelease
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/

PRIVATE void flmem__waitGroupInit(
	flmem__Inst *			pInst
) {
	/* Set up reg-fb listener */
	reg_novPutPtr = &flmem__waitGroupRegPut;
}

PRIVATE void flmem__waitGroupDone(
	flmem__Inst *			pInst,
	flmem__MemoryIndex *	tagInfo
) {
	/* NOT IMPLEMENTED YET! */
}

PRIVATE void flmem__waitGroupRelease(
	flmem__Inst *			pInst
) {
	/* NOT IMPLEMENTED YET! */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__waitGroupRegPut
*
*--------------------------------------------------------------------------*//**
*
*	\brief		NOT IMPLEMENTED YET!
*
*	\param		targetIndex BYTE offset to the target (NOV) location of the
*							data.
*	\param		byteCount 	Number of BYTEs (0..) to save.
*
*	\details
*
*	\note
*
*	\returns	-
*
*	\see		flmem_regPutNoWait
*	\see		flmem_regPutWaitSingle
*	\see		flmem_regPutWaitGroup
*
*******************************************************************************/

PRIVATE void flmem__waitGroupRegPut(
	Uint16					targetIndex,
	ptrs_RegData_cDp		pData,
	Uint8					byteCount
) {
	/* NOT IMPLEMENTED YET! */

	/*
	*	Idea here is that we could use the same mechanism how FLMEM processes
	*	the requests. For specific group we could rise bit-flags at
	*	pInst->pMemoryIndexes-array (FLMEM supports it already). For group we
	*	could check after every write that if its bit-flags are down.
	*
	*	If some groups collides for some bit-flags we could add an additional
	*	waiting-routine, that would wait for specific bit-flags to be down
	*	before heading ahead.
	*/

	FLMEM__MSG("flmem_regPutWaitGroup isn't yet implemented!!");
	deb_assert(FALSE);

	/* NOT IMPLEMENTED YET! */
}
