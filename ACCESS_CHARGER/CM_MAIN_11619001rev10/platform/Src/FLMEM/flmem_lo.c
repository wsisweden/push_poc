/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_lo.c
*
*	\ingroup	FLMEM
*
*	\brief		Startup data load handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* c library */
#include <string.h>

/* T-Plat.E library */
#include "tools.h"
#include "sys.h"
#include "reg.h"
#include "flash.h"
#include "deb.h"
#include "util.h"

/* local */
#include "local.h"

/* project */
#include "global.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#ifndef PROJECT_FLMEM_SECT_SIZE
#define PROJECT_FLMEM_SECT_SIZE					FLMEM_SECT_SIZE_TO_8
#endif

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void	flmem__loadStatusEntries(flmem__Inst *,Uint16 *);
PRIVATE void	flmem__loadStatusEntry(flmem__Inst *,flmem__SectorIdx,Uint16 *);
PRIVATE Boolean flmem__flashHasData(flmem__Inst *);
PRIVATE Uint16	flmem__loadDataEntry(flmem__Inst *,memdrv_AddrType,flmem__SectorIdx,Uint16 *);
PRIVATE void	flmem__resumeInterruptedOperation(flmem__Inst *,Uint16 *);
PRIVATE void	flmem__checkInterruptedErase(flmem__Inst *pInst,Uint16 *);
PRIVATE void	flmem__writeFirstCounterValue(flmem__Inst *);
PRIVATE void	flmem__loadSectors(flmem__Inst *,Uint16 *);
PRIVATE void	flmem__sortStatusEntries(flmem__Inst *,Uint16 *);
PRIVATE int		flmem__compareSequenceAges(flmem__SectorIdx,flmem__SectorIdx,Uint16 *);
PRIVATE void	flmem__handleCorruptedSectors(flmem__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__initializeFlash
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializing will handle all the necessary steps before flmem
*				is ready to operate with REG module.
*
*	\param		pInst	Pointer to FB instance.
*
*	\details	Initializing includes loading current data from flash. Before
*				doing so it must verify that flash is intact and if not it
*				will recover from situations when sector-erasing might have
*				been interrupted. When flash is ready to operate, it will
*				inform REG module.
*
*	\returns	TRUE if all OK, otherwise FALSE.
*
*	\note
*
*******************************************************************************/

PUBLIC Boolean flmem__initializeFlash(
	flmem__Inst * 			pInst
) {
	Uint16 counters[PROJECT_FLMEM_SECT_SIZE];

	if (!setjmp(pInst->jmpBuf)) {
		pInst->currentSectorIdx = 0;

		flmem__loadStatusEntries(pInst, counters);

		if (flmem__flashHasData(pInst)) {
			flmem__checkInterruptedErase(pInst, counters);
			flmem__handleCorruptedSectors(pInst);
			flmem__sortStatusEntries(pInst, counters);
			flmem__loadSectors(pInst, counters);
			flmem__resumeInterruptedOperation(pInst, counters);
			flmem__eraseNextIfNeeded(pInst);

		} else { 
			/* No data in flash..*/
			/*
			 *	We need counter value to be 0 at the beginning of first sector
			 *	ever written. (sorting would otherwise sort first sector as
			 *	youngest, and it should be the oldest!)
			 */

			FLMEM__MSG("13");

			flmem__writeFirstCounterValue(pInst);
			flmem__handleCorruptedSectors(pInst);
#ifndef NDEBUG
			flmem__initDone = TRUE;
#endif
			flmem__eraseNextSector(pInst);
			flmem__moveToNextSector(pInst);
		}

		return TRUE;

	} else {
		return FALSE;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__loadStatusEntries
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function that will iterate sectors and load status-entries from
*				the beginning of sectors.
*
*	\param		pInst			Pointer to FB instance.
*	\param		pCounters		Pointer to counters-array.
*
*	\details	Function will create linked list of status entries. The list
*				is not sorted by this function.
*
*	\returns
*	\note
*
*******************************************************************************/

PRIVATE void flmem__loadStatusEntries(
	flmem__Inst * 			pInst,
	Uint16 *				pCounters
) {
	flash_SectorIndexSizeType sectorNum;
	flmem__SectorInfo *		pPrevSector;
	flmem__SectorInfo *		pSector;

	sectorNum = (*pInst->pInit->sectorCount);
	pPrevSector = NULL;
	pSector = &pInst->pSectors[sectorNum - 1];
	pCounters = &pCounters[sectorNum - 1];

	do {
		sectorNum--;

		/*
		 *	Check if next read operations should be canceled.
		 */

		flmem__checkForPowerDown(pInst);

		flmem__loadStatusEntry(pInst, sectorNum, pCounters);

		/*
		 *	Link nodes so that the next pointer of last node is null,
		 *	others will point next one.
		 */

		pSector->pNext = pPrevSector;
		pPrevSector = pSector;

		pCounters--;
		pSector--;
	} while(sectorNum != 0);

	pInst->pOldestSector = pInst->pSectors;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__loadStatusEntry
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Loads single sector status entry from flash.
*
*	\param		pInst			Pointer to FB instance.
*	\param		sectorNum		Sector index that is loaded to RAM.
*	\param		pCounter		Pointer to location where seCounter-value of
*								status entry is added.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__loadStatusEntry(
	flmem__Inst * 			pInst,
	flmem__SectorIdx		sectorNum,
	Uint16 *				pCounter
) {
	Uint8				seStatus1;
	Uint8				seStatus2;
	flmem__SectorIdx	seNextIdx;
	Uint16				seSeqNum;
	flmem__SectStatus	sectStatus;

	/*
	 *	Read the status entry from flash memory.
	 *	(the status entry is always at the beginning of the sector)
	 */
	{
		BYTE * pMemdrvData;

		pMemdrvData = pInst->pMemdrvData;

		flmem__setMemdrvValues(
			pInst,
			sectorNum,
			0,
			FLMEM__SENTRY2_BLOCKSIZE
		);
		flmem__callMemdrvGet(pInst);

		/*
		 *	Parse status entry from byte-values
		 */

		seSeqNum = (Uint16) 
			(pMemdrvData[FLMEM__SENTRY_COUNTER_H] << 8) 
			| pMemdrvData[FLMEM__SENTRY_COUNTER_L];

		seNextIdx = (Uint8) pMemdrvData[FLMEM__SENTRY_NEXTIDX];
		seStatus1 = pMemdrvData[FLMEM__SENTRY2_STATUS1];
		seStatus2 = pMemdrvData[FLMEM__SENTRY2_STATUS2];
	}

	/*
	 *	Validate the status entry. Also detect if it is in format 1 or format 2.
	 */

	if (seStatus1 == 0xFFU && seStatus2 == 0xFFU) {
		/*
		 *	Sector is empty or the next sector has not been selected yet.
		 */
		sectStatus = FLMEM__SE_UNUSED;

	} else if (
		seStatus1 == FLMEM__SE1_NEXTIDX
		|| seStatus1 == FLMEM__SE1_COPYING
		|| seStatus1 == FLMEM__SE1_ERASING
		|| seStatus1 == FLMEM__SE1_READY
	) {
		/*
		 * Format 1 status code found.
		 */
		if (
			(seStatus1 != FLMEM__SE1_NEXTIDX)
			&& (seNextIdx >= *pInst->pInit->sectorCount)
		) {
			/*
			 *	The status entry of the sector had an invalid next sector index.
			 */
			sectStatus = FLMEM__SE_INVALID;

		} else {
			/*
			 *	Valid format 1 status entry.
			 */

			switch (seStatus1) {
				case FLMEM__SE1_NEXTIDX:
					sectStatus = FLMEM__SE_NEXTIDX;
					break;

				case FLMEM__SE1_COPYING:
					sectStatus = FLMEM__SE_COPYING;
					break;

				case FLMEM__SE1_ERASING:
					sectStatus = FLMEM__SE_ERASING;
					break;

				case FLMEM__SE1_READY:
					sectStatus = FLMEM__SE_READY;
					break;
					
				default:
					sectStatus = FLMEM__SE_INVALID;
					break;
			}

			pInst->tFlags |= FLMEM__TFLG_FORMAT1;
		}

	} else if (
		(seStatus1 == FLMEM__SE2_NEXTIDX)
		|| (seStatus1 == (FLMEM__SE2_NEXTIDX & FLMEM__SE2_COPYING))
	) {
		/*
		 *	Format 2 status code found.
		 */

		if (seStatus1 == FLMEM__SE2_NEXTIDX) {
			sectStatus = FLMEM__SE_NEXTIDX;

		} else {
			if (seNextIdx >= *pInst->pInit->sectorCount) {
				/*
				 *	The status entry of the sector had an invalid next sector
				 *	index.
				 */
				sectStatus = FLMEM__SE_INVALID;

			} else {
				if (seStatus2 == 0xFFU) {
					sectStatus = FLMEM__SE_COPYING;

				} else if (seStatus2 == FLMEM__SE2_ERASING) {
					sectStatus = FLMEM__SE_ERASING;

				} else if (seStatus2 == (FLMEM__SE2_ERASING & FLMEM__SE2_READY)) {
					sectStatus = FLMEM__SE_READY;

				} else {
					sectStatus = FLMEM__SE_INVALID;
				}
			}
		}

	} else if (seStatus1 == 0xFFU) {
		/* 
		 *	Could be empty sector. Or the current sector but the next sector has 
		 *	not been chosen yet.
		 */

		sectStatus = FLMEM__SE_UNUSED;

	} else {
		/* 
		 * The status entry of the sector did not contain a valid
		 * status code.
		 */

		sectStatus = FLMEM__SE_INVALID;
	}

	if (sectStatus == FLMEM__SE_INVALID) {
		/* 
		 *	Continue regardless of the faulty sectors. They will be erased
		 *	before they are used.
		 * 
		 *	If sector erasing was interrupted then that sector may be
		 *	detected as faulty here. In that case no data has been lost.
		 */
		seNextIdx = FLMEM__SECTORINDEX_UNUSED;
		seSeqNum = FLMEM__STATUSENTRY_COUNT_UNUSED;
	}

	{
		flmem__SectorInfo *	pSector;

		pSector = &pInst->pSectors[sectorNum];

		pSector->seStatus = sectStatus;
		pSector->seNextIdx = seNextIdx;
		*pCounter = seSeqNum;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__flashHasData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function checks if flash has any Flmem-data.
*
*	\param		pInst			Pointer to FB instance.
*
*	\return		TRUE if data exists, otherwise FALSE.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE Boolean flmem__flashHasData(
	flmem__Inst * 			pInst
) {
	flmem__SectorInfo *		pSector;
	flmem__SectorIdx		ii;

	pSector = pInst->pSectors;

	ii = *pInst->pInit->sectorCount;
	do {
		if (
			pSector->seStatus != FLMEM__SE_UNUSED
			&& pSector->seStatus != FLMEM__SE_INVALID
		) {
			/* A valid non-empty sector was found. */
			return TRUE;
		}

		pSector++;
	} while (--ii);

	return FALSE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__loadSectors
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Loads data entries from all sectors and copies register
*				values from flash to the REG RAM image.
*
*	\param		pInst			Pointer to FB instance
*	\param		pCounters		Pointer to counters array.
*
*	\details	The sectors are read from the oldest to the newest. That way 
*				the latest stored data will be in the RAM image after this 
*				function has returned.
*
*	\return
*	\note
*
*******************************************************************************/

PRIVATE void flmem__loadSectors(
	flmem__Inst * 			pInst,
	Uint16 *				pCounters
) {
	flmem__SectorInfo *		pCurrent;
	flmem__SectorIdx		lastSectorToContainDataEntries;
	Uint32					lastSectorToContainDataEntriesUsage;
	//memdrv_AddrType			lastEntryAddress;
	Uint16					faultCount;
	flmem__SectorIdx		sectorIdx;

	FLMEM__MSG("12");

	lastSectorToContainDataEntries = FLMEM__SECTORINDEX_UNUSED;
	lastSectorToContainDataEntriesUsage = 0;
	pCurrent = pInst->pOldestSector;
	faultCount = 0;
	//lastEntryAddress = 0;

	do {
		sectorIdx = (flmem__SectorIdx) (pCurrent - pInst->pSectors);

		deb_assert(sectorIdx < *pInst->pInit->sectorCount);

		/* 
		 *	Don't read unused status entries. In that case just run to next one.
		 */

		if (pCounters[sectorIdx] != FLMEM__STATUSENTRY_COUNT_UNUSED) {
			Uint16			dataEntryOffset;
			Uint16			currentSectorSize;
			memdrv_AddrType sectorAddress;

			/* calculate sector size */
			currentSectorSize = (Uint16) FLMEM__EXP2(
				pInst->pInit->sectorList[sectorIdx].sectorSizeFactor
			);

			sectorAddress =
				pInst->pInit->sectorList[sectorIdx].sectorOffset *
				(*pInst->pInit->sectorSmallestSize) * 1024;

			/* First data entry will be after the status entry */
			if (pInst->tFlags & FLMEM__TFLG_FORMAT1) {
				dataEntryOffset = FLMEM__SENTRY1_BLOCKSIZE;
			} else {
				dataEntryOffset = FLMEM__SENTRY2_BLOCKSIZE;
			}

#if FLMEM__ENABLE_LOGGING == 1
			{
				char logBuff[10];

				logBuff[0] = '6';
				logBuff[1] = '6';
				logBuff[2] = ' ';

				util_u8toa(
					sectorIdx,
					&logBuff[3],
					7,
					10
				);

				FLMEM__MSG(logBuff);
			}
#endif
			/*
			 *	Read all data entries from the sector.
			 */

			FOREVER {
				Uint16 dataEntrySize;

				dataEntrySize = flmem__loadDataEntry(
					pInst,
					sectorAddress + dataEntryOffset,
					sectorIdx,
					&faultCount
				);

				if (dataEntrySize == 0) {
					/*
					 *	No data entry found. There are no more data entries in
					 *	this sector.
					 */

					break;
				}
		
				/*
				 *	Update the sector usage counters.
				 */

				//lastEntryAddress = sectorAddress + dataEntryOffset;
				dataEntryOffset += dataEntrySize;
				lastSectorToContainDataEntries = sectorIdx;
				lastSectorToContainDataEntriesUsage = dataEntryOffset;

				if (
					dataEntryOffset + FLMEM__DATAENTRY_BLOCKSIZE >=
					currentSectorSize
				) {
					/* 
					 *	There is no room for any more data entries in this 
					 *	sector.
					 */
					FLMEM__MSG("67");
					break;
				}
			}
		}

		pCurrent = pCurrent->pNext;
	} while (pCurrent != NULL);

	if (lastSectorToContainDataEntries != FLMEM__SECTORINDEX_UNUSED) {
		pInst->currentSectorIdx = lastSectorToContainDataEntries;
		pInst->currentSectorUsage = (Uint16) lastSectorToContainDataEntriesUsage;

		/*
		 *	If the last write was interrupted then the bits might be in a
		 *	unstable state. Rewrite the status byte of the last data entry to
		 *	make sure it is stable.
		 *
		 *	TODO:	Will this work with Adesto data flash memories?
		 */

		//pInst->memdrvAlloc.address = lastEntryAddress + FLMEM__DENTRY_STATUS;
		//pInst->memdrvAlloc.count = 1;
		//flmem__callMemdrvGet(pInst);
		//pInst->memdrvAlloc.count = 1;
		//flmem__callMemdrvPut(pInst);

	} else {
		/*
		 *	No data entries found in any sector. This usually happens only if
		 *	the sectors have been initialized but no data entry has been
		 *	written. The newest sector should then be used.
		 */

		pInst->currentSectorIdx = sectorIdx;

		if (pInst->tFlags & FLMEM__TFLG_FORMAT1) {
			pInst->currentSectorUsage = FLMEM__SENTRY1_BLOCKSIZE;
		} else {
			pInst->currentSectorUsage = FLMEM__SENTRY2_BLOCKSIZE;
		}
	}

	pInst->currentCounter = pCounters[pInst->currentSectorIdx];

	/*
	 *	Check if next sector is ready.
	 */

	if (pInst->pSectors[pInst->currentSectorIdx].seStatus == FLMEM__SE_READY) {
		pInst->tFlags |= FLMEM__TFLG_NEXT_READY;
	}

	/*
	 *	Increment the recovery counter if needed.
	 */

#ifndef NDEBUG
	flmem__initDone = TRUE;
#endif

	if (pInst->pInit->pModeIf->pLoadedFn != 0) {
		pInst->pInit->pModeIf->pLoadedFn(pInst, faultCount);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__loadDataEntry
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function will read data entry from current sector.
*
*	\param		entryAddress	Flash memory address of the data entry.
*	\param		sectorIdx		Current sector index.
*	\param		pFaultCount		Pointer to fault counter.
*
*	\return		The size of the loaded entry.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE Uint16 flmem__loadDataEntry(
	flmem__Inst * 			pInst,
	memdrv_AddrType			entryAddress,
	flmem__SectorIdx		sectorIdx,
	Uint16 *				pFaultCount
) {
	BYTE *					pMemdrvData;
	flmem__DataEntryBlock	loadedEntry;
	Uint8					tryCount;

	pMemdrvData = pInst->pMemdrvData;
	tryCount = FLMEM__DATAENTRY_LOAD_TRYCOUNT_MAX;
	loadedEntry.deSize = 0;

	do {
		Uint16 entrySize;

		flmem__checkForPowerDown(pInst);

		/*
		 *	Read the data entry from memory. Four bytes of the data portion is
		 *	read at this point. If the data is only four bytes or smaller then
		 *	there is no need to read it separately later.
		 */

		{
			Uint16	bytesLeft;
			Uint8	sizeFactor;

			sizeFactor = pInst->pInit->sectorList[sectorIdx].sectorSizeFactor;

			bytesLeft = (Uint16)
				((1 << sizeFactor)
				- (entryAddress & ~(0xFFFFFFFF << sizeFactor)));

			/*
			 *	If the program stops here then the entryAddress is faulty. It
			 *	should have been checked before calling this function.
			 */
			deb_assert(bytesLeft > FLMEM__DATAENTRY_BLOCKSIZE);

			/* Do not perform reads over the the sector boundary. */
			if (bytesLeft < FLMEM__DATAENTRY_BLOCKSIZE + 4) {
				pInst->memdrvAlloc.count = bytesLeft;
			} else {
				pInst->memdrvAlloc.count = FLMEM__DATAENTRY_BLOCKSIZE + 4;
			}
		}
		pInst->memdrvAlloc.address = entryAddress;

		entrySize = flmem__readEntry(pInst, &loadedEntry);

		if (loadedEntry.deStatus == FLMEM__DE_INVALID) {
			/*
			 *	The status byte does not contain any valid value. This is a 
			 *	serious problem. Just try to read the data again until a
			 *	valid value is read.
			 */

			if (tryCount == 1) {
				(*pFaultCount)++;

				/*
				 *	The status byte is still corrupted after all retry
				 *	attempts. There is no good way to recover at this point.
				 */

				if (pInst->pInit->projectRecoverOpt == FLMEM_RECOVER_ENABLE) {
					/*
					 *	There is no way to find the next record because the
					 *	size might be corrupted. The rest of the sector must
					 *	be skipped.
					 */

					FLMEM__MSG("61");
					return 0;

				} else {
					flmem__errorInformLocal(
						pInst,
						FLMEM__ERR_CORRUPTED_2,
						flmemCorruptedErr
					);
				}
			}

			continue;

		} else if (loadedEntry.deStatus == FLMEM__DE_UNUSED) {
			if (loadedEntry.deSize != 0xFF) {
				/*
				 *	Size might have been only partially written. It cannot be
				 *	trusted.
				 *
				 *	The entry is marked so that it will always be discarded.
				 */

				pInst->memdrvAlloc.address = entryAddress;
				pInst->memdrvAlloc.count = 1;
				pMemdrvData[0] = (BYTE) FLMEM__DATAENTRY_SIZE_UNUSED;
				flmem__callMemdrvPut(pInst);

				pInst->memdrvAlloc.address = entryAddress +FLMEM__DENTRY_STATUS;
				pInst->memdrvAlloc.count = 1;

				if (pInst->tFlags & FLMEM__TFLG_FORMAT1) {
					pMemdrvData[0] = (BYTE) FLMEM__DE1_SKIP;
				} else {
					pMemdrvData[0] = (BYTE) FLMEM__DE2_SKIP;
				}

				flmem__callMemdrvPut(pInst);

				FLMEM__MSG("51");
			}
			
			return entrySize;

		} else if (loadedEntry.deStatus != FLMEM__DE_DONE) {
			return entrySize;

		} else if (loadedEntry.deStatus == FLMEM__DE_DONE) {
			/*
			 *	This entry should be ok. Read the data from flash memory.
			 */

			if (loadedEntry.deSize > reg_maxFixedSize()) {
				/*
				 *	The stored register data is larger than any
				 *	current register. It is probably a register that has
				 *	been removed. The entry should be discarded.
				 */

				return entrySize;
			}

			if (loadedEntry.deSize > 4) {
				/*
				 *	The entire data has not been read into the buffer. Read
				 *	the whole data part of the entry.
				 */

				pInst->memdrvAlloc.address =
					entryAddress + FLMEM__DATAENTRY_BLOCKSIZE;
				pInst->memdrvAlloc.count = loadedEntry.deSize;
				flmem__callMemdrvGet(pInst);

			} else {
				/*
				 *	Step over the entry header.
				 */
				pMemdrvData += FLMEM__DATAENTRY_BLOCKSIZE;
			}

			/*
			 *	Data has been read. Check that the checksum matches.
			 */
			{
				Uint8 checksum;

				checksum = flmem__calculateChecksum(
					loadedEntry.deTag,
					loadedEntry.deSize,
					pMemdrvData
				);

				/*
				 *	If checksum does not match then the data entry is read
				 *	again from the flash memory until the checksum matches or
				 *	the  retry count limit is reached.
				 */
				if (checksum == loadedEntry.deChecksum) {
					/*
					 *	The checksum matches. continue parsing the loaded
					 *	data entry.
					 */
					break;
				}
			}
		}

	} while (--tryCount != 0);

	if (tryCount == 0) {
		FLMEM__MSG("49");
		/*
		 *	The retry limit was reached. The data is corrupted. The data of this
		 *	register is lost if this was the last data entry for the register.
		 */
		if (pInst->pInit->projectRecoverOpt == FLMEM_RECOVER_ENABLE) {
			(*pFaultCount)++;

			if (pInst->tFlags & FLMEM__TFLG_FORMAT1) {
				/*
				 *	Mark the data entry so that it will be skipped next time. It
				 *	will save time the next startup.
				 *
				 *	This is only done with format 1 flash contents because it
				 *	may not work on memories that require the nibble to be
				 *	completely erased before it is written (Adesto).
				 */

				pInst->memdrvAlloc.address = entryAddress +FLMEM__DENTRY_STATUS;
				pInst->memdrvAlloc.count = 1;
				pMemdrvData[0] = (BYTE) FLMEM__DE1_SKIP;
				flmem__callMemdrvPut(pInst);
			}

			return loadedEntry.deSize + FLMEM__DATAENTRY_BLOCKSIZE;

		} else {
			flmem__errorInformLocal(
				pInst,
				FLMEM__ERR_CORRUPTED_2,
				flmemCorruptedErr
			);
		}
	}

	/*
	 *	Data entry is ok.
	 */

	{
		flmem__TagIdx			tagIdx;
		flmem__MemoryIndex *	pTagInfo;

		/*
		 *	Update register value in REG RAM-image.
		 */

		tagIdx = flmem__regWrite(
			pInst,
			loadedEntry.deTag,
			pMemdrvData,
			loadedEntry.deSize
		);

		if (tagIdx == FLMEM__MEMORYINDEX_INDEX_UNUSED) {
			/*
			 *	The tag is not part of current configuration. The register
			 *	has probably been removed. The data is discarded.
			 */
			flmem__warningInform(
				pInst,
				FLMEM__ERR_TAGREMOVED,
				flmemFlashTagWarn,
				loadedEntry.deTag
			);

			return loadedEntry.deSize + FLMEM__DATAENTRY_BLOCKSIZE;
		}

		if (tryCount != FLMEM__DATAENTRY_LOAD_TRYCOUNT_MAX) {
			(*pFaultCount)++;

			/*
			 *	The data entry was read ok after some retries. The register 
			 *	should be moved to the current sector. Putting the register to
			 *	the FIFO will do that.
			 */

			flmem__fifoPush(pInst, tagIdx);
		}

		pTagInfo = &pInst->pMemoryIndexes[tagIdx];

		if (pTagInfo->sectorIdx != FLMEM__SECTORINDEX_UNUSED) {
			/*
			 *	A data entry with this tag was previously read from another
			 *	sector. The previous entry is not an active entry.
			 */
			pInst->pSectors[pTagInfo->sectorIdx].activeBytes -=
				FLMEM__DATAENTRY_BLOCKSIZE + pTagInfo->flashSize;
		}
		pInst->pSectors[sectorIdx].activeBytes +=
			FLMEM__DATAENTRY_BLOCKSIZE + loadedEntry.deSize;

		pTagInfo->sectorIdx = sectorIdx;
		pTagInfo->flashSize = loadedEntry.deSize;
	}

	return loadedEntry.deSize + FLMEM__DATAENTRY_BLOCKSIZE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__resumeInterruptedOperation
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if any operation was interrupted when the device was 
*				shut down.
*
*	\param		pInst			Pointer to FB instance.
*	\param		pCounters		Pointer to counters-array.
*
*	\return		-
*
*	\details	Interrupted operations are resumed.
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__resumeInterruptedOperation(
	flmem__Inst * 			pInst,
	Uint16 *				pCounters
) {
	flmem__SectorInfo *		pSector;
	Uint8					nextPhase;

	nextPhase = 0xFF;
	pSector = pInst->pOldestSector;
	do {
		if (pSector->seStatus == FLMEM__SE_NEXTIDX) {
			/*
			 *	Next index writing was interrupted.
			 */

			nextPhase = FLMEM__SECTOR_ERASE_JUMP_TO_PHASE_1;
			pSector->seNextIdx = flmem__chooseEraseCandidate(pInst);
			break;
		}

		if (pSector->seStatus == FLMEM__SE_COPYING) {
			/*
			 *	Active data entry copying was interrupted.
			 */

			deb_assert(pSector->seNextIdx != FLMEM__SECTORINDEX_UNUSED);
			nextPhase = FLMEM__SECTOR_ERASE_JUMP_TO_PHASE_2;
			break;
		}

		pSector = pSector->pNext;
	} while (pSector != NULL);

	if (nextPhase != 0xFF) {
		FLMEM__MSG("65");
		pInst->currentCounter = pCounters[pSector - pInst->pSectors];
		pInst->currentSectorIdx = (flmem__SectorIdx)(pSector - pInst->pSectors);
		flmem__nextSectorEraseBase(pInst, pSector->seNextIdx, nextPhase);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__checkInterruptedErase
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check if sector erasing has been interrupted.
*
*	\param		pInst			Pointer to FB instance
*	\param		pCounters		Pointer to counters-array.
*
*	\return		-
*
*	\details	If erasing has been interrupted then it will be resumed.
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__checkInterruptedErase(
	flmem__Inst * 			pInst,
	Uint16 *				pCounters
) {
	flmem__SectorIdx		sectorIdx;
	flmem__SectorIdx		interruptedIdx1;
	flmem__SectorIdx		interruptedIdx2;

	interruptedIdx1 = FLMEM__SECTORINDEX_UNUSED;
	interruptedIdx2 = FLMEM__SECTORINDEX_UNUSED;

	/* 
	 *	Iterate through all sectors to find sectors where erasing has been
	 *	interrupted. Only one sector is erased at a time.
	 */

	sectorIdx = *pInst->pInit->sectorCount;
	do {
		sectorIdx--;
		if (pInst->pSectors[sectorIdx].seStatus == FLMEM__SE_ERASING) {
			if (interruptedIdx1 != FLMEM__SECTORINDEX_UNUSED) {
				deb_assert(interruptedIdx2 == FLMEM__SECTORINDEX_UNUSED);
				interruptedIdx2 = interruptedIdx1;
			} 

			interruptedIdx1 = sectorIdx;
		}
	} while (sectorIdx != 0);

	if (interruptedIdx1 == FLMEM__SECTORINDEX_UNUSED) {
		/*
		 *	Erasing was not interrupted during last shutdown. Just continue
		 *	normally.
		 */

	} else if (interruptedIdx2 == FLMEM__SECTORINDEX_UNUSED) {
		/*
		 *	One sector is saying that sector-erase was interrupted.
		 *	That sector can be trusted and erasing can be continued.
		 */

		FLMEM__MSG("16");

		pInst->currentCounter = pCounters[interruptedIdx1];
		pInst->currentSectorIdx = interruptedIdx1;

		flmem__nextSectorEraseBase(
			pInst,
			pInst->pSectors[interruptedIdx1].seNextIdx,
			FLMEM__SECTOR_ERASE_JUMP_TO_PHASE_3
		);

	} else {
		flmem__SectorIdx	newestIdx;

		/*
		 *	Two sectors claims to be erasing the next sector. One of the two 
		 *	sectors is partially erased and contains garbage. All other sectors
		 *	are valid. Find the newest of the other sectors. That sector will
		 *	point to the valid one of the two problem sectors.
		 */

		FLMEM__MSG("14");

		newestIdx = FLMEM__SECTORINDEX_UNUSED;
		sectorIdx = *pInst->pInit->sectorCount;
		while (--sectorIdx != 0) {
			if (sectorIdx != interruptedIdx1 && sectorIdx != interruptedIdx2) {
				if (newestIdx == FLMEM__SECTORINDEX_UNUSED) {
					newestIdx = sectorIdx;

				} else {
					int result;

					result = flmem__compareSequenceAges(
						sectorIdx,
						newestIdx,
						pCounters
					);
				
					if (result == 0xFFFF) {
						flmem__errorInformLocal(
							pInst,
							FLMEM__ERR_CORRUPTED_1,
							flmemCorruptedErr
						);

					} else if (result > 0) {
						/* The sector is newer */
						newestIdx = sectorIdx;
					}
				}
			}
		} 

		{
			flmem__SectorInfo *	pSector;

			pSector = &pInst->pSectors[newestIdx];
			pInst->currentCounter = pCounters[pSector->seNextIdx];
			pInst->currentSectorIdx = pSector->seNextIdx;
		}

		flmem__nextSectorEraseBase(
			pInst,
			pInst->pSectors[pInst->currentSectorIdx].seNextIdx,
			FLMEM__SECTOR_ERASE_JUMP_TO_PHASE_3
		);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__writeFirstCounterValue
*
*--------------------------------------------------------------------------*//**
*
*	\brief		When FLMEM module is started the first time we need to add first
*				seCounter-value to first sector.
*
*	\param		pInst			Pointer to FB instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__writeFirstCounterValue(
	flmem__Inst * 			pInst
) {
	BYTE *					pMemdrvData;

	pMemdrvData = pInst->pMemdrvData;

	if (pInst->pSectors[0].seStatus == FLMEM__SE_INVALID) {
		Uint8 status;

		/*
		 *	The first sector contains some garbage. All the data in that sector
		 *	is lost.
		 */

		if (pInst->pInit->projectRecoverOpt == FLMEM_RECOVER_ENABLE) {
			/* 
			 * It is ok if data is lost. Erase the sector and continue.
			 */

			status = flmem__sectorEraseBase(pInst, 0);
			if (status == MEMDRV_ERROR) {
				flmem__errorInformLocal(
					pInst,
					FLMEM__ERR_ERASE_1,
					flmemEraseErr
				);
			}

			pInst->pSectors[0].seStatus = FLMEM__SE_UNUSED;

		} else {
			/*
			 *	FLMEM has been configured to halt operation in this
			 *	situation.
			 */

			flmem__errorInformLocal(
				pInst,
				FLMEM__ERR_CORRUPTED_1,
				flmemCorruptedErr
			);
		}
	}

	/*
	 * Update new counter value in FLASH.
	 */

	flmem__setMemdrvValues(pInst, 0, 0, 2);
	pMemdrvData[FLMEM__SENTRY_COUNTER_H] =
		(BYTE)((FLMEM__STATUSENTRY_COUNT_MIN & 0xFF00) >> 8);
	pMemdrvData[FLMEM__SENTRY_COUNTER_L] =
		(BYTE)FLMEM__STATUSENTRY_COUNT_MIN & 0x00FF;
	flmem__callMemdrvPut(pInst);

	/*
	 *	Modify linked-list of status entries
	 */

	flmem__clearAndMoveNodeToEnd(pInst, 0);

	/*
	 *	Update status entry in RAM and the sector usage counter.
	 */

	pInst->currentCounter =	FLMEM__STATUSENTRY_COUNT_MIN;

	if (pInst->tFlags & FLMEM__TFLG_FORMAT1) {
		pInst->currentSectorUsage = FLMEM__SENTRY1_BLOCKSIZE;
	} else {
		pInst->currentSectorUsage = FLMEM__SENTRY2_BLOCKSIZE;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__sortStatusEntries
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function will sort the sector status entry list.
*
*	\param		pInst			Pointer to function block instance.
*	\param		pCounters		Pointer to counters-array.
*
*	\returns	-
*
*	\details	The list is sorted by sector age.
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__sortStatusEntries(
	flmem__Inst *			pInst,
	Uint16 *				pCounters
) {
	flmem__SectorInfo *		pUnsortedHead;
	flmem__SectorInfo *		pPicked;

	pPicked = pInst->pOldestSector->pNext;
	pInst->pOldestSector->pNext = NULL;

	do {
		flmem__SectorInfo * pCurrent;
		flmem__SectorInfo * pPrevious;

		pUnsortedHead = pPicked->pNext;

		/* Iterate sorted list until the right position is found */
		pCurrent = pInst->pOldestSector;
		pPrevious = NULL;
		do {
			int result;

			result = flmem__compareSequenceAges(
				(flmem__SectorIdx) (pPicked - pInst->pSectors),
				(flmem__SectorIdx) (pCurrent - pInst->pSectors),
				pCounters
			);

			if (result == 0xFFFF) {
				flmem__errorInformLocal(
					pInst,
					FLMEM__ERR_CORRUPTED_1,
					flmemCorruptedErr
				);

			} if (result < 0) {
				/* Picked is older than current. */
				break;
			}

			pPrevious = pCurrent;
			pCurrent = pCurrent->pNext;
		} while (pCurrent != NULL);

		/*
		 *	Insert the sector into the sorted list.
		 */

		pPicked->pNext = pCurrent;
		if (pInst->pOldestSector == pCurrent) {
			pInst->pOldestSector = pPicked;
		} else {
			if (pPrevious) {
				pPrevious->pNext = pPicked;
			}
		}

		/*
		 *	Pick next from unsorted list
		 */

		pPicked = pUnsortedHead;
	} while (pPicked != NULL);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__compareSequenceAges
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Comparing function for sorting.
*
*	\param		aIdx		Index of first status entry to compare.
*	\param		bIdx		Index of second status entry to compare.
*	\param		pCounters	Pointer to counters array.
*
*	\retval		-1			pA is older than pB.
*	\retval		1			pA is younger than pB.
*	\retval		0xFFFF		Invalid sequence numbers.
*
*	\details	Compares the age of two sectors. The sequence number is used 
*				to do this. A unused sector is always considered to be older
*				than a used sector. If both sectors are are unused, the
*				comparison is made using the sector index number.
*
*	\note		Function assumes that counter value of status entries will never
*				be ahead or behind more than half of given age-maxvalue. This
*				is assured by doing a periodic background copy.
*
*******************************************************************************/

PRIVATE int flmem__compareSequenceAges(
	flmem__SectorIdx 		aIdx,
	flmem__SectorIdx 		bIdx,
	Uint16 *				pCounters
) {
	Uint16					aCounter;
	Uint16					bCounter;
	int						retVal;

	aCounter = pCounters[aIdx];
	bCounter = pCounters[bIdx];

	if (
		aCounter == FLMEM__STATUSENTRY_COUNT_UNUSED
		&& bCounter == FLMEM__STATUSENTRY_COUNT_UNUSED
	) {
		retVal = aIdx > bIdx ? 1 : -1;

	} else if (aCounter == FLMEM__STATUSENTRY_COUNT_UNUSED) {
		retVal = -1;

	} else if (bCounter == FLMEM__STATUSENTRY_COUNT_UNUSED) {
		retVal = 1;

	} else {
		if (aCounter > bCounter) {
			if ((aCounter - bCounter) > (FLMEM__STATUSENTRY_COUNT_MAX / 2)) {
				/* b is newer */
				retVal = -1;

			} else {
				/* a is newer */
				retVal = 1;
			}
		} else {
			if (bCounter == aCounter) {
				retVal = 0xFFFF;

			} else if ((bCounter - aCounter) > (FLMEM__STATUSENTRY_COUNT_MAX / 2)) {
				/* a is newer */
				retVal = 1;
			
			} else if (bCounter == aCounter) {
				retVal = aIdx > bIdx ? 1 : -1;

			} else {
				/* b is newer */
				retVal = -1;
			}
		}
	}

	return retVal;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__handleCorruptedSectors
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle corrupted sectors according to FLMEM configuration.
*
*	\param		pInst		Pointer to FLMEM instance data.
*
*	\details	
*
*	\return		-
*
*	\note		
*
*******************************************************************************/

PRIVATE void flmem__handleCorruptedSectors(
	flmem__Inst *			pInst
) {
	flash_SectorIndexSizeType sectorNum;
	flmem__SectorInfo *		pSector;

	sectorNum = *pInst->pInit->sectorCount;
	pSector = pInst->pSectors;

	do {
		if (pSector->seStatus == FLMEM__SE_INVALID) {
			if (pInst->pInit->projectRecoverOpt == FLMEM_RECOVER_ENABLE) {
				/* The sector is corrupted but it is ok if data is lost. Mark
				 * the sector as unused. The sector will then be erased before
				 * the next use. */
				pSector->seStatus = FLMEM__SE_UNUSED;

			} else {
				/*
				 *	Data has been lost and FLMEM has been configured to halt
				 *	operation in that situation.
				 */

				flmem__errorInformLocal(
					pInst,
					FLMEM__ERR_CORRUPTED_1,
					flmemCorruptedErr
				);
			}
		}

		pSector++;
	} while(--sectorNum);
}
