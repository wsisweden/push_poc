/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_no.c
*
*	\ingroup	FLMEM
*
*	\brief		No wait mode handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "sys.h"
#include "reg.h"
#include "deb.h"

/* local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void	flmem__noWaitInit(flmem__Inst *);
PRIVATE void	flmem__noWaitDone(flmem__Inst *,flmem__MemoryIndex *);
PRIVATE void	flmem__noWaitRelease(flmem__Inst *);
PRIVATE void	flmem__noWaitLoaded(flmem__Inst *,Uint16);

PRIVATE void	flmem__noWaitRegPut(Uint16,ptrs_RegData_cDp,Uint8);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * This is the normal FLMEM operating mode. A pointer to this constant should
 * be supplied in the FLMEM init parameters when this mode is used.
 */

PUBLIC flmem_ModeIf const_P flmem_noWaitMode = {
	&flmem__noWaitInit,
	&flmem__noWaitDone,
	&flmem__noWaitRelease,
	&flmem__noWaitLoaded
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__noWaitInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the no wait mode.
*
*	\param		pInst			Pointer to FB instance
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__noWaitInit(
	flmem__Inst *			pInst
) {
	/* Set up reg-fb listener */
	reg_novPutPtr = &flmem__noWaitRegPut;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__noWaitDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Performs post write operations.
*
*	\param		pInst			Pointer to FB instance
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__noWaitDone(
	flmem__Inst *			pInst,
	flmem__MemoryIndex *	pTagInfo
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__noWaitRelease
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Performs abort operations.
*
*	\param		pInst			Pointer to FB instance
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__noWaitRelease(
	flmem__Inst *			pInst
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__noWaitLoaded
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Performs sectors loaded operations.
*
*	\param		pInst			Pointer to FB instance
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__noWaitLoaded(
	flmem__Inst *			pInst,
	Uint16					faultCount
) {
	Uint8					recoveryCounter;

	FLMEM__MSG("31");

	reg_getLocal(&recoveryCounter, reg_i_recoveryCounter, pInst->instId);

	if (faultCount != 0 || recoveryCounter != 0) {
		/*
		 * Add the fault count from current startup but limit the value to
		 * 255.
		 */
		if (recoveryCounter + faultCount < 255) {
			recoveryCounter += faultCount;

		} else {
			recoveryCounter = 255;
		}

		/*
		 * Decrement the recovery counter by one each startup unless it already
		 * is zero.
		 */
		if (recoveryCounter >= 1) {
			recoveryCounter--;
		}

		/* report warning if max exceeded */
		if (recoveryCounter > FLMEM__RECOVERY_COUNTER_MAX) {
			FLMEM__MSG("32");

			flmem__warningInform(
				pInst,
				FLMEM__ERR_CORRUPTED_3,
				flmemHardwareWarn,
				0
			);

		} else {
			reg_putLocal(
				&recoveryCounter,
				reg_i_recoveryCounter,
				pInst->instId
			);
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__noWaitRegPut
*
*--------------------------------------------------------------------------*//**
*
*	\details	This function is one of the main operating functions how FLMEM
*				is wanted to operate when REG-module sends data and how this
*				data should be persisted to Flash-memory. One of these
*				functions can be given as argument to the
*				flmem_Init -structure.
*
*				No wait means here that when any of the calling task saves
*				non-volatile register with reg_put -command flmem queues the
*				register and allows calling task to continue it's operations.
*				Write-request is queued to FIFO. If the FIFO holds
*				already the write-request it's discarded by FLMEM. FLMEM will
*				use only the latest value from the RAM when it handles the
*				request from the FIFO.
*
*	\param		offset		BYTE offset to the target (NOV) location of the
*							data.
*	\param		pData		Pointer to the data that should be stored.
*	\param		byteCount 	Number of BYTEs (0..) to save.
*
*	\returns	-
*
*	\note
*
*	\see		flmem_FnAccess
*	\see		flmem_regPutWaitSingle
*	\see		flmem_regPutWaitGroup
*
*******************************************************************************/

PRIVATE void flmem__noWaitRegPut(
	Uint16					offset,
	ptrs_RegData_cDp		pData,
	Uint8					byteCount
) {
	flmem__Inst *			pInst;
	Uint16					tagIndex;

	pInst = &flmem__instance;

	tagIndex = flmem__regPutSearchTag(pInst, offset);

	if (pInst->err > 0 || tagIndex == Uint16_MAX) {
		return;
	}

	flmem__regPutBase(pInst, tagIndex, byteCount);
}
