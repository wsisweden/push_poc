/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	FLMEM
*
*	\brief		Reg interface.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 3986 $ \n
*				\$Date: 2019-11-07 16:25:05 +0200 (to, 07 marras 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* c library */
#include <string.h>

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"
#include "sys.h"
#include "reg.h"
#include "flash.h"
#include "FLMEM.H"

/* FLMEM local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

extern BYTE sys_imageRAM[];

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__regWrite
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write data directly to the REG RAM image.
*
*	\param		pInst			Pointer to FLMEM instance.
*	\param		tag				Register tag.
*	\param		pMemdrvData		Pointer to buffer containing the data to be
*								written.
*	\param		dataSize		Number of bytes to be written.
*
*	\return		The tag index number of the written register.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Uint16 flmem__regWrite(
	flmem__Inst *			pInst,
	Uint16					tag,
	BYTE *					pMemdrvData,
	Uint8					dataSize
) {
	flmem__TagIdx	tagIdx;
	Uint8			searchRet;
	
	searchRet = flmem__binarySearchMemoryIndexByTag(pInst, tag, &tagIdx);

	if (searchRet == 1) {
		flmem__MemoryIndex * pTagInfo;

		pTagInfo = &pInst->pMemoryIndexes[tagIdx];

		DISABLE;
		pTagInfo->ramSize = dataSize;
		memcpy(
			sys_imageRAM + pInst->pInit->flashtagsByTagsOrder[tagIdx].offset,
			pMemdrvData,
			dataSize
		);
		ENABLE;
	}

	return(tagIdx);	
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__regRead
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read data directly from the REG RAM image.
*
*	\param		pInst			Pointer to FLMEM instance.
*	\param		tagIdx			Index to tag information table.
*	\param		pMemdrvData		Pointer to memory buffer where the data will
*								be put.
*
*	\return		Size of the data that was read.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Uint8 flmem__regRead(
	flmem__Inst *			pInst,
	flmem__TagIdx			tagIdx,
	BYTE *					pMemdrvData
) {
	Uint8						dataSize;
	flmem_TagOffset const_P *	pTagInfoP;
	flmem__MemoryIndex *		pTagInfo;

	pTagInfoP = &pInst->pInit->flashtagsByTagsOrder[tagIdx];
	pTagInfo = &pInst->pMemoryIndexes[tagIdx];

	/*
	 * 	Register size and data may be changed at any time from another task.
	 * 	The access must be protected. Size and data must be read atomically.
	 */

	DISABLE;
	dataSize = pTagInfo->ramSize;
	memcpy(
		pMemdrvData,
		sys_imageRAM + pTagInfoP->offset,
		dataSize
	);
	ENABLE;

	return(dataSize);
}
