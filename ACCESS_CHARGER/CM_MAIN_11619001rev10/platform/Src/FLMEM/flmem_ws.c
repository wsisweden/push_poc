/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_ws.c
*
*	\ingroup	FLMEM
*
*	\brief		Wait single mode handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"
#include "sys.h"
#include "reg.h"

/* local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	osa_TaskType *			pActiveTask;/** WaitSingle-implementation uses 
										 *	this to signal single task to
										 *	continue						*/
	osa_SemaType			taskSema;	/** This semaphore is used to
										 *	implement the waitSingle 
										 *	reg_put-implementation			*/
} flmem__WaitSingleData;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void	flmem__waitSingleInit(flmem__Inst *);
PRIVATE void	flmem__waitSingleRegPut(Uint16,ptrs_RegData_cDp,Uint8);
PRIVATE void	flmem__waitSingleDone(flmem__Inst *,flmem__MemoryIndex *);
PRIVATE void	flmem__waitSingleRelease(flmem__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

PUBLIC flmem_ModeIf const_P flmem_waitSingleMode = {
	&flmem__waitSingleInit,
	&flmem__waitSingleDone,
	&flmem__waitSingleRelease
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE flmem__WaitSingleData flmem__waitSingleData;

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/

PRIVATE void flmem__waitSingleInit(
	flmem__Inst *			pInst
) {
	/* initialize flmem task triggering semaphore */
	osa_newSemaSet(&flmem__waitSingleData.taskSema);

	flmem__waitSingleData.pActiveTask = NULL;

	/* Set up reg-fb listener */
	reg_novPutPtr = &flmem__waitSingleRegPut;
}

PRIVATE void flmem__waitSingleDone(
	flmem__Inst *			pInst,
	flmem__MemoryIndex *	pTagInfo
) {
	DISABLE;

	pTagInfo->nextArrIdx &= ~FLMEM__MEMORYINDEX_INDEX_MASK_HIGH;

	if (flmem__waitSingleData.pActiveTask != NULL) {
		/* osa_TaskType tempTask; */
		/* tempTask = (*pInst->pActiveTask); */
		flmem__waitSingleData.pActiveTask = NULL;
		/* osa_signalSend(&tempTask); */
	}

	ENABLE;
}

PRIVATE void flmem__waitSingleRelease(
	flmem__Inst *			pInst
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__waitSingleRegPut
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This function is one of the main operating functions how FLMEM
*				is wanted to operate when REG-module sends data and how this
*				data should be persisted to Flash-memory. One of these
*				functions can be given as argument to the
*				flmem_Init -structure.
*
*				"Wait Single" means here that the calling task will be blocked
*				until FLMEM has processed the write-request.
*
*	\param		targetIndex BYTE offset to the target (NOV) location of the
*							data.
*	\param		pData		Pointer to the data to be stored.
*	\param		byteCount 	Number of BYTEs (0..) to save.
*
*	\returns	-
*
*	\details
*
*	\note
*
*	\see		flmem_regPutNoWait
*	\see		flmem_regPutWaitSingle
*	\see		flmem_regPutWaitGroup
*
*******************************************************************************/

PRIVATE void flmem__waitSingleRegPut(
	Uint16					targetIndex,
	ptrs_RegData_cDp		pData,
	Uint8					byteCount
) {
	Uint16					sFlg;
	flmem__Inst *			pInst;
	Uint16					tagIndex;

	pInst = &flmem__instance;

	tagIndex = flmem__regPutSearchTag(pInst, targetIndex);

	atomic(
		sFlg = pInst->sFlags;
	);

	if (pInst->err > 0 || tagIndex == Uint16_MAX) {
		return;

	/* Write-requests denied! */
	} else if (sFlg & FLMEM__SFLG_POWER_DOWN_DENY_WRITE ||
		sFlg & FLMEM__SFLG_POWER_DOWN_ALLOW_LAST) {
		return;
	}

	/*
	 *	Wait for previous write-request to processed.
	 */

	osa_semaGet(&flmem__waitSingleData.taskSema);

	atomic(
		sFlg = pInst->sFlags;
	);

	if (pInst->err > 0) {

		FLMEM__MSG("10");

		/* loop and release all tasks that are waiting here!! */
		osa_semaSet(&flmem__waitSingleData.taskSema);

		return;

	/* Write-requests denied! */
	} else if (sFlg & FLMEM__SFLG_POWER_DOWN_DENY_WRITE ||
		sFlg & FLMEM__SFLG_POWER_DOWN_ALLOW_LAST) {

		FLMEM__MSG("11");

		/* loop and release all tasks that are waiting here!! */
		osa_semaSet(&flmem__waitSingleData.taskSema);

		return;
	}

	atomic(
		flmem__waitSingleData.pActiveTask = osa_taskCurrent();
	);

	/* Validate and change bitflag */
#ifndef NDEBUG
	if (
		(
			pInst->pMemoryIndexes[tagIndex].nextArrIdx &
				FLMEM__MEMORYINDEX_INDEX_MASK_HIGH
		) != 0
	) {
		deb_assert(FALSE);
	}
#endif

	pInst->pMemoryIndexes[tagIndex].nextArrIdx |=
		FLMEM__MEMORYINDEX_INDEX_MASK_HIGH;

	/* Add request to be processed by FLMEM */
	{
		Uint8 error;

		error = flmem__regPutBase(
			pInst,
			tagIndex,
			byteCount
		);

		if (error == FLMEM_SERVICE_OK) {

			osa_TaskType * pTask;

			do {
				
				/*
				 *	todo:	this could be implemented with osa_signalGet, but it
				 *			needs also a additional osa_semaGet/Set. This is
				 *			because flmem-powerdown might give the osa_signalSet
				 *			before we get even to the osa_signalGet here. Tasks 
				 *			would be locked here and powerdown handling for
				 *			multiple tasks wouldn't succeed.
				 */

				osa_yield();

				atomic(
					pTask = flmem__waitSingleData.pActiveTask;
				);

			} until (pTask == NULL);

		} else {
			atomic(
				flmem__waitSingleData.pActiveTask = NULL;
			);
		}

		/*
		 *	Release next write-request(next task) to be processed and
		 *	release current task on its way.
		 */

		osa_semaSet(&flmem__waitSingleData.taskSema);
	}
}
