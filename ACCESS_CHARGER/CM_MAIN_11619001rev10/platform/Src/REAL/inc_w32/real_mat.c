/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		REAL\inc_w32\real_mat.c
*
*	\ingroup	REAL_W32
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	REAL_W32	W32
*
*	\ingroup	REAL
*
*	\brief		
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "real.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#define real__loWord(r_)	((Uint32)(r_) & 0xFFFF)
#define real__hiWord(r_)	(((Uint32)(r_) >> 16) & 0xFFFF)

#define REAL_BIT_HIGH		((Uint32)1 << (REAL__BITS - 1))


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/

PUBLIC void real__div(
	Real *					pReal1,
	Real *					pReal2
) {
	Uint8					nTmp;
	Int8					nSign = 1;
	Uint32					nRemain;

	if (*pReal1 < 0) 
	{
		*pReal1 *= -1;
		nSign *= -1;
	}

	if (*pReal2 < 0) 
	{
		*pReal2 *= -1;
		nSign *= -1;
	}

	nRemain = (*pReal1 >> (REAL__BITS - REAL__FIX)) - *pReal2;
	*pReal1 <<= REAL__FIX;

	nTmp = 8;

	do 
	{
		Real				rTemp;

		rTemp = (nRemain << 1) | ((Uint32) *pReal1 >> (REAL__BITS - 1));
		*pReal1 <<= 1;
		nRemain = nRemain & REAL_BIT_HIGH ? rTemp + *pReal2 : (*pReal1 |= 1, rTemp - *pReal2);

		rTemp = (nRemain << 1) | ((Uint32) *pReal1 >> (REAL__BITS - 1));
		*pReal1 <<= 1;
		nRemain = (nRemain & REAL_BIT_HIGH) ? (rTemp + *pReal2) : (*pReal1 |= 1, rTemp - *pReal2);

		rTemp = nRemain << 1 | (Uint32) *pReal1 >> (REAL__BITS - 1);
		*pReal1 <<= 1;
		nRemain = nRemain & REAL_BIT_HIGH ? rTemp + *pReal2 : (*pReal1 |= 1, rTemp - *pReal2);

		rTemp = nRemain << 1 | (Uint32) *pReal1 >> (REAL__BITS - 1);
		*pReal1 <<= 1;
		nRemain = nRemain & REAL_BIT_HIGH ? rTemp + *pReal2 : (*pReal1 |= 1, rTemp - *pReal2);

	} while (--nTmp);

	*pReal1 <<= 1;

	if (nRemain & REAL_BIT_HIGH) 
	{
		nRemain = (Real) nRemain + *pReal2;
	} 
	else 
	{
		*pReal1 |= 1;
	}

	if (nRemain >= *pReal2 - nRemain) 
	{
		(*pReal1)++;
	}

	*pReal1 *= nSign;
}

PUBLIC void real__mul(
	Real *					pReal1,
	Real *					pReal2
) {
	Uint32					nTmp1;
	Uint32					nTmp2;
	Uint32					nTmp3;
	Int8					nSign = 1;

	if (*pReal1 < 0) 
	{
		*pReal1 *= -1;
		nSign *= -1;
	}

	if (*pReal2 < 0) 
	{
		*pReal2 *= -1;
		nSign *= -1;
	}

	/*	
	 *  Multiply
	 */
	nTmp1 = real__loWord(*pReal1) * real__hiWord(*pReal2);
	nTmp2 = real__hiWord(*pReal1) * real__loWord(*pReal2);
	nTmp3 = real__hiWord(*pReal1) * real__hiWord(*pReal2) + real__hiWord(nTmp1) + real__hiWord(nTmp2);
	nTmp2 = real__loWord(nTmp2) + real__loWord(nTmp1);
	nTmp1 = real__loWord(*pReal1) * real__loWord(*pReal2);
	nTmp2 += real__hiWord(nTmp1);

	/*	
	 *  Rounding
	 */
	nTmp1 = real__loWord(nTmp1) + (1 << (REAL__FIX - 1));
	nTmp2 += real__hiWord(nTmp1);
	nTmp1 = real__loWord(nTmp1) + (real__loWord(nTmp2) << 16);
	nTmp3 += real__hiWord(nTmp2);

	/* Shift right the result, add the sign and return. */

	nTmp1 = (nTmp1 >> REAL__FIX) + (nTmp3 << (REAL__BITS - REAL__FIX));

	nTmp1 *= nSign;

	*pReal1 = nTmp1;
}
