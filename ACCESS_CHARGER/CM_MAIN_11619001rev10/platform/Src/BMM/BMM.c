/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	BMM
*
*	\brief		Block structure handling functions.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	BMM	BMM
*
*	\brief		Block memory management.
*
********************************************************************************
*
*	\details	BMM is used to manage buffers that can be split into multiple
*				blocks.
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "BMM.H"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_getBuffer
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns a pointer to the data buffer of the block.
*
*	\param		pBlock		Pointer to a block.
*	\param		pUsedBytes	The number of data bytes that the block contains
*							will be stored to this variable. Can be NULL if the
*							used size is not needed.
*
*   \return		Pointer to the data buffer of the block.
*
*	\details	The user must check that there is enough space in the returned
*				buffer before writing data.
*
*******************************************************************************/

PUBLIC BYTE * bmm_getBuffer(
	bmm_Block *					pBlock,
	bmm_BlockSize *				pUsedBytes
) {
	BYTE *						pRet;

	deb_assert(pBlock != NULL);

	switch (pBlock->type) {
		case BMM_BLTYPE_RAM_EXTBUF:
		case BMM_BLTYPE_RAM_EXT:
			pRet = ((bmm_BlockExt *) pBlock)->pData;
			break;

		case BMM_BLTYPE_RAM_PTR:
			pRet = ((bmm__BlockPtr *) pBlock)->pData;
			break;

		case BMM_BLTYPE_RAM_ARR:
			pRet = ((bmm__BlockArr *) pBlock)->data;
			break;

		default:
			pRet = NULL;
			break;
	}

	if (pUsedBytes != NULL) {
		*pUsedBytes = pBlock->usedSize;
	}

	return(pRet);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_setUsedSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the amount of used bytes in the block buffer.
*
*	\param		pBlock		Pointer to the block.
*	\param		usedSize	Number of used bytes in the block buffer.
*
*   \return		-
*
*	\details	This needs to be called after data has been copied to the 
*				block buffer.
*
*	\see		bmm_getBuffer
*
*******************************************************************************/

PUBLIC void bmm_setUsedSize(
	bmm_Block *				pBlock,
	bmm_BlockSize			usedSize
) {
	deb_assert(pBlock != NULL);

	pBlock->usedSize = usedSize;
}
