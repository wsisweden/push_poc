/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		BMM_EXTB.c
*
*	\ingroup	BMM
*
*	\brief		RAM_EXTBUF block handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "BMM.H"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_allocBlockExtBuf
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate a RAM_EXTBUF type block from the pool.
*
*	\param		pPool			Pointer to the block pool.
*	\param		pData			Pointer to the data buffer of the block.
*	\param		pFreeBlockFn	Pointer to the free buffer callback function.
*								Can be set to NULL if no callback is needed.
*
*   \return		Pointer to the allocated block. NULL if there were no free
*				blocks in the pool.
*
*	\details	This function will allocate a full sized block from the pool.
*				The data part of the block is only used to store a pointer to
*				the actual data.
*				
*				This type of block should be avoided because it wastes a lot of
*				RAM. The EXT type block should be considered instead.
*	
*	\note		May be called from an ISR.
*
*******************************************************************************/

PUBLIC bmm_Block * bmm_allocBlockExtBuf(
	bmm_BlockPool *				pPool,
	BYTE *						pData,
	bmm_FreeBlockFn *			pFreeBlockFn
) {
	bmm_Block *					pBlock;

	deb_assert(pPool);
	deb_assert(pData);

	pBlock = bmm_allocBlock(pPool);

	if (pBlock) {
		bmm_BlockExt * pPtrBlock;

		pBlock->type = BMM_BLTYPE_RAM_EXTBUF;

		pPtrBlock = (bmm_BlockExt *) pBlock;
		pPtrBlock->pData = pData;
		pPtrBlock->pFreeBlockFn = pFreeBlockFn;

		/* The block data buffer should be aligned */
		deb_assert((Uint32)pPtrBlock->pData % TARGET_MEM_ALIGN == 0);
	}

	return pBlock;
}
