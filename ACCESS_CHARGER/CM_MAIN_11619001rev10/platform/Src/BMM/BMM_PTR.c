/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		BMM_PTR.c
*
*	\ingroup	BMM
*
*	\brief		Handling for ARR_PTR type blocks.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "BMM.H"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_allocBlockPtr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate a RAM_PTR type block from the pool.
*
*	\param		pPool			Pointer to the block pool.
*
*   \return		Pointer to the allocated block. NULL if there were no free
*				blocks in the pool.
*
*	\details	This function allocates a block structure and buffer from the
*				block pool. This type of block supports hiding of headers which
*				is useful when passing received frames to higher level
*				protocols.
*	
*	\note		May be called from an ISR.
*
*******************************************************************************/

PUBLIC bmm_Block * bmm_allocBlockPtr(
	bmm_BlockPool *				pPool
) {
	bmm_Block *					pBlock;

	pBlock = bmm_allocBlock(pPool);

	if (pBlock) {
		bmm__BlockPtr * pPtrBlock;

		pBlock->type = BMM_BLTYPE_RAM_PTR;

		pPtrBlock = (bmm__BlockPtr *) pBlock;
		pPtrBlock->pData = pPtrBlock->data;

		/* The block data buffer should be aligned */
		deb_assert((Uint32)pPtrBlock->pData % TARGET_MEM_ALIGN == 0);
	}

	return pBlock;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_movePtr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Move the data pointer to hide or reveal protocol headers.
*
*	\param		pBlock		Pointer to the block.
*	\param		offset		Number of bytes the data pointer should be moved.
*							The number may also be negative.
*
*   \return		-
*
*	\details	The function will also adjust the "usedSize" value. It is up to
*				the user to check that there is enough space in the block.
*
*******************************************************************************/

PUBLIC void bmm_movePtr(
	bmm_Block *					pBlock,
	Int16						offset
) {
	bmm__BlockPtr *				pPtrBlock;

	deb_assert(pBlock);
	deb_assert(pBlock->type == BMM_BLTYPE_RAM_PTR);

	pPtrBlock = (bmm__BlockPtr *) pBlock;

	pPtrBlock->pData += offset;
	pPtrBlock->base.usedSize -= MIN(offset, pPtrBlock->base.usedSize);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_getPtrBlockSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the size of the block data buffer in a PTR type block.
*
*	\param		pBlock		Pointer to the block.
*	\param		pPool		Pointer to the block pool.
*
*   \return		The size of the block data buffer.
*
*	\details	
*
*******************************************************************************/

PUBLIC bmm_BlockSize bmm_getPtrBlockSize(
	bmm_Block *					pBlock,
	bmm_BlockPool *				pPool
) {
	bmm_BlockSize				blockSize;

	blockSize = bmm_getBlockSize(pPool);
	blockSize -= offsetof(bmm__BlockPtr, data) - sizeof(bmm_Block);

	return(blockSize);
}
