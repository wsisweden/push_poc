/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	BMM
*
*	\brief		Local declarations for the block memory management FB.
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

#ifndef LOCAL_H_INCLUDED
#define LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"

#include "BMM.H"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	The two last pointers of the bmm__BlockPtr struct must always fit inside the
 *	data buffer for BMM_BLTYPE_PTR type blocks to work.
 */

#define BMM__MIN_BLOCK_SIZE	(sizeof(bmm__BlockPtr) - sizeof(bmm_Block))

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type used by blocks of type BMM_BLTYPE_RAM_ARR.
 */

typedef struct {							/*''''''''''''''''''''''''' RAM */	
	bmm_Block					base;		/**< The base block structure.	*/
	BYTE 						data[1];	/**< The data buffer.			*/
} bmm__BlockArr;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Type used by blocks of type BMM_BLTYPE_RAM_ARR.
 */

typedef struct {							/*''''''''''''''''''''''''' RAM */	
	bmm_Block					base;		/**< The base block structure.	*/
	BYTE *						pData;		/**< Pointer to the data buffer.*/
	BYTE						data[1];	/**< The data buffer.			*/	
} bmm__BlockPtr;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Type containing information about a block pool.
 */

struct bmm__blockPool {						/*''''''''''''''''''''''''' RAM */
	bmm_Block * 				pFirstFree;	/**< List of free blocks.		*/
	Uint16 						blockCount;	/**< Number of blocks in block
											 *	pool.						*/
	bmm_BlockSize 				blockSize;	/**< Data buffer size of the
											 *	blocks in the pool.			*/
	bmm__BlockArr				pool[1];	/**< First block in block pool,
											 *	remaining blocks follow in equal
											 *	intervals set by block size.*/
};											/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************//** \endcond pub_decl */

#endif
