/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	BMM
*
*	\brief		Block pool handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "BMM.H"
#include "deb.h"
#include "lib.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint16 bmm__getWholeBlockSize(bmm_BlockSize);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_initPool
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the block pool.
*
*	\param		pBuffer		Pointer to buffer that will be used by the
*							block pool.
*	\param		buffSize	Size of the buffer pPool points to.
*	\param		blockSize	The desired block size. The actual block size may
*							be larger.
*								
*   \return		Pointer to the created pool.
*
*	\details	
*
*******************************************************************************/

PUBLIC bmm_BlockPool * bmm_initPool(
	BYTE *						pBuffer,
	Uint16						buffSize,
	bmm_BlockSize				blockSize
) {
	bmm_Block *					pBlock;
	bmm_BlockPool *				pPool;
	Uint16						wholeBlockSize;

	deb_assert(pBuffer != NULL);
	deb_assert(blockSize >= BMM__MIN_BLOCK_SIZE);

	pPool = (bmm_BlockPool *) lib_memAlign(pBuffer, TARGET_MEM_ALIGN);
	buffSize -= (Uint16) ((BYTE *)pPool - pBuffer);

	pPool->blockCount = 0;
	pPool->pFirstFree = NULL;

	/*
	 *	Split the buffer into blocks of the desired size. Build a free blocks
	 *	list.
	 */

	wholeBlockSize = bmm__getWholeBlockSize(blockSize);
	pPool->blockSize = (bmm_BlockSize) (
		wholeBlockSize - offsetof(bmm__BlockArr, data)
	);

	buffSize -= offsetof(bmm_BlockPool, pool);

	/*
	 *	The pool buffer should be big enough to hold at least one block.
	 */
	deb_assert(buffSize >= wholeBlockSize);

	pBlock = (bmm_Block *) pPool->pool;
	do {
		pBlock->usedSize = 0;
		pBlock->type = BMM_BLTYPE_RAM_ARR;

		pBlock->pNext = pPool->pFirstFree;
		pPool->pFirstFree = pBlock;
		pPool->blockCount++;

		pBlock = (bmm_Block *) (void *) (((BYTE *)pBlock) + wholeBlockSize);
		buffSize -= wholeBlockSize;
	} while (buffSize >= wholeBlockSize);

	return pPool;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_getPoolSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the required buffer size for the whole block pool.
*
*	\param		blockSize	The desired block size.
*	\param		blockCount	The desired amount of blocks.
*
*	\return		The required memory size of the specified block pool.
*
*	\details	This function may be used when calculating the needed buffer
*				size for a block pool.
*	
*	\par		Example
*	\code		
*				poolSize = bmm_getPoolSize(256, 3);
*				pPoolBuffer = mem_reserve(&mem_normal, poolSize);
*				deb_assert(pPoolBuffer != NULL);
*				bmm_initPool(pPoolBuffer, poolSize, 256);
*	\endcode
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Uint16 bmm_getPoolSize(
	bmm_BlockSize			blockSize,
	Uint16					blockCount
) {
	return(
		offsetof(bmm_BlockPool, pool)
		+ bmm__getWholeBlockSize(blockSize) * blockCount
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_allocBlock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate an array type block from the pool.
*
*	\param		pPool		Pointer to the block pool.
*
*   \return		Pointer to the allocated block. NULL if there were no free
*				blocks in the pool.
*
*	\details	
*	
*	\note		May be called from an ISR.
*
*******************************************************************************/

PUBLIC bmm_Block * bmm_allocBlock(
	bmm_BlockPool *				pPool
) {
	bmm_Block *					pBlock;
	tools_IsrState				isrState;

	DISABLEINTR(&isrState);
	pBlock = pPool->pFirstFree;
	if (pBlock != NULL) {
		pPool->pFirstFree = pBlock->pNext;
		pBlock->pNext = NULL;

		/* The block data buffer should be aligned */
		deb_assert(
			(Uint32) ((bmm__BlockArr *) pBlock)->data % TARGET_MEM_ALIGN == 0
		);
	}
	ENABLEINTR(&isrState);

	return(pBlock);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_allocBlocks
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate multiple ARR-type blocks and add them to the supplied
*				list.
*
*	\param		pPool	Pointer to the block pool.
*	\param		pList	Pointer to the block list.
*	\param		count	Number of block that should be allocated.
*
*   \retval		TRUE	The block were allocated successfully.
*   \retval		FALSE	Not enough free blocks in the pool. No block were
*						allocated.
*
*	\details	May be called from an ISR.
*
*******************************************************************************/

PUBLIC Boolean bmm_allocBlocks(
	bmm_BlockPool *				pPool,
	bmm_BlockList *				pList,
	Uint8						count
) {
	bmm_Block *					pBlock;
	tools_IsrState				isrState;
	Boolean						retVal;

	DISABLEINTR(&isrState);

	pBlock = pList->pRead = pPool->pFirstFree;
	
	while (--count && pBlock != NULL) {
		pBlock = (bmm_Block *) pBlock->pNext;
	}

	if (pBlock != NULL) {
		pPool->pFirstFree = pBlock->pNext;
		pBlock->pNext = NULL;
		pList->pPool = pPool;
		pList->pWrite = pBlock;
		ENABLEINTR(&isrState);

		retVal = TRUE;

	} else {
		pList->pRead = NULL;
		ENABLEINTR(&isrState);

		retVal = FALSE;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_freeBlock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Free the supplied block.
*
*	\param		pPool		Pointer to the block pool.
*	\param		pBlock		Pointer to block that should be put back into the
*							free block pool.
*
*   \return		-
*
*	\details	
*	
*	\note		May be called from an ISR.
*
*******************************************************************************/

PUBLIC void bmm_freeBlock(
	bmm_BlockPool *				pPool,
	bmm_Block *					pBlock
) {
	deb_assert(pPool);
	deb_assert(pBlock);

	switch (pBlock->type) {
		case BMM_BLTYPE_RAM_EXTBUF:
		case BMM_BLTYPE_RAM_EXT: {
			bmm_BlockExt * pPtrBlock;

			pPtrBlock = (bmm_BlockExt *) pBlock;
			if (pPtrBlock->pFreeBlockFn) {
				pPtrBlock->pFreeBlockFn(pBlock, pPtrBlock->pData);
			}

			if (pBlock->type == BMM_BLTYPE_RAM_EXT) {
				/*
				 *	External blocks should not be put in the pool.
				 */
				break;
			}
		}
		case BMM_BLTYPE_RAM_PTR:
			pBlock->type = BMM_BLTYPE_RAM_ARR;
		case BMM_BLTYPE_RAM_ARR: {
			pBlock->usedSize = 0;
			atomicintr(
				pBlock->pNext = pPool->pFirstFree;
				pPool->pFirstFree = pBlock;
			);
			break;
		}
		
		default:
			deb_assert(FALSE);
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_getBlockSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the block data buffer size of RAM_ARR type blocks used by
*				the specified pool.
*
*	\param		pPool		Pointer to the block pool.
*
*   \return		Size of the blocks in the pool.
*
*	\details	
*	
*	\note		May be called from an ISR.
*
*******************************************************************************/

bmm_BlockSize bmm_getBlockSize(
	bmm_BlockPool *			pPool
) {
	return(pPool->blockSize);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_getWholeBlockSize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the total size of RAM_ARR type blocks.
*
*	\param		blockSize	Desired size of the block data area.
*
*	\return		Size of the block header and the block data buffer.
*
*	\details	This function may be used to calculate the total size of a
*				block when using a block pool.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE Uint16 bmm__getWholeBlockSize(
	bmm_BlockSize			blockSize
) {
	Uint16 wholeBlockSize;

	/*
	 *	Add block header size.
	 */

	wholeBlockSize = offsetof(bmm__BlockArr, data) + blockSize;

	/*
	 *	Add additional bytes to the end of the block to make the block end at an
	 *	aligned address. This way each block in an array of blocks will start
	 *	at an aligned address.
	 *
	 *	The block data buffer may actually be larger than the supplied size.
	 */

	wholeBlockSize =
		(wholeBlockSize + TARGET_MEM_ALIGN - 1)
		& ~(TARGET_MEM_ALIGN - 1);

	return(wholeBlockSize);
}