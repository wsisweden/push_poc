/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	BMM
*
*	\brief		RAM_EXT block handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "BMM.H"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_allocBlockExtBuf
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize a RAM_EXT type block.
*
*	\param		pBlock			Pointer to the block.
*	\param		pData			Pointer to the data buffer of the block.
*	\param		pFreeBlockFn	Pointer to the free buffer callback function.
*								Can be set to NULL if no callback is needed.
*
*   \return		-
*
*	\details	This function will initialize a RAM_EXT type block.
*	
*	\note		May be called from an ISR.
*
*******************************************************************************/

PUBLIC void bmm_initExtBlock(
	bmm_BlockExt *				pBlock,
	BYTE *						pData,
	bmm_FreeBlockFn *			pFreeBlockFn
) {
	deb_assert(pBlock);
	deb_assert(pData);

	pBlock->base.type = BMM_BLTYPE_RAM_EXT;
	pBlock->pData = pData;
	pBlock->pFreeBlockFn = pFreeBlockFn;
}
