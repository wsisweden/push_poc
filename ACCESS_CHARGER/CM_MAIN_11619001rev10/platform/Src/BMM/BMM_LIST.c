/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	BMM
*
*	\brief		Block list handling function definitions.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "BMM.H"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_getReadBlock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Removes the first block from the list.
*
*	\param		pList		Pointer to the block list.
*
*	\return		Pointer to the block that has been removed from the list.
*
*	\details	The removed block is not deallocated by this function.
*
*	\note		This function may be called from an ISR.
*
*******************************************************************************/

PUBLIC bmm_Block * bmm_getFirst(
	bmm_BlockList *				pList
) {
	bmm_Block *					pRead;
	tools_IsrState				isrState;

	DISABLEINTR(&isrState);

	pRead = pList->pRead;
	if (pRead != NULL) {
		/*
		 *	There are blocks on the list.
		 */

		pList->pRead = pRead->pNext;
		if (pList->pRead == NULL) {
			/*
			 *	The list is empty now.
			 */
			pList->pWrite = NULL;
		}

		pRead->pNext = NULL;
	}
	
	ENABLEINTR(&isrState);

	return(pRead);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_peekFirst
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get pointer to the first block in the list.
*
*	\param		pList		Pointer to the block list.
*
*   \return		Pointer to the first block on the list.
*
*	\details	This function does not remove the block from the list.
*
*******************************************************************************/

PUBLIC bmm_Block * bmm_peekFirst(
	bmm_BlockList *				pList
) {
	bmm_Block *					pRead;

	deb_assert(pList != NULL);

	atomicintr(
		pRead = pList->pRead;
	);

	return(pRead);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_putFirst
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Put the supplied block first in the block list.
*
*	\param		pList			Pointer to the block list.
*	\param		pBlock			Pointer to the block that should be added to the
*								list.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PUBLIC void bmm_putFirst(
	bmm_BlockList *				pList,
	bmm_Block *					pBlock
) {
	tools_IsrState				isrState;

	DISABLEINTR(&isrState);

	/*
	 *	The supplied block should not be on any list at this point.
	 */
	deb_assert(pBlock->pNext == NULL);

	if (pList->pRead == NULL) {
		/*
		 *	The list is empty.
		 */

		pList->pWrite = pBlock;

	} else {
		pBlock->pNext = pList->pRead;
	}

	pList->pRead = pBlock;

	ENABLEINTR(&isrState);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_putLast
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Puts the supplied block last in the list.
*
*	\param		pList		Pointer to the block list.
*	\param		pBlock		Pointer to the block that should be added to the 
*							list.
*
*	\return		-
*
*	\details	
*
*	\note		This function may be called from an ISR.
*
*******************************************************************************/

PUBLIC void bmm_putLast(
	bmm_BlockList *				pList,
	bmm_Block *					pBlock
) {
	tools_IsrState				isrState;

	DISABLEINTR(&isrState);

	/*
	 *	The supplied block should not be on any list at this point.
	 */
	deb_assert(pBlock->pNext == NULL);

	if (pList->pWrite == NULL) {
		/*
		 *	The list is empty.
		 */

		pList->pRead = pBlock;

	} else {
		pList->pWrite->pNext = pBlock;
	}

	pList->pWrite = pBlock;

	ENABLEINTR(&isrState);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_listIsEmpty
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check if the block list is empty.
*
*	\param		pList	Pointer to the list.
*
*   \retval		TRUE	The list is empty.
*   \retval		FALSE	The list is not empty.
*
*	\details	
*	
*	\note		This function may be called from an ISR.
*
*******************************************************************************/

PUBLIC Boolean bmm_listIsEmpty(
	bmm_BlockList *				pList
) {
	Boolean						isEmpty;

	atomicintr(
		isEmpty = pList->pRead == NULL ? TRUE : FALSE;	
	);

	return(isEmpty);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_moveList
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Move all the blocks from the source list to the destination
*				list.
*
*	\param		pSrcList	Source list.
*	\param		pDstList	Destination list.
*
*   \return		-
*
*	\details	The blocks of the source list will be appended to the
*				destination list. The source list will be empty after this call.
*				
*				If both lists contain blocks then they have to use the same
*				block pool. The source list block pool will be assigned to the
*				destination list.
*				
*	\note		This function may be called from an ISR.
*
*******************************************************************************/

PUBLIC void bmm_moveList(
	bmm_BlockList *				pSrcList,
	bmm_BlockList *				pDstList
) {
	tools_IsrState				isrState;

	/*
	 *	The lists must use the same block pool.
	 */

	if (pDstList->pPool != NULL && pDstList->pPool != pSrcList->pPool) {
		/*
		 *	Cannot move blocks to a list that uses a different block pool.
		 */

		deb_assert(FALSE);
		return;
	}

	DISABLEINTR(&isrState);

	if (pSrcList->pWrite != NULL) {
		if (pDstList->pWrite == NULL) {
			/*
			 *	The destination list is empty.
			 */

			pDstList->pRead = pSrcList->pRead;

		} else {
			pDstList->pWrite->pNext = pSrcList->pRead;
		}

		pDstList->pWrite = pSrcList->pWrite;
		pDstList->pPool = pSrcList->pPool;

		pSrcList->pRead = NULL;
		pSrcList->pWrite = NULL;
	}

	ENABLEINTR(&isrState);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_freeBlocks
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Free all the blocks on the supplied list.
*
*	\param		pList	Pointer to the list.
*
*   \return		-
*
*	\details	Frees all the block on the supplied list.
*	
*	\note		May be called from an ISR but the function might take a while
*				to execute as it has to loop through the supplied list.
*
*******************************************************************************/

PUBLIC void bmm_freeBlocks(
	bmm_BlockList *				pList
) {
	bmm_Block *					pBlock;

	deb_assert(pList != NULL);
	deb_assert(pList->pPool != NULL);

	FOREVER {
		pBlock = bmm_getFirst(pList);
		if (pBlock == NULL) {
			break;
		}

		bmm_freeBlock(pList->pPool, pBlock);
	}
}
