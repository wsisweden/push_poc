/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file
*
*	\ingroup	HW
*
*	\brief		Assert handling for STM32F3xx LL and HAL code.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 4043 $ \n
*				\$Date: 2020-01-08 12:43:25 +0200 (ke, 08 tammi 2020) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "deb.h"
#include "hw.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	assert_failed
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reports the name of the source file and the source line number
*				where the assert_param error has occurred.
*
*	\param		file		pointer to the source file name.
*	\param		line		assert_param error line source number.
*
*	\details	
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

void assert_failed(
	char const_P *			file,
	Uint32					line
) {
	/*
	 *	The T-Plat.E deb_assert macro uses a constant declared by
	 *	SYS_DEFINE_FILE_NAME for the filename to prevent a new string literal
	 *	from being created for each assert.
	 *
	 *	This is not possible in ST LL and HAL code without adding 
	 *	SYS_DEFINE_FILE_NAME to each .c file.
	 */

#ifndef NDEBUG
	deb__assertInt(file, (Uint16) __LINE__);
#endif
}
