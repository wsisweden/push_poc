/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	HW_S4F4
*
*	\brief		The common part of the HAL initialization.
*
*	\details
*
*	\note
*
*	\version	
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	HW_S4F4		STM32F4xx
*
*	\ingroup	HW
*
*	\brief		HW functions for STM32F4xx based MCUs.
*
********************************************************************************
*
*	\details
*
*******************************************************************************/

/******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"

#include "stm32f4xx_hal.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define IDCODE_DEVID_MASK    0x00000FFFU

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/* ------------ RCC registers bit address in the alias region ----------- */

#define SYSCFG_OFFSET             (SYSCFG_BASE - PERIPH_BASE)
/* ---  MEMRMP Register ---*/ 
/* Alias word address of UFB_MODE bit */ 
#define MEMRMP_OFFSET             SYSCFG_OFFSET 
#define UFB_MODE_BIT_NUMBER       SYSCFG_MEMRMP_UFB_MODE_Pos
#define UFB_MODE_BB               (uint32_t)(PERIPH_BB_BASE + (MEMRMP_OFFSET * 32U) + (UFB_MODE_BIT_NUMBER * 4U)) 

/* ---  CMPCR Register ---*/ 
/* Alias word address of CMP_PD bit */ 
#define CMPCR_OFFSET              (SYSCFG_OFFSET + 0x20U) 
#define CMP_PD_BIT_NUMBER         SYSCFG_CMPCR_CMP_PD_Pos
#define CMPCR_CMP_PD_BB           (uint32_t)(PERIPH_BB_BASE + (CMPCR_OFFSET * 32U) + (CMP_PD_BIT_NUMBER * 4U))

/* ---  MCHDLYCR Register ---*/ 
/* Alias word address of BSCKSEL bit */ 
#define MCHDLYCR_OFFSET            (SYSCFG_OFFSET + 0x30U) 
#define BSCKSEL_BIT_NUMBER         SYSCFG_MCHDLYCR_BSCKSEL_Pos
#define MCHDLYCR_BSCKSEL_BB        (uint32_t)(PERIPH_BB_BASE + (MCHDLYCR_OFFSET * 32U) + (BSCKSEL_BIT_NUMBER * 4U))

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

__IO uint32_t uwTick;
uint32_t uwTickPrio   = (1UL << __NVIC_PRIO_BITS); /* Invalid PRIO */
HAL_TickFreqTypeDef uwTickFreq = HAL_TICK_FREQ_DEFAULT;  /* 1KHz */

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_Init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize ST HAL library.
*
*	\return		HAL status code.
*
*	\details	This function is used to initialize the HAL Library. It must be
*				called before a to call any other HAL function.
*
*				Th function does the following things:
*				- Configure the Flash prefetch, instruction and Data caches.
*				- Configures the SysTick to generate an interrupt each 
*				  millisecond.
*
*	\note		
*
*******************************************************************************/

HAL_StatusTypeDef HAL_Init(
	void
) {
	/* Configure Flash prefetch, Instruction cache, Data cache */ 
#if (INSTRUCTION_CACHE_ENABLE != 0U)
	__HAL_FLASH_INSTRUCTION_CACHE_ENABLE();
#endif /* INSTRUCTION_CACHE_ENABLE */

#if (DATA_CACHE_ENABLE != 0U)
	__HAL_FLASH_DATA_CACHE_ENABLE();
#endif /* DATA_CACHE_ENABLE */

#if (PREFETCH_ENABLE != 0U)
	__HAL_FLASH_PREFETCH_BUFFER_ENABLE();
#endif /* PREFETCH_ENABLE */

	/* Use systick as time base source and configure 1ms tick (default clock after Reset is HSI) */
	HAL_InitTick(TICK_INT_PRIORITY);

	/* Init the low level hardware */
	HAL_MspInit();

	/* Return function status */
	return HAL_OK;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DeInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This function de-Initializes common part of the HAL.
*
*	\return		HAL status code.
*
*	\details	
*
*	\note		
*
*******************************************************************************/

HAL_StatusTypeDef HAL_DeInit(
	void
) {
	/* Reset of all peripherals */
	__HAL_RCC_APB1_FORCE_RESET();
	__HAL_RCC_APB1_RELEASE_RESET();

	__HAL_RCC_APB2_FORCE_RESET();
	__HAL_RCC_APB2_RELEASE_RESET();

	__HAL_RCC_AHB1_FORCE_RESET();
	__HAL_RCC_AHB1_RELEASE_RESET();

	__HAL_RCC_AHB2_FORCE_RESET();
	__HAL_RCC_AHB2_RELEASE_RESET();

	__HAL_RCC_AHB3_FORCE_RESET();
	__HAL_RCC_AHB3_RELEASE_RESET();

	/* De-Init the low level hardware */
	HAL_MspDeInit();

	/* Return function status */
	return HAL_OK;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_MspInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the MSP.
*
*	\details	
*
*	\note		
*
*******************************************************************************/

__weak void HAL_MspInit(
	void
) {
	/* 
	 *	NOTE: This function should not be modified, when the callback is needed,
	 *	the HAL_MspInit could be implemented in the user file.
	 */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_MspDeInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		DeInitializes the MSP.
*
*	\details	
*
*	\note		
*
*******************************************************************************/

__weak void HAL_MspDeInit(
	void
) {
	/* NOTE : This function should not be modified, when the callback is needed,
	the HAL_MspDeInit could be implemented in the user file
	*/ 
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_InitTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This function configures the source of the time base.
*
*	\details	The time source is configured to have 1ms time base.
*
*	\note 		This function is called automatically at the beginning of
*				program after reset by HAL_Init() or at any time when clock is
*				reconfigured by HAL_RCC_ClockConfig().
*
*******************************************************************************/

HAL_StatusTypeDef HAL_InitTick(
	uint32_t 				TickPriority
) {
	/*
	 * 	Configure the SysTick to have interrupt in 1ms time basis. This does
	 * 	not yet enable the SysTick timer. The SysTick is enabled later when
	 * 	the RTOS is started.
	 */

	SysTick->CTRL = 0;
	SysTick->LOAD = ((SystemCoreClock) / 1000UL) - 1;
	SysTick->CTRL = SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_CLKSOURCE_Msk;

	uwTickFreq = HAL_TICK_FREQ_1KHZ;
	
	/*
	 *  Configure the SysTick IRQ priority.
	 */

	NVIC_SetPriority(SysTick_IRQn, TickPriority);
	uwTickPrio = TickPriority;

	return HAL_OK;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_IncTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This function is called to increment a global variable "uwTick"
*				used as application time base.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_IncTick(
	void
) {
	uwTick += uwTickFreq;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Provides a tick value in millisecond.
*
*	\return		Current tick value.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

uint32_t HAL_GetTick(
	void
) {
	return uwTick;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This function returns a tick priority.
*
*	\return		Current tick interrupt priority.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

uint32_t HAL_GetTickPrio(
	void
) {
	return uwTickPrio;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetTickFreq
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Return tick frequency.
*
*	\return		The tick period in milliseconds.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

HAL_TickFreqTypeDef HAL_GetTickFreq(
	void
) {
	return uwTickFreq;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_Delay
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Wait for the specified time.
*
*	\param 		Delay 		Specifies the delay time length, in milliseconds.
*
*	\return		
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_Delay(
	uint32_t      			Delay
) {
	if (osa_isRunning == TRUE) {
		/*
		 *	Giving all execution time to other tasks if the RTOS is running.
		 */

		osa_taskWait(osa_msToTicks(Delay));

	} else {
		volatile Uint32 cnt;

		/*
		 *	The RTOS is not running yet. The SysTick timer has not been started
		 *	at this point.
		 *
		 *	Causing the function to loop for the desired amount of milliseconds.
		 *	This kind of delay will not be accurate.
		 *	
		 *	The divider value 8000 seems to produce about correct results. This 
		 *	would mean that one iteration takes 8 clock cycles. At least the IAR
		 *	compiler produces an identical loop regardless of the used 
		 *	optimization level. The volatile variable cannot be optimized.
		 *
		 * 	Using 7000 instead of 8000 to make sure the function takes at least
		 * 	as long as it should.
		 */

		cnt = Delay * (SystemCoreClock / 7000U);
		while (cnt--) {

		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_SuspendTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Suspend Tick increment.	
*
*	\details	SysTick timer is the source of time base. It is used to generate
*				interrupts at regular time intervals. Once HAL_SuspendTick()
*				is called, the SysTick interrupt will be disabled and so Tick
*				increment is suspended.
*
*	\note 		
*
*******************************************************************************/

void HAL_SuspendTick(
	void
) {
	/* Disable SysTick Interrupt */
	SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_ResumeTick
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Resume Tick increment.
*
*	\details	SysTick timer is the source of time base. It is used to generate
*				interrupts at regular time intervals. Once HAL_ResumeTick()
*				is called, the SysTick interrupt will be enabled and so Tick
*				increment is resumed.
*
*	\note 		
*
*******************************************************************************/

void HAL_ResumeTick(
	void
) {
	/* Enable SysTick Interrupt */
	SysTick->CTRL  |= SysTick_CTRL_TICKINT_Msk;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetREVID
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the device revision identifier.
*	
*	\return		Device revision identifier.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

uint32_t HAL_GetREVID(
	void
) {
	return((DBGMCU->IDCODE) >> 16U);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetDEVID
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the device identifier.
*	
*	\return		Device identifier
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

uint32_t HAL_GetDEVID(
	void
) {
	return((DBGMCU->IDCODE) & IDCODE_DEVID_MASK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DBGMCU_EnableDBGSleepMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enable the Debug Module during SLEEP mode.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_DBGMCU_EnableDBGSleepMode(
	void
) {
	SET_BIT(DBGMCU->CR, DBGMCU_CR_DBG_SLEEP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DBGMCU_DisableDBGSleepMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Disable the Debug Module during SLEEP mode.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_DBGMCU_DisableDBGSleepMode(
	void
) {
	CLEAR_BIT(DBGMCU->CR, DBGMCU_CR_DBG_SLEEP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DBGMCU_EnableDBGStopMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enable the Debug Module during STOP mode.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_DBGMCU_EnableDBGStopMode(
	void
) {
	SET_BIT(DBGMCU->CR, DBGMCU_CR_DBG_STOP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DBGMCU_DisableDBGStopMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Disable the Debug Module during STOP mode.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_DBGMCU_DisableDBGStopMode(
	void
) {
	CLEAR_BIT(DBGMCU->CR, DBGMCU_CR_DBG_STOP);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DBGMCU_EnableDBGStandbyMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enable the Debug Module during STANDBY mode.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_DBGMCU_EnableDBGStandbyMode(
	void
) {
	SET_BIT(DBGMCU->CR, DBGMCU_CR_DBG_STANDBY);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DBGMCU_DisableDBGStandbyMode
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Disable the Debug Module during STANDBY mode.
*
*	\details	
*
*	\note 		
*
*******************************************************************************/

void HAL_DBGMCU_DisableDBGStandbyMode(
	void
) {
	CLEAR_BIT(DBGMCU->CR, DBGMCU_CR_DBG_STANDBY);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_EnableCompensationCell
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enables the I/O Compensation Cell.
*
*	\details	The I/O compensation cell can be used only when the device
*				supply voltage ranges from 2.4 to 3.6 V. 
*
*	\note 		
*
*******************************************************************************/

void HAL_EnableCompensationCell(
	void
) {
	*(__IO uint32_t *)CMPCR_CMP_PD_BB = (uint32_t)ST_ENABLE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_DisableCompensationCell
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Power-down the I/O Compensation Cell.
*
*	\details	The I/O compensation cell can be used only when the device
*				supply voltage ranges from 2.4 to 3.6 V.  
*
*	\note 		
*
*******************************************************************************/

void HAL_DisableCompensationCell(
	void
) {
	*(__IO uint32_t *)CMPCR_CMP_PD_BB = (uint32_t)ST_DISABLE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetUIDw0
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns first word of the unique device identifier (UID based on
*				96 bits).
*
*	\return		Device identifier.
*
*	\details	 
*
*	\note 		
*
*******************************************************************************/

uint32_t HAL_GetUIDw0(
	void
) {
	return (READ_REG(*((uint32_t *)UID_BASE)));
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetUIDw0
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns second word of the unique device identifier (UID based
*				on 96 bits).
*
*	\return		Device identifier.
*
*	\details	 
*
*	\note 		
*
*******************************************************************************/

uint32_t HAL_GetUIDw1(
	void
) {
	return (READ_REG(*((uint32_t *)(UID_BASE + 4U))));
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	HAL_GetUIDw2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns third word of the unique device identifier (UID based
*				on 96 bits).
*
*	\return		Device identifier.
*
*	\details	 
*
*	\note 		
*
*******************************************************************************/

uint32_t HAL_GetUIDw2(
	void
) {
	return (READ_REG(*((uint32_t *)(UID_BASE + 8U))));
}

#if defined(STM32F427xx) || defined(STM32F437xx) || defined(STM32F429xx)|| defined(STM32F439xx) ||\
defined(STM32F469xx) || defined(STM32F479xx)
/**
* @brief  Enables the Internal FLASH Bank Swapping.
*   
* @note   This function can be used only for STM32F42xxx/43xxx/469xx/479xx devices. 
*
* @note   Flash Bank2 mapped at 0x08000000 (and aliased @0x00000000) 
*         and Flash Bank1 mapped at 0x08100000 (and aliased at 0x00100000)   
*
* @retval None
*/
void HAL_EnableMemorySwappingBank(
	void
) {
	*(__IO uint32_t *)UFB_MODE_BB = (uint32_t)ST_ENABLE;
}

/**
* @brief  Disables the Internal FLASH Bank Swapping.
*   
* @note   This function can be used only for STM32F42xxx/43xxx/469xx/479xx devices. 
*
* @note   The default state : Flash Bank1 mapped at 0x08000000 (and aliased @0x00000000) 
*         and Flash Bank2 mapped at 0x08100000 (and aliased at 0x00100000) 
*           
* @retval None
*/
void HAL_DisableMemorySwappingBank(
	void
) {
	*(__IO uint32_t *)UFB_MODE_BB = (uint32_t)ST_DISABLE;
}
#endif /* STM32F427xx || STM32F437xx || STM32F429xx || STM32F439xx || STM32F469xx || STM32F479xx */
