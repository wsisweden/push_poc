/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SECTORW32.C
*
*	\ingroup	SECTORW32
*
*	\brief		Flash memory driver simulation for windows. 
*
*	\details	
*
*	\note		
*				
*	\version	\$Rev: 4423 $ \n
*				\$Date: 2021-01-13 17:08:06 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	SECTORW32 SECTORW32
*
*	\brief		Flash memory driver simulation for windows.
*
********************************************************************************
*
*	\details	-
*				
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tools.h"
#include "deb.h"
#include "sys.h"
#include "osa.h"
#include "math_.h"
#include "mem.h"
#include "sectorw32.h"

#include "../init.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type used for instance data of the driver.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	sectorw32_Init const_P * pInit;		/**< Ptr to initialization values	*/
	Uint32					fileSize;	/**< File size in bytes				*/
	osa_SemaType			taskSema;	/**< File locking semaphore			*/
	FILE *					pFile;		/**< File							*/
} sectorw32__Inst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void			sectorw32__put(void *, memdrv_Alloc *);
PRIVATE void			sectorw32__get(void *, memdrv_Alloc *);
PRIVATE void			sectorw32__busy(void *, memdrv_Alloc *);
PRIVATE void			sectorw32__doOp(void *,memdrv_Alloc *);
PRIVATE Uint8			sectorw32__eraseSector(sectorw32__Inst *,memdrv_Alloc *);
PRIVATE Uint8			sectorw32__getMemInfo(sectorw32__Inst *,memdrv_Alloc *);
PRIVATE Uint8			sectorw32__eraseChip(sectorw32__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Public interface.
 */

PUBLIC memdrv_Interface		sectorw32_fnIf = {
	sectorw32__put,
	sectorw32__get,
	sectorw32__busy,
	sectorw32__doOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/******************************************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocates a instance and initializes it.
*
*	\param		iid		Function block instance id.
*	\param		pArgs	Pointer to initialization parameters.
*
*	\return		Pointer to allocated instance.
*
*	\details	
*
*	\note			
*
*******************************************************************************/

PUBLIC void * sectorw32_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	return sectorw32_initialize((sectorw32_Init const_P *) pArgs);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32_initialize
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the driver.
*
*	\param		pInit	Pointer to initialization parameters.
*
*	\return		Pointer to instance.
*
*	\details	
*
*	\note			
*
*******************************************************************************/

PUBLIC void * sectorw32_initialize(
	sectorw32_Init const_P * pInit
) {
	FILE *					pFile;
	errno_t					err;
	sectorw32__Inst *		pInst;

	pInst = (sectorw32__Inst *) mem_reserve(&mem_normal, sizeof(sectorw32__Inst));

	pInst->pInit = pInit;

	/*
	 *	Try to open the file. Opening will succeed if the file exists.
	 */

	err = fopen_s(&pFile, pInst->pInit->file, "r");

	// is file needed to be constructed as sectors, or just one big chunk
	if(pInst->pInit->sectorCount > 0) {
		flash_SectorIndexSizeType sectorIdx;

		/*
		 * Calculate file size.
		 */

		if (pInst->pInit->sectorList == NULL) {
			/*
			 * Same sector size for all sectors.
			 */

			deb_assert(pInst->pInit->sectorSize != 0);

			pInst->fileSize =
				pInst->pInit->sectorCount * pInst->pInit->sectorSize;

		} else {
			/*
			 * Variable sector size. Calculate the sum of all sectors sizes.
			 */

			deb_assert(pInst->pInit->sectorCount <= Uint8_MAX);
			pInst->fileSize = 0;
			sectorIdx = (flash_SectorIndexSizeType) pInst->pInit->sectorCount - 1;
			do {
				pInst->fileSize += 
					1 << pInst->pInit->sectorList[sectorIdx].sectorSizeFactor;
			} while (sectorIdx--);
		}

		if (!pFile) {
			/* 
			 * The file could not be opened. The file probably does not exist.
			 * Create the file and initialize the contents as an empty flash
			 * memory.
			 */
			sectorw32__eraseChip(pInst);
		}
	}

	if (pFile != NULL) {
		fclose(pFile);
	}

	/*
	 * Open file once and save handle to instance if init parameter
	 * has requested that file should not be opened and closed for each operation.
	 */
	if (pInst->pInit->noOpenCloseFile) {
		fopen_s(&pFile, pInst->pInit->file, "r+b");
		pInst->pFile = pFile;
	}

	osa_newSemaSet(&pInst->taskSema);

	return pInst;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32_put
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write data to memory.	
*
*	\param		pUtil		Pointer to SECTORW32 instance.
*	\param		pAlloc		Pointer to MEMDRV request.
*
*	\return		-	
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC void sectorw32__put(
	void *					pUtil,
	memdrv_Alloc *			pAlloc
) {
	sectorw32__Inst *		pInst = (sectorw32__Inst *)pUtil;
	FILE *					pFile;
	errno_t					err;
	int						tryCount;
	int						offset;
	BYTE *					pBuffer;

	deb_assert(pAlloc->count != 0);
	deb_assert(pAlloc->address + pAlloc->count <= pInst->fileSize);

	osa_semaGet(&pInst->taskSema);

	if (pInst->pInit->noOpenCloseFile) {

		pFile = pInst->pFile;

		/*
		 *	Read the current data into a buffer and perform a AND with the new
		 *	data. This will simulate how a flash memory works.
		 */

		pBuffer = (BYTE *) malloc(pAlloc->count);
		deb_assert(pBuffer != NULL);
		
		fseek(pFile, pAlloc->address, SEEK_SET);
		fread(pBuffer, 1, pAlloc->count, pFile);

		offset = pAlloc->count;
		do {
			offset--;
			pBuffer[offset] &= pAlloc->pData[offset];
		} while (offset);
		
		fseek(pFile, pAlloc->address, SEEK_SET);
		fwrite(pBuffer, 1, pAlloc->count, pFile);

		free(pBuffer);

		/*
		 *	Write done. close the file and call the callback function.
		 */
		pAlloc->count = 0;

		fflush(pFile);

	} else {
	
		tryCount = 100;
		do {
			err = fopen_s(&pFile, pInst->pInit->file, "r+b");
			if (pFile != NULL && err == 0) {
				break;
			}
			osa_yield();
			tryCount--;
		} while (tryCount != 0);
		deb_assert(pFile && err == 0);
	
		/*
		 *	Read the current data into a buffer and perform a AND with the new
		 *	data. This will simulate how a flash memory works.
		 */

		pBuffer = (BYTE *) malloc(pAlloc->count);
		deb_assert(pBuffer != NULL);
		
		fseek(pFile, pAlloc->address, SEEK_SET);
		fread(pBuffer, 1, pAlloc->count, pFile);

		offset = pAlloc->count;
		do {
			offset--;
			pBuffer[offset] &= pAlloc->pData[offset];
		} while (offset);
		
		fseek(pFile, pAlloc->address, SEEK_SET);
		fwrite(pBuffer, 1, pAlloc->count, pFile);

		free(pBuffer);

		/*
		 *	Write done. close the file and call the callback function.
		 */

		if(err == 0) {
			pAlloc->count = 0;
		}

		fflush(pFile);
		fclose(pFile);
	}

	osa_semaSet(&pInst->taskSema);

	pAlloc->fnDone(pAlloc);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32_get
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read data from memory.
*
*	\param		pUtil		Pointer to SECTORW32 instance.
*	\param		pAlloc		Pointer to MEMDRV request.
*
*	\return		-
*
*	\details	
*
*	\note				
*
*******************************************************************************/

PUBLIC void sectorw32__get(
	void *					pUtil,
	memdrv_Alloc *			pAlloc
) {
	sectorw32__Inst *		pInst = (sectorw32__Inst *)pUtil;
	FILE *					pFile;
	errno_t					err;
	int						tryCount;

	deb_assert(pAlloc->address + pAlloc->count <= pInst->fileSize);

	osa_semaGet(&pInst->taskSema);

	if (pInst->pInit->noOpenCloseFile) {

		pFile = pInst->pFile;

		fseek(pFile, pAlloc->address, SEEK_SET);
		fread(pAlloc->pData, 1, pAlloc->count, pFile);

		pAlloc->count = 0;

	} else {

		tryCount = 100;
		do {
			err = fopen_s(&pFile, pInst->pInit->file, "r+b");
			if (pFile != NULL && err == 0) {
				break;
			}
			osa_yield();
			tryCount--;
		} while (tryCount != 0);
		deb_assert(pFile && err == 0);

		fseek(pFile, pAlloc->address, SEEK_SET);
		fread(pAlloc->pData, 1, pAlloc->count, pFile);

		if(err == 0) {
			pAlloc->count = 0;
		}

		fclose(pFile);
	}

	osa_semaSet(&pInst->taskSema);

	pAlloc->fnDone(pAlloc);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32_busy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if the memory is busy.
*
*	\param		pUtil		Pointer to SECTORW32 instance.
*	\param		pAlloc		Pointer to MEMDRV request.
*
*	\return		-
*
*	\details	
*
*	\note					
*
*******************************************************************************/

PUBLIC void sectorw32__busy(
	void *					pUtil,
	memdrv_Alloc *			pAlloc
) {
	pAlloc->count = MEMDRV_FREE;

	pAlloc->fnDone(pAlloc);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32_doOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle MEMDRV request.
*
*	\param		pUtil		Pointer to SECTORW32 instance.
*	\param		pAlloc		Pointer to MEMDRV request.
*
*	\return		-
*
*	\details	This function will handle all operations except put, get and
*				busy.
*
*	\note					
*
*******************************************************************************/

PRIVATE void sectorw32__doOp(
	void *					pUtil,
	memdrv_Alloc *			pAlloc
) {
	sectorw32__Inst *		pInst;
	memdrv_DoneFn			pDoneFn;

	pInst = (sectorw32__Inst *) pUtil;

	pDoneFn = pAlloc->fnDone;

	switch(pAlloc->op) {
		case MEMDRV_OP_ERASE_SECTOR:
			pAlloc->count = sectorw32__eraseSector(pInst, pAlloc);
			break;

		case MEMDRV_OP_ERASE_CHIP:
			pAlloc->count = sectorw32__eraseChip(pInst);
			break;

		case MEMDRV_OP_ERASE_BLOCK:
			/*
			 * todo:	Some code might rely on these operations to actually 
			 *			erase the memory. They should be implemented if the
			 *			application level code uses them.
			 */
			deb_assert(FALSE);
			pAlloc->count = MEMDRV_NOT_SUPPORTED;
			break;

		case MEMDRV_OP_SLEEP:
		case MEMDRV_OP_WAKEUP:
			/*
			 * Sleep doesn't make much difference in windows. This could be 
			 * simulated better by locking the driver while it is in sleep mode.
			 */
			pAlloc->count = MEMDRV_NOT_SUPPORTED;
			break;

		case MEMDRV_OP_GET_INFO:
			pAlloc->count = sectorw32__getMemInfo(pInst, pAlloc);
			break;

		case MEMDRV_OP_INIT_MEM:
			/*
			 *	No warm initialization is needed in sectorw32.
			 */
			pAlloc->count = MEMDRV_OK;
			break;

		case MEMDRV_OP_MEDIA_PRESENT:
			pAlloc->count = MEMDRV_MEDIA_IS_PRESENT;
			pDoneFn = NULL; /* No callback for this operation */
			break;

		default:
			/*
			 * The operation is not supported by sectorw32.
			 */
			deb_assert(FALSE);
			pAlloc->count = MEMDRV_NOT_SUPPORTED;
			break;
	}

	if (pDoneFn != NULL) {
		/*
		 *	TODO:	The callback function should be called from another task.
		 */
		pAlloc->fnDone(pAlloc);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32__eraseSector
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Erase a sector (Fill it with 0xFF).
*
*	\param		pInst		Pointer to sectorw32 instance.
*	\param		pAlloc		Pointer to MEMDRV request.
*
*	\return		MEMDRV_OK if successful.
*
*	\details	
*
*	\note					
*
*******************************************************************************/

PRIVATE Uint8 sectorw32__eraseSector(
	sectorw32__Inst *		pInst,
	memdrv_Alloc *			pAlloc
) {
	FILE *					pFile;
	errno_t					err;
	char *					pBuffer;
	Uint8					retVal;

	retVal = MEMDRV_ERROR;

	/*
	 * Find the sector with the provided address and get its size.
	 */

	if (pInst->pInit->sectorList == NULL) {
		deb_assert(pInst->pInit->sectorSize != 0);
		deb_assert((pAlloc->address % pInst->pInit->sectorSize) == 0);
		pAlloc->count = pInst->pInit->sectorSize;

	} else {
		flash_SectorIndexSizeType ii;

		pAlloc->count = 0;
		for(ii = 0; ii <= pInst->pInit->sectorCount - 1; ii++) {
			Uint32 sectorAddr;

			sectorAddr =
				pInst->pInit->sectorList[ii].sectorOffset
				* (*pInst->pInit->sectorSmallestSize)
				* 1024;

			if(pAlloc->address == sectorAddr) {
				pAlloc->count = 
					(memdrv_SizeType) (1 << pInst->pInit->sectorList[ii].sectorSizeFactor);
			}
		}
		deb_assert(pAlloc->count != 0);
	}

	deb_assert((Uint32)(pAlloc->address + pAlloc->count) <= pInst->fileSize);			

	/*
	 * Only one task at a time should be able to modify the file.
	 */

	osa_semaGet(&pInst->taskSema);

	if (pInst->pInit->noOpenCloseFile) {

		pFile = pInst->pFile;

		pBuffer = malloc(pAlloc->count);
		memset(pBuffer, 0xFF, pAlloc->count);

		fseek(pFile, pAlloc->address, SEEK_SET);
		fwrite(pBuffer, 1, pAlloc->count, pFile);

		retVal = MEMDRV_OK;
		
		free(pBuffer);

		fflush(pFile);

	} else {

		err = fopen_s(&pFile, pInst->pInit->file, "r+b");

		deb_assert(pFile && err == 0);

		pBuffer = malloc(pAlloc->count);
		memset(pBuffer, 0xFF, pAlloc->count);

		fseek(pFile, pAlloc->address, SEEK_SET);
		fwrite(pBuffer, 1, pAlloc->count, pFile);

		if(err == 0) {
			retVal = MEMDRV_OK;
		}

		fflush(pFile);
		fclose(pFile);
		free(pBuffer);

	}

	osa_semaSet(&pInst->taskSema);

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32__eraseChip
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Erase the entire memory (Fill it with 0xFF).
*
*	\param		pInst		Pointer to sectorw32 instance.
*
*	\return		MEMDRV_OK if successful.
*
*	\details	
*
*	\note					
*
*******************************************************************************/

PRIVATE Uint8 sectorw32__eraseChip(
	sectorw32__Inst *		pInst
) {
	FILE *					pFile;
	errno_t					err;
	char *					pBuffer;
	Uint32					written;

	pBuffer = (char *) malloc(512);
	memset(pBuffer, 0xFF, 512);

	osa_semaGet(&pInst->taskSema);
	
	err = fopen_s(&pFile, pInst->pInit->file, "a+");
	deb_assert(pFile && err == 0);

	/*
	 * Fill file with 0xFF in chunks of 512 bytes.
	 */

	written = 0;
	do {
		fwrite(pBuffer, 1, 512, pFile);
		written += 512;
	} while (written < pInst->fileSize);
	
	fclose(pFile);

	osa_semaSet(&pInst->taskSema);

	free(pBuffer); 

	return(MEMDRV_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	sectorw32__getMemInfo
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get memory info.
*
*	\param		pInst		Pointer to sectorw32 instance.
*	\param		pAlloc		Pointer to MEMDRV request.
*
*	\return		MEMDRV_OK if successful.
*
*	\details	
*
*	\note					
*
*******************************************************************************/

PRIVATE Uint8 sectorw32__getMemInfo(
	sectorw32__Inst *		pInst,
	memdrv_Alloc *			pAlloc
) {
	memdrv_MemInfo *		pMemInfo;

	pMemInfo = (memdrv_MemInfo *) pAlloc->pData;

	if (pInst->pInit->sectorList == NULL) {
		pMemInfo->sectorSize = pInst->pInit->sectorSize;

	} else {
		/*
		 *	Assume all sectors have the same size. This could also use the
		 *	size of the largest sector.
		 */
		pMemInfo->sectorSize = 1 << pInst->pInit->sectorList[0].sectorSizeFactor;
	}

	pMemInfo->sectorCount = pInst->pInit->sectorCount;
	pMemInfo->flags = MEMDRV_FLG_ERASE;
	
	return(MEMDRV_OK);
}