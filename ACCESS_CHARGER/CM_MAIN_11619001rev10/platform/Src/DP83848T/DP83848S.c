/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DP83848T
*
*	\brief		DP83848T synchronous interface handling.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 2847 $ \n
*				\$Date: 2015-07-28 13:36:31 +0300 (ti, 28 heinä 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* Platform lib */
#include "tools.h"
#include "deb.h"
#include "phyif.h"

/* Testrig lib */
#include "DP83848T.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE phyif_PhyInst * dp83848t__syncInit(phyif_Init const_P *	pInit);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * This is public interface constant that can be used to access the driver
 * functions. A pointer to this constant should be provided to the FB that
 * will use this driver.
 *
 * This is an synchronous interface that will block the calling task until the
 * operation is done. It accesses the SME through the asynchronous AMIF 
 * interface.
 */

PUBLIC phyif_Interface const_P dp83848t_syncIf = {
	&dp83848t__syncInit,
	&phyif_asyncWarmInit,
	&phyif_asyncPoll,
	&phyif_asyncDoOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* File name for assert */
SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__syncInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the synchronous PHY driver instance.
*
*	\param		pInit	Pointer to the initialization values for the PHY driver.
*
*	\returns	A pointer to the new PHY driver instance.
*
*	\details	Should be called during FB initialization before OSA is
* 				running.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE phyif_PhyInst * dp83848t__syncInit(
	phyif_Init const_P *	pInit
) {
	void *					pInst;
	phyif_PhyInst *			pSyncInst;

	pInst = dp83848t_if.pInitFn(&pInit->base);
	deb_assert(pInst);

	pSyncInst = phyif_asyncInit(&dp83848t_if, pInst);

	return(pSyncInst);
}
