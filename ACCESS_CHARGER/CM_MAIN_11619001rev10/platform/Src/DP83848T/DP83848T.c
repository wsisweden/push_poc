/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DP83848T
*
*	\brief		DP83848T main file.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 2874 $ \n
*				\$Date: 2015-10-13 19:05:59 +0300 (ti, 13 loka 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	DP83848T DP83848T
*
*	\brief		DP83848T PHY driver.
*
********************************************************************************
*
*	\details	The driver implements the APIF interface. It also contains an
*				optional wrapper which implements the PHYIF interface.
*				It accesses the MDIO bus through the AMIF interface.
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* Platform lib */
#include "tools.h"
#include "mem.h"
#include "deb.h"
#include "amif.h"
#include "apif.h"

/* Testrig lib */
#include "DP83848T.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * Value that should be found in the PHYIDR1 register.
 */

#define DP83848T__IDR1_VALUE 0x2000

/**
 * Value that should be found in the PHYIDR2 register.
 */

#define DP83848T__IDR2_VALUE 0x5C90

/**
 * Warm initialization steps.
 */

enum dp83848t__warmInitSteps {			/*''''''''''''''''''''''''''''''''''*/
	DP83848T__WI_READID1,				/**< Read ID1 register.				*/
	DP83848T__WI_READID2,				/**< Read ID2 register.				*/
	DP83848T__WI_WRITESPEED				/**< Write link speed settings.		*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/**
 *	\brief	Sets the state handler pointer and calls the handler.
 *
 *	\param	pInst_		Pointer to FB instance.
 *	\param	pStateFn_	Pointer to the state handler function.
 */
 
#define dp83848t__enterState(pInst_, pStateFn_) do {				\
	(pInst_)->pStateFn = (pStateFn_);								\
	(pStateFn_)((pInst_), DP83848T__E_ENTRY);						\
} while (FALSE)

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

struct dp83848t__inst;
typedef struct dp83848t__inst dp83848t__Inst;

/**
 * State machine event code.
 */

typedef enum {							/*''''''''''''''''''''''''''''''''''*/
	DP83848T__E_ENTRY,					/**< State entry event.				*/
	DP83848T__E_NEW_REQ,				/**< New PHY request received.		*/
	DP83848T__E_MDIO_DONE				/**< MDIO callback function called.	*/
} dp83848t__Event;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	\brief	Type of state machine state handler functions.
 *
 *	\param	pInst	Pointer to PHY driver instance.
 *	\param	event	Event code identifying the reason why the state machine has
 *					been activated.
 */

typedef void dp83848t__StateFn(dp83848t__Inst * pInst,dp83848t__Event event);

/**
 * Type for DP83848T instance data.
 */

struct dp83848t__inst {					/*''''''''''''''''''''''''''''' RAM */
	Uint8					step;		/**< Current step.					*/
	Uint8					retryCount;	/**< Number of retries done.		*/
	Uint8					status;		/**< PHY status.					*/
	Uint8					options;	/**< Currently active option.		*/
	gai_RetCode				mdioRet;	/**< Result of previous MDIO op.	*/		
	dp83848t__StateFn *		pStateFn;	/**< Pointer to state handler.		*/
	apif_Init const_P *		pInit;		/**< Pointer to init parameters.	*/
	apif_Req *				pReq;		/**< Pointer to current request.	*/
	amif_Req 				mdioReq;	/**< The MDIO request.				*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE apif_PhyInst * dp83848t__init(apif_Init const_P * pInit);

PRIVATE gai_RetCode dp83848t__access(void * pDrvInst,gai_Req * pReq);
PRIVATE void		dp83848t__mdioDone(gai_Req * pReq,gai_RetCode retCode);

PRIVATE void		dp83848t__idleState(dp83848t__Inst *,dp83848t__Event);
PRIVATE void		dp83848t__warmInitState(dp83848t__Inst *,dp83848t__Event);
PRIVATE void		dp83848t__pollState(dp83848t__Inst *,dp83848t__Event);
PRIVATE void		dp83848t__readState(dp83848t__Inst *,dp83848t__Event);
PRIVATE void		dp83848t__writeState(dp83848t__Inst *,dp83848t__Event);

PRIVATE	void		dp83848t__readPhy(dp83848t__Inst *,Uint8);
PRIVATE void		dp83848t__writePhy(dp83848t__Inst *,Uint8,Uint16);
PRIVATE void		dp83848t__reqDone(dp83848t__Inst *,apif_RetCode);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * This is the public interface constant that can be used to access the driver
 * functions. A pointer to this constant should be provided to the FB that
 * will use this driver.
 */

PUBLIC apif_Interface const_P dp83848t_if = {
	&dp83848t__access,
	&dp83848t__init
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* File name for assert */
SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the function block.
*
*	\param		pInit	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE apif_PhyInst * dp83848t__init(
	apif_Init const_P *		pInit
) {
	dp83848t__Inst *		pInst;
	
	pInst = (dp83848t__Inst *) mem_reserve(&mem_normal, sizeof(dp83848t__Inst));
	deb_assert(pInst);

	/*
	 *	The DP83848T driver accesses the SME through the asynchronous AMIF
	 *	interface. It does not currently support the MIIMIF interface.
	 *
	 *	Supporting the MIIMIF interface would require a completely separate 
	 *	version of the driver. That version would only implement the PHYIF
	 *	interface.
	 */
	deb_assert(pInit->pAmif != NULL);
	deb_assert(pInit->pAmif->pAccessFn != NULL);

	/*
	 *	Auto address is currently not supported by this driver.
	 */
	deb_assert(pInit->phyAddr != APIF_ADDR_AUTO);

	pInst->pStateFn = &dp83848t__idleState;
	pInst->status = 0;
	pInst->pInit = pInit;
	pInst->pReq = NULL;
	pInst->options = 0;

	pInst->mdioReq.phyAddr = pInit->phyAddr;
	pInst->mdioReq.baseReq.pDoneFn = &dp83848t__mdioDone;
	pInst->mdioReq.baseReq.pNext = NULL;
	pInst->mdioReq.baseReq.pUtil = pInst;

	return pInst;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__access
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Request PHY chip operation.
*
*	\param		pDrvInst		Pointer to FB instance.
*	\param		pReq			Pointer to request.
*
*	\details	
*
*	\retval		GAI_RET_OK		The request was accepted and the callback
*								function will be called when the request has
*								been handled.
*	\retval		GAI_RET_BUSY	The driver is busy handling a previous request.
*								The request is not accepted and the callback 
*								function will not be called.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE gai_RetCode dp83848t__access(
	void *					pDrvInst,
	gai_Req *				pReq
) {
	dp83848t__Inst *		pInst;
	gai_RetCode				retCode;

	pInst = (dp83848t__Inst *) pDrvInst;

	DISABLE;
	if (pInst->pReq == NULL) {
		pInst->pReq = (apif_Req *) pReq;
		ENABLE;

		pInst->pStateFn(pInst, DP83848T__E_NEW_REQ);

		retCode = GAI_RET_OK;

	} else {
		ENABLE;
		retCode = GAI_RET_BUSY;
	}

	return(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__mdioDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reports to the state machine that the MDIO request has been 
*				handled.
*
*	\param		pReq		Pointer to the MDIO request.
*	\param		retCode		Result of the operation.
*
*	\details
*
*	\note		Should be called from a coTask or a task.
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__mdioDone(
	gai_Req *				pReq,
	gai_RetCode				retCode
) {
	dp83848t__Inst *		pInst;

	pInst = (dp83848t__Inst *) pReq->pUtil;

	pInst->mdioRet = retCode;
	pInst->pStateFn(pInst, DP83848T__E_MDIO_DONE);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__idleState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles the idle state.
*
*	\param		pInst	Pointer to FB instance.
*	\param		event	The event that triggered the state machine.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__idleState(
	dp83848t__Inst *		pInst,
	dp83848t__Event			event
) {
	switch (event) {
		case DP83848T__E_ENTRY:
			/* Do nothing when idle */
			break;

		case DP83848T__E_NEW_REQ:
			pInst->retryCount = 0;

			switch (pInst->pReq->op) {
				default:
				case APIF_OP_PWRSAVE:
				case APIF_OP_WAKEUP:
					pInst->pReq->baseReq.pDoneFn(
						&pInst->pReq->baseReq,
						GAI_RET_NOT_SUPPORTED
					);
					break;

				case APIF_OP_WARMINIT:
					dp83848t__enterState(pInst, &dp83848t__warmInitState);
					break;

				case APIF_OP_POLL:
					dp83848t__enterState(pInst, &dp83848t__pollState);
					break;

				case APIF_OP_READ:
					dp83848t__enterState(pInst, &dp83848t__readState);
					break;

				case APIF_OP_WRITE:
					dp83848t__enterState(pInst, &dp83848t__writeState);
					break;
			}
			break;

		case DP83848T__E_MDIO_DONE:
			deb_assert(FALSE);
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__warmInitState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles the PHY chip initialization.
*
*	\param		pInst	Pointer to FB instance.
*	\param		event	The event that triggered the state machine.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__warmInitState(
	dp83848t__Inst *		pInst,
	dp83848t__Event			event
) {
	switch (event) {
		case DP83848T__E_ENTRY:
			/* DP83848T does not have an interrupt pin. */
			deb_assert((pInst->pReq->data & APIF_OPT_PHYINT) == 0);

			pInst->step = DP83848T__WI_READID1;
			dp83848t__readPhy(pInst, MDIO_REG_PHYIDR1);
			break;

		case DP83848T__E_NEW_REQ:
			/* 
			 * No new requests should have been accepted while handling a 
			 * request.
			 */
			deb_assert(FALSE);
			break;

		case DP83848T__E_MDIO_DONE: {
			switch (pInst->step) {
				case DP83848T__WI_READID1:
					if (pInst->mdioRet != GAI_RET_OK) {
						/*
						 *	Bus error.
						 */
						if (++pInst->retryCount < 3) {
							/*
							 *	Try to read again.
							 */
							dp83848t__readPhy(pInst, MDIO_REG_PHYIDR1);

						} else {
							dp83848t__reqDone(pInst, APIF_RET_BUS_ERROR);
						}

					} else if (pInst->mdioReq.data != DP83848T__IDR1_VALUE) {
						/*
						 *	The most significant OUI bits did not match.
						 */
						if (++pInst->retryCount < 3) {
							/*
							 *	Try to read again.
							 */
							dp83848t__readPhy(pInst, MDIO_REG_PHYIDR1);

						} else {
							dp83848t__reqDone(pInst, APIF_RET_NOT_FOUND);
						}

					} else {
						/*
						 *	The most significant bits of the OUI are ok.
						 *	Read the least significant bits.
						 */
						pInst->step = DP83848T__WI_READID2;
						dp83848t__readPhy(pInst, MDIO_REG_PHYIDR2);
					}
					break;

				case DP83848T__WI_READID2:
					if (pInst->mdioRet != GAI_RET_OK) {
						/*
						 *	Bus error
						 */
						if (++pInst->retryCount < 3) {
							/*
							 *	Try to read again.
							 */
							dp83848t__readPhy(pInst, MDIO_REG_PHYIDR2);

						}  else {
							dp83848t__reqDone(pInst, APIF_RET_BUS_ERROR);
						}

					} else if (
						(pInst->mdioReq.data & 0xFFF0) != DP83848T__IDR2_VALUE
					) {
						/*
						 *	The least significant OUI bits or the vendor
						 *	model number did not match.
						 */
						if (++pInst->retryCount < 3) {
							/* Try to read again. */
							dp83848t__readPhy(pInst, MDIO_REG_PHYIDR1);

						} else {
							/*
							 *	The OUI or the vendor model number does not
							 *	match. Wrong or broken PHY chip or something is
							 *	wrong with the MDIO bus.
							 */
							dp83848t__reqDone(pInst, APIF_RET_NOT_FOUND);
						}

					} else {
						Uint16 value = 0;

						/*
						 *	The least significant bits of the OUI are ok and the
						 *	vendor model number matches.
						 */

						if (pInst->pReq->data & APIF_OPT_AUTO_NEGO) {
							value = MDIO_BMCR_AN | MDIO_BMCR_RE_AN;

						} else {
							if (pInst->pReq->data & APIF_OPT_SPEED_100) {
								value = MDIO_BMCR_SP100;
							}

							if (pInst->pReq->data & APIF_OPT_FULL_DUP) {
								value |= MDIO_BMCR_DUPLEX;
							}
						}

						pInst->step = DP83848T__WI_WRITESPEED;
						dp83848t__writePhy(pInst, MDIO_REG_BMCR, value);
					}
					break;

				case DP83848T__WI_WRITESPEED:
					if (pInst->mdioRet != GAI_RET_OK) {
						/*
						 *	Bus error.
						 */
						if (++pInst->retryCount < 3) {
							/*
							 *	Try to write again.
							 */
							dp83848t__writePhy(
								pInst,
								MDIO_REG_BMCR,
								pInst->mdioReq.data
							);

						} else {
							/*
							 *	Something is wrong with the MDIO bus.
							 */
							dp83848t__reqDone(pInst, GAI_RET_ERROR);
						}

					} else {
						pInst->options = (Uint8) pInst->pReq->data;
						dp83848t__reqDone(pInst, GAI_RET_OK);
					}
					break;
			}
			break;
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__pollState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles the status polling state.
*
*	\param		pInst	Pointer to FB instance.
*	\param		event	The event that triggered the state machine.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__pollState(
	dp83848t__Inst *		pInst,
	dp83848t__Event			event
) {
	switch (event) {
		case DP83848T__E_ENTRY:
			dp83848t__readPhy(pInst, DP83848T_REG_PHYSTS);
			break;

		case DP83848T__E_NEW_REQ:
			/* 
			 * No new requests should have been accepted while handling a 
			 * request.
			 */
			deb_assert(FALSE);
			break;

		case DP83848T__E_MDIO_DONE:
			if (pInst->mdioRet != GAI_RET_OK) {
				if (++pInst->retryCount < 3) {
					dp83848t__readPhy(pInst, DP83848T_REG_PHYSTS);

				} else {
					dp83848t__reqDone(pInst, GAI_RET_ERROR);
				}

			} else {
				Uint8 status = 0;

				if (pInst->mdioReq.data & DP83848T_STS_LSTATUS) {
					status = APIF_STAT_LINKUP;
				}

				if (
					(pInst->options & APIF_OPT_AUTO_NEGO)
					&& not(pInst->mdioReq.data & DP83848T_STS_AN_COMP)
				) {
					/*
					 *	Auto-negotiation not complete yet.
					 */
					status = 0;
				}

				if (status != (pInst->status & APIF_STAT_LINKUP)) {
					/*
					 *	Link status has changed.
					 */

					status |= APIF_STAT_LINKCHNG;
				}

				if (status & APIF_STAT_LINKUP) {
					/*
					 *	Link is up.
					 */

					if not(pInst->mdioReq.data & DP83848T_STS_SPEED10){
						status |= APIF_STAT_SPEED100;
					}

					if (pInst->mdioReq.data & DP83848T_STS_DUPLEX) {
						status |= APIF_STAT_DUPFULL;
					}

					if (
						(status ^ pInst->status)
						& (APIF_STAT_SPEED100 | APIF_STAT_DUPFULL)
					) {
						/*
						 *	Speed has changed.
						 */
						status |= APIF_STAT_SPEED;
					}
				}

				pInst->status = status;
				pInst->pReq->data = status;

				dp83848t__reqDone(pInst, GAI_RET_OK);
			}
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__readState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles the register reading state.
*
*	\param		pInst	Pointer to FB instance.
*	\param		event	The event that triggered the state machine.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__readState(
	dp83848t__Inst *		pInst,
	dp83848t__Event			event
) {
	switch (event) {
		case DP83848T__E_ENTRY:
			dp83848t__readPhy(pInst, (Uint8) pInst->pReq->reg);
			break;

		case DP83848T__E_NEW_REQ:
			/* 
			 * No new requests should have been accepted while handling a 
			 * request.
			 */
			deb_assert(FALSE);
			break;

		case DP83848T__E_MDIO_DONE:
			if (pInst->mdioRet != GAI_RET_OK) {
				if (++pInst->retryCount < 3) {
					dp83848t__readPhy(pInst, (Uint8) pInst->pReq->data);

				} else {
					dp83848t__reqDone(pInst, GAI_RET_ERROR);
				}

			} else {
				pInst->pReq->data = pInst->mdioReq.data;
				dp83848t__reqDone(pInst, GAI_RET_OK);
			}
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__writeState
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handles the register writing state.
*
*	\param		pInst	Pointer to FB instance.
*	\param		event	The event that triggered the state machine.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__writeState(
	dp83848t__Inst *		pInst,
	dp83848t__Event			event
) {
	switch (event) {
		case DP83848T__E_ENTRY:
			dp83848t__writePhy(pInst, pInst->pReq->reg, pInst->pReq->data);
			break;

		case DP83848T__E_NEW_REQ:
			/* 
			 * No new requests should have been accepted while handling a 
			 * request.
			 */
			deb_assert(FALSE);
			break;

		case DP83848T__E_MDIO_DONE:
			if (pInst->mdioRet != GAI_RET_OK) {
				if (++pInst->retryCount < 3) {
					dp83848t__writePhy(
						pInst,
						pInst->pReq->reg,
						pInst->pReq->data
					);

				} else {
					dp83848t__reqDone(pInst, GAI_RET_ERROR);
				}

			} else {
				dp83848t__reqDone(pInst, GAI_RET_OK);
			}
			break;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__readPhy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start PHY register read.
*
*	\param		pInst	Pointer to FB instance.
*	\param		reg		The register that should be read.
*
*	\details
*
*	\note		
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__readPhy(
	dp83848t__Inst *		pInst,
	Uint8					reg
) {
	gai_RetCode				retCode;

	pInst->mdioReq.op = AMIF_OP_READ;
	pInst->mdioReq.regNum = reg;

	retCode = pInst->pInit->pAmif->pAccessFn(
		*pInst->pInit->ppSmeInst,
		&pInst->mdioReq.baseReq
	);

	/*
	 * Assuming the access function always returns GAI_RET_OK. Queuing
	 * should be added to the station management entity driver if
	 * execution stops because of this assertion.
	 */

	deb_assert(retCode == GAI_RET_OK);
	DUMMY_VAR(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__writePhy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start PHY register write.
*
*	\param		pInst	Pointer to FB instance.
*	\param		reg		The register that should be written.
*	\param		data	The 16-bit data that should be written.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__writePhy(
	dp83848t__Inst *		pInst,
	Uint8					reg,
	Uint16					data
) {	
	gai_RetCode retCode;

	pInst->mdioReq.data = data;
	pInst->mdioReq.op = AMIF_OP_WRITE;
	pInst->mdioReq.regNum = reg;

	retCode = pInst->pInit->pAmif->pAccessFn(
		*pInst->pInit->ppSmeInst,
		&pInst->mdioReq.baseReq
	);

	/*
	 *	Assuming the access function always returns GAI_RET_OK.	Queuing
	 *	should be added to the station management entity driver if execution
	 *	stops because of this assertion.
	 */

	deb_assert(retCode == GAI_RET_OK);
	DUMMY_VAR(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	dp83848t__reqDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Ends the current request by calling the callback function and
*				clearing the current request pointer.
*
*	\param		pInst		Pointer to FB instance.
*	\param		retCode		The return code that will be supplied to the 
*							callback function.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void dp83848t__reqDone(
	dp83848t__Inst *		pInst,
	apif_RetCode			retCode
) {
	apif_Req *				pReq;

	atomic(
		pReq = pInst->pReq;
		pInst->pReq = NULL;
	);

	dp83848t__enterState(pInst, &dp83848t__idleState);
	pReq->baseReq.pDoneFn(&pReq->baseReq, retCode);
}
