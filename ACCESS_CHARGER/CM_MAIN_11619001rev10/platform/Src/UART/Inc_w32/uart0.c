/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		uart0.c
*
*	\ingroup	UART_W32
*
*	\brief		UART W32 implementation.
*
*	\details		
*
*	\note		
*
*	\version	\$Rev: 563 $ \n
*				\$Date: 2011-02-10 17:59:48 +0200 (Thu, 10 Feb 2011) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "mem.h"
#include "uart.h"

#include "..\local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void *				uart__init0(void const_P *,void *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

extern uart_Callback const_P project_uart0Callback[];

/**
 * Public interface for uart2.
 */
 
PUBLIC const_P drv1_Interface	uart0_interface = {
	uart__init0,
	uart_set,
	uart_write
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE uart_Inst uart__inst0 = {
	0,
	NULL,
	NULL,
	INVALID_HANDLE_VALUE,
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__init0
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function for initialization of UART.
*
*	\param		pVoidUartSettings	Pointer to UART config structure.
*	\param		pUtil 				Pointer to protocol instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void * uart__init0(
	void const_P * 			pVoidUartSettings, 
	void *					pUtil
) {
	uart_Inst *				pPort;
	uart_Config *			pUartSettings;

	pUartSettings = (uart_Config *) pVoidUartSettings;

	//pPort = mem_reserve(&mem_normal, sizeof(uart_Inst));
	pPort = &uart__inst0;
	deb_assert(pPort);

	pPort->pUtil = pUtil;
	pPort->pConfig = pUartSettings;
	pPort->pCallbacks = project_uart0Callback;

	uart__commonInit(pPort);

	return(pPort);
}
