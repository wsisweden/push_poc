/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	UART
*
*	\brief		Local declarations of the UART FB.
*
*	\details	This file contains declarations that should only be visible
*				internally in the function block. This file is never included
*				from outside of the function block.
*
*	\note
*
*	\version	\$Rev: 4423 $ \n
*				\$Date: 2021-01-13 17:08:06 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	UART	UART
*
*	\brief		UART driver.
*
********************************************************************************
*
*	\details	This function block handles the low level part of UART
*				communication.
*
*				Other function blocks can use this driver through the drv1
*				interface.
*
*******************************************************************************/

#ifndef UART_LOCAL_H_INCLUDED
#define UART_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_W32
#include <windows.h>
#endif

#include "uart.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	State flags.
 */

/**@{*/
#define UART__FLG_TXBUSY	(1<<0)		/**< Sending in progress.			*/
/**@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_W32

struct uart__inst {
	Uint8 					flags;
	void *					pUtil;
	uart_Config const_P *	pConfig;
	HANDLE					hPort;
	CRITICAL_SECTION		crit;
	uart_Callback const_P * pCallbacks;
};

#else

struct uart__inst {
	Uint8 					flags;
	Uint16					nSendLen;
	uart_Config const_P *	pConfig;
	void *					pUtil;
	Uint8 *					pSendPtr;
};

#endif

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

#if TARGET_SELECTED & TARGET_W32
extern void uart__commonInit(uart_Inst *);
#endif

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
