/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	UART
*
*	\brief		Local declarations for the S4F4 UART implementation.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4422 $ \n
*				\$Date: 2021-01-13 16:48:56 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef UART_HW_H_INCLUDED
#define UART_HW_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "hw.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Type used for UART instances.
 */

typedef struct {
	uart_Inst				base;
	UART_HandleTypeDef 		hwHandle;
	BYTE 					rxData;
	uart_Callback const_P *	pCallbacks;
} uart__HwUartInst;

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* UART_DRV.c */
Boolean uart__drvInit(uart__HwUartInst *,pUART_CallbackTypeDef,USART_TypeDef *);
Boolean uart__drvSet(uart__HwUartInst * pInst);
void uart__drvWrite(uart__HwUartInst * pInst, Uint8 * pBytes, Uint16 nLen);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif /* LOCAL_USART_H_INCLUDED */
