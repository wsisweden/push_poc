/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	UART_S4F4
*
*	\brief		STM32F4xx driver using ST HAL.
*
*	\details	This driver may be used for all UARTs of the device.
*
*	\note
*
*	\version	\$Rev: 4465 $ \n
*				\$Date: 2021-03-18 13:57:01 +0200 (to, 18 maalis 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>

#include "tools.h"
#include "hw.h"
#include "drv1.h"
#include "uart.h"
#include "deb.h"

#include "..\local.h"
#include "UART_hw.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void uart__drvTxCompleteCb(UART_HandleTypeDef * hUart);
PRIVATE void uart__drvRxCompleteCb(UART_HandleTypeDef * hUart);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__drvInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of UART.
*
*	\param		pInst 			Pointer to UART instance.
*	\param		pMspInitCb		Pointer to the msp init callback function.
*	\param		pPeripheral		Pointer to the UART peripheral.
*
*	\retval		TRUE			Initialization was successful.
*	\retval		FALSE			Initialization failed.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Boolean uart__drvInit(
	uart__HwUartInst *		pInst,
	pUART_CallbackTypeDef	pMspInitCb,
	USART_TypeDef *			pPeripheral
) {
	memset(&pInst->hwHandle, 0, sizeof(UART_HandleTypeDef));

	pInst->hwHandle.Instance = pPeripheral;

	HAL_UART_RegisterCallback(
		&pInst->hwHandle,
		HAL_UART_MSPINIT_CB_ID,
		pMspInitCb
	);

	return uart__drvSet(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__drvSet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Apply UART settings.
*
*	\param		pInst 			Pointer to UART instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Boolean uart__drvSet(
	uart__HwUartInst *		pInst
) {
	HAL_StatusTypeDef initResult;
	
	/*
	 *	Configure baudrate.
	 */

	pInst->hwHandle.Init.BaudRate = pInst->base.pConfig->baudRate;

	/*
	 *	Configure data bits. Only 8 and 9 bits supported by ST hardware.
	 */

	deb_assert(pInst->base.pConfig->dataBits == UART_DATABITS_8);
	pInst->hwHandle.Init.WordLength = UART_WORDLENGTH_8B;

	/*
	 *	Configure stop bits.
	 */

	if (pInst->base.pConfig->stopBits == UART_STOPBITS_1) {
		pInst->hwHandle.Init.StopBits = ST_UART_STOPBITS_1;

	} else if (pInst->base.pConfig->stopBits == UART_STOPBITS_2) {
		pInst->hwHandle.Init.StopBits = ST_UART_STOPBITS_2;

	} else {
		deb_assert(FALSE);
	}

	/*
	 *	Configure parity.
	 */

	if (pInst->base.pConfig->parity == UART_PARITY_NONE) {
		pInst->hwHandle.Init.Parity = ST_UART_PARITY_NONE;

	} else if (pInst->base.pConfig->parity == UART_PARITY_EVEN) {
		pInst->hwHandle.Init.Parity = ST_UART_PARITY_EVEN;

	} else if (pInst->base.pConfig->parity == UART_PARITY_ODD) {
		pInst->hwHandle.Init.Parity = ST_UART_PARITY_ODD;

	} else {
		deb_assert(FALSE);
	}

	pInst->hwHandle.Init.Mode = UART_MODE_TX_RX;
	pInst->hwHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	pInst->hwHandle.Init.OverSampling = UART_OVERSAMPLING_16;

	initResult = HAL_UART_Init(&pInst->hwHandle);

	if (initResult == HAL_OK) {
		HAL_UART_RegisterCallback(
			&pInst->hwHandle,
			HAL_UART_TX_COMPLETE_CB_ID,
			uart__drvTxCompleteCb
		);

		HAL_UART_RegisterCallback(
			&pInst->hwHandle,
			HAL_UART_RX_COMPLETE_CB_ID,
			uart__drvRxCompleteCb
		);

		/*
		 *	Setup data reception.
		 */

		HAL_UART_Receive_IT(&pInst->hwHandle, &pInst->rxData, 1);	
	}

	return initResult == HAL_OK ? TRUE : FALSE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__drvWrite
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function for sending data to the UART.
*
*	\param		pUart 	Pointer to the uart instance
*	\param		pBytes 	Pointer to the data that is to be sent.
*	\param		nLen 	Length of the data
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void uart__drvWrite(
	uart__HwUartInst *		pInst,
	Uint8 *					pBytes,
	Uint16					nLen
) {
	/*
	 *	Writ should not be called before the previous write is completed.
	 */
	deb_assert(pInst->base.pSendPtr == NULL);

	if (nLen > 0) {
		pInst->base.pSendPtr = pBytes;
		HAL_UART_Transmit_IT(&pInst->hwHandle, pBytes, nLen);

	} else {
		/*
		 *	There is no point in calling the write function with no data.
		 */

		deb_assert(FALSE);
		pInst->pCallbacks[pInst->base.pConfig->cbIndex].fnDataSent(
			pInst->base.pUtil
		);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__drvTxCompleteCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform low level peripheral initialization.
*
*	\param		hUart		Pointer to UART driver handle.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__drvTxCompleteCb(
	UART_HandleTypeDef *	hUart
) {
	uart__HwUartInst * pInst;

	pInst = tools_structPtr(uart__HwUartInst, hwHandle, hUart);

	/*
	 *	Call the "data sent" callback function to inform the protocol code that
	 *	the requested data has been sent.
	 */
	
	pInst->base.pSendPtr = NULL;
	pInst->pCallbacks[pInst->base.pConfig->cbIndex].fnDataSent(
		pInst->base.pUtil
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__drvRxCompleteCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform low level peripheral initialization.
*
*	\param		hUart		Pointer to UART driver handle.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__drvRxCompleteCb(
	UART_HandleTypeDef *	hUart
) {
	uart__HwUartInst * pInst;

	pInst = tools_structPtr(uart__HwUartInst, hwHandle, hUart);

	/*
	 *	Provide the data to the receive callback function.
	 */

	pInst->pCallbacks[pInst->base.pConfig->cbIndex].fnDataReceived(
		pInst->rxData,
		pInst->base.pUtil
	);

	/*
	 *	Setup receiving of the next byte.
	 */

	HAL_UART_Receive_IT(hUart, &pInst->rxData, 1);
}
