/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	UART_S4F4
*
*	\brief		STM32F4xx UART2 driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4422 $ \n
*				\$Date: 2021-01-13 16:48:56 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "hw.h"
#include "drv1.h"
#include "uart.h"
#include "deb.h"

#include "..\local.h"
#include "UART_hw.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void * uart__init2(void const_P *pConfig,void * pUtil);
PRIVATE void uart__set2(void);
PRIVATE void uart__write2(void * pUart, Uint8 * pBytes, Uint16 nLen);

PRIVATE void uart__mspInitCb2(UART_HandleTypeDef * hUart);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface for USART2 without clock and pin initialization. This
 * interface should be given as a initialization parameter to the function block
 * that will be using the driver. The function block can then call the driver
 * through this interface.
 * 
 * The peripheral clock must be enabled before using this interface. The UART
 * pins must also be configured before using the interface.
 */

PUBLIC const_P drv1_Interface uart_ifNoInit2 = {
	uart__init2,
	uart__set2,
	uart__write2
};

/* These callbacks should be defined somewhere in the exe folder. */
extern uart_Callback const_P project_uart2Callback[];

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Instance of the USART2 driver.
 */

PRIVATE uart__HwUartInst 	uart__inst2;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__init2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of UART2.
*
*	\param		pConfig Pointer to UART config structure.
*	\param		pUtil 	Pointer to protocol instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void * uart__init2(
	void const_P *			pConfig,
	void *					pUtil
) {
	uart__inst2.base.pConfig = (uart_Config const_P *) pConfig;
	uart__inst2.base.pUtil = pUtil;
	uart__inst2.base.flags = 0;
	uart__inst2.base.pSendPtr = NULL;
	
	uart__inst2.pCallbacks = project_uart2Callback;

	uart__drvInit(&uart__inst2, uart__mspInitCb2, USART2);

	return(&uart__inst2);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__set2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Apply UART settings.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__set2(
	void
) {
	uart__drvSet(&uart__inst2);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__write2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function for sending data to the UART.
*
*	\param		pUart 	Pointer to the uart instance
*	\param		pBytes 	Pointer to the data that is to be sent.
*	\param		nLen 	Length of the data
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__write2(
	void *					pUart,
	Uint8 *					pBytes,
	Uint16					nLen
) {
	/*
	 *	The supplied instance pointer should be valid.
	 */

	deb_assert(pUart == &uart__inst2);

	uart__drvWrite(&uart__inst2, pBytes, nLen);
}
/** \cond priv_decl */
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Interrupt service routine for USART2
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC osa_isrOSFn(NULL, USART2_IRQHandler) {
	osa_isrEnter();
	HAL_UART_IRQHandler(&uart__inst2.hwHandle);
	osa_isrLeave();
}
osa_endOSIsr
/** \endcond */
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__mspInitCb2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform low level peripheral initialization.
*
*	\param		hUart		Pointer to UART driver handle.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__mspInitCb2(
	UART_HandleTypeDef *	hUart
) {
	/*
	 *	Peripheral clock and pins should have been configured already.
	 *	Enable the USART2 interrupt.
	 */

	hw_enableIrq(USART2_IRQn);
}
