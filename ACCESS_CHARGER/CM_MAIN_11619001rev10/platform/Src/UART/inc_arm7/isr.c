/*******************************************************************************
;
;
;
;
;
;
;
;				P R O G R A M   D E S C R I P T I O N
;
;
;				Name:		
;
;				Version:	
;
;				File(s):	
;
;
;
;
;
;
;
;******************************************************************** tabs:[5 9]

;*******************************************************************************
;
;	P R O G R A M   D E S C R I P T I O N
;
;-------------------------------------------------------------------------------
;
;	*************************************************************************
;	**                                                                     **
;	**     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
;	**  This source file is made available solely for use by the customer  **
;	**       of TJK Tietolaite Oy according to the signed agreement.       **
;	**         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
;	**       unless such use is expressly permitted by the agreement.      **
;	**                                                                     **
;	*************************************************************************
;
;	Identification:		
;
;	Tasks:				
;
;	Environment:		
;
;	Input:				
;
;	Output:				
;
;	Use of common data:	
;
;	Implementation:		
;
;	Original:
;
;	Versions,	changes:
;
;	Index:				
;
;******************************************************************************/

/*	
 *  This code is to be included inside the 1ms ISR
 */
{

#include "src\\modbus\\local.h"

	Uint8					i;

	for(i = 0; i <= UART_MAX; i++)
	{
		modbus_Inst * pModbusInst;

		if(modbus__uartTbl[i])
		{
			pModbusInst = modbus__uartTbl[i]->pUtil;
		}
		else
		{
			continue;
		}

		if(pModbusInst->status & RECEIVING_DATA)
		{	
			pModbusInst->idleCounter++;

			if(pModbusInst->idleCounter >= 4)
			{	
				pModbusInst->status |= BUFFER_LOCKED | FRAME_READY;
				pModbusInst->status &= ~RECEIVING_DATA;
				osa_semaSetIsr(&pModbusInst->sema);
			}	
		}
	}
}
