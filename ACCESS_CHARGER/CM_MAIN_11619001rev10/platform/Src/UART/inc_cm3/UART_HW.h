/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	UART_CM3
*
*	\brief		
*
*	\details
*
*	\note
*
*	\version	\$Rev: 3795 $ \n
*				\$Date: 2019-08-14 10:30:22 +0300 (ke, 14 elo 2019) $ \n
*				\$Author: tlvian $
*
*******************************************************************************/

#ifndef UART_HW_H_INCLUDED
#define UART_HW_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "../local.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* UART_0.c */
void * 	uart__init0(void const_P *,void *);
void 	uart__set0(void);
void 	uart__write0(void *,Uint8 *,Uint16);

/* UART_1.c */
void * 	uart__init1(void const_P *,void *);
void 	uart__set1(void);
void 	uart__write1(void *,Uint8 *,Uint16);

/* UART_2.c */
void * 	uart__init2(void const_P *,void *);
void 	uart__set2(void);
void 	uart__write2(void *,Uint8 *,Uint16);

/* UART_3.c */
void * 	uart__init3(void const_P *,void *);
void 	uart__set3(void);
void 	uart__write3(void *,Uint8 *,Uint16);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
