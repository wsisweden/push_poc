/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	OSA
*
*	\brief		Local declarations for the OSA FB.
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef OSA_LOCAL_H_INCLUDED
#define OSA_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "osa.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*
 *	The following two structures are declared to minimize the RAM usage. Data
 *	in 'osa__PreRunData' is valid during the initialization till osa_run() is
 *	called. Data in 'osa__RunTimeData' is valid after the call to osa_run().
 *
 *	TODO: Additional elements, if any, should be moved into these structures in
 *	order to release RAM space.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	osa_fnPostInit			postInit;	/**< Fn to call at end of osa_run().*/
	osa_fnPostInit			coTaskInit;	/**< Fn to call if CoTasks are used.*/
} osa__PreRunData;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type used to store run-time data.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	osa_SemaType			idleSema;	/**<                                */
	osa_TaskType			idleTask;	/**< The task for osa_execIdle().    */
} osa__RunTimeData;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type used to store both pre-run data and run-time data.
 */

typedef union {							/*''''''''''''''''''''''''''''''''''*/
	osa__PreRunData			preRun;		/**< Pre run data.					*/
	osa__RunTimeData		runTime;	/**< Run-time data.					*/
} osa__DataType;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern osa__DataType 		osa__data;
extern osa_syncFlagType		osa__syncWaiting;
extern osa_SyncCallType *	osa__syncQueue;

/******************************************************************************/

#endif
