	.MODULE		osa_asm.asm

	.DEFINE		Z_HI	R31
	.DEFINE		Z_LO	R30
	.DEFINE		Y_HI	R29
	.DEFINE		Y_LO	R28
	.DEFINE		X_HI	R27
	.DEFINE		X_LO	R26

	ZERO_FLAG		=1

	HW_STKSIZE		=20

	SREG			=0x3F
	SP_HI			=0x3E
	SP_LO			=0x3D

	.AREA text(rom, con, rel)
;******************************************************************************
;
;	S U B R O U T I N E    D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
; 	   name:	_osa__osaStackEnterIsr	
;
;	purpose:	
;
;	  input:
;
;	 output:
;
;	 common:
;
;	 notice:
;
;******************************************************************************
_osa__osaStackEnterIsr::
	;
	;	One work register is needed, save this and SREG to current SW-stack.
	;
	ST		-Y, R0
	IN		R0, SREG
	ST		-Y, R0
	;
	;	Counter is 0xFF when OSA_STACK is not loaded. Value is incremented on 
	;	every enter and decremented on every exit. First entering the OSA_STACK
	;	inits it to the default.
	;
	LDS		R0, _osa__OsaStackCounter
	INC		R0
	BRBC	ZERO_FLAG, L0
	;	
	;	OSA_STACK points to the beginning of the Software Stack.
	;
	LDS		Y_LO, _osa__Stack
	LDS		Y_HI, _osa__Stack+1
L0:
	STS		_osa__OsaStackCounter, R0
	;
	;	Push call-used registers(except R0) into the SW-OSA_STACK.
	;
	ST		-Y, R1
	ST		-Y, R2
	ST		-Y, R3
	ST		-Y, R4
	ST		-Y, R5
	ST		-Y,	R6
	ST		-Y, R7
	ST		-Y,	R8
	ST		-Y, R9
	ST		-Y, R16
	ST		-Y, R17
	ST		-Y, R18
	ST		-Y, R19
	ST		-Y, R24
	ST		-Y, R25
	ST		-Y, X_LO
	ST		-Y, X_HI
	ST		-Y, Z_LO
	ST		-Y, Z_HI
	;
	;	Get the current HW-SP to X-registerpair. Save the return
	;	address of this function call to R2:R3 which are pushed
	;	before RET. Then save the current HW-SP into the
	;	topmost of the SW-OSA_STACK.
	;
	POP		R2
	POP		R3
	IN		R4, SP_HI
	IN		R5, SP_LO
	ST		-Y, R5
	ST		-Y, R4	
	;
	;	Init the OSA_STACK Hardware Stack if necessery. Notice, there must
	;	be no instructions affecting the SREG Z-flag after the first test.
	;
	BRBC	ZERO_FLAG, L1
	MOVW	X_LO, Y_LO
	ADIW	X_LO, HW_STKSIZE+21
	OUT		SP_HI, X_HI
	OUT		SP_LO, X_LO
	;
	;	Push the return address to HW-OSA_STACK and return.
	;	No RETI!
	;
L1:									
	PUSH	R3
	PUSH	R2
	RET	
;******************************************************************************
;
;	S U B R O U T I N E    D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
; 	   name:	_osa__osaStackExitIsr
;
;	purpose:	
;
;	  input:
;
;	 output:
;
;	 common:
;
;	 notice:
;
;******************************************************************************
_osa__osaStackExitIsr::
	;
	;	Precaution if interrupts was enabled on the ISR function.
	;
	CLI
	;
	;	Decrement the counter.
	;
	LDS		R0, _osa__OsaStackCounter
	DEC		R0
	STS		_osa__OsaStackCounter, R0
	;
	;	Restore	the Hardware Stack Pointer from the Top of the
	;	OSA - Software Stack.
	;
	LD		X_HI, Y+
	LD		X_LO, Y+
	OUT		SP_HI, X_HI
	OUT		SP_LO, X_LO
	;
	;	Restore call-saved registers from the OSA - Software stack.
	;
	LD		Z_HI, Y+
	LD		Z_LO, Y+
	LD		X_HI, Y+
	LD		X_LO, Y+
	LD		R25, Y+
	LD		R24, Y+
	LD		R19, Y+
	LD		R18, Y+
	LD		R17, Y+
	LD		R16, Y+
	LD		R9, Y+
	LD		R8, Y+
	LD		R7, Y+
	LD		R6, Y+
	LD		R5, Y+
	LD		R4, Y+
	LD		R3, Y+
	LD		R2, Y+
	LD		R1, Y+
	;
	;	Restore the Software Stack Pointer value which was set before
	;	this ISR execution.
	;
	POP		Y_HI
	POP		Y_LO
	;
	;	Finally, restore SREG and R0 from the original SW-stack and 
	;	return from the interrupt with RETI!.
	;
	SBIW	Y_LO, 2
	LD		R0, Y+
	OUT		SREG, R0
	LD		R0, Y+
	RETI
;******************************************************************************
;
;	S U B R O U T I N E    D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
; 	   name:	_osa__osaStackEnter
;
;	purpose:	
;
;	  input:
;
;	 output:
;
;	 common:
;
;	 notice:	Interrupt's must be disabled when executing this sub-routine!
;
;******************************************************************************
_osa__osaStackEnter::
	;
	;	Save the return address of this call's
	;
	POP		R1
	POP		R2
	;
	;	Save the Current task's Software SP
	;
	MOVW	X_LO, Y_LO
	;
	;	
	;
	CLR		R0
	STS		_osa__OsaStackCounter, R0
	;
	;	Change to initial OSA -Software Stack
	;
	LDS		Y_LO, _osa__Stack
	LDS		Y_HI, _osa__Stack+1
	;
	;	Save the Current task's hardware and software SPs to OSA -Software Stack
	;
	ST		-Y, X_LO
	ST		-Y, X_HI
	IN		X_HI, SP_HI
	IN		X_LO, SP_LO
	ST		-Y, X_LO
	ST		-Y, X_HI
	SBIW	Y_LO, 1
	;
	;	
	;
	MOVW	X_LO, Y_LO
	ADIW	X_LO, HW_STKSIZE+3
	OUT		SP_LO, X_LO
	OUT		SP_HI, X_HI
	;
	;	Restore the return address and return
	;
	PUSH	R2
	PUSH	R1
	RET
;******************************************************************************
;
;	S U B R O U T I N E    D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
; 	   name:	_osa__osaStackExit
;
;	purpose:	
;
;	  input:
;
;	 output:
;
;	 common:
;
;	 notice:	Interrupt's must be disabled when executing this sub-routine!
;
;******************************************************************************
_osa__osaStackExit::
	;	Save the return address of this call's
	;
	POP		R1
	POP		R2
	;
	SER		R24
	STS		_osa__OsaStackCounter, R24
	;
	;	Restore the return address and return
	;
	ADIW	Y_LO, 1
	LD		X_HI, Y+
	LD		X_LO, Y+
	OUT		SP_HI, X_HI
	OUT		SP_LO, X_LO
	LD		X_HI, Y+
	LD		X_LO, Y+
	MOVW	Y_LO, X_LO

	PUSH	R2
	PUSH	R1
	RET
;******************************************************************************
;
;	S U B R O U T I N E    D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
; 	   name:	_osa__contextSwitch
;
;	purpose:	
;
;	  input:
;
;	 output:
;
;	 common:
;
;	 notice:
;
;		osa_TaskType	+0:1	Next Task Ready
;		osa_TaskType	+2:3	Stack
;
;******************************************************************************
_osa__contextSwitch::
	;
	;	Get pointer to running task
	;
	LDS		Z_LO, _osa__TaskNow
	LDS		Z_HI, _osa__TaskNow+1
	;
	;	Get	HW-SP
	;
	IN		R4, SP_HI
	IN		R5, SP_LO
	;
	;	Push hw stack pointer and nonvolatile registers into SW-stack
	;
	ST		-Y, R4
	ST		-Y, R5
	ST		-Y, R10
	ST		-Y, R11
	ST		-Y, R12
	ST		-Y, R13
	ST		-Y, R14
	ST		-Y, R15
	ST		-Y, R20
	ST		-Y, R21
	ST		-Y, R22
	ST		-Y, R23
	;
	;	Save SW-SP
	;
	STD		Z+3, R28
	STD		Z+4, R29
;	RJMP	_osa__contextRestore
;******************************************************************************
;
;	S U B R O U T I N E    D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
; 	   name:	_osa__contextRestore
;
;	purpose:	
;
;	  input:
;
;	 output:
;
;	 common:
;
;	 notice:	FLOW THROUGH
;
;******************************************************************************
_osa__contextRestore::
	;
	;	Get pointer to next running task
	;
	LDS		Z_LO, _osa__TaskNextRun
	LDS		Z_HI, _osa__TaskNextRun+1
	;
	;	Set pointer to task now running	
	;
	STS		_osa__TaskNow, Z_LO
	STS		_osa__TaskNow+1, Z_HI
	;
	;	Load task's sw stack pointer to Y
	LDD		Y_LO, Z+3
	LDD		Y_HI, Z+4
	;	
	;	Pop hw stack pointer and nonvolatile registers from SW-stack
	;
	LD		R23, Y+
	LD		R22, Y+
	LD		R21, Y+
	LD		R20, Y+		
	LD		R15, Y+
	LD		R14, Y+
	LD		R13, Y+
	LD		R12, Y+
	LD		R11, Y+
	LD		R10, Y+
	LD		R5,	 Y+
	LD		R4,	 Y+
	;
	;	Set HW-stack pointer out and return (RETI!)
	;
	OUT		SP_HI, R4
	OUT		SP_LO, R5
	;
	;	When task is run the first time, this will return to 
	;	_osa__taskDormantStart. Rest of the time this will return to the
	;	function called the scheduler as it was a normal function call.
	;
	;	R16 is the return register, return TRUE to indicate that there was
	;	a contextSwitch done.
	;
	SER		R16
	RETI
;******************************************************************************
;
;	S U B R O U T I N E    D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
; 	   name:	_osa__taskDormantStart
;
;	purpose:	
;
;	  input:
;
;	 output:
;
;	 common:
;
;	 notice:	
;
;******************************************************************************
_osa__taskDormantStart::
	LDS		R16, _osa__TaskNow
	LDS		R17, _osa__TaskNow+1	
	RETI
