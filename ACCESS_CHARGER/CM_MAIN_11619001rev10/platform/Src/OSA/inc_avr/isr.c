/******************************************************************************
;
;	M O D U L E   D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
;	Name: 			isr.c
;
;	Author:			
;
;	Purpose:		
;
;	Description:	
;
;	Versions:		
;
;	Orginal:		2.6.2004   10:14	
;	
;	Versions:		
;
******************************************************************************/

/*-----------------------------------------------------------------------------
 *
 *	Includes.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Defines.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Types.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Macros.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Globals.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Locals.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Functions.
 *
 *---------------------------------------------------------------------------*/

{
	static volatile Uint16				timer_tick_u16;

	timer_tick_u16++;
}