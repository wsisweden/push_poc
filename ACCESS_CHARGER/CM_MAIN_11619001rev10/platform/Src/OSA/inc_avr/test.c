
/*
;
;	T E S T   A P P L I C A T I O N    for OSAAVR	
;
*/

#include "TOOLS.H"
#include "OSA.H"
#include "timer.h"
#include "Real.h"
#include "MEM.H"

#if ( TARGET_SELECTED & TARGET_AVR )
#include "HW.H"
#endif


extern	void osa__addRetAddress(void);

OSA_TASK void taskFunction_1(void*);
OSA_TASK void taskFunction_2(void*);
OSA_TASK void taskFunction_3(void*);
OSA_TASK void taskFunction_4(void*);

osa_TaskType	task1;
osa_TaskType	task2;
osa_TaskType	task3;
osa_TaskType	task4;
osa_SemaType	sema1;


#define	ISR_HANDLER		timer0compare
#define	ISR_VECTOR		ISR_V_TIM0_COMP
#include "ISR.H"		
void osa_isrOSFn(ISR_V_TIM0_COMP, timer0compare) 
{
	static Uint8		counter = 0;
	Uint8				loop;
	volatile Uint8		table[12] = {0xee, 0xee, 0xee,0xee, 0xee, 0xee,
		0xee, 0xee, 0xee,0xee, 0xee, 0xee};
	osa_isrEnter();

	osa_tick();

	if ( (++counter % 20) == 0 ) {
		osa_semaSetIsr(&sema1);
	}

	for(loop = 0; loop < 12; loop++) {
		table[loop]++;
	}

	osa_isrLeave();
}

//#pragma ctask	taskFunction_1 taskFunction_2 taskFunction_3 taskFunction_4
/******************************************************************************
;
;	F U N C T I O N   D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
;	Name:		main
;
;	Purpose:	
;
;	In:			void =
;
;	Out:		int
;
;	Exceptions:	
;
;	Common:		
;
;	Operation:	
;
;	Notice:		
;
;*****************************************************************************/
int main(
	void
) {
	static	BYTE	memBlock[512];

	#if TARGET_SELECTED & TARGET_AVR
	timer_init();
	#endif
	osa_init();

	mem_init();

	mem_add( &mem_fast, memBlock, sizeof(memBlock) );
	ENABLE;

	osa_newTask(&task1, NULL, OSA_PRIORITY_NORMAL, taskFunction_1, 100);
	osa_newTask(&task2, NULL, OSA_PRIORITY_NORMAL, taskFunction_2, 100);
	osa_newTask(&task3, NULL, OSA_PRIORITY_NORMAL, taskFunction_3, 100);
	osa_newSemaTaken(&sema1);

#if	( TARGET_SELECTED & TARGET_AVR )
	TCCR0 = 0x08|0x03; 
	TCNT0 = 0x00;
	TCNT1H = 0xff;
	OCR0 = 125 - 1;
	TIMSK = 0x2;	
/*	TIMSK = 0x2|0x4;	
	TCCR1B = 0x1;	*/
#endif
	osa_run();

	return 1;
}
/******************************************************************************
;
;	F U N C T I O N   D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
;	Name:		taskFunction_1
;
;	Purpose:	
;
;	In:			pData =
;
;	Out:		-
;
;	Exceptions:	
;
;	Common:		
;
;	Operation:	
;
;	Notice:		
;
;*****************************************************************************/
OSA_TASK void	taskFunction_1(
	void *		pData
) {
	volatile	ii;
	Uint8		cntr;
	volatile	Uint16	dummy[6] = {0xDDDD, 0xDDDD, 0xDDDD, 0xDDDD, 0xDDDD, 0xDDDD};
	volatile register Uint16		u32;

	FOREVER {

		for ( cntr = 0; cntr<6; cntr++) {
			dummy[cntr]++;
		}

		if ( dummy[0] != dummy[1] ) {
			ii++;
		}

		if ( dummy[0] != dummy[2] ) {
			ii++;
		}

		if ( dummy[0] != dummy[3] ) {
			ii++;
		}

		if ( dummy[0] != dummy[4] ) {
			ii++;
		}

		u32 = 0x3344;
		osa_taskWait(100);
		if ( u32 != 0x3344 ) {
			ii++;
		}

	}
}
/******************************************************************************
;
;	F U N C T I O N   D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
;	Name:		taskFunction_2
;
;	Purpose:	
;
;	In:			pData =
;
;	Out:		-
;
;	Exceptions:	
;
;	Common:		
;
;	Operation:	
;
;	Notice:		
;
;*****************************************************************************/
OSA_TASK void	taskFunction_2(
	void *		pData
) {
	
	FOREVER {

		osa_semaGet(&sema1);
		osa_taskWait(100);

	}
}
/******************************************************************************
;
;	F U N C T I O N   D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
;	Name:		taskFunction_3
;
;	Purpose:	
;
;	In:			pData =
;
;	Out:		-
;
;	Exceptions:	
;
;	Common:		
;
;	Operation:	
;
;	Notice:		
;
;*****************************************************************************/
OSA_TASK void	taskFunction_3(
	void *		pData
) {
	volatile Uint8	vars[4] = {0x11, 0x22, 0x33, 0x44};

	FOREVER {

		osa_taskWait(150);
		osa_taskWait(0);
		osa_semaGet(&sema1);

	}
}
/******************************************************************************
;
;	F U N C T I O N   D E S C R I P T I O N
;
;------------------------------------------------------------------------------
;
;	Name:		taskFunction_4
;
;	Purpose:	
;
;	In:			pData =
;
;	Out:		-
;
;	Exceptions:	
;
;	Common:		
;
;	Operation:	
;
;	Notice:		
;
;*****************************************************************************/
OSA_TASK void	taskFunction_4(
	void *		pData
) {

	FOREVER {
	}
}
