################################################################################
#
#	This is an include file to the MAKEFILE of the parent directory
#
#	Target:		S4F4 (STM32F4xx)
#	Compiler:	IAR
#	Make:		Microsoft NMAKE
#
################################################################################

objects=$(objects)\
	obj_$(TLLIB_T)\portIar.$o

# Rule for the FreeRTOS 9.0.0 Cortex-M4 folder
{OS\FRE09M4\iar}.s{Obj_$(TLLIB_T)}.$o:
	@echo Compiling assembly file $(<) to $(@)
	@$(AS) $(<) $(c_incs) $(a_flags) -o $(@)

obj_$(TLLIB_T)\portIar.$o		: OS\FRE09M4\iar\portIar.s
