################################################################################
#
#	This is an include file to the MAKEFILE of the FB root directory.
#
#	Target:		Any Cortex-M3 target
#	Compiler:	IAR
#	Make:		Microsoft NMAKE
#	OS:			FreeRTOS V9.0.0
#
################################################################################

objects=$(objects)\
	obj_$(TLLIB_T)\port.$o\
	obj_$(TLLIB_T)\portasm.$o

# Rule for C files in the FreeRTOS 9.0.0 Cortex-M3 folder
{OS\FRE09M3\iar}.c{obj_$(TLLIB_T)}.$o:
	@echo Compiling C file $(<) to $(@)
	@$(CC) $(<) $(c_incs) $(c_flags) -o $(@)

# Rule for assembler files in the FreeRTOS 9.0.0 Cortex-M3 folder
{OS\FRE09M3\iar}.s{obj_$(TLLIB_T)}.$o:
	@echo Compiling assembly file $(<) to $(@)
	@$(AS) $(<) $(c_incs) $(a_flags) -o $(@)

obj_$(TLLIB_T)\port.$o			: OS\FRE09M3\iar\port.c
obj_$(TLLIB_T)\portasm.$o		: OS\FRE09M3\iar\portasm.s
