/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[X]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		OSA\inc_cm3\_osa.c
*
*	\ingroup	OSA_CM3
*
*	\brief		Operating System API
*
*	\details		
*
*	\note 		
*
*	\version	
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	OSA_CM3	CM3
*
*	\ingroup	OSA
*
*	\brief		Cortex-M3 implementation of OSA.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/******************************************************************************
;
;	HEADER AND INCLUDE FILES
;
******************************************************************************/

#include "tools.h"
#include "deb.h"
#include "mem.h"
#include "sys.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h" /* For stack width */
#include "task.h"

#include "../../osa_loc.h"

/******************************************************************************
;
;	CONSTANTS AND DEFINITIONS
;
******************************************************************************/

#if (TARGET_SELECTED & TARGET_GNU) == 0
# error The FRE06M3 OS currently compiles only with GCC.
#endif

#if OSA_PRIORITY_HIGH   != OSA_PRIORITY_NORMAL + 3 \
 || OSA_PRIORITY_NORMAL != OSA_PRIORITY_LOW + 3
#error Priority values must be contiguous OSA_PRIORITY_LOW having lowest value
#endif

#define OSA__PRIORITY_1ST	(OSA_PRIORITY_LOW - 1)
#define OSA__PRIORITY_CNT	(OSA_PRIORITY_HIGH - OSA_PRIORITY_LOW + 3)

/* Get stack width from stack type */
#define OSA__STACK_WIDTH	sizeof(portSTACK_TYPE)

/******************************************************************************
;
;	MACROS
;
******************************************************************************/

/******************************************************************************
;
;	DATA TYPES AND STRUCTURES
;
******************************************************************************/

typedef void (*				fnFreeRTOSTask)(void *);

/******************************************************************************
;
;	PRIVATE FUNCTIONS
;
******************************************************************************/

/******************************************************************************
;
;	DATA
;
******************************************************************************/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_newTask
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create a new task.
*
*	\param		pTask		Pointer to the scratch task-object to initialize.
*	\param		name		Unique name for the task. The name must be a hard
*							constant that is valid and available throughout
*							the program execution.
*	\param		priority	Task's priority, one of the predefined constants
*							(see below) optionally fine-tuned with +/-1.
*	\param		taskFn		Task's "main" function.
*	\param		stackSize	Size of the task's stack in bytes.
*
*	\details	Must be called before the OS is started i.e. osa_run() is
*				called.
*
*				Priority			Description
*				OSA_PRIORITY_HIGH	Critical tasks should use this priority.
*				OSA_PRIORITY_NORMAL	This should be the priority of each task,
*									unless there are reasons that force to raise
*									or lower it.
*				OSA_PRIORITY_LOW	Less-important tasks should use this
*									priority.
*
*				All the three priority values can be fine-tuned, when necessary,
*				for higher or lower priority by adding or subtracting,
*				respectively, one to/from the constant value. How the actual
*				priorities are set is an implementation dependent matter, but
*				OSA supports up to nine different priority levels.
*
*				Task's "main" function should include the OSA_TASK type
*				qualifier. Tasks never return thus the compiler allowed optimize
*				such functions in a more efficient way.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_newTask(
	osa_TaskType *			pTask,
	char const_P *			name,
	Uint8					priority,
	void (OSA_TASK *		taskFn)(void),
	Uint16					stackSize
) {
	signed portBASE_TYPE xReturn;
	deb_assert(priority < configMAX_PRIORITIES);
	deb_assert(stackSize >= 256); /* 256 is barely enough in cortex */

	/*	
	 *  Stack size is given as bytes to OSA but FreeRTOS takes it as variable
	 *	count so bytecount is divided by stack width.
	 */
	stackSize = (stackSize + (OSA__STACK_WIDTH - 1)) / OSA__STACK_WIDTH;
	
	/*	
	 *  Initialize osa_TaskType variables
	 */
	pTask->nSignal = 0;
	pTask->pUtil = NULL;
	pTask->pName = name;

	/*	
	 *  pTask is given as utility ptr. This is assumed to be so in FreeRTOS 
	 *	code changes!
	 */

	xReturn = xTaskCreate(
		(fnFreeRTOSTask)taskFn,	/* Function type cast! */	
		(const signed portCHAR * const)name,
		stackSize,
		pTask,
		priority,
		NULL
	);

	DUMMY_VAR(xReturn);
	deb_assert(xReturn == pdPASS);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_run
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start multitasking.
*
*	\details	This function newer returns.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_run(
	void
) {
	osa_fnPostInit pPostInit;

	pPostInit = osa__data.preRun.postInit; /* osa__run() will overwrite */

	deb_assert(!osa_isRunning);
	{
		void osa__run(void);

		/*
		 *	Complete the initialization of the internals. This will e.g. create
		 *	the last task, the one with the lowest priority.
		 */

		osa__run();
	}

	pPostInit();

	osa_isRunning = TRUE;

	/* Start scheduler */
	vTaskStartScheduler();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_semaSetFn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function that set a semaphore, so it will be "available".
*
*	\param		pSema	Pointer to the initialized semaphore-object.
*
*	\details	This is exactly the same as osa_semaSet() but is guaranteed to
*				be a function, not a macro. The function version should be used
*				only when a function-pointer is required.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_semaSetFn(
	osa_SemaType *			pSema
) {
	osa_semaSet(pSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_signalSendFn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function that send signal to a task.
*
*	\param		pTask	Pointer to the initialized task-object.
*
*	\details	This is exactly the same as osa_signalSend() but is guaranteed
*				to be a function, not a macro. The function version should be
*				used only when a function-pointer is required.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_signalSendFn(
	osa_TaskType *			pTask
) {
	osa_signalSend(pTask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_taskResumeFn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function that resume a suspended task.
*
*	\param		pTask	Pointer to the initialized task-object.
*
*	\details	This is exactly the same as osa_taskResume() but is guaranteed
*				to be a function, not a macro. The function version should be
*				used only when a function-pointer is required.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_taskResumeFn(
	osa_TaskType *			pTask
) {
	osa_taskResume(pTask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	vApplicationStackOverflowHook
*
*--------------------------------------------------------------------------*//**
*
*	\brief		FreeRTOS calls this when stack overflow is detected.
*
*	\param		pxTask			Task handle.
*	\param		pcTaskName		Task name.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void vApplicationStackOverflowHook(
	xTaskHandle *			pxTask,
	signed char *			pcTaskName
) {
	osa_TaskType * pTask;

	pTask = tools_structPtr(osa_TaskType, rtosTask, pxTask);

	sys_reportFatalError(SYS_FATAL_STACK_OVERFLOW, pTask);

	deb_assert(FALSE);

	DISABLE;

	while (1) {
		/* It is not safe to continue when a stack overflow has occurred. */
	}
}
