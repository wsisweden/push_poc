################################################################################
#
#	This is an include file to the MAKEFILE of the parent directory
#
################################################################################

objects=$(objects)\
 obj_$(TLLIB_T)\croutine.$o\
 obj_$(TLLIB_T)\FRTOSmem.$o\
 obj_$(TLLIB_T)\list.$o\
 obj_$(TLLIB_T)\port.$o\
 obj_$(TLLIB_T)\portISR.$o\
 obj_$(TLLIB_T)\queue.$o\
 obj_$(TLLIB_T)\tasks.$o


obj_$(TLLIB_T)\croutine.$o		: inc_$(TLLIB_T)\croutine.c
obj_$(TLLIB_T)\FRTOSmem.$o		: inc_$(TLLIB_T)\FRTOSmem.c
obj_$(TLLIB_T)\list.$o			: inc_$(TLLIB_T)\list.c
obj_$(TLLIB_T)\port.$o			: inc_$(TLLIB_T)\port.c
obj_$(TLLIB_T)\portISR.$o			: inc_$(TLLIB_T)\portISR.c
obj_$(TLLIB_T)\queue.$o			: inc_$(TLLIB_T)\queue.c
obj_$(TLLIB_T)\tasks.$o			: inc_$(TLLIB_T)\tasks.c

