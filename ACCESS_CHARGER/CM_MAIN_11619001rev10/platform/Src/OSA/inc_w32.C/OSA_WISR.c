/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	OSA_W32
*
*	\brief		W32 interrupt handling emulation.
*
*	\details	Contains the lowest level of W32 OSA functions. Calling these
*				functions will not link other OSA files.
*
*				OSA tasks are implemented with Windows threads in the W32
*				target code. All OSA task threads are allowed to run
*				concurrently.
*
*				Interrupts are emulated with Windows threads that are not
*				OSA tasks.
*
*	\par		Disabling scheduling
*				This is emulated by suspending all OSA task threads except
*				the one disabling scheduling. Enabling tasking will resume the
*				tasks again. Interrupt emulation threads are allowed to run
*				freely.
*
*	\par		Interrupt disabling
*				This is emulated by taking a critical section and suspending
*				all OSA task threads. All interrupt emulation threads also take
*				the same critical section when executing the interrupt code.
*				This prevents the interrupt emulation threads from executing
*				while an OSA task thread has disabled interrupts. Suspending all
*				OSA tasks makes sure no other task are executed while interrupts
*				are disabled. Enabling interrupts will resume all OSA task 
*				threads and release the critical section.
*
*	\note
*
*	\version	\$Rev: 4300 $ \n
*				\$Date: 2020-11-10 13:28:36 +0200 (ti, 10 marras 2020) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
*
*	D E C L A R A T I O N   M O D U L E
*
********************************************************************************/

/*******************************************************************************
*
*	HEADER / INCLUDE FILES
*
*-----------------------------------------------------------------------------*/

/* Windows headers */
#include <windows.h>
#include <assert.h>

/* Platform headers */
#include "tools.h"
#include "osa.h"

/*******************************************************************************
*
*	CONSTANTS
*
*-----------------------------------------------------------------------------*/

/*******************************************************************************
*
*	INTERNAL MACROS
*
*-----------------------------------------------------------------------------*/

#define osa__taskCurrent()	((osa_TaskType *) TlsGetValue(osa__tlsIdx))

/*******************************************************************************
*
*	INTERNAL STRUCTURES AND DATA TYPES
*
*-----------------------------------------------------------------------------*/

/*******************************************************************************
*
*	STATIC FUNCTIONS
*
*-----------------------------------------------------------------------------*/

PUBLIC void osa__isrEnterCritical(void);
PUBLIC void osa__isrExitCritical(void);

/******************************************************************************
*
*	EXTERNAL FUNCTIONS
*
******************************************************************************/

/*******************************************************************************
*
*	PUBLIC DATA
*
*-----------------------------------------------------------------------------*/

PUBLIC HANDLE				osa__startEvent;
PUBLIC DWORD				osa__tlsIdx;
PUBLIC osa_TaskType			osa__tasks;

PUBLIC const char *			osa__lockingTaskName = NULL;
PUBLIC const char *			osa__disablingTaskName = NULL;
PUBLIC int					osa__disableCount = 0;
PUBLIC int					osa__critCounter = 0;
PUBLIC int					osa__taskingLockCount = 0;

/*******************************************************************************
*
*	STATIC DATA
*
*-----------------------------------------------------------------------------*/

PRIVATE CRITICAL_SECTION	osa__taskCrit;
PRIVATE CRITICAL_SECTION	osa__taskCritTasking;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize OSA and the Operating System.
*
*	\return		-
*
*	\details	This function should be called before invoking any other
*				OSA-service.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_init(
	void
) {
	assert(!osa_isRunning);

	/*
	 *	Set the current process priority and disable thread boosting.
	 */

	SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS);
	SetProcessPriorityBoost(GetCurrentProcess(), TRUE);

	InitializeCriticalSection(&osa__taskCrit);
	InitializeCriticalSection(&osa__taskCritTasking);

	/*
	 *	Thread local storage will be used to store the task ptr.
	 */

	osa__tlsIdx = TlsAlloc();
	assert(osa__tlsIdx != 0xFFFFFFFF);

	/*
	 *	Create the event used to synchronize the tasking starting.
	 */

	osa__startEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	assert(osa__startEvent != NULL);

	link2_init(&osa__tasks.link);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa__isrEnter
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Mark start of ISR processing.
*
*	\return		-
*
*	\details	Should be used at the beginning of an ISR whenever there are
*				other OSA calls, namely osa_semaSetIsr(), inside that ISR. When
*				osa_isrEnter() is used, osa_isrLeave() must be executed at
*				exiting the ISR.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa__isrEnter(
	void
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa__isrLeave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		End of ISR processing.
*
*	\return		-
*
*	\details	Should be used at the end of an ISR whenever osa_isrEnter() is
*				executed at the beginning.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa__isrLeave(
	void
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_taskingDisable
*	
*-------------------------------------------------------------------------------		
*
*	\brief		Temporarily disable task switching.
*
*	\return		-
*
*	\details	osa_taskingDisable() prevents the OS to schedule between tasks
*				thus the current task receives all the CPU resources. Hardware
*				interrupts are not disabled. Disabling task switching is useful
*				in protecting critical resources and parts of the code from
*				the other tasks.
*
*				When task switching is disabled, the task must not invoke
*				the "Wait", "Get" or "Suspend" functions of OSA, or any other
*				function that may suspend the operation of the current task.
*
*				Task switching is re-enabled by osa_taskingEnable(). Task
*				switching can be disabled more than once. An internal counter is
*				maintained for the purpose. Each osa_taskingDisable() needs
*				an own osa_taskingEnable() before the counter reach the initial
*				value and task switching is enabled again.
*
*				Depending of the implementation, osa_taskingDisable() with its
*				osa_taskingEnable() counterpart may be relatively heavy tool to
*				use. Disabling hardware interrupts should be considered as
*				an alternative when the critical part consists of one to few
*				simple operations only. As a rule of thumb, hardware interrupts
*				should not be disabled over a function call.
*
*	\note		This is a dangerous service that can corrupt the real-time
*				characteristics of the system and therefore task switching
*				should be disabled over very short period of times only.
*				Should be used with caution!
*
*******************************************************************************/

PUBLIC void osa_taskingDisable(
	void
) {
	osa_TaskType * pCurrentTask;

	if (!osa_isRunning) {
		return;
	}

	/*
	 *	Simulating disabling of scheduling by just using a critical section.
	 *	This will not actually prevent Windows from switching between tasks
	 *	but it will prevent multiple tasks from executing protected parts
	 *	simultaneously.
	 *
	 *	This function previously used SuspendThread()
	 */

	EnterCriticalSection(&osa__taskCritTasking);
	
	assert(osa__taskingLockCount >= 0);

	/*
	 *	Store the name of the task that suspended scheduling.
	 */

	pCurrentTask = osa__taskCurrent();
	
	if(pCurrentTask != NULL) {
		osa__lockingTaskName = pCurrentTask->pName;
	} else {
		osa__lockingTaskName = "NonOSAtask";
	}
	
	osa__taskingLockCount++;

	/*
	 *	osa_taskingDisable will enable interrupts on some targets but not on
	 *	all of them. Not enabling interrupts here.
	 */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_taskingEnable
*	
*-------------------------------------------------------------------------------	
*
*	\brief		Enable task switching.
*
*	\return		-
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC void osa_taskingEnable(
	void
) {
	if (!osa_isRunning) {
		return;
	}

	/*
	 *	There should be one osa_taskingEnable() call for each
	 *	osa_taskingDisable() call. The functions count how many times they have
	 *	been called.
	 */

	assert(osa__taskingLockCount > 0);
	osa__taskingLockCount--;

	osa__lockingTaskName = "None";

	LeaveCriticalSection(&osa__taskCritTasking);

	/*
	 *	Other targets also enable interrupts during the osa_taskingEnable call.
	 */

	osa_interruptsEnable();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_interruptsDisable
*	
*-------------------------------------------------------------------------------
*
*	\brief		A hidden function to "DISABLE" interrupt servicing.
*
*	\return		-
*
*	\details	The function emulates interrupt disabling by suspending all
*				other tasks and entering a critical section.
*
*	\note		
*
*******************************************************************************/

PUBLIC void osa_interruptsDisable(
	void
) {
	osa_TaskType * pCurrentTask;

	/*
	 *	On W32 target interrupts are usually emulated by creating a Windows
	 *	thread with CreateThread() without using the osa_newTask() 
	 *	function. osa_taskingDisable() will not suspend those threads,
	 *	because it only stops OSA task threads. That makes them work kind of
	 *	like an interrupt. 
	 *
	 *	The DISABLE and ENABLE macros should also stop the Windows
	 *	interrupt enulation threads from accessing protected data concurrently.
	 *	This is implemented with a critical section. This function will take the
	 *	osa__taskCrit critical section and hold it until osa_interruptsEnable is
	 *	called.
	 *
	 *	This must use the same critical section as the osa_taskingDisable()
	 *	function. Otherwise a task could be inside the critical section when
	 *	another task calls osa_taskingDisable(). That would cause a deadlock.
	 */

	osa__isrEnterCritical();

	assert(osa__disableCount >= 0);
	if (osa__disableCount == 0) {
		/*
		 *	Storing the name of the disabling task if the current thread is an
		 *	OSA task. This function may also be called from interrupt emulation
		 *	threads.
		 */

		pCurrentTask = osa__taskCurrent();

		if (pCurrentTask != NULL) {
			osa__disablingTaskName = pCurrentTask->pName;
		} else {
			osa__disablingTaskName = "NonOsaThread";
		}
	}

	/*
	 *	A counter is used because both DISABLE and ENABLE may be called
	 *	multiple times in a row. There does not have to be an DISABLE
	 *	for each ENABLE.
	 */

	osa__disableCount++;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_interruptsEnable
*	
*-------------------------------------------------------------------------------
*
*	\brief		A hidden function to "ENABLE" interrupt servicing.
*
*	\return		-
*
*	\details	The function emulates interrupt enabling by exiting the
*				critical section.
*
*	\note		
*
*******************************************************************************/

PUBLIC void osa_interruptsEnable(
	void
) {
	/*
	 *	The whole operation is protected by taking the critical section.
	 */

	osa__isrEnterCritical();

	/*
	 *	A counter is used because both DISABLE and ENABLE may be called
	 *	multiple times in a row. Calling DISABLE multiple times in a row will
	 *	do nothing. Only the first call does something.
	 *	The same is true for ENABLE calls. Only the first ENABLE call does
	 *	something.
	 */

	assert(osa__disableCount >= 0);
	if (osa__disableCount > 0) {
		/*
		 *	interrupts are currently disabled.
		 *
		 *	The Windows critical sections have an internal counter. Calling
		 *	LeaveCriticalSection the same amount of times the 
		 *	EnterCriticalSection has been called in the osa_interruptsDisable
		 *	function.
		 *
		 *	One ENABLE should be enough to undo multiple DISABLEs.
		 */

		do {
			osa__isrExitCritical();
		} while (--osa__disableCount);
	}
	
	osa__disablingTaskName = "None";

	/*
	 *	One last LeaveCriticalSection because of the EnterCriticalSection at the
	 *	beginning of this function.
	 */

	osa__isrExitCritical();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_interruptsDisableIntr
*
*-------------------------------------------------------------------------------
*
*	\brief		A hidden function to "DISABLEINTR" interrupt servicing.
*
*	\return		The interrupt state.
*
*	\details	The function emulates interrupt disabling by a critical section.
*
*	\note
*
*******************************************************************************/

PUBLIC tools_IsrState osa_interruptsDisableIntr(
	void
) {
	tools_IsrState oldCount;

	osa__isrEnterCritical();

	oldCount = osa__disableCount;
	osa_interruptsDisable();

	osa__isrExitCritical();

	return oldCount;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_interruptsEnableIntr
*
*-------------------------------------------------------------------------------
*
*	\brief		A hidden function to "ENABLEINTR" interrupt servicing.
*
*	\param		pOldIntState		Pointer to the previous interrupt state.
*
*	\return		-
*
*	\details	The function emulates interrupt enabling by exiting the
*				critical section.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_interruptsEnableIntr(
	tools_IsrState *			pOldIntState
) {
	if (*pOldIntState == 0) {
		/*
		 *	Interrupts were enabled before the DISABLEINTR call
		 */

		osa_interruptsEnable();
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_enterEmulatedIsr
*
*-------------------------------------------------------------------------------
*
*	\brief		Configures W32 OSA for running ISR emulation.
*
*	\return		-
*
*	\details	This function should be called from Windows threads that are
*				used to emulate interrupts.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_enterEmulatedIsr(
	void
) {
	/*
	 *	Just using the DISABLE-function for now. This will prevent a task from 
	 *	executing protected code while the simulated interrupt thread is
	 *	executing.
	 */
	osa_interruptsDisable();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_leaveEmulatedIsr
*
*-------------------------------------------------------------------------------
*
*	\brief		Configures W32 OSA for normal operation.
*
*	\return		-
*
*	\details	This function should be called from Windows threads that are
*				used to emulate interrupts when leaving the ISR code.
*
*	\note
*
*******************************************************************************/

PUBLIC void osa_leaveEmulatedIsr(
	void
) {
	osa_interruptsEnable();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa__isrEnterCritical
*
*-------------------------------------------------------------------------------
*
*	\brief		Enter internal critical section of OSA interrupt emulation.
*
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void osa__isrEnterCritical(
	void
) {
	EnterCriticalSection(&osa__taskCrit);
	assert(osa__critCounter >= 0);
	osa__critCounter++;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa__isrExitCritical
*
*-------------------------------------------------------------------------------
*
*	\brief		Exit internal critical section of OSA interrupt emulation.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void osa__isrExitCritical(
	void
) {
	assert(osa__critCounter > 0);
	osa__critCounter--;
	LeaveCriticalSection(&osa__taskCrit);
}
