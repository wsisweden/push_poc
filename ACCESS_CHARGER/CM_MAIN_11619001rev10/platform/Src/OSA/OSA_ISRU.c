/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		OSA_ISRU.C
*
*	\ingroup	OSA
*
*	\brief		Variable with operating system running state.
*
*	\details		
*
*	\note 		
*
*	\version	\$Rev: 2938 $ \n
*				\$Date: 2016-06-07 18:57:26 +0300 (ti, 07 kesä 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/ 

/******************************************************************************
;
;	HEADER AND INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "OSA.H"

/******************************************************************************
;
;	CONSTANTS AND DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES AND STRUCTURES
;
;-----------------------------------------------------------------------------*/

/******************************************************************************
;
;	PRIVATE FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/* 
 *	This was defined in each target OSA separately but used in each one.
 *	Added 060206 TKN
 *
 *	Moved to separate file to prevent OSA.C from being linked when referencing
 *	this variable.
 */

PUBLIC Boolean				osa_isRunning = FALSE;

/* End of declaration module **************************************************/
