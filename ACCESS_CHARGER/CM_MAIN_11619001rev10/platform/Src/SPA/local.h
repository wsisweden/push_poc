/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPA\LOCAL.H
*
*	\ingroup	SPA
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef SPA_LOCAL_H_INCLUDED
#define SPA_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "real.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define THISPtr				((spa__Inst*)pInst)
#define SPAIF				(THISPtr->pInit->pInterface)

#define SPA_BCAST_ADDRESS	900

/*	
 *  SPA status flags
 */
#define SPA_FLG_WAIT_RST	(1<<0)		/**< Waiting OSA reset              */
#define SPA_FLG_RST_PEND	(1<<1)		/**< Pending OSA reset              */
#define SPA_FLG_FRM_IN_L	(1<<2)		/**< Input frame locked             */

/*
 * SPA indication flags
 */
#define SPA_I_FLG_ADDR		(1<<0)		/**< Slave address needs refreshing */
#define SPA_I_FLG_FRM_READY	(1<<1)		/**< Frame received                 */
#define SPA_I_FLG_RST_PEND	(1<<2)		/**< Pending OSA reset              */


/*	
 *  Message control flags
 */
#define SPA_MSG_CONT		(1<<0)		/**< Message will continue          */

/*	
 *  Message types from master to slave
 */
#define SPA_T_READ			'R'
#define SPA_T_WRITE			'W'

/*	
 *  Message types from slave to master
 */
#define SPA_T_DATA			'D'
#define SPA_T_NACK			'N'
#define SPA_T_ACK			'A'

/*	
 *  Categories
 */
#define SPA_CAT_F			'F'			/**< Slave identification           */	
#define SPA_CAT_T			'T'			/**< Time                           */
#define SPA_CAT_D			'D'			/**< Date                           */
#define SPA_CAT_C			'C'			/**< Slave status                   */
#define SPA_CAT_L			'L'			/**< Last events                    */
#define SPA_CAT_B			'B'			/**< Last backup events             */
#define SPA_CAT_A			'A'			/**< Alarms                         */
#define SPA_CAT_I			'I'			/**< Inputs                         */
#define SPA_CAT_O			'O'			/**< Outputs                        */
#define SPA_CAT_S			'S'			/**< Settings                       */
#define SPA_CAT_V			'V'			/**< Internal variables             */
#define SPA_CAT_M			'M'			/**< Memory data                    */

enum {
	SPA_CAT_IDX_F,						/**< Slave identification           */	
	SPA_CAT_IDX_T,						/**< Time                           */
	SPA_CAT_IDX_D,						/**< Date                           */
	SPA_CAT_IDX_C,						/**< Slave status                   */
	SPA_CAT_IDX_L,						/**< Last events                    */
	SPA_CAT_IDX_B,						/**< Last backup events             */
	SPA_CAT_IDX_A,						/**< Alarms                         */
	SPA_CAT_IDX_I,						/**< Inputs                         */
	SPA_CAT_IDX_O,						/**< Outputs                        */
	SPA_CAT_IDX_S,						/**< Settings                       */
	SPA_CAT_IDX_V,						/**< Internal variables             */
	SPA_CAT_IDX_M						/**< Memory data                    */
};

/*	
 *  Return values
 */
#define SPA_OK				0
#define SPA_IGNORE			1
#define SPA_ERROR			2
#define SPA_NACK0			100
#define SPA_NACK1			101
#define SPA_NACK2			102
#define SPA_NACK3			103
#define SPA_NACK4			104
#define SPA_NACK5			105
#define SPA_NACK6			106
#define SPA_NACK7			107
#define SPA_NACK8			108
#define SPA_NACK9			109


/*	
 *  Special characters
 */
#define SPA_INV_BYTE		0x00

#define SPA_CHAR_CR			0x0D
#define SPA_CHAR_LF			0x0A
#define SPA_CHAR_END		0x00

/*	
 *  FTR channels
 */
#define SPA_FTR_CHANNEL		0
#define SPA_FTR_START		200
#define SPA_FTR_WRITE		201
#define SPA_FTR_READ_COUNT	200
#define SPA_FTR_READ_BLOCK	201
#define SPA_FTR_END			202

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#define spa__fromHex(x_)	((x_) >= 'A' ? ((x_) - 'A') + 10 : (x_) - '0')
#define spa__toHex(d_)		((d_) >= 10 ? 'A' + ((d_) - 10) : '0' + (d_))

#define spa__isDigit(n_)	(((n_) >= '0') && ((n_) <= '9'))

/* Resets frame buffer to beginning */
#define spa__resetFrmBuf(p_)												\
	{																		\
		(p_)->pWritePos = (p_)->pData;										\
		(p_)->nFree = (p_)->nSize;											\
	}

/* Appends byte to frame buffer */
#define spa__appendFrame(f_,b_)												\
	{																		\
		*((f_)->pWritePos++) = (b_);										\
		(f_)->nFree--;														\
	}

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef union {							/*''''''''''''''''''''''''''''''''''*/
	Int8					i8;			/*									*/
	Uint8					u8;			/*									*/
	Int16					i16;		/*									*/
	Uint16					u16;		/*									*/
	Int32					i32;		/*									*/
	Uint32					u32;		/*									*/
	Real					real;		/*									*/
}spa__Number;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#if 0
typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8 *					pBuf;		/*									*/
	Uint16					nSize;		/*									*/
	Uint16					nCount;		/*									*/
	Uint16					nWrite;		/*									*/
	Uint16					nRead;		/*									*/
} spa__Buffer;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#endif

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8 *					pData;		/**< Ptr to data buffer             */
	Uint8 *					pWritePos;	/**< Ptr to current write position  */
	Uint16					nSize;		/**< Size of the buffer             */
	Uint16					nFree;		/**< Number of free bytes in buffer */
} spa__FrmBuf;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint16					nAddress;	/**< Slave address (1 - 999)        */
	Uint16					nType;		/**< Message type (R / W)           */
	Uint16					nFirstCh;	/**< Channel (0 - 999)              */
	Uint8					nDataCat;	/**< Data category                  */
	Uint32					nFirstData;	/**< Data (0 - 999999)              */
	Uint8 *					pData;		/**< Ptr to data part               */
	Uint8					nDataLen;	/**< Length of data in pData        */
	Uint8 *					pName;		/**< Ptr to name                    */
	Uint8					nNameLen;	/**< Length of name                 */
} spa__BasicMsg;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	spa__BasicMsg			base;		/**< Basic message info             */
	Uint16					nLastCh;	/**< Last channel in range          */
	Uint32					nLastData;	/**< Last data in range             */
} spa__ComplexMsg;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	osa_TaskType			task;		/**< Task                           */
	spa_Init const_P * 		pInit;		/**< Initialization data            */
	sys_FBInstId			nInstId;	/**< Instance ID                    */
	osa_SemaType			sema;		/**< Signaling semaphore            */
	Uint16					nAddress;	/**< Spa slave address              */
	Uint16					nFlags;		/**< Status flags                   */
	Uint16					nIndFlags;	/**< Indication flags               */
	spa__FrmBuf *			pFrmIn;		/**< Input frame                    */
	spa__FrmBuf *			pFrmOut;	/**< Output frame                   */
	spa__BasicMsg *			pMsg;		/**< Input message info             */
	void *					pUart;		/**<                                */
} spa__Inst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


typedef void (				*spa__fnByteReceived)(Uint8, void *);

/*************************************************************//** \endcond *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern Uint8				spa__startFrame(spa__Inst *, spa__FrmBuf *);
extern Uint8				spa__endFrame(spa__Inst *, spa__FrmBuf *);
extern Uint8				spa__writeNack(spa__Inst *, Uint8, spa__FrmBuf *);

extern void *				spa__uartInit(Uint8, void *, spa__fnByteReceived);
extern void					spa__uartWrite(void *, Uint8 *, Uint16);

extern void					spa__trInit(spa__Inst *);
extern void					spa_trByteIn(Uint8, void *);
extern void					spa_trBytesOut(spa__Inst *, spa__FrmBuf *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
