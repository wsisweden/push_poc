/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPA\INIT.H
*
*	\ingroup	SPA
*
*	\brief		
*
*	\note		
*
*	\version
*
*******************************************************************************/
 
#ifndef SPA_INIT_H_INCLUDED
#define SPA_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

/*
#define name@lwc_reset		dummy_reset
#define name@lwc_down		dummy_down
#define name@lwc_read		dummy_read
#define name@lwc_write		dummy_write
#define name@lwc_ctrl		dummy_ctrl
#define name@lwc_test		dummy_test
*/

#include "spa.h"

/**
 * SPA initialization type
 */
 
typedef struct {						
	const_P spa_Interface *	pInterface;
	const_P void	 *		pPod;
	Uint16					nInputBufSize;
	Uint16					nOutputBufSize;
} spa_Init;							

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_SPA
#elif defined(EXE_GEN_SPA)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_SPA
#endif

#define EXE_APPL(n)			n##spa

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN /**< Non-volatile numeric                                */
//#define EXE_USE_NS /* Non-volatile string	*/
//#define EXE_USE_VN /* Volatile numeric		*/
//#define EXE_USE_VS /* Volatile string		*/
//#define EXE_USE_PN /* Process-point numeric	*/
//#define EXE_USE_PS /* Process-point string	*/
//#define EXE_USE_BL /* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name		Flags				Dim		Type	Def		Min	Max
            ---------	---------------		----	-----	----	---	----*/
EXE_REG_NN( address,	SYS_REG_DEFAULT,	1,		Uint16, 0,		0,	999 )


/* References to external registers */


/* Write-indications */
EXE_IND_ME( addressInd, address )


/* Language-dependent texts */

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
