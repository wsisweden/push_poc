/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	File: local.h
*
*--------------------------------------------------------------------*//** \file		
*
*	\ingroup	SWTIMER
*
*	\brief		Local declarations for the software timer FB.
*
*	\details	
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef SWTIMER_LOCAL_H_INCLUDED
#define SWTIMER_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#define SWTIMER_LINKPtr(t_)		((swtimer_Link_st *)(t_))	
#define SWTIMER_TIMERPtr(t_) 	((swtimer_Timer_st *)(t_))

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef struct {								/*''''''''''''''''''''''''''*/
	osa_TaskType			task;				/**< Task                   */
	osa_SemaType			semaIsr;			/**< Sema for isr code      */
	swtimer_Link_st			linkRunning;		/**< List of running timers */
	swtimer_Link_st			linkExpired;		/**< List of expired timers */
} swtimer_Instance;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern swtimer_Instance 	swtimer_instance;

/******************************************************************************/

#endif
