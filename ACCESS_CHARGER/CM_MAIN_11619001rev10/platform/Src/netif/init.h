/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file
*
*	\ingroup	NETIF
*
*	\brief		Netif initialization declarations.
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef NETIF_INIT_H_INCLUDED
#define NETIF_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

#include "ndif.h"
#include "ntcpip.h"
#include "netif.h"

#define netif_reset		dummy_reset
#define netif_down		dummy_down
/* #define netif_read		dummy_read	*/
/* #define netif_write		dummy_write */
#define netif_ctrl		dummy_ctrl
#define netif_test		dummy_test

#if 0
/**
 * Function block ERR enum values.
 */

typedef enum netif__errEnumType {	/*''''''''''''''''''''''''''''''''''''''*/
	NETIF__ERR_AUTO_DHCP_START,		/**< DHCP-client and auto-IP started.	*/
	NETIF__ERR_DHCP_START,			/**< DHCP-client started.				*/
	NETIF__ERR_AUTO_START,			/**< Auto-IP started.					*/
	NETIF__ERR_STATIC_CONF,			/**< Static IP-address configured.		*/
	NETIF__ERR_LINK_LOCAL,			/**< Link-local IP-address active.		*/
									/*										*/
	NETIF__ERR_COUNT				/**< Number of NETIF ERR codes.			*/
} netif__ErrEnumType;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
#endif

/**
 * \brief	Type used for ip configuration functions.
 *
 * \param	pInst	Pointer to FB instance data.
 *
 * \details	All IP-address configuration functions are of this type.
 */

typedef void netif_IpCfgFn(netif__Inst * pInst);

/**
 * Initialization parameters for the NETIF FB.
 */

typedef struct {
	/**
	 * Pointer to ip-address configuration function. This is used to select
	 * which ip-address configuration method should be used. Possible functions
	 * are netif_useDhcp, netif_useStatic, netif_useAutoip and
	 * netif_useDhcpAndAutoip.
	 */
	netif_IpCfgFn *			pIpCfgFn;

	/**
	 * Default ip-address. May be NULL if AUTOIP or DHCP is used. This
	 * should point to a Uint8 array with four elements.
	 *
	 * If DHCP is used then this address is used if DHCP fails. It can also be
	 * set to NULL when DHCP is enabled which will make the device inaccessible
	 * if DHCP fails.
	 *
	 * The configured ip-address is not used at all by AUTOIP.
	 */
	Uint8 const_P *			pIpAddr;

	/**
	 * Default subnet mask bit count. The number defines how many host
	 * bits there should be in the ip-address. This is normally 8, 16 or 24
	 * bits. The number will not be used if AUTOIP is used.
	 */
	Uint8 					netmask;

	/**
	 * Default gateway ip-address. May be NULL if no default gateway is used.
	 * This should point to a Uint8 array with four elements.
	 */
	Uint8 const_P *			pGateway;

	/**
	 * Option flags.
	 */
	Uint8					flags;
} netif_Init;

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_NETIF
#elif defined(EXE_GEN_NETIF)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_NETIF
#endif

#define EXE_APPL(n)			n##netif

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN /**< Non-volatile numeric                                */
//#define EXE_USE_NS /* Non-volatile string	*/
#define EXE_USE_VN /* Volatile numeric		*/
//#define EXE_USE_VS /* Volatile string		*/
#define EXE_USE_PN /* Process-point numeric	*/
//#define EXE_USE_PS /* Process-point string	*/
//#define EXE_USE_BL /* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name		Flags			Dim		Type	Def	Min	Max
            ---------	---------------	----	-----	---	---	--*/

EXE_REG_NN(	ipAddr,		SYS_REG_DEFAULT,  1,	Uint32,	0,	0,	Uint32_MAX )
EXE_MMI_NONE(ipAddr )
EXE_HELP_EN( ipAddr,	0,	"TL_IPADDR", "IP address",
	"Configured IP address of the device. This can be zero which means that DHCP or link local address is used instead.")

EXE_REG_NN(	gateway,	SYS_REG_DEFAULT,  1,	Uint32,	0,	0,	Uint32_MAX )
EXE_MMI_NONE(gateway )
EXE_HELP_EN( gateway,	0,	"TL_GWADDR", "Default gateway",
	"IP address of the default gateway.")

EXE_REG_NN(	netmask,	SYS_REG_DEFAULT,  1,	Uint8,  24,	8,	30 )
EXE_MMI_NONE(netmask )
EXE_HELP_EN( netmask,	0,	"TL_NETMASK", "Subnet mask",
	"Number of network bits in the subnet mask.")

EXE_REG_PN(	ipStatus,	SYS_REG_DEFAULT,  1,	Uint8,		0,	Uint8_MAX )
EXE_MMI_NONE(ipStatus )
EXE_HELP_EN( ipStatus,	0,	"TL_IPSTATUS", "Current IP-address status.",
	"Bitfield with IP-address status information\f"
	"Bit\tName\tDescription\v"
	"0\tLINKLOCAL\t\b1 = A link-local address is in use. \b0 = link-local address is not in use.\v"
	"1\tDHCP\t\b1 = An address from the DHCP server is in use. \b0 = Current IP-address is not from the DHCP server.\v"
	"2\tDHCPMODE\t\b1 = DHCP client has been forced on with the DHCP mode setting. The used IP-address will be from the DHCP server or a link-local address. \b0 = DHCP client is set to auto mode.\r"
)

EXE_REG_NN(	dhcpMode,	SYS_REG_DEFAULT,  1,	Uint8,  0,	0,	1 )
EXE_MMI_NONE(dhcpMode )
EXE_HELP_EN( dhcpMode,	0,	"TL_DHCPMODE", "DHCP client mode",
	"Specifies the DHCP client behavior.\f"
	"Value\tDescription\v"
	"0\tAutomatic. DHCP client will be enabled if the ip-address is set to zero.\v"
	"1\tEnabled. DHCP client is enabled. A link-local address will be used if a DHCP server cannot be found.\r"
)

EXE_REG_PN(	currIpAddr,	SYS_REG_DEFAULT,  1,	Uint32,		0,	Uint32_MAX )
EXE_MMI_NONE(currIpAddr )
EXE_HELP_EN( currIpAddr,0,	"TL_CURRIPADDR", "Current IP-address",
	"This is the current ip-address which may be a statically assigned address or an address from a DHCP server or an link-local address.")

EXE_REG_PN(	currGateway,SYS_REG_DEFAULT,  1,	Uint32,		0,	Uint32_MAX )
EXE_MMI_NONE(currGateway )
EXE_HELP_EN( currGateway,0,	"TL_CURRGWADDR", "Current gateway address",
	"This address is the same as in the TL_GWADDR register or a gateway address from the DHCP server.")

EXE_REG_PN(	currNetmask,SYS_REG_DEFAULT,  1,	Uint8,		8,	30 )
EXE_MMI_NONE(currNetmask )
EXE_HELP_EN( currNetmask,0,	"TL_CURRNETMASK", "Current subnet mask",
	"This value is the same as in the NETMASK register or a subnet mask from the DHCP server or 255.255.0.0 if a link-local ip-address is in use.")

EXE_REG_VN(	status,		SYS_REG_DEFAULT,  1,	Uint8,  0,	0,	Uint8_MAX )
EXE_MMI_NONE(status )
EXE_HELP_EN( status,	0,	"TL_NETIFSTATUS", "Network interface status",
	"Bitfield with status information. This register can be used to get indications from link state changes.\f"
	"Bit\tName\tDescription\v"
	"0\tNETIFUP\t\b1 = the network interface is enabled. \b0 = it is disabled\v"
	"1\tNETIFLINK\t\b1 = the network link is up. \b0 = the link is down e.g. network cable disconnected\r"
)

/* References to external registers */
//EXE_REG_EX( buttonStatusHandle, buttonStatus, button1 )


/* Write/read indications */
EXE_IND_ME( ipAddr, 		ipAddr )
EXE_IND_ME( gateway, 		gateway )
EXE_IND_ME( netmask, 		netmask )
EXE_IND_ME( ipStatus, 		ipStatus )
EXE_IND_ME( dhcpMode, 		dhcpMode )
EXE_IND_ME( currIpAddr, 	currIpAddr )
EXE_IND_ME( currGateway, 	currGateway )
EXE_IND_ME( currNetmask, 	currNetmask )


/* *****************************************************************************
 *	
 *	ERR declarations
 */

EXE_ERR_TYPE(NETIF__ERR_AUTO_DHCP_START, tIpConf, netif, ERR_CLASS(info))	/**< DHCP-client and auto-IP started.	*/
EXE_ERR_TYPE(NETIF__ERR_DHCP_START, tIpConf, netif, ERR_CLASS(info))		/**< DHCP-client started.				*/
EXE_ERR_TYPE(NETIF__ERR_AUTO_START, tIpConf, netif, ERR_CLASS(info))		/**< Auto-IP started.					*/
EXE_ERR_TYPE(NETIF__ERR_STATIC_CONF, tIpConf, netif, ERR_CLASS(info))		/**< Static IP-address configured.		*/
EXE_ERR_TYPE(NETIF__ERR_LINK_LOCAL, tIpConf, netif, ERR_CLASS(info))		/**< Link-local IP-address active.		*/
EXE_ERR_TYPE(NETIF__ERR_PHY_STATUS, tIpConf, netif, ERR_CLASS(info))		/**< Driver reported PHY status.		*/

//EXE_ERR_TYPE(ipConfig, tIpConf, netif, ERR_CLASS(info))

/* *****************************************************************************
 *
 *	Language-dependent texts 
 */

EXE_TXT( tIpConf, EXE_T_EN("IP conf. changed") EXE_T_FI("Param kuvaus 1") "")

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
