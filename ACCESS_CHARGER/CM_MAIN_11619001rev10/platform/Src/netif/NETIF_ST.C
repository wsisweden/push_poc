/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NETIF_ST.C
*
*	\ingroup	NETIF
*
*	\brief		Static ip-address configuration.
*
*	\details
*
*	\note
*
*	\version	25-03-2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "ntcpip.h"
#include "netif.h"
#include "err.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif_useStatic
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Use a statically configured ip-address.
*
*	\param		pInst	Pointer to FB instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void netif_useStatic(
	netif__Inst *			pInst
) {
	ntcpip_IpAddr 			ipAddr;
	ntcpip_IpAddr			netmask;
	ntcpip_IpAddr 			gateway;

	ntcpip_IpAddr *			pGateway = &gateway;

	deb_assert(pInst->pInit->pIpAddr);
	deb_assert(pInst->pInit->netmask >= 8);
	deb_assert(pInst->pInit->netmask <= 30);

	NTCPIP_IP4_ADDR(
		&ipAddr,
		pInst->pInit->pIpAddr[0],
		pInst->pInit->pIpAddr[1],
		pInst->pInit->pIpAddr[2],
		pInst->pInit->pIpAddr[3]
	);

	if (pInst->pInit->pGateway == NULL) {
		pGateway = NULL;

	} else {
		NTCPIP_IP4_ADDR(
			pGateway,
			pInst->pInit->pGateway[0],
			pInst->pInit->pGateway[1],
			pInst->pInit->pGateway[2],
			pInst->pInit->pGateway[3]
		);
	}

	{
		Uint32 tempAddr;

		tempAddr = 0xFFFFFFFF << (32 - pInst->pInit->netmask);

		NTCPIP_IP4_ADDR(
			&netmask,
			(Uint8) tempAddr,
			(Uint8) (tempAddr >> 8),
			(Uint8) (tempAddr >> 16),
			(Uint8) (tempAddr >> 24)
		);
	}

	ntcpip_netifapiNetifSetAddr(
		&pInst->netif,
		&ipAddr,
		&netmask,
		pGateway
	);

	err_error(pInst->instId, 0, NETIF__ERR_STATIC_CONF);

	ntcpip_netifapiNetifSetUp(&pInst->netif);
}
