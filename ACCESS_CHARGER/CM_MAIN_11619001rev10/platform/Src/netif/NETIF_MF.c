/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NETIF_MF.c
*
*	\ingroup	NETIF
*
*	\brief		Multicast frame filter handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "ntcpip.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__macFilter
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Change multicast frame filter.
*
*	\param		pNetif		Pointer to netif structure.
*	\param		pGroupAddr	Pointer to multicast group address.
*	\param		action		Action code. Can be NTCPIP_IGMP_DEL_MAC or 
*							NTCPIP_IGMP_ADD_MAC.
*
*	\return		-
*
*	\details	Called by the TCP/IP-stack when the device joins or leaves a
*				multicast group.
*
*				The function calls the MAC-driver to configure multicast frame
*				filtering.
*
*	\note
*
*******************************************************************************/

PUBLIC ntcpip_Err netif__macFilter(
	ntcpip_Netif *			pNetif,
	ntcpip_IpAddr const_D *	pGroupAddr,
	Uint8					action
) {
	Boolean retVal = FALSE;

#if NTCPIP__LWIP_IGMP
	netif__Inst *			pInst;
	BYTE					macGroupAddr[6];
	Uint32					ipHostOrder;


	pInst = (netif__Inst *) pNetif->state;
	retVal = FALSE;

	if (pInst->pNdifInit != NULL) {
		/*
		 *	Convert IP multicast group address to host byte order.
		 */

		ipHostOrder = ntcpip_ntohl(pGroupAddr->addr);

		/*
		 *	Build the multicast MAC address from the IP group address.
		 */

		macGroupAddr[0] = 0x01;
		macGroupAddr[1] = 0x00;
		macGroupAddr[2] = 0x5E;
		macGroupAddr[3] = (BYTE) ((ipHostOrder >> 16) & 0x7F);
		macGroupAddr[4] = (BYTE) (ipHostOrder >> 8);
		macGroupAddr[5] = (BYTE) ipHostOrder;

		switch (action) {
			case NTCPIP_IGMP_DEL_MAC:
				retVal = pInst->pNdifInit->pTxIf->pMultiFiltFn(
					pInst->pNdifInit->pMacInst,
					macGroupAddr,
					FALSE
				);	
				break;

			case NTCPIP_IGMP_ADD_MAC:
				retVal = pInst->pNdifInit->pTxIf->pMultiFiltFn(
					pInst->pNdifInit->pMacInst,
					macGroupAddr,
					TRUE
				);	
				break;

			default:
				retVal = FALSE;
				break;
		}
	}
#endif
	return retVal == TRUE ? NTCPIP_ERR_OK : NTCPIP_ERR_IF;
}
