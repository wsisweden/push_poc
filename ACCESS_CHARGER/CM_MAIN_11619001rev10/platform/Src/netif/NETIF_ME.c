/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NETIF_ME.c
*
*	\ingroup	NETIF
*
*	\brief		NETIF buffer handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "ndif.h"
#include "ntcpip.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Instance type for the NETIF buffer handler.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16				blockSize;		/**< Size of buffer blocks.			*/
} netif__MemHandler;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void * netif__memInit(Uint16 blockSize,Uint8 blockCount);
PRIVATE Uint8 netif__memAlloc(void * pInst, ndif_Cbuff * pData,Uint8 count);
PRIVATE void netif__memFree(void * pInst,void * pUtil);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface constant for memory handler that uses NTCPIP buffer format.
 * This memory handler must be used if the MAC driver passes received frames
 * to NETIF.
 */

PUBLIC ndif_BuffHanIf const_P netif_memHandler = {
	&netif__memInit,
	&netif__memAlloc,
	&netif__memFree
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__memInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize buffer handler.
*
*	\param		blockSize	Size of blocks that the hander allocates.
*	\param		blockCount	Maximum number of blocks that can be allocated. This
*							limit is not implemented in all buffer handlers.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PRIVATE void * netif__memInit(
	Uint16					blockSize,
	Uint8					blockCount
) {
	netif__MemHandler * pInst;

	pInst = (netif__MemHandler *) mem_reserve(
		&mem_normal,
		sizeof(netif__MemHandler)
	);

	pInst->blockSize = blockSize;

	return pInst;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__memAlloc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate buffer blocks.
*
*	\param		pData		Pointer to array of buffer block information.
*	\param		count		Number of blocks in the array.
*	\param		blockSize	The size of each block that should be allocated.
*
*   \return		Number of blocks that could be allocated.
*
*	\details	
*
*******************************************************************************/

PRIVATE Uint8 netif__memAlloc(
	void *					pVoidInst,
	ndif_Cbuff *			pData,
	Uint8					count
) {
	Ufast8					buffCount;
	Uint16					blockSize;

	blockSize = ((netif__MemHandler *) pVoidInst)->blockSize;
	buffCount = count;
	do {
		ntcpip_Pbuf * pBuf;

		pBuf = ntcpip_pbufAlloc(
			NTCPIP_PBUF_RAW,
			blockSize,
			NTCPIP_PBUF_RAM
		);

		if (pBuf == NULL) {
			/*
			 *	Memory might be fragmented. Try to allocate a smaller block.
			 */

			pBuf = ntcpip_pbufAlloc(
				NTCPIP_PBUF_RAW,
				blockSize >> 1,
				NTCPIP_PBUF_RAM
			);

			if (pBuf == NULL) {
				break;
			}
		}

		deb_assert(pBuf->ref == 1);

		pData->pData = pBuf->payload;
		pData->size = pBuf->len;
		pData->pBlockInfo = pBuf;

		pData++;
	} while (--buffCount);

	return (Uint8) (count - buffCount);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__memFree
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Frees the buffer block.
*
*	\param		pBlockInfo	Pointer to data needed to free the buffer block.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PRIVATE void netif__memFree(
	void *					pVoidInst,
	void *					pBlockInfo
) {
	ntcpip_pbufFree(pBlockInfo);
}
