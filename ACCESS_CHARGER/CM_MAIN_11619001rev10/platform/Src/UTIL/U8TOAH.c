/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		U8TOAH.c
*
*	\ingroup	UTIL
*
*	\brief		Unsigned 8-bit value to hexadecimal ASCII implementation.
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "util.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	util_u8toahex
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts unsigned 8-bit value to hexadecimal ASCII.
*
*	\param		nVal 	Value to be converted.
*	\param		pBuf 	Pointer to the start of the buffer.
*	\param		pEnd 	Pointer to the end of the buffer.
*
*	\return		Pointer to the first character of the ASCII number.
*
*	\details	Two ASCII characters will always be produced regardless of the
*				supplied number. Only one character will be produced if the
*				buffer is too small for two characters.
*
*	\note	
*
*******************************************************************************/

PUBLIC char * util_u8toahex(
	Uint8					nVal,
	char *					pBuf,
	char *					pEnd
) {
	Uint8 nTmp;

	if (pEnd > pBuf) {
		nTmp = nVal & 0x0F;

		if (nTmp < 10) {
			*--pEnd = nTmp + '0';
		} else {
			*--pEnd = (nTmp - 10) + 'A';
		}
	}

	if (pEnd > pBuf) {
		nTmp = (nVal >> 4) & 0x0F;

		if (nTmp < 10) {
			*--pEnd = nTmp + '0';
		} else {
			*--pEnd = (nTmp - 10) + 'A';
		}
	}

	return pEnd;
}
