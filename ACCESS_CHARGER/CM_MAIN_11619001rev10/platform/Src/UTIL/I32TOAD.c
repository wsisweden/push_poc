/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		I32TOAD.c
*
*	\ingroup	UTIL
*
*	\brief		Int32 to decimal string conversion.
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "util.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	util_i32toadec
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts signed 32 bit value to ASCII.
*
*	\param		nVal 	Balue to convert.
*	\param		pBuf 	Target buffer.
*	\param		nSize 	Size of target buffer.
*
*	\return		Pointer to the first character of the ASCII number.
*
*	\details	This function stores the ASCII string to the end of the buffer.
*				The string will not be null-terminated.
*
*	\note	
*
*******************************************************************************/

PUBLIC char * util_i32toadec(
	Int32					nVal,
	char *					pBuf,
	Uint8					nSize
) {
	char * pStart;
	char * pEnd;
	Uint32 unsignedVal;

	/*
	 *	First convert the value to an unsigned value.
	 */

	if (nVal < 0) {
		if (nVal == Int32_MIN) {
			unsignedVal = 2147483648;

		} else {
			unsignedVal = -nVal;
		}

	} else {
		unsignedVal = nVal;
	}

	pEnd = &pBuf[nSize];

	/*
	 *	Write the unsigned value.
	 */

	pStart = util_u32toadec(unsignedVal, pBuf, pEnd);

	/*
	 *	Add the sign if the value is negative.
	 */

	if (nVal < 0) {
		if (pStart > pBuf) {
			pStart--;
			*pStart = '-';
		}
	}

	return pStart;
}
