/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		ATOI16D.C
*
*	\ingroup	UTIL
*
*	\brief		Decimal string to Int16 conversion.
*
*	\note
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "util.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	util_atoi16dec
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert numeric string in decimal format to Int16.
*
*	\param  	pPtr	Pointer to string.
*	\param		len		Length of string. The function will not read beyond this
*						length.
*	\param		ppEnd	Pointer to pointer that will be set to the end of the
*						value.
*
*	\details
*
*	\note		Not tested, copied from ATOI32D.c
*
*	\sa
*
*******************************************************************************/

PUBLIC Int16 util_atoi16dec(
	char const_D *			pPtr,
	Uint16					len,
	char const_D **			ppEnd
) {
	Int16			nRet;
	Uint32			ret_u16;
	Boolean			negative;
	char const_D *	pEnd;

	pEnd = pPtr;

	if (len == 0) {
		*ppEnd = pEnd;
		return(0);
	}

	/*
	 *	Check for sign character and store the information.
	 */

	negative = FALSE;
	if (*pEnd == '-') {
		pEnd++;
		len--;
		negative = TRUE;
	}

	ret_u16 = util_atou16dec(pEnd, len, &pEnd);

	/*
	 *	Check if the number fits into an Int32. This limits the smallest
	 *	possible number to -32767 instead of -32768. Then adding
	 *	the stored sign to the value.
	 */

	nRet = 0;
	if (ret_u16 < Int16_MAX) {
		if (negative) {
			nRet = -(Int16)ret_u16;
		} else {
			nRet = (Int16)ret_u16;
		}

	} else {
		pEnd = pPtr;
	}

	*ppEnd = pEnd;

	return(nRet);
}
