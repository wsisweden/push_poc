/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		U32TOAD.c
*
*	\ingroup	UTIL
*
*	\brief		Unsigned 32 bit value to decimal ASCII implementation.
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "util.h"

#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	util_u32toadec
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts unsigned 32 bit value to decimal ASCII.
*
*	\param		nVal 	Value to be converted.
*	\param		pBuf 	Pointer to the start of the buffer.
*	\param		pEnd 	Pointer to one byte past the end of the buffer.
*
*	\return		Pointer to the first character of the ASCII number.
*
*	\details	This function stores the ASCII string to the end of the buffer.
*				The string will not be null-terminated.
*
*	\note		
*
*******************************************************************************/

PUBLIC char * util_u32toadec(
	Uint32					nVal,
	char *					pBuf,
	char *					pEnd
) {
	do 
	{
		if (pEnd == pBuf) 
		{
			break;
		}

		*--pEnd = (char)((char)(nVal % 10) + '0');
	} while((nVal /= 10) > 0);

	return pEnd;
}
