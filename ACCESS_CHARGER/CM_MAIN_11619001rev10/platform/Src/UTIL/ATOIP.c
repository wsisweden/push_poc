/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		ATOIP.C
*
*	\ingroup	UTIL
*
*	\brief		IP-address string to Uint32 conversion.
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "util.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	util_atoip
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Parse IPv4 address string in quad-dotted notation.
*
*	\param		pString		Pointer to the string.
*	\param		strLen		Length of string. The function will not read beyond
*							this length.
*	\param		ppEnd		Pointer to pointer that will be set to where parsing
*							ended. The ppEnd pointer will be set to pString if
*							parsing fails.
*
*	\return		The IP-address as an Uint32 with the first octet in the string
*				as MSB.
*
*	\details	The function does not verify that the address is a valid host
*				address. Such validation can be done after the address has been
*				parsed. For example the broadcast address 255.255.255.255 is
*				valid for this function.
*
*				The function will stop parsing at the first non-numeric
*				character after the last dot.
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC Uint32 util_atoip(
	char const_D *			pString,
	Uint16					strLen,
	char const_D **			ppEnd
) {
	Uint32			num;
	Uint32			retVal;
	char const_D *	pEnd;

	*ppEnd = pString;

	/*
	 *	Parse first number. Using 32-bit conversion function to better detect
	 *	invalid numbers. For example if 192.168.0.256 is supplied.
	 */

	num = util_atou32dec(pString, strLen, &pEnd);
	if (num > 255) {
		return(0);
	}

	retVal = num << 24;
	strLen -= pEnd - pString;

	/*
	 *	Parse dot.
	 */

	if (strLen == 0 || *pEnd != '.') {
		return(0);
	}

	if (--strLen == 0) {
		return(0);
	}

	pString = ++pEnd;

	/*
	 *	Parse second number.
	 */
	
	num = util_atou32dec(pString, strLen, &pEnd);
	if (num > 255) {
		return(0);
	}

	retVal |= num << 16;
	strLen -= pEnd - pString;

	/*
	 *	Parse dot.
	 */

	if (strLen == 0 || *pEnd != '.') {
		return(0);
	}

	if (--strLen == 0) {
		return(0);
	}

	pString = ++pEnd;

	/*
	 *	Parse third number.
	 */

	num = util_atou32dec(pString, strLen, &pEnd);
	if (num > 255) {
		return(0);
	}

	retVal |= num << 8;
	strLen -= pEnd - pString;

	/*
	 *	Parse dot.
	 */

	if (strLen == 0 || *pEnd != '.') {
		return(0);
	}

	if (--strLen == 0) {
		return(0);
	}

	pString = ++pEnd;

	/*
	 *	Parse fourth number.
	 */

	num = util_atou32dec(pString, strLen, &pEnd);
	if (num > 255) {
		return(0);
	}

	retVal |= num;



	*ppEnd = pEnd;

	return(retVal);
}