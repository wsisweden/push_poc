/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

 /* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DUMMY\INIT.H
*
*	\ingroup	DUMMY
*
*	\brief		Dummy init declarations
*
*	\note		
*
*	\version
*
*******************************************************************************/
 
#ifndef DUMMY_INIT_H_INCLUDED
#define DUMMY_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

/**
 * Dummy init type.
 */
 
typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	Uint8					dummy;		/**<                                */
} eepr_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_DUMMY
#elif defined(EXE_GEN_DUMMY)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_DUMMY
#endif

#define EXE_APPL(n)			n##dummy

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

/* The registers */

/* References to external registers */

/* Write-indications */

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
