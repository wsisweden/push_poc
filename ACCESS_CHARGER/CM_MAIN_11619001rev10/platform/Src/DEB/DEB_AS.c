/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DEB_AS.c
*
*	\ingroup	DEB
*
*	\brief		Assert storage handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "sys.h"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define DEB__ASSERT_MAGIC	0xCCAA

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type for the assert information.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16					magicNum;	/**< Magic number to check init.	*/
	Uint16					rowNum;		/**< Assert row number.				*/
	char					fileName[16];/**< Assert file name.				*/
} deb__AssertInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Storage instance data.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	deb__AssertInfo *	pAssertInfo;	/**< Pointer to the assert info.	*/
} deb__StorageData;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/
SYS_DEFINE_FILE_NAME;

#if TARGET_SELECTED & TARGET_W32
PRIVATE deb__AssertInfo		deb__assertInfo;
#endif

PRIVATE deb__StorageData	deb__storageData = { NULL };

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	deb_initAssertStorage
*	
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the assert storage.
*
*	\param		pAssertStorage	Pointer to ram area where the assert information
*								should be stored. The pointer must be aligned
*								according to the maximum alignment required by
*								the target.
*
*	\return		-
*
*	\details	This function should be called from the main function as soon
*				as possible. It must be called before any assert is made.
*				
*				The supplied pointer should point to a memory area which is not
*				initialized by the startup code. The reason for this is that
*				the data will remain in RAM during a reset as long as the device
*				is powered.
*				
*				The assert information requires 20 bytes space.
*
*	\note			
*
*******************************************************************************/

PUBLIC void deb_initAssertStorage(
	void *					pAssertStorage
) {

#if TARGET_SELECTED & TARGET_W32
	deb__storageData.pAssertInfo = &deb__assertInfo;
#else
	deb__storageData.pAssertInfo = (deb__AssertInfo *) pAssertStorage;
#endif

	if (deb__storageData.pAssertInfo->magicNum != DEB__ASSERT_MAGIC) {
		deb_clearAssertStorage();
	}
	
	deb__storeAssertFn_p = &deb_storeAssert;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	deb_storeAssert
*	
*--------------------------------------------------------------------------*//**
*
*	\brief		Store assert information to dedicated RAM area.
*
*	\param		pFileName	Name of file where assert occurred. This should be
*							a null-terminated string.
*	\param		line		Row number where the assert occurred.
*
*	\return		-
*
*	\details	This function is called automatically when an assert fails if 
*				the assert storage has been configured.
*
*				The function may be called from other places also. For example
*				The Cortex-M Hard Fault could store the program counter value
*				by calling this function.
*				
*	\note			
*
*******************************************************************************/

PUBLIC void deb_storeAssert(
	char const_P *			pFileName,
	Uint16					line
) {
	Ufast16 stringLen;
	char * pName;

	if (deb__storageData.pAssertInfo != NULL) {

		deb__storageData.pAssertInfo->rowNum = line;

		/*
		 *	Search for the terminating null and calculate string length. The
		 *	length is limited to 15 because larger strings will not fit into the
		 *	buffer.
		 *
		 *	If the string does not fit then only the end of the string is
		 *	copied. This way the file name will always be included even if the
		 *	string contains an absolute path.
		 */

		stringLen = 0;
		do {
			stringLen++;
			pFileName++;
		} while (*pFileName != 0);
		pFileName--;

		if (stringLen > 15) {
			stringLen = 15;
		}

		/*
		 *	Copy the string.
		 */

		deb__storageData.pAssertInfo->fileName[0] = (char) stringLen;
		pName = deb__storageData.pAssertInfo->fileName + stringLen;
		do {
			*pName-- = *pFileName--;
		} while (--stringLen);

	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	deb_getAssertRow
*	
*--------------------------------------------------------------------------*//**
*
*	\brief		Read on which row the last assert failed.
*
*	\return		The row number.
*
*	\details	This function may be used to get the row number in a read
*				indication function.
*
*	\note			
*
*******************************************************************************/

PUBLIC Uint16 deb_getAssertRow(
	void
) {
	Uint16 assertRow;

	assertRow = 0;

	if (deb__storageData.pAssertInfo != NULL) {
		assertRow = deb__storageData.pAssertInfo->rowNum;
	}

	return(assertRow);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	f__readAssertFile
*	
*--------------------------------------------------------------------------*//**
*
*	\brief		Get the name of the file where the last assert occurred.
*
*	\param		pBuffer		Pointer to buffer where the file name will be 
*							stored. The buffer size must be at least 16 bytes.
*
*	\return		-
*
*	\details	The first byte of the buffer will contain the length of the 
*				string. The string is not null-terminated.
*				
*				This function may be used to get the filename in a read
*				indication function.
*
*	\note		
*
*******************************************************************************/

PUBLIC void deb_getAssertFile(
	char *					pBuffer
) {
	const char *	pAssertFile;
	Ufast8			charsLeft;

	if (pBuffer != NULL) {

		*pBuffer = 0;

		if (deb__storageData.pAssertInfo != NULL) {
			pAssertFile = deb__storageData.pAssertInfo->fileName;
			charsLeft = *pAssertFile + 1;
			if (charsLeft <= 16) {
				do {
					*pBuffer++ = *pAssertFile++;
				} while (--charsLeft);
			}
		}

	} else {

		/*
		 * TODO: This assert could be replaced
		 * with FOREVER loop or calling a "fatal DEB error"
		 * callback
		 */
		deb_assert(0);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	deb_clearAssertStorage
*	
*--------------------------------------------------------------------------*//**
*
*	\brief		Clear the assert storage.
*
*	\return		
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC void deb_clearAssertStorage(
	void
) {
	if (deb__storageData.pAssertInfo != NULL) {
		deb__storageData.pAssertInfo->magicNum = DEB__ASSERT_MAGIC;
		deb__storageData.pAssertInfo->rowNum = 0;
		deb__storageData.pAssertInfo->fileName[0] = 0;
	}
}
