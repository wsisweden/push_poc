/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DEB\local.h
*
*	\ingroup	DEB
*
*	\brief		Local declarations of the DEB FB
*
*	\details	
*
*	\note		
*
*	\version	
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/
 
#include "osa.h"
#include "deb.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Logger type
 */
 
typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	deb_FnLog				logger;		/**< Logger calback function        */
	osa_SemaType			sema;		/**< Serializer semaphore	        */
	char					buf[16];	/**< Temporary buffer for formatting*/
} deb__Logger;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Type of function storing assert information.
 */

typedef void deb__StoreAssertFn(char const_P * pFileName,Uint16 line);

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern deb__Logger 			deb__logInstance;
extern deb__StoreAssertFn *	deb__storeAssertFn_p;

/******************************************************************************/
