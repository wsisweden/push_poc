/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	PHYIF
*
*	\brief		Synchronous PHYIF wrapper for asynchronous drivers that
*				implement the APIF interface.
*
*	\details	This wrapper can be used to get a PHYIF interface to any driver
*				that implements the APIF interface.
*
*	\note
*
*	\version	\$Rev: 3654 $ \n
*				\$Date: 2019-03-22 15:16:11 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* Platform lib */
#include "tools.h"
#include "mem.h"
#include "deb.h"
#include "osa.h"
#include "apif.h"
#include "phyif.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Instance type for the synchronous APIF interface wrapper.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	volatile Boolean		busy;		/**< Driver busy or not.			*/
	volatile Uint8			pollStatus;	/**< Last known status.				*/
	phyif_RetCode			retCode;	/**< Return code from driver.		*/
	volatile phyif_RetCode	pollRet;	/**< Last poll return code.			*/
	apif_PhyInst *			pAsyncInst;	/**< Pointer to PHY driver instance.*/
	apif_Interface const_P * pApif;		/**< Pointer to async interface.	*/
	osa_SemaType			syncSema;	/**< Synchronization semaphore.		*/
	apif_Req 				req;		/**< APIF request.					*/
} phyif__AsyncInst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void phyif__asyncDone(gai_Req * pReq,gai_RetCode retCode);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* File name for assert */
SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	phyif_asyncInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the synchronous wrapper for an asynchronous PHY
*				driver.
*
*	\param		pApif		Pointer to the asynchronous interface constant of a
*							PHY driver.
*	\param		pAsyncInst	Pointer to the asynchronous driver instance.
*
*	\returns	A pointer to the synchronous PHY driver wrapper.
*
*	\details	Should be called during FB initialization before OSA is
* 				running.
*
*				This function can be used in a PHY driver, which implements the
*				APIF interface. It can be used to initialize a wrapper so that
*				the driver can be accessed through the PHYIF interface.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC phyif_PhyInst * phyif_asyncInit(
	apif_Interface const_P * pApif,
	void *					pAsyncInst
) {
	phyif__AsyncInst * pInst;

	deb_assert(pApif != NULL);
	deb_assert(pAsyncInst != NULL);

	pInst = (phyif__AsyncInst *) mem_reserve(
		&mem_normal,
		sizeof(phyif__AsyncInst)
	);
	deb_assert(pInst != NULL);

	osa_newSemaTaken(&pInst->syncSema);

	pInst->pApif = pApif;
	pInst->busy = FALSE;
	pInst->pAsyncInst = pAsyncInst;

	pInst->req.baseReq.pDoneFn = &phyif__asyncDone;
	pInst->req.baseReq.pNext = NULL;
	pInst->req.baseReq.pUtil = pInst;

	return(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	phyif_asyncWarmInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the PHY chip.
*
*	\param		pVoidInst	Pointer to the PHY driver instance data.
*	\param		options		Initialization option flags.
*
*	\return		PHYIF return code.
*
*	\details	The function should be called from a FB task after OSA has been
* 				started. It is assumed that the calling task can be blocked
* 				while the PHY is being initialized.
*
*	\note
*
*	\sa			phyif_RetCode
*
*******************************************************************************/

PUBLIC phyif_RetCode phyif_asyncWarmInit(
	phyif_PhyInst *			pVoidInst,
	Uint8					options
) {
	phyif__AsyncInst *	pInst;
	phyif_RetCode		retCode;

	deb_assert(pVoidInst != NULL);

	pInst = (phyif__AsyncInst *) pVoidInst;
	
	DISABLE;
	if (pInst->busy == FALSE) {
		pInst->busy = TRUE;
		ENABLE;

		pInst->req.op = APIF_OP_WARMINIT;
		pInst->req.data = options;

		pInst->pApif->pAccessFn(
			pInst->pAsyncInst,
			&pInst->req.baseReq
		);

		osa_semaGet(&pInst->syncSema);

		retCode = pInst->retCode;

		atomic(
			pInst->busy = FALSE;
		);

	} else {
		retCode = GAI_RET_BUSY;
		ENABLE;
	}

	return(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	phyif_asyncPoll
*
*--------------------------------------------------------------------------*//**
*
*	\brief		This function reads the PHY link status.
*
*	\param		pVoidInst	Pointer to the PHY driver instance data.
*	\param		pStatus		Pointer to Uint8 value where the link status change
* 				  			information will be stored.
*
*	\return		PHYIF return code.
*
*	\details	The function should be called when a PHY link change interrupt
* 				is generated. If the PHY does not support link change interrupts
* 				then the function should be called at constant intervals. An
* 				appropriate interval might be 1s.
*
* 				The most common type of link change is when the network cable
* 				is plugged or unplugged.
*
*	\note
*
*	\sa			phyif_RetCode
*
*******************************************************************************/

PUBLIC phyif_RetCode phyif_asyncPoll(
	phyif_PhyInst *			pVoidInst,
	Uint8 *					pStatus
) {
	phyif__AsyncInst *	pInst;
	phyif_RetCode		retCode;

	deb_assert(pVoidInst != NULL);
	deb_assert(pStatus != NULL);

	pInst = (phyif__AsyncInst *) pVoidInst;

	DISABLE;
	if (pInst->busy == FALSE) {
		pInst->busy = TRUE;
		ENABLE;

		pInst->req.op = APIF_OP_POLL;
		pInst->req.data = 0;

		pInst->pApif->pAccessFn(
			pInst->pAsyncInst,
			&pInst->req.baseReq
		);

		/*
		 *	We do not wait for the poll operation to complete here because
		 *	that could block the MAC driver task for a long time. The poll
		 *	operation can take over 100ms if software MDIO is used.
		 *
		 *	Instead of waiting for the poll result we just supply the result
		 *	from the previous polling.
		 *
		 *	This could cause some problems in some cases. A nice way to fix this
		 *	would be to add support for the APIF (Asynchronous PHY Interface)
		 *	to the NDRV FB.
		 */

		atomic(
			*pStatus = pInst->pollStatus;
			retCode = pInst->pollRet;
		);

	} else {
		retCode = GAI_RET_BUSY;
		ENABLE;
	}

	return(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	phyif_asyncDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Do operation function.
*
*	\param		pVoidInst	Pointer to PHY driver instance.
*	\param		op			Operation code.
*
*	\return		PHYIF return code.
*
*	\details	This function is used to request a operation to be performed.
* 				Some operations are not supported by all PHY chips.
*
*	\note
*
*	\sa			phyif_RetCode
*
*******************************************************************************/

PUBLIC phyif_RetCode phyif_asyncDoOp(
	phyif_PhyInst *			pVoidInst,
	phyif_OpCode			op
) {
	phyif__AsyncInst *	pInst;
	phyif_RetCode		retCode;

	deb_assert(pVoidInst != NULL);

	pInst = (phyif__AsyncInst *) pVoidInst;

	DISABLE;
	if (pInst->busy == FALSE) {
		pInst->busy = TRUE;
		ENABLE;

		pInst->req.op = op;
		pInst->req.data = 0;

		pInst->pApif->pAccessFn(
			pInst->pAsyncInst,
			&pInst->req.baseReq
		);

		osa_semaGet(&pInst->syncSema);

		retCode = pInst->retCode;

		atomic(
			pInst->busy = FALSE;
		);

	} else {
		retCode = GAI_RET_BUSY;
		ENABLE;
	}

	return(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	phyif__syncDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set the semaphore to schedule the task that requested the 
*				operation.
*
*	\param		pReq		Pointer to the request structure.
*	\param		retCode		Return code specifying the result of the operation.
*							Can be A GAI or APIF return code.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void phyif__asyncDone(
	gai_Req *				pReq,
	gai_RetCode				retCode
) {
	phyif__AsyncInst *	pInst;
	apif_Req *			pApifReq;

	pInst = (phyif__AsyncInst *) pReq->pUtil;
	pApifReq = (apif_Req *) pReq;

	if (pApifReq->op == APIF_OP_POLL) {
		atomic(
			pInst->pollRet = retCode;
			pInst->pollStatus = (Uint8) pApifReq->data;
			pInst->busy = FALSE;
		);

	} else {
		pInst->retCode = retCode;
		osa_semaSet(&pInst->syncSema);
	}
}
