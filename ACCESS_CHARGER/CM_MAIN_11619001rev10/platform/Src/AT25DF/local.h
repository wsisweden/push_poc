/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	AT25DF
*
*	\brief		Local declarations for the AT25DFxxx flash driver.
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

#ifndef LOCAL_H_INCLUDED
#define LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "swtimer.h"
#include "protif.h"
#include "AT25DF.H"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	Memory page size in bytes.
 */

#define AT25DF__PAGE_SIZE	256

/**
 * 	\name		AT25DF instruction set
 *
 *	\details	The following instructions can be sent to the memory.
 */

/*@{*/
#define AT25DF__OP_WREN		0x06		/**< Write Enable					*/
#define AT25DF__OP_WRDI		0x04		/**< Write Disable					*/
#define AT25DF__OP_WPDIS	0x39		/**< Unprotect block				*/
#define AT25DF__OP_WPENS	0x36		/**< Protect block					*/
#define AT25DF__OP_RDSR		0x05		/**< Read Status Register			*/
#define AT25DF__OP_WRSR		0x01		/**< Write Status Register			*/
#define AT25DF__OP_READ		0x03		/**< Read Data 						*/
#define AT25DF__OP_FREAD	0x0B		/**< Read Data highspeed			*/
#define AT25DF__OP_PP		0x02		/**< Page Program					*/
#define AT25DF__OP_SE		0x20		/**< Sector Erase					*/
#define AT25DF__OP_BE		0xD8		/**< Block Erase					*/
#define AT25DF__OP_CE		0xC7		/**< Chip Erase						*/
#define AT25DF__OP_DP		0xB9		/**< Deep Power-down				*/
#define AT25DF__OP_RES		0xAB		/**< Release from Deep Power-down	*/
#define AT25DF__OP_REMS		0x9F		/**< Read Manufacturer & Device Id	*/
#define AT25DF__OP_BE32		0x52		/**< Block Erase (32 Kbytes)		*/
#define AT25DF__OP_PSECT	0x36		/**< Protect sector					*/
#define AT25DF__OP_UPSECT	0x39		/**< Unprotect sector				*/
/*@}*/

/**
 * AT25DF instruction frame bytes.
 */

enum at25df__frmByte {					/*''''''''''''''''''''''''''''''''''*/
	AT25DF__FRM_INSTRUCTION,			/**< Instruction byte               */
	AT25DF__FRM_ADDRH,					/**< Address byte high              */
	AT25DF__FRM_ADDRM,					/**< Address byte middle			*/
	AT25DF__FRM_ADDRL,					/**< Address byte low				*/
										/*==================================*/
	AT25DF__FRM_LENGTH					/**< Frame length in bytes          */
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * 	\name		AT25DF status register bits
 *
 *	\brief		The AT25DFXX family FLASH memories have a status register that 
 *				can be read and written. The register contains the following
 *				bits.
 */

/*@{*/
#define AT25DF__STAT_WIP	(1<<0)		/**< 1=Write in progress			*/
#define AT25DF__STAT_WEL	(1<<1)		/**< 1=Write enable latch is on		*/
#define AT25DF__STAT_SWP0	(1<<2)		/**< Software write protections status*/
#define AT25DF__STAT_SWP1	(1<<3)		/**< Software write protections status*/
#define AT25DF__STAT_WPP	(1<<4)		/**< Write protect pin status		*/
#define AT25DF__STAT_EPE	(1<<5)		/**< Erase/Program error			*/
#define AT25DF__STAT_SPRL	(1<<7)		/**< 1=Write protection enabled		*/
/*@}*/

/**
 * \name	Supported manufacturers IDs
 */

#define AT25DF__MID_ATMEL	0x1F		/**< Atmel/Adesto manufacturer ID	*/
#define AT25DF__MID_WINBOND	0xEF		/**< Winbond ID						*/

/**
 * \name	Supported device ID numbers
 */

/*@{*/
#define AT25DF__DID_AT25DF041A	0x44	/**< Device ID of AT25DF041A byte 1	*/
#define AT25DF__DID_AT25DF641	0x48	/**< Device ID of AT25DF641 byte 1	*/
/*@}*/

/**
 *	Atmel/Adesto family codes.                                                                     
 *
 * @{
 */
#define AT25DF__FC_AT25DF	0x40		/**< AT25DF/26DFxxx series.			*/
#define AT25DF__FC_AT25SF	0x80		/**< AT25SFxxx series.				*/
/** @} */

/**
 *	State handler table size.
 *	
 *	\see at25df__opTable
 */

#define AT25DF__HANDLER_TABLE_SIZE	11

/**
 * 	\name		AT25DF state flags.
 */

/*@{*/
#define AT25DF__FLG_WIP		(1<<0)		/**< Write/Erase started.			*/
#define AT25DF__FLG_COSTAT	(1<<1)		/**< CoTask should read status.		*/
#define AT25DF__FLG_PROT	(1<<2)		/**< Sector protection in use.		*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef struct at25df__inst at25df__Inst;

/**
 *	Type used for state actions.                                                                     
 */

typedef Uint8				at25df__Action;

/**
 *	Type for all state handler functions.                                                                     
 */

typedef void				at25df__StateFn(at25df__Inst *,at25df__Action);

/**
 *	Type for information about driver state.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint8					step;		/**< Step number.					*/
	at25df__StateFn *		pHandler;	/**< Pointer to state handler.		*/
} at25df__StateInfo;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type for storing state flags.
 */

typedef Uint8				at25df__Flags;

/**
 * Driver instance type.
 */

struct at25df__inst {					/*''''''''''''''''''''''''''''' RAM */
	at25df__Flags			flags;		/**< State flags.					*/
	Uint8					densityCode;/**< Memory size information.		*/
	Uint8					errorCount;	/**< Bus error counter.				*/
	BYTE  					data[5];	/**< Buffer for flash instructions.	*/
	BYTE					vendInfo[4];/**< Vendor specific memory info.	*/
	Uint16					sentBytes;	/**< Number of bytes sent.          */
	Uint16					manId;		/**< Manufacturer id.				*/
	Uint16					pollInterval;/**< Status polling interval.		*/
	memdrv_Alloc *			pFront;		/**< Pointer to FIFO front.			*/
	memdrv_Alloc *			pBack;		/**< Pointer to FIFO back.			*/
	memdrv_Alloc *			pActReq;	/**< Pointer to active request.		*/
	at25df__StateInfo *		pCurrState;	/**< Pointer to current state info.	*/
	at25df_Init const_P *	pInit;		/**< Ptr to driver init data.       */
	Uint32					waitTime;	/**< Time waited for current step.	*/
	at25df__StateInfo		state[3];	/**< Driver state stack.			*/
	osa_CoTaskType			coTask;		/**< CoTask object.					*/
	protif_Data				reqData2;	/**< Second request data block.     */
	protif_Request			spiRequest;	/**< Protocol handler request.      */
	swtimer_Timer_st		pollTmr;	/**< Poll wait timer.				*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	List of possible state machine actions.                                                                     
 */

enum at25df__action {					/*''''''''''''''''''''''''''''''''''*/
	AT25DF__ACTION_ENTRY,				/**< State entry action.			*/
	AT25DF__ACTION_REQ_DONE,			/**< BUS request done successfully.	*/
	AT25DF__ACTION_BUS_ERROR,			/**< BUS request done but failed.	*/
	AT25DF__ACTION_RETURN,				/**< Returned from sub-state.		*/
	AT25DF__ACTION_RETURN_ERR,			/**< Returned error from sub-state.	*/
	AT25DF__ACTION_COTASK				/**< CoTask triggered.				*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* AT25DF.c */
void		at25df__triggerTask(at25df__Inst * pInst);

/* AT25DF_S.C */
void		at25df__protifCallback(Uint8,protif_Request *);
void		at25df__enterState(at25df__Inst *,at25df__StateFn *);

/* AT25DF_I.C */
void		at25df__requestDone(at25df__Inst *,Uint8);
void		at25df__handleRequest(at25df__Inst *);

/* AT25DF_O.C */
void		at25df__readStatus(at25df__Inst *,BYTE *);
void		at25df__enableWrite(at25df__Inst *);
void		at25df__readDevId(at25df__Inst *,BYTE *);
void		at25df__setGlobalWriteProtection(at25df__Inst *,Boolean);
void 		at25df__setBlockWriteProtection(at25df__Inst *,Boolean,Uint32);
void		at25df__writePage(at25df__Inst *);
void		at25df__readData(at25df__Inst *);
void		at25df__eraseSector(at25df__Inst *,Uint32);
void		at25df__eraseChip(at25df__Inst *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern at25df__StateFn * const_P at25df__opTable[];

/***************************************************************//** \endcond */

#endif
