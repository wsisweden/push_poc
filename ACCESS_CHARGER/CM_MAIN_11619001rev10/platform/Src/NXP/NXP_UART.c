/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	NXP
*
*	\brief		Baudrate calculation.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3062 $ \n
*				\$Date: 2016-10-10 15:38:17 +0300 (ma, 10 loka 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "nxp.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Table containing the FDR register value.
 */

PRIVATE Uint8 const_P uart__mulDivVals[] = {
	(10<<4) | 1,
	(9<<4) | 1,
	(8<<4) | 1,
	(15<<4) | 2,
	(7<<4) | 1,
	(13<<4) | 2,
	(6<<4) | 1,
	(11<<4) | 2,
	(5<<4) | 1,
	(14<<4) | 3,
	(9<<4) | 2,
	(13<<4) | 3,
	(4<<4) | 1,
	(15<<4) | 4,
	(11<<4) | 3,
	(7<<4) | 2,
	(10<<4) | 3,
	(13<<4) | 4,
	(3<<4) | 1,
	(14<<4) | 5,
	(11<<4) | 4,
	(8<<4) | 3,
	(13<<4) | 5,
	(5<<4) | 2,
	(12<<4) | 5,
	(7<<4) | 3,
	(9<<4) | 4,
	(11<<4) | 5,
	(13<<4) | 6,
	(15<<4) | 7,
	(2<<4) | 1,
	(15<<4) | 8,
	(13<<4) | 7,
	(11<<4) | 6,
	(9<<4) | 5,
	(7<<4) | 4,
	(12<<4) | 7,
	(5<<4) | 3,
	(13<<4) | 8,
	(8<<4) | 5,
	(11<<4) | 7,
	(14<<4) | 9,
	(3<<4) | 2,
	(13<<4) | 9,
	(10<<4) | 7,
	(7<<4) | 5,
	(11<<4) | 8,
	(15<<4) | 11,
	(4<<4) | 3,
	(13<<4) | 10,
	(9<<4) | 7,
	(14<<4) | 11,
	(5<<4) | 4,
	(11<<4) | 9,
	(6<<4) | 5,
	(13<<4) | 11,
	(7<<4) | 6,
	(15<<4) | 13,
	(8<<4) | 7,
	(9<<4) | 8,
	(10<<4) | 9
};

/**
 * Table containing pre-calculated fractional values.
 */

PRIVATE Uint16 const_P uart__fractions[] = {
	1100,
	1111,
	1125,
	1133,
	1143,
	1154,
	1167,
	1182,
	1200,
	1214,
	1222,
	1231,
	1250,
	1267,
	1273,
	1286,
	1300,
	1308,
	1333,
	1357,
	1364,
	1375,
	1385,
	1400,
	1417,
	1429,
	1444,
	1455,
	1462,
	1467,
	1500,
	1533,
	1538,
	1545,
	1556,
	1571,
	1583,
	1600,
	1615,
	1625,
	1636,
	1643,
	1667,
	1692,
	1700,
	1714,
	1727,
	1733,
	1750,
	1769,
	1778,
	1786,
	1800,
	1818,
	1833,
	1846,
	1857,
	1867,
	1875,
	1889,
	1900
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	nxp_uartCalcBaudrate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate values for baudrate setting registers.
*
*	\param		pclk		UART peripheral clock rate.
*	\param		baudrate	The desired baudrate.	
*	\param		pSettings	Pointer to structure which will be filled by the
*							function.
*
*	\details	The function will calculate the FDR, DLM and DLL register
*				values.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void nxp_uartCalcBaudrate(
	Uint32					pclk,
	Uint32					baudrate,
	nxp_UartCfg *			pSettings
) {
	if (pclk % (16 * baudrate)) {
		Uint32	frEst;
		Uint32	ii;

		/*
		 *  The divVal and mulVal must be calculated to get a minimal error.
		 */

		{
			Uint32	dlEst;

			ii = 0;
			pclk = (pclk >> 4) * 1000;

			do {
				frEst = uart__fractions[ii++];
				dlEst = pclk / (baudrate * frEst);
				frEst = pclk / (baudrate * dlEst);

				if (frEst > 1100 && frEst < 1900) {
					break;
				}

			} while (ii < dim(uart__fractions));

			deb_assert(ii < dim(uart__fractions));

			pSettings->dll = (Uint8) (dlEst & 0xFF);
			pSettings->dlm = (Uint8) (dlEst >> 8);
		}

		/*
		 * Find the closest match from the lookup table using binary search.
		 */
		
		{
			Uint32					high;
			Uint32					mid;

			ii = 0;
			high = 60;
			
			do {
				mid = ii + ((high - ii) >> 1);
				
				if (uart__fractions[mid] < frEst) {
					ii = mid + 1;
					mid = high;
				} 

				high = mid; 

			} while (ii < high);
		}

		/*
		 * The previous value in the lookup table might be a closer match.
		 */
		
		{
			Uint32 diff;

			diff = uart__fractions[ii] - frEst;
		
			if ((frEst - uart__fractions[ii-1]) < diff)
			{
				ii--;
			}
		}

		/*
		 * Store the calculated value
		 */

		pSettings->fdr = uart__mulDivVals[ii];

	} else {
		Uint32 fDiv;

		/*
		 * No need to calculate divVal and mulVal with this PCLK and baud rate.
		 */
		
		fDiv = pclk / (16 * baudrate);

		pSettings->dlm = (Uint8) (fDiv / 256);							
		pSettings->dll = (Uint8) (fDiv % 256);
		pSettings->fdr = (1 << 4);
	}
}
