/*-----------------------------------------------------------------------
/  Low level disk interface modlue include file   (C)ChaN, 2010
/-----------------------------------------------------------------------*/

#ifndef DISKIO__H_INCLUDED

#define FAT__DIO_READONLY	0	/* 1: Remove write functions */
#define FAT__DIO_USE_IOCTL	1	/* 1: Use fat__dioIoctl fucntion */

#include "integer.h"


/* Status of Disk Functions */
typedef BYTE	fsm__DioStatus;

/* Results of Disk Functions */
typedef enum {
	FAT__DIO_RESULT_OK = 0,		/* 0: Successful */
	FAT__DIO_RESULT_ERROR,		/* 1: R/W Error */
	FAT__DIO_RESULT_WRPRT,		/* 2: Write Protected */
	FAT__DIO_RESULT_NOTRDY,		/* 3: Not Ready */
	FAT__DIO_RESULT_PARERR		/* 4: Invalid Parameter */
} fsm__DioResult;


/*---------------------------------------*/
/* Prototypes for disk control functions */

int fsm__dioAssignDrives (int, int);
fsm__DioStatus fat__dioInitialize (BYTE);
fsm__DioStatus fat__dioStatus (BYTE);
fsm__DioResult fat__dioRead (BYTE, BYTE*, Uint32, BYTE);
#if	FAT__DIO_READONLY == 0
fsm__DioResult fat__dioWrite (BYTE, const BYTE*, Uint32, BYTE);
#endif
fsm__DioResult fat__dioIoctl (BYTE, BYTE, void*);



/* Disk Status Bits (fsm__DioStatus) */

#define FAT__DIO_STATUS_NOINIT		0x01	/* Drive not initialized */
#define FAT__DIO_STATUS_NODISK		0x02	/* No medium in the drive */
#define FAT__DIO_STATUS_PROTECT		0x04	/* Write protected */


/* Command code for disk_ioctrl fucntion */

/* Generic command (defined for FatFs) */
#define FAT__DIO_CTRL_SYNC			0	/* Flush disk cache (for write functions) */
#define FAT__DIO_GET_SECTOR_CNT	1	/* Get media size (for only fat__fatfsMkfs()) */
#define FAT__DIO_GET_SECTOR_SZ		2	/* Get sector size (for multiple sector size (FAT_MAX_SS >= 1024)) */
#define FAT__DIO_GET_BLOCK_SZ		3	/* Get erase block size (for only fat__fatfsMkfs()) */
#define CTRL_ERASE_SECTOR	4	/* Force erased a block of sectors (for only FAT_USE_ERASE) */

/* Generic command */
#define FAT__DIO_CTRL_PWR			5	/* Get/Set power status */
#define FAT__DIO_CTRL_LCK			6	/* Lock/Unlock media removal */
#define FAT__DIO_CTRL_EJECT			7	/* Eject media */

/* FAT__DIO_MMC/SDC specific ioctl command */
#define FAT__DIO_MMC_GET_TYPE		10	/* Get card type */
#define FAT__DIO_MMC_GET_CSD			11	/* Get CSD */
#define FAT__DIO_MMC_GET_CID			12	/* Get CID */
#define FAT__DIO_MMC_GET_OCR			13	/* Get OCR */
#define FAT__DIO_MMC_GET_SDSTAT		14	/* Get SD status */

/* FAT__DIO_ATA/CF specific ioctl command */
#define FAT__DIO_ATA_GET_REV			20	/* Get F/W revision */
#define FAT__DIO_ATA_GET_MODEL		21	/* Get model name */
#define FAT__DIO_ATA_GET_SN			22	/* Get serial number */

/* NAND specific ioctl command */
#define NAND_FORMAT			30	/* Create physical format */


#define DISKIO__H_INCLUDED
#endif


