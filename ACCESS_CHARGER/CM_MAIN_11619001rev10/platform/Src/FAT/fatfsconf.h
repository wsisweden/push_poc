/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		fatfsconf.h
*
*	\ingroup	FAT
*
*	\brief		FATFS configuration defines.
*
*	\details	These defines can be overridden in a project specific header.
*				The fsm_fcfg.h file can be added to the project include folder
*				to override settings made here.
*
*				This file only defines settings that has not been defined 
*				previously.
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

#ifndef FAT__FFCONF
#define FAT__FFCONF 8255	/**< Fatfs Revision ID */

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "osa.h"
#include "fat_cfg.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	FATFS configuration defines
 *
 * \brief	These defines modify the FATFS and other FAT FB code.
 *
 * \details	Default values are set in the fatfsconf.h file in the FAT directory.
 *			If project specific settings are required then a file named
 *			fsm_fcfg.h can be added to the EXE include directory. Defines that
 *			are done there will override the default settings. 
 *
 *			Modification of the defines will always require a full
 *			re-compilation of FAT FB.
 */
/*@{*/

/*-----------------------------------------------------------------------------/
/ Function and Buffer Configurations
/-----------------------------------------------------------------------------*/

#ifndef FAT_TINY
/**
 *	When FAT_TINY is set to 1, FatFs uses the sector buffer in the file
 *	system object instead of the sector buffer in the individual file object for
 *	file data transfer. This reduces memory consumption 512 bytes each file
 *	object. 
 *	- 0:Normal 
 *	- 1:Tiny
 */
#define	FAT_TINY		0
#endif

#ifndef FAT_READONLY
/** Setting FAT_READONLY to 1 defines read only configuration. This removes
 *	writing functions, fat__fatfsWrite, fat__fatfsSync, fat__fatfsUnlink,
 *	fat__fatfsMkDir, fat__fatfsChMod, fat__fatfsRename,
 *  fat__fatfsTruncate and useless fat__fatfsGetFree. 
 *	- 0:Read/Write
 *	- 1:Read only
 */
#define FAT_READONLY	0
#endif

#ifndef FAT_MINIMIZE
/** The FAT_MINIMIZE option defines minimization level to remove some
 *	functions.
 *  - 0: Full function.
 *  - 1: fat__fatfsStat, fat__fatfsGetFree, fat__fatfsUnlink, fat__fatfsMkDir,
 *	     fat__fatfsChMod, fat__fatfsTruncate and fat__fatfsRename are removed.
 *  - 2: fat__fatfsOpenDir and fat__fatfsReadDir are removed in addition to 1.
 *  - 3: fat__fatfsLseek is removed in addition to 2. 
 */
#define FAT_MINIMIZE	0
#endif

/** To enable string functions, set FAT__USE_STRFUNC to 1 or 2. 
 *	- 0:Disable 
 *	- 1/2:Enable
 *
 *	FSM does not support string functions.
 */
#define	FAT__USE_STRFUNC	0

#ifndef FAT_USE_MKFS
/** To enable fat__fatfsMkfs function, set FAT_USE_MKFS to 1 and set
 *	FAT_READONLY to 0 
 *	- 0:Disable 
 *	- 1:Enable
 */
#define	FAT_USE_MKFS	0
#endif

/** To enable fat__fatfsForward function, set FAT__USE_FORWARD to 1 and set
 *	FAT_TINY to 1. 
 *	- 0:Disable
 *	- 1:Enable
 */
#define	FAT__USE_FORWARD	0

/**	
 *	To enable fast seek feature, set FAT__USE_FASTSEEK to 1. 
 *	When FAT__USE_FASTSEEK is set to 1 and cltbl member in the file object is
 *	not NULL, the fast seek feature is enabled. This feature enables fast
 *	backward/long seek operations without FAT access by cluster link information
 *	stored on the user defined table. The cluster link information must be
 *	created prior to do the fast seek. The required size of the table is (number
 *	of fragments + 1) * 2 items. For example, when the file is fragmented in 5,
 *	12 items will be required to store the cluster link information. The file
 *	size cannot be expanded when the fast seek feature is enabled.
 *
 *	- 0:Disable
 *	- 1:Enable
 *
 *	FSM does not currently support fast seek. FSM should always be able to 
 *	change file size.
 */
#define	FAT__USE_FASTSEEK	0

/*-----------------------------------------------------------------------------/
/ Locale and Namespace Configurations
/-----------------------------------------------------------------------------*/

#ifndef FAT_CODE_PAGE
/** 
 *	The FAT_CODE_PAGE specifies the OEM code page to be used on the target
 *	system. Incorrect setting of the code page can cause a file open failure.
 *  - 932  - Japanese Shift-JIS (DBCS, OEM, Windows)
 *  - 936  - Simplified Chinese GBK (DBCS, OEM, Windows)
 *  - 949  - Korean (DBCS, OEM, Windows)
 *  - 950  - Traditional Chinese Big5 (DBCS, OEM, Windows)
 *  - 1250 - Central Europe (Windows)
 *  - 1251 - Cyrillic (Windows)
 *  - 1252 - Latin 1 (Windows)
 *  - 1253 - Greek (Windows)
 *  - 1254 - Turkish (Windows)
 *  - 1255 - Hebrew (Windows)
 *  - 1256 - Arabic (Windows)
 *  - 1257 - Baltic (Windows)
 *  - 1258 - Vietnam (OEM, Windows)
 *  - 437  - U.S. (OEM)
 *  - 720  - Arabic (OEM)
 *  - 737  - Greek (OEM)
 *  - 775  - Baltic (OEM)
 *  - 850  - Multilingual Latin 1 (OEM)
 *  - 858  - Multilingual Latin 1 + Euro (OEM)
 *  - 852  - Latin 2 (OEM)
 *  - 855  - Cyrillic (OEM)
 *  - 866  - Russian (OEM)
 *  - 857  - Turkish (OEM)
 *  - 862  - Hebrew (OEM)
 *  - 874  - Thai (OEM, Windows)
 *  - 1    - ASCII only (Valid for non LFN cfg.)
 */
#define FAT_CODE_PAGE	1252
#endif

#ifndef FAT_USE_LFN
/** The FAT_USE_LFN option switches the LFN support.
 *  - 0: Disable LFN feature. FAT_MAX_LFN and FAT_LFN_UNICODE have no
 *	  effect.
 *  - 1: Enable LFN with static working buffer on the BSS. Always NOT reentrant.
 *  - 2: Enable LFN with dynamic working buffer on the STACK.
 *  - 3: Enable LFN with dynamic working buffer on the HEAP.
 *
 *	The LFN working buffer occupies (FAT_MAX_LFN + 1) * 2 bytes. To enable
 *	LFN, Unicode handling functions fat__fatfsConvert() and fat__fatfsWtoupper()
 *	must be added to the project. When enable to use heap, memory control
 *	functions ff_memalloc() and ff_memfree() must be added to the project.
 */
#define	FAT_USE_LFN	0
#endif

#ifndef FAT_MAX_LFN
/** Maximum LFN length to handle (12 to 255) */
#define	FAT_MAX_LFN	255		
#endif

#ifndef FAT_LFN_UNICODE
/** To switch the character code set on FatFs API to Unicode,
 *  enable LFN feature and set FAT_LFN_UNICODE to 1. 
 *	- 0:ANSI/OEM
 *	- 1:Unicode
 */
#define	FAT_LFN_UNICODE	0
#endif

/** The FAT__RPATH option configures relative path feature.
 *  - 0: Disable relative path feature and remove related functions.
 *  - 1: Enable relative path. fat__fatfsChDrive() and fat__fatfsChDir() are
 *	     available.
 *  - 2: f_getcwd() is available in addition to 1.
 *
 *	Note that output of the fat__fatfsReadDir function is affected by this
 *	option. 
 *
 *	FSM does not support relative paths. All paths must be absolute.
 */
#define FAT__RPATH	0

/*-----------------------------------------------------------------------------/
/ Physical Drive Configurations
/-----------------------------------------------------------------------------*/

#ifndef FAT_VOLUMES
/** Number of volumes (logical drives) to be used. */
#define FAT_VOLUMES	1
#endif

#ifndef FAT_MAX_SS
/** Maximum sector size to be handled.
 *  Always set 512 for memory card and hard disk but a larger value may be
 *  required for floppy disk (512/1024) and optical disk (512/2048).
 *  When FAT_MAX_SS is larger than 512, FAT__DIO_GET_SECTOR_SZ command must
 *	be implememted to the fsm__dioIoctl function.
 *	- 512
 *	- 1024
 *	- 2048
 *	- 4096
 */
#define	FAT_MAX_SS		512
#endif

/** When set to 0, each volume is bound to the same physical drive number and
 * it can mount only first primary partition. When it is set to 1, each volume
 * may contain multiple partitions. 
 *	- 0: Single partition
 *	- 1: Multiple partition
 *
 * This has been set on because FSM uses the (slightly modified) multi partition
 * feature.
 */
#define	FAT__MULTI_PARTITION	1

#ifndef FAT_USE_ERASE
/** To enable sector erase feature, set FAT_USE_ERASE to 1. 
 *	- 0:Disable
 *	- 1:Enable
 */
#define	FAT_USE_ERASE	0
#endif


/*-----------------------------------------------------------------------------/
/ System Configurations
/-----------------------------------------------------------------------------*/

/** Set 0 first and it is always compatible with all platforms. The
 *	FAT__WORD_ACCESS option defines which access method is used to the word
 *	data on the FAT volume.
 *  - 0: Byte-by-byte access.
 *  - 1: Word access. Do not choose this unless following condition is met.
 *
 *  When the byte order on the memory is big-endian or address miss-aligned word
 *  access results incorrect behavior, the FAT__WORD_ACCESS must be set to
 *	0. If it is not the case, the value can also be set to 1 to improve the
 *  performance and code size.
 *
 *	This feature has been set to zero because the platform should only contain
 *	portable code.
 */
#define FAT__WORD_ACCESS	0

/** The FSM_FAT_REENTRANT option switches the reentrancy of the FatFs module.
 *   - 0: Disable reentrancy. FSM_FAT_SYNC_T and FSM_FAT_TIMEOUT have no effect.
 *   - 1: Enable reentrancy. Also user provided synchronization handlers,
 *     fsm__fatReqGrant, fsm__fatRelGrant, fsm__fatDelSyncObj and
 *	 fsm__fatCreSyncObj function must be added to the project.
 *
 *	The reentrant mode should not be enabled when FSM is used. Reentrancy is
 *	handled on a higher level and does not need to be handled in FATFS.
 */
#define FAT__REENTRANT	0

#ifndef FAT_FS_SHARE
/** To enable file shareing feature, set _FS_SHARE to 1 or greater. The value
 *  defines how many files can be opened simultaneously. 
 *	- 0:Disable
 *	- >=1:Enable
 */
#define	FAT_FS_SHARE	2
#endif

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

/*@}*/

#endif

