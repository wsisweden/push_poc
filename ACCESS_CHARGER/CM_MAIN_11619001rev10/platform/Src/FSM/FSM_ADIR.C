/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		FSM_ADIR.C
*
*	\ingroup	FSM
*
*	\brief		FSM API file listing functions.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
********************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"
#include "fsm.h"
#include "fsm_fs.h"

/* Local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm_opendir
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Opens a directory and fetches the directory object.
*
*	\param		path		Absolute path to the directory
*	\param		pRetCode	The return code will be copied to this location.
*
*	\return		Pointer to the directory object.
*
*	\details	Files should not be created/removed in a directory after opendir
*				has been called. Modifications can be done after closedir has
*				been called.
*
*	\par		Error codes
*				- FSM_RET_OK			The function succeeded and the directory
*										object is created. It is used for
*										subsequent calls to read the directory
*										entries.
*				- FSM_RET_NOT_FOUND		Could not find the path.
*				- FSM_RET_INVAL_PATH	The path name is invalid.
*				- FSM_RET_DISK_ERROR	Low level disk error.
*				- FSM_RET_NOT_MOUNTED	The drive is not mounted.
*				- FSM_RET_MEM			No free directory objects.
*				- FSM_RET_TIMEOUT		Timeout while waiting for drive lock.
*				- FSM_RET_ERROR			Some other error.
*
*	\note
*
*******************************************************************************/

PUBLIC fsm_Dir * fsm_opendir(
	char *					path,
	fsm_RetCode *			pRetCode
) {
	fsm_Dir * pDir;

	pDir = NULL;
	if (path[0] <= 'z' && (path[0] - 'a') < fsm__logCount) {
		fsm_FsIf const_P *	pFsIf;
		fsm_RetCode			dummy;

		atomic(
			pFsIf = fsm__ramLogDrives[path[0] - 'a'].pFsIf;
		)
		
		if (pRetCode == NULL) {
			pRetCode = &dummy;
		}

		*pRetCode = FSM_RET_NOT_MOUNTED;
		
		if (pFsIf != NULL) {
			deb_assert(pFsIf->opendirFn != NULL);
			pDir = pFsIf->opendirFn(path, pRetCode);
		}

	} else {
		*pRetCode = FSM_RET_INVAL_PATH;
	}

	return(pDir);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm_readdir
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reads the next entry from a directory.
*
*	\param		pDir		Pointer to a directory object of a opened directory.
*	\param		pDirEntry	Pointer to a directory entry object that will be 
*							filled by this function.
*
*	\retval		FSM_RET_OK			The function succeeded.
*	\retval		FSM_RET_DISK_ERROR	Low level disk error.
*	\retval		FSM_RET_NOT_MOUNTED	The drive is not mounted.
*	\retval		FSM_RET_TIMEOUT		Timeout while waiting for drive lock.
*	\retval		FSM_RET_ERROR		Some other error.
*
*	\details	The fsm_readdir function reads directory entries in sequence.
*				All  items in the directory can be read by calling fsm_readdir
*				function repeatedly. When all directory entries have been read
*				and there is no no more items to read, the function returns a
*				null string into name member without any error.
*
*				The filename buffer and size must be initialized to the 
*				pDirEntry struct before calling this function. The nameSize
*				field should contain the available buffer size.
*
*				If a file is removed from or added to the directory after the
*				most recent call to fsm_opendir, then that file may or may not
*				be returned by fsm_readdir.
*
*	\note		
*
*******************************************************************************/

PUBLIC fsm_RetCode fsm_readdir(
	fsm_Dir *				pDir,
	fsm_Dirent *			pDirEntry
) {
	deb_assert(pDir != NULL);
	deb_assert(pDir->pFsIf != NULL);
	deb_assert(pDir->pFsIf->readdirFn != NULL);

	deb_assert(pDirEntry != NULL);
	deb_assert(pDirEntry->name != NULL);
	deb_assert(pDirEntry->nameSize != 0);

	return(pDir->pFsIf->readdirFn(pDir, pDirEntry));
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm_closedir
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Closes a directory.
*
*	\param		pDir		Pointer to a directory object of a opened directory.
*
*	\retval		FSM_RET_OK			The file/directory was renamed/moved.
*	\retval		FSM_RET_NOT_MOUNTED	The drive is not mounted.
*
*	\details	The closedir function must be called to close a directory.
*
*	\note		
*
*******************************************************************************/

PUBLIC fsm_RetCode fsm_closedir(
	fsm_Dir *				pDir
) {
	deb_assert(pDir != NULL);
	deb_assert(pDir->pFsIf != NULL);
	deb_assert(pDir->pFsIf->closedirFn != NULL);

	return(pDir->pFsIf->closedirFn(pDir));
}
