/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	FSM
*
*	\brief		File system manager local declarations.
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

#ifndef FSM_LOCAL_H_INCLUDED
#define FSM_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "memdrv.h"
#include "fsm.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Logical drive status flags
 *
 * \breif	These flags should be used with the flags field of the fsm__LogDrive
 *			struct.
 *
 * \see		fsm__LogDrive
 */

/*@{*/
#define FSM__LOGFLG_MOUNT	(1<<0)		/**< Mounting/unmounting in progress*/
#define FSM__LOGFLG_UNMOUNT	(1<<1)		/**< Has been manually unmounted	*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 *	\brief	FS class init function type.
 *
 *	\param	logDrv	Logical drive number.
 */

typedef void fsm__TypeInitFn(void);

/**
 *	\brief	Check if media is present.
 *
 *	\param	logDrv	Logical drive number.
 */

typedef Boolean fsm__MediaFn(Uint8 logDrv);

/**
 *	File system type interface. All file system types must implement a interface
 *	of this type.
 */

struct fsm__fsClassIf {					/*''''''''''''''''''''''''''' CONST */
	Uint8					fsClass;	/**< File system class.				*/
	fsm__TypeInitFn *		initFn;		/**< FS type handler initialization.*/
	fsm__MediaFn *			pollFn;		/**< Media present polling function.*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
