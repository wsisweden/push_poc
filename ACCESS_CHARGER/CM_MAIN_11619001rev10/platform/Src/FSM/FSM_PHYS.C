/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		FSM_PHYS.C
*
*	\ingroup	FSM
*
*	\brief		Physical memory handling.
*
*	\details	This file contains the physical memory file system class
*				handling.
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$	
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
********************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "mem.h"
#include "deb.h"
#include "fsm.h"
#include "fsm_fs.h"

#include "local.h"
#include "LOCAL_PH.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void		fsm__physInit(void);
PRIVATE Boolean		fsm__physIsMediaPresent(fsm_LogDrvNum);
PRIVATE void		fsm__physCallback(memdrv_Alloc *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Public interface for the physical memory file system class.
 */

PUBLIC fsm__FsClassIf const_P fsm_physIf = {
	FSM_FSCLASS_PHYS,
	fsm__physInit,
	fsm__physIsMediaPresent
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Physical memory manager data.
 */

PUBLIC fsm__PhysData 			fsm__physData = { 0 };

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm__physInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize physical drive handler.
*
*	\return		Result of the operation.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void fsm__physInit(
	void
) {
	if not(fsm__physData.status & FSM__PHYSFLG_INIT) {
		Uint8 ii;

		/*
		 * Allocate physical drive data structures.
		 */

		fsm__physData.physDrives = (fsm__PhysDrv *) mem_reserve(
			&mem_normal,
			sizeof(fsm__PhysDrv) * fsm__physCount
		);
		deb_assert(fsm__physData.physDrives != NULL);

		/*
		 * Initialize the physical drive structures.
		 */

		ii = fsm__physCount;
		while (ii--) {
			osa_newSemaTaken(&fsm__physData.physDrives[ii].reqSema);
			fsm__physData.physDrives[ii].statusFlags = 0;
		};

		fsm__physData.status |= FSM__PHYSFLG_INIT;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm__physExecRequest
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Execute MEMDRV request on physical drive.
*
*	\param		physDrv		Physical drive number.
*	\param		pRequest	Pointer to the request.
*
*	\return		Result of the operation.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC memdrv_RetCode fsm_physExecRequest(
	Uint8					physDrv,
	fsm_PhysRequest *		pRequest
) {
	memdrv_fnAccess 		pAccesFn;
	fsm__PhysDrv *			pPhysDrvInfo;

	pPhysDrvInfo = &fsm__physData.physDrives[physDrv];

	pRequest->request.fnDone = fsm__physCallback;
	pRequest->physDrv = physDrv;

	/*
	 *	Select the function and call it.
	 */

	deb_assert(fsm_physDrives[physDrv].pDrvIf != NULL);

	switch (pRequest->request.op) {
		case MEMDRV_OP_PUT:
			pAccesFn = fsm_physDrives[physDrv].pDrvIf->put;
			break;

		case MEMDRV_OP_GET:
			pAccesFn = fsm_physDrives[physDrv].pDrvIf->get;
			break;

		default:
			pAccesFn = fsm_physDrives[physDrv].pDrvIf->doOp;
			break;
	}

	pAccesFn(
		*fsm_physDrives[physDrv].ppDrvInst,
		(memdrv_Alloc *) pRequest
	);

	/*
	 *	Wait for the request to complete if the driver is asynchronous.
	 */
    if ((fsm_physDrives[physDrv].pDrvIf->flags & MEMDRV_FUNC_SYNCHRONOUS) == 0) {
        osa_semaGet(&pPhysDrvInfo->reqSema);
    }

	return((memdrv_RetCode) pRequest->request.count);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm__physInitDrive
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize physical memory.
*
*	\param		physDrv		Physical drive number.
*
*	\return		-
*
*	\details	Called when the drive is mounted.
*
*	\note
*
*******************************************************************************/

PUBLIC fsm_RetCode fsm_physInitDrive(
	fsm_PhysDrvNum			physDrv
) {
	fsm_PhysRequest			request;
	memdrv_RetCode			result;
	fsm_RetCode				retCode;

	retCode = FSM_RET_ERROR;

	/*
	 * Initialize memory.
	 */

	request.physDrv = physDrv;

	request.request.address = 0;
	request.request.count = 0;
	request.request.op = MEMDRV_OP_INIT_MEM;
	request.request.pData = 0;
	result = fsm_physExecRequest(physDrv, &request);

	if (result == MEMDRV_OK || result == MEMDRV_NOT_SUPPORTED) {
		/*
		 * Read memory info.
		 */

		request.request.count = 0;
		request.request.op = MEMDRV_OP_GET_INFO;
		request.request.pData =
			(void *) &fsm__physData.physDrives[physDrv].memInfo;
		result = fsm_physExecRequest(physDrv, &request);

		if (result == MEMDRV_OK) {
			if (fsm__physData.physDrives[physDrv].memInfo.sectorSize == 1) {
				/*
				 * The memory is not split into blocks (Maybe EEPROM). Use
				 * default block size.
				 */
				fsm__physData.physDrives[physDrv].memInfo.sectorSize = 512;
				fsm__physData.physDrives[physDrv].memInfo.sectorCount /= 512;
			}

			atomic(
				fsm__physData.physDrives[physDrv].statusFlags |=
					FSM__PHYSDFLG_INIT;
			)

			retCode = FSM_RET_OK;

		} else if (result == MEMDRV_NOT_SUPPORTED) {
			/*
			 * The memory driver does not support the GET_INFO operation.
			 * Set default values.
			 */
			
			fsm__physData.physDrives[physDrv].memInfo.sectorCount = 0;
			fsm__physData.physDrives[physDrv].memInfo.sectorSize = 512;
			fsm__physData.physDrives[physDrv].memInfo.flags = 0;

			atomic(
				fsm__physData.physDrives[physDrv].statusFlags |=
					FSM__PHYSDFLG_INIT;
			)

			retCode = FSM_RET_OK;
		}
	}

	return(retCode);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm__physMediaIsPresent
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check if media is present.
*
*	\param		physDrv		Physical drive number.
*
*	\retval		TRUE		Media is present.
*	\retval		FALSE		Media is not present.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC Boolean fsm_physMediaIsPresent(
	fsm_PhysDrvNum			physDrv
) {
	fsm_PhysRequest			request;
	Boolean					mediaPresent;
	fsm__PhysDrv *			pPhysDrive;
	memdrv_RetCode			retCode;

	pPhysDrive = &fsm__physData.physDrives[physDrv];

	request.request.address = 0;
	request.request.count = 0;
	request.request.op = MEMDRV_OP_MEDIA_PRESENT;
	request.request.pData = 0;

	retCode = fsm_physExecRequest(physDrv, &request);

	if (retCode == MEMDRV_MEDIA_IS_PRESENT) {
		atomic(pPhysDrive->statusFlags |= FSM__PHYSDFLG_MEDIA;)
		mediaPresent = TRUE;
	} else {
		atomic(pPhysDrive->statusFlags &= ~FSM__PHYSDFLG_MEDIA;)
		mediaPresent = FALSE;
	}

	return(mediaPresent);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm__physIsMediaPresent
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Ask a physical drive if media is present.
*
*	\param		logDrv		Logical drive number.
*
*	\return		Result of the operation.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE Boolean fsm__physIsMediaPresent(
	fsm_LogDrvNum			logDrv
) {
	return(
		fsm_physMediaIsPresent(
			((fsm_PhysMnt *) fsm_logDrives[logDrv].pArgs)->physDrvIdx
		)
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	fsm__physCallback
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Callback function called by memory drivers.
*
*	\param		pRequest	Pointer to the request that has been handled.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void fsm__physCallback(
	memdrv_Alloc *			pRequest
) {
	fsm_PhysRequest *		pDioReq;

	pDioReq = (fsm_PhysRequest *) pRequest;

	osa_semaSet(&fsm__physData.physDrives[pDioReq->physDrv].reqSema);
}
