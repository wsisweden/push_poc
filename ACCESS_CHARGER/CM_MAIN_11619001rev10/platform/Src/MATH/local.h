/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		MATH\LOCAL.H
*
*	\ingroup	MATH
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MATH	MATH
*
*	\brief		MATH functions
*
********************************************************************************
*
*	\details	
*				
*******************************************************************************/

#ifndef MATH_LOCAL_H_INCLUDED
#define MATH_LOCAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/


/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/**
 * This macro is called repeatedly to obtain the square root
 */
#define INNER_ISQRT(s)							\
	temp = (g << (s)) + (1 << ((s) * 2 - 2));	\
	if (val >= temp)							\
	{											\
		g += 1 << ((s)-1);						\
		val -= temp;							\
	}

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/


/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/


/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/


/*****************************************************************************/

#endif
