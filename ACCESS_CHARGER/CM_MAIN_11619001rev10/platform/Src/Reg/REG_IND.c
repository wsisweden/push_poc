/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	Name:	REG_IND.C
*	
*--------------------------------------------------------------------*//** \file		
*
*	\ingroup	REG
*
*	\brief		Indication attribute handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "sys.h"
#include "reg.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_getIndAttributes
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get indication attributes.
*
*	\param		indInstId	Instance id for the indication FB instance.
*	\param		indIdx		Indication index.
*
*	\return		Pointer to the indication attributes of the specified
*				indication.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC void const_P * reg_getIndAttributes(
	sys_FBInstId			indInstId,
	sys_IndIndex			indIdx
) {
	reg__IndInstInfo const_P *	pIndInstInfo;

	deb_assert(indInstId < sys_dimFBInst);
	deb_assert(indIdx < sys_fBInstInfo[indInstId].dimIndList);

	/*	
	 *	reg_indInstInfo is generated by Pod Composer to the projind.h file.
	 */

	pIndInstInfo = &reg__indInstInfo[indInstId];

	return ((BYTE *) pIndInstInfo->pIndAttr) + indIdx * pIndInstInfo->size;
}
