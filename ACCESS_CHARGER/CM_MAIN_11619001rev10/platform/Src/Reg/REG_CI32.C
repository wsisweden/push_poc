/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		REG_CI32.C
*
*	\ingroup	REG
*
*	\brief		Conversion from Int32 to the register data type.
*
*	\details
*
*	\note
*
*	\version	30-04-2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "sys.h"
#define REG__IMPLEM /**< Declare the internal / implementation specific parts*/
#include "reg.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * List of maximum values for different register types. The index in this list
 * is the register data type number.
 */

PRIVATE Int32 const_P reg__ci32MaxValues[] = {
	Int8_MAX,							/* SYS_REG_T_Int8					*/
	Uint8_MAX,							/* SYS_REG_T_Uint8					*/
	Int16_MAX,							/* SYS_REG_T_Int16					*/
	Uint16_MAX,							/* SYS_REG_T_Uint16					*/
	Int32_MAX,							/* SYS_REG_T_Int32					*/
	Int32_MAX							/* SYS_REG_T_Uint32					*/
};

/**
 * List of minimum values for different register types. The index in this list
 * is the register data type number.
 */

PRIVATE Int32 const_P reg__ci32MinValues[] = {
	Int8_MIN,							/* SYS_REG_T_Int8					*/
	Uint8_MIN,							/* SYS_REG_T_Uint8					*/
	Int16_MIN,							/* SYS_REG_T_Int16					*/
	Uint16_MIN,							/* SYS_REG_T_Uint16					*/
	Int32_MIN,							/* SYS_REG_T_Int32					*/
	Uint32_MIN							/* SYS_REG_T_Uint32					*/
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_i32ToRegType
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert Int32 variable to the correct register data type.
*
*	\param		inValue		The value that will be converted.
*	\param		regHandle	Register handle.
*	\param		pOutValue	Pointer to buffer where the converted value will be
*							stored. The buffer must be large enough and aligned
*							properly for the register data type.
*
*	\retval		REG_OK			If the conversion was successful.
*	\retval		REG_OUT_RANGE	If the value was out of range for the register
*								data type.
*
*	\details	The value will be converted to the data type of the register.
*				This will only work with numeric registers.
*
*				The buffer containing the converted value can directly be
*				given as a parameter to reg_test or reg_put after calling this
*				function.
*
*	\note
*
*******************************************************************************/

PUBLIC reg_Status reg_i32ToRegType(
	Int32 					inValue,
	sys_RegHandle			regHandle,
	void *					pOutValue
) {
	sys_RegDescr const_P *	pDescr;
	reg_Status				retVal;
	Uint8					type;

	deb_assert(regHandle <= sys_dimRegFile); /* Error if bad handle			*/

	pDescr = reg__descrPtr(regHandle);
	type = pDescr->status & REG_TYPE_MASK;

	retVal = REG_UNEXPECTED;
	if (type <= SYS_REG_T_Uint32) {

		retVal = REG_OUT_RANGE;
		if (
			inValue >= reg__ci32MinValues[type]
			&& inValue <= reg__ci32MaxValues[type]
		) {
			/*
			 * Range has been checked now. The value will fit into the register.
			 * Perform the actual conversion.
			 */

			retVal = REG_OK;

			switch (type) {
				case SYS_REG_T_Int8:
					*((Int8 *) pOutValue) = (Int8) inValue;
					break;

				case SYS_REG_T_Uint8:
					*((Uint8 *) pOutValue) = (Uint8) inValue;
					break;

				case SYS_REG_T_Int16:
					*((Int16 *) pOutValue) = (Int16) inValue;
					break;

				case SYS_REG_T_Uint16:
					*((Uint16 *) pOutValue) = (Uint16) inValue;
					break;

				case SYS_REG_T_Int32:
					*((Int32 *) pOutValue) = inValue;
					break;

				case SYS_REG_T_Uint32:
					*((Uint32 *) pOutValue) = (Uint32) inValue;
					break;

				default:
					break;
			}
		}
	}

	return(retVal);
}
