/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*	Name:	REG_IND.C
*	
*--------------------------------------------------------------------*//** \file		
*
*	\ingroup	REG
*
*	\brief		Indication list size.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 3002 $ \n
*				\$Date: 2016-09-19 14:16:33 +0300 (ma, 19 syys 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "sys.h"
#include "deb.h"
#define REG__IMPLEM /**< Declare the internal / implementation specific parts*/
#include "reg.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_getIndCount
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns how many elements there is in the indication list.
*
*	\param		instId	The function block instance to get indication list size
*						from
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC sys_IndIndex reg_getIndCount(
	sys_FBInstId			instId
) {
	sys_FBInstDescr const_P *pFBI;
	
	deb_assert(instId < sys_dimFBInst);

	pFBI = reg__fbInstDescrPtr(instId);

	return pFBI->dimIndList;
}
