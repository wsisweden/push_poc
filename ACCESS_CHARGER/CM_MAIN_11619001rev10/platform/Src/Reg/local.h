/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Reg\local.h
*
*	\ingroup	REG
*
*	\brief		Local declarations for REG
*
*	\note		
*
*	\version
*
*******************************************************************************/

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "ptrs.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

reg_Status reg__drvPutDefault(reg_Def *);
reg_Status reg__drvGetDefault(reg_Def *);
reg_Status reg__drvCmpDefault(reg_Def *);
Uint32 reg__drvSizDefault(reg_Def *);
Uint8 reg__definition(reg_Def *);
reg_Status reg__validate(reg_Def *);
void const_P *reg__typesPtr(Uint16);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*
 *	Extern declarations of the PROJECT specific data not declared in a header.
 *
 *	Note that the project specific objects exe__regDefaults[], exe__regMin[] and
 *	exe__regMax[] are "incorrectly" declared (compared the definition) and
 *	the LINKER (e.g. IAR) may complain. Don't worry, nothing wrong there.
 */

extern BYTE const_P			exe__regMinMax[];
extern Uint16 const_P		exe__regMMOffs[];
extern Uint16 const_P		sys_sizeImageNOV;
#if 0 /*17-10-07*/
//	extern BYTE const_P			exe__regMin[];
//	extern BYTE const_P			exe__regMax[];
//	extern Uint16 const_P		sys_offsMinMax;
//	extern Uint16 const_P		sys_offsDefault;
//	extern Uint16 const_P		sys_sizeImageRAM;
//	extern Uint16 const_P		sys_sizeImageDEF;
#endif
extern BYTE const_P			exe__regDefaults[];
extern BYTE					sys_imageRAM[];

extern void const_P * const_P sys_mmiAttrib[];
extern sys_RegDescr const_P	sys_regFile[];
extern sys_RegHandle const_P sys_dimRegFile;

extern sys_FBInstDescr const_P sys_fBInstInfo[];
extern Uint8 const_P		sys_dimFBInst;
extern void *				sys_fBInstPtr[];

extern sys_BufTools const_P	sys_bufTools;

/*
 *	Ring buffer specific.
 */

#define REG__BUF_INDEX		0
#define REG__BUF_SIZE		1

/*
 *	Target specific definitions. Assumed that 'unsigned' is sufficient to hold
 *	the values of 'sys_IndIndex'. Use of 'unsigned' produce better code on some
 *	targets.
 */

#if (TARGET_SELECTED & TARGET_AVR) == TARGET_AVR
#define REG__INLINE_LEVEL	30
#define reg__IndIndex		sys_IndIndex

#elif (TARGET_SELECTED & TARGET_M16C) == TARGET_M16C
#define REG__INLINE_LEVEL	50
#define reg__IndIndex		sys_IndIndex

#elif (TARGET_SELECTED & TARGET_W32) == TARGET_W32
#define REG__INLINE_LEVEL	80
#define reg__IndIndex		unsigned

#elif (TARGET_SELECTED & TARGET_PC) == TARGET_PC
#define REG__INLINE_LEVEL	80
#define reg__IndIndex		unsigned

#elif (TARGET_SELECTED & TARGET_ARM7) == TARGET_ARM7
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#elif (TARGET_SELECTED & TARGET_CM3) == TARGET_CM3
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#elif (TARGET_SELECTED & TARGET_CM0) == TARGET_CM0
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#elif (TARGET_SELECTED & TARGET_CM4) == TARGET_CM4
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#elif (TARGET_SELECTED & TARGET_MM7) == TARGET_MM7
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#elif (TARGET_SELECTED & TARGET_UNIX) == TARGET_UNIX
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned

#elif (TARGET_SELECTED & TARGET_DOXY) == TARGET_DOXY
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned

#elif (TARGET_SELECTED & TARGET_SM4) == TARGET_SM4
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#elif (TARGET_SELECTED & TARGET_S4F4) == TARGET_S4F4
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#elif (TARGET_SELECTED & TARGET_S3F2) == TARGET_S3F2
#define REG__INLINE_LEVEL	70
#define reg__IndIndex		unsigned long

#else
# error "Unknown target, updates needed"
#endif

/*
 *	REG__INLINE_LEVEL controls use of macros within the REG FB. The greater
 *	the value, the greater use of macros with the selected targer.
 */

#if REG__INLINE_LEVEL < 50

	sys_RegDescr const_P *reg__descrPtr(sys_RegHandle);
	reg__Implem const_P *reg__implemPtr(Uint8);
	sys_FBInstDescr const_P *reg__fbInstDescrPtr(sys_FBInstId);

#else

	extern reg_ImplemType reg_typeImplem;

#	define reg__descrPtr(handle)	(&sys_regFile[handle])
#	define reg__implemPtr(type)		(&reg_typeImplem[type & REG_TYPE_MASK])
#	define reg__fbInstDescrPtr(id)	(&sys_fBInstInfo[id])

#endif

sys_IndIndex reg__findIndIndex(sys_RegHandle,sys_FBInstDescr const_P *);

/******************************************************//** \endcond pub_decl */
