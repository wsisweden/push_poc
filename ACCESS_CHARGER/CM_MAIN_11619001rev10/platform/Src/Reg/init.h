/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	REG
*
*	\brief		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef REG_INIT_H_INCLUDED
#define REG_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST	*/
	Uint8					dummy;		/**<                                */
} reg_Init;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_REG
#elif defined(EXE_GEN_REG)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_REG
#endif

#define EXE_APPL(n)			n##reg

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */
#if 0
#define EXE_USE_NN /**< Non-volatile numeric                                */
#define EXE_USE_NS /**< Non-volatile string                                 */
#define EXE_USE_VN /**< Volatile numeric                                    */
#define EXE_USE_VS /**< Volatile string                                     */
#define EXE_USE_PN /**< Process-point numeric                               */
#define EXE_USE_PS /**< Process-point string                                */
#define EXE_USE_BL /**< Block of byte-data                                  */


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

EXE_REG_NN( myRegName1, SYS_REG_DEFAULT, 1, Uint8, 10, 0, 255 )
EXE_REG_NS( myRegName2, SYS_REG_DEFAULT,            4, "Init" )
EXE_REG_VN( myRegName3, SYS_REG_DEFAULT, 3, Uint8, 10, 0, 255 )
EXE_REG_VS( myRegName4, SYS_REG_DEFAULT,           10, "Init" )
EXE_REG_PN( myRegName5, SYS_REG_DEFAULT, 1, Uint8,     0, 255 )
EXE_REG_PS( myRegName6, SYS_REG_DEFAULT,           10         )
EXE_REG_BL( myRegName7, SYS_REG_DEFAULT,                      )

EXE_MMI_NONE( myRegName1 )
EXE_MMI_NONE( myRegName2 )
EXE_MMI_NONE( myRegName3 )
EXE_MMI_NONE( myRegName4 )
EXE_MMI_NONE( myRegName5 )
EXE_MMI_NONE( myRegName6 )
EXE_MMI_NONE( myRegName7 )


/* Export the following handles of all instances */

EXE_REG_EXPORT( app_publicName, myRegName1 )


/* Import the following handles */

EXE_REG_IMPORT( foo_publicName, fooInstance )


/* References to external registers */

EXE_REG_EX( myRegName8, thatRegName, thatInstanceName )


/* Write-indications */

EXE_IND_ME( myIndName1, myRegName6 )
EXE_IND_EX( myIndName2, myRegName5, test /*instance*/ )
#endif


/* Language-dependent texts */

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
