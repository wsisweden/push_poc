/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		dbg_serial.c
*
*	\ingroup	MEM
*
*	\brief		Code for debugging mem.
*
*	\note		
*
*	\version	??-??-2009 / Daniel Tisza
*
*******************************************************************************/

#include "local.h"
#include "dbg_static.h"

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*************************************************************//** \endcond *//*
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 * Debugging variables
 */
#if (MEM__DEBUG == 1)

PUBLIC	Boolean	mem__debug	= 0; /* Off by default */

	#if (TARGET_SELECTED & TARGET_W32) == TARGET_W32
		PUBLIC	FILE* mem__pLog	= NULL;

		char* mem__info_to_str[] = {
		"\n",
		"res",
		"red",
		"rpl",
		"err",

		"<1 req",
		"inAllo",
		"al",
		"min fr",
		"ovr max",

		"fr",
		"fre",
		"~fst",
		"pr fr",
		"~lst",

		"nx fr",
		"empty cat",
		"FL",
		"fr order err",
		"fr chain",

		"fr miss",
		"beg mem cat",
		"end mem cat",
		"beg cmp",
		"end cmp",

		"beg sav",
		"end sav",
		"pre sav",
		"sav",
		"beg ld",

		"end ld",
		"ld",
		"beg init cat",
		"cat",
		"dyn",

		"tot",
		"max",
		"rea",
		"sm",
		"~lst nx",

		"ud+nx",
		"pr+ud+nx",
		"pr+ud",
		"fnd fr",
		"cp",

		"beg cat",
		"end cat",

		"add"
		};


	#endif

#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/** 
 * \cond priv_decl
 */

#if (MEM__DEBUG == 1)

PUBLIC void prt_blocks(
	void ** 				ppCat
) {
	mem__StaticBlk *		pHeader;

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_BEGMEMCAT);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	for (	pHeader = (mem__StaticBlk*) *ppCat;
			pHeader != NULL;
			pHeader = pHeader->next) {

				BYTE* pEnd;
				pEnd = (BYTE*)pHeader	- MEM__DYNTEST_SIZE
										+ pHeader->size;

				LOG(MEM__DBG_WRITE_PTR(pHeader);
					MEM__DBG_WRITE_PTR(pEnd);
					MEM__DBG_WRITE_PTR(pHeader->next);
					MEM__DBG_WRITE_UINT32(pHeader->size);
					MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
	}

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ENDMEMCAT);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

}

PUBLIC void fillMem(
	BYTE *					pMem,
	size_t 					nSize,
	BYTE 					fillVal
) {

	while(nSize>0) {
		*pMem = fillVal;
		pMem++;
		nSize--;
	}
}

PUBLIC void cmpMem(
	BYTE *pMem,
	size_t nSize,
	BYTE testVal
) {
	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_BEGCMP);
		MEM__DBG_WRITE_PTR(pMem);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	while(nSize>0) {
		if (*pMem != testVal) {

			LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ERR);
				MEM__DBG_WRITE_PTR(pMem);
				MEM__DBG_WRITE_UINT32(*pMem);
				MEM__DBG_WRITE_UINT32(testVal);
				MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

			deb_assert(0);
		}
		pMem++;
		nSize--;
	}

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ENDCMP);
			MEM__DBG_WRITE_PTR(pMem);
			MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
}



#endif

/** 
 * \endcond 
 */

