/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DBG_DYN.H
*
*	\ingroup	MEM
*
*	\brief		Memory Debugging
*
*	\details		
*
*	\version	??-??-2009 / Daniel Tisza
*
*	\version
*
*******************************************************************************/

#ifndef DBG_DYN_H_INCLUDED
#define DBG_DYN_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "local.h"
#include "MEM__DYN.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*
 * Category memory usage counters included only in DEBUG build.
 */
#ifndef NDEBUG

#define MEM__DBG_DYNBYTES(pCat)		pCat->counters.dynBytes
#define MEM__DBG_ALLOCBYTES(pCat)	pCat->counters.allocBytes
#define MEM__DBG_PEAKBYTES(pCat)	pCat->counters.peakBytes
#define MEM__DBG_FAILCOUNT(pCat)	pCat->counters.failCount
#define MEM__DBG_REFCOUNT(pCat)		pCat->counters.refCount
#define MEM__DBG_MAXREFCOUNT(pCat)	pCat->counters.maxRefCount
#define MEM__DBG_FREECOUNT(pCat)	pCat->counters.freeCount

#define MEM__DBG_DYNBYTES_INIT(pCat)		\
			MEM__DBG_DYNBYTES(pCat) = 0;	\
			MEM__DBG_FREECOUNT(pCat) = 0;

#define	MEM__DBG_DYNBYTES_INC(pCat, size)		\
			MEM__DBG_DYNBYTES(pCat) += size;

#define MEM__DBG_ALLOCCOUNTERS_INIT(pCat)		\
			MEM__DBG_ALLOCBYTES(pCat) = 0;		\
			MEM__DBG_PEAKBYTES(pCat) = 0;		\
			MEM__DBG_FAILCOUNT(pCat) = 0;		\
			MEM__DBG_REFCOUNT(pCat) = 0;		\
			MEM__DBG_MAXREFCOUNT(pCat) = 0;


#define	MEM__DBG_ALLOCBYTES_INC(pCat, size)								\
			MEM__DBG_ALLOCBYTES(pCat) += size; 							\
			if (MEM__DBG_ALLOCBYTES(pCat) >	MEM__DBG_PEAKBYTES(pCat))	\
				MEM__DBG_PEAKBYTES(pCat) = MEM__DBG_ALLOCBYTES(pCat);

#define	MEM__DBG_ALLOCBYTES_DEC(pCat, size)	\
			MEM__DBG_ALLOCBYTES(pCat) -= size;

#define MEM__DBG_FAILCOUNT_INC(pCat) \
			MEM__DBG_FAILCOUNT(pCat) += 1;

#define MEM__DBG_REFCOUNT_INC(pCat)										\
			MEM__DBG_REFCOUNT(pCat) += 1;								\
			if (MEM__DBG_REFCOUNT(pCat) > MEM__DBG_MAXREFCOUNT(pCat))	\
				MEM__DBG_MAXREFCOUNT(pCat) = MEM__DBG_REFCOUNT(pCat);

#define MEM__DBG_REFCOUNT_DEC(pCat) \
			MEM__DBG_REFCOUNT(pCat) -= 1;

#define MEM__DBG_FREECOUNT_INC(pCat)	\
			MEM__DBG_FREECOUNT(pCat) += 1;

#define MEM__DBG_FREECOUNT_DEC(pCat)	\
			MEM__DBG_FREECOUNT(pCat) -= 1;

#else

#define MEM__DBG_DYNBYTES_INIT(pCat)
#define	MEM__DBG_DYNBYTES_INC(pCat, size)
#define MEM__DBG_ALLOCCOUNTERS_INIT(pCat)
#define	MEM__DBG_ALLOCBYTES_INC(pCat, size)
#define	MEM__DBG_ALLOCBYTES_DEC(pCat, size)
#define MEM__DBG_FAILCOUNT_INC(pCat)
#define MEM__DBG_REFCOUNT_INC(pCat)
#define MEM__DBG_REFCOUNT_DEC(pCat)
#define MEM__DBG_FREECOUNT_INC(pCat)
#define MEM__DBG_FREECOUNT_DEC(pCat)

#endif

#if (MEM__DEBUG == 0)

#define CHECK_SBDATA(ptr, oldSiz, newSiz, expVal, pC)
#define MEM__ASSERT_SBFREE(pB, offs, pDB)
#define MEM__ASSERT_SBUSED(pB, offs, pDB)
#define PRT_AND_CHECK_SBLKS(pCat)
#define	MEM__DBG_DYNNUM_INC
#define MEM__DBG_DYNNUM_ZERO
#define MEM__DBG_PHYSNUM_INC
#define MEM__DBG_PHYSNUM_ZERO
#define MEM__CBLKS_PRINT(stepMsg, pCbs, pLsts, pMx, catMaxSiz)

#else

#define MEM__CBLKS_PRINT(stepMsg, pCbs, pLsts, pMx, catMaxSiz)		\
	LOG(fprintf(mem__pLog, "\nStep %s\n", (stepMsg)));				\
	prt_contBlkTree((pCbs));										\
	LOG(fprintf(mem__pLog, "pMax %p %d\n", (pMx), (catMaxSiz)));	\
	LOG(fprintf(mem__pLog, "Lasts\n"));								\
	prt_contBlkLasts((pLsts));

/*
 * Verification after reallocation
 */
#define CHECK_SBDATA(ptr, oldSiz, newSiz, expVal, pC)	\
	if (ptr != NULL) {	\
		LOG(	MEM__DBG_WRITE_PTR(ptr);	\
				MEM__DBG_WRITE_UINT32(oldSiz);	\
				MEM__DBG_WRITE_UINT32(newSiz);	\
				MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););	\
		if (newSiz < oldSiz) {	\
			cmpMem(ptr, newSiz, expVal);	\
		} else {	\
			cmpMem(ptr, oldSiz, expVal);	\
		}	\
		PRT_AND_CHECK_SBLKS(pC);	\
		prt_freelist(pC);	\
	} else {	\
		LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ERR););	\
	}

/*
 * Verification of sub-blocks
 */
#define MEM__ASSERT_SBFREE(pB, offs, pDB)	\
	deb_assert((pB)->hdr.size == MEM__FREE);	\
	deb_assert((pB)->size <= MEM__MAX_SBINDEX);	\
	deb_assert((((BYTE*)(pB)) + ((offs) << MEM__SBSIZE_BITS)) == (BYTE*)(pDB));

#define MEM__ASSERT_SBUSED(pB, offs, pDB)	\
	deb_assert((pB)->hdr.size <= MEM__MAX_SBINDEX);	\
	deb_assert((((BYTE*)(pB)) + ((offs) << MEM__SBSIZE_BITS)) == (BYTE*)(pDB));


#define PRT_AND_CHECK_SBLKS(pCat)	(prt_catphys((mem__Category*)(pCat)))

#define	MEM__DBG_DYNNUM_INC		dynNum++;
#define MEM__DBG_DYNNUM_ZERO	dynNum = 0;

#define MEM__DBG_PHYSNUM_INC	physNum++;
#define MEM__DBG_PHYSNUM_ZERO	physNum = 0;


#endif

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

#if (MEM__DEBUG == 0)

	#define prt_freelist(p)
	#define prt_contBlkTree(p)
	#define	prt_contBlkLasts(p)
	#define	prt_cat(p)
	#define	prt_subblks(p1, p2)
	#define	prt_catphys(p)

#else

	void prt_freelist(void *);
	void prt_contBlkTree(void*);
	void prt_contBlkLasts(void*);
	void prt_cat(void*);
	void prt_subblks(mem__DynBlk*, mem__Category*);
	void prt_catphys(mem__Category*);

#endif

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

#if (MEM__DEBUG == 1)

extern int dynNum;
extern int physNum;

#if MEM__LINKPTR_F
extern mem__DynBlk* mem__pDyns[];
#endif

#endif

/***************************************************************//** \endcond */

#endif
