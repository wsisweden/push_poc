/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		MEM_CLEA.C
*
*	\ingroup	MEM
*
*	\brief		Contains the mem_catSave and mem_catLoad functions.
*
*	\note		
*
*	\version	??-??-2009 / Daniel Tisza
*
*	\version	11-09-2009 / Ari Suomi / Fixed bug in mem_catLoad that occured
*					if there was no blocks in the category.
*
*******************************************************************************/
 
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "local.h"
#include "dbg_static.h"
#include "mem.h"

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*************************************************************//** \endcond *//*
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	mem_catSave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Saves StaticBlks of a Category in static state.
*
*	\param		ppCat	Addr of the memory category variable e.g. 'mem_normal'.
*
*	\return		ptr to a structure holding the save information.
*
*	\details	The category can later be restored to the saved state by using
*				mem_catLoad.
*
*	\note	
*
*******************************************************************************/

PUBLIC void * mem_catSave(
	void **					ppCat
) {
	mem__StaticBlk *		pBlk;
	mem__Save *				pSave;
	int						count;
	size_t					saveSize;

	ENTER_EXCLUSIVE;

	if (ppCat == NULL)			/* Invalid cat pptr	*/
	{
		deb_assert(0);
		EXIT_EXCLUSIVE;
		return NULL;
	}

	pBlk = *ppCat;

	if (pBlk == NULL)			/* Nothing to save	*/
	{
		deb_assert(0);
		EXIT_EXCLUSIVE;
		return NULL;
	}

	if (MEM__IS_DYNAMIC(pBlk))	/* Not in static mode */
	{
		deb_assert(0);
		EXIT_EXCLUSIVE;
		return NULL;
	}

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_BEGSAV);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	count = 0;

	do {						/* Count StaticBlks		*/

		LOG(MEM__DBG_WRITE_PTR(pBlk);
			MEM__DBG_WRITE_PTR(pBlk->next);
			MEM__DBG_WRITE_UINT32(pBlk->size);
			MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

		count++;
		pBlk = pBlk->next;

	} while (pBlk != NULL);

	saveSize = MEM__SAVE_HDR + count * MEM__STATICBLK;
	pSave = (mem__Save*) mem_reserve(ppCat, saveSize);

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_PRESAV);
		MEM__DBG_WRITE_UINT32(count);
		MEM__DBG_WRITE_UINT32(saveSize);
		MEM__DBG_WRITE_PTR(pSave);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	prt_blocks(ppCat);

	if (pSave == NULL)			/* Reserve failure		*/
	{
		deb_assert(0);
		EXIT_EXCLUSIVE;
		return NULL;
	}

	pBlk = *ppCat;				/* Reserve might change */

	if (pBlk == NULL)			/* Nothing to save		*/
	{
		deb_assert(0);
		EXIT_EXCLUSIVE;
		return NULL;
	}

	count = 0;

	do {						/* Fill block infos in pSave */

		pSave->block[count].next = pBlk;
		pSave->block[count].size = pBlk->size;

		LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_SAV);
			MEM__DBG_WRITE_PTR(pBlk);
			MEM__DBG_WRITE_PTR(pBlk->next);
			MEM__DBG_WRITE_UINT32(pBlk->size);
			MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

		count++;
		pBlk = pBlk->next;

	} while (pBlk != NULL);

	pSave->count = count;

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ENDSAV);
		MEM__DBG_WRITE_UINT32(count);
		MEM__DBG_WRITE_UINT32(saveSize);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	EXIT_EXCLUSIVE;
	return pSave;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	mem_catLoad
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Restores a category from a given saved point.
*
*	\param		ppCat 	Addr of the memory category variable e.g. 'mem_normal'.
*	\param		pSave 	Pointer to the save information.
*
*	\details	-
*
*	\note	
*
*******************************************************************************/

PUBLIC Boolean mem_catLoad(
	void **					ppCat,
	void *					pSave
) {
	mem__StaticBlk **		ppPrev;
	mem__StaticBlk *		pBlk = NULL;
	int						count;
	int						i;

	ENTER_EXCLUSIVE;

	if (ppCat == NULL) {
		deb_assert(0);					/* Invalid cat pptr */
		EXIT_EXCLUSIVE;
		return FALSE;
	}

	if (pSave == NULL) {
		deb_assert(0);					/* Invalid save ptr */
		EXIT_EXCLUSIVE;
		return FALSE;
	}

	count = ((mem__Save*)pSave)->count;
	ppPrev = (mem__StaticBlk**)ppCat;

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_BEGLD);
		MEM__DBG_WRITE_UINT32(count);
		MEM__DBG_WRITE_PTR(ppPrev);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	for (i = 0; i < count; i++)			/* Restore list of StaticBlks */
	{
		pBlk = ((mem__Save*)pSave)->block[i].next;
		pBlk->size = ((mem__Save*)pSave)->block[i].size;
		MEM__SET_STATIC(pBlk);

		LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_LD);
			MEM__DBG_WRITE_PTR(pBlk);
			MEM__DBG_WRITE_UINT32(pBlk->size);
			MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

		*ppPrev = pBlk;
		ppPrev = &pBlk->next;
	}
	
	if (pBlk == NULL)
	{
		EXIT_EXCLUSIVE;
		return FALSE;
	}

	pBlk->next = NULL;					/* Terminate */

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ENDLD);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	EXIT_EXCLUSIVE;
	return TRUE;
}
