/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		dbg_dyn.c
*
*	\ingroup	MEM
*
*	\brief		Code for debugging mem.
*
*	\note		
*
*	\version	??-??-2009 / Daniel Tisza
*
*******************************************************************************/

/*******************************************************************************
;
;    D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;    HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "local.h"
#include "dbg_dyn.h"

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*************************************************************//** \endcond *//*
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

#if (MEM__DEBUG == 1)

int physNum = 0;
int dynNum = 0;

#if MEM__LINKPTR_F
mem__DynBlk* mem__pDyns[30] = { NULL };
#endif

#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

#if (MEM__DEBUG == 1)

/** 
 * \cond priv_decl
 */

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	prt_freelist
*
*--------------------------------------------------------------------------*//**
*
*	\brief	
*
*	\param		ptr 
*
*	\details	-
*
*	\note		-
*
*******************************************************************************/

PUBLIC void prt_freelist(
	void * 					ptr
) {
	mem__Category *			pCat;
	mem__FreeLink 			link;
	mem__SubFree *			pFree;
	BYTE * 					pEnd;
	size_t 					byteSize;

	pCat = (mem__Category*)ptr;

	if (pCat == NULL)
	{
		LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_EMPTYCAT);
			MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
		deb_assert(0);
		return;
	}

	MEM__LINK_COPY_VAR(pCat->firstFree, link);

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_FL);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	while (!MEM__LINK_IS_END(link)) {

		MEM__LINK_SUBFREE_VAR(link, pCat, pFree);

		byteSize = MEM__SB_TO_BYTES(pFree->size);
		pEnd = ((BYTE*)pFree) + byteSize;

		if (!MEM__LINK_IS_END(link)) {
			mem__SubFree *pNext;
			MEM__LINK_SUBFREE_VAR(link, pCat, pNext);

			if (pFree->size > pNext->size)
			{
				LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ORDRERR);
					MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
				deb_assert(0);
			}
		}

#if MEM__BLOCKID_F

		LOG(fprintf(mem__pLog, "%p-%p\t%d,%d\t%d,%d %d/%d\n",
			pFree,
			pEnd,
			pFree->next.id, pFree->next.offs,
			pFree->prev.id, pFree->prev.offs,
			pEnd - (BYTE*)pFree,
			pFree->size));
#else
		LOG(MEM__DBG_WRITE_PTR(pFree);
			MEM__DBG_WRITE_PTR(pEnd);
			MEM__DBG_WRITE_PTR(pFree->next);
			MEM__DBG_WRITE_PTR(pFree->prev);
			MEM__DBG_WRITE_UINT32(pEnd - (BYTE*)pFree);
			MEM__DBG_WRITE_UINT32(pFree->size);
			MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
#endif

		deb_assert(pFree->hdr.size == MEM__FREE);

		MEM__LINK_COPY_VAR(pFree->next, link);
	}

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
}

/*
 * Multiple PhysBlks and DynBlks
 */

#if MEM__LINKPTR_F

void prt_cat(void *ptr) {}


PUBLIC void prt_catphys(mem__Category* pCat)
{
	int i;

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_BEGCAT);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	for (i = 0;i < dynNum; i++)
	{
		prt_subblks(mem__pDyns[i], pCat);
		LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
	}

	LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_ENDCAT);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););
}


#elif MEM__DYNID_BITS


PUBLIC void prt_contBlkTree(void* ptr) {

	mem__ContBlk* pCb;

	pCb = (mem__ContBlk*) ptr;

	while (pCb != NULL) {

		LOG(fprintf(mem__pLog, "%p %p max %d last %d incl %d\n",
			pCb, pCb->next, pCb->maxCount, pCb->lastSize, pCb->included));

		pCb = pCb->next;
	}
}

PUBLIC void prt_contBlkLasts(void* ptr) {

	mem__ContBlk* pCb;

	pCb = (mem__ContBlk*)ptr;
	while (pCb != NULL) {

		LOG(fprintf(mem__pLog, "%p %p %d\n", pCb, pCb->lastNext, pCb->lastSize));

		pCb = pCb->lastNext;
	}

	LOG(fprintf(mem__pLog, "\n"));

}


PUBLIC void prt_cat(void *ptr) {

	mem__Category*	pCat;
	BYTE**			ppPhys;
	int i;

	pCat = (mem__Category*)ptr;
	ppPhys = (BYTE**)pCat;
	ppPhys--;

	LOG(fprintf(mem__pLog, "PhysBlks\n"));

	for (i = 0; i < physNum; i++) {
		mem__PhysBlk* pP;
		pP = (mem__PhysBlk*)*ppPhys;

		LOG(fprintf(mem__pLog, "%d - %p %d %p\n", i, pP, pP->idBase, pP->pCat));
		ppPhys--;
	}

	LOG(fprintf(mem__pLog, "DynBlks\n"));
	for (i = 0; i < dynNum; i++) {
		BYTE**	ppP;
		mem__PhysBlk* pP;
		mem__DynBlk* pD;

		ppP = (BYTE**)pCat;
		ppP -= pCat->dynToPhys[i];

		pP = (mem__PhysBlk*)*ppP;
		pD = (mem__DynBlk*)((BYTE*)pP	- MEM__DYNBLK_SIZE
										- ((i - pP->idBase)*MEM__MAX_DYNBLK_SIZE));

		LOG(fprintf(mem__pLog, "%d %d %p %p %d\n", i, pCat->dynToPhys[i], pP, pD, pD->id));
	}
}

PUBLIC void prt_catphys(
	mem__Category* pCat
) {
	int i;
	mem__PhysBlk* pPrev;

	LOG(fprintf(mem__pLog, "---Begin Print Cat\n"));

	pPrev = NULL;
	for(i = 0;i < dynNum;i++) {
		BYTE** ppP;
		mem__PhysBlk* pP;
		mem__DynBlk* pD;
		mem__DynIdType locId;

		ppP = (BYTE**)pCat;
		pP = (mem__PhysBlk*)*(ppP - pCat->dynToPhys[i]);
		locId = i - pP->idBase;
		pD = (mem__DynBlk*)((BYTE*)pP	- MEM__DYNBLK_SIZE
										- locId * MEM__MAX_DYNBLK_SIZE);

		if (pP != pPrev) {

			LOG(fprintf(mem__pLog, "\nPB: %p %d %p\n\n", pP, pP->idBase, pP->pCat));
			pPrev = pP;
		}

		LOG(fprintf(mem__pLog, "DB %p\n", pD));
		prt_subblks(pD, pCat);
	}

	LOG(fprintf(mem__pLog, "---End Print Cat\n"));
}

/*
 * Single DynBlk
 */
#else

PUBLIC void prt_catphys(
	mem__Category* pCat
) {
	// LOG(fprintf(mem__pLog, "---Begin Print Cat\n"));

	prt_subblks(((mem__DynBlk*)((BYTE*)pCat - MEM__DYNBLK_HDR)), pCat);

	// LOG(fprintf(mem__pLog, "---End Print Cat\n"));
}

#endif

PUBLIC void prt_subblks(
	mem__DynBlk * 			pD,
	mem__Category * 		pCat
) {

	mem__SubFree* pSb;
	mem__SizeType size;
	mem__SizeType offs;
	mem__SizeType sizeP;
	mem__SizeType nextOsp;
	Boolean prevFree;
	size_t totSize;

	/*
	 * osP of DynBlk contains size of last SubBlk
	 * because the size of it equals its offs and then:
	 *
	 * osP = (offs - size) ^ size
	 * osP = 0 ^ size
	 * osP = size
	 *
	 */

	LOG(MEM__DBG_WRITE_PTR(pD);
		MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

	totSize = 0;
	sizeP = pD->osP;
	pSb = (mem__SubFree*)pD;
	nextOsp = pD->osP;
	prevFree = FALSE;

	do {
		size_t byteSize;
		BYTE* pEnd;

		pSb = (mem__SubFree*)MEM__SB_BACKWD(pSb, sizeP);
		size = pSb->hdr.size;

		if (size == MEM__FREE) {

			if (prevFree) {

				LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_FRCHAIN);
					MEM__DBG_WRITE_PTR(pSb);
					MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

				deb_assert(0);
			} else {
				prevFree = TRUE;
			}

			size = pSb->size;

		} else {
			prevFree = FALSE;
		}

		totSize += size;

		MEM__OSP_UNPACK_OFFS(size, nextOsp, offs);
		MEM__OSP_UNPACK_SIZEP(pSb->hdr.osP, offs, sizeP);

		byteSize = MEM__SB_TO_BYTES(size);
		pEnd = ((BYTE*)pSb) + byteSize;

		LOG(MEM__DBG_WRITE_PTR(pSb);
			MEM__DBG_WRITE_PTR(pEnd);
			MEM__DBG_WRITE_UINT32(pEnd - (BYTE*)pSb);
			MEM__DBG_WRITE_UINT32(size);
			MEM__DBG_WRITE_UINT32(offs);
			MEM__DBG_WRITE_UINT32(sizeP););

		if (pSb->hdr.size == MEM__FREE)
		{
			LOG(MEM__DBG_WRITE_UINT32(0xF););
		}

		LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

		if (pSb->hdr.size == MEM__FREE) {

			/* Check if in free list */
			mem__FreeLink head;
			Boolean found;

			fillMem((BYTE*)pSb + sizeof(mem__SubFree),
						byteSize - sizeof(mem__SubFree), 0x1);

			/* Check if found in free list */
			found = 0;
			head = pCat->firstFree;
			while(!MEM__LINK_IS_END(head)) {

				mem__SubFree *pSubHead;
				MEM__LINK_SUBFREE_VAR(head, pCat, pSubHead);

				if ((mem__SubFree*)pSubHead == pSb) {
					found = 1;
					break;
				}

				head = pSubHead->next;
			}

			if (!found)
			{
				LOG(MEM__DBG_WRITE_INFO(MEM__DBG_INFO_FRMISS);
					MEM__DBG_WRITE_PTR(pSb);
					MEM__DBG_WRITE_INFO(MEM__DBG_INFO_NL););

				prt_freelist(pCat);
				deb_assert(0);
			}

		} else {

			fillMem((BYTE*)pSb + sizeof(mem__SubUsed),
						byteSize - sizeof(mem__SubUsed), 0x3);
		}

		nextOsp = pSb->hdr.osP;

	} while (sizeP != 0);
}

/**
 * \endcond 
 */

#endif
