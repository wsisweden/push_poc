/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		dbg_static.h
*
*	\ingroup	MEM
*
*	\brief		Memory Debugging
*
*	\details		
*
*	\note		
*
*	\version	??-??-2009 / Daniel Tisza
*
*******************************************************************************/

#ifndef DBG_STATIC_H_INCLUDED
#define DBG_STATIC_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

#include "local.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#define MEM__PTR_ALIGNED(ptr)	\
	deb_assert((((Uint32)ptr) & (TARGET_MEM_ALIGN - 1)) == 0);

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

#if (MEM__DEBUG == 0)

#define	prt_blocks(p)
#define fillMem(p,s,f)
#define cmpMem(p,s,t)

#else

void prt_blocks(void ** ppCat);
void fillMem(BYTE *, size_t, BYTE);
void cmpMem(BYTE *, size_t, BYTE);

#endif

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
