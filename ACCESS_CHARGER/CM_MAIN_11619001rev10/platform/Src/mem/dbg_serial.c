/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		dbg_serial.c
*
*	\ingroup	MEM
*
*	\brief		Code for debug output to serial port.
*
*	\version	??-??-2009 / Daniel Tisza
*
*	\version	
*
*******************************************************************************/

#include "tools.h"
#include "deb.h"

#include "local.h"
#include "mem__dbg.h"

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

#if (MEM__DEBUG == 1)

#if (TARGET_SELECTED & TARGET_AVR) == TARGET_AVR

PUBLIC void mem__dbg_uart_init(void)
{
	UBRRH = 0x0;

	/* 8M XTAL 		Baud Double mode, 115200 = 0x8 ERR = 3,549 %	*/
	/* 14.318M XTAL Baud Single mode, 115200 = 0x7 ERR = -2,899 % 	*/

	UBRRL = 0x7;
	UCSRA = 0x0;	//(1 << U2X);

							/* Ena tx, 8bit dat, 1 stop, no par */
	UCSRB = (1 << TXEN);
	UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);
}

#elif (TARGET_SELECTED & TARGET_M16C) == TARGET_M16C

PUBLIC void mem__dbg_uart_init(void)
{
	U1MR 	= 0x5;		/* 8bit dat, 1 stop, no par					*/
	U1C0 	= 0x10;		/* f1 										*/
	U1BRG	= 0xC; 		/* 24M XTAL Baud 115200 = 0xC ERR = 0,16 %	*/
	U1C1 	= 0x1; 		/* TX enabled 								*/
}

#elif (TARGET_SELECTED & TARGET_ARM7) == TARGET_ARM7

PUBLIC void mem__dbg_uart_init(void)
{
    /* Configure UART0 */
    PINSEL0 |= 0x00000050;			/* UART0 RxD0 and TxD0		*/

    U0IER = 0x00;                   /* disable all interrupts	*/

    U0LCR = (1<<7);					/* Enable latch				*/

    U0DLL = (Uint8)4;      			/* Set for baud low byte 	*/
    U0DLM = (Uint8)0;				/* set for baud high byte 	*/
    U0FDR = 2 | (9<<4);				/* DIVADDVAL = 2,MULVAL = 9	*/
    U0LCR |= (1<<0)|(1<<1); 		/* 8 data bits 				*/
    U0FCR = 0x7;					/* Enable FIFO 				*/

    U0LCR &= ~(1<<7);				/* Disable latch 			*/
}

#endif


PUBLIC void mem__dbg_uart_write_num(Uint32 n)
{
	MEM__DBG_WRITE_UART_BYTE(0x4E);

	if (n > 0xFFFFFF)
	{
		MEM__DBG_WRITE_UART_BYTE(0x4);
		MEM__DBG_WRITE_UART_BYTE((n >> 24) & 0xFF);
		MEM__DBG_WRITE_UART_BYTE((n >> 16) & 0xFF);
		MEM__DBG_WRITE_UART_BYTE((n >> 8)  & 0xFF);
	}
	else if (n > 0xFFFF)
	{
		MEM__DBG_WRITE_UART_BYTE(0x3);
		MEM__DBG_WRITE_UART_BYTE((n >> 16) & 0xFF);
		MEM__DBG_WRITE_UART_BYTE((n >> 8)  & 0xFF);
	}
	else if (n > 0xFF)
	{
		MEM__DBG_WRITE_UART_BYTE(0x2);
		MEM__DBG_WRITE_UART_BYTE((n >> 8)  & 0xFF);
	}
	else
	{
		MEM__DBG_WRITE_UART_BYTE(0x1);
	}

	MEM__DBG_WRITE_UART_BYTE(n & 0xFF);
}

PUBLIC void mem__dbg_uart_write_str(char* str)
{
	FOREVER
	{
		MEM__DBG_WRITE_UART_BYTE(*str);

		if (*str == '\0')
		{
			break;
		}

		str++;
	}
}

#endif

