/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
 
/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		mem__dbg.h
*
*	\ingroup	MEM
*
*	\brief		Memory Debugging
*
*	\details		
*
*	\note		
*
*	\version	??-??-2009 / Daniel Tisza
*
*******************************************************************************/

#ifndef	TOOLS_H_INCLUDED
# error	"TOOLS.H must be included before MEM__DBG.H"
#endif

#ifndef MEM__DBG_H
#define MEM__DBG_H

/*******************************************************************************
;
;  HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "local.h"
#include "mem_dstr.h"
#include "osa.h"
#include "hw.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#if (MEM__DEBUG == 0)

/* RELEASE VERSION */
#define LOG(logcmd)

#else

/* DEBUG VERSION */
extern Boolean 				mem__debug;

#define LOG(logcmd)		\
	if (mem__debug)		\
	{					\
		logcmd;			\
	} else {			\
						\
	}


/* DEBUG FILE, WINDOWS */
#if (TARGET_SELECTED & TARGET_W32) == TARGET_W32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
extern FILE*				mem__pLog;
extern char* 				mem__info_to_str[];

#define MEM__DBG_THREADID			(_threadid)
#define MEM__DBG_WRITE_STR(str)		fprintf(mem__pLog, "%s", (str))
#define MEM__DBG_WRITE_NL			fprintf(mem__pLog, "\n")

#define MEM__DBG_WRITE_UINT32(num)						\
		fprintf(mem__pLog, " 0x%X(%d) ", (num), (num));	\

#define MEM__DBG_WRITE_HEX(num)							\
	fprintf(mem__pLog, " 0x%X ", (num));

#define MEM__DBG_WRITE_PTR(ptr)		MEM__DBG_WRITE_HEX((Uint32)(ptr))

#define MEM__DBG_WRITE_INFO(inf)	\
		fprintf(mem__pLog, "%s", mem__info_to_str[(inf)])

/* DEBUG SERIAL */
#else


	/* DEBUG SERIAL, ARM specific */
	#if (TARGET_SELECTED & TARGET_ARM7) == TARGET_ARM7

		#define MEM__DBG_THREADID			(osa_taskCurrent()->pName)

		#define MEM__DBG_WRITE_UART_READY	(U0LSR & (1<<5))
		#define MEM__DBG_WRITE_UART_DATA	(U0THR)

		#define MEM__DBG_BUT1_OPEN	(PROJECT_PORT_BUT1 & (1 << PROJECT_PIN_BUT1))
		#define MEM__DBG_BUT2_OPEN	(PROJECT_PORT_BUT2 & (1 << PROJECT_PIN_BUT2))

		#define MEM__DBG_LED0(on)							\
		if ((on))											\
		{													\
			PROJECT_PORT_LED |= (1 << PROJECT_PIN_LED);		\
		}													\
		else												\
		{													\
			PROJECT_PORT_LED &= ~(1 << PROJECT_PIN_LED);	\
		}


	/* DEBUG SERIAL, AVR specific */
	#elif (TARGET_SELECTED & TARGET_AVR) == TARGET_AVR

		#define MEM__DBG_THREADID			(0xB)

		#define MEM__DBG_WRITE_UART_READY	(UCSRA & (1 << UDRE))
		#define MEM__DBG_WRITE_UART_DATA	(UDR)

		#define MEM__DBG_BUT1_OPEN	(PINA & (1 << PINA0))
		#define MEM__DBG_BUT2_OPEN	(PINA & (1 << PINA1))

		#define MEM__DBG_LED0(on)			\
			if ((on))						\
			{								\
				PORTB &= ~(1 << PORTB0);	\
			}								\
			else							\
			{								\
				PORTB |= (1 << PORTB0);		\
			}

	#elif	(TARGET_SELECTED & TARGET_M16C) == TARGET_M16C

		#define MEM__DBG_THREADID			(0xC)

		#define MEM__DBG_WRITE_UART_READY	(U1C1 & 0x2)
		#define MEM__DBG_WRITE_UART_DATA	(U1TB)

		#define MEM__DBG_BUT1_OPEN	(P8 & (1 << 3))
		#define MEM__DBG_BUT2_OPEN	(P8 & (1 << 0))

		#define MEM__DBG_LED0(on)			\
			if ((on))						\
			{								\
				P8 |= (1 << 4);				\
			}								\
			else							\
			{								\
				P8 &= ~(1 << 4);			\
			}
	#endif


#define MEM__DBG_WRITE_UART_BYTE(data)				\
			while (!MEM__DBG_WRITE_UART_READY);		\
			MEM__DBG_WRITE_UART_DATA = (Uint8)data;

#define MEM__DBG_WRITE_UINT32(num)	mem__dbg_uart_write_num((Uint32)(num));
#define MEM__DBG_WRITE_PTR(ptr)		mem__dbg_uart_write_num((Uint32)(ptr));

#define MEM__DBG_WRITE_STR(str)			\
	MEM__DBG_WRITE_UART_BYTE(0x54);	\
	mem__dbg_uart_write_str((str));

#define MEM__DBG_WRITE_INFO(inf)	\
	MEM__DBG_WRITE_UART_BYTE(0xD);	\
	MEM__DBG_WRITE_UART_BYTE(MEM__DBG_INFO_BASE + (inf));

#endif /* File / Serial */

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void mem__dbg_uart_init(void);
void mem__dbg_uart_write_num(Uint32);
void mem__dbg_uart_write_str(char*);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/


#endif /* MEM__DEBUG */

/***************************************************************//** \endcond */



#endif

