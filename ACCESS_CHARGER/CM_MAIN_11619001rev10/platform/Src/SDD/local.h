/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SDD\LOCAL.H
*
*	\ingroup	SDD
*
*	\brief		Local declarations for the SD card driver.
*
*	\details
*
*	\note
*
*	\version	24-08-2010 / Tommi Nakkila
*
*******************************************************************************/

#ifndef SDD_LOCAL_H_INCLUDED
#define SDD_LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

#include "memdrv.h"
#include "protif.h"
#include "spim.h"
#include "tstamp.h"

#include "swtimer.h"

#include "sdd.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * 	SD configuration.
 */
/*@{*/
#define SDD_TO_INIT			1000		/**< Initialization timeout			*/
#define SDD_TO_READ_TOKEN	200			/**< Read token wait timeout		*/
#define SDD_TO_BUSY_WAIT	500			/**< Busy wait timeout				*/
#define SDD_TO_TOKEN_POLL	2			/**< Token poll interval			*/
#define SDD_RETRIES			3			/**< Number of retries				*/
/*@}*/


/**
 * 	SD command defines.
 */

/*@{*/
#define SDD_CMD0			(0)			/**< GO_IDLE_STATE					*/
#define SDD_CMD1			(1)			/**< SEND_OP_COND (MMC)				*/
#define SDD_CMD8			(8)			/**< SEND_IF_COND					*/
#define SDD_CMD9			(9)			/**< SEND_CSD						*/
#define SDD_CMD10			(10)		/**< SEND_CID						*/
#define SDD_CMD12			(12)		/**< STOP_TRANSMISSION				*/
#define SDD_CMD16			(16)		/**< SET_BLOCKLEN					*/
#define SDD_CMD17			(17)		/**< READ_SINGLE_BLOCK				*/
#define SDD_CMD18			(18)		/**< READ_MULTIPLE_BLOCK			*/
#define SDD_CMD23			(23)		/**< SET_BLOCK_COUNT (MMC)			*/
#define SDD_CMD24			(24)		/**< WRITE_BLOCK					*/
#define SDD_CMD25			(25)		/**< WRITE_MULTIPLE_BLOCK			*/
#define SDD_CMD55			(55)		/**< APP_CMD						*/
#define SDD_CMD58			(58)		/**< READ_OCR						*/
#define SDD_CMD59			(59)		/**< CRC on / off					*/
#define	SDD_ACMD41			(41)		/**< SEND_OP_COND (SDC)				*/
#define SDD_ACMD13			(13)		/**< SD_STATUS (SDC)				*/
#define	SDD_ACMD23			(23)		/**< SET_WR_BLK_ERASE_COUNT (SDC)	*/
#define	SDD_DUMMY_CMD		(0xFF)		/**< Not a command					*/
#define	SDD_CMD_APP			(1<<7)		/**< App command marker				*/
#define	SDD_CMD_RETRY		(1<<6)		/**< Retry command					*/
#define SDD_CMD_MASK		0x3F		/**< Command index mask				*/
/*@}*/

/**
 * 	R1 response bits.
 */
/*@{*/
#define SDD_R1_IDLE			(1<<0)		/**< Idle bit set if idle			*/
#define SDD_R1_ERASE_RST	(1<<1)		/**< Erase reset					*/
#define SDD_R1_ILLEGAL_CMD	(1<<2)		/**< Illegal command				*/
#define SDD_R1_CRC_ERROR	(1<<3)		/**< CRC error						*/
#define SDD_R1_ERASE_ERROR	(1<<4)		/**< Erase error					*/
#define SDD_R1_ADDR_ERROR	(1<<5)		/**< Address error					*/
#define SDD_R1_PARAM_ERROR	(1<<6)		/**< Parameter error				*/
#define SDD_R1_ZERO			(1<<7)		/**< Always zero					*/
#define SDD_R1_ERROR_MASK	0x7E		/**< Error bits mask				*/
/*@}*/

/**
 * 	Error token bits.
 */
/*@{*/
#define SDD_ER_ERROR		(1<<0)		/**< Error bit						*/
#define SDD_ER_CC			(1<<1)		/**< Internal card controller error	*/
#define SDD_ER_ECC			(1<<2)		/**< ECC correction failed			*/
#define SDD_ER_RANGE		(1<<3)		/**< Argument out of range			*/
#define SDD_ER_LOCKED		(1<<4)		/**< Card is password locked		*/
/*@}*/

/**
 * 	Data response tokens.
 */
/*@{*/
#define SDD_DRT_OK			0x05		/**< Ok								*/
#define SDD_DRT_CRC			0x0B		/**< CRC error						*/
#define SDD_DRT_WR			0x0D		/**< Write error					*/
/*@}*/


/**
 * 	Card type flags.
 */
/*@{*/
#define SDD_CT_MMC			(1<<0)		/**< MMC ver 3						*/
#define SDD_CT_SD1			(1<<1)		/**< SD version 1					*/
#define SDD_CT_SD2			(1<<2)		/**< SD verion 2					*/
#define SDD_CT_SDC			(SDD_CT_SD1|SDD_CT_SD2)	/**< SD					*/
#define SDD_CT_BLOCK		(1<<3)		/* Block addressing					*/
/*@}*/

/**
 * 	Flags.
 */
/*@{*/
#define SDD_F_RW_SINGLE		(1<<0)		/**< Single read or write			*/
#define SDD_F_POWER			(1<<1)		/**< SD card power status			*/
#define SDD_F_CMD_NO_BUSY	(1<<2)		/**< Command with no busy check		*/
#define SDD_F_WR_RETRY		(1<<3)		/**< Write retry					*/
/*@}*/

/** 
 *	State machine call arguments.
 */
enum									/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	SDD__C_INIT,						/**< Init request					*/			
	SDD__C_READ,						/**< Read request					*/			
	SDD__C_WRITE,						/**< Write request					*/			
	SDD__C_ISR,							/**< Isr callback					*/
	SDD__C_CB,							/**< Protif callback				*/
	SDD__C_TIMER						/**< Polling timer					*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/**
 * 	State machine defines.
 */
/*@{*/
#define SDD__STATE_MASK		0xFF		/**< Bitmask of state bits			*/
#define	SDD__SM_MAIN		(0<<8)		/**< Main state						*/
#define	SDD__SM_INIT		(1<<8)		/**< Initialization state			*/
#define	SDD__SM_READ		(2<<8)		/**< Reading state					*/
#define	SDD__SM_WRITE		(3<<8)		/**< Writing state					*/
#define	SDD__SM_CMD			(4<<8)		/**< Command processing state		*/
#define SDD__STATE_INVALID	0xFFFF		/**< Invalid state					*/
/*@}*/


/**
 *	Main states.
 */
enum									/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	SDD__SM_IDLE,						/**< Idle							*/
	SDD__SM_RELEASE,					/**< Release SPI					*/
	SDD__SM_EXIT						/**< End request processing			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Init states.
 */
enum									/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	SDD__SI_80CLOKS,					/**< Init: 80 clocks sent			*/
	SDD__SI_CMD0,						/**< Init: SPI reserved				*/
	SDD__SI_CRC_ON,						/**< Init: CMD59 (CRC on) sent		*/
	SDD__SI_CMD0_DONE,					/**< Init: CMD0 sent				*/
	SDD__SI_CMD8_DONE,					/**< Init: CMD8 sent				*/
	SDD__SI_SDV2_ACMD41,				/**< Init: SDv2, waiting for init	*/
	SDD__SI_SDV2_CMD58,					/**< Init: SDv2, CMD58 sent			*/
	SDD__SI_SDV1_ACMD41,				/**< Init: SDv1, ACMD41 sent		*/
	SDD__SI_SDV1_INIT_WAIT,				/**< Init: SDv1, waiting for init	*/
	SDD__SI_SDV1__CMD16,				/**< Init: SDv1, CMD16 sent			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Command states.
 */
enum									/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	SDD__SC_IDLE,						/**< Idle							*/
	SDD__SC_CMD55_DONE,					/**< 								*/
	SDD__SC_WAIT_BUSY,					/**< Waiting for busy to clear		*/
	SDD__SC_WAIT_CMD,					/**< Comamnd sent, waiting for reply*/
	SDD__SC_WAIT_REPLY,					/**< Got reply to command			*/
	SDD__SC_WAIT_DATA,					/**< Waiting for command data		*/
	SDD__SC_RETRY,						/**< Command retry					*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Write states.
 */
enum									/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	SDD__SW_SEND_CMD,					/**< Spi reserved					*/
	SDD__SW_CMD_REPLY,					/**< CMD24/25 sent					*/
	SDD__SW_BUSY_WAIT,					/**< Waiting for busy to clear		*/
	SDD__SW_START_TOKEN,				/**< Start token sent				*/
	SDD__SW_DATA_WRITTEN,				/**< Data written					*/
	SDD__SW_CRC_WRITTEN,				/**< Crc written					*/
	SDD__SW_REPLY_READ,					/**< Data reply read				*/
	SDD__SW_STOP_TOKEN_SENT,			/**< Stop token sent				*/
	SDD__SW_STOP_TOKEN_SENT_RETRY,		/**< Stop token sent before retry	*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Read states.
 */
enum									/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	SDD__SR_SEND_CMD,					/**< Spi reserved					*/
	SDD__SR_CMD_REPLY,					/**< CMD17/18 sent					*/
	SDD__SR_WAIT_TOKEN,					/**< Waiting data token				*/
	SDD__SR_WAIT_DATA,					/**< Data received					*/
	SDD__SR_WAIT_CRC,					/**< Crc received					*/
	SDD__SR_STOP_SENT,					/**< Stop transmission sent			*/
	SDD__SR_RETRY,						/**< Retry multiple read			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef struct sdd__Instance	sdd_Instance;

typedef protif_Data * (* 	sdd__fnState)(sdd_Instance *, Uint8);

/** 
 *	Stored command data.
 */
typedef struct							/*''''''''''''''''''''''''''''' RAM */
{										/*									*/
	Uint32					data;		/**< Command data					*/
	Uint16					next_state;	/**< Next state to set after cmd	*/
	Uint8					cmd;		/**< Command id						*/
} sdd_StoredCommand;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/** 
 *	Command processing data.
 */
typedef struct							/*''''''''''''''''''''''''''''' RAM */
{										/*									*/
	sdd_StoredCommand		stored;		/**< Stored command data			*/
	sdd_StoredCommand		current;	/**< Current command data			*/
	tstamp_Stamp			tstamp;		/**< Command timeout stamp			*/
	Uint16					next_state;	/**< Next state to set				*/
	Uint8					reply_cnt;	/**< Expected reply byte count		*/
	Uint8					stuff_bytes;/**< Stuff bytes in cmd reply		*/
	Uint8					data_bytes;	/**< Data bytes in reply			*/
	Uint8					retries;	/**< Retries on crc error			*/
	BYTE					data[10];	/**< Command buffer					*/
} sdd_Command;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/** 
 *	SD card driver interface.
 */
struct	sdd__Instance					/*''''''''''''''''''''''''''''' RAM */
{										/*									*/
	sdd_Init const_P *		pInit;		/**< Initialization data			*/
	sdd_Command				cmd;		/**< Command processing data		*/
	protif_Request			request;	/**< Protif request					*/
	swtimer_Timer_st		timer;		/**< Delayed operation timer		*/
	tstamp_Stamp			tstamp_d;	/**< Operation timeout stamp		*/
	memdrv_Alloc *			pRequest;	/**< Current memdrv request			*/
	Uint8 *					pBufferPtr;	/**< Current read / write buffer ptr*/
	sdd__fnState			fnState;	/**< Current state handler func		*/
	void *					pProtif;	/**< Protif handler ptr				*/
	Uint32					rw_address;	/**< Read / write address			*/
	Uint16					state;		/**< State machine state			*/
	Uint8					status;		/**< SD status						*/
	BYTE					token;		/**< Read / write token				*/
	Uint8					ret_code;	/**< Request return code			*/
	Uint8					card_type;	/**< Card type flags				*/
	Uint8					flags;		/**< Misc flags						*/
	Uint8					rw_retry;	/**< Read / write retries			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

Uint16						sdd__crc16(Uint8 *, Uint16);
Uint8						sdd__crc7(Uint8 *, Uint16);

/******************************************************************************/

#endif
