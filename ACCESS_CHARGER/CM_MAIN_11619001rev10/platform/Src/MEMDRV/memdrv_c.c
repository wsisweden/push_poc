/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		memdrv_c.c
*
*	\ingroup	MEMDRV
*
*	\brief		
*
*	\details
*
*	\note
*
*	\version	20-09-2012 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "sys.h"
#include "MEM.H"

#include "memdrv.h"
#include "memdrv_c.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * Configuration.
 */
/*@{*/
#define MEMDRV__C_FIFO		6			/**< Command fifo size.				*/
/*@}*/


/**
 * Driver flags.
 */
/*@{*/
#define MEMDRV__CFLG_INIT	(1<<0)		/**< Init needs to be done.			*/
#define MEMDRV__CFLG_DIRTY	(1<<1)		/**< Current sector dirty.			*/
/*@}*/

/**
 * Driver states.
 */
enum
{
	MEMDRV__CST_IDLE,					/**< Idle.							*/
	MEMDRV__CST_INIT,					/**< Initializing driver.			*/
	MEMDRV__CST_PUT,					/**< Write to targte driver.		*/
	MEMDRV__CST_GET,					/**< Read from target driver.		*/
	MEMDRV__CST_DOOP,					/**< Do-operation on target driver.	*/
	MEMDRV__CST_ERASE,					/**< Erasing driver sector.			*/
	MEMDRV__CST_WRITE,					/**< Writing driver sector.			*/
	MEMDRV__CST_LOAD,					/**< Loading driver sector.			*/
	MEMDRV__CST_COUNT					/**< Number of states.				*/
};

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/** 
 *	\brief	Initializes / resets command FIFO.
 *	\param	i_	Pointer to command FIFO.
 */
#define memdrv__cacheCmdFifoInit(i_)										\
	{																		\
		(i_)->read = 0;														\
		(i_)->write = 0;													\
	}

/** 
 *	\brief	Gets item from command FIFO.
 *	\param	i_	Pointer to command FIFO.
 *	\param	c_	Retrieved command will be assigned to this variable.
 */
#define memdrv__cacheCmdFifoGet(i_, c_)										\
	{																		\
		deb_assert((i_)->read != (i_)->write);								\
		c_ = (i_)->fifo[(i_)->read++];										\
		if ((i_)->read >= MEMDRV__C_FIFO) {									\
			(i_)->read = 0;													\
		}																	\
	}

/** 
 *	\brief	Puts command to command FIFO.
 *	\param	i_	Pointer to command FIFO.
 *	\param	c_	Command to put.
 */
#define memdrv__cacheCmdFifoPut(i_, c_)										\
	{																		\
		(i_)->fifo[(i_)->write++] = c_;										\
		if ((i_)->write >= MEMDRV__C_FIFO) {								\
			(i_)->write = 0;												\
		}																	\
		deb_assert((i_)->read != (i_)->write);								\
	}

/** 
 *	\brief	Checks if command FIFO is empty.
 *	\param	i_	Pointer to command FIFO.
 *	\retval	TRUE	Command FIFO is empty.
 *	\retval	FALSE	Command FIFO is not empty.
 */
#define memdrv__cacheCmdFifoEmpty(i_)										\
		((i_)->read == (i_)->write)

/** 
 *	\brief	Fills memory driver request for read / write.
 *	\param	i_	Cache driver instance.
 *	\param	d_	Pointer to read / write data buffer.
 *	\param	a_	Destination address in target driver sectors.
 *	\param	c_	Read / write size in bytes.
 */
#define memdrv__cacheFillRequest(i_, d_, a_, c_)							\
	{																		\
		pInst->request.pData = d_;											\
		if ((i_)->driver.flags & MEMDRV_FLG_BLOCK) {						\
			(i_)->request.address = a_;										\
			(i_)->request.count = (c_) / pInst->driver.sectorSize;			\
		} else {															\
			(i_)->request.address = (a_) * pInst->driver.sectorSize;		\
			(i_)->request.count = c_;										\
		}																	\
	}
		

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/** 
 *
 */
typedef struct							/*''''''''''''''''''''''''''''' RAM */
{										/*									*/
	Uint8					fifo[MEMDRV__C_FIFO];	/**< Command fifo.		*/
	Uint8					read;		/**< FIFO read position.			*/
	Uint8					write;		/**< FIFO write position.			*/
} memdrv__CacheCommands;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Driver instance type
 */
typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	memdrv_CacheConfig const_P * pConfig;/**< Config info.					*/
	memdrv_Alloc			request;	/**< Memdrv request to driver.		*/
	memdrv_Alloc *			pServiced;	/**< Serviced request pointer.		*/
	memdrv_MemInfo			driver;		/**< Target driver information.		*/
	BYTE *					pCache;		/**< Data cache.					*/
	memdrv__Sector			currentSector;/**< Current target sector.		*/
	memdrv__Sector			nextSector;	/**< Next target sector.			*/
	Uint16					sectorRatio;/**< Ration of target and this driver 
												sectors.					*/
	Uint32					dataOffset;	/**< Read / write offset.			*/
	Uint8					flags;		/**< Instance status flags.			*/
	memdrv__CacheCommands	cmdFifo;	/**< Command FIFO.					*/
	Uint8					cmdCurrent;	/**< Current command.				*/
	void (*		fnStored)(struct memdrv__alloc *); /**< */
	void *					pStoredUtil;/**< */
} memdrv__CacheInstance;				/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void				memdrv__cachePut(void *,memdrv_Alloc *);
PRIVATE void				memdrv__cacheGet(void *,memdrv_Alloc *);
PRIVATE void				memdrv__cacheBusy(void *,memdrv_Alloc *);
PRIVATE void				memdrv__cacheDoOp(void *,memdrv_Alloc *);

PRIVATE void				memdrv__cacheFinalize(memdrv__CacheInstance *, Uint8);

PRIVATE Boolean				memdrv__cacheSwitchSector(memdrv__CacheInstance *, memdrv__Sector);
PRIVATE Boolean				memdrv__cacheFlushData(memdrv__CacheInstance *);

PRIVATE void				memdrv__cacheStateMachine(memdrv__CacheInstance *);
PRIVATE Boolean				memdrv__cacheReadWrite(memdrv__CacheInstance *, Boolean, memdrv_Alloc *);

PRIVATE void				memdrv__cacheCallback(memdrv_Alloc *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface for the driver. The type of the interface object is
 * specified by the MEMDRV interface. A pointer to this interface object should
 * be passed as an initialization parameter to each FB that will be using this
 * driver.
 */

PUBLIC memdrv_Interface const_P memdrv_cacheInterface = {
	memdrv__cachePut,
	memdrv__cacheGet,
	memdrv__cacheBusy,
	memdrv__cacheDoOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes cache driver.
*
*	\param		pConfig		Pointer to initialization data.
*
*	\return		Pointer to driver instance.
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void * memdrv__cacheInit(
	memdrv_CacheConfig const_P *	pConfig
) {
	memdrv__CacheInstance *	pInst;

	deb_assert(!(pConfig->cacheSize % pConfig->sectorSize));

	pInst = mem_reserve(&mem_normal, sizeof(memdrv__CacheInstance));
	deb_assert(pInst != NULL);

	pInst->pCache = mem_reserve(&mem_normal, pConfig->cacheSize);
	deb_assert(pInst->pCache != NULL);

	pInst->pConfig = pConfig;
	pInst->flags = MEMDRV__CFLG_INIT;
	pInst->pServiced = NULL;
	pInst->currentSector = Uint16_MAX;

	pInst->request.pUtil = pInst;
	pInst->request.fnDone = memdrv__cacheCallback;

	memdrv__cacheCmdFifoInit(&pInst->cmdFifo);

	return (void *)pInst;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Writes data to memory asynchronously.
*
*	\param		pVoidInst	Pointer to driver instance.
*	\param		pRequest	Pointer to MEMDRV request.
*
*	\details	Request will be processed and callback registered in request
*				will be called when done.
*
*	\note		It is possible that this function will call the callback before
*				the function returns.
*
*	\sa			
*
*******************************************************************************/
PRIVATE void memdrv__cachePut(
	void * 					pVoidInst,
	memdrv_Alloc *			pRequest
) {
	memdrv__CacheInstance *	pInst = (memdrv__CacheInstance *)pVoidInst;

	deb_assert(pInst->pServiced == NULL);

	pInst->pServiced = pRequest;
	pInst->dataOffset = 0;

	if (pInst->flags & MEMDRV__CFLG_INIT) {
		memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_INIT);
	}

	memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_PUT);

	memdrv__cacheStateMachine(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Reads data from memory asynchronously.
*
*	\param		pVoidInst	Pointer to driver instance.
*	\param		pRequest	Pointer to MEMDRV request.
*
*	\details	Request will be processed and callback registered in request
*				will be called when done.
*
*	\note		It is possible that this function will call the callback before
*				the function returns.
*
*	\sa			
*
*******************************************************************************/
PRIVATE void memdrv__cacheGet(
	void * 					pVoidInst,
	memdrv_Alloc *			pRequest
) {
	memdrv__CacheInstance *	pInst = (memdrv__CacheInstance *)pVoidInst;

	deb_assert(pInst->pServiced == NULL);

	pInst->pServiced = pRequest;
	pInst->dataOffset = 0;

	if (pInst->flags & MEMDRV__CFLG_INIT) {
		memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_INIT);
	}

	memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_GET);

	memdrv__cacheStateMachine(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check is memory is busy.
*
*	\param		pVoidInst	Pointer to driver instance.
*	\param		pRequest	Pointer to MEMDRV request.
*
*	\details	This function is not supported by the driver.
*
*	\note		It is possible that this function will call the callback before
*				the function returns.
*
*	\sa			
*
*******************************************************************************/
PRIVATE void memdrv__cacheBusy(
	void * 					pVoidInst,
	memdrv_Alloc *			pRequest
) {
	memdrv__CacheInstance *	pInst = (memdrv__CacheInstance *)pVoidInst;

	deb_assert(pInst->pServiced == NULL);

	deb_assert(FALSE);

	pInst->pServiced = pRequest;

	memdrv__cacheFinalize(pInst, MEMDRV_NOT_SUPPORTED);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Drivers do-operation funtion.
*
*	\param		pVoidInst	Pointer to driver instance.
*	\param		pRequest	Pointer to MEMDRV request.
*
*	\details	Request will be processed and callback registered in request
*				will be called when done. Field \a op in request will determine
*				what operation will be executed.
*
*				This driver handles MEMDRV_OP_GET_INFO and MEMDRV_OP_FLUSH
*				operations, others are routed to connected driver.
*
*	\note		It is possible that this function will call the callback before
*				the function returns.
*
*	\sa			
*
*******************************************************************************/
PRIVATE void memdrv__cacheDoOp(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	memdrv__CacheInstance *	pInst = (memdrv__CacheInstance *)pInstance;

	deb_assert(pInst->pServiced == NULL);

	pInst->pServiced = pRequest;

	if (pInst->flags & MEMDRV__CFLG_INIT) {
		memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_INIT);
	}

	memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_DOOP);

	memdrv__cacheStateMachine(pInst);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Finalizes MEMDRV request handling.
*
*	\param		pInst		Pointer to driver instance.
*	\param		code		Return code to set.
*
*	\details	Clears internal states and calls callback fucntion of the 
*				request being processed.
*
*				This function should be called once user request has been 
*				processed.
*
*	\note		It is safe to call this even is a request is not being 
*				processed.
*
*	\sa			
*
*******************************************************************************/
PRIVATE void memdrv__cacheFinalize(
	memdrv__CacheInstance *	pInst,
	Uint8					code
) {
	pInst->cmdCurrent = MEMDRV__CST_IDLE;
	memdrv__cacheCmdFifoInit(&pInst->cmdFifo);

	if (pInst->pServiced) {

		memdrv_Alloc *	pReq = pInst->pServiced;

		pInst->pServiced = NULL;

		pReq->count = code;
		pReq->fnDone(pReq);
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Cache driver state machine.
*
*	\param		pInst		Pointer to driver instance.
*
*	\details	Gets next command from command FIFO and processed it. This is
*				called by the MEMDRV interface functions and the target driver
*				callback fucntion.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void memdrv__cacheStateMachine(
	memdrv__CacheInstance *	pInst
) {
	pInst->cmdCurrent = MEMDRV__CST_IDLE;

	/*
	 *	Process next command.
	 */
	while (pInst->cmdCurrent == MEMDRV__CST_IDLE) {

		if (memdrv__cacheCmdFifoEmpty(&pInst->cmdFifo)) {
			pInst->cmdCurrent = MEMDRV__CST_IDLE;
			break;
		}

		/*
		 *	Process next command.
		 */
		memdrv__cacheCmdFifoGet(&pInst->cmdFifo, pInst->cmdCurrent);

		switch (pInst->cmdCurrent)
		{
		/*
		 *	Initialize driver by reading info of connected driver.
		 */
		case MEMDRV__CST_INIT:
			pInst->request.op = MEMDRV_OP_GET_INFO;
			pInst->request.pData = (BYTE *)&pInst->driver;
			pInst->pConfig->pInterface->doOp(
				*pInst->pConfig->pDriver, 
				&pInst->request
			);
			break;

		/*
		 *	Write to cache.
		 */
		case MEMDRV__CST_PUT:
			if (!memdrv__cacheReadWrite(pInst, TRUE, pInst->pServiced)) {
				/* 
				 *	Write not completed right away. Write pushed erase / write
				 *	and load to command stack, repush PUT so that it will be
				 *	executed again after sector is ready.
				 */
				memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_PUT);
			}

			/* Force next or clear immediately executed command. */
			pInst->cmdCurrent = MEMDRV__CST_IDLE;
			break;

		/*
		 *	Read from cache.
		 */
		case MEMDRV__CST_GET:
			if (!memdrv__cacheReadWrite(pInst, FALSE, pInst->pServiced)) {
				/* 
				 *	Write not completed right away. Write pushed erase / write
				 *	and load to command stack, repush PUT so that it will be
				 *	executed again after sector is ready.
				 */
				memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_GET);
			}

			/* Force next or clear immediately executed command. */
			pInst->cmdCurrent = MEMDRV__CST_IDLE;
			break;

		/*
		 *	Driver do operation.
		 */
		case MEMDRV__CST_DOOP:
			if (pInst->pServiced->op == MEMDRV_OP_GET_INFO) {

				memdrv_MemInfo *		pMemInfo;
				Uint32					driverSize;

				pMemInfo = (memdrv_MemInfo *) (void *)pInst->pServiced->pData;

				driverSize = pInst->driver.sectorCount * pInst->driver.sectorSize;

				pMemInfo->flags = MEMDRV_FLG_BLOCK | MEMDRV_FLG_FLUSH;
				pMemInfo->sectorCount = driverSize / pInst->pConfig->sectorSize;
				pMemInfo->sectorSize = pInst->pConfig->sectorSize;

				pInst->cmdCurrent = MEMDRV__CST_IDLE;

			} else if (pInst->pServiced->op == MEMDRV_OP_FLUSH) {

				/*
				 *	Start / execute flush. Either way current state needs to be
				 *	idle (done / force next).
				 */
				memdrv__cacheFlushData(pInst);
				pInst->cmdCurrent = MEMDRV__CST_IDLE;

			} else {

				/*
				 *	Request is routed directly to attached driver. Replace
				 *	callback and util pointer so that return call is captured.
				 */
				pInst->fnStored = pInst->pServiced->fnDone;
				pInst->pStoredUtil = pInst->pServiced->pUtil;

				pInst->pServiced->fnDone = memdrv__cacheCallback;
				pInst->pServiced->pUtil = pInst;

				pInst->pConfig->pInterface->doOp(
					*pInst->pConfig->pDriver, 
					pInst->pServiced
				);
			}
			break;

		/*
		 *	Erase sector from driver. Sector is always the current sector.
		 */
		case MEMDRV__CST_ERASE:
			if (pInst->driver.flags & MEMDRV_FLG_BLOCK) {
				pInst->request.address = pInst->currentSector;
			} else {
				pInst->request.address = pInst->currentSector * pInst->driver.sectorSize;
			}

			pInst->request.op = MEMDRV_OP_ERASE_SECTOR;
			pInst->pConfig->pInterface->doOp(
				*pInst->pConfig->pDriver, 
				&pInst->request
			);
			break;

		/*
		 *	Write sector to driver. Sector is always the current sector.
		 */
		case MEMDRV__CST_WRITE:

			memdrv__cacheFillRequest(
				pInst, 
				pInst->pCache, 
				pInst->currentSector, 
				pInst->pConfig->cacheSize
			);

			pInst->pConfig->pInterface->put(
				*pInst->pConfig->pDriver, 
				&pInst->request
			);
			break;

		/*
		 *	Load sector from driver. Sector is always next sector.
		 *	Current sector is updated to next sector.
		 */
		case MEMDRV__CST_LOAD:
			memdrv__cacheFillRequest(
				pInst, 
				pInst->pCache, 
				pInst->nextSector, 
				pInst->pConfig->cacheSize
			);

			pInst->currentSector = pInst->nextSector;

			pInst->pConfig->pInterface->get(
				*pInst->pConfig->pDriver, 
				&pInst->request
			);
			break;

		default:
			break;
		}
	}

	if (
		(pInst->cmdCurrent == MEMDRV__CST_IDLE) && 
		memdrv__cacheCmdFifoEmpty(&pInst->cmdFifo)
	) {
		memdrv__cacheFinalize(pInst, MEMDRV_OK);
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Target driver calback function.
*
*	\param		pRequest	Pointer to MEMDRV request.
*
*	\details	This function is called once the target driver has processed
*				a request sent by this driver.
*
*				If error is noticed this driver finalizes the request 
*				immediately, else the state machine is called.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE void memdrv__cacheCallback(
	memdrv_Alloc *			pRequest
) {
	memdrv__CacheInstance *	pInst = (memdrv__CacheInstance *)pRequest->pUtil;

	/*
	 *	If this is a callback to captured do-op command restore the original
	 *	callback and util values.
	 */
	if (pInst->cmdCurrent == MEMDRV__CST_DOOP) {
		deb_assert(pRequest == pInst->pServiced);
		pRequest->fnDone = pInst->fnStored;
		pRequest->pUtil = pInst->pStoredUtil;
	}

	if (pRequest->count == MEMDRV_OK) {
		/*
		 *	Process end of previous command.
		 */
		switch (pInst->cmdCurrent)
		{
		/*
		 *	Init command => calculate variables needed for sector 
		 *	transformation.
		 */
		case MEMDRV__CST_INIT:
			pInst->flags &= ~MEMDRV__CFLG_INIT;
			pInst->sectorRatio = pInst->driver.sectorSize / pInst->pConfig->sectorSize;
			break;

		/*
		 *	Load operation done => clear dirty flag from current sector.
		 */
		case MEMDRV__CST_LOAD:
			/* Mark loaded sector as not dirty. */
			pInst->flags &= ~MEMDRV__CFLG_DIRTY;
			break;

		case MEMDRV__CST_DOOP:
		case MEMDRV__CST_PUT:
		case MEMDRV__CST_GET:
		case MEMDRV__CST_ERASE:
		case MEMDRV__CST_WRITE:
		default:
			break;
		}

		memdrv__cacheStateMachine(pInst);

	} else {
		memdrv__cacheFinalize(pInst, (Uint8)pRequest->count);
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Flushes cache data to target driver.
*
*	\param		pInst		Pointer to driver instance.
*
*	\retval		TRUE		Request was processed immediately (nothing to 
*							flush).
*	\retval		FALSE		Request was added to command FIFO.
*
*	\details	Does not actually flush the data, just adds the commands for 
*				erasing (if necessary) and writing to command FIFO.
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean memdrv__cacheFlushData(
	memdrv__CacheInstance *	pInst
) {
	if (pInst->flags & MEMDRV__CFLG_DIRTY) {

		if (pInst->driver.flags & MEMDRV_FLG_ERASE) {

			/*
			 *	Driver requires erasing => erase current sector.
			 */
			memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_ERASE);
		} 

		memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_WRITE);

		return FALSE;
	}

	return TRUE;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		
*
*	\param		pInst		Pointer to driver instance.
*	\param		isWrite		If TRUE write is done, else read.
*	\param		pRequest	Pointer to user request.
*
*	\retval		TRUE		Request was executed immediately.
*	\retval		FALSE		Not all data was written yet.
*
*	\details	Writes data to cache, sectors are flushed / loaded as necessary.
*				Current state of the operation is stored so that this function
*				can be called multiple times until all data has been processed.
*
*	\note		Flushing / loading is not done in this fucntion, commands are
*				just added to command FIFO.
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean memdrv__cacheReadWrite(
	memdrv__CacheInstance *	pInst,
	Boolean					isWrite,
	memdrv_Alloc *			pRequest
) {
	memdrv__Sector			driverSector;
	Uint32					offset;
	Uint32					count;
	memdrv__Sector			writeSectors;
	Uint32					dataOffset;
	Uint32					address;

	/*
	 *	Calculate address based on request address and written / read size.
	 */
	address = pRequest->address + pInst->dataOffset / pInst->pConfig->sectorSize;

	driverSector = (memdrv__Sector)address / pInst->sectorRatio;

	if (!memdrv__cacheSwitchSector(pInst, driverSector)) {
		/* Sector is not yet ready for writing. */
		return FALSE;
	}

	offset = address % pInst->sectorRatio; /* Sector offset. */

	/*
	 *	Limit copying to end of driver sector.
	 */
	if (pRequest->count > (pInst->sectorRatio - offset)) {
		writeSectors = (memdrv__Sector)(pInst->sectorRatio - offset);
	} else {
		writeSectors = pRequest->count;
	}

	offset *= pInst->pConfig->sectorSize; /* Byte offset. */
	count = writeSectors * pInst->pConfig->sectorSize;
	dataOffset = pInst->dataOffset;

	if (isWrite) {

		while (count) {

			pInst->pCache[offset] = pRequest->pData[dataOffset];

			offset++;
			dataOffset++;
			count--;
		}

		/* Mark sector as modified. */
		pInst->flags |= MEMDRV__CFLG_DIRTY;

	} else {

		while (count) {

			pRequest->pData[dataOffset] = pInst->pCache[offset];

			offset++;
			dataOffset++;
			count--;
		}

	}

	pRequest->count -= writeSectors;
	pInst->dataOffset = dataOffset;

	return (pRequest->count == 0);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Switches cached sector.
*
*	\param		pInst		Pointer to driver instance.
*	\param		sector		Sector to load.
*
*	\retval		TRUE		Request was processed immediately (sector is current
*							sector).
*	\retval		FALSE		Request was added to command FIFO.
*
*	\details	Switches from current sector to target sector. If necessary
*				current sector is flushed before the new sector is loaded).
*
*	\note		Flushing / loading is not done in this fucntion, commands are
*				just added to command FIFO.
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean memdrv__cacheSwitchSector(
	memdrv__CacheInstance *	pInst,
	memdrv__Sector			sector
) {
	if (sector != pInst->currentSector) {

		/* Check if current sector needs flushing. */
		memdrv__cacheFlushData(pInst);

		/* Load new sector. */
		pInst->nextSector = sector;
		memdrv__cacheCmdFifoPut(&pInst->cmdFifo, MEMDRV__CST_LOAD);

		return FALSE;
	}

	return TRUE;
}
