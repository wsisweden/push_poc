/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	IICMSTR
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	dd-mm-yy / Tommi Nakkila
*
*******************************************************************************/
 
#ifndef IICMSTR_INIT_H_INCLUDED
#define IICMSTR_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */


#define iicmstr_reset		dummy_reset
#define iicmstr_down		dummy_down
#define iicmstr_read		dummy_read
#define iicmstr_write		dummy_write
#define iicmstr_ctrl		dummy_ctrl
#define iicmstr_test		dummy_test

#include "iicmstr.h"

/**
 * Initialization data type
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	iicmstr__If	const_P *	fn;			/**< Function interface table.      */
	iicmstr_Settings		settings;	/**< I2C settings table.            */
	Uint32					busTimeout;	/**< I2C bus timeout (ms).			*/
} iicmstr_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_IICMSTR
#elif defined(EXE_GEN_IICMSTR)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_IICMSTR
#endif

#define EXE_APPL(n)			n##iicmstr

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN /**< Non-volatile numeric                                */
#define EXE_USE_NS /**< Non-volatile string                                 */
//#define EXE_USE_VN /**< Volatile numeric                                    */
//#define EXE_USE_VS /**< Volatile string                                     */
//#define EXE_USE_PN /* Process-point numeric	*/
//#define EXE_USE_PS /* Process-point string	*/
//#define EXE_USE_BL /* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name      Flags           Dim  Type  Def  Min  Max
            --------- --------------- ---- ----- ---- --- ----*/
EXE_REG_NN( myRName1, SYS_REG_DEFAULT,  1, Uint8,  10,  0, 255 )
EXE_REG_NS( myRName2, SYS_REG_DEFAULT,  5,      "Test"         )
//EXE_REG_VN( myRName3, SYS_REG_DEFAULT,  1, Uint8,  10,  0, 255 )
//EXE_REG_VS( myRName4, SYS_REG_DEFAULT,  5,      "Test"         )
//EXE_REG_PN( myRName5, SYS_REG_DEFAULT,  1, Uint8,       0, 255 )
//EXE_REG_PS( myRName6, SYS_REG_DEFAULT,  1,         10          )
//EXE_REG_BL( myRName7, SYS_REG_DEFAULT,                         )

EXE_MMI_NONE( myRName1 )
EXE_MMI_NONE( myRName2 )
//EXE_MMI_NONE( myRName3 )
//EXE_MMI_NONE( myRName4 )
//EXE_MMI_NONE( myRName5 )
//EXE_MMI_NONE( myRName6 )
//EXE_MMI_NONE( myRName7 )


/* References to external registers */

//EXE_REG_EX( myRName8, thatRName, thatInstanceName )


/* Write-indications */

//EXE_IND_ME( myIndName3, myRName3 )
//EXE_IND_EX( myIndName2, thatRName, thatInstanceName )

/* *****************************************************************************
 *	
 *	ERR declarations
 */

EXE_ERR_TYPE(IICMSTR__ERR_TIMEOUT, tErrTimeout, iicmstr, ERR_CLASS(info))	/**< I2C timeout */

/* Language-dependent texts */

EXE_TXT( tErrTimeout, EXE_T_EN("I2C timeout") EXE_T_FI("Param kuvaus 1") "")

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
