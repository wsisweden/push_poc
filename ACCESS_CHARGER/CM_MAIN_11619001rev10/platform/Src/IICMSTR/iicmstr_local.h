/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	IICMSTR
*
*	\brief		Local declarations for the IICMSTR function block.
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef IICMSTR_LOCAL_H_INCLUDED
#define IICMSTR_LOCAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "iicmstr.h"
#include "protif.h"
#include "lib.h"
#include "swtimer.h"

#include "init.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

enum {
	IICMSTR__IDLE,
	IICMSTR__BUSY,
	IICMSTR__DONE
};

/**
 * Type for the iicmstr instance.
 */

struct iicmstr__Inst {					/*''''''''''''''''''''''''''''' RAM */
	osa_CoTaskType			task;		/**< Task                           */
	iicmstr_Init const_P * 	pInit;		/**< Initialization data            */
	sys_FBInstId			instId;		/**< Instance ID					*/
	lib_FifoType			fifo;		/**< Protif request fifo            */
	Uint8					state;		/**<                                */
	swtimer_Timer_st		timer;		/**< I2C bus timeout timer.			*/
	Uint8					result;		/**< Protif request result code		*/
	protif_Request *		pReq;		/**< Current protif request			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/* iicmstrc.c */
PUBLIC protif_Data * iicmstr__copyCombinedData(BYTE *,Uint16,protif_Data *,Uint16 *);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
