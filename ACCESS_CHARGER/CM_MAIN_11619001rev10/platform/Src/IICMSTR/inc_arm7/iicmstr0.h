/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_arm7/IICMSTR0.H
*
*	\ingroup	IICMSTR_ARM7
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version
*
*******************************************************************************/

#ifndef IICMSTR0_H_INCLUDED
#define IICMSTR0_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

#define IICMSTR_ISR_FUNC	iicmstr0__isr		/**< I2C interrupt function */
#define IICMSTR_INIT_FUNC	iicmstr0__init		/**< Init function          */
#define IICMSTR_PROC_FUNC	iicmstr0__process	/**< Process function       */
#define IICMSTR_IF_TBL		iicmstr_if0			/**< Interface table        */
#define IICMSTR_IF_NOINIT	iicmstr_if0NoInit	/**< Interface table without init */

#define IICMSTR_VIC_BIT		9					/**< VIC bit                */
#define IICMSTR_VIC_ADDR	VICVectAddr9		/**< VIC address            */
#define IICMSTR_VIC_CNT		VICVectCntl9		/**< VIC priority           */

#define IICMSTR_PCONP		7					/**< Power bit for PCONP    */

#define IICMSTR_PCLKSEL		PCLKSEL0			/**< Clock setting register */
#define IICMSTR_PCLKSEL_MSK	(1<<14)				/**< Clock setting bit      */
	
#define IICMSTR_PINSEL		PINSEL1				/**< Pin register           */
#define IICMSTR_PINSEL_MSK	((1<<22)|(1<<24))	/**< Mode bits              */

#define IICMSTR_CONSET		I20CONSET			/**< Control register       */
#define IICMSTR_CONCLR		I20CONCLR			/**< Control clear register */
#define IICMSTR_STAT		I20STAT				/**< Status register        */
#define IICMSTR_DAT			I20DAT				/**< Data register          */
#define IICMSTR_ADDR		I20ADR				/**< Slave address register */

#define IICMSTR_SCLL		I20SCLL				/**< SCL low duty register  */
#define IICMSTR_SCLH		I20SCLH				/**< SCL high duty register */


/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/



/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/




/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/




/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/



/*****************************************************************************/

#endif
