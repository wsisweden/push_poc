/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	IICMSTR_ARM7
*
*	\brief		
*
*	\details		
*
*	\note		
*
*	\version	
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	IICMSTR_ARM7	ARM7
*
*	\ingroup	IICMSTR
*
*	\brief		I2C master implementation for ARM7 target.
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

#ifndef IICMSTR_ARM7_LOCAL_H_INCLUDED
#define IICMSTR_ARM7_LOCAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "iicmstr.h"
#include "lib.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

//#define IICMSTR_FLAG_TX		(1<<0)			/**<						*/
#define IICMSTR_FLAG_DONE	(1<<1)				/**<                        */


#define IICMSTR_READ_BIT	1					/**< If set it is a read
												operation                   */

#define IICMSTR_CS_ENA		(1<<6)				/**<                        */
#define IICMSTR_CS_START	(1<<5)				/**<                        */
#define IICMSTR_CS_STOP		(1<<4)				/**<                        */
#define IICMSTR_CS_SI		(1<<3)				/**<                        */
#define IICMSTR_CS_AA		(1<<2)				/**<                        */

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/


/**
 *	IICMSTR ARM7 instance structure
 */
typedef struct							/*''''''''''''''''''''''''''''''''''*/
{										/*									*/
	iicmstr_Inst *			pMain;		/**< Ptr to generic IICMSTR instance*/
	protif_Data *			pSubReq;	/**< Ptr to request                 */
	Uint16					pos;		/**< Read / write position          */
	Uint8					slave;		/**< Slave address                  */
	Uint8					flags;		/**< State flags                    */
	Uint8					result;		/**< Result code                    */
} iicmstr__Hw;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/



/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
