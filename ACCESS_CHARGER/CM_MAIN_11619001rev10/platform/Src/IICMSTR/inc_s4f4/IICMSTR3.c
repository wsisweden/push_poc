/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	IICMSTR_S4F4
*
*	\brief		Implementation for the I2C3 peripheral.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 4459 $ \n
*				\$Date: 2021-03-12 16:28:35 +0200 (pe, 12 maalis 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E */
#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "deb.h"
#include "hw.h"
#include "mem.h"

/* Local */
#include "..\iicmstr_local.h"
#include "iicmstr_s4f4local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* Interface functions */
PRIVATE void iicmstr__init3(iicmstr_Inst *);
PRIVATE void iicmstr__process3(void);

/* HAL callback functions */
PRIVATE void iicmstr__hwMspInit3SclPa8Cb(I2C_HandleTypeDef *);
PRIVATE void iicmstr__hwMspNoPinInitCb(I2C_HandleTypeDef *);
PRIVATE void iicmstr__hwMasterTxCompCb(I2C_HandleTypeDef *);
PRIVATE void iicmstr__hwMasterRxCompCb(I2C_HandleTypeDef *);
PRIVATE void iicmstr__hwErrorCb(I2C_HandleTypeDef *);

/* Request handling functions */
PRIVATE Boolean iicmstr__hwHandleData(iicmstr__S4f4Inst *);
PRIVATE void iicmstr__hwContinueRequest(void);
PRIVATE void iicmstr__hwTriggerTaskIsr(iicmstr__S4f4Inst *,enum protif_retCodes);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public I2C interface constant. Supplying a pointer to this constant to
 * IICMSTR as an init parameter will configure SCL on PA8 and SDA on PC9.
 */

PUBLIC const_P iicmstr__If iicmstr_if3SclPa8 = {
	iicmstr__init3,
	iicmstr__process3
};

/**
 * Public I2C interface constant. If this is supplied to IICMSTR as an init
 * parameter no pins will be configured for I2C. The pins must be configured by
 * the user to be done during startup.
 */

PUBLIC const_P iicmstr__If iicmstr_if3NoPinInit = {
	iicmstr__init3,
	iicmstr__process3
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE iicmstr__S4f4Inst			iicmstr__hwInstance;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__init3
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the I2C peripheral.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		-
*
*	\details
*
*	\note		Called from main function during startup.
*
*******************************************************************************/

PRIVATE void iicmstr__init3(
	iicmstr_Inst *			pInst
) {
	iicmstr__S4f4Inst * pHwInst;

	pHwInst = &iicmstr__hwInstance;

	pHwInst->pMain = pInst;

	/*
	 *	Allocate a buffer for combined data blocks.
	 */

	if (pInst->pInit->settings.maxCombSize == 0) {
		pHwInst->pCombBuffer = NULL;
	} else {
		pHwInst->pCombBuffer = mem_reserve(
			&mem_normal,
			pInst->pInit->settings.maxCombSize
		);
	}

	/*
	 *	Making sure all members are cleared. ST HAL requires that they are 
	 *	cleared.
	 */

	memset(&pHwInst->hwHandle, 0, sizeof(I2C_HandleTypeDef));

	/*
	 *	Configure HAL I2C init structure.
	 */

	pHwInst->hwHandle.Instance             = I2C3;

	pHwInst->hwHandle.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
	pHwInst->hwHandle.Init.ClockSpeed      = pInst->pInit->settings.busClock;
	pHwInst->hwHandle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	pHwInst->hwHandle.Init.DutyCycle       = I2C_DUTYCYCLE_16_9;
	pHwInst->hwHandle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	pHwInst->hwHandle.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;
	pHwInst->hwHandle.Init.OwnAddress1     = 0;
	pHwInst->hwHandle.Init.OwnAddress2     = 0;

	/*
	 *	Register MSP callback function which is called during HAL_I2C_Init().
	 */

	if (pInst->pInit->fn == &iicmstr_if3SclPa8) {
		HAL_I2C_RegisterCallback(
			&pHwInst->hwHandle,
			HAL_I2C_MSPINIT_CB_ID,
			iicmstr__hwMspInit3SclPa8Cb
		);

	} else {
		HAL_I2C_RegisterCallback(
			&pHwInst->hwHandle,
			HAL_I2C_MSPINIT_CB_ID,
			iicmstr__hwMspNoPinInitCb
		);
	}

	deb_verify(HAL_I2C_Init(&pHwInst->hwHandle) == HAL_OK);

	/*
	 *	Register the rest of the callback functions.
	 */

	HAL_I2C_RegisterCallback(
		&pHwInst->hwHandle,
		HAL_I2C_MASTER_TX_COMPLETE_CB_ID,
		iicmstr__hwMasterTxCompCb
	);

	HAL_I2C_RegisterCallback(
		&pHwInst->hwHandle,
		HAL_I2C_MASTER_RX_COMPLETE_CB_ID,
		iicmstr__hwMasterRxCompCb
	);

	HAL_I2C_RegisterCallback(
		&pHwInst->hwHandle,
		HAL_I2C_ERROR_CB_ID,
		iicmstr__hwErrorCb
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__process3
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes I2C requests from common IICMSTR code.
*
*	\return		-
*
*	\details	
*
*	\note		Called from the IICMSTR task.
*
*******************************************************************************/

PRIVATE void iicmstr__process3(
	void
) {
	Boolean 			success;
	iicmstr__S4f4Inst * pHwInst;

	pHwInst = &iicmstr__hwInstance;

	deb_assert(pHwInst->pMain != NULL);
	deb_assert(pHwInst->pMain->pReq != NULL);

	/*
	 * Initialize variables for ISR.
	 */

	pHwInst->pDataBlock = &pHwInst->pMain->pReq->data;

	pHwInst->slaveAddr = ((iicmstr_SlaveInfo const_P *)
		pHwInst->pMain->pReq->pTargetInfo)->address;
	pHwInst->slaveAddr <<= 1;

	/*
	 * When state is set to BUSY then a request is active
	 * and ISR can modify request and instance variables.
	 */

	atomic ( pHwInst->pMain->state = IICMSTR__BUSY; )

	success = iicmstr__hwHandleData(pHwInst);

	if (success == FALSE) {
		/*
		 *	An error occurred when trying to handle the request.
		 */

		pHwInst->pMain->result = PROTIF_ERROR;
		pHwInst->pMain->state = IICMSTR__DONE;
		osa_coTaskRun(&pHwInst->pMain->task);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	I2C3_EV_IRQHandler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		I2C event interrupt handler.
*
*	\return
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC osa_isrOSFn(
	NULL,
	I2C3_EV_IRQHandler
) {
	osa_isrEnter();
	HAL_I2C_EV_IRQHandler(&iicmstr__hwInstance.hwHandle);
	osa_isrLeave();
}
osa_endOSIsr
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	I2C3_ER_IRQHandler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		I2C error interrupt handler.
*
*	\return		
*
*	\details	
*
*	\note		
*
*******************************************************************************/

PUBLIC osa_isrOSFn(
	NULL,
	I2C3_ER_IRQHandler
) {
	osa_isrEnter();
	HAL_I2C_ER_IRQHandler(&iicmstr__hwInstance.hwHandle);
	osa_isrLeave();
}
osa_endOSIsr
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwMspInitCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Low level hardware initialization.
*
*	\param		pHandle		Pointer to ST HAL handle for the I2C peripheral.
*
*	\return		-
*
*	\details	This function is called by the ST HAL library.
*
*	\note		Called from project main function context.
*
*******************************************************************************/

PRIVATE void iicmstr__hwMspInit3SclPa8Cb(
	I2C_HandleTypeDef *		pHandle
) {  
	GPIO_InitTypeDef gpioInit;

	/*
	 *	Enable needed clocks.
	 */

	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();

	__HAL_RCC_I2C3_CLK_ENABLE();

	/*
	 *	Configure the I2C pins.
	 */

	/* I2C TX GPIO pin configuration  */
	gpioInit.Pin       = GPIO_PIN_8;
	gpioInit.Mode      = GPIO_MODE_AF_OD;
	gpioInit.Pull      = GPIO_PULLUP;
	gpioInit.Speed     = GPIO_SPEED_FAST;
	gpioInit.Alternate = GPIO_AF4_I2C3;

	HAL_GPIO_Init(GPIOA, &gpioInit);

	/* I2C RX GPIO pin configuration  */
	gpioInit.Pin = GPIO_PIN_9;
	gpioInit.Alternate = GPIO_AF4_I2C3;

	HAL_GPIO_Init(GPIOC, &gpioInit);

	/*
	 *	Enable the I2C interrupts.
	 */

	hw_enableIrq(I2C3_ER_IRQn);
	hw_enableIrq(I2C3_EV_IRQn);
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwMspNoPinInitCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Low level hardware initialization.
*
*	\param		pHandle		Pointer to ST HAL handle for the I2C peripheral.
*
*	\return		-
*
*	\details	This function is called by the ST HAL library.
*
*	\note		Called from project main function context.
*
*******************************************************************************/

PRIVATE void iicmstr__hwMspNoPinInitCb(
	I2C_HandleTypeDef *		pHandle
) {  
	/*
	 *	Enable needed clocks.
	 */

	__HAL_RCC_I2C3_CLK_ENABLE();

	/*
	 *	Enable the I2C interrupts.
	 */

	hw_enableIrq(I2C3_ER_IRQn);
	hw_enableIrq(I2C3_EV_IRQn);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwHandleData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle current data block in the request.
*
*	\param		pHwInst		Pointer to S4F4 specific instance.
*
*	\retval		TRUE		The current data block was accepted for
*							transmission.
*	\retval		FALSE		An error occurred while initializing transfer.
*
*	\details	
*
*	\note		May be called from a task or from an ISR!
*
*******************************************************************************/

PRIVATE Boolean iicmstr__hwHandleData(
	iicmstr__S4f4Inst * 	pHwInst
) {
	int 			halRet;
	protif_Data *	pDataBlock;
	protif_Data *	pNextBlock;
	Uint16 			dataSize;
	uint8_t *		pData;

	pDataBlock = pHwInst->pDataBlock;
	deb_assert(pDataBlock != NULL);
	pNextBlock = pDataBlock->pNext;

	halRet = FALSE;

	if (pNextBlock != NULL && (pNextBlock->flags & PROTIF_FLAG_CONT) != 0) {
		/*
		 *	ST HAL requires that the data is in one continous buffer. Combine
		 *	the data of all blocks with the continue flag.
		 *	The buffer size must be specified in the init parameters to the
		 *	function block.
		 */

		deb_assert(pHwInst->pCombBuffer != NULL);

		pDataBlock = iicmstr__copyCombinedData(
			pHwInst->pCombBuffer,
			pHwInst->pMain->pInit->settings.maxCombSize,
			pDataBlock,
			&dataSize
		);

		if (pDataBlock != NULL) {
			pHwInst->pDataBlock = pDataBlock;
			pData = (uint8_t *) pHwInst->pCombBuffer;
			halRet = TRUE;
		}

	} else {
		/*
		 *	This is a one block transfer. No need to copy the data.
		 */

		pData = pDataBlock->pData;
		dataSize = pDataBlock->size;
		halRet = TRUE;
	}

	if (halRet == TRUE) {
		if (pDataBlock->flags & PROTIF_FLAG_READ) {
			halRet = HAL_I2C_Master_Receive_IT(
				&pHwInst->hwHandle,
				pHwInst->slaveAddr,
				pData,
				dataSize
			);

		} else if (pDataBlock->flags & PROTIF_FLAG_WRITE) {
			halRet = HAL_I2C_Master_Transmit_IT(
				&pHwInst->hwHandle,
				pHwInst->slaveAddr,
				pData,
				dataSize
			);

		} else {
			/*
			 *	The READ or the WRITE flag should always be set.
			 */
			halRet = FALSE;
			deb_assert(FALSE);
		}
	}

	return halRet == HAL_OK ? TRUE : FALSE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwContinueRequest
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Continue handling current request.
*
*	\return		-
*
*	\details	
*
*	\note		Called from the I2C ISR!
*
*******************************************************************************/

PRIVATE void iicmstr__hwContinueRequest(
	void
) {
	iicmstr__S4f4Inst * pHwInst;

	pHwInst = &iicmstr__hwInstance;

	/*
	 *	First step to the next data block.
	 */

	pHwInst->pDataBlock = pHwInst->pDataBlock->pNext;

	if (pHwInst->pDataBlock == NULL) {
		/*
		 *	The last data block has been handled. Trigger task to call the
		 *	PROTIF callback.
		 */

		iicmstr__hwTriggerTaskIsr(pHwInst, PROTIF_OK);

	} else {
		Boolean success;

		/*
		 *	There are more data blocks to handle.
		 */

		success = iicmstr__hwHandleData(pHwInst);

		if (success == FALSE) {
			/*
			 *	Something went wrong when initializing the transfer.
			 */

			iicmstr__hwTriggerTaskIsr(pHwInst, PROTIF_ERROR);
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwTriggerTask
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Trigger task to call the PROTIF callback.
*
*	\param		pHwInst		Pointer to S4F4 specific instance.
*	\param		retCode		The result code that should be supplied to the
*							PROTIF callback.
*
*	\return		-
*
*	\details	
*
*	\note		May only be called from an ISR!
*
*******************************************************************************/

PRIVATE void iicmstr__hwTriggerTaskIsr(
	iicmstr__S4f4Inst * 	pHwInst,
	enum protif_retCodes	retCode
) {
	pHwInst->pMain->result = retCode;
	pHwInst->pMain->state = IICMSTR__DONE;
	osa_coTaskRunIsr(&pHwInst->pMain->task);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwMasterTxCompCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		I2C Master Tx Transfer completed callback.
*
*	\param		pHandle		Pointer to ST HAL handle for the I2C peripheral.
*
*	\return		
*
*	\details	
*
*	\note		Called from the I2C ISR!
*
*******************************************************************************/

PRIVATE void iicmstr__hwMasterTxCompCb(
	I2C_HandleTypeDef * 	pHandle
) {
	iicmstr__hwContinueRequest();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwMasterRxCompCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		I2C Master Rx Transfer completed callback.
*
*	\param		pHandle		Pointer to ST HAL handle for the I2C peripheral.
*
*	\return		
*
*	\details	
*
*	\note		Called from the I2C ISR!
*
*******************************************************************************/

PRIVATE void iicmstr__hwMasterRxCompCb(
	I2C_HandleTypeDef * 	pHandle
) {
	iicmstr__hwContinueRequest();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__hwErrorCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		I2C Error callback.
*
*	\param		pHandle		Pointer to ST HAL handle for the I2C peripheral.
*
*	\return		
*
*	\details	
*
*	\note		Called from the I2C ISR!
*
*******************************************************************************/

PRIVATE void iicmstr__hwErrorCb(
	I2C_HandleTypeDef * 	pHandle
) {
	Uint32 					halI2cError;
	enum protif_retCodes 	retCode;

	halI2cError = HAL_I2C_GetError(pHandle);

	if (halI2cError & HAL_I2C_ERROR_AF) {
		/*
		 *	This was an acknowledge failure.
		 */

		retCode = PROTIF_NACK;

	} else {
		retCode = PROTIF_ERROR;
	}

	iicmstr__hwTriggerTaskIsr(&iicmstr__hwInstance, retCode);
}
