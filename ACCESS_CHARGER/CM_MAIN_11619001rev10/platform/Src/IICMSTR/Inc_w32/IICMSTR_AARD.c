/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	IICMSTR_W32
*
*	\brief		IICMSTR simulation with Total Phase Aardvark.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>
#include <stdlib.h>

#include "aardvark.h"

#include "TOOLS.H"
#include "MEM.H"
#include "protif.h"
#include "iicmstr.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Aardvark module instance data.
 */

typedef struct iicmstr_aardData {
	Aardvark				aardHandle;
} iicmstr__AardData;

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr_aardvarkInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize Aardvark.
*
*	\param		portNumber		Aardvark port number.
*	\param		bitrate			Bus bitrate in kHz.
*	\param		optionFlags		Option flags. May be set to zero.
*
*   \return		Pointer to aardvark handler instance.
*
*	\details	
*
*******************************************************************************/

PUBLIC struct iicmstr_aardData * iicmstr_aardvarkInit(
	int						portNumber,
	int						bitrate,
	Uint32					optionFlags
) {
	iicmstr__AardData *		pAardData;
	AardvarkExt				aardInfo;

	pAardData = (iicmstr__AardData *) malloc(sizeof(iicmstr__AardData));

	pAardData->aardHandle = aa_open_ext(portNumber, &aardInfo);

	if (pAardData->aardHandle > 0) {
		aa_configure(pAardData->aardHandle, AA_CONFIG_GPIO_I2C);
		aa_i2c_pullup(pAardData->aardHandle, AA_I2C_PULLUP_BOTH);
		aa_target_power(pAardData->aardHandle, AA_TARGET_POWER_BOTH);
		aa_i2c_bitrate(pAardData->aardHandle, bitrate);
		aa_i2c_bus_timeout(pAardData->aardHandle, 150);
	}

	return pAardData;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr_aardvarkGpioDir
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set direction of GPIO pins.
*
*	\param		pAardData
*	\param		dirMask		Pin direction mask. A zero bit in the mask means 
*							the pin is an input.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC int iicmstr_aardvarkGpioDir(
	struct iicmstr_aardData *	pAardData,
	Uint8						dirMask
) {
	return aa_gpio_direction(pAardData->aardHandle, dirMask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr_aardvarkGpioSet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set GPIO output states.
*
*	\param		pAardData	Pointer to aardvark handler instance.
*	\param		outMask		Pin state mask.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC int iicmstr_aardvarkGpioSet(
	struct iicmstr_aardData *	pAardData,
	Uint8						outMask
) {
	return aa_gpio_set(pAardData->aardHandle, outMask);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr_aardvarkGpioCha
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Wait for state change on GPIO pin.
*
*	\param		pAardData	Pointer to aardvark handler instance.
*	\param		timeOut		Timeout in milliseconds.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC int iicmstr_aardvarkGpioCha(
	struct iicmstr_aardData *	pAardData,
	Uint16						timeOut
) {
   return aa_gpio_change(pAardData->aardHandle , timeOut);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr_aardvark
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle a PROTIF request with the Aardvark.
*
*	\param		pAardData	Pointer to aardvark handler instance.
*	\param		pRequest	Pointer to the PROTIF request.
*
*   \return		PROTIF result code.
*
*	\details	This function should be called from the project_iicmstrSimu
*				function.
*
*******************************************************************************/

PUBLIC Uint8 iicmstr_aardvark(
	struct iicmstr_aardData * pAardData,
	protif_Request *		pRequest
) {
	iicmstr_SlaveInfo const_P * pSlaveInfo;
	protif_Data *				pBlock;
	Uint8						retVal;

	pSlaveInfo = (iicmstr_SlaveInfo const_P *) pRequest->pTargetInfo;

	retVal = PROTIF_OK;
	pBlock = &pRequest->data;
	do {
		AardvarkI2cFlags	aardflags;
		BYTE *				pDataBuffer;
		Uint16				transferSize;

		/*
		 *	Check that flags are valid.
		 */
		{
			Uint8 readWriteFlags;

			deb_assert((pBlock->flags & PROTIF_FLAG_CB) == 0);
			deb_assert((pBlock->flags & PROTIF_FLAG_RES) == 0);

			readWriteFlags = pBlock->flags & (PROTIF_FLAG_READ|PROTIF_FLAG_WRITE);
			deb_assert(
				readWriteFlags == PROTIF_FLAG_READ
				|| readWriteFlags == PROTIF_FLAG_WRITE
			);
		}

		/*
		 *	Calculate the transfer size from current block and all combined
		 *	blocks.
		 */

		iicmstr__copyCombinedData(NULL, 0, pBlock, &transferSize);

		{
			int retCode;
			u16 transfered;

			if (pBlock->flags & PROTIF_FLAG_READ) {
				/*
				 *	Continue flag currently not supported for read data blocks.
				 */
				deb_assert(transferSize == pBlock->size);

				aardflags = AA_I2C_NO_FLAGS;
				if (pBlock->pNext != NULL) {
					/*
					 *	All data blocks of one protif request should be
					 *	transfered before sending stop condition.
					 */

					aardflags = AA_I2C_NO_STOP;
				}

				retCode = aa_i2c_read_ext(
					pAardData->aardHandle,
					pSlaveInfo->address,
					aardflags,
					transferSize,
					pBlock->pData,
					&transfered
				);

			} else {
				pDataBuffer = (BYTE *) malloc(transferSize);
				pBlock = iicmstr__copyCombinedData(
					pDataBuffer,
					transferSize,
					pBlock,
					NULL
				);

				aardflags = AA_I2C_NO_FLAGS;
				if (pBlock->pNext != NULL) {
					/*
					 *	All data blocks of one protif request should be
					 *	transfered before sending stop condition.
					 */

					aardflags = AA_I2C_NO_STOP;
				}

				retCode = aa_i2c_write_ext(
					pAardData->aardHandle,
					pSlaveInfo->address,
					aardflags,
					transferSize,
					pDataBuffer,
					&transfered
				);

				free(pDataBuffer);
			}

			if (retCode != AA_I2C_STATUS_OK || transfered != transferSize) {
				/*
				 *	Something went wrong when reading or writing through the
				 *	Aardvark.
				 */

				retVal = PROTIF_ERROR;
				if (
					retCode == AA_I2C_STATUS_DATA_NACK
					|| retCode == AA_I2C_STATUS_SLA_NACK
				) {
					retVal = PROTIF_NACK;
				}
				break;
			}
		}

		pBlock = pBlock->pNext;
	} while (pBlock != NULL);

	return retVal;
}
