/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	IICMSTR_W32
*
*	\brief		IICMSTR simulation with Total Phase Aardvark.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 4423 $ \n
*				\$Date: 2021-01-13 17:08:06 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>
#include <stdlib.h>

#include "TOOLS.H"
#include "protif.h"
#include "iicmstr.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__copyCombinedData
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Copy data from all blocks with the continue flag set.
*
*	\param		pBuffer			Pointer to destination buffer. Can be set to 
*								NULL to only count the total transfer size.
*	\param		bufferSize		Size of the destination buffer.
*	\param		pBlock			Pointer to first combined block.
*	\param		pTranferSize	The combined transfer size will be stored here.
*								May be set to null if the transfer size is not
*								needed.
*
*	\return		Pointer to the last combined block.
*
*	\details	Copies data of combined blocks into one continuous buffer. This
*				function is useful for targets where the HW driver does not 
*				support combined data blocks.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC protif_Data * iicmstr__copyCombinedData(
	BYTE *					pBuffer,
	Uint16					bufferSize,
	protif_Data *			pBlock,
	Uint16 *				pTranferSize
) {
	Ufast16 transferSize;
	Uint8 	firstFlag;

	/*
	 *	Store the read/write flags of the first block so that we can compare
	 *	them to combined blocks.
	 */

	firstFlag = pBlock->flags & (PROTIF_FLAG_READ|PROTIF_FLAG_WRITE);

	/*
	 *	Loop through blocks as long as they have the "continue" flag set.
	 */

	transferSize = 0U;

	do {
		Uint8 blockFlags;

		/*
		 *	Checking that the read and write flags are identical in all
		 *	combined blocks.
		 */

		blockFlags = pBlock->flags & (PROTIF_FLAG_READ|PROTIF_FLAG_WRITE);
		if (blockFlags != firstFlag) {
			/*
			 *	All combined blocks must have the same read/write flags set.
			 *	A flag mismatch has been detected.
			 */

			deb_assert(FALSE);
			transferSize = 0U;
			pBlock = NULL;
			break;
		}

		transferSize += pBlock->size;

		if (pBuffer != NULL) {

			if (transferSize <= bufferSize) {
				memcpy(pBuffer, pBlock->pData, pBlock->size);
				pBuffer += pBlock->size;

			} else {
				/*
				 * The combined block size exceeds the buffer size. A larger
				 * buffer needs to be configured.
				 */

				deb_assert(FALSE);
				transferSize = 0U;
				pBlock = NULL;
				break;
			}
		}

		pBlock = pBlock->pNext;
		if (pBlock == NULL) {
			/*
			 *	We have reached the end of the data block chain.
			 */
			break;
		}

	} while ((pBlock->flags & PROTIF_FLAG_CONT) != 0);

	if (pTranferSize != NULL) {
		*pTranferSize = (Uint16) transferSize;
	}

	return(pBlock);
}
