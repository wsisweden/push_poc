/*
 * Copyright (c) 2001-2003 Swedish Institute of Computer Science.
 * Copyright (c) 2003-2004 Leon Woestenberg <leon.woestenberg@axon.tv>
 * Copyright (c) 2003-2004 Axon Digital Design B.V., The Netherlands.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#ifndef __NETIF_ETHARP_H__
#define __NETIF_ETHARP_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_ARP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/pbuf.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "ntcpip/ip.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef ETH_PAD_SIZE
#define ETH_PAD_SIZE          0
#endif

#ifndef ETHARP_HWADDR_LEN
#define ETHARP_HWADDR_LEN     6
#endif

#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct eth_addr {
  NTCPIP__PACK_STRUCT_FIELD(Uint8 addr[ETHARP_HWADDR_LEN]);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct eth_hdr {
#if ETH_PAD_SIZE
  NTCPIP__PACK_STRUCT_FIELD(Uint8 padding[ETH_PAD_SIZE]);
#endif
  NTCPIP__PACK_STRUCT_FIELD(struct eth_addr dest);
  NTCPIP__PACK_STRUCT_FIELD(struct eth_addr src);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 type);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

#define SIZEOF_ETH_HDR (14 + ETH_PAD_SIZE)

#if ETHARP_SUPPORT_VLAN

#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct eth_vlan_hdr {
  NTCPIP__PACK_STRUCT_FIELD(Uint16 tpid);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 prio_vid);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

#define SIZEOF_VLAN_HDR 4
#define VLAN_ID(vlan_hdr) (ntcpip_htons((vlan_hdr)->prio_vid) & 0xFFF)

#endif /* ETHARP_SUPPORT_VLAN */

#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
/** the ARP message */
struct etharp_hdr {
  NTCPIP__PACK_STRUCT_FIELD(Uint16 hwtype);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 proto);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _hwlen_protolen);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 opcode);
  NTCPIP__PACK_STRUCT_FIELD(struct eth_addr shwaddr);
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip__ipAddr2 sipaddr);
  NTCPIP__PACK_STRUCT_FIELD(struct eth_addr dhwaddr);
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip__ipAddr2 dipaddr);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

#define SIZEOF_ETHARP_HDR 28
#define SIZEOF_ETHARP_PACKET (SIZEOF_ETH_HDR + SIZEOF_ETHARP_HDR)

/** 5 seconds period */
#define ARP_TMR_INTERVAL 5000

#define ETHTYPE_ARP       0x0806
#define ETHTYPE_IP        0x0800
#define ETHTYPE_VLAN      0x8100
#define ETHTYPE_PPPOEDISC 0x8863  /* PPP Over Ethernet Discovery Stage */
#define ETHTYPE_PPPOE     0x8864  /* PPP Over Ethernet Session Stage */

/** ARP message types (opcodes) */
#define ARP_REQUEST 1
#define ARP_REPLY   2

#if NTCPIP__ARP_QUEUEING
/** struct for queueing outgoing packets for unknown address
  * defined here to be accessed by memp.h
  */
struct etharp_q_entry {
  struct etharp_q_entry *next;
  struct ntcpip_pbuf *p;
};
#endif /* NTCPIP__ARP_QUEUEING */

#define etharp_init() /* Compatibility define, not init needed. */
void ntcpip__etharpTmr(void);
Int8 ntcpip__etharpFindAddr(struct ntcpip__netif *netif, struct ntcpip_ipAddr *ipaddr,
         struct eth_addr **eth_ret, struct ntcpip_ipAddr **ip_ret);
void ntcpip__etharpIpInput(struct ntcpip__netif *netif, struct ntcpip_pbuf *p);
void ntcpip__etharpArpInput(struct ntcpip__netif *netif, struct eth_addr *ethaddr,
         struct ntcpip_pbuf *p);
ntcpip_Err ntcpip_etharpOutput(struct ntcpip__netif *netif, struct ntcpip_pbuf *q, const struct ntcpip_ipAddr *ipaddr);
ntcpip_Err ntcpip__etharpQuery(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *ipaddr, struct ntcpip_pbuf *q);
ntcpip_Err ntcpip__etharpRequest(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *ipaddr);
/** For Ethernet network interfaces, we might want to send "gratuitous ARP";
 *  this is an ARP packet sent by a node in order to spontaneously cause other
 *  nodes to update an entry in their ARP cache.
 *  From RFC 3220 "IP Mobility Support for IPv4" section 4.6. */
#define etharp_gratuitous(netif) ntcpip__etharpRequest((netif), &(netif)->ip_addr)

ntcpip_Err ntcpip__etharpInput(struct ntcpip_pbuf *p, struct ntcpip__netif *netif);

#if NTCPIP__LWIP_AUTOIP
ntcpip_Err ntcpip__etharpRaw(struct ntcpip__netif *netif, const struct eth_addr *ethsrc_addr,
                 const struct eth_addr *ethdst_addr,
                 const struct eth_addr *hwsrc_addr, const struct ntcpip_ipAddr *ipsrc_addr,
                 const struct eth_addr *hwdst_addr, const struct ntcpip_ipAddr *ipdst_addr,
                 const Uint16 opcode);
#endif /* NTCPIP__LWIP_AUTOIP */

#define eth_addr_cmp(addr1, addr2) (memcmp((addr1)->addr, (addr2)->addr, ETHARP_HWADDR_LEN) == 0)

extern const struct eth_addr ntcpip__etharpBroadcast, ntcpip__etharpZero;

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_ARP */

#endif /* __NETIF_ARP_H__ */

