/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		ntcpip_sysArch.c
*
*	\ingroup	NTCPIP
*
*	\brief		LwIP and T-Plat.E glue code.
*
*	\details	All OS calls made from LwIP code is implemented here. The file
*				acts as a wrapper for T-Plat.E OSA.
*
*	\note
*
*	\version	\$Rev: 600 $ \n
*				\$Date: 2016-06-10 15:24:11 +0300 (pe, 10 kes� 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "mem.h"
#include "tstamp.h"
#include "deb.h"
#include "ntcpip/sys.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#ifndef NTCPIP__SYS_TIMEOUTS
# define NTCPIP__SYS_TIMEOUTS	15
#endif

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Thread info type used by LwIP threads.
 */

struct ntcpip__sysThread {				/*''''''''''''''''''''''''''''''''''*/
	osa_TaskType 			task;		/**< OSA task                       */
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Task timeout information.
 */

typedef struct {
	osa_TaskType *			pTask;
	struct ntcpip__sysTimeouts timeouts;
} ntcpip__SysTmOutElem;

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE ntcpip__SysProt 	ntcpip__sysProtect;

/* TODO:	Allocate this during initialization. */
PRIVATE ntcpip__SysTmOutElem ntcpip__sysTimeoutList[NTCPIP__SYS_TIMEOUTS];

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Init the OS emulation layer.
*
*	\details	Initializes the allocated lists.
*
*	\note		Will be called with interrupts disabled before OSA is running.
*
*******************************************************************************/

PUBLIC void ntcpip__sysInit(
	void
) {
	Ufast8 ii;

	/*
	 *	Timeouts.
	 */

	for (ii = 0; ii < NTCPIP__SYS_TIMEOUTS; ii++) {
		ntcpip__sysTimeoutList[ii].pTask = NULL;
	}

	/*
	 * Setup protection state
	 */

	ntcpip__sysProtect = 0;			/* start with no protection */

	ntcpip__sysMboxInit();
	ntcpip__sysSemaInit();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysThreadNew
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create a new thread.
*
*	\param		name 		Thread name
*	\param		thread 		The thread main function
*	\param		pArg		Argument that will be passed to the thread.
*	\param		stacksize 	The size of the stack that will be used by the
*							thread.
*	\param		prio 		Task priority.
*
*	\details	Starts a new thread named "name" with priority "prio" that will
*				begin its execution in the function "thread()". The "arg"
*				argument will be passed as an argument to the thread() function.
*				The stack size to used for this thread is the "stacksize"
*				parameter. The id of the new thread is returned. Both the id and
*				the priority are system dependent.
*
*	\note		Will be called with interrupts disabled before OSA is running.
*
*******************************************************************************/

PUBLIC ntcpip__SysThread ntcpip__sysThreadNew(
	char *					name,
	void (* 				thread)(void * arg),
	void *					pArg,
	int 					stacksize,
	int 					prio
) {
	ntcpip__SysThread pThread;

	/*
	 * Allocating the task and initializing it.
	 */

	pThread = (ntcpip__SysThread) mem_reserve(
		&mem_normal,
		sizeof(struct ntcpip__sysThread)
	);

	deb_assert(pThread != NULL);

//	pThread->pNext = NULL;
//	pThread->timeouts.next = NULL;
//	pThread->pArg = pArg;

	/*
	 * Using OSA to make a new task.
	 */

	osa_newTask(
		&pThread->task,
		name,
		(Uint8) prio,
		(void (OSA_TASK *)(void)) thread,	/* casting the function to fit */
		(Uint16) stacksize
	);

	pThread->task.pUtil = pThread;

	return(pThread);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysArchTimeouts
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get pointer to thread timeouts structure.
*
*	\return		Pointer to a thread timeout structure.
*
*	\details	Returns a pointer to the per-thread sys_timeouts structure. In
*				lwIP, each thread has a list of timeouts which is represented as
*				a linked list of ntcpip__sysTimeout structures. The sys_timeouts
*				structure holds a pointer to a linked list of timeouts. This
*				function is called by the lwIP timeout scheduler and must not
*				return a NULL value.
*
*				In a single thread sys_arch implementation, this function will
*				simply return a pointer to a global sys_timeouts variable stored
*				in the sys_arch module.
*
*	\note
*
*******************************************************************************/

PUBLIC struct ntcpip__sysTimeouts * ntcpip__sysArchTimeouts(
	void
) {
	osa_TaskType * 					pTask;
	struct ntcpip__sysTimeouts * 	pRet;
	ntcpip__SysTmOutElem *			pTmOut;

	pTask = osa_taskCurrent();

	/*
	 *	Search for the timeout structure that belongs to the current task.
	 *
	 *	Timeout structures are not included in the lwIP thread structure
	 *	because that would mean that all tasks that call lwip functions would
	 *	have to create the task by using the lwip new task function.
	 *
	 *	By having the timeout structures separately like this it does not matter
	 *	which task creation function was used when creating the task. A timeout
	 *	structure can be attached to any task here.
	 */

	pTmOut = ntcpip__sysTimeoutList;
	while (pTask != pTmOut->pTask) {
		if (pTmOut->pTask == NULL) {
			/*
			 *	End of list. The current task does not have a timeout structure
			 *	yet. Use this timeout structure for the current task.
			 */

			pTmOut->pTask = pTask;
			break;
		}

		/*
		 *	Move to the next timeout structure to check if that one belongs to
		 *	the current task.
		 */

		pTmOut++;
	}

	pRet = &pTmOut->timeouts;

	deb_assert(pRet != NULL);

	return(pRet);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip_sysGetStatus
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Provides the current pool status.
*
*	\param		pStatus		Pointer to structure that will be filled by the
*							function.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void ntcpip_sysGetStatus(
	ntcpip_SysStatus * 		pStatus
) {
	ntcpip__sysGetSemaStatus(pStatus);
	ntcpip__sysGetMboxStatus(pStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysArchProtect
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform a "fast" protect.
*
*	\return		Previous protection state.
*
*	\details	This could be implemented by disabling interrupts for an
*				embedded system or by using a semaphore or mutex. The
*				implementation should allow calling ntcpip__sysArchProtect when
*				already protected. The old protection level is returned.
*
*	\note
*
*******************************************************************************/

PUBLIC ntcpip__SysProt ntcpip__sysArchProtect(
	void
) {
	ntcpip__SysProt ret;

	DISABLE;

	ret = ntcpip__sysProtect;
	ntcpip__sysProtect++;

	return(ret);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysArchUnprotect
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform a "fast" set of the protection to the supplied level.
*
*	\param		pval		The protection state to revert to.
*
*	\details	This could be implemented by setting the interrupt level to the
*				supplied level or by using a semaphore or mutex.
*
*	\note
*
*******************************************************************************/

PUBLIC void ntcpip__sysArchUnprotect(
	ntcpip__SysProt 			pval
) {
	ntcpip__sysProtect = pval;

	if (pval == 0) {
		ENABLE;
	}
}
