/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_MEM_H__
#define __LWIP_MEM_H__

#include "ntcpip/opt.h"
#include "../local.h"

#ifdef __cplusplus
extern "C" {
#endif

#if NTCPIP__MEM_LIBC_MALLOC

#include <stddef.h> /* for size_t */

typedef size_t mem_size_t;

/* aliases for C library malloc() */
#define ntcpip__memInit()
/* in case C library malloc() needs extra protection,
 * allow these defines to be overridden.
 */
#ifndef ntcpip__memFree
#define ntcpip__memFree free
#endif
#ifndef ntcpip__memMalloc
#define ntcpip__memMalloc malloc
#endif
#ifndef ntcpip__memCalloc
#define ntcpip__memCalloc calloc
#endif
#ifndef ntcpip__memRealloc
static void *ntcpip__memRealloc(void *mem, mem_size_t size)
{
  NTCPIP__LWIP_UNUSED_ARG(size);
  return mem;
}
#endif
#else /* NTCPIP__MEM_LIBC_MALLOC */

/* NTCPIP__MEM_SIZE would have to be aligned, but using 64000 here instead of
 * 65535 leaves some room for alignment...
 */
#if NTCPIP__MEM_SIZE > 64000l
typedef Uint32 mem_size_t;
#else
typedef Uint16 mem_size_t;
#endif /* NTCPIP__MEM_SIZE > 64000 */

#if NTCPIP__MEM_USE_POOLS
/** ntcpip__memInit is not used when using pools instead of a heap */
#define ntcpip__memInit()
/** ntcpip__memRealloc is not used when using pools instead of a heap:
    we can't free part of a pool element and don't want to copy the rest */
#define ntcpip__memRealloc(mem, size) (mem)
#else /* NTCPIP__MEM_USE_POOLS */

#if NTCPIP__USE_PLAT_MEM == 1
#define ntcpip__memInit()
#else

/* lwIP alternative malloc */
void  ntcpip__memInit(void);
void *ntcpip__memRealloc(void *mem, mem_size_t size);
#endif

#endif /* NTCPIP__MEM_USE_POOLS */

#if NTCPIP__USE_PLAT_MEM == 0
void *ntcpip__memMalloc(mem_size_t size);
void *ntcpip__memCalloc(mem_size_t count, mem_size_t size);
void  ntcpip__memFree(void *mem);
#endif

#endif /* NTCPIP__MEM_LIBC_MALLOC */

#ifndef LWIP_MEM_ALIGN_SIZE
#define LWIP_MEM_ALIGN_SIZE(size) (((size) + NTCPIP__MEM_ALIGNMENT - 1) & ~(NTCPIP__MEM_ALIGNMENT-1))
#endif

#ifndef LWIP_MEM_ALIGN
#define LWIP_MEM_ALIGN(addr) ((void *)(((ntcpip__MemPtr)(addr) + NTCPIP__MEM_ALIGNMENT - 1) & ~(ntcpip__MemPtr)(NTCPIP__MEM_ALIGNMENT-1)))
#endif

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_MEM_H__ */

