/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_INET_CHKSUM_H__
#define __LWIP_INET_CHKSUM_H__

#include "ntcpip/opt.h"

#include "ntcpip/pbuf.h"
#include "ntcpip/ip_addr.h"

#include "../local.h"

#ifdef __cplusplus
extern "C" {
#endif

Uint16 ntcpip__inetChksum(void *dataptr, Uint16 len);
Uint16 ntcpip__inetChksumPbuf(struct ntcpip_pbuf *p);
Uint16 ntcpip__inetChksumPseudo(struct ntcpip_pbuf *p,
       const struct ntcpip_ipAddr *src, const struct ntcpip_ipAddr *dest,
       Uint8 proto, Uint16 proto_len);
#if NTCPIP__LWIP_UDPLITE
Uint16 ntcpip__inetChksumPseudoPartial(struct ntcpip_pbuf *p,
       struct ntcpip_ipAddr *src, struct ntcpip_ipAddr *dest,
       Uint8 proto, Uint16 proto_len, Uint16 chksum_len);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_INET_H__ */


