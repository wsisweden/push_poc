/**
 * @file
 * SNMP input message processing (RFC1157).
 */

/*
 * Copyright (c) 2006 Axon Digital Design B.V., The Netherlands.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Christiaan Simons <christiaan.simons@axon.tv>
 */

 
#include "ntcpip/opt.h"

#if NTCPIP__LWIP_SNMP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/ip_addr.h"
#include "lwip/mem.h"
#include "lwip/udp.h"
#include "lwip/stats.h"
#include "ntcpip/snmp.h"
#include "ntcpip/snmp_asn1.h"
#include "lwip/snmp_msg.h"
#include "ntcpip/snmp_structs.h"

#include <string.h>

/* public (non-static) constants */
/** SNMP v1 == 0 */
const Int32 snmp_version = 0;
/** default SNMP community string */
const char snmp_publiccommunity[7] = "public";

/* statically allocated buffers for NTCPIP__SNMP_CONCURRENT_REQUESTS */
struct snmp_msg_pstat msg_input_list[NTCPIP__SNMP_CONCURRENT_REQUESTS];
/* UDP Protocol Control Block */
struct ntcpip__udpPcb *snmp1_pcb;

static void snmp_recv(void *arg, struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p, struct ntcpip_ipAddr *addr, Uint16 port);
static ntcpip_Err snmp_pdu_header_check(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 pdu_len, Uint16 *ofs_ret, struct snmp_msg_pstat *m_stat);
static ntcpip_Err snmp_pdu_dec_varbindlist(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 *ofs_ret, struct snmp_msg_pstat *m_stat);


/**
 * Starts SNMP Agent.
 * Allocates UDP pcb and binds it to NTCPIP_IP_ADDR_ANY port 161.
 */
void
ntcpip__snmpInit(void)
{
  struct snmp_msg_pstat *msg_ps;
  Uint8 i;

  snmp1_pcb = ntcpip__udpNew();
  if (snmp1_pcb != NULL)
  {
    ntcpip__udpRecv(snmp1_pcb, snmp_recv, (void *)SNMP_IN_PORT);
    ntcpip__udpBind(snmp1_pcb, NTCPIP_IP_ADDR_ANY, SNMP_IN_PORT);
  }
  msg_ps = &msg_input_list[0];
  for (i=0; i<NTCPIP__SNMP_CONCURRENT_REQUESTS; i++)
  {
    msg_ps->state = SNMP_MSG_EMPTY;
    msg_ps->error_index = 0;
    msg_ps->error_status = SNMP_ES_NOERROR;
    msg_ps++;
  }
  trap_msg.pcb = snmp1_pcb;
  /* The coldstart trap will only be output
     if our outgoing interface is up & configured  */
  ntcpip__snmpColdstartTrap();
}

static void
snmp_error_response(struct snmp_msg_pstat *msg_ps, Uint8 error)
{
  ntcpip__snmpVarbindLFree(&msg_ps->outvb);
  msg_ps->outvb = msg_ps->invb;
  msg_ps->invb.head = NULL;
  msg_ps->invb.tail = NULL;
  msg_ps->invb.count = 0;
  msg_ps->error_status = error;
  msg_ps->error_index = 1 + msg_ps->vb_idx;
  ntcpip__snmpSendResponse(msg_ps);
  ntcpip__snmpVarbindLFree(&msg_ps->outvb);
  msg_ps->state = SNMP_MSG_EMPTY;
}

static void
snmp_ok_response(struct snmp_msg_pstat *msg_ps)
{
  ntcpip_Err err_ret;

  err_ret = ntcpip__snmpSendResponse(msg_ps);
  if (err_ret == NTCPIP_ERR_MEM)
  {
    /* serious memory problem, can't return tooBig */
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("ntcpip_snmpMsgEvent = %"S32_F"\n",msg_ps->error_status));
  }
  /* free varbinds (if available) */
  ntcpip__snmpVarbindLFree(&msg_ps->invb);
  ntcpip__snmpVarbindLFree(&msg_ps->outvb);
  msg_ps->state = SNMP_MSG_EMPTY;
}

/**
 * Service an internal or external event for SNMP GET.
 *
 * @param request_id identifies requests from 0 to (NTCPIP__SNMP_CONCURRENT_REQUESTS-1)
 * @param msg_ps points to the assosicated message process state
 */
static void
snmp_msg_get_event(Uint8 request_id, struct snmp_msg_pstat *msg_ps)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_msg_get_event: msg_ps->state==%"U16_F"\n",(Uint16)msg_ps->state));

  if (msg_ps->state == SNMP_MSG_EXTERNAL_GET_OBJDEF)
  {
    struct mib_external_node *en;
    struct snmp_name_ptr np;

    /* get_object_def() answer*/
    en = msg_ps->ext_mib_node;
    np = msg_ps->ext_name_ptr;

    /* translate answer into a known lifeform */
    en->get_object_def_a(request_id, np.ident_len, np.ident, &msg_ps->ext_object_def);
    if (msg_ps->ext_object_def.instance != MIB_OBJECT_NONE)
    {
      msg_ps->state = SNMP_MSG_EXTERNAL_GET_VALUE;
      en->get_value_q(request_id, &msg_ps->ext_object_def);
    }
    else
    {
      en->get_object_def_pc(request_id, np.ident_len, np.ident);
      /* search failed, object id points to unknown object (nosuchname) */
      snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
    }
  }
  else if (msg_ps->state == SNMP_MSG_EXTERNAL_GET_VALUE)
  {
    struct mib_external_node *en;
    struct snmp_varbind *vb;

    /* get_value() answer */
    en = msg_ps->ext_mib_node;

    /* allocate output varbind */
    vb = (struct snmp_varbind *)ntcpip__memMalloc(sizeof(struct snmp_varbind));
    NTCPIP__LWIP_ASSERT("vb != NULL",vb != NULL);
    if (vb != NULL)
    {
      vb->next = NULL;
      vb->prev = NULL;

      /* move name from invb to outvb */
      vb->ident = msg_ps->vb_ptr->ident;
      vb->ident_len = msg_ps->vb_ptr->ident_len;
      /* ensure this memory is refereced once only */
      msg_ps->vb_ptr->ident = NULL;
      msg_ps->vb_ptr->ident_len = 0;

      vb->value_type = msg_ps->ext_object_def.asn_type;
      vb->value_len =  msg_ps->ext_object_def.v_len;
      if (vb->value_len > 0)
      {
        vb->value = ntcpip__memMalloc(vb->value_len);
        NTCPIP__LWIP_ASSERT("vb->value != NULL",vb->value != NULL);
        if (vb->value != NULL)
        {
          en->get_value_a(request_id, &msg_ps->ext_object_def, vb->value_len, vb->value);
          ntcpip__snmpVarbindTailAdd(&msg_ps->outvb, vb);
          /* search again (if vb_idx < msg_ps->invb.count) */
          msg_ps->state = SNMP_MSG_SEARCH_OBJ;
          msg_ps->vb_idx += 1;
        }
        else
        {
          en->get_value_pc(request_id, &msg_ps->ext_object_def);
          NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("ntcpip_snmpMsgEvent: no variable space\n"));
          msg_ps->vb_ptr->ident = vb->ident;
          msg_ps->vb_ptr->ident_len = vb->ident_len;
          ntcpip__memFree(vb);
          snmp_error_response(msg_ps,SNMP_ES_TOOBIG);
        }
      }
      else
      {
        /* vb->value_len == 0, empty value (e.g. empty string) */
        en->get_value_a(request_id, &msg_ps->ext_object_def, 0, NULL);
        vb->value = NULL;
        ntcpip__snmpVarbindTailAdd(&msg_ps->outvb, vb);
        /* search again (if vb_idx < msg_ps->invb.count) */
        msg_ps->state = SNMP_MSG_SEARCH_OBJ;
        msg_ps->vb_idx += 1;
      }
    }
    else
    {
      en->get_value_pc(request_id, &msg_ps->ext_object_def);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("ntcpip_snmpMsgEvent: no outvb space\n"));
      snmp_error_response(msg_ps,SNMP_ES_TOOBIG);
    }
  }

  while ((msg_ps->state == SNMP_MSG_SEARCH_OBJ) &&
         (msg_ps->vb_idx < msg_ps->invb.count))
  {
    struct mib_node *mn;
    struct snmp_name_ptr np;

    if (msg_ps->vb_idx == 0)
    {
      msg_ps->vb_ptr = msg_ps->invb.head;
    }
    else
    {
      msg_ps->vb_ptr = msg_ps->vb_ptr->next;
    }
    /** test object identifier for .iso.org.dod.internet prefix */
    if (ntcpip__snmpIsoPrefixTst(msg_ps->vb_ptr->ident_len,  msg_ps->vb_ptr->ident))
    {
      mn = ntcpip__snmpSearchTree((struct mib_node*)&internet, msg_ps->vb_ptr->ident_len - 4,
                             msg_ps->vb_ptr->ident + 4, &np);
      if (mn != NULL)
      {
        if (mn->node_type == MIB_NODE_EX)
        {
          /* external object */
          struct mib_external_node *en = (struct mib_external_node*)mn;

          msg_ps->state = SNMP_MSG_EXTERNAL_GET_OBJDEF;
          /* save en && args in msg_ps!! */
          msg_ps->ext_mib_node = en;
          msg_ps->ext_name_ptr = np;

          en->get_object_def_q(en->addr_inf, request_id, np.ident_len, np.ident);
        }
        else
        {
          /* internal object */
          struct obj_def object_def;

          msg_ps->state = SNMP_MSG_INTERNAL_GET_OBJDEF;
          mn->get_object_def(np.ident_len, np.ident, &object_def);
          if (object_def.instance != MIB_OBJECT_NONE)
          {
            mn = mn;
          }
          else
          {
            /* search failed, object id points to unknown object (nosuchname) */
            mn =  NULL;
          }
          if (mn != NULL)
          {
            struct snmp_varbind *vb;

            msg_ps->state = SNMP_MSG_INTERNAL_GET_VALUE;
            /* allocate output varbind */
            vb = (struct snmp_varbind *)ntcpip__memMalloc(sizeof(struct snmp_varbind));
            NTCPIP__LWIP_ASSERT("vb != NULL",vb != NULL);
            if (vb != NULL)
            {
              vb->next = NULL;
              vb->prev = NULL;

              /* move name from invb to outvb */
              vb->ident = msg_ps->vb_ptr->ident;
              vb->ident_len = msg_ps->vb_ptr->ident_len;
              /* ensure this memory is refereced once only */
              msg_ps->vb_ptr->ident = NULL;
              msg_ps->vb_ptr->ident_len = 0;

              vb->value_type = object_def.asn_type;
              vb->value_len = object_def.v_len;
              if (vb->value_len > 0)
              {
                vb->value = ntcpip__memMalloc(vb->value_len);
                NTCPIP__LWIP_ASSERT("vb->value != NULL",vb->value != NULL);
                if (vb->value != NULL)
                {
                  mn->get_value(&object_def, vb->value_len, vb->value);
                  ntcpip__snmpVarbindTailAdd(&msg_ps->outvb, vb);
                  msg_ps->state = SNMP_MSG_SEARCH_OBJ;
                  msg_ps->vb_idx += 1;
                }
                else
                {
                  NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("ntcpip_snmpMsgEvent: couldn't allocate variable space\n"));
                  msg_ps->vb_ptr->ident = vb->ident;
                  msg_ps->vb_ptr->ident_len = vb->ident_len;
                  ntcpip__memFree(vb);
                  snmp_error_response(msg_ps,SNMP_ES_TOOBIG);
                }
              }
              else
              {
                /* vb->value_len == 0, empty value (e.g. empty string) */
                vb->value = NULL;
                ntcpip__snmpVarbindTailAdd(&msg_ps->outvb, vb);
                msg_ps->state = SNMP_MSG_SEARCH_OBJ;
                msg_ps->vb_idx += 1;
              }
            }
            else
            {
              NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("ntcpip_snmpMsgEvent: couldn't allocate outvb space\n"));
              snmp_error_response(msg_ps,SNMP_ES_TOOBIG);
            }
          }
        }
      }
    }
    else
    {
      mn = NULL;
    }
    if (mn == NULL)
    {
      /* mn == NULL, noSuchName */
      snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
    }
  }
  if ((msg_ps->state == SNMP_MSG_SEARCH_OBJ) &&
      (msg_ps->vb_idx == msg_ps->invb.count))
  {
    snmp_ok_response(msg_ps);
  }
}

/**
 * Service an internal or external event for SNMP GETNEXT.
 *
 * @param request_id identifies requests from 0 to (NTCPIP__SNMP_CONCURRENT_REQUESTS-1)
 * @param msg_ps points to the assosicated message process state
 */
static void
snmp_msg_getnext_event(Uint8 request_id, struct snmp_msg_pstat *msg_ps)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_msg_getnext_event: msg_ps->state==%"U16_F"\n",(Uint16)msg_ps->state));

  if (msg_ps->state == SNMP_MSG_EXTERNAL_GET_OBJDEF)
  {
    struct mib_external_node *en;

    /* get_object_def() answer*/
    en = msg_ps->ext_mib_node;

    /* translate answer into a known lifeform */
    en->get_object_def_a(request_id, 1, &msg_ps->ext_oid.id[msg_ps->ext_oid.len - 1], &msg_ps->ext_object_def);
    if (msg_ps->ext_object_def.instance != MIB_OBJECT_NONE)
    {
      msg_ps->state = SNMP_MSG_EXTERNAL_GET_VALUE;
      en->get_value_q(request_id, &msg_ps->ext_object_def);
    }
    else
    {
      en->get_object_def_pc(request_id, 1, &msg_ps->ext_oid.id[msg_ps->ext_oid.len - 1]);
      /* search failed, object id points to unknown object (nosuchname) */
      snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
    }
  }
  else if (msg_ps->state == SNMP_MSG_EXTERNAL_GET_VALUE)
  {
    struct mib_external_node *en;
    struct snmp_varbind *vb;

    /* get_value() answer */
    en = msg_ps->ext_mib_node;

    vb = ntcpip__snmpVarBindAlloc(&msg_ps->ext_oid,
                            msg_ps->ext_object_def.asn_type,
                            msg_ps->ext_object_def.v_len);
    if (vb != NULL)
    {
      en->get_value_a(request_id, &msg_ps->ext_object_def, vb->value_len, vb->value);
      ntcpip__snmpVarbindTailAdd(&msg_ps->outvb, vb);
      msg_ps->state = SNMP_MSG_SEARCH_OBJ;
      msg_ps->vb_idx += 1;
    }
    else
    {
      en->get_value_pc(request_id, &msg_ps->ext_object_def);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_msg_getnext_event: couldn't allocate outvb space\n"));
      snmp_error_response(msg_ps,SNMP_ES_TOOBIG);
    }
  }

  while ((msg_ps->state == SNMP_MSG_SEARCH_OBJ) &&
         (msg_ps->vb_idx < msg_ps->invb.count))
  {
    struct mib_node *mn;
    struct snmp_obj_id oid;

    if (msg_ps->vb_idx == 0)
    {
      msg_ps->vb_ptr = msg_ps->invb.head;
    }
    else
    {
      msg_ps->vb_ptr = msg_ps->vb_ptr->next;
    }
    if (ntcpip__snmpIsoPrefixExpand(msg_ps->vb_ptr->ident_len, msg_ps->vb_ptr->ident, &oid))
    {
      if (msg_ps->vb_ptr->ident_len > 3)
      {
        /* can offset ident_len and ident */
        mn = ntcpip__snmpExpandTree((struct mib_node*)&internet,
                              msg_ps->vb_ptr->ident_len - 4,
                              msg_ps->vb_ptr->ident + 4, &oid);
      }
      else
      {
        /* can't offset ident_len -4, ident + 4 */
        mn = ntcpip__snmpExpandTree((struct mib_node*)&internet, 0, NULL, &oid);
      }
    }
    else
    {
      mn = NULL;
    }
    if (mn != NULL)
    {
      if (mn->node_type == MIB_NODE_EX)
      {
        /* external object */
        struct mib_external_node *en = (struct mib_external_node*)mn;

        msg_ps->state = SNMP_MSG_EXTERNAL_GET_OBJDEF;
        /* save en && args in msg_ps!! */
        msg_ps->ext_mib_node = en;
        msg_ps->ext_oid = oid;

        en->get_object_def_q(en->addr_inf, request_id, 1, &oid.id[oid.len - 1]);
      }
      else
      {
        /* internal object */
        struct obj_def object_def;
        struct snmp_varbind *vb;

        msg_ps->state = SNMP_MSG_INTERNAL_GET_OBJDEF;
        mn->get_object_def(1, &oid.id[oid.len - 1], &object_def);

        vb = ntcpip__snmpVarBindAlloc(&oid, object_def.asn_type, object_def.v_len);
        if (vb != NULL)
        {
          msg_ps->state = SNMP_MSG_INTERNAL_GET_VALUE;
          mn->get_value(&object_def, object_def.v_len, vb->value);
          ntcpip__snmpVarbindTailAdd(&msg_ps->outvb, vb);
          msg_ps->state = SNMP_MSG_SEARCH_OBJ;
          msg_ps->vb_idx += 1;
        }
        else
        {
          NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_recv couldn't allocate outvb space\n"));
          snmp_error_response(msg_ps,SNMP_ES_TOOBIG);
        }
      }
    }
    if (mn == NULL)
    {
      /* mn == NULL, noSuchName */
      snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
    }
  }
  if ((msg_ps->state == SNMP_MSG_SEARCH_OBJ) &&
      (msg_ps->vb_idx == msg_ps->invb.count))
  {
    snmp_ok_response(msg_ps);
  }
}

/**
 * Service an internal or external event for SNMP SET.
 *
 * @param request_id identifies requests from 0 to (NTCPIP__SNMP_CONCURRENT_REQUESTS-1)
 * @param msg_ps points to the assosicated message process state
 */
static void
snmp_msg_set_event(Uint8 request_id, struct snmp_msg_pstat *msg_ps)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_msg_set_event: msg_ps->state==%"U16_F"\n",(Uint16)msg_ps->state));

  if (msg_ps->state == SNMP_MSG_EXTERNAL_GET_OBJDEF)
  {
    struct mib_external_node *en;
    struct snmp_name_ptr np;

    /* get_object_def() answer*/
    en = msg_ps->ext_mib_node;
    np = msg_ps->ext_name_ptr;

    /* translate answer into a known lifeform */
    en->get_object_def_a(request_id, np.ident_len, np.ident, &msg_ps->ext_object_def);
    if (msg_ps->ext_object_def.instance != MIB_OBJECT_NONE)
    {
      msg_ps->state = SNMP_MSG_EXTERNAL_SET_TEST;
      en->set_test_q(request_id, &msg_ps->ext_object_def);
    }
    else
    {
      en->get_object_def_pc(request_id, np.ident_len, np.ident);
      /* search failed, object id points to unknown object (nosuchname) */
      snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
    }
  }
  else if (msg_ps->state == SNMP_MSG_EXTERNAL_SET_TEST)
  {
    struct mib_external_node *en;

    /* set_test() answer*/
    en = msg_ps->ext_mib_node;

    if (msg_ps->ext_object_def.access == MIB_OBJECT_READ_WRITE)
    {
       if ((msg_ps->ext_object_def.asn_type == msg_ps->vb_ptr->value_type) &&
           (en->set_test_a(request_id,&msg_ps->ext_object_def,
                           msg_ps->vb_ptr->value_len,msg_ps->vb_ptr->value) != 0))
      {
        msg_ps->state = SNMP_MSG_SEARCH_OBJ;
        msg_ps->vb_idx += 1;
      }
      else
      {
        en->set_test_pc(request_id,&msg_ps->ext_object_def);
        /* bad value */
        snmp_error_response(msg_ps,SNMP_ES_BADVALUE);
      }
    }
    else
    {
      en->set_test_pc(request_id,&msg_ps->ext_object_def);
      /* object not available for set */
      snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
    }
  }
  else if (msg_ps->state == SNMP_MSG_EXTERNAL_GET_OBJDEF_S)
  {
    struct mib_external_node *en;
    struct snmp_name_ptr np;

    /* get_object_def() answer*/
    en = msg_ps->ext_mib_node;
    np = msg_ps->ext_name_ptr;

    /* translate answer into a known lifeform */
    en->get_object_def_a(request_id, np.ident_len, np.ident, &msg_ps->ext_object_def);
    if (msg_ps->ext_object_def.instance != MIB_OBJECT_NONE)
    {
      msg_ps->state = SNMP_MSG_EXTERNAL_SET_VALUE;
      en->set_value_q(request_id, &msg_ps->ext_object_def,
                      msg_ps->vb_ptr->value_len,msg_ps->vb_ptr->value);
    }
    else
    {
      en->get_object_def_pc(request_id, np.ident_len, np.ident);
      /* set_value failed, object has disappeared for some odd reason?? */
      snmp_error_response(msg_ps,SNMP_ES_GENERROR);
    }
  }
  else if (msg_ps->state == SNMP_MSG_EXTERNAL_SET_VALUE)
  {
    struct mib_external_node *en;

    /** set_value_a() */
    en = msg_ps->ext_mib_node;
    en->set_value_a(request_id, &msg_ps->ext_object_def,
      msg_ps->vb_ptr->value_len, msg_ps->vb_ptr->value);

    /** @todo use set_value_pc() if toobig */
    msg_ps->state = SNMP_MSG_INTERNAL_SET_VALUE;
    msg_ps->vb_idx += 1;
  }

  /* test all values before setting */
  while ((msg_ps->state == SNMP_MSG_SEARCH_OBJ) &&
         (msg_ps->vb_idx < msg_ps->invb.count))
  {
    struct mib_node *mn;
    struct snmp_name_ptr np;

    if (msg_ps->vb_idx == 0)
    {
      msg_ps->vb_ptr = msg_ps->invb.head;
    }
    else
    {
      msg_ps->vb_ptr = msg_ps->vb_ptr->next;
    }
    /** test object identifier for .iso.org.dod.internet prefix */
    if (ntcpip__snmpIsoPrefixTst(msg_ps->vb_ptr->ident_len,  msg_ps->vb_ptr->ident))
    {
      mn = ntcpip__snmpSearchTree((struct mib_node*)&internet, msg_ps->vb_ptr->ident_len - 4,
                             msg_ps->vb_ptr->ident + 4, &np);
      if (mn != NULL)
      {
        if (mn->node_type == MIB_NODE_EX)
        {
          /* external object */
          struct mib_external_node *en = (struct mib_external_node*)mn;

          msg_ps->state = SNMP_MSG_EXTERNAL_GET_OBJDEF;
          /* save en && args in msg_ps!! */
          msg_ps->ext_mib_node = en;
          msg_ps->ext_name_ptr = np;

          en->get_object_def_q(en->addr_inf, request_id, np.ident_len, np.ident);
        }
        else
        {
          /* internal object */
          struct obj_def object_def;

          msg_ps->state = SNMP_MSG_INTERNAL_GET_OBJDEF;
          mn->get_object_def(np.ident_len, np.ident, &object_def);
          if (object_def.instance != MIB_OBJECT_NONE)
          {
            mn = mn;
          }
          else
          {
            /* search failed, object id points to unknown object (nosuchname) */
            mn = NULL;
          }
          if (mn != NULL)
          {
            msg_ps->state = SNMP_MSG_INTERNAL_SET_TEST;

            if (object_def.access == MIB_OBJECT_READ_WRITE)
            {
              if ((object_def.asn_type == msg_ps->vb_ptr->value_type) &&
                  (mn->set_test(&object_def,msg_ps->vb_ptr->value_len,msg_ps->vb_ptr->value) != 0))
              {
                msg_ps->state = SNMP_MSG_SEARCH_OBJ;
                msg_ps->vb_idx += 1;
              }
              else
              {
                /* bad value */
                snmp_error_response(msg_ps,SNMP_ES_BADVALUE);
              }
            }
            else
            {
              /* object not available for set */
              snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
            }
          }
        }
      }
    }
    else
    {
      mn = NULL;
    }
    if (mn == NULL)
    {
      /* mn == NULL, noSuchName */
      snmp_error_response(msg_ps,SNMP_ES_NOSUCHNAME);
    }
  }

  if ((msg_ps->state == SNMP_MSG_SEARCH_OBJ) &&
      (msg_ps->vb_idx == msg_ps->invb.count))
  {
    msg_ps->vb_idx = 0;
    msg_ps->state = SNMP_MSG_INTERNAL_SET_VALUE;
  }

  /* set all values "atomically" (be as "atomic" as possible) */
  while ((msg_ps->state == SNMP_MSG_INTERNAL_SET_VALUE) &&
         (msg_ps->vb_idx < msg_ps->invb.count))
  {
    struct mib_node *mn;
    struct snmp_name_ptr np;

    if (msg_ps->vb_idx == 0)
    {
      msg_ps->vb_ptr = msg_ps->invb.head;
    }
    else
    {
      msg_ps->vb_ptr = msg_ps->vb_ptr->next;
    }
    /* skip iso prefix test, was done previously while settesting() */
    mn = ntcpip__snmpSearchTree((struct mib_node*)&internet, msg_ps->vb_ptr->ident_len - 4,
                           msg_ps->vb_ptr->ident + 4, &np);
    /* check if object is still available
       (e.g. external hot-plug thingy present?) */
    if (mn != NULL)
    {
      if (mn->node_type == MIB_NODE_EX)
      {
        /* external object */
        struct mib_external_node *en = (struct mib_external_node*)mn;

        msg_ps->state = SNMP_MSG_EXTERNAL_GET_OBJDEF_S;
        /* save en && args in msg_ps!! */
        msg_ps->ext_mib_node = en;
        msg_ps->ext_name_ptr = np;

        en->get_object_def_q(en->addr_inf, request_id, np.ident_len, np.ident);
      }
      else
      {
        /* internal object */
        struct obj_def object_def;

        msg_ps->state = SNMP_MSG_INTERNAL_GET_OBJDEF_S;
        mn->get_object_def(np.ident_len, np.ident, &object_def);
        msg_ps->state = SNMP_MSG_INTERNAL_SET_VALUE;
        mn->set_value(&object_def,msg_ps->vb_ptr->value_len,msg_ps->vb_ptr->value);
        msg_ps->vb_idx += 1;
      }
    }
  }
  if ((msg_ps->state == SNMP_MSG_INTERNAL_SET_VALUE) &&
      (msg_ps->vb_idx == msg_ps->invb.count))
  {
    /* simply echo the input if we can set it
       @todo do we need to return the actual value?
       e.g. if value is silently modified or behaves sticky? */
    msg_ps->outvb = msg_ps->invb;
    msg_ps->invb.head = NULL;
    msg_ps->invb.tail = NULL;
    msg_ps->invb.count = 0;
    snmp_ok_response(msg_ps);
  }
}


/**
 * Handle one internal or external event.
 * Called for one async event. (recv external/private answer)
 *
 * @param request_id identifies requests from 0 to (NTCPIP__SNMP_CONCURRENT_REQUESTS-1)
 */
void
ntcpip_snmpMsgEvent(Uint8 request_id)
{
  struct snmp_msg_pstat *msg_ps;

  if (request_id < NTCPIP__SNMP_CONCURRENT_REQUESTS)
  {
    msg_ps = &msg_input_list[request_id];
    if (msg_ps->rt == SNMP_ASN1_PDU_GET_NEXT_REQ)
    {
      snmp_msg_getnext_event(request_id, msg_ps);
    }
    else if (msg_ps->rt == SNMP_ASN1_PDU_GET_REQ)
    {
      snmp_msg_get_event(request_id, msg_ps);
    }
    else if(msg_ps->rt == SNMP_ASN1_PDU_SET_REQ)
    {
      snmp_msg_set_event(request_id, msg_ps);
    }
  }
}


/* lwIP UDP receive callback function */
static void
snmp_recv(void *arg, struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p, struct ntcpip_ipAddr *addr, Uint16 port)
{
  struct udp_hdr *udphdr;

  /* suppress unused argument warning */
  NTCPIP__LWIP_UNUSED_ARG(arg);
  /* peek in the UDP header (goto IP payload) */
  if(ntcpip__pbufHeader(p, UDP_HLEN)){
    NTCPIP__LWIP_ASSERT("Can't move to UDP header", 0);
    ntcpip_pbufFree(p);
    return;
  }
  udphdr = p->payload;

  /* check if datagram is really directed at us (including broadcast requests) */
  if ((pcb == snmp1_pcb) && (ntcpip_ntohs(udphdr->dest) == SNMP_IN_PORT))
  {
    struct snmp_msg_pstat *msg_ps;
    Uint8 req_idx;

    /* traverse input message process list, look for SNMP_MSG_EMPTY */
    msg_ps = &msg_input_list[0];
    req_idx = 0;
    while ((req_idx<NTCPIP__SNMP_CONCURRENT_REQUESTS) && (msg_ps->state != SNMP_MSG_EMPTY))
    {
      req_idx++;
      msg_ps++;
    }
    if (req_idx != NTCPIP__SNMP_CONCURRENT_REQUESTS)
    {
      ntcpip_Err err_ret;
      Uint16 payload_len;
      Uint16 payload_ofs;
      Uint16 varbind_ofs = 0;

      /* accepting request */
      ntcpip__snmpIncSnmpinpkts();
      /* record used 'protocol control block' */
      msg_ps->pcb = pcb;
      /* source address (network order) */
      msg_ps->sip = *addr;
      /* source port (host order (lwIP oddity)) */
      msg_ps->sp = port;
      /* read UDP payload length from UDP header */
      payload_len = ntcpip_ntohs(udphdr->len) - UDP_HLEN;

      /* adjust to UDP payload */
      payload_ofs = UDP_HLEN;

      /* check total length, version, community, pdu type */
      err_ret = snmp_pdu_header_check(p, payload_ofs, payload_len, &varbind_ofs, msg_ps);
      if (((msg_ps->rt == SNMP_ASN1_PDU_GET_REQ) ||
           (msg_ps->rt == SNMP_ASN1_PDU_GET_NEXT_REQ) ||
           (msg_ps->rt == SNMP_ASN1_PDU_SET_REQ)) &&
          ((msg_ps->error_status == SNMP_ES_NOERROR) &&
           (msg_ps->error_index == 0)) )
      {
        /* Only accept requests and requests without error (be robust) */
        err_ret = err_ret;
      }
      else
      {
        /* Reject response and trap headers or error requests as input! */
        err_ret = NTCPIP_ERR_ARG;
      }
      if (err_ret == NTCPIP_ERR_OK)
      {
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_recv ok, community %s\n", msg_ps->community));

        /* Builds a list of variable bindings. Copy the varbinds from the pbuf
          chain to glue them when these are divided over two or more pbuf's. */
        err_ret = snmp_pdu_dec_varbindlist(p, varbind_ofs, &varbind_ofs, msg_ps);
        if ((err_ret == NTCPIP_ERR_OK) && (msg_ps->invb.count > 0))
        {
          /* we've decoded the incoming message, release input msg now */
          ntcpip_pbufFree(p);

          msg_ps->error_status = SNMP_ES_NOERROR;
          msg_ps->error_index = 0;
          /* find object for each variable binding */
          msg_ps->state = SNMP_MSG_SEARCH_OBJ;
          /* first variable binding from list to inspect */
          msg_ps->vb_idx = 0;

          NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_recv varbind cnt=%"U16_F"\n",(Uint16)msg_ps->invb.count));

          /* handle input event and as much objects as possible in one go */
          ntcpip_snmpMsgEvent(req_idx);
        }
        else
        {
          /* varbind-list decode failed, or varbind list empty.
             drop request silently, do not return error!
             (errors are only returned for a specific varbind failure) */
          ntcpip_pbufFree(p);
          NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_pdu_dec_varbindlist() failed\n"));
        }
      }
      else
      {
        /* header check failed
           drop request silently, do not return error! */
        ntcpip_pbufFree(p);
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_pdu_header_check() failed\n"));
      }
    }
    else
    {
      /* exceeding number of concurrent requests */
      ntcpip_pbufFree(p);
    }
  }
  else
  {
    /* datagram not for us */
    ntcpip_pbufFree(p);
  }
}

/**
 * Checks and decodes incoming SNMP message header, logs header errors.
 *
 * @param p points to pbuf chain of SNMP message (UDP payload)
 * @param ofs points to first octet of SNMP message
 * @param pdu_len the length of the UDP payload
 * @param ofs_ret returns the ofset of the variable bindings
 * @param m_stat points to the current message request state return
 * @return
 * - NTCPIP_ERR_OK SNMP header is sane and accepted
 * - NTCPIP_ERR_ARG SNMP header is either malformed or rejected
 */
static ntcpip_Err
snmp_pdu_header_check(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 pdu_len, Uint16 *ofs_ret, struct snmp_msg_pstat *m_stat)
{
  ntcpip_Err derr;
  Uint16 len, ofs_base;
  Uint8  len_octets;
  Uint8  type;
  Int32 version;

  ofs_base = ofs;
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
  if ((derr != NTCPIP_ERR_OK) ||
      (pdu_len != (1 + len_octets + len)) ||
      (type != (SNMP_ASN1_UNIV | SNMP_ASN1_CONSTR | SNMP_ASN1_SEQ)))
  {
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  ofs += (1 + len_octets);
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
  if ((derr != NTCPIP_ERR_OK) || (type != (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG)))
  {
    /* can't decode or no integer (version) */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  derr = ntcpip__snmpAsn1DecS32t(p, ofs + 1 + len_octets, len, &version);
  if (derr != NTCPIP_ERR_OK)
  {
    /* can't decode */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  if (version != 0)
  {
    /* not version 1 */
    ntcpip__snmpIncSnmpinbadversions();
    return NTCPIP_ERR_ARG;
  }
  ofs += (1 + len_octets + len);
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
  if ((derr != NTCPIP_ERR_OK) || (type != (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR)))
  {
    /* can't decode or no octet string (community) */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  derr = ntcpip__snmpAsn1DecRaw(p, ofs + 1 + len_octets, len, SNMP_COMMUNITY_STR_LEN, m_stat->community);
  if (derr != NTCPIP_ERR_OK)
  {
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  /* add zero terminator */
  len = ((len < (SNMP_COMMUNITY_STR_LEN))?(len):(SNMP_COMMUNITY_STR_LEN));
  m_stat->community[len] = 0;
  m_stat->com_strlen = len;
  if (strncmp(snmp_publiccommunity, (const char*)m_stat->community, SNMP_COMMUNITY_STR_LEN) != 0)
  {
    /** @todo: move this if we need to check more names */
    ntcpip__snmpIncSnmpinbadcommunitynames();
    ntcpip__snmpAuthfailTrap();
    return NTCPIP_ERR_ARG;
  }
  ofs += (1 + len_octets + len);
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
  if (derr != NTCPIP_ERR_OK)
  {
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  switch(type)
  {
    case (SNMP_ASN1_CONTXT | SNMP_ASN1_CONSTR | SNMP_ASN1_PDU_GET_REQ):
      /* GetRequest PDU */
      ntcpip__snmpIncSnmpingetrequests();
      derr = NTCPIP_ERR_OK;
      break;
    case (SNMP_ASN1_CONTXT | SNMP_ASN1_CONSTR | SNMP_ASN1_PDU_GET_NEXT_REQ):
      /* GetNextRequest PDU */
      ntcpip__snmpIncSnmpingetnexts();
      derr = NTCPIP_ERR_OK;
      break;
    case (SNMP_ASN1_CONTXT | SNMP_ASN1_CONSTR | SNMP_ASN1_PDU_GET_RESP):
      /* GetResponse PDU */
      ntcpip__snmpIncSnmpingetresponses();
      derr = NTCPIP_ERR_ARG;
      break;
    case (SNMP_ASN1_CONTXT | SNMP_ASN1_CONSTR | SNMP_ASN1_PDU_SET_REQ):
      /* SetRequest PDU */
      ntcpip__snmpIncSnmpinsetrequests();
      derr = NTCPIP_ERR_OK;
      break;
    case (SNMP_ASN1_CONTXT | SNMP_ASN1_CONSTR | SNMP_ASN1_PDU_TRAP):
      /* Trap PDU */
      ntcpip__snmpIncSnmpintraps();
      derr = NTCPIP_ERR_ARG;
      break;
    default:
      ntcpip__snmpIncSnmpinasnparseerrs();
      derr = NTCPIP_ERR_ARG;
      break;
  }
  if (derr != NTCPIP_ERR_OK)
  {
    /* unsupported input PDU for this agent (no parse error) */
    return NTCPIP_ERR_ARG;
  }
  m_stat->rt = type & 0x1F;
  ofs += (1 + len_octets);
  if (len != (pdu_len - (ofs - ofs_base)))
  {
    /* decoded PDU length does not equal actual payload length */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
  if ((derr != NTCPIP_ERR_OK) || (type != (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG)))
  {
    /* can't decode or no integer (request ID) */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  derr = ntcpip__snmpAsn1DecS32t(p, ofs + 1 + len_octets, len, &m_stat->rid);
  if (derr != NTCPIP_ERR_OK)
  {
    /* can't decode */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  ofs += (1 + len_octets + len);
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
  if ((derr != NTCPIP_ERR_OK) || (type != (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG)))
  {
    /* can't decode or no integer (error-status) */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  /* must be noError (0) for incoming requests.
     log errors for mib-2 completeness and for debug purposes */
  derr = ntcpip__snmpAsn1DecS32t(p, ofs + 1 + len_octets, len, &m_stat->error_status);
  if (derr != NTCPIP_ERR_OK)
  {
    /* can't decode */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  switch (m_stat->error_status)
  {
    case SNMP_ES_TOOBIG:
      ntcpip__snmpIncSnmpintoobigs();
      break;
    case SNMP_ES_NOSUCHNAME:
      ntcpip__snmpIncSnmpinnosuchnames();
      break;
    case SNMP_ES_BADVALUE:
      ntcpip__snmpIncSnmpinbadvalues();
      break;
    case SNMP_ES_READONLY:
      ntcpip__snmpIncSnmpinreadonlys();
      break;
    case SNMP_ES_GENERROR:
      ntcpip__snmpIncSnmpingenerrs();
      break;
  }
  ofs += (1 + len_octets + len);
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
  if ((derr != NTCPIP_ERR_OK) || (type != (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG)))
  {
    /* can't decode or no integer (error-index) */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  /* must be 0 for incoming requests.
     decode anyway to catch bad integers (and dirty tricks) */
  derr = ntcpip__snmpAsn1DecS32t(p, ofs + 1 + len_octets, len, &m_stat->error_index);
  if (derr != NTCPIP_ERR_OK)
  {
    /* can't decode */
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  ofs += (1 + len_octets + len);
  *ofs_ret = ofs;
  return NTCPIP_ERR_OK;
}

static ntcpip_Err
snmp_pdu_dec_varbindlist(struct ntcpip_pbuf *p, Uint16 ofs, Uint16 *ofs_ret, struct snmp_msg_pstat *m_stat)
{
  ntcpip_Err derr;
  Uint16 len, vb_len;
  Uint8  len_octets;
  Uint8 type;

  /* variable binding list */
  ntcpip__snmpAsn1DecType(p, ofs, &type);
  derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &vb_len);
  if ((derr != NTCPIP_ERR_OK) ||
      (type != (SNMP_ASN1_UNIV | SNMP_ASN1_CONSTR | SNMP_ASN1_SEQ)))
  {
    ntcpip__snmpIncSnmpinasnparseerrs();
    return NTCPIP_ERR_ARG;
  }
  ofs += (1 + len_octets);

  /* start with empty list */
  m_stat->invb.count = 0;
  m_stat->invb.head = NULL;
  m_stat->invb.tail = NULL;

  while (vb_len > 0)
  {
    struct snmp_obj_id oid, oid_value;
    struct snmp_varbind *vb;

    ntcpip__snmpAsn1DecType(p, ofs, &type);
    derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
    if ((derr != NTCPIP_ERR_OK) ||
        (type != (SNMP_ASN1_UNIV | SNMP_ASN1_CONSTR | SNMP_ASN1_SEQ)) ||
        (len == 0) || (len > vb_len))
    {
      ntcpip__snmpIncSnmpinasnparseerrs();
      /* free varbinds (if available) */
      ntcpip__snmpVarbindLFree(&m_stat->invb);
      return NTCPIP_ERR_ARG;
    }
    ofs += (1 + len_octets);
    vb_len -= (1 + len_octets);

    ntcpip__snmpAsn1DecType(p, ofs, &type);
    derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
    if ((derr != NTCPIP_ERR_OK) || (type != (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID)))
    {
      /* can't decode object name length */
      ntcpip__snmpIncSnmpinasnparseerrs();
      /* free varbinds (if available) */
      ntcpip__snmpVarbindLFree(&m_stat->invb);
      return NTCPIP_ERR_ARG;
    }
    derr = ntcpip__snmpAsn1DecOid(p, ofs + 1 + len_octets, len, &oid);
    if (derr != NTCPIP_ERR_OK)
    {
      /* can't decode object name */
      ntcpip__snmpIncSnmpinasnparseerrs();
      /* free varbinds (if available) */
      ntcpip__snmpVarbindLFree(&m_stat->invb);
      return NTCPIP_ERR_ARG;
    }
    ofs += (1 + len_octets + len);
    vb_len -= (1 + len_octets + len);

    ntcpip__snmpAsn1DecType(p, ofs, &type);
    derr = ntcpip__snmpAsn1DecLength(p, ofs+1, &len_octets, &len);
    if (derr != NTCPIP_ERR_OK)
    {
      /* can't decode object value length */
      ntcpip__snmpIncSnmpinasnparseerrs();
      /* free varbinds (if available) */
      ntcpip__snmpVarbindLFree(&m_stat->invb);
      return NTCPIP_ERR_ARG;
    }

    switch (type)
    {
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG):
        vb = ntcpip__snmpVarBindAlloc(&oid, type, sizeof(Int32));
        if (vb != NULL)
        {
          Int32 *vptr = vb->value;

          derr = ntcpip__snmpAsn1DecS32t(p, ofs + 1 + len_octets, len, vptr);
          ntcpip__snmpVarbindTailAdd(&m_stat->invb, vb);
        }
        else
        {
          derr = NTCPIP_ERR_ARG;
        }
        break;
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_GAUGE):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_TIMETICKS):
        vb = ntcpip__snmpVarBindAlloc(&oid, type, sizeof(Uint32));
        if (vb != NULL)
        {
          Uint32 *vptr = vb->value;

          derr = ntcpip__snmpAsn1DecU32t(p, ofs + 1 + len_octets, len, vptr);
          ntcpip__snmpVarbindTailAdd(&m_stat->invb, vb);
        }
        else
        {
          derr = NTCPIP_ERR_ARG;
        }
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_OPAQUE):
        vb = ntcpip__snmpVarBindAlloc(&oid, type, len);
        if (vb != NULL)
        {
          derr = ntcpip__snmpAsn1DecRaw(p, ofs + 1 + len_octets, len, vb->value_len, vb->value);
          ntcpip__snmpVarbindTailAdd(&m_stat->invb, vb);
        }
        else
        {
          derr = NTCPIP_ERR_ARG;
        }
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_NUL):
        vb = ntcpip__snmpVarBindAlloc(&oid, type, 0);
        if (vb != NULL)
        {
          ntcpip__snmpVarbindTailAdd(&m_stat->invb, vb);
          derr = NTCPIP_ERR_OK;
        }
        else
        {
          derr = NTCPIP_ERR_ARG;
        }
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID):
        derr = ntcpip__snmpAsn1DecOid(p, ofs + 1 + len_octets, len, &oid_value);
        if (derr == NTCPIP_ERR_OK)
        {
          vb = ntcpip__snmpVarBindAlloc(&oid, type, oid_value.len * sizeof(Int32));
          if (vb != NULL)
          {
            Uint8 i = oid_value.len;
            Int32 *vptr = vb->value;

            while(i > 0)
            {
              i--;
              vptr[i] = oid_value.id[i];
            }
            ntcpip__snmpVarbindTailAdd(&m_stat->invb, vb);
            derr = NTCPIP_ERR_OK;
          }
          else
          {
            derr = NTCPIP_ERR_ARG;
          }
        }
        break;
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR):
        if (len == 4)
        {
          /* must be exactly 4 octets! */
          vb = ntcpip__snmpVarBindAlloc(&oid, type, 4);
          if (vb != NULL)
          {
            derr = ntcpip__snmpAsn1DecRaw(p, ofs + 1 + len_octets, len, vb->value_len, vb->value);
            ntcpip__snmpVarbindTailAdd(&m_stat->invb, vb);
          }
          else
          {
            derr = NTCPIP_ERR_ARG;
          }
        }
        else
        {
          derr = NTCPIP_ERR_ARG;
        }
        break;
      default:
        derr = NTCPIP_ERR_ARG;
        break;
    }
    if (derr != NTCPIP_ERR_OK)
    {
      ntcpip__snmpIncSnmpinasnparseerrs();
      /* free varbinds (if available) */
      ntcpip__snmpVarbindLFree(&m_stat->invb);
      return NTCPIP_ERR_ARG;
    }
    ofs += (1 + len_octets + len);
    vb_len -= (1 + len_octets + len);
  }

  if (m_stat->rt == SNMP_ASN1_PDU_SET_REQ)
  {
    ntcpip__snmpAddSnmpintotalsetvars(m_stat->invb.count);
  }
  else
  {
    ntcpip__snmpAddSnmpintotalreqvars(m_stat->invb.count);
  }

  *ofs_ret = ofs;
  return NTCPIP_ERR_OK;
}

struct snmp_varbind*
ntcpip__snmpVarBindAlloc(struct snmp_obj_id *oid, Uint8 type, Uint8 len)
{
  struct snmp_varbind *vb;

  vb = (struct snmp_varbind *)ntcpip__memMalloc(sizeof(struct snmp_varbind));
  NTCPIP__LWIP_ASSERT("vb != NULL",vb != NULL);
  if (vb != NULL)
  {
    Uint8 i;

    vb->next = NULL;
    vb->prev = NULL;
    i = oid->len;
    vb->ident_len = i;
    if (i > 0)
    {
      /* allocate array of Int32 for our object identifier */
      vb->ident = (Int32*)ntcpip__memMalloc(sizeof(Int32) * i);
      NTCPIP__LWIP_ASSERT("vb->ident != NULL",vb->ident != NULL);
      if (vb->ident == NULL)
      {
        ntcpip__memFree(vb);
        return NULL;
      }
      while(i > 0)
      {
        i--;
        vb->ident[i] = oid->id[i];
      }
    }
    else
    {
      /* i == 0, pass zero length object identifier */
      vb->ident = NULL;
    }
    vb->value_type = type;
    vb->value_len = len;
    if (len > 0)
    {
      /* allocate raw bytes for our object value */
      vb->value = ntcpip__memMalloc(len);
      NTCPIP__LWIP_ASSERT("vb->value != NULL",vb->value != NULL);
      if (vb->value == NULL)
      {
        if (vb->ident != NULL)
        {
          ntcpip__memFree(vb->ident);
        }
        ntcpip__memFree(vb);
        return NULL;
      }
    }
    else
    {
      /* ASN1_NUL type, or zero length ASN1_OC_STR */
      vb->value = NULL;
    }
  }
  return vb;
}

void
ntcpip__snmpVarbindFree(struct snmp_varbind *vb)
{
  if (vb->value != NULL )
  {
    ntcpip__memFree(vb->value);
  }
  if (vb->ident != NULL )
  {
    ntcpip__memFree(vb->ident);
  }
  ntcpip__memFree(vb);
}

void
ntcpip__snmpVarbindLFree(struct snmp_varbind_root *root)
{
  struct snmp_varbind *vb, *prev;

  vb = root->tail;
  while ( vb != NULL )
  {
    prev = vb->prev;
    ntcpip__snmpVarbindFree(vb);
    vb = prev;
  }
  root->count = 0;
  root->head = NULL;
  root->tail = NULL;
}

void
ntcpip__snmpVarbindTailAdd(struct snmp_varbind_root *root, struct snmp_varbind *vb)
{
  if (root->count == 0)
  {
    /* add first varbind to list */
    root->head = vb;
    root->tail = vb;
  }
  else
  {
    /* add nth varbind to list tail */
    root->tail->next = vb;
    vb->prev = root->tail;
    root->tail = vb;
  }
  root->count += 1;
}

struct snmp_varbind*
ntcpip__snmpVarbindTailRem(struct snmp_varbind_root *root)
{
  struct snmp_varbind* vb;

  if (root->count > 0)
  {
    /* remove tail varbind */
    vb = root->tail;
    root->tail = vb->prev;
    vb->prev->next = NULL;
    root->count -= 1;
  }
  else
  {
    /* nothing to remove */
    vb = NULL;
  }
  return vb;
}

#endif /* NTCPIP__LWIP_SNMP */


