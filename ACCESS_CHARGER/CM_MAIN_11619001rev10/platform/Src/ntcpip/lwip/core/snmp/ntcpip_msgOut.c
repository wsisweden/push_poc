/**
 * @file
 * SNMP output message processing (RFC1157).
 *
 * Output responses and traps are build in two passes:
 *
 * Pass 0: iterate over the output message backwards to determine encoding lengths
 * Pass 1: the actual forward encoding of internal form into ASN1
 *
 * The single-pass encoding method described by Comer & Stevens
 * requires extra buffer space and copying for reversal of the packet.
 * The buffer requirement can be prohibitively large for big payloads
 * (>= 484) therefore we use the two encoding passes.
 */

 
/*
 * Copyright (c) 2006 Axon Digital Design B.V., The Netherlands.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Christiaan Simons <christiaan.simons@axon.tv>
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_SNMP /* don't build if not configured for use in lwipopts.h */

#include "lwip/udp.h"
#include "ntcpip/netif.h"
#include "ntcpip/snmp.h"
#include "ntcpip/snmp_asn1.h"
#include "lwip/snmp_msg.h"

struct snmp_trap_dst
{
  /* destination IP address in network order */
  struct ntcpip_ipAddr dip;
  /* set to 0 when disabled, >0 when enabled */
  Uint8 enable;
};
struct snmp_trap_dst trap_dst[NTCPIP__SNMP_TRAP_DESTINATIONS];

/** TRAP message structure */
struct snmp_msg_trap trap_msg;

static Uint16 snmp_resp_header_sum(struct snmp_msg_pstat *m_stat, Uint16 vb_len);
static Uint16 snmp_trap_header_sum(struct snmp_msg_trap *m_trap, Uint16 vb_len);
static Uint16 snmp_varbind_list_sum(struct snmp_varbind_root *root);

static Uint16 snmp_resp_header_enc(struct snmp_msg_pstat *m_stat, struct ntcpip_pbuf *p);
static Uint16 snmp_trap_header_enc(struct snmp_msg_trap *m_trap, struct ntcpip_pbuf *p);
static Uint16 snmp_varbind_list_enc(struct snmp_varbind_root *root, struct ntcpip_pbuf *p, Uint16 ofs);

/**
 * Sets enable switch for this trap destination.
 * @param dst_idx index in 0 .. NTCPIP__SNMP_TRAP_DESTINATIONS-1
 * @param enable switch if 0 destination is disabled >0 enabled.
 */
void
ntcpip__snmpTrapDstEnable(Uint8 dst_idx, Uint8 enable)
{
  if (dst_idx < NTCPIP__SNMP_TRAP_DESTINATIONS)
  {
    trap_dst[dst_idx].enable = enable;
  }
}

/**
 * Sets IPv4 address for this trap destination.
 * @param dst_idx index in 0 .. NTCPIP__SNMP_TRAP_DESTINATIONS-1
 * @param dst IPv4 address in host order.
 */
void
ntcpip__snmpTrapDstIpSet(Uint8 dst_idx, struct ntcpip_ipAddr *dst)
{
  if (dst_idx < NTCPIP__SNMP_TRAP_DESTINATIONS)
  {
    trap_dst[dst_idx].dip.addr = ntcpip_htonl(dst->addr);
  }
}

/**
 * Sends a 'getresponse' message to the request originator.
 *
 * @param m_stat points to the current message request state source
 * @return NTCPIP_ERR_OK when success, NTCPIP_ERR_MEM if we're out of memory
 *
 * @note the caller is responsible for filling in outvb in the m_stat
 * and provide error-status and index (except for tooBig errors) ...
 */
ntcpip_Err
ntcpip__snmpSendResponse(struct snmp_msg_pstat *m_stat)
{
  struct snmp_varbind_root emptyvb = {NULL, NULL, 0, 0, 0};
  struct ntcpip_pbuf *p;
  Uint16 tot_len;
  ntcpip_Err err;

  /* pass 0, calculate length fields */
  tot_len = snmp_varbind_list_sum(&m_stat->outvb);
  tot_len = snmp_resp_header_sum(m_stat, tot_len);

  /* try allocating pbuf(s) for complete response */
  p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, tot_len, NTCPIP_PBUF_POOL);
  if (p == NULL)
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_snd_response() tooBig\n"));

    /* can't construct reply, return error-status tooBig */
    m_stat->error_status = SNMP_ES_TOOBIG;
    m_stat->error_index = 0;
    /* pass 0, recalculate lengths, for empty varbind-list */
    tot_len = snmp_varbind_list_sum(&emptyvb);
    tot_len = snmp_resp_header_sum(m_stat, tot_len);
    /* retry allocation once for header and empty varbind-list */
    p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, tot_len, NTCPIP_PBUF_POOL);
  }
  if (p != NULL)
  {
    /* first pbuf alloc try or retry alloc success */
    Uint16 ofs;

    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_snd_response() p != NULL\n"));

    /* pass 1, size error, encode packet ino the pbuf(s) */
    ofs = snmp_resp_header_enc(m_stat, p);
    if (m_stat->error_status == SNMP_ES_TOOBIG)
    {
      snmp_varbind_list_enc(&emptyvb, p, ofs);
    }
    else
    {
      snmp_varbind_list_enc(&m_stat->outvb, p, ofs);
    }

    switch (m_stat->error_status)
    {
      case SNMP_ES_TOOBIG:
        ntcpip__snmpIncSnmpouttoobigs();
        break;
      case SNMP_ES_NOSUCHNAME:
        ntcpip__snmpIncSnmpoutnosuchnames();
        break;
      case SNMP_ES_BADVALUE:
        ntcpip__snmpIncSnmpoutbadvalues();
        break;
      case SNMP_ES_GENERROR:
        ntcpip__snmpIncSnmpoutgenerrs();
        break;
    }
    ntcpip__snmpIncSnmpoutgetresponses();
    ntcpip__snmpIncSnmpoutpkts();

    /** @todo do we need separate rx and tx pcbs for threaded case? */
    /** connect to the originating source */
    ntcpip__udpConnect(m_stat->pcb, &m_stat->sip, m_stat->sp);
    err = ntcpip__udpSend(m_stat->pcb, p);
    if (err == NTCPIP_ERR_MEM)
    {
      /** @todo release some memory, retry and return tooBig? tooMuchHassle? */
      err = NTCPIP_ERR_MEM;
    }
    else
    {
      err = NTCPIP_ERR_OK;
    }
    /** disassociate remote address and port with this pcb */
    ntcpip__udpDisconnect(m_stat->pcb);

    ntcpip_pbufFree(p);
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MSG_DEBUG, ("snmp_snd_response() done\n"));
    return err;
  }
  else
  {
    /* first pbuf alloc try or retry alloc failed
       very low on memory, couldn't return tooBig */
    return NTCPIP_ERR_MEM;
  }
}


/**
 * Sends an generic or enterprise specific trap message.
 *
 * @param generic_trap is the trap code
 * @param eoid points to enterprise object identifier
 * @param specific_trap used for enterprise traps when generic_trap == 6
 * @return NTCPIP_ERR_OK when success, NTCPIP_ERR_MEM if we're out of memory
 *
 * @note the caller is responsible for filling in outvb in the trap_msg
 * @note the use of the enterpise identifier field
 * is per RFC1215.
 * Use .iso.org.dod.internet.mgmt.mib-2.snmp for generic traps
 * and .iso.org.dod.internet.private.enterprises.yourenterprise
 * (sysObjectID) for specific traps.
 */
ntcpip_Err
ntcpip__snmpSendTrap(Int8 generic_trap, struct snmp_obj_id *eoid, Int32 specific_trap)
{
  struct snmp_trap_dst *td;
  struct ntcpip__netif *dst_if;
  struct ntcpip_ipAddr dst_ip;
  struct ntcpip_pbuf *p;
  Uint16 i,tot_len;

  for (i=0, td = &trap_dst[0]; i<NTCPIP__SNMP_TRAP_DESTINATIONS; i++, td++)
  {
    if ((td->enable != 0) && (td->dip.addr != 0))
    {
      /* network order trap destination */
      trap_msg.dip.addr = td->dip.addr;
      /* lookup current source address for this dst */
      dst_if = ntcpip__ipRoute(&td->dip);
      dst_ip.addr = ntcpip_ntohl(dst_if->ip_addr.addr);
      trap_msg.sip_raw[0] = dst_ip.addr >> 24;
      trap_msg.sip_raw[1] = dst_ip.addr >> 16;
      trap_msg.sip_raw[2] = dst_ip.addr >> 8;
      trap_msg.sip_raw[3] = dst_ip.addr;
      trap_msg.gen_trap = generic_trap;
      trap_msg.spc_trap = specific_trap;
      if (generic_trap == SNMP_GENTRAP_ENTERPRISESPC)
      {
        /* enterprise-Specific trap */
        trap_msg.enterprise = eoid;
      }
      else
      {
        /* generic (MIB-II) trap */
        ntcpip__snmpGetSnmpgrpid_ptr(&trap_msg.enterprise);
      }
      ntcpip__snmpGetSysuptime(&trap_msg.ts);

      /* pass 0, calculate length fields */
      tot_len = snmp_varbind_list_sum(&trap_msg.outvb);
      tot_len = snmp_trap_header_sum(&trap_msg, tot_len);

      /* allocate pbuf(s) */
      p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, tot_len, NTCPIP_PBUF_POOL);
      if (p != NULL)
      {
        Uint16 ofs;

        /* pass 1, encode packet ino the pbuf(s) */
        ofs = snmp_trap_header_enc(&trap_msg, p);
        snmp_varbind_list_enc(&trap_msg.outvb, p, ofs);

        ntcpip__snmpIncSnmpouttraps();
        ntcpip__snmpIncSnmpoutpkts();

        /** connect to the TRAP destination */
        ntcpip__udpConnect(trap_msg.pcb, &trap_msg.dip, SNMP_TRAP_PORT);
        ntcpip__udpSend(trap_msg.pcb, p);
        /** disassociate remote address and port with this pcb */
        ntcpip__udpDisconnect(trap_msg.pcb);

        ntcpip_pbufFree(p);
      }
      else
      {
        return NTCPIP_ERR_MEM;
      }
    }
  }
  return NTCPIP_ERR_OK;
}

void
ntcpip__snmpColdstartTrap(void)
{
  trap_msg.outvb.head = NULL;
  trap_msg.outvb.tail = NULL;
  trap_msg.outvb.count = 0;
  ntcpip__snmpSendTrap(SNMP_GENTRAP_COLDSTART, NULL, 0);
}

void
ntcpip__snmpAuthfailTrap(void)
{
  Uint8 enable;
  ntcpip__snmpGetSnmpenableauthentraps(&enable);
  if (enable == 1)
  {
    trap_msg.outvb.head = NULL;
    trap_msg.outvb.tail = NULL;
    trap_msg.outvb.count = 0;
    ntcpip__snmpSendTrap(SNMP_GENTRAP_AUTHFAIL, NULL, 0);
  }
}

/**
 * Sums response header field lengths from tail to head and
 * returns resp_header_lengths for second encoding pass.
 *
 * @param vb_len varbind-list length
 * @param rhl points to returned header lengths
 * @return the required lenght for encoding the response header
 */
static Uint16
snmp_resp_header_sum(struct snmp_msg_pstat *m_stat, Uint16 vb_len)
{
  Uint16 tot_len;
  struct snmp_resp_header_lengths *rhl;

  rhl = &m_stat->rhl;
  tot_len = vb_len;
  ntcpip__snmpAsn1EncS32tCnt(m_stat->error_index, &rhl->erridxlen);
  ntcpip__snmpAsn1EncLengthCnt(rhl->erridxlen, &rhl->erridxlenlen);
  tot_len += 1 + rhl->erridxlenlen + rhl->erridxlen;

  ntcpip__snmpAsn1EncS32tCnt(m_stat->error_status, &rhl->errstatlen);
  ntcpip__snmpAsn1EncLengthCnt(rhl->errstatlen, &rhl->errstatlenlen);
  tot_len += 1 + rhl->errstatlenlen + rhl->errstatlen;

  ntcpip__snmpAsn1EncS32tCnt(m_stat->rid, &rhl->ridlen);
  ntcpip__snmpAsn1EncLengthCnt(rhl->ridlen, &rhl->ridlenlen);
  tot_len += 1 + rhl->ridlenlen + rhl->ridlen;

  rhl->pdulen = tot_len;
  ntcpip__snmpAsn1EncLengthCnt(rhl->pdulen, &rhl->pdulenlen);
  tot_len += 1 + rhl->pdulenlen;

  rhl->comlen = m_stat->com_strlen;
  ntcpip__snmpAsn1EncLengthCnt(rhl->comlen, &rhl->comlenlen);
  tot_len += 1 + rhl->comlenlen + rhl->comlen;

  ntcpip__snmpAsn1EncS32tCnt(snmp_version, &rhl->verlen);
  ntcpip__snmpAsn1EncLengthCnt(rhl->verlen, &rhl->verlenlen);
  tot_len += 1 + rhl->verlen + rhl->verlenlen;

  rhl->seqlen = tot_len;
  ntcpip__snmpAsn1EncLengthCnt(rhl->seqlen, &rhl->seqlenlen);
  tot_len += 1 + rhl->seqlenlen;

  return tot_len;
}

/**
 * Sums trap header field lengths from tail to head and
 * returns trap_header_lengths for second encoding pass.
 *
 * @param vb_len varbind-list length
 * @param thl points to returned header lengths
 * @return the required lenght for encoding the trap header
 */
static Uint16
snmp_trap_header_sum(struct snmp_msg_trap *m_trap, Uint16 vb_len)
{
  Uint16 tot_len;
  struct snmp_trap_header_lengths *thl;

  thl = &m_trap->thl;
  tot_len = vb_len;

  ntcpip__snmpAsn1EncU32tCnt(m_trap->ts, &thl->tslen);
  ntcpip__snmpAsn1EncLengthCnt(thl->tslen, &thl->tslenlen);
  tot_len += 1 + thl->tslen + thl->tslenlen;

  ntcpip__snmpAsn1EncS32tCnt(m_trap->spc_trap, &thl->strplen);
  ntcpip__snmpAsn1EncLengthCnt(thl->strplen, &thl->strplenlen);
  tot_len += 1 + thl->strplen + thl->strplenlen;

  ntcpip__snmpAsn1EncS32tCnt(m_trap->gen_trap, &thl->gtrplen);
  ntcpip__snmpAsn1EncLengthCnt(thl->gtrplen, &thl->gtrplenlen);
  tot_len += 1 + thl->gtrplen + thl->gtrplenlen;

  thl->aaddrlen = 4;
  ntcpip__snmpAsn1EncLengthCnt(thl->aaddrlen, &thl->aaddrlenlen);
  tot_len += 1 + thl->aaddrlen + thl->aaddrlenlen;

  ntcpip__snmpAsn1EncOidCnt(m_trap->enterprise->len, &m_trap->enterprise->id[0], &thl->eidlen);
  ntcpip__snmpAsn1EncLengthCnt(thl->eidlen, &thl->eidlenlen);
  tot_len += 1 + thl->eidlen + thl->eidlenlen;

  thl->pdulen = tot_len;
  ntcpip__snmpAsn1EncLengthCnt(thl->pdulen, &thl->pdulenlen);
  tot_len += 1 + thl->pdulenlen;

  thl->comlen = sizeof(snmp_publiccommunity) - 1;
  ntcpip__snmpAsn1EncLengthCnt(thl->comlen, &thl->comlenlen);
  tot_len += 1 + thl->comlenlen + thl->comlen;

  ntcpip__snmpAsn1EncS32tCnt(snmp_version, &thl->verlen);
  ntcpip__snmpAsn1EncLengthCnt(thl->verlen, &thl->verlenlen);
  tot_len += 1 + thl->verlen + thl->verlenlen;

  thl->seqlen = tot_len;
  ntcpip__snmpAsn1EncLengthCnt(thl->seqlen, &thl->seqlenlen);
  tot_len += 1 + thl->seqlenlen;

  return tot_len;
}

/**
 * Sums varbind lengths from tail to head and
 * annotates lengths in varbind for second encoding pass.
 *
 * @param root points to the root of the variable binding list
 * @return the required lenght for encoding the variable bindings
 */
static Uint16
snmp_varbind_list_sum(struct snmp_varbind_root *root)
{
  struct snmp_varbind *vb;
  Uint32 *uint_ptr;
  Int32 *sint_ptr;
  Uint16 tot_len;

  tot_len = 0;
  vb = root->tail;
  while ( vb != NULL )
  {
    /* encoded value lenght depends on type */
    switch (vb->value_type)
    {
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG):
        sint_ptr = vb->value;
        ntcpip__snmpAsn1EncS32tCnt(*sint_ptr, &vb->vlen);
        break;
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_GAUGE):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_TIMETICKS):
        uint_ptr = vb->value;
        ntcpip__snmpAsn1EncU32tCnt(*uint_ptr, &vb->vlen);
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR):
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_NUL):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_OPAQUE):
        vb->vlen = vb->value_len;
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID):
        sint_ptr = vb->value;
        ntcpip__snmpAsn1EncOidCnt(vb->value_len / sizeof(Int32), sint_ptr, &vb->vlen);
        break;
      default:
        /* unsupported type */
        vb->vlen = 0;
        break;
    };
    /* encoding length of value length field */
    ntcpip__snmpAsn1EncLengthCnt(vb->vlen, &vb->vlenlen);
    ntcpip__snmpAsn1EncOidCnt(vb->ident_len, vb->ident, &vb->olen);
    ntcpip__snmpAsn1EncLengthCnt(vb->olen, &vb->olenlen);

    vb->seqlen = 1 + vb->vlenlen + vb->vlen;
    vb->seqlen += 1 + vb->olenlen + vb->olen;
    ntcpip__snmpAsn1EncLengthCnt(vb->seqlen, &vb->seqlenlen);

    /* varbind seq */
    tot_len += 1 + vb->seqlenlen + vb->seqlen;

    vb = vb->prev;
  }

  /* varbind-list seq */
  root->seqlen = tot_len;
  ntcpip__snmpAsn1EncLengthCnt(root->seqlen, &root->seqlenlen);
  tot_len += 1 + root->seqlenlen;

  return tot_len;
}

/**
 * Encodes response header from head to tail.
 */
static Uint16
snmp_resp_header_enc(struct snmp_msg_pstat *m_stat, struct ntcpip_pbuf *p)
{
  Uint16 ofs;

  ofs = 0;
  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_CONSTR | SNMP_ASN1_SEQ));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_stat->rhl.seqlen);
  ofs += m_stat->rhl.seqlenlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_stat->rhl.verlen);
  ofs += m_stat->rhl.verlenlen;
  ntcpip__snmpAsn1EncS32t(p, ofs, m_stat->rhl.verlen, snmp_version);
  ofs += m_stat->rhl.verlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_stat->rhl.comlen);
  ofs += m_stat->rhl.comlenlen;
  ntcpip__snmpAsn1EncRaw(p, ofs, m_stat->rhl.comlen, m_stat->community);
  ofs += m_stat->rhl.comlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_CONTXT | SNMP_ASN1_CONSTR | SNMP_ASN1_PDU_GET_RESP));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_stat->rhl.pdulen);
  ofs += m_stat->rhl.pdulenlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_stat->rhl.ridlen);
  ofs += m_stat->rhl.ridlenlen;
  ntcpip__snmpAsn1EncS32t(p, ofs, m_stat->rhl.ridlen, m_stat->rid);
  ofs += m_stat->rhl.ridlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_stat->rhl.errstatlen);
  ofs += m_stat->rhl.errstatlenlen;
  ntcpip__snmpAsn1EncS32t(p, ofs, m_stat->rhl.errstatlen, m_stat->error_status);
  ofs += m_stat->rhl.errstatlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_stat->rhl.erridxlen);
  ofs += m_stat->rhl.erridxlenlen;
  ntcpip__snmpAsn1EncS32t(p, ofs, m_stat->rhl.erridxlen, m_stat->error_index);
  ofs += m_stat->rhl.erridxlen;

  return ofs;
}

/**
 * Encodes trap header from head to tail.
 */
static Uint16
snmp_trap_header_enc(struct snmp_msg_trap *m_trap, struct ntcpip_pbuf *p)
{
  Uint16 ofs;

  ofs = 0;
  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_CONSTR | SNMP_ASN1_SEQ));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.seqlen);
  ofs += m_trap->thl.seqlenlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.verlen);
  ofs += m_trap->thl.verlenlen;
  ntcpip__snmpAsn1EncS32t(p, ofs, m_trap->thl.verlen, snmp_version);
  ofs += m_trap->thl.verlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.comlen);
  ofs += m_trap->thl.comlenlen;
  ntcpip__snmpAsn1EncRaw(p, ofs, m_trap->thl.comlen, (Uint8 *)&snmp_publiccommunity[0]);
  ofs += m_trap->thl.comlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_CONTXT | SNMP_ASN1_CONSTR | SNMP_ASN1_PDU_TRAP));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.pdulen);
  ofs += m_trap->thl.pdulenlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.eidlen);
  ofs += m_trap->thl.eidlenlen;
  ntcpip__snmpAsn1EncOid(p, ofs, m_trap->enterprise->len, &m_trap->enterprise->id[0]);
  ofs += m_trap->thl.eidlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.aaddrlen);
  ofs += m_trap->thl.aaddrlenlen;
  ntcpip__snmpAsn1EncRaw(p, ofs, m_trap->thl.aaddrlen, &m_trap->sip_raw[0]);
  ofs += m_trap->thl.aaddrlen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.gtrplen);
  ofs += m_trap->thl.gtrplenlen;
  ntcpip__snmpAsn1EncU32t(p, ofs, m_trap->thl.gtrplen, m_trap->gen_trap);
  ofs += m_trap->thl.gtrplen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.strplen);
  ofs += m_trap->thl.strplenlen;
  ntcpip__snmpAsn1EncU32t(p, ofs, m_trap->thl.strplen, m_trap->spc_trap);
  ofs += m_trap->thl.strplen;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_TIMETICKS));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, m_trap->thl.tslen);
  ofs += m_trap->thl.tslenlen;
  ntcpip__snmpAsn1EncU32t(p, ofs, m_trap->thl.tslen, m_trap->ts);
  ofs += m_trap->thl.tslen;

  return ofs;
}

/**
 * Encodes varbind list from head to tail.
 */
static Uint16
snmp_varbind_list_enc(struct snmp_varbind_root *root, struct ntcpip_pbuf *p, Uint16 ofs)
{
  struct snmp_varbind *vb;
  Int32 *sint_ptr;
  Uint32 *uint_ptr;
  Uint8 *raw_ptr;

  ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_CONSTR | SNMP_ASN1_SEQ));
  ofs += 1;
  ntcpip__snmpAsn1EncLength(p, ofs, root->seqlen);
  ofs += root->seqlenlen;

  vb = root->head;
  while ( vb != NULL )
  {
    ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_CONSTR | SNMP_ASN1_SEQ));
    ofs += 1;
    ntcpip__snmpAsn1EncLength(p, ofs, vb->seqlen);
    ofs += vb->seqlenlen;

    ntcpip__snmpAsn1EncType(p, ofs, (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID));
    ofs += 1;
    ntcpip__snmpAsn1EncLength(p, ofs, vb->olen);
    ofs += vb->olenlen;
    ntcpip__snmpAsn1EncOid(p, ofs, vb->ident_len, &vb->ident[0]);
    ofs += vb->olen;

    ntcpip__snmpAsn1EncType(p, ofs, vb->value_type);
    ofs += 1;
    ntcpip__snmpAsn1EncLength(p, ofs, vb->vlen);
    ofs += vb->vlenlen;

    switch (vb->value_type)
    {
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG):
        sint_ptr = vb->value;
        ntcpip__snmpAsn1EncS32t(p, ofs, vb->vlen, *sint_ptr);
        break;
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_GAUGE):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_TIMETICKS):
        uint_ptr = vb->value;
        ntcpip__snmpAsn1EncU32t(p, ofs, vb->vlen, *uint_ptr);
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR):
      case (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_OPAQUE):
        raw_ptr = vb->value;
        ntcpip__snmpAsn1EncRaw(p, ofs, vb->vlen, raw_ptr);
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_NUL):
        break;
      case (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID):
        sint_ptr = vb->value;
        ntcpip__snmpAsn1EncOid(p, ofs, vb->value_len / sizeof(Int32), sint_ptr);
        break;
      default:
        /* unsupported type */
        break;
    };
    ofs += vb->vlen;
    vb = vb->next;
  }
  return ofs;
}

#endif /* NTCPIP__LWIP_SNMP */


