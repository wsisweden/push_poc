/**
 * @file
 * Management Information Base II (RFC1213) objects and functions.
 *
 * @note the object identifiers for this MIB-2 and private MIB tree
 * must be kept in sorted ascending order. This to ensure correct getnext operation.
 */

/*
 * Copyright (c) 2006 Axon Digital Design B.V., The Netherlands.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Christiaan Simons <christiaan.simons@axon.tv>
 */

 
#include "ntcpip/opt.h"

#if NTCPIP__LWIP_SNMP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/snmp.h"
#include "ntcpip/netif.h"
#include "lwip/ip.h"
#include "lwip/ip_frag.h"
#include "lwip/tcp.h"
#include "lwip/udp.h"
#include "ntcpip/snmp_asn1.h"
#include "ntcpip/snmp_structs.h"
#include "netif/etharp.h"

/* Tietolaite */
#include "snmpa.h"

/**
 * IANA assigned enterprise ID for lwIP is 26381
 * @see http://www.iana.org/assignments/enterprise-numbers
 *
 * @note this enterprise ID is assigned to the lwIP project,
 * all object identifiers living under this ID are assigned
 * by the lwIP maintainers (contact Christiaan Simons)!
 * @note don't change this define, use ntcpip_snmpSetSysobjid()
 *
 * If you need to create your own private MIB you'll need
 * to apply for your own enterprise ID with IANA:
 * http://www.iana.org/numbers.html
 */
#define SNMP_ENTERPRISE_ID 26381
#define SNMP_SYSOBJID_LEN 7
#define SNMP_SYSOBJID {1, 3, 6, 1, 4, 1, SNMP_ENTERPRISE_ID}

#ifndef SNMP_SYSSERVICES
#define SNMP_SYSSERVICES ((1 << 6) | (1 << 3) | ((NTCPIP__IP_FORWARD) << 2))
#endif

#ifndef SNMP_GET_SYSUPTIME
#define SNMP_GET_SYSUPTIME(sysuptime)
#endif

static void system_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void system_get_value(struct obj_def *od, Uint16 len, void *value);
static Uint8 system_set_test(struct obj_def *od, Uint16 len, void *value);
static void system_set_value(struct obj_def *od, Uint16 len, void *value);
static void interfaces_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void interfaces_get_value(struct obj_def *od, Uint16 len, void *value);
static void ifentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void ifentry_get_value(struct obj_def *od, Uint16 len, void *value);
#if !NTCPIP__SNMP_SAFE_REQUESTS
static Uint8 ifentry_set_test (struct obj_def *od, Uint16 len, void *value);
static void ifentry_set_value (struct obj_def *od, Uint16 len, void *value);
#endif /* NTCPIP__SNMP_SAFE_REQUESTS */
static void atentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void atentry_get_value(struct obj_def *od, Uint16 len, void *value);
static void ip_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void ip_get_value(struct obj_def *od, Uint16 len, void *value);
static Uint8 ip_set_test(struct obj_def *od, Uint16 len, void *value);
static void ip_addrentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void ip_addrentry_get_value(struct obj_def *od, Uint16 len, void *value);
static void ip_rteentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void ip_rteentry_get_value(struct obj_def *od, Uint16 len, void *value);
static void ip_ntomentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void ip_ntomentry_get_value(struct obj_def *od, Uint16 len, void *value);
static void icmp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void icmp_get_value(struct obj_def *od, Uint16 len, void *value);
#if NTCPIP__LWIP_TCP
static void tcp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void tcp_get_value(struct obj_def *od, Uint16 len, void *value);
#ifdef THIS_SEEMS_UNUSED
static void tcpconnentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void tcpconnentry_get_value(struct obj_def *od, Uint16 len, void *value);
#endif
#endif
static void udp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void udp_get_value(struct obj_def *od, Uint16 len, void *value);
static void udpentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void udpentry_get_value(struct obj_def *od, Uint16 len, void *value);
static void snmp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od);
static void snmp_get_value(struct obj_def *od, Uint16 len, void *value);
static Uint8 snmp_set_test(struct obj_def *od, Uint16 len, void *value);
static void snmp_set_value(struct obj_def *od, Uint16 len, void *value);


/* snmp .1.3.6.1.2.1.11 */
const mib_scalar_node snmp_scalar = {
  &snmp_get_object_def,
  &snmp_get_value,
  &snmp_set_test,
  &snmp_set_value,
  MIB_NODE_SC,
  0
};
const Int32 snmp_ids[28] = {
  1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16,
  17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30
};
struct mib_node* const snmp_nodes[28] = {
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar,
  (struct mib_node* const)&snmp_scalar, (struct mib_node* const)&snmp_scalar
};
const struct mib_array_node snmp = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  28,
  snmp_ids,
  snmp_nodes
};

/* dot3 and EtherLike MIB not planned. (transmission .1.3.6.1.2.1.10) */
/* historical (some say hysterical). (cmot .1.3.6.1.2.1.9) */
/* lwIP has no EGP, thus may not implement it. (egp .1.3.6.1.2.1.8) */

/* udp .1.3.6.1.2.1.7 */
/** index root node for udpTable */
struct mib_list_rootnode udp_root = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_LR,
  0,
  NULL,
  NULL,
  0
};
const Int32 udpentry_ids[2] = { 1, 2 };
struct mib_node* const udpentry_nodes[2] = {
  (struct mib_node* const)&udp_root, (struct mib_node* const)&udp_root,
};
const struct mib_array_node udpentry = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  2,
  udpentry_ids,
  udpentry_nodes
};

Int32 udptable_id = 1;
struct mib_node* udptable_node = (struct mib_node* const)&udpentry;
struct mib_ram_array_node udptable = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_RA,
  0,
  &udptable_id,
  &udptable_node
};

const mib_scalar_node udp_scalar = {
  &udp_get_object_def,
  &udp_get_value,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_SC,
  0
};
const Int32 udp_ids[5] = { 1, 2, 3, 4, 5 };
struct mib_node* const udp_nodes[5] = {
  (struct mib_node* const)&udp_scalar, (struct mib_node* const)&udp_scalar,
  (struct mib_node* const)&udp_scalar, (struct mib_node* const)&udp_scalar,
  (struct mib_node* const)&udptable
};
const struct mib_array_node udp = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  5,
  udp_ids,
  udp_nodes
};

/* tcp .1.3.6.1.2.1.6 */
#if NTCPIP__LWIP_TCP
/* only if the TCP protocol is available may implement this group */
/** index root node for tcpConnTable */
struct mib_list_rootnode tcpconntree_root = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_LR,
  0,
  NULL,
  NULL,
  0
};
const Int32 tcpconnentry_ids[5] = { 1, 2, 3, 4, 5 };
struct mib_node* const tcpconnentry_nodes[5] = {
  (struct mib_node* const)&tcpconntree_root, (struct mib_node* const)&tcpconntree_root,
  (struct mib_node* const)&tcpconntree_root, (struct mib_node* const)&tcpconntree_root,
  (struct mib_node* const)&tcpconntree_root
};
const struct mib_array_node tcpconnentry = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  5,
  tcpconnentry_ids,
  tcpconnentry_nodes
};

Int32 tcpconntable_id = 1;
struct mib_node* tcpconntable_node = (struct mib_node* const)&tcpconnentry;
struct mib_ram_array_node tcpconntable = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_RA,
/** @todo update maxlength when inserting / deleting from table
   0 when table is empty, 1 when more than one entry */
  0,
  &tcpconntable_id,
  &tcpconntable_node
};

const mib_scalar_node tcp_scalar = {
  &tcp_get_object_def,
  &tcp_get_value,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_SC,
  0
};
const Int32 tcp_ids[15] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
struct mib_node* const tcp_nodes[15] = {
  (struct mib_node* const)&tcp_scalar, (struct mib_node* const)&tcp_scalar,
  (struct mib_node* const)&tcp_scalar, (struct mib_node* const)&tcp_scalar,
  (struct mib_node* const)&tcp_scalar, (struct mib_node* const)&tcp_scalar,
  (struct mib_node* const)&tcp_scalar, (struct mib_node* const)&tcp_scalar,
  (struct mib_node* const)&tcp_scalar, (struct mib_node* const)&tcp_scalar,
  (struct mib_node* const)&tcp_scalar, (struct mib_node* const)&tcp_scalar,
  (struct mib_node* const)&tcpconntable, (struct mib_node* const)&tcp_scalar,
  (struct mib_node* const)&tcp_scalar
};
const struct mib_array_node tcp = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  15,
  tcp_ids,
  tcp_nodes
};
#endif

/* icmp .1.3.6.1.2.1.5 */
const mib_scalar_node icmp_scalar = {
  &icmp_get_object_def,
  &icmp_get_value,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_SC,
  0
};
const Int32 icmp_ids[26] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 };
struct mib_node* const icmp_nodes[26] = {
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar,
  (struct mib_node* const)&icmp_scalar, (struct mib_node* const)&icmp_scalar
};
const struct mib_array_node icmp = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  26,
  icmp_ids,
  icmp_nodes
};

/** index root node for ipNetToMediaTable */
struct mib_list_rootnode ipntomtree_root = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_LR,
  0,
  NULL,
  NULL,
  0
};
const Int32 ipntomentry_ids[4] = { 1, 2, 3, 4 };
struct mib_node* const ipntomentry_nodes[4] = {
  (struct mib_node* const)&ipntomtree_root, (struct mib_node* const)&ipntomtree_root,
  (struct mib_node* const)&ipntomtree_root, (struct mib_node* const)&ipntomtree_root
};
const struct mib_array_node ipntomentry = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  4,
  ipntomentry_ids,
  ipntomentry_nodes
};

Int32 ipntomtable_id = 1;
struct mib_node* ipntomtable_node = (struct mib_node* const)&ipntomentry;
struct mib_ram_array_node ipntomtable = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_RA,
  0,
  &ipntomtable_id,
  &ipntomtable_node
};

/** index root node for ipRouteTable */
struct mib_list_rootnode iprtetree_root = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_LR,
  0,
  NULL,
  NULL,
  0
};
const Int32 iprteentry_ids[13] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
struct mib_node* const iprteentry_nodes[13] = {
  (struct mib_node* const)&iprtetree_root, (struct mib_node* const)&iprtetree_root,
  (struct mib_node* const)&iprtetree_root, (struct mib_node* const)&iprtetree_root,
  (struct mib_node* const)&iprtetree_root, (struct mib_node* const)&iprtetree_root,
  (struct mib_node* const)&iprtetree_root, (struct mib_node* const)&iprtetree_root,
  (struct mib_node* const)&iprtetree_root, (struct mib_node* const)&iprtetree_root,
  (struct mib_node* const)&iprtetree_root, (struct mib_node* const)&iprtetree_root,
  (struct mib_node* const)&iprtetree_root
};
const struct mib_array_node iprteentry = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  13,
  iprteentry_ids,
  iprteentry_nodes
};

Int32 iprtetable_id = 1;
struct mib_node* iprtetable_node = (struct mib_node* const)&iprteentry;
struct mib_ram_array_node iprtetable = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_RA,
  0,
  &iprtetable_id,
  &iprtetable_node
};

/** index root node for ipAddrTable */
struct mib_list_rootnode ipaddrtree_root = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_LR,
  0,
  NULL,
  NULL,
  0
};
const Int32 ipaddrentry_ids[5] = { 1, 2, 3, 4, 5 };
struct mib_node* const ipaddrentry_nodes[5] = {
  (struct mib_node* const)&ipaddrtree_root,
  (struct mib_node* const)&ipaddrtree_root,
  (struct mib_node* const)&ipaddrtree_root,
  (struct mib_node* const)&ipaddrtree_root,
  (struct mib_node* const)&ipaddrtree_root
};
const struct mib_array_node ipaddrentry = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  5,
  ipaddrentry_ids,
  ipaddrentry_nodes
};

Int32 ipaddrtable_id = 1;
struct mib_node* ipaddrtable_node = (struct mib_node* const)&ipaddrentry;
struct mib_ram_array_node ipaddrtable = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_RA,
  0,
  &ipaddrtable_id,
  &ipaddrtable_node
};

/* ip .1.3.6.1.2.1.4 */
const mib_scalar_node ip_scalar = {
  &ip_get_object_def,
  &ip_get_value,
  &ip_set_test,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_SC,
  0
};
const Int32 ip_ids[23] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };
struct mib_node* const ip_nodes[23] = {
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ip_scalar,
  (struct mib_node* const)&ip_scalar, (struct mib_node* const)&ipaddrtable,
  (struct mib_node* const)&iprtetable, (struct mib_node* const)&ipntomtable,
  (struct mib_node* const)&ip_scalar
};
const struct mib_array_node mib2_ip = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  23,
  ip_ids,
  ip_nodes
};

/** index root node for atTable */
struct mib_list_rootnode arptree_root = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_LR,
  0,
  NULL,
  NULL,
  0
};
const Int32 atentry_ids[3] = { 1, 2, 3 };
struct mib_node* const atentry_nodes[3] = {
  (struct mib_node* const)&arptree_root,
  (struct mib_node* const)&arptree_root,
  (struct mib_node* const)&arptree_root
};
const struct mib_array_node atentry = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  3,
  atentry_ids,
  atentry_nodes
};

const Int32 attable_id = 1;
struct mib_node* const attable_node = (struct mib_node* const)&atentry;
const struct mib_array_node attable = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  1,
  &attable_id,
  &attable_node
};

/* at .1.3.6.1.2.1.3 */
Int32 at_id = 1;
struct mib_node* mib2_at_node = (struct mib_node* const)&attable;
struct mib_ram_array_node at = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_RA,
  0,
  &at_id,
  &mib2_at_node
};

/** index root node for ifTable */
struct mib_list_rootnode iflist_root = {
  &ifentry_get_object_def,
  &ifentry_get_value,
#if NTCPIP__SNMP_SAFE_REQUESTS
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
#else /* NTCPIP__SNMP_SAFE_REQUESTS */
  &ifentry_set_test,
  &ifentry_set_value,
#endif /* NTCPIP__SNMP_SAFE_REQUESTS */
  MIB_NODE_LR,
  0,
  NULL,
  NULL,
  0
};
const Int32 ifentry_ids[22] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
struct mib_node* const ifentry_nodes[22] = {
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root,
  (struct mib_node* const)&iflist_root, (struct mib_node* const)&iflist_root
};
const struct mib_array_node ifentry = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  22,
  ifentry_ids,
  ifentry_nodes
};

Int32 iftable_id = 1;
struct mib_node* iftable_node = (struct mib_node* const)&ifentry;
struct mib_ram_array_node iftable = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_RA,
  0,
  &iftable_id,
  &iftable_node
};

/* interfaces .1.3.6.1.2.1.2 */
const mib_scalar_node interfaces_scalar = {
  &interfaces_get_object_def,
  &interfaces_get_value,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_SC,
  0
};
const Int32 interfaces_ids[2] = { 1, 2 };
struct mib_node* const interfaces_nodes[2] = {
  (struct mib_node* const)&interfaces_scalar, (struct mib_node* const)&iftable
};
const struct mib_array_node interfaces = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  2,
  interfaces_ids,
  interfaces_nodes
};


/*             0 1 2 3 4 5 6 */
/* system .1.3.6.1.2.1.1 */
const mib_scalar_node sys_tem_scalar = {
  &system_get_object_def,
  &system_get_value,
  &system_set_test,
  &system_set_value,
  MIB_NODE_SC,
  0
};
const Int32 sys_tem_ids[7] = { 1, 2, 3, 4, 5, 6, 7 };
struct mib_node* const sys_tem_nodes[7] = {
  (struct mib_node* const)&sys_tem_scalar, (struct mib_node* const)&sys_tem_scalar,
  (struct mib_node* const)&sys_tem_scalar, (struct mib_node* const)&sys_tem_scalar,
  (struct mib_node* const)&sys_tem_scalar, (struct mib_node* const)&sys_tem_scalar,
  (struct mib_node* const)&sys_tem_scalar
};
/* work around name issue with 'sys_tem', some compiler(s?) seem to reserve 'system' */
const struct mib_array_node sys_tem = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  7,
  sys_tem_ids,
  sys_tem_nodes
};

/* mib-2 .1.3.6.1.2.1 */
#if NTCPIP__LWIP_TCP
#define MIB2_GROUPS 8
#else
#define MIB2_GROUPS 7
#endif
const Int32 mib2_ids[MIB2_GROUPS] =
{
  1,
  2,
  3,
  4,
  5,
#if NTCPIP__LWIP_TCP
  6,
#endif
  7,
  11
};
struct mib_node* const mib2_nodes[MIB2_GROUPS] = {
  (struct mib_node* const)&sys_tem,
  (struct mib_node* const)&interfaces,
  (struct mib_node* const)&at,
  (struct mib_node* const)&mib2_ip,
  (struct mib_node* const)&icmp,
#if NTCPIP__LWIP_TCP
  (struct mib_node* const)&tcp,
#endif
  (struct mib_node* const)&udp,
  (struct mib_node* const)&snmp
};

const struct mib_array_node mib2 = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  MIB2_GROUPS,
  mib2_ids,
  mib2_nodes
};

/* mgmt .1.3.6.1.2 */
const Int32 mgmt_ids[1] = { 1 };
struct mib_node* const mgmt_nodes[1] = { (struct mib_node* const)&mib2 };
const struct mib_array_node mgmt = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  1,
  mgmt_ids,
  mgmt_nodes
};

/* internet .1.3.6.1 */
#if NTCPIP__SNMP_PRIVATE_MIB
Int32 internet_ids[2] = { 2, 4 };
struct mib_node* const internet_nodes[2] = { (struct mib_node* const)&mgmt, (struct mib_node* const)&private };
const struct mib_array_node internet = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  2,
  internet_ids,
  internet_nodes
};
#else
const Int32 internet_ids[1] = { 2 };
struct mib_node* const internet_nodes[1] = { (struct mib_node* const)&mgmt };
const struct mib_array_node internet = {
  &ntcpip_snmpNoleafsGetObjectDef,
  &ntcpip_snmpNoleafsGetValue,
  &ntcpip_snmpNoleafsSetTest,
  &ntcpip_snmpNoleafsSetValue,
  MIB_NODE_AR,
  1,
  internet_ids,
  internet_nodes
};
#endif

/** mib-2.system.sysObjectID  */
static struct snmp_obj_id sysobjid = {SNMP_SYSOBJID_LEN, SNMP_SYSOBJID};
/** enterprise ID for generic TRAPs, .iso.org.dod.internet.mgmt.mib-2.snmp */
static struct snmp_obj_id snmpgrp_id = {7,{1,3,6,1,2,1,11}};
/** mib-2.system.sysServices */
static const Int32 sysservices = SNMP_SYSSERVICES;

/** mib-2.system.sysDescr */
static const Uint8 sysdescr_len_default = 4;
static const Uint8 sysdescr_default[] = "lwIP";
static Uint8* sysdescr_len_ptr = (Uint8*)&sysdescr_len_default;
static Uint8* sysdescr_ptr = (Uint8*)&sysdescr_default[0];
/** mib-2.system.sysContact */
static const Uint8 syscontact_len_default = 0;
static const Uint8 syscontact_default[] = "";
static Uint8* syscontact_len_ptr = (Uint8*)&syscontact_len_default;
static Uint8* syscontact_ptr = (Uint8*)&syscontact_default[0];
/** mib-2.system.sysName */
static const Uint8 sysname_len_default = 8;
static const Uint8 sysname_default[] = "FQDN-unk";
static Uint8* sysname_len_ptr = (Uint8*)&sysname_len_default;
static Uint8* sysname_ptr = (Uint8*)&sysname_default[0];
/** mib-2.system.sysLocation */
static const Uint8 syslocation_len_default = 0;
static const Uint8 syslocation_default[] = "";
static Uint8* syslocation_len_ptr = (Uint8*)&syslocation_len_default;
static Uint8* syslocation_ptr = (Uint8*)&syslocation_default[0];
/** mib-2.snmp.snmpEnableAuthenTraps */
static const Uint8 snmpenableauthentraps_default = 2; /* disabled */
static Uint8* snmpenableauthentraps_ptr = (Uint8*)&snmpenableauthentraps_default;

/** mib-2.interfaces.ifTable.ifEntry.ifSpecific (zeroDotZero) */
static const struct snmp_obj_id ifspecific = {2, {0, 0}};
/** mib-2.ip.ipRouteTable.ipRouteEntry.ipRouteInfo (zeroDotZero) */
static const struct snmp_obj_id iprouteinfo = {2, {0, 0}};



/* mib-2.system counter(s) */
static Uint32 sysuptime = 0;

/* mib-2.ip counter(s) */
static Uint32 ipinreceives = 0,
             ipinhdrerrors = 0,
             ipinaddrerrors = 0,
             ipforwdatagrams = 0,
             ipinunknownprotos = 0,
             ipindiscards = 0,
             ipindelivers = 0,
             ipoutrequests = 0,
             ipoutdiscards = 0,
             ipoutnoroutes = 0,
             ipreasmreqds = 0,
             ipreasmoks = 0,
             ipreasmfails = 0,
             ipfragoks = 0,
             ipfragfails = 0,
             ipfragcreates = 0,
             iproutingdiscards = 0;
/* mib-2.icmp counter(s) */
static Uint32 icmpinmsgs = 0,
             icmpinerrors = 0,
             icmpindestunreachs = 0,
             icmpintimeexcds = 0,
             icmpinparmprobs = 0,
             icmpinsrcquenchs = 0,
             icmpinredirects = 0,
             icmpinechos = 0,
             icmpinechoreps = 0,
             icmpintimestamps = 0,
             icmpintimestampreps = 0,
             icmpinaddrmasks = 0,
             icmpinaddrmaskreps = 0,
             icmpoutmsgs = 0,
             icmpouterrors = 0,
             icmpoutdestunreachs = 0,
             icmpouttimeexcds = 0,
             icmpoutparmprobs = 0,
             icmpoutsrcquenchs = 0,
             icmpoutredirects = 0,
             icmpoutechos = 0,
             icmpoutechoreps = 0,
             icmpouttimestamps = 0,
             icmpouttimestampreps = 0,
             icmpoutaddrmasks = 0,
             icmpoutaddrmaskreps = 0;
/* mib-2.tcp counter(s) */
static Uint32 tcpactiveopens = 0,
             tcppassiveopens = 0,
             tcpattemptfails = 0,
             tcpestabresets = 0,
             tcpinsegs = 0,
             tcpoutsegs = 0,
             tcpretranssegs = 0,
             tcpinerrs = 0,
             tcpoutrsts = 0;
/* mib-2.udp counter(s) */
static Uint32 udpindatagrams = 0,
             udpnoports = 0,
             udpinerrors = 0,
             udpoutdatagrams = 0;
/* mib-2.snmp counter(s) */
static Uint32 snmpinpkts = 0,
             snmpoutpkts = 0,
             snmpinbadversions = 0,
             snmpinbadcommunitynames = 0,
             snmpinbadcommunityuses = 0,
             snmpinasnparseerrs = 0,
             snmpintoobigs = 0,
             snmpinnosuchnames = 0,
             snmpinbadvalues = 0,
             snmpinreadonlys = 0,
             snmpingenerrs = 0,
             snmpintotalreqvars = 0,
             snmpintotalsetvars = 0,
             snmpingetrequests = 0,
             snmpingetnexts = 0,
             snmpinsetrequests = 0,
             snmpingetresponses = 0,
             snmpintraps = 0,
             snmpouttoobigs = 0,
             snmpoutnosuchnames = 0,
             snmpoutbadvalues = 0,
             snmpoutgenerrs = 0,
             snmpoutgetrequests = 0,
             snmpoutgetnexts = 0,
             snmpoutsetrequests = 0,
             snmpoutgetresponses = 0,
             snmpouttraps = 0;



/* prototypes of the following functions are in lwip/src/include/lwip/snmp.h */
/**
 * Copy octet string.
 *
 * @param dst points to destination
 * @param src points to source
 * @param n number of octets to copy.
 */
void ntcpip__snmpOcstrncpy(Uint8 *dst, Uint8 *src, Uint8 n)
{
  while (n > 0)
  {
    n--;
    *dst++ = *src++;
  }
}

/**
 * Copy object identifier (Int32) array.
 *
 * @param dst points to destination
 * @param src points to source
 * @param n number of sub identifiers to copy.
 */
void ntcpip__snmpObjectidncpy(Int32 *dst, Int32 *src, Uint8 n)
{
  while(n > 0)
  {
    n--;
    *dst++ = *src++;
  }
}

/**
 * Initializes sysDescr pointers.
 *
 * @param str if non-NULL then copy str pointer
 * @param len points to string length, excluding zero terminator
 */
void ntcpip_snmpSetSysdesr(Uint8 *str, Uint8 *len)
{
  if (str != NULL)
  {
    sysdescr_ptr = str;
    sysdescr_len_ptr = len;
  }
}

void ntcpip__snmpGetSysobjid_ptr(struct snmp_obj_id **oid)
{
  *oid = &sysobjid;
}

/**
 * Initializes sysObjectID value.
 *
 * @param oid points to stuct snmp_obj_id to copy
 */
void ntcpip_snmpSetSysobjid(struct snmp_obj_id *oid)
{
  sysobjid = *oid;
}

/**
 * Must be called at regular 10 msec interval from a timer interrupt
 * or signal handler depending on your runtime environment.
 */
void ntcpip__snmpIncSysuptime(void)
{
  sysuptime++;
}

void ntcpip_snmpAddSysuptime(Uint32 value)
{
  sysuptime+=value;
}

void ntcpip__snmpGetSysuptime(Uint32 *value)
{
  SNMP_GET_SYSUPTIME(sysuptime);
  *value = sysuptime;
}

/**
 * Initializes sysContact pointers,
 * e.g. ptrs to non-volatile memory external to lwIP.
 *
 * @param ocstr if non-NULL then copy str pointer
 * @param ocstrlen points to string length, excluding zero terminator
 */
void ntcpip_snmpSetSyscontact(Uint8 *ocstr, Uint8 *ocstrlen)
{
  if (ocstr != NULL)
  {
    syscontact_ptr = ocstr;
    syscontact_len_ptr = ocstrlen;
  }
}

/**
 * Initializes sysName pointers,
 * e.g. ptrs to non-volatile memory external to lwIP.
 *
 * @param ocstr if non-NULL then copy str pointer
 * @param ocstrlen points to string length, excluding zero terminator
 */
void ntcpip_snmpSetSysname(Uint8 *ocstr, Uint8 *ocstrlen)
{
  if (ocstr != NULL)
  {
    sysname_ptr = ocstr;
    sysname_len_ptr = ocstrlen;
  }
}

/**
 * Initializes sysLocation pointers,
 * e.g. ptrs to non-volatile memory external to lwIP.
 *
 * @param ocstr if non-NULL then copy str pointer
 * @param ocstrlen points to string length, excluding zero terminator
 */
void ntcpip_snmpSetSyslocation(Uint8 *ocstr, Uint8 *ocstrlen)
{
  if (ocstr != NULL)
  {
    syslocation_ptr = ocstr;
    syslocation_len_ptr = ocstrlen;
  }
}


void ntcpip_snmpAddIfinoctets(struct ntcpip__netif *ni, Uint32 value)
{
  ni->ifinoctets += value;
}

void ntcpip_snmpIncIfinucastpkts(struct ntcpip__netif *ni)
{
  (ni->ifinucastpkts)++;
}

void ntcpip_snmpIncIfinnucastpkts(struct ntcpip__netif *ni)
{
  (ni->ifinnucastpkts)++;
}

void ntcpip_snmpIncIfindiscards(struct ntcpip__netif *ni)
{
  (ni->ifindiscards)++;
}

void ntcpip_snmpAddIfoutoctets(struct ntcpip__netif *ni, Uint32 value)
{
  ni->ifoutoctets += value;
}

void ntcpip_snmpIncIfoutucastpkts(struct ntcpip__netif *ni)
{
  (ni->ifoutucastpkts)++;
}

void ntcpip_snmpIncIfoutnucastpkts(struct ntcpip__netif *ni)
{
  (ni->ifoutnucastpkts)++;
}

void ntcpip_snmpIncIfoutdiscards(struct ntcpip__netif *ni)
{
  (ni->ifoutdiscards)++;
}

void ntcpip__snmpIncIflist(void)
{
  struct mib_list_node *if_node = NULL;

  ntcpip__snmpMibNodeInsert(&iflist_root, iflist_root.count + 1, &if_node);
  /* enable getnext traversal on filled table */
  iftable.maxlength = 1;
}

void ntcpip__snmpDecIflist(void)
{
  ntcpip__snmpMibNodeDelete(&iflist_root, iflist_root.tail);
  /* disable getnext traversal on empty table */
  if(iflist_root.count == 0) iftable.maxlength = 0;
}

/**
 * Inserts ARP table indexes (.xIfIndex.xNetAddress)
 * into arp table index trees (both atTable and ipNetToMediaTable).
 */
void ntcpip__snmpInsertArpidxTree(struct ntcpip__netif *ni, struct ntcpip_ipAddr *ip)
{
  struct mib_list_rootnode *at_rn;
  struct mib_list_node *at_node;
  struct ntcpip_ipAddr hip;
  Int32 arpidx[5];
  Uint8 level, tree;

  NTCPIP__LWIP_ASSERT("ni != NULL", ni != NULL);
  ntcpip__snmpNetiftoifindex(ni, &arpidx[0]);
  hip.addr = ntcpip_ntohl(ip->addr);
  ntcpip__snmpIptooid(&hip, &arpidx[1]);

  for (tree = 0; tree < 2; tree++)
  {
    if (tree == 0)
    {
      at_rn = &arptree_root;
    }
    else
    {
      at_rn = &ipntomtree_root;
    }
    for (level = 0; level < 5; level++)
    {
      at_node = NULL;
      ntcpip__snmpMibNodeInsert(at_rn, arpidx[level], &at_node);
      if ((level != 4) && (at_node != NULL))
      {
        if (at_node->nptr == NULL)
        {
          at_rn = ntcpip__snmpMibLrnAlloc();
          at_node->nptr = (struct mib_node*)at_rn;
          if (at_rn != NULL)
          {
            if (level == 3)
            {
              if (tree == 0)
              {
                at_rn->get_object_def = atentry_get_object_def;
                at_rn->get_value = atentry_get_value;
              }
              else
              {
                at_rn->get_object_def = ip_ntomentry_get_object_def;
                at_rn->get_value = ip_ntomentry_get_value;
              }
              at_rn->set_test = ntcpip_snmpNoleafsSetTest;
              at_rn->set_value = ntcpip_snmpNoleafsSetValue;
            }
          }
          else
          {
            /* at_rn == NULL, malloc failure */
            NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ntcpip__snmpInsertArpidxTree() insert failed, mem full"));
            break;
          }
        }
        else
        {
          at_rn = (struct mib_list_rootnode*)at_node->nptr;
        }
      }
    }
  }
  /* enable getnext traversal on filled tables */
  at.maxlength = 1;
  ipntomtable.maxlength = 1;
}

/**
 * Removes ARP table indexes (.xIfIndex.xNetAddress)
 * from arp table index trees.
 */
void ntcpip__snmpDeleteArpidxTree(struct ntcpip__netif *ni, struct ntcpip_ipAddr *ip)
{
  struct mib_list_rootnode *at_rn, *next, *del_rn[5];
  struct mib_list_node *at_n, *del_n[5];
  struct ntcpip_ipAddr hip;
  Int32 arpidx[5];
  Uint8 fc, tree, level, del_cnt;

  ntcpip__snmpNetiftoifindex(ni, &arpidx[0]);
  hip.addr = ntcpip_ntohl(ip->addr);
  ntcpip__snmpIptooid(&hip, &arpidx[1]);

  for (tree = 0; tree < 2; tree++)
  {
    /* mark nodes for deletion */
    if (tree == 0)
    {
      at_rn = &arptree_root;
    }
    else
    {
      at_rn = &ipntomtree_root;
    }
    level = 0;
    del_cnt = 0;
    while ((level < 5) && (at_rn != NULL))
    {
      fc = ntcpip__snmpMibNodeFind(at_rn, arpidx[level], &at_n);
      if (fc == 0)
      {
        /* arpidx[level] does not exist */
        del_cnt = 0;
        at_rn = NULL;
      }
      else if (fc == 1)
      {
        del_rn[del_cnt] = at_rn;
        del_n[del_cnt] = at_n;
        del_cnt++;
        at_rn = (struct mib_list_rootnode*)(at_n->nptr);
      }
      else if (fc == 2)
      {
        /* reset delete (2 or more childs) */
        del_cnt = 0;
        at_rn = (struct mib_list_rootnode*)(at_n->nptr);
      }
      level++;
    }
    /* delete marked index nodes */
    while (del_cnt > 0)
    {
      del_cnt--;

      at_rn = del_rn[del_cnt];
      at_n = del_n[del_cnt];

      next = ntcpip__snmpMibNodeDelete(at_rn, at_n);
      if (next != NULL)
      {
        NTCPIP__LWIP_ASSERT("next_count == 0",next->count == 0);
        ntcpip__snmpMibLrnFree(next);
      }
    }
  }
  /* disable getnext traversal on empty tables */
  if(arptree_root.count == 0) at.maxlength = 0;
  if(ipntomtree_root.count == 0) ipntomtable.maxlength = 0;
}

void ntcpip__snmpIncIpinreceives(void)
{
  ipinreceives++;
}

void ntcpip__snmpIncIpinhdrerrors(void)
{
  ipinhdrerrors++;
}

void ntcpip__snmpIncIpinaddrerrors(void)
{
  ipinaddrerrors++;
}

void ntcpip__snmpIncIpforwdatagrams(void)
{
  ipforwdatagrams++;
}

void ntcpip__snmpIncIpinunknownprotos(void)
{
  ipinunknownprotos++;
}

void ntcpip__snmpIncIpindiscards(void)
{
  ipindiscards++;
}

void ntcpip__snmpIncIpindelivers(void)
{
  ipindelivers++;
}

void ntcpip__snmpIncIpoutrequests(void)
{
  ipoutrequests++;
}

void ntcpip__snmpIncIpoutdiscards(void)
{
  ipoutdiscards++;
}

void ntcpip__snmpIncIpoutnoroutes(void)
{
  ipoutnoroutes++;
}

void ntcpip__snmpIncIpreasmreqds(void)
{
  ipreasmreqds++;
}

void ntcpip__snmpIncIpreasmoks(void)
{
  ipreasmoks++;
}

void ntcpip__snmpIncIpreasmfails(void)
{
  ipreasmfails++;
}

void ntcpip__snmpIncIpfragoks(void)
{
  ipfragoks++;
}

void ntcpip__snmpIncIpfragfails(void)
{
  ipfragfails++;
}

void ntcpip__snmpIncIpfragcreates(void)
{
  ipfragcreates++;
}

void ntcpip__snmpIncIproutingdiscards(void)
{
  iproutingdiscards++;
}

/**
 * Inserts ipAddrTable indexes (.ipAdEntAddr)
 * into index tree.
 */
void ntcpip__snmpInsertIpaddridxTree(struct ntcpip__netif *ni)
{
  struct mib_list_rootnode *ipa_rn;
  struct mib_list_node *ipa_node;
  struct ntcpip_ipAddr ip;
  Int32 ipaddridx[4];
  Uint8 level;

  NTCPIP__LWIP_ASSERT("ni != NULL", ni != NULL);
  ip.addr = ntcpip_ntohl(ni->ip_addr.addr);
  ntcpip__snmpIptooid(&ip, &ipaddridx[0]);

  level = 0;
  ipa_rn = &ipaddrtree_root;
  while (level < 4)
  {
    ipa_node = NULL;
    ntcpip__snmpMibNodeInsert(ipa_rn, ipaddridx[level], &ipa_node);
    if ((level != 3) && (ipa_node != NULL))
    {
      if (ipa_node->nptr == NULL)
      {
        ipa_rn = ntcpip__snmpMibLrnAlloc();
        ipa_node->nptr = (struct mib_node*)ipa_rn;
        if (ipa_rn != NULL)
        {
          if (level == 2)
          {
            ipa_rn->get_object_def = ip_addrentry_get_object_def;
            ipa_rn->get_value = ip_addrentry_get_value;
            ipa_rn->set_test = ntcpip_snmpNoleafsSetTest;
            ipa_rn->set_value = ntcpip_snmpNoleafsSetValue;
          }
        }
        else
        {
          /* ipa_rn == NULL, malloc failure */
          NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ntcpip__snmpInsertIpaddridxTree() insert failed, mem full"));
          break;
        }
      }
      else
      {
        ipa_rn = (struct mib_list_rootnode*)ipa_node->nptr;
      }
    }
    level++;
  }
  /* enable getnext traversal on filled table */
  ipaddrtable.maxlength = 1;
}

/**
 * Removes ipAddrTable indexes (.ipAdEntAddr)
 * from index tree.
 */
void ntcpip__snmpDeleteIpaddridxTree(struct ntcpip__netif *ni)
{
  struct mib_list_rootnode *ipa_rn, *next, *del_rn[4];
  struct mib_list_node *ipa_n, *del_n[4];
  struct ntcpip_ipAddr ip;
  Int32 ipaddridx[4];
  Uint8 fc, level, del_cnt;

  NTCPIP__LWIP_ASSERT("ni != NULL", ni != NULL);
  ip.addr = ntcpip_ntohl(ni->ip_addr.addr);
  ntcpip__snmpIptooid(&ip, &ipaddridx[0]);

  /* mark nodes for deletion */
  level = 0;
  del_cnt = 0;
  ipa_rn = &ipaddrtree_root;
  while ((level < 4) && (ipa_rn != NULL))
  {
    fc = ntcpip__snmpMibNodeFind(ipa_rn, ipaddridx[level], &ipa_n);
    if (fc == 0)
    {
      /* ipaddridx[level] does not exist */
      del_cnt = 0;
      ipa_rn = NULL;
    }
    else if (fc == 1)
    {
      del_rn[del_cnt] = ipa_rn;
      del_n[del_cnt] = ipa_n;
      del_cnt++;
      ipa_rn = (struct mib_list_rootnode*)(ipa_n->nptr);
    }
    else if (fc == 2)
    {
      /* reset delete (2 or more childs) */
      del_cnt = 0;
      ipa_rn = (struct mib_list_rootnode*)(ipa_n->nptr);
    }
    level++;
  }
  /* delete marked index nodes */
  while (del_cnt > 0)
  {
    del_cnt--;

    ipa_rn = del_rn[del_cnt];
    ipa_n = del_n[del_cnt];

    next = ntcpip__snmpMibNodeDelete(ipa_rn, ipa_n);
    if (next != NULL)
    {
      NTCPIP__LWIP_ASSERT("next_count == 0",next->count == 0);
      ntcpip__snmpMibLrnFree(next);
    }
  }
  /* disable getnext traversal on empty table */
  if (ipaddrtree_root.count == 0) ipaddrtable.maxlength = 0;
}

/**
 * Inserts ipRouteTable indexes (.ipRouteDest)
 * into index tree.
 *
 * @param dflt non-zero for the default rte, zero for network rte
 * @param ni points to network interface for this rte
 *
 * @todo record sysuptime for _this_ route when it is installed
 *   (needed for ipRouteAge) in the netif.
 */
void ntcpip__snmpInsertIprteidxTree(Uint8 dflt, struct ntcpip__netif *ni)
{
  Uint8 insert = 0;
  struct ntcpip_ipAddr dst;

  if (dflt != 0)
  {
    /* the default route 0.0.0.0 */
    dst.addr = 0;
    insert = 1;
  }
  else
  {
    /* route to the network address */
    dst.addr = ntcpip_ntohl(ni->ip_addr.addr & ni->netmask.addr);
    /* exclude 0.0.0.0 network (reserved for default rte) */
    if (dst.addr != 0) insert = 1;
  }
  if (insert)
  {
    struct mib_list_rootnode *iprte_rn;
    struct mib_list_node *iprte_node;
    Int32 iprteidx[4];
    Uint8 level;

    ntcpip__snmpIptooid(&dst, &iprteidx[0]);
    level = 0;
    iprte_rn = &iprtetree_root;
    while (level < 4)
    {
      iprte_node = NULL;
      ntcpip__snmpMibNodeInsert(iprte_rn, iprteidx[level], &iprte_node);
      if ((level != 3) && (iprte_node != NULL))
      {
        if (iprte_node->nptr == NULL)
        {
          iprte_rn = ntcpip__snmpMibLrnAlloc();
          iprte_node->nptr = (struct mib_node*)iprte_rn;
          if (iprte_rn != NULL)
          {
            if (level == 2)
            {
              iprte_rn->get_object_def = ip_rteentry_get_object_def;
              iprte_rn->get_value = ip_rteentry_get_value;
              iprte_rn->set_test = ntcpip_snmpNoleafsSetTest;
              iprte_rn->set_value = ntcpip_snmpNoleafsSetValue;
            }
          }
          else
          {
            /* iprte_rn == NULL, malloc failure */
            NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ntcpip__snmpInsertIprteidxTree() insert failed, mem full"));
            break;
          }
        }
        else
        {
          iprte_rn = (struct mib_list_rootnode*)iprte_node->nptr;
        }
      }
      level++;
    }
  }
  /* enable getnext traversal on filled table */
  iprtetable.maxlength = 1;
}

/**
 * Removes ipRouteTable indexes (.ipRouteDest)
 * from index tree.
 *
 * @param dflt non-zero for the default rte, zero for network rte
 * @param ni points to network interface for this rte or NULL
 *   for default route to be removed.
 */
void ntcpip__snmpDeleteIprteidxTree(Uint8 dflt, struct ntcpip__netif *ni)
{
  Uint8 delete = 0;
  struct ntcpip_ipAddr dst;

  if (dflt != 0)
  {
    /* the default route 0.0.0.0 */
    dst.addr = 0;
    delete = 1;
  }
  else
  {
    /* route to the network address */
    dst.addr = ntcpip_ntohl(ni->ip_addr.addr & ni->netmask.addr);
    /* exclude 0.0.0.0 network (reserved for default rte) */
    if (dst.addr != 0) delete = 1;
  }
  if (delete)
  {
    struct mib_list_rootnode *iprte_rn, *next, *del_rn[4];
    struct mib_list_node *iprte_n, *del_n[4];
    Int32 iprteidx[4];
    Uint8 fc, level, del_cnt;

    ntcpip__snmpIptooid(&dst, &iprteidx[0]);
    /* mark nodes for deletion */
    level = 0;
    del_cnt = 0;
    iprte_rn = &iprtetree_root;
    while ((level < 4) && (iprte_rn != NULL))
    {
      fc = ntcpip__snmpMibNodeFind(iprte_rn, iprteidx[level], &iprte_n);
      if (fc == 0)
      {
        /* iprteidx[level] does not exist */
        del_cnt = 0;
        iprte_rn = NULL;
      }
      else if (fc == 1)
      {
        del_rn[del_cnt] = iprte_rn;
        del_n[del_cnt] = iprte_n;
        del_cnt++;
        iprte_rn = (struct mib_list_rootnode*)(iprte_n->nptr);
      }
      else if (fc == 2)
      {
        /* reset delete (2 or more childs) */
        del_cnt = 0;
        iprte_rn = (struct mib_list_rootnode*)(iprte_n->nptr);
      }
      level++;
    }
    /* delete marked index nodes */
    while (del_cnt > 0)
    {
      del_cnt--;

      iprte_rn = del_rn[del_cnt];
      iprte_n = del_n[del_cnt];

      next = ntcpip__snmpMibNodeDelete(iprte_rn, iprte_n);
      if (next != NULL)
      {
        NTCPIP__LWIP_ASSERT("next_count == 0",next->count == 0);
        ntcpip__snmpMibLrnFree(next);
      }
    }
  }
  /* disable getnext traversal on empty table */
  if (iprtetree_root.count == 0) iprtetable.maxlength = 0;
}


void ntcpip__snmpIncIcmpinmsgs(void)
{
  icmpinmsgs++;
}

void ntcpip__snmpIncIcmpinerrors(void)
{
  icmpinerrors++;
}

void ntcpip__snmpIncIcmpindestunreachs(void)
{
  icmpindestunreachs++;
}

void ntcpip__snmpIncIcmpintimeexcds(void)
{
  icmpintimeexcds++;
}

void ntcpip__snmpIncIcmpinparmprobs(void)
{
  icmpinparmprobs++;
}

void ntcpip__snmpIncIcmpinsrcquenchs(void)
{
  icmpinsrcquenchs++;
}

void ntcpip__snmpIncIcmpinredirects(void)
{
  icmpinredirects++;
}

void ntcpip__snmpIncIcmpinechos(void)
{
  icmpinechos++;
}

void ntcpip__snmpIncIcmpinechoreps(void)
{
  icmpinechoreps++;
}

void ntcpip__snmpIncIcmpintimestamps(void)
{
  icmpintimestamps++;
}

void ntcpip__snmpIncIcmpintimestampreps(void)
{
  icmpintimestampreps++;
}

void ntcpip__snmpIncIcmpinaddrmasks(void)
{
  icmpinaddrmasks++;
}

void ntcpip__snmpIncIcmpinaddrmaskreps(void)
{
  icmpinaddrmaskreps++;
}

void ntcpip__snmpIncIcmpoutmsgs(void)
{
  icmpoutmsgs++;
}

void ntcpip__snmpIncIcmpouterrors(void)
{
  icmpouterrors++;
}

void ntcpip__snmpIncIcmpoutdestunreachs(void)
{
  icmpoutdestunreachs++;
}

void ntcpip__snmpIncIcmpouttimeexcds(void)
{
  icmpouttimeexcds++;
}

void ntcpip__snmpIncIcmpoutparmprobs(void)
{
  icmpoutparmprobs++;
}

void ntcpip__snmpIncIcmpoutsrcquenchs(void)
{
  icmpoutsrcquenchs++;
}

void ntcpip__snmpIncIcmpoutredirects(void)
{
  icmpoutredirects++;
}

void ntcpip__snmpIncIcmpoutechos(void)
{
  icmpoutechos++;
}

void ntcpip__snmpIncIcmpoutechoreps(void)
{
  icmpoutechoreps++;
}

void ntcpip__snmpIncIcmpouttimestamps(void)
{
  icmpouttimestamps++;
}

void ntcpip__snmpIncIcmpouttimestampreps(void)
{
  icmpouttimestampreps++;
}

void ntcpip__snmpIncIcmpoutaddrmasks(void)
{
  icmpoutaddrmasks++;
}

void ntcpip__snmpIncIcmpoutaddrmaskreps(void)
{
  icmpoutaddrmaskreps++;
}

void ntcpip__snmpIncTcpactiveopens(void)
{
  tcpactiveopens++;
}

void ntcpip__snmpIncTcppassiveopens(void)
{
  tcppassiveopens++;
}

void ntcpip__snmpIncTcpattemptfails(void)
{
  tcpattemptfails++;
}

void ntcpip__snmpIncTcpestabresets(void)
{
  tcpestabresets++;
}

void ntcpip__snmpIncTcpinsegs(void)
{
  tcpinsegs++;
}

void ntcpip__snmpIncTcpoutsegs(void)
{
  tcpoutsegs++;
}

void ntcpip__snmpIncTcpretranssegs(void)
{
  tcpretranssegs++;
}

void ntcpip__snmpIncTcpinerrs(void)
{
  tcpinerrs++;
}

void ntcpip__snmpIncTcpoutrsts(void)
{
  tcpoutrsts++;
}

void ntcpip__snmpInc_udpindatagrams(void)
{
  udpindatagrams++;
}

void ntcpip__snmpIncUdpnoports(void)
{
  udpnoports++;
}

void ntcpip__snmpIncUdpinerrors(void)
{
  udpinerrors++;
}

void ntcpip__snmpIncUdpoutdatagrams(void)
{
  udpoutdatagrams++;
}

/**
 * Inserts udpTable indexes (.udpLocalAddress.udpLocalPort)
 * into index tree.
 */
void ntcpip__snmpInsertUdpidxTree(struct ntcpip__udpPcb *pcb)
{
  struct mib_list_rootnode *udp_rn;
  struct mib_list_node *udp_node;
  struct ntcpip_ipAddr ip;
  Int32 udpidx[5];
  Uint8 level;

  NTCPIP__LWIP_ASSERT("pcb != NULL", pcb != NULL);
  ip.addr = ntcpip_ntohl(pcb->local_ip.addr);
  ntcpip__snmpIptooid(&ip, &udpidx[0]);
  udpidx[4] = pcb->local_port;

  udp_rn = &udp_root;
  for (level = 0; level < 5; level++)
  {
    udp_node = NULL;
    ntcpip__snmpMibNodeInsert(udp_rn, udpidx[level], &udp_node);
    if ((level != 4) && (udp_node != NULL))
    {
      if (udp_node->nptr == NULL)
      {
        udp_rn = ntcpip__snmpMibLrnAlloc();
        udp_node->nptr = (struct mib_node*)udp_rn;
        if (udp_rn != NULL)
        {
          if (level == 3)
          {
            udp_rn->get_object_def = udpentry_get_object_def;
            udp_rn->get_value = udpentry_get_value;
            udp_rn->set_test = ntcpip_snmpNoleafsSetTest;
            udp_rn->set_value = ntcpip_snmpNoleafsSetValue;
          }
        }
        else
        {
          /* udp_rn == NULL, malloc failure */
          NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ntcpip__snmpInsertUdpidxTree() insert failed, mem full"));
          break;
        }
      }
      else
      {
        udp_rn = (struct mib_list_rootnode*)udp_node->nptr;
      }
    }
  }
  udptable.maxlength = 1;
}

/**
 * Removes udpTable indexes (.udpLocalAddress.udpLocalPort)
 * from index tree.
 */
void ntcpip__snmpDeleteUdpidxTree(struct ntcpip__udpPcb *pcb)
{
  struct mib_list_rootnode *udp_rn, *next, *del_rn[5];
  struct mib_list_node *udp_n, *del_n[5];
  struct ntcpip_ipAddr ip;
  Int32 udpidx[5];
  Uint8 bindings, fc, level, del_cnt;

  NTCPIP__LWIP_ASSERT("pcb != NULL", pcb != NULL);
  ip.addr = ntcpip_ntohl(pcb->local_ip.addr);
  ntcpip__snmpIptooid(&ip, &udpidx[0]);
  udpidx[4] = pcb->local_port;

  /* count PCBs for a given binding
     (e.g. when reusing ports or for temp output PCBs) */
  bindings = 0;
  pcb = ntcpip__udpPcbs;
  while ((pcb != NULL))
  {
    if ((pcb->local_ip.addr == ip.addr) &&
        (pcb->local_port == udpidx[4]))
    {
      bindings++;
    }
    pcb = pcb->next;
  }
  if (bindings == 1)
  {
    /* selectively remove */
    /* mark nodes for deletion */
    level = 0;
    del_cnt = 0;
    udp_rn = &udp_root;
    while ((level < 5) && (udp_rn != NULL))
    {
      fc = ntcpip__snmpMibNodeFind(udp_rn, udpidx[level], &udp_n);
      if (fc == 0)
      {
        /* udpidx[level] does not exist */
        del_cnt = 0;
        udp_rn = NULL;
      }
      else if (fc == 1)
      {
        del_rn[del_cnt] = udp_rn;
        del_n[del_cnt] = udp_n;
        del_cnt++;
        udp_rn = (struct mib_list_rootnode*)(udp_n->nptr);
      }
      else if (fc == 2)
      {
        /* reset delete (2 or more childs) */
        del_cnt = 0;
        udp_rn = (struct mib_list_rootnode*)(udp_n->nptr);
      }
      level++;
    }
    /* delete marked index nodes */
    while (del_cnt > 0)
    {
      del_cnt--;

      udp_rn = del_rn[del_cnt];
      udp_n = del_n[del_cnt];

      next = ntcpip__snmpMibNodeDelete(udp_rn, udp_n);
      if (next != NULL)
      {
        NTCPIP__LWIP_ASSERT("next_count == 0",next->count == 0);
        ntcpip__snmpMibLrnFree(next);
      }
    }
  }
  /* disable getnext traversal on empty table */
  if (udp_root.count == 0) udptable.maxlength = 0;
}


void ntcpip__snmpIncSnmpinpkts(void)
{
  snmpinpkts++;
}

void ntcpip__snmpIncSnmpoutpkts(void)
{
  snmpoutpkts++;
}

void ntcpip__snmpIncSnmpinbadversions(void)
{
  snmpinbadversions++;
}

void ntcpip__snmpIncSnmpinbadcommunitynames(void)
{
  snmpinbadcommunitynames++;
}

void ntcpip__snmpIncSnmpinbadcommunityuses(void)
{
  snmpinbadcommunityuses++;
}

void ntcpip__snmpIncSnmpinasnparseerrs(void)
{
  snmpinasnparseerrs++;
}

void ntcpip__snmpIncSnmpintoobigs(void)
{
  snmpintoobigs++;
}

void ntcpip__snmpIncSnmpinnosuchnames(void)
{
  snmpinnosuchnames++;
}

void ntcpip__snmpIncSnmpinbadvalues(void)
{
  snmpinbadvalues++;
}

void ntcpip__snmpIncSnmpinreadonlys(void)
{
  snmpinreadonlys++;
}

void ntcpip__snmpIncSnmpingenerrs(void)
{
  snmpingenerrs++;
}

void ntcpip__snmpAddSnmpintotalreqvars(Uint8 value)
{
  snmpintotalreqvars += value;
}

void ntcpip__snmpAddSnmpintotalsetvars(Uint8 value)
{
  snmpintotalsetvars += value;
}

void ntcpip__snmpIncSnmpingetrequests(void)
{
  snmpingetrequests++;
}

void ntcpip__snmpIncSnmpingetnexts(void)
{
  snmpingetnexts++;
}

void ntcpip__snmpIncSnmpinsetrequests(void)
{
  snmpinsetrequests++;
}

void ntcpip__snmpIncSnmpingetresponses(void)
{
  snmpingetresponses++;
}

void ntcpip__snmpIncSnmpintraps(void)
{
  snmpintraps++;
}

void ntcpip__snmpIncSnmpouttoobigs(void)
{
  snmpouttoobigs++;
}

void ntcpip__snmpIncSnmpoutnosuchnames(void)
{
  snmpoutnosuchnames++;
}

void ntcpip__snmpIncSnmpoutbadvalues(void)
{
  snmpoutbadvalues++;
}

void ntcpip__snmpIncSnmpoutgenerrs(void)
{
  snmpoutgenerrs++;
}

void ntcpip__snmpIncSnmpoutgetrequests(void)
{
  snmpoutgetrequests++;
}

void ntcpip__snmpIncSnmpoutgetnexts(void)
{
  snmpoutgetnexts++;
}

void ntcpip__snmpIncSnmpoutsetrequests(void)
{
  snmpoutsetrequests++;
}

void ntcpip__snmpIncSnmpoutgetresponses(void)
{
  snmpoutgetresponses++;
}

void ntcpip__snmpIncSnmpouttraps(void)
{
  snmpouttraps++;
}

void ntcpip__snmpGetSnmpgrpid_ptr(struct snmp_obj_id **oid)
{
  *oid = &snmpgrp_id;
}

void ntcpip__snmpSetSnmpenableauthentraps(Uint8 *value)
{
  if (value != NULL)
  {
    snmpenableauthentraps_ptr = value;
  }
}

void ntcpip__snmpGetSnmpenableauthentraps(Uint8 *value)
{
  *value = *snmpenableauthentraps_ptr;
}

void
ntcpip_snmpNoleafsGetObjectDef(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  NTCPIP__LWIP_UNUSED_ARG(ident_len);
  NTCPIP__LWIP_UNUSED_ARG(ident);
  od->instance = MIB_OBJECT_NONE;
}

void
ntcpip_snmpNoleafsGetValue(struct obj_def *od, Uint16 len, void *value)
{
  NTCPIP__LWIP_UNUSED_ARG(od);
  NTCPIP__LWIP_UNUSED_ARG(len);
  NTCPIP__LWIP_UNUSED_ARG(value);
}

Uint8
ntcpip_snmpNoleafsSetTest(struct obj_def *od, Uint16 len, void *value)
{
  NTCPIP__LWIP_UNUSED_ARG(od);
  NTCPIP__LWIP_UNUSED_ARG(len);
  NTCPIP__LWIP_UNUSED_ARG(value);
  /* can't set */
  return 0;
}

void
ntcpip_snmpNoleafsSetValue(struct obj_def *od, Uint16 len, void *value)
{
  NTCPIP__LWIP_UNUSED_ARG(od);
  NTCPIP__LWIP_UNUSED_ARG(len);
  NTCPIP__LWIP_UNUSED_ARG(value);
}


/**
 * Returns systems object definitions.
 *
 * @param ident_len the address length (2)
 * @param ident points to objectname.0 (object id trailer)
 * @param od points to object definition.
 */
static void
system_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  Uint8 id;

  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if (ident_len == 2)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("get_object_def system.%"U16_F".0\n",(Uint16)id));
    switch (id)
    {
      case 1: /* sysDescr */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
        od->v_len = snmpa_getRegSize(SNMPA_REGID_SYSDESCR); /* tietolaite */
        break;
      case 2: /* sysObjectID */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID);
        od->v_len = sysobjid.len * sizeof(Int32);
        break;
      case 3: /* sysUpTime */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_TIMETICKS);
        od->v_len = sizeof(Uint32);
        break;
      case 4: /* sysContact */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
        od->v_len = snmpa_getRegSize(SNMPA_REGID_CONTACT); /* tietolaite */
        break;
      case 5: /* sysName */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
        od->v_len = snmpa_getRegSize(SNMPA_REGID_NAME); /* tietolaite */
        break;
      case 6: /* sysLocation */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
        od->v_len = snmpa_getRegSize(SNMPA_REGID_LOCATION); /* tietolaite */
        break;
      case 7: /* sysServices */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("system_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    };
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("system_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

/**
 * Returns system object value.
 *
 * @param ident_len the address length (2)
 * @param ident points to objectname.0 (object id trailer)
 * @param len return value space (in bytes)
 * @param value points to (varbind) space to copy value into.
 */
static void
system_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id;

  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 1: /* sysDescr */
      snmpa_getRegValue(SNMPA_REGID_SYSDESCR, value, len); /* tietolaite */
      break;
    case 2: /* sysObjectID */
      ntcpip__snmpObjectidncpy((Int32*)value, (Int32*)sysobjid.id, (Uint8)(len / sizeof(Int32)));
      break;
    case 3: /* sysUpTime */
      {
        ntcpip__snmpGetSysuptime(value);
      }
      break;
    case 4: /* sysContact */
      snmpa_getRegValue(SNMPA_REGID_CONTACT, value, len);	/* tietolaite */
      break;
    case 5: /* sysName */
      snmpa_getRegValue(SNMPA_REGID_NAME, value, len);	/* tietolaite */
      break;
    case 6: /* sysLocation */
      snmpa_getRegValue(SNMPA_REGID_LOCATION, value, len);	/* tietolaite */
      break;
    case 7: /* sysServices */
      {
        Int32 *sint_ptr = value;
        *sint_ptr = sysservices;
      }
      break;
  };
}

static Uint8
system_set_test(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id, set_ok;

  NTCPIP__LWIP_UNUSED_ARG(value);
  set_ok = 0;
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 4: /* sysContact */
      if (len <= 20)
      {
        set_ok = 1;
      }
      break;
    case 5: /* sysName */
      if (len <= 20)
      {
        set_ok = 1;
      }
      break;
    case 6: /* sysLocation */
      if (len <= 30)
      {
        set_ok = 1;
      }
      break;
  };
  return set_ok;
}

static void
system_set_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id;

  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 4: /* sysContact */
      snmpa_setRegValue(SNMPA_REGID_CONTACT, value, len); /* Tietolaite */
      break;
    case 5: /* sysName */
      snmpa_setRegValue(SNMPA_REGID_NAME, value, len);	/* Tietolaite */
      break;
    case 6: /* sysLocation */
      snmpa_setRegValue(SNMPA_REGID_LOCATION, value, len);	/* Tietolaite */
      break;
  };
}

/**
 * Returns interfaces.ifnumber object definition.
 *
 * @param ident_len the address length (2)
 * @param ident points to objectname.index
 * @param od points to object definition.
 */
static void
interfaces_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if (ident_len == 2)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    od->instance = MIB_OBJECT_SCALAR;
    od->access = MIB_OBJECT_READ_ONLY;
    od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
    od->v_len = sizeof(Int32);
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("interfaces_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

/**
 * Returns interfaces.ifnumber object value.
 *
 * @param ident_len the address length (2)
 * @param ident points to objectname.0 (object id trailer)
 * @param len return value space (in bytes)
 * @param value points to (varbind) space to copy value into.
 */
static void
interfaces_get_value(struct obj_def *od, Uint16 len, void *value)
{
  NTCPIP__LWIP_UNUSED_ARG(len);
  if (od->id_inst_ptr[0] == 1)
  {
    Int32 *sint_ptr = value;
    *sint_ptr = iflist_root.count;
  }
}

/**
 * Returns ifentry object definitions.
 *
 * @param ident_len the address length (2)
 * @param ident points to objectname.index
 * @param od points to object definition.
 */
static void
ifentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  Uint8 id;

  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if (ident_len == 2)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("get_object_def ifentry.%"U16_F"\n",(Uint16)id));
    switch (id)
    {
      case 1: /* ifIndex */
      case 3: /* ifType */
      case 4: /* ifMtu */
      case 8: /* ifOperStatus */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 2: /* ifDescr */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
        /** @todo this should be some sort of sizeof(struct ntcpip__netif.name) */
        od->v_len = 2;
        break;
      case 5: /* ifSpeed */
      case 21: /* ifOutQLen */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_GAUGE);
        od->v_len = sizeof(Uint32);
        break;
      case 6: /* ifPhysAddress */
        {
          struct ntcpip__netif *netif;

          ntcpip__snmpIfindextonetif(ident[1], &netif);
          od->instance = MIB_OBJECT_TAB;
          od->access = MIB_OBJECT_READ_ONLY;
          od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
          od->v_len = netif->hwaddr_len;
        }
        break;
      case 7: /* ifAdminStatus */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 9: /* ifLastChange */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_TIMETICKS);
        od->v_len = sizeof(Uint32);
        break;
      case 10: /* ifInOctets */
      case 11: /* ifInUcastPkts */
      case 12: /* ifInNUcastPkts */
      case 13: /* ifInDiscarts */
      case 14: /* ifInErrors */
      case 15: /* ifInUnkownProtos */
      case 16: /* ifOutOctets */
      case 17: /* ifOutUcastPkts */
      case 18: /* ifOutNUcastPkts */
      case 19: /* ifOutDiscarts */
      case 20: /* ifOutErrors */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER);
        od->v_len = sizeof(Uint32);
        break;
      case 22: /* ifSpecific */
        /** @note returning zeroDotZero (0.0) no media specific MIB support */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID);
        od->v_len = ifspecific.len * sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ifentry_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    };
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ifentry_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

/**
 * Returns ifentry object value.
 *
 * @param ident_len the address length (2)
 * @param ident points to objectname.0 (object id trailer)
 * @param len return value space (in bytes)
 * @param value points to (varbind) space to copy value into.
 */
static void
ifentry_get_value(struct obj_def *od, Uint16 len, void *value)
{
  struct ntcpip__netif *netif;
  Uint8 id;

  ntcpip__snmpIfindextonetif(od->id_inst_ptr[1], &netif);
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 1: /* ifIndex */
      {
        Int32 *sint_ptr = value;
        *sint_ptr = od->id_inst_ptr[1];
      }
      break;
    case 2: /* ifDescr */
      ntcpip__snmpOcstrncpy(value,(Uint8*)netif->name,len);
      break;
    case 3: /* ifType */
      {
        Int32 *sint_ptr = value;
        *sint_ptr = netif->link_type;
      }
      break;
    case 4: /* ifMtu */
      {
        Int32 *sint_ptr = value;
        *sint_ptr = netif->mtu;
      }
      break;
    case 5: /* ifSpeed */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->link_speed;
      }
      break;
    case 6: /* ifPhysAddress */
      ntcpip__snmpOcstrncpy(value,netif->hwaddr,len);
      break;
    case 7: /* ifAdminStatus */
#if NTCPIP__LWIP_NETIF_LINK_CALLBACK
      {
        Int32 *sint_ptr = value;
        if (ntcpip__netifIsUp(netif))
        {
          if (ntcpip__netifIsLinkUp(netif))
          {
            *sint_ptr = 1; /* up */
          }
          else
          {
            *sint_ptr = 7; /* lowerLayerDown */
          }
        }
        else
        {
          *sint_ptr = 2; /* down */
        }
      }
      break;
#endif
    case 8: /* ifOperStatus */
      {
        Int32 *sint_ptr = value;
        if (ntcpip__netifIsUp(netif))
        {
          *sint_ptr = 1;
        }
        else
        {
          *sint_ptr = 2;
        }
      }
      break;
    case 9: /* ifLastChange */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ts;
      }
      break;
    case 10: /* ifInOctets */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifinoctets;
      }
      break;
    case 11: /* ifInUcastPkts */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifinucastpkts;
      }
      break;
    case 12: /* ifInNUcastPkts */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifinnucastpkts;
      }
      break;
    case 13: /* ifInDiscarts */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifindiscards;
      }
      break;
    case 14: /* ifInErrors */
    case 15: /* ifInUnkownProtos */
      /** @todo add these counters! */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = 0;
      }
      break;
    case 16: /* ifOutOctets */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifoutoctets;
      }
      break;
    case 17: /* ifOutUcastPkts */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifoutucastpkts;
      }
      break;
    case 18: /* ifOutNUcastPkts */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifoutnucastpkts;
      }
      break;
    case 19: /* ifOutDiscarts */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = netif->ifoutdiscards;
      }
      break;
    case 20: /* ifOutErrors */
       /** @todo add this counter! */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = 0;
      }
      break;
    case 21: /* ifOutQLen */
      /** @todo figure out if this must be 0 (no queue) or 1? */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = 0;
      }
      break;
    case 22: /* ifSpecific */
      ntcpip__snmpObjectidncpy((Int32*)value, (Int32*)ifspecific.id, (Uint8)(len / sizeof(Int32)));
      break;
  };
}

#if !NTCPIP__SNMP_SAFE_REQUESTS
static Uint8
ifentry_set_test (struct obj_def *od, Uint16 len, void *value)
{
  struct ntcpip__netif *netif;
  Uint8 id, set_ok;

  set_ok = 0;
  ntcpip__snmpIfindextonetif(od->id_inst_ptr[1], &netif);
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 7: /* ifAdminStatus */
      {
        Int32 *sint_ptr = value;
        if (*sint_ptr == 1 || *sint_ptr == 2)
          set_ok = 1;
      }
      break;
  }
  return set_ok;
}

static void
ifentry_set_value (struct obj_def *od, Uint16 len, void *value)
{
  struct ntcpip__netif *netif;
  Uint8 id;

  ntcpip__snmpIfindextonetif(od->id_inst_ptr[1], &netif);
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 7: /* ifAdminStatus */
      {
        Int32 *sint_ptr = value;
        if (*sint_ptr == 1)
        {
          ntcpip__netifSetUp(netif);
        }
        else if (*sint_ptr == 2)
        {
          ntcpip__netifSetDown(netif);
         }
      }
      break;
  }
}
#endif /* NTCPIP__SNMP_SAFE_REQUESTS */

/**
 * Returns atentry object definitions.
 *
 * @param ident_len the address length (6)
 * @param ident points to objectname.atifindex.atnetaddress
 * @param od points to object definition.
 */
static void
atentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (5) */
  ident_len += 5;
  ident -= 5;

  if (ident_len == 6)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    switch (ident[0])
    {
      case 1: /* atIfIndex */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 2: /* atPhysAddress */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
        od->v_len = 6; /** @todo try to use netif::hwaddr_len */
        break;
      case 3: /* atNetAddress */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR);
        od->v_len = 4;
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("atentry_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    }
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("atentry_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
atentry_get_value(struct obj_def *od, Uint16 len, void *value)
{
#if NTCPIP__LWIP_ARP
  Uint8 id;
  struct eth_addr* ethaddr_ret;
  struct ntcpip_ipAddr* ipaddr_ret;
#endif /* NTCPIP__LWIP_ARP */
  struct ntcpip_ipAddr ip;
  struct ntcpip__netif *netif;

  NTCPIP__LWIP_UNUSED_ARG(len);
  NTCPIP__LWIP_UNUSED_ARG(value);/* if !NTCPIP__LWIP_ARP */

  ntcpip__snmpIfindextonetif(od->id_inst_ptr[1], &netif);
  ntcpip__snmpOidtoip(&od->id_inst_ptr[2], &ip);
  ip.addr = ntcpip_htonl(ip.addr);

#if NTCPIP__LWIP_ARP /** @todo implement a netif_find_addr */
  if (ntcpip__etharpFindAddr(netif, &ip, &ethaddr_ret, &ipaddr_ret) > -1)
  {
    id = od->id_inst_ptr[0];
    switch (id)
    {
      case 1: /* atIfIndex */
        {
          Int32 *sint_ptr = value;
          *sint_ptr = od->id_inst_ptr[1];
        }
        break;
      case 2: /* atPhysAddress */
        {
          struct eth_addr *dst = value;

          *dst = *ethaddr_ret;
        }
        break;
      case 3: /* atNetAddress */
        {
          struct ntcpip_ipAddr *dst = value;

          *dst = *ipaddr_ret;
        }
        break;
    }
  }
#endif /* NTCPIP__LWIP_ARP */
}

static void
ip_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  Uint8 id;

  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if (ident_len == 2)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("get_object_def ip.%"U16_F".0\n",(Uint16)id));
    switch (id)
    {
      case 1: /* ipForwarding */
      case 2: /* ipDefaultTTL */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 3: /* ipInReceives */
      case 4: /* ipInHdrErrors */
      case 5: /* ipInAddrErrors */
      case 6: /* ipForwDatagrams */
      case 7: /* ipInUnknownProtos */
      case 8: /* ipInDiscards */
      case 9: /* ipInDelivers */
      case 10: /* ipOutRequests */
      case 11: /* ipOutDiscards */
      case 12: /* ipOutNoRoutes */
      case 14: /* ipReasmReqds */
      case 15: /* ipReasmOKs */
      case 16: /* ipReasmFails */
      case 17: /* ipFragOKs */
      case 18: /* ipFragFails */
      case 19: /* ipFragCreates */
      case 23: /* ipRoutingDiscards */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER);
        od->v_len = sizeof(Uint32);
        break;
      case 13: /* ipReasmTimeout */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    };
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
ip_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id;

  NTCPIP__LWIP_UNUSED_ARG(len);
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 1: /* ipForwarding */
      {
        Int32 *sint_ptr = value;
#if NTCPIP__IP_FORWARD
        /* forwarding */
        *sint_ptr = 1;
#else
        /* not-forwarding */
        *sint_ptr = 2;
#endif
      }
      break;
    case 2: /* ipDefaultTTL */
      {
        Int32 *sint_ptr = value;
        *sint_ptr = NTCPIP__IP_DEFAULT_TTL;
      }
      break;
    case 3: /* ipInReceives */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipinreceives;
      }
      break;
    case 4: /* ipInHdrErrors */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipinhdrerrors;
      }
      break;
    case 5: /* ipInAddrErrors */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipinaddrerrors;
      }
      break;
    case 6: /* ipForwDatagrams */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipforwdatagrams;
      }
      break;
    case 7: /* ipInUnknownProtos */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipinunknownprotos;
      }
      break;
    case 8: /* ipInDiscards */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipindiscards;
      }
      break;
    case 9: /* ipInDelivers */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipindelivers;
      }
      break;
    case 10: /* ipOutRequests */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipoutrequests;
      }
      break;
    case 11: /* ipOutDiscards */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipoutdiscards;
      }
      break;
    case 12: /* ipOutNoRoutes */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipoutnoroutes;
      }
      break;
    case 13: /* ipReasmTimeout */
      {
        Int32 *sint_ptr = value;
#if NTCPIP__IP_REASSEMBLY
        *sint_ptr = NTCPIP__IP_REASS_MAXAGE;
#else
        *sint_ptr = 0;
#endif
      }
      break;
    case 14: /* ipReasmReqds */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipreasmreqds;
      }
      break;
    case 15: /* ipReasmOKs */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipreasmoks;
      }
      break;
    case 16: /* ipReasmFails */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipreasmfails;
      }
      break;
    case 17: /* ipFragOKs */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipfragoks;
      }
      break;
    case 18: /* ipFragFails */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipfragfails;
      }
      break;
    case 19: /* ipFragCreates */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = ipfragcreates;
      }
      break;
    case 23: /* ipRoutingDiscards */
      /** @todo can lwIP discard routes at all?? hardwire this to 0?? */
      {
        Uint32 *uint_ptr = value;
        *uint_ptr = iproutingdiscards;
      }
      break;
  };
}

/**
 * Test ip object value before setting.
 *
 * @param od is the object definition
 * @param len return value space (in bytes)
 * @param value points to (varbind) space to copy value from.
 *
 * @note we allow set if the value matches the hardwired value,
 *   otherwise return badvalue.
 */
static Uint8
ip_set_test(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id, set_ok;
  Int32 *sint_ptr = value;

  NTCPIP__LWIP_UNUSED_ARG(len);
  set_ok = 0;
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 1: /* ipForwarding */
#if NTCPIP__IP_FORWARD
      /* forwarding */
      if (*sint_ptr == 1)
#else
      /* not-forwarding */
      if (*sint_ptr == 2)
#endif
      {
        set_ok = 1;
      }
      break;
    case 2: /* ipDefaultTTL */
      if (*sint_ptr == NTCPIP__IP_DEFAULT_TTL)
      {
        set_ok = 1;
      }
      break;
  };
  return set_ok;
}

static void
ip_addrentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (4) */
  ident_len += 4;
  ident -= 4;

  if (ident_len == 5)
  {
    Uint8 id;

    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    switch (id)
    {
      case 1: /* ipAdEntAddr */
      case 3: /* ipAdEntNetMask */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR);
        od->v_len = 4;
        break;
      case 2: /* ipAdEntIfIndex */
      case 4: /* ipAdEntBcastAddr */
      case 5: /* ipAdEntReasmMaxSize */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_addrentry_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    }
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_addrentry_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
ip_addrentry_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id;
  Uint16 ifidx;
  struct ntcpip_ipAddr ip;
  struct ntcpip__netif *netif = ntcpip__netifList;

  NTCPIP__LWIP_UNUSED_ARG(len);
  ntcpip__snmpOidtoip(&od->id_inst_ptr[1], &ip);
  ip.addr = ntcpip_htonl(ip.addr);
  ifidx = 0;
  while ((netif != NULL) && !ntcpip_ipaddrCmp(&ip, &netif->ip_addr))
  {
    netif = netif->next;
    ifidx++;
  }

  if (netif != NULL)
  {
    id = od->id_inst_ptr[0];
    switch (id)
    {
      case 1: /* ipAdEntAddr */
        {
          struct ntcpip_ipAddr *dst = value;
          *dst = netif->ip_addr;
        }
        break;
      case 2: /* ipAdEntIfIndex */
        {
          Int32 *sint_ptr = value;
          *sint_ptr = ifidx + 1;
        }
        break;
      case 3: /* ipAdEntNetMask */
        {
          struct ntcpip_ipAddr *dst = value;
          *dst = netif->netmask;
        }
        break;
      case 4: /* ipAdEntBcastAddr */
        {
          Int32 *sint_ptr = value;

          /* lwIP oddity, there's no broadcast
            address in the netif we can rely on */
          *sint_ptr = ntcpip__ipaddrBroadcast.addr & 1;
        }
        break;
      case 5: /* ipAdEntReasmMaxSize */
        {
          Int32 *sint_ptr = value;
#if NTCPIP__IP_REASSEMBLY
          /* @todo The theoretical maximum is NTCPIP__IP_REASS_MAX_PBUFS * size of the pbufs,
           * but only if receiving one fragmented packet at a time.
           * The current solution is to calculate for 2 simultaneous packets...
           */
          *sint_ptr = (IP_HLEN + ((NTCPIP__IP_REASS_MAX_PBUFS/2) *
            (NTCPIP__PBUF_POOL_BUFSIZE - NTCPIP__PBUF_LINK_HLEN - IP_HLEN)));
#else
          /** @todo returning MTU would be a bad thing and
             returning a wild guess like '576' isn't good either */
          *sint_ptr = 0;
#endif
        }
        break;
    }
  }
}

/**
 * @note
 * lwIP IP routing is currently using the network addresses in ntcpip__netifList.
 * if no suitable network IP is found in ntcpip__netifList, the default_netif is used.
 */
static void
ip_rteentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  Uint8 id;

  /* return to object name, adding index depth (4) */
  ident_len += 4;
  ident -= 4;

  if (ident_len == 5)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    switch (id)
    {
      case 1: /* ipRouteDest */
      case 7: /* ipRouteNextHop */
      case 11: /* ipRouteMask */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR);
        od->v_len = 4;
        break;
      case 2: /* ipRouteIfIndex */
      case 3: /* ipRouteMetric1 */
      case 4: /* ipRouteMetric2 */
      case 5: /* ipRouteMetric3 */
      case 6: /* ipRouteMetric4 */
      case 8: /* ipRouteType */
      case 10: /* ipRouteAge */
      case 12: /* ipRouteMetric5 */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 9: /* ipRouteProto */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 13: /* ipRouteInfo */
        /** @note returning zeroDotZero (0.0) no routing protocol specific MIB */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OBJ_ID);
        od->v_len = iprouteinfo.len * sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_rteentry_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    }
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_rteentry_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
ip_rteentry_get_value(struct obj_def *od, Uint16 len, void *value)
{
  struct ntcpip__netif *netif;
  struct ntcpip_ipAddr dest;
  Int32 *ident;
  Uint8 id;

  ident = od->id_inst_ptr;
  ntcpip__snmpOidtoip(&ident[1], &dest);
  dest.addr = ntcpip_htonl(dest.addr);

  if (dest.addr == 0)
  {
    /* ntcpip__ipRoute() uses default netif for default route */
    netif = ntcpip__netifDefault;
  }
  else
  {
    /* not using ntcpip__ipRoute(), need exact match! */
    netif = ntcpip__netifList;
    while ((netif != NULL) &&
            !ntcpip_ipaddrNetCmp(&dest, &(netif->ip_addr), &(netif->netmask)) )
    {
      netif = netif->next;
    }
  }
  if (netif != NULL)
  {
    id = ident[0];
    switch (id)
    {
      case 1: /* ipRouteDest */
        {
          struct ntcpip_ipAddr *dst = value;

          if (dest.addr == 0)
          {
            /* default rte has 0.0.0.0 dest */
            dst->addr = 0;
          }
          else
          {
            /* netifs have netaddress dest */
            dst->addr = netif->ip_addr.addr & netif->netmask.addr;
          }
        }
        break;
      case 2: /* ipRouteIfIndex */
        {
          Int32 *sint_ptr = value;

          ntcpip__snmpNetiftoifindex(netif, sint_ptr);
        }
        break;
      case 3: /* ipRouteMetric1 */
        {
          Int32 *sint_ptr = value;

          if (dest.addr == 0)
          {
            /* default rte has metric 1 */
            *sint_ptr = 1;
          }
          else
          {
            /* other rtes have metric 0 */
            *sint_ptr = 0;
          }
        }
        break;
      case 4: /* ipRouteMetric2 */
      case 5: /* ipRouteMetric3 */
      case 6: /* ipRouteMetric4 */
      case 12: /* ipRouteMetric5 */
        {
          Int32 *sint_ptr = value;
          /* not used */
          *sint_ptr = -1;
        }
        break;
      case 7: /* ipRouteNextHop */
        {
          struct ntcpip_ipAddr *dst = value;

          if (dest.addr == 0)
          {
            /* default rte: gateway */
            *dst = netif->gw;
          }
          else
          {
            /* other rtes: netif ip_addr  */
            *dst = netif->ip_addr;
          }
        }
        break;
      case 8: /* ipRouteType */
        {
          Int32 *sint_ptr = value;

          if (dest.addr == 0)
          {
            /* default rte is indirect */
            *sint_ptr = 4;
          }
          else
          {
            /* other rtes are direct */
            *sint_ptr = 3;
          }
        }
        break;
      case 9: /* ipRouteProto */
        {
          Int32 *sint_ptr = value;
          /* locally defined routes */
          *sint_ptr = 2;
        }
        break;
      case 10: /* ipRouteAge */
        {
          Int32 *sint_ptr = value;
          /** @todo (sysuptime - timestamp last change) / 100
              @see ntcpip__snmpInsertIprteidxTree() */
          *sint_ptr = 0;
        }
        break;
      case 11: /* ipRouteMask */
        {
          struct ntcpip_ipAddr *dst = value;

          if (dest.addr == 0)
          {
            /* default rte use 0.0.0.0 mask */
            dst->addr = 0;
          }
          else
          {
            /* other rtes use netmask */
            *dst = netif->netmask;
          }
        }
        break;
      case 13: /* ipRouteInfo */
        ntcpip__snmpObjectidncpy((Int32*)value, (Int32*)iprouteinfo.id, (Uint8)(len / sizeof(Int32)));
        break;
    }
  }
}

static void
ip_ntomentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (5) */
  ident_len += 5;
  ident -= 5;

  if (ident_len == 6)
  {
    Uint8 id;

    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    switch (id)
    {
      case 1: /* ipNetToMediaIfIndex */
      case 4: /* ipNetToMediaType */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 2: /* ipNetToMediaPhysAddress */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_OC_STR);
        od->v_len = 6; /** @todo try to use netif::hwaddr_len */
        break;
      case 3: /* ipNetToMediaNetAddress */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR);
        od->v_len = 4;
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_ntomentry_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    }
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("ip_ntomentry_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
ip_ntomentry_get_value(struct obj_def *od, Uint16 len, void *value)
{
#if NTCPIP__LWIP_ARP
  Uint8 id;
  struct eth_addr* ethaddr_ret;
  struct ntcpip_ipAddr* ipaddr_ret;
#endif /* NTCPIP__LWIP_ARP */
  struct ntcpip_ipAddr ip;
  struct ntcpip__netif *netif;

  NTCPIP__LWIP_UNUSED_ARG(len);
  NTCPIP__LWIP_UNUSED_ARG(value);/* if !NTCPIP__LWIP_ARP */

  ntcpip__snmpIfindextonetif(od->id_inst_ptr[1], &netif);
  ntcpip__snmpOidtoip(&od->id_inst_ptr[2], &ip);
  ip.addr = ntcpip_htonl(ip.addr);

#if NTCPIP__LWIP_ARP /** @todo implement a netif_find_addr */
  if (ntcpip__etharpFindAddr(netif, &ip, &ethaddr_ret, &ipaddr_ret) > -1)
  {
    id = od->id_inst_ptr[0];
    switch (id)
    {
      case 1: /* ipNetToMediaIfIndex */
        {
          Int32 *sint_ptr = value;
          *sint_ptr = od->id_inst_ptr[1];
        }
        break;
      case 2: /* ipNetToMediaPhysAddress */
        {
          struct eth_addr *dst = value;

          *dst = *ethaddr_ret;
        }
        break;
      case 3: /* ipNetToMediaNetAddress */
        {
          struct ntcpip_ipAddr *dst = value;

          *dst = *ipaddr_ret;
        }
        break;
      case 4: /* ipNetToMediaType */
        {
          Int32 *sint_ptr = value;
          /* dynamic (?) */
          *sint_ptr = 3;
        }
        break;
    }
  }
#endif /* NTCPIP__LWIP_ARP */
}

static void
icmp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if ((ident_len == 2) &&
      (ident[0] > 0) && (ident[0] < 27))
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    od->instance = MIB_OBJECT_SCALAR;
    od->access = MIB_OBJECT_READ_ONLY;
    od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER);
    od->v_len = sizeof(Uint32);
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("icmp_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
icmp_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint32 *uint_ptr = value;
  Uint8 id;

  NTCPIP__LWIP_UNUSED_ARG(len);
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 1: /* icmpInMsgs */
      *uint_ptr = icmpinmsgs;
      break;
    case 2: /* icmpInErrors */
      *uint_ptr = icmpinerrors;
      break;
    case 3: /* icmpInDestUnreachs */
      *uint_ptr = icmpindestunreachs;
      break;
    case 4: /* icmpInTimeExcds */
      *uint_ptr = icmpintimeexcds;
      break;
    case 5: /* icmpInParmProbs */
      *uint_ptr = icmpinparmprobs;
      break;
    case 6: /* icmpInSrcQuenchs */
      *uint_ptr = icmpinsrcquenchs;
      break;
    case 7: /* icmpInRedirects */
      *uint_ptr = icmpinredirects;
      break;
    case 8: /* icmpInEchos */
      *uint_ptr = icmpinechos;
      break;
    case 9: /* icmpInEchoReps */
      *uint_ptr = icmpinechoreps;
      break;
    case 10: /* icmpInTimestamps */
      *uint_ptr = icmpintimestamps;
      break;
    case 11: /* icmpInTimestampReps */
      *uint_ptr = icmpintimestampreps;
      break;
    case 12: /* icmpInAddrMasks */
      *uint_ptr = icmpinaddrmasks;
      break;
    case 13: /* icmpInAddrMaskReps */
      *uint_ptr = icmpinaddrmaskreps;
      break;
    case 14: /* icmpOutMsgs */
      *uint_ptr = icmpoutmsgs;
      break;
    case 15: /* icmpOutErrors */
      *uint_ptr = icmpouterrors;
      break;
    case 16: /* icmpOutDestUnreachs */
      *uint_ptr = icmpoutdestunreachs;
      break;
    case 17: /* icmpOutTimeExcds */
      *uint_ptr = icmpouttimeexcds;
      break;
    case 18: /* icmpOutParmProbs */
      *uint_ptr = icmpoutparmprobs;
      break;
    case 19: /* icmpOutSrcQuenchs */
      *uint_ptr = icmpoutsrcquenchs;
      break;
    case 20: /* icmpOutRedirects */
      *uint_ptr = icmpoutredirects;
      break;
    case 21: /* icmpOutEchos */
      *uint_ptr = icmpoutechos;
      break;
    case 22: /* icmpOutEchoReps */
      *uint_ptr = icmpoutechoreps;
      break;
    case 23: /* icmpOutTimestamps */
      *uint_ptr = icmpouttimestamps;
      break;
    case 24: /* icmpOutTimestampReps */
      *uint_ptr = icmpouttimestampreps;
      break;
    case 25: /* icmpOutAddrMasks */
      *uint_ptr = icmpoutaddrmasks;
      break;
    case 26: /* icmpOutAddrMaskReps */
      *uint_ptr = icmpoutaddrmaskreps;
      break;
  }
}

#if NTCPIP__LWIP_TCP
/** @todo tcp grp */
static void
tcp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  Uint8 id;

  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if (ident_len == 2)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("get_object_def tcp.%"U16_F".0\n",(Uint16)id));

    switch (id)
    {
      case 1: /* tcpRtoAlgorithm */
      case 2: /* tcpRtoMin */
      case 3: /* tcpRtoMax */
      case 4: /* tcpMaxConn */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 5: /* tcpActiveOpens */
      case 6: /* tcpPassiveOpens */
      case 7: /* tcpAttemptFails */
      case 8: /* tcpEstabResets */
      case 10: /* tcpInSegs */
      case 11: /* tcpOutSegs */
      case 12: /* tcpRetransSegs */
      case 14: /* tcpInErrs */
      case 15: /* tcpOutRsts */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER);
        od->v_len = sizeof(Uint32);
        break;
      case 9: /* tcpCurrEstab */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_GAUGE);
        od->v_len = sizeof(Uint32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("tcp_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    };
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("tcp_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
tcp_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint32 *uint_ptr = value;
  Int32 *sint_ptr = value;
  Uint8 id;

  NTCPIP__LWIP_UNUSED_ARG(len);
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 1: /* tcpRtoAlgorithm, vanj(4) */
      *sint_ptr = 4;
      break;
    case 2: /* tcpRtoMin */
      /* @todo not the actual value, a guess,
          needs to be calculated */
      *sint_ptr = 1000;
      break;
    case 3: /* tcpRtoMax */
      /* @todo not the actual value, a guess,
         needs to be calculated */
      *sint_ptr = 60000;
      break;
    case 4: /* tcpMaxConn */
      *sint_ptr = NTCPIP__MEMP_NUM_TCP_PCB;
      break;
    case 5: /* tcpActiveOpens */
      *uint_ptr = tcpactiveopens;
      break;
    case 6: /* tcpPassiveOpens */
      *uint_ptr = tcppassiveopens;
      break;
    case 7: /* tcpAttemptFails */
      *uint_ptr = tcpattemptfails;
      break;
    case 8: /* tcpEstabResets */
      *uint_ptr = tcpestabresets;
      break;
    case 9: /* tcpCurrEstab */
      {
        Uint16 tcpcurrestab = 0;
        struct ntcpip__tcpPcb *pcb = ntcpip__tcpActivePcbs;
        while (pcb != NULL)
        {
          if ((pcb->state == ESTABLISHED) ||
              (pcb->state == CLOSE_WAIT))
          {
            tcpcurrestab++;
          }
          pcb = pcb->next;
        }
        *uint_ptr = tcpcurrestab;
      }
      break;
    case 10: /* tcpInSegs */
      *uint_ptr = tcpinsegs;
      break;
    case 11: /* tcpOutSegs */
      *uint_ptr = tcpoutsegs;
      break;
    case 12: /* tcpRetransSegs */
      *uint_ptr = tcpretranssegs;
      break;
    case 14: /* tcpInErrs */
      *uint_ptr = tcpinerrs;
      break;
    case 15: /* tcpOutRsts */
      *uint_ptr = tcpoutrsts;
      break;
  }
}
#ifdef THIS_SEEMS_UNUSED
static void
tcpconnentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (10) */
  ident_len += 10;
  ident -= 10;

  if (ident_len == 11)
  {
    Uint8 id;

    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("get_object_def tcp.%"U16_F".0\n",(Uint16)id));

    switch (id)
    {
      case 1: /* tcpConnState */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      case 2: /* tcpConnLocalAddress */
      case 4: /* tcpConnRemAddress */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR);
        od->v_len = 4;
        break;
      case 3: /* tcpConnLocalPort */
      case 5: /* tcpConnRemPort */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("tcpconnentry_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    };
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("tcpconnentry_get_object_def: no such object\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
tcpconnentry_get_value(struct obj_def *od, Uint16 len, void *value)
{
  struct ntcpip_ipAddr lip, rip;
  Uint16 lport, rport;
  Int32 *ident;

  ident = od->id_inst_ptr;
  ntcpip__snmpOidtoip(&ident[1], &lip);
  lip.addr = ntcpip_htonl(lip.addr);
  lport = ident[5];
  ntcpip__snmpOidtoip(&ident[6], &rip);
  rip.addr = ntcpip_htonl(rip.addr);
  rport = ident[10];

  /** @todo find matching PCB */
}
#endif /* if 0 */
#endif

static void
udp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if ((ident_len == 2) &&
      (ident[0] > 0) && (ident[0] < 6))
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    od->instance = MIB_OBJECT_SCALAR;
    od->access = MIB_OBJECT_READ_ONLY;
    od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER);
    od->v_len = sizeof(Uint32);
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("udp_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
udp_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint32 *uint_ptr = value;
  Uint8 id;

  NTCPIP__LWIP_UNUSED_ARG(len);
  id = od->id_inst_ptr[0];
  switch (id)
  {
    case 1: /* udpInDatagrams */
      *uint_ptr = udpindatagrams;
      break;
    case 2: /* udpNoPorts */
      *uint_ptr = udpnoports;
      break;
    case 3: /* udpInErrors */
      *uint_ptr = udpinerrors;
      break;
    case 4: /* udpOutDatagrams */
      *uint_ptr = udpoutdatagrams;
      break;
  }
}

static void
udpentry_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (5) */
  ident_len += 5;
  ident -= 5;

  if (ident_len == 6)
  {
    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    switch (ident[0])
    {
      case 1: /* udpLocalAddress */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_IPADDR);
        od->v_len = 4;
        break;
      case 2: /* udpLocalPort */
        od->instance = MIB_OBJECT_TAB;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("udpentry_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    }
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("udpentry_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
udpentry_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id;
  struct ntcpip__udpPcb *pcb;
  struct ntcpip_ipAddr ip;
  Uint16 port;

  NTCPIP__LWIP_UNUSED_ARG(len);
  ntcpip__snmpOidtoip(&od->id_inst_ptr[1], &ip);
  ip.addr = ntcpip_htonl(ip.addr);
  port = od->id_inst_ptr[5];

  pcb = ntcpip__udpPcbs;
  while ((pcb != NULL) &&
         !((pcb->local_ip.addr == ip.addr) &&
           (pcb->local_port == port)))
  {
    pcb = pcb->next;
  }

  if (pcb != NULL)
  {
    id = od->id_inst_ptr[0];
    switch (id)
    {
      case 1: /* udpLocalAddress */
        {
          struct ntcpip_ipAddr *dst = value;
          *dst = pcb->local_ip;
        }
        break;
      case 2: /* udpLocalPort */
        {
          Int32 *sint_ptr = value;
          *sint_ptr = pcb->local_port;
        }
        break;
    }
  }
}

static void
snmp_get_object_def(Uint8 ident_len, Int32 *ident, struct obj_def *od)
{
  /* return to object name, adding index depth (1) */
  ident_len += 1;
  ident -= 1;
  if (ident_len == 2)
  {
    Uint8 id;

    od->id_inst_len = ident_len;
    od->id_inst_ptr = ident;

    id = ident[0];
    switch (id)
    {
      case 1: /* snmpInPkts */
      case 2: /* snmpOutPkts */
      case 3: /* snmpInBadVersions */
      case 4: /* snmpInBadCommunityNames */
      case 5: /* snmpInBadCommunityUses */
      case 6: /* snmpInASNParseErrs */
      case 8: /* snmpInTooBigs */
      case 9: /* snmpInNoSuchNames */
      case 10: /* snmpInBadValues */
      case 11: /* snmpInReadOnlys */
      case 12: /* snmpInGenErrs */
      case 13: /* snmpInTotalReqVars */
      case 14: /* snmpInTotalSetVars */
      case 15: /* snmpInGetRequests */
      case 16: /* snmpInGetNexts */
      case 17: /* snmpInSetRequests */
      case 18: /* snmpInGetResponses */
      case 19: /* snmpInTraps */
      case 20: /* snmpOutTooBigs */
      case 21: /* snmpOutNoSuchNames */
      case 22: /* snmpOutBadValues */
      case 24: /* snmpOutGenErrs */
      case 25: /* snmpOutGetRequests */
      case 26: /* snmpOutGetNexts */
      case 27: /* snmpOutSetRequests */
      case 28: /* snmpOutGetResponses */
      case 29: /* snmpOutTraps */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_ONLY;
        od->asn_type = (SNMP_ASN1_APPLIC | SNMP_ASN1_PRIMIT | SNMP_ASN1_COUNTER);
        od->v_len = sizeof(Uint32);
        break;
      case 30: /* snmpEnableAuthenTraps */
        od->instance = MIB_OBJECT_SCALAR;
        od->access = MIB_OBJECT_READ_WRITE;
        od->asn_type = (SNMP_ASN1_UNIV | SNMP_ASN1_PRIMIT | SNMP_ASN1_INTEG);
        od->v_len = sizeof(Int32);
        break;
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("snmp_get_object_def: no such object\n"));
        od->instance = MIB_OBJECT_NONE;
        break;
    };
  }
  else
  {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SNMP_MIB_DEBUG,("snmp_get_object_def: no scalar\n"));
    od->instance = MIB_OBJECT_NONE;
  }
}

static void
snmp_get_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint32 *uint_ptr = value;
  Uint8 id;

  NTCPIP__LWIP_UNUSED_ARG(len);
  id = od->id_inst_ptr[0];
  switch (id)
  {
      case 1: /* snmpInPkts */
        *uint_ptr = snmpinpkts;
        break;
      case 2: /* snmpOutPkts */
        *uint_ptr = snmpoutpkts;
        break;
      case 3: /* snmpInBadVersions */
        *uint_ptr = snmpinbadversions;
        break;
      case 4: /* snmpInBadCommunityNames */
        *uint_ptr = snmpinbadcommunitynames;
        break;
      case 5: /* snmpInBadCommunityUses */
        *uint_ptr = snmpinbadcommunityuses;
        break;
      case 6: /* snmpInASNParseErrs */
        *uint_ptr = snmpinasnparseerrs;
        break;
      case 8: /* snmpInTooBigs */
        *uint_ptr = snmpintoobigs;
        break;
      case 9: /* snmpInNoSuchNames */
        *uint_ptr = snmpinnosuchnames;
        break;
      case 10: /* snmpInBadValues */
        *uint_ptr = snmpinbadvalues;
        break;
      case 11: /* snmpInReadOnlys */
        *uint_ptr = snmpinreadonlys;
        break;
      case 12: /* snmpInGenErrs */
        *uint_ptr = snmpingenerrs;
        break;
      case 13: /* snmpInTotalReqVars */
        *uint_ptr = snmpintotalreqvars;
        break;
      case 14: /* snmpInTotalSetVars */
        *uint_ptr = snmpintotalsetvars;
        break;
      case 15: /* snmpInGetRequests */
        *uint_ptr = snmpingetrequests;
        break;
      case 16: /* snmpInGetNexts */
        *uint_ptr = snmpingetnexts;
        break;
      case 17: /* snmpInSetRequests */
        *uint_ptr = snmpinsetrequests;
        break;
      case 18: /* snmpInGetResponses */
        *uint_ptr = snmpingetresponses;
        break;
      case 19: /* snmpInTraps */
        *uint_ptr = snmpintraps;
        break;
      case 20: /* snmpOutTooBigs */
        *uint_ptr = snmpouttoobigs;
        break;
      case 21: /* snmpOutNoSuchNames */
        *uint_ptr = snmpoutnosuchnames;
        break;
      case 22: /* snmpOutBadValues */
        *uint_ptr = snmpoutbadvalues;
        break;
      case 24: /* snmpOutGenErrs */
        *uint_ptr = snmpoutgenerrs;
        break;
      case 25: /* snmpOutGetRequests */
        *uint_ptr = snmpoutgetrequests;
        break;
      case 26: /* snmpOutGetNexts */
        *uint_ptr = snmpoutgetnexts;
        break;
      case 27: /* snmpOutSetRequests */
        *uint_ptr = snmpoutsetrequests;
        break;
      case 28: /* snmpOutGetResponses */
        *uint_ptr = snmpoutgetresponses;
        break;
      case 29: /* snmpOutTraps */
        *uint_ptr = snmpouttraps;
        break;
      case 30: /* snmpEnableAuthenTraps */
        *uint_ptr = *snmpenableauthentraps_ptr;
        break;
  };
}

/**
 * Test snmp object value before setting.
 *
 * @param od is the object definition
 * @param len return value space (in bytes)
 * @param value points to (varbind) space to copy value from.
 */
static Uint8
snmp_set_test(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id, set_ok;

  NTCPIP__LWIP_UNUSED_ARG(len);
  set_ok = 0;
  id = od->id_inst_ptr[0];
  if (id == 30)
  {
    /* snmpEnableAuthenTraps */
    Int32 *sint_ptr = value;

    if (snmpenableauthentraps_ptr != &snmpenableauthentraps_default)
    {
      /* we should have writable non-volatile mem here */
      if ((*sint_ptr == 1) || (*sint_ptr == 2))
      {
        set_ok = 1;
      }
    }
    else
    {
      /* const or hardwired value */
      if (*sint_ptr == snmpenableauthentraps_default)
      {
        set_ok = 1;
      }
    }
  }
  return set_ok;
}

static void
snmp_set_value(struct obj_def *od, Uint16 len, void *value)
{
  Uint8 id;

  NTCPIP__LWIP_UNUSED_ARG(len);
  id = od->id_inst_ptr[0];
  if (id == 30)
  {
    /* snmpEnableAuthenTraps */
    Int32 *sint_ptr = value;
    *snmpenableauthentraps_ptr = *sint_ptr;
  }
}

#endif /* NTCPIP__LWIP_SNMP */


