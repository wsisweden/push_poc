/**
 * @file
 * User Datagram Protocol module
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */


/* udp.c
 *
 * The code for the User Datagram Protocol UDP & UDPLite (RFC 3828).
 *
 */

/* @todo Check the use of '(struct ntcpip__udpPcb).chksum_len_rx'!
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_UDP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/udp.h"
#include "../def.h"
#include "../memp.h"
#include "ntcpip/inet.h"
#include "../inet_chksum.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "../icmp.h"
#include "../stats.h"
#include "ntcpip/snmp.h"
#include "../../arch/perf.h"
#include "../dhcp.h"

#include <string.h>

/* The list of UDP PCBs */
/* exported in udp.h (was static) */
struct ntcpip__udpPcb *ntcpip__udpPcbs;

/**
 * Process an incoming UDP datagram.
 *
 * Given an incoming UDP datagram (as a chain of pbufs) this function
 * finds a corresponding UDP PCB and hands over the pbuf to the pcbs
 * recv function. If no pcb is found or the datagram is incorrect, the
 * pbuf is freed.
 *
 * @param p pbuf to be demultiplexed to a UDP PCB.
 * @param inp network interface on which the datagram was received.
 *
 */
void
ntcpip__udpInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp)
{
  struct udp_hdr *udphdr;
  struct ntcpip__udpPcb *pcb, *prev;
  struct ntcpip__udpPcb *uncon_pcb;
  struct ip_hdr *iphdr;
  Uint16 src, dest;
  Uint8 local_match;
  Uint8 broadcast;

  NTCPIP__PERF_START;

  UDP_STATS_INC(udp.recv);

  iphdr = p->payload;

  /* Check minimum length (IP header + UDP header)
   * and move payload pointer to UDP header */
  if (p->tot_len < (IPH_HL(iphdr) * 4 + UDP_HLEN) || ntcpip__pbufHeader(p, -(Int16)(IPH_HL(iphdr) * 4))) {
    /* drop short packets */
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG,
                ("ntcpip__udpInput: short UDP datagram (%"U16_F" bytes) discarded\n", p->tot_len));
    UDP_STATS_INC(udp.lenerr);
    UDP_STATS_INC(udp.drop);
    ntcpip__snmpIncUdpinerrors();
    ntcpip_pbufFree(p);
    goto end;
  }

  udphdr = (struct udp_hdr *)p->payload;

  /* is broadcast packet ? */
  broadcast = ntcpip__ipaddrIsBroadcast(&(iphdr->dest), inp);

  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip__udpInput: received datagram of length %"U16_F"\n", p->tot_len));

  /* convert src and dest ports to host byte order */
  src = ntcpip_ntohs(udphdr->src);
  dest = ntcpip_ntohs(udphdr->dest);

  udp_debug_print(udphdr);

  /* print the UDP source and destination */
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG,
              ("udp (%"U16_F".%"U16_F".%"U16_F".%"U16_F", %"U16_F") <-- "
               "(%"U16_F".%"U16_F".%"U16_F".%"U16_F", %"U16_F")\n",
               ntcpip_ipaddr1(&iphdr->dest), ntcpip_ipaddr2(&iphdr->dest),
               ntcpip_ipaddr3(&iphdr->dest), ntcpip_ipaddr4(&iphdr->dest), ntcpip_ntohs(udphdr->dest),
               ntcpip_ipaddr1(&iphdr->src), ntcpip_ipaddr2(&iphdr->src),
               ntcpip_ipaddr3(&iphdr->src), ntcpip_ipaddr4(&iphdr->src), ntcpip_ntohs(udphdr->src)));

#if NTCPIP__LWIP_DHCP
  pcb = NULL;
  /* when NTCPIP__LWIP_DHCP is active, packets to DHCP_CLIENT_PORT may only be processed by
     the dhcp module, no other UDP pcb may use the local UDP port DHCP_CLIENT_PORT */
  if (dest == DHCP_CLIENT_PORT) {
    /* all packets for DHCP_CLIENT_PORT not coming from DHCP_SERVER_PORT are dropped! */
    if (src == DHCP_SERVER_PORT) {
      if ((inp->dhcp != NULL) && (inp->dhcp->pcb != NULL)) {
        /* accept the packe if 
           (- broadcast or directed to us) -> DHCP is link-layer-addressed, local ip is always ANY!
           - inp->dhcp->pcb->remote == ANY or iphdr->src */
        if ((ntcpip_ipaddrIsAny(&inp->dhcp->pcb->remote_ip) ||
           ntcpip_ipaddrCmp(&(inp->dhcp->pcb->remote_ip), &(iphdr->src)))) {
          pcb = inp->dhcp->pcb;
        }
      }
    }
  } else
#endif /* NTCPIP__LWIP_DHCP */
  {
    prev = NULL;
    local_match = 0;
    uncon_pcb = NULL;
    /* Iterate through the UDP pcb list for a matching pcb.
     * 'Perfect match' pcbs (connected to the remote port & ip address) are
     * preferred. If no perfect match is found, the first unconnected pcb that
     * matches the local port and ip address gets the datagram. */
    for (pcb = ntcpip__udpPcbs; pcb != NULL; pcb = pcb->next) {
      local_match = 0;
      /* print the PCB local and remote address */
      NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG,
                  ("pcb (%"U16_F".%"U16_F".%"U16_F".%"U16_F", %"U16_F") --- "
                   "(%"U16_F".%"U16_F".%"U16_F".%"U16_F", %"U16_F")\n",
                   ntcpip_ipaddr1(&pcb->local_ip), ntcpip_ipaddr2(&pcb->local_ip),
                   ntcpip_ipaddr3(&pcb->local_ip), ntcpip_ipaddr4(&pcb->local_ip), pcb->local_port,
                   ntcpip_ipaddr1(&pcb->remote_ip), ntcpip_ipaddr2(&pcb->remote_ip),
                   ntcpip_ipaddr3(&pcb->remote_ip), ntcpip_ipaddr4(&pcb->remote_ip), pcb->remote_port));

      /* compare PCB local addr+port to UDP destination addr+port */
      if ((pcb->local_port == dest) &&
          ((!broadcast && ntcpip_ipaddrIsAny(&pcb->local_ip)) ||
           ntcpip_ipaddrCmp(&(pcb->local_ip), &(iphdr->dest)) ||
#if NTCPIP__LWIP_IGMP
           ntcpip_ipaddrIsMulticast(&(iphdr->dest)) ||
#endif /* NTCPIP__LWIP_IGMP */
#if IP_SOF_BROADCAST_RECV
           (broadcast && (pcb->so_options & NTCPIP_SOF_BROADCAST)))) {
#else  /* IP_SOF_BROADCAST_RECV */
           (broadcast))) {
#endif /* IP_SOF_BROADCAST_RECV */
        local_match = 1;
        if ((uncon_pcb == NULL) && 
            ((pcb->flags & UDP_FLAGS_CONNECTED) == 0)) {
          /* the first unconnected matching PCB */
          uncon_pcb = pcb;
        }
      }
      /* compare PCB remote addr+port to UDP source addr+port */
      if ((local_match != 0) &&
          (pcb->remote_port == src) &&
          (ntcpip_ipaddrIsAny(&pcb->remote_ip) ||
           ntcpip_ipaddrCmp(&(pcb->remote_ip), &(iphdr->src)))) {
        /* the first fully matching PCB */
        if (prev != NULL) {
          /* move the pcb to the front of ntcpip__udpPcbs so that is
             found faster next time */
          prev->next = pcb->next;
          pcb->next = ntcpip__udpPcbs;
          ntcpip__udpPcbs = pcb;
        } else {
          UDP_STATS_INC(udp.cachehit);
        }
        break;
      }
      prev = pcb;
    }
    /* no fully matching pcb found? then look for an unconnected pcb */
    if (pcb == NULL) {
      pcb = uncon_pcb;
    }
  }

  /* Check checksum if this is a match or if it was directed at us. */
  if (pcb != NULL || ntcpip_ipaddrCmp(&inp->ip_addr, &iphdr->dest)) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__udpInput: calculating checksum\n"));
#if NTCPIP__LWIP_UDPLITE
    if (IPH_PROTO(iphdr) == IP_PROTO_UDPLITE) {
      /* Do the UDP Lite checksum */
#if NTCPIP__CHECKSUM_CHECK_UDP
      Uint16 chklen = ntcpip_ntohs(udphdr->len);
      if (chklen < sizeof(struct udp_hdr)) {
        if (chklen == 0) {
          /* For UDP-Lite, checksum length of 0 means checksum
             over the complete packet (See RFC 3828 chap. 3.1) */
          chklen = p->tot_len;
        } else {
          /* At least the UDP-Lite header must be covered by the
             checksum! (Again, see RFC 3828 chap. 3.1) */
          UDP_STATS_INC(udp.chkerr);
          UDP_STATS_INC(udp.drop);
          ntcpip__snmpIncUdpinerrors();
          ntcpip_pbufFree(p);
          goto end;
        }
      }
      if (ntcpip__inetChksumPseudoPartial(p, (struct ntcpip_ipAddr *)&(iphdr->src),
                             (struct ntcpip_ipAddr *)&(iphdr->dest),
                             IP_PROTO_UDPLITE, p->tot_len, chklen) != 0) {
       NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
                   ("ntcpip__udpInput: UDP Lite datagram discarded due to failing checksum\n"));
        UDP_STATS_INC(udp.chkerr);
        UDP_STATS_INC(udp.drop);
        ntcpip__snmpIncUdpinerrors();
        ntcpip_pbufFree(p);
        goto end;
      }
#endif /* NTCPIP__CHECKSUM_CHECK_UDP */
    } else
#endif /* NTCPIP__LWIP_UDPLITE */
    {
#if NTCPIP__CHECKSUM_CHECK_UDP
      if (udphdr->chksum != 0) {
        if (ntcpip__inetChksumPseudo(p, (struct ntcpip_ipAddr *)&(iphdr->src),
                               (struct ntcpip_ipAddr *)&(iphdr->dest),
                               IP_PROTO_UDP, p->tot_len) != 0) {
          NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
                      ("ntcpip__udpInput: UDP datagram discarded due to failing checksum\n"));
          UDP_STATS_INC(udp.chkerr);
          UDP_STATS_INC(udp.drop);
          ntcpip__snmpIncUdpinerrors();
          ntcpip_pbufFree(p);
          goto end;
        }
      }
#endif /* NTCPIP__CHECKSUM_CHECK_UDP */
    }
    if(ntcpip__pbufHeader(p, -UDP_HLEN)) {
      /* Can we cope with this failing? Just assert for now */
      NTCPIP__LWIP_ASSERT("ntcpip__pbufHeader failed\n", 0);
      UDP_STATS_INC(udp.drop);
      ntcpip__snmpIncUdpinerrors();
      ntcpip_pbufFree(p);
      goto end;
    }
    if (pcb != NULL) {
      ntcpip__snmpInc_udpindatagrams();
      /* callback */
      if (pcb->recv != NULL) {
        /* now the recv function is responsible for freeing p */
        pcb->recv(pcb->recv_arg, pcb, p, &iphdr->src, src);
      } else {
        /* no recv function registered? then we have to free the pbuf! */
        ntcpip_pbufFree(p);
        goto end;
      }
    } else {
      NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__udpInput: not for us.\n"));

#if NTCPIP__LWIP_ICMP
      /* No match was found, send ICMP destination port unreachable unless
         destination address was broadcast/multicast. */
      if (!broadcast &&
          !ntcpip_ipaddrIsMulticast(&iphdr->dest)) {
        /* move payload pointer back to ip header */
        ntcpip__pbufHeader(p, (IPH_HL(iphdr) * 4) + UDP_HLEN);
        NTCPIP__LWIP_ASSERT("p->payload == iphdr", (p->payload == iphdr));
        ntcpip__icmpDestUnreach(p, ICMP_DUR_PORT);
      }
#endif /* NTCPIP__LWIP_ICMP */
      UDP_STATS_INC(udp.proterr);
      UDP_STATS_INC(udp.drop);
      ntcpip__snmpIncUdpnoports();
      ntcpip_pbufFree(p);
    }
  } else {
    ntcpip_pbufFree(p);
  }
end:
  NTCPIP__PERF_STOP("ntcpip__udpInput");
}

/**
 * Send data using UDP.
 *
 * @param pcb UDP PCB used to send the data.
 * @param p chain of pbuf's to be sent.
 *
 * The datagram will be sent to the current remote_ip & remote_port
 * stored in pcb. If the pcb is not bound to a port, it will
 * automatically be bound to a random port.
 *
 * @return lwIP error code.
 * - NTCPIP_ERR_OK. Successful. No error occured.
 * - NTCPIP_ERR_MEM. Out of memory.
 * - NTCPIP_ERR_RTE. Could not find route to destination address.
 * - More errors could be returned by lower protocol layers.
 *
 * @see ntcpip_udpDisconnect() ntcpip__udpSendTo()
 */
ntcpip_Err
ntcpip_udpSend(struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p)
{
  /* send to the packet using remote ip and port stored in the pcb */
  return ntcpip_udpSendTo(pcb, p, &pcb->remote_ip, pcb->remote_port);
}

/**
 * Send data to a specified address using UDP.
 *
 * @param pcb UDP PCB used to send the data.
 * @param p chain of pbuf's to be sent.
 * @param dst_ip Destination IP address.
 * @param dst_port Destination UDP port.
 *
 * dst_ip & dst_port are expected to be in the same byte order as in the pcb.
 *
 * If the PCB already has a remote address association, it will
 * be restored after the data is sent.
 * 
 * @return lwIP error code (@see ntcpip_udpSend for possible error codes)
 *
 * @see ntcpip__udpDisconnect() ntcpip_udpSend()
 */
ntcpip_Err
ntcpip_udpSendTo(struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p,
  const struct ntcpip_ipAddr *dst_ip, Uint16 dst_port)
{
  struct ntcpip__netif *netif;

  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_udpSend\n"));

  /* find the outgoing network interface for this packet */
#if NTCPIP__LWIP_IGMP
  netif = ntcpip__ipRoute((ntcpip_ipaddrIsMulticast(dst_ip))?(&(pcb->multicast_ip)):(dst_ip));
#else
  netif = ntcpip__ipRoute(dst_ip);
#endif /* NTCPIP__LWIP_IGMP */

  /* no outgoing network interface could be found? */
  if (netif == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip_udpSend: No route to 0x%"X32_F"\n", dst_ip->addr));
    UDP_STATS_INC(udp.rterr);
    return NTCPIP_ERR_RTE;
  }
  return ntcpip_udpSendToIf(pcb, p, dst_ip, dst_port, netif);
}

/**
 * Send data to a specified address using UDP.
 * The netif used for sending can be specified.
 *
 * This function exists mainly for DHCP, to be able to send UDP packets
 * on a netif that is still down.
 *
 * @param pcb UDP PCB used to send the data.
 * @param p chain of pbuf's to be sent.
 * @param dst_ip Destination IP address.
 * @param dst_port Destination UDP port.
 * @param netif the netif used for sending.
 *
 * dst_ip & dst_port are expected to be in the same byte order as in the pcb.
 *
 * @return lwIP error code (@see ntcpip_udpSend for possible error codes)
 *
 * @see ntcpip_udpDisconnect() ntcpip_udpSend()
 */
ntcpip_Err
ntcpip_udpSendToIf(struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p,
  const struct ntcpip_ipAddr *dst_ip, Uint16 dst_port, struct ntcpip__netif *netif)
{
  struct udp_hdr *udphdr;
  struct ntcpip_ipAddr *src_ip;
  ntcpip_Err err;
  struct ntcpip_pbuf *q; /* q will be sent down the stack */

#if IP_SOF_BROADCAST
  /* broadcast filter? */
  if ( ((pcb->so_options & NTCPIP_SOF_BROADCAST) == 0) && ntcpip__ipaddrIsBroadcast(dst_ip, netif) ) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
      ("ntcpip_udpSendToIf: NTCPIP_SOF_BROADCAST not enabled on pcb %p\n", (void *)pcb));
    return NTCPIP_ERR_VAL;
  }
#endif /* IP_SOF_BROADCAST */

  /* if the PCB is not yet bound to a port, bind it here */
  if (pcb->local_port == 0) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_udpSend: not yet bound to a port, binding now\n"));
    err = ntcpip_udpBind(pcb, &pcb->local_ip, pcb->local_port);
    if (err != NTCPIP_ERR_OK) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip_udpSend: forced port bind failed\n"));
      return err;
    }
  }

  /* not enough space to add an UDP header to first pbuf in given p chain? */
  if (ntcpip__pbufHeader(p, UDP_HLEN)) {
    /* allocate header in a separate new pbuf */
    q = ntcpip_pbufAlloc(NTCPIP_PBUF_IP, UDP_HLEN, NTCPIP_PBUF_RAM);
    /* new header pbuf could not be allocated? */
    if (q == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip_udpSend: could not allocate header\n"));
      return NTCPIP_ERR_MEM;
    }
    /* chain header q in front of given pbuf p */
    ntcpip__pbufChain(q, p);
    /* first pbuf q points to header pbuf */
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG,
                ("ntcpip_udpSend: added header pbuf %p before given pbuf %p\n", (void *)q, (void *)p));
  } else {
    /* adding space for header within p succeeded */
    /* first pbuf q equals given pbuf */
    q = p;
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: added header in given pbuf %p\n", (void *)p));
  }
  NTCPIP__LWIP_ASSERT("check that first pbuf can hold struct udp_hdr",
              (q->len >= sizeof(struct udp_hdr)));
  /* q now represents the packet to be sent */
  udphdr = q->payload;
  udphdr->src = ntcpip_htons(pcb->local_port);
  udphdr->dest = ntcpip_htons(dst_port);
  /* in UDP, 0 checksum means 'no checksum' */
  udphdr->chksum = 0x0000; 

  /* PCB local address is IP_ANY_ADDR? */
  if (ntcpip_ipaddrIsAny(&pcb->local_ip)) {
    /* use outgoing network interface IP address as source address */
    src_ip = &(netif->ip_addr);
  } else {
    /* check if UDP PCB local IP address is correct
     * this could be an old address if netif->ip_addr has changed */
    if (!ntcpip_ipaddrCmp(&(pcb->local_ip), &(netif->ip_addr))) {
      /* local_ip doesn't match, drop the packet */
      if (q != p) {
        /* free the header pbuf */
        ntcpip_pbufFree(q);
        q = NULL;
        /* p is still referenced by the caller, and will live on */
      }
      return NTCPIP_ERR_VAL;
    }
    /* use UDP PCB local IP address as source address */
    src_ip = &(pcb->local_ip);
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: sending datagram of length %"U16_F"\n", q->tot_len));

#if NTCPIP__LWIP_UDPLITE
  /* UDP Lite protocol? */
  if (pcb->flags & UDP_FLAGS_UDPLITE) {
    Uint16 chklen, chklen_hdr;
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: UDP LITE packet length %"U16_F"\n", q->tot_len));
    /* set UDP message length in UDP header */
    chklen_hdr = chklen = pcb->chksum_len_tx;
    if ((chklen < sizeof(struct udp_hdr)) || (chklen > q->tot_len)) {
      if (chklen != 0) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: UDP LITE pcb->chksum_len is illegal: %"U16_F"\n", chklen));
      }
      /* For UDP-Lite, checksum length of 0 means checksum
         over the complete packet. (See RFC 3828 chap. 3.1)
         At least the UDP-Lite header must be covered by the
         checksum, therefore, if chksum_len has an illegal
         value, we generate the checksum over the complete
         packet to be safe. */
      chklen_hdr = 0;
      chklen = q->tot_len;
    }
    udphdr->len = ntcpip_htons(chklen_hdr);
    /* calculate checksum */
#if NTCPIP__CHECKSUM_GEN_UDP
    udphdr->chksum = ntcpip__inetChksumPseudoPartial(q, src_ip, dst_ip,
                                        IP_PROTO_UDPLITE, q->tot_len, chklen);
    /* chksum zero must become 0xffff, as zero means 'no checksum' */
    if (udphdr->chksum == 0x0000)
      udphdr->chksum = 0xffff;
#endif /* NTCPIP__CHECKSUM_CHECK_UDP */
    /* output to IP */
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: ntcpip__ipOutputIf (,,,,IP_PROTO_UDPLITE,)\n"));
#if NTCPIP__LWIP_NETIF_HWADDRHINT
    netif->addr_hint = &(pcb->addr_hint);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
    err = ntcpip__ipOutputIf(q, src_ip, dst_ip, pcb->ttl, pcb->tos, IP_PROTO_UDPLITE, netif);
#if NTCPIP__LWIP_NETIF_HWADDRHINT
    netif->addr_hint = NULL;
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  } else
#endif /* NTCPIP__LWIP_UDPLITE */
  {      /* UDP */
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: UDP packet length %"U16_F"\n", q->tot_len));
    udphdr->len = ntcpip_htons(q->tot_len);
    /* calculate checksum */
#if NTCPIP__CHECKSUM_GEN_UDP
    if ((pcb->flags & UDP_FLAGS_NOCHKSUM) == 0) {
      udphdr->chksum = ntcpip__inetChksumPseudo(q, src_ip, dst_ip, IP_PROTO_UDP, q->tot_len);
      /* chksum zero must become 0xffff, as zero means 'no checksum' */
      if (udphdr->chksum == 0x0000) udphdr->chksum = 0xffff;
    }
#endif /* NTCPIP__CHECKSUM_CHECK_UDP */
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: UDP checksum 0x%04"X16_F"\n", udphdr->chksum));
    NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpSend: ntcpip__ipOutputIf (,,,,IP_PROTO_UDP,)\n"));
    /* output to IP */
#if NTCPIP__LWIP_NETIF_HWADDRHINT
    netif->addr_hint = &(pcb->addr_hint);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
    err = ntcpip__ipOutputIf(q, src_ip, dst_ip, pcb->ttl, pcb->tos, IP_PROTO_UDP, netif);
#if NTCPIP__LWIP_NETIF_HWADDRHINT
    netif->addr_hint = NULL;
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  }
  /* TODO: must this be increased even if error occured? */
  ntcpip__snmpIncUdpoutdatagrams();

  /* did we chain a separate header pbuf earlier? */
  if (q != p) {
    /* free the header pbuf */
    ntcpip_pbufFree(q);
    q = NULL;
    /* p is still referenced by the caller, and will live on */
  }

  UDP_STATS_INC(udp.xmit);
  return err;
}

/**
 * Bind an UDP PCB.
 *
 * @param pcb UDP PCB to be bound with a local address ipaddr and port.
 * @param ipaddr local IP address to bind with. Use NTCPIP_IP_ADDR_ANY to
 * bind to all local interfaces.
 * @param port local UDP port to bind with. Use 0 to automatically bind
 * to a random port between UDP_LOCAL_PORT_RANGE_START and
 * UDP_LOCAL_PORT_RANGE_END.
 *
 * ipaddr & port are expected to be in the same byte order as in the pcb.
 *
 * @return lwIP error code.
 * - NTCPIP_ERR_OK. Successful. No error occured.
 * - NTCPIP_ERR_USE. The specified ipaddr and port are already bound to by
 * another UDP PCB.
 *
 * @see ntcpip_udpDisconnect()
 */
ntcpip_Err
ntcpip_udpBind(struct ntcpip__udpPcb *pcb, const struct ntcpip_ipAddr *ipaddr, Uint16 port)
{
  struct ntcpip__udpPcb *ipcb;
  Uint8 rebind;

  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_udpBind(ipaddr = "));
  ntcpip_ipaddrDebugPrint(NTCPIP__UDP_DEBUG, ipaddr);
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE, (", port = %"U16_F")\n", port));

  rebind = 0;
  /* Check for double bind and rebind of the same pcb */
  for (ipcb = ntcpip__udpPcbs; ipcb != NULL; ipcb = ipcb->next) {
    /* is this UDP PCB already on active list? */
    if (pcb == ipcb) {
      /* pcb may occur at most once in active list */
      NTCPIP__LWIP_ASSERT("rebind == 0", rebind == 0);
      /* pcb already in list, just rebind */
      rebind = 1;
    }

    /* this code does not allow upper layer to share a UDP port for
       listening to broadcast or multicast traffic (See SO_REUSE_ADDR and
       SO_REUSE_PORT under *BSD). TODO: See where it fits instead, OR
       combine with implementation of UDP PCB flags. Leon Woestenberg. */
#ifdef LWIP_UDP_TODO
    /* port matches that of PCB in list? */
    else
      if ((ipcb->local_port == port) &&
          /* IP address matches, or one is NTCPIP_IP_ADDR_ANY? */
          (ntcpip_ipaddrIsAny(&(ipcb->local_ip)) ||
           ntcpip_ipaddrIsAny(ipaddr) ||
           ntcpip_ipaddrCmp(&(ipcb->local_ip), ipaddr))) {
        /* other PCB already binds to this local IP and port */
        NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG,
                    ("ntcpip_udpBind: local port %"U16_F" already bound by another pcb\n", port));
        return NTCPIP_ERR_USE;
      }
#endif
  }

  ntcpip_ipaddrSet(&pcb->local_ip, ipaddr);

  /* no port specified? */
  if (port == 0) {
#ifndef UDP_LOCAL_PORT_RANGE_START
#define UDP_LOCAL_PORT_RANGE_START 4096
#define UDP_LOCAL_PORT_RANGE_END   0x7fff
#endif
    port = UDP_LOCAL_PORT_RANGE_START;
    ipcb = ntcpip__udpPcbs;
    while ((ipcb != NULL) && (port != UDP_LOCAL_PORT_RANGE_END)) {
      if (ipcb->local_port == port) {
        /* port is already used by another udp_pcb */
        port++;
        /* restart scanning all udp pcbs */
        ipcb = ntcpip__udpPcbs;
      } else
        /* go on with next udp pcb */
        ipcb = ipcb->next;
    }
    if (ipcb != NULL) {
      /* no more ports available in local range */
      NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpBind: out of free UDP ports\n"));
      return NTCPIP_ERR_USE;
    }
  }
  pcb->local_port = port;
  ntcpip__snmpInsertUdpidxTree(pcb);
  /* pcb not active yet? */
  if (rebind == 0) {
    /* place the PCB on the active list if not already there */
    pcb->next = ntcpip__udpPcbs;
    ntcpip__udpPcbs = pcb;
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE,
              ("ntcpip_udpBind: bound to %"U16_F".%"U16_F".%"U16_F".%"U16_F", port %"U16_F"\n",
               (Uint16)((ntcpip_ntohl(pcb->local_ip.addr) >> 24) & 0xff),
               (Uint16)((ntcpip_ntohl(pcb->local_ip.addr) >> 16) & 0xff),
               (Uint16)((ntcpip_ntohl(pcb->local_ip.addr) >> 8) & 0xff),
               (Uint16)(ntcpip_ntohl(pcb->local_ip.addr) & 0xff), pcb->local_port));
  return NTCPIP_ERR_OK;
}
/**
 * Connect an UDP PCB.
 *
 * This will associate the UDP PCB with the remote address.
 *
 * @param pcb UDP PCB to be connected with remote address ipaddr and port.
 * @param ipaddr remote IP address to connect with.
 * @param port remote UDP port to connect with.
 *
 * @return lwIP error code
 *
 * ipaddr & port are expected to be in the same byte order as in the pcb.
 *
 * The udp pcb is bound to a random local port if not already bound.
 *
 * @see ntcpip_udpDisconnect()
 */
ntcpip_Err
ntcpip_udpConnect(struct ntcpip__udpPcb *pcb, const struct ntcpip_ipAddr *ipaddr, Uint16 port)
{
  struct ntcpip__udpPcb *ipcb;

  if (pcb->local_port == 0) {
    ntcpip_Err err = ntcpip_udpBind(pcb, &pcb->local_ip, pcb->local_port);
    if (err != NTCPIP_ERR_OK)
      return err;
  }

  ntcpip_ipaddrSet(&pcb->remote_ip, ipaddr);
  pcb->remote_port = port;
  pcb->flags |= UDP_FLAGS_CONNECTED;
/** TODO: this functionality belongs in upper layers */
#ifdef LWIP_UDP_TODO
  /* Nail down local IP for ntcpip_netconnAddr()/getsockname() */
  if (ntcpip_ipaddrIsAny(&pcb->local_ip) && !ntcpip_ipaddrIsAny(&pcb->remote_ip)) {
    struct ntcpip__netif *netif;

    if ((netif = ntcpip__ipRoute(&(pcb->remote_ip))) == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("ntcpip_udpConnect: No route to 0x%lx\n", pcb->remote_ip.addr));
      UDP_STATS_INC(udp.rterr);
      return NTCPIP_ERR_RTE;
    }
    /** TODO: this will bind the udp pcb locally, to the interface which
        is used to route output packets to the remote address. However, we
        might want to accept incoming packets on any interface! */
    pcb->local_ip = netif->ip_addr;
  } else if (ntcpip_ipaddrIsAny(&pcb->remote_ip)) {
    pcb->local_ip.addr = 0;
  }
#endif
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE,
              ("ntcpip_udpConnect: connected to %"U16_F".%"U16_F".%"U16_F".%"U16_F",port %"U16_F"\n",
               (Uint16)((ntcpip_ntohl(pcb->remote_ip.addr) >> 24) & 0xff),
               (Uint16)((ntcpip_ntohl(pcb->remote_ip.addr) >> 16) & 0xff),
               (Uint16)((ntcpip_ntohl(pcb->remote_ip.addr) >> 8) & 0xff),
               (Uint16)(ntcpip_ntohl(pcb->remote_ip.addr) & 0xff), pcb->remote_port));

  /* Insert UDP PCB into the list of active UDP PCBs. */
  for (ipcb = ntcpip__udpPcbs; ipcb != NULL; ipcb = ipcb->next) {
    if (pcb == ipcb) {
      /* already on the list, just return */
      return NTCPIP_ERR_OK;
    }
  }
  /* PCB not yet on the list, add PCB now */
  pcb->next = ntcpip__udpPcbs;
  ntcpip__udpPcbs = pcb;
  return NTCPIP_ERR_OK;
}

/**
 * Disconnect a UDP PCB
 *
 * @param pcb the udp pcb to disconnect.
 */
void
ntcpip_udpDisconnect(struct ntcpip__udpPcb *pcb)
{
  /* reset remote address association */
  ntcpip_ipaddrSet(&pcb->remote_ip, NTCPIP_IP_ADDR_ANY);
  pcb->remote_port = 0;
  /* mark PCB as unconnected */
  pcb->flags &= ~UDP_FLAGS_CONNECTED;
}

/**
 * Set a receive callback for a UDP PCB
 *
 * This callback will be called when receiving a datagram for the pcb.
 *
 * @param pcb the pcb for wich to set the recv callback
 * @param recv function pointer of the callback function
 * @param recv_arg additional argument to pass to the callback function
 */
void
ntcpip_udpRecv(struct ntcpip__udpPcb *pcb,
         void (* recv)(void *arg, struct ntcpip__udpPcb *upcb, struct ntcpip_pbuf *p,
                       struct ntcpip_ipAddr *addr, Uint16 port),
         void *recv_arg)
{
  /* remember recv() callback and user data */
  pcb->recv = recv;
  pcb->recv_arg = recv_arg;
}

/**
 * Remove an UDP PCB.
 *
 * @param pcb UDP PCB to be removed. The PCB is removed from the list of
 * UDP PCB's and the data structure is freed from memory.
 *
 * @see ntcpip_udpNew()
 */
void
ntcpip_udpRemove(struct ntcpip__udpPcb *pcb)
{
  struct ntcpip__udpPcb *pcb2;

  ntcpip__snmpDeleteUdpidxTree(pcb);
  /* pcb to be removed is first in list? */
  if (ntcpip__udpPcbs == pcb) {
    /* make list start at 2nd pcb */
    ntcpip__udpPcbs = ntcpip__udpPcbs->next;
    /* pcb not 1st in list */
  } else
    for (pcb2 = ntcpip__udpPcbs; pcb2 != NULL; pcb2 = pcb2->next) {
      /* find pcb in ntcpip__udpPcbs list */
      if (pcb2->next != NULL && pcb2->next == pcb) {
        /* remove pcb from list */
        pcb2->next = pcb->next;
      }
    }
  ntcpip__mempFree(MEMP_UDP_PCB, pcb);
}

/**
 * Create a UDP PCB.
 *
 * @return The UDP PCB which was created. NULL if the PCB data structure
 * could not be allocated.
 *
 * @see ntcpip_udpRemove()
 */
struct ntcpip__udpPcb *
ntcpip_udpNew(void)
{
  struct ntcpip__udpPcb *pcb;
  pcb = ntcpip__mempMalloc(MEMP_UDP_PCB);
  /* could allocate UDP PCB? */
  if (pcb != NULL) {
    /* UDP Lite: by initializing to all zeroes, chksum_len is set to 0
     * which means checksum is generated over the whole datagram per default
     * (recommended as default by RFC 3828). */
    /* initialize PCB to all zeroes */
    memset(pcb, 0, sizeof(struct ntcpip__udpPcb));
    pcb->ttl = NTCPIP__UDP_TTL;
  }
  return pcb;
}

#if NTCPIP__UDP_DEBUG
/**
 * Print UDP header information for debug purposes.
 *
 * @param udphdr pointer to the udp header in memory.
 */
void
udp_debug_print(struct udp_hdr *udphdr)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("UDP header:\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("|     %5"U16_F"     |     %5"U16_F"     | (src port, dest port)\n",
                          ntcpip_ntohs(udphdr->src), ntcpip_ntohs(udphdr->dest)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("|     %5"U16_F"     |     0x%04"X16_F"    | (len, chksum)\n",
                          ntcpip_ntohs(udphdr->len), ntcpip_ntohs(udphdr->chksum)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__UDP_DEBUG, ("+-------------------------------+\n"));
}
#endif /* NTCPIP__UDP_DEBUG */

#endif /* NTCPIP__LWIP_UDP */


