/**
 * @file
 * Implementation of raw protocol PCBs for low-level handling of
 * different types of protocols besides (or overriding) those
 * already available in lwIP.
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_RAW /* don't build if not configured for use in lwipopts.h */

#include "../def.h"
#include "../memp.h"
#include "ntcpip/inet.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "../raw.h"
#include "../stats.h"
#include "ntcpip/snmp.h"
#include "arch/perf.h"

#include <string.h>

/** The list of RAW PCBs */
static struct ntcpip__rawPcb *raw_pcbs;

/**
 * Determine if in incoming IP packet is covered by a RAW PCB
 * and if so, pass it to a user-provided receive callback function.
 *
 * Given an incoming IP datagram (as a chain of pbufs) this function
 * finds a corresponding RAW PCB and calls the corresponding receive
 * callback function.
 *
 * @param p pbuf to be demultiplexed to a RAW PCB.
 * @param inp network interface on which the datagram was received.
 * @return - 1 if the packet has been eaten by a RAW PCB receive
 *           callback function. The caller MAY NOT not reference the
 *           packet any longer, and MAY NOT call ntcpip_pbufFree().
 * @return - 0 if packet is not eaten (pbuf is still referenced by the
 *           caller).
 *
 */
Uint8
raw_input(struct ntcpip_pbuf *p, struct ntcpip__netif *inp)
{
  struct ntcpip__rawPcb *pcb, *prev;
  struct ip_hdr *iphdr;
  Int16 proto;
  Uint8 eaten = 0;

  NTCPIP__LWIP_UNUSED_ARG(inp);

  iphdr = p->payload;
  proto = IPH_PROTO(iphdr);

  prev = NULL;
  pcb = raw_pcbs;
  /* loop through all raw pcbs until the packet is eaten by one */
  /* this allows multiple pcbs to match against the packet by design */
  while ((eaten == 0) && (pcb != NULL)) {
    if (pcb->protocol == proto) {
#if IP_SOF_BROADCAST_RECV
      /* broadcast filter? */
      if ((pcb->so_options & NTCPIP_SOF_BROADCAST) || !ntcpip__ipaddrIsBroadcast(&(iphdr->dest), inp))
#endif /* IP_SOF_BROADCAST_RECV */
      {
        /* receive callback function available? */
        if (pcb->recv != NULL) {
          /* the receive callback function did not eat the packet? */
          if (pcb->recv(pcb->recv_arg, pcb, p, &(iphdr->src)) != 0) {
            /* receive function ate the packet */
            p = NULL;
            eaten = 1;
            if (prev != NULL) {
            /* move the pcb to the front of raw_pcbs so that is
               found faster next time */
              prev->next = pcb->next;
              pcb->next = raw_pcbs;
              raw_pcbs = pcb;
            }
          }
        }
        /* no receive callback function was set for this raw PCB */
      }
      /* drop the packet */
    }
    prev = pcb;
    pcb = pcb->next;
  }
  return eaten;
}

/**
 * Bind a RAW PCB.
 *
 * @param pcb RAW PCB to be bound with a local address ipaddr.
 * @param ipaddr local IP address to bind with. Use NTCPIP_IP_ADDR_ANY to
 * bind to all local interfaces.
 *
 * @return lwIP error code.
 * - NTCPIP_ERR_OK. Successful. No error occured.
 * - NTCPIP_ERR_USE. The specified IP address is already bound to by
 * another RAW PCB.
 *
 * @see raw_disconnect()
 */
ntcpip_Err
raw_bind(struct ntcpip__rawPcb *pcb, struct ntcpip_ipAddr *ipaddr)
{
  ntcpip_ipaddrSet(&pcb->local_ip, ipaddr);
  return NTCPIP_ERR_OK;
}

/**
 * Connect an RAW PCB. This function is required by upper layers
 * of lwip. Using the raw api you could use raw_sendto() instead
 *
 * This will associate the RAW PCB with the remote address.
 *
 * @param pcb RAW PCB to be connected with remote address ipaddr and port.
 * @param ipaddr remote IP address to connect with.
 *
 * @return lwIP error code
 *
 * @see raw_disconnect() and raw_sendto()
 */
ntcpip_Err
raw_connect(struct ntcpip__rawPcb *pcb, struct ntcpip_ipAddr *ipaddr)
{
  ntcpip_ipaddrSet(&pcb->remote_ip, ipaddr);
  return NTCPIP_ERR_OK;
}


/**
 * Set the callback function for received packets that match the
 * raw PCB's protocol and binding. 
 * 
 * The callback function MUST either
 * - eat the packet by calling ntcpip_pbufFree() and returning non-zero. The
 *   packet will not be passed to other raw PCBs or other protocol layers.
 * - not free the packet, and return zero. The packet will be matched
 *   against further PCBs and/or forwarded to another protocol layers.
 * 
 * @return non-zero if the packet was free()d, zero if the packet remains
 * available for others.
 */
void
raw_recv(struct ntcpip__rawPcb *pcb,
         Uint8 (* recv)(void *arg, struct ntcpip__rawPcb *upcb, struct ntcpip_pbuf *p,
                      struct ntcpip_ipAddr *addr),
         void *recv_arg)
{
  /* remember recv() callback and user data */
  pcb->recv = recv;
  pcb->recv_arg = recv_arg;
}

/**
 * Send the raw IP packet to the given address. Note that actually you cannot
 * modify the IP headers (this is inconsistent with the receive callback where
 * you actually get the IP headers), you can only specify the IP payload here.
 * It requires some more changes in lwIP. (there will be a raw_send() function
 * then.)
 *
 * @param pcb the raw pcb which to send
 * @param p the IP payload to send
 * @param ipaddr the destination address of the IP packet
 *
 */
ntcpip_Err
raw_sendto(struct ntcpip__rawPcb *pcb, struct ntcpip_pbuf *p, struct ntcpip_ipAddr *ipaddr)
{
  ntcpip_Err err;
  struct ntcpip__netif *netif;
  struct ntcpip_ipAddr *src_ip;
  struct ntcpip_pbuf *q; /* q will be sent down the stack */
  
  NTCPIP__LWIP_DEBUGF(NTCPIP__RAW_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("raw_sendto\n"));
  
  /* not enough space to add an IP header to first pbuf in given p chain? */
  if (ntcpip__pbufHeader(p, NTCPIP__IP_HLEN)) {
    /* allocate header in new pbuf */
    q = ntcpip_pbufAlloc(NTCPIP_PBUF_IP, 0, NTCPIP_PBUF_RAM);
    /* new header pbuf could not be allocated? */
    if (q == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__RAW_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("raw_sendto: could not allocate header\n"));
      return NTCPIP_ERR_MEM;
    }
    /* chain header q in front of given pbuf p */
    ntcpip__pbufChain(q, p);
    /* { first pbuf q points to header pbuf } */
    NTCPIP__LWIP_DEBUGF(NTCPIP__RAW_DEBUG, ("raw_sendto: added header pbuf %p before given pbuf %p\n", (void *)q, (void *)p));
  }  else {
    /* first pbuf q equals given pbuf */
    q = p;
    if(ntcpip__pbufHeader(q, -NTCPIP__IP_HLEN)) {
      NTCPIP__LWIP_ASSERT("Can't restore header we just removed!", 0);
      return NTCPIP_ERR_MEM;
    }
  }

  if ((netif = ntcpip__ipRoute(ipaddr)) == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__RAW_DEBUG | NTCPIP__LWIP_DBG_LEVEL_WARNING, ("raw_sendto: No route to 0x%"X32_F"\n", ipaddr->addr));
    /* free any temporary header pbuf allocated by ntcpip__pbufHeader() */
    if (q != p) {
      ntcpip_pbufFree(q);
    }
    return NTCPIP_ERR_RTE;
  }

#if IP_SOF_BROADCAST
  /* broadcast filter? */
  if ( ((pcb->so_options & NTCPIP_SOF_BROADCAST) == 0) && ntcpip__ipaddrIsBroadcast(ipaddr, netif) ) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__RAW_DEBUG | NTCPIP__LWIP_DBG_LEVEL_WARNING, ("raw_sendto: NTCPIP_SOF_BROADCAST not enabled on pcb %p\n", (void *)pcb));
    /* free any temporary header pbuf allocated by ntcpip__pbufHeader() */
    if (q != p) {
      ntcpip_pbufFree(q);
    }
    return NTCPIP_ERR_VAL;
  }
#endif /* IP_SOF_BROADCAST */

  if (ntcpip_ipaddrIsAny(&pcb->local_ip)) {
    /* use outgoing network interface IP address as source address */
    src_ip = &(netif->ip_addr);
  } else {
    /* use RAW PCB local IP address as source address */
    src_ip = &(pcb->local_ip);
  }

#if NTCPIP__LWIP_NETIF_HWADDRHINT
  netif->addr_hint = &(pcb->addr_hint);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  err = ntcpip__ipOutputIf (q, src_ip, ipaddr, pcb->ttl, pcb->tos, pcb->protocol, netif);
#if NTCPIP__LWIP_NETIF_HWADDRHINT
  netif->addr_hint = NULL;
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/

  /* did we chain a header earlier? */
  if (q != p) {
    /* free the header */
    ntcpip_pbufFree(q);
  }
  return err;
}

/**
 * Send the raw IP packet to the address given by raw_connect()
 *
 * @param pcb the raw pcb which to send
 * @param p the IP payload to send
 *
 */
ntcpip_Err
raw_send(struct ntcpip__rawPcb *pcb, struct ntcpip_pbuf *p)
{
  return raw_sendto(pcb, p, &pcb->remote_ip);
}

/**
 * Remove an RAW PCB.
 *
 * @param pcb RAW PCB to be removed. The PCB is removed from the list of
 * RAW PCB's and the data structure is freed from memory.
 *
 * @see raw_new()
 */
void
raw_remove(struct ntcpip__rawPcb *pcb)
{
  struct ntcpip__rawPcb *pcb2;
  /* pcb to be removed is first in list? */
  if (raw_pcbs == pcb) {
    /* make list start at 2nd pcb */
    raw_pcbs = raw_pcbs->next;
    /* pcb not 1st in list */
  } else {
    for(pcb2 = raw_pcbs; pcb2 != NULL; pcb2 = pcb2->next) {
      /* find pcb in raw_pcbs list */
      if (pcb2->next != NULL && pcb2->next == pcb) {
        /* remove pcb from list */
        pcb2->next = pcb->next;
      }
    }
  }
  ntcpip__mempFree(MEMP_RAW_PCB, pcb);
}

/**
 * Create a RAW PCB.
 *
 * @return The RAW PCB which was created. NULL if the PCB data structure
 * could not be allocated.
 *
 * @param proto the protocol number of the IPs payload (e.g. IP_PROTO_ICMP)
 *
 * @see raw_remove()
 */
struct ntcpip__rawPcb *
raw_new(Uint8 proto) {
  struct ntcpip__rawPcb *pcb;

  NTCPIP__LWIP_DEBUGF(NTCPIP__RAW_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("raw_new\n"));

  pcb = ntcpip__mempMalloc(MEMP_RAW_PCB);
  /* could allocate RAW PCB? */
  if (pcb != NULL) {
    /* initialize PCB to all zeroes */
    memset(pcb, 0, sizeof(struct ntcpip__rawPcb));
    pcb->protocol = proto;
    pcb->ttl = NTCPIP__RAW_TTL;
    pcb->next = raw_pcbs;
    raw_pcbs = pcb;
  }
  return pcb;
}

#endif /* NTCPIP__LWIP_RAW */

