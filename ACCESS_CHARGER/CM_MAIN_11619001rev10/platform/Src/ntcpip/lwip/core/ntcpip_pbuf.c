/**
 * @file
 * Packet buffer management
 *
 * Packets are built from the pbuf data structure. It supports dynamic
 * memory allocation for packet contents or can reference externally
 * managed packet contents both in RAM and ROM. Quick allocation for
 * incoming packets is provided through pools with fixed sized pbufs.
 *
 * A packet may span over multiple pbufs, chained as a singly linked
 * list. This is called a "pbuf chain".
 *
 * Multiple packets may be queued, also using this singly linked list.
 * This is called a "packet queue".
 * 
 * So, a packet queue consists of one or more pbuf chains, each of
 * which consist of one or more pbufs. CURRENTLY, PACKET QUEUES ARE
 * NOT SUPPORTED!!! Use helper structs to queue multiple packets.
 * 
 * The differences between a pbuf chain and a packet queue are very
 * precise but subtle. 
 *
 * The last pbuf of a packet has a ->tot_len field that equals the
 * ->len field. It can be found by traversing the list. If the last
 * pbuf of a packet has a ->next field other than NULL, more packets
 * are on the queue.
 *
 * Therefore, looping through a pbuf of a single packet, has an
 * loop end condition (tot_len == p->len), NOT (next == NULL).
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#include "../stats.h"
#include "../def.h"
#include "../mem.h"
#include "../memp.h"
#include "ntcpip/pbuf.h"
#include "ntcpip/sys.h"
#include "../../arch/perf.h"
#if NTCPIP__TCP_QUEUE_OOSEQ
#include "ntcpip/tcp.h"
#endif

#include <string.h>

#define SIZEOF_STRUCT_PBUF        LWIP_MEM_ALIGN_SIZE(sizeof(struct ntcpip_pbuf))
/* Since the pool is created in memp, NTCPIP__PBUF_POOL_BUFSIZE will be automatically
   aligned there. Therefore, PBUF_POOL_BUFSIZE_ALIGNED can be used here. */
#define PBUF_POOL_BUFSIZE_ALIGNED LWIP_MEM_ALIGN_SIZE(NTCPIP__PBUF_POOL_BUFSIZE)

#if !NTCPIP__TCP_QUEUE_OOSEQ || NTCPIP__NO_SYS
#define PBUF_POOL_IS_EMPTY()
#else /* !NTCPIP__TCP_QUEUE_OOSEQ || NTCPIP__NO_SYS */
/** Define this to 0 to prevent freeing ooseq pbufs when the NTCPIP_PBUF_POOL is empty */
#ifndef PBUF_POOL_FREE_OOSEQ
#define PBUF_POOL_FREE_OOSEQ 1
#endif /* PBUF_POOL_FREE_OOSEQ */

#if PBUF_POOL_FREE_OOSEQ
#include "ntcpip/tcpip.h"
#define PBUF_POOL_IS_EMPTY() pbuf_pool_is_empty()
static Uint8 pbuf_free_ooseq_queued;
/**
 * Attempt to reclaim some memory from queued out-of-sequence TCP segments
 * if we run out of pool pbufs. It's better to give priority to new packets
 * if we're running out.
 *
 * This must be done in the correct thread context therefore this function
 * can only be used with NTCPIP__NO_SYS=0 and through tcpip_callback.
 */
static void
pbuf_free_ooseq(void* arg)
{
  struct ntcpip__tcpPcb* pcb;
  NTCPIP__SYS_ARCH_DECL_PROTECT(old_level);
  NTCPIP__LWIP_UNUSED_ARG(arg);

  NTCPIP__SYS_ARCH_PROTECT(old_level);
  pbuf_free_ooseq_queued = 0;
  NTCPIP__SYS_ARCH_UNPROTECT(old_level);

  for (pcb = ntcpip__tcpActivePcbs; NULL != pcb; pcb = pcb->next) {
    if (NULL != pcb->ooseq) {
      /** Free the ooseq pbufs of one PCB only */
      NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("pbuf_free_ooseq: freeing out-of-sequence pbufs\n"));
      ntcpip__tcpSegsFree(pcb->ooseq);
      pcb->ooseq = NULL;
      return;
    }
  }
}

/** Queue a call to pbuf_free_ooseq if not already queued. */
static void
pbuf_pool_is_empty(void)
{
  Uint8 queued;
  NTCPIP__SYS_ARCH_DECL_PROTECT(old_level);

  NTCPIP__SYS_ARCH_PROTECT(old_level);
  queued = pbuf_free_ooseq_queued;
  pbuf_free_ooseq_queued = 1;
  NTCPIP__SYS_ARCH_UNPROTECT(old_level);

  if(!queued) {
    /* queue a call to pbuf_free_ooseq if not already queued */
    if(ntcpip_tcpipCallbackWithBlock(pbuf_free_ooseq, NULL, 0) != NTCPIP_ERR_OK) {
      NTCPIP__SYS_ARCH_PROTECT(old_level);
      pbuf_free_ooseq_queued = 0;
      NTCPIP__SYS_ARCH_UNPROTECT(old_level);
    }
  }
}
#endif /* PBUF_POOL_FREE_OOSEQ */
#endif /* !NTCPIP__TCP_QUEUE_OOSEQ || NTCPIP__NO_SYS */

/**
 * Allocates a pbuf of the given type (possibly a chain for NTCPIP_PBUF_POOL type).
 *
 * The actual memory allocated for the pbuf is determined by the
 * layer at which the pbuf is allocated and the requested size
 * (from the size parameter).
 *
 * @param layer flag to define header size
 * @param length size of the pbuf's payload
 * @param type this parameter decides how and where the pbuf
 * should be allocated as follows:
 *
 * - NTCPIP_PBUF_RAM: buffer memory for pbuf is allocated as one large
 *             chunk. This includes protocol headers as well.
 * - NTCPIP_PBUF_ROM: no buffer memory is allocated for the pbuf, even for
 *             protocol headers. Additional headers must be prepended
 *             by allocating another pbuf and chain in to the front of
 *             the ROM pbuf. It is assumed that the memory used is really
 *             similar to ROM in that it is immutable and will not be
 *             changed. Memory which is dynamic should generally not
 *             be attached to NTCPIP_PBUF_ROM pbufs. Use NTCPIP_PBUF_REF instead.
 * - NTCPIP_PBUF_REF: no buffer memory is allocated for the pbuf, even for
 *             protocol headers. It is assumed that the pbuf is only
 *             being used in a single thread. If the pbuf gets queued,
 *             then ntpcip__pbufTake should be called to copy the buffer.
 * - NTCPIP_PBUF_POOL: the pbuf is allocated as a pbuf chain, with pbufs from
 *              the pbuf pool that is allocated during ntcpip__pbufInit().
 *
 * @return the allocated pbuf. If multiple pbufs where allocated, this
 * is the first pbuf of a pbuf chain.
 */
struct ntcpip_pbuf *
ntcpip_pbufAlloc(ntcpip__PbufLayer layer, Uint16 length, ntcpip__PbufType type)
{
  struct ntcpip_pbuf *p, *q, *r;
  Uint16 offset;
  Int32 rem_len; /* remaining length */
  NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufAlloc(length=%"U16_F")\n", length));

  /* determine header offset */
  offset = 0;
  switch (layer) {
  case NTCPIP_PBUF_TRANSPORT:
    /* add room for transport (often TCP) layer header */
    offset += NTCPIP__PBUF_TRANSPORT_HLEN;
    /* FALLTHROUGH */
  case NTCPIP_PBUF_IP:
    /* add room for IP layer header */
    offset += NTCPIP__PBUF_IP_HLEN;
    /* FALLTHROUGH */
  case NTCPIP_PBUF_LINK:
    /* add room for link layer header */
    offset += NTCPIP__PBUF_LINK_HLEN;
    break;
  case NTCPIP_PBUF_RAW:
    break;
  default:
    NTCPIP__LWIP_ASSERT("ntcpip_pbufAlloc: bad pbuf layer", 0);
    return NULL;
  }

  switch (type) {
  case NTCPIP_PBUF_POOL:
    /* allocate head of pbuf chain into p */
    p = ntcpip__mempMalloc(MEMP_NTCPIP_PBUF_POOL);
    NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufAlloc: allocated pbuf %p\n", (void *)p));
    if (p == NULL) {
      PBUF_POOL_IS_EMPTY();
      return NULL;
    }
    p->type = type;
    p->next = NULL;

    /* make the payload pointer point 'offset' bytes into pbuf data memory */
    p->payload = LWIP_MEM_ALIGN((void *)((Uint8 *)p + (SIZEOF_STRUCT_PBUF + offset)));
    NTCPIP__LWIP_ASSERT("ntcpip_pbufAlloc: pbuf p->payload properly aligned",
            ((ntcpip__MemPtr)p->payload % NTCPIP__MEM_ALIGNMENT) == 0);
    /* the total length of the pbuf chain is the requested size */
    p->tot_len = length;
    /* set the length of the first pbuf in the chain */
    p->len = LWIP_MIN(length, PBUF_POOL_BUFSIZE_ALIGNED - LWIP_MEM_ALIGN_SIZE(offset));
    NTCPIP__LWIP_ASSERT("check p->payload + p->len does not overflow pbuf",
                ((Uint8*)p->payload + p->len <=
                 (Uint8*)p + SIZEOF_STRUCT_PBUF + PBUF_POOL_BUFSIZE_ALIGNED));
    NTCPIP__LWIP_ASSERT("NTCPIP__PBUF_POOL_BUFSIZE must be bigger than NTCPIP__MEM_ALIGNMENT",
      (PBUF_POOL_BUFSIZE_ALIGNED - LWIP_MEM_ALIGN_SIZE(offset)) > 0 );
    /* set reference count (needed here in case we fail) */
    p->ref = 1;

    /* now allocate the tail of the pbuf chain */

    /* remember first pbuf for linkage in next iteration */
    r = p;
    /* remaining length to be allocated */
    rem_len = length - p->len;
    /* any remaining pbufs to be allocated? */
    while (rem_len > 0) {
      q = ntcpip__mempMalloc(MEMP_NTCPIP_PBUF_POOL);
      if (q == NULL) {
        PBUF_POOL_IS_EMPTY();
        /* free chain so far allocated */
        ntcpip_pbufFree(p);
        /* bail out unsuccesfully */
        return NULL;
      }
      q->type = type;
      q->flags = 0;
      q->next = NULL;
      /* make previous pbuf point to this pbuf */
      r->next = q;
      /* set total length of this pbuf and next in chain */
      NTCPIP__LWIP_ASSERT("rem_len < max_u16_t", rem_len < 0xffff);
      q->tot_len = (Uint16)rem_len;
      /* this pbuf length is pool size, unless smaller sized tail */
      q->len = LWIP_MIN((Uint16)rem_len, PBUF_POOL_BUFSIZE_ALIGNED);
      q->payload = (void *)((Uint8 *)q + SIZEOF_STRUCT_PBUF);
      NTCPIP__LWIP_ASSERT("ntcpip_pbufAlloc: pbuf q->payload properly aligned",
              ((ntcpip__MemPtr)q->payload % NTCPIP__MEM_ALIGNMENT) == 0);
      NTCPIP__LWIP_ASSERT("check p->payload + p->len does not overflow pbuf",
                  ((Uint8*)p->payload + p->len <=
                   (Uint8*)p + SIZEOF_STRUCT_PBUF + PBUF_POOL_BUFSIZE_ALIGNED));
      q->ref = 1;
      /* calculate remaining length to be allocated */
      rem_len -= q->len;
      /* remember this pbuf for linkage in next iteration */
      r = q;
    }
    /* end of chain */
    /*r->next = NULL;*/

    break;
  case NTCPIP_PBUF_RAM:
    /* If pbuf is to be allocated in RAM, allocate memory for it. */
    p = (struct ntcpip_pbuf*)ntcpip__memMalloc(LWIP_MEM_ALIGN_SIZE(SIZEOF_STRUCT_PBUF + offset) + LWIP_MEM_ALIGN_SIZE(length));
    if (p == NULL) {
      return NULL;
    }
    /* Set up internal structure of the pbuf. */
    p->payload = LWIP_MEM_ALIGN((void *)((Uint8 *)p + SIZEOF_STRUCT_PBUF + offset));
    p->len = p->tot_len = length;
    p->next = NULL;
    p->type = type;

    NTCPIP__LWIP_ASSERT("ntcpip_pbufAlloc: pbuf->payload properly aligned",
           ((ntcpip__MemPtr)p->payload % NTCPIP__MEM_ALIGNMENT) == 0);
    break;
  /* pbuf references existing (non-volatile static constant) ROM payload? */
  case NTCPIP_PBUF_ROM:
  /* pbuf references existing (externally allocated) RAM payload? */
  case NTCPIP_PBUF_REF:
    /* only allocate memory for the pbuf structure */
    p = ntcpip__mempMalloc(MEMP_PBUF);
    if (p == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
                  ("ntcpip_pbufAlloc: Could not allocate MEMP_PBUF for PBUF_%s.\n",
                  (type == NTCPIP_PBUF_ROM) ? "ROM" : "REF"));
      return NULL;
    }
    /* caller must set this field properly, afterwards */
    p->payload = NULL;
    p->len = p->tot_len = length;
    p->next = NULL;
    p->type = type;
    break;
  default:
    NTCPIP__LWIP_ASSERT("ntcpip_pbufAlloc: erroneous type", 0);
    return NULL;
  }
  /* set reference count */
  p->ref = 1;
  /* set flags */
  p->flags = 0;
  NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufAlloc(length=%"U16_F") == %p\n", length, (void *)p));
  return p;
}


/**
 * Shrink a pbuf chain to a desired length.
 *
 * @param p pbuf to shrink.
 * @param new_len desired new length of pbuf chain
 *
 * Depending on the desired length, the first few pbufs in a chain might
 * be skipped and left unchanged. The new last pbuf in the chain will be
 * resized, and any remaining pbufs will be freed.
 *
 * @note If the pbuf is ROM/REF, only the ->tot_len and ->len fields are adjusted.
 * @note May not be called on a packet queue.
 *
 * @note Despite its name, ntcpip_pbufRealloc cannot grow the size of a pbuf (chain).
 */
void
ntcpip_pbufRealloc(struct ntcpip_pbuf *p, Uint16 new_len)
{
  struct ntcpip_pbuf *q;
  Uint16 rem_len; /* remaining length */
  Int32 grow;

  NTCPIP__LWIP_ASSERT("ntcpip_pbufRealloc: p != NULL", p != NULL);
  NTCPIP__LWIP_ASSERT("ntcpip_pbufRealloc: sane p->type", p->type == NTCPIP_PBUF_POOL ||
              p->type == NTCPIP_PBUF_ROM ||
              p->type == NTCPIP_PBUF_RAM ||
              p->type == NTCPIP_PBUF_REF);

  /* desired length larger than current length? */
  if (new_len >= p->tot_len) {
    /* enlarging not yet supported */
    return;
  }

  /* the pbuf chain grows by (new_len - p->tot_len) bytes
   * (which may be negative in case of shrinking) */
  grow = new_len - p->tot_len;

  /* first, step over any pbufs that should remain in the chain */
  rem_len = new_len;
  q = p;
  /* should this pbuf be kept? */
  while (rem_len > q->len) {
    /* decrease remaining length by pbuf length */
    rem_len -= q->len;
    /* decrease total length indicator */
    NTCPIP__LWIP_ASSERT("grow < max_u16_t", grow < 0xffff);
    q->tot_len += (Uint16)grow;
    /* proceed to next pbuf in chain */
    q = q->next;
    NTCPIP__LWIP_ASSERT("ntcpip_pbufRealloc: q != NULL", q != NULL);
  }
  /* we have now reached the new last pbuf (in q) */
  /* rem_len == desired length for pbuf q */

  /* shrink allocated memory for NTCPIP_PBUF_RAM */
  /* (other types merely adjust their length fields */
  if ((q->type == NTCPIP_PBUF_RAM) && (rem_len != q->len)) {
    /* reallocate and adjust the length of the pbuf that will be split */
    q = ntcpip__memRealloc(q, (Uint8 *)q->payload - (Uint8 *)q + rem_len);
    NTCPIP__LWIP_ASSERT("ntcpip__memRealloc give q == NULL", q != NULL);
  }
  /* adjust length fields for new last pbuf */
  q->len = rem_len;
  q->tot_len = q->len;

  /* any remaining pbufs in chain? */
  if (q->next != NULL) {
    /* free remaining pbufs in chain */
    ntcpip_pbufFree(q->next);
  }
  /* q is last packet in chain */
  q->next = NULL;

}

/**
 * Adjusts the payload pointer to hide or reveal headers in the payload.
 *
 * Adjusts the ->payload pointer so that space for a header
 * (dis)appears in the pbuf payload.
 *
 * The ->payload, ->tot_len and ->len fields are adjusted.
 *
 * @param p pbuf to change the header size.
 * @param header_size_increment Number of bytes to increment header size which
 * increases the size of the pbuf. New space is on the front.
 * (Using a negative value decreases the header size.)
 * If hdr_size_inc is 0, this function does nothing and returns succesful.
 *
 * NTCPIP_PBUF_ROM and NTCPIP_PBUF_REF type buffers cannot have their sizes increased, so
 * the call will fail. A check is made that the increase in header size does
 * not move the payload pointer in front of the start of the buffer.
 * @return non-zero on failure, zero on success.
 *
 */
Uint8
ntcpip__pbufHeader(struct ntcpip_pbuf *p, Int16 header_size_increment)
{
  Uint16 type;
  void *payload;
  Uint16 increment_magnitude;

  NTCPIP__LWIP_ASSERT("p != NULL", p != NULL);
  if ((header_size_increment == 0) || (p == NULL))
    return 0;
 
  if (header_size_increment < 0){
    increment_magnitude = -header_size_increment;
    /* Check that we aren't going to move off the end of the pbuf */
    NTCPIP__LWIP_ERROR("increment_magnitude <= p->len", (increment_magnitude <= p->len), return 1;);
  } else {
    increment_magnitude = header_size_increment;
#if 0
    /* Can't assert these as some callers speculatively call
         ntcpip__pbufHeader() to see if it's OK.  Will return 1 below instead. */
    /* Check that we've got the correct type of pbuf to work with */
    NTCPIP__LWIP_ASSERT("p->type == NTCPIP_PBUF_RAM || p->type == NTCPIP_PBUF_POOL", 
                p->type == NTCPIP_PBUF_RAM || p->type == NTCPIP_PBUF_POOL);
    /* Check that we aren't going to move off the beginning of the pbuf */
    NTCPIP__LWIP_ASSERT("p->payload - increment_magnitude >= p + SIZEOF_STRUCT_PBUF",
                (Uint8 *)p->payload - increment_magnitude >= (Uint8 *)p + SIZEOF_STRUCT_PBUF);
#endif
  }

  type = p->type;
  /* remember current payload pointer */
  payload = p->payload;

  /* pbuf types containing payloads? */
  if (type == NTCPIP_PBUF_RAM || type == NTCPIP_PBUF_POOL) {
    /* set new payload pointer */
    p->payload = (Uint8 *)p->payload - header_size_increment;
    /* boundary check fails? */
    if ((Uint8 *)p->payload < (Uint8 *)p + SIZEOF_STRUCT_PBUF) {
      NTCPIP__LWIP_DEBUGF( NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
        ("ntcpip__pbufHeader: failed as %p < %p (not enough space for new header size)\n",
        (void *)p->payload, (void *)(p + 1)));
      /* restore old payload pointer */
      p->payload = payload;
      /* bail out unsuccesfully */
      return 1;
    }
  /* pbuf types refering to external payloads? */
  } else if (type == NTCPIP_PBUF_REF || type == NTCPIP_PBUF_ROM) {
    /* hide a header in the payload? */
    if ((header_size_increment < 0) && (increment_magnitude <= p->len)) {
      /* increase payload pointer */
      p->payload = (Uint8 *)p->payload - header_size_increment;
    } else {
      /* cannot expand payload to front (yet!)
       * bail out unsuccesfully */
      return 1;
    }
  }
  else {
    /* Unknown type */
    NTCPIP__LWIP_ASSERT("bad pbuf type", 0);
    return 1;
  }
  /* modify pbuf length fields */
  p->len += header_size_increment;
  p->tot_len += header_size_increment;

  NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__pbufHeader: old %p new %p (%"S16_F")\n",
    (void *)payload, (void *)p->payload, header_size_increment));

  return 0;
}

/**
 * Dereference a pbuf chain or queue and deallocate any no-longer-used
 * pbufs at the head of this chain or queue.
 *
 * Decrements the pbuf reference count. If it reaches zero, the pbuf is
 * deallocated.
 *
 * For a pbuf chain, this is repeated for each pbuf in the chain,
 * up to the first pbuf which has a non-zero reference count after
 * decrementing. So, when all reference counts are one, the whole
 * chain is free'd.
 *
 * @param p The pbuf (chain) to be dereferenced.
 *
 * @return the number of pbufs that were de-allocated
 * from the head of the chain.
 *
 * @note MUST NOT be called on a packet queue (Not verified to work yet).
 * @note the reference counter of a pbuf equals the number of pointers
 * that refer to the pbuf (or into the pbuf).
 *
 * @internal examples:
 *
 * Assuming existing chains a->b->c with the following reference
 * counts, calling ntcpip_pbufFree(a) results in:
 * 
 * 1->2->3 becomes ...1->3
 * 3->3->3 becomes 2->3->3
 * 1->1->2 becomes ......1
 * 2->1->1 becomes 1->1->1
 * 1->1->1 becomes .......
 *
 */
Uint8
ntcpip_pbufFree(struct ntcpip_pbuf *p)
{
  Uint16 type;
  struct ntcpip_pbuf *q;
  Uint8 count;

  if (p == NULL) {
    NTCPIP__LWIP_ASSERT("p != NULL", p != NULL);
    /* if assertions are disabled, proceed with debug output */
    NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
      ("ntcpip_pbufFree(p == NULL) was called.\n"));
    return 0;
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufFree(%p)\n", (void *)p));

  NTCPIP__PERF_START;

  NTCPIP__LWIP_ASSERT("ntcpip_pbufFree: sane type",
    p->type == NTCPIP_PBUF_RAM || p->type == NTCPIP_PBUF_ROM ||
    p->type == NTCPIP_PBUF_REF || p->type == NTCPIP_PBUF_POOL);

  count = 0;
  /* de-allocate all consecutive pbufs from the head of the chain that
   * obtain a zero reference count after decrementing*/
  while (p != NULL) {
    Uint16 ref;
    NTCPIP__SYS_ARCH_DECL_PROTECT(old_level);
    /* Since decrementing ref cannot be guaranteed to be a single machine operation
     * we must protect it. We put the new ref into a local variable to prevent
     * further protection. */
    NTCPIP__SYS_ARCH_PROTECT(old_level);
    /* all pbufs in a chain are referenced at least once */
    NTCPIP__LWIP_ASSERT("ntcpip_pbufFree: p->ref > 0", p->ref > 0);
    /* decrease reference count (number of pointers to pbuf) */
    ref = --(p->ref);
    NTCPIP__SYS_ARCH_UNPROTECT(old_level);
    /* this pbuf is no longer referenced to? */
    if (ref == 0) {
      /* remember next pbuf in chain for next iteration */
      q = p->next;
      NTCPIP__LWIP_DEBUGF( NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufFree: deallocating %p\n", (void *)p));
      type = p->type;
      /* is this a pbuf from the pool? */
      if (type == NTCPIP_PBUF_POOL) {
        ntcpip__mempFree(MEMP_NTCPIP_PBUF_POOL, p);
      /* is this a ROM or RAM referencing pbuf? */
      } else if (type == NTCPIP_PBUF_ROM || type == NTCPIP_PBUF_REF) {
        ntcpip__mempFree(MEMP_PBUF, p);
      /* type == NTCPIP_PBUF_RAM */
      } else {
        ntcpip__memFree(p);
      }
      count++;
      /* proceed to next pbuf */
      p = q;
    /* p->ref > 0, this pbuf is still referenced to */
    /* (and so the remaining pbufs in chain as well) */
    } else {
      NTCPIP__LWIP_DEBUGF( NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufFree: %p has ref %"U16_F", ending here.\n", (void *)p, ref));
      /* stop walking through the chain */
      p = NULL;
    }
  }
  NTCPIP__PERF_STOP("ntcpip_pbufFree");
  /* return number of de-allocated pbufs */
  return count;
}

/**
 * Count number of pbufs in a chain
 *
 * @param p first pbuf of chain
 * @return the number of pbufs in a chain
 */

Uint8
ntcpip__pbufClen(struct ntcpip_pbuf *p)
{
  Uint8 len;

  len = 0;
  while (p != NULL) {
    ++len;
    p = p->next;
  }
  return len;
}

/**
 * Increment the reference count of the pbuf.
 *
 * @param p pbuf to increase reference counter of
 *
 */
void
ntcpip_pbufRef(struct ntcpip_pbuf *p)
{
  NTCPIP__SYS_ARCH_DECL_PROTECT(old_level);
  /* pbuf given? */
  if (p != NULL) {
    NTCPIP__SYS_ARCH_PROTECT(old_level);
    ++(p->ref);
    NTCPIP__SYS_ARCH_UNPROTECT(old_level);
  }
}

/**
 * Concatenate two pbufs (each may be a pbuf chain) and take over
 * the caller's reference of the tail pbuf.
 * 
 * @note The caller MAY NOT reference the tail pbuf afterwards.
 * Use ntcpip__pbufChain() for that purpose.
 * 
 * @see ntcpip__pbufChain()
 */

void
ntcpip_pbufCat(struct ntcpip_pbuf *h, struct ntcpip_pbuf *t)
{
  struct ntcpip_pbuf *p;

  NTCPIP__LWIP_ERROR("(h != NULL) && (t != NULL) (programmer violates API)",
             ((h != NULL) && (t != NULL)), return;);

  /* proceed to last pbuf of chain */
  for (p = h; p->next != NULL; p = p->next) {
    /* add total length of second chain to all totals of first chain */
    p->tot_len += t->tot_len;
  }
  /* { p is last pbuf of first h chain, p->next == NULL } */
  NTCPIP__LWIP_ASSERT("p->tot_len == p->len (of last pbuf in chain)", p->tot_len == p->len);
  NTCPIP__LWIP_ASSERT("p->next == NULL", p->next == NULL);
  /* add total length of second chain to last pbuf total of first chain */
  p->tot_len += t->tot_len;
  /* chain last pbuf of head (p) with first of tail (t) */
  p->next = t;
  /* p->next now references t, but the caller will drop its reference to t,
   * so netto there is no change to the reference count of t.
   */
}

/**
 * Chain two pbufs (or pbuf chains) together.
 * 
 * The caller MUST call ntcpip_pbufFree(t) once it has stopped
 * using it. Use ntcpip_pbufCat() instead if you no longer use t.
 * 
 * @param h head pbuf (chain)
 * @param t tail pbuf (chain)
 * @note The pbufs MUST belong to the same packet.
 * @note MAY NOT be called on a packet queue.
 *
 * The ->tot_len fields of all pbufs of the head chain are adjusted.
 * The ->next field of the last pbuf of the head chain is adjusted.
 * The ->ref field of the first pbuf of the tail chain is adjusted.
 *
 */
void
ntcpip__pbufChain(struct ntcpip_pbuf *h, struct ntcpip_pbuf *t)
{
  ntcpip_pbufCat(h, t);
  /* t is now referenced by h */
  ntcpip_pbufRef(t);
  NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__pbufChain: %p references %p\n", (void *)h, (void *)t));
}

/**
 * Dechains the first pbuf from its succeeding pbufs in the chain.
 *
 * Makes p->tot_len field equal to p->len.
 * @param p pbuf to dechain
 * @return remainder of the pbuf chain, or NULL if it was de-allocated.
 * @note May not be called on a packet queue.
 */
struct ntcpip_pbuf *
ntcpip__pbufDechain(struct ntcpip_pbuf *p)
{
  struct ntcpip_pbuf *q;
  Uint8 tail_gone = 1;
  /* tail */
  q = p->next;
  /* pbuf has successor in chain? */
  if (q != NULL) {
    /* assert tot_len invariant: (p->tot_len == p->len + (p->next? p->next->tot_len: 0) */
    NTCPIP__LWIP_ASSERT("p->tot_len == p->len + q->tot_len", q->tot_len == p->tot_len - p->len);
    /* enforce invariant if assertion is disabled */
    q->tot_len = p->tot_len - p->len;
    /* decouple pbuf from remainder */
    p->next = NULL;
    /* total length of pbuf p is its own length only */
    p->tot_len = p->len;
    /* q is no longer referenced by p, free it */
    NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__pbufDechain: unreferencing %p\n", (void *)q));
    tail_gone = ntcpip_pbufFree(q);
    if (tail_gone > 0) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE,
                  ("ntcpip__pbufDechain: deallocated %p (as it is no longer referenced)\n", (void *)q));
    }
    /* return remaining tail or NULL if deallocated */
  }
  /* assert tot_len invariant: (p->tot_len == p->len + (p->next? p->next->tot_len: 0) */
  NTCPIP__LWIP_ASSERT("p->tot_len == p->len", p->tot_len == p->len);
  return ((tail_gone > 0) ? NULL : q);
}

/**
 *
 * Create NTCPIP_PBUF_RAM copies of pbufs.
 *
 * Used to queue packets on behalf of the lwIP stack, such as
 * ARP based queueing.
 *
 * @note You MUST explicitly use p = ntpcip__pbufTake(p);
 *
 * @note Only one packet is copied, no packet queue!
 *
 * @param p_to pbuf destination of the copy
 * @param p_from pbuf source of the copy
 *
 * @return NTCPIP_ERR_OK if pbuf was copied
 *         NTCPIP_ERR_ARG if one of the pbufs is NULL or p_to is not big
 *                 enough to hold p_from
 */
ntcpip_Err
ntcpip_pbufCopy(struct ntcpip_pbuf *p_to, struct ntcpip_pbuf *p_from)
{
  Uint16 offset_to=0, offset_from=0, len;

  NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufCopy(%p, %p)\n",
    (void*)p_to, (void*)p_from));

  /* is the target big enough to hold the source? */
  NTCPIP__LWIP_ERROR("ntcpip_pbufCopy: target not big enough to hold source", ((p_to != NULL) &&
             (p_from != NULL) && (p_to->tot_len >= p_from->tot_len)), return NTCPIP_ERR_ARG;);

  /* iterate through pbuf chain */
  do
  {
    NTCPIP__LWIP_ASSERT("p_to != NULL", p_to != NULL);
    /* copy one part of the original chain */
    if ((p_to->len - offset_to) >= (p_from->len - offset_from)) {
      /* complete current p_from fits into current p_to */
      len = p_from->len - offset_from;
    } else {
      /* current p_from does not fit into current p_to */
      len = p_to->len - offset_to;
    }
    NTCPIP__MEMCPY((Uint8*)p_to->payload + offset_to, (Uint8*)p_from->payload + offset_from, len);
    offset_to += len;
    offset_from += len;
    NTCPIP__LWIP_ASSERT("offset_to <= p_to->len", offset_to <= p_to->len);
    if (offset_to == p_to->len) {
      /* on to next p_to (if any) */
      offset_to = 0;
      p_to = p_to->next;
    }
    NTCPIP__LWIP_ASSERT("offset_from <= p_from->len", offset_from <= p_from->len);
    if (offset_from >= p_from->len) {
      /* on to next p_from (if any) */
      offset_from = 0;
      p_from = p_from->next;
    }

    if((p_from != NULL) && (p_from->len == p_from->tot_len)) {
      /* don't copy more than one packet! */
      NTCPIP__LWIP_ERROR("ntcpip_pbufCopy() does not allow packet queues!\n",
                 (p_from->next == NULL), return NTCPIP_ERR_VAL;);
    }
    if((p_to != NULL) && (p_to->len == p_to->tot_len)) {
      /* don't copy more than one packet! */
      NTCPIP__LWIP_ERROR("ntcpip_pbufCopy() does not allow packet queues!\n",
                  (p_to->next == NULL), return NTCPIP_ERR_VAL;);
    }
  } while (p_from);
  NTCPIP__LWIP_DEBUGF(NTCPIP__PBUF_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip_pbufCopy: end of chain reached.\n"));
  return NTCPIP_ERR_OK;
}

/**
 * Copy (part of) the contents of a packet buffer
 * to an application supplied buffer.
 *
 * @param buf the pbuf from which to copy data
 * @param dataptr the application supplied buffer
 * @param len length of data to copy (dataptr must be big enough). No more 
 * than buf->tot_len will be copied, irrespective of len
 * @param offset offset into the packet buffer from where to begin copying len bytes
 * @return the number of bytes copied, or 0 on failure
 */
Uint16
ntcpip_pbufCopyPartial(struct ntcpip_pbuf *buf, void *dataptr, Uint16 len, Uint16 offset)
{
  struct ntcpip_pbuf *p;
  Uint16 left;
  Uint16 buf_copy_len;
  Uint16 copied_total = 0;

  NTCPIP__LWIP_ERROR("ntcpip_pbufCopyPartial: invalid buf", (buf != NULL), return 0;);
  NTCPIP__LWIP_ERROR("ntcpip_pbufCopyPartial: invalid dataptr", (dataptr != NULL), return 0;);

  left = 0;

  if((buf == NULL) || (dataptr == NULL)) {
    return 0;
  }

  /* Note some systems use byte copy if dataptr or one of the pbuf payload pointers are unaligned. */
  for(p = buf; len != 0 && p != NULL; p = p->next) {
    if ((offset != 0) && (offset >= p->len)) {
      /* don't copy from this buffer -> on to the next */
      offset -= p->len;
    } else {
      /* copy from this buffer. maybe only partially. */
      buf_copy_len = p->len - offset;
      if (buf_copy_len > len)
          buf_copy_len = len;
      /* copy the necessary parts of the buffer */
      NTCPIP__MEMCPY(&((char*)dataptr)[left], &((char*)p->payload)[offset], buf_copy_len);
      copied_total += buf_copy_len;
      left += buf_copy_len;
      len -= buf_copy_len;
      offset = 0;
    }
  }
  return copied_total;
}

/**
 * Copy application supplied data into a pbuf.
 * This function can only be used to copy the equivalent of buf->tot_len data.
 *
 * @param buf pbuf to fill with data
 * @param dataptr application supplied data buffer
 * @param len length of the application supplied data buffer
 *
 * @return NTCPIP_ERR_OK if successful, NTCPIP_ERR_MEM if the pbuf is not big enough
 */
ntcpip_Err
ntpcip__pbufTake(struct ntcpip_pbuf *buf, const void *dataptr, Uint16 len)
{
  struct ntcpip_pbuf *p;
  Uint16 buf_copy_len;
  Uint16 total_copy_len = len;
  Uint16 copied_total = 0;

  NTCPIP__LWIP_ERROR("pbuf_take: invalid buf", (buf != NULL), return 0;);
  NTCPIP__LWIP_ERROR("pbuf_take: invalid dataptr", (dataptr != NULL), return 0;);

  if ((buf == NULL) || (dataptr == NULL) || (buf->tot_len < len)) {
    return NTCPIP_ERR_ARG;
  }

  /* Note some systems use byte copy if dataptr or one of the pbuf payload pointers are unaligned. */
  for(p = buf; total_copy_len != 0; p = p->next) {
    NTCPIP__LWIP_ASSERT("pbuf_take: invalid pbuf", p != NULL);
    buf_copy_len = total_copy_len;
    if (buf_copy_len > p->len) {
      /* this pbuf cannot hold all remaining data */
      buf_copy_len = p->len;
    }
    /* copy the necessary parts of the buffer */
    NTCPIP__MEMCPY(p->payload, &((const char*)dataptr)[copied_total], buf_copy_len);
    total_copy_len -= buf_copy_len;
    copied_total += buf_copy_len;
  }
  NTCPIP__LWIP_ASSERT("did not copy all data", total_copy_len == 0 && copied_total == len);
  return NTCPIP_ERR_OK;
}

/**
 * Creates a single pbuf out of a queue of pbufs.
 *
 * @remark: The source pbuf 'p' is not freed by this function because that can
 *          be illegal in some places!
 *
 * @param p the source pbuf
 * @param layer ntcpip__PbufLayer of the new pbuf
 *
 * @return a new, single pbuf (p->next is NULL)
 *         or the old pbuf if allocation fails
 */
struct ntcpip_pbuf*
ntcpip__pbufCoalesce(struct ntcpip_pbuf *p, ntcpip__PbufLayer layer)
{
  struct ntcpip_pbuf *q;
  ntcpip_Err err;
  if (p->next == NULL) {
    return p;
  }
  q = ntcpip_pbufAlloc(layer, p->tot_len, NTCPIP_PBUF_RAM);
  if (q == NULL) {
    /* @todo: what do we do now? */
    return p;
  }
  err = ntcpip_pbufCopy(q, p);
  NTCPIP__LWIP_ASSERT("ntcpip_pbufCopy failed", err == NTCPIP_ERR_OK);
  ntcpip_pbufFree(p);
  return q;
}

