/**
 * @file
 * Transmission Control Protocol, incoming traffic
 *
 * The input processing functions of the TCP layer.
 *
 * These functions are generally called in the order (ntcpip__ipInput() ->)
 * ntcpip__tcpInput() -> * tcp_process() -> tcp_receive() (-> application).
 * 
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_TCP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/tcp.h"
#include "../def.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "../mem.h"
#include "../memp.h"
#include "ntcpip/inet.h"
#include "../inet_chksum.h"
#include "../stats.h"
#include "ntcpip/snmp.h"
#include "../../arch/perf.h"

/* These variables are global to all functions involved in the input
   processing of TCP segments. They are set by the ntcpip__tcpInput()
   function. */
static struct ntcpip__tcpSeg inseg;
static struct ntcpip__tcpHdr *tcphdr;
static struct ip_hdr *iphdr;
static Uint32 seqno, ackno;
static Uint8 flags;
static Uint16 tcplen;

static Uint8 recv_flags;
static struct ntcpip_pbuf *recv_data;

struct ntcpip__tcpPcb *ntcpip__tcpInputPcb;

/* Forward declarations. */
static ntcpip_Err tcp_process(struct ntcpip__tcpPcb *pcb);
static void tcp_receive(struct ntcpip__tcpPcb *pcb);
static void tcp_parseopt(struct ntcpip__tcpPcb *pcb);

static ntcpip_Err tcp_listen_input(struct ntcpip__tcpPcbListen *pcb);
static ntcpip_Err tcp_timewait_input(struct ntcpip__tcpPcb *pcb);

/**
 * The initial input processing of TCP. It verifies the TCP header, demultiplexes
 * the segment between the PCBs and passes it on to tcp_process(), which implements
 * the TCP finite state machine. This function is called by the IP layer (in
 * ntcpip__ipInput()).
 *
 * @param p received TCP segment to process (p->payload pointing to the IP header)
 * @param inp network interface on which this segment was received
 */
void
ntcpip__tcpInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp)
{
  struct ntcpip__tcpPcb *pcb, *prev;
  struct ntcpip__tcpPcbListen *lpcb;
  Uint8 hdrlen;
  ntcpip_Err err;

  NTCPIP__PERF_START;

  TCP_STATS_INC(tcp.recv);
  ntcpip__snmpIncTcpinsegs();

  iphdr = p->payload;
  tcphdr = (struct ntcpip__tcpHdr *)((Uint8 *)p->payload + IPH_HL(iphdr) * 4);

#if NTCPIP__TCP_INPUT_DEBUG
  tcp_debug_print(tcphdr);
#endif

  /* remove header from payload */
  if (ntcpip__pbufHeader(p, -((Int16)(IPH_HL(iphdr) * 4))) || (p->tot_len < sizeof(struct ntcpip__tcpHdr))) {
    /* drop short packets */
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: short packet (%"U16_F" bytes) discarded\n", p->tot_len));
    TCP_STATS_INC(tcp.lenerr);
    TCP_STATS_INC(tcp.drop);
    ntcpip__snmpIncTcpinerrs();
    ntcpip_pbufFree(p);
    return;
  }

  /* Don't even process incoming broadcasts/multicasts. */
  if (ntcpip__ipaddrIsBroadcast(&(iphdr->dest), inp) ||
      ntcpip_ipaddrIsMulticast(&(iphdr->dest))) {
    TCP_STATS_INC(tcp.proterr);
    TCP_STATS_INC(tcp.drop);
    ntcpip__snmpIncTcpinerrs();
    ntcpip_pbufFree(p);
    return;
  }

#if NTCPIP__CHECKSUM_CHECK_TCP
  /* Verify TCP checksum. */
  if (ntcpip__inetChksumPseudo(p, (struct ntcpip_ipAddr *)&(iphdr->src),
      (struct ntcpip_ipAddr *)&(iphdr->dest),
      IP_PROTO_TCP, p->tot_len) != 0) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: packet discarded due to failing checksum 0x%04"X16_F"\n",
        ntcpip__inetChksumPseudo(p, (struct ntcpip_ipAddr *)&(iphdr->src), (struct ntcpip_ipAddr *)&(iphdr->dest),
      IP_PROTO_TCP, p->tot_len)));
#if NTCPIP__TCP_DEBUG
    tcp_debug_print(tcphdr);
#endif /* NTCPIP__TCP_DEBUG */
    TCP_STATS_INC(tcp.chkerr);
    TCP_STATS_INC(tcp.drop);
    ntcpip__snmpIncTcpinerrs();
    ntcpip_pbufFree(p);
    return;
  }
#endif

  /* Move the payload pointer in the pbuf so that it points to the
     TCP data instead of the TCP header. */
  hdrlen = NTCPIP__TCPH_HDRLEN(tcphdr);
  if(ntcpip__pbufHeader(p, -(hdrlen * 4))){
    /* drop short packets */
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: short packet\n"));
    TCP_STATS_INC(tcp.lenerr);
    TCP_STATS_INC(tcp.drop);
    ntcpip__snmpIncTcpinerrs();
    ntcpip_pbufFree(p);
    return;
  }

  /* Convert fields in TCP header to host byte order. */
  tcphdr->src = ntcpip_ntohs(tcphdr->src);
  tcphdr->dest = ntcpip_ntohs(tcphdr->dest);
  seqno = tcphdr->seqno = ntcpip_ntohl(tcphdr->seqno);
  ackno = tcphdr->ackno = ntcpip_ntohl(tcphdr->ackno);
  tcphdr->wnd = ntcpip_ntohs(tcphdr->wnd);

  flags = NTCPIP__TCPH_FLAGS(tcphdr);
  tcplen = p->tot_len + ((flags & (NTCPIP__TCP_FIN | NTCPIP__TCP_SYN)) ? 1 : 0);

  /* Demultiplex an incoming segment. First, we check if it is destined
     for an active connection. */
  prev = NULL;

  
  for(pcb = ntcpip__tcpActivePcbs; pcb != NULL; pcb = pcb->next) {
    NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: active pcb->state != NTCPIP_CLOSED", pcb->state != NTCPIP_CLOSED);
    NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: active pcb->state != TIME-WAIT", pcb->state != NTCPIP_TIME_WAIT);
    NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: active pcb->state != NTCPIP_LISTEN", pcb->state != NTCPIP_LISTEN);
    if (pcb->remote_port == tcphdr->src &&
       pcb->local_port == tcphdr->dest &&
       ntcpip_ipaddrCmp(&(pcb->remote_ip), &(iphdr->src)) &&
       ntcpip_ipaddrCmp(&(pcb->local_ip), &(iphdr->dest))) {

      /* Move this PCB to the front of the list so that subsequent
         lookups will be faster (we exploit locality in TCP segment
         arrivals). */
      NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: pcb->next != pcb (before cache)", pcb->next != pcb);
      if (prev != NULL) {
        prev->next = pcb->next;
        pcb->next = ntcpip__tcpActivePcbs;
        ntcpip__tcpActivePcbs = pcb;
      }
      NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: pcb->next != pcb (after cache)", pcb->next != pcb);
      break;
    }
    prev = pcb;
  }

  if (pcb == NULL) {
    /* If it did not go to an active connection, we check the connections
       in the TIME-WAIT state. */
    for(pcb = ntcpip__tcpTwPcb; pcb != NULL; pcb = pcb->next) {
      NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: TIME-WAIT pcb->state == TIME-WAIT", pcb->state == NTCPIP_TIME_WAIT);
      if (pcb->remote_port == tcphdr->src &&
         pcb->local_port == tcphdr->dest &&
         ntcpip_ipaddrCmp(&(pcb->remote_ip), &(iphdr->src)) &&
         ntcpip_ipaddrCmp(&(pcb->local_ip), &(iphdr->dest))) {
        /* We don't really care enough to move this PCB to the front
           of the list since we are not very likely to receive that
           many segments for connections in TIME-WAIT. */
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: packed for TIME_WAITing connection.\n"));
        tcp_timewait_input(pcb);
        ntcpip_pbufFree(p);
        return;
      }
    }

  /* Finally, if we still did not get a match, we check all PCBs that
     are LISTENing for incoming connections. */
    prev = NULL;
    for(lpcb = ntcpip__tcpListenPcbs.listen_pcbs; lpcb != NULL; lpcb = lpcb->next) {
      if ((ntcpip_ipaddrIsAny(&(lpcb->local_ip)) ||
        ntcpip_ipaddrCmp(&(lpcb->local_ip), &(iphdr->dest))) &&
        lpcb->local_port == tcphdr->dest) {
        /* Move this PCB to the front of the list so that subsequent
           lookups will be faster (we exploit locality in TCP segment
           arrivals). */
        if (prev != NULL) {
          ((struct ntcpip__tcpPcbListen *)prev)->next = lpcb->next;
                /* our successor is the remainder of the listening list */
          lpcb->next = ntcpip__tcpListenPcbs.listen_pcbs;
                /* put this listening pcb at the head of the listening list */
          ntcpip__tcpListenPcbs.listen_pcbs = lpcb;
        }
      
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: packed for LISTENing connection.\n"));
        tcp_listen_input(lpcb);
        ntcpip_pbufFree(p);
        return;
      }
      prev = (struct ntcpip__tcpPcb *)lpcb;
    }
  }

#if NTCPIP__TCP_INPUT_DEBUG
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("+-+-+-+-+-+-+-+-+-+-+-+-+-+- ntcpip__tcpInput: flags "));
  tcp_debug_print_flags(NTCPIP__TCPH_FLAGS(tcphdr));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n"));
#endif /* NTCPIP__TCP_INPUT_DEBUG */


  if (pcb != NULL) {
    /* The incoming segment belongs to a connection. */
#if NTCPIP__TCP_INPUT_DEBUG
#if NTCPIP__TCP_DEBUG
    tcp_debug_print_state(pcb->state);
#endif /* NTCPIP__TCP_DEBUG */
#endif /* NTCPIP__TCP_INPUT_DEBUG */

    /* Set up a ntcpip__tcpSeg structure. */
    inseg.next = NULL;
    inseg.len = p->tot_len;
    inseg.dataptr = p->payload;
    inseg.p = p;
    inseg.tcphdr = tcphdr;

    recv_data = NULL;
    recv_flags = 0;

    /* If there is data which was previously "refused" by upper layer */
    if (pcb->refused_data != NULL) {
      /* Notify again application with data previously received. */
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: notify kept packet\n"));
      NTCPIP__TCP_EVENT_RECV(pcb, pcb->refused_data, NTCPIP_ERR_OK, err);
      if (err == NTCPIP_ERR_OK) {
        pcb->refused_data = NULL;
      } else {
        /* drop incoming packets, because pcb is "full" */
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: drop incoming packets, because pcb is \"full\"\n"));
        TCP_STATS_INC(tcp.drop);
        ntcpip__snmpIncTcpinerrs();
        ntcpip_pbufFree(p);
        return;
      }
    }
    ntcpip__tcpInputPcb = pcb;
    err = tcp_process(pcb);
    /* A return value of NTCPIP_ERR_ABRT means that ntcpip_tcpAbort() was called
       and that the pcb has been freed. If so, we don't do anything. */
    if (err != NTCPIP_ERR_ABRT) {
      if (recv_flags & NTCPIP_TF_RESET) {
        /* NTCPIP_TF_RESET means that the connection was reset by the other
           end. We then call the error callback to inform the
           application that the connection is dead before we
           deallocate the PCB. */
        NTCPIP__TCP_EVENT_ERR(pcb->errf, pcb->callback_arg, NTCPIP_ERR_RST);
        ntcpip__tcpPcbRemove(&ntcpip__tcpActivePcbs, pcb);
        ntcpip__mempFree(MEMP_TCP_PCB, pcb);
      } else if (recv_flags & NTCPIP_TF_CLOSED) {
        /* The connection has been closed and we will deallocate the
           PCB. */
        ntcpip__tcpPcbRemove(&ntcpip__tcpActivePcbs, pcb);
        ntcpip__mempFree(MEMP_TCP_PCB, pcb);
      } else {
        err = NTCPIP_ERR_OK;
        /* If the application has registered a "sent" function to be
           called when new send buffer space is available, we call it
           now. */
        if (pcb->acked > 0) {
          NTCPIP__TCP_EVENT_SENT(pcb, pcb->acked, err);
        }
      
        if (recv_data != NULL) {
          if(flags & NTCPIP__TCP_PSH) {
            recv_data->flags |= NTCPIP__PBUF_FLAG_PUSH;
          }

          /* Notify application that data has been received. */
          NTCPIP__TCP_EVENT_RECV(pcb, recv_data, NTCPIP_ERR_OK, err);

          /* If the upper layer can't receive this data, store it */
          if (err != NTCPIP_ERR_OK) {
            pcb->refused_data = recv_data;
            NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpInput: keep incoming packet, because pcb is \"full\"\n"));
          }
        }

        /* If a FIN segment was received, we call the callback
           function with a NULL buffer to indicate EOF. */
        if (recv_flags & NTCPIP_TF_GOT_FIN) {
          NTCPIP__TCP_EVENT_RECV(pcb, NULL, NTCPIP_ERR_OK, err);
        }

        ntcpip__tcpInputPcb = NULL;
        /* Try to send something out. */
        ntcpip__tcpOutput(pcb);
#if NTCPIP__TCP_INPUT_DEBUG
#if NTCPIP__TCP_DEBUG
        tcp_debug_print_state(pcb->state);
#endif /* NTCPIP__TCP_DEBUG */
#endif /* NTCPIP__TCP_INPUT_DEBUG */
      }
    }
    ntcpip__tcpInputPcb = NULL;


    /* give up our reference to inseg.p */
    if (inseg.p != NULL)
    {
      ntcpip_pbufFree(inseg.p);
      inseg.p = NULL;
    }
  } else {

    /* If no matching PCB was found, send a TCP RST (reset) to the
       sender. */
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RST_DEBUG, ("ntcpip__tcpInput: no PCB match found, resetting.\n"));
    if (!(NTCPIP__TCPH_FLAGS(tcphdr) & NTCPIP__TCP_RST)) {
      TCP_STATS_INC(tcp.proterr);
      TCP_STATS_INC(tcp.drop);
      ntcpip__tcpRst(ackno, seqno + tcplen,
        &(iphdr->dest), &(iphdr->src),
        tcphdr->dest, tcphdr->src);
    }
    ntcpip_pbufFree(p);
  }

  NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: tcp_pcbs_sane()", tcp_pcbs_sane());
  NTCPIP__PERF_STOP("ntcpip__tcpInput");
}

/**
 * Called by ntcpip__tcpInput() when a segment arrives for a listening
 * connection (from ntcpip__tcpInput()).
 *
 * @param pcb the ntcpip__tcpPcbListen for which a segment arrived
 * @return NTCPIP_ERR_OK if the segment was processed
 *         another ntcpip_Err on error
 *
 * @note the return value is not (yet?) used in ntcpip__tcpInput()
 * @note the segment which arrived is saved in global variables, therefore only the pcb
 *       involved is passed as a parameter to this function
 */
static ntcpip_Err
tcp_listen_input(struct ntcpip__tcpPcbListen *pcb)
{
  struct ntcpip__tcpPcb *npcb;
  ntcpip_Err rc;

  /* In the NTCPIP_LISTEN state, we check for incoming SYN segments,
     creates a new PCB, and responds with a SYN|ACK. */
  if (flags & NTCPIP__TCP_ACK) {
    /* For incoming segments with the ACK flag set, respond with a
       RST. */
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RST_DEBUG, ("tcp_listen_input: ACK in NTCPIP_LISTEN, sending reset\n"));
    ntcpip__tcpRst(ackno + 1, seqno + tcplen,
      &(iphdr->dest), &(iphdr->src),
      tcphdr->dest, tcphdr->src);
  } else if (flags & NTCPIP__TCP_SYN) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("TCP connection request %"U16_F" -> %"U16_F".\n", tcphdr->src, tcphdr->dest));
#if NTCPIP__TCP_LISTEN_BACKLOG
    if (pcb->accepts_pending >= pcb->backlog) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("tcp_listen_input: listen backlog exceeded for port %"U16_F"\n", tcphdr->dest));
      return NTCPIP_ERR_ABRT;
    }
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
    npcb = ntcpip_tcpAlloc(pcb->prio);
    /* If a new PCB could not be created (probably due to lack of memory),
       we don't do anything, but rely on the sender will retransmit the
       SYN at a time when we have more memory available. */
    if (npcb == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("tcp_listen_input: could not allocate PCB\n"));
      TCP_STATS_INC(tcp.memerr);
      return NTCPIP_ERR_MEM;
    }
#if NTCPIP__TCP_LISTEN_BACKLOG
    pcb->accepts_pending++;
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
    /* Set up the new PCB. */
    ntcpip_ipaddrSet(&(npcb->local_ip), &(iphdr->dest));
    npcb->local_port = pcb->local_port;
    ntcpip_ipaddrSet(&(npcb->remote_ip), &(iphdr->src));
    npcb->remote_port = tcphdr->src;
    npcb->state = NTCPIP_SYN_RCVD;
    npcb->rcv_nxt = seqno + 1;
    npcb->rcv_ann_right_edge = npcb->rcv_nxt;
    npcb->snd_wnd = tcphdr->wnd;
    npcb->ssthresh = npcb->snd_wnd;
    npcb->snd_wl1 = seqno - 1;/* initialise to seqno-1 to force window update */
    npcb->callback_arg = pcb->callback_arg;
#if NTCPIP__LWIP_CALLBACK_API
    npcb->accept = pcb->accept;
#endif /* NTCPIP__LWIP_CALLBACK_API */
    /* inherit socket options */
    npcb->so_options = pcb->so_options & (NTCPIP_SOF_DEBUG|NTCPIP_SOF_DONTROUTE|NTCPIP_SOF_KEEPALIVE|NTCPIP_SOF_OOBINLINE|NTCPIP_SOF_LINGER);
    /* Register the new PCB so that we can begin receiving segments
       for it. */
    NTCPIP__TCP_REG(&ntcpip__tcpActivePcbs, npcb);

    /* Parse any options in the SYN. */
    tcp_parseopt(npcb);
#if NTCPIP__TCP_CALCULATE_EFF_SEND_MSS
    npcb->mss = ntcpip__tcpEffSendMss(npcb->mss, &(npcb->remote_ip));
#endif /* NTCPIP__TCP_CALCULATE_EFF_SEND_MSS */

    ntcpip__snmpIncTcppassiveopens();

    /* Send a SYN|ACK together with the MSS option. */
    rc = ntcpip__tcpEnqueue(npcb, NULL, 0, NTCPIP__TCP_SYN | NTCPIP__TCP_ACK, 0, TF_SEG_OPTS_MSS
#if LWIP_TCP_TIMESTAMPS
      /* and maybe include the TIMESTAMP option */
     | (npcb->flags & NTCPIP_TF_TIMESTAMP ? TF_SEG_OPTS_TS : 0)
#endif
      );
    if (rc != NTCPIP_ERR_OK) {
    	ntcpip__tcpAbandon(npcb, 0);
      return rc;
    }
    return ntcpip__tcpOutput(npcb);
  }
  return NTCPIP_ERR_OK;
}

/**
 * Called by ntcpip__tcpInput() when a segment arrives for a connection in
 * NTCPIP_TIME_WAIT.
 *
 * @param pcb the tcp_pcb for which a segment arrived
 *
 * @note the segment which arrived is saved in global variables, therefore only the pcb
 *       involved is passed as a parameter to this function
 */
static ntcpip_Err
tcp_timewait_input(struct ntcpip__tcpPcb *pcb)
{
  /* RFC 1337: in NTCPIP_TIME_WAIT, ignore RST and ACK FINs + any 'acceptable' segments */
  /* RFC 793 3.9 Event Processing - Segment Arrives:
   * - first check sequence number - we skip that one in NTCPIP_TIME_WAIT (always
   *   acceptable since we only send ACKs)
   * - second check the RST bit (... return) */
  if (flags & NTCPIP__TCP_RST)  {
    return NTCPIP_ERR_OK;
  }
  /* - fourth, check the SYN bit, */
  if (flags & NTCPIP__TCP_SYN) {
    /* If an incoming segment is not acceptable, an acknowledgment
       should be sent in reply */
    if (NTCPIP__TCP_SEQ_BETWEEN(seqno, pcb->rcv_nxt, pcb->rcv_nxt+pcb->rcv_wnd)) {
      /* If the SYN is in the window it is an error, send a reset */
      ntcpip__tcpRst(ackno, seqno + tcplen, &(iphdr->dest), &(iphdr->src),
        tcphdr->dest, tcphdr->src);
      return NTCPIP_ERR_OK;
    }
  } else if (flags & NTCPIP__TCP_FIN) {
    /* - eighth, check the FIN bit: Remain in the TIME-WAIT state.
         Restart the 2 MSL time-wait timeout.*/
    pcb->tmr = ntcpip__tcpTicks;
  }

  if ((tcplen > 0))  {
    /* Acknowledge data, FIN or out-of-window SYN */
    pcb->flags |= NTCPIP_TF_ACK_NOW;
    return ntcpip__tcpOutput(pcb);
  }
  return NTCPIP_ERR_OK;
}

/**
 * Implements the TCP state machine. Called by ntcpip__tcpInput. In some
 * states tcp_receive() is called to receive data. The ntcpip__tcpSeg
 * argument will be freed by the caller (ntcpip__tcpInput()) unless the
 * recv_data pointer in the pcb is set.
 *
 * @param pcb the tcp_pcb for which a segment arrived
 *
 * @note the segment which arrived is saved in global variables, therefore only the pcb
 *       involved is passed as a parameter to this function
 */
static ntcpip_Err
tcp_process(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip__tcpSeg *rseg;
  Uint8 acceptable = 0;
  ntcpip_Err err;

  err = NTCPIP_ERR_OK;

  /* Process incoming RST segments. */
  if (flags & NTCPIP__TCP_RST) {
    /* First, determine if the reset is acceptable. */
    if (pcb->state == NTCPIP_SYN_SENT) {
      if (ackno == pcb->snd_nxt) {
        acceptable = 1;
      }
    } else {
      if (NTCPIP__TCP_SEQ_BETWEEN(seqno, pcb->rcv_nxt, 
                          pcb->rcv_nxt+pcb->rcv_wnd)) {
        acceptable = 1;
      }
    }

    if (acceptable) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_process: Connection RESET\n"));
      NTCPIP__LWIP_ASSERT("ntcpip__tcpInput: pcb->state != NTCPIP_CLOSED", pcb->state != NTCPIP_CLOSED);
      recv_flags |= NTCPIP_TF_RESET;
      pcb->flags &= ~NTCPIP_TF_ACK_DELAY;
      return NTCPIP_ERR_RST;
    } else {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_process: unacceptable reset seqno %"U32_F" rcv_nxt %"U32_F"\n",
       seqno, pcb->rcv_nxt));
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("tcp_process: unacceptable reset seqno %"U32_F" rcv_nxt %"U32_F"\n",
       seqno, pcb->rcv_nxt));
      return NTCPIP_ERR_OK;
    }
  }

  if ((flags & NTCPIP__TCP_SYN) && (pcb->state != NTCPIP_SYN_SENT && pcb->state != NTCPIP_SYN_RCVD)) { 
    /* Cope with new connection attempt after remote end crashed */
    ntcpip__tcpAckNow(pcb);
    return NTCPIP_ERR_OK;
  }
  
  /* Update the PCB (in)activity timer. */
  pcb->tmr = ntcpip__tcpTicks;
  pcb->keep_cnt_sent = 0;

  tcp_parseopt(pcb);

  /* Do different things depending on the TCP state. */
  switch (pcb->state) {
  case NTCPIP_SYN_SENT:
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("SYN-SENT: ackno %"U32_F" pcb->snd_nxt %"U32_F" unacked %"U32_F"\n", ackno,
     pcb->snd_nxt, ntcpip_ntohl(pcb->unacked->tcphdr->seqno)));
    /* received SYN ACK with expected sequence number? */
    if ((flags & NTCPIP__TCP_ACK) && (flags & NTCPIP__TCP_SYN)
        && ackno == ntcpip_ntohl(pcb->unacked->tcphdr->seqno) + 1) {
      pcb->snd_buf++;
      pcb->rcv_nxt = seqno + 1;
      pcb->rcv_ann_right_edge = pcb->rcv_nxt;
      pcb->lastack = ackno;
      pcb->snd_wnd = tcphdr->wnd;
      pcb->snd_wl1 = seqno - 1; /* initialise to seqno - 1 to force window update */
      pcb->state = NTCPIP_ESTABLISHED;

#if NTCPIP__TCP_CALCULATE_EFF_SEND_MSS
      pcb->mss = ntcpip__tcpEffSendMss(pcb->mss, &(pcb->remote_ip));
#endif /* NTCPIP__TCP_CALCULATE_EFF_SEND_MSS */

      /* Set ssthresh again after changing pcb->mss (already set in ntcpip__tcpConnect
       * but for the default value of pcb->mss) */
      pcb->ssthresh = pcb->mss * 10;

      pcb->cwnd = ((pcb->cwnd == 1) ? (pcb->mss * 2) : pcb->mss);
      NTCPIP__LWIP_ASSERT("pcb->snd_queuelen > 0", (pcb->snd_queuelen > 0));
      --pcb->snd_queuelen;
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG, ("tcp_process: SYN-SENT --queuelen %"U16_F"\n", (Uint16)pcb->snd_queuelen));
      rseg = pcb->unacked;
      pcb->unacked = rseg->next;

      /* If there's nothing left to acknowledge, stop the retransmit
         timer, otherwise reset it to start again */
      if(pcb->unacked == NULL)
        pcb->rtime = -1;
      else {
        pcb->rtime = 0;
        pcb->nrtx = 0;
      }

      ntcpip__tcpSegFree(rseg);

      /* Call the user specified function to call when sucessfully
       * connected. */
      NTCPIP__TCP_EVENT_CONNECTED(pcb, NTCPIP_ERR_OK, err);
      ntcpip__tcpAckNow(pcb);
    }
    /* received ACK? possibly a half-open connection */
    else if (flags & NTCPIP__TCP_ACK) {
      /* send a RST to bring the other side in a non-synchronized state. */
      ntcpip__tcpRst(ackno, seqno + tcplen, &(iphdr->dest), &(iphdr->src),
        tcphdr->dest, tcphdr->src);
    }
    break;
  case NTCPIP_SYN_RCVD:
    if (flags & NTCPIP__TCP_ACK) {
      /* expected ACK number? */
      if (NTCPIP__TCP_SEQ_BETWEEN(ackno, pcb->lastack+1, pcb->snd_nxt)) {
        Uint16 old_cwnd;
        pcb->state = NTCPIP_ESTABLISHED;
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("TCP connection established %"U16_F" -> %"U16_F".\n", inseg.tcphdr->src, inseg.tcphdr->dest));
#if NTCPIP__LWIP_CALLBACK_API
        NTCPIP__LWIP_ASSERT("pcb->accept != NULL", pcb->accept != NULL);
#endif
        /* Call the accept function. */
        NTCPIP__TCP_EVENT_ACCEPT(pcb, NTCPIP_ERR_OK, err);
        if (err != NTCPIP_ERR_OK) {
          /* If the accept function returns with an error, we abort
           * the connection. */
          ntcpip_tcpAbort(pcb);
          return NTCPIP_ERR_ABRT;
        }
        old_cwnd = pcb->cwnd;
        /* If there was any data contained within this ACK,
         * we'd better pass it on to the application as well. */
        tcp_receive(pcb);

        /* Prevent ACK for SYN to generate a sent event */
        if (pcb->acked != 0) {
          pcb->acked--;
        }

        pcb->cwnd = ((old_cwnd == 1) ? (pcb->mss * 2) : pcb->mss);

        if (recv_flags & NTCPIP_TF_GOT_FIN) {
          ntcpip__tcpAckNow(pcb);
          pcb->state = NTCPIP_CLOSE_WAIT;
        }
      }
      /* incorrect ACK number */
      else {
        /* send RST */
        ntcpip__tcpRst(ackno, seqno + tcplen, &(iphdr->dest), &(iphdr->src),
                tcphdr->dest, tcphdr->src);
      }
    } else if ((flags & NTCPIP__TCP_SYN) && (seqno == pcb->rcv_nxt - 1)) {
      /* Looks like another copy of the SYN - retransmit our SYN-ACK */
      ntcpip__tcpRexmit(pcb);
    }
    break;
  case NTCPIP_CLOSE_WAIT:
    /* FALLTHROUGH */
  case NTCPIP_ESTABLISHED:
    tcp_receive(pcb);
    if (recv_flags & NTCPIP_TF_GOT_FIN) { /* passive close */
      ntcpip__tcpAckNow(pcb);
      pcb->state = NTCPIP_CLOSE_WAIT;
    }
    break;
  case NTCPIP_FIN_WAIT_1:
    tcp_receive(pcb);
    if (recv_flags & NTCPIP_TF_GOT_FIN) {
      if ((flags & NTCPIP__TCP_ACK) && (ackno == pcb->snd_nxt)) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG,
          ("TCP connection closed: NTCPIP_FIN_WAIT_1 %"U16_F" -> %"U16_F".\n", inseg.tcphdr->src, inseg.tcphdr->dest));
        ntcpip__tcpAckNow(pcb);
        ntcpip__tcpPcbPurge(pcb);
        NTCPIP__TCP_RMV(&ntcpip__tcpActivePcbs, pcb);
        pcb->state = NTCPIP_TIME_WAIT;
        NTCPIP__TCP_REG(&ntcpip__tcpTwPcb, pcb);
      } else {
        ntcpip__tcpAckNow(pcb);
        pcb->state = NTCPIP_CLOSING;
      }
    } else if ((flags & NTCPIP__TCP_ACK) && (ackno == pcb->snd_nxt)) {
      pcb->state = NTCPIP_FIN_WAIT_2;
    }
    break;
  case NTCPIP_FIN_WAIT_2:
    tcp_receive(pcb);
    if (recv_flags & NTCPIP_TF_GOT_FIN) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("TCP connection closed: NTCPIP_FIN_WAIT_2 %"U16_F" -> %"U16_F".\n", inseg.tcphdr->src, inseg.tcphdr->dest));
      ntcpip__tcpAckNow(pcb);
      ntcpip__tcpPcbPurge(pcb);
      NTCPIP__TCP_RMV(&ntcpip__tcpActivePcbs, pcb);
      pcb->state = NTCPIP_TIME_WAIT;
      NTCPIP__TCP_REG(&ntcpip__tcpTwPcb, pcb);
    }
    break;
  case NTCPIP_CLOSING:
    tcp_receive(pcb);
    if (flags & NTCPIP__TCP_ACK && ackno == pcb->snd_nxt) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("TCP connection closed: NTCPIP_CLOSING %"U16_F" -> %"U16_F".\n", inseg.tcphdr->src, inseg.tcphdr->dest));
      ntcpip__tcpPcbPurge(pcb);
      NTCPIP__TCP_RMV(&ntcpip__tcpActivePcbs, pcb);
      pcb->state = NTCPIP_TIME_WAIT;
      NTCPIP__TCP_REG(&ntcpip__tcpTwPcb, pcb);
    }
    break;
  case NTCPIP_LAST_ACK:
    tcp_receive(pcb);
    if (flags & NTCPIP__TCP_ACK && ackno == pcb->snd_nxt) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("TCP connection closed: NTCPIP_LAST_ACK %"U16_F" -> %"U16_F".\n", inseg.tcphdr->src, inseg.tcphdr->dest));
      /* bugfix #21699: don't set pcb->state to NTCPIP_CLOSED here or we risk leaking segments */
      recv_flags |= NTCPIP_TF_CLOSED;
    }
    break;
  default:
    break;
  }
  return NTCPIP_ERR_OK;
}

#if NTCPIP__TCP_QUEUE_OOSEQ
/**
 * Insert segment into the list (segments covered with new one will be deleted)
 *
 * Called from tcp_receive()
 */
static void
tcp_oos_insert_segment(struct ntcpip__tcpSeg *cseg, struct ntcpip__tcpSeg *next)
{
  struct ntcpip__tcpSeg *old_seg;

  if (NTCPIP__TCPH_FLAGS(cseg->tcphdr) & NTCPIP__TCP_FIN) {
    /* received segment overlaps all following segments */
    ntcpip__tcpSegsFree(next);
    next = NULL;
  }
  else {
    /* delete some following segments
       oos queue may have segments with FIN flag */
    while (next &&
           NTCPIP__TCP_SEQ_GEQ((seqno + cseg->len),
                      (next->tcphdr->seqno + next->len))) {
      /* cseg with FIN already processed */
      if (NTCPIP__TCPH_FLAGS(next->tcphdr) & NTCPIP__TCP_FIN) {
        NTCPIP__TCPH_FLAGS_SET(cseg->tcphdr, NTCPIP__TCPH_FLAGS(cseg->tcphdr) | NTCPIP__TCP_FIN);
      }
      old_seg = next;
      next = next->next;
      ntcpip__tcpSegFree(old_seg);
    }
    if (next &&
        NTCPIP__TCP_SEQ_GT(seqno + cseg->len, next->tcphdr->seqno)) {
      /* We need to trim the incoming segment. */
      cseg->len = (Uint16)(next->tcphdr->seqno - seqno);
      ntcpip_pbufRealloc(cseg->p, cseg->len);
    }
  }
  cseg->next = next;
}
#endif

/**
 * Called by tcp_process. Checks if the given segment is an ACK for outstanding
 * data, and if so frees the memory of the buffered data. Next, is places the
 * segment on any of the receive queues (pcb->recved or pcb->ooseq). If the segment
 * is buffered, the pbuf is referenced by ntcpip_pbufRef so that it will not be freed until
 * i it has been removed from the buffer.
 *
 * If the incoming segment constitutes an ACK for a segment that was used for RTT
 * estimation, the RTT is estimated here as well.
 *
 * Called from tcp_process().
 */
static void
tcp_receive(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip__tcpSeg *next;
#if NTCPIP__TCP_QUEUE_OOSEQ
  struct ntcpip__tcpSeg *prev, *cseg;
#endif
  struct ntcpip_pbuf *p;
  Int32 off;
  Int16 m;
  Uint32 right_wnd_edge;
  Uint16 new_tot_len;
  int found_dupack = 0;

  if (flags & NTCPIP__TCP_ACK) {
    right_wnd_edge = pcb->snd_wnd + pcb->snd_wl2;

    /* Update window. */
    if (NTCPIP__TCP_SEQ_LT(pcb->snd_wl1, seqno) ||
       (pcb->snd_wl1 == seqno && NTCPIP__TCP_SEQ_LT(pcb->snd_wl2, ackno)) ||
       (pcb->snd_wl2 == ackno && tcphdr->wnd > pcb->snd_wnd)) {
      pcb->snd_wnd = tcphdr->wnd;
      pcb->snd_wl1 = seqno;
      pcb->snd_wl2 = ackno;
      if (pcb->snd_wnd > 0 && pcb->persist_backoff > 0) {
          pcb->persist_backoff = 0;
      }
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_WND_DEBUG, ("tcp_receive: window update %"U16_F"\n", pcb->snd_wnd));
#if NTCPIP__TCP_WND_DEBUG
    } else {
      if (pcb->snd_wnd != tcphdr->wnd) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_WND_DEBUG, 
                    ("tcp_receive: no window update lastack %"U32_F" ackno %"
                     U32_F" wl1 %"U32_F" seqno %"U32_F" wl2 %"U32_F"\n",
                     pcb->lastack, ackno, pcb->snd_wl1, seqno, pcb->snd_wl2));
      }
#endif /* NTCPIP__TCP_WND_DEBUG */
    }

    /* (From Stevens TCP/IP Illustrated Vol II, p970.) Its only a
     * duplicate ack if:
     * 1) It doesn't ACK new data 
     * 2) length of received packet is zero (i.e. no payload) 
     * 3) the advertised window hasn't changed 
     * 4) There is outstanding unacknowledged data (retransmission timer running)
     * 5) The ACK is == biggest ACK sequence number so far seen (snd_una)
     * 
     * If it passes all five, should process as a dupack: 
     * a) dupacks < 3: do nothing 
     * b) dupacks == 3: fast retransmit 
     * c) dupacks > 3: increase cwnd 
     * 
     * If it only passes 1-3, should reset dupack counter (and add to
     * stats, which we don't do in lwIP)
     *
     * If it only passes 1, should reset dupack counter
     *
     */

    /* Clause 1 */
    if (NTCPIP__TCP_SEQ_LEQ(ackno, pcb->lastack)) {
      pcb->acked = 0;
      /* Clause 2 */
      if (tcplen == 0) {
        /* Clause 3 */
        if (pcb->snd_wl2 + pcb->snd_wnd == right_wnd_edge){
          /* Clause 4 */
          if (pcb->rtime >= 0) {
            /* Clause 5 */
            if (pcb->lastack == ackno) {
              found_dupack = 1;
              if (pcb->dupacks + 1 > pcb->dupacks)
                ++pcb->dupacks;
              if (pcb->dupacks > 3) {
                /* Inflate the congestion window, but not if it means that
                   the value overflows. */
                if ((Uint16)(pcb->cwnd + pcb->mss) > pcb->cwnd) {
                  pcb->cwnd += pcb->mss;
                }
              } else if (pcb->dupacks == 3) {
                /* Do fast retransmit */
            	  ntcpip__tcpRexmitFast(pcb);
              }
            }
          }
        }
      }
      /* If Clause (1) or more is true, but not a duplicate ack, reset
       * count of consecutive duplicate acks */
      if (!found_dupack) {
        pcb->dupacks = 0;
      }
    } else if (NTCPIP__TCP_SEQ_BETWEEN(ackno, pcb->lastack+1, pcb->snd_nxt)){
      /* We come here when the ACK acknowledges new data. */

      /* Reset the "IN Fast Retransmit" flag, since we are no longer
         in fast retransmit. Also reset the congestion window to the
         slow start threshold. */
      if (pcb->flags & NTCPIP_TF_INFR) {
        pcb->flags &= ~NTCPIP_TF_INFR;
        pcb->cwnd = pcb->ssthresh;
      }

      /* Reset the number of retransmissions. */
      pcb->nrtx = 0;

      /* Reset the retransmission time-out. */
      pcb->rto = (pcb->sa >> 3) + pcb->sv;

      /* Update the send buffer space. Diff between the two can never exceed 64K? */
      pcb->acked = (Uint16)(ackno - pcb->lastack);

      pcb->snd_buf += pcb->acked;

      /* Reset the fast retransmit variables. */
      pcb->dupacks = 0;
      pcb->lastack = ackno;

      /* Update the congestion control variables (cwnd and
         ssthresh). */
      if (pcb->state >= NTCPIP_ESTABLISHED) {
        if (pcb->cwnd < pcb->ssthresh) {
          if ((Uint16)(pcb->cwnd + pcb->mss) > pcb->cwnd) {
            pcb->cwnd += pcb->mss;
          }
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_CWND_DEBUG, ("tcp_receive: slow start cwnd %"U16_F"\n", pcb->cwnd));
        } else {
          Uint16 new_cwnd = (pcb->cwnd + pcb->mss * pcb->mss / pcb->cwnd);
          if (new_cwnd > pcb->cwnd) {
            pcb->cwnd = new_cwnd;
          }
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_CWND_DEBUG, ("tcp_receive: congestion avoidance cwnd %"U16_F"\n", pcb->cwnd));
        }
      }
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_receive: ACK for %"U32_F", unacked->seqno %"U32_F":%"U32_F"\n",
                                    ackno,
                                    pcb->unacked != NULL?
                                    ntcpip_ntohl(pcb->unacked->tcphdr->seqno): 0,
                                    pcb->unacked != NULL?
                                    ntcpip_ntohl(pcb->unacked->tcphdr->seqno) + NTCPIP__TCP_TCPLEN(pcb->unacked): 0));

      /* Remove segment from the unacknowledged list if the incoming
         ACK acknowlegdes them. */
      while (pcb->unacked != NULL &&
             NTCPIP__TCP_SEQ_LEQ(ntcpip_ntohl(pcb->unacked->tcphdr->seqno) +
                         NTCPIP__TCP_TCPLEN(pcb->unacked), ackno)) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_receive: removing %"U32_F":%"U32_F" from pcb->unacked\n",
                                      ntcpip_ntohl(pcb->unacked->tcphdr->seqno),
                                      ntcpip_ntohl(pcb->unacked->tcphdr->seqno) +
                                      NTCPIP__TCP_TCPLEN(pcb->unacked)));

        next = pcb->unacked;
        pcb->unacked = pcb->unacked->next;

        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG, ("tcp_receive: queuelen %"U16_F" ... ", (Uint16)pcb->snd_queuelen));
        NTCPIP__LWIP_ASSERT("pcb->snd_queuelen >= ntcpip__pbufClen(next->p)", (pcb->snd_queuelen >= ntcpip__pbufClen(next->p)));
        /* Prevent ACK for FIN to generate a sent event */
        if ((pcb->acked != 0) && ((NTCPIP__TCPH_FLAGS(next->tcphdr) & NTCPIP__TCP_FIN) != 0)) {
          pcb->acked--;
        }

        pcb->snd_queuelen -= ntcpip__pbufClen(next->p);
        ntcpip__tcpSegFree(next);

        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG, ("%"U16_F" (after freeing unacked)\n", (Uint16)pcb->snd_queuelen));
        if (pcb->snd_queuelen != 0) {
          NTCPIP__LWIP_ASSERT("tcp_receive: valid queue length", pcb->unacked != NULL ||
                      pcb->unsent != NULL);
        }
      }

      /* If there's nothing left to acknowledge, stop the retransmit
         timer, otherwise reset it to start again */
      if(pcb->unacked == NULL)
        pcb->rtime = -1;
      else
        pcb->rtime = 0;

      pcb->polltmr = 0;
    } else {
      /* Fix bug bug #21582: out of sequence ACK, didn't really ack anything */
      pcb->acked = 0;
    }

    /* We go through the ->unsent list to see if any of the segments
       on the list are acknowledged by the ACK. This may seem
       strange since an "unsent" segment shouldn't be acked. The
       rationale is that lwIP puts all outstanding segments on the
       ->unsent list after a retransmission, so these segments may
       in fact have been sent once. */
    while (pcb->unsent != NULL &&
           NTCPIP__TCP_SEQ_BETWEEN(ackno, ntcpip_ntohl(pcb->unsent->tcphdr->seqno) + 
                           NTCPIP__TCP_TCPLEN(pcb->unsent), pcb->snd_nxt)) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_receive: removing %"U32_F":%"U32_F" from pcb->unsent\n",
                                    ntcpip_ntohl(pcb->unsent->tcphdr->seqno), ntcpip_ntohl(pcb->unsent->tcphdr->seqno) +
                                    NTCPIP__TCP_TCPLEN(pcb->unsent)));

      next = pcb->unsent;
      pcb->unsent = pcb->unsent->next;
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG, ("tcp_receive: queuelen %"U16_F" ... ", (Uint16)pcb->snd_queuelen));
      NTCPIP__LWIP_ASSERT("pcb->snd_queuelen >= ntcpip__pbufClen(next->p)", (pcb->snd_queuelen >= ntcpip__pbufClen(next->p)));
      /* Prevent ACK for FIN to generate a sent event */
      if ((pcb->acked != 0) && ((NTCPIP__TCPH_FLAGS(next->tcphdr) & NTCPIP__TCP_FIN) != 0)) {
        pcb->acked--;
      }
      pcb->snd_queuelen -= ntcpip__pbufClen(next->p);
      ntcpip__tcpSegFree(next);
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG, ("%"U16_F" (after freeing unsent)\n", (Uint16)pcb->snd_queuelen));
      if (pcb->snd_queuelen != 0) {
        NTCPIP__LWIP_ASSERT("tcp_receive: valid queue length",
          pcb->unacked != NULL || pcb->unsent != NULL);
      }
    }
    /* End of ACK for new data processing. */

    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RTO_DEBUG, ("tcp_receive: pcb->rttest %"U32_F" rtseq %"U32_F" ackno %"U32_F"\n",
                                pcb->rttest, pcb->rtseq, ackno));

    /* RTT estimation calculations. This is done by checking if the
       incoming segment acknowledges the segment we use to take a
       round-trip time measurement. */
    if (pcb->rttest && NTCPIP__TCP_SEQ_LT(pcb->rtseq, ackno)) {
      /* diff between this shouldn't exceed 32K since this are tcp timer ticks
         and a round-trip shouldn't be that long... */
      m = (Int16)(ntcpip__tcpTicks - pcb->rttest);

      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RTO_DEBUG, ("tcp_receive: experienced rtt %"U16_F" ticks (%"U16_F" msec).\n",
                                  m, m * NTCPIP__TCP_SLOW_INTERVAL));

      /* This is taken directly from VJs original code in his paper */
      m = m - (pcb->sa >> 3);
      pcb->sa += m;
      if (m < 0) {
        m = -m;
      }
      m = m - (pcb->sv >> 2);
      pcb->sv += m;
      pcb->rto = (pcb->sa >> 3) + pcb->sv;

      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RTO_DEBUG, ("tcp_receive: RTO %"U16_F" (%"U16_F" milliseconds)\n",
                                  pcb->rto, pcb->rto * NTCPIP__TCP_SLOW_INTERVAL));

      pcb->rttest = 0;
    }
  }

  /* If the incoming segment contains data, we must process it
     further. */
  if (tcplen > 0) {
    /* This code basically does three things:

    +) If the incoming segment contains data that is the next
    in-sequence data, this data is passed to the application. This
    might involve trimming the first edge of the data. The rcv_nxt
    variable and the advertised window are adjusted.

    +) If the incoming segment has data that is above the next
    sequence number expected (->rcv_nxt), the segment is placed on
    the ->ooseq queue. This is done by finding the appropriate
    place in the ->ooseq queue (which is ordered by sequence
    number) and trim the segment in both ends if needed. An
    immediate ACK is sent to indicate that we received an
    out-of-sequence segment.

    +) Finally, we check if the first segment on the ->ooseq queue
    now is in sequence (i.e., if rcv_nxt >= ooseq->seqno). If
    rcv_nxt > ooseq->seqno, we must trim the first edge of the
    segment on ->ooseq before we adjust rcv_nxt. The data in the
    segments that are now on sequence are chained onto the
    incoming segment so that we only need to call the application
    once.
    */

    /* First, we check if we must trim the first edge. We have to do
       this if the sequence number of the incoming segment is less
       than rcv_nxt, and the sequence number plus the length of the
       segment is larger than rcv_nxt. */
    /*    if (NTCPIP__TCP_SEQ_LT(seqno, pcb->rcv_nxt)){
          if (NTCPIP__TCP_SEQ_LT(pcb->rcv_nxt, seqno + tcplen)) {*/
    if (NTCPIP__TCP_SEQ_BETWEEN(pcb->rcv_nxt, seqno + 1, seqno + tcplen - 1)){
      /* Trimming the first edge is done by pushing the payload
         pointer in the pbuf downwards. This is somewhat tricky since
         we do not want to discard the full contents of the pbuf up to
         the new starting point of the data since we have to keep the
         TCP header which is present in the first pbuf in the chain.

         What is done is really quite a nasty hack: the first pbuf in
         the pbuf chain is pointed to by inseg.p. Since we need to be
         able to deallocate the whole pbuf, we cannot change this
         inseg.p pointer to point to any of the later pbufs in the
         chain. Instead, we point the ->payload pointer in the first
         pbuf to data in one of the later pbufs. We also set the
         inseg.data pointer to point to the right place. This way, the
         ->p pointer will still point to the first pbuf, but the
         ->p->payload pointer will point to data in another pbuf.

         After we are done with adjusting the pbuf pointers we must
         adjust the ->data pointer in the seg and the segment
         length.*/

      off = pcb->rcv_nxt - seqno;
      p = inseg.p;
      NTCPIP__LWIP_ASSERT("inseg.p != NULL", inseg.p);
      NTCPIP__LWIP_ASSERT("insane offset!", (off < 0x7fff));
      if (inseg.p->len < off) {
        NTCPIP__LWIP_ASSERT("pbuf too short!", (((Int32)inseg.p->tot_len) >= off));
        new_tot_len = (Uint16)(inseg.p->tot_len - off);
        while (p->len < off) {
          off -= p->len;
          /* KJM following line changed (with addition of new_tot_len var)
             to fix bug #9076
             inseg.p->tot_len -= p->len; */
          p->tot_len = new_tot_len;
          p->len = 0;
          p = p->next;
        }
        if(ntcpip__pbufHeader(p, (Int16)-off)) {
          /* Do we need to cope with this failing?  Assert for now */
          NTCPIP__LWIP_ASSERT("ntcpip__pbufHeader failed", 0);
        }
      } else {
        if(ntcpip__pbufHeader(inseg.p, (Int16)-off)) {
          /* Do we need to cope with this failing?  Assert for now */
          NTCPIP__LWIP_ASSERT("ntcpip__pbufHeader failed", 0);
        }
      }
      /* KJM following line changed to use p->payload rather than inseg->p->payload
         to fix bug #9076 */
      inseg.dataptr = p->payload;
      inseg.len -= (Uint16)(pcb->rcv_nxt - seqno);
      inseg.tcphdr->seqno = seqno = pcb->rcv_nxt;
    }
    else {
      if (NTCPIP__TCP_SEQ_LT(seqno, pcb->rcv_nxt)){
        /* the whole segment is < rcv_nxt */
        /* must be a duplicate of a packet that has already been correctly handled */

        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_receive: duplicate seqno %"U32_F"\n", seqno));
        ntcpip__tcpAckNow(pcb);
      }
    }

    /* The sequence number must be within the window (above rcv_nxt
       and below rcv_nxt + rcv_wnd) in order to be further
       processed. */
    if (NTCPIP__TCP_SEQ_BETWEEN(seqno, pcb->rcv_nxt, 
                        pcb->rcv_nxt + pcb->rcv_wnd - 1)){
      if (pcb->rcv_nxt == seqno) {
        /* The incoming segment is the next in sequence. We check if
           we have to trim the end of the segment and update rcv_nxt
           and pass the data to the application. */
        tcplen = NTCPIP__TCP_TCPLEN(&inseg);

        if (tcplen > pcb->rcv_wnd) {
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, 
                      ("tcp_receive: other end overran receive window"
                       "seqno %"U32_F" len %"U32_F" right edge %"U32_F"\n",
                       seqno, tcplen, pcb->rcv_nxt + pcb->rcv_wnd));
          if (NTCPIP__TCPH_FLAGS(inseg.tcphdr) & NTCPIP__TCP_FIN) {
            /* Must remove the FIN from the header as we're trimming 
             * that byte of sequence-space from the packet */
            NTCPIP__TCPH_FLAGS_SET(inseg.tcphdr, NTCPIP__TCPH_FLAGS(inseg.tcphdr) &~ NTCPIP__TCP_FIN);
          }
          /* Adjust length of segment to fit in the window. */
          inseg.len = pcb->rcv_wnd;
          if (NTCPIP__TCPH_FLAGS(inseg.tcphdr) & NTCPIP__TCP_SYN) {
            inseg.len -= 1;
          }
          ntcpip_pbufRealloc(inseg.p, inseg.len);
          tcplen = NTCPIP__TCP_TCPLEN(&inseg);
          NTCPIP__LWIP_ASSERT("tcp_receive: segment not trimmed correctly to rcv_wnd\n",
                      (seqno + tcplen) == (pcb->rcv_nxt + pcb->rcv_wnd));
        }
#if NTCPIP__TCP_QUEUE_OOSEQ
        if (pcb->ooseq != NULL) {
          if (NTCPIP__TCPH_FLAGS(inseg.tcphdr) & NTCPIP__TCP_FIN) {
            NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, 
                        ("tcp_receive: received in-order FIN, binning ooseq queue\n"));
            /* Received in-order FIN means anything that was received
             * out of order must now have been received in-order, so
             * bin the ooseq queue
             * rcv_nxt
             * .    |--ooseq--|
             * .==seg============|FIN
             */
            while (pcb->ooseq != NULL) {
              struct ntcpip__tcpSeg *old_ooseq = pcb->ooseq;
              pcb->ooseq = pcb->ooseq->next;
              ntcpip__tcpSegFree(old_ooseq);
            }               
          } 
          else {
            struct ntcpip__tcpSeg* nextSeg = pcb->ooseq;
            struct ntcpip__tcpSeg *old_seg;
            /* rcv_nxt
             * .    |--ooseq--|
             * .==seg============|
             */
            while (nextSeg &&
                   NTCPIP__TCP_SEQ_GEQ(seqno + tcplen,
                		   nextSeg->tcphdr->seqno + nextSeg->len)) {
              /* inseg doesn't have FIN (already processed) */
              if (NTCPIP__TCPH_FLAGS(nextSeg->tcphdr) & NTCPIP__TCP_FIN &&
                  (NTCPIP__TCPH_FLAGS(inseg.tcphdr) & NTCPIP__TCP_SYN) == 0) {
                NTCPIP__TCPH_FLAGS_SET(inseg.tcphdr, 
                               NTCPIP__TCPH_FLAGS(inseg.tcphdr) | NTCPIP__TCP_FIN);
                tcplen = NTCPIP__TCP_TCPLEN(&inseg);
              }
              old_seg = nextSeg;
              nextSeg = nextSeg->next;
              ntcpip__tcpSegFree(old_seg);
            }
            /* rcv_nxt
             * .             |--ooseq--|
             * .==seg============|
             */
            if (nextSeg &&
                NTCPIP__TCP_SEQ_GT(seqno + tcplen,
                		nextSeg->tcphdr->seqno)) {
              /* FIN in inseg already handled by dropping whole ooseq queue */
              inseg.len = (Uint16)(pcb->ooseq->tcphdr->seqno - seqno);
              if (NTCPIP__TCPH_FLAGS(inseg.tcphdr) & NTCPIP__TCP_SYN) {
                inseg.len -= 1;
              }
              ntcpip_pbufRealloc(inseg.p, inseg.len);
              tcplen = NTCPIP__TCP_TCPLEN(&inseg);
              NTCPIP__LWIP_ASSERT("tcp_receive: segment not trimmed correctly to ooseq queue\n",
                          (seqno + tcplen) == pcb->ooseq->tcphdr->seqno);
            }
            pcb->ooseq = nextSeg;
          }
        }
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */

        pcb->rcv_nxt = seqno + tcplen;

        /* Update the receiver's (our) window. */
        NTCPIP__LWIP_ASSERT("tcp_receive: tcplen > rcv_wnd\n", pcb->rcv_wnd >= tcplen);
        pcb->rcv_wnd -= tcplen;

        ntcpip__tcpUpdateRcvAnnWnd(pcb);

        /* If there is data in the segment, we make preparations to
           pass this up to the application. The ->recv_data variable
           is used for holding the pbuf that goes to the
           application. The code for reassembling out-of-sequence data
           chains its data on this pbuf as well.

           If the segment was a FIN, we set the NTCPIP_TF_GOT_FIN flag that will
           be used to indicate to the application that the remote side has
           closed its end of the connection. */
        if (inseg.p->tot_len > 0) {
          recv_data = inseg.p;
          /* Since this pbuf now is the responsibility of the
             application, we delete our reference to it so that we won't
             (mistakingly) deallocate it. */
          inseg.p = NULL;
        }
        if (NTCPIP__TCPH_FLAGS(inseg.tcphdr) & NTCPIP__TCP_FIN) {
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_receive: received FIN.\n"));
          recv_flags |= NTCPIP_TF_GOT_FIN;
        }

#if NTCPIP__TCP_QUEUE_OOSEQ
        /* We now check if we have segments on the ->ooseq queue that
           is now in sequence. */
        while (pcb->ooseq != NULL &&
               pcb->ooseq->tcphdr->seqno == pcb->rcv_nxt) {

          cseg = pcb->ooseq;
          seqno = pcb->ooseq->tcphdr->seqno;

          pcb->rcv_nxt += NTCPIP__TCP_TCPLEN(cseg);
          NTCPIP__LWIP_ASSERT("tcp_receive: ooseq tcplen > rcv_wnd\n",
                      pcb->rcv_wnd >= NTCPIP__TCP_TCPLEN(cseg));
          pcb->rcv_wnd -= NTCPIP__TCP_TCPLEN(cseg);

          ntcpip__tcpUpdateRcvAnnWnd(pcb);

          if (cseg->p->tot_len > 0) {
            /* Chain this pbuf onto the pbuf that we will pass to
               the application. */
            if (recv_data) {
              ntcpip_pbufCat(recv_data, cseg->p);
            } else {
              recv_data = cseg->p;
            }
            cseg->p = NULL;
          }
          if (NTCPIP__TCPH_FLAGS(cseg->tcphdr) & NTCPIP__TCP_FIN) {
            NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_receive: dequeued FIN.\n"));
            recv_flags |= NTCPIP_TF_GOT_FIN;
            if (pcb->state == NTCPIP_ESTABLISHED) { /* force passive close or we can move to active close */
              pcb->state = NTCPIP_CLOSE_WAIT;
            } 
          }

          pcb->ooseq = cseg->next;
          ntcpip__tcpSegFree(cseg);
        }
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */


        /* Acknowledge the segment(s). */
        ntcpip__tcpAck(pcb);

      } else {
        /* We get here if the incoming segment is out-of-sequence. */
    	  ntcpip__tcpSendEmptyAck(pcb);
#if NTCPIP__TCP_QUEUE_OOSEQ
        /* We queue the segment on the ->ooseq queue. */
        if (pcb->ooseq == NULL) {
          pcb->ooseq = ntcpip__tcpSegCopy(&inseg);
        } else {
          /* If the queue is not empty, we walk through the queue and
             try to find a place where the sequence number of the
             incoming segment is between the sequence numbers of the
             previous and the next segment on the ->ooseq queue. That is
             the place where we put the incoming segment. If needed, we
             trim the second edges of the previous and the incoming
             segment so that it will fit into the sequence.

             If the incoming segment has the same sequence number as a
             segment on the ->ooseq queue, we discard the segment that
             contains less data. */

          prev = NULL;
          for(next = pcb->ooseq; next != NULL; next = next->next) {
            if (seqno == next->tcphdr->seqno) {
              /* The sequence number of the incoming segment is the
                 same as the sequence number of the segment on
                 ->ooseq. We check the lengths to see which one to
                 discard. */
              if (inseg.len > next->len) {
                /* The incoming segment is larger than the old
                   segment. We replace some segments with the new
                   one. */
                cseg = ntcpip__tcpSegCopy(&inseg);
                if (cseg != NULL) {
                  if (prev != NULL) {
                    prev->next = cseg;
                  } else {
                    pcb->ooseq = cseg;
                  }
                  tcp_oos_insert_segment(cseg, next);
                }
                break;
              } else {
                /* Either the lenghts are the same or the incoming
                   segment was smaller than the old one; in either
                   case, we ditch the incoming segment. */
                break;
              }
            } else {
              if (prev == NULL) {
                if (NTCPIP__TCP_SEQ_LT(seqno, next->tcphdr->seqno)) {
                  /* The sequence number of the incoming segment is lower
                     than the sequence number of the first segment on the
                     queue. We put the incoming segment first on the
                     queue. */
                  cseg = ntcpip__tcpSegCopy(&inseg);
                  if (cseg != NULL) {
                    pcb->ooseq = cseg;
                    tcp_oos_insert_segment(cseg, next);
                  }
                  break;
                }
              } else {
                /*if (NTCPIP__TCP_SEQ_LT(prev->tcphdr->seqno, seqno) &&
                  NTCPIP__TCP_SEQ_LT(seqno, next->tcphdr->seqno)) {*/
                if (NTCPIP__TCP_SEQ_BETWEEN(seqno, prev->tcphdr->seqno+1, next->tcphdr->seqno-1)) {
                  /* The sequence number of the incoming segment is in
                     between the sequence numbers of the previous and
                     the next segment on ->ooseq. We trim trim the previous
                     segment, delete next segments that included in received segment
                     and trim received, if needed. */
                  cseg = ntcpip__tcpSegCopy(&inseg);
                  if (cseg != NULL) {
                    if (NTCPIP__TCP_SEQ_GT(prev->tcphdr->seqno + prev->len, seqno)) {
                      /* We need to trim the prev segment. */
                      prev->len = (Uint16)(seqno - prev->tcphdr->seqno);
                      ntcpip_pbufRealloc(prev->p, prev->len);
                    }
                    prev->next = cseg;
                    tcp_oos_insert_segment(cseg, next);
                  }
                  break;
                }
              }
              /* If the "next" segment is the last segment on the
                 ooseq queue, we add the incoming segment to the end
                 of the list. */
              if (next->next == NULL &&
                  NTCPIP__TCP_SEQ_GT(seqno, next->tcphdr->seqno)) {
                if (NTCPIP__TCPH_FLAGS(next->tcphdr) & NTCPIP__TCP_FIN) {
                  /* segment "next" already contains all data */
                  break;
                }
                next->next = ntcpip__tcpSegCopy(&inseg);
                if (next->next != NULL) {
                  if (NTCPIP__TCP_SEQ_GT(next->tcphdr->seqno + next->len, seqno)) {
                    /* We need to trim the last segment. */
                    next->len = (Uint16)(seqno - next->tcphdr->seqno);
                    ntcpip_pbufRealloc(next->p, next->len);
                  }
                }
                break;
              }
            }
            prev = next;
          }
        }
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */

      }
    } else {
      /* The incoming segment is not withing the window. */
    	ntcpip__tcpSendEmptyAck(pcb);
    }
  } else {
    /* Segments with length 0 is taken care of here. Segments that
       fall out of the window are ACKed. */
    /*if (NTCPIP__TCP_SEQ_GT(pcb->rcv_nxt, seqno) ||
      NTCPIP__TCP_SEQ_GEQ(seqno, pcb->rcv_nxt + pcb->rcv_wnd)) {*/
    if(!NTCPIP__TCP_SEQ_BETWEEN(seqno, pcb->rcv_nxt, pcb->rcv_nxt + pcb->rcv_wnd-1)){
      ntcpip__tcpAckNow(pcb);
    }
  }
}

/**
 * Parses the options contained in the incoming segment. 
 *
 * Called from tcp_listen_input() and tcp_process().
 * Currently, only the MSS option is supported!
 *
 * @param pcb the tcp_pcb for which a segment arrived
 */
static void
tcp_parseopt(struct ntcpip__tcpPcb *pcb)
{
  Uint16 c, max_c;
  Uint16 mss;
  Uint8 *opts, opt;
#if LWIP_TCP_TIMESTAMPS
  Uint32 tsval;
#endif

  opts = (Uint8 *)tcphdr + NTCPIP__TCP_HLEN;

  /* Parse the TCP MSS option, if present. */
  if(NTCPIP__TCPH_HDRLEN(tcphdr) > 0x5) {
    max_c = (NTCPIP__TCPH_HDRLEN(tcphdr) - 5) << 2;
    for (c = 0; c < max_c; ) {
      opt = opts[c];
      switch (opt) {
      case 0x00:
        /* End of options. */
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: EOL\n"));
        return;
      case 0x01:
        /* NOP option. */
        ++c;
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: NOP\n"));
        break;
      case 0x02:
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: MSS\n"));
        if (opts[c + 1] != 0x04 || c + 0x04 > max_c) {
          /* Bad length */
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: bad length\n"));
          return;
        }
        /* An MSS option with the right option length. */
        mss = (opts[c + 2] << 8) | opts[c + 3];
        /* Limit the mss to the configured NTCPIP__TCP_MSS and prevent division by zero */
        pcb->mss = ((mss > NTCPIP__TCP_MSS) || (mss == 0)) ? NTCPIP__TCP_MSS : mss;
        /* Advance to next option */
        c += 0x04;
        break;
#if LWIP_TCP_TIMESTAMPS
      case 0x08:
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: TS\n"));
        if (opts[c + 1] != 0x0A || c + 0x0A > max_c) {
          /* Bad length */
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: bad length\n"));
          return;
        }
        /* TCP timestamp option with valid length */
        tsval = (opts[c+2]) | (opts[c+3] << 8) | 
          (opts[c+4] << 16) | (opts[c+5] << 24);
        if (flags & NTCPIP__TCP_SYN) {
          pcb->ts_recent = ntcpip_ntohl(tsval);
          pcb->flags |= NTCPIP_TF_TIMESTAMP;
        } else if (NTCPIP__TCP_SEQ_BETWEEN(pcb->ts_lastacksent, seqno, seqno+tcplen)) {
          pcb->ts_recent = ntcpip_ntohl(tsval);
        }
        /* Advance to next option */
        c += 0x0A;
        break;
#endif
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: other\n"));
        if (opts[c + 1] == 0) {
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("tcp_parseopt: bad length\n"));
          /* If the length field is zero, the options are malformed
             and we don't process them further. */
          return;
        }
        /* All other options have a length field, so that we easily
           can skip past them. */
        c += opts[c + 1];
      }
    }
  }
}

#endif /* NTCPIP__LWIP_TCP */


