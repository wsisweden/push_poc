/**
 * @file
 * Statistics module
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_STATS /* don't build if not configured for use in lwipopts.h */

#include "../def.h"
#include "../stats.h"
#include "../mem.h"

#include <string.h>

struct stats_ ntcpip__lwipStats;

#if NTCPIP__LWIP_STATS_DISPLAY
void
stats_display_proto(struct stats_proto *proto, char *name)
{
  NTCPIP__PLATFORM_DIAG(("\n%s\n\t", name));
  NTCPIP__PLATFORM_DIAG(("xmit: %"STAT_COUNTER_F"\n\t", proto->xmit)); 
  NTCPIP__PLATFORM_DIAG(("recv: %"STAT_COUNTER_F"\n\t", proto->recv)); 
  NTCPIP__PLATFORM_DIAG(("fw: %"STAT_COUNTER_F"\n\t", proto->fw)); 
  NTCPIP__PLATFORM_DIAG(("drop: %"STAT_COUNTER_F"\n\t", proto->drop)); 
  NTCPIP__PLATFORM_DIAG(("chkerr: %"STAT_COUNTER_F"\n\t", proto->chkerr)); 
  NTCPIP__PLATFORM_DIAG(("lenerr: %"STAT_COUNTER_F"\n\t", proto->lenerr)); 
  NTCPIP__PLATFORM_DIAG(("memerr: %"STAT_COUNTER_F"\n\t", proto->memerr)); 
  NTCPIP__PLATFORM_DIAG(("rterr: %"STAT_COUNTER_F"\n\t", proto->rterr)); 
  NTCPIP__PLATFORM_DIAG(("proterr: %"STAT_COUNTER_F"\n\t", proto->proterr)); 
  NTCPIP__PLATFORM_DIAG(("opterr: %"STAT_COUNTER_F"\n\t", proto->opterr)); 
  NTCPIP__PLATFORM_DIAG(("err: %"STAT_COUNTER_F"\n\t", proto->err)); 
  NTCPIP__PLATFORM_DIAG(("cachehit: %"STAT_COUNTER_F"\n", proto->cachehit)); 
}

#if NTCPIP__IGMP_STATS
void
stats_display_igmp(struct stats_igmp *igmp)
{
  NTCPIP__PLATFORM_DIAG(("\nIGMP\n\t"));
  NTCPIP__PLATFORM_DIAG(("lenerr: %"STAT_COUNTER_F"\n\t", igmp->lenerr)); 
  NTCPIP__PLATFORM_DIAG(("chkerr: %"STAT_COUNTER_F"\n\t", igmp->chkerr)); 
  NTCPIP__PLATFORM_DIAG(("v1_rxed: %"STAT_COUNTER_F"\n\t", igmp->v1_rxed)); 
  NTCPIP__PLATFORM_DIAG(("join_sent: %"STAT_COUNTER_F"\n\t", igmp->join_sent)); 
  NTCPIP__PLATFORM_DIAG(("leave_sent: %"STAT_COUNTER_F"\n\t", igmp->leave_sent)); 
  NTCPIP__PLATFORM_DIAG(("unicast_query: %"STAT_COUNTER_F"\n\t", igmp->unicast_query)); 
  NTCPIP__PLATFORM_DIAG(("report_sent: %"STAT_COUNTER_F"\n\t", igmp->report_sent)); 
  NTCPIP__PLATFORM_DIAG(("report_rxed: %"STAT_COUNTER_F"\n\t", igmp->report_rxed)); 
  NTCPIP__PLATFORM_DIAG(("group_query_rxed: %"STAT_COUNTER_F"\n", igmp->group_query_rxed));
}
#endif /* NTCPIP__IGMP_STATS */

#if NTCPIP__MEM_STATS || NTCPIP__MEMP_STATS
void
stats_display_mem(struct stats_mem *mem, char *name)
{
  NTCPIP__PLATFORM_DIAG(("\nMEM %s\n\t", name));
  NTCPIP__PLATFORM_DIAG(("avail: %"U32_F"\n\t", (Uint32)mem->avail)); 
  NTCPIP__PLATFORM_DIAG(("used: %"U32_F"\n\t", (Uint32)mem->used)); 
  NTCPIP__PLATFORM_DIAG(("max: %"U32_F"\n\t", (Uint32)mem->max)); 
  NTCPIP__PLATFORM_DIAG(("err: %"U32_F"\n", (Uint32)mem->err));
}

#if NTCPIP__MEMP_STATS
void
stats_display_memp(struct stats_mem *mem, int index)
{
  char * memp_names[] = {
#define LWIP_MEMPOOL(name,num,size,desc) desc,
#include "lwip/memp_std.h"
  };
  if(index < MEMP_MAX) {
    stats_display_mem(mem, memp_names[index]);
  }
}
#endif /* NTCPIP__MEMP_STATS */
#endif /* NTCPIP__MEM_STATS || NTCPIP__MEMP_STATS */

#if NTCPIP__SYS_STATS
void
stats_display_sys(struct stats_sys *sys)
{
  NTCPIP__PLATFORM_DIAG(("\nSYS\n\t"));
  NTCPIP__PLATFORM_DIAG(("sem.used: %"U32_F"\n\t", (Uint32)sys->sem.used)); 
  NTCPIP__PLATFORM_DIAG(("sem.max:  %"U32_F"\n\t", (Uint32)sys->sem.max)); 
  NTCPIP__PLATFORM_DIAG(("sem.err:  %"U32_F"\n\t", (Uint32)sys->sem.err)); 
  NTCPIP__PLATFORM_DIAG(("mbox.used: %"U32_F"\n\t", (Uint32)sys->mbox.used)); 
  NTCPIP__PLATFORM_DIAG(("mbox.max:  %"U32_F"\n\t", (Uint32)sys->mbox.max)); 
  NTCPIP__PLATFORM_DIAG(("mbox.err:  %"U32_F"\n\t", (Uint32)sys->mbox.err)); 
}
#endif /* NTCPIP__SYS_STATS */

void
stats_display(void)
{
  Int16 i;

  LINK_STATS_DISPLAY();
  ETHARP_STATS_DISPLAY();
  IPFRAG_STATS_DISPLAY();
  IP_STATS_DISPLAY();
  IGMP_STATS_DISPLAY();
  ICMP_STATS_DISPLAY();
  UDP_STATS_DISPLAY();
  TCP_STATS_DISPLAY();
  MEM_STATS_DISPLAY();
  for (i = 0; i < MEMP_MAX; i++) {
    MEMP_STATS_DISPLAY(i);
  }
  SYS_STATS_DISPLAY();
}
#endif /* NTCPIP__LWIP_STATS_DISPLAY */

#endif /* NTCPIP__LWIP_STATS */


