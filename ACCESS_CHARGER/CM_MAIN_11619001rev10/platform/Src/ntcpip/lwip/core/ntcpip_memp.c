/**
 * @file
 * Dynamic pool memory manager
 *
 * lwIP has dedicated pools for many structures (netconn, protocol control blocks,
 * packet buffers, ...). All these pools are managed here.
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#include "../memp.h"
#include "ntcpip/pbuf.h"
#include "ntcpip/udp.h"
#include "../raw.h"
#include "ntcpip/tcp.h"
#include "ntcpip/igmp.h"
#include "ntcpip/api.h"
#include "../api_msg.h"
#include "ntcpip/tcpip.h"
#include "ntcpip/sys.h"
#include "../stats.h"
#include "../../netif/etharp.h"
#include "../ip_frag.h"

#include <string.h>

#if !MEMP_MEM_MALLOC /* don't build if not configured for use in lwipopts.h */

struct memp {
  struct memp *next;
#if NTCPIP__MEMP_OVERFLOW_CHECK
  const char *file;
  int line;
#endif /* NTCPIP__MEMP_OVERFLOW_CHECK */
};

#if NTCPIP__MEMP_OVERFLOW_CHECK
/* if NTCPIP__MEMP_OVERFLOW_CHECK is turned on, we reserve some bytes at the beginning
 * and at the end of each element, initialize them as 0xcd and check
 * them later. */
/* If NTCPIP__MEMP_OVERFLOW_CHECK is >= 2, on every call to ntcpip__mempMalloc or ntcpip__mempFree,
 * every single element in each pool is checked!
 * This is VERY SLOW but also very helpful. */
/* MEMP_SANITY_REGION_BEFORE and MEMP_SANITY_REGION_AFTER can be overridden in
 * lwipopts.h to change the amount reserved for checking. */
#ifndef MEMP_SANITY_REGION_BEFORE
#define MEMP_SANITY_REGION_BEFORE  16
#endif /* MEMP_SANITY_REGION_BEFORE*/
#if MEMP_SANITY_REGION_BEFORE > 0
#define MEMP_SANITY_REGION_BEFORE_ALIGNED    LWIP_MEM_ALIGN_SIZE(MEMP_SANITY_REGION_BEFORE)
#else
#define MEMP_SANITY_REGION_BEFORE_ALIGNED    0
#endif /* MEMP_SANITY_REGION_BEFORE*/
#ifndef MEMP_SANITY_REGION_AFTER
#define MEMP_SANITY_REGION_AFTER   16
#endif /* MEMP_SANITY_REGION_AFTER*/
#if MEMP_SANITY_REGION_AFTER > 0
#define MEMP_SANITY_REGION_AFTER_ALIGNED     LWIP_MEM_ALIGN_SIZE(MEMP_SANITY_REGION_AFTER)
#else
#define MEMP_SANITY_REGION_AFTER_ALIGNED     0
#endif /* MEMP_SANITY_REGION_AFTER*/

/* MEMP_SIZE: save space for struct memp and for sanity check */
#define MEMP_SIZE          (LWIP_MEM_ALIGN_SIZE(sizeof(struct memp)) + MEMP_SANITY_REGION_BEFORE_ALIGNED)
#define MEMP_ALIGN_SIZE(x) (LWIP_MEM_ALIGN_SIZE(x) + MEMP_SANITY_REGION_AFTER_ALIGNED)

#else /* NTCPIP__MEMP_OVERFLOW_CHECK */

/* No sanity checks
 * We don't need to preserve the struct memp while not allocated, so we
 * can save a little space and set MEMP_SIZE to 0.
 */
#define MEMP_SIZE           0
#define MEMP_ALIGN_SIZE(x) (LWIP_MEM_ALIGN_SIZE(x))

#endif /* NTCPIP__MEMP_OVERFLOW_CHECK */

/** This array holds the first free element of each pool.
 *  Elements form a linked list. */
static struct memp *memp_tab[MEMP_MAX];

#else /* MEMP_MEM_MALLOC */

#define MEMP_ALIGN_SIZE(x) (LWIP_MEM_ALIGN_SIZE(x))

#endif /* MEMP_MEM_MALLOC */

/** This array holds the element sizes of each pool. */
#if !NTCPIP__MEM_USE_POOLS && !MEMP_MEM_MALLOC
static
#endif
const Uint16 ntcpip__mempSizes[MEMP_MAX] = {
#define LWIP_MEMPOOL(name,num,size,desc)  LWIP_MEM_ALIGN_SIZE(size),
#include "../memp_std.h"
};

#if !MEMP_MEM_MALLOC /* don't build if not configured for use in lwipopts.h */

/** This array holds the number of elements in each pool. */
static const Uint16 memp_num[MEMP_MAX] = {
#define LWIP_MEMPOOL(name,num,size,desc)  (num),
#include "../memp_std.h"
};

/** This array holds a textual description of each pool. */
#ifdef LWIP_DEBUG
static const char *memp_desc[MEMP_MAX] = {
#define LWIP_MEMPOOL(name,num,size,desc)  (desc),
#include "../memp_std.h"
};
#endif /* LWIP_DEBUG */

/** This is the actual memory used by the pools. */
static Uint8 memp_memory[NTCPIP__MEM_ALIGNMENT - 1 
#define LWIP_MEMPOOL(name,num,size,desc) + ( (num) * (MEMP_SIZE + MEMP_ALIGN_SIZE(size) ) )
#include "../memp_std.h"
];

#if NTCPIP__MEMP_SANITY_CHECK
/**
 * Check that memp-lists don't form a circle
 */
static int
memp_sanity(void)
{
  Int16 i, c;
  struct memp *m, *n;

  for (i = 0; i < MEMP_MAX; i++) {
    for (m = memp_tab[i]; m != NULL; m = m->next) {
      c = 1;
      for (n = memp_tab[i]; n != NULL; n = n->next) {
        if (n == m && --c < 0) {
          return 0;
        }
      }
    }
  }
  return 1;
}
#endif /* NTCPIP__MEMP_SANITY_CHECK*/
#if NTCPIP__MEMP_OVERFLOW_CHECK
/**
 * Check if a memp element was victim of an overflow
 * (e.g. the restricted area after it has been altered)
 *
 * @param p the memp element to check
 * @param memp_size the element size of the pool p comes from
 */
static void
memp_overflow_check_element(struct memp *p, Uint16 memp_size)
{
  Uint16 k;
  Uint8 *m;
#if MEMP_SANITY_REGION_BEFORE_ALIGNED > 0
  m = (Uint8*)p + MEMP_SIZE - MEMP_SANITY_REGION_BEFORE_ALIGNED;
  for (k = 0; k < MEMP_SANITY_REGION_BEFORE_ALIGNED; k++) {
    if (m[k] != 0xcd) {
      NTCPIP__LWIP_ASSERT("detected memp underflow!", 0);
    }
  }
#endif
#if MEMP_SANITY_REGION_AFTER_ALIGNED > 0
  m = (Uint8*)p + MEMP_SIZE + memp_size;
  for (k = 0; k < MEMP_SANITY_REGION_AFTER_ALIGNED; k++) {
    if (m[k] != 0xcd) {
      NTCPIP__LWIP_ASSERT("detected memp overflow!", 0);
    }
  }
#endif
}

/**
 * Do an overflow check for all elements in every pool.
 *
 * @see memp_overflow_check_element for a description of the check
 */
static void
memp_overflow_check_all(void)
{
  Uint16 i, j;
  struct memp *p;

  p = LWIP_MEM_ALIGN(memp_memory);
  for (i = 0; i < MEMP_MAX; ++i) {
    p = p;
    for (j = 0; j < memp_num[i]; ++j) {
      memp_overflow_check_element(p, ntcpip__mempSizes[i]);
      p = (struct memp*)((Uint8*)p + MEMP_SIZE + ntcpip__mempSizes[i] + MEMP_SANITY_REGION_AFTER_ALIGNED);
    }
  }
}

/**
 * Initialize the restricted areas of all memp elements in every pool.
 */
static void
memp_overflow_init(void)
{
  Uint16 i, j;
  struct memp *p;
  Uint8 *m;

  p = LWIP_MEM_ALIGN(memp_memory);
  for (i = 0; i < MEMP_MAX; ++i) {
    p = p;
    for (j = 0; j < memp_num[i]; ++j) {
#if MEMP_SANITY_REGION_BEFORE_ALIGNED > 0
      m = (Uint8*)p + MEMP_SIZE - MEMP_SANITY_REGION_BEFORE_ALIGNED;
      memset(m, 0xcd, MEMP_SANITY_REGION_BEFORE_ALIGNED);
#endif
#if MEMP_SANITY_REGION_AFTER_ALIGNED > 0
      m = (Uint8*)p + MEMP_SIZE + ntcpip__mempSizes[i];
      memset(m, 0xcd, MEMP_SANITY_REGION_AFTER_ALIGNED);
#endif
      p = (struct memp*)((Uint8*)p + MEMP_SIZE + ntcpip__mempSizes[i] + MEMP_SANITY_REGION_AFTER_ALIGNED);
    }
  }
}
#endif /* NTCPIP__MEMP_OVERFLOW_CHECK */

/**
 * Initialize this module.
 * 
 * Carves out memp_memory into linked lists for each pool-type.
 */
void
ntcpip__mempInit(void)
{
  struct memp *memp;
  Uint16 i, j;

  for (i = 0; i < MEMP_MAX; ++i) {
    MEMP_STATS_AVAIL(used, i, 0);
    MEMP_STATS_AVAIL(max, i, 0);
    MEMP_STATS_AVAIL(err, i, 0);
    MEMP_STATS_AVAIL(avail, i, memp_num[i]);
  }

  memp = LWIP_MEM_ALIGN(memp_memory);
  /* for every pool: */
  for (i = 0; i < MEMP_MAX; ++i) {
    memp_tab[i] = NULL;
    /* create a linked list of memp elements */
    for (j = 0; j < memp_num[i]; ++j) {
      memp->next = memp_tab[i];
      memp_tab[i] = memp;
      memp = (struct memp *)((void *) ((Uint8 *)memp + MEMP_SIZE + ntcpip__mempSizes[i]
#if NTCPIP__MEMP_OVERFLOW_CHECK
        + MEMP_SANITY_REGION_AFTER_ALIGNED
#endif
      ));
    }
  }
#if NTCPIP__MEMP_OVERFLOW_CHECK
  memp_overflow_init();
  /* check everything a first time to see if it worked */
  memp_overflow_check_all();
#endif /* NTCPIP__MEMP_OVERFLOW_CHECK */
}

/**
 * Get an element from a specific pool.
 *
 * @param type the pool to get an element from
 *
 * the debug version has two more parameters:
 * @param file file name calling this function
 * @param line number of line where this function is called
 *
 * @return a pointer to the allocated memory or a NULL pointer on error
 */
void *
#if !NTCPIP__MEMP_OVERFLOW_CHECK
ntcpip__mempMalloc(memp_t type)
#else
memp_malloc_fn(memp_t type, const char* file, const int line)
#endif
{
  struct memp *memp;
  NTCPIP__SYS_ARCH_DECL_PROTECT(old_level);
 
  NTCPIP__LWIP_ERROR("ntcpip__mempMalloc: type < MEMP_MAX", (type < MEMP_MAX), return NULL;);

  NTCPIP__SYS_ARCH_PROTECT(old_level);
#if NTCPIP__MEMP_OVERFLOW_CHECK >= 2
  memp_overflow_check_all();
#endif /* NTCPIP__MEMP_OVERFLOW_CHECK >= 2 */

  memp = memp_tab[type];
  
  if (memp != NULL) {
    memp_tab[type] = memp->next;
#if NTCPIP__MEMP_OVERFLOW_CHECK
    memp->next = NULL;
    memp->file = file;
    memp->line = line;
#endif /* NTCPIP__MEMP_OVERFLOW_CHECK */
    MEMP_STATS_INC_USED(used, type);
    NTCPIP__LWIP_ASSERT("ntcpip__mempMalloc: memp properly aligned",
                ((ntcpip__MemPtr)memp % NTCPIP__MEM_ALIGNMENT) == 0);
    memp = (struct memp*) ((void *) ((Uint8*)memp + MEMP_SIZE));
  } else {
    NTCPIP__LWIP_DEBUGF(NTCPIP__MEMP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip__mempMalloc: out of memory in pool %s\n", memp_desc[type]));
    MEMP_STATS_INC(err, type);
  }

  NTCPIP__SYS_ARCH_UNPROTECT(old_level);

  return memp;
}

/**
 * Put an element back into its pool.
 *
 * @param type the pool where to put mem
 * @param mem the memp element to free
 */
void
ntcpip__mempFree(memp_t type, void *mem)
{
  struct memp *memp;
  NTCPIP__SYS_ARCH_DECL_PROTECT(old_level);

  if (mem == NULL) {
    return;
  }
  NTCPIP__LWIP_ASSERT("ntcpip__mempFree: mem properly aligned",
                ((ntcpip__MemPtr)mem % NTCPIP__MEM_ALIGNMENT) == 0);

  memp = (struct memp *) ((void *) ((Uint8 *) mem - MEMP_SIZE));

  NTCPIP__SYS_ARCH_PROTECT(old_level);
#if NTCPIP__MEMP_OVERFLOW_CHECK
#if NTCPIP__MEMP_OVERFLOW_CHECK >= 2
  memp_overflow_check_all();
#else
  memp_overflow_check_element(memp, ntcpip__mempSizes[type]);
#endif /* NTCPIP__MEMP_OVERFLOW_CHECK >= 2 */
#endif /* NTCPIP__MEMP_OVERFLOW_CHECK */

  MEMP_STATS_DEC(used, type); 
  
  memp->next = memp_tab[type]; 
  memp_tab[type] = memp;

#if NTCPIP__MEMP_SANITY_CHECK
  NTCPIP__LWIP_ASSERT("memp sanity", memp_sanity());
#endif /* NTCPIP__MEMP_SANITY_CHECK */

  NTCPIP__SYS_ARCH_UNPROTECT(old_level);
}

#endif /* MEMP_MEM_MALLOC */

