/**
 * @file
 * Modules initialization
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#include "../init.h"
#include "../stats.h"
#include "ntcpip/sys.h"
#include "../mem.h"
#include "../memp.h"
#include "ntcpip/pbuf.h"
#include "ntcpip/netif.h"
#include "../sockets.h"
#include "ntcpip/ip.h"
#include "../raw.h"
#include "ntcpip/udp.h"
#include "ntcpip/tcp.h"
#include "../snmp_msg.h"
#include "../autoip.h"
#include "ntcpip/igmp.h"
#include "../dns.h"
#include "../../netif/etharp.h"

/* Compile-time sanity checks for configuration errors.
 * These can be done independently of LWIP_DEBUG, without penalty.
 */
#ifndef NTCPIP__BYTE_ORDER
  #error "NTCPIP__BYTE_ORDER is not defined, you have to define it in your cc.h"
#endif
#if (!IP_SOF_BROADCAST && IP_SOF_BROADCAST_RECV)
  #error "If you want to use broadcast filter per pcb on recv operations, you have to define IP_SOF_BROADCAST=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_ARP && NTCPIP__ARP_QUEUEING)
  #error "If you want to use ARP Queueing, you have to define NTCPIP__LWIP_ARP=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_UDP && NTCPIP__LWIP_UDPLITE)
  #error "If you want to use UDP Lite, you have to define NTCPIP__LWIP_UDP=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_UDP && NTCPIP__LWIP_SNMP)
  #error "If you want to use SNMP, you have to define NTCPIP__LWIP_UDP=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_UDP && NTCPIP__LWIP_DHCP)
  #error "If you want to use DHCP, you have to define NTCPIP__LWIP_UDP=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_UDP && NTCPIP__LWIP_IGMP)
  #error "If you want to use IGMP, you have to define NTCPIP__LWIP_UDP=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_UDP && NTCPIP__LWIP_DNS)
  #error "If you want to use DNS, you have to define NTCPIP__LWIP_UDP=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_ARP && (NTCPIP__ARP_TABLE_SIZE > 0x7f))
  #error "If you want to use ARP, NTCPIP__ARP_TABLE_SIZE must fit in an Int8, so, you have to reduce it in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_ARP && NTCPIP__ARP_QUEUEING && (NTCPIP__MEMP_NUM_ARP_QUEUE<=0))
  #error "If you want to use ARP Queueing, you have to define NTCPIP__MEMP_NUM_ARP_QUEUE>=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_RAW && (NTCPIP__MEMP_NUM_RAW_PCB<=0))
  #error "If you want to use RAW, you have to define NTCPIP__MEMP_NUM_RAW_PCB>=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_UDP && (NTCPIP__MEMP_NUM_UDP_PCB<=0))
  #error "If you want to use UDP, you have to define NTCPIP__MEMP_NUM_UDP_PCB>=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_TCP && (NTCPIP__MEMP_NUM_TCP_PCB<=0))
  #error "If you want to use TCP, you have to define NTCPIP__MEMP_NUM_TCP_PCB>=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_TCP && (NTCPIP__TCP_WND > 0xffff))
  #error "If you want to use TCP, NTCPIP__TCP_WND must fit in an Uint16, so, you have to reduce it in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_TCP && (NTCPIP__TCP_SND_QUEUELEN > 0xffff))
  #error "If you want to use TCP, NTCPIP__TCP_SND_QUEUELEN must fit in an Uint16, so, you have to reduce it in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_TCP && ((NTCPIP__TCP_MAXRTX > 12) || (NTCPIP__TCP_SYNMAXRTX > 12)))
  #error "If you want to use TCP, NTCPIP__TCP_MAXRTX and NTCPIP__TCP_SYNMAXRTX must less or equal to 12 (due to ntcpip__tcpBackoff table), so, you have to reduce them in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_TCP && NTCPIP__TCP_LISTEN_BACKLOG && (NTCPIP__TCP_DEFAULT_LISTEN_BACKLOG < 0) || (NTCPIP__TCP_DEFAULT_LISTEN_BACKLOG > 0xff))
  #error "If you want to use TCP backlog, NTCPIP__TCP_DEFAULT_LISTEN_BACKLOG must fit into an Uint8"
#endif
#if (NTCPIP__LWIP_IGMP && (NTCPIP__MEMP_NUM_IGMP_GROUP<=1))
  #error "If you want to use IGMP, you have to define NTCPIP__MEMP_NUM_IGMP_GROUP>1 in your lwipopts.h"
#endif
#if (NTCPIP__PPP_SUPPORT && (NTCPIP__NO_SYS==1))
  #error "If you want to use PPP, you have to define NTCPIP__NO_SYS=0 in your lwipopts.h"
#endif 
#if (NTCPIP__LWIP_NETIF_API && (NTCPIP__NO_SYS==1))
  #error "If you want to use NETIF API, you have to define NTCPIP__NO_SYS=0 in your lwipopts.h"
#endif
#if ((NTCPIP__LWIP_SOCKET || NTCPIP__LWIP_NETCONN) && (NTCPIP__NO_SYS==1))
  #error "If you want to use Sequential API, you have to define NTCPIP__NO_SYS=0 in your lwipopts.h"
#endif
#if ((NTCPIP__LWIP_NETCONN || NTCPIP__LWIP_SOCKET) && (NTCPIP__MEMP_NUM_TCPIP_MSG_API<=0))
  #error "If you want to use Sequential API, you have to define NTCPIP__MEMP_NUM_TCPIP_MSG_API>=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_NETCONN && NTCPIP__LWIP_SOCKET)
  #error "If you want to use Socket API, you have to define NTCPIP__LWIP_NETCONN=1 in your lwipopts.h"
#endif
#if (((!NTCPIP__LWIP_DHCP) || (!NTCPIP__LWIP_AUTOIP)) && NTCPIP__LWIP_DHCP_AUTOIP_COOP)
  #error "If you want to use DHCP/AUTOIP cooperation mode, you have to define NTCPIP__LWIP_DHCP=1 and NTCPIP__LWIP_AUTOIP=1 in your lwipopts.h"
#endif
#if (((!NTCPIP__LWIP_DHCP) || (!NTCPIP__LWIP_ARP)) && NTCPIP__DHCP_DOES_ARP_CHECK)
  #error "If you want to use DHCP ARP checking, you have to define NTCPIP__LWIP_DHCP=1 and NTCPIP__LWIP_ARP=1 in your lwipopts.h"
#endif
#if (!NTCPIP__LWIP_ARP && NTCPIP__LWIP_AUTOIP)
  #error "If you want to use AUTOIP, you have to define NTCPIP__LWIP_ARP=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_SNMP && (NTCPIP__SNMP_CONCURRENT_REQUESTS<=0))
  #error "If you want to use SNMP, you have to define NTCPIP__SNMP_CONCURRENT_REQUESTS>=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_SNMP && (NTCPIP__SNMP_TRAP_DESTINATIONS<=0))
  #error "If you want to use SNMP, you have to define NTCPIP__SNMP_TRAP_DESTINATIONS>=1 in your lwipopts.h"
#endif
#if (NTCPIP__LWIP_TCP && ((NTCPIP__LWIP_EVENT_API && NTCPIP__LWIP_CALLBACK_API) || (!NTCPIP__LWIP_EVENT_API && !NTCPIP__LWIP_CALLBACK_API)))
  #error "One and exactly one of NTCPIP__LWIP_EVENT_API and NTCPIP__LWIP_CALLBACK_API has to be enabled in your lwipopts.h"
#endif
/* There must be sufficient timeouts, taking into account requirements of the subsystems. */
#if ((NTCPIP__NO_SYS==0) && (NTCPIP__MEMP_NUM_SYS_TIMEOUT < (NTCPIP__LWIP_TCP + NTCPIP__IP_REASSEMBLY + NTCPIP__LWIP_ARP + (2*NTCPIP__LWIP_DHCP) + NTCPIP__LWIP_AUTOIP + NTCPIP__LWIP_IGMP + NTCPIP__LWIP_DNS + NTCPIP__PPP_SUPPORT)))
  #error "NTCPIP__MEMP_NUM_SYS_TIMEOUT is too low to accomodate all required timeouts"
#endif
#if (NTCPIP__IP_REASSEMBLY && (NTCPIP__MEMP_NUM_REASSDATA > NTCPIP__IP_REASS_MAX_PBUFS))
  #error "NTCPIP__MEMP_NUM_REASSDATA > NTCPIP__IP_REASS_MAX_PBUFS doesn't make sense since each struct ip_reassdata must hold 2 pbufs at least!"
#endif
#if (NTCPIP__MEM_LIBC_MALLOC && NTCPIP__MEM_USE_POOLS)
  #error "NTCPIP__MEM_LIBC_MALLOC and NTCPIP__MEM_USE_POOLS may not both be simultaneously enabled in your lwipopts.h"
#endif
#if (NTCPIP__MEM_USE_POOLS && !NTCPIP__MEMP_USE_CUSTOM_POOLS)
  #error "NTCPIP__MEM_USE_POOLS requires custom pools (NTCPIP__MEMP_USE_CUSTOM_POOLS) to be enabled in your lwipopts.h"
#endif
#if (NTCPIP__PBUF_POOL_BUFSIZE <= NTCPIP__MEM_ALIGNMENT)
  #error "NTCPIP__PBUF_POOL_BUFSIZE must be greater than NTCPIP__MEM_ALIGNMENT or the offset may take the full first pbuf"
#endif
#if (NTCPIP__TCP_QUEUE_OOSEQ && !NTCPIP__LWIP_TCP)
  #error "NTCPIP__TCP_QUEUE_OOSEQ requires NTCPIP__LWIP_TCP"
#endif
#if (DNS_LOCAL_HOSTLIST && !DNS_LOCAL_HOSTLIST_IS_DYNAMIC && !(defined(DNS_LOCAL_HOSTLIST_INIT)))
  #error "you have to define define DNS_LOCAL_HOSTLIST_INIT {{'host1', 0x123}, {'host2', 0x234}} to initialize DNS_LOCAL_HOSTLIST"
#endif
#if NTCPIP__PPP_SUPPORT && !NTCPIP__PPPOS_SUPPORT & !NTCPIP__PPPOE_SUPPORT
  #error "NTCPIP__PPP_SUPPORT needs either NTCPIP__PPPOS_SUPPORT or NTCPIP__PPPOE_SUPPORT turned on"
#endif


/* Compile-time checks for deprecated options.
 */
#ifdef MEMP_NUM_TCPIP_MSG
  #error "MEMP_NUM_TCPIP_MSG option is deprecated. Remove it from your lwipopts.h."
#endif
#ifdef MEMP_NUM_API_MSG
  #error "MEMP_NUM_API_MSG option is deprecated. Remove it from your lwipopts.h."
#endif
#ifdef TCP_REXMIT_DEBUG
  #error "TCP_REXMIT_DEBUG option is deprecated. Remove it from your lwipopts.h."
#endif
#ifdef RAW_STATS
  #error "RAW_STATS option is deprecated. Remove it from your lwipopts.h."
#endif
#ifdef ETHARP_QUEUE_FIRST
  #error "ETHARP_QUEUE_FIRST option is deprecated. Remove it from your lwipopts.h."
#endif
#ifdef ETHARP_ALWAYS_INSERT
  #error "ETHARP_ALWAYS_INSERT option is deprecated. Remove it from your lwipopts.h."
#endif
#if NTCPIP__SO_REUSE
/* I removed the lot since this was an ugly hack. It broke the raw-API.
   It also came with many ugly goto's, Christiaan Simons. */
  #error "NTCPIP__SO_REUSE currently unavailable, this was a hack"
#endif

#ifdef LWIP_DEBUG
static void
lwip_sanity_check(void)
{
  /* Warnings */
#if NTCPIP__LWIP_NETCONN
  if (NTCPIP__MEMP_NUM_NETCONN > (NTCPIP__MEMP_NUM_TCP_PCB+NTCPIP__MEMP_NUM_TCP_PCB_LISTEN+NTCPIP__MEMP_NUM_UDP_PCB+NTCPIP__MEMP_NUM_RAW_PCB))
    NTCPIP__PLATFORM_DIAG(("lwip_sanity_check: WARNING: NTCPIP__MEMP_NUM_NETCONN should be less than the sum of MEMP_NUM_{TCP,RAW,UDP}_PCB+NTCPIP__MEMP_NUM_TCP_PCB_LISTEN\n"));
#endif /* NTCPIP__LWIP_NETCONN */
#if NTCPIP__LWIP_TCP
  if (NTCPIP__MEMP_NUM_TCP_SEG < NTCPIP__TCP_SND_QUEUELEN)
    NTCPIP__PLATFORM_DIAG(("lwip_sanity_check: WARNING: NTCPIP__MEMP_NUM_TCP_SEG should be at least as big as NTCPIP__TCP_SND_QUEUELEN\n"));
  if (NTCPIP__TCP_SND_BUF < 2 * NTCPIP__TCP_MSS)
    NTCPIP__PLATFORM_DIAG(("lwip_sanity_check: WARNING: NTCPIP__TCP_SND_BUF must be at least as much as (2 * NTCPIP__TCP_MSS) for things to work smoothly\n"));
  if (NTCPIP__TCP_SND_QUEUELEN < (2 * (NTCPIP__TCP_SND_BUF/NTCPIP__TCP_MSS)))
    NTCPIP__PLATFORM_DIAG(("lwip_sanity_check: WARNING: NTCPIP__TCP_SND_QUEUELEN must be at least as much as (2 * NTCPIP__TCP_SND_BUF/NTCPIP__TCP_MSS) for things to work\n"));
  if (NTCPIP__TCP_SNDLOWAT > NTCPIP__TCP_SND_BUF)
    NTCPIP__PLATFORM_DIAG(("lwip_sanity_check: WARNING: NTCPIP__TCP_SNDLOWAT must be less than or equal to NTCPIP__TCP_SND_BUF.\n"));
  if (NTCPIP__TCP_WND > (NTCPIP__PBUF_POOL_SIZE*NTCPIP__PBUF_POOL_BUFSIZE))
    NTCPIP__PLATFORM_DIAG(("lwip_sanity_check: WARNING: NTCPIP__TCP_WND is larger than space provided by NTCPIP__PBUF_POOL_SIZE*NTCPIP__PBUF_POOL_BUFSIZE\n"));
  if (NTCPIP__TCP_WND < NTCPIP__TCP_MSS)
    NTCPIP__PLATFORM_DIAG(("lwip_sanity_check: WARNING: NTCPIP__TCP_WND is smaller than MSS\n"));
#endif /* NTCPIP__LWIP_TCP */
}
#else  /* LWIP_DEBUG */
#define lwip_sanity_check()
#endif /* LWIP_DEBUG */

/**
 * Perform Sanity check of user-configurable values, and initialize all modules.
 */
void
ntcpip__lwipInit(void)
{
  /* Sanity check user-configurable values */
  lwip_sanity_check();

  /* Modules initialization */
  stats_init();
  ntcpip__sysInit();
  ntcpip__memInit();
  ntcpip__mempInit();
  ntcpip__pbufInit();
  ntcpip__netifInit();
#if NTCPIP__LWIP_SOCKET
  lwip_socket_init();
#endif /* NTCPIP__LWIP_SOCKET */
  ip_init();
#if NTCPIP__LWIP_ARP
  etharp_init();
#endif /* NTCPIP__LWIP_ARP */
#if NTCPIP__LWIP_RAW
  raw_init();
#endif /* NTCPIP__LWIP_RAW */
#if NTCPIP__LWIP_UDP
  udp_init();
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
  ntcpip__tcpInit();
#endif /* NTCPIP__LWIP_TCP */
#if NTCPIP__LWIP_SNMP
  ntcpip__snmpInit();
#endif /* NTCPIP__LWIP_SNMP */
#if NTCPIP__LWIP_AUTOIP
  ntcpip__autoipInit();
#endif /* NTCPIP__LWIP_AUTOIP */
#if NTCPIP__LWIP_IGMP
  ntcpip__igmpInit();
#endif /* NTCPIP__LWIP_IGMP */
#if NTCPIP__LWIP_DNS
  dns_init();
#endif /* NTCPIP__LWIP_DNS */
}


