/**
 * @file
 * Transmission Control Protocol, outgoing traffic
 *
 * The output functions of TCP.
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_TCP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/tcp.h"
#include "../def.h"
#include "../mem.h"
#include "../memp.h"
#include "ntcpip/sys.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "ntcpip/inet.h"
#include "../inet_chksum.h"
#include "../stats.h"
#include "ntcpip/snmp.h"

#include <string.h>

/* Forward declarations.*/
static void tcp_output_segment(struct ntcpip__tcpSeg *seg, struct ntcpip__tcpPcb *pcb);

static struct ntcpip__tcpHdr *
tcp_output_set_header(struct ntcpip__tcpPcb *pcb, struct ntcpip_pbuf *p, int optlen,
                      Uint32 seqno_be /* already in network byte order */)
{
  struct ntcpip__tcpHdr *tcphdr = p->payload;
  tcphdr->src = ntcpip_htons(pcb->local_port);
  tcphdr->dest = ntcpip_htons(pcb->remote_port);
  tcphdr->seqno = seqno_be;
  tcphdr->ackno = ntcpip_htonl(pcb->rcv_nxt);
  NTCPIP__TCPH_FLAGS_SET(tcphdr, NTCPIP__TCP_ACK);
  tcphdr->wnd = ntcpip_htons(pcb->rcv_ann_wnd);
  tcphdr->urgp = 0;
  NTCPIP__TCPH_HDRLEN_SET(tcphdr, (5 + optlen / 4));
  tcphdr->chksum = 0;

  /* If we're sending a packet, update the announced right window edge */
  pcb->rcv_ann_right_edge = pcb->rcv_nxt + pcb->rcv_ann_wnd;

  return tcphdr;
}

/**
 * Called by ntcpip__tcpClose() to send a segment including flags but not data.
 *
 * @param pcb the tcp_pcb over which to send a segment
 * @param flags the flags to set in the segment header
 * @return NTCPIP_ERR_OK if sent, another ntcpip_Err otherwise
 */
ntcpip_Err
ntcpip__tcpSendCtrl(struct ntcpip__tcpPcb *pcb, Uint8 flags)
{
  /* no data, no length, flags, copy=1, no optdata */
  return ntcpip__tcpEnqueue(pcb, NULL, 0, flags, NTCPIP_TCP_W_FLG_COPY, 0);
}

/**
 * Write data for sending (but does not send it immediately).
 *
 * It waits in the expectation of more data being sent soon (as
 * it can send them more efficiently by combining them together).
 * To prompt the system to send data now, call ntcpip__tcpOutput() after
 * calling ntcpip_tcpWrite().
 * 
 * @param pcb Protocol control block of the TCP connection to enqueue data for.
 * @param data pointer to the data to send
 * @param len length (in bytes) of the data to send
 * @param apiflags combination of following flags :
 * - NTCPIP_TCP_W_FLG_COPY (0x01) data will be copied into memory belonging to the stack
 * - NTCPIP_TCP_W_FLAG_MORE (0x02) for TCP connection, PSH flag will be set on last segment sent,
 * @return NTCPIP_ERR_OK if enqueued, another ntcpip_Err on error
 * 
 * @see ntcpip_tcpWrite()
 */
ntcpip_Err
ntcpip_tcpWrite(struct ntcpip__tcpPcb *pcb, const void *data, Uint16 len, Uint8 apiflags)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG, ("ntcpip_tcpWrite(pcb=%p, data=%p, len=%"U16_F", apiflags=%"U16_F")\n", (void *)pcb,
    data, len, (Uint16)apiflags));
  /* connection is in valid state for data transmission? */
  if (pcb->state == NTCPIP_ESTABLISHED ||
     pcb->state == NTCPIP_CLOSE_WAIT ||
     pcb->state == NTCPIP_SYN_SENT ||
     pcb->state == NTCPIP_SYN_RCVD) {
    if (len > 0) {
#if LWIP_TCP_TIMESTAMPS
      return ntcpip__tcpEnqueue(pcb, (void *)data, len, 0, apiflags, 
                         pcb->flags & NTCPIP_TF_TIMESTAMP ? TF_SEG_OPTS_TS : 0);
#else
      return ntcpip__tcpEnqueue(pcb, (void *)data, len, 0, apiflags, 0);
#endif
    }
    return NTCPIP_ERR_OK;
  } else {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_STATE | NTCPIP__LWIP_DBG_LEVEL_SEVERE, ("ntcpip_tcpWrite() called in invalid state\n"));
    return NTCPIP_ERR_CONN;
  }
}

/**
 * Enqueue data and/or TCP options for transmission
 *
 * Called by ntcpip__tcpConnect(), tcp_listen_input(), ntcpip__tcpSendCtrl() and ntcpip_tcpWrite().
 *
 * @param pcb Protocol control block for the TCP connection to enqueue data for.
 * @param arg Pointer to the data to be enqueued for sending.
 * @param len Data length in bytes
 * @param flags tcp header flags to set in the outgoing segment
 * @param apiflags combination of following flags :
 * - NTCPIP_TCP_W_FLG_COPY (0x01) data will be copied into memory belonging to the stack
 * - NTCPIP_TCP_W_FLAG_MORE (0x02) for TCP connection, PSH flag will be set on last segment sent,
 * @param optflags options to include in segment later on (see definition of struct ntcpip__tcpSeg)
 */
ntcpip_Err
ntcpip__tcpEnqueue(struct ntcpip__tcpPcb *pcb, void *arg, Uint16 len,
            Uint8 flags, Uint8 apiflags, Uint8 optflags)
{
  struct ntcpip_pbuf *p;
  struct ntcpip__tcpSeg *seg, *useg, *queue;
  Uint32 seqno;
  Uint16 left, seglen;
  void *ptr;
  Uint16 queuelen;
  Uint8 optlen;

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG, 
              ("ntcpip__tcpEnqueue(pcb=%p, arg=%p, len=%"U16_F", flags=%"X16_F", apiflags=%"U16_F")\n",
               (void *)pcb, arg, len, (Uint16)flags, (Uint16)apiflags));
  NTCPIP__LWIP_ERROR("ntcpip__tcpEnqueue: packet needs payload, options, or SYN/FIN (programmer violates API)",
             ((len != 0) || (optflags != 0) || ((flags & (NTCPIP__TCP_SYN | NTCPIP__TCP_FIN)) != 0)),
             return NTCPIP_ERR_ARG;);
  NTCPIP__LWIP_ERROR("ntcpip__tcpEnqueue: len != 0 || arg == NULL (programmer violates API)", 
             ((len != 0) || (arg == NULL)), return NTCPIP_ERR_ARG;);

  /* fail on too much data */
  if (len > pcb->snd_buf) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_WARNING,
      ("ntcpip__tcpEnqueue: too much data (len=%"U16_F" > snd_buf=%"U16_F")\n", len, pcb->snd_buf));
    pcb->flags |= NTCPIP_TF_NAGLEMEMERR;
    return NTCPIP_ERR_MEM;
  }
  left = len;
  ptr = arg;

  optlen = NTCPIP__TCP_OPT_LENGTH(optflags);

  /* seqno will be the sequence number of the first segment enqueued
   * by the call to this function. */
  seqno = pcb->snd_lbb;

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG, ("ntcpip__tcpEnqueue: queuelen: %"U16_F"\n", (Uint16)pcb->snd_queuelen));

  /* If total number of pbufs on the unsent/unacked queues exceeds the
   * configured maximum, return an error */
  queuelen = pcb->snd_queuelen;
  /* check for configured max queuelen and possible overflow */
  if ((queuelen >= NTCPIP__TCP_SND_QUEUELEN) || (queuelen > NTCPIP_TCP_SNDQUEUELEN_OF)) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_WARNING,
      ("ntcpip__tcpEnqueue: too long queue %"U16_F" (max %"U16_F")\n", queuelen, NTCPIP__TCP_SND_QUEUELEN));
    TCP_STATS_INC(tcp.memerr);
    pcb->flags |= NTCPIP_TF_NAGLEMEMERR;
    return NTCPIP_ERR_MEM;
  }
  if (queuelen != 0) {
    NTCPIP__LWIP_ASSERT("ntcpip__tcpEnqueue: pbufs on queue => at least one queue non-empty",
      pcb->unacked != NULL || pcb->unsent != NULL);
  } else {
    NTCPIP__LWIP_ASSERT("ntcpip__tcpEnqueue: no pbufs on queue => both queues empty",
      pcb->unacked == NULL && pcb->unsent == NULL);
  }

  /* First, break up the data into segments and tuck them together in
   * the local "queue" variable. */
  useg = queue = seg = NULL;
  seglen = 0;
  while (queue == NULL || left > 0) {
    /* The segment length (including options) should be at most the MSS */
    seglen = left > (pcb->mss - optlen) ? (pcb->mss - optlen) : left;

    /* Allocate memory for ntcpip__tcpSeg, and fill in fields. */
    seg = ntcpip__mempMalloc(MEMP_TCP_SEG);
    if (seg == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, 
                  ("ntcpip__tcpEnqueue: could not allocate memory for ntcpip__tcpSeg\n"));
      goto memerr;
    }
    seg->next = NULL;
    seg->p = NULL;

    /* first segment of to-be-queued data? */
    if (queue == NULL) {
      queue = seg;
    }
    /* subsequent segments of to-be-queued data */
    else {
      /* Attach the segment to the end of the queued segments */
      NTCPIP__LWIP_ASSERT("useg != NULL", useg != NULL);
      useg->next = seg;
    }
    /* remember last segment of to-be-queued data for next iteration */
    useg = seg;

    /* If copy is set, memory should be allocated
     * and data copied into pbuf, otherwise data comes from
     * ROM or other static memory, and need not be copied.  */
    if (apiflags & NTCPIP_TCP_W_FLG_COPY) {
      if ((seg->p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, seglen + optlen, NTCPIP_PBUF_RAM)) == NULL) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, 
                    ("ntcpip__tcpEnqueue : could not allocate memory for pbuf copy size %"U16_F"\n", seglen));
        goto memerr;
      }
      NTCPIP__LWIP_ASSERT("check that first pbuf can hold the complete seglen",
                  (seg->p->len >= seglen + optlen));
      queuelen += ntcpip__pbufClen(seg->p);
      if (arg != NULL) {
        NTCPIP__MEMCPY((char *)seg->p->payload + optlen, ptr, seglen);
      }
      seg->dataptr = seg->p->payload;
    }
    /* do not copy data */
    else {
      /* First, allocate a pbuf for the headers. */
      if ((seg->p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, optlen, NTCPIP_PBUF_RAM)) == NULL) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, 
                    ("ntcpip__tcpEnqueue: could not allocate memory for header pbuf\n"));
        goto memerr;
      }
      queuelen += ntcpip__pbufClen(seg->p);

      /* Second, allocate a pbuf for holding the data.
       * since the referenced data is available at least until it is sent out on the
       * link (as it has to be ACKed by the remote party) we can safely use NTCPIP_PBUF_ROM
       * instead of NTCPIP_PBUF_REF here.
       */
      if (left > 0) {
        if ((p = ntcpip_pbufAlloc(NTCPIP_PBUF_RAW, seglen, NTCPIP_PBUF_ROM)) == NULL) {
          /* If allocation fails, we have to deallocate the header pbuf as well. */
          ntcpip_pbufFree(seg->p);
          seg->p = NULL;
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, 
                      ("ntcpip__tcpEnqueue: could not allocate memory for zero-copy pbuf\n"));
          goto memerr;
        }
        ++queuelen;
        /* reference the non-volatile payload data */
        p->payload = ptr;
        seg->dataptr = ptr;

        /* Concatenate the headers and data pbufs together. */
        ntcpip_pbufCat(seg->p/*header*/, p/*data*/);
        p = NULL;
      }
    }

    /* Now that there are more segments queued, we check again if the
    length of the queue exceeds the configured maximum or overflows. */
    if ((queuelen > NTCPIP__TCP_SND_QUEUELEN) || (queuelen > NTCPIP_TCP_SNDQUEUELEN_OF)) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
        ("ntcpip__tcpEnqueue: queue too long %"U16_F" (%"U16_F")\n", queuelen, NTCPIP__TCP_SND_QUEUELEN));
      goto memerr;
    }

    seg->len = seglen;

    /* build TCP header */
    if (ntcpip__pbufHeader(seg->p, NTCPIP__TCP_HLEN)) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip__tcpEnqueue: no room for TCP header in pbuf.\n"));
      TCP_STATS_INC(tcp.err);
      goto memerr;
    }
    seg->tcphdr = seg->p->payload;
    seg->tcphdr->src = ntcpip_htons(pcb->local_port);
    seg->tcphdr->dest = ntcpip_htons(pcb->remote_port);
    seg->tcphdr->seqno = ntcpip_htonl(seqno);
    seg->tcphdr->urgp = 0;
    NTCPIP__TCPH_FLAGS_SET(seg->tcphdr, flags);
    /* don't fill in tcphdr->ackno and tcphdr->wnd until later */

    seg->flags = optflags;

    /* Set the length of the header */
    NTCPIP__TCPH_HDRLEN_SET(seg->tcphdr, (5 + optlen / 4));
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__tcpEnqueue: queueing %"U32_F":%"U32_F" (0x%"X16_F")\n",
      ntcpip_ntohl(seg->tcphdr->seqno),
      ntcpip_ntohl(seg->tcphdr->seqno) + NTCPIP__TCP_TCPLEN(seg),
      (Uint16)flags));

    left -= seglen;
    seqno += seglen;
    ptr = (void *)((Uint8 *)ptr + seglen);
  }

  /* Now that the data to be enqueued has been broken up into TCP
  segments in the queue variable, we add them to the end of the
  pcb->unsent queue. */
  if (pcb->unsent == NULL) {
    useg = NULL;
  }
  else {
    for (useg = pcb->unsent; useg->next != NULL; useg = useg->next);
  }
  /* { useg is last segment on the unsent queue, NULL if list is empty } */

  /* If there is room in the last pbuf on the unsent queue,
  chain the first pbuf on the queue together with that. */
  if (useg != NULL &&
    NTCPIP__TCP_TCPLEN(useg) != 0 &&
    !(NTCPIP__TCPH_FLAGS(useg->tcphdr) & (NTCPIP__TCP_SYN | NTCPIP__TCP_FIN)) &&
    (!(flags & (NTCPIP__TCP_SYN | NTCPIP__TCP_FIN)) || (flags == NTCPIP__TCP_FIN)) &&
    /* fit within max seg size */
    (useg->len + queue->len <= pcb->mss) &&
    /* only concatenate segments with the same options */
    (useg->flags == queue->flags) &&
    /* segments are consecutive */
    (ntcpip_ntohl(useg->tcphdr->seqno) + useg->len == ntcpip_ntohl(queue->tcphdr->seqno)) ) {
    /* Remove TCP header from first segment of our to-be-queued list */
    if(ntcpip__pbufHeader(queue->p, -(NTCPIP__TCP_HLEN + optlen))) {
      /* Can we cope with this failing?  Just assert for now */
      NTCPIP__LWIP_ASSERT("ntcpip__pbufHeader failed\n", 0);
      TCP_STATS_INC(tcp.err);
      goto memerr;
    }
    if (queue->p->len == 0) {
      /* free the first (header-only) pbuf if it is now empty (contained only headers) */
      struct ntcpip_pbuf *old_q = queue->p;
      queue->p = queue->p->next;
      old_q->next = NULL;
      queuelen--;
      ntcpip_pbufFree(old_q);
    }
    if (flags & NTCPIP__TCP_FIN) {
      /* the new segment contains only FIN, no data -> put the FIN into the last segment */
      NTCPIP__LWIP_ASSERT("FIN enqueued together with data", queue->p == NULL && queue->len == 0);
      NTCPIP__TCPH_SET_FLAG(useg->tcphdr, NTCPIP__TCP_FIN);
    } else {
      NTCPIP__LWIP_ASSERT("zero-length pbuf", (queue->p != NULL) && (queue->p->len > 0));
      ntcpip_pbufCat(useg->p, queue->p);
      useg->len += queue->len;
      useg->next = queue->next;
    }

    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE, ("ntcpip__tcpEnqueue: chaining segments, new len %"U16_F"\n", useg->len));
    if (seg == queue) {
      seg = useg;
      seglen = useg->len;
    }
    ntcpip__mempFree(MEMP_TCP_SEG, queue);
  }
  else {
    /* empty list */
    if (useg == NULL) {
      /* initialize list with this segment */
      pcb->unsent = queue;
    }
    /* enqueue segment */
    else {
      useg->next = queue;
    }
  }
  if ((flags & NTCPIP__TCP_SYN) || (flags & NTCPIP__TCP_FIN)) {
    ++len;
  }
  if (flags & NTCPIP__TCP_FIN) {
    pcb->flags |= NTCPIP_TF_FIN;
  }
  pcb->snd_lbb += len;

  pcb->snd_buf -= len;

  /* update number of segments on the queues */
  pcb->snd_queuelen = queuelen;
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG, ("ntcpip__tcpEnqueue: %"S16_F" (after enqueued)\n", pcb->snd_queuelen));
  if (pcb->snd_queuelen != 0) {
    NTCPIP__LWIP_ASSERT("ntcpip__tcpEnqueue: valid queue length",
      pcb->unacked != NULL || pcb->unsent != NULL);
  }

  /* Set the PSH flag in the last segment that we enqueued, but only
  if the segment has data (indicated by seglen > 0). */
  if (seg != NULL && seglen > 0 && seg->tcphdr != NULL && ((apiflags & NTCPIP_TCP_W_FLAG_MORE)==0)) {
    NTCPIP__TCPH_SET_FLAG(seg->tcphdr, NTCPIP__TCP_PSH);
  }

  return NTCPIP_ERR_OK;
memerr:
  pcb->flags |= NTCPIP_TF_NAGLEMEMERR;
  TCP_STATS_INC(tcp.memerr);

  if (queue != NULL) {
    ntcpip__tcpSegsFree(queue);
  }
  if (pcb->snd_queuelen != 0) {
    NTCPIP__LWIP_ASSERT("ntcpip__tcpEnqueue: valid queue length", pcb->unacked != NULL ||
      pcb->unsent != NULL);
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_QLEN_DEBUG | NTCPIP__LWIP_DBG_STATE, ("ntcpip__tcpEnqueue: %"S16_F" (with mem err)\n", pcb->snd_queuelen));
  return NTCPIP_ERR_MEM;
}


#if LWIP_TCP_TIMESTAMPS
/* Build a timestamp option (12 bytes long) at the specified options pointer)
 *
 * @param pcb tcp_pcb
 * @param opts option pointer where to store the timestamp option
 */
static void
tcp_build_timestamp_option(struct ntcpip__tcpPcb *pcb, Uint32 *opts)
{
  /* Pad with two NOP options to make everything nicely aligned */
  opts[0] = ntcpip_htonl(0x0101080A);
  opts[1] = ntcpip_htonl(ntcpip__sysNow());
  opts[2] = ntcpip_htonl(pcb->ts_recent);
}
#endif

/** Send an ACK without data.
 *
 * @param pcb Protocol control block for the TCP connection to send the ACK
 */
ntcpip_Err
ntcpip__tcpSendEmptyAck(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip_pbuf *p;
  struct ntcpip__tcpHdr *tcphdr;
  Uint8 optlen = 0;

#if LWIP_TCP_TIMESTAMPS
  if (pcb->flags & NTCPIP_TF_TIMESTAMP) {
    optlen = NTCPIP__TCP_OPT_LENGTH(TF_SEG_OPTS_TS);
  }
#endif
  p = ntcpip_pbufAlloc(NTCPIP_PBUF_IP, NTCPIP__TCP_HLEN + optlen, NTCPIP_PBUF_RAM);
  if (p == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG, ("ntcpip__tcpOutput: (ACK) could not allocate pbuf\n"));
    return NTCPIP_ERR_BUF;
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG, 
              ("ntcpip__tcpOutput: sending ACK for %"U32_F"\n", pcb->rcv_nxt));
  /* remove ACK flags from the PCB, as we send an empty ACK now */
  pcb->flags &= ~(NTCPIP_TF_ACK_DELAY | NTCPIP_TF_ACK_NOW);

  tcphdr = tcp_output_set_header(pcb, p, optlen, ntcpip_htonl(pcb->snd_nxt));

  /* NB. MSS option is only sent on SYNs, so ignore it here */
#if LWIP_TCP_TIMESTAMPS
  pcb->ts_lastacksent = pcb->rcv_nxt;

  if (pcb->flags & NTCPIP_TF_TIMESTAMP) {
    tcp_build_timestamp_option(pcb, (Uint32 *)(tcphdr + 1));
  }
#endif 

#if NTCPIP__CHECKSUM_GEN_TCP
  tcphdr->chksum = ntcpip__inetChksumPseudo(p, &(pcb->local_ip), &(pcb->remote_ip),
        IP_PROTO_TCP, p->tot_len);
#endif
#if NTCPIP__LWIP_NETIF_HWADDRHINT
  ip_output_hinted(p, &(pcb->local_ip), &(pcb->remote_ip), pcb->ttl, pcb->tos,
      IP_PROTO_TCP, &(pcb->addr_hint));
#else /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  ntcpip__ipOutput(p, &(pcb->local_ip), &(pcb->remote_ip), pcb->ttl, pcb->tos,
      IP_PROTO_TCP);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  ntcpip_pbufFree(p);

  return NTCPIP_ERR_OK;
}

/**
 * Find out what we can send and send it
 *
 * @param pcb Protocol control block for the TCP connection to send data
 * @return NTCPIP_ERR_OK if data has been sent or nothing to send
 *         another ntcpip_Err on error
 */
ntcpip_Err
ntcpip__tcpOutput(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip__tcpSeg *seg, *useg;
  Uint32 wnd, snd_nxt;
#if NTCPIP__TCP_CWND_DEBUG
  Int16 i = 0;
#endif /* NTCPIP__TCP_CWND_DEBUG */

  /* First, check if we are invoked by the TCP input processing
     code. If so, we do not output anything. Instead, we rely on the
     input processing code to call us when input processing is done
     with. */
  if (ntcpip__tcpInputPcb == pcb) {
    return NTCPIP_ERR_OK;
  }

  wnd = LWIP_MIN(pcb->snd_wnd, pcb->cwnd);

  seg = pcb->unsent;

  /* If the NTCPIP_TF_ACK_NOW flag is set and no data will be sent (either
   * because the ->unsent queue is empty or because the window does
   * not allow it), construct an empty ACK segment and send it.
   *
   * If data is to be sent, we will just piggyback the ACK (see below).
   */
  if (pcb->flags & NTCPIP_TF_ACK_NOW &&
     (seg == NULL ||
      ntcpip_ntohl(seg->tcphdr->seqno) - pcb->lastack + seg->len > wnd)) {
     return ntcpip__tcpSendEmptyAck(pcb);
  }

  /* useg should point to last segment on unacked queue */
  useg = pcb->unacked;
  if (useg != NULL) {
    for (; useg->next != NULL; useg = useg->next);
  }

#if NTCPIP__TCP_OUTPUT_DEBUG
  if (seg == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG, ("ntcpip__tcpOutput: nothing to send (%p)\n",
                                   (void*)pcb->unsent));
  }
#endif /* NTCPIP__TCP_OUTPUT_DEBUG */
#if NTCPIP__TCP_CWND_DEBUG
  if (seg == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_CWND_DEBUG, ("ntcpip__tcpOutput: snd_wnd %"U16_F
                                 ", cwnd %"U16_F", wnd %"U32_F
                                 ", seg == NULL, ack %"U32_F"\n",
                                 pcb->snd_wnd, pcb->cwnd, wnd, pcb->lastack));
  } else {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_CWND_DEBUG, 
                ("ntcpip__tcpOutput: snd_wnd %"U16_F", cwnd %"U16_F", wnd %"U32_F
                 ", effwnd %"U32_F", seq %"U32_F", ack %"U32_F"\n",
                 pcb->snd_wnd, pcb->cwnd, wnd,
                 ntcpip_ntohl(seg->tcphdr->seqno) - pcb->lastack + seg->len,
                 ntcpip_ntohl(seg->tcphdr->seqno), pcb->lastack));
  }
#endif /* NTCPIP__TCP_CWND_DEBUG */
  /* data available and window allows it to be sent? */
  while (seg != NULL &&
         ntcpip_ntohl(seg->tcphdr->seqno) - pcb->lastack + seg->len <= wnd) {
    NTCPIP__LWIP_ASSERT("RST not expected here!", 
                (NTCPIP__TCPH_FLAGS(seg->tcphdr) & NTCPIP__TCP_RST) == 0);
    /* Stop sending if the nagle algorithm would prevent it
     * Don't stop:
     * - if ntcpip__tcpEnqueue had a memory error before (prevent delayed ACK timeout) or
     * - if FIN was already enqueued for this PCB (SYN is always alone in a segment -
     *   either seg->next != NULL or pcb->unacked == NULL;
     *   RST is no sent using ntcpip__tcpEnqueue/ntcpip__tcpOutput.
     */
    if((ntcpip__tcpDoOutputNagle(pcb) == 0) &&
      ((pcb->flags & (NTCPIP_TF_NAGLEMEMERR | NTCPIP_TF_FIN)) == 0)){
      break;
    }
#if NTCPIP__TCP_CWND_DEBUG
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_CWND_DEBUG, ("ntcpip__tcpOutput: snd_wnd %"U16_F", cwnd %"U16_F", wnd %"U32_F", effwnd %"U32_F", seq %"U32_F", ack %"U32_F", i %"S16_F"\n",
                            pcb->snd_wnd, pcb->cwnd, wnd,
                            ntcpip_ntohl(seg->tcphdr->seqno) + seg->len -
                            pcb->lastack,
                            ntcpip_ntohl(seg->tcphdr->seqno), pcb->lastack, i));
    ++i;
#endif /* NTCPIP__TCP_CWND_DEBUG */

    pcb->unsent = seg->next;

    if (pcb->state != NTCPIP_SYN_SENT) {
      NTCPIP__TCPH_SET_FLAG(seg->tcphdr, NTCPIP__TCP_ACK);
      pcb->flags &= ~(NTCPIP_TF_ACK_DELAY | NTCPIP_TF_ACK_NOW);
    }

    tcp_output_segment(seg, pcb);
    snd_nxt = ntcpip_ntohl(seg->tcphdr->seqno) + NTCPIP__TCP_TCPLEN(seg);
    if (NTCPIP__TCP_SEQ_LT(pcb->snd_nxt, snd_nxt)) {
      pcb->snd_nxt = snd_nxt;
    }
    /* put segment on unacknowledged list if length > 0 */
    if (NTCPIP__TCP_TCPLEN(seg) > 0) {
      seg->next = NULL;
      /* unacked list is empty? */
      if (pcb->unacked == NULL) {
        pcb->unacked = seg;
        useg = seg;
      /* unacked list is not empty? */
      } else {
        /* In the case of fast retransmit, the packet should not go to the tail
         * of the unacked queue, but rather somewhere before it. We need to check for
         * this case. -STJ Jul 27, 2004 */
        if (NTCPIP__TCP_SEQ_LT(ntcpip_ntohl(seg->tcphdr->seqno), ntcpip_ntohl(useg->tcphdr->seqno))){
          /* add segment to before tail of unacked list, keeping the list sorted */
          struct ntcpip__tcpSeg **cur_seg = &(pcb->unacked);
          while (*cur_seg &&
            NTCPIP__TCP_SEQ_LT(ntcpip_ntohl((*cur_seg)->tcphdr->seqno), ntcpip_ntohl(seg->tcphdr->seqno))) {
              cur_seg = &((*cur_seg)->next );
          }
          seg->next = (*cur_seg);
          (*cur_seg) = seg;
        } else {
          /* add segment to tail of unacked list */
          useg->next = seg;
          useg = useg->next;
        }
      }
    /* do not queue empty segments on the unacked list */
    } else {
      ntcpip__tcpSegFree(seg);
    }
    seg = pcb->unsent;
  }

  if (seg != NULL && pcb->persist_backoff == 0 && 
      ntcpip_ntohl(seg->tcphdr->seqno) - pcb->lastack + seg->len > pcb->snd_wnd) {
    /* prepare for persist timer */
    pcb->persist_cnt = 0;
    pcb->persist_backoff = 1;
  }

  pcb->flags &= ~NTCPIP_TF_NAGLEMEMERR;
  return NTCPIP_ERR_OK;
}

/**
 * Called by ntcpip__tcpOutput() to actually send a TCP segment over IP.
 *
 * @param seg the ntcpip__tcpSeg to send
 * @param pcb the tcp_pcb for the TCP connection used to send the segment
 */
static void
tcp_output_segment(struct ntcpip__tcpSeg *seg, struct ntcpip__tcpPcb *pcb)
{
  Uint16 len;
  struct ntcpip__netif *netif;
  Uint32 *opts;

  /** @bug Exclude retransmitted segments from this count. */
  ntcpip__snmpIncTcpoutsegs();

  /* The TCP header has already been constructed, but the ackno and
   wnd fields remain. */
  seg->tcphdr->ackno = ntcpip_htonl(pcb->rcv_nxt);

  /* advertise our receive window size in this TCP segment */
  seg->tcphdr->wnd = ntcpip_htons(pcb->rcv_ann_wnd);

  pcb->rcv_ann_right_edge = pcb->rcv_nxt + pcb->rcv_ann_wnd;

  /* Add any requested options.  NB MSS option is only set on SYN
     packets, so ignore it here */
  opts = (Uint32 *) ((void *) (seg->tcphdr + 1));
  if (seg->flags & TF_SEG_OPTS_MSS) {
    NTCPIP__TCP_BLD_MSS_OPT(*opts);
    opts += 1;
  }
#if LWIP_TCP_TIMESTAMPS
  pcb->ts_lastacksent = pcb->rcv_nxt;

  if (seg->flags & TF_SEG_OPTS_TS) {
    tcp_build_timestamp_option(pcb, opts);
    opts += 3;
  }
#endif

  /* If we don't have a local IP address, we get one by
     calling ntcpip__ipRoute(). */
  if (ntcpip_ipaddrIsAny(&(pcb->local_ip))) {
    netif = ntcpip__ipRoute(&(pcb->remote_ip));
    if (netif == NULL) {
      return;
    }
    ntcpip_ipaddrSet(&(pcb->local_ip), &(netif->ip_addr));
  }

  /* Set retransmission timer running if it is not currently enabled */
  if(pcb->rtime == -1)
    pcb->rtime = 0;

  if (pcb->rttest == 0) {
    pcb->rttest = ntcpip__tcpTicks;
    pcb->rtseq = ntcpip_ntohl(seg->tcphdr->seqno);

    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RTO_DEBUG, ("tcp_output_segment: rtseq %"U32_F"\n", pcb->rtseq));
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_OUTPUT_DEBUG, ("tcp_output_segment: %"U32_F":%"U32_F"\n",
          ntcpip_htonl(seg->tcphdr->seqno), ntcpip_htonl(seg->tcphdr->seqno) +
          seg->len));

  len = (Uint16)((Uint8 *)seg->tcphdr - (Uint8 *)seg->p->payload);

  seg->p->len -= len;
  seg->p->tot_len -= len;

  seg->p->payload = seg->tcphdr;

  seg->tcphdr->chksum = 0;
#if NTCPIP__CHECKSUM_GEN_TCP
  seg->tcphdr->chksum = ntcpip__inetChksumPseudo(seg->p,
             &(pcb->local_ip),
             &(pcb->remote_ip),
             IP_PROTO_TCP, seg->p->tot_len);
#endif
  TCP_STATS_INC(tcp.xmit);

#if NTCPIP__LWIP_NETIF_HWADDRHINT
  ip_output_hinted(seg->p, &(pcb->local_ip), &(pcb->remote_ip), pcb->ttl, pcb->tos,
      IP_PROTO_TCP, &(pcb->addr_hint));
#else /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  ntcpip__ipOutput(seg->p, &(pcb->local_ip), &(pcb->remote_ip), pcb->ttl, pcb->tos,
      IP_PROTO_TCP);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
}

/**
 * Send a TCP RESET packet (empty segment with RST flag set) either to
 * abort a connection or to show that there is no matching local connection
 * for a received segment.
 *
 * Called by ntcpip__tcpAbort() (to abort a local connection), ntcpip__tcpInput() (if no
 * matching local pcb was found), tcp_listen_input() (if incoming segment
 * has ACK flag set) and tcp_process() (received segment in the wrong state)
 *
 * Since a RST segment is in most cases not sent for an active connection,
 * ntcpip__tcpRst() has a number of arguments that are taken from a tcp_pcb for
 * most other segment output functions.
 *
 * @param seqno the sequence number to use for the outgoing segment
 * @param ackno the acknowledge number to use for the outgoing segment
 * @param local_ip the local IP address to send the segment from
 * @param remote_ip the remote IP address to send the segment to
 * @param local_port the local TCP port to send the segment from
 * @param remote_port the remote TCP port to send the segment to
 */
void
ntcpip__tcpRst(Uint32 seqno, Uint32 ackno,
  struct ntcpip_ipAddr *local_ip, struct ntcpip_ipAddr *remote_ip,
  Uint16 local_port, Uint16 remote_port)
{
  struct ntcpip_pbuf *p;
  struct ntcpip__tcpHdr *tcphdr;
  p = ntcpip_pbufAlloc(NTCPIP_PBUF_IP, NTCPIP__TCP_HLEN, NTCPIP_PBUF_RAM);
  if (p == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpRst: could not allocate memory for pbuf\n"));
      return;
  }
  NTCPIP__LWIP_ASSERT("check that first pbuf can hold struct ntcpip__tcpHdr",
              (p->len >= sizeof(struct ntcpip__tcpHdr)));

  tcphdr = p->payload;
  tcphdr->src = ntcpip_htons(local_port);
  tcphdr->dest = ntcpip_htons(remote_port);
  tcphdr->seqno = ntcpip_htonl(seqno);
  tcphdr->ackno = ntcpip_htonl(ackno);
  NTCPIP__TCPH_FLAGS_SET(tcphdr, NTCPIP__TCP_RST | NTCPIP__TCP_ACK);
  tcphdr->wnd = ntcpip_htons(NTCPIP__TCP_WND);
  tcphdr->urgp = 0;
  NTCPIP__TCPH_HDRLEN_SET(tcphdr, 5);

  tcphdr->chksum = 0;
#if NTCPIP__CHECKSUM_GEN_TCP
  tcphdr->chksum = ntcpip__inetChksumPseudo(p, local_ip, remote_ip,
              IP_PROTO_TCP, p->tot_len);
#endif
  TCP_STATS_INC(tcp.xmit);
  ntcpip__snmpIncTcpoutrsts();
   /* Send output with hardcoded TTL since we have no access to the pcb */
  ntcpip__ipOutput(p, local_ip, remote_ip, NTCPIP__TCP_TTL, 0, IP_PROTO_TCP);
  ntcpip_pbufFree(p);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RST_DEBUG, ("ntcpip__tcpRst: seqno %"U32_F" ackno %"U32_F".\n", seqno, ackno));
}

/**
 * Requeue all unacked segments for retransmission
 *
 * Called by ntcpip__tcpSlowTmr() for slow retransmission.
 *
 * @param pcb the tcp_pcb for which to re-enqueue all unacked segments
 */
void
ntcpip__tcpRexmitRto(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip__tcpSeg *seg;

  if (pcb->unacked == NULL) {
    return;
  }

  /* Move all unacked segments to the head of the unsent queue */
  for (seg = pcb->unacked; seg->next != NULL; seg = seg->next);
  /* concatenate unsent queue after unacked queue */
  seg->next = pcb->unsent;
  /* unsent queue is the concatenated queue (of unacked, unsent) */
  pcb->unsent = pcb->unacked;
  /* unacked queue is now empty */
  pcb->unacked = NULL;

  /* increment number of retransmissions */
  ++pcb->nrtx;

  /* Don't take any RTT measurements after retransmitting. */
  pcb->rttest = 0;

  /* Do the actual retransmission */
  ntcpip__tcpOutput(pcb);
}

/**
 * Requeue the first unacked segment for retransmission
 *
 * Called by tcp_receive() for fast retramsmit.
 *
 * @param pcb the tcp_pcb for which to retransmit the first unacked segment
 */
void
ntcpip__tcpRexmit(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip__tcpSeg *seg;
  struct ntcpip__tcpSeg **cur_seg;

  if (pcb->unacked == NULL) {
    return;
  }

  /* Move the first unacked segment to the unsent queue */
  /* Keep the unsent queue sorted. */
  seg = pcb->unacked;
  pcb->unacked = seg->next;

  cur_seg = &(pcb->unsent);
  while (*cur_seg &&
    NTCPIP__TCP_SEQ_LT(ntcpip_ntohl((*cur_seg)->tcphdr->seqno), ntcpip_ntohl(seg->tcphdr->seqno))) {
      cur_seg = &((*cur_seg)->next );
  }
  seg->next = *cur_seg;
  *cur_seg = seg;

  ++pcb->nrtx;

  /* Don't take any rtt measurements after retransmitting. */
  pcb->rttest = 0;

  /* Do the actual retransmission. */
  ntcpip__snmpIncTcpretranssegs();
  /* No need to call ntcpip__tcpOutput: we are always called from ntcpip__tcpInput()
     and thus ntcpip__tcpOutput directly returns. */
}


/**
 * Handle retransmission after three dupacks received
 *
 * @param pcb the tcp_pcb for which to retransmit the first unacked segment
 */
void 
ntcpip__tcpRexmitFast(struct ntcpip__tcpPcb *pcb)
{
  if (pcb->unacked != NULL && !(pcb->flags & NTCPIP_TF_INFR)) {
    /* This is fast retransmit. Retransmit the first unacked segment. */
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_FR_DEBUG, 
                ("tcp_receive: dupacks %"U16_F" (%"U32_F
                 "), fast retransmit %"U32_F"\n",
                 (Uint16)pcb->dupacks, pcb->lastack,
                 ntcpip_ntohl(pcb->unacked->tcphdr->seqno)));
    ntcpip__tcpRexmit(pcb);

    /* Set ssthresh to half of the minimum of the current
     * cwnd and the advertised window */
    if (pcb->cwnd > pcb->snd_wnd)
      pcb->ssthresh = pcb->snd_wnd / 2;
    else
      pcb->ssthresh = pcb->cwnd / 2;
    
    /* The minimum value for ssthresh should be 2 MSS */
    if (pcb->ssthresh < 2*pcb->mss) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_FR_DEBUG, 
                  ("tcp_receive: The minimum value for ssthresh %"U16_F
                   " should be min 2 mss %"U16_F"...\n",
                   pcb->ssthresh, 2*pcb->mss));
      pcb->ssthresh = 2*pcb->mss;
    }
    
    pcb->cwnd = pcb->ssthresh + 3 * pcb->mss;
    pcb->flags |= NTCPIP_TF_INFR;
  } 
}


/**
 * Send keepalive packets to keep a connection active although
 * no data is sent over it.
 *
 * Called by ntcpip__tcpSlowTmr()
 *
 * @param pcb the tcp_pcb for which to send a keepalive packet
 */
void
ntcpip__tcpKeepAlive(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip_pbuf *p;
  struct ntcpip__tcpHdr *tcphdr;

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpKeepAlive: sending KEEPALIVE probe to %"U16_F".%"U16_F".%"U16_F".%"U16_F"\n",
                          ntcpip_ipaddr1(&pcb->remote_ip), ntcpip_ipaddr2(&pcb->remote_ip),
                          ntcpip_ipaddr3(&pcb->remote_ip), ntcpip_ipaddr4(&pcb->remote_ip)));

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpKeepAlive: ntcpip__tcpTicks %"U32_F"   pcb->tmr %"U32_F" pcb->keep_cnt_sent %"U16_F"\n", 
                          ntcpip__tcpTicks, pcb->tmr, pcb->keep_cnt_sent));
   
  p = ntcpip_pbufAlloc(NTCPIP_PBUF_IP, NTCPIP__TCP_HLEN, NTCPIP_PBUF_RAM);
   
  if(p == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, 
                ("ntcpip__tcpKeepAlive: could not allocate memory for pbuf\n"));
    return;
  }
  NTCPIP__LWIP_ASSERT("check that first pbuf can hold struct ntcpip__tcpHdr",
              (p->len >= sizeof(struct ntcpip__tcpHdr)));

  tcphdr = tcp_output_set_header(pcb, p, 0, ntcpip_htonl(pcb->snd_nxt - 1));

#if NTCPIP__CHECKSUM_GEN_TCP
  tcphdr->chksum = ntcpip__inetChksumPseudo(p, &pcb->local_ip, &pcb->remote_ip,
                                      IP_PROTO_TCP, p->tot_len);
#endif
  TCP_STATS_INC(tcp.xmit);

  /* Send output to IP */
#if NTCPIP__LWIP_NETIF_HWADDRHINT
  ip_output_hinted(p, &pcb->local_ip, &pcb->remote_ip, pcb->ttl, 0, IP_PROTO_TCP,
    &(pcb->addr_hint));
#else /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  ntcpip__ipOutput(p, &pcb->local_ip, &pcb->remote_ip, pcb->ttl, 0, IP_PROTO_TCP);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/

  ntcpip_pbufFree(p);

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpKeepAlive: seqno %"U32_F" ackno %"U32_F".\n",
                          pcb->snd_nxt - 1, pcb->rcv_nxt));
}


/**
 * Send persist timer zero-window probes to keep a connection active
 * when a window update is lost.
 *
 * Called by ntcpip__tcpSlowTmr()
 *
 * @param pcb the tcp_pcb for which to send a zero-window probe packet
 */
void
ntcpip__tcpZeroWindowProbe(struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip_pbuf *p;
  struct ntcpip__tcpHdr *tcphdr;
  struct ntcpip__tcpSeg *seg;
  Uint16 len;
  Uint8 is_fin;

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, 
              ("ntcpip__tcpZeroWindowProbe: sending ZERO WINDOW probe to %"
               U16_F".%"U16_F".%"U16_F".%"U16_F"\n",
               ntcpip_ipaddr1(&pcb->remote_ip), ntcpip_ipaddr2(&pcb->remote_ip),
               ntcpip_ipaddr3(&pcb->remote_ip), ntcpip_ipaddr4(&pcb->remote_ip)));

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, 
              ("ntcpip__tcpZeroWindowProbe: ntcpip__tcpTicks %"U32_F
               "   pcb->tmr %"U32_F" pcb->keep_cnt_sent %"U16_F"\n", 
               ntcpip__tcpTicks, pcb->tmr, pcb->keep_cnt_sent));

  seg = pcb->unacked;

  if(seg == NULL)
    seg = pcb->unsent;

  if(seg == NULL)
    return;

  is_fin = ((NTCPIP__TCPH_FLAGS(seg->tcphdr) & NTCPIP__TCP_FIN) != 0) && (seg->len == 0);
  len = is_fin ? NTCPIP__TCP_HLEN : NTCPIP__TCP_HLEN + 1;

  p = ntcpip_pbufAlloc(NTCPIP_PBUF_IP, len, NTCPIP_PBUF_RAM);
  if(p == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpZeroWindowProbe: no memory for pbuf\n"));
    return;
  }
  NTCPIP__LWIP_ASSERT("check that first pbuf can hold struct ntcpip__tcpHdr",
              (p->len >= sizeof(struct ntcpip__tcpHdr)));

  tcphdr = tcp_output_set_header(pcb, p, 0, seg->tcphdr->seqno);

  if (is_fin) {
    /* FIN segment, no data */
    NTCPIP__TCPH_FLAGS_SET(tcphdr, NTCPIP__TCP_ACK | NTCPIP__TCP_FIN);
  } else {
    /* Data segment, copy in one byte from the head of the unacked queue */
    *((char *)p->payload + sizeof(struct ntcpip__tcpHdr)) = *(char *)seg->dataptr;
  }

#if NTCPIP__CHECKSUM_GEN_TCP
  tcphdr->chksum = ntcpip__inetChksumPseudo(p, &pcb->local_ip, &pcb->remote_ip,
                                      IP_PROTO_TCP, p->tot_len);
#endif
  TCP_STATS_INC(tcp.xmit);

  /* Send output to IP */
#if NTCPIP__LWIP_NETIF_HWADDRHINT
  ip_output_hinted(p, &pcb->local_ip, &pcb->remote_ip, pcb->ttl, 0, IP_PROTO_TCP,
    &(pcb->addr_hint));
#else /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
  ntcpip__ipOutput(p, &pcb->local_ip, &pcb->remote_ip, pcb->ttl, 0, IP_PROTO_TCP);
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/

  ntcpip_pbufFree(p);

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpZeroWindowProbe: seqno %"U32_F
                          " ackno %"U32_F".\n",
                          pcb->snd_nxt - 1, pcb->rcv_nxt));
}
#endif /* NTCPIP__LWIP_TCP */


