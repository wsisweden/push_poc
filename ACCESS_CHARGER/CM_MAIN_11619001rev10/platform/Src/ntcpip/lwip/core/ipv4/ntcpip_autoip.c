/**
 * @file
 * AutoIP Automatic LinkLocal IP Configuration
 *
 */

/*
 *
 * Copyright (c) 2007 Dominik Spies <kontakt@dspies.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Dominik Spies <kontakt@dspies.de>
 *
 * This is a AutoIP implementation for the lwIP TCP/IP stack. It aims to conform
 * with RFC 3927.
 *
 *
 * Please coordinate changes and requests with Dominik Spies
 * <kontakt@dspies.de>
 */

/*******************************************************************************
 * USAGE:
 * 
 * define NTCPIP__LWIP_AUTOIP 1  in your lwipopts.h
 * 
 * If you don't use tcpip.c (so, don't call, you don't call ntcpip__tcpipInit):
 * - First, call ntcpip__autoipInit().
 * - call ntcpip__autoipTmr() all AUTOIP_TMR_INTERVAL msces,
 *   that should be defined in autoip.h.
 *   I recommend a value of 100. The value must divide 1000 with a remainder almost 0.
 *   Possible values are 1000, 500, 333, 250, 200, 166, 142, 125, 111, 100 ....
 *
 * Without DHCP:
 * - Call ntcpip__autoipStart() after ntcpip__netifAdd().
 * 
 * With DHCP:
 * - define NTCPIP__LWIP_DHCP_AUTOIP_COOP 1 in your lwipopts.h.
 * - Configure your DHCP Client.
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_AUTOIP /* don't build if not configured for use in lwipopts.h */

#include "../../mem.h"
#include "ntcpip/udp.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "../../autoip.h"
#include "../../../netif/etharp.h"

#include <stdlib.h>
#include <string.h>

/* 169.254.0.0 */
#define AUTOIP_NET         0xA9FE0000
/* 169.254.1.0 */
#define AUTOIP_RANGE_START (AUTOIP_NET | 0x0100)
/* 169.254.254.255 */
#define AUTOIP_RANGE_END   (AUTOIP_NET | 0xFEFF)


/** Pseudo random macro based on netif informations.
 * You could use "rand()" from the C Library if you define LWIP_AUTOIP_RAND in lwipopts.h */
#ifndef LWIP_AUTOIP_RAND
#define LWIP_AUTOIP_RAND(netif) ( (((Uint32)((netif->hwaddr[5]) & 0xff) << 24) | \
                                   ((Uint32)((netif->hwaddr[3]) & 0xff) << 16) | \
                                   ((Uint32)((netif->hwaddr[2]) & 0xff) << 8) | \
                                   ((Uint32)((netif->hwaddr[4]) & 0xff))) + \
                                   (netif->autoip?netif->autoip->tried_llipaddr:0))
#endif /* LWIP_AUTOIP_RAND */

/**
 * Macro that generates the initial IP address to be tried by AUTOIP.
 * If you want to override this, define it to something else in lwipopts.h.
 */
#ifndef LWIP_AUTOIP_CREATE_SEED_ADDR
#define LWIP_AUTOIP_CREATE_SEED_ADDR(netif) \
  ntcpip_htonl(AUTOIP_RANGE_START + ((Uint32)(((Uint8)(netif->hwaddr[4])) | \
                 ((Uint32)((Uint8)(netif->hwaddr[5]))) << 8)))
#endif /* LWIP_AUTOIP_CREATE_SEED_ADDR */

/* static functions */
static void autoip_handle_arp_conflict(struct ntcpip__netif *netif);

/* creates a pseudo random LL IP-Address for a network interface */
static void autoip_create_addr(struct ntcpip__netif *netif, struct ntcpip_ipAddr *ipaddr);

/* sends an ARP probe */
static ntcpip_Err autoip_arp_probe(struct ntcpip__netif *netif);

/* sends an ARP announce */
static ntcpip_Err autoip_arp_announce(struct ntcpip__netif *netif);

/* configure interface for use with current LL IP-Address */
static ntcpip_Err autoip_bind(struct ntcpip__netif *netif);

/* start sending probes for llipaddr */
static void autoip_start_probing(struct ntcpip__netif *netif);

/**
 * Initialize this module
 */
void
ntcpip__autoipInit(void)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__autoipInit()\n"));
}

/**
 * Handle a IP address conflict after an ARP conflict detection
 */
static void
autoip_handle_arp_conflict(struct ntcpip__netif *netif)
{
  /* Somehow detect if we are defending or retreating */
  unsigned char defend = 1; /* tbd */

  if(defend) {
    if(netif->autoip->lastconflict > 0) {
      /* retreat, there was a conflicting ARP in the last
       * DEFEND_INTERVAL seconds
       */
      NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE,
        ("autoip_handle_arp_conflict(): we are defending, but in DEFEND_INTERVAL, retreating\n"));

      /* TODO: close all TCP sessions */
      ntcpip__autoipStart(netif);
    } else {
      NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE,
        ("autoip_handle_arp_conflict(): we are defend, send ARP Announce\n"));
      autoip_arp_announce(netif);
      netif->autoip->lastconflict = DEFEND_INTERVAL * AUTOIP_TICKS_PER_SECOND;
    }
  } else {
    NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE,
      ("autoip_handle_arp_conflict(): we do not defend, retreating\n"));
    /* TODO: close all TCP sessions */
    ntcpip__autoipStart(netif);
  }
}

/**
 * Create an IP-Address out of range 169.254.1.0 to 169.254.254.255
 *
 * @param netif network interface on which create the IP-Address
 * @param ipaddr ip address to initialize
 */
static void
autoip_create_addr(struct ntcpip__netif *netif, struct ntcpip_ipAddr *ipaddr)
{
  /* Here we create an IP-Address out of range 169.254.1.0 to 169.254.254.255
   * compliant to RFC 3927 Section 2.1
   * We have 254 * 256 possibilities */

  Uint32 addr = ntcpip_ntohl(LWIP_AUTOIP_CREATE_SEED_ADDR(netif));
  addr += netif->autoip->tried_llipaddr;
  addr = AUTOIP_NET | (addr & 0xffff);
  /* Now, 169.254.0.0 <= addr <= 169.254.255.255 */ 

  if (addr < AUTOIP_RANGE_START) {
    addr += AUTOIP_RANGE_END - AUTOIP_RANGE_START + 1;
  }
  if (addr > AUTOIP_RANGE_END) {
    addr -= AUTOIP_RANGE_END - AUTOIP_RANGE_START + 1;
  }
  NTCPIP__LWIP_ASSERT("AUTOIP address not in range", (addr >= AUTOIP_RANGE_START) &&
    (addr <= AUTOIP_RANGE_END));
  ipaddr->addr = ntcpip_htonl(addr);
  
  NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE,
    ("autoip_create_addr(): tried_llipaddr=%"U16_F", 0x%08"X32_F"\n",
    (Uint16)(netif->autoip->tried_llipaddr), (Uint32)(ipaddr->addr)));
}

/**
 * Sends an ARP probe from a network interface
 *
 * @param netif network interface used to send the probe
 */
static ntcpip_Err
autoip_arp_probe(struct ntcpip__netif *netif)
{
  return ntcpip__etharpRaw(netif, (struct eth_addr *)netif->hwaddr, &ntcpip__etharpBroadcast,
    (struct eth_addr *)netif->hwaddr, NTCPIP_IP_ADDR_ANY, &ntcpip__etharpZero,
    &netif->autoip->llipaddr, ARP_REQUEST);
}

/**
 * Sends an ARP announce from a network interface
 *
 * @param netif network interface used to send the announce
 */
static ntcpip_Err
autoip_arp_announce(struct ntcpip__netif *netif)
{
  return ntcpip__etharpRaw(netif, (struct eth_addr *)netif->hwaddr, &ntcpip__etharpBroadcast,
    (struct eth_addr *)netif->hwaddr, &netif->autoip->llipaddr, &ntcpip__etharpZero,
    &netif->autoip->llipaddr, ARP_REQUEST);
}

/**
 * Configure interface for use with current LL IP-Address
 *
 * @param netif network interface to configure with current LL IP-Address
 */
static ntcpip_Err
autoip_bind(struct ntcpip__netif *netif)
{
  struct autoip *autoip = netif->autoip;
  struct ntcpip_ipAddr sn_mask, gw_addr;

  NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE,
    ("autoip_bind(netif=%p) %c%c%"U16_F" 0x%08"X32_F"\n",
    (void*)netif, netif->name[0], netif->name[1], (Uint16)netif->num, autoip->llipaddr.addr));

  NTCPIP_IP4_ADDR(&sn_mask, 255, 255, 0, 0);
  NTCPIP_IP4_ADDR(&gw_addr, 0, 0, 0, 0);

  ntcpip__netifSetIpaddr(netif, &autoip->llipaddr);
  ntcpip__netifSetNetmask(netif, &sn_mask);
  ntcpip__netifSetGw(netif, &gw_addr);  

  /* bring the interface up */
  ntcpip__netifSetUp(netif);

  return NTCPIP_ERR_OK;
}

/**
 * Start AutoIP client
 *
 * @param netif network interface on which start the AutoIP client
 */
ntcpip_Err
ntcpip__autoipStart(struct ntcpip__netif *netif)
{
  struct autoip *autoip = netif->autoip;
  ntcpip_Err result = NTCPIP_ERR_OK;

  if(ntcpip__netifIsUp(netif)) {
    ntcpip__netifSetDown(netif);
  }

  /* Set IP-Address, Netmask and Gateway to 0 to make sure that
   * ARP Packets are formed correctly
   */
  netif->ip_addr.addr = 0;
  netif->netmask.addr = 0;
  netif->gw.addr      = 0;

  NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE,
    ("ntcpip__autoipStart(netif=%p) %c%c%"U16_F"\n", (void*)netif, netif->name[0],
    netif->name[1], (Uint16)netif->num));
  if(autoip == NULL) {
    /* no AutoIP client attached yet? */
    NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE,
      ("ntcpip__autoipStart(): starting new AUTOIP client\n"));
    autoip = ntcpip__memMalloc(sizeof(struct autoip));
    if(autoip == NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE,
        ("ntcpip__autoipStart(): could not allocate autoip\n"));
      return NTCPIP_ERR_MEM;
    }
    memset( autoip, 0, sizeof(struct autoip));
    /* store this AutoIP client in the netif */
    netif->autoip = autoip;
    NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__autoipStart(): allocated autoip"));
  } else {
    autoip->state = AUTOIP_STATE_OFF;
    autoip->ttw = 0;
    autoip->sent_num = 0;
    memset(&autoip->llipaddr, 0, sizeof(struct ntcpip_ipAddr));
    autoip->lastconflict = 0;
  }

  autoip_create_addr(netif, &(autoip->llipaddr));
  autoip->tried_llipaddr++;
  autoip_start_probing(netif);

  return result;
}

static void
autoip_start_probing(struct ntcpip__netif *netif)
{
  struct autoip *autoip = netif->autoip;

  autoip->state = AUTOIP_STATE_PROBING;
  autoip->sent_num = 0;

  /* time to wait to first probe, this is randomly
   * choosen out of 0 to PROBE_WAIT seconds.
   * compliant to RFC 3927 Section 2.2.1
   */
  autoip->ttw = (Uint16)(LWIP_AUTOIP_RAND(netif) % (PROBE_WAIT * AUTOIP_TICKS_PER_SECOND));

  /*
   * if we tried more then MAX_CONFLICTS we must limit our rate for
   * accquiring and probing address
   * compliant to RFC 3927 Section 2.2.1
   */
  if(autoip->tried_llipaddr > MAX_CONFLICTS) {
    autoip->ttw = RATE_LIMIT_INTERVAL * AUTOIP_TICKS_PER_SECOND;
  }
}

/**
 * Handle a possible change in the network configuration.
 *
 * If there is an AutoIP address configured, take the interface down
 * and begin probing with the same address.
 */
void
ntcpip__autoipNetworkChanged(struct ntcpip__netif *netif)
{
  if (netif->autoip && netif->autoip->state != AUTOIP_STATE_OFF) {
    ntcpip__netifSetDown(netif);
    autoip_start_probing(netif);
  }
}

/**
 * Stop AutoIP client
 *
 * @param netif network interface on which stop the AutoIP client
 */
ntcpip_Err
ntcpip__autoipStop(struct ntcpip__netif *netif)
{
  netif->autoip->state = AUTOIP_STATE_OFF;
  ntcpip__netifSetDown(netif);
  return NTCPIP_ERR_OK;
}

/**
 * Has to be called in loop every AUTOIP_TMR_INTERVAL milliseconds
 */
void
ntcpip__autoipTmr()
{
  struct ntcpip__netif *netif = ntcpip__netifList;
  /* loop through netif's */
  while (netif != NULL) {
    /* only act on AutoIP configured interfaces */
    if (netif->autoip != NULL) {
      if(netif->autoip->lastconflict > 0) {
        netif->autoip->lastconflict--;
      }

      NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE,
        ("ntcpip__autoipTmr() AutoIP-State: %"U16_F", ttw=%"U16_F"\n",
        (Uint16)(netif->autoip->state), netif->autoip->ttw));

      switch(netif->autoip->state) {
        case AUTOIP_STATE_PROBING:
          if(netif->autoip->ttw > 0) {
            netif->autoip->ttw--;
          } else {
            if(netif->autoip->sent_num >= PROBE_NUM) {
              netif->autoip->state = AUTOIP_STATE_ANNOUNCING;
              netif->autoip->sent_num = 0;
              netif->autoip->ttw = ANNOUNCE_WAIT * AUTOIP_TICKS_PER_SECOND;
            } else {
              autoip_arp_probe(netif);
              NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE,
                ("ntcpip__autoipTmr() PROBING Sent Probe\n"));
              netif->autoip->sent_num++;
              /* calculate time to wait to next probe */
              netif->autoip->ttw = (Uint16)((LWIP_AUTOIP_RAND(netif) %
                ((PROBE_MAX - PROBE_MIN) * AUTOIP_TICKS_PER_SECOND) ) +
                PROBE_MIN * AUTOIP_TICKS_PER_SECOND);
            }
          }
          break;

        case AUTOIP_STATE_ANNOUNCING:
          if(netif->autoip->ttw > 0) {
            netif->autoip->ttw--;
          } else {
            if(netif->autoip->sent_num == 0) {
             /* We are here the first time, so we waited ANNOUNCE_WAIT seconds
              * Now we can bind to an IP address and use it.
              *
              * autoip_bind calls ntcpip__netifSetUp. This triggers a gratuitous ARP
              * which counts as an announcement.
              */
              autoip_bind(netif);
            } else {
              autoip_arp_announce(netif);
              NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE,
                ("ntcpip__autoipTmr() ANNOUNCING Sent Announce\n"));
            }
            netif->autoip->ttw = ANNOUNCE_INTERVAL * AUTOIP_TICKS_PER_SECOND;
            netif->autoip->sent_num++;

            if(netif->autoip->sent_num >= ANNOUNCE_NUM) {
                netif->autoip->state = AUTOIP_STATE_BOUND;
                netif->autoip->sent_num = 0;
                netif->autoip->ttw = 0;
            }
          }
          break;
      }
    }
    /* proceed to next network interface */
    netif = netif->next;
  }
}

/**
 * Handles every incoming ARP Packet, called by ntcpip__etharpArpInput.
 *
 * @param netif network interface to use for autoip processing
 * @param hdr Incoming ARP packet
 */
void
ntcpip__autoipArpReply(struct ntcpip__netif *netif, struct etharp_hdr *hdr)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__autoipArpReply()\n"));
  if ((netif->autoip != NULL) && (netif->autoip->state != AUTOIP_STATE_OFF)) {
   /* when ip.src == llipaddr && hw.src != netif->hwaddr
    *
    * when probing  ip.dst == llipaddr && hw.src != netif->hwaddr
    * we have a conflict and must solve it
    */
    struct ntcpip_ipAddr sipaddr, dipaddr;
    struct eth_addr netifaddr;
    netifaddr.addr[0] = netif->hwaddr[0];
    netifaddr.addr[1] = netif->hwaddr[1];
    netifaddr.addr[2] = netif->hwaddr[2];
    netifaddr.addr[3] = netif->hwaddr[3];
    netifaddr.addr[4] = netif->hwaddr[4];
    netifaddr.addr[5] = netif->hwaddr[5];

    /* Copy struct ntcpip__ipAddr2 to aligned ip_addr, to support compilers without
     * structure packing (not using structure copy which breaks strict-aliasing rules).
     */
    NTCPIP__SMEMCPY(&sipaddr, &hdr->sipaddr, sizeof(sipaddr));
    NTCPIP__SMEMCPY(&dipaddr, &hdr->dipaddr, sizeof(dipaddr));
      
    if ((netif->autoip->state == AUTOIP_STATE_PROBING) ||
        ((netif->autoip->state == AUTOIP_STATE_ANNOUNCING) &&
         (netif->autoip->sent_num == 0))) {
     /* RFC 3927 Section 2.2.1:
      * from beginning to after ANNOUNCE_WAIT
      * seconds we have a conflict if
      * ip.src == llipaddr OR
      * ip.dst == llipaddr && hw.src != own hwaddr
      */
      if ((ntcpip_ipaddrCmp(&sipaddr, &netif->autoip->llipaddr)) ||
          (ntcpip_ipaddrCmp(&dipaddr, &netif->autoip->llipaddr) &&
           !eth_addr_cmp(&netifaddr, &hdr->shwaddr))) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE | NTCPIP__LWIP_DBG_LEVEL_WARNING,
          ("ntcpip__autoipArpReply(): Probe Conflict detected\n"));
        ntcpip__autoipStart(netif);
      }
    } else {
     /* RFC 3927 Section 2.5:
      * in any state we have a conflict if
      * ip.src == llipaddr && hw.src != own hwaddr
      */
      if (ntcpip_ipaddrCmp(&sipaddr, &netif->autoip->llipaddr) &&
          !eth_addr_cmp(&netifaddr, &hdr->shwaddr)) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__AUTOIP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE | NTCPIP__LWIP_DBG_LEVEL_WARNING,
          ("ntcpip__autoipArpReply(): Conflicting ARP-Packet detected\n"));
        autoip_handle_arp_conflict(netif);
      }
    }
  }
}

#endif /* NTCPIP__LWIP_AUTOIP */

