/**
 * @file
 * ICMP - Internet Control Message Protocol
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

/* Some ICMP messages should be passed to the transport protocols. This
   is not implemented. */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_ICMP /* don't build if not configured for use in lwipopts.h */

#include "../../icmp.h"
#include "ntcpip/inet.h"
#include "../../inet_chksum.h"
#include "ntcpip/ip.h"
#include "../../def.h"
#include "../../stats.h"
#include "ntcpip/snmp.h"

#include <string.h>

/** Small optimization: set to 0 if incoming NTCPIP_PBUF_POOL pbuf always can be
 * used to modify and send a response packet (and to 1 if this is not the case,
 * e.g. when link header is stripped of when receiving) */
#ifndef LWIP_ICMP_ECHO_CHECK_INPUT_PBUF_LEN
#define LWIP_ICMP_ECHO_CHECK_INPUT_PBUF_LEN 1
#endif /* LWIP_ICMP_ECHO_CHECK_INPUT_PBUF_LEN */

/* The amount of data from the original packet to return in a dest-unreachable */
#define ICMP_DEST_UNREACH_DATASIZE 8

static void icmp_send_response(struct ntcpip_pbuf *p, Uint8 type, Uint8 code);

/**
 * Processes ICMP input packets, called from ntcpip__ipInput().
 *
 * Currently only processes icmp echo requests and sends
 * out the echo response.
 *
 * @param p the icmp echo request packet, p->payload pointing to the ip header
 * @param inp the netif on which this packet was received
 */
void
ntcpip__icmpInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp)
{
  Uint8 type;
#ifdef LWIP_DEBUG
  Uint8 code;
#endif /* LWIP_DEBUG */
  struct icmp_echo_hdr *iecho;
  struct ip_hdr *iphdr;
  struct ntcpip_ipAddr tmpaddr;
  Int16 hlen;

  ICMP_STATS_INC(icmp.recv);
  ntcpip__snmpIncIcmpinmsgs();


  iphdr = p->payload;
  hlen = IPH_HL(iphdr) * 4;
  if (ntcpip__pbufHeader(p, -hlen) || (p->tot_len < sizeof(Uint16)*2)) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: short ICMP (%"U16_F" bytes) received\n", p->tot_len));
    goto lenerr;
  }

  type = *((Uint8 *)p->payload);
#ifdef LWIP_DEBUG
  code = *(((Uint8 *)p->payload)+1);
#endif /* LWIP_DEBUG */
  switch (type) {
  case ICMP_ECHO:
#if !LWIP_MULTICAST_PING || !LWIP_BROADCAST_PING
    {
      int accepted = 1;
#if !LWIP_MULTICAST_PING
      /* multicast destination address? */
      if (ntcpip_ipaddrIsMulticast(&iphdr->dest)) {
        accepted = 0;
      }
#endif /* LWIP_MULTICAST_PING */
#if !LWIP_BROADCAST_PING
      /* broadcast destination address? */
      if (ntcpip__ipaddrIsBroadcast(&iphdr->dest, inp)) {
        accepted = 0;
      }
#endif /* LWIP_BROADCAST_PING */
      /* broadcast or multicast destination address not acceptd? */
      if (!accepted) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: Not echoing to multicast or broadcast pings\n"));
        ICMP_STATS_INC(icmp.err);
        ntcpip_pbufFree(p);
        return;
      }
    }
#endif /* !LWIP_MULTICAST_PING || !LWIP_BROADCAST_PING */
    NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: ping\n"));
    if (p->tot_len < sizeof(struct icmp_echo_hdr)) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: bad ICMP echo received\n"));
      goto lenerr;
    }
    if (ntcpip__inetChksumPbuf(p) != 0) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: checksum failed for received ICMP echo\n"));
      ntcpip_pbufFree(p);
      ICMP_STATS_INC(icmp.chkerr);
      ntcpip__snmpIncIcmpinerrors();
      return;
    }
#if LWIP_ICMP_ECHO_CHECK_INPUT_PBUF_LEN
    if (ntcpip__pbufHeader(p, (NTCPIP__PBUF_IP_HLEN + NTCPIP__PBUF_LINK_HLEN))) {
      /* p is not big enough to contain link headers
       * allocate a new one and copy p into it
       */
      struct ntcpip_pbuf *r;
      /* switch p->payload to ip header */
      if (ntcpip__pbufHeader(p, hlen)) {
        NTCPIP__LWIP_ASSERT("ntcpip__icmpInput: moving p->payload to ip header failed\n", 0);
        goto memerr;
      }
      /* allocate new packet buffer with space for link headers */
      r = ntcpip_pbufAlloc(NTCPIP_PBUF_LINK, p->tot_len, NTCPIP_PBUF_RAM);
      if (r == NULL) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: allocating new pbuf failed\n"));
        goto memerr;
      }
      NTCPIP__LWIP_ASSERT("check that first pbuf can hold struct the ICMP header",
                  (r->len >= hlen + sizeof(struct icmp_echo_hdr)));
      /* copy the whole packet including ip header */
      if (ntcpip_pbufCopy(r, p) != NTCPIP_ERR_OK) {
        NTCPIP__LWIP_ASSERT("ntcpip__icmpInput: copying to new pbuf failed\n", 0);
        goto memerr;
      }
      iphdr = r->payload;
      /* switch r->payload back to icmp header */
      if (ntcpip__pbufHeader(r, -hlen)) {
        NTCPIP__LWIP_ASSERT("ntcpip__icmpInput: restoring original p->payload failed\n", 0);
        goto memerr;
      }
      /* free the original p */
      ntcpip_pbufFree(p);
      /* we now have an identical copy of p that has room for link headers */
      p = r;
    } else {
      /* restore p->payload to point to icmp header */
      if (ntcpip__pbufHeader(p, -(Int16)(NTCPIP__PBUF_IP_HLEN + NTCPIP__PBUF_LINK_HLEN))) {
        NTCPIP__LWIP_ASSERT("ntcpip__icmpInput: restoring original p->payload failed\n", 0);
        goto memerr;
      }
    }
#endif /* LWIP_ICMP_ECHO_CHECK_INPUT_PBUF_LEN */
    /* At this point, all checks are OK. */
    /* We generate an answer by switching the dest and src ip addresses,
     * setting the icmp type to ECHO_RESPONSE and updating the checksum. */
    iecho = p->payload;
    tmpaddr.addr = iphdr->src.addr;
    iphdr->src.addr = iphdr->dest.addr;
    iphdr->dest.addr = tmpaddr.addr;
    ICMPH_TYPE_SET(iecho, ICMP_ER);
    /* adjust the checksum */
    if (iecho->chksum >= ntcpip_htons(0xffff - (ICMP_ECHO << 8))) {
      iecho->chksum += ntcpip_htons(ICMP_ECHO << 8) + 1;
    } else {
      iecho->chksum += ntcpip_htons(ICMP_ECHO << 8);
    }

    /* Set the correct TTL and recalculate the header checksum. */
    IPH_TTL_SET(iphdr, NTCPIP__ICMP_TTL);
    IPH_CHKSUM_SET(iphdr, 0);
#if NTCPIP__CHECKSUM_GEN_IP
    IPH_CHKSUM_SET(iphdr, ntcpip__inetChksum(iphdr, NTCPIP__IP_HLEN));
#endif /* NTCPIP__CHECKSUM_GEN_IP */

    ICMP_STATS_INC(icmp.xmit);
    /* increase number of messages attempted to send */
    ntcpip__snmpIncIcmpoutmsgs();
    /* increase number of echo replies attempted to send */
    ntcpip__snmpIncIcmpoutechoreps();

    if(ntcpip__pbufHeader(p, hlen)) {
      NTCPIP__LWIP_ASSERT("Can't move over header in packet", 0);
    } else {
      ntcpip_Err ret;
      ret = ntcpip__ipOutputIf(p, &(iphdr->src), IP_HDRINCL,
                   NTCPIP__ICMP_TTL, 0, IP_PROTO_ICMP, inp);
      if (ret != NTCPIP_ERR_OK) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: ntcpip__ipOutputIf returned an error: %c.\n", ret));
      }
    }
    break;
  default:
    NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpInput: ICMP type %"S16_F" code %"S16_F" not supported.\n", 
                (Int16)type, (Int16)code));
    ICMP_STATS_INC(icmp.proterr);
    ICMP_STATS_INC(icmp.drop);
  }
  ntcpip_pbufFree(p);
  return;
lenerr:
  ntcpip_pbufFree(p);
  ICMP_STATS_INC(icmp.lenerr);
  ntcpip__snmpIncIcmpinerrors();
  return;
#if LWIP_ICMP_ECHO_CHECK_INPUT_PBUF_LEN
memerr:
  ntcpip_pbufFree(p);
  ICMP_STATS_INC(icmp.err);
  ntcpip__snmpIncIcmpinerrors();
  return;
#endif /* LWIP_ICMP_ECHO_CHECK_INPUT_PBUF_LEN */
}

/**
 * Send an icmp 'destination unreachable' packet, called from ntcpip__ipInput() if
 * the transport layer protocol is unknown and from ntcpip__udpInput() if the local
 * port is not bound.
 *
 * @param p the input packet for which the 'unreachable' should be sent,
 *          p->payload pointing to the IP header
 * @param t type of the 'unreachable' packet
 */
void
ntcpip__icmpDestUnreach(struct ntcpip_pbuf *p, enum icmp_dur_type t)
{
  icmp_send_response(p, ICMP_DUR, t);
}

#if NTCPIP__IP_FORWARD || NTCPIP__IP_REASSEMBLY
/**
 * Send a 'time exceeded' packet, called from ip_forward() if TTL is 0.
 *
 * @param p the input packet for which the 'time exceeded' should be sent,
 *          p->payload pointing to the IP header
 * @param t type of the 'time exceeded' packet
 */
void
ntcpip__icmpTimeExceeded(struct ntcpip_pbuf *p, enum icmp_te_type t)
{
  icmp_send_response(p, ICMP_TE, t);
}

#endif /* NTCPIP__IP_FORWARD || NTCPIP__IP_REASSEMBLY */

/**
 * Send an icmp packet in response to an incoming packet.
 *
 * @param p the input packet for which the 'unreachable' should be sent,
 *          p->payload pointing to the IP header
 * @param type Type of the ICMP header
 * @param code Code of the ICMP header
 */
static void
icmp_send_response(struct ntcpip_pbuf *p, Uint8 type, Uint8 code)
{
  struct ntcpip_pbuf *q;
  struct ip_hdr *iphdr;
  /* we can use the echo header here */
  struct icmp_echo_hdr *icmphdr;

  /* ICMP header + IP header + 8 bytes of data */
  q = ntcpip_pbufAlloc(NTCPIP_PBUF_IP, sizeof(struct icmp_echo_hdr) + NTCPIP__IP_HLEN + ICMP_DEST_UNREACH_DATASIZE,
                 NTCPIP_PBUF_RAM);
  if (q == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpTimeExceeded: failed to allocate pbuf for ICMP packet.\n"));
    return;
  }
  NTCPIP__LWIP_ASSERT("check that first pbuf can hold icmp message",
             (q->len >= (sizeof(struct icmp_echo_hdr) + NTCPIP__IP_HLEN + ICMP_DEST_UNREACH_DATASIZE)));

  iphdr = p->payload;
  NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("ntcpip__icmpTimeExceeded from "));
  ntcpip_ipaddrDebugPrint(NTCPIP__ICMP_DEBUG, &(iphdr->src));
  NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, (" to "));
  ntcpip_ipaddrDebugPrint(NTCPIP__ICMP_DEBUG, &(iphdr->dest));
  NTCPIP__LWIP_DEBUGF(NTCPIP__ICMP_DEBUG, ("\n"));

  icmphdr = q->payload;
  icmphdr->type = type;
  icmphdr->code = code;
  icmphdr->id = 0;
  icmphdr->seqno = 0;

  /* copy fields from original packet */
  NTCPIP__SMEMCPY((Uint8 *)q->payload + sizeof(struct icmp_echo_hdr), (Uint8 *)p->payload,
          NTCPIP__IP_HLEN + ICMP_DEST_UNREACH_DATASIZE);

  /* calculate checksum */
  icmphdr->chksum = 0;
  icmphdr->chksum = ntcpip__inetChksum(icmphdr, q->len);
  ICMP_STATS_INC(icmp.xmit);
  /* increase number of messages attempted to send */
  ntcpip__snmpIncIcmpoutmsgs();
  /* increase number of destination unreachable messages attempted to send */
  ntcpip__snmpIncIcmpouttimeexcds();
  ntcpip__ipOutput(q, NULL, &(iphdr->src), NTCPIP__ICMP_TTL, 0, IP_PROTO_ICMP);
  ntcpip_pbufFree(q);
}

#endif /* NTCPIP__LWIP_ICMP */


