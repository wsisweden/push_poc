/**
 * @file
 * This is the IPv4 layer implementation for incoming and outgoing IP traffic.
 * 
 * @see ntcpip__ipFrag.c
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"
#include "ntcpip/ip.h"
#include "../../def.h"
#include "../../mem.h"
#include "../../ip_frag.h"
#include "ntcpip/inet.h"
#include "../../inet_chksum.h"
#include "ntcpip/netif.h"
#include "../../icmp.h"
#include "ntcpip/igmp.h"
#include "../../raw.h"
#include "ntcpip/udp.h"
#include "ntcpip/tcp.h"
#include "ntcpip/snmp.h"
#include "../../dhcp.h"
#include "../../stats.h"
#include "../../../arch/perf.h"

#include <string.h>

#if TARGET_SELECTED & TARGET_W32
#include <stdio.h>
#endif

/**
 * The interface that provided the packet for the current callback
 * invocation.
 */
struct ntcpip__netif *ntcpip__currentNetif;

/**
 * Header of the input packet currently being processed.
 */
const struct ip_hdr *ntcpip__currentHeader;

/**
 * Finds the appropriate network interface for a given IP address. It
 * searches the list of network interfaces linearly. A match is found
 * if the masked IP address of the network interface equals the masked
 * IP address given to the function.
 *
 * @param dest the destination IP address for which to find the route
 * @return the netif on which to send to reach dest
 */
struct ntcpip__netif *
ntcpip__ipRoute(const struct ntcpip_ipAddr *dest)
{
  struct ntcpip__netif *netif;

  /* iterate through netifs */
  for(netif = ntcpip__netifList; netif != NULL; netif = netif->next) {
    /* network mask matches? */
    if (ntcpip__netifIsUp(netif)) {
      if (ntcpip_ipaddrNetCmp(dest, &(netif->ip_addr), &(netif->netmask))) {
        /* return netif on which to forward IP packet */
        return netif;
      }
    }
  }
  if ((ntcpip__netifDefault == NULL) || (!ntcpip__netifIsUp(ntcpip__netifDefault))) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip__ipRoute: No route to 0x%"X32_F"\n", dest->addr));
    IP_STATS_INC(ip.rterr);
    ntcpip__snmpIncIpoutnoroutes();
    return NULL;
  }
  /* no matching netif found, use default netif */
  return ntcpip__netifDefault;
}

#if NTCPIP__IP_FORWARD
/**
 * Forwards an IP packet. It finds an appropriate route for the
 * packet, decrements the TTL value of the packet, adjusts the
 * checksum and outputs the packet on the appropriate interface.
 *
 * @param p the packet to forward (p->payload points to IP header)
 * @param iphdr the IP header of the input packet
 * @param inp the netif on which this packet was received
 * @return the netif on which the packet was sent (NULL if it wasn't sent)
 */
static struct ntcpip__netif *
ip_forward(struct ntcpip_pbuf *p, struct ip_hdr *iphdr, struct ntcpip__netif *inp)
{
  struct ntcpip__netif *netif;

  NTCPIP__PERF_START;
  /* Find network interface where to forward this IP packet to. */
  netif = ntcpip__ipRoute((struct ntcpip_ipAddr *)&(iphdr->dest));
  if (netif == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ip_forward: no forwarding route for 0x%"X32_F" found\n",
                      iphdr->dest.addr));
    ntcpip__snmpIncIpoutnoroutes();
    return (struct ntcpip__netif *)NULL;
  }
  /* Do not forward packets onto the same network interface on which
   * they arrived. */
  if (netif == inp) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ip_forward: not bouncing packets back on incoming interface.\n"));
    ntcpip__snmpIncIpoutnoroutes();
    return (struct ntcpip__netif *)NULL;
  }

  /* decrement TTL */
  IPH_TTL_SET(iphdr, IPH_TTL(iphdr) - 1);
  /* send ICMP if TTL == 0 */
  if (IPH_TTL(iphdr) == 0) {
    ntcpip__snmpIncIpinhdrerrors();
#if NTCPIP__LWIP_ICMP
    /* Don't send ICMP messages in response to ICMP messages */
    if (IPH_PROTO(iphdr) != IP_PROTO_ICMP) {
      ntcpip__icmpTimeExceeded(p, ICMP_TE_TTL);
    }
#endif /* NTCPIP__LWIP_ICMP */
    return (struct ntcpip__netif *)NULL;
  }

  /* Incrementally update the IP checksum. */
  if (IPH_CHKSUM(iphdr) >= ntcpip_htons(0xffff - 0x100)) {
    IPH_CHKSUM_SET(iphdr, IPH_CHKSUM(iphdr) + ntcpip_htons(0x100) + 1);
  } else {
    IPH_CHKSUM_SET(iphdr, IPH_CHKSUM(iphdr) + ntcpip_htons(0x100));
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ip_forward: forwarding packet to 0x%"X32_F"\n",
                    iphdr->dest.addr));

  IP_STATS_INC(ip.fw);
  IP_STATS_INC(ip.xmit);
  ntcpip__snmpIncIpforwdatagrams();

  NTCPIP__PERF_STOP("ip_forward");
  /* transmit pbuf on chosen interface */
  netif->output(netif, p, (struct ntcpip_ipAddr *)&(iphdr->dest));
  return netif;
}
#endif /* NTCPIP__IP_FORWARD */

/**
 * This function is called by the network interface device driver when
 * an IP packet is received. The function does the basic checks of the
 * IP header such as packet size being at least larger than the header
 * size etc. If the packet was not destined for us, the packet is
 * forwarded (using ip_forward). The IP checksum is always checked.
 *
 * Finally, the packet is sent to the upper layer protocol input function.
 * 
 * @param p the received IP packet (p->payload points to IP header)
 * @param inp the netif on which this packet was received
 * @return NTCPIP_ERR_OK if the packet was processed (could return ERR_* if it wasn't
 *         processed, but currently always returns NTCPIP_ERR_OK)
 */
ntcpip_Err
ntcpip__ipInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp)
{
  struct ip_hdr *iphdr;
  struct ntcpip__netif *netif;
  Uint16 iphdr_hlen;
  Uint16 iphdr_len;
#if NTCPIP__LWIP_DHCP
  int check_ip_src=1;
#endif /* NTCPIP__LWIP_DHCP */

  IP_STATS_INC(ip.recv);
  ntcpip__snmpIncIpinreceives();

  /* identify the IP header */
  iphdr = p->payload;
  if (IPH_V(iphdr) != 4) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_WARNING, ("IP packet dropped due to bad version number %"U16_F"\n", IPH_V(iphdr)));
    ip_debug_print(p);
    ntcpip_pbufFree(p);
    IP_STATS_INC(ip.err);
    IP_STATS_INC(ip.drop);
    ntcpip__snmpIncIpinhdrerrors();
    return NTCPIP_ERR_OK;
  }

  /* obtain IP header length in number of 32-bit words */
  iphdr_hlen = IPH_HL(iphdr);
  /* calculate IP header length in bytes */
  iphdr_hlen *= 4;
  /* obtain ip length in bytes */
  iphdr_len = ntcpip_ntohs(IPH_LEN(iphdr));

  /* header length exceeds first pbuf length, or ip length exceeds total pbuf length? */
  if ((iphdr_hlen > p->len) || (iphdr_len > p->tot_len)) {
    if (iphdr_hlen > p->len) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
        ("IP header (len %"U16_F") does not fit in first pbuf (len %"U16_F"), IP packet dropped.\n",
        iphdr_hlen, p->len));
    }
    if (iphdr_len > p->tot_len) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
        ("IP (len %"U16_F") is longer than pbuf (len %"U16_F"), IP packet dropped.\n",
        iphdr_len, p->tot_len));
    }
    /* free (drop) packet pbufs */
    ntcpip_pbufFree(p);
    IP_STATS_INC(ip.lenerr);
    IP_STATS_INC(ip.drop);
    ntcpip__snmpIncIpindiscards();
    return NTCPIP_ERR_OK;
  }

  /* verify checksum */
#if NTCPIP__CHECKSUM_CHECK_IP
  if (ntcpip__inetChksum(iphdr, iphdr_hlen) != 0) {

#if TARGET_SELECTED & TARGET_W32
	printf("IP layer dropped packet. Verify that NIC checksum offload is disabled!\n");
#endif

    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS,
      ("Checksum (0x%"X16_F") failed, IP packet dropped.\n", ntcpip__inetChksum(iphdr, iphdr_hlen)));
    ip_debug_print(p);
    ntcpip_pbufFree(p);
    IP_STATS_INC(ip.chkerr);
    IP_STATS_INC(ip.drop);
    ntcpip__snmpIncIpinhdrerrors();
    return NTCPIP_ERR_OK;
  }
#endif

  /* Trim pbuf. This should have been done at the netif layer,
   * but we'll do it anyway just to be sure that its done. */
  ntcpip_pbufRealloc(p, iphdr_len);

  /* match packet against an interface, i.e. is this packet for us? */
#if NTCPIP__LWIP_IGMP
  if (ntcpip_ipaddrIsMulticast(&(iphdr->dest))) {
    if ((inp->flags & NTCPIP_NETIF_IGMP) && (ntcpip__igmpLookforGroup(inp, &(iphdr->dest)))) {
      netif = inp;
    } else {
      netif = NULL;
    }
  } else
#endif /* NTCPIP__LWIP_IGMP */
  {
    /* start trying with inp. if that's not acceptable, start walking the
       list of configured netifs.
       'first' is used as a boolean to mark whether we started walking the list */
    int first = 1;
    netif = inp;
    do {
      NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ntcpip__ipInput: iphdr->dest 0x%"X32_F" netif->ip_addr 0x%"X32_F" (0x%"X32_F", 0x%"X32_F", 0x%"X32_F")\n",
          iphdr->dest.addr, netif->ip_addr.addr,
          iphdr->dest.addr & netif->netmask.addr,
          netif->ip_addr.addr & netif->netmask.addr,
          iphdr->dest.addr & ~(netif->netmask.addr)));

      /* interface is up and configured? */
      if ((ntcpip__netifIsUp(netif)) && (!ntcpip_ipaddrIsAny(&(netif->ip_addr)))) {
        /* unicast to this interface address? */
        if (ntcpip_ipaddrCmp(&(iphdr->dest), &(netif->ip_addr)) ||
            /* or broadcast on this interface network address? */
            ntcpip__ipaddrIsBroadcast(&(iphdr->dest), netif)) {
          NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ntcpip__ipInput: packet accepted on interface %c%c\n",
              netif->name[0], netif->name[1]));
          /* break out of for loop */
          break;
        }
      }
      if (first) {
        first = 0;
        netif = ntcpip__netifList;
      } else {
        netif = netif->next;
      }
      if (netif == inp) {
        netif = netif->next;
      }
    } while(netif != NULL);
  }

#if NTCPIP__LWIP_DHCP
  /* Pass DHCP messages regardless of destination address. DHCP traffic is addressed
   * using link layer addressing (such as Ethernet MAC) so we must not filter on IP.
   * According to RFC 1542 section 3.1.1, referred by RFC 2131).
   */
  if (netif == NULL) {
    /* remote port is DHCP server? */
    if (IPH_PROTO(iphdr) == IP_PROTO_UDP) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__ipInput: UDP packet to DHCP client port %"U16_F"\n",
        ntcpip_ntohs(((struct udp_hdr *)((Uint8 *)iphdr + iphdr_hlen))->dest)));
      if (ntcpip_ntohs(((struct udp_hdr *)((Uint8 *)iphdr + iphdr_hlen))->dest) == DHCP_CLIENT_PORT) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__ipInput: DHCP packet accepted.\n"));
        netif = inp;
        check_ip_src = 0;
      }
    }
  }
#endif /* NTCPIP__LWIP_DHCP */

  /* broadcast or multicast packet source address? Compliant with RFC 1122: 3.2.1.3 */
#if NTCPIP__LWIP_DHCP
  /* DHCP servers need 0.0.0.0 to be allowed as source address (RFC 1.1.2.2: 3.2.1.3/a) */
  if (check_ip_src && (iphdr->src.addr != 0))
#endif /* NTCPIP__LWIP_DHCP */
  {  if ((ntcpip__ipaddrIsBroadcast(&(iphdr->src), inp)) ||
         (ntcpip_ipaddrIsMulticast(&(iphdr->src)))) {
      /* packet source is not valid */
      NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_LEVEL_WARNING, ("ntcpip__ipInput: packet source is not valid.\n"));
      /* free (drop) packet pbufs */
      ntcpip_pbufFree(p);
      IP_STATS_INC(ip.drop);
      ntcpip__snmpIncIpinaddrerrors();
      ntcpip__snmpIncIpindiscards();
      return NTCPIP_ERR_OK;
    }
  }

  /* packet not for us? */
  if (netif == NULL) {
    /* packet not for us, route or discard */
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_TRACE, ("ntcpip__ipInput: packet not for us.\n"));
#if NTCPIP__IP_FORWARD
    /* non-broadcast packet? */
    if (!ntcpip__ipaddrIsBroadcast(&(iphdr->dest), inp)) {
      /* try to forward IP packet on (other) interfaces */
      ip_forward(p, iphdr, inp);
    } else
#endif /* NTCPIP__IP_FORWARD */
    {
      ntcpip__snmpIncIpinaddrerrors();
      ntcpip__snmpIncIpindiscards();
    }
    ntcpip_pbufFree(p);
    return NTCPIP_ERR_OK;
  }
  /* packet consists of multiple fragments? */
  if ((IPH_OFFSET(iphdr) & ntcpip_htons(IP_OFFMASK | IP_MF)) != 0) {
#if NTCPIP__IP_REASSEMBLY /* packet fragment reassembly code present? */
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("IP packet is a fragment (id=0x%04"X16_F" tot_len=%"U16_F" len=%"U16_F" MF=%"U16_F" offset=%"U16_F"), calling ntcpip__ipReass()\n",
      ntcpip_ntohs(IPH_ID(iphdr)), p->tot_len, ntcpip_ntohs(IPH_LEN(iphdr)), !!(IPH_OFFSET(iphdr) & ntcpip_htons(IP_MF)), (ntcpip_ntohs(IPH_OFFSET(iphdr)) & IP_OFFMASK)*8));
    /* reassemble the packet*/
    p = ntcpip__ipReass(p);
    /* packet not fully reassembled yet? */
    if (p == NULL) {
      return NTCPIP_ERR_OK;
    }
    iphdr = p->payload;
#else /* NTCPIP__IP_REASSEMBLY == 0, no packet fragment reassembly code present */
    ntcpip_pbufFree(p);
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("IP packet dropped since it was fragmented (0x%"X16_F") (while NTCPIP__IP_REASSEMBLY == 0).\n",
      ntcpip_ntohs(IPH_OFFSET(iphdr))));
    IP_STATS_INC(ip.opterr);
    IP_STATS_INC(ip.drop);
    /* unsupported protocol feature */
    ntcpip__snmpIncIpinunknownprotos();
    return NTCPIP_ERR_OK;
#endif /* NTCPIP__IP_REASSEMBLY */
  }

#if NTCPIP__IP_OPTIONS_ALLOWED == 0 /* no support for IP options in the IP header? */

#if NTCPIP__LWIP_IGMP
  /* there is an extra "router alert" option in IGMP messages which we allow for but do not police */
  if((iphdr_hlen > NTCPIP__IP_HLEN &&  (IPH_PROTO(iphdr) != NTCPIP__IP_PROTO_IGMP)) {
#else
  if (iphdr_hlen > NTCPIP__IP_HLEN) {
#endif /* NTCPIP__LWIP_IGMP */
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("IP packet dropped since there were IP options (while NTCPIP__IP_OPTIONS_ALLOWED == 0).\n"));
    ntcpip_pbufFree(p);
    IP_STATS_INC(ip.opterr);
    IP_STATS_INC(ip.drop);
    /* unsupported protocol feature */
    ntcpip__snmpIncIpinunknownprotos();
    return NTCPIP_ERR_OK;
  }
#endif /* NTCPIP__IP_OPTIONS_ALLOWED == 0 */

  /* send to upper layers */
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ntcpip__ipInput: \n"));
  ip_debug_print(p);
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ntcpip__ipInput: p->len %"U16_F" p->tot_len %"U16_F"\n", p->len, p->tot_len));

  ntcpip__currentNetif = inp;
  ntcpip__currentHeader = iphdr;

#if NTCPIP__LWIP_RAW
  /* raw input did not eat the packet? */
  if (raw_input(p, inp) == 0)
#endif /* NTCPIP__LWIP_RAW */
  {

    switch (IPH_PROTO(iphdr)) {
#if NTCPIP__LWIP_UDP
    case IP_PROTO_UDP:
#if NTCPIP__LWIP_UDPLITE
    case IP_PROTO_UDPLITE:
#endif /* NTCPIP__LWIP_UDPLITE */
      ntcpip__snmpIncIpindelivers();
      ntcpip__udpInput(p, inp);
      break;
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
    case IP_PROTO_TCP:
      ntcpip__snmpIncIpindelivers();
      ntcpip__tcpInput(p, inp);
      break;
#endif /* NTCPIP__LWIP_TCP */
#if NTCPIP__LWIP_ICMP
    case IP_PROTO_ICMP:
      ntcpip__snmpIncIpindelivers();
      ntcpip__icmpInput(p, inp);
      break;
#endif /* NTCPIP__LWIP_ICMP */
#if NTCPIP__LWIP_IGMP
    case NTCPIP__IP_PROTO_IGMP:
      ntcpip__igmpInput(p,inp,&(iphdr->dest));
      break;
#endif /* NTCPIP__LWIP_IGMP */
    default:
#if NTCPIP__LWIP_ICMP
      /* send ICMP destination protocol unreachable unless is was a broadcast */
      if (!ntcpip__ipaddrIsBroadcast(&(iphdr->dest), inp) &&
          !ntcpip_ipaddrIsMulticast(&(iphdr->dest))) {
        p->payload = iphdr;
        ntcpip__icmpDestUnreach(p, ICMP_DUR_PROTO);
      }
#endif /* NTCPIP__LWIP_ICMP */
      ntcpip_pbufFree(p);

      NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("Unsupported transport protocol %"U16_F"\n", IPH_PROTO(iphdr)));

      IP_STATS_INC(ip.proterr);
      IP_STATS_INC(ip.drop);
      ntcpip__snmpIncIpinunknownprotos();
    }
  }

  ntcpip__currentNetif = NULL;
  ntcpip__currentHeader = NULL;

  return NTCPIP_ERR_OK;
}

/**
 * Sends an IP packet on a network interface. This function constructs
 * the IP header and calculates the IP header checksum. If the source
 * IP address is NULL, the IP address of the outgoing network
 * interface is filled in as source address.
 * If the destination IP address is IP_HDRINCL, p is assumed to already
 * include an IP header and p->payload points to it instead of the data.
 *
 * @param p the packet to send (p->payload points to the data, e.g. next
            protocol header; if dest == IP_HDRINCL, p already includes an IP
            header and p->payload points to that IP header)
 * @param src the source IP address to send from (if src == NTCPIP_IP_ADDR_ANY, the
 *         IP  address of the netif used to send is used as source address)
 * @param dest the destination IP address to send the packet to
 * @param ttl the TTL value to be set in the IP header
 * @param tos the TOS value to be set in the IP header
 * @param proto the PROTOCOL to be set in the IP header
 * @param netif the netif on which to send this packet
 * @return NTCPIP_ERR_OK if the packet was sent OK
 *         NTCPIP_ERR_BUF if p doesn't have enough space for IP/LINK headers
 *         returns errors returned by netif->output
 *
 * @note ip_id: RFC791 "some host may be able to simply use
 *  unique identifiers independent of destination"
 */
ntcpip_Err
ntcpip__ipOutputIf(struct ntcpip_pbuf *p, const struct ntcpip_ipAddr *src, const struct ntcpip_ipAddr *dest,
             Uint8 ttl, Uint8 tos,
             Uint8 proto, struct ntcpip__netif *netif)
{
#if IP_OPTIONS_SEND
  return ntcpip__ipOutputIfOpt(p, src, dest, ttl, tos, proto, netif, NULL, 0);
}

/**
 * Same as ntcpip__ipOutputIf() but with the possibility to include IP options:
 *
 * @ param ip_options pointer to the IP options, copied into the IP header
 * @ param optlen length of ip_options
 */
ntcpip_Err ntcpip__ipOutputIfOpt(struct ntcpip_pbuf *p, const struct ntcpip_ipAddr *src, const struct ntcpip_ipAddr *dest,
       Uint8 ttl, Uint8 tos, Uint8 proto, struct ntcpip__netif *netif, void *ip_options,
       Uint16 optlen)
{
#endif /* IP_OPTIONS_SEND */
  struct ip_hdr *iphdr;
  static Uint16 ip_id = 0;

  ntcpip__snmpIncIpoutrequests();

  /* Should the IP header be generated or is it already included in p? */
  if (dest != IP_HDRINCL) {
    Uint16 ip_hlen = NTCPIP__IP_HLEN;
#if IP_OPTIONS_SEND
    Uint16 optlen_aligned = 0;
    if (optlen != 0) {
      /* round up to a multiple of 4 */
      optlen_aligned = ((optlen + 3) & ~3);
      ip_hlen += optlen_aligned;
      /* First write in the IP options */
      if (ntcpip__pbufHeader(p, optlen_aligned)) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip__ipOutputIfOpt: not enough room for IP options in pbuf\n"));
        IP_STATS_INC(ip.err);
        ntcpip__snmpIncIpoutdiscards();
        return NTCPIP_ERR_BUF;
      }
      NTCPIP__MEMCPY(p->payload, ip_options, optlen);
      if (optlen < optlen_aligned) {
        /* zero the remaining bytes */
        memset(((char*)p->payload) + optlen, 0, optlen_aligned - optlen);
      }
    }
#endif /* IP_OPTIONS_SEND */
    /* generate IP header */
    if (ntcpip__pbufHeader(p, NTCPIP__IP_HLEN)) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG | NTCPIP__LWIP_DBG_LEVEL_SERIOUS, ("ntcpip__ipOutput: not enough room for IP header in pbuf\n"));

      IP_STATS_INC(ip.err);
      ntcpip__snmpIncIpoutdiscards();
      return NTCPIP_ERR_BUF;
    }

    iphdr = p->payload;
    NTCPIP__LWIP_ASSERT("check that first pbuf can hold struct ip_hdr",
               (p->len >= sizeof(struct ip_hdr)));

    IPH_TTL_SET(iphdr, ttl);
    IPH_PROTO_SET(iphdr, proto);

    ntcpip_ipaddrSet(&(iphdr->dest), dest);

    IPH_VHLTOS_SET(iphdr, 4, ip_hlen / 4, tos);
    IPH_LEN_SET(iphdr, ntcpip_htons(p->tot_len));
    IPH_OFFSET_SET(iphdr, 0);
    IPH_ID_SET(iphdr, ntcpip_htons(ip_id));
    ++ip_id;

    if (ntcpip_ipaddrIsAny(src)) {
      ntcpip_ipaddrSet(&(iphdr->src), &(netif->ip_addr));
    } else {
      ntcpip_ipaddrSet(&(iphdr->src), src);
    }

    IPH_CHKSUM_SET(iphdr, 0);
#if NTCPIP__CHECKSUM_GEN_IP
    IPH_CHKSUM_SET(iphdr, ntcpip__inetChksum(iphdr, ip_hlen));
#endif
  } else {
    /* IP header already included in p */
    iphdr = p->payload;
    dest = &(iphdr->dest);
  }

  IP_STATS_INC(ip.xmit);

  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ntcpip__ipOutputIf: %c%c%"U16_F"\n", netif->name[0], netif->name[1], netif->num));
  ip_debug_print(p);

#if ENABLE_LOOPBACK
  if (ntcpip_ipaddrCmp(dest, &netif->ip_addr)) {
    /* Packet to self, enqueue it for loopback */
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("netif_loop_output()"));
    return netif_loop_output(netif, p, dest);
  }
#endif /* ENABLE_LOOPBACK */
#if NTCPIP__IP_FRAG
  /* don't fragment if interface has mtu set to 0 [loopif] */
  if (netif->mtu && (p->tot_len > netif->mtu)) {
    return ntcpip__ipFrag(p,netif,dest);
  }
#endif

  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("netif->output()"));
  return netif->output(netif, p, dest);
}

/**
 * Simple interface to ntcpip__ipOutputIf. It finds the outgoing network
 * interface and calls upon ntcpip__ipOutputIf to do the actual work.
 *
 * @param p the packet to send (p->payload points to the data, e.g. next
            protocol header; if dest == IP_HDRINCL, p already includes an IP
            header and p->payload points to that IP header)
 * @param src the source IP address to send from (if src == NTCPIP_IP_ADDR_ANY, the
 *         IP  address of the netif used to send is used as source address)
 * @param dest the destination IP address to send the packet to
 * @param ttl the TTL value to be set in the IP header
 * @param tos the TOS value to be set in the IP header
 * @param proto the PROTOCOL to be set in the IP header
 *
 * @return NTCPIP_ERR_RTE if no route is found
 *         see ntcpip__ipOutputIf() for more return values
 */
ntcpip_Err
ntcpip__ipOutput(struct ntcpip_pbuf *p, struct ntcpip_ipAddr *src, struct ntcpip_ipAddr *dest,
          Uint8 ttl, Uint8 tos, Uint8 proto)
{
  struct ntcpip__netif *netif;

  if ((netif = ntcpip__ipRoute(dest)) == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ntcpip__ipOutput: No route to 0x%"X32_F"\n", dest->addr));
    IP_STATS_INC(ip.rterr);
    return NTCPIP_ERR_RTE;
  }

  return ntcpip__ipOutputIf(p, src, dest, ttl, tos, proto, netif);
}

#if NTCPIP__LWIP_NETIF_HWADDRHINT
/** Like ntcpip__ipOutput, but takes and addr_hint pointer that is passed on to netif->addr_hint
 *  before calling ntcpip__ipOutputIf.
 *
 * @param p the packet to send (p->payload points to the data, e.g. next
            protocol header; if dest == IP_HDRINCL, p already includes an IP
            header and p->payload points to that IP header)
 * @param src the source IP address to send from (if src == NTCPIP_IP_ADDR_ANY, the
 *         IP  address of the netif used to send is used as source address)
 * @param dest the destination IP address to send the packet to
 * @param ttl the TTL value to be set in the IP header
 * @param tos the TOS value to be set in the IP header
 * @param proto the PROTOCOL to be set in the IP header
 * @param addr_hint address hint pointer set to netif->addr_hint before
 *        calling ntcpip__ipOutputIf()
 *
 * @return NTCPIP_ERR_RTE if no route is found
 *         see ntcpip__ipOutputIf() for more return values
 */
ntcpip_Err
ip_output_hinted(struct ntcpip_pbuf *p, struct ntcpip_ipAddr *src, struct ntcpip_ipAddr *dest,
          Uint8 ttl, Uint8 tos, Uint8 proto, Uint8 *addr_hint)
{
  struct ntcpip__netif *netif;
  ntcpip_Err err;

  if ((netif = ntcpip__ipRoute(dest)) == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("ntcpip__ipOutput: No route to 0x%"X32_F"\n", dest->addr));
    IP_STATS_INC(ip.rterr);
    return NTCPIP_ERR_RTE;
  }

  netif->addr_hint = addr_hint;
  err = ntcpip__ipOutputIf(p, src, dest, ttl, tos, proto, netif);
  netif->addr_hint = NULL;

  return err;
}
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/

#if NTCPIP__IP_DEBUG
/* Print an IP header by using NTCPIP__LWIP_DEBUGF
 * @param p an IP packet, p->payload pointing to the IP header
 */
void
ip_debug_print(struct ntcpip_pbuf *p)
{
  struct ip_hdr *iphdr = p->payload;
  Uint8 *payload;

  payload = (Uint8 *)iphdr + NTCPIP__IP_HLEN;

  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("IP header:\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("|%2"S16_F" |%2"S16_F" |  0x%02"X16_F" |     %5"U16_F"     | (v, hl, tos, len)\n",
                    IPH_V(iphdr),
                    IPH_HL(iphdr),
                    IPH_TOS(iphdr),
                    ntcpip_ntohs(IPH_LEN(iphdr))));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("|    %5"U16_F"      |%"U16_F"%"U16_F"%"U16_F"|    %4"U16_F"   | (id, flags, offset)\n",
                    ntcpip_ntohs(IPH_ID(iphdr)),
                    ntcpip_ntohs(IPH_OFFSET(iphdr)) >> 15 & 1,
                    ntcpip_ntohs(IPH_OFFSET(iphdr)) >> 14 & 1,
                    ntcpip_ntohs(IPH_OFFSET(iphdr)) >> 13 & 1,
                    ntcpip_ntohs(IPH_OFFSET(iphdr)) & IP_OFFMASK));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("|  %3"U16_F"  |  %3"U16_F"  |    0x%04"X16_F"     | (ttl, proto, chksum)\n",
                    IPH_TTL(iphdr),
                    IPH_PROTO(iphdr),
                    ntcpip_ntohs(IPH_CHKSUM(iphdr))));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("|  %3"U16_F"  |  %3"U16_F"  |  %3"U16_F"  |  %3"U16_F"  | (src)\n",
                    ntcpip_ipaddr1(&iphdr->src),
                    ntcpip_ipaddr2(&iphdr->src),
                    ntcpip_ipaddr3(&iphdr->src),
                    ntcpip_ipaddr4(&iphdr->src)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("|  %3"U16_F"  |  %3"U16_F"  |  %3"U16_F"  |  %3"U16_F"  | (dest)\n",
                    ntcpip_ipaddr1(&iphdr->dest),
                    ntcpip_ipaddr2(&iphdr->dest),
                    ntcpip_ipaddr3(&iphdr->dest),
                    ntcpip_ipaddr4(&iphdr->dest)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IP_DEBUG, ("+-------------------------------+\n"));
}
#endif /* NTCPIP__IP_DEBUG */


