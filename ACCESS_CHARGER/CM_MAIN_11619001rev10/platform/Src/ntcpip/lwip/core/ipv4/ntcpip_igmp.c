/**
 * @file
 * IGMP - Internet Group Management Protocol
 *
 */

/*
 * Copyright (c) 2002 CITEL Technologies Ltd.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met: 
 * 1. Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 3. Neither the name of CITEL Technologies Ltd nor the names of its contributors 
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY CITEL TECHNOLOGIES AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED.  IN NO EVENT SHALL CITEL TECHNOLOGIES OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. 
 *
 * This file is a contribution to the lwIP TCP/IP stack.
 * The Swedish Institute of Computer Science and Adam Dunkels
 * are specifically granted permission to redistribute this
 * source code.
*/

/*-------------------------------------------------------------
Note 1)
Although the rfc requires V1 AND V2 capability
we will only support v2 since now V1 is very old (August 1989)
V1 can be added if required

a debug print and statistic have been implemented to
show this up.
-------------------------------------------------------------
-------------------------------------------------------------
Note 2)
A query for a specific group address (as opposed to ALLHOSTS)
has now been implemented as I am unsure if it is required

a debug print and statistic have been implemented to
show this up.
-------------------------------------------------------------
-------------------------------------------------------------
Note 3)
The router alert rfc 2113 is implemented in outgoing packets
but not checked rigorously incoming
-------------------------------------------------------------
Steve Reynolds
------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 * RFC 988  - Host extensions for IP multicasting                         - V0
 * RFC 1054 - Host extensions for IP multicasting                         -
 * RFC 1112 - Host extensions for IP multicasting                         - V1
 * RFC 2236 - Internet Group Management Protocol, Version 2               - V2  <- this code is based on this RFC (it's the "de facto" standard)
 * RFC 3376 - Internet Group Management Protocol, Version 3               - V3
 * RFC 4604 - Using Internet Group Management Protocol Version 3...       - V3+
 * RFC 2113 - IP Router Alert Option                                      - 
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 * Includes
 *----------------------------------------------------------------------------*/

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_IGMP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/igmp.h"
#include "ntcpip/debug.h"
#include "../../def.h"
#include "../../mem.h"
#include "ntcpip/ip.h"
#include "ntcpip/inet.h"
#include "../../inet_chksum.h"
#include "ntcpip/netif.h"
#include "../../icmp.h"
#include "ntcpip/udp.h"
#include "ntcpip/tcp.h"
#include "../../stats.h"

#include "string.h"

/*-----------------------------------------------------------------------------
 * Globales
 *----------------------------------------------------------------------------*/

static struct ntcpip__igmpGroup* igmp_group_list;
static struct ntcpip_ipAddr     allsystems;
static struct ntcpip_ipAddr     allrouters;

/**
 * Initialize the IGMP module
 */
void
ntcpip__igmpInit(void)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInit: initializing\n"));

  NTCPIP_IP4_ADDR(&allsystems, 224, 0, 0, 1);
  NTCPIP_IP4_ADDR(&allrouters, 224, 0, 0, 2);
}

#ifdef LWIP_DEBUG
/**
 * Dump global IGMP groups list
 */
void
ntcpip__igmpDumpGroupList()
{ 
  struct ntcpip__igmpGroup *group = igmp_group_list;

  while (group != NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpDumpGroupList: [%"U32_F"] ", (Uint32)(group->group_state)));
    ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, &group->group_address);
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (" on if %p\n", group->interface));
    group = group->next;
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("\n"));
}
#else
#define ntcpip__igmpDumpGroupList()
#endif /* LWIP_DEBUG */

/**
 * Start IGMP processing on interface
 *
 * @param netif network interface on which start IGMP processing
 */
ntcpip_Err
ntcpip__igmpStart(struct ntcpip__netif *netif)
{
  struct ntcpip__igmpGroup* group;

  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpStart: starting IGMP processing on if %p\n", netif));

  group = ntcpip__igmpLookupGroup(netif, &allsystems);

  if (group != NULL) {
    group->group_state = NTCPIP__IGMP_GROUP_IDLE_MEMBER;
    group->use++;

    /* Allow the igmp messages at the MAC level */
    if (netif->igmp_mac_filter != NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpStart: igmp_mac_filter(ADD "));
      ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, &allsystems);
      NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (") on if %p\n", netif));
      netif->igmp_mac_filter( netif, &allsystems, NTCPIP_IGMP_ADD_MAC);
    }

    return NTCPIP_ERR_OK;
  }

  return NTCPIP_ERR_MEM;
}

/**
 * Stop IGMP processing on interface
 *
 * @param netif network interface on which stop IGMP processing
 */
ntcpip_Err
ntcpip__igmpStop(struct ntcpip__netif *netif)
{
  struct ntcpip__igmpGroup *group = igmp_group_list;
  struct ntcpip__igmpGroup *prev  = NULL;
  struct ntcpip__igmpGroup *next;

  /* look for groups joined on this interface further down the list */
  while (group != NULL) {
    next = group->next;
    /* is it a group joined on this interface? */
    if (group->interface == netif) {
      /* is it the first group of the list? */
      if (group == igmp_group_list) {
        igmp_group_list = next;
      }
      /* is there a "previous" group defined? */
      if (prev != NULL) {
        prev->next = next;
      }
      /* disable the group at the MAC level */
      if (netif->igmp_mac_filter != NULL) {
        NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpStop: igmp_mac_filter(DEL "));
        ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, &group->group_address);
        NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (") on if %p\n", netif));
        netif->igmp_mac_filter(netif, &(group->group_address), NTCPIP_IGMP_DEL_MAC);
      }
      /* free group */
      ntcpip__mempFree(MEMP_IGMP_GROUP, group);
    } else {
      /* change the "previous" */
      prev = group;
    }
    /* move to "next" */
    group = next;
  }
  return NTCPIP_ERR_OK;
}

/**
 * Report IGMP memberships for this interface
 *
 * @param netif network interface on which report IGMP memberships
 */
void
ntcpip__igmpReportGroups( struct ntcpip__netif *netif)
{
  struct ntcpip__igmpGroup *group = igmp_group_list;

  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpReportGroups: sending IGMP reports on if %p\n", netif));

  while (group != NULL) {
    if (group->interface == netif) {
      ntcpip__igmpDelayingMember( group, NTCPIP__IGMP_JOIN_DLY_MEMBER_TMR);
    }
    group = group->next;
  }
}

/**
 * Search for a group in the global igmp_group_list
 *
 * @param ifp the network interface for which to look
 * @param addr the group ip address to search for
 * @return a struct ntcpip__igmpGroup* if the group has been found,
 *         NULL if the group wasn't found.
 */
struct ntcpip__igmpGroup *
ntcpip__igmpLookforGroup(struct ntcpip__netif *ifp, const struct ntcpip_ipAddr *addr)
{
  struct ntcpip__igmpGroup *group = igmp_group_list;

  while (group != NULL) {
    if ((group->interface == ifp) && (ntcpip_ipaddrCmp(&(group->group_address), addr))) {
      return group;
    }
    group = group->next;
  }

  /* to be clearer, we return NULL here instead of
   * 'group' (which is also NULL at this point).
   */
  return NULL;
}

/**
 * Search for a specific igmp group and create a new one if not found-
 *
 * @param ifp the network interface for which to look
 * @param addr the group ip address to search
 * @return a struct ntcpip__igmpGroup*,
 *         NULL on memory error.
 */
struct ntcpip__igmpGroup *
ntcpip__igmpLookupGroup(struct ntcpip__netif *ifp, const struct ntcpip_ipAddr *addr)
{
  struct ntcpip__igmpGroup *group = igmp_group_list;
  
  /* Search if the group already exists */
  group = ntcpip__igmpLookforGroup(ifp, addr);
  if (group != NULL) {
    /* Group already exists. */
    return group;
  }

  /* Group doesn't exist yet, create a new one */
  group = ntcpip__mempMalloc(MEMP_IGMP_GROUP);
  if (group != NULL) {
    group->interface          = ifp;
    ntcpip_ipaddrSet(&(group->group_address), addr);
    group->timer              = 0; /* Not running */
    group->group_state        = NTCPIP__IGMP_GROUP_NON_MEMBER;
    group->last_reporter_flag = 0;
    group->use                = 0;
    group->next               = igmp_group_list;
    
    igmp_group_list = group;
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpLookupGroup: %sallocated a new group with address ", (group?"":"impossible to ")));
  ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, addr);
  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (" on if %p\n", ifp));

  return group;
}

/**
 * Remove a group in the global igmp_group_list
 *
 * @param group the group to remove from the global igmp_group_list
 * @return NTCPIP_ERR_OK if group was removed from the list, an ntcpip_Err otherwise
 */
ntcpip_Err
ntcpip__igmpRemoveGroup(struct ntcpip__igmpGroup *group)
{
  ntcpip_Err err = NTCPIP_ERR_OK;

  /* Is it the first group? */
  if (igmp_group_list == group) {
    igmp_group_list = group->next;
  } else {
    /* look for group further down the list */
    struct ntcpip__igmpGroup *tmpGroup;
    for (tmpGroup = igmp_group_list; tmpGroup != NULL; tmpGroup = tmpGroup->next) {
      if (tmpGroup->next == group) {
        tmpGroup->next = group->next;
        break;
      }
    }
    /* Group not found in the global igmp_group_list */
    if (tmpGroup == NULL)
      err = NTCPIP_ERR_ARG;
  }
  /* free group */
  ntcpip__mempFree(MEMP_IGMP_GROUP, group);

  return err;
}

/**
 * Called from ntcpip__ipInput() if a new IGMP packet is received.
 *
 * @param p received igmp packet, p->payload pointing to the ip header
 * @param inp network interface on which the packet was received
 * @param dest destination ip address of the igmp packet
 */
void
ntcpip__igmpInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp, struct ntcpip_ipAddr *dest)
{
  struct ip_hdr *    iphdr;
  struct ntcpip__igmpMsg*   igmp;
  struct ntcpip__igmpGroup* group;
  struct ntcpip__igmpGroup* groupref;

  /* Note that the length CAN be greater than 8 but only 8 are used - All are included in the checksum */    
  iphdr = p->payload;
  if (ntcpip__pbufHeader(p, -(Int16)(IPH_HL(iphdr) * 4)) || (p->len < NTCPIP__IGMP_MINLEN)) {
    ntcpip_pbufFree(p);
    IGMP_STATS_INC(igmp.lenerr);
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: length error\n"));
    return;
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: message from "));
  ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, &(iphdr->src));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (" to address "));
  ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, &(iphdr->dest));
  NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (" on if %p\n", inp));

  /* Now calculate and check the checksum */
  igmp = (struct ntcpip__igmpMsg *)p->payload;
  if (ntcpip__inetChksum(igmp, p->len)) {
    ntcpip_pbufFree(p);
    IGMP_STATS_INC(igmp.chkerr);
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: checksum error\n"));
    return;
  }

  /* Packet is ok so find an existing group */
  group = ntcpip__igmpLookforGroup(inp, dest); /* use the incoming IP address! */
  
  /* If group can be found or create... */
  if (!group) {
    ntcpip_pbufFree(p);
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: IGMP frame not for us\n"));
    return;
  }

  /* NOW ACT ON THE INCOMING MESSAGE TYPE... */
  switch (igmp->igmp_msgtype) {
   case NTCPIP__IGMP_MEMB_QUERY: {
     /* NTCPIP__IGMP_MEMB_QUERY to the "all systems" address ? */
     if ((ntcpip_ipaddrCmp(dest, &allsystems)) && (igmp->igmp_group_address.addr == 0)) {
       /* THIS IS THE GENERAL QUERY */
       NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: General NTCPIP__IGMP_MEMB_QUERY on \"ALL SYSTEMS\" address (224.0.0.1) [igmp_maxresp=%i]\n", (int)(igmp->igmp_maxresp)));

       if (igmp->igmp_maxresp == 0) {
         IGMP_STATS_INC(igmp.v1_rxed);
         NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: got an all hosts query with time== 0 - this is V1 and not implemented - treat as v2\n"));
         igmp->igmp_maxresp = NTCPIP__IGMP_V1_DLY_MEMBER_TMR;
       }

       IGMP_STATS_INC(igmp.group_query_rxed);
       groupref = igmp_group_list;
       while (groupref) {
         /* Do not send messages on the all systems group address! */
         if ((groupref->interface == inp) && (!(ntcpip_ipaddrCmp(&(groupref->group_address), &allsystems)))) {
           ntcpip__igmpDelayingMember( groupref, igmp->igmp_maxresp);
         }
         groupref = groupref->next;
       }
     } else {
       /* NTCPIP__IGMP_MEMB_QUERY to a specific group ? */
       if (group->group_address.addr != 0) {
         NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: NTCPIP__IGMP_MEMB_QUERY to a specific group "));
         ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, &group->group_address);
         if (ntcpip_ipaddrCmp (dest, &allsystems)) {
           NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (" using \"ALL SYSTEMS\" address (224.0.0.1) [igmp_maxresp=%i]\n", (int)(igmp->igmp_maxresp)));
           /* we first need to re-lookfor the group since we used dest last time */
           group = ntcpip__igmpLookforGroup(inp, &igmp->igmp_group_address);
         } else {
           NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (" with the group address as destination [igmp_maxresp=%i]\n", (int)(igmp->igmp_maxresp)));
         }

         if (group != NULL) {
           IGMP_STATS_INC(igmp.unicast_query);
           ntcpip__igmpDelayingMember( group, igmp->igmp_maxresp);
         }
       }
     }
     break;
   }
   case NTCPIP__IGMP_V2_MEMB_REPORT: {
     NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: NTCPIP__IGMP_V2_MEMB_REPORT\n"));

     IGMP_STATS_INC(igmp.report_rxed);
     if (group->group_state == NTCPIP__IGMP_GROUP_DLY_MEMBER) {
       /* This is on a specific group we have already looked up */
       group->timer = 0; /* stopped */
       group->group_state = NTCPIP__IGMP_GROUP_IDLE_MEMBER;
       group->last_reporter_flag = 0;
     }
     break;
   }
   default: {
     NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpInput: unexpected msg %d in state %d on group %p on if %p\n",
       igmp->igmp_msgtype, group->group_state, &group, group->interface));
     break;
   }
  }

  ntcpip_pbufFree(p);
  return;
}

/**
 * Join a group on one network interface.
 *
 * @param ifaddr ip address of the network interface which should join a new group
 * @param groupaddr the ip address of the group which to join
 * @return NTCPIP_ERR_OK if group was joined on the netif(s), an ntcpip_Err otherwise
 */
ntcpip_Err
ntcpip_igmpJoingroup(const struct ntcpip_ipAddr *ifaddr, const struct ntcpip_ipAddr *groupaddr)
{
  ntcpip_Err              err = NTCPIP_ERR_VAL; /* no matching interface */
  struct ntcpip__igmpGroup *group;
  struct ntcpip__netif      *netif;

  /* make sure it is multicast address */
  NTCPIP__LWIP_ERROR("ntcpip_igmpJoingroup: attempt to join non-multicast address", ntcpip_ipaddrIsMulticast(groupaddr), return NTCPIP_ERR_VAL;);
  NTCPIP__LWIP_ERROR("ntcpip_igmpJoingroup: attempt to join allsystems address", (!ntcpip_ipaddrCmp(groupaddr, &allsystems)), return NTCPIP_ERR_VAL;);

  /* loop through netif's */
  netif = ntcpip__netifList;
  while (netif != NULL) {
    /* Should we join this interface ? */
    if ((netif->flags & NTCPIP_NETIF_IGMP) && ((ntcpip_ipaddrIsAny(ifaddr) || ntcpip_ipaddrCmp(&(netif->ip_addr), ifaddr)))) {
      /* find group or create a new one if not found */
      group = ntcpip__igmpLookupGroup(netif, groupaddr);

      if (group != NULL) {
        /* This should create a new group, check the state to make sure */
        if (group->group_state != NTCPIP__IGMP_GROUP_NON_MEMBER) {
          NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip_igmpJoingroup: join to group not in state NTCPIP__IGMP_GROUP_NON_MEMBER\n"));
        } else {
          /* OK - it was new group */
          NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip_igmpJoingroup: join to new group: "));
          ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, groupaddr);
          NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("\n"));

          /* If first use of the group, allow the group at the MAC level */
          if ((group->use==0) && (netif->igmp_mac_filter != NULL)) {
            NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip_igmpJoingroup: igmp_mac_filter(ADD "));
            ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, groupaddr);
            NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (") on if %p\n", netif));
            netif->igmp_mac_filter(netif, groupaddr, NTCPIP_IGMP_ADD_MAC);
          }

          IGMP_STATS_INC(igmp.join_sent);
          ntcpip__igmpSend(group, NTCPIP__IGMP_V2_MEMB_REPORT);

          ntcpip__igmpStartTimer(group, NTCPIP__IGMP_JOIN_DLY_MEMBER_TMR);

          /* Need to work out where this timer comes from */
          group->group_state = NTCPIP__IGMP_GROUP_DLY_MEMBER;
        }
        /* Increment group use */
        group->use++;
        /* Join on this interface */
        err = NTCPIP_ERR_OK;
      } else {
        /* Return an error even if some network interfaces are joined */
        /** @todo undo any other netif already joined */
        NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip_igmpJoingroup: Not enought memory to join to group\n"));
        return NTCPIP_ERR_MEM;
      }
    }
    /* proceed to next network interface */
    netif = netif->next;
  }

  return err;
}

/**
 * Leave a group on one network interface.
 *
 * @param ifaddr ip address of the network interface which should leave a group
 * @param groupaddr the ip address of the group which to leave
 * @return NTCPIP_ERR_OK if group was left on the netif(s), an ntcpip_Err otherwise
 */
ntcpip_Err
ntcpip__igmpLeavegroup(struct ntcpip_ipAddr *ifaddr, struct ntcpip_ipAddr *groupaddr)
{
  ntcpip_Err              err = NTCPIP_ERR_VAL; /* no matching interface */
  struct ntcpip__igmpGroup *group;
  struct ntcpip__netif      *netif;

  /* make sure it is multicast address */
  NTCPIP__LWIP_ERROR("ntcpip__igmpLeavegroup: attempt to leave non-multicast address", ntcpip_ipaddrIsMulticast(groupaddr), return NTCPIP_ERR_VAL;);
  NTCPIP__LWIP_ERROR("ntcpip__igmpLeavegroup: attempt to leave allsystems address", (!ntcpip_ipaddrCmp(groupaddr, &allsystems)), return NTCPIP_ERR_VAL;);

  /* loop through netif's */
  netif = ntcpip__netifList;
  while (netif != NULL) {
    /* Should we leave this interface ? */
    if ((netif->flags & NTCPIP_NETIF_IGMP) && ((ntcpip_ipaddrIsAny(ifaddr) || ntcpip_ipaddrCmp(&(netif->ip_addr), ifaddr)))) {
      /* find group */
      group = ntcpip__igmpLookforGroup(netif, groupaddr);

      if (group != NULL) {
        /* Only send a leave if the flag is set according to the state diagram */
        NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpLeavegroup: Leaving group: "));
        ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, groupaddr);
        NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("\n"));

        /* If there is no other use of the group */
        if (group->use <= 1) {
          /* If we are the last reporter for this group */
          if (group->last_reporter_flag) {
            NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpLeavegroup: sending leaving group\n"));
            IGMP_STATS_INC(igmp.leave_sent);
            ntcpip__igmpSend(group, NTCPIP__IGMP_LEAVE_GROUP);
          }
          
          /* Disable the group at the MAC level */
          if (netif->igmp_mac_filter != NULL) {
            NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpLeavegroup: igmp_mac_filter(DEL "));
            ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, groupaddr);
            NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (") on if %p\n", netif));
            netif->igmp_mac_filter(netif, groupaddr, NTCPIP_IGMP_DEL_MAC);
          }
          
          NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpLeavegroup: remove group: "));
          ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, groupaddr);
          NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("\n"));          
          
          /* Free the group */
          ntcpip__igmpRemoveGroup(group);
        } else {
          /* Decrement group use */
          group->use--;
        }
        /* Leave on this interface */
        err = NTCPIP_ERR_OK;
      } else {
        /* It's not a fatal error on "leavegroup" */
        NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpLeavegroup: not member of group\n"));
      }
    }
    /* proceed to next network interface */
    netif = netif->next;
  }

  return err;
}

/**
 * The igmp timer function (both for NTCPIP__NO_SYS=1 and =0)
 * Should be called every IGMP_TMR_INTERVAL milliseconds (100 ms is default).
 */
void
ntcpip__igmpTmr(void)
{
  struct ntcpip__igmpGroup *group = igmp_group_list;

  while (group != NULL) {
    if (group->timer != 0) {
      group->timer -= 1;
      if (group->timer == 0) {
        ntcpip__igmpTimeout(group);
      }
    }
    group = group->next;
  }
}

/**
 * Called if a timeout for one group is reached.
 * Sends a report for this group.
 *
 * @param group an ntcpip__igmpGroup for which a timeout is reached
 */
void
ntcpip__igmpTimeout(struct ntcpip__igmpGroup *group)
{
  /* If the state is NTCPIP__IGMP_GROUP_DLY_MEMBER then we send a report for this group */
  if (group->group_state == NTCPIP__IGMP_GROUP_DLY_MEMBER) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpTimeout: report membership for group with address "));
    ntcpip_ipaddrDebugPrint(NTCPIP__IGMP_DEBUG, &(group->group_address));
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, (" on if %p\n", group->interface));

    ntcpip__igmpSend(group, NTCPIP__IGMP_V2_MEMB_REPORT);
  }
}

/**
 * Start a timer for an igmp group
 *
 * @param group the ntcpip__igmpGroup for which to start a timer
 * @param max_time the time in multiples of IGMP_TMR_INTERVAL (decrease with
 *        every call to ntcpip__igmpTmr())
 */
void
ntcpip__igmpStartTimer(struct ntcpip__igmpGroup *group, Uint8 max_time)
{
  /**
   * @todo Important !! this should be random 0 -> max_time. Find out how to do this
   */
  group->timer = max_time;
}

/**
 * Stop a timer for an ntcpip__igmpGroup
 *
 * @param group the ntcpip__igmpGroup for which to stop the timer
 */
void
ntcpip__igmpStopTimer(struct ntcpip__igmpGroup *group)
{
  group->timer = 0;
}

/**
 * Delaying membership report for a group if necessary
 *
 * @param group the ntcpip__igmpGroup for which "delaying" membership report
 * @param maxresp query delay
 */
void
ntcpip__igmpDelayingMember( struct ntcpip__igmpGroup *group, Uint8 maxresp)
{
  if ((group->group_state == NTCPIP__IGMP_GROUP_IDLE_MEMBER) ||
     ((group->group_state == NTCPIP__IGMP_GROUP_DLY_MEMBER) && (maxresp > group->timer))) {
    ntcpip__igmpStartTimer(group, (maxresp)/2);
    group->group_state = NTCPIP__IGMP_GROUP_DLY_MEMBER;
  }
}


/**
 * Sends an IP packet on a network interface. This function constructs the IP header
 * and calculates the IP header checksum. If the source IP address is NULL,
 * the IP address of the outgoing network interface is filled in as source address.
 *
 * @param p the packet to send (p->payload points to the data, e.g. next
            protocol header; if dest == IP_HDRINCL, p already includes an IP
            header and p->payload points to that IP header)
 * @param src the source IP address to send from (if src == NTCPIP_IP_ADDR_ANY, the
 *         IP  address of the netif used to send is used as source address)
 * @param dest the destination IP address to send the packet to
 * @param ttl the TTL value to be set in the IP header
 * @param proto the PROTOCOL to be set in the IP header
 * @param netif the netif on which to send this packet
 * @return NTCPIP_ERR_OK if the packet was sent OK
 *         NTCPIP_ERR_BUF if p doesn't have enough space for IP/LINK headers
 *         returns errors returned by netif->output
 */
ntcpip_Err
ntcpip__igmpIpOutputIf(struct ntcpip_pbuf *p, struct ntcpip_ipAddr *src, struct ntcpip_ipAddr *dest,
                  Uint8 ttl, Uint8 proto, struct ntcpip__netif *netif)
{
  /* This is the "router alert" option */
  Uint16 ra[2];
  ra[0] = ntcpip_htons (NTCPIP__ROUTER_ALERT);
  ra[1] = 0x0000; /* Router shall examine packet */
  return ntcpip__ipOutputIfOpt(p, src, dest, ttl, 0, proto, netif, ra, NTCPIP__ROUTER_ALERTLEN);
}

/**
 * Send an igmp packet to a specific group.
 *
 * @param group the group to which to send the packet
 * @param type the type of igmp packet to send
 */
void
ntcpip__igmpSend(struct ntcpip__igmpGroup *group, Uint8 type)
{
  struct ntcpip_pbuf*     p    = NULL;
  struct ntcpip__igmpMsg* igmp = NULL;
  struct ntcpip_ipAddr   src  = {0};
  struct ntcpip_ipAddr*  dest = NULL;

  /* IP header + "router alert" option + IGMP header */
  p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, NTCPIP__IGMP_MINLEN, NTCPIP_PBUF_RAM);
  
  if (p) {
    igmp = p->payload;
    NTCPIP__LWIP_ASSERT("ntcpip__igmpSend: check that first pbuf can hold struct ntcpip__igmpMsg",
               (p->len >= sizeof(struct ntcpip__igmpMsg)));
    ntcpip_ipaddrSet(&src, &((group->interface)->ip_addr));
     
    if (type == NTCPIP__IGMP_V2_MEMB_REPORT) {
      dest = &(group->group_address);
      IGMP_STATS_INC(igmp.report_sent);
      ntcpip_ipaddrSet(&(igmp->igmp_group_address), &(group->group_address));
      group->last_reporter_flag = 1; /* Remember we were the last to report */
    } else {
      if (type == NTCPIP__IGMP_LEAVE_GROUP) {
        dest = &allrouters;
        ntcpip_ipaddrSet(&(igmp->igmp_group_address), &(group->group_address));
      }
    }

    if ((type == NTCPIP__IGMP_V2_MEMB_REPORT) || (type == NTCPIP__IGMP_LEAVE_GROUP)) {
      igmp->igmp_msgtype  = type;
      igmp->igmp_maxresp  = 0;
      igmp->igmp_checksum = 0;
      igmp->igmp_checksum = ntcpip__inetChksum( igmp, NTCPIP__IGMP_MINLEN);

      ntcpip__igmpIpOutputIf(p, &src, dest, NTCPIP__IGMP_TTL, NTCPIP__IP_PROTO_IGMP, group->interface);
    }

    ntcpip_pbufFree(p);
  } else {
    NTCPIP__LWIP_DEBUGF(NTCPIP__IGMP_DEBUG, ("ntcpip__igmpSend: not enough memory for ntcpip__igmpSend\n"));
  }
}

#endif /* NTCPIP__LWIP_IGMP */

