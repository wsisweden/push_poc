/**
 * @file
 * lwIP network interface abstraction
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#include "../def.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/netif.h"
#include "ntcpip/tcp.h"
#include "ntcpip/snmp.h"
#include "ntcpip/igmp.h"
#include "../../netif/etharp.h"
#if ENABLE_LOOPBACK
#include "ntcpip/sys.h"
#if LWIP_NETIF_LOOPBACK_MULTITHREADING
#include "ntcpip/tcpip.h"
#endif /* LWIP_NETIF_LOOPBACK_MULTITHREADING */
#endif /* ENABLE_LOOPBACK */

#if NTCPIP__LWIP_AUTOIP
#include "../autoip.h"
#endif /* NTCPIP__LWIP_AUTOIP */
#if NTCPIP__LWIP_DHCP
#include "../dhcp.h"
#endif /* NTCPIP__LWIP_DHCP */

#if NTCPIP__LWIP_NETIF_STATUS_CALLBACK
#define NETIF_STATUS_CALLBACK(n) { if (n->status_callback) (n->status_callback)(n); }
#else
#define NETIF_STATUS_CALLBACK(n) { /* NOP */ }
#endif /* NTCPIP__LWIP_NETIF_STATUS_CALLBACK */ 

#if NTCPIP__LWIP_NETIF_LINK_CALLBACK
#define NETIF_LINK_CALLBACK(n) { if (n->link_callback) (n->link_callback)(n); }
#else
#define NETIF_LINK_CALLBACK(n) { /* NOP */ }
#endif /* NTCPIP__LWIP_NETIF_LINK_CALLBACK */ 

struct ntcpip__netif *ntcpip__netifList;
struct ntcpip__netif *ntcpip__netifDefault;

/**
 * Add a network interface to the list of lwIP netifs.
 *
 * @param 	netif 	a pre-allocated netif structure
 * @param 	ipaddr 	IP address for the new netif
 * @param 	netmask network mask for the new netif
 * @param 	gw 		default gateway IP address for the new netif
 * @param 	state 	opaque data passed to the new netif
 * @param 	init 	callback function that initializes the interface
 * @param 	input 	callback function that is called to pass
 * 					ingress packets up in the protocol layer stack.
 *
 * @return 	netif, or NULL if failed.
 */
struct ntcpip__netif * ntcpip__netifAdd(
	struct ntcpip__netif *	netif,
	struct ntcpip_ipAddr *	ipaddr,
	struct ntcpip_ipAddr *	netmask,
	struct ntcpip_ipAddr *	gw,
	void *					state,
	ntcpip_Err (* init)(struct ntcpip__netif *netif),
	ntcpip_Err (* input)(struct ntcpip_pbuf *p, struct ntcpip__netif *netif))
{
  static Uint8 netifnum = 0;

  /* reset new interface configuration state */
  netif->ip_addr.addr = 0;
  netif->netmask.addr = 0;
  netif->gw.addr = 0;
  netif->flags = 0;
#if NTCPIP__LWIP_DHCP
  /* netif not under DHCP control by default */
  netif->dhcp = NULL;
#endif /* NTCPIP__LWIP_DHCP */
#if NTCPIP__LWIP_AUTOIP
  /* netif not under AutoIP control by default */
  netif->autoip = NULL;
#endif /* NTCPIP__LWIP_AUTOIP */
#if NTCPIP__LWIP_NETIF_STATUS_CALLBACK
  netif->status_callback = NULL;
#endif /* NTCPIP__LWIP_NETIF_STATUS_CALLBACK */
#if NTCPIP__LWIP_NETIF_LINK_CALLBACK
  netif->link_callback = NULL;
#endif /* NTCPIP__LWIP_NETIF_LINK_CALLBACK */
#if NTCPIP__LWIP_IGMP
  netif->igmp_mac_filter = NULL;
#endif /* NTCPIP__LWIP_IGMP */
#if ENABLE_LOOPBACK
  netif->loop_first = NULL;
  netif->loop_last = NULL;
#endif /* ENABLE_LOOPBACK */

  /* remember netif specific state information data */
  netif->state = state;
  netif->num = netifnum++;
  netif->input = input;
#if NTCPIP__LWIP_NETIF_HWADDRHINT
  netif->addr_hint = NULL;
#endif /* NTCPIP__LWIP_NETIF_HWADDRHINT*/
#if ENABLE_LOOPBACK && LWIP_LOOPBACK_MAX_PBUFS
  netif->loop_cnt_current = 0;
#endif /* ENABLE_LOOPBACK && LWIP_LOOPBACK_MAX_PBUFS */

  ntcpip__netifSetAddr(netif, ipaddr, netmask, gw);

  /* call user specified initialization function for netif */
  if (init(netif) != NTCPIP_ERR_OK) {
    return NULL;
  }

  /* add this netif to the list */
  netif->next = ntcpip__netifList;
  ntcpip__netifList = netif;
  ntcpip__snmpIncIflist();

#if NTCPIP__LWIP_IGMP
  /* start IGMP processing */
  if (netif->flags & NTCPIP_NETIF_IGMP) {
    ntcpip__igmpStart( netif);
  }
#endif /* NTCPIP__LWIP_IGMP */

  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG, ("netif: added interface %c%c IP addr ",
    netif->name[0], netif->name[1]));
  ntcpip_ipaddrDebugPrint(NTCPIP__NETIF_DEBUG, ipaddr);
  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG, (" netmask "));
  ntcpip_ipaddrDebugPrint(NTCPIP__NETIF_DEBUG, netmask);
  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG, (" gw "));
  ntcpip_ipaddrDebugPrint(NTCPIP__NETIF_DEBUG, gw);
  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG, ("\n"));
  return netif;
}

/**
 * Change IP address configuration for a network interface (including netmask
 * and default gateway).
 *
 * @param netif the network interface to change
 * @param ipaddr the new IP address
 * @param netmask the new netmask
 * @param gw the new default gateway
 */
void
ntcpip__netifSetAddr(struct ntcpip__netif *netif, struct ntcpip_ipAddr *ipaddr, struct ntcpip_ipAddr *netmask,
    struct ntcpip_ipAddr *gw)
{
  ntcpip__netifSetIpaddr(netif, ipaddr);
  ntcpip__netifSetNetmask(netif, netmask);
  ntcpip__netifSetGw(netif, gw);
}

/**
 * Remove a network interface from the list of lwIP netifs.
 *
 * @param netif the network interface to remove
 */
void ntcpip__netifRemove(struct ntcpip__netif * netif)
{
  if ( netif == NULL ) return;

#if NTCPIP__LWIP_IGMP
  /* stop IGMP processing */
  if (netif->flags & NTCPIP_NETIF_IGMP) {
    ntcpip__igmpStop( netif);
  }
#endif /* NTCPIP__LWIP_IGMP */

  ntcpip__snmpDeleteIpaddridxTree(netif);

  /*  is it the first netif? */
  if (ntcpip__netifList == netif) {
    ntcpip__netifList = netif->next;
    ntcpip__snmpDecIflist();
  }
  else {
    /*  look for netif further down the list */
    struct ntcpip__netif * tmpNetif;
    for (tmpNetif = ntcpip__netifList; tmpNetif != NULL; tmpNetif = tmpNetif->next) {
      if (tmpNetif->next == netif) {
        tmpNetif->next = netif->next;
        ntcpip__snmpDecIflist();
        break;
      }
    }
    if (tmpNetif == NULL)
      return; /*  we didn't find any netif today */
  }
  /* this netif is default? */
  if (ntcpip__netifDefault == netif)
    /* reset default netif */
    ntcpip__netifSetDefault(NULL);
  NTCPIP__LWIP_DEBUGF( NTCPIP__NETIF_DEBUG, ("ntcpip__netifRemove: removed netif\n") );
}

/**
 * Find a network interface by searching for its name
 *
 * @param name the name of the netif (like netif->name) plus concatenated number
 * in ascii representation (e.g. 'en0')
 */
struct ntcpip__netif *
ntcpip__netifFind(char *name)
{
  struct ntcpip__netif *netif;
  Uint8 num;

  if (name == NULL) {
    return NULL;
  }

  num = name[2] - '0';

  for(netif = ntcpip__netifList; netif != NULL; netif = netif->next) {
    if (num == netif->num &&
       name[0] == netif->name[0] &&
       name[1] == netif->name[1]) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG, ("ntcpip__netifFind: found %c%c\n", name[0], name[1]));
      return netif;
    }
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG, ("ntcpip__netifFind: didn't find %c%c\n", name[0], name[1]));
  return NULL;
}

/**
 * Change the IP address of a network interface
 *
 * @param netif the network interface to change
 * @param ipaddr the new IP address
 *
 * @note call ntcpip__netifSetAddr() if you also want to change netmask and
 * default gateway
 */
void
ntcpip__netifSetIpaddr(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *ipaddr)
{
  /* TODO: Handling of obsolete pcbs */
  /* See:  http://mail.gnu.org/archive/html/lwip-users/2003-03/msg00118.html */
#if NTCPIP__LWIP_TCP
  struct ntcpip__tcpPcb *pcb;
  struct ntcpip__tcpPcbListen *lpcb;

  /* address is actually being changed? */
  if ((ntcpip_ipaddrCmp(ipaddr, &(netif->ip_addr))) == 0)
  {
    /* extern struct ntcpip__tcpPcb *ntcpip__tcpActivePcbs; defined by tcp.h */
    NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG | NTCPIP__LWIP_DBG_STATE, ("ntcpip__netifSetIpaddr: netif address being changed\n"));
    pcb = ntcpip__tcpActivePcbs;
    while (pcb != NULL) {
      /* PCB bound to current local interface address? */
      if (ntcpip_ipaddrCmp(&(pcb->local_ip), &(netif->ip_addr))) {
        /* this connection must be aborted */
        struct ntcpip__tcpPcb *next = pcb->next;
        NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG | NTCPIP__LWIP_DBG_STATE, ("ntcpip__netifSetIpaddr: aborting TCP pcb %p\n", (void *)pcb));
        ntcpip_tcpAbort(pcb);
        pcb = next;
      } else {
        pcb = pcb->next;
      }
    }
    for (lpcb = ntcpip__tcpListenPcbs.listen_pcbs; lpcb != NULL; lpcb = lpcb->next) {
      /* PCB bound to current local interface address? */
      if ((!(ntcpip_ipaddrIsAny(&(lpcb->local_ip)))) &&
          (ntcpip_ipaddrCmp(&(lpcb->local_ip), &(netif->ip_addr)))) {
        /* The PCB is listening to the old ipaddr and
         * is set to listen to the new one instead */
        ntcpip_ipaddrSet(&(lpcb->local_ip), ipaddr);
      }
    }
  }
#endif
  ntcpip__snmpDeleteIpaddridxTree(netif);
  ntcpip__snmpDeleteIprteidxTree(0,netif);
  /* set new IP address to netif */
  ntcpip_ipaddrSet(&(netif->ip_addr), ipaddr);
  ntcpip__snmpInsertIpaddridxTree(netif);
  ntcpip__snmpInsertIprteidxTree(0,netif);

  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE, ("netif: IP address of interface %c%c set to %"U16_F".%"U16_F".%"U16_F".%"U16_F"\n",
    netif->name[0], netif->name[1],
    ntcpip_ipaddr1(&netif->ip_addr),
    ntcpip_ipaddr2(&netif->ip_addr),
    ntcpip_ipaddr3(&netif->ip_addr),
    ntcpip_ipaddr4(&netif->ip_addr)));
}

/**
 * Change the default gateway for a network interface
 *
 * @param netif the network interface to change
 * @param gw the new default gateway
 *
 * @note call ntcpip__netifSetAddr() if you also want to change ip address and netmask
 */
void
ntcpip__netifSetGw(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *gw)
{
  ntcpip_ipaddrSet(&(netif->gw), gw);
  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE, ("netif: GW address of interface %c%c set to %"U16_F".%"U16_F".%"U16_F".%"U16_F"\n",
    netif->name[0], netif->name[1],
    ntcpip_ipaddr1(&netif->gw),
    ntcpip_ipaddr2(&netif->gw),
    ntcpip_ipaddr3(&netif->gw),
    ntcpip_ipaddr4(&netif->gw)));
}

/**
 * Change the netmask of a network interface
 *
 * @param netif the network interface to change
 * @param netmask the new netmask
 *
 * @note call ntcpip__netifSetAddr() if you also want to change ip address and
 * default gateway
 */
void
ntcpip__netifSetNetmask(struct ntcpip__netif *netif, const struct ntcpip_ipAddr *netmask)
{
  ntcpip__snmpDeleteIprteidxTree(0, netif);
  /* set new netmask to netif */
  ntcpip_ipaddrSet(&(netif->netmask), netmask);
  ntcpip__snmpInsertIprteidxTree(0, netif);
  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG | NTCPIP__LWIP_DBG_TRACE | NTCPIP__LWIP_DBG_STATE, ("netif: netmask of interface %c%c set to %"U16_F".%"U16_F".%"U16_F".%"U16_F"\n",
    netif->name[0], netif->name[1],
    ntcpip_ipaddr1(&netif->netmask),
    ntcpip_ipaddr2(&netif->netmask),
    ntcpip_ipaddr3(&netif->netmask),
    ntcpip_ipaddr4(&netif->netmask)));
}

/**
 * Set a network interface as the default network interface
 * (used to output all packets for which no specific route is found)
 *
 * @param netif the default network interface
 */
void
ntcpip__netifSetDefault(struct ntcpip__netif *netif)
{
  if (netif == NULL)
  {
    /* remove default route */
    ntcpip__snmpDeleteIprteidxTree(1, netif);
  }
  else
  {
    /* install default route */
    ntcpip__snmpInsertIprteidxTree(1, netif);
  }
  ntcpip__netifDefault = netif;
  NTCPIP__LWIP_DEBUGF(NTCPIP__NETIF_DEBUG, ("netif: setting default interface %c%c\n",
           netif ? netif->name[0] : '\'', netif ? netif->name[1] : '\''));
}

/**
 * Bring an interface up, available for processing
 * traffic.
 * 
 * @note: Enabling DHCP on a down interface will make it come
 * up once configured.
 * 
 * @see dhcp_start()
 */ 
void ntcpip__netifSetUp(struct ntcpip__netif *netif)
{
  if ( !(netif->flags & NTCPIP_NETIF_UP )) {
    netif->flags |= NTCPIP_NETIF_UP;
    
#if NTCPIP__LWIP_SNMP
    ntcpip__snmpGetSysuptime(&netif->ts);
#endif /* NTCPIP__LWIP_SNMP */

    NETIF_LINK_CALLBACK(netif);
    NETIF_STATUS_CALLBACK(netif);

#if NTCPIP__LWIP_ARP
    /* For Ethernet network interfaces, we would like to send a "gratuitous ARP" */ 
    if (netif->flags & NTCPIP_NETIF_ETHARP) {
      etharp_gratuitous(netif);
    }
#endif /* NTCPIP__LWIP_ARP */

#if NTCPIP__LWIP_IGMP
    /* resend IGMP memberships */
    if (netif->flags & NTCPIP_NETIF_IGMP) {
      ntcpip__igmpReportGroups( netif);
    }
#endif /* NTCPIP__LWIP_IGMP */
  }
}

/**
 * Bring an interface down, disabling any traffic processing.
 *
 * @note: Enabling DHCP on a down interface will make it come
 * up once configured.
 * 
 * @see dhcp_start()
 */ 
void ntcpip__netifSetDown(struct ntcpip__netif *netif)
{
  if ( netif->flags & NTCPIP_NETIF_UP )
    {
      netif->flags &= ~NTCPIP_NETIF_UP;
#if NTCPIP__LWIP_SNMP
      ntcpip__snmpGetSysuptime(&netif->ts);
#endif
      
      NETIF_LINK_CALLBACK(netif);
      NETIF_STATUS_CALLBACK(netif);
    }
}

/**
 * Ask if an interface is up
 */ 
Uint8 ntcpip__netifIsUp(struct ntcpip__netif *netif)
{
  return (netif->flags & NTCPIP_NETIF_UP)?1:0;
}

#if NTCPIP__LWIP_NETIF_STATUS_CALLBACK
/**
 * Set callback to be called when interface is brought up/down
 */
void netif_set_status_callback(struct ntcpip__netif *netif, void (* status_callback)(struct ntcpip__netif *netif ))
{
    if ( netif )
        netif->status_callback = status_callback;
}
#endif /* NTCPIP__LWIP_NETIF_STATUS_CALLBACK */


/**
 * Called by a driver when its link goes up
 */
void ntcpip__netifSetLinkUp(struct ntcpip__netif *netif )
{
  netif->flags |= NTCPIP_NETIF_LINK_UP;

#if NTCPIP__LWIP_DHCP
  if (netif->dhcp) {
    ntcpip__dhcpNetworkChanged(netif);
  }
#endif /* NTCPIP__LWIP_DHCP */

#if NTCPIP__LWIP_AUTOIP
  if (netif->autoip) {
    ntcpip__autoipNetworkChanged(netif);
  }
#endif /* NTCPIP__LWIP_AUTOIP */

  if (netif->flags & NTCPIP_NETIF_UP) {
#if NTCPIP__LWIP_ARP
  /* For Ethernet network interfaces, we would like to send a "gratuitous ARP" */ 
  if (netif->flags & NTCPIP_NETIF_ETHARP) {
    etharp_gratuitous(netif);
  }
#endif /* NTCPIP__LWIP_ARP */

#if NTCPIP__LWIP_IGMP
    /* resend IGMP memberships */
    if (netif->flags & NTCPIP_NETIF_IGMP) {
      ntcpip__igmpReportGroups( netif);
    }
#endif /* NTCPIP__LWIP_IGMP */
  }
  NETIF_LINK_CALLBACK(netif);
}

/**
 * Called by a driver when its link goes down
 */
void ntcpip__netifSetLinkDown(struct ntcpip__netif *netif )
{
  netif->flags &= ~NTCPIP_NETIF_LINK_UP;
  NETIF_LINK_CALLBACK(netif);
}

/**
 * Ask if a link is up
 */ 
Uint8 ntcpip__netifIsLinkUp(struct ntcpip__netif *netif)
{
  return (netif->flags & NTCPIP_NETIF_LINK_UP) ? 1 : 0;
}

#if NTCPIP__LWIP_NETIF_LINK_CALLBACK
/**
 * Set callback to be called when link is brought up/down
 */
void ntcpip__netifSetLinkCallback(struct ntcpip__netif *netif, void (* link_callback)(struct ntcpip__netif *netif ))
{
  if (netif) {
    netif->link_callback = link_callback;
  }
}
#endif /* NTCPIP__LWIP_NETIF_LINK_CALLBACK */

#if ENABLE_LOOPBACK
/**
 * Send an IP packet to be received on the same netif (loopif-like).
 * The pbuf is simply copied and handed back to netif->input.
 * In multithreaded mode, this is done directly since netif->input must put
 * the packet on a queue.
 * In callback mode, the packet is put on an internal queue and is fed to
 * netif->input by netif_poll().
 *
 * @param netif the lwip network interface structure
 * @param p the (IP) packet to 'send'
 * @param ipaddr the ip address to send the packet to (not used)
 * @return NTCPIP_ERR_OK if the packet has been sent
 *         NTCPIP_ERR_MEM if the pbuf used to copy the packet couldn't be allocated
 */
ntcpip_Err
netif_loop_output(struct ntcpip__netif *netif, struct ntcpip_pbuf *p,
       struct ntcpip_ipAddr *ipaddr)
{
  struct ntcpip_pbuf *r;
  ntcpip_Err err;
  struct ntcpip_pbuf *last;
#if LWIP_LOOPBACK_MAX_PBUFS
  Uint8 clen = 0;
#endif /* LWIP_LOOPBACK_MAX_PBUFS */
  NTCPIP__SYS_ARCH_DECL_PROTECT(lev);
  NTCPIP__LWIP_UNUSED_ARG(ipaddr);

  /* Allocate a new pbuf */
  r = ntcpip_pbufAlloc(NTCPIP_PBUF_LINK, p->tot_len, NTCPIP_PBUF_RAM);
  if (r == NULL) {
    return NTCPIP_ERR_MEM;
  }
#if LWIP_LOOPBACK_MAX_PBUFS
  clen = ntcpip__pbufClen(r);
  /* check for overflow or too many pbuf on queue */
  if(((netif->loop_cnt_current + clen) < netif->loop_cnt_current) ||
    ((netif->loop_cnt_current + clen) > LWIP_LOOPBACK_MAX_PBUFS)) {
      ntcpip_pbufFree(r);
      r = NULL;
      return NTCPIP_ERR_MEM;
  }
  netif->loop_cnt_current += clen;
#endif /* LWIP_LOOPBACK_MAX_PBUFS */

  /* Copy the whole pbuf queue p into the single pbuf r */
  if ((err = ntcpip_pbufCopy(r, p)) != NTCPIP_ERR_OK) {
    ntcpip_pbufFree(r);
    r = NULL;
    return err;
  }

  /* Put the packet on a linked list which gets emptied through calling
     netif_poll(). */

  /* let last point to the last pbuf in chain r */
  for (last = r; last->next != NULL; last = last->next);

  NTCPIP__SYS_ARCH_PROTECT(lev);
  if(netif->loop_first != NULL) {
    NTCPIP__LWIP_ASSERT("if first != NULL, last must also be != NULL", netif->loop_last != NULL);
    netif->loop_last->next = r;
    netif->loop_last = last;
  } else {
    netif->loop_first = r;
    netif->loop_last = last;
  }
  NTCPIP__SYS_ARCH_UNPROTECT(lev);

#if LWIP_NETIF_LOOPBACK_MULTITHREADING
  /* For multithreading environment, schedule a call to netif_poll */
  ntcpip__tcpipCb((void (*)(void *))(netif_poll), netif);
#endif /* LWIP_NETIF_LOOPBACK_MULTITHREADING */

  return NTCPIP_ERR_OK;
}

/**
 * Call netif_poll() in the main loop of your application. This is to prevent
 * reentering non-reentrant functions like ntcpip__tcpInput(). Packets passed to
 * netif_loop_output() are put on a list that is passed to netif->input() by
 * netif_poll().
 */
void
netif_poll(struct ntcpip__netif *netif)
{
  struct ntcpip_pbuf *in;
  NTCPIP__SYS_ARCH_DECL_PROTECT(lev);

  do {
    /* Get a packet from the list. With NTCPIP__SYS_LIGHTWEIGHT_PROT=1, this is protected */
    NTCPIP__SYS_ARCH_PROTECT(lev);
    in = netif->loop_first;
    if(in != NULL) {
      struct ntcpip_pbuf *in_end = in;
#if LWIP_LOOPBACK_MAX_PBUFS
      Uint8 clen = ntcpip__pbufClen(in);
      /* adjust the number of pbufs on queue */
      NTCPIP__LWIP_ASSERT("netif->loop_cnt_current underflow",
        ((netif->loop_cnt_current - clen) < netif->loop_cnt_current));
      netif->loop_cnt_current -= clen;
#endif /* LWIP_LOOPBACK_MAX_PBUFS */
      while(in_end->len != in_end->tot_len) {
        NTCPIP__LWIP_ASSERT("bogus pbuf: len != tot_len but next == NULL!", in_end->next != NULL);
        in_end = in_end->next;
      }
      /* 'in_end' now points to the last pbuf from 'in' */
      if(in_end == netif->loop_last) {
        /* this was the last pbuf in the list */
        netif->loop_first = netif->loop_last = NULL;
      } else {
        /* pop the pbuf off the list */
        netif->loop_first = in_end->next;
        NTCPIP__LWIP_ASSERT("should not be null since first != last!", netif->loop_first != NULL);
      }
      /* De-queue the pbuf from its successors on the 'loop_' list. */
      in_end->next = NULL;
    }
    NTCPIP__SYS_ARCH_UNPROTECT(lev);

    if(in != NULL) {
      /* loopback packets are always IP packets! */
      if(ntcpip__ipInput(in, netif) != NTCPIP_ERR_OK) {
        ntcpip_pbufFree(in);
      }
      /* Don't reference the packet any more! */
      in = NULL;
    }
  /* go on while there is a packet on the list */
  } while(netif->loop_first != NULL);
}

#if !LWIP_NETIF_LOOPBACK_MULTITHREADING
/**
 * Calls netif_poll() for every netif on the ntcpip__netifList.
 */
void
netif_poll_all(void)
{
  struct ntcpip__netif *netif = ntcpip__netifList;
  /* loop through netifs */
  while (netif != NULL) {
    netif_poll(netif);
    /* proceed to next network interface */
    netif = netif->next;
  }
}
#endif /* !LWIP_NETIF_LOOPBACK_MULTITHREADING */
#endif /* ENABLE_LOOPBACK */


