/**
 * @file
 * Transmission Control Protocol for IP
 *
 * This file contains common functions for the TCP implementation, such as functinos
 * for manipulating the data structures and the TCP timer functions. TCP functions
 * related to input and output is found in tcp_in.c and tcp_out.c respectively.
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_TCP /* don't build if not configured for use in lwipopts.h */

#include "../def.h"
#include "../mem.h"
#include "../memp.h"
#include "ntcpip/snmp.h"
#include "ntcpip/tcp.h"
#include "ntcpip/debug.h"
#include "../stats.h"

#include <string.h>

const char * const_P ntcpip__tcpStateStr[] = {
  "NTCPIP_CLOSED",      
  "NTCPIP_LISTEN",      
  "NTCPIP_SYN_SENT",    
  "NTCPIP_SYN_RCVD",    
  "NTCPIP_ESTABLISHED", 
  "NTCPIP_FIN_WAIT_1",  
  "NTCPIP_FIN_WAIT_2",  
  "NTCPIP_CLOSE_WAIT",  
  "CLOSING",     
  "NTCPIP_LAST_ACK",    
  "NTCPIP_TIME_WAIT"   
};

/* Incremented every coarse grained timer shot (typically every 500 ms). */
Uint32 ntcpip__tcpTicks;
const Uint8 ntcpip__tcpBackoff[13] =
    { 1, 2, 3, 4, 5, 6, 7, 7, 7, 7, 7, 7, 7};
 /* Times per slowtmr hits */
const Uint8 ntcpip__tcpPresistBackoff[7] = { 3, 6, 12, 24, 48, 96, 120 };

/* The TCP PCB lists. */

/** List of all TCP PCBs bound but not yet (connected || listening) */
struct ntcpip__tcpPcb *ntcpip__tcpBoundPcb;  
/** List of all TCP PCBs in NTCPIP_LISTEN state */
union ntcpip__tcpListenPcbst ntcpip__tcpListenPcbs;
/** List of all TCP PCBs that are in a state in which
 * they accept or send data. */
struct ntcpip__tcpPcb *ntcpip__tcpActivePcbs;  
/** List of all TCP PCBs in TIME-WAIT state */
struct ntcpip__tcpPcb *ntcpip__tcpTwPcb;

struct ntcpip__tcpPcb *ntcpip__tcpTmpPcb;

static Uint8 tcp_timer;
static Uint16 tcp_new_port(void);

/**
 * Called periodically to dispatch TCP timers.
 *
 */
void
ntcpip__tcpTmr(void)
{
  /* Call ntcpip__tcpFastTmr() every 250 ms */
  ntcpip__tcpFastTmr();

  if (++tcp_timer & 1) {
    /* Call ntcpip__tcpTmr() every 500 ms, i.e., every other timer
       ntcpip__tcpTmr() is called. */
    ntcpip__tcpSlowTmr();
  }
}

/**
 * Closes the connection held by the PCB.
 *
 * Listening pcbs are freed and may not be referenced any more.
 * Connection pcbs are freed if not yet connected and may not be referenced
 * any more. If a connection is established (at least SYN received or in
 * a closing state), the connection is closed, and put in a closing state.
 * The pcb is then automatically freed in ntcpip__tcpSlowTmr(). It is therefore
 * unsafe to reference it.
 *
 * @param pcb the tcp_pcb to close
 * @return NTCPIP_ERR_OK if connection has been closed
 *         another ntcpip_Err if closing failed and pcb is not freed
 */
ntcpip_Err
ntcpip_tcpClose(struct ntcpip__tcpPcb *pcb)
{
  ntcpip_Err err;

#if NTCPIP__TCP_DEBUG
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip_tcpClose: closing in "));
  tcp_debug_print_state(pcb->state);
#endif /* NTCPIP__TCP_DEBUG */

  switch (pcb->state) {
  case NTCPIP_CLOSED:
    /* Closing a pcb in the NTCPIP_CLOSED state might seem erroneous,
     * however, it is in this state once allocated and as yet unused
     * and the user needs some way to free it should the need arise.
     * Calling ntcpip_tcpClose() with a pcb that has already been closed, (i.e. twice)
     * or for a pcb that has been used and then entered the NTCPIP_CLOSED state 
     * is erroneous, but this should never happen as the pcb has in those cases
     * been freed, and so any remaining handles are bogus. */
    err = NTCPIP_ERR_OK;
    NTCPIP__TCP_RMV(&ntcpip__tcpBoundPcb, pcb);
    ntcpip__mempFree(MEMP_TCP_PCB, pcb);
    pcb = NULL;
    break;
  case NTCPIP_LISTEN:
    err = NTCPIP_ERR_OK;
    ntcpip__tcpPcbRemove((struct ntcpip__tcpPcb **)&ntcpip__tcpListenPcbs.pcbs, pcb);
    ntcpip__mempFree(MEMP_TCP_PCB_LISTEN, pcb);
    pcb = NULL;
    break;
  case NTCPIP_SYN_SENT:
    err = NTCPIP_ERR_OK;
    ntcpip__tcpPcbRemove(&ntcpip__tcpActivePcbs, pcb);
    ntcpip__mempFree(MEMP_TCP_PCB, pcb);
    pcb = NULL;
    ntcpip__snmpIncTcpattemptfails();
    break;
  case NTCPIP_SYN_RCVD:
    err = ntcpip__tcpSendCtrl(pcb, NTCPIP__TCP_FIN);
    if (err == NTCPIP_ERR_OK) {
      ntcpip__snmpIncTcpattemptfails();
      pcb->state = NTCPIP_FIN_WAIT_1;
    }
    break;
  case NTCPIP_ESTABLISHED:
    err = ntcpip__tcpSendCtrl(pcb, NTCPIP__TCP_FIN);
    if (err == NTCPIP_ERR_OK) {
      ntcpip__snmpIncTcpestabresets();
      pcb->state = NTCPIP_FIN_WAIT_1;
    }
    break;
  case NTCPIP_CLOSE_WAIT:
    err = ntcpip__tcpSendCtrl(pcb, NTCPIP__TCP_FIN);
    if (err == NTCPIP_ERR_OK) {
      ntcpip__snmpIncTcpestabresets();
      pcb->state = NTCPIP_LAST_ACK;
    }
    break;
  default:
    /* Has already been closed, do nothing. */
    err = NTCPIP_ERR_OK;
    pcb = NULL;
    break;
  }

  if (pcb != NULL && err == NTCPIP_ERR_OK) {
    /* To ensure all data has been sent when ntcpip_tcpClose returns, we have
       to make sure ntcpip__tcpOutput doesn't fail.
       Since we don't really have to ensure all data has been sent when ntcpip_tcpClose
       returns (unsent data is sent from tcp timer functions, also), we don't care
       for the return value of ntcpip__tcpOutput for now. */
    /* @todo: When implementing SO_LINGER, this must be changed somehow:
       If SOF_LINGER is set, the data should be sent when ntcpip_tcpClose returns. */
    ntcpip__tcpOutput(pcb);
  }
  return err;
}

/**
 * Abandons a connection and optionally sends a RST to the remote
 * host.  Deletes the local protocol control block. This is done when
 * a connection is killed because of shortage of memory.
 *
 * @param pcb the tcp_pcb to abort
 * @param reset boolean to indicate whether a reset should be sent
 */
void
ntcpip__tcpAbandon(struct ntcpip__tcpPcb *pcb, int reset)
{
  Uint32 seqno, ackno;
  Uint16 remote_port, local_port;
  struct ntcpip_ipAddr remote_ip, local_ip;
#if NTCPIP__LWIP_CALLBACK_API  
  void (* errf)(void *arg, ntcpip_Err err);
#endif /* NTCPIP__LWIP_CALLBACK_API */
  void *errf_arg;

  
  /* Figure out on which TCP PCB list we are, and remove us. If we
     are in an active state, call the receive function associated with
     the PCB with a NULL argument, and send an RST to the remote end. */
  if (pcb->state == NTCPIP_TIME_WAIT) {
    ntcpip__tcpPcbRemove(&ntcpip__tcpTwPcb, pcb);
    ntcpip__mempFree(MEMP_TCP_PCB, pcb);
  } else {
    seqno = pcb->snd_nxt;
    ackno = pcb->rcv_nxt;
    ntcpip_ipaddrSet(&local_ip, &(pcb->local_ip));
    ntcpip_ipaddrSet(&remote_ip, &(pcb->remote_ip));
    local_port = pcb->local_port;
    remote_port = pcb->remote_port;
#if NTCPIP__LWIP_CALLBACK_API
    errf = pcb->errf;
#endif /* NTCPIP__LWIP_CALLBACK_API */
    errf_arg = pcb->callback_arg;
    ntcpip__tcpPcbRemove(&ntcpip__tcpActivePcbs, pcb);
    if (pcb->unacked != NULL) {
      ntcpip__tcpSegsFree(pcb->unacked);
    }
    if (pcb->unsent != NULL) {
      ntcpip__tcpSegsFree(pcb->unsent);
    }
#if NTCPIP__TCP_QUEUE_OOSEQ    
    if (pcb->ooseq != NULL) {
      ntcpip__tcpSegsFree(pcb->ooseq);
    }
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */
    ntcpip__mempFree(MEMP_TCP_PCB, pcb);
    NTCPIP__TCP_EVENT_ERR(errf, errf_arg, NTCPIP_ERR_ABRT);
    if (reset) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RST_DEBUG, ("ntcpip__tcpAbandon: sending RST\n"));
      ntcpip__tcpRst(seqno, ackno, &local_ip, &remote_ip, local_port, remote_port);
    }
  }
}

/**
 * Binds the connection to a local portnumber and IP address. If the
 * IP address is not given (i.e., ipaddr == NULL), the IP address of
 * the outgoing network interface is used instead.
 *
 * @param pcb the tcp_pcb to bind (no check is done whether this pcb is
 *        already bound!)
 * @param ipaddr the local ip address to bind to (use NTCPIP_IP_ADDR_ANY to bind
 *        to any local address
 * @param port the local port to bind to
 * @return NTCPIP_ERR_USE if the port is already in use
 *         NTCPIP_ERR_OK if bound
 */
ntcpip_Err
ntcpip_tcpBind(struct ntcpip__tcpPcb *pcb, const struct ntcpip_ipAddr *ipaddr, Uint16 port)
{
  struct ntcpip__tcpPcb *cpcb;

  NTCPIP__LWIP_ERROR("ntcpip_tcpBind: can only bind in state NTCPIP_CLOSED", pcb->state == NTCPIP_CLOSED, return NTCPIP_ERR_ISCONN);

  if (port == 0) {
    port = tcp_new_port();
  }
  /* Check if the address already is in use. */
  /* Check the listen pcbs. */
  for(cpcb = (struct ntcpip__tcpPcb *)ntcpip__tcpListenPcbs.pcbs;
      cpcb != NULL; cpcb = cpcb->next) {
    if (cpcb->local_port == port) {
      if (ntcpip_ipaddrIsAny(&(cpcb->local_ip)) ||
          ntcpip_ipaddrIsAny(ipaddr) ||
          ntcpip_ipaddrCmp(&(cpcb->local_ip), ipaddr)) {
        return NTCPIP_ERR_USE;
      }
    }
  }
  /* Check the connected pcbs. */
  for(cpcb = ntcpip__tcpActivePcbs;
      cpcb != NULL; cpcb = cpcb->next) {
    if (cpcb->local_port == port) {
      if (ntcpip_ipaddrIsAny(&(cpcb->local_ip)) ||
          ntcpip_ipaddrIsAny(ipaddr) ||
          ntcpip_ipaddrCmp(&(cpcb->local_ip), ipaddr)) {
        return NTCPIP_ERR_USE;
      }
    }
  }
  /* Check the bound, not yet connected pcbs. */
  for(cpcb = ntcpip__tcpBoundPcb; cpcb != NULL; cpcb = cpcb->next) {
    if (cpcb->local_port == port) {
      if (ntcpip_ipaddrIsAny(&(cpcb->local_ip)) ||
          ntcpip_ipaddrIsAny(ipaddr) ||
          ntcpip_ipaddrCmp(&(cpcb->local_ip), ipaddr)) {
        return NTCPIP_ERR_USE;
      }
    }
  }
  /* @todo: until SO_REUSEADDR is implemented (see task #6995 on savannah),
   * we have to check the pcbs in TIME-WAIT state, also: */
  for(cpcb = ntcpip__tcpTwPcb; cpcb != NULL; cpcb = cpcb->next) {
    if (cpcb->local_port == port) {
      if (ntcpip_ipaddrCmp(&(cpcb->local_ip), ipaddr)) {
        return NTCPIP_ERR_USE;
      }
    }
  }

  if (!ntcpip_ipaddrIsAny(ipaddr)) {
    pcb->local_ip = *ipaddr;
  }
  pcb->local_port = port;
  NTCPIP__TCP_REG(&ntcpip__tcpBoundPcb, pcb);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip_tcpBind: bind to port %"U16_F"\n", port));
  return NTCPIP_ERR_OK;
}
#if NTCPIP__LWIP_CALLBACK_API
/**
 * Default accept callback if no accept callback is specified by the user.
 */
static ntcpip_Err
tcp_accept_null(void *arg, struct ntcpip__tcpPcb *pcb, ntcpip_Err err)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_UNUSED_ARG(pcb);
  NTCPIP__LWIP_UNUSED_ARG(err);

  return NTCPIP_ERR_ABRT;
}
#endif /* NTCPIP__LWIP_CALLBACK_API */

/**
 * Set the state of the connection to be NTCPIP_LISTEN, which means that it
 * is able to accept incoming connections. The protocol control block
 * is reallocated in order to consume less memory. Setting the
 * connection to NTCPIP_LISTEN is an irreversible process.
 *
 * @param pcb the original tcp_pcb
 * @param backlog the incoming connections queue limit
 * @return tcp_pcb used for listening, consumes less memory.
 *
 * @note The original tcp_pcb is freed. This function therefore has to be
 *       called like this:
 *             tpcb = tcp_listen(tpcb);
 */
struct ntcpip__tcpPcb *
ntcpip_tcpListenWithBacklog(struct ntcpip__tcpPcb *pcb, Uint8 backlog)
{
  struct ntcpip__tcpPcbListen *lpcb;

  NTCPIP__LWIP_UNUSED_ARG(backlog);
  NTCPIP__LWIP_ERROR("tcp_listen: pcb already connected", pcb->state == NTCPIP_CLOSED, return NULL);

  /* already listening? */
  if (pcb->state == NTCPIP_LISTEN) {
    return pcb;
  }
  lpcb = ntcpip__mempMalloc(MEMP_TCP_PCB_LISTEN);
  if (lpcb == NULL) {
    return NULL;
  }
  lpcb->callback_arg = pcb->callback_arg;
  lpcb->local_port = pcb->local_port;
  lpcb->state = NTCPIP_LISTEN;
  lpcb->so_options = pcb->so_options;
  lpcb->so_options |= NTCPIP_SOF_ACCEPTCONN;
  lpcb->ttl = pcb->ttl;
  lpcb->tos = pcb->tos;
  ntcpip_ipaddrSet(&lpcb->local_ip, &pcb->local_ip);
  NTCPIP__TCP_RMV(&ntcpip__tcpBoundPcb, pcb);
  ntcpip__mempFree(MEMP_TCP_PCB, pcb);
#if NTCPIP__LWIP_CALLBACK_API
  lpcb->accept = tcp_accept_null;
#endif /* NTCPIP__LWIP_CALLBACK_API */
#if NTCPIP__TCP_LISTEN_BACKLOG
  lpcb->accepts_pending = 0;
  lpcb->backlog = (backlog ? backlog : 1);
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
  NTCPIP__TCP_REG(&ntcpip__tcpListenPcbs.listen_pcbs, lpcb);
  return (struct ntcpip__tcpPcb *)lpcb;
}

/** 
 * Update the state that tracks the available window space to advertise.
 *
 * Returns how much extra window would be advertised if we sent an
 * update now.
 */
Uint32 ntcpip__tcpUpdateRcvAnnWnd(struct ntcpip__tcpPcb *pcb)
{
  Uint32 new_right_edge = pcb->rcv_nxt + pcb->rcv_wnd;

  if (NTCPIP__TCP_SEQ_GEQ(new_right_edge, pcb->rcv_ann_right_edge + LWIP_MIN((NTCPIP__TCP_WND / 2), pcb->mss))) {
    /* we can advertise more window */
    pcb->rcv_ann_wnd = pcb->rcv_wnd;
    return new_right_edge - pcb->rcv_ann_right_edge;
  } else {
    if (NTCPIP__TCP_SEQ_GT(pcb->rcv_nxt, pcb->rcv_ann_right_edge)) {
      /* Can happen due to other end sending out of advertised window,
       * but within actual available (but not yet advertised) window */
      pcb->rcv_ann_wnd = 0;
    } else {
      /* keep the right edge of window constant */
      pcb->rcv_ann_wnd = (Uint16) (pcb->rcv_ann_right_edge - pcb->rcv_nxt);
    }
    return 0;
  }
}

/**
 * This function should be called by the application when it has
 * processed the data. The purpose is to advertise a larger window
 * when the data has been processed.
 *
 * @param pcb the tcp_pcb for which data is read
 * @param len the amount of bytes that have been read by the application
 */
void
ntcpip_tcpRecved(struct ntcpip__tcpPcb *pcb, Uint16 len)
{
  int wnd_inflation;

  NTCPIP__LWIP_ASSERT("ntcpip_tcpRecved: len would wrap rcv_wnd\n",
              len <= 0xffff - pcb->rcv_wnd );

  pcb->rcv_wnd += len;
  if (pcb->rcv_wnd > NTCPIP__TCP_WND)
    pcb->rcv_wnd = NTCPIP__TCP_WND;

  wnd_inflation = ntcpip__tcpUpdateRcvAnnWnd(pcb);

  /* If the change in the right edge of window is significant (default
   * watermark is NTCPIP__TCP_WND/2), then send an explicit update now.
   * Otherwise wait for a packet to be sent in the normal course of
   * events (or more window to be available later) */
  if (wnd_inflation >= TCP_WND_UPDATE_THRESHOLD) 
    ntcpip__tcpAckNow(pcb);

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip_tcpRecved: recveived %"U16_F" bytes, wnd %"U16_F" (%"U16_F").\n",
         len, pcb->rcv_wnd, NTCPIP__TCP_WND - pcb->rcv_wnd));
}

/**
 * A nastly hack featuring 'goto' statements that allocates a
 * new TCP local port.
 *
 * @return a new (free) local TCP port number
 */
static Uint16
tcp_new_port(void)
{
  struct ntcpip__tcpPcb *pcb;
#ifndef TCP_LOCAL_PORT_RANGE_START
#define TCP_LOCAL_PORT_RANGE_START 4096
#define TCP_LOCAL_PORT_RANGE_END   0x7fff
#endif
  static Uint16 port = TCP_LOCAL_PORT_RANGE_START;
  
 again:
  if (++port > TCP_LOCAL_PORT_RANGE_END) {
    port = TCP_LOCAL_PORT_RANGE_START;
  }
  
  for(pcb = ntcpip__tcpActivePcbs; pcb != NULL; pcb = pcb->next) {
    if (pcb->local_port == port) {
      goto again;
    }
  }
  for(pcb = ntcpip__tcpTwPcb; pcb != NULL; pcb = pcb->next) {
    if (pcb->local_port == port) {
      goto again;
    }
  }
  for(pcb = (struct ntcpip__tcpPcb *)ntcpip__tcpListenPcbs.pcbs; pcb != NULL; pcb = pcb->next) {
    if (pcb->local_port == port) {
      goto again;
    }
  }
  return port;
}

/**
 * Connects to another host. The function given as the "connected"
 * argument will be called when the connection has been established.
 *
 * @param pcb the tcp_pcb used to establish the connection
 * @param ipaddr the remote ip address to connect to
 * @param port the remote tcp port to connect to
 * @param connected callback function to call when connected (or on error)
 * @return NTCPIP_ERR_VAL if invalid arguments are given
 *         NTCPIP_ERR_OK if connect request has been sent
 *         other ntcpip_Err values if connect request couldn't be sent
 */
ntcpip_Err
ntcpip_tcpConnect(struct ntcpip__tcpPcb *pcb, const struct ntcpip_ipAddr *ipaddr, Uint16 port,
      ntcpip_Err (* connected)(void *arg, struct ntcpip__tcpPcb *tpcb, ntcpip_Err err))
{
  ntcpip_Err ret;
  Uint32 iss;

  NTCPIP__LWIP_ERROR("ntcpip_tcpConnect: can only connected from state NTCPIP_CLOSED", pcb->state == NTCPIP_CLOSED, return NTCPIP_ERR_ISCONN);

  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip_tcpConnect to port %"U16_F"\n", port));
  if (ipaddr != NULL) {
    pcb->remote_ip = *ipaddr;
  } else {
    return NTCPIP_ERR_VAL;
  }
  pcb->remote_port = port;
  if (pcb->local_port == 0) {
    pcb->local_port = tcp_new_port();
  }
  iss = ntcpip__tcpNextIss();
  pcb->rcv_nxt = 0;
  pcb->snd_nxt = iss;
  pcb->lastack = iss - 1;
  pcb->snd_lbb = iss - 1;
  pcb->rcv_wnd = NTCPIP__TCP_WND;
  pcb->rcv_ann_wnd = NTCPIP__TCP_WND;
  pcb->rcv_ann_right_edge = pcb->rcv_nxt;
  pcb->snd_wnd = NTCPIP__TCP_WND;
  /* As initial send MSS, we use NTCPIP__TCP_MSS but limit it to 536.
     The send MSS is updated when an MSS option is received. */
  pcb->mss = (NTCPIP__TCP_MSS > 536) ? 536 : NTCPIP__TCP_MSS;
#if NTCPIP__TCP_CALCULATE_EFF_SEND_MSS
  pcb->mss = ntcpip__tcpEffSendMss(pcb->mss, ipaddr);
#endif /* NTCPIP__TCP_CALCULATE_EFF_SEND_MSS */
  pcb->cwnd = 1;
  pcb->ssthresh = pcb->mss * 10;
  pcb->state = NTCPIP_SYN_SENT;
#if NTCPIP__LWIP_CALLBACK_API  
  pcb->connected = connected;
#endif /* NTCPIP__LWIP_CALLBACK_API */
  NTCPIP__TCP_RMV(&ntcpip__tcpBoundPcb, pcb);
  NTCPIP__TCP_REG(&ntcpip__tcpActivePcbs, pcb);

  ntcpip__snmpIncTcpactiveopens();
  
  ret = ntcpip__tcpEnqueue(pcb, NULL, 0, NTCPIP__TCP_SYN, 0, TF_SEG_OPTS_MSS
#if LWIP_TCP_TIMESTAMPS
                    | TF_SEG_OPTS_TS
#endif
                    );
  if (ret == NTCPIP_ERR_OK) { 
    ntcpip__tcpOutput(pcb);
  }
  return ret;
} 

/**
 * Called every 500 ms and implements the retransmission timer and the timer that
 * removes PCBs that have been in TIME-WAIT for enough time. It also increments
 * various timers such as the inactivity timer in each PCB.
 *
 * Automatically called from ntcpip__tcpTmr().
 */
void
ntcpip__tcpSlowTmr(void)
{
  struct ntcpip__tcpPcb *pcb, *pcb2, *prev;
  Uint16 eff_wnd;
  Uint8 pcb_remove;      /* flag if a PCB should be removed */
  Uint8 pcb_reset;       /* flag if a RST should be sent when removing */
  ntcpip_Err err;

  err = NTCPIP_ERR_OK;

  ++ntcpip__tcpTicks;

  /* Steps through all of the active PCBs. */
  prev = NULL;
  pcb = ntcpip__tcpActivePcbs;
  if (pcb == NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: no active pcbs\n"));
  }
  while (pcb != NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: processing active pcb\n"));
    NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: active pcb->state != NTCPIP_CLOSED\n", pcb->state != NTCPIP_CLOSED);
    NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: active pcb->state != NTCPIP_LISTEN\n", pcb->state != NTCPIP_LISTEN);
    NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: active pcb->state != TIME-WAIT\n", pcb->state != NTCPIP_TIME_WAIT);

    pcb_remove = 0;
    pcb_reset = 0;

    if (pcb->state == NTCPIP_SYN_SENT && pcb->nrtx == NTCPIP__TCP_SYNMAXRTX) {
      ++pcb_remove;
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: max SYN retries reached\n"));
    }
    else if (pcb->nrtx == NTCPIP__TCP_MAXRTX) {
      ++pcb_remove;
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: max DATA retries reached\n"));
    } else {
      if (pcb->persist_backoff > 0) {
        /* If snd_wnd is zero, use persist timer to send 1 byte probes
         * instead of using the standard retransmission mechanism. */
        pcb->persist_cnt++;
        if (pcb->persist_cnt >= ntcpip__tcpPresistBackoff[pcb->persist_backoff-1]) {
          pcb->persist_cnt = 0;
          if (pcb->persist_backoff < sizeof(ntcpip__tcpPresistBackoff)) {
            pcb->persist_backoff++;
          }
          ntcpip__tcpZeroWindowProbe(pcb);
        }
      } else {
        /* Increase the retransmission timer if it is running */
        if(pcb->rtime >= 0)
          ++pcb->rtime;

        if (pcb->unacked != NULL && pcb->rtime >= pcb->rto) {
          /* Time for a retransmission. */
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_RTO_DEBUG, ("ntcpip__tcpSlowTmr: rtime %"S16_F
                                      " pcb->rto %"S16_F"\n",
                                      pcb->rtime, pcb->rto));

          /* Double retransmission time-out unless we are trying to
           * connect to somebody (i.e., we are in NTCPIP_SYN_SENT). */
          if (pcb->state != NTCPIP_SYN_SENT) {
            pcb->rto = ((pcb->sa >> 3) + pcb->sv) << ntcpip__tcpBackoff[pcb->nrtx];
          }

          /* Reset the retransmission timer. */
          pcb->rtime = 0;

          /* Reduce congestion window and ssthresh. */
          eff_wnd = LWIP_MIN(pcb->cwnd, pcb->snd_wnd);
          pcb->ssthresh = eff_wnd >> 1;
          if (pcb->ssthresh < pcb->mss) {
            pcb->ssthresh = pcb->mss * 2;
          }
          pcb->cwnd = pcb->mss;
          NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_CWND_DEBUG, ("ntcpip__tcpSlowTmr: cwnd %"U16_F
                                       " ssthresh %"U16_F"\n",
                                       pcb->cwnd, pcb->ssthresh));
 
          /* The following needs to be called AFTER cwnd is set to one
             mss - STJ */
          ntcpip__tcpRexmitRto(pcb);
        }
      }
    }
    /* Check if this PCB has stayed too long in FIN-WAIT-2 */
    if (pcb->state == NTCPIP_FIN_WAIT_2) {
      if ((Uint32)(ntcpip__tcpTicks - pcb->tmr) >
          NTCPIP__TCP_FIN_WAIT_TIMEOUT / NTCPIP__TCP_SLOW_INTERVAL) {
        ++pcb_remove;
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: removing pcb stuck in FIN-WAIT-2\n"));
      }
    }

    /* Check if KEEPALIVE should be sent */
    if((pcb->so_options & NTCPIP_SOF_KEEPALIVE) && 
       ((pcb->state == NTCPIP_ESTABLISHED) || 
        (pcb->state == NTCPIP_CLOSE_WAIT))) {
#if NTCPIP__LWIP_TCP_KEEPALIVE
      if((Uint32)(ntcpip__tcpTicks - pcb->tmr) > 
         (pcb->keep_idle + (pcb->keep_cnt*pcb->keep_intvl))
         / NTCPIP__TCP_SLOW_INTERVAL)
#else      
      if((Uint32)(ntcpip__tcpTicks - pcb->tmr) > 
         (pcb->keep_idle + NTCPIP__TCP_MAXIDLE) / NTCPIP__TCP_SLOW_INTERVAL)
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */
      {
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: KEEPALIVE timeout. Aborting connection to %"U16_F".%"U16_F".%"U16_F".%"U16_F".\n",
                                ntcpip_ipaddr1(&pcb->remote_ip), ntcpip_ipaddr2(&pcb->remote_ip),
                                ntcpip_ipaddr3(&pcb->remote_ip), ntcpip_ipaddr4(&pcb->remote_ip)));
        
        ++pcb_remove;
        ++pcb_reset;
      }
#if NTCPIP__LWIP_TCP_KEEPALIVE
      else if((Uint32)(ntcpip__tcpTicks - pcb->tmr) > 
              (pcb->keep_idle + pcb->keep_cnt_sent * pcb->keep_intvl)
              / NTCPIP__TCP_SLOW_INTERVAL)
#else
      else if((Uint32)(ntcpip__tcpTicks - pcb->tmr) > 
              (pcb->keep_idle + pcb->keep_cnt_sent * NTCPIP_TCP_KEEPINTVL_DEFAULT) 
              / NTCPIP__TCP_SLOW_INTERVAL)
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */
      {
        ntcpip__tcpKeepAlive(pcb);
        pcb->keep_cnt_sent++;
      }
    }

    /* If this PCB has queued out of sequence data, but has been
       inactive for too long, will drop the data (it will eventually
       be retransmitted). */
#if NTCPIP__TCP_QUEUE_OOSEQ    
    if (pcb->ooseq != NULL &&
        (Uint32)ntcpip__tcpTicks - pcb->tmr >= pcb->rto * NTCPIP__TCP_OOSEQ_TIMEOUT) {
      ntcpip__tcpSegsFree(pcb->ooseq);
      pcb->ooseq = NULL;
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_CWND_DEBUG, ("ntcpip__tcpSlowTmr: dropping OOSEQ queued data\n"));
    }
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */

    /* Check if this PCB has stayed too long in SYN-RCVD */
    if (pcb->state == NTCPIP_SYN_RCVD) {
      if ((Uint32)(ntcpip__tcpTicks - pcb->tmr) >
          NTCPIP__TCP_SYN_RCVD_TIMEOUT / NTCPIP__TCP_SLOW_INTERVAL) {
        ++pcb_remove;
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: removing pcb stuck in SYN-RCVD\n"));
      }
    }

    /* Check if this PCB has stayed too long in LAST-ACK */
    if (pcb->state == NTCPIP_LAST_ACK) {
      if ((Uint32)(ntcpip__tcpTicks - pcb->tmr) > 2 * NTCPIP__TCP_MSL / NTCPIP__TCP_SLOW_INTERVAL) {
        ++pcb_remove;
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: removing pcb stuck in LAST-ACK\n"));
      }
    }

    /* If the PCB should be removed, do it. */
    if (pcb_remove) {
      ntcpip__tcpPcbPurge(pcb);      
      /* Remove PCB from ntcpip__tcpActivePcbs list. */
      if (prev != NULL) {
        NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: middle tcp != ntcpip__tcpActivePcbs", pcb != ntcpip__tcpActivePcbs);
        prev->next = pcb->next;
      } else {
        /* This PCB was the first. */
        NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: first pcb == ntcpip__tcpActivePcbs", ntcpip__tcpActivePcbs == pcb);
        ntcpip__tcpActivePcbs = pcb->next;
      }

      NTCPIP__TCP_EVENT_ERR(pcb->errf, pcb->callback_arg, NTCPIP_ERR_ABRT);
      if (pcb_reset) {
        ntcpip__tcpRst(pcb->snd_nxt, pcb->rcv_nxt, &pcb->local_ip, &pcb->remote_ip,
          pcb->local_port, pcb->remote_port);
      }

      pcb2 = pcb->next;
      ntcpip__mempFree(MEMP_TCP_PCB, pcb);
      pcb = pcb2;
    } else {

      /* We check if we should poll the connection. */
      ++pcb->polltmr;
      if (pcb->polltmr >= pcb->pollinterval) {
        pcb->polltmr = 0;
        NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpSlowTmr: polling application\n"));
        NTCPIP__TCP_EVENT_POLL(pcb, err);
        if (err == NTCPIP_ERR_OK) {
          ntcpip__tcpOutput(pcb);
        }
      }
      
      prev = pcb;
      pcb = pcb->next;
    }
  }

  
  /* Steps through all of the TIME-WAIT PCBs. */
  prev = NULL;    
  pcb = ntcpip__tcpTwPcb;
  while (pcb != NULL) {
    NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: TIME-WAIT pcb->state == TIME-WAIT", pcb->state == NTCPIP_TIME_WAIT);
    pcb_remove = 0;

    /* Check if this PCB has stayed long enough in TIME-WAIT */
    if ((Uint32)(ntcpip__tcpTicks - pcb->tmr) > 2 * NTCPIP__TCP_MSL / NTCPIP__TCP_SLOW_INTERVAL) {
      ++pcb_remove;
    }
    


    /* If the PCB should be removed, do it. */
    if (pcb_remove) {
      ntcpip__tcpPcbPurge(pcb);      
      /* Remove PCB from ntcpip__tcpTwPcb list. */
      if (prev != NULL) {
        NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: middle tcp != ntcpip__tcpTwPcb", pcb != ntcpip__tcpTwPcb);
        prev->next = pcb->next;
      } else {
        /* This PCB was the first. */
        NTCPIP__LWIP_ASSERT("ntcpip__tcpSlowTmr: first pcb == ntcpip__tcpTwPcb", ntcpip__tcpTwPcb == pcb);
        ntcpip__tcpTwPcb = pcb->next;
      }
      pcb2 = pcb->next;
      ntcpip__mempFree(MEMP_TCP_PCB, pcb);
      pcb = pcb2;
    } else {
      prev = pcb;
      pcb = pcb->next;
    }
  }
}

/**
 * Is called every TCP_FAST_INTERVAL (250 ms) and process data previously
 * "refused" by upper layer (application) and sends delayed ACKs.
 *
 * Automatically called from ntcpip__tcpTmr().
 */
void
ntcpip__tcpFastTmr(void)
{
  struct ntcpip__tcpPcb *pcb;

  for(pcb = ntcpip__tcpActivePcbs; pcb != NULL; pcb = pcb->next) {
    /* If there is data which was previously "refused" by upper layer */
    if (pcb->refused_data != NULL) {
      /* Notify again application with data previously received. */
      ntcpip_Err err;
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_INPUT_DEBUG, ("ntcpip__tcpFastTmr: notify kept packet\n"));
      NTCPIP__TCP_EVENT_RECV(pcb, pcb->refused_data, NTCPIP_ERR_OK, err);
      if (err == NTCPIP_ERR_OK) {
        pcb->refused_data = NULL;
      }
    }

    /* send delayed ACKs */  
    if (pcb->flags & NTCPIP_TF_ACK_DELAY) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpFastTmr: delayed ACK\n"));
      ntcpip__tcpAckNow(pcb);
      pcb->flags &= ~(NTCPIP_TF_ACK_DELAY | NTCPIP_TF_ACK_NOW);
    }
  }
}

/**
 * Deallocates a list of TCP segments (ntcpip__tcpSeg structures).
 *
 * @param seg ntcpip__tcpSeg list of TCP segments to free
 * @return the number of pbufs that were deallocated
 */
Uint8
ntcpip__tcpSegsFree(struct ntcpip__tcpSeg *seg)
{
  Uint8 count = 0;
  struct ntcpip__tcpSeg *next;
  while (seg != NULL) {
    next = seg->next;
    count += ntcpip__tcpSegFree(seg);
    seg = next;
  }
  return count;
}

/**
 * Frees a TCP segment (ntcpip__tcpSeg structure).
 *
 * @param seg single ntcpip__tcpSeg to free
 * @return the number of pbufs that were deallocated
 */
Uint8
ntcpip__tcpSegFree(struct ntcpip__tcpSeg *seg)
{
  Uint8 count = 0;
  
  if (seg != NULL) {
    if (seg->p != NULL) {
      count = ntcpip_pbufFree(seg->p);
#if NTCPIP__TCP_DEBUG
      seg->p = NULL;
#endif /* NTCPIP__TCP_DEBUG */
    }
    ntcpip__mempFree(MEMP_TCP_SEG, seg);
  }
  return count;
}

/**
 * Sets the priority of a connection.
 *
 * @param pcb the tcp_pcb to manipulate
 * @param prio new priority
 */
void
ntcpip__tcpSetPrio(struct ntcpip__tcpPcb *pcb, Uint8 prio)
{
  pcb->prio = prio;
}
#if NTCPIP__TCP_QUEUE_OOSEQ

/**
 * Returns a copy of the given TCP segment.
 * The pbuf and data are not copied, only the pointers
 *
 * @param seg the old ntcpip__tcpSeg
 * @return a copy of seg
 */ 
struct ntcpip__tcpSeg *
ntcpip__tcpSegCopy(struct ntcpip__tcpSeg *seg)
{
  struct ntcpip__tcpSeg *cseg;

  cseg = ntcpip__mempMalloc(MEMP_TCP_SEG);
  if (cseg == NULL) {
    return NULL;
  }
  NTCPIP__SMEMCPY((Uint8 *)cseg, (const Uint8 *)seg, sizeof(struct ntcpip__tcpSeg)); 
  ntcpip_pbufRef(cseg->p);
  return cseg;
}
#endif

#if NTCPIP__LWIP_CALLBACK_API
/**
 * Default receive callback that is called if the user didn't register
 * a recv callback for the pcb.
 */
ntcpip_Err
ntcpip__tcpRecvNull(void *arg, struct ntcpip__tcpPcb *pcb, struct ntcpip_pbuf *p, ntcpip_Err err)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  if (p != NULL) {
    ntcpip_tcpRecved(pcb, p->tot_len);
    ntcpip_pbufFree(p);
  } else if (err == NTCPIP_ERR_OK) {
    return ntcpip_tcpClose(pcb);
  }
  return NTCPIP_ERR_OK;
}
#endif /* NTCPIP__LWIP_CALLBACK_API */

/**
 * Kills the oldest active connection that has lower priority than prio.
 *
 * @param prio minimum priority
 */
static void
tcp_kill_prio(Uint8 prio)
{
  struct ntcpip__tcpPcb *pcb, *inactive;
  Uint32 inactivity;
  Uint8 mprio;


  mprio = NTCPIP__TCP_PRIO_MAX;
  
  /* We kill the oldest active connection that has lower priority than prio. */
  inactivity = 0;
  inactive = NULL;
  for(pcb = ntcpip__tcpActivePcbs; pcb != NULL; pcb = pcb->next) {
    if (pcb->prio <= prio &&
       pcb->prio <= mprio &&
       (Uint32)(ntcpip__tcpTicks - pcb->tmr) >= inactivity) {
      inactivity = ntcpip__tcpTicks - pcb->tmr;
      inactive = pcb;
      mprio = pcb->prio;
    }
  }
  if (inactive != NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("tcp_kill_prio: killing oldest PCB %p (%"S32_F")\n",
           (void *)inactive, inactivity));
    ntcpip_tcpAbort(inactive);
  }      
}

/**
 * Kills the oldest connection that is in NTCPIP_TIME_WAIT state.
 * Called from ntcpip_tcpAlloc() if no more connections are available.
 */
static void
tcp_kill_timewait(void)
{
  struct ntcpip__tcpPcb *pcb, *inactive;
  Uint32 inactivity;

  inactivity = 0;
  inactive = NULL;
  /* Go through the list of NTCPIP_TIME_WAIT pcbs and get the oldest pcb. */
  for(pcb = ntcpip__tcpTwPcb; pcb != NULL; pcb = pcb->next) {
    if ((Uint32)(ntcpip__tcpTicks - pcb->tmr) >= inactivity) {
      inactivity = ntcpip__tcpTicks - pcb->tmr;
      inactive = pcb;
    }
  }
  if (inactive != NULL) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("tcp_kill_timewait: killing oldest TIME-WAIT PCB %p (%"S32_F")\n",
           (void *)inactive, inactivity));
    ntcpip_tcpAbort(inactive);
  }      
}

/**
 * Allocate a new tcp_pcb structure.
 *
 * @param prio priority for the new pcb
 * @return a new tcp_pcb that initially is in state NTCPIP_CLOSED
 */
struct ntcpip__tcpPcb *
ntcpip_tcpAlloc(Uint8 prio)
{
  struct ntcpip__tcpPcb *pcb;
  Uint32 iss;
  
  pcb = ntcpip__mempMalloc(MEMP_TCP_PCB);
  if (pcb == NULL) {
    /* Try killing oldest connection in TIME-WAIT. */
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip_tcpAlloc: killing off oldest TIME-WAIT connection\n"));
    tcp_kill_timewait();
    /* Try to allocate a tcp_pcb again. */
    pcb = ntcpip__mempMalloc(MEMP_TCP_PCB);
    if (pcb == NULL) {
      /* Try killing active connections with lower priority than the new one. */
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip_tcpAlloc: killing connection with prio lower than %d\n", prio));
      tcp_kill_prio(prio);
      /* Try to allocate a tcp_pcb again. */
      pcb = ntcpip__mempMalloc(MEMP_TCP_PCB);
      if (pcb != NULL) {
        /* adjust err stats: ntcpip__mempMalloc failed twice before */
        MEMP_STATS_DEC(err, MEMP_TCP_PCB);
      }
    }
    if (pcb != NULL) {
      /* adjust err stats: timewait PCB was freed above */
      MEMP_STATS_DEC(err, MEMP_TCP_PCB);
    }
  }
  if (pcb != NULL) {
    memset(pcb, 0, sizeof(struct ntcpip__tcpPcb));
    pcb->prio = NTCPIP__TCP_PRIO_NORMAL;
    pcb->snd_buf = NTCPIP__TCP_SND_BUF;
    pcb->snd_queuelen = 0;
    pcb->rcv_wnd = NTCPIP__TCP_WND;
    pcb->rcv_ann_wnd = NTCPIP__TCP_WND;
    pcb->tos = 0;
    pcb->ttl = NTCPIP__TCP_TTL;
    /* As initial send MSS, we use NTCPIP__TCP_MSS but limit it to 536.
       The send MSS is updated when an MSS option is received. */
    pcb->mss = (NTCPIP__TCP_MSS > 536) ? 536 : NTCPIP__TCP_MSS;
    pcb->rto = 3000 / NTCPIP__TCP_SLOW_INTERVAL;
    pcb->sa = 0;
    pcb->sv = 3000 / NTCPIP__TCP_SLOW_INTERVAL;
    pcb->rtime = -1;
    pcb->cwnd = 1;
    iss = ntcpip__tcpNextIss();
    pcb->snd_wl2 = iss;
    pcb->snd_nxt = iss;
    pcb->lastack = iss;
    pcb->snd_lbb = iss;   
    pcb->tmr = ntcpip__tcpTicks;

    pcb->polltmr = 0;

#if NTCPIP__LWIP_CALLBACK_API
    pcb->recv = ntcpip__tcpRecvNull;
#endif /* NTCPIP__LWIP_CALLBACK_API */  
    
    /* Init KEEPALIVE timer */
    pcb->keep_idle  = NTCPIP_TCP_KEEPIDLE_DEFAULT;
    
#if NTCPIP__LWIP_TCP_KEEPALIVE
    pcb->keep_intvl = NTCPIP_TCP_KEEPINTVL_DEFAULT;
    pcb->keep_cnt   = NTCPIP_TCP_KEEPCNT_DEFAULT;
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */

    pcb->keep_cnt_sent = 0;
  }
  return pcb;
}

/**
 * Creates a new TCP protocol control block but doesn't place it on
 * any of the TCP PCB lists.
 * The pcb is not put on any list until binding using ntcpip_tcpBind().
 *
 * @internal: Maybe there should be a idle TCP PCB list where these
 * PCBs are put on. Port reservation using ntcpip_tcpBind() is implemented but
 * allocated pcbs that are not bound can't be killed automatically if wanting
 * to allocate a pcb with higher prio (@see tcp_kill_prio())
 *
 * @return a new tcp_pcb that initially is in state NTCPIP_CLOSED
 */
struct ntcpip__tcpPcb *
ntcpip_tcpNew(void)
{
  return ntcpip_tcpAlloc(NTCPIP__TCP_PRIO_NORMAL);
}

/**
 * Used to specify the argument that should be passed callback
 * functions.
 *
 * @param pcb tcp_pcb to set the callback argument
 * @param arg void pointer argument to pass to callback functions
 */ 
void
ntcpip_tcpArg(struct ntcpip__tcpPcb *pcb, void *arg)
{  
  pcb->callback_arg = arg;
}
#if NTCPIP__LWIP_CALLBACK_API

/**
 * Used to specify the function that should be called when a TCP
 * connection receives data.
 *
 * @param pcb tcp_pcb to set the recv callback
 * @param recv callback function to call for this pcb when data is received
 */ 
void
ntcpip_tcpRecv(struct ntcpip__tcpPcb *pcb,
   ntcpip_Err (* recv)(void *arg, struct ntcpip__tcpPcb *tpcb, struct ntcpip_pbuf *p, ntcpip_Err err))
{
  pcb->recv = recv;
}

/**
 * Used to specify the function that should be called when TCP data
 * has been successfully delivered to the remote host.
 *
 * @param pcb tcp_pcb to set the sent callback
 * @param sent callback function to call for this pcb when data is successfully sent
 */ 
void
ntcpip_tcpSent(struct ntcpip__tcpPcb *pcb,
   ntcpip_Err (* sent)(void *arg, struct ntcpip__tcpPcb *tpcb, Uint16 len))
{
  pcb->sent = sent;
}

/**
 * Used to specify the function that should be called when a fatal error
 * has occured on the connection.
 *
 * @param pcb tcp_pcb to set the err callback
 * @param errf callback function to call for this pcb when a fatal error
 *        has occured on the connection
 */ 
void
ntcpip_tcpErr(struct ntcpip__tcpPcb *pcb,
   void (* errf)(void *arg, ntcpip_Err err))
{
  pcb->errf = errf;
}

/**
 * Used for specifying the function that should be called when a
 * LISTENing connection has been connected to another host.
 *
 * @param pcb tcp_pcb to set the accept callback
 * @param accept callback function to call for this pcb when LISTENing
 *        connection has been connected to another host
 */ 
void
ntcpip_tcpAccept(struct ntcpip__tcpPcb *pcb,
     ntcpip_Err (* accept)(void *arg, struct ntcpip__tcpPcb *newpcb, ntcpip_Err err))
{
  pcb->accept = accept;
}
#endif /* NTCPIP__LWIP_CALLBACK_API */


/**
 * Used to specify the function that should be called periodically
 * from TCP. The interval is specified in terms of the TCP coarse
 * timer interval, which is called twice a second.
 *
 */ 
void
ntcpip_tcpPoll(struct ntcpip__tcpPcb *pcb,
   ntcpip_Err (* poll)(void *arg, struct ntcpip__tcpPcb *tpcb), Uint8 interval)
{
#if NTCPIP__LWIP_CALLBACK_API
  pcb->poll = poll;
#endif /* NTCPIP__LWIP_CALLBACK_API */  
  pcb->pollinterval = interval;
}

/**
 * Purges a TCP PCB. Removes any buffered data and frees the buffer memory
 * (pcb->ooseq, pcb->unsent and pcb->unacked are freed).
 *
 * @param pcb tcp_pcb to purge. The pcb itself is not deallocated!
 */
void
ntcpip__tcpPcbPurge(struct ntcpip__tcpPcb *pcb)
{
  if (pcb->state != NTCPIP_CLOSED &&
     pcb->state != NTCPIP_TIME_WAIT &&
     pcb->state != NTCPIP_LISTEN) {

    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpPcbPurge\n"));

#if NTCPIP__TCP_LISTEN_BACKLOG
    if (pcb->state == NTCPIP_SYN_RCVD) {
      /* Need to find the corresponding listen_pcb and decrease its accepts_pending */
      struct ntcpip__tcpPcbListen *lpcb;
      NTCPIP__LWIP_ASSERT("ntcpip__tcpPcbPurge: pcb->state == NTCPIP_SYN_RCVD but ntcpip__tcpListenPcbs is NULL",
        ntcpip__tcpListenPcbs.listen_pcbs != NULL);
      for (lpcb = ntcpip__tcpListenPcbs.listen_pcbs; lpcb != NULL; lpcb = lpcb->next) {
        if ((lpcb->local_port == pcb->local_port) &&
            (ntcpip_ipaddrIsAny(&lpcb->local_ip) ||
             ntcpip_ipaddrCmp(&pcb->local_ip, &lpcb->local_ip))) {
            /* port and address of the listen pcb match the timed-out pcb */
            NTCPIP__LWIP_ASSERT("ntcpip__tcpPcbPurge: listen pcb does not have accepts pending",
              lpcb->accepts_pending > 0);
            lpcb->accepts_pending--;
            break;
          }
      }
    }
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */


    if (pcb->refused_data != NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpPcbPurge: data left on ->refused_data\n"));
      ntcpip_pbufFree(pcb->refused_data);
      pcb->refused_data = NULL;
    }
    if (pcb->unsent != NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpPcbPurge: not all data sent\n"));
    }
    if (pcb->unacked != NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpPcbPurge: data left on ->unacked\n"));
    }
#if NTCPIP__TCP_QUEUE_OOSEQ /* LW */
    if (pcb->ooseq != NULL) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ntcpip__tcpPcbPurge: data left on ->ooseq\n"));
    }

    /* Stop the retransmission timer as it will expect data on unacked
       queue if it fires */
    pcb->rtime = -1;

    ntcpip__tcpSegsFree(pcb->ooseq);
    pcb->ooseq = NULL;
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */
    ntcpip__tcpSegsFree(pcb->unsent);
    ntcpip__tcpSegsFree(pcb->unacked);
    pcb->unacked = pcb->unsent = NULL;
  }
}

/**
 * Purges the PCB and removes it from a PCB list. Any delayed ACKs are sent first.
 *
 * @param pcblist PCB list to purge.
 * @param pcb tcp_pcb to purge. The pcb itself is NOT deallocated!
 */
void
ntcpip__tcpPcbRemove(struct ntcpip__tcpPcb **pcblist, struct ntcpip__tcpPcb *pcb)
{
  NTCPIP__TCP_RMV(pcblist, pcb);

  ntcpip__tcpPcbPurge(pcb);
  
  /* if there is an outstanding delayed ACKs, send it */
  if (pcb->state != NTCPIP_TIME_WAIT &&
     pcb->state != NTCPIP_LISTEN &&
     pcb->flags & NTCPIP_TF_ACK_DELAY) {
    pcb->flags |= NTCPIP_TF_ACK_NOW;
    ntcpip__tcpOutput(pcb);
  }

  if (pcb->state != NTCPIP_LISTEN) {
    NTCPIP__LWIP_ASSERT("unsent segments leaking", pcb->unsent == NULL);
    NTCPIP__LWIP_ASSERT("unacked segments leaking", pcb->unacked == NULL);
#if NTCPIP__TCP_QUEUE_OOSEQ
    NTCPIP__LWIP_ASSERT("ooseq segments leaking", pcb->ooseq == NULL);
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */
  }

  pcb->state = NTCPIP_CLOSED;

  NTCPIP__LWIP_ASSERT("ntcpip__tcpPcbRemove: tcp_pcbs_sane()", tcp_pcbs_sane());
}

/**
 * Calculates a new initial sequence number for new connections.
 *
 * @return Uint32 pseudo random sequence number
 */
Uint32
ntcpip__tcpNextIss(void)
{
  static Uint32 iss = 6510;
  
  iss += ntcpip__tcpTicks;       /* XXX */
  return iss;
}

#if NTCPIP__TCP_CALCULATE_EFF_SEND_MSS
/**
 * Calcluates the effective send mss that can be used for a specific IP address
 * by using ntcpip__ipRoute to determin the netif used to send to the address and
 * calculating the minimum of NTCPIP__TCP_MSS and that netif's mtu (if set).
 */
Uint16
ntcpip__tcpEffSendMss(Uint16 sendmss, const struct ntcpip_ipAddr *addr)
{
  Uint16 mss_s;
  struct ntcpip__netif *outif;

  outif = ntcpip__ipRoute(addr);
  if ((outif != NULL) && (outif->mtu != 0)) {
    mss_s = outif->mtu - NTCPIP__IP_HLEN - NTCPIP__TCP_HLEN;
    /* RFC 1122, chap 4.2.2.6:
     * Eff.snd.MSS = min(SendMSS+20, MMS_S) - TCPhdrsize - IPoptionsize
     * We correct for TCP options in ntcpip__tcpEnqueue(), and don't support
     * IP options
     */
    sendmss = LWIP_MIN(sendmss, mss_s);
  }
  return sendmss;
}
#endif /* NTCPIP__TCP_CALCULATE_EFF_SEND_MSS */

const char*
ntcpip__tcpDebugStateStr(enum ntcpip__tcpState s)
{
  return ntcpip__tcpStateStr[s];
}

#if NTCPIP__TCP_DEBUG || NTCPIP__TCP_INPUT_DEBUG || NTCPIP__TCP_OUTPUT_DEBUG
/**
 * Print a tcp header for debugging purposes.
 *
 * @param tcphdr pointer to a struct tcp_hdr
 */
void
tcp_debug_print(struct tcp_hdr *tcphdr)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("TCP header:\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("|    %5"U16_F"      |    %5"U16_F"      | (src port, dest port)\n",
         ntcpip_ntohs(tcphdr->src), ntcpip_ntohs(tcphdr->dest)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("|           %010"U32_F"          | (seq no)\n",
          ntcpip_ntohl(tcphdr->seqno)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("|           %010"U32_F"          | (ack no)\n",
         ntcpip_ntohl(tcphdr->ackno)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("| %2"U16_F" |   |%"U16_F"%"U16_F"%"U16_F"%"U16_F"%"U16_F"%"U16_F"|     %5"U16_F"     | (hdrlen, flags (",
       NTCPIP__TCPH_HDRLEN(tcphdr),
         NTCPIP__TCPH_FLAGS(tcphdr) >> 5 & 1,
         NTCPIP__TCPH_FLAGS(tcphdr) >> 4 & 1,
         NTCPIP__TCPH_FLAGS(tcphdr) >> 3 & 1,
         NTCPIP__TCPH_FLAGS(tcphdr) >> 2 & 1,
         NTCPIP__TCPH_FLAGS(tcphdr) >> 1 & 1,
         NTCPIP__TCPH_FLAGS(tcphdr) & 1,
         ntcpip_ntohs(tcphdr->wnd)));
  tcp_debug_print_flags(NTCPIP__TCPH_FLAGS(tcphdr));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("), win)\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("+-------------------------------+\n"));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("|    0x%04"X16_F"     |     %5"U16_F"     | (chksum, urgp)\n",
         ntcpip_ntohs(tcphdr->chksum), ntcpip_ntohs(tcphdr->urgp)));
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("+-------------------------------+\n"));
}

/**
 * Print a tcp state for debugging purposes.
 *
 * @param s enum ntcpip__tcpState to print
 */
void
tcp_debug_print_state(enum ntcpip__tcpState s)
{
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("State: %s\n", ntcpip__tcpStateStr[s]));
}

/**
 * Print tcp flags for debugging purposes.
 *
 * @param flags tcp flags, all active flags are printed
 */
void
tcp_debug_print_flags(Uint8 flags)
{
  if (flags & TCP_FIN) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("FIN "));
  }
  if (flags & TCP_SYN) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("SYN "));
  }
  if (flags & TCP_RST) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("RST "));
  }
  if (flags & TCP_PSH) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("PSH "));
  }
  if (flags & TCP_ACK) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ACK "));
  }
  if (flags & TCP_URG) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("URG "));
  }
  if (flags & TCP_ECE) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("ECE "));
  }
  if (flags & TCP_CWR) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("CWR "));
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("\n"));
}

/**
 * Print all tcp_pcbs in every list for debugging purposes.
 */
void
tcp_debug_print_pcbs(void)
{
  struct ntcpip__tcpPcb *pcb;
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("Active PCB states:\n"));
  for(pcb = ntcpip__tcpActivePcbs; pcb != NULL; pcb = pcb->next) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("Local port %"U16_F", foreign port %"U16_F" snd_nxt %"U32_F" rcv_nxt %"U32_F" ",
                       pcb->local_port, pcb->remote_port,
                       pcb->snd_nxt, pcb->rcv_nxt));
    tcp_debug_print_state(pcb->state);
  }    
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("Listen PCB states:\n"));
  for(pcb = (struct ntcpip__tcpPcb *)ntcpip__tcpListenPcbs.pcbs; pcb != NULL; pcb = pcb->next) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("Local port %"U16_F", foreign port %"U16_F" snd_nxt %"U32_F" rcv_nxt %"U32_F" ",
                       pcb->local_port, pcb->remote_port,
                       pcb->snd_nxt, pcb->rcv_nxt));
    tcp_debug_print_state(pcb->state);
  }    
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("TIME-WAIT PCB states:\n"));
  for(pcb = ntcpip__tcpTwPcb; pcb != NULL; pcb = pcb->next) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("Local port %"U16_F", foreign port %"U16_F" snd_nxt %"U32_F" rcv_nxt %"U32_F" ",
                       pcb->local_port, pcb->remote_port,
                       pcb->snd_nxt, pcb->rcv_nxt));
    tcp_debug_print_state(pcb->state);
  }    
}

/**
 * Check state consistency of the tcp_pcb lists.
 */
Int16
tcp_pcbs_sane(void)
{
  struct ntcpip__tcpPcb *pcb;
  for(pcb = ntcpip__tcpActivePcbs; pcb != NULL; pcb = pcb->next) {
    NTCPIP__LWIP_ASSERT("tcp_pcbs_sane: active pcb->state != NTCPIP_CLOSED", pcb->state != NTCPIP_CLOSED);
    NTCPIP__LWIP_ASSERT("tcp_pcbs_sane: active pcb->state != NTCPIP_LISTEN", pcb->state != NTCPIP_LISTEN);
    NTCPIP__LWIP_ASSERT("tcp_pcbs_sane: active pcb->state != TIME-WAIT", pcb->state != NTCPIP_TIME_WAIT);
  }
  for(pcb = ntcpip__tcpTwPcb; pcb != NULL; pcb = pcb->next) {
    NTCPIP__LWIP_ASSERT("tcp_pcbs_sane: tw pcb->state == TIME-WAIT", pcb->state == NTCPIP_TIME_WAIT);
  }
  return 1;
}
#endif /* NTCPIP__TCP_DEBUG */

#endif /* NTCPIP__LWIP_TCP */


