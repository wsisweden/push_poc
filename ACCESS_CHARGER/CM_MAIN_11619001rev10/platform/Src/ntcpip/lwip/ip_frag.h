/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Jani Monoses <jani@iv.ro>
 *
 */

#ifndef __LWIP_IP_FRAG_H__
#define __LWIP_IP_FRAG_H__

#include "ntcpip/opt.h"
#include "ntcpip/err.h"
#include "ntcpip/pbuf.h"
#include "ntcpip/netif.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/ip.h"

#ifdef __cplusplus
extern "C" {
#endif

#if NTCPIP__IP_REASSEMBLY
/* The IP reassembly timer interval in milliseconds. */
#define IP_TMR_INTERVAL 1000

/* IP reassembly helper struct.
 * This is exported because memp needs to know the size.
 */
struct ip_reassdata {
  struct ip_reassdata *next;
  struct ntcpip_pbuf *p;
  struct ip_hdr iphdr;
  Uint16 datagram_len;
  Uint8 flags;
  Uint8 timer;
};

void ntcpip__ipReassInit(void);
void ntcpip__ipReassTmr(void);
struct ntcpip_pbuf * ntcpip__ipReass(struct ntcpip_pbuf *p);
#endif /* NTCPIP__IP_REASSEMBLY */

#if NTCPIP__IP_FRAG
ntcpip_Err ntcpip__ipFrag(struct ntcpip_pbuf *p, struct ntcpip__netif *netif, const struct ntcpip_ipAddr *dest);
#endif /* NTCPIP__IP_FRAG */

#ifdef __cplusplus
}
#endif

#endif /* __LWIP_IP_FRAG_H__ */

