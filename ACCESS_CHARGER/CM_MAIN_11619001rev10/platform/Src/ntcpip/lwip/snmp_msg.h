/**
 * @file
 * SNMP Agent message handling structures.
 */

/*
 * Copyright (c) 2006 Axon Digital Design B.V., The Netherlands.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Author: Christiaan Simons <christiaan.simons@axon.tv>
 */

#ifndef __LWIP_SNMP_MSG_H__
#define __LWIP_SNMP_MSG_H__

#include "ntcpip/opt.h"
#include "ntcpip/snmp.h"
#include "ntcpip/snmp_structs.h"

#if NTCPIP__LWIP_SNMP

#if NTCPIP__SNMP_PRIVATE_MIB
#include "private_mib.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* The listen port of the SNMP agent. Clients have to make their requests to
   this port. Most standard clients won't work if you change this! */
#ifndef SNMP_IN_PORT
#define SNMP_IN_PORT 161
#endif
/* The remote port the SNMP agent sends traps to. Most standard trap sinks won't
   work if you change this! */
#ifndef SNMP_TRAP_PORT
#define SNMP_TRAP_PORT 162
#endif

#define SNMP_ES_NOERROR 0
#define SNMP_ES_TOOBIG 1
#define SNMP_ES_NOSUCHNAME 2
#define SNMP_ES_BADVALUE 3
#define SNMP_ES_READONLY 4
#define SNMP_ES_GENERROR 5

#define SNMP_GENTRAP_COLDSTART 0
#define SNMP_GENTRAP_WARMSTART 1
#define SNMP_GENTRAP_AUTHFAIL 4
#define SNMP_GENTRAP_ENTERPRISESPC 6

struct snmp_varbind
{
  /* next pointer, NULL for last in list */
  struct snmp_varbind *next;
  /* previous pointer, NULL for first in list */
  struct snmp_varbind *prev;

  /* object identifier length (in Int32) */
  Uint8 ident_len;
  /* object identifier array */
  Int32 *ident;

  /* object value ASN1 type */
  Uint8 value_type;
  /* object value length (in Uint8) */
  Uint8 value_len;
  /* object value */
  void *value;

  /* encoding varbind seq length length */
  Uint8 seqlenlen;
  /* encoding object identifier length length */
  Uint8 olenlen;
  /* encoding object value length length */
  Uint8 vlenlen;
  /* encoding varbind seq length */
  Uint16 seqlen;
  /* encoding object identifier length */
  Uint16 olen;
  /* encoding object value length */
  Uint16 vlen;
};

struct snmp_varbind_root
{
  struct snmp_varbind *head;
  struct snmp_varbind *tail;
  /* number of variable bindings in list */
  Uint8 count;
  /* encoding varbind-list seq length length */
  Uint8 seqlenlen;
  /* encoding varbind-list seq length */
  Uint16 seqlen;
};

/** output response message header length fields */
struct snmp_resp_header_lengths
{
  /* encoding error-index length length */
  Uint8 erridxlenlen;
  /* encoding error-status length length */
  Uint8 errstatlenlen;
  /* encoding request id length length */
  Uint8 ridlenlen;
  /* encoding pdu length length */
  Uint8 pdulenlen;
  /* encoding community length length */
  Uint8 comlenlen;
  /* encoding version length length */
  Uint8 verlenlen;
  /* encoding sequence length length */
  Uint8 seqlenlen;

  /* encoding error-index length */
  Uint16 erridxlen;
  /* encoding error-status length */
  Uint16 errstatlen;
  /* encoding request id length */
  Uint16 ridlen;
  /* encoding pdu length */
  Uint16 pdulen;
  /* encoding community length */
  Uint16 comlen;
  /* encoding version length */
  Uint16 verlen;
  /* encoding sequence length */
  Uint16 seqlen;
};

/** output response message header length fields */
struct snmp_trap_header_lengths
{
  /* encoding timestamp length length */
  Uint8 tslenlen;
  /* encoding specific-trap length length */
  Uint8 strplenlen;
  /* encoding generic-trap length length */
  Uint8 gtrplenlen;
  /* encoding agent-addr length length */
  Uint8 aaddrlenlen;
  /* encoding enterprise-id length length */
  Uint8 eidlenlen;
  /* encoding pdu length length */
  Uint8 pdulenlen;
  /* encoding community length length */
  Uint8 comlenlen;
  /* encoding version length length */
  Uint8 verlenlen;
  /* encoding sequence length length */
  Uint8 seqlenlen;

  /* encoding timestamp length */
  Uint16 tslen;
  /* encoding specific-trap length */
  Uint16 strplen;
  /* encoding generic-trap length */
  Uint16 gtrplen;
  /* encoding agent-addr length */
  Uint16 aaddrlen;
  /* encoding enterprise-id length */
  Uint16 eidlen;
  /* encoding pdu length */
  Uint16 pdulen;
  /* encoding community length */
  Uint16 comlen;
  /* encoding version length */
  Uint16 verlen;
  /* encoding sequence length */
  Uint16 seqlen;
};

/* Accepting new SNMP messages. */
#define SNMP_MSG_EMPTY                 0
/* Search for matching object for variable binding. */
#define SNMP_MSG_SEARCH_OBJ            1
/* Perform SNMP operation on in-memory object.
   Pass-through states, for symmetry only. */
#define SNMP_MSG_INTERNAL_GET_OBJDEF   2
#define SNMP_MSG_INTERNAL_GET_VALUE    3
#define SNMP_MSG_INTERNAL_SET_TEST     4
#define SNMP_MSG_INTERNAL_GET_OBJDEF_S 5
#define SNMP_MSG_INTERNAL_SET_VALUE    6
/* Perform SNMP operation on object located externally.
   In theory this could be used for building a proxy agent.
   Practical use is for an enterprise spc. app. gateway. */
#define SNMP_MSG_EXTERNAL_GET_OBJDEF   7
#define SNMP_MSG_EXTERNAL_GET_VALUE    8
#define SNMP_MSG_EXTERNAL_SET_TEST     9
#define SNMP_MSG_EXTERNAL_GET_OBJDEF_S 10
#define SNMP_MSG_EXTERNAL_SET_VALUE    11

#define SNMP_COMMUNITY_STR_LEN 64
struct snmp_msg_pstat
{
  /* lwIP local port (161) binding */
  struct ntcpip__udpPcb *pcb;
  /* source IP address */
  struct ntcpip_ipAddr sip;
  /* source UDP port */
  Uint16 sp;
  /* request type */
  Uint8 rt;
  /* request ID */
  Int32 rid;
  /* error status */
  Int32 error_status;
  /* error index */
  Int32 error_index;
  /* community name (zero terminated) */
  Uint8 community[SNMP_COMMUNITY_STR_LEN + 1];
  /* community string length (exclusive zero term) */
  Uint8 com_strlen;
  /* one out of MSG_EMPTY, MSG_DEMUX, MSG_INTERNAL, MSG_EXTERNAL_x */
  Uint8 state;
  /* saved arguments for MSG_EXTERNAL_x */
  struct mib_external_node *ext_mib_node;
  struct snmp_name_ptr ext_name_ptr;
  struct obj_def ext_object_def;
  struct snmp_obj_id ext_oid;
  /* index into input variable binding list */
  Uint8 vb_idx;
  /* ptr into input variable binding list */
  struct snmp_varbind *vb_ptr;
  /* list of variable bindings from input */
  struct snmp_varbind_root invb;
  /* list of variable bindings to output */
  struct snmp_varbind_root outvb;
  /* output response lengths used in ASN encoding */
  struct snmp_resp_header_lengths rhl;
};

struct snmp_msg_trap
{
  /* lwIP local port (161) binding */
  struct ntcpip__udpPcb *pcb;
  /* destination IP address in network order */
  struct ntcpip_ipAddr dip;

  /* source enterprise ID (sysObjectID) */
  struct snmp_obj_id *enterprise;
  /* source IP address, raw network order format */
  Uint8 sip_raw[4];
  /* generic trap code */
  Uint32 gen_trap;
  /* specific trap code */
  Uint32 spc_trap;
  /* timestamp */
  Uint32 ts;
  /* list of variable bindings to output */
  struct snmp_varbind_root outvb;
  /* output trap lengths used in ASN encoding */
  struct snmp_trap_header_lengths thl;
};

/** Agent Version constant, 0 = v1 oddity */
extern const Int32 snmp_version;
/** Agent default "public" community string */
extern const char snmp_publiccommunity[7];

extern struct snmp_msg_trap trap_msg;

/** Agent setup, start listening to port 161. */
void ntcpip__snmpInit(void);
void ntcpip__snmpTrapDstEnable(Uint8 dst_idx, Uint8 enable);
void ntcpip__snmpTrapDstIpSet(Uint8 dst_idx, struct ntcpip_ipAddr *dst);

/** Varbind-list functions. */
struct snmp_varbind* ntcpip__snmpVarBindAlloc(struct snmp_obj_id *oid, Uint8 type, Uint8 len);
void ntcpip__snmpVarbindFree(struct snmp_varbind *vb);
void ntcpip__snmpVarbindLFree(struct snmp_varbind_root *root);
void ntcpip__snmpVarbindTailAdd(struct snmp_varbind_root *root, struct snmp_varbind *vb);
struct snmp_varbind* ntcpip__snmpVarbindTailRem(struct snmp_varbind_root *root);

/** Handle an internal (recv) or external (private response) event. */
void ntcpip_snmpMsgEvent(Uint8 request_id);
ntcpip_Err ntcpip__snmpSendResponse(struct snmp_msg_pstat *m_stat);
ntcpip_Err ntcpip__snmpSendTrap(Int8 generic_trap, struct snmp_obj_id *eoid, Int32 specific_trap);
void ntcpip__snmpColdstartTrap(void);
void ntcpip__snmpAuthfailTrap(void);

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_SNMP */

#endif /* __LWIP_SNMP_MSG_H__ */


