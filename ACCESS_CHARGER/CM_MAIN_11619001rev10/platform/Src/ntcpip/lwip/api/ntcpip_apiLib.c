/**
 * @file
 * Sequential API External module
 *
 */
 
/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

/* This is the part of the API that is linked with
   the application */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_NETCONN /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/api.h"
#include "ntcpip/tcpip.h"
#include "../memp.h"

#include "ntcpip/ip.h"
#include "../raw.h"
#include "ntcpip/udp.h"
#include "ntcpip/tcp.h"

#include <string.h>

/**
 * Create a new netconn (of a specific type) that has a callback function.
 * The corresponding pcb is also created.
 *
 * @param t the type of 'connection' to create (@see enum ntcpip_netconnType)
 * @param proto the IP protocol for RAW IP pcbs
 * @param callback a function to call on status changes (RX available, TX'ed)
 * @return a newly allocated struct ntcpip__netconn or
 *         NULL on memory error
 */
struct ntcpip__netconn*
ntcpip_netconnNewWithProtoAndCallback(enum ntcpip_netconnType t, Uint8 proto, ntcpip__NetconnCallback callback)
{
  struct ntcpip__netconn *conn;
  struct ntcpip__apiMsg msg;

  conn = ntcpip__netconnAlloc(t, callback);
  if (conn != NULL ) {
    msg.function = ntcpip__apimsgDoNewConn;
    msg.msg.msg.n.proto = proto;
    msg.msg.conn = conn;
    NTCPIP__APIMSG(&msg);

    if (conn->err != NTCPIP_ERR_OK) {
      NTCPIP__LWIP_ASSERT("freeing conn without freeing pcb", conn->pcb.tcp == NULL);
      NTCPIP__LWIP_ASSERT("conn has no op_completed", conn->op_completed != NTCPIP__SYS_SEM_NULL);
      NTCPIP__LWIP_ASSERT("conn has no recvmbox", conn->recvmbox != NTCPIP__SYS_MBOX_NULL);
      NTCPIP__LWIP_ASSERT("conn->acceptmbox shouldn't exist", conn->acceptmbox == NTCPIP__SYS_MBOX_NULL);
      ntcpip__sysSemFree(conn->op_completed);
      ntcpip__sysMboxFree(conn->recvmbox);
      ntcpip__mempFree(MEMP_NETCONN, conn);
      return NULL;
    }
  }
  return conn;
}

/**
 * Close a netconn 'connection' and free its resources.
 * UDP and RAW connection are completely closed, TCP pcbs might still be in a waitstate
 * after this returns.
 *
 * @param conn the netconn to delete
 * @return NTCPIP_ERR_OK if the connection was deleted
 */
ntcpip_Err
ntcpip_netconnDelete(struct ntcpip__netconn *conn)
{
  struct ntcpip__apiMsg msg;

  /* No ASSERT here because possible to get a (conn == NULL) if we got an accept error */
  if (conn == NULL) {
    return NTCPIP_ERR_OK;
  }

  msg.function = ntcpip__apimsgDoDelconn;
  msg.msg.conn = conn;
  ntcpip__tcpipApiMsg(&msg);

  conn->pcb.tcp = NULL;
  ntcpip__netconnFree(conn);

  return NTCPIP_ERR_OK;
}

/**
 * Get the local or remote IP address and port of a netconn.
 * For RAW netconns, this returns the protocol instead of a port!
 *
 * @param conn the netconn to query
 * @param addr a pointer to which to save the IP address
 * @param port a pointer to which to save the port (or protocol for RAW)
 * @param local 1 to get the local IP address, 0 to get the remote one
 * @return NTCPIP_ERR_CONN for invalid connections
 *         NTCPIP_ERR_OK if the information was retrieved
 */
ntcpip_Err
ntcpip_netconnGetAddr(struct ntcpip__netconn *conn, struct ntcpip_ipAddr *addr, Uint16 *port, Uint8 local)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnGetAddr: invalid conn", (conn != NULL), return NTCPIP_ERR_ARG;);
  NTCPIP__LWIP_ERROR("ntcpip_netconnGetAddr: invalid addr", (addr != NULL), return NTCPIP_ERR_ARG;);
  NTCPIP__LWIP_ERROR("ntcpip_netconnGetAddr: invalid port", (port != NULL), return NTCPIP_ERR_ARG;);

  msg.function = ntcpip__apimsgDoGetAddr;
  msg.msg.conn = conn;
  msg.msg.msg.ad.ipaddr = addr;
  msg.msg.msg.ad.port = port;
  msg.msg.msg.ad.local = local;
  NTCPIP__APIMSG(&msg);

  return conn->err;
}

/**
 * Bind a netconn to a specific local IP address and port.
 * Binding one netconn twice might not always be checked correctly!
 *
 * @param conn the netconn to bind
 * @param addr the local IP address to bind the netconn to (use NTCPIP_IP_ADDR_ANY
 *             to bind to all addresses)
 * @param port the local port to bind the netconn to (not used for RAW)
 * @return NTCPIP_ERR_OK if bound, any other ntcpip_Err on failure
 */
ntcpip_Err
ntcpip_netconnBind(struct ntcpip__netconn *conn, const struct ntcpip_ipAddr *addr, Uint16 port)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnBind: invalid conn", (conn != NULL), return NTCPIP_ERR_ARG;);

  msg.function = ntcpip__apimsgDoBind;
  msg.msg.conn = conn;
  msg.msg.msg.bc.ipaddr = addr;
  msg.msg.msg.bc.port = port;
  NTCPIP__APIMSG(&msg);
  return conn->err;
}

/**
 * Connect a netconn to a specific remote IP address and port.
 *
 * @param conn the netconn to connect
 * @param addr the remote IP address to connect to
 * @param port the remote port to connect to (no used for RAW)
 * @return NTCPIP_ERR_OK if connected, return value of tcp_/udp_/raw_connect otherwise
 */
ntcpip_Err
ntcpip_netconnConnect(struct ntcpip__netconn *conn, struct ntcpip_ipAddr *addr, Uint16 port)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnConnect: invalid conn", (conn != NULL), return NTCPIP_ERR_ARG;);

  msg.function = ntcpip__apimsgDoConnect;
  msg.msg.conn = conn;
  msg.msg.msg.bc.ipaddr = addr;
  msg.msg.msg.bc.port = port;
  /* This is the only function which need to not block tcpip_thread */
  ntcpip__tcpipApiMsg(&msg);
  return conn->err;
}

/**
 * Disconnect a netconn from its current peer (only valid for UDP netconns).
 *
 * @param conn the netconn to disconnect
 * @return TODO: return value is not set here...
 */
ntcpip_Err
ntcpip_netconnDisconnect(struct ntcpip__netconn *conn)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnDisconnect: invalid conn", (conn != NULL), return NTCPIP_ERR_ARG;);

  msg.function = ntcpip__apimsgDoDisconnect;
  msg.msg.conn = conn;
  NTCPIP__APIMSG(&msg);
  return conn->err;
}

/**
 * Set a TCP netconn into listen mode
 *
 * @param conn the tcp netconn to set to listen mode
 * @param backlog the listen backlog, only used if NTCPIP__TCP_LISTEN_BACKLOG==1
 * @return NTCPIP_ERR_OK if the netconn was set to listen (UDP and RAW netconns
 *         don't return any error (yet?))
 */
ntcpip_Err
ntcpip_netconnlistenWithBacklog(struct ntcpip__netconn *conn, Uint8 backlog)
{
  struct ntcpip__apiMsg msg;

  /* This does no harm. If NTCPIP__TCP_LISTEN_BACKLOG is off, backlog is unused. */
  NTCPIP__LWIP_UNUSED_ARG(backlog);

  NTCPIP__LWIP_ERROR("ntcpip_netconnListen: invalid conn", (conn != NULL), return NTCPIP_ERR_ARG;);

  msg.function = ntcpip__apimsgDoListen;
  msg.msg.conn = conn;
#if NTCPIP__TCP_LISTEN_BACKLOG
  msg.msg.msg.lb.backlog = backlog;
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
  NTCPIP__APIMSG(&msg);
  return conn->err;
}

/**
 * Accept a new connection on a TCP listening netconn.
 *
 * @param 	conn 	the TCP listen netconn
 * @return the newly accepted netconn or NULL on timeout
 */
struct ntcpip__netconn *
ntcpip_netconnAccept(struct ntcpip__netconn *conn)
{
  struct ntcpip__netconn *newconn;

  NTCPIP__LWIP_ERROR("ntcpip_netconnAccept: invalid conn",       (conn != NULL),                      return NULL;);
  NTCPIP__LWIP_ERROR("ntcpip_netconnAccept: invalid acceptmbox", (conn->acceptmbox != NTCPIP__SYS_MBOX_NULL), return NULL;);

#if NTCPIP__LWIP_SO_RCVTIMEO
  if (ntcpip__sysArchMboxFetch(conn->acceptmbox, (void *)&newconn, conn->recv_timeout) == NTCPIP__SYS_ARCH_TIMEOUT) {
    newconn = NULL;
  } else
#else
  ntcpip__sysArchMboxFetch(conn->acceptmbox, (void *)&newconn, 0);
#endif /* NTCPIP__LWIP_SO_RCVTIMEO*/
  {
    /* Register event with callback */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVMINUS, 0);

#if NTCPIP__TCP_LISTEN_BACKLOG
    if (newconn != NULL) {
      /* Let the stack know that we have accepted the connection. */
      struct ntcpip__apiMsg msg;
      msg.function = ntcpip__apimsgDoRecv;
      msg.msg.conn = conn;
      NTCPIP__APIMSG(&msg);
    }
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
  }

  return newconn;
}

/**
 * Receive data (in form of a netbuf containing a packet buffer) from a netconn
 *
 * @param 	conn 	the netconn from which to receive data
 * @return 	a new netbuf containing received data or NULL on memory error or timeout
 */
struct ntcpip__netbuf *
ntcpip_netconnRecv(struct ntcpip__netconn *conn)
{
  struct ntcpip__apiMsg msg;
  struct ntcpip__netbuf *buf = NULL;
  struct ntcpip_pbuf *p;
  Uint16 len;

  NTCPIP__LWIP_ERROR("ntcpip_netconnRecv: invalid conn",  (conn != NULL), return NULL;);

  if (conn->recvmbox == NTCPIP__SYS_MBOX_NULL) {
    /* @todo: should calling ntcpip_netconnRecv on a TCP listen conn be fatal (NTCPIP_ERR_CONN)?? */
    /* TCP listen conns don't have a recvmbox! */
    conn->err = NTCPIP_ERR_CONN;
    return NULL;
  }

  if (NTCPIP_ERR_IS_FATAL(conn->err)) {
    return NULL;
  }

  if (conn->type == NTCPIP_NETCONN_TCP) {
#if NTCPIP__LWIP_TCP
    if (conn->state == NTCPIP__NETCONN_LISTEN) {
      /* @todo: should calling ntcpip_netconnRecv on a TCP listen conn be fatal?? */
      conn->err = NTCPIP_ERR_CONN;
      return NULL;
    }

    buf = ntcpip__mempMalloc(MEMP_NETBUF);

    if (buf == NULL) {
      conn->err = NTCPIP_ERR_MEM;
      return NULL;
    }

#if NTCPIP__LWIP_SO_RCVTIMEO
    if (ntcpip__sysArchMboxFetch(conn->recvmbox, (void *)&p, conn->recv_timeout)==NTCPIP__SYS_ARCH_TIMEOUT) {
      ntcpip__mempFree(MEMP_NETBUF, buf);
      conn->err = NTCPIP_ERR_TIMEOUT;
      return NULL;
    }
#else
    ntcpip__sysArchMboxFetch(conn->recvmbox, (void *)&p, 0);
#endif /* NTCPIP__LWIP_SO_RCVTIMEO*/

    if (p != NULL) {
      len = p->tot_len;
      NTCPIP__SYS_ARCH_DEC(conn->recv_avail, len);
    } else {
      len = 0;
    }

    /* Register event with callback */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVMINUS, len);

    /* If we are closed, we indicate that we no longer wish to use the socket */
    if (p == NULL) {
      ntcpip__mempFree(MEMP_NETBUF, buf);
      /* Avoid to lose any previous error code */
      if (conn->err == NTCPIP_ERR_OK) {
        conn->err = NTCPIP_ERR_CLSD;
      }
      return NULL;
    }

    buf->p = p;
    buf->ptr = p;
    buf->port = 0;
    buf->addr = NULL;

    /* Let the stack know that we have taken the data. */
    msg.function = ntcpip__apimsgDoRecv;
    msg.msg.conn = conn;
    if (buf != NULL) {
      msg.msg.msg.r.len = buf->p->tot_len;
    } else {
      msg.msg.msg.r.len = 1;
    }
    NTCPIP__APIMSG(&msg);
#endif /* NTCPIP__LWIP_TCP */
  } else {
#if (NTCPIP__LWIP_UDP || NTCPIP__LWIP_RAW)
#if NTCPIP__LWIP_SO_RCVTIMEO
    if (ntcpip__sysArchMboxFetch(conn->recvmbox, (void *)&buf, conn->recv_timeout)==NTCPIP__SYS_ARCH_TIMEOUT) {
      buf = NULL;
    }
#else
    ntcpip__sysArchMboxFetch(conn->recvmbox, (void *)&buf, 0);
#endif /* NTCPIP__LWIP_SO_RCVTIMEO*/
    if (buf!=NULL) {
      NTCPIP__SYS_ARCH_DEC(conn->recv_avail, buf->p->tot_len);
      /* Register event with callback */
      NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVMINUS, buf->p->tot_len);
    }
#endif /* (NTCPIP__LWIP_UDP || NTCPIP__LWIP_RAW) */
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__API_LIB_DEBUG, ("ntcpip_netconnRecv: received %p (err %d)\n", (void *)buf, conn->err));

  return buf;
}

/**
 * Send data (in form of a netbuf) to a specific remote IP address and port.
 * Only to be used for UDP and RAW netconns (not TCP).
 *
 * @param conn the netconn over which to send data
 * @param buf a netbuf containing the data to send
 * @param addr the remote IP address to which to send the data
 * @param port the remote port to which to send the data
 * @return NTCPIP_ERR_OK if data was sent, any other ntcpip_Err on error
 */
ntcpip_Err
ntcpip_netconnSendTo(struct ntcpip__netconn *conn, struct ntcpip__netbuf *buf, const struct ntcpip_ipAddr *addr, Uint16 port)
{
  if (buf != NULL) {
    buf->addr = addr;
    buf->port = port;
    return ntcpip_netconnSend(conn, buf);
  }
  return NTCPIP_ERR_VAL;
}

/**
 * Send data over a UDP or RAW netconn (that is already connected).
 *
 * @param conn the UDP or RAW netconn over which to send data
 * @param buf a netbuf containing the data to send
 * @return NTCPIP_ERR_OK if data was sent, any other ntcpip_Err on error
 */
ntcpip_Err
ntcpip_netconnSend(struct ntcpip__netconn *conn, struct ntcpip__netbuf *buf)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnSend: invalid conn",  (conn != NULL), return NTCPIP_ERR_ARG;);

  NTCPIP__LWIP_DEBUGF(NTCPIP__API_LIB_DEBUG, ("ntcpip_netconnSend: sending %"U16_F" bytes\n", buf->p->tot_len));
  msg.function = ntcpip__apimsgDoSend;
  msg.msg.conn = conn;
  msg.msg.msg.b = buf;
  NTCPIP__APIMSG(&msg);
  return conn->err;
}

/**
 * Send data over a TCP netconn.
 *
 * @param conn the TCP netconn over which to send data
 * @param dataptr pointer to the application buffer that contains the data to send
 * @param size size of the application data to send
 * @param apiflags combination of following flags :
 * - NTCPIP_NETCONN_COPY (0x01) data will be copied into memory belonging to the stack
 * - NTCPIP_NETCONN_MORE (0x02) for TCP connection, PSH flag will be set on last segment sent
 * @return NTCPIP_ERR_OK if data was sent, any other ntcpip_Err on error
 */
ntcpip_Err
ntcpip_netconnWrite(struct ntcpip__netconn *conn, const void *dataptr, size_t size, Uint8 apiflags)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnWrite: invalid conn",  (conn != NULL), return NTCPIP_ERR_ARG;);
  NTCPIP__LWIP_ERROR("ntcpip_netconnWrite: invalid conn->type",  (conn->type == NTCPIP_NETCONN_TCP), return NTCPIP_ERR_VAL;);

  msg.function = ntcpip__apimsgDoWrite;
  msg.msg.conn = conn;
  msg.msg.msg.w.dataptr = dataptr;
  msg.msg.msg.w.apiflags = apiflags;
  msg.msg.msg.w.len = size;
  /* For locking the core: this _can_ be delayed on low memory/low send buffer,
     but if it is, this is done inside ntcpip__apiMsg.c:ntcpip__apimsgDoWrite(), so we can use the
     non-blocking version here. */
  NTCPIP__APIMSG(&msg);
  return conn->err;
}

/**
 * Close a TCP netconn (doesn't delete it).
 *
 * @param conn the TCP netconn to close
 * @return NTCPIP_ERR_OK if the netconn was closed, any other ntcpip_Err on error
 */
ntcpip_Err
ntcpip_netconnClose(struct ntcpip__netconn *conn)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnClose: invalid conn",  (conn != NULL), return NTCPIP_ERR_ARG;);

  msg.function = ntcpip__apimsgDoClose;
  msg.msg.conn = conn;
  ntcpip__tcpipApiMsg(&msg);
  return conn->err;
}

#if NTCPIP__LWIP_IGMP
/**
 * Join multicast groups for UDP netconns.
 *
 * @param conn the UDP netconn for which to change multicast addresses
 * @param multiaddr IP address of the multicast group to join or leave
 * @param interface the IP address of the network interface on which to send
 *                  the igmp message
 * @param join_or_leave flag whether to send a join- or leave-message
 * @return NTCPIP_ERR_OK if the action was taken, any ntcpip_Err on error
 */
ntcpip_Err
ntcpip_netconnjoinLeaveGroup(struct ntcpip__netconn *conn,
                         struct ntcpip_ipAddr *multiaddr,
                         struct ntcpip_ipAddr *interface,
                         enum ntcpip__netconnIgmp join_or_leave)
{
  struct ntcpip__apiMsg msg;

  NTCPIP__LWIP_ERROR("ntcpip_netconnjoinLeaveGroup: invalid conn",  (conn != NULL), return NTCPIP_ERR_ARG;);

  msg.function = ntcpip__apimsgDoJoinLeaveGroup;
  msg.msg.conn = conn;
  msg.msg.msg.jl.multiaddr = multiaddr;
  msg.msg.msg.jl.interface = interface;
  msg.msg.msg.jl.join_or_leave = join_or_leave;
  NTCPIP__APIMSG(&msg);
  return conn->err;
}
#endif /* NTCPIP__LWIP_IGMP */

#if NTCPIP__LWIP_DNS
/**
 * Execute a DNS query, only one IP address is returned
 *
 * @param name a string representation of the DNS host name to query
 * @param addr a preallocated struct ntcpip_ipAddr where to store the resolved IP address
 * @return NTCPIP_ERR_OK: resolving succeeded
 *         NTCPIP_ERR_MEM: memory error, try again later
 *         NTCPIP_ERR_ARG: dns client not initialized or invalid hostname
 *         NTCPIP_ERR_VAL: dns server response was invalid
 */
ntcpip_Err
ntcpip_netconnGetHostByName(const char *name, struct ntcpip_ipAddr *addr)
{
  struct ntcpip__dnsApiMsg msg;
  ntcpip_Err err;
  ntcpip__SysSema sem;

  NTCPIP__LWIP_ERROR("ntcpip_netconnGetHostByName: invalid name", (name != NULL), return NTCPIP_ERR_ARG;);
  NTCPIP__LWIP_ERROR("ntcpip_netconnGetHostByName: invalid addr", (addr != NULL), return NTCPIP_ERR_ARG;);

  sem = ntcpip__sysSemNew(0);
  if (sem == NTCPIP__SYS_SEM_NULL) {
    return NTCPIP_ERR_MEM;
  }

  msg.name = name;
  msg.addr = addr;
  msg.err = &err;
  msg.sem = sem;

  ntcpip__tcpipCb(do_gethostbyname, &msg);
  ntcpip__sysSemWait(sem);
  ntcpip__sysSemFree(sem);

  return err;
}
#endif /* NTCPIP__LWIP_DNS*/

#endif /* NTCPIP__LWIP_NETCONN */

