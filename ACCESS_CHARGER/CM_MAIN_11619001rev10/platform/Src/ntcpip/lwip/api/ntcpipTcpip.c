/**
 * @file
 * Sequential API Main thread module
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if !NTCPIP__NO_SYS /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/sys.h"
#include "../memp.h"
#include "ntcpip/pbuf.h"
#include "../ip_frag.h"
#include "ntcpip/tcp.h"
#include "../autoip.h"
#include "../dhcp.h"
#include "ntcpip/igmp.h"
#include "../dns.h"
#include "ntcpip/tcpip.h"
#include "../init.h"
#include "../../netif/etharp.h"
#include "../../netif/ppp_oe.h"

/* global variables */
static void (* tcpip_init_done)(void *arg);
static void *tcpip_init_done_arg;
static ntcpip__SysMbox mbox = NTCPIP__SYS_MBOX_NULL;

#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
/** The global semaphore to lock the stack. */
ntcpip__SysSema lock_tcpip_core;
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */

#if NTCPIP__LWIP_TCP
/* global variable that shows if the tcp timer is currently scheduled or not */
static int tcpip_tcp_timer_active;

/**
 * Timer callback function that calls ntcpip__tcpTmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
tcpip_tcp_timer(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);

  /* call TCP timer handler */
  ntcpip__tcpTmr();
  /* timer still needed? */
  if (ntcpip__tcpActivePcbs || ntcpip__tcpTwPcb) {
    /* restart timer */
    ntcpip__sysTimeout(NTCPIP__TCP_TMR_INTERVAL, tcpip_tcp_timer, NULL);
  } else {
    /* disable timer */
    tcpip_tcp_timer_active = 0;
  }
}

#if !NTCPIP__NO_SYS
/**
 * Called from TCP_REG when registering a new PCB:
 * the reason is to have the TCP timer only running when
 * there are active (or time-wait) PCBs.
 */
void
ntcpip__tcpipTimerNeeded(void)
{
  /* timer is off but needed again? */
  if (!tcpip_tcp_timer_active && (ntcpip__tcpActivePcbs || ntcpip__tcpTwPcb)) {
    /* enable and start timer */
    tcpip_tcp_timer_active = 1;
    ntcpip__sysTimeout(NTCPIP__TCP_TMR_INTERVAL, tcpip_tcp_timer, NULL);
  }
}
#endif /* !NTCPIP__NO_SYS */
#endif /* NTCPIP__LWIP_TCP */

#if NTCPIP__IP_REASSEMBLY
/**
 * Timer callback function that calls ntcpip__ipReassTmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
ip_reass_timer(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip: ntcpip__ipReassTmr()\n"));
  ntcpip__ipReassTmr();
  ntcpip__sysTimeout(IP_TMR_INTERVAL, ip_reass_timer, NULL);
}
#endif /* NTCPIP__IP_REASSEMBLY */

#if NTCPIP__LWIP_ARP
/**
 * Timer callback function that calls ntcpip__etharpTmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
arp_timer(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip: ntcpip__etharpTmr()\n"));
  ntcpip__etharpTmr();
  ntcpip__sysTimeout(ARP_TMR_INTERVAL, arp_timer, NULL);
}
#endif /* NTCPIP__LWIP_ARP */

#if NTCPIP__LWIP_DHCP
/**
 * Timer callback function that calls ntcpip__dhcpCoarseTmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
dhcp_timer_coarse(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip: ntcpip__dhcpCoarseTmr()\n"));
  ntcpip__dhcpCoarseTmr();
  ntcpip__sysTimeout(DHCP_COARSE_TIMER_MSECS, dhcp_timer_coarse, NULL);
}

/**
 * Timer callback function that calls ntcpip__dhcpFineTmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
dhcp_timer_fine(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip: ntcpip__dhcpFineTmr()\n"));
  ntcpip__dhcpFineTmr();
  ntcpip__sysTimeout(DHCP_FINE_TIMER_MSECS, dhcp_timer_fine, NULL);
}
#endif /* NTCPIP__LWIP_DHCP */

#if NTCPIP__LWIP_AUTOIP
/**
 * Timer callback function that calls ntcpip__autoipTmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
autoip_timer(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip: ntcpip__autoipTmr()\n"));
  ntcpip__autoipTmr();
  ntcpip__sysTimeout(AUTOIP_TMR_INTERVAL, autoip_timer, NULL);
}
#endif /* NTCPIP__LWIP_AUTOIP */

#if NTCPIP__LWIP_IGMP
/**
 * Timer callback function that calls ntcpip__igmpTmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
igmp_timer(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip: ntcpip__igmpTmr()\n"));
  ntcpip__igmpTmr();
  ntcpip__sysTimeout(NTCPIP__IGMP_TMR_INTERVAL, igmp_timer, NULL);
}
#endif /* NTCPIP__LWIP_IGMP */

#if NTCPIP__LWIP_DNS
/**
 * Timer callback function that calls dns_tmr() and reschedules itself.
 *
 * @param arg unused argument
 */
static void
dns_timer(void *arg)
{
  NTCPIP__LWIP_UNUSED_ARG(arg);
  NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip: dns_tmr()\n"));
  dns_tmr();
  ntcpip__sysTimeout(DNS_TMR_INTERVAL, dns_timer, NULL);
}
#endif /* NTCPIP__LWIP_DNS */

/**
 * The main lwIP thread. This thread has exclusive access to lwIP core functions
 * (unless access to them is not locked). Other threads communicate with this
 * thread using message boxes.
 *
 * It also starts all the timers to make sure they are running in the right
 * thread context.
 *
 * @param arg unused argument
 */
static void
tcpip_thread(void *arg)
{
  struct ntcpip__msg *msg;
  NTCPIP__LWIP_UNUSED_ARG(arg);

#if NTCPIP__IP_REASSEMBLY
  ntcpip__sysTimeout(IP_TMR_INTERVAL, ip_reass_timer, NULL);
#endif /* NTCPIP__IP_REASSEMBLY */
#if NTCPIP__LWIP_ARP
  ntcpip__sysTimeout(ARP_TMR_INTERVAL, arp_timer, NULL);
#endif /* NTCPIP__LWIP_ARP */
#if NTCPIP__LWIP_DHCP
  ntcpip__sysTimeout(DHCP_COARSE_TIMER_MSECS, dhcp_timer_coarse, NULL);
  ntcpip__sysTimeout(DHCP_FINE_TIMER_MSECS, dhcp_timer_fine, NULL);
#endif /* NTCPIP__LWIP_DHCP */
#if NTCPIP__LWIP_AUTOIP
  ntcpip__sysTimeout(AUTOIP_TMR_INTERVAL, autoip_timer, NULL);
#endif /* NTCPIP__LWIP_AUTOIP */
#if NTCPIP__LWIP_IGMP
  ntcpip__sysTimeout(NTCPIP__IGMP_TMR_INTERVAL, igmp_timer, NULL);
#endif /* NTCPIP__LWIP_IGMP */
#if NTCPIP__LWIP_DNS
  ntcpip__sysTimeout(DNS_TMR_INTERVAL, dns_timer, NULL);
#endif /* NTCPIP__LWIP_DNS */

  if (tcpip_init_done != NULL) {
    tcpip_init_done(tcpip_init_done_arg);
  }

  NTCPIP__LOCK_CORE();
  while (1) {                          /* MAIN Loop */
    ntcpip__sysMboxFetch(mbox, (void *)&msg);
    switch (msg->type) {
#if NTCPIP__LWIP_NETCONN
    case NTCPIP__MSG_API:
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip_thread: API message %p\n", (void *)msg));
      msg->msg.apimsg->function(&(msg->msg.apimsg->msg));
      break;
#endif /* NTCPIP__LWIP_NETCONN */

    case NTCPIP__MSG_INPKT:
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip_thread: PACKET %p\n", (void *)msg));
#if NTCPIP__LWIP_ARP
      if (msg->msg.inp.netif->flags & NTCPIP_NETIF_ETHARP) {
        ntcpip__etharpInput(msg->msg.inp.p, msg->msg.inp.netif);
      } else
#endif /* NTCPIP__LWIP_ARP */
      { ntcpip__ipInput(msg->msg.inp.p, msg->msg.inp.netif);
      }
      ntcpip__mempFree(MEMP_TCPIP_MSG_INPKT, msg);
      break;

#if NTCPIP__LWIP_NETIF_API
    case NTCPIP__MSG_NETIFAPI:
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip_thread: Netif API message %p\n", (void *)msg));
      msg->msg.netifapimsg->function(&(msg->msg.netifapimsg->msg));
      break;
#endif /* NTCPIP__LWIP_NETIF_API */

    case NTCPIP__MSG_CALLBACK:
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip_thread: CALLBACK %p\n", (void *)msg));
      msg->msg.cb.f(msg->msg.cb.ctx);
      ntcpip__mempFree(MEMP_TCPIP_MSG_API, msg);
      break;

    case NTCPIP__MSG_TIMEOUT:
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip_thread: TIMEOUT %p\n", (void *)msg));
      ntcpip__sysTimeout(msg->msg.tmo.msecs, msg->msg.tmo.h, msg->msg.tmo.arg);
      ntcpip__mempFree(MEMP_TCPIP_MSG_API, msg);
      break;
    case NTCPIP__MSG_UNTIMEOUT:
      NTCPIP__LWIP_DEBUGF(NTCPIP__TCPIP_DEBUG, ("tcpip_thread: UNTIMEOUT %p\n", (void *)msg));
      ntcpip__sysUntimeout(msg->msg.tmo.h, msg->msg.tmo.arg);
      ntcpip__mempFree(MEMP_TCPIP_MSG_API, msg);
      break;

    default:
      break;
    }
  }
}

/**
 * Pass a received packet to tcpip_thread for input processing
 *
 * @param p the received packet, p->payload pointing to the Ethernet header or
 *          to an IP header (if netif doesn't got NTCPIP_NETIF_ETHARP flag)
 * @param inp the network interface on which the packet was received
 */
ntcpip_Err
ntcpip_tcpipInput(struct ntcpip_pbuf *p, struct ntcpip__netif *inp)
{
  struct ntcpip__msg *msg;

  if (mbox != NTCPIP__SYS_MBOX_NULL) {
    msg = ntcpip__mempMalloc(MEMP_TCPIP_MSG_INPKT);
    if (msg == NULL) {
      return NTCPIP_ERR_MEM;
    }

    msg->type = NTCPIP__MSG_INPKT;
    msg->msg.inp.p = p;
    msg->msg.inp.netif = inp;
    if (ntcpip__sysMboxTrypost(mbox, msg) != NTCPIP_ERR_OK) {
      ntcpip__mempFree(MEMP_TCPIP_MSG_INPKT, msg);
      return NTCPIP_ERR_MEM;
    }
    return NTCPIP_ERR_OK;
  }
  return NTCPIP_ERR_VAL;
}

/**
 * Call a specific function in the thread context of
 * tcpip_thread for easy access synchronization.
 * A function called in that way may access lwIP core code
 * without fearing concurrent access.
 *
 * @param f the function to call
 * @param ctx parameter passed to f
 * @param block 1 to block until the request is posted, 0 to non-blocking mode
 * @return NTCPIP_ERR_OK if the function was called, another ntcpip_Err if not
 */
ntcpip_Err
ntcpip_tcpipCallbackWithBlock(void (*f)(void *ctx), void *ctx, Uint8 block)
{
  struct ntcpip__msg *msg;

  if (mbox != NTCPIP__SYS_MBOX_NULL) {
    msg = ntcpip__mempMalloc(MEMP_TCPIP_MSG_API);
    if (msg == NULL) {
      return NTCPIP_ERR_MEM;
    }

    msg->type = NTCPIP__MSG_CALLBACK;
    msg->msg.cb.f = f;
    msg->msg.cb.ctx = ctx;
    if (block) {
      ntcpip__sysMboxPost(mbox, msg);
    } else {
      if (ntcpip__sysMboxTrypost(mbox, msg) != NTCPIP_ERR_OK) {
        ntcpip__mempFree(MEMP_TCPIP_MSG_API, msg);
        return NTCPIP_ERR_MEM;
      }
    }
    return NTCPIP_ERR_OK;
  }
  return NTCPIP_ERR_VAL;
}

/**
 * call ntcpip__sysTimeout in tcpip_thread
 *
 * @param msec time in miliseconds for timeout
 * @param h function to be called on timeout
 * @param arg argument to pass to timeout function h
 * @return NTCPIP_ERR_MEM on memory error, NTCPIP_ERR_OK otherwise
 */
ntcpip_Err
ntcpip__tcpipTimeout(Uint32 msecs, ntcpip__sysTimeoutHandler h, void *arg)
{
  struct ntcpip__msg *msg;

  if (mbox != NTCPIP__SYS_MBOX_NULL) {
    msg = ntcpip__mempMalloc(MEMP_TCPIP_MSG_API);
    if (msg == NULL) {
      return NTCPIP_ERR_MEM;
    }

    msg->type = NTCPIP__MSG_TIMEOUT;
    msg->msg.tmo.msecs = msecs;
    msg->msg.tmo.h = h;
    msg->msg.tmo.arg = arg;
    ntcpip__sysMboxPost(mbox, msg);
    return NTCPIP_ERR_OK;
  }
  return NTCPIP_ERR_VAL;
}

/**
 * call ntcpip__sysUntimeout in tcpip_thread
 *
 * @param msec time in miliseconds for timeout
 * @param h function to be called on timeout
 * @param arg argument to pass to timeout function h
 * @return NTCPIP_ERR_MEM on memory error, NTCPIP_ERR_OK otherwise
 */
ntcpip_Err
ntcpip__tcpipUntimeout(ntcpip__sysTimeoutHandler h, void *arg)
{
  struct ntcpip__msg *msg;

  if (mbox != NTCPIP__SYS_MBOX_NULL) {
    msg = ntcpip__mempMalloc(MEMP_TCPIP_MSG_API);
    if (msg == NULL) {
      return NTCPIP_ERR_MEM;
    }

    msg->type = NTCPIP__MSG_UNTIMEOUT;
    msg->msg.tmo.h = h;
    msg->msg.tmo.arg = arg;
    ntcpip__sysMboxPost(mbox, msg);
    return NTCPIP_ERR_OK;
  }
  return NTCPIP_ERR_VAL;
}

#if NTCPIP__LWIP_NETCONN
/**
 * Call the lower part of a netconn_* function
 * This function is then running in the thread context
 * of tcpip_thread and has exclusive access to lwIP core code.
 *
 * @param apimsg a struct containing the function to call and its parameters
 * @return NTCPIP_ERR_OK if the function was called, another ntcpip_Err if not
 */
ntcpip_Err
ntcpip__tcpipApiMsg(struct ntcpip__apiMsg *apimsg)
{
  struct ntcpip__msg msg;
  
  if (mbox != NTCPIP__SYS_MBOX_NULL) {
    msg.type = NTCPIP__MSG_API;
    msg.msg.apimsg = apimsg;
    ntcpip__sysMboxPost(mbox, &msg);
    ntcpip__sysArchSemWait(apimsg->msg.conn->op_completed, 0);
    return NTCPIP_ERR_OK;
  }
  return NTCPIP_ERR_VAL;
}

#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
/**
 * Call the lower part of a netconn_* function
 * This function has exclusive access to lwIP core code by locking it
 * before the function is called.
 *
 * @param apimsg a struct containing the function to call and its parameters
 * @return NTCPIP_ERR_OK (only for compatibility fo ntcpip__tcpipApiMsg())
 */
ntcpip_Err
tcpip_apimsg_lock(struct ntcpip__apiMsg *apimsg)
{
  NTCPIP__LOCK_CORE();
  apimsg->function(&(apimsg->msg));
  NTCPIP__UNLOCK_CORE();
  return NTCPIP_ERR_OK;

}
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */
#endif /* NTCPIP__LWIP_NETCONN */

#if NTCPIP__LWIP_NETIF_API
#if !NTCPIP__LWIP_TCPIP_CORE_LOCKING
/**
 * Much like ntcpip__tcpipApiMsg, but calls the lower part of a netifapi_*
 * function.
 *
 * @param netifapimsg a struct containing the function to call and its parameters
 * @return error code given back by the function that was called
 */
ntcpip_Err
ntcpip__tcpipNetifApi(struct ntcpip__netifapiMsg* netifapimsg)
{
  struct ntcpip__msg msg;
  
  if (mbox != NTCPIP__SYS_MBOX_NULL) {
    netifapimsg->msg.sem = ntcpip__sysSemNew(0);
    if (netifapimsg->msg.sem == NTCPIP__SYS_SEM_NULL) {
      netifapimsg->msg.err = NTCPIP_ERR_MEM;
      return netifapimsg->msg.err;
    }
    
    msg.type = NTCPIP__MSG_NETIFAPI;
    msg.msg.netifapimsg = netifapimsg;
    ntcpip__sysMboxPost(mbox, &msg);
    ntcpip__sysSemWait(netifapimsg->msg.sem);
    ntcpip__sysSemFree(netifapimsg->msg.sem);
    return netifapimsg->msg.err;
  }
  return NTCPIP_ERR_VAL;
}
#else /* !NTCPIP__LWIP_TCPIP_CORE_LOCKING */
/**
 * Call the lower part of a netifapi_* function
 * This function has exclusive access to lwIP core code by locking it
 * before the function is called.
 *
 * @param netifapimsg a struct containing the function to call and its parameters
 * @return NTCPIP_ERR_OK (only for compatibility fo ntcpip__tcpipNetifApi())
 */
ntcpip_Err
tcpip_netifapi_lock(struct ntcpip__netifapiMsg* netifapimsg)
{
  NTCPIP__LOCK_CORE();  
  netifapimsg->function(&(netifapimsg->msg));
  NTCPIP__UNLOCK_CORE();
  return netifapimsg->msg.err;
}
#endif /* !NTCPIP__LWIP_TCPIP_CORE_LOCKING */
#endif /* NTCPIP__LWIP_NETIF_API */

/**
 * Initialize this module:
 * - initialize all sub modules
 * - start the tcpip_thread
 *
 * @param initfunc a function to call when tcpip_thread is running and finished initializing
 * @param arg argument to pass to initfunc
 */
void
ntcpip__tcpipInit(void (* initfunc)(void *), void *arg)
{
  ntcpip__lwipInit();

  tcpip_init_done = initfunc;
  tcpip_init_done_arg = arg;
  mbox = ntcpip__sysMboxNew(NTCPIP__TCPIP_MBOX_SIZE);
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
  lock_tcpip_core = ntcpip__sysSemNew(1);
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */

  ntcpip__sysThreadNew(NTCPIP__TCPIP_THREAD_NAME, tcpip_thread, NULL, NTCPIP__TCPIP_THREAD_STACKSIZE, NTCPIP__TCPIP_THREAD_PRIO);
}

/**
 * Simple callback function used with tcpip_callback to free a pbuf
 * (ntcpip_pbufFree has a wrong signature for tcpip_callback)
 *
 * @param p The pbuf (chain) to be dereferenced.
 */
static void
pbuf_free_int(void *p)
{
  struct ntcpip_pbuf *q = p;
  ntcpip_pbufFree(q);
}

/**
 * A simple wrapper function that allows you to free a pbuf from interrupt context.
 *
 * @param p The pbuf (chain) to be dereferenced.
 * @return NTCPIP_ERR_OK if callback could be enqueued, an ntcpip_Err if not
 */
ntcpip_Err
ntcpip_pbufFreeCallback(struct ntcpip_pbuf *p)
{
  return ntcpip_tcpipCallbackWithBlock(pbuf_free_int, p, 0);
}

#if 0
/**
 * A simple wrapper function that allows you to free heap memory from
 * interrupt context.
 *
 * @param m the heap memory to free
 * @return NTCPIP_ERR_OK if callback could be enqueued, an ntcpip_Err if not
 */
ntcpip_Err
mem_free_callback(void *m)
{
  return ntcpip__tcpipCallbackWithBlock(ntcpip__memFree, m, 0);
}
#endif

#endif /* !NTCPIP__NO_SYS */

