/**
 * @file
 * API functions for name resolving
 *
 */

/*
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Simon Goldschmidt
 *
 */

#include "../netdb.h"

#if NTCPIP__LWIP_DNS && NTCPIP__LWIP_SOCKET

#include "ntcpip/err.h"
#include "../mem.h"
#include "ntcpip/ip_addr.h"
#include "ntcpip/api.h"

#include <string.h>
#include <stdlib.h>

/** helper struct for gethostbyname_r to access the char* buffer */
struct gethostbyname_r_helper {
  struct ntcpip_ipAddr *addrs;
  struct ntcpip_ipAddr addr;
  char *aliases;
};

/** h_errno is exported in netdb.h for access by applications. */
#if LWIP_DNS_API_DECLARE_H_ERRNO
int h_errno;
#endif /* LWIP_DNS_API_DECLARE_H_ERRNO */

/** define "hostent" variables storage: 0 if we use a static (but unprotected)
 * set of variables for lwip_gethostbyname, 1 if we use a local storage */
#ifndef LWIP_DNS_API_HOSTENT_STORAGE
#define LWIP_DNS_API_HOSTENT_STORAGE 0
#endif

/** define "hostent" variables storage */
#if LWIP_DNS_API_HOSTENT_STORAGE
#define HOSTENT_STORAGE
#else
#define HOSTENT_STORAGE static
#endif /* LWIP_DNS_API_STATIC_HOSTENT */

/**
 * Returns an entry containing addresses of address family AF_INET
 * for the host with name name.
 * Due to dns_gethostbyname limitations, only one address is returned.
 *
 * @param name the hostname to resolve
 * @return an entry containing addresses of address family AF_INET
 *         for the host with name name
 */
struct hostent*
lwip_gethostbyname(const char *name)
{
  ntcpip_Err err;
  struct ntcpip_ipAddr addr;

  /* buffer variables for lwip_gethostbyname() */
  HOSTENT_STORAGE struct hostent s_hostent;
  HOSTENT_STORAGE char *s_aliases;
  HOSTENT_STORAGE struct ntcpip_ipAddr s_hostent_addr;
  HOSTENT_STORAGE struct ntcpip_ipAddr *s_phostent_addr;

  /* query host IP address */
  err = ntcpip_netconnGetHostByName(name, &addr);
  if (err != NTCPIP_ERR_OK) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("lwip_gethostbyname(%s) failed, err=%d\n", name, err));
    h_errno = HOST_NOT_FOUND;
    return NULL;
  }

  /* fill hostent */
  s_hostent_addr = addr;
  s_phostent_addr = &s_hostent_addr;
  s_hostent.h_name = (char*)name;
  s_hostent.h_aliases = &s_aliases;
  s_hostent.h_addrtype = AF_INET;
  s_hostent.h_length = sizeof(struct ntcpip_ipAddr);
  s_hostent.h_addr_list = (char**)&s_phostent_addr;

#if NTCPIP__DNS_DEBUG
  /* dump hostent */
  NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_name           == %s\n", s_hostent.h_name));
  NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_aliases        == %p\n", s_hostent.h_aliases));
  if (s_hostent.h_aliases != NULL) {
    Uint8 idx;
    for ( idx=0; s_hostent.h_aliases[idx]; idx++) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_aliases[%i]->   == %p\n", idx, s_hostent.h_aliases[idx]));
      NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_aliases[%i]->   == %s\n", idx, s_hostent.h_aliases[idx]));
    }
  }
  NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_addrtype       == %d\n", s_hostent.h_addrtype));
  NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_length         == %d\n", s_hostent.h_length));
  NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_addr_list      == %p\n", s_hostent.h_addr_list));
  if (s_hostent.h_addr_list != NULL) {
    Uint8 idx;
    for ( idx=0; s_hostent.h_addr_list[idx]; idx++) {
      NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_addr_list[%i]   == %p\n", idx, s_hostent.h_addr_list[idx]));
      NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("hostent.h_addr_list[%i]-> == %s\n", idx, ntcpip_ipNtoa(s_hostent.h_addr_list[idx])));
    }
  }
#endif /* NTCPIP__DNS_DEBUG */

#if LWIP_DNS_API_HOSTENT_STORAGE
  /* this function should return the "per-thread" hostent after copy from s_hostent */
  return sys_thread_hostent(&s_hostent);
#else
  return &s_hostent;
#endif /* LWIP_DNS_API_HOSTENT_STORAGE */
}

/**
 * Thread-safe variant of lwip_gethostbyname: instead of using a static
 * buffer, this function takes buffer and errno pointers as arguments
 * and uses these for the result.
 *
 * @param name the hostname to resolve
 * @param ret pre-allocated struct where to store the result
 * @param buf pre-allocated buffer where to store additional data
 * @param buflen the size of buf
 * @param result pointer to a hostent pointer that is set to ret on success
 *               and set to zero on error
 * @param h_errnop pointer to an int where to store errors (instead of modifying
 *                 the global h_errno)
 * @return 0 on success, non-zero on error, additional error information
 *         is stored in *h_errnop instead of h_errno to be thread-safe
 */
int
lwip_gethostbyname_r(const char *name, struct hostent *ret, char *buf,
                size_t buflen, struct hostent **result, int *h_errnop)
{
  ntcpip_Err err;
  struct gethostbyname_r_helper *h;
  char *hostname;
  size_t namelen;
  int lh_errno;

  if (h_errnop == NULL) {
    /* ensure h_errnop is never NULL */
    h_errnop = &lh_errno;
  }

  if (result == NULL) {
    /* not all arguments given */
    *h_errnop = NTCPIP__EINVAL;
    return -1;
  }
  /* first thing to do: set *result to nothing */
  *result = NULL;
  if ((name == NULL) || (ret == NULL) || (buf == 0)) {
    /* not all arguments given */
    *h_errnop = NTCPIP__EINVAL;
    return -1;
  }

  namelen = strlen(name);
  if (buflen < (sizeof(struct gethostbyname_r_helper) + namelen + 1 + (NTCPIP__MEM_ALIGNMENT - 1))) {
    /* buf can't hold the data needed + a copy of name */
    *h_errnop = NTCPIP__ERANGE;
    return -1;
  }

  h = (struct gethostbyname_r_helper*)LWIP_MEM_ALIGN(buf);
  hostname = ((char*)h) + sizeof(struct gethostbyname_r_helper);

  /* query host IP address */
  err = ntcpip_netconnGetHostByName(name, &(h->addr));
  if (err != NTCPIP_ERR_OK) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__DNS_DEBUG, ("lwip_gethostbyname(%s) failed, err=%d\n", name, err));
    *h_errnop = NTCPIP__ENSRNOTFOUND;
    return -1;
  }

  /* copy the hostname into buf */
  NTCPIP__MEMCPY(hostname, name, namelen);
  hostname[namelen] = 0;

  /* fill hostent */
  h->addrs = &(h->addr);
  h->aliases = NULL;
  ret->h_name = (char*)hostname;
  ret->h_aliases = &(h->aliases);
  ret->h_addrtype = AF_INET;
  ret->h_length = sizeof(struct ntcpip_ipAddr);
  ret->h_addr_list = (char**)&(h->addrs);

  /* set result != NULL */
  *result = ret;

  /* return success */
  return 0;
}

/**
 * Frees one or more addrinfo structures returned by getaddrinfo(), along with
 * any additional storage associated with those structures. If the ai_next field
 * of the structure is not null, the entire list of structures is freed.
 *
 * @param ai struct addrinfo to free
 */
void
lwip_freeaddrinfo(struct addrinfo *ai)
{
  struct addrinfo *next;

  while (ai != NULL) {
    next = ai->ai_next;
    ntcpip__memFree(ai);
    ai = next;
  }
}

/**
 * Translates the name of a service location (for example, a host name) and/or
 * a service name and returns a set of socket addresses and associated
 * information to be used in creating a socket with which to address the
 * specified service.
 * Memory for the result is allocated internally and must be freed by calling
 * lwip_freeaddrinfo()!
 *
 * Due to a limitation in dns_gethostbyname, only the first address of a
 * host is returned.
 * Also, service names are not supported (only port numbers)!
 *
 * @param nodename descriptive name or address string of the host
 *                 (may be NULL -> local address)
 * @param servname port number as string of NULL 
 * @param hints structure containing input values that set socktype and protocol
 * @param res pointer to a pointer where to store the result (set to NULL on failure)
 * @return 0 on success, non-zero on failure
 */
int
lwip_getaddrinfo(const char *nodename, const char *servname,
       const struct addrinfo *hints, struct addrinfo **res)
{
  ntcpip_Err err;
  struct ntcpip_ipAddr addr;
  struct addrinfo *ai;
  struct sockaddr_in *sa = NULL;
  int port_nr = 0;
  size_t total_size;
  size_t namelen = 0;

  if (res == NULL) {
    return EAI_FAIL;
  }
  *res = NULL;
  if ((nodename == NULL) && (servname == NULL)) {
    return EAI_NONAME;
  }

  if (servname != NULL) {
    /* service name specified: convert to port number
     * @todo?: currently, only ASCII integers (port numbers) are supported! */
    port_nr = atoi(servname);
    if ((port_nr <= 0) || (port_nr > 0xffff)) {
      return EAI_SERVICE;
    }
  }

  if (nodename != NULL) {
    /* service location specified, try to resolve */
    err = ntcpip_netconnGetHostByName(nodename, &addr);
    if (err != NTCPIP_ERR_OK) {
      return EAI_FAIL;
    }
  } else {
    /* service location specified, use loopback address */
    addr.addr = ntcpip_htonl(NTCPIP_INADDR_LOOPBACK);
  }

  total_size = sizeof(struct addrinfo) + sizeof(struct sockaddr_in);
  if (nodename != NULL) {
    namelen = strlen(nodename);
    NTCPIP__LWIP_ASSERT("namelen is too long", (namelen + 1) <= (mem_size_t)-1);
    total_size += namelen + 1;
  }
  ai = ntcpip__memMalloc(total_size);
  if (ai == NULL) {
    goto memerr;
  }
  memset(ai, 0, total_size);
  sa = (struct sockaddr_in*)((Uint8*)ai + sizeof(struct addrinfo));
  /* set up sockaddr */
  sa->sin_addr.s_addr = addr.addr;
  sa->sin_family = AF_INET;
  sa->sin_len = sizeof(struct sockaddr_in);
  sa->sin_port = ntcpip_htons(port_nr);

  /* set up addrinfo */
  ai->ai_family = AF_INET;
  if (hints != NULL) {
    /* copy socktype & protocol from hints if specified */
    ai->ai_socktype = hints->ai_socktype;
    ai->ai_protocol = hints->ai_protocol;
  }
  if (nodename != NULL) {
    /* copy nodename to canonname if specified */
    ai->ai_canonname = ((char*)ai + sizeof(struct addrinfo) + sizeof(struct sockaddr_in));
    NTCPIP__MEMCPY(ai->ai_canonname, nodename, namelen);
    ai->ai_canonname[namelen] = 0;
  }
  ai->ai_addrlen = sizeof(struct sockaddr_in);
  ai->ai_addr = (struct sockaddr*)sa;

  *res = ai;

  return 0;
memerr:
  if (ai != NULL) {
    ntcpip__memFree(ai);
  }
  return EAI_MEMORY;
}

#endif /* NTCPIP__LWIP_DNS && NTCPIP__LWIP_SOCKET */

