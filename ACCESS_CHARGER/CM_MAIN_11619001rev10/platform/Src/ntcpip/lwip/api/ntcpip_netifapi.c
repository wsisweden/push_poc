/**
 * @file
 * Network Interface Sequential API module
 *
 */

/*
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_NETIF_API /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/netifapi.h"
#include "ntcpip/tcpip.h"

/**
 * Call ntcpip__netifAdd() inside the tcpip_thread context.
 */
void
ntcpip__netifapiDoNetifAdd( struct ntcpip__netifapiMsgMsg *msg)
{
  if (!ntcpip__netifAdd( msg->netif,
                  msg->msg.add.ipaddr,
                  msg->msg.add.netmask,
                  msg->msg.add.gw,
                  msg->msg.add.state,
                  msg->msg.add.init,
                  msg->msg.add.input)) {
    msg->err = NTCPIP_ERR_IF;
  } else {
    msg->err = NTCPIP_ERR_OK;
  }
  NTCPIP__NETIFAPI_ACK(msg);
}

/**
 * Call ntcpip__netifSetAddr() inside the tcpip_thread context.
 */
void
ntcpip__doNetifapiNetifSetAddr( struct ntcpip__netifapiMsgMsg *msg)
{
  ntcpip__netifSetAddr( msg->netif,
                  msg->msg.add.ipaddr,
                  msg->msg.add.netmask,
                  msg->msg.add.gw);
  msg->err = NTCPIP_ERR_OK;
  NTCPIP__NETIFAPI_ACK(msg);
}

/**
 * Call the "errtfunc" (or the "voidfunc" if "errtfunc" is NULL) inside the
 * tcpip_thread context.
 */
void
ntcpip__netifapiDoNetifCommon( struct ntcpip__netifapiMsgMsg *msg)
{
  if (msg->msg.common.errtfunc!=NULL) {
    msg->err =
    msg->msg.common.errtfunc(msg->netif);
  } else {
    msg->err = NTCPIP_ERR_OK;
    msg->msg.common.voidfunc(msg->netif);
  }
  NTCPIP__NETIFAPI_ACK(msg);
}

/**
 * Call ntcpip__netifAdd() in a thread-safe way by running that function inside the
 * tcpip_thread context.
 *
 * @note for params @see ntcpip__netifAdd()
 */
ntcpip_Err
ntcpip_netifapiNetifAdd(struct ntcpip__netif *netif,
                   struct ntcpip_ipAddr *ipaddr,
                   struct ntcpip_ipAddr *netmask,
                   struct ntcpip_ipAddr *gw,
                   void *state,
                   ntcpip_Err (* init)(struct ntcpip__netif *netif),
                   ntcpip_Err (* input)(struct ntcpip_pbuf *p, struct ntcpip__netif *netif))
{
  struct ntcpip__netifapiMsg msg;
  msg.function = ntcpip__netifapiDoNetifAdd;
  msg.msg.netif = netif;
  msg.msg.msg.add.ipaddr  = ipaddr;
  msg.msg.msg.add.netmask = netmask;
  msg.msg.msg.add.gw      = gw;
  msg.msg.msg.add.state   = state;
  msg.msg.msg.add.init    = init;
  msg.msg.msg.add.input   = input;
  NTCPIP__NETIFAPI(&msg);
  return msg.msg.err;
}

/**
 * Call ntcpip__netifSetAddr() in a thread-safe way by running that function inside the
 * tcpip_thread context.
 *
 * @note for params @see ntcpip__netifSetAddr()
 */
ntcpip_Err
ntcpip_netifapiNetifSetAddr(struct ntcpip__netif *netif,
                        struct ntcpip_ipAddr *ipaddr,
                        struct ntcpip_ipAddr *netmask,
                        struct ntcpip_ipAddr *gw)
{
  struct ntcpip__netifapiMsg msg;
  msg.function = ntcpip__doNetifapiNetifSetAddr;
  msg.msg.netif = netif;
  msg.msg.msg.add.ipaddr  = ipaddr;
  msg.msg.msg.add.netmask = netmask;
  msg.msg.msg.add.gw      = gw;
  NTCPIP__NETIFAPI(&msg);
  return msg.msg.err;
}

/**
 * call the "errtfunc" (or the "voidfunc" if "errtfunc" is NULL) in a thread-safe
 * way by running that function inside the tcpip_thread context.
 *
 * @note use only for functions where there is only "netif" parameter.
 */
ntcpip_Err
ntcpip_netifapiNetifCommon( struct ntcpip__netif *netif,
                       void  (* voidfunc)(struct ntcpip__netif *netif),
                       ntcpip_Err (* errtfunc)(struct ntcpip__netif *netif) )
{
  struct ntcpip__netifapiMsg msg;
  msg.function = ntcpip__netifapiDoNetifCommon;
  msg.msg.netif = netif;
  msg.msg.msg.common.voidfunc = voidfunc;
  msg.msg.msg.common.errtfunc = errtfunc;
  NTCPIP__NETIFAPI(&msg);
  return msg.msg.err;
}

#endif /* NTCPIP__LWIP_NETIF_API */

