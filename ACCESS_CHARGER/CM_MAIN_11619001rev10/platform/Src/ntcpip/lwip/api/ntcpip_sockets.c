/**
 * @file
 * Sockets BSD-Like API module
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 * Improved by Marc Boucher <marc@mbsi.ca> and David Haas <dhaas@alum.rpi.edu>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_SOCKET /* don't build if not configured for use in lwipopts.h */

#include "lwip/sockets.h"
#include "ntcpip/api.h"
#include "ntcpip/sys.h"
#include "ntcpip/igmp.h"
#include "ntcpip/inet.h"
#include "lwip/tcp.h"
#include "lwip/raw.h"
#include "ntcpip/udp.h"
#include "lwip/tcpip.h"

#include <string.h>

#define NUM_SOCKETS NTCPIP__MEMP_NUM_NETCONN

/** Contains all internal pointers and states used for a socket */
struct lwip_socket {
  /** sockets currently are built on netconns, each socket has one netconn */
  struct ntcpip__netconn *conn;
  /** data that was left from the previous read */
  struct ntcpip__netbuf *lastdata;
  /** offset in the data that was left from the previous read */
  Uint16 lastoffset;
  /** number of times data was received, set by event_callback(),
      tested by the receive and select functions */
  Int16 rcvevent;
  /** number of times data was received, set by event_callback(),
      tested by select */
  Uint16 sendevent;
  /** socket flags (currently, only used for O_NONBLOCK) */
  Uint16 flags;
  /** last error that occurred on this socket */
  int err;
};

/** Description for a task waiting in select */
struct lwip_select_cb {
  /** Pointer to the next waiting task */
  struct lwip_select_cb *next;
  /** readset passed to select */
  fd_set *readset;
  /** writeset passed to select */
  fd_set *writeset;
  /** unimplemented: exceptset passed to select */
  fd_set *exceptset;
  /** don't signal the same semaphore twice: set to 1 when signalled */
  int sem_signalled;
  /** semaphore to wake up a task waiting for select */
  ntcpip__SysSema sem;
};

/** This struct is used to pass data to the set/getsockopt_internal
 * functions running in tcpip_thread context (only a void* is allowed) */
struct lwip_setgetsockopt_data {
  /** socket struct for which to change options */
  struct lwip_socket *sock;
  /** socket index for which to change options */
  int s;
  /** level of the option to process */
  int level;
  /** name of the option to process */
  int optname;
  /** set: value to set the option to
    * get: value of the option is stored here */
  void *optval;
  /** size of *optval */
  socklen_t *optlen;
  /** if an error occures, it is temporarily stored here */
  ntcpip_Err err;
};

/** The global array of available sockets */
static struct lwip_socket sockets[NUM_SOCKETS];
/** The global list of tasks waiting for select */
static struct lwip_select_cb *select_cb_list;

/** Semaphore protecting the sockets array */
static ntcpip__SysSema socksem;
/** Semaphore protecting select_cb_list */
static ntcpip__SysSema selectsem;

/** Table to quickly map an lwIP error (ntcpip_Err) to a socket error
  * by using -err as an index */
static const int err_to_errno_table[] = {
  0,             /* NTCPIP_ERR_OK          0      No error, everything OK. */
  NTCPIP__ENOMEM,        /* NTCPIP_ERR_MEM        -1      Out of memory error.     */
  NTCPIP__ENOBUFS,       /* NTCPIP_ERR_BUF        -2      Buffer error.            */
  NTCPIP__ETIMEDOUT,     /* NTCPIP_ERR_TIMEOUT    -3      Timeout                  */
  NTCPIP__EHOSTUNREACH,  /* NTCPIP_ERR_RTE        -4      Routing problem.         */
  NTCPIP__ECONNABORTED,  /* NTCPIP_ERR_ABRT       -5      Connection aborted.      */
  NTCPIP__ECONNRESET,    /* NTCPIP_ERR_RST        -6      Connection reset.        */
  NTCPIP__ESHUTDOWN,     /* NTCPIP_ERR_CLSD       -7      Connection closed.       */
  NTCPIP__ENOTCONN,      /* NTCPIP_ERR_CONN       -8      Not connected.           */
  NTCPIP__EINVAL,        /* NTCPIP_ERR_VAL        -9      Illegal value.           */
  NTCPIP__EIO,           /* NTCPIP_ERR_ARG        -10     Illegal argument.        */
  NTCPIP__EADDRINUSE,    /* NTCPIP_ERR_USE        -11     Address in use.          */
  -1,            /* NTCPIP_ERR_IF         -12     Low-level netif error    */
  -1,            /* NTCPIP_ERR_ISCONN     -13     Already connected.       */
  NTCPIP__EINPROGRESS    /* NTCPIP_ERR_INPROGRESS -14     Operation in progress    */
};

#define ERR_TO_ERRNO_TABLE_SIZE \
  (sizeof(err_to_errno_table)/sizeof(err_to_errno_table[0]))

#define err_to_errno(err) \
  ((unsigned)(-(err)) < ERR_TO_ERRNO_TABLE_SIZE ? \
    err_to_errno_table[-(err)] : NTCPIP__EIO)

#ifdef ERRNO
#ifndef set_errno
#define set_errno(err) errno = (err)
#endif
#else
#define set_errno(err)
#endif

#define sock_set_errno(sk, e) do { \
  sk->err = (e); \
  set_errno(sk->err); \
} while (0)

/* Forward delcaration of some functions */
static void event_callback(struct ntcpip__netconn *conn, enum ntcpip_netconnEvt evt, Uint16 len);
static void lwip_getsockopt_internal(void *arg);
static void lwip_setsockopt_internal(void *arg);

/**
 * Initialize this module. This function has to be called before any other
 * functions in this module!
 */
void
lwip_socket_init(void)
{
  socksem   = ntcpip__sysSemNew(1);
  selectsem = ntcpip__sysSemNew(1);
}

/**
 * Map a externally used socket index to the internal socket representation.
 *
 * @param s externally used socket index
 * @return struct lwip_socket for the socket or NULL if not found
 */
static struct lwip_socket *
get_socket(int s)
{
  struct lwip_socket *sock;

  if ((s < 0) || (s >= NUM_SOCKETS)) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("get_socket(%d): invalid\n", s));
    set_errno(NTCPIP__EBADF);
    return NULL;
  }

  sock = &sockets[s];

  if (!sock->conn) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("get_socket(%d): not active\n", s));
    set_errno(NTCPIP__EBADF);
    return NULL;
  }

  return sock;
}

/**
 * Allocate a new socket for a given netconn.
 *
 * @param newconn the netconn for which to allocate a socket
 * @return the index of the new socket; -1 on error
 */
static int
alloc_socket(struct ntcpip__netconn *newconn)
{
  int i;

  /* Protect socket array */
  ntcpip__sysSemWait(socksem);

  /* allocate a new socket identifier */
  for (i = 0; i < NUM_SOCKETS; ++i) {
    if (!sockets[i].conn) {
      sockets[i].conn       = newconn;
      sockets[i].lastdata   = NULL;
      sockets[i].lastoffset = 0;
      sockets[i].rcvevent   = 0;
      sockets[i].sendevent  = 1; /* TCP send buf is empty */
      sockets[i].flags      = 0;
      sockets[i].err        = 0;
      ntcpip__sysSemSignal(socksem);
      return i;
    }
  }
  ntcpip__sysSemSignal(socksem);
  return -1;
}

/* Below this, the well-known socket functions are implemented.
 * Use google.com or opengroup.org to get a good description :-)
 *
 * Exceptions are documented!
 */

int
lwip_accept(int s, struct sockaddr *addr, socklen_t *addrlen)
{
  struct lwip_socket *sock, *nsock;
  struct ntcpip__netconn *newconn;
  struct ntcpip_ipAddr naddr;
  Uint16 port;
  int newsock;
  struct sockaddr_in sin;
  ntcpip_Err err;

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_accept(%d)...\n", s));
  sock = get_socket(s);
  if (!sock)
    return -1;

  if ((sock->flags & O_NONBLOCK) && (sock->rcvevent <= 0)) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_accept(%d): returning NTCPIP__EWOULDBLOCK\n", s));
    sock_set_errno(sock, NTCPIP__EWOULDBLOCK);
    return -1;
  }

  newconn = ntcpip_netconnAccept(sock->conn);
  if (!newconn) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_accept(%d) failed, err=%d\n", s, sock->conn->err));
    sock_set_errno(sock, err_to_errno(sock->conn->err));
    return -1;
  }

  /* get the IP address and port of the remote host */
  err = ntcpip_netconnPeer(newconn, &naddr, &port);
  if (err != NTCPIP_ERR_OK) {
    ntcpip_netconnDelete(newconn);
    sock_set_errno(sock, err_to_errno(err));
    return -1;
  }

  /* Note that POSIX only requires us to check addr is non-NULL. addrlen must
   * not be NULL if addr is valid.
   */
  if (NULL != addr) {
    NTCPIP__LWIP_ASSERT("addr valid but addrlen NULL", addrlen != NULL);
    memset(&sin, 0, sizeof(sin));
    sin.sin_len = sizeof(sin);
    sin.sin_family = AF_INET;
    sin.sin_port = ntcpip_htons(port);
    sin.sin_addr.s_addr = naddr.addr;

    if (*addrlen > sizeof(sin))
      *addrlen = sizeof(sin);

    NTCPIP__MEMCPY(addr, &sin, *addrlen);
  }

  newsock = alloc_socket(newconn);
  if (newsock == -1) {
    ntcpip_netconnDelete(newconn);
    sock_set_errno(sock, NTCPIP__ENFILE);
    return -1;
  }
  NTCPIP__LWIP_ASSERT("invalid socket index", (newsock >= 0) && (newsock < NUM_SOCKETS));
  newconn->callback = event_callback;
  nsock = &sockets[newsock];
  NTCPIP__LWIP_ASSERT("invalid socket pointer", nsock != NULL);

  ntcpip__sysSemWait(socksem);
  /* See event_callback: If data comes in right away after an accept, even
   * though the server task might not have created a new socket yet.
   * In that case, newconn->socket is counted down (newconn->socket--),
   * so nsock->rcvevent is >= 1 here!
   */
  nsock->rcvevent += -1 - newconn->socket;
  newconn->socket = newsock;
  ntcpip__sysSemSignal(socksem);

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_accept(%d) returning new sock=%d addr=", s, newsock));
  ntcpip_ipaddrDebugPrint(NTCPIP__SOCKETS_DEBUG, &naddr);
  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, (" port=%"U16_F"\n", port));

  sock_set_errno(sock, 0);
  return newsock;
}

int
lwip_bind(int s, const struct sockaddr *name, socklen_t namelen)
{
  struct lwip_socket *sock;
  struct ntcpip_ipAddr local_addr;
  Uint16 local_port;
  ntcpip_Err err;

  sock = get_socket(s);
  if (!sock)
    return -1;

  NTCPIP__LWIP_ERROR("lwip_bind: invalid address", ((namelen == sizeof(struct sockaddr_in)) &&
             ((((const struct sockaddr_in *)name)->sin_family) == AF_INET)),
             sock_set_errno(sock, err_to_errno(NTCPIP_ERR_ARG)); return -1;);

  local_addr.addr = ((const struct sockaddr_in *)name)->sin_addr.s_addr;
  local_port = ((const struct sockaddr_in *)name)->sin_port;

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_bind(%d, addr=", s));
  ntcpip_ipaddrDebugPrint(NTCPIP__SOCKETS_DEBUG, &local_addr);
  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, (" port=%"U16_F")\n", ntcpip_ntohs(local_port)));

  err = ntcpip_netconnBind(sock->conn, &local_addr, ntcpip_ntohs(local_port));

  if (err != NTCPIP_ERR_OK) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_bind(%d) failed, err=%d\n", s, err));
    sock_set_errno(sock, err_to_errno(err));
    return -1;
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_bind(%d) succeeded\n", s));
  sock_set_errno(sock, 0);
  return 0;
}

int
lwip_close(int s)
{
  struct lwip_socket *sock;

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_close(%d)\n", s));

  sock = get_socket(s);
  if (!sock) {
    return -1;
  }

  ntcpip_netconnDelete(sock->conn);

  ntcpip__sysSemWait(socksem);
  if (sock->lastdata) {
    ntcpip_netbufDelete(sock->lastdata);
  }
  sock->lastdata   = NULL;
  sock->lastoffset = 0;
  sock->conn       = NULL;
  sock_set_errno(sock, 0);
  ntcpip__sysSemSignal(socksem);
  return 0;
}

int
lwip_connect(int s, const struct sockaddr *name, socklen_t namelen)
{
  struct lwip_socket *sock;
  ntcpip_Err err;

  sock = get_socket(s);
  if (!sock)
    return -1;

  NTCPIP__LWIP_ERROR("lwip_connect: invalid address", ((namelen == sizeof(struct sockaddr_in)) &&
             ((((const struct sockaddr_in *)name)->sin_family) == AF_INET)),
             sock_set_errno(sock, err_to_errno(NTCPIP_ERR_ARG)); return -1;);

  if (((const struct sockaddr_in *)name)->sin_family == AF_UNSPEC) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_connect(%d, AF_UNSPEC)\n", s));
    err = ntcpip_netconnDisconnect(sock->conn);
  } else {
    struct ntcpip_ipAddr remote_addr;
    Uint16 remote_port;

    remote_addr.addr = ((const struct sockaddr_in *)name)->sin_addr.s_addr;
    remote_port = ((const struct sockaddr_in *)name)->sin_port;

    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_connect(%d, addr=", s));
    ntcpip_ipaddrDebugPrint(NTCPIP__SOCKETS_DEBUG, &remote_addr);
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, (" port=%"U16_F")\n", ntcpip_ntohs(remote_port)));

    err = ntcpip_netconnConnect(sock->conn, &remote_addr, ntcpip_ntohs(remote_port));
  }

  if (err != NTCPIP_ERR_OK) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_connect(%d) failed, err=%d\n", s, err));
    sock_set_errno(sock, err_to_errno(err));
    return -1;
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_connect(%d) succeeded\n", s));
  sock_set_errno(sock, 0);
  return 0;
}

/**
 * Set a socket into listen mode.
 * The socket may not have been used for another connection previously.
 *
 * @param s the socket to set to listening mode
 * @param backlog (ATTENTION: need NTCPIP__TCP_LISTEN_BACKLOG=1)
 * @return 0 on success, non-zero on failure
 */
int
lwip_listen(int s, int backlog)
{
  struct lwip_socket *sock;
  ntcpip_Err err;

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_listen(%d, backlog=%d)\n", s, backlog));

  sock = get_socket(s);
  if (!sock)
    return -1;

  /* limit the "backlog" parameter to fit in an Uint8 */
  if (backlog < 0) {
    backlog = 0;
  }
  if (backlog > 0xff) {
    backlog = 0xff;
  }

  err = ntcpip_netconnlistenWithBacklog(sock->conn, backlog);

  if (err != NTCPIP_ERR_OK) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_listen(%d) failed, err=%d\n", s, err));
    sock_set_errno(sock, err_to_errno(err));
    return -1;
  }

  sock_set_errno(sock, 0);
  return 0;
}

int
lwip_recvfrom(int s, void *mem, size_t len, int flags,
        struct sockaddr *from, socklen_t *fromlen)
{
  struct lwip_socket *sock;
  struct ntcpip__netbuf      *buf;
  Uint16               buflen, copylen, off = 0;
  struct ntcpip_ipAddr     *addr;
  Uint16               port;
  Uint8                done = 0;

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom(%d, %p, %"SZT_F", 0x%x, ..)\n", s, mem, len, flags));
  sock = get_socket(s);
  if (!sock)
    return -1;

  do {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom: top while sock->lastdata=%p\n", (void*)sock->lastdata));
    /* Check if there is data left from the last recv operation. */
    if (sock->lastdata) {
      buf = sock->lastdata;
    } else {
      /* If this is non-blocking call, then check first */
      if (((flags & MSG_DONTWAIT) || (sock->flags & O_NONBLOCK)) && 
          (sock->rcvevent <= 0)) {
        if (off > 0) {
          /* already received data, return that */
          sock_set_errno(sock, 0);
          return off;
        }
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom(%d): returning NTCPIP__EWOULDBLOCK\n", s));
        sock_set_errno(sock, NTCPIP__EWOULDBLOCK);
        return -1;
      }

      /* No data was left from the previous operation, so we try to get
      some from the network. */
      sock->lastdata = buf = ntcpip_netconnRecv(sock->conn);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom: ntcpip_netconnRecv netbuf=%p\n", (void*)buf));

      if (!buf) {
        if (off > 0) {
          /* already received data, return that */
          sock_set_errno(sock, 0);
          return off;
        }
        /* We should really do some error checking here. */
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom(%d): buf == NULL!\n", s));
        sock_set_errno(sock, (((sock->conn->pcb.ip != NULL) && (sock->conn->err == NTCPIP_ERR_OK))
          ? NTCPIP__ETIMEDOUT : err_to_errno(sock->conn->err)));
        return 0;
      }
    }

    buflen = ntcpip_netbufLen(buf);
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom: buflen=%"U16_F" len=%"SZT_F" off=%"U16_F" sock->lastoffset=%"U16_F"\n",
      buflen, len, off, sock->lastoffset));

    buflen -= sock->lastoffset;

    if (len > buflen) {
      copylen = buflen;
    } else {
      copylen = (Uint16)len;
    }

    /* copy the contents of the received buffer into
    the supplied memory pointer mem */
    ntcpip_netbufCopyPartial(buf, (Uint8*)mem + off, copylen, sock->lastoffset);

    off += copylen;

    if (ntcpip_netconnType(sock->conn) == NTCPIP_NETCONN_TCP) {
      NTCPIP__LWIP_ASSERT("invalid copylen, len would underflow", len >= copylen);
      len -= copylen;
      if ( (len <= 0) || 
           (buf->p->flags & NTCPIP__PBUF_FLAG_PUSH) || 
           (sock->rcvevent <= 0) || 
           ((flags & MSG_PEEK)!=0)) {
        done = 1;
      }
    } else {
      done = 1;
    }

    /* Check to see from where the data was.*/
    if (done) {
      if (from && fromlen) {
        struct sockaddr_in sin;

        if (ntcpip_netconnType(sock->conn) == NTCPIP_NETCONN_TCP) {
          addr = (struct ntcpip_ipAddr*)&(sin.sin_addr.s_addr);
          ntcpip_netconnGetAddr(sock->conn, addr, &port, 0);
        } else {
          addr = ntcpip_netbufFromAddr(buf);
          port = ntcpip_netbufFromPort(buf);
        }

        memset(&sin, 0, sizeof(sin));
        sin.sin_len = sizeof(sin);
        sin.sin_family = AF_INET;
        sin.sin_port = ntcpip_htons(port);
        sin.sin_addr.s_addr = addr->addr;

        if (*fromlen > sizeof(sin)) {
          *fromlen = sizeof(sin);
        }

        NTCPIP__MEMCPY(from, &sin, *fromlen);

        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom(%d): addr=", s));
        ntcpip_ipaddrDebugPrint(NTCPIP__SOCKETS_DEBUG, addr);
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, (" port=%"U16_F" len=%"U16_F"\n", port, off));
      } else {
  #if NTCPIP__SOCKETS_DEBUG
        struct sockaddr_in sin;

        if (ntcpip_netconnType(sock->conn) == NTCPIP_NETCONN_TCP) {
          addr = (struct ntcpip_ipAddr*)&(sin.sin_addr.s_addr);
          ntcpip_netconnGetAddr(sock->conn, addr, &port, 0);
        } else {
          addr = ntcpip_netbufFromAddr(buf);
          port = ntcpip_netbufFromPort(buf);
        }

        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom(%d): addr=", s));
        ntcpip_ipaddrDebugPrint(NTCPIP__SOCKETS_DEBUG, addr);
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, (" port=%"U16_F" len=%"U16_F"\n", port, off));
  #endif /*  NTCPIP__SOCKETS_DEBUG */
      }
    }

    /* If we don't peek the incoming message... */
    if ((flags & MSG_PEEK)==0) {
      /* If this is a TCP socket, check if there is data left in the
         buffer. If so, it should be saved in the sock structure for next
         time around. */
      if ((ntcpip_netconnType(sock->conn) == NTCPIP_NETCONN_TCP) && (buflen - copylen > 0)) {
        sock->lastdata = buf;
        sock->lastoffset += copylen;
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom: lastdata now netbuf=%p\n", (void*)buf));
      } else {
        sock->lastdata = NULL;
        sock->lastoffset = 0;
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_recvfrom: deleting netbuf=%p\n", (void*)buf));
        ntcpip_netbufDelete(buf);
      }
    }
  } while (!done);

  sock_set_errno(sock, 0);
  return off;
}

int
lwip_read(int s, void *mem, size_t len)
{
  return lwip_recvfrom(s, mem, len, 0, NULL, NULL);
}

int
lwip_recv(int s, void *mem, size_t len, int flags)
{
  return lwip_recvfrom(s, mem, len, flags, NULL, NULL);
}

int
lwip_send(int s, const void *data, size_t size, int flags)
{
  struct lwip_socket *sock;
  ntcpip_Err err;

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_send(%d, data=%p, size=%"SZT_F", flags=0x%x)\n",
                              s, data, size, flags));

  sock = get_socket(s);
  if (!sock)
    return -1;

  if (sock->conn->type != NTCPIP_NETCONN_TCP) {
#if (NTCPIP__LWIP_UDP || NTCPIP__LWIP_RAW)
    return lwip_sendto(s, data, size, flags, NULL, 0);
#else
    sock_set_errno(sock, err_to_errno(NTCPIP_ERR_ARG));
    return -1;
#endif /* (NTCPIP__LWIP_UDP || NTCPIP__LWIP_RAW) */
  }

  err = ntcpip_netconnWrite(sock->conn, data, size, NTCPIP_NETCONN_COPY | ((flags & MSG_MORE)?NTCPIP_NETCONN_MORE:0));

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_send(%d) err=%d size=%"SZT_F"\n", s, err, size));
  sock_set_errno(sock, err_to_errno(err));
  return (err == NTCPIP_ERR_OK ? (int)size : -1);
}

int
lwip_sendto(int s, const void *data, size_t size, int flags,
       const struct sockaddr *to, socklen_t tolen)
{
  struct lwip_socket *sock;
  struct ntcpip_ipAddr remote_addr;
  ntcpip_Err err;
  Uint16 short_size;
#if !NTCPIP__LWIP_TCPIP_CORE_LOCKING
  struct ntcpip__netbuf buf;
  Uint16 remote_port;
#endif

  sock = get_socket(s);
  if (!sock)
    return -1;

  if (sock->conn->type == NTCPIP_NETCONN_TCP) {
#if NTCPIP__LWIP_TCP
    return lwip_send(s, data, size, flags);
#else
    sock_set_errno(sock, err_to_errno(NTCPIP_ERR_ARG));
    return -1;
#endif /* NTCPIP__LWIP_TCP */
  }

  NTCPIP__LWIP_ASSERT("lwip_sendto: size must fit in Uint16", size <= 0xffff);
  short_size = (Uint16)size;
  NTCPIP__LWIP_ERROR("lwip_sendto: invalid address", (((to == NULL) && (tolen == 0)) ||
             ((tolen == sizeof(struct sockaddr_in)) &&
             ((((const struct sockaddr_in *)to)->sin_family) == AF_INET))),
             sock_set_errno(sock, err_to_errno(NTCPIP_ERR_ARG)); return -1;);

#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
  /* Should only be consider like a sample or a simple way to experiment this option (no check of "to" field...) */
  { struct ntcpip_pbuf* p;
  
    p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, 0, NTCPIP_PBUF_REF);
    if (p == NULL) {
      err = NTCPIP_ERR_MEM;
    } else {
      p->payload = (void*)data;
      p->len = p->tot_len = short_size;
      
      remote_addr.addr = ((const struct sockaddr_in *)to)->sin_addr.s_addr;
      
      NTCPIP__LOCK_CORE();
      if (sock->conn->type==NTCPIP_NETCONN_RAW) {
        err = sock->conn->err = raw_sendto(sock->conn->pcb.raw, p, &remote_addr);
      } else {
        err = sock->conn->err = ntcpip_udpSendTo(sock->conn->pcb.udp, p, &remote_addr, ntcpip_ntohs(((const struct sockaddr_in *)to)->sin_port));
      }
      NTCPIP__UNLOCK_CORE();
      
      ntcpip_pbufFree(p);
    }
  }
#else
  /* initialize a buffer */
  buf.p = buf.ptr = NULL;
  if (to) {
    remote_addr.addr = ((const struct sockaddr_in *)to)->sin_addr.s_addr;
    remote_port      = ntcpip_ntohs(((const struct sockaddr_in *)to)->sin_port);
    buf.addr         = &remote_addr;
    buf.port         = remote_port;
  } else {
    remote_addr.addr = 0;
    remote_port      = 0;
    buf.addr         = NULL;
    buf.port         = 0;
  }

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_sendto(%d, data=%p, short_size=%d"U16_F", flags=0x%x to=",
              s, data, short_size, flags));
  ntcpip_ipaddrDebugPrint(NTCPIP__SOCKETS_DEBUG, &remote_addr);
  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, (" port=%"U16_F"\n", remote_port));

  /* make the buffer point to the data that should be sent */
#if LWIP_NETIF_TX_SINGLE_PBUF
  /* Allocate a new netbuf and copy the data into it. */
  if (ntcpip_netbufAlloc(&buf, short_size) == NULL) {
    err = NTCPIP_ERR_MEM;
  } else {
    err = netbuf_take(&buf, data, short_size);
  }
#else /* LWIP_NETIF_TX_SINGLE_PBUF */
  err = ntcpip__netbufRef(&buf, data, short_size);
#endif /* LWIP_NETIF_TX_SINGLE_PBUF */
  if (err == NTCPIP_ERR_OK) {
    /* send the data */
    err = ntcpip_netconnSend(sock->conn, &buf);
  }

  /* deallocated the buffer */
  ntcpip_netbufFree(&buf);
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */
  sock_set_errno(sock, err_to_errno(err));
  return (err == NTCPIP_ERR_OK ? short_size : -1);
}

int
lwip_socket(int domain, int type, int protocol)
{
  struct ntcpip__netconn *conn;
  int i;

  NTCPIP__LWIP_UNUSED_ARG(domain);

  /* create a netconn */
  switch (type) {
  case SOCK_RAW:
    conn = ntcpip_netconnNewWithProtoAndCallback(NTCPIP_NETCONN_RAW, (Uint8)protocol, event_callback);
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_socket(%s, SOCK_RAW, %d) = ",
                                 domain == PF_INET ? "PF_INET" : "UNKNOWN", protocol));
    break;
  case SOCK_DGRAM:
    conn = ntcpip_netconnNewWithCallback( (protocol == IPPROTO_UDPLITE) ?
                 NTCPIP_NETCONN_UDPLITE : NTCPIP_NETCONN_UDP, event_callback);
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_socket(%s, SOCK_DGRAM, %d) = ",
                                 domain == PF_INET ? "PF_INET" : "UNKNOWN", protocol));
    break;
  case SOCK_STREAM:
    conn = ntcpip_netconnNewWithCallback(NTCPIP_NETCONN_TCP, event_callback);
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_socket(%s, SOCK_STREAM, %d) = ",
                                 domain == PF_INET ? "PF_INET" : "UNKNOWN", protocol));
    break;
  default:
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_socket(%d, %d/UNKNOWN, %d) = -1\n",
                                 domain, type, protocol));
    set_errno(NTCPIP__EINVAL);
    return -1;
  }

  if (!conn) {
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("-1 / NTCPIP__ENOBUFS (could not create netconn)\n"));
    set_errno(NTCPIP__ENOBUFS);
    return -1;
  }

  i = alloc_socket(conn);

  if (i == -1) {
    ntcpip_netconnDelete(conn);
    set_errno(NTCPIP__ENFILE);
    return -1;
  }
  conn->socket = i;
  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("%d\n", i));
  set_errno(0);
  return i;
}

int
lwip_write(int s, const void *data, size_t size)
{
  return lwip_send(s, data, size, 0);
}

/**
 * Go through the readset and writeset lists and see which socket of the sockets
 * set in the sets has events. On return, readset, writeset and exceptset have
 * the sockets enabled that had events.
 *
 * exceptset is not used for now!!!
 *
 * @param maxfdp1 the highest socket index in the sets
 * @param readset in: set of sockets to check for read events;
 *                out: set of sockets that had read events
 * @param writeset in: set of sockets to check for write events;
 *                 out: set of sockets that had write events
 * @param exceptset not yet implemented
 * @return number of sockets that had events (read+write)
 */
static int
lwip_selscan(int maxfdp1, fd_set *readset, fd_set *writeset, fd_set *exceptset)
{
  int i, nready = 0;
  fd_set lreadset, lwriteset, lexceptset;
  struct lwip_socket *p_sock;
  
  FD_ZERO(&lreadset);
  FD_ZERO(&lwriteset);
  FD_ZERO(&lexceptset);
  
  /* Go through each socket in each list to count number of sockets which
  currently match */
  for(i = 0; i < maxfdp1; i++) {
    if (FD_ISSET(i, readset)) {
      /* See if netconn of this socket is ready for read */
      p_sock = get_socket(i);
      if (p_sock && (p_sock->lastdata || (p_sock->rcvevent > 0))) {
        FD_SET(i, &lreadset);
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_selscan: fd=%d ready for reading\n", i));
        nready++;
      }
    }
    if (FD_ISSET(i, writeset)) {
      /* See if netconn of this socket is ready for write */
      p_sock = get_socket(i);
      if (p_sock && p_sock->sendevent) {
        FD_SET(i, &lwriteset);
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_selscan: fd=%d ready for writing\n", i));
        nready++;
      }
    }
  }
  *readset = lreadset;
  *writeset = lwriteset;
  FD_ZERO(exceptset);
  
  return nready;
}


/**
 * Processing exceptset is not yet implemented.
 */
int
lwip_select(int maxfdp1, fd_set *readset, fd_set *writeset, fd_set *exceptset,
               struct timeval *timeout)
{
  int i;
  int nready;
  fd_set lreadset, lwriteset, lexceptset;
  Uint32 msectimeout;
  struct lwip_select_cb select_cb;
  struct lwip_select_cb *p_selcb;

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_select(%d, %p, %p, %p, tvsec=%ld tvusec=%ld)\n",
                  maxfdp1, (void *)readset, (void *) writeset, (void *) exceptset,
                  timeout ? (long)timeout->tv_sec : (long)-1,
                  timeout ? (long)timeout->tv_usec : (long)-1));

  select_cb.next = 0;
  select_cb.readset = readset;
  select_cb.writeset = writeset;
  select_cb.exceptset = exceptset;
  select_cb.sem_signalled = 0;

  /* Protect ourselves searching through the list */
  ntcpip__sysSemWait(selectsem);

  if (readset)
    lreadset = *readset;
  else
    FD_ZERO(&lreadset);
  if (writeset)
    lwriteset = *writeset;
  else
    FD_ZERO(&lwriteset);
  if (exceptset)
    lexceptset = *exceptset;
  else
    FD_ZERO(&lexceptset);

  /* Go through each socket in each list to count number of sockets which
     currently match */
  nready = lwip_selscan(maxfdp1, &lreadset, &lwriteset, &lexceptset);

  /* If we don't have any current events, then suspend if we are supposed to */
  if (!nready) {
    if (timeout && timeout->tv_sec == 0 && timeout->tv_usec == 0) {
      ntcpip__sysSemSignal(selectsem);
      if (readset)
        FD_ZERO(readset);
      if (writeset)
        FD_ZERO(writeset);
      if (exceptset)
        FD_ZERO(exceptset);
  
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_select: no timeout, returning 0\n"));
      set_errno(0);
  
      return 0;
    }
    
    /* add our semaphore to list */
    /* We don't actually need any dynamic memory. Our entry on the
     * list is only valid while we are in this function, so it's ok
     * to use local variables */
    
    select_cb.sem = ntcpip__sysSemNew(0);
    /* Note that we are still protected */
    /* Put this select_cb on top of list */
    select_cb.next = select_cb_list;
    select_cb_list = &select_cb;
    
    /* Now we can safely unprotect */
    ntcpip__sysSemSignal(selectsem);
    
    /* Now just wait to be woken */
    if (timeout == 0)
      /* Wait forever */
      msectimeout = 0;
    else {
      msectimeout =  ((timeout->tv_sec * 1000) + ((timeout->tv_usec + 500)/1000));
      if(msectimeout == 0)
        msectimeout = 1;
    }
    
    i = ntcpip__sysSemWaitTimeout(select_cb.sem, msectimeout);
    
    /* Take us off the list */
    ntcpip__sysSemWait(selectsem);
    if (select_cb_list == &select_cb)
      select_cb_list = select_cb.next;
    else
      for (p_selcb = select_cb_list; p_selcb; p_selcb = p_selcb->next) {
        if (p_selcb->next == &select_cb) {
          p_selcb->next = select_cb.next;
          break;
        }
      }
    
    ntcpip__sysSemSignal(selectsem);
    
    ntcpip__sysSemFree(select_cb.sem);
    if (i == 0)  {
      /* Timeout */
      if (readset)
        FD_ZERO(readset);
      if (writeset)
        FD_ZERO(writeset);
      if (exceptset)
        FD_ZERO(exceptset);
  
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_select: timeout expired\n"));
      set_errno(0);
  
      return 0;
    }
    
    if (readset)
      lreadset = *readset;
    else
      FD_ZERO(&lreadset);
    if (writeset)
      lwriteset = *writeset;
    else
      FD_ZERO(&lwriteset);
    if (exceptset)
      lexceptset = *exceptset;
    else
      FD_ZERO(&lexceptset);
    
    /* See what's set */
    nready = lwip_selscan(maxfdp1, &lreadset, &lwriteset, &lexceptset);
  } else
    ntcpip__sysSemSignal(selectsem);
  
  if (readset)
    *readset = lreadset;
  if (writeset)
    *writeset = lwriteset;
  if (exceptset)
    *exceptset = lexceptset;
  
  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_select: nready=%d\n", nready));
  set_errno(0);
  
  return nready;
}

/**
 * Callback registered in the netconn layer for each socket-netconn.
 * Processes recvevent (data available) and wakes up tasks waiting for select.
 */
static void
event_callback(struct ntcpip__netconn *conn, enum ntcpip_netconnEvt evt, Uint16 len)
{
  int s;
  struct lwip_socket *sock;
  struct lwip_select_cb *scb;

  NTCPIP__LWIP_UNUSED_ARG(len);

  /* Get socket */
  if (conn) {
    s = conn->socket;
    if (s < 0) {
      /* Data comes in right away after an accept, even though
       * the server task might not have created a new socket yet.
       * Just count down (or up) if that's the case and we
       * will use the data later. Note that only receive events
       * can happen before the new socket is set up. */
      ntcpip__sysSemWait(socksem);
      if (conn->socket < 0) {
        if (evt == NTCPIP_NETCONN_EVT_RCVPLUS) {
          conn->socket--;
        }
        ntcpip__sysSemSignal(socksem);
        return;
      }
      s = conn->socket;
      ntcpip__sysSemSignal(socksem);
    }

    sock = get_socket(s);
    if (!sock) {
      return;
    }
  } else {
    return;
  }

  ntcpip__sysSemWait(selectsem);
  /* Set event as required */
  switch (evt) {
    case NTCPIP_NETCONN_EVT_RCVPLUS:
      sock->rcvevent++;
      break;
    case NTCPIP_NETCONN_EVT_RCVMINUS:
      sock->rcvevent--;
      break;
    case NTCPIP_NETCONN_EVT_SENDPLUS:
      sock->sendevent = 1;
      break;
    case NTCPIP_NETCONN_EVT_SENDMINUS:
      sock->sendevent = 0;
      break;
    default:
      NTCPIP__LWIP_ASSERT("unknown event", 0);
      break;
  }
  ntcpip__sysSemSignal(selectsem);

  /* Now decide if anyone is waiting for this socket */
  /* NOTE: This code is written this way to protect the select link list
     but to avoid a deadlock situation by releasing socksem before
     signalling for the select. This means we need to go through the list
     multiple times ONLY IF a select was actually waiting. We go through
     the list the number of waiting select calls + 1. This list is
     expected to be small. */
  while (1) {
    ntcpip__sysSemWait(selectsem);
    for (scb = select_cb_list; scb; scb = scb->next) {
      if (scb->sem_signalled == 0) {
        /* Test this select call for our socket */
        if (scb->readset && FD_ISSET(s, scb->readset))
          if (sock->rcvevent > 0)
            break;
        if (scb->writeset && FD_ISSET(s, scb->writeset))
          if (sock->sendevent)
            break;
      }
    }
    if (scb) {
      scb->sem_signalled = 1;
      ntcpip__sysSemSignal(scb->sem);
      ntcpip__sysSemSignal(selectsem);
    } else {
      ntcpip__sysSemSignal(selectsem);
      break;
    }
  }
}

/**
 * Unimplemented: Close one end of a full-duplex connection.
 * Currently, the full connection is closed.
 */
int
lwip_shutdown(int s, int how)
{
  NTCPIP__LWIP_UNUSED_ARG(how);
  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_shutdown(%d, how=%d)\n", s, how));
  return lwip_close(s); /* XXX temporary hack until proper implementation */
}

static int
lwip_getaddrname(int s, struct sockaddr *name, socklen_t *namelen, Uint8 local)
{
  struct lwip_socket *sock;
  struct sockaddr_in sin;
  struct ntcpip_ipAddr naddr;

  sock = get_socket(s);
  if (!sock)
    return -1;

  memset(&sin, 0, sizeof(sin));
  sin.sin_len = sizeof(sin);
  sin.sin_family = AF_INET;

  /* get the IP address and port */
  ntcpip_netconnGetAddr(sock->conn, &naddr, &sin.sin_port, local);

  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getaddrname(%d, addr=", s));
  ntcpip_ipaddrDebugPrint(NTCPIP__SOCKETS_DEBUG, &naddr);
  NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, (" port=%"U16_F")\n", sin.sin_port));

  sin.sin_port = ntcpip_htons(sin.sin_port);
  sin.sin_addr.s_addr = naddr.addr;

  if (*namelen > sizeof(sin))
    *namelen = sizeof(sin);

  NTCPIP__MEMCPY(name, &sin, *namelen);
  sock_set_errno(sock, 0);
  return 0;
}

int
lwip_getpeername(int s, struct sockaddr *name, socklen_t *namelen)
{
  return lwip_getaddrname(s, name, namelen, 0);
}

int
lwip_getsockname(int s, struct sockaddr *name, socklen_t *namelen)
{
  return lwip_getaddrname(s, name, namelen, 1);
}

int
lwip_getsockopt(int s, int level, int optname, void *optval, socklen_t *optlen)
{
  ntcpip_Err err = NTCPIP_ERR_OK;
  struct lwip_socket *sock = get_socket(s);
  struct lwip_setgetsockopt_data data;

  if (!sock)
    return -1;

  if ((NULL == optval) || (NULL == optlen)) {
    sock_set_errno(sock, NTCPIP__EFAULT);
    return -1;
  }

  /* Do length and type checks for the various options first, to keep it readable. */
  switch (level) {
   
/* Level: SOL_SOCKET */
  case SOL_SOCKET:
    switch (optname) {
       
    case SO_ACCEPTCONN:
    case SO_BROADCAST:
    /* UNIMPL case SO_DEBUG: */
    /* UNIMPL case SO_DONTROUTE: */
    case SO_ERROR:
    case SO_KEEPALIVE:
    /* UNIMPL case SO_CONTIMEO: */
    /* UNIMPL case SO_SNDTIMEO: */
#if NTCPIP__LWIP_SO_RCVTIMEO
    case SO_RCVTIMEO:
#endif /* NTCPIP__LWIP_SO_RCVTIMEO */
#if NTCPIP__LWIP_SO_RCVBUF
    case SO_RCVBUF:
#endif /* NTCPIP__LWIP_SO_RCVBUF */
    /* UNIMPL case SO_OOBINLINE: */
    /* UNIMPL case SO_SNDBUF: */
    /* UNIMPL case SO_RCVLOWAT: */
    /* UNIMPL case SO_SNDLOWAT: */
#if NTCPIP__SO_REUSE
    case SO_REUSEADDR:
    case SO_REUSEPORT:
#endif /* NTCPIP__SO_REUSE */
    case SO_TYPE:
    /* UNIMPL case SO_USELOOPBACK: */
      if (*optlen < sizeof(int)) {
        err = NTCPIP__EINVAL;
      }
      break;

    case SO_NO_CHECK:
      if (*optlen < sizeof(int)) {
        err = NTCPIP__EINVAL;
      }
#if NTCPIP__LWIP_UDP
      if ((sock->conn->type != NTCPIP_NETCONN_UDP) ||
          ((udp_flags(sock->conn->pcb.udp) & UDP_FLAGS_UDPLITE) != 0)) {
        /* this flag is only available for UDP, not for UDP lite */
        err = NTCPIP__EAFNOSUPPORT;
      }
#endif /* NTCPIP__LWIP_UDP */
      break;

    default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, SOL_SOCKET, UNIMPL: optname=0x%x, ..)\n",
                                  s, optname));
      err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;
                     
/* Level: IPPROTO_IP */
  case IPPROTO_IP:
    switch (optname) {
    /* UNIMPL case IP_HDRINCL: */
    /* UNIMPL case IP_RCVDSTADDR: */
    /* UNIMPL case IP_RCVIF: */
    case IP_TTL:
    case IP_TOS:
      if (*optlen < sizeof(int)) {
        err = NTCPIP__EINVAL;
      }
      break;
#if NTCPIP__LWIP_IGMP
    case IP_MULTICAST_TTL:
      if (*optlen < sizeof(Uint8)) {
        err = NTCPIP__EINVAL;
      }
      break;
    case IP_MULTICAST_IF:
      if (*optlen < sizeof(struct ntcpip__inAddr)) {
        err = NTCPIP__EINVAL;
      }
      break;
#endif /* NTCPIP__LWIP_IGMP */

    default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, UNIMPL: optname=0x%x, ..)\n",
                                  s, optname));
      err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;
         
#if NTCPIP__LWIP_TCP
/* Level: IPPROTO_TCP */
  case IPPROTO_TCP:
    if (*optlen < sizeof(int)) {
      err = NTCPIP__EINVAL;
      break;
    }
    
    /* If this is no TCP socket, ignore any options. */
    if (sock->conn->type != NTCPIP_NETCONN_TCP)
      return 0;

    switch (optname) {
    case TCP_NODELAY:
    case TCP_KEEPALIVE:
#if NTCPIP__LWIP_TCP_KEEPALIVE
    case TCP_KEEPIDLE:
    case TCP_KEEPINTVL:
    case TCP_KEEPCNT:
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */
      break;
       
    default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_TCP, UNIMPL: optname=0x%x, ..)\n",
                                  s, optname));
      err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_TCP */
#if NTCPIP__LWIP_UDP && NTCPIP__LWIP_UDPLITE
/* Level: IPPROTO_UDPLITE */
  case IPPROTO_UDPLITE:
    if (*optlen < sizeof(int)) {
      err = NTCPIP__EINVAL;
      break;
    }
    
    /* If this is no UDP lite socket, ignore any options. */
    if (sock->conn->type != NTCPIP_NETCONN_UDPLITE)
      return 0;

    switch (optname) {
    case UDPLITE_SEND_CSCOV:
    case UDPLITE_RECV_CSCOV:
      break;
       
    default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_UDPLITE, UNIMPL: optname=0x%x, ..)\n",
                                  s, optname));
      err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_UDP && NTCPIP__LWIP_UDPLITE*/
/* UNDEFINED LEVEL */
  default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, level=0x%x, UNIMPL: optname=0x%x, ..)\n",
                                  s, level, optname));
      err = NTCPIP__ENOPROTOOPT;
  }  /* switch */

   
  if (err != NTCPIP_ERR_OK) {
    sock_set_errno(sock, err);
    return -1;
  }

  /* Now do the actual option processing */
  data.sock = sock;
  data.level = level;
  data.optname = optname;
  data.optval = optval;
  data.optlen = optlen;
  data.err = err;
  ntcpip__tcpipCb(lwip_getsockopt_internal, &data);
  ntcpip__sysArchSemWait(sock->conn->op_completed, 0);
  /* maybe lwip_getsockopt_internal has changed err */
  err = data.err;

  sock_set_errno(sock, err);
  return err ? -1 : 0;
}

static void
lwip_getsockopt_internal(void *arg)
{
  struct lwip_socket *sock;
#ifdef LWIP_DEBUG
  int s;
#endif /* LWIP_DEBUG */
  int level, optname;
  void *optval;
  struct lwip_setgetsockopt_data *data;

  NTCPIP__LWIP_ASSERT("arg != NULL", arg != NULL);

  data = (struct lwip_setgetsockopt_data*)arg;
  sock = data->sock;
#ifdef LWIP_DEBUG
  s = data->s;
#endif /* LWIP_DEBUG */
  level = data->level;
  optname = data->optname;
  optval = data->optval;

  switch (level) {
   
/* Level: SOL_SOCKET */
  case SOL_SOCKET:
    switch (optname) {

    /* The option flags */
    case SO_ACCEPTCONN:
    case SO_BROADCAST:
    /* UNIMPL case SO_DEBUG: */
    /* UNIMPL case SO_DONTROUTE: */
    case SO_KEEPALIVE:
    /* UNIMPL case SO_OOBINCLUDE: */
#if NTCPIP__SO_REUSE
    case SO_REUSEADDR:
    case SO_REUSEPORT:
#endif /* NTCPIP__SO_REUSE */
    /*case SO_USELOOPBACK: UNIMPL */
      *(int*)optval = sock->conn->pcb.ip->so_options & optname;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, SOL_SOCKET, optname=0x%x, ..) = %s\n",
                                  s, optname, (*(int*)optval?"on":"off")));
      break;

    case SO_TYPE:
      switch (NTCPIP__NETCONNTYPE_GROUP(sock->conn->type)) {
      case NTCPIP_NETCONN_RAW:
        *(int*)optval = SOCK_RAW;
        break;
      case NTCPIP_NETCONN_TCP:
        *(int*)optval = SOCK_STREAM;
        break;
      case NTCPIP_NETCONN_UDP:
        *(int*)optval = SOCK_DGRAM;
        break;
      default: /* unrecognized socket type */
        *(int*)optval = sock->conn->type;
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG,
                    ("lwip_getsockopt(%d, SOL_SOCKET, SO_TYPE): unrecognized socket type %d\n",
                    s, *(int *)optval));
      }  /* switch (sock->conn->type) */
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, SOL_SOCKET, SO_TYPE) = %d\n",
                  s, *(int *)optval));
      break;

    case SO_ERROR:
      if (sock->err == 0) {
        sock_set_errno(sock, err_to_errno(sock->conn->err));
      } 
      *(int *)optval = sock->err;
      sock->err = 0;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, SOL_SOCKET, SO_ERROR) = %d\n",
                  s, *(int *)optval));
      break;

#if NTCPIP__LWIP_SO_RCVTIMEO
    case SO_RCVTIMEO:
      *(int *)optval = sock->conn->recv_timeout;
      break;
#endif /* NTCPIP__LWIP_SO_RCVTIMEO */
#if NTCPIP__LWIP_SO_RCVBUF
    case SO_RCVBUF:
      *(int *)optval = sock->conn->recv_bufsize;
      break;
#endif /* NTCPIP__LWIP_SO_RCVBUF */
#if NTCPIP__LWIP_UDP
    case SO_NO_CHECK:
      *(int*)optval = (udp_flags(sock->conn->pcb.udp) & UDP_FLAGS_NOCHKSUM) ? 1 : 0;
      break;
#endif /* NTCPIP__LWIP_UDP*/
    }  /* switch (optname) */
    break;

/* Level: IPPROTO_IP */
  case IPPROTO_IP:
    switch (optname) {
    case IP_TTL:
      *(int*)optval = sock->conn->pcb.ip->ttl;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, IP_TTL) = %d\n",
                  s, *(int *)optval));
      break;
    case IP_TOS:
      *(int*)optval = sock->conn->pcb.ip->tos;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, IP_TOS) = %d\n",
                  s, *(int *)optval));
      break;
#if NTCPIP__LWIP_IGMP
    case IP_MULTICAST_TTL:
      *(Uint8*)optval = sock->conn->pcb.ip->ttl;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, IP_MULTICAST_TTL) = %d\n",
                  s, *(int *)optval));
      break;
    case IP_MULTICAST_IF:
      ((struct ntcpip__inAddr*) optval)->s_addr = sock->conn->pcb.udp->multicast_ip.addr;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, IP_MULTICAST_IF) = 0x%"X32_F"\n",
                  s, *(Uint32 *)optval));
      break;
#endif /* NTCPIP__LWIP_IGMP */
    }  /* switch (optname) */
    break;

#if NTCPIP__LWIP_TCP
/* Level: IPPROTO_TCP */
  case IPPROTO_TCP:
    switch (optname) {
    case TCP_NODELAY:
      *(int*)optval = ntcpip_tcpNagleDisabled(sock->conn->pcb.tcp);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_TCP, TCP_NODELAY) = %s\n",
                  s, (*(int*)optval)?"on":"off") );
      break;
    case TCP_KEEPALIVE:
      *(int*)optval = (int)sock->conn->pcb.tcp->keep_idle;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, TCP_KEEPALIVE) = %d\n",
                  s, *(int *)optval));
      break;

#if NTCPIP__LWIP_TCP_KEEPALIVE
    case TCP_KEEPIDLE:
      *(int*)optval = (int)(sock->conn->pcb.tcp->keep_idle/1000);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, TCP_KEEPIDLE) = %d\n",
                  s, *(int *)optval));
      break;
    case TCP_KEEPINTVL:
      *(int*)optval = (int)(sock->conn->pcb.tcp->keep_intvl/1000);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, TCP_KEEPINTVL) = %d\n",
                  s, *(int *)optval));
      break;
    case TCP_KEEPCNT:
      *(int*)optval = (int)sock->conn->pcb.tcp->keep_cnt;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_IP, TCP_KEEPCNT) = %d\n",
                  s, *(int *)optval));
      break;
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */

    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_TCP */
#if NTCPIP__LWIP_UDP && NTCPIP__LWIP_UDPLITE
  /* Level: IPPROTO_UDPLITE */
  case IPPROTO_UDPLITE:
    switch (optname) {
    case UDPLITE_SEND_CSCOV:
      *(int*)optval = sock->conn->pcb.udp->chksum_len_tx;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_UDPLITE, UDPLITE_SEND_CSCOV) = %d\n",
                  s, (*(int*)optval)) );
      break;
    case UDPLITE_RECV_CSCOV:
      *(int*)optval = sock->conn->pcb.udp->chksum_len_rx;
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_getsockopt(%d, IPPROTO_UDPLITE, UDPLITE_RECV_CSCOV) = %d\n",
                  s, (*(int*)optval)) );
      break;
    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_UDP */
  } /* switch (level) */
  ntcpip__sysSemSignal(sock->conn->op_completed);
}

int
lwip_setsockopt(int s, int level, int optname, const void *optval, socklen_t optlen)
{
  struct lwip_socket *sock = get_socket(s);
  int err = NTCPIP_ERR_OK;
  struct lwip_setgetsockopt_data data;

  if (!sock)
    return -1;

  if (NULL == optval) {
    sock_set_errno(sock, NTCPIP__EFAULT);
    return -1;
  }

  /* Do length and type checks for the various options first, to keep it readable. */
  switch (level) {

/* Level: SOL_SOCKET */
  case SOL_SOCKET:
    switch (optname) {

    case SO_BROADCAST:
    /* UNIMPL case SO_DEBUG: */
    /* UNIMPL case SO_DONTROUTE: */
    case SO_KEEPALIVE:
    /* UNIMPL case case SO_CONTIMEO: */
    /* UNIMPL case case SO_SNDTIMEO: */
#if NTCPIP__LWIP_SO_RCVTIMEO
    case SO_RCVTIMEO:
#endif /* NTCPIP__LWIP_SO_RCVTIMEO */
#if NTCPIP__LWIP_SO_RCVBUF
    case SO_RCVBUF:
#endif /* NTCPIP__LWIP_SO_RCVBUF */
    /* UNIMPL case SO_OOBINLINE: */
    /* UNIMPL case SO_SNDBUF: */
    /* UNIMPL case SO_RCVLOWAT: */
    /* UNIMPL case SO_SNDLOWAT: */
#if NTCPIP__SO_REUSE
    case SO_REUSEADDR:
    case SO_REUSEPORT:
#endif /* NTCPIP__SO_REUSE */
    /* UNIMPL case SO_USELOOPBACK: */
      if (optlen < sizeof(int)) {
        err = NTCPIP__EINVAL;
      }
      break;
    case SO_NO_CHECK:
      if (optlen < sizeof(int)) {
        err = NTCPIP__EINVAL;
      }
#if NTCPIP__LWIP_UDP
      if ((sock->conn->type != NTCPIP_NETCONN_UDP) ||
          ((udp_flags(sock->conn->pcb.udp) & UDP_FLAGS_UDPLITE) != 0)) {
        /* this flag is only available for UDP, not for UDP lite */
        err = NTCPIP__EAFNOSUPPORT;
      }
#endif /* NTCPIP__LWIP_UDP */
      break;
    default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, SOL_SOCKET, UNIMPL: optname=0x%x, ..)\n",
                  s, optname));
      err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;

/* Level: IPPROTO_IP */
  case IPPROTO_IP:
    switch (optname) {
    /* UNIMPL case IP_HDRINCL: */
    /* UNIMPL case IP_RCVDSTADDR: */
    /* UNIMPL case IP_RCVIF: */
    case IP_TTL:
    case IP_TOS:
      if (optlen < sizeof(int)) {
        err = NTCPIP__EINVAL;
      }
      break;
#if NTCPIP__LWIP_IGMP
    case IP_MULTICAST_TTL:
      if (optlen < sizeof(Uint8)) {
        err = NTCPIP__EINVAL;
      }
      if (NTCPIP__NETCONNTYPE_GROUP(sock->conn->type) != NTCPIP_NETCONN_UDP) {
        err = NTCPIP__EAFNOSUPPORT;
      }
      break;
    case IP_MULTICAST_IF:
      if (optlen < sizeof(struct ntcpip__inAddr)) {
        err = NTCPIP__EINVAL;
      }
      if (NTCPIP__NETCONNTYPE_GROUP(sock->conn->type) != NTCPIP_NETCONN_UDP) {
        err = NTCPIP__EAFNOSUPPORT;
      }
      break;
    case IP_ADD_MEMBERSHIP:
    case IP_DROP_MEMBERSHIP:
      if (optlen < sizeof(struct ip_mreq)) {
        err = NTCPIP__EINVAL;
      }
      if (NTCPIP__NETCONNTYPE_GROUP(sock->conn->type) != NTCPIP_NETCONN_UDP) {
        err = NTCPIP__EAFNOSUPPORT;
      }
      break;
#endif /* NTCPIP__LWIP_IGMP */
      default:
        NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_IP, UNIMPL: optname=0x%x, ..)\n",
                    s, optname));
        err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;

#if NTCPIP__LWIP_TCP
/* Level: IPPROTO_TCP */
  case IPPROTO_TCP:
    if (optlen < sizeof(int)) {
      err = NTCPIP__EINVAL;
      break;
    }

    /* If this is no TCP socket, ignore any options. */
    if (sock->conn->type != NTCPIP_NETCONN_TCP)
      return 0;

    switch (optname) {
    case TCP_NODELAY:
    case TCP_KEEPALIVE:
#if NTCPIP__LWIP_TCP_KEEPALIVE
    case TCP_KEEPIDLE:
    case TCP_KEEPINTVL:
    case TCP_KEEPCNT:
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */
      break;

    default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_TCP, UNIMPL: optname=0x%x, ..)\n",
                  s, optname));
      err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_TCP */
#if NTCPIP__LWIP_UDP && NTCPIP__LWIP_UDPLITE
/* Level: IPPROTO_UDPLITE */
  case IPPROTO_UDPLITE:
    if (optlen < sizeof(int)) {
      err = NTCPIP__EINVAL;
      break;
    }

    /* If this is no UDP lite socket, ignore any options. */
    if (sock->conn->type != NTCPIP_NETCONN_UDPLITE)
      return 0;

    switch (optname) {
    case UDPLITE_SEND_CSCOV:
    case UDPLITE_RECV_CSCOV:
      break;

    default:
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_UDPLITE, UNIMPL: optname=0x%x, ..)\n",
                  s, optname));
      err = NTCPIP__ENOPROTOOPT;
    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_UDP && NTCPIP__LWIP_UDPLITE */
/* UNDEFINED LEVEL */
  default:
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, level=0x%x, UNIMPL: optname=0x%x, ..)\n",
                s, level, optname));
    err = NTCPIP__ENOPROTOOPT;
  }  /* switch (level) */


  if (err != NTCPIP_ERR_OK) {
    sock_set_errno(sock, err);
    return -1;
  }


  /* Now do the actual option processing */
  data.sock = sock;
  data.level = level;
  data.optname = optname;
  data.optval = (void*)optval;
  data.optlen = &optlen;
  data.err = err;
  ntcpip__tcpipCb(lwip_setsockopt_internal, &data);
  ntcpip__sysArchSemWait(sock->conn->op_completed, 0);
  /* maybe lwip_setsockopt_internal has changed err */
  err = data.err;

  sock_set_errno(sock, err);
  return err ? -1 : 0;
}

static void
lwip_setsockopt_internal(void *arg)
{
  struct lwip_socket *sock;
#ifdef LWIP_DEBUG
  int s;
#endif /* LWIP_DEBUG */
  int level, optname;
  const void *optval;
  struct lwip_setgetsockopt_data *data;

  NTCPIP__LWIP_ASSERT("arg != NULL", arg != NULL);

  data = (struct lwip_setgetsockopt_data*)arg;
  sock = data->sock;
#ifdef LWIP_DEBUG
  s = data->s;
#endif /* LWIP_DEBUG */
  level = data->level;
  optname = data->optname;
  optval = data->optval;

  switch (level) {

/* Level: SOL_SOCKET */
  case SOL_SOCKET:
    switch (optname) {

    /* The option flags */
    case SO_BROADCAST:
    /* UNIMPL case SO_DEBUG: */
    /* UNIMPL case SO_DONTROUTE: */
    case SO_KEEPALIVE:
    /* UNIMPL case SO_OOBINCLUDE: */
#if NTCPIP__SO_REUSE
    case SO_REUSEADDR:
    case SO_REUSEPORT:
#endif /* NTCPIP__SO_REUSE */
    /* UNIMPL case SO_USELOOPBACK: */
      if (*(int*)optval) {
        sock->conn->pcb.ip->so_options |= optname;
      } else {
        sock->conn->pcb.ip->so_options &= ~optname;
      }
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, SOL_SOCKET, optname=0x%x, ..) -> %s\n",
                  s, optname, (*(int*)optval?"on":"off")));
      break;
#if NTCPIP__LWIP_SO_RCVTIMEO
    case SO_RCVTIMEO:
      sock->conn->recv_timeout = ( *(int*)optval );
      break;
#endif /* NTCPIP__LWIP_SO_RCVTIMEO */
#if NTCPIP__LWIP_SO_RCVBUF
    case SO_RCVBUF:
      sock->conn->recv_bufsize = ( *(int*)optval );
      break;
#endif /* NTCPIP__LWIP_SO_RCVBUF */
#if NTCPIP__LWIP_UDP
    case SO_NO_CHECK:
      if (*(int*)optval) {
        udp_setflags(sock->conn->pcb.udp, udp_flags(sock->conn->pcb.udp) | UDP_FLAGS_NOCHKSUM);
      } else {
        udp_setflags(sock->conn->pcb.udp, udp_flags(sock->conn->pcb.udp) & ~UDP_FLAGS_NOCHKSUM);
      }
      break;
#endif /* NTCPIP__LWIP_UDP */
    }  /* switch (optname) */
    break;

/* Level: IPPROTO_IP */
  case IPPROTO_IP:
    switch (optname) {
    case IP_TTL:
      sock->conn->pcb.ip->ttl = (Uint8)(*(int*)optval);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_IP, IP_TTL, ..) -> %d\n",
                  s, sock->conn->pcb.ip->ttl));
      break;
    case IP_TOS:
      sock->conn->pcb.ip->tos = (Uint8)(*(int*)optval);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_IP, IP_TOS, ..)-> %d\n",
                  s, sock->conn->pcb.ip->tos));
      break;
#if NTCPIP__LWIP_IGMP
    case IP_MULTICAST_TTL:
      sock->conn->pcb.udp->ttl = (Uint8)(*(Uint8*)optval);
      break;
    case IP_MULTICAST_IF:
      sock->conn->pcb.udp->multicast_ip.addr = ((struct ntcpip__inAddr*) optval)->s_addr;
      break;
    case IP_ADD_MEMBERSHIP:
    case IP_DROP_MEMBERSHIP:
      {
        /* If this is a TCP or a RAW socket, ignore these options. */
        struct ip_mreq *imr = (struct ip_mreq *)optval;
        if(optname == IP_ADD_MEMBERSHIP){
          data->err = ntcpip_igmpJoingroup((struct ntcpip_ipAddr*)&(imr->imr_interface.s_addr), (struct ntcpip_ipAddr*)&(imr->imr_multiaddr.s_addr));
        } else {
          data->err = ntcpip__igmpLeavegroup((struct ntcpip_ipAddr*)&(imr->imr_interface.s_addr), (struct ntcpip_ipAddr*)&(imr->imr_multiaddr.s_addr));
        }
        if(data->err != NTCPIP_ERR_OK) {
          data->err = NTCPIP__EADDRNOTAVAIL;
        }
      }
      break;
#endif /* NTCPIP__LWIP_IGMP */
    }  /* switch (optname) */
    break;

#if NTCPIP__LWIP_TCP
/* Level: IPPROTO_TCP */
  case IPPROTO_TCP:
    switch (optname) {
    case TCP_NODELAY:
      if (*(int*)optval) {
        ntcpip_tcpNagleDisable(sock->conn->pcb.tcp);
      } else {
        ntcpip_tcpNagleEnable(sock->conn->pcb.tcp);
      }
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_TCP, TCP_NODELAY) -> %s\n",
                  s, (*(int *)optval)?"on":"off") );
      break;
    case TCP_KEEPALIVE:
      sock->conn->pcb.tcp->keep_idle = (Uint32)(*(int*)optval);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_TCP, TCP_KEEPALIVE) -> %"U32_F"\n",
                  s, sock->conn->pcb.tcp->keep_idle));
      break;

#if NTCPIP__LWIP_TCP_KEEPALIVE
    case TCP_KEEPIDLE:
      sock->conn->pcb.tcp->keep_idle = 1000*(Uint32)(*(int*)optval);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_TCP, TCP_KEEPIDLE) -> %"U32_F"\n",
                  s, sock->conn->pcb.tcp->keep_idle));
      break;
    case TCP_KEEPINTVL:
      sock->conn->pcb.tcp->keep_intvl = 1000*(Uint32)(*(int*)optval);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_TCP, TCP_KEEPINTVL) -> %"U32_F"\n",
                  s, sock->conn->pcb.tcp->keep_intvl));
      break;
    case TCP_KEEPCNT:
      sock->conn->pcb.tcp->keep_cnt = (Uint32)(*(int*)optval);
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_TCP, TCP_KEEPCNT) -> %"U32_F"\n",
                  s, sock->conn->pcb.tcp->keep_cnt));
      break;
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */

    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_TCP*/
#if NTCPIP__LWIP_UDP && NTCPIP__LWIP_UDPLITE
  /* Level: IPPROTO_UDPLITE */
  case IPPROTO_UDPLITE:
    switch (optname) {
    case UDPLITE_SEND_CSCOV:
      if ((*(int*)optval != 0) && (*(int*)optval < 8)) {
        /* don't allow illegal values! */
        sock->conn->pcb.udp->chksum_len_tx = 8;
      } else {
        sock->conn->pcb.udp->chksum_len_tx = *(int*)optval;
      }
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_UDPLITE, UDPLITE_SEND_CSCOV) -> %d\n",
                  s, (*(int*)optval)) );
      break;
    case UDPLITE_RECV_CSCOV:
      if ((*(int*)optval != 0) && (*(int*)optval < 8)) {
        /* don't allow illegal values! */
        sock->conn->pcb.udp->chksum_len_rx = 8;
      } else {
        sock->conn->pcb.udp->chksum_len_rx = *(int*)optval;
      }
      NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_setsockopt(%d, IPPROTO_UDPLITE, UDPLITE_RECV_CSCOV) -> %d\n",
                  s, (*(int*)optval)) );
      break;
    }  /* switch (optname) */
    break;
#endif /* NTCPIP__LWIP_UDP */
  }  /* switch (level) */
  ntcpip__sysSemSignal(sock->conn->op_completed);
}

int
lwip_ioctl(int s, long cmd, void *argp)
{
  struct lwip_socket *sock = get_socket(s);
  Uint16 buflen = 0;
  Int16 recv_avail;

  if (!sock)
    return -1;

  switch (cmd) {
  case FIONREAD:
    if (!argp) {
      sock_set_errno(sock, NTCPIP__EINVAL);
      return -1;
    }

    NTCPIP__SYS_ARCH_GET(sock->conn->recv_avail, recv_avail);
    if (recv_avail < 0)
      recv_avail = 0;
    *((Uint16*)argp) = (Uint16)recv_avail;

    /* Check if there is data left from the last recv operation. /maq 041215 */
    if (sock->lastdata) {
      buflen = ntcpip_netbufLen(sock->lastdata);
      buflen -= sock->lastoffset;

      *((Uint16*)argp) += buflen;
    }

    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_ioctl(%d, FIONREAD, %p) = %"U16_F"\n", s, argp, *((Uint16*)argp)));
    sock_set_errno(sock, 0);
    return 0;

  case FIONBIO:
    if (argp && *(Uint32*)argp)
      sock->flags |= O_NONBLOCK;
    else
      sock->flags &= ~O_NONBLOCK;
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_ioctl(%d, FIONBIO, %d)\n", s, !!(sock->flags & O_NONBLOCK)));
    sock_set_errno(sock, 0);
    return 0;

  default:
    NTCPIP__LWIP_DEBUGF(NTCPIP__SOCKETS_DEBUG, ("lwip_ioctl(%d, UNIMPL: 0x%lx, %p)\n", s, cmd, argp));
    sock_set_errno(sock, NTCPIP__ENOSYS); /* not yet implemented */
    return -1;
  } /* switch (cmd) */
}

#endif /* NTCPIP__LWIP_SOCKET */

