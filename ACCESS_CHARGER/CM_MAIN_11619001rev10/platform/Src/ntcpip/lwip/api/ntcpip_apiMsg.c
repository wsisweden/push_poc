/**
 * @file
 * Sequential API Internal module
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_NETCONN /* don't build if not configured for use in lwipopts.h */

#include "../api_msg.h"

#include "ntcpip/ip.h"
#include "ntcpip/udp.h"
#include "ntcpip/tcp.h"
#include "../raw.h"

#include "../memp.h"
#include "ntcpip/tcpip.h"
#include "ntcpip/igmp.h"
#include "../dns.h"

#include <string.h>

/* forward declarations */
#if NTCPIP__LWIP_TCP
static ntcpip_Err do_writemore(struct ntcpip__netconn *conn);
static void do_close_internal(struct ntcpip__netconn *conn);
#endif

#if NTCPIP__LWIP_RAW
/**
 * Receive callback function for RAW netconns.
 * Doesn't 'eat' the packet, only references it and sends it to
 * conn->recvmbox
 *
 * @see raw.h (struct ntcpip__rawPcb.recv) for parameters and return value
 */
static Uint8
recv_raw(void *arg, struct ntcpip__rawPcb *pcb, struct ntcpip_pbuf *p,
    struct ntcpip_ipAddr *addr)
{
  struct ntcpip_pbuf *q;
  struct ntcpip__netbuf *buf;
  struct ntcpip__netconn *conn;
#if NTCPIP__LWIP_SO_RCVBUF
  int recv_avail;
#endif /* NTCPIP__LWIP_SO_RCVBUF */

  NTCPIP__LWIP_UNUSED_ARG(addr);
  conn = arg;

#if NTCPIP__LWIP_SO_RCVBUF
  NTCPIP__SYS_ARCH_GET(conn->recv_avail, recv_avail);
  if ((conn != NULL) && (conn->recvmbox != NTCPIP__SYS_MBOX_NULL) &&
      ((recv_avail + (int)(p->tot_len)) <= conn->recv_bufsize)) {
#else  /* NTCPIP__LWIP_SO_RCVBUF */
  if ((conn != NULL) && (conn->recvmbox != NTCPIP__SYS_MBOX_NULL)) {
#endif /* NTCPIP__LWIP_SO_RCVBUF */
    /* copy the whole packet into new pbufs */
    q = ntcpip_pbufAlloc(NTCPIP_PBUF_RAW, p->tot_len, NTCPIP_PBUF_RAM);
    if(q != NULL) {
      if (ntcpip_pbufCopy(q, p) != NTCPIP_ERR_OK) {
        ntcpip_pbufFree(q);
        q = NULL;
      }
    }

    if(q != NULL) {
      buf = ntcpip__mempMalloc(MEMP_NETBUF);
      if (buf == NULL) {
        ntcpip_pbufFree(q);
        return 0;
      }

      buf->p = q;
      buf->ptr = q;
      buf->addr = &(((struct ip_hdr*)(q->payload))->src);
      buf->port = pcb->protocol;

      if (ntcpip__sysMboxTrypost(conn->recvmbox, buf) != NTCPIP_ERR_OK) {
        ntcpip_netbufDelete(buf);
        return 0;
      } else {
        NTCPIP__SYS_ARCH_INC(conn->recv_avail, q->tot_len);
        /* Register event with callback */
        NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVPLUS, q->tot_len);
      }
    }
  }

  return 0; /* do not eat the packet */
}
#endif /* NTCPIP__LWIP_RAW*/

#if NTCPIP__LWIP_UDP
/**
 * Receive callback function for UDP netconns.
 * Posts the packet to conn->recvmbox or deletes it on memory error.
 *
 * @see udp.h (struct ntcpip__udpPcb.recv) for parameters
 */
static void
recv_udp(void *arg, struct ntcpip__udpPcb *pcb, struct ntcpip_pbuf *p,
   struct ntcpip_ipAddr *addr, Uint16 port)
{
  struct ntcpip__netbuf *buf;
  struct ntcpip__netconn *conn;
#if NTCPIP__LWIP_SO_RCVBUF
  int recv_avail;
#endif /* NTCPIP__LWIP_SO_RCVBUF */

  NTCPIP__LWIP_UNUSED_ARG(pcb); /* only used for asserts... */
  NTCPIP__LWIP_ASSERT("recv_udp must have a pcb argument", pcb != NULL);
  NTCPIP__LWIP_ASSERT("recv_udp must have an argument", arg != NULL);
  conn = arg;
  NTCPIP__LWIP_ASSERT("recv_udp: recv for wrong pcb!", conn->pcb.udp == pcb);

#if NTCPIP__LWIP_SO_RCVBUF
  NTCPIP__SYS_ARCH_GET(conn->recv_avail, recv_avail);
  if ((conn == NULL) || (conn->recvmbox == NTCPIP__SYS_MBOX_NULL) ||
      ((recv_avail + (int)(p->tot_len)) > conn->recv_bufsize)) {
#else  /* NTCPIP__LWIP_SO_RCVBUF */
  if ((conn == NULL) || (conn->recvmbox == NTCPIP__SYS_MBOX_NULL)) {
#endif /* NTCPIP__LWIP_SO_RCVBUF */
    ntcpip_pbufFree(p);
    return;
  }

  buf = ntcpip__mempMalloc(MEMP_NETBUF);
  if (buf == NULL) {
    ntcpip_pbufFree(p);
    return;
  } else {
    buf->p = p;
    buf->ptr = p;
    buf->addr = addr;
    buf->port = port;
#if NTCPIP__NETBUF_RECVINFO
    {
      const struct ip_hdr* iphdr = ntcpip_ipCurrentHeader();
      /* get the UDP header - always in the first pbuf, ensured by ntcpip__udpInput */
      const struct udp_hdr* udphdr = (void*)(((char*)iphdr)+ (IPH_HL(iphdr) * 4)); /* tlarsu: The original code ("+ IPH_LEN(iphdr));") does not seem to work. Using IPH_HL instead */
      buf->toaddr = (struct ntcpip_ipAddr*)&iphdr->dest;
      buf->toport = udphdr->dest;
    }
#endif /* NTCPIP__NETBUF_RECVINFO */
  }

  if (ntcpip__sysMboxTrypost(conn->recvmbox, buf) != NTCPIP_ERR_OK) {
    ntcpip_netbufDelete(buf);
    return;
  } else {
    NTCPIP__SYS_ARCH_INC(conn->recv_avail, p->tot_len);
    /* Register event with callback */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVPLUS, p->tot_len);
  }
}
#endif /* NTCPIP__LWIP_UDP */

#if NTCPIP__LWIP_TCP
/**
 * Receive callback function for TCP netconns.
 * Posts the packet to conn->recvmbox, but doesn't delete it on errors.
 *
 * @see tcp.h (struct ntcpip__tcpPcb.recv) for parameters and return value
 */
static ntcpip_Err
recv_tcp(void *arg, struct ntcpip__tcpPcb *pcb, struct ntcpip_pbuf *p, ntcpip_Err err)
{
  struct ntcpip__netconn *conn;
  Uint16 len;

  NTCPIP__LWIP_UNUSED_ARG(pcb);
  NTCPIP__LWIP_ASSERT("recv_tcp must have a pcb argument", pcb != NULL);
  NTCPIP__LWIP_ASSERT("recv_tcp must have an argument", arg != NULL);
  conn = arg;
  NTCPIP__LWIP_ASSERT("recv_tcp: recv for wrong pcb!", conn->pcb.tcp == pcb);

  if ((conn == NULL) || (conn->recvmbox == NTCPIP__SYS_MBOX_NULL)) {
    return NTCPIP_ERR_VAL;
  }

  conn->err = err;
  if (p != NULL) {
    len = p->tot_len;
    NTCPIP__SYS_ARCH_INC(conn->recv_avail, len);
  } else {
    len = 0;
  }

  if (ntcpip__sysMboxTrypost(conn->recvmbox, p) != NTCPIP_ERR_OK) {
    return NTCPIP_ERR_MEM;
  } else {
    /* Register event with callback */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVPLUS, len);
  }

  return NTCPIP_ERR_OK;
}

/**
 * Poll callback function for TCP netconns.
 * Wakes up an application thread that waits for a connection to close
 * or data to be sent. The application thread then takes the
 * appropriate action to go on.
 *
 * Signals the conn->sem.
 * ntcpip_netconnClose waits for conn->sem if closing failed.
 *
 * @see tcp.h (struct ntcpip__tcpPcb.poll) for parameters and return value
 */
static ntcpip_Err
poll_tcp(void *arg, struct ntcpip__tcpPcb *pcb)
{
  struct ntcpip__netconn *conn = arg;

  NTCPIP__LWIP_UNUSED_ARG(pcb);
  NTCPIP__LWIP_ASSERT("conn != NULL", (conn != NULL));

  if (conn->state == NTCPIP__NETCONN_WRITE) {
    do_writemore(conn);
  } else if (conn->state == NTCPIP__NETCONN_CLOSE) {
    do_close_internal(conn);
  }

  return NTCPIP_ERR_OK;
}

/**
 * Sent callback function for TCP netconns.
 * Signals the conn->sem and calls NTCPIP__APIEVENT.
 * ntcpip_netconnWrite waits for conn->sem if send buffer is low.
 *
 * @see tcp.h (struct ntcpip__tcpPcb.sent) for parameters and return value
 */
static ntcpip_Err
sent_tcp(void *arg, struct ntcpip__tcpPcb *pcb, Uint16 len)
{
  struct ntcpip__netconn *conn = arg;

  NTCPIP__LWIP_UNUSED_ARG(pcb);
  NTCPIP__LWIP_ASSERT("conn != NULL", (conn != NULL));

  if (conn->state == NTCPIP__NETCONN_WRITE) {
    NTCPIP__LWIP_ASSERT("conn->pcb.tcp != NULL", conn->pcb.tcp != NULL);
    do_writemore(conn);
  } else if (conn->state == NTCPIP__NETCONN_CLOSE) {
    do_close_internal(conn);
  }

  if (conn) {
    if ((conn->pcb.tcp != NULL) && (ntcpip_tcpSndbuf(conn->pcb.tcp) > NTCPIP__TCP_SNDLOWAT)) {
      NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_SENDPLUS, len);
    }
  }
  
  return NTCPIP_ERR_OK;
}

/**
 * Error callback function for TCP netconns.
 * Signals conn->sem, posts to all conn mboxes and calls NTCPIP__APIEVENT.
 * The application thread has then to decide what to do.
 *
 * @see tcp.h (struct ntcpip__tcpPcb.err) for parameters
 */
static void
err_tcp(void *arg, ntcpip_Err err)
{
  struct ntcpip__netconn *conn;

  conn = arg;
  NTCPIP__LWIP_ASSERT("conn != NULL", (conn != NULL));

  conn->pcb.tcp = NULL;

  conn->err = err;
  if (conn->recvmbox != NTCPIP__SYS_MBOX_NULL) {
    /* Register event with callback */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVPLUS, 0);
    ntcpip__sysMboxPost(conn->recvmbox, NULL);
  }
  if (conn->op_completed != NTCPIP__SYS_SEM_NULL && conn->state == NTCPIP__NETCONN_CONNECT) {
    conn->state = NTCPIP__NETCONN_NONE;
    ntcpip__sysSemSignal(conn->op_completed);
  }
  if (conn->acceptmbox != NTCPIP__SYS_MBOX_NULL) {
    /* Register event with callback */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVPLUS, 0);
    ntcpip__sysMboxPost(conn->acceptmbox, NULL);
  }
  if ((conn->state == NTCPIP__NETCONN_WRITE) || (conn->state == NTCPIP__NETCONN_CLOSE)) {
    /* calling do_writemore/do_close_internal is not necessary
       since the pcb has already been deleted! */
    conn->state = NTCPIP__NETCONN_NONE;
    /* wake up the waiting task */
    ntcpip__sysSemSignal(conn->op_completed);
  }
}

/**
 * Setup a tcp_pcb with the correct callback function pointers
 * and their arguments.
 *
 * @param conn the TCP netconn to setup
 */
static void
setup_tcp(struct ntcpip__netconn *conn)
{
  struct ntcpip__tcpPcb *pcb;

  pcb = conn->pcb.tcp;
  ntcpip_tcpArg(pcb, conn);
  ntcpip_tcpRecv(pcb, recv_tcp);
  ntcpip_tcpSent(pcb, sent_tcp);
  ntcpip_tcpPoll(pcb, poll_tcp, 4);
  ntcpip_tcpErr(pcb, err_tcp);
}

/**
 * Accept callback function for TCP netconns.
 * Allocates a new netconn and posts that to conn->acceptmbox.
 *
 * @see tcp.h (struct tcp_pcb_listen.accept) for parameters and return value
 */
static ntcpip_Err
accept_function(void *arg, struct ntcpip__tcpPcb *newpcb, ntcpip_Err err)
{
  struct ntcpip__netconn *newconn;
  struct ntcpip__netconn *conn;

#if NTCPIP__API_MSG_DEBUG
#if NTCPIP__TCP_DEBUG
  tcp_debug_print_state(newpcb->state);
#endif /* NTCPIP__TCP_DEBUG */
#endif /* NTCPIP__API_MSG_DEBUG */
  conn = (struct ntcpip__netconn *)arg;

  NTCPIP__LWIP_ERROR("accept_function: invalid conn->acceptmbox",
             conn->acceptmbox != NTCPIP__SYS_MBOX_NULL, return NTCPIP_ERR_VAL;);

  /* We have to set the callback here even though
   * the new socket is unknown. conn->socket is marked as -1. */
  newconn = ntcpip__netconnAlloc(conn->type, conn->callback);
  if (newconn == NULL) {
    return NTCPIP_ERR_MEM;
  }
  newconn->pcb.tcp = newpcb;
  setup_tcp(newconn);
  newconn->err = err;

  /*
   * Tietolaite:	Added copying of the pUtil pointer here so that it is always
   *				set for accepted netconns.
   */
  newconn->pUtil = conn->pUtil;

  if (ntcpip__sysMboxTrypost(conn->acceptmbox, newconn) != NTCPIP_ERR_OK) {
    /* When returning != NTCPIP_ERR_OK, the connection is aborted in tcp_process(),
       so do nothing here! */
    newconn->pcb.tcp = NULL;
    ntcpip__netconnFree(newconn);
    return NTCPIP_ERR_MEM;
  } else {
    /* Register event with callback */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVPLUS, 0);
  }

  return NTCPIP_ERR_OK;
}
#endif /* NTCPIP__LWIP_TCP */

/**
 * Create a new pcb of a specific type.
 * Called from ntcpip__apimsgDoNewConn().
 *
 * @param msg the ntcpip__apiMsgMsg describing the connection type
 * @return msg->conn->err, but the return value is currently ignored
 */
static ntcpip_Err
pcb_new(struct ntcpip__apiMsgMsg *msg)
{
   msg->conn->err = NTCPIP_ERR_OK;

   NTCPIP__LWIP_ASSERT("pcb_new: pcb already allocated", msg->conn->pcb.tcp == NULL);

   /* Allocate a PCB for this connection */
   switch(NTCPIP__NETCONNTYPE_GROUP(msg->conn->type)) {
#if NTCPIP__LWIP_RAW
   case NTCPIP_NETCONN_RAW:
     msg->conn->pcb.raw = raw_new(msg->msg.n.proto);
     if(msg->conn->pcb.raw == NULL) {
       msg->conn->err = NTCPIP_ERR_MEM;
       break;
     }
     raw_recv(msg->conn->pcb.raw, recv_raw, msg->conn);
     break;
#endif /* NTCPIP__LWIP_RAW */
#if NTCPIP__LWIP_UDP
   case NTCPIP_NETCONN_UDP:
     msg->conn->pcb.udp = ntcpip_udpNew();
     if(msg->conn->pcb.udp == NULL) {
       msg->conn->err = NTCPIP_ERR_MEM;
       break;
     }
#if NTCPIP__LWIP_UDPLITE
     if (msg->conn->type==NTCPIP_NETCONN_UDPLITE) {
       udp_setflags(msg->conn->pcb.udp, UDP_FLAGS_UDPLITE);
     }
#endif /* NTCPIP__LWIP_UDPLITE */
     if (msg->conn->type==NTCPIP_NETCONN_UDPNOCHKSUM) {
       udp_setflags(msg->conn->pcb.udp, UDP_FLAGS_NOCHKSUM);
     }
     ntcpip_udpRecv(msg->conn->pcb.udp, recv_udp, msg->conn);
     break;
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
   case NTCPIP_NETCONN_TCP:
     msg->conn->pcb.tcp = ntcpip_tcpNew();
     if(msg->conn->pcb.tcp == NULL) {
       msg->conn->err = NTCPIP_ERR_MEM;
       break;
     }
     setup_tcp(msg->conn);
     break;
#endif /* NTCPIP__LWIP_TCP */
   default:
     /* Unsupported netconn type, e.g. protocol disabled */
     msg->conn->err = NTCPIP_ERR_VAL;
     break;
   }

  return msg->conn->err;
}

/**
 * Create a new pcb of a specific type inside a netconn.
 * Called from ntcpip_netconnNewWithProtoAndCallback.
 *
 * @param msg the ntcpip__apiMsgMsg describing the connection type
 */
void
ntcpip__apimsgDoNewConn(struct ntcpip__apiMsgMsg *msg)
{
   if(msg->conn->pcb.tcp == NULL) {
     pcb_new(msg);
   }
   /* Else? This "new" connection already has a PCB allocated. */
   /* Is this an error condition? Should it be deleted? */
   /* We currently just are happy and return. */

   NTCPIP__APIMSG_ACK(msg);
}

/**
 * Create a new netconn (of a specific type) that has a callback function.
 * The corresponding pcb is NOT created!
 *
 * @param t the type of 'connection' to create (@see enum ntcpip_netconnType)
 * @param proto the IP protocol for RAW IP pcbs
 * @param callback a function to call on status changes (RX available, TX'ed)
 * @return a newly allocated struct ntcpip__netconn or
 *         NULL on memory error
 */
struct ntcpip__netconn*
ntcpip__netconnAlloc(enum ntcpip_netconnType t, ntcpip__NetconnCallback callback)
{
  struct ntcpip__netconn *conn;
  int size;

  conn = ntcpip__mempMalloc(MEMP_NETCONN);
  if (conn == NULL) {
    return NULL;
  }

  conn->err = NTCPIP_ERR_OK;
  conn->type = t;
  conn->pcb.tcp = NULL;

#if (NTCPIP__DEFAULT_RAW_RECVMBOX_SIZE == NTCPIP__DEFAULT_UDP_RECVMBOX_SIZE) && \
    (NTCPIP__DEFAULT_RAW_RECVMBOX_SIZE == NTCPIP__DEFAULT_TCP_RECVMBOX_SIZE)
  size = NTCPIP__DEFAULT_RAW_RECVMBOX_SIZE;
#else
  switch(NTCPIP__NETCONNTYPE_GROUP(t)) {
#if NTCPIP__LWIP_RAW
  case NTCPIP_NETCONN_RAW:
    size = NTCPIP__DEFAULT_RAW_RECVMBOX_SIZE;
    break;
#endif /* NTCPIP__LWIP_RAW */
#if NTCPIP__LWIP_UDP
  case NTCPIP_NETCONN_UDP:
    size = NTCPIP__DEFAULT_UDP_RECVMBOX_SIZE;
    break;
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
  case NTCPIP_NETCONN_TCP:
    size = NTCPIP__DEFAULT_TCP_RECVMBOX_SIZE;
    break;
#endif /* NTCPIP__LWIP_TCP */
  default:
    NTCPIP__LWIP_ASSERT("ntcpip__netconnAlloc: undefined ntcpip_netconnType", 0);
    break;
  }
#endif

  if ((conn->op_completed = ntcpip__sysSemNew(0)) == NTCPIP__SYS_SEM_NULL) {
    ntcpip__mempFree(MEMP_NETCONN, conn);
    return NULL;
  }
  if ((conn->recvmbox = ntcpip__sysMboxNew(size)) == NTCPIP__SYS_MBOX_NULL) {
    ntcpip__sysSemFree(conn->op_completed);
    ntcpip__mempFree(MEMP_NETCONN, conn);
    return NULL;
  }

  conn->acceptmbox   = NTCPIP__SYS_MBOX_NULL;
  conn->state        = NTCPIP__NETCONN_NONE;
  /* initialize socket to -1 since 0 is a valid socket */
  conn->socket       = -1;
  conn->pUtil		 = NULL;
  conn->callback     = callback;
  conn->recv_avail   = 0;
#if NTCPIP__LWIP_TCP
  conn->write_msg    = NULL;
  conn->write_offset = 0;
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
  conn->write_delayed = 0;
#endif /* NTCPIP__LWIP_TCPIP_CORE_LOCKING */
#endif /* NTCPIP__LWIP_TCP */
#if NTCPIP__LWIP_SO_RCVTIMEO
  conn->recv_timeout = 0;
#endif /* NTCPIP__LWIP_SO_RCVTIMEO */
#if NTCPIP__LWIP_SO_RCVBUF
  conn->recv_bufsize = RECV_BUFSIZE_DEFAULT;
#endif /* NTCPIP__LWIP_SO_RCVBUF */
  return conn;
}

/**
 * Delete a netconn and all its resources.
 * The pcb is NOT freed (since we might not be in the right thread context do this).
 *
 * @param conn the netconn to free
 */
void
ntcpip__netconnFree(struct ntcpip__netconn *conn)
{
  void *mem;
  NTCPIP__LWIP_ASSERT("PCB must be deallocated outside this function", conn->pcb.tcp == NULL);

  /* Drain the recvmbox. */
  if (conn->recvmbox != NTCPIP__SYS_MBOX_NULL) {
    while (ntcpip__sysMboxTryFetch(conn->recvmbox, &mem) != NTCPIP__SYS_MBOX_EMPTY) {
      if (conn->type == NTCPIP_NETCONN_TCP) {
        if(mem != NULL) {
          ntcpip_pbufFree((struct ntcpip_pbuf *)mem);
        }
      } else {
        ntcpip_netbufDelete((struct ntcpip__netbuf *)mem);
      }
    }
    ntcpip__sysMboxFree(conn->recvmbox);
    conn->recvmbox = NTCPIP__SYS_MBOX_NULL;
  }

  /* Drain the acceptmbox. */
  if (conn->acceptmbox != NTCPIP__SYS_MBOX_NULL) {
    while (ntcpip__sysMboxTryFetch(conn->acceptmbox, &mem) != NTCPIP__SYS_MBOX_EMPTY) {
      ntcpip_netconnDelete((struct ntcpip__netconn *)mem);
    }
    ntcpip__sysMboxFree(conn->acceptmbox);
    conn->acceptmbox = NTCPIP__SYS_MBOX_NULL;
  }

  ntcpip__sysSemFree(conn->op_completed);
  conn->op_completed = NTCPIP__SYS_SEM_NULL;

  ntcpip__mempFree(MEMP_NETCONN, conn);
}

#if NTCPIP__LWIP_TCP
/**
 * Internal helper function to close a TCP netconn: since this sometimes
 * doesn't work at the first attempt, this function is called from multiple
 * places.
 *
 * @param conn the TCP netconn to close
 */
static void
do_close_internal(struct ntcpip__netconn *conn)
{
  ntcpip_Err err;

  NTCPIP__LWIP_ASSERT("invalid conn", (conn != NULL));
  NTCPIP__LWIP_ASSERT("this is for tcp netconns only", (conn->type == NTCPIP_NETCONN_TCP));
  NTCPIP__LWIP_ASSERT("conn must be in state NTCPIP__NETCONN_CLOSE", (conn->state == NTCPIP__NETCONN_CLOSE));
  NTCPIP__LWIP_ASSERT("pcb already closed", (conn->pcb.tcp != NULL));

  /* Set back some callback pointers */
  ntcpip_tcpArg(conn->pcb.tcp, NULL);
  if (conn->pcb.tcp->state == NTCPIP_LISTEN) {
    ntcpip_tcpAccept(conn->pcb.tcp, NULL);
  } else {
    ntcpip_tcpRecv(conn->pcb.tcp, NULL);
    ntcpip_tcpAccept(conn->pcb.tcp, NULL);
    /* some callbacks have to be reset if ntcpip_tcpClose is not successful */
    ntcpip_tcpSent(conn->pcb.tcp, NULL);
    ntcpip_tcpPoll(conn->pcb.tcp, NULL, 4);
    ntcpip_tcpErr(conn->pcb.tcp, NULL);
  }
  /* Try to close the connection */
  err = ntcpip_tcpClose(conn->pcb.tcp);
  if (err == NTCPIP_ERR_OK) {
    /* Closing succeeded */
    conn->state = NTCPIP__NETCONN_NONE;
    /* Set back some callback pointers as conn is going away */
    conn->pcb.tcp = NULL;
    conn->err = NTCPIP_ERR_OK;
    /* Trigger select() in socket layer. This send should something else so the
       errorfd is set, not the read and write fd! */
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_RCVPLUS, 0);
    NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_SENDPLUS, 0);
    /* wake up the application task */
    ntcpip__sysSemSignal(conn->op_completed);
  } else {
    /* Closing failed, restore some of the callbacks */
    /* Closing of listen pcb will never fail! */
    NTCPIP__LWIP_ASSERT("Closing a listen pcb may not fail!", (conn->pcb.tcp->state != NTCPIP_LISTEN));
    ntcpip_tcpSent(conn->pcb.tcp, sent_tcp);
    ntcpip_tcpPoll(conn->pcb.tcp, poll_tcp, 4);
    ntcpip_tcpErr(conn->pcb.tcp, err_tcp);
    ntcpip_tcpArg(conn->pcb.tcp, conn);
  }
  /* If closing didn't succeed, we get called again either
     from poll_tcp or from sent_tcp */
}
#endif /* NTCPIP__LWIP_TCP */

/**
 * Delete the pcb inside a netconn.
 * Called from ntcpip_netconnDelete.
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoDelconn(struct ntcpip__apiMsgMsg *msg)
{
  if (msg->conn->pcb.tcp != NULL) {
    switch (NTCPIP__NETCONNTYPE_GROUP(msg->conn->type)) {
#if NTCPIP__LWIP_RAW
    case NTCPIP_NETCONN_RAW:
      raw_remove(msg->conn->pcb.raw);
      break;
#endif /* NTCPIP__LWIP_RAW */
#if NTCPIP__LWIP_UDP
    case NTCPIP_NETCONN_UDP:
      msg->conn->pcb.udp->recv_arg = NULL;
      ntcpip_udpRemove(msg->conn->pcb.udp);
      break;
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
    case NTCPIP_NETCONN_TCP:
      msg->conn->state = NTCPIP__NETCONN_CLOSE;
      do_close_internal(msg->conn);
      /* NTCPIP__APIEVENT is called inside do_close_internal, before releasing
         the application thread, so we can return at this point! */
      return;
#endif /* NTCPIP__LWIP_TCP */
    default:
      break;
    }
  }
  /* tcp netconns don't come here! */

  /* Trigger select() in socket layer. This send should something else so the
     errorfd is set, not the read and write fd! */
  NTCPIP__APIEVENT(msg->conn, NTCPIP_NETCONN_EVT_RCVPLUS, 0);
  NTCPIP__APIEVENT(msg->conn, NTCPIP_NETCONN_EVT_SENDPLUS, 0);

  if (msg->conn->op_completed != NTCPIP__SYS_SEM_NULL) {
    ntcpip__sysSemSignal(msg->conn->op_completed);
  }
}

/**
 * Bind a pcb contained in a netconn
 * Called from ntcpip_netconnBind.
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection and containing
 *            the IP address and port to bind to
 */
void
ntcpip__apimsgDoBind(struct ntcpip__apiMsgMsg *msg)
{
  if (!NTCPIP_ERR_IS_FATAL(msg->conn->err)) {
    if (msg->conn->pcb.tcp != NULL) {
      switch (NTCPIP__NETCONNTYPE_GROUP(msg->conn->type)) {
#if NTCPIP__LWIP_RAW
      case NTCPIP_NETCONN_RAW:
        msg->conn->err = raw_bind(msg->conn->pcb.raw, msg->msg.bc.ipaddr);
        break;
#endif /* NTCPIP__LWIP_RAW */
#if NTCPIP__LWIP_UDP
      case NTCPIP_NETCONN_UDP:
        msg->conn->err = ntcpip_udpBind(msg->conn->pcb.udp, msg->msg.bc.ipaddr, msg->msg.bc.port);
        break;
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
      case NTCPIP_NETCONN_TCP:
        msg->conn->err = ntcpip_tcpBind(msg->conn->pcb.tcp, msg->msg.bc.ipaddr, msg->msg.bc.port);
        break;
#endif /* NTCPIP__LWIP_TCP */
      default:
        break;
      }
    } else {
      /* msg->conn->pcb is NULL */
      msg->conn->err = NTCPIP_ERR_VAL;
    }
  }
  NTCPIP__APIMSG_ACK(msg);
}

#if NTCPIP__LWIP_TCP
/**
 * TCP callback function if a connection (opened by ntcpip_tcpConnect/ntcpip__apimsgDoConnect) has
 * been established (or reset by the remote host).
 *
 * @see tcp.h (struct ntcpip__tcpPcb.connected) for parameters and return values
 */
static ntcpip_Err
do_connected(void *arg, struct ntcpip__tcpPcb *pcb, ntcpip_Err err)
{
  struct ntcpip__netconn *conn;

  NTCPIP__LWIP_UNUSED_ARG(pcb);

  conn = arg;

  if (conn == NULL) {
    return NTCPIP_ERR_VAL;
  }

  conn->err = err;
  if ((conn->type == NTCPIP_NETCONN_TCP) && (err == NTCPIP_ERR_OK)) {
    setup_tcp(conn);
  }
  conn->state = NTCPIP__NETCONN_NONE;
  ntcpip__sysSemSignal(conn->op_completed);
  return NTCPIP_ERR_OK;
}
#endif /* NTCPIP__LWIP_TCP */

/**
 * Connect a pcb contained inside a netconn
 * Called from ntcpip_netconnConnect.
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection and containing
 *            the IP address and port to connect to
 */
void
ntcpip__apimsgDoConnect(struct ntcpip__apiMsgMsg *msg)
{
  if (msg->conn->pcb.tcp == NULL) {
    ntcpip__sysSemSignal(msg->conn->op_completed);
    return;
  }

  switch (NTCPIP__NETCONNTYPE_GROUP(msg->conn->type)) {
#if NTCPIP__LWIP_RAW
  case NTCPIP_NETCONN_RAW:
    msg->conn->err = raw_connect(msg->conn->pcb.raw, msg->msg.bc.ipaddr);
    ntcpip__sysSemSignal(msg->conn->op_completed);
    break;
#endif /* NTCPIP__LWIP_RAW */
#if NTCPIP__LWIP_UDP
  case NTCPIP_NETCONN_UDP:
    msg->conn->err = ntcpip_udpConnect(msg->conn->pcb.udp, msg->msg.bc.ipaddr, msg->msg.bc.port);
    ntcpip__sysSemSignal(msg->conn->op_completed);
    break;
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
  case NTCPIP_NETCONN_TCP:
    msg->conn->state = NTCPIP__NETCONN_CONNECT;
    setup_tcp(msg->conn);
    msg->conn->err = ntcpip_tcpConnect(msg->conn->pcb.tcp, msg->msg.bc.ipaddr, msg->msg.bc.port,
                                 do_connected);
    /* ntcpip__sysSemSignal() is called from do_connected (or err_tcp()),
     * when the connection is established! */
    break;
#endif /* NTCPIP__LWIP_TCP */
  default:
    NTCPIP__LWIP_ERROR("Invalid netconn type", 0, do{ msg->conn->err = NTCPIP_ERR_VAL;
      ntcpip__sysSemSignal(msg->conn->op_completed); }while(0));
    break;
  }
}

/**
 * Connect a pcb contained inside a netconn
 * Only used for UDP netconns.
 * Called from ntcpip_netconnDisconnect.
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection to disconnect
 */
void
ntcpip__apimsgDoDisconnect(struct ntcpip__apiMsgMsg *msg)
{
#if NTCPIP__LWIP_UDP
  if (NTCPIP__NETCONNTYPE_GROUP(msg->conn->type) == NTCPIP_NETCONN_UDP) {
    ntcpip_udpDisconnect(msg->conn->pcb.udp);
  }
#endif /* NTCPIP__LWIP_UDP */
  NTCPIP__APIMSG_ACK(msg);
}

/**
 * Set a TCP pcb contained in a netconn into listen mode
 * Called from ntcpip_netconnListen.
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoListen(struct ntcpip__apiMsgMsg *msg)
{
#if NTCPIP__LWIP_TCP
  if (!NTCPIP_ERR_IS_FATAL(msg->conn->err)) {
    if (msg->conn->pcb.tcp != NULL) {
      if (msg->conn->type == NTCPIP_NETCONN_TCP) {
        if (msg->conn->pcb.tcp->state == NTCPIP_CLOSED) {
#if NTCPIP__TCP_LISTEN_BACKLOG
          struct ntcpip__tcpPcb* lpcb = ntcpip_tcpListenWithBacklog(msg->conn->pcb.tcp, msg->msg.lb.backlog);
#else  /* NTCPIP__TCP_LISTEN_BACKLOG */
          struct ntcpip__tcpPcb* lpcb = ntcpip_tcpListen(msg->conn->pcb.tcp);
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
          if (lpcb == NULL) {
            msg->conn->err = NTCPIP_ERR_MEM;
          } else {
            /* delete the recvmbox and allocate the acceptmbox */
            if (msg->conn->recvmbox != NTCPIP__SYS_MBOX_NULL) {
              /** @todo: should we drain the recvmbox here? */
              ntcpip__sysMboxFree(msg->conn->recvmbox);
              msg->conn->recvmbox = NTCPIP__SYS_MBOX_NULL;
            }
            if (msg->conn->acceptmbox == NTCPIP__SYS_MBOX_NULL) {
              if ((msg->conn->acceptmbox = ntcpip__sysMboxNew(NTCPIP__DEFAULT_ACCEPTMBOX_SIZE)) == NTCPIP__SYS_MBOX_NULL) {
                msg->conn->err = NTCPIP_ERR_MEM;
              }
            }
            if (msg->conn->err == NTCPIP_ERR_OK) {
              msg->conn->state = NTCPIP__NETCONN_LISTEN;
              msg->conn->pcb.tcp = lpcb;
              ntcpip_tcpArg(msg->conn->pcb.tcp, msg->conn);
              ntcpip_tcpAccept(msg->conn->pcb.tcp, accept_function);
            }
          }
        } else {
          msg->conn->err = NTCPIP_ERR_CONN;
        }
      }
    }
  }
#endif /* NTCPIP__LWIP_TCP */
  NTCPIP__APIMSG_ACK(msg);
}

/**
 * Send some data on a RAW or UDP pcb contained in a netconn
 * Called from ntcpip_netconnSend
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoSend(struct ntcpip__apiMsgMsg *msg)
{
  if (!NTCPIP_ERR_IS_FATAL(msg->conn->err)) {
    if (msg->conn->pcb.tcp != NULL) {
      switch (NTCPIP__NETCONNTYPE_GROUP(msg->conn->type)) {
#if NTCPIP__LWIP_RAW
      case NTCPIP_NETCONN_RAW:
        if (msg->msg.b->addr == NULL) {
          msg->conn->err = raw_send(msg->conn->pcb.raw, msg->msg.b->p);
        } else {
          msg->conn->err = raw_sendto(msg->conn->pcb.raw, msg->msg.b->p, msg->msg.b->addr);
        }
        break;
#endif
#if NTCPIP__LWIP_UDP
      case NTCPIP_NETCONN_UDP:
        if (msg->msg.b->addr == NULL) {
          msg->conn->err = ntcpip_udpSend(msg->conn->pcb.udp, msg->msg.b->p);
        } else {
          msg->conn->err = ntcpip_udpSendTo(msg->conn->pcb.udp, msg->msg.b->p, msg->msg.b->addr, msg->msg.b->port);
        }
        break;
#endif /* NTCPIP__LWIP_UDP */
      default:
        break;
      }
    }
  }
  NTCPIP__APIMSG_ACK(msg);
}

/**
 * Indicate data has been received from a TCP pcb contained in a netconn
 * Called from ntcpip_netconnRecv
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoRecv(struct ntcpip__apiMsgMsg *msg)
{
#if NTCPIP__LWIP_TCP
  if (!NTCPIP_ERR_IS_FATAL(msg->conn->err)) {
    if (msg->conn->pcb.tcp != NULL) {
      if (msg->conn->type == NTCPIP_NETCONN_TCP) {
#if NTCPIP__TCP_LISTEN_BACKLOG
        if (msg->conn->pcb.tcp->state == NTCPIP_LISTEN) {
          ntcpip_tcpAccepted(msg->conn->pcb.tcp);
        } else
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
        {
          ntcpip_tcpRecved(msg->conn->pcb.tcp, msg->msg.r.len);
        }
      }
    }
  }
#endif /* NTCPIP__LWIP_TCP */
  NTCPIP__APIMSG_ACK(msg);
}

#if NTCPIP__LWIP_TCP
/**
 * See if more data needs to be written from a previous call to ntcpip_netconnWrite.
 * Called initially from ntcpip__apimsgDoWrite. If the first call can't send all data
 * (because of low memory or empty send-buffer), this function is called again
 * from sent_tcp() or poll_tcp() to send more data. If all data is sent, the
 * blocking application thread (waiting in ntcpip_netconnWrite) is released.
 *
 * @param conn netconn (that is currently in state NTCPIP__NETCONN_WRITE) to process
 * @return NTCPIP_ERR_OK
 *         NTCPIP_ERR_MEM if NTCPIP__LWIP_TCPIP_CORE_LOCKING=1 and sending hasn't yet finished
 */
static ntcpip_Err
do_writemore(struct ntcpip__netconn *conn)
{
  ntcpip_Err err;
  const void *dataptr;
  Uint16 len, available;
  Uint8 write_finished = 0;
  size_t diff;

  NTCPIP__LWIP_ASSERT("conn->state == NTCPIP__NETCONN_WRITE", (conn->state == NTCPIP__NETCONN_WRITE));

  dataptr = (const Uint8*)conn->write_msg->msg.w.dataptr + conn->write_offset;
  diff = conn->write_msg->msg.w.len - conn->write_offset;
  if (diff > 0xffffUL) { /* max_u16_t */
    len = 0xffff;
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
    conn->write_delayed = 1;
#endif
  } else {
    len = (Uint16)diff;
  }
  available = ntcpip_tcpSndbuf(conn->pcb.tcp);
  if (available < len) {
    /* don't try to write more than sendbuf */
    len = available;
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
    conn->write_delayed = 1;
#endif
  }

  err = ntcpip_tcpWrite(conn->pcb.tcp, dataptr, len, conn->write_msg->msg.w.apiflags);
  NTCPIP__LWIP_ASSERT("do_writemore: invalid length!", ((conn->write_offset + len) <= conn->write_msg->msg.w.len));
  if (err == NTCPIP_ERR_OK) {
    conn->write_offset += len;
    if (conn->write_offset == conn->write_msg->msg.w.len) {
      /* everything was written */
      write_finished = 1;
      conn->write_msg = NULL;
      conn->write_offset = 0;
      /* NTCPIP__APIEVENT might call ntcpip__tcpTmr, so reset conn->state now */
      conn->state = NTCPIP__NETCONN_NONE;
    }
    err = ntcpip__tcpOutputNagle(conn->pcb.tcp);
    conn->err = err;
    if ((err == NTCPIP_ERR_OK) && (ntcpip_tcpSndbuf(conn->pcb.tcp) <= NTCPIP__TCP_SNDLOWAT)) {
      NTCPIP__APIEVENT(conn, NTCPIP_NETCONN_EVT_SENDMINUS, len);
    }
  } else if (err == NTCPIP_ERR_MEM) {
    /* If NTCPIP_ERR_MEM, we wait for sent_tcp or poll_tcp to be called
       we do NOT return to the application thread, since NTCPIP_ERR_MEM is
       only a temporary error! */

    /* ntcpip__tcpEnqueue returned NTCPIP_ERR_MEM, try ntcpip__tcpOutput anyway */
    err = ntcpip__tcpOutput(conn->pcb.tcp);

#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
    conn->write_delayed = 1;
#endif
  } else {
    /* On errors != NTCPIP_ERR_MEM, we don't try writing any more but return
       the error to the application thread. */
    conn->err = err;
    write_finished = 1;
  }

  if (write_finished) {
    /* everything was written: set back connection state
       and back to application task */
    conn->state = NTCPIP__NETCONN_NONE;
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
    if (conn->write_delayed != 0)
#endif
    {
      ntcpip__sysSemSignal(conn->op_completed);
    }
  }
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
  else
    return NTCPIP_ERR_MEM;
#endif
  return NTCPIP_ERR_OK;
}
#endif /* NTCPIP__LWIP_TCP */

/**
 * Send some data on a TCP pcb contained in a netconn
 * Called from ntcpip_netconnWrite
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoWrite(struct ntcpip__apiMsgMsg *msg)
{
  if (!NTCPIP_ERR_IS_FATAL(msg->conn->err)) {
    if ((msg->conn->pcb.tcp != NULL) && (msg->conn->type == NTCPIP_NETCONN_TCP)) {
#if NTCPIP__LWIP_TCP
      msg->conn->state = NTCPIP__NETCONN_WRITE;
      /* set all the variables used by do_writemore */
      NTCPIP__LWIP_ASSERT("already writing", msg->conn->write_msg == NULL &&
        msg->conn->write_offset == 0);
      msg->conn->write_msg = msg;
      msg->conn->write_offset = 0;
#if NTCPIP__LWIP_TCPIP_CORE_LOCKING
      msg->conn->write_delayed = 0;
      if (do_writemore(msg->conn) != NTCPIP_ERR_OK) {
        NTCPIP__LWIP_ASSERT("state!", msg->conn->state == NTCPIP__NETCONN_WRITE);
        NTCPIP__UNLOCK_CORE();
        ntcpip__sysArchSemWait(msg->conn->op_completed, 0);
        NTCPIP__LOCK_CORE();
        NTCPIP__LWIP_ASSERT("state!", msg->conn->state == NTCPIP__NETCONN_NONE);
      }
#else
      do_writemore(msg->conn);
#endif
      /* for both cases: if do_writemore was called, don't ACK the APIMSG! */
      return;
#endif /* NTCPIP__LWIP_TCP */
#if (NTCPIP__LWIP_UDP || NTCPIP__LWIP_RAW)
    } else {
      msg->conn->err = NTCPIP_ERR_VAL;
#endif /* (NTCPIP__LWIP_UDP || NTCPIP__LWIP_RAW) */
    }
  }
  NTCPIP__APIMSG_ACK(msg);
}

/**
 * Return a connection's local or remote address
 * Called from ntcpip_netconnGetAddr
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoGetAddr(struct ntcpip__apiMsgMsg *msg)
{
  if (msg->conn->pcb.ip != NULL) {
    *(msg->msg.ad.ipaddr) = (msg->msg.ad.local?msg->conn->pcb.ip->local_ip:msg->conn->pcb.ip->remote_ip);
    
    switch (NTCPIP__NETCONNTYPE_GROUP(msg->conn->type)) {
#if NTCPIP__LWIP_RAW
    case NTCPIP_NETCONN_RAW:
      if (msg->msg.ad.local) {
        *(msg->msg.ad.port) = msg->conn->pcb.raw->protocol;
      } else {
        /* return an error as connecting is only a helper for upper layers */
        msg->conn->err = NTCPIP_ERR_CONN;
      }
      break;
#endif /* NTCPIP__LWIP_RAW */
#if NTCPIP__LWIP_UDP
    case NTCPIP_NETCONN_UDP:
      if (msg->msg.ad.local) {
        *(msg->msg.ad.port) = msg->conn->pcb.udp->local_port;
      } else {
        if ((msg->conn->pcb.udp->flags & UDP_FLAGS_CONNECTED) == 0) {
          msg->conn->err = NTCPIP_ERR_CONN;
        } else {
          *(msg->msg.ad.port) = msg->conn->pcb.udp->remote_port;
        }
      }
      break;
#endif /* NTCPIP__LWIP_UDP */
#if NTCPIP__LWIP_TCP
    case NTCPIP_NETCONN_TCP:
      *(msg->msg.ad.port) = (msg->msg.ad.local?msg->conn->pcb.tcp->local_port:msg->conn->pcb.tcp->remote_port);
      break;
#endif /* NTCPIP__LWIP_TCP */
    }
  } else {
    msg->conn->err = NTCPIP_ERR_CONN;
  }
  NTCPIP__APIMSG_ACK(msg);
}

/**
 * Close a TCP pcb contained in a netconn
 * Called from ntcpip_netconnClose
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoClose(struct ntcpip__apiMsgMsg *msg)
{
#if NTCPIP__LWIP_TCP
  if ((msg->conn->pcb.tcp != NULL) && (msg->conn->type == NTCPIP_NETCONN_TCP)) {
      msg->conn->state = NTCPIP__NETCONN_CLOSE;
      do_close_internal(msg->conn);
      /* for tcp netconns, do_close_internal ACKs the message */
  } else
#endif /* NTCPIP__LWIP_TCP */
  {
    msg->conn->err = NTCPIP_ERR_VAL;
    ntcpip__sysSemSignal(msg->conn->op_completed);
  }
}

#if NTCPIP__LWIP_IGMP
/**
 * Join multicast groups for UDP netconns.
 * Called from ntcpip_netconnjoinLeaveGroup
 *
 * @param msg the ntcpip__apiMsgMsg pointing to the connection
 */
void
ntcpip__apimsgDoJoinLeaveGroup(struct ntcpip__apiMsgMsg *msg)
{ 
  if (!NTCPIP_ERR_IS_FATAL(msg->conn->err)) {
    if (msg->conn->pcb.tcp != NULL) {
      if (NTCPIP__NETCONNTYPE_GROUP(msg->conn->type) == NTCPIP_NETCONN_UDP) {
#if NTCPIP__LWIP_UDP
        if (msg->msg.jl.join_or_leave == NTCPIP_NETCONN_JOIN) {
          msg->conn->err = ntcpip_igmpJoingroup(msg->msg.jl.interface, msg->msg.jl.multiaddr);
        } else {
          msg->conn->err = ntcpip__igmpLeavegroup(msg->msg.jl.interface, msg->msg.jl.multiaddr);
        }
#endif /* NTCPIP__LWIP_UDP */
#if (NTCPIP__LWIP_TCP || NTCPIP__LWIP_RAW)
      } else {
        msg->conn->err = NTCPIP_ERR_VAL;
#endif /* (NTCPIP__LWIP_TCP || NTCPIP__LWIP_RAW) */
      }
    }
  }
  NTCPIP__APIMSG_ACK(msg);
}
#endif /* NTCPIP__LWIP_IGMP */

#if NTCPIP__LWIP_DNS
/**
 * Callback function that is called when DNS name is resolved
 * (or on timeout). A waiting application thread is waked up by
 * signaling the semaphore.
 */
static void
do_dns_found(const char *name, struct ntcpip_ipAddr *ipaddr, void *arg)
{
  struct ntcpip__dnsApiMsg *msg = (struct ntcpip__dnsApiMsg*)arg;

  NTCPIP__LWIP_ASSERT("DNS response for wrong host name", strcmp(msg->name, name) == 0);

  if (ipaddr == NULL) {
    /* timeout or memory error */
    *msg->err = NTCPIP_ERR_VAL;
  } else {
    /* address was resolved */
    *msg->err = NTCPIP_ERR_OK;
    *msg->addr = *ipaddr;
  }
  /* wake up the application task waiting in ntcpip_netconnGetHostByName */
  ntcpip__sysSemSignal(msg->sem);
}

/**
 * Execute a DNS query
 * Called from ntcpip_netconnGetHostByName
 *
 * @param arg the ntcpip__dnsApiMsg pointing to the query
 */
void
do_gethostbyname(void *arg)
{
  struct ntcpip__dnsApiMsg *msg = (struct ntcpip__dnsApiMsg*)arg;

  *msg->err = dns_gethostbyname(msg->name, msg->addr, do_dns_found, msg);
  if (*msg->err != NTCPIP_ERR_INPROGRESS) {
    /* on error or immediate success, wake up the application
     * task waiting in ntcpip_netconnGetHostByName */
    ntcpip__sysSemSignal(msg->sem);
  }
}
#endif /* NTCPIP__LWIP_DNS */

#endif /* NTCPIP__LWIP_NETCONN */

