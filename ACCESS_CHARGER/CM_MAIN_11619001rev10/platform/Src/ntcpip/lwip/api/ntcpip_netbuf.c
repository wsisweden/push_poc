/**
 * @file
 * Network buffer management
 *
 */
 
/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_NETCONN /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/netbuf.h"
#include "../memp.h"

#include <string.h>

/**
 * Create (allocate) and initialize a new netbuf.
 * The netbuf doesn't yet contain a packet buffer!
 *
 * @return a pointer to a new netbuf
 *         NULL on lack of memory
 */
struct
ntcpip__netbuf *ntcpip_netbufNew(void)
{
  struct ntcpip__netbuf *buf;

  buf = ntcpip__mempMalloc(MEMP_NETBUF);
  if (buf != NULL) {
    buf->p = NULL;
    buf->ptr = NULL;
    buf->addr = NULL;
    buf->port = 0;
#if NTCPIP__NETBUF_RECVINFO
    buf->toaddr = NULL;
    buf->toport = 0;
#endif /* NTCPIP__NETBUF_RECVINFO */
    return buf;
  } else {
    return NULL;
  }
}

/**
 * Deallocate a netbuf allocated by ntcpip_netbufNew().
 *
 * @param buf pointer to a netbuf allocated by ntcpip_netbufNew()
 */
void
ntcpip_netbufDelete(struct ntcpip__netbuf *buf)
{
  if (buf != NULL) {
    if (buf->p != NULL) {
      ntcpip_pbufFree(buf->p);
      buf->p = buf->ptr = NULL;
    }
    ntcpip__mempFree(MEMP_NETBUF, buf);
  }
}

/**
 * Allocate memory for a packet buffer for a given netbuf.
 *
 * @param buf the netbuf for which to allocate a packet buffer
 * @param size the size of the packet buffer to allocate
 * @return pointer to the allocated memory
 *         NULL if no memory could be allocated
 */
void *
ntcpip_netbufAlloc(struct ntcpip__netbuf *buf, Uint16 size)
{
  NTCPIP__LWIP_ERROR("ntcpip_netbufAlloc: invalid buf", (buf != NULL), return NULL;);

  /* Deallocate any previously allocated memory. */
  if (buf->p != NULL) {
    ntcpip_pbufFree(buf->p);
  }
  buf->p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, size, NTCPIP_PBUF_RAM);
  if (buf->p == NULL) {
     return NULL;
  }
  NTCPIP__LWIP_ASSERT("check that first pbuf can hold size",
             (buf->p->len >= size));
  buf->ptr = buf->p;
  return buf->p->payload;
}

/**
 * Free the packet buffer included in a netbuf
 *
 * @param buf pointer to the netbuf which contains the packet buffer to free
 */
void
ntcpip_netbufFree(struct ntcpip__netbuf *buf)
{
  NTCPIP__LWIP_ERROR("ntcpip_netbufFree: invalid buf", (buf != NULL), return;);
  if (buf->p != NULL) {
    ntcpip_pbufFree(buf->p);
  }
  buf->p = buf->ptr = NULL;
}

/**
 * Let a netbuf reference existing (non-volatile) data.
 *
 * @param buf netbuf which should reference the data
 * @param dataptr pointer to the data to reference
 * @param size size of the data
 * @return NTCPIP_ERR_OK if data is referenced
 *         NTCPIP_ERR_MEM if data couldn't be referenced due to lack of memory
 */
ntcpip_Err
ntcpip__netbufRef(struct ntcpip__netbuf *buf, const void *dataptr, Uint16 size)
{
  NTCPIP__LWIP_ERROR("ntcpip__netbufRef: invalid buf", (buf != NULL), return NTCPIP_ERR_ARG;);
  if (buf->p != NULL) {
    ntcpip_pbufFree(buf->p);
  }
  buf->p = ntcpip_pbufAlloc(NTCPIP_PBUF_TRANSPORT, 0, NTCPIP_PBUF_REF);
  if (buf->p == NULL) {
    buf->ptr = NULL;
    return NTCPIP_ERR_MEM;
  }
  buf->p->payload = (void*)dataptr;
  buf->p->len = buf->p->tot_len = size;
  buf->ptr = buf->p;
  return NTCPIP_ERR_OK;
}

/**
 * Chain one netbuf to another (@see ntcpip__pbufChain)
 *
 * @param head the first netbuf
 * @param tail netbuf to chain after head, freed by this function, may not be reference after returning
 */
void
ntcpip__netbufChain(struct ntcpip__netbuf *head, struct ntcpip__netbuf *tail)
{
  NTCPIP__LWIP_ERROR("ntcpip__netbufRef: invalid head", (head != NULL), return;);
  NTCPIP__LWIP_ERROR("ntcpip__netbufChain: invalid tail", (tail != NULL), return;);
  ntcpip_pbufCat(head->p, tail->p);
  head->ptr = head->p;
  ntcpip__mempFree(MEMP_NETBUF, tail);
}

/**
 * Get the data pointer and length of the data inside a netbuf.
 *
 * @param buf netbuf to get the data from
 * @param dataptr pointer to a void pointer where to store the data pointer
 * @param len pointer to an Uint16 where the length of the data is stored
 * @return NTCPIP_ERR_OK if the information was retreived,
 *         NTCPIP_ERR_BUF on error.
 */
ntcpip_Err
ntcpip_netbufData(struct ntcpip__netbuf *buf, void **dataptr, Uint16 *len)
{
  NTCPIP__LWIP_ERROR("ntcpip_netbufData: invalid buf", (buf != NULL), return NTCPIP_ERR_ARG;);
  NTCPIP__LWIP_ERROR("ntcpip_netbufData: invalid dataptr", (dataptr != NULL), return NTCPIP_ERR_ARG;);
  NTCPIP__LWIP_ERROR("ntcpip_netbufData: invalid len", (len != NULL), return NTCPIP_ERR_ARG;);

  if (buf->ptr == NULL) {
    return NTCPIP_ERR_BUF;
  }
  *dataptr = buf->ptr->payload;
  *len = buf->ptr->len;
  return NTCPIP_ERR_OK;
}

/**
 * Move the current data pointer of a packet buffer contained in a netbuf
 * to the next part.
 * The packet buffer itself is not modified.
 *
 * @param buf the netbuf to modify
 * @return -1 if there is no next part
 *         1  if moved to the next part but now there is no next part
 *         0  if moved to the next part and there are still more parts
 */
Int8
ntcpip_netbufNext(struct ntcpip__netbuf *buf)
{
  NTCPIP__LWIP_ERROR("ntcpip_netbufFree: invalid buf", (buf != NULL), return -1;);
  if (buf->ptr->next == NULL) {
    return -1;
  }
  buf->ptr = buf->ptr->next;
  if (buf->ptr->next == NULL) {
    return 1;
  }
  return 0;
}

/**
 * Move the current data pointer of a packet buffer contained in a netbuf
 * to the beginning of the packet.
 * The packet buffer itself is not modified.
 *
 * @param buf the netbuf to modify
 */
void
ntcpip__netbufFirst(struct ntcpip__netbuf *buf)
{
  NTCPIP__LWIP_ERROR("ntcpip_netbufFree: invalid buf", (buf != NULL), return;);
  buf->ptr = buf->p;
}

#endif /* NTCPIP__LWIP_NETCONN */

