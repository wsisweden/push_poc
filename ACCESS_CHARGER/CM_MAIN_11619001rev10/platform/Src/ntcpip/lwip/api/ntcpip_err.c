/**
 * @file
 * Error Management module
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "ntcpip/err.h"

#ifdef LWIP_DEBUG

static const char *err_strerr[] = {
           "Ok.",                    /* NTCPIP_ERR_OK          0  */
           "Out of memory error.",   /* NTCPIP_ERR_MEM        -1  */
           "Buffer error.",          /* NTCPIP_ERR_BUF        -2  */
           "Timeout.",               /* NTCPIP_ERR_TIMEOUT    -3 */
           "Routing problem.",       /* NTCPIP_ERR_RTE        -4  */
           "Connection aborted.",    /* NTCPIP_ERR_ABRT       -5  */
           "Connection reset.",      /* NTCPIP_ERR_RST        -6  */
           "Connection closed.",     /* NTCPIP_ERR_CLSD       -7  */
           "Not connected.",         /* NTCPIP_ERR_CONN       -8  */
           "Illegal value.",         /* NTCPIP_ERR_VAL        -9  */
           "Illegal argument.",      /* NTCPIP_ERR_ARG        -10 */
           "Address in use.",        /* NTCPIP_ERR_USE        -11 */
           "Low-level netif error.", /* NTCPIP_ERR_IF         -12 */
           "Already connected.",     /* NTCPIP_ERR_ISCONN     -13 */
           "Operation in progress."  /* NTCPIP_ERR_INPROGRESS -14 */
};

/**
 * Convert an lwip internal error to a string representation.
 *
 * @param err an lwip internal ntcpip_Err
 * @return a string representation for err
 */
const char *
ntcpip__lwipStrErr(ntcpip_Err err)
{
  return err_strerr[-err];

}

#endif /* LWIP_DEBUG */

