/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_API_MSG_H__
#define __LWIP_API_MSG_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_NETCONN /* don't build if not configured for use in lwipopts.h */

#include <stddef.h> /* for size_t */

#include "ntcpip/ip_addr.h"
#include "ntcpip/err.h"
#include "ntcpip/sys.h"
#include "ntcpip/igmp.h"
#include "ntcpip/api.h"

#ifdef __cplusplus
extern "C" {
#endif

/* IP addresses and port numbers are expected to be in
 * the same byte order as in the corresponding pcb.
 */
/** This struct includes everything that is necessary to execute a function
    for a netconn in another thread context (mainly used to process netconns
    in the tcpip_thread context to be thread safe). */
struct ntcpip__apiMsgMsg {
  /** The netconn which to process - always needed: it includes the semaphore
      which is used to block the application thread until the function finished. */
  struct ntcpip__netconn *conn;
  /** Depending on the executed function, one of these union members is used */
  union {
    /** used for ntcpip__apimsgDoSend */
    struct ntcpip__netbuf *b;
    /** used for ntcpip__apimsgDoNewConn */
    struct {
      Uint8 proto;
    } n;
    /** used for ntcpip__apimsgDoBind and ntcpip__apimsgDoConnect */
    struct {
      const struct ntcpip_ipAddr *ipaddr;
      Uint16 port;
    } bc;
    /** used for ntcpip__apimsgDoGetAddr */
    struct {
      struct ntcpip_ipAddr *ipaddr;
      Uint16 *port;
      Uint8 local;
    } ad;
    /** used for ntcpip__apimsgDoWrite */
    struct {
      const void *dataptr;
      size_t len;
      Uint8 apiflags;
    } w;
    /** used for ntcpip__apimsgDoRecv */
    struct {
      Uint16 len;
    } r;
#if NTCPIP__LWIP_IGMP
    /** used for ntcpip__apimsgDoJoinLeaveGroup */
    struct {
      struct ntcpip_ipAddr *multiaddr;
      struct ntcpip_ipAddr *interface;
      enum ntcpip__netconnIgmp join_or_leave;
    } jl;
#endif /* NTCPIP__LWIP_IGMP */
#if NTCPIP__TCP_LISTEN_BACKLOG
    struct {
      Uint8 backlog;
    } lb;
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
  } msg;
};

/** This struct contains a function to execute in another thread context and
    a struct ntcpip__apiMsgMsg that serves as an argument for this function.
    This is passed to ntcpip__tcpipApiMsg to execute functions in tcpip_thread context. */
struct ntcpip__apiMsg {
  /** function to execute in tcpip_thread context */
  void (* function)(struct ntcpip__apiMsgMsg *msg);
  /** arguments for this function */
  struct ntcpip__apiMsgMsg msg;
};

#if NTCPIP__LWIP_DNS
/** As do_gethostbyname requires more arguments but doesn't require a netconn,
    it has its own struct (to avoid struct ntcpip__apiMsg getting bigger than necessary).
    do_gethostbyname must be called using tcpip_callback instead of ntcpip__tcpipApiMsg
    (see ntcpip_netconnGetHostByName). */
struct ntcpip__dnsApiMsg {
  /** Hostname to query or dotted IP address string */
  const char *name;
  /** Rhe resolved address is stored here */
  struct ntcpip_ipAddr *addr;
  /** This semaphore is posted when the name is resolved, the application thread
      should wait on it. */
  ntcpip__SysSema sem;
  /** Errors are given back here */
  ntcpip_Err *err;
};
#endif /* NTCPIP__LWIP_DNS */

void ntcpip__apimsgDoNewConn         ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoDelconn         ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoBind            ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoConnect         ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoDisconnect      ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoListen          ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoSend            ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoRecv            ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoWrite           ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoGetAddr         ( struct ntcpip__apiMsgMsg *msg);
void ntcpip__apimsgDoClose           ( struct ntcpip__apiMsgMsg *msg);
#if NTCPIP__LWIP_IGMP
void ntcpip__apimsgDoJoinLeaveGroup( struct ntcpip__apiMsgMsg *msg);
#endif /* NTCPIP__LWIP_IGMP */

#if NTCPIP__LWIP_DNS
void do_gethostbyname(void *arg);
#endif /* NTCPIP__LWIP_DNS */

struct ntcpip__netconn* ntcpip__netconnAlloc(enum ntcpip_netconnType t, ntcpip__NetconnCallback callback);
void ntcpip__netconnFree(struct ntcpip__netconn *conn);

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_NETCONN */

#endif /* __LWIP_API_MSG_H__ */

