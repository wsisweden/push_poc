/*
 * SETUP: Make sure we define everything we will need.
 *
 * We have create three types of pools:
 *   1) MEMPOOL - standard pools
 *   2) MALLOC_MEMPOOL - to be used by ntcpip__memMalloc in mem.c
 *   3) PBUF_MEMPOOL - a mempool of pbuf's, so include space for the pbuf struct
 *
 * If the include'r doesn't require any special treatment of each of the types
 * above, then will declare #2 & #3 to be just standard mempools.
 */
#ifndef LWIP_MALLOC_MEMPOOL
/* This treats "malloc pools" just like any other pool.
   The pools are a little bigger to provide 'size' as the amount of user data. */
#define LWIP_MALLOC_MEMPOOL(num, size) LWIP_MEMPOOL(POOL_##size, num, (size + sizeof(struct memp_malloc_helper)), "MALLOC_"#size)
#define LWIP_MALLOC_MEMPOOL_START
#define LWIP_MALLOC_MEMPOOL_END
#endif /* LWIP_MALLOC_MEMPOOL */ 

#ifndef LWIP_PBUF_MEMPOOL
/* This treats "pbuf pools" just like any other pool.
 * Allocates buffers for a pbuf struct AND a payload size */
#define LWIP_PBUF_MEMPOOL(name, num, payload, desc) LWIP_MEMPOOL(name, num, (MEMP_ALIGN_SIZE(sizeof(struct ntcpip_pbuf)) + MEMP_ALIGN_SIZE(payload)), desc)
#endif /* LWIP_PBUF_MEMPOOL */


/*
 * A list of internal pools used by LWIP.
 *
 * LWIP_MEMPOOL(pool_name, number_elements, element_size, pool_description)
 *     creates a pool name MEMP_pool_name. description is used in stats.c
 */
#if NTCPIP__LWIP_RAW
LWIP_MEMPOOL(RAW_PCB,        NTCPIP__MEMP_NUM_RAW_PCB,         sizeof(struct ntcpip__rawPcb),        "RAW_PCB")
#endif /* NTCPIP__LWIP_RAW */

#if NTCPIP__LWIP_UDP
LWIP_MEMPOOL(UDP_PCB,        NTCPIP__MEMP_NUM_UDP_PCB,         sizeof(struct ntcpip__udpPcb),        "UDP_PCB")
#endif /* NTCPIP__LWIP_UDP */

#if NTCPIP__LWIP_TCP
LWIP_MEMPOOL(TCP_PCB,        NTCPIP__MEMP_NUM_TCP_PCB,         sizeof(struct ntcpip__tcpPcb),        "TCP_PCB")
LWIP_MEMPOOL(TCP_PCB_LISTEN, NTCPIP__MEMP_NUM_TCP_PCB_LISTEN,  sizeof(struct ntcpip__tcpPcbListen), "TCP_PCB_LISTEN")
LWIP_MEMPOOL(TCP_SEG,        NTCPIP__MEMP_NUM_TCP_SEG,         sizeof(struct ntcpip__tcpSeg),        "TCP_SEG")
#endif /* NTCPIP__LWIP_TCP */

#if NTCPIP__IP_REASSEMBLY
LWIP_MEMPOOL(REASSDATA,      NTCPIP__MEMP_NUM_REASSDATA,       sizeof(struct ip_reassdata),   "REASSDATA")
#endif /* NTCPIP__IP_REASSEMBLY */

#if NTCPIP__LWIP_NETCONN
LWIP_MEMPOOL(NETBUF,         NTCPIP__MEMP_NUM_NETBUF,          sizeof(struct ntcpip__netbuf),         "NETBUF")
LWIP_MEMPOOL(NETCONN,        NTCPIP__MEMP_NUM_NETCONN,         sizeof(struct ntcpip__netconn),        "NETCONN")
#endif /* NTCPIP__LWIP_NETCONN */

#if NTCPIP__NO_SYS==0
LWIP_MEMPOOL(TCPIP_MSG_API,  NTCPIP__MEMP_NUM_TCPIP_MSG_API,   sizeof(struct ntcpip__msg),      "NTCPIP__MSG_API")
LWIP_MEMPOOL(TCPIP_MSG_INPKT,NTCPIP__MEMP_NUM_TCPIP_MSG_INPKT, sizeof(struct ntcpip__msg),      "NTCPIP__MSG_INPKT")
#endif /* NTCPIP__NO_SYS==0 */

#if NTCPIP__ARP_QUEUEING
LWIP_MEMPOOL(ARP_QUEUE,      NTCPIP__MEMP_NUM_ARP_QUEUE,       sizeof(struct etharp_q_entry), "ARP_QUEUE")
#endif /* NTCPIP__ARP_QUEUEING */

#if NTCPIP__LWIP_IGMP
LWIP_MEMPOOL(IGMP_GROUP,     NTCPIP__MEMP_NUM_IGMP_GROUP,      sizeof(struct ntcpip__igmpGroup),     "IGMP_GROUP")
#endif /* NTCPIP__LWIP_IGMP */

#if NTCPIP__NO_SYS==0
LWIP_MEMPOOL(SYS_TIMEOUT,    NTCPIP__MEMP_NUM_SYS_TIMEOUT,     sizeof(struct ntcpip__sysTimeo),      "SYS_TIMEOUT")
#endif /* NTCPIP__NO_SYS==0 */


/*
 * A list of pools of pbuf's used by LWIP.
 *
 * LWIP_PBUF_MEMPOOL(pool_name, number_elements, pbuf_payload_size, pool_description)
 *     creates a pool name MEMP_pool_name. description is used in stats.c
 *     This allocates enough space for the pbuf struct and a payload.
 *     (Example: pbuf_payload_size=0 allocates only size for the struct)
 */
LWIP_PBUF_MEMPOOL(PBUF,      NTCPIP__MEMP_NUM_PBUF,            0,                             "NTCPIP_PBUF_REF/ROM")
LWIP_PBUF_MEMPOOL(NTCPIP_PBUF_POOL, NTCPIP__PBUF_POOL_SIZE,           NTCPIP__PBUF_POOL_BUFSIZE,             "NTCPIP_PBUF_POOL")


/*
 * Allow for user-defined pools; this must be explicitly set in lwipopts.h
 * since the default is to NOT look for lwippools.h
 */
#if NTCPIP__MEMP_USE_CUSTOM_POOLS
#include "lwippools.h"
#endif /* NTCPIP__MEMP_USE_CUSTOM_POOLS */

/*
 * REQUIRED CLEANUP: Clear up so we don't get "multiply defined" error later
 * (#undef is ignored for something that is not defined)
 */
#undef LWIP_MEMPOOL
#undef LWIP_MALLOC_MEMPOOL
#undef LWIP_MALLOC_MEMPOOL_START
#undef LWIP_MALLOC_MEMPOOL_END
#undef LWIP_PBUF_MEMPOOL

