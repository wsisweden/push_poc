/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		ntcpip_sysMbox.c
*
*	\ingroup	NTCPIP
*
*	\brief		Mailbox implementation.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 600 $ \n
*				\$Date: 2016-06-10 15:24:11 +0300 (pe, 10 kes� 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "mem.h"
#include "deb.h"
#include "ntcpip/sys.h"
#include "TSTAMP.H"
#include "lib.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#ifndef NTCPIP__SYS_MBOX_SIZE
#define NTCPIP__SYS_MBOX_SIZE	10		/**< Number of message slots in mbox.*/
#endif

#ifndef NTCPIP__SYS_MBOX_NUM
# define NTCPIP__SYS_MBOX_NUM	10		/**< Number of mailboxes to allocate */
#endif

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*
 *	TODO:	Make a pointer ring buffer implementation in lib.
 */

#if TARGET_SELECTED & TARGET_CM3 \
	|| TARGET_SELECTED == TARGET_CM4GNU \
	|| TARGET_SELECTED == TARGET_W32 \
	|| TARGET_SELECTED & TARGET_SM4 \
	|| TARGET_SELECTED & TARGET_S4F4 \
	|| TARGET_SELECTED & TARGET_S3F2
# define ntcpip__sysRbufInit(s_)		lib_rBufS4Init(s_)
# define ntcpip__sysRbufPut(p_, d_)		lib_rBufS4Put(p_, d_)
# define ntcpip__sysRbufGet(p_, d_)		lib_rBufS4Get(p_, d_)
# define ntcpip__sysRbufReset(p_)		lib_rBufS4Reset(p_)
# define ntcpip__sysRbufInfo(p_,d_)		lib_rBufS4Info(p_,d_)
#else
    #error Target does not support NTCPIP
#endif

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Mailbox type used by LwIP.
 */

struct ntcpip__sysMbox {				/*''''''''''''''''''''''''''''''''''*/
	ntcpip__SysMbox			pNext;		/**< Pointer to next mailbox.       */
	void *					pRingBuf;	/**< Pointer to ring buffer.		*/
	osa_SemaType 			newMail;	/**< Set when mail arrives.         */
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Free mailbox list type. A list of mailboxes is statically allocated with this
 * type. When LwIP wants to allocate a new mailbox then a box from this list
 * is provided.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	Uint8 					freeCount;	/**< Number of free mailboxes.      */
	Uint8 					lowCount;	/**< Lowest free mailbox count.		*/
	Uint8					mboxLowest;	/**< Lowest mbox space.				*/
	ntcpip__SysMbox 		pHead;		/**< Pointer to list head.          */
	struct ntcpip__sysMbox	boxes[NTCPIP__SYS_MBOX_NUM];	/**<            */
} ntcpip__SysMboxInst;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE ntcpip__SysMboxInst	ntcpip__sysMboxInst;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysMboxInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the mailbox pool.
*
*	\param		mboxCount	Number of mailboxes to allocate.
*
*	\details	Initializes the allocated lists.
*
*	\note		Will be called with interrupts disabled before OSA is running.
*
*******************************************************************************/

PUBLIC void ntcpip__sysMboxInit(
	void
) {
	Ufast8			ii;
	ntcpip__SysMbox mailBoxes;

	/*
	 *	Initialize the free mailbox list. Allocates a ring buffer and
	 *	initializes the semaphore for each mailbox.
	 *
	 *	All mailboxes are chained and put on the free mailbox list.	
	 */

	mailBoxes = ntcpip__sysMboxInst.boxes;

	for (ii = 0; ii < NTCPIP__SYS_MBOX_NUM; ii++) {
		mailBoxes[ii].pNext = &mailBoxes[ii+1];
		mailBoxes[ii].pRingBuf = ntcpip__sysRbufInit(NTCPIP__SYS_MBOX_SIZE);
		deb_assert(mailBoxes[ii].pRingBuf != NULL);

		osa_newSemaSet(&mailBoxes[ii].newMail);
	}

	/*
	 *	The last mailbox on the list will point to NULL.
	 */

	ntcpip__sysMboxInst.boxes[NTCPIP__SYS_MBOX_NUM-1].pNext = NULL;
	ntcpip__sysMboxInst.pHead = &ntcpip__sysMboxInst.boxes[0];

	/*
	 *	Initialize diagnostic variables.
	 */

	ntcpip__sysMboxInst.freeCount = NTCPIP__SYS_MBOX_NUM;
	ntcpip__sysMboxInst.lowCount = NTCPIP__SYS_MBOX_NUM;
	ntcpip__sysMboxInst.mboxLowest = NTCPIP__SYS_MBOX_SIZE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysMboxNew
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Return a new mailbox, or NTCPIP_SYS_MBOX_NULL on error.
*
*	\param		size 	The size of the mailbox (not used in this
*						implementation).
*
*	\details	Allocates a mailbox from the free mailbox list. Elements
*				stored in mailboxes are pointers.
*
*	\note		May be called with interrupts disabled.
*
*******************************************************************************/

PUBLIC ntcpip__SysMbox ntcpip__sysMboxNew(
	int 					size
) {
	ntcpip__SysMbox pMbox;
	tools_IsrState	isrState;

	DUMMY_VAR(size);

	/*
	 * Picking one mailbox from the free mailbox list.
	 */

	DISABLEINTR(&isrState);

	pMbox = ntcpip__sysMboxInst.pHead;
	if (pMbox != NULL) {
		ntcpip__sysMboxInst.pHead = pMbox->pNext;

		/*
		 *	Update diagnostic variables.
		 */

		ntcpip__sysMboxInst.freeCount--;
		if (ntcpip__sysMboxInst.freeCount < ntcpip__sysMboxInst.lowCount) {
			ntcpip__sysMboxInst.lowCount = ntcpip__sysMboxInst.freeCount;
		}
	}

	ENABLEINTR(&isrState);

	if (pMbox != NTCPIP__SYS_MBOX_NULL) {
		pMbox->pNext = NULL;

		/*
		 *	Make sure the mailbox read semaphore is taken. The next fetch call
		 *	for this mailbox should block unless a message has been posted to
		 *	the mailbox.
		 */

		osa_semaSet(&pMbox->newMail);
		osa_semaGet(&pMbox->newMail);
	}

	return(pMbox);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysMboxFree
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Deallocates a mailbox.
*
*	\param		pMbox 	Pointer to the mailbox.
*
*	\details	Deallocates a mailbox. If there are messages still present in
*				the mailbox when the mailbox is deallocated, it is an indication
*				of a programming error in lwIP and the developer should be
*				notified.
*
*	\note
*
*******************************************************************************/

PUBLIC void ntcpip__sysMboxFree(
	ntcpip__SysMbox 			pMbox
) {
	tools_IsrState isrState;

	/*
	 *	Throw away any mail in the mailbox by resetting the ring buffer.
	 */

	ntcpip__sysRbufReset(pMbox->pRingBuf);

	/*
	 * The semaphores of mailboxes in the list should be set (released).
	 */

	osa_semaSet(&pMbox->newMail);

	/*
	 * Inserting the mailbox to the free mailbox list.
	 */

	DISABLEINTR(&isrState);
	pMbox->pNext = ntcpip__sysMboxInst.pHead;
	ntcpip__sysMboxInst.pHead = pMbox;
	ntcpip__sysMboxInst.freeCount++;
	ENABLEINTR(&isrState);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysMboxPost
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Posts a message to the mailbox.
*
*	\param		pMbox 	Pointer to the mail box.
*	\param		pMsg 	Pointer to the message.
*
*	\details	The mailbox is shared between threads so the access has to be
*				atomic.
*
*	\note		This function has to block until the "msg" is really posted.
*
*******************************************************************************/

PUBLIC void ntcpip__sysMboxPost(
	ntcpip__SysMbox 		pMbox,
	void *					pMsg
) {
	ntcpip_Err postRet;

	/*
	 *	Try to post the message.
	 */

	postRet = ntcpip__sysMboxTrypost(pMbox, pMsg);

	while (postRet == NTCPIP_ERR_MEM) {
		/*
		 *	The mailbox is full. Give time to other tasks and then try
		 *	again. This should not be a normal situation. Normally the receiving
		 *	task will handle the messages before the mailbox gets full.
		 */

		osa_yield();

		postRet = ntcpip__sysMboxTrypost(pMbox, pMsg);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysMboxTrypost
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Try to post the message to the mailbox.
*
*	\param		pMbox	Pointer to the mailbox.
*	\param		pMsg	Pointer to the message.
*
*	\retval		NTCPIP_ERR_MEM	The mailbox is full.
*	\retval		NTCPIP_ERR_OK	The message was posted.
*
*	\details	The mailbox is shared between threads so the access has to be
*				atomic.
*
*	\note		This functions will never block.
*
*******************************************************************************/

PUBLIC ntcpip_Err ntcpip__sysMboxTrypost(
	ntcpip__SysMbox 		pMbox,
	void *					pMsg
) {
	lib_RBufStat	ringBufStatus;
	ntcpip_Err		ret;

	ret = NTCPIP_ERR_MEM;

	osa_taskingDisable();

	/*
	 *	Try to put the message on the ring buffer.
	 */

	ringBufStatus = ntcpip__sysRbufPut(pMbox->pRingBuf, &pMsg);

	if (ringBufStatus != LIB_RBUF_OFLOW) {
		lib_RBufSDim mboxFree;

		/*
		 *	The message was successfully posted. Signal that mail is ready to be
		 *	read.
		 */

		osa_semaSet(&pMbox->newMail);

		/*
		 *	Check mailbox state so that we can track how full the mailboxes have
		 *	been.
		 */

		ntcpip__sysRbufInfo(pMbox->pRingBuf, &mboxFree);
		if (mboxFree < ntcpip__sysMboxInst.mboxLowest) {
			ntcpip__sysMboxInst.mboxLowest = mboxFree;
		}

		ret = NTCPIP_ERR_OK;
	}

	osa_taskingEnable();

	return(ret);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysArchMboxFetch
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Fetches a mail from the mailbox.
*
*	\param		pMbox 	Pointer to the mailbox.
*	\param		ppMsg 	Pointer to message pointer. The message pointer will be
*						updated by this function.
*	\param		timeout Time in milliseconds to wait for a new message. Zero if
*						the function should wait forever.
*
*	\return		The time that the function has blocked in milliseconds or
*				NTCPIP__SYS_ARCH_TIMEOUT if timeout occurred.
*
*	\details	Blocks the thread until a message arrives in the mailbox, but
*				does not block the thread longer than timeout milliseconds
*				(similar to the ntcpip__sysArchSemWait() function). The msg
*				argument is a pointer to the message in the mailbox and may be
*				NULL to indicate that the message should be dropped.
*
*	\note
*
*******************************************************************************/

PUBLIC Uint32 ntcpip__sysArchMboxFetch(
	ntcpip__SysMbox 		pMbox,
	void **					ppMsg,
	Uint32 					timeout
) {
	Uint32 funcTime;
	void * pDummy;
	Uint32 fetchRet;
	Uint16 elapsed;

	funcTime = 0;

	if(ppMsg == NULL) {
		ppMsg = &pDummy;
	}

	/*
	 *	First try to fetch a message from the mailbox.
	 */

	fetchRet = ntcpip__sysArchMboxTryFetch(pMbox, ppMsg);
	
	elapsed = 0;
	while (fetchRet == NTCPIP__SYS_MBOX_EMPTY) {

		/*
		 *	There was no message in the mailbox. Reduce the elapsed time
		 *	from the timeout variable.
		 */

		if (timeout != 0) {
			if (timeout > elapsed) {
				timeout -= elapsed;

			} else {
				/*
				 *	We have now waited the requested amount of milliseconds
				 *	and no message has been fetched. Return with timeout
				 *	code.
				 */

				*ppMsg = NULL;
				funcTime = NTCPIP__SYS_ARCH_TIMEOUT;
				break;
			}
		}

		/*
		 *	Wait for a message.
		 */

		{
			Uint32			waitTime;
			tstamp_Stamp	startStamp;
			osa_Status		error;
			
			/*
			 *	We can not wait longer than 65535ms because TSTAMP does not
			 *	support it. If the requested timeout is longer than 65535ms,
			 *	then we have to split up the wait time into multiple smaller
			 *	chunks.
			 */

			waitTime = timeout;
			if (waitTime == 0 || waitTime > 60000UL) {
				/*
				 *	Maximum time to wait is set to 60s.
				 */

				waitTime = 60000UL;
			}
			
			tstamp_getStamp(&startStamp);
			osa_semaWait(
				&pMbox->newMail,
				(Uint16) osa_msToTicks(waitTime),
				&error
			);
			tstamp_getDiffCurr(&startStamp, &elapsed);

			error = error;
		}

		/*
		 *	The semaphore was signaled or a timeout occurred. Try to fetch
		 *	a message again.
		 */

		fetchRet = ntcpip__sysArchMboxTryFetch(pMbox, ppMsg);

		/*
		 *	Calculate total time waited in this loop.
		 */

		funcTime += elapsed;
	}

	return(funcTime);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysArchMboxTryFetch
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Try to fetch a mail from the mailbox.
*
*	\param		pMbox 	Pointer to the mailbox.
*	\param		ppMsg 	Pointer to the message in the mailbox.
*
*	\return		NTCPIP_SYS_MBOX_EMPTY if mailbox is empty. 0 if a mail was
*				fetched.
*
*	\details	This is similar to ntcpip__sysArchMboxFetch, however if a message
*				is not present in the mailbox, it immediately returns with the
*				code NTCPIP_SYS_MBOX_EMPTY. On success 0 is returned.
*
*				To allow for efficient implementations, this can be defined as a
*				function-like macro in sys_arch.h instead of a normal function.
*				For example, a naive implementation could be:\n
*	\code
*				#define ntcpip__sysArchMboxTryFetch(mbox,msg) \\
*				    ntcpip__sysArchMboxFetch(mbox,msg,1)
*	\endcode
*				although this would introduce unnecessary delays.
*
*	\note		This function will never block.
*
*******************************************************************************/

PUBLIC Uint32 ntcpip__sysArchMboxTryFetch(
	ntcpip__SysMbox 		pMbox,
	void **					ppMsg
) {
	Uint32			ret;
	lib_RBufStat	ringBuffStatus;

	osa_taskingDisable();

	/*
	 *	Try to get a message from the ring buffer.
	 */

	ringBuffStatus = ntcpip__sysRbufGet(pMbox->pRingBuf, ppMsg);

	ret = NTCPIP__SYS_MBOX_EMPTY;
	if (ringBuffStatus != LIB_RBUF_UFLOW) {
		/*
		 *	A message was successfully fetched from the ring buffer.
		 */

		if (ringBuffStatus == LIB_RBUF_OK) {
			/*
			 *	There was at least one message left on the ring buffer. Set
			 *	the semaphore again for the next fetch.
			 */

			osa_semaSet(&pMbox->newMail);
		}

		ret = 0;
	}

	osa_taskingEnable();

	return(ret);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysGetMboxStatus
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Provides the current pool status.
*
*	\param		pStatus		Pointer to structure that will be filled by the
*							function.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void ntcpip__sysGetMboxStatus(
	ntcpip_SysStatus * 		pStatus
) {
	Ufast8				boxesLeft;
	ntcpip__SysMbox 	pMbox;
	Uint32				freeSpace;

	osa_taskingDisable();

	/*
	 * Loop through the mailbox list and check status.
	 */

	pMbox = ntcpip__sysMboxInst.boxes;
	freeSpace = Uint32_MAX;
	boxesLeft = NTCPIP__SYS_MBOX_NUM;
	do {
		lib_RBufSDim mboxFree;

		ntcpip__sysRbufInfo(pMbox->pRingBuf, &mboxFree);

		if (mboxFree < freeSpace) {
			freeSpace = mboxFree;
		}

		pMbox++;
	} while (--boxesLeft);

	pStatus->mboxPool.freeCount = ntcpip__sysMboxInst.freeCount;
	pStatus->mboxPool.lowestCount = ntcpip__sysMboxInst.lowCount;

	pStatus->mboxStatus.mboxLowSpace = ntcpip__sysMboxInst.mboxLowest;

	osa_taskingEnable();

	pStatus->mboxPool.totalCount = NTCPIP__SYS_MBOX_NUM;

	pStatus->mboxStatus.mboxSpace = freeSpace;
	pStatus->mboxStatus.mboxSize = NTCPIP__SYS_MBOX_SIZE;
}
