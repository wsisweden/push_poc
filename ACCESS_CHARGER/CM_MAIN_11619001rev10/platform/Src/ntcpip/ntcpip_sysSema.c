/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		ntcpip_sysSema.c
*
*	\ingroup	NTCPIP
*
*	\brief		LwIP semaphore implementation.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 600 $ \n
*				\$Date: 2016-06-10 15:24:11 +0300 (pe, 10 kes� 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "deb.h"
#include "ntcpip/sys.h"
#include "TSTAMP.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/* Semaphores are 76 bytes in FreeRTOS! */

#ifndef NTCPIP__SYS_SEMA_NUM
# define NTCPIP__SYS_SEMA_NUM	10		/**< Number of semaphores to allocate*/
#endif

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Semaphore type used by LwIP.
 */

struct ntcpip__sysSema {				/*''''''''''''''''''''''''''''''''''*/
	osa_SemaType			osaSema;	/**< OSA semaphore                  */
	ntcpip__SysSema 		pNext;		/**< Pointer to the next semaphore  */
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Free semaphore list type. A list of semaphores is statically allocated. When
 * LwIP wants to "dynamically allocate" a semaphore then a semaphore from this
 * list is used.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	struct ntcpip__sysSema	semas[NTCPIP__SYS_SEMA_NUM];	/**<            */
	ntcpip__SysSema			pHead;		/**< Pointer to list head.          */
	Uint8 					freeCount;	/**< Number of free semaphores.     */
	Uint8 					lowCount;	/**< Lowest free semaphore count.	*/
} ntcpip__SysSemaList;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE ntcpip__SysSemaList	ntcpip__sysSemaList;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Init the OS emulation layer.
*
*	\details	Initializes the allocated lists.
*
*	\note		Will be called with interrupts disabled before OSA is running.
*
*******************************************************************************/

PUBLIC void ntcpip__sysSemaInit(
	void
) {
	Ufast8 ii;

	/*
	 * Initialize the semaphore pool
	 */

	for (ii = 0; ii < NTCPIP__SYS_SEMA_NUM; ii++) {
		ntcpip__sysSemaList.semas[ii].pNext =
			&ntcpip__sysSemaList.semas[ii+1];

		osa_newSemaSet(&ntcpip__sysSemaList.semas[ii].osaSema);
	}

	ntcpip__sysSemaList.semas[NTCPIP__SYS_SEMA_NUM-1].pNext = NULL;

	ntcpip__sysSemaList.pHead = &ntcpip__sysSemaList.semas[0];
	ntcpip__sysSemaList.freeCount = NTCPIP__SYS_SEMA_NUM;
	ntcpip__sysSemaList.lowCount = NTCPIP__SYS_SEMA_NUM;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysSemNew
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Creates and returns a new semaphore.
*
*	\param		count 	Initial state of the semaphore. 0 = taken.
*
*	\details	The count argument specifies the initial state of the semaphore.
*				Returns the semaphore, or NTCPIP_SYS_SEM_NULL on error.
*
*	\note		May be called with interrupts disabled.
*
*******************************************************************************/

PUBLIC ntcpip__SysSema ntcpip__sysSemNew(
	Uint8 					count
) {
	ntcpip__SysSema pSema;

	pSema = NTCPIP__SYS_SEM_NULL;

	/*
	 * A semaphore is picked from the free semaphore list.
	 */

	atomicintr(
		if (ntcpip__sysSemaList.pHead != NULL) {
			pSema = ntcpip__sysSemaList.pHead;
			ntcpip__sysSemaList.pHead = pSema->pNext;

			ntcpip__sysSemaList.freeCount--;
			if (ntcpip__sysSemaList.freeCount < ntcpip__sysSemaList.lowCount) {
				ntcpip__sysSemaList.lowCount = ntcpip__sysSemaList.freeCount;
			}
		}
	);

	if (pSema != NTCPIP__SYS_SEM_NULL) {
		pSema->pNext = NULL;

		/*
		 * The semaphore should be set here but it is set again just to be
		 * sure it is really set.
		 */

		osa_semaSet(&pSema->osaSema);

		if (count == 0) {
			/*
			 * Make sure the semaphore is taken. This should not result in any
			 * waiting because the semaphore is set.
			 */

			osa_semaGet(&pSema->osaSema);
		}
	}

	return(pSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysSemFree
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Frees a semaphore created by ntcpip__sysSemNew.
*
*	\param		pSema 	Pointer to the semaphore to free.
*
*	\details	Since these two functions provide the entry and exit point for
*				all semaphores used by lwIP, you have great flexibility in how
*				these are allocated and deallocated (for example, from the heap,
*				a memory pool, a semaphore pool, etc).
*
*	\note
*
*******************************************************************************/

PUBLIC void ntcpip__sysSemFree(
	ntcpip__SysSema 				pSema
) {
	if (pSema != NULL) {
		/*
		* All semaphores in the list should be set (released).
		*/

		osa_semaSet(&pSema->osaSema);

		/*
		* Inserting the semaphore into the free semaphore list.
		*/

		DISABLE;
		pSema->pNext = ntcpip__sysSemaList.pHead;
		ntcpip__sysSemaList.pHead = pSema;
		ntcpip__sysSemaList.freeCount++;
		ENABLE;

	} else {
		/*
		 *	This should never happen.
		 */
		deb_assert(FALSE);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysSemSignal
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Signals (or releases) a semaphore.
*
*	\param		pSema 	Pointer to the semaphore
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void ntcpip__sysSemSignal(
	ntcpip__SysSema 				pSema
) {
	osa_semaSet(&pSema->osaSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip__sysArchSemWait
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Blocks the thread while waiting for the semaphore to be
*				signaled.
*
*	\param		pSema 		The semaphore to wait for.
*	\param		timeout		The max waiting time in milliseconds. If timeout
*							is 0, then the function should block indefinitely.
*
*	\details	The timeout parameter specifies how many milliseconds the
*				function should block before returning; if the function times
*				out, it should return NTCPIP_SYS_ARCH_TIMEOUT. If the function
*				acquires the semaphore, it should return how many milliseconds
*				expired while waiting for the semaphore. The function may return
*				0 if the semaphore was immediately available.
*
*	\note
*
*******************************************************************************/

PUBLIC Uint32 ntcpip__sysArchSemWait(
	ntcpip__SysSema 		pSema,
	Uint32 					timeout
) {
	Uint32 			elapsed;
	tstamp_Stamp	stamp;

	tstamp_getStamp(&stamp);

	if (timeout == 0) {
		osa_semaGet(&pSema->osaSema);

	} else {
		osa_Status error;

		osa_semaWait(&pSema->osaSema, (Uint16) osa_msToTicks(timeout), &error);

		if (error) {
			return(NTCPIP__SYS_ARCH_TIMEOUT);
		}
	}

	tstamp_getDiffCurr(&stamp, &elapsed);

	return(elapsed);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ntcpip_sysGetStatus
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Provides the current pool status.
*
*	\param		pStatus		Pointer to structure that will be filled by the
*							function.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void ntcpip__sysGetSemaStatus(
	ntcpip_SysStatus * 		pStatus
) {
	pStatus->semaPool.totalCount = NTCPIP__SYS_SEMA_NUM;

	DISABLE;

	pStatus->semaPool.freeCount = ntcpip__sysSemaList.freeCount;
	pStatus->semaPool.lowestCount = ntcpip__sysSemaList.lowCount;	

	ENABLE;
}
