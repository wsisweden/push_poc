/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		memdump.c
*
*	\ingroup	MEMDUMP
*
*	\brief		Memdump implementation.
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	MEMDUMP		MEMDUMP
*
*	\brief		Memory contents dumper.
*
********************************************************************************
*
*	\details	It can be difficult to get the contents of some memories, for
*				example external non-volatile memories. This block will read the
*				data of the whole memory and send it through UART or some other
*				port that uses the DRV1 interface.
*
*				MEMDUMP can be used with any memory driver that uses the MEMDRV
*				interface.
*	<pre>		
*				                 MEMDRV interface     DRV1 interface
*				                 :                    :
*				+-------------+  :   +-------------+  :   +-------------------+
*				| Some memory |--:-->|   MEMDUMP   |--:-->| Some port handler |
*				| driver      |  :   |             |  :   | e.g. UART         |
*				+-------------+  :   +-------------+  :   +-------------------+
*				                 :                    :
*	</pre>
*				MEMDUMP will read and dump the memory contents before the
*				sys_mainInitReady function is called which means that it will
*				happen at startup before the reset call is made.
*
*				The function block has a higher priority task so that memory
*				contents can be dumped as uninterrupted as possible. It is still
*				possible for other tasks to interrupt the process when MEMDUMP
*				is waiting for some driver to complete a request. Writing to the
*				memory while MEMDUMP is reading its contents will almost
*				certainly give unwanted results. Usually the best way to get
*				reliable output is to disable all other function blocks that
*				are writing to the memory during startup.
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "sys.h"
#include "reg.h"
#include "mem.h"
#include "deb.h"

#include "memdump.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

struct memdump__inst;
typedef struct memdump__inst memdump__Inst;

/**
 *	Type used as memory driver request.
 */

typedef struct {
	memdrv_Alloc			drvReq;
	memdump__Inst *			pInst;
} memdump__DrvRequest;

/**
 *	Memdump FB instance data.
 */

struct memdump__inst {					/*''''''''''''''''''''''''''''' RAM */
	osa_TaskType			task;		/**< FB main task.					*/
	osa_SemaType			taskSema;	/**< Semaphore for task triggering.	*/
	memdump_Init const_P *	pInit;		/**< Ptr to initialization values.	*/
	memdump__DrvRequest		request;	/**< Memory driver request.			*/
	void *					pDrvInst;	/**< Pointer to memory driver inst.	*/
	void *					pUartInst;	/**< Pointer to UART FB instance.	*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE OSA_TASK void		memdump__task(void);
PRIVATE void				memdump__reqDoneFn(memdrv_Alloc *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	memdump_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		pInit		Pointer to FB initialization values.
*	\param		pDrvInst	Pointer to memory driver instance if it is available
*							when calling this function. If it's not available
*							then the driver instance id has to be supplied
*							in the init values struct. This parameter must be 
*							null if the pointer isn't available.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void memdump_init(
	memdump_Init const_P *	pInit,
	void *					pDrvInst
) {
	memdump__Inst *			pInst;

	pInst = (memdump__Inst *) mem_reserve(&mem_normal, sizeof(memdump__Inst));

	deb_assert(pInst);

	osa_newTask(
		&pInst->task,
		"memdump__task",
		OSA_PRIORITY_HIGH,
		memdump__task,
		512
	);

	osa_newSemaTaken(&pInst->taskSema);

	pInst->task.pUtil = pInst;
	pInst->pInit = pInit;
	pInst->pDrvInst = pDrvInst;
	pInst->request.pInst = pInst;

	sys_mainInitCount(1);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	memdump__task
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE OSA_TASK void memdump__task(
	void
) {
	memdump__Inst *			pInst;
	Uint32					endAddress;

	pInst = (memdump__Inst *) osa_taskCurrent()->pUtil;

	/*
	 * Setup memory driver request and initialize UART.
	 */

	endAddress = pInst->pInit->startAddr + pInst->pInit->memSize;
	pInst->request.drvReq.address = pInst->pInit->startAddr;
	pInst->request.drvReq.fnDone = &memdump__reqDoneFn;
	pInst->request.drvReq.pData = reg_bufAlloc();
	deb_assert(pInst->request.drvReq.pData != NULL);

	if (pInst->pDrvInst == NULL) {
		deb_assert(pInst->pInit->drvInstId != SYS_BAD_FB_INST_ID);
		pInst->pDrvInst = sys_instIdToTHIS(pInst->pInit->drvInstId);
	}

	pInst->pUartInst = pInst->pInit->uartIf->init(pInst->pInit->pUartCfg,pInst);

	/*
	 * Read the whole memory in blocks that fits into the reg buffer and write 
	 * them to UART one block at a time.
	 */

	do {
		Uint32 nextEnd;
		Uint16 readSize;

		readSize = pInst->pInit->regBuffSize;

		/*
		 * Check that the next read will be within the memory address space
		 */

		nextEnd = pInst->request.drvReq.address + readSize;
		if (nextEnd > endAddress) {
			readSize -= (Uint16) (nextEnd - endAddress);
		}
		pInst->request.drvReq.count = (memdrv_SizeType) readSize;

		/*
		 * Call memory driver and wait until callback function is called.
		 */

		pInst->pInit->drvIf->get(pInst->pDrvInst, &pInst->request.drvReq);
		osa_semaGet(&pInst->taskSema);

		/*
		 * Write the data to UART and wait until it has been sent.
		 */

		pInst->pInit->uartIf->write(
			pInst->pUartInst,
			pInst->request.drvReq.pData,
			readSize
		);
		osa_semaGet(&pInst->taskSema);

		pInst->request.drvReq.address += readSize;

	} while (pInst->request.drvReq.address < endAddress);
	
	/*
	 * The job is done. Just do nothing from now on.
	 */

	reg_bufFree(pInst->request.drvReq.pData);
	sys_mainInitReady();
	
	FOREVER {
		osa_signalGet();
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	memdump__reqDoneFn
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Callback function called by the memory driver when the request
*				has been handled.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void memdump__reqDoneFn(
	memdrv_Alloc *			pAlloc
) {
	memdump__DrvRequest *	pReq;

	pReq = (memdump__DrvRequest *) pAlloc;

	deb_assert(pReq->drvReq.count == MEMDRV_OK);

	osa_semaSet(&pReq->pInst->taskSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	memdump_dataSent
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Called by UART driver when data has been sent.
*
*	\param		pUtil	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note		The function is called from an ISR.
*
*******************************************************************************/

PUBLIC void memdump_dataSent(
	void *					pUtil
) {
	memdump__Inst *			pInst;

	pInst = (memdump__Inst *) pUtil;

	osa_semaSetIsr(&pInst->taskSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	memdump_dataReceived
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Called by UART driver when a byte has been received.
*
*	\param		data	The received byte.
*	\param		pUtil	Pointer to instance.
*
*	\return		
*
*	\details
*
*	\note		The function is called from an ISR.
*
*******************************************************************************/

PUBLIC void memdump_dataReceived(
	Uint8					data,
	void *					pUtil
) {
	/*
	 * This function block never receives any data through UART. If it does
	 * then it's just thrown away.
	 */
}
