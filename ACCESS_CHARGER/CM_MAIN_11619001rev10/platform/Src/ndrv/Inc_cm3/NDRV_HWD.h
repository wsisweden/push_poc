/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file
*
*	\ingroup	NDRV_CM3
*
*	\brief		Public declarations for the NXP MAC driver module.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 3062 $ \n
*				\$Date: 2016-10-10 15:38:17 +0300 (ma, 10 loka 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef NDRV_HWD_H_INCLUDED
#define NDRV_HWD_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#define NDRV__TX_DESC_COUNT	20			/**< Number of TX descriptors		*/
#define NDRV__RX_DESC_COUNT	20			/**< Number of RX descriptors		*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/* EMAC interface functions */
PUBLIC ndrv__Inst *		ndrv__hwInit(ndrv_Init const_P *);
PUBLIC Uint8 			ndrv__hwWarmInit(ndrv__Inst *,Uint8 *);
PUBLIC void 			ndrv__hwUpdLink(ndrv__Inst *);
PUBLIC Uint8 			ndrv__hwInput(ndrv__Inst *);
PUBLIC void 			ndrv__hwOutput(ndrv__Inst *,ndif_Request const_D *);
PUBLIC void				ndrv__hwTxDone(ndrv__Inst *);
PUBLIC void 			ndrv__hwSetupRx(ndrv__Inst *,ndif_Cbuff *,Ufast8);
PUBLIC void 			ndrv__hwUpdFilter(ndrv__Inst *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
