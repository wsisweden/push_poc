/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[X]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NDRV.C
*
*	\ingroup	NDRV
*
*	\brief		FB interface functions and the main task.
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	NDRV	NDRV
*
*	\brief		Network driver for internal EMACs.
*
********************************************************************************
*
*	\details	The driver can be used trough the Network Driver Interface
*				(NDIF).
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "deb.h"
#include "ndrv.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE OSA_TASK void ndrv__task(void);
PRIVATE void 	ndrv__output(void *,ndif_Request const_D *);
PRIVATE void	ndrv__getActivation(void * pInst);
PRIVATE Boolean	ndrv__multiFiltDef(void *,Uint8 const_D *,Boolean);
PRIVATE Uint8	ndrv__allocBuffer(void * pInst,ndif_Cbuff * pData,Uint8 count);
PRIVATE void	ndrv__freeBuffer(void * pInst, void * pUtil);
PRIVATE void	ndrv__allocRxBuffers(ndrv__Inst * pInst);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * TX Interface constant.
 */

PRIVATE ndif_TxIf const_P ndrv__txIf = {
	&ndrv__output,
	&ndrv__getActivation,
	&ndrv__multiFiltDef,
	&ndrv__allocBuffer,
	&ndrv__freeBuffer
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		pInit 	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * ndrv_init(
	ndrv_Init const_P *		pInit
) {
	ndrv__Inst *			pInst;

	pInst = pInit->pEmacIf->pInitFn(pInit);

	osa_newTask(
		&pInst->task,
		"ndrv__task",
		pInit->priority,
		ndrv__task,
		1024		/* TODO: check this */
	);

	pInst->task.pUtil = pInst;
	osa_newSemaTaken(&pInst->taskSema);
	osa_newSemaSet(&pInst->txSema);
	pInst->pInit = pInit;
	pInst->triggers = 0;
	pInst->rxDone = 0;

	pInst->pBufHandler = pInit->pBuffIf->pInitBuffFn(
		pInit->blockSize,
		pInit->blockCount
	);

	/*
	 * Initialize the PHY driver.
	 */

	pInst->pPhyInst = pInst->pInit->pPhyIf->pInitFn(pInst->pInit->pPhyInit);

	/*
	 * Initialize the multicast frame filter.
	 */

	if (pInit->pMultiFiltIf != NULL) {
		pInit->pMultiFiltIf->pInitFiltFn(pInst);
	}

	sys_mainInitCount(1);

	return((void *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__task
*
*--------------------------------------------------------------------------*//**
*
*	\brief		The main task of the FB.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE OSA_TASK void ndrv__task(
	void
) {
	ndrv__Inst *				pInst;

	pInst = (ndrv__Inst *) osa_taskCurrent()->pUtil;

	/*
	 * Initialize EMAC hardware and input function blocks.
	 */

	if (
		pInst->pInit->macAddrReg != SYS_BAD_REGISTER
		|| pInst->pInit->pMacAddReg != NULL
	) {
		Uint8 macAddr[7];

		/*
		 * Must wait for REG to be operational before reading the MAC
		 * address. The MAC address is stored in a string register.
		 */

		osa_syncGet(OSA__SYNC_REG);

		if (pInst->pInit->macAddrReg != SYS_BAD_REGISTER) {
			reg_get(&macAddr, pInst->pInit->macAddrReg);
		} else {
			reg_get(&macAddr, *pInst->pInit->pMacAddReg);
		}

		pInst->ndifInit.macAddr[0] = macAddr[1];
		pInst->ndifInit.macAddr[1] = macAddr[2];
		pInst->ndifInit.macAddr[2] = macAddr[3];
		pInst->ndifInit.macAddr[3] = macAddr[4];
		pInst->ndifInit.macAddr[4] = macAddr[5];
		pInst->ndifInit.macAddr[5] = macAddr[6];

	} else if (pInst->pInit->pMacAddr != NULL) {
		pInst->ndifInit.macAddr[0] = pInst->pInit->pMacAddr[0];
		pInst->ndifInit.macAddr[1] = pInst->pInit->pMacAddr[1];
		pInst->ndifInit.macAddr[2] = pInst->pInit->pMacAddr[2];
		pInst->ndifInit.macAddr[3] = pInst->pInit->pMacAddr[3];
		pInst->ndifInit.macAddr[4] = pInst->pInit->pMacAddr[4];
		pInst->ndifInit.macAddr[5] = pInst->pInit->pMacAddr[5];
	}

	pInst->ndifInit.mtu = pInst->pInit->mtu;
	pInst->ndifInit.flags = NDIF_OPT_ARP;
	pInst->ndifInit.pTxIf = &ndrv__txIf;
	pInst->ndifInit.pMacInst = pInst;

  	if (pInst->pInit->options & NDRV_OPT_ACC_BROADCAST) {
  		pInst->ndifInit.flags |= NDIF_CAP_BROADCAST;
  	}

  	if (
  		(pInst->pInit->options & NDRV_OPT_ACC_MULTICAST)
  		|| (pInst->pInit->fltAddrCount != 0)
  	) {
  		pInst->ndifInit.flags |= NDIF_CAP_IGMP;
  	}

  	if (pInst->pInit->options & NDRV_OPT_DEFAULT) {
  		pInst->ndifInit.flags |= NDIF_OPT_DEFAULT;
  	}

  	/*
  	 * Allocate required RX buffers according to number of
  	 * buffers available on lower level.
  	 */
  	pInst->unallocated = pInst->pInit->pEmacIf->rxDescCount;
  	ndrv__allocRxBuffers(pInst);

	/*
	 * EMAC driver warm initialization.
	 */

	pInst->pInit->pEmacIf->pWarmInitFn(
		pInst,
		pInst->ndifInit.macAddr
	);

	/*
	 * Call the warm init function of RX handing function blocks.
	 */

	{
		ndrv_ProtHandler const_P * pProtHandler;

		pProtHandler = pInst->pInit->pProtHan;
		do {
			pProtHandler->pRxIf->pInitFn(
				*pProtHandler->ppUtil,
				&pInst->ndifInit
			);
			pProtHandler++;
		} while (pProtHandler->ppUtil != 0);
	}


	/*
	 * Start poll timer if PHY polling is enabled
	 */

	if (pInst->pInit->pInitPollFn) {
		pInst->pInit->pInitPollFn(pInst);
	}

	sys_mainInitReady();

	FOREVER {
		ndrv__Triggers	triggers;

		osa_semaGet(&pInst->taskSema);

		atomic(
			triggers = pInst->triggers;
			pInst->triggers = 0;
		);

		if (triggers & NDRV__TRG_RXTRIG) {
			pInst->unallocated += pInst->pInit->pEmacIf->pInputFn(pInst);
		}

		if (triggers & NDRV__TRG_TXTRIG) {
			pInst->pInit->pEmacIf->pTxDoneFn(pInst);
		}

		if (triggers & NDRV__TRG_POLLPHY) {
			pInst->pInit->pEmacIf->pUpdLinkFn(pInst);
		}

		if (triggers & NDRV__TRG_MULTIFILT) {
			pInst->pInit->pEmacIf->pUpdFiltFn(pInst);
		}

		if (triggers & NDRV__TRG_ACTIVATE) {
			ndif_Status status;
			ndrv_ProtHandler const_P * pProtHandler;

			status.flags = 0;
			pProtHandler = pInst->pInit->pProtHan;

			do {
				pProtHandler->pRxIf->pStatusFn(
					*pProtHandler->ppUtil,
					&status
				);
				pProtHandler++;
			} while (pProtHandler->ppUtil != 0);
		}

		/* Allocate new RX buffers if needed */
		if (pInst->unallocated != 0) {
			ndrv__allocRxBuffers(pInst);
		}
	}
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__allocRxBuffers
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate RX buffers.
*
*	\param		pInst		Pointer to FB instance.
*
*	\return		-
*
*	\details	Allocates more RX buffers.
*
*	\note		Uses and updates pInst->unallocated variable.
*
*******************************************************************************/

PRIVATE void ndrv__allocRxBuffers(
	ndrv__Inst *			pInst
) {
	Uint8 			count;
	ndif_Cbuff 		cbuff[13];

	deb_assert(pInst != NULL);
	deb_assert(pInst->pInit);
	deb_assert(pInst->pInit->pEmacIf != NULL);
	deb_assert(pInst->unallocated <= pInst->pInit->pEmacIf->rxDescCount);


	deb_assert(pInst->pInit->pBuffIf);
	deb_assert(pInst->pInit->pBuffIf->pAllocBuffFn);
	deb_assert(pInst->pInit->pEmacIf->pSetupRxFn);

	/*
	 * Allocate up to 13 new RX buffers.
	 * This number is used to limit how much time
	 * is spent when allocating new buffers.
	 *
	 * Allocation is called repeatedly so not too many
	 * buffers have to be allocated per call.
	 *
	 * If this is set too low, then long packets may be
	 * dropped when only few buffers are available.
	 */

	/*
	 * Working settings (1...3,6).
	 *
	 * Tested on VIKE with 20 or 32 kbytes RAM for Ethernet.
	 * 489 kbytes transfer OK.
	 */
	//count = MIN(pInst->unallocated, 1); //Very slow
	//count = MIN(pInst->unallocated, 2); //Quick
	//count = MIN(pInst->unallocated, 3);
	//count = MIN(pInst->unallocated, 6); //Why does not fail?

	/*
	 * Failing settings (4,5,7...13)
	 *
	 * Fails catastrophically with 32 kbytes RAM for Ethernet.
	 */
	//count = MIN(pInst->unallocated, 4);
	//count = MIN(pInst->unallocated, 5);

	//count = MIN(pInst->unallocated, 7);
	//count = MIN(pInst->unallocated, 8);
	//count = MIN(pInst->unallocated, 9);
	//count = MIN(pInst->unallocated, 10);
	//count = MIN(pInst->unallocated, 11);
	//count = MIN(pInst->unallocated, 12);
	count = MIN(pInst->unallocated, 13); //Original T.Plat.E setting

	count = pInst->pInit->pBuffIf->pAllocBuffFn(
		pInst->pBufHandler,
		cbuff,
		count
	);

	/*
	 * Lower level has allocated more than what we are
	 * prepared for, something is very wrong.
	 */
	deb_assert(count <= pInst->unallocated);

	if (count != 0) {
		/* Setup hardware to receive into the allocated buffers */
		pInst->pInit->pEmacIf->pSetupRxFn(pInst, cbuff, count);
		pInst->unallocated -= count;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__output
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Transmit the contents of the TX buffer.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pRequest	Pointer to a NDIF output request.
*
*	\return		-
*
*	\details	This is just a wrapper that calls the hardware specific
*				function.
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__output(
	void *					pInst,
	ndif_Request const_D *	pRequest
) {
	((ndrv__Inst *) pInst)->pInit->pEmacIf->pOutputFn(
		(ndrv__Inst *) pInst,
		pRequest
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__getActivation
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Triggers call of activation function from the driver task.
*
*	\param		pInst	Pointer to driver instance.
*
*	\return		-
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE void ndrv__getActivation(
	void *					pVoidInst
) {
	ndrv__Inst *			pInst;

	pInst = (ndrv__Inst *) pVoidInst;

	atomic(
		pInst->triggers |= NDRV__TRG_ACTIVATE;
	);
	
	osa_semaSet(&pInst->taskSema);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__multiFiltDef
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Update multicast filter.
*
*	\param		pVoidInst	Pointer to driver instance.
*	\param		pMacAddr	Pointer to MAC address that should be added or
*							removed from the filter list.
*	\param		addAddress	TRUE if the address should be added to the list.
*							FALSE if the address should be removed.
*
*	\retval		TRUE 		The list update was successful.
*	\retval		FALSE		The list could not be updated.
*
*	\details	Adding an address to the filter list will pass all frames
*				sent to that address.
*
*	\note
*
*******************************************************************************/

PRIVATE Boolean ndrv__multiFiltDef(
	void *					pVoidInst,
	Uint8 const_D *			pMacAddr,
	Boolean					addAddress
) {
	ndrv__Inst *			pInst;
	Boolean 				retVal;

	pInst = (ndrv__Inst *) pVoidInst;

	retVal = FALSE;
	if (pInst->pInit->pMultiFiltIf->pMultiFiltFn != NULL) {
		retVal = pInst->pInit->pMultiFiltIf->pMultiFiltFn(
			pInst,
			pMacAddr,
			addAddress
		);
	}

	return retVal;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__allocBuffer
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate buffer blocks.
*
*	\param		pVoidInst	Pointer to NDRV instance data.
*	\param		pData		Pointer to chain buffer array that will be filled
*							with information about the allocated buffer blocks.
*	\param		count		Number of buffer blocks that should be allocated.
*
*   \return		The number of buffer blocks that could be allocated.
*
*	\details	
*
*******************************************************************************/

PRIVATE Uint8 ndrv__allocBuffer(
	void *					pVoidInst,
	ndif_Cbuff *			pData,
	Uint8					count
) {
	ndrv__Inst *			pInst;
	
	pInst = (ndrv__Inst *) pVoidInst;

	return pInst->pInit->pBuffIf->pAllocBuffFn(
		pInst->pBufHandler,
		pData,
		count
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__freeBuffer
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Free a buffer block.
*
*	\param		pVoidInst	Pointer to NDRV instance data.
*	\param		pBlockInfo	Pointer to buffer block information.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PRIVATE void ndrv__freeBuffer(
	void *					pVoidInst,
	void *					pBlockInfo
) {
	ndrv__Inst *			pInst;
	
	pInst = (ndrv__Inst *) pVoidInst;

	pInst->pInit->pBuffIf->pFreeBuffFn(pInst->pBufHandler, pBlockInfo);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__handleFrame
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle received frame.
*
*	\param		pInst	Pointer to NDRV instance.
*	\param		pInData	Pointer to frame information structure.
*
*   \return		-
*
*	\details	This function should be called by the low level handler when a
*				frame has been received.
*
*******************************************************************************/

PUBLIC void ndrv__handleFrame(
	ndrv__Inst *			pInst,
	ndif_InData *			pInData
) {
	ndrv_ProtHandler const_P * pProtHandler;
	Boolean accepted;

	/*
	 *	Loop through all receiving function blocks until one of them accepts the
	 *	incoming frame.
	 */

	pProtHandler = pInst->pInit->pProtHan;
	do {
		accepted = pProtHandler->pRxIf->pInputFn(
			*pProtHandler->ppUtil,
			pInData
		);
		pProtHandler++;
	} while (accepted != FALSE && pProtHandler->ppUtil != 0);

	if (accepted == FALSE) {

		/*
		 *	No protocol handler accepted the frame. Free the buffer.
		 */

		ndrv__freeFrame(pInst, pInData);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__freeFrame
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Free received frame.
*
*	\param		pInst	Pointer to NDRV instance.
*	\param		pInData	Pointer to frame information structure.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PUBLIC void ndrv__freeFrame(
	ndrv__Inst *			pInst,
	ndif_InData *			pInData
) {
	Ufast8				buffCount;
	ndif_Cbuff *		pCBuff;
	ndif_FreeBuffFn *	pFreeBuffFn;

	buffCount = pInData->count;
	pCBuff = pInData->pData;
	pFreeBuffFn = pInst->pInit->pBuffIf->pFreeBuffFn;

	do {
		pFreeBuffFn(pInst->pBufHandler, pCBuff->pBlockInfo);
		pCBuff++;
	} while (--buffCount);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv_readClearRxDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read and clear RX done flag.
*
*	\param		pInst	Pointer to NDRV instance.
*
*   \return		Returns 1 = RX done flag was set, 0 = RX done flag was not set
*
*	\details	
*
*******************************************************************************/
Uint8 ndrv_readClearRxDone(
	ndrv__Inst *			pInst
) {
	Uint8					ret;

	DISABLE;
	ret = pInst->rxDone;
	pInst->rxDone = 0;
	ENABLE;

	return ret;
}
