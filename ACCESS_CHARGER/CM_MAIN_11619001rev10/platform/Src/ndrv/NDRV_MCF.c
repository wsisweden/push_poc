/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NDRV_MCF.c
*
*	\brief		Multicast address filter handling.
*
*	\details		
*
*	\note		
*
*	\version	\$Rev$ \n
*				\$Date$	\n
*				\$Author$
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "deb.h"
#include "mem.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void ndrv__initFilter(ndrv__Inst *);
PRIVATE Boolean ndrv__multiFilter(ndrv__Inst *,Uint8 const_D *,Boolean);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Multicast frame filter interface constant. A pointer to this constant should
 * be configured in the NDRV initialization parameters to enable the multicast
 * frame filter.
 */

PUBLIC ndrv__MultiFiltIf const_P ndrv_MultiFilt = {
	&ndrv__initFilter,
	&ndrv__multiFilter
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * This constant is used by the multicast address filter.
 */

PRIVATE Uint8 const_D ndrv__invalidAddr[] = {
	0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__initFilter
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the multicast frame filter.
*
*	\param		pInst		Pointer to driver instance.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__initFilter(
	ndrv__Inst *			pInst
) {
	Ufast8 filterIdx;
	ndrv__MacAddr * pMacAddr;

	deb_assert(pInst->pInit->fltAddrCount != 0);

	pInst->pFilterMacs = mem_reserve(
		&mem_normal,
		sizeof(ndrv__MacAddr) * pInst->pInit->fltAddrCount
	);
	deb_assert(pInst->pFilterMacs != NULL);

	filterIdx = pInst->pInit->fltAddrCount;
	pMacAddr = pInst->pFilterMacs;
	do {
		(pMacAddr)->addr[0] = 0xFFU;
		(pMacAddr)->addr[1] = 0xFFU;
		(pMacAddr)->addr[2] = 0xFFU;
		(pMacAddr)->addr[3] = 0xFFU;
		(pMacAddr)->addr[4] = 0xFFU;
		(pMacAddr++)->addr[5] = 0xFFU;
	} while (--filterIdx != 0);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__multiFilt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Update multicast filter.
*
*	\param		pInst		Pointer to driver instance.
*	\param		pMacAddr	Pointer to MAC address that should be added or
*							removed from the filter list.
*	\param		addAddress	TRUE if the address should be added to the list.
*							FALSE if the address should be removed.
*
*	\retval		TRUE 		The list update was successful.
*	\retval		FALSE		The list could not be updated.
*
*	\details	A pointer to this function can be provided in the NDRV init
*				parameters to enable multicast frame filtering.
*
*	\note		The function should never be called from an other function
*				block.
*
*******************************************************************************/

PRIVATE Boolean ndrv__multiFilter(
	ndrv__Inst *			pInst,
	Uint8 const_D *			pMacAddr,
	Boolean					addAddress
) {
	Boolean					retVal;
	Ufast8 					filtIdx;
	ndrv__MacAddr * 		pListAddr;
	Uint8 const_D * 		pSearchAddr;
	Uint8 const_D * 		pSetAddr;

	retVal = FALSE;

	if (addAddress) {
		pSearchAddr = ndrv__invalidAddr;
		pSetAddr = pMacAddr;
	} else {
		pSearchAddr = pMacAddr;
		pSetAddr = ndrv__invalidAddr;
	}

	/*
	 *	Find the element with the supplied MAC address.
	 */

	osa_taskingDisable();

	pListAddr = pInst->pFilterMacs;
	filtIdx = pInst->pInit->fltAddrCount;
	do {
		if (
			pListAddr->addr[0] == pSearchAddr[0]
			&& pListAddr->addr[1] == pSearchAddr[1]
			&& pListAddr->addr[2] == pSearchAddr[2]
			&& pListAddr->addr[3] == pSearchAddr[3]
			&& pListAddr->addr[4] == pSearchAddr[4]
			&& pListAddr->addr[5] == pSearchAddr[5]
		) {
			pListAddr->addr[0] = pSetAddr[0];
			pListAddr->addr[1] = pSetAddr[1];
			pListAddr->addr[2] = pSetAddr[2];
			pListAddr->addr[3] = pSetAddr[3];
			pListAddr->addr[4] = pSetAddr[4];
			pListAddr->addr[5] = pSetAddr[5];
			retVal = TRUE;
			break;
		}
		pListAddr++;
	} while (--filtIdx != 0);

	osa_taskingEnable();

	/*
	 * Trigger the task if a change was made to the address list.
	 */

	if (retVal == TRUE) {
		atomic(
			pInst->triggers |= NDRV__TRG_MULTIFILT;
		);

		osa_semaSet(&pInst->taskSema);
	}

	return retVal;
}
