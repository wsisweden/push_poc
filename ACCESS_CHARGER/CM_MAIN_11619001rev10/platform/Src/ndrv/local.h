/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	NDRV
*
*	\brief		Local declarations for the network driver FB
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

#ifndef LOCAL_H_INCLUDED
#define LOCAL_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "phyif.h"
#include "ndrv.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	Number of MAC addresses that can be stored to the filter.
 */

#define NDRV__MACFILTER_COUNT	5		

/**
 * \name	Task signaling flags
 */

/*@{*/
#define NDRV__TRG_POLLPHY	(1<<0)		/**< Poll PHY for link changes      */
#define NDRV__TRG_RXTRIG	(1<<1)		/**< Trigger RX function            */
#define NDRV__TRG_ACTIVATE	(1<<2)		/**< Call activation function		*/
#define NDRV__TRG_TXTRIG	(1<<3)		/**< Frame has been transmitted.	*/
#define NDRV__TRG_MULTIFILT	(1<<4)		/**< Update multicast filter.		*/
/*@}*/

/**
 * \name	Ethernet frame bytes
 */

enum ndrv__frame {						/*''''''''''''''''''''''''''''''''''*/
	NDRV__ETH_DSTADDR_0 = 0,			/**< Destination address byte 0		*/
	NDRV__ETH_DSTADDR_1,				/**< Destination address byte 1		*/
	NDRV__ETH_DSTADDR_2,				/**< Destination address byte 2		*/
	NDRV__ETH_DSTADDR_3,				/**< Destination address byte 3		*/
	NDRV__ETH_DSTADDR_4,				/**< Destination address byte 4		*/
	NDRV__ETH_DSTADDR_5,				/**< Destination address byte 5		*/
	NDRV__ETH_SRCADDR_0,				/**< Source address byte 0			*/
	NDRV__ETH_SRCADDR_1,				/**< Source address byte 1			*/
	NDRV__ETH_SRCADDR_2,				/**< Source address byte 2			*/
	NDRV__ETH_SRCADDR_3,				/**< Source address byte 3			*/
	NDRV__ETH_SRCADDR_4,				/**< Source address byte 4			*/
	NDRV__ETH_SRCADDR_5,				/**< Source address byte 5			*/
	NDRV__ETH_TYPE_H,					/**< Ethtype/size high byte			*/
	NDRV__ETH_TYPE_L,					/**< Ethtype/size low byte			*/
	NDRV__ETH_PAYLOAD					/**< Payload offset					*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Type used by task triggers. The size of this type can be increased if there
 * is need for more than 8 triggers.
 */

typedef Uint8				ndrv__Triggers;

/**
 * Type used for MAC addresses in the multicast filter.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	Uint8 					addr[6];	/**< The address.					*/
} ndrv__MacAddr;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * FB instance type.
 */

struct ndrv__inst {						/*''''''''''''''''''''''''''''' RAM	*/
	ndrv__Triggers 			triggers;	/**< Task execution trigger flags	*/
	Uint8					unallocated;/**< Unallocated RX buffers			*/
	ndrv_Init const_P * 	pInit;		/**< Pointer to initialization data */
	phyif_PhyInst * 		pPhyInst;	/**< Ptr to PHY driver instance		*/
	ndrv__MacAddr *			pFilterMacs;/**< Multicast filter MAC addresses.*/
	void *					pBufHandler;/**< Pointer to buffer handler.		*/
	ndif_Init 				ndifInit;	/**< NDIF init values.				*/
	osa_TaskType			task;		/**< Task                           */
	osa_SemaType			taskSema;	/**< Task trigger semaphore			*/
	osa_SemaType			txSema;		/**< TX buffer locking semaphore	*/
	Uint8					rxDone;		/**< RX done interrupt flag			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief	EMAC HW init function type.
 */

typedef ndrv__Inst *		ndrv__InitFn(ndrv_Init const_P *);

/**
 * \brief	EMAC HW warm init function type.
 *
 * \details	The Warm initialization function is called after OSA has been
 * 			started.
 *
 * 			Parameters:
 * 			- NDRV instance pointer
 * 			- Pointer to array containing the MAC-address.
 */

typedef Uint8 				ndrv__WarmInitFn(ndrv__Inst *,Uint8 *);

/** EMAC HW link update function type */
typedef void 				ndrv__UpdLinkFn(ndrv__Inst *);

/** EMAC HW input function type */
typedef Uint8 				ndrv__InputFn(ndrv__Inst *);

/** EMAC HW RX setup function type */
typedef void 				ndrv__SetupRxFn(ndrv__Inst *,ndif_Cbuff *,Ufast8);

/** EMAC HW TX activation function type */
typedef void				ndrv__OutputFn(ndrv__Inst *,ndif_Request const_D *);

/** Frame sent function */
typedef void				ndrv__TxDoneFn(ndrv__Inst *);

/** Type for multicast filter update function. */
typedef void				ndrv__UpdFiltFn(ndrv__Inst *);

/**
 * Type for EMAC hw interface. There should be one public constant of this type
 * for each HW EMAC. Targets with multiple EMACs should have multiple public
 * constants.
 */

struct ndrv__emacIf {					/*''''''''''''''''''''''''''' CONST */
	ndrv__InitFn *		pInitFn;		/**< Ptr to HW init function		*/
	ndrv__WarmInitFn *	pWarmInitFn;	/**< Ptr to warm init function		*/
	ndrv__UpdLinkFn *	pUpdLinkFn;		/**< Ptr to link update function	*/
	ndrv__InputFn *		pInputFn;		/**< Ptr to frame input function	*/
	ndrv__OutputFn * 	pOutputFn;		/**< Ptr to output function			*/
	ndrv__TxDoneFn *	pTxDoneFn;		/**< Ptr to TX done function		*/
	ndrv__SetupRxFn *	pSetupRxFn;		/**< Ptr to RX setup function.		*/
	ndrv__UpdFiltFn *	pUpdFiltFn;		/**< Update multicast filter function.*/
	Uint8				rxDescCount;	/**< Number of RX descriptors.		*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

void ndrv__handleFrame(ndrv__Inst * pInst,ndif_InData * pData);
void ndrv__freeFrame(ndrv__Inst * pInst,ndif_InData * pInData);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/***************************************************************//** \endcond */

#endif
