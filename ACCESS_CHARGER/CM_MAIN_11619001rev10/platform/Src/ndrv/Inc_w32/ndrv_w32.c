/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	NDRV_W32
*
*	\brief		Low level NIC handling.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 2784 $ \n
*				\$Date: 2015-05-18 13:54:53 +0300 (ma, 18 touko 2015) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	NDRV_W32	W32
*
*	\ingroup	NDRV
*
*	\brief		Ethernet MAC driver for W32.
*
********************************************************************************
*
*	\details	W32 target may have multiple NICs.
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "mem.h"
#include "deb.h"
#include "lib.h"

#include "pktdrv.h"
#include "windows.h"

#include "../local.h"

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define NDRV__RX_DESC_COUNT	20			/**< Number of RX descriptors		*/

/**
 * \name	EMAC Descriptor TX and RX Control fields
 */

/*@{*/
#define NDRV__RX_DESC_INT		(1<<31)	/**< Interrupt when data is ready.	*/
/*@}*/

/**
 * \name	Receive status information bits
 */

/*@{*/
#define NDRV__RXSTATUS_LAST		(1<<30)	/**< last fragment of a frame		*/
/*@}*/

#define NDRV__DESC_SIZE_MASK	0x000007FF	/**< 11 bits for both TX and RX */

/**
 * \name	Status flags
 */

/*@{*/
#define NDRV__HWFLG_LINKUP		(1<<0)	/**< Ethernet link is up			*/
/*@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Descriptor type used by TX and RX descriptors.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	Uint32					packet;		/**< Packet buffer addresss.		*/
	Uint32					control;	/**< Control flags					*/
} ndrv__Descriptor;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	RX Status structure.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	Uint32					Info;		/**< Receive Information Status		*/
	Uint32					HashCRC;	/**< Receive Hash CRC Status		*/
} ndrv__RxStatus;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	RX Descriptor state information.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	void *					pUtil;		/**< Data linked with the descriptor */
} ndrv__RxInfo;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 *	Hardware register simulation variables.
 */

typedef struct {
	Uint32			RxConsumeIndex;
	Uint32			RxProduceIndex;
} ndrv__HwEmac;

/**
 * \brief	Instance type used in windows.
 *
 * \details	The windows implementation needs some additional fields for each
 *			instance.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	ndrv__Inst				parent;		/**< The parent instance.			*/
	Boolean					buffersAdded;/**< Buffers added to the FIFO.	*/
	Uint8					flags;		/**< Status flags					*/
	enum link_adapter_event	initEvent;	/**< Init event						*/
	Uint32					recvIdx;	/**< Index of received RX descriptor*/
	void *					packetAdapter;/**< Pointer to packet adapter	*/
	Uint8					macAddr[6];	/**< Interface mac address			*/
	ndrv__HwEmac			hwEmac;		/**< Hardware emulation counters	*/
	ndrv__Descriptor RxDescr[NDRV__RX_DESC_COUNT];/**< RX descriptor array.	*/
	ndrv__RxStatus	RxStatus[NDRV__RX_DESC_COUNT];
	ndrv__RxInfo	RxInfo[NDRV__RX_DESC_COUNT];
} ndrv__HwInst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE ndrv__Inst *		ndrv__hwInit(ndrv_Init const_P *);
PRIVATE Uint8				ndrv__hwWarmInit0(ndrv__Inst *,Uint8 *);
PRIVATE Uint8				ndrv__hwWarmInit1(ndrv__Inst *,Uint8 *);
PRIVATE Uint8				ndrv__hwWarmInit2(ndrv__Inst *,Uint8 *);
PRIVATE Uint8				ndrv__hwWarmInit3(ndrv__Inst *,Uint8 *);
PRIVATE Uint8				ndrv__hwWarmInit4(ndrv__Inst *,Uint8 *);
PRIVATE Uint8				ndrv__hwWarmInit(ndrv__Inst *,Uint8 *);
PRIVATE void 				ndrv__hwUpdLink(ndrv__Inst *);
PRIVATE Uint8 				ndrv__hwInput(ndrv__Inst *);
PRIVATE void 				ndrv__hwOutput(ndrv__Inst *,ndif_Request const_P *);
PRIVATE void				ndrv__hwTxDone(ndrv__Inst *);
PRIVATE void 				ndrv__hwSetupRx(ndrv__Inst *,ndif_Cbuff *,Ufast8);
PRIVATE void				ndrv__hwUpdFilter(ndrv__Inst *);

PRIVATE void				ndrv__hwDescInit(ndrv__HwInst *);

PRIVATE void				ndrv__hwFrameInput(void *, void *, int);
PRIVATE void				ndrv__hwPollThread(void *);

/*************************************************************//** \endcond *//*
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface constant for windows ethernet wrapper. A pointer to this
 * constant should be set to the FB instance init values if EMAC0 should be
 * used.
 */

PUBLIC const_P ndrv_EmacIf ndrv_emac0 = {
	&ndrv__hwInit,
	&ndrv__hwWarmInit0,
	&ndrv__hwUpdLink,
	&ndrv__hwInput,
	&ndrv__hwOutput,
	&ndrv__hwTxDone,
	&ndrv__hwSetupRx,
	&ndrv__hwUpdFilter,
	NDRV__RX_DESC_COUNT
};

PUBLIC const_P ndrv_EmacIf ndrv_emac1 = {
	&ndrv__hwInit,
	&ndrv__hwWarmInit1,
	&ndrv__hwUpdLink,
	&ndrv__hwInput,
	&ndrv__hwOutput,
	&ndrv__hwTxDone,
	&ndrv__hwSetupRx,
	&ndrv__hwUpdFilter,
	NDRV__RX_DESC_COUNT
};

PUBLIC const_P ndrv_EmacIf ndrv_emac2 = {
	&ndrv__hwInit,
	&ndrv__hwWarmInit2,
	&ndrv__hwUpdLink,
	&ndrv__hwInput,
	&ndrv__hwOutput,
	&ndrv__hwTxDone,
	&ndrv__hwSetupRx,
	&ndrv__hwUpdFilter,
	NDRV__RX_DESC_COUNT
};

PUBLIC const_P ndrv_EmacIf ndrv_emac3 = {
	&ndrv__hwInit,
	&ndrv__hwWarmInit3,
	&ndrv__hwUpdLink,
	&ndrv__hwInput,
	&ndrv__hwOutput,
	&ndrv__hwTxDone,
	&ndrv__hwSetupRx,
	&ndrv__hwUpdFilter,
	NDRV__RX_DESC_COUNT
};

PUBLIC const_P ndrv_EmacIf ndrv_emac4 = {
	&ndrv__hwInit,
	&ndrv__hwWarmInit4,
	&ndrv__hwUpdLink,
	&ndrv__hwInput,
	&ndrv__hwOutput,
	&ndrv__hwTxDone,
	&ndrv__hwSetupRx,
	&ndrv__hwUpdFilter,
	NDRV__RX_DESC_COUNT
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the EMAC.
*
*	\param		pInit	Pointer FB to initialization parameters.
*
*	\return		-
*
*	\details	The instance is allocated in this function so that a HW
*				specific struct may be allocated.
*
*	\note		This function is called before OSA is running.
*
*******************************************************************************/

PRIVATE ndrv__Inst * ndrv__hwInit(
	ndrv_Init const_P *		pInit
) {
	ndrv__HwInst *			pHwInst;
	Ufast8					ii;

	/*
	 * The windows implementation uses an own instance type.
	 */

	pHwInst = (ndrv__HwInst *) mem_reserve(&mem_normal, sizeof(ndrv__HwInst));
	pHwInst->parent.pInit = pInit;
	pHwInst->flags = 0;

	pHwInst->hwEmac.RxConsumeIndex = 0;
	pHwInst->hwEmac.RxProduceIndex = 0;
	pHwInst->recvIdx = 0;
	pHwInst->buffersAdded = FALSE;
	
	for (ii = 0; ii < NDRV__RX_DESC_COUNT; ii++) {
		pHwInst->RxDescr[ii].control = 0;
		pHwInst->RxDescr[ii].packet = 0;
	}
	
	for (ii = 0; ii < NDRV__RX_DESC_COUNT; ii++) {
		pHwInst->RxStatus[ii].HashCRC = 0;
		pHwInst->RxStatus[ii].Info = 0;
	}

	for (ii = 0; ii < NDRV__RX_DESC_COUNT; ii++) {
		pHwInst->RxInfo[ii].pUtil = 0;
	}

	return(&pHwInst->parent);
}

PRIVATE Uint8 ndrv__hwWarmInit0(
	ndrv__Inst * 			pInst,
	Uint8 *					pHwAddr
) {
	ndrv__HwInst *			pHwInst;

	pHwInst = (ndrv__HwInst *) pInst;

	pHwInst->packetAdapter = init_adapter(
		0,
		NULL,
		&ndrv__hwFrameInput,
		pInst,
		&pHwInst->initEvent
	);

	return ndrv__hwWarmInit(pInst, pHwAddr);
}

PRIVATE Uint8 ndrv__hwWarmInit1(
	ndrv__Inst * 			pInst,
	Uint8 *					pHwAddr
) {
	ndrv__HwInst *			pHwInst;

	pHwInst = (ndrv__HwInst *) pInst;

	pHwInst->packetAdapter = init_adapter(
		1,
		NULL,
		&ndrv__hwFrameInput,
		pInst,
		&pHwInst->initEvent
	);

	return ndrv__hwWarmInit(pInst, pHwAddr);
}

PRIVATE Uint8 ndrv__hwWarmInit2(
	ndrv__Inst * 			pInst,
	Uint8 *					pHwAddr
) {
	ndrv__HwInst *			pHwInst;

	pHwInst = (ndrv__HwInst *) pInst;

	pHwInst->packetAdapter = init_adapter(
		2,
		NULL,
		&ndrv__hwFrameInput,
		pInst,
		&pHwInst->initEvent
	);

	return ndrv__hwWarmInit(pInst, pHwAddr);
}

PRIVATE Uint8 ndrv__hwWarmInit3(
	ndrv__Inst * 			pInst,
	Uint8 *					pHwAddr
) {
	ndrv__HwInst *			pHwInst;

	pHwInst = (ndrv__HwInst *) pInst;

	pHwInst->packetAdapter = init_adapter(
		3,
		NULL,
		&ndrv__hwFrameInput,
		pInst,
		&pHwInst->initEvent
	);

	return ndrv__hwWarmInit(pInst, pHwAddr);
}

PRIVATE Uint8 ndrv__hwWarmInit4(
	ndrv__Inst * 			pInst,
	Uint8 *					pHwAddr
) {
	ndrv__HwInst *			pHwInst;

	pHwInst = (ndrv__HwInst *) pInst;

	pHwInst->packetAdapter = init_adapter(
		4,
		NULL,
		&ndrv__hwFrameInput,
		pInst,
		&pHwInst->initEvent
	);

	return ndrv__hwWarmInit(pInst, pHwAddr);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwWarmInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Hardware warm initialization.
*
*	\param		pInst	Pointer to the FB instance.
*	\param		pHwAddr	Array containing the MAC address.
*
*	\details
*
*	\note		This function is called when OSA is running.
*
*******************************************************************************/

PRIVATE Uint8 ndrv__hwWarmInit(
	ndrv__Inst * 			pInst,
	Uint8 *					pHwAddr
) {
	ndrv__HwInst *			pHwInst;

	pHwInst = (ndrv__HwInst *) pInst;

	/* Store the MAC address for RX packet filtering */
	memcpy(pHwInst->macAddr, pHwAddr, 6);

	if (pHwInst->packetAdapter != NULL) {
		CreateThread(
			0,
			0,
			(LPTHREAD_START_ROUTINE) ndrv__hwPollThread,
			pHwInst,
			0,
			NULL
		);
	}

	return(NDRV__RX_DESC_COUNT);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwOutput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Transmit the contents of the TX buffer.
*
*	\param		pInst		Pointer to FB instance.
*	\param		pRequest	Pointer to a NDIF output request.
*
*	\return		-
*
*	\details	
*	
*	\note		
*
*******************************************************************************/

PRIVATE void ndrv__hwOutput(
	ndrv__Inst *			pInst,
	ndif_Request const_D *	pRequest
) {
	ndrv__HwInst *			pHwInst;
	
	pHwInst = (ndrv__HwInst *) pInst;

	if (pHwInst->flags & NDRV__HWFLG_LINKUP) {
		ndif_Cbuff const_D *	pCbuff;
		BYTE					buffer[0x600];
		Uint32					dataSize;
		Ufast8					count;

		pCbuff = pRequest->pCBuffer;
		dataSize = 0;

		deb_assert(pRequest->count != 0);
		
		count = pRequest->count;
		do {
			memcpy(&buffer[dataSize], pCbuff->pData, pCbuff->size);
			dataSize += pCbuff->size;
			pCbuff++;
		} while (--count);

		packet_send(pHwInst->packetAdapter, buffer, dataSize);

	}

	pRequest->pFrameSentFn(pRequest->pCBuffer);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwInput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle received data.
*
*	\param		pInst	Pointer to FB instance.
*
*	\return		Number of RX buffers used.
*
*	\details	This function sends all unhandled frames to the protocol stack
*				input function.
*
*	\note
*
*******************************************************************************/

PRIVATE Uint8 ndrv__hwInput(
	ndrv__Inst *			pInst
) {
	ndrv__HwInst *			pHwInst;
	Uint8					freeDescriptors;
	Uint32					recvIdx;
	ndif_Cbuff				cBuff[13];
	Ufast8					chainIdx;
	Ufast8					produceIdx;

	pHwInst = (ndrv__HwInst *) (void *) pInst;
	freeDescriptors = 0;
	recvIdx = pHwInst->recvIdx;
	chainIdx = 0;
	
	DISABLE;
	produceIdx = pHwInst->hwEmac.RxProduceIndex;
	ENABLE;

	/*
	 *	Pick all received packet buffers and put them on a chain-buffer.
	 */

	while (recvIdx != produceIdx) {
		/*
		 * Get the address, size and EtherType of the received frame and
		 * call the input functions of the input FBs.
		 */

		cBuff[chainIdx].pData = (BYTE *) pHwInst->RxDescr[recvIdx].packet;
		cBuff[chainIdx].size = (Uint16) (
			pHwInst->RxStatus[recvIdx].Info & NDRV__DESC_SIZE_MASK
		) + 1;
		cBuff[chainIdx].pBlockInfo = pHwInst->RxInfo[recvIdx].pUtil;
		chainIdx++;

		if (pHwInst->RxStatus[recvIdx].Info & NDRV__RXSTATUS_LAST) {
			ndif_InData data;

			/*
			 *	A full frame has been built on the chain buffer. Send the
			 *	frame to next layer.
			 */

			data.pData = cBuff;
			data.type =
				cBuff[0].pData[NDRV__ETH_TYPE_H] << 8 |
				cBuff[0].pData[NDRV__ETH_TYPE_L];
			data.count = (Uint8) chainIdx;

			ndrv__handleFrame(pInst, &data);

			recvIdx++;
			if (recvIdx == NDRV__RX_DESC_COUNT) {
				recvIdx = 0;
			}
			pHwInst->recvIdx = recvIdx;

			freeDescriptors += (Uint8) chainIdx;
			chainIdx = 0;

		} else {
			recvIdx++;
			if (recvIdx == NDRV__RX_DESC_COUNT) {
				recvIdx = 0;
			}
		}

		deb_assert(chainIdx != 13);
	}

	return(freeDescriptors);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwSetupRx
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Setup hardware to receive into supplied buffers.
*
*	\param		pInst	Pointer to FB instance.
*	\param		pCbuff	Pointer to chain-buffer.
*	\param		count	Number of chain buffer elements.
*
*	\details	This function must be called before data can be received. The
*				hardware will receive data into the buffers that are supplied.
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__hwSetupRx(
	ndrv__Inst * 			pInst,
	ndif_Cbuff *			pCbuff,
	Ufast8					count
) {
	Ufast8					cBuffIdx;
	ndrv__HwInst *			pHwInst;
	Uint32					rxConsumeIndex;

	pHwInst = (ndrv__HwInst *) (void *) pInst;

	DISABLE;
	rxConsumeIndex = pHwInst->hwEmac.RxConsumeIndex;
	ENABLE;

	/*
	 * 	Configure the buffers to the DMA RX descriptors
	 */

	for (cBuffIdx = 0; cBuffIdx < count; cBuffIdx++) {
		pHwInst->RxDescr[rxConsumeIndex].packet = (Uint32) pCbuff[cBuffIdx].pData;
		pHwInst->RxDescr[rxConsumeIndex].control =
				(pCbuff[cBuffIdx].size - 1) | NDRV__RX_DESC_INT;
		pHwInst->RxInfo[rxConsumeIndex].pUtil = pCbuff[cBuffIdx].pBlockInfo;

		rxConsumeIndex++;
		if (rxConsumeIndex == NDRV__RX_DESC_COUNT) {
			rxConsumeIndex = 0;
		}
	}

	DISABLE;
	pHwInst->buffersAdded = TRUE;
	pHwInst->hwEmac.RxConsumeIndex = rxConsumeIndex;
	ENABLE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwUpdFilter
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Update multicast address filter.
*
*	\param		pInst	Pointer to FB instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__hwUpdFilter(
	ndrv__Inst *			pInst
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwUpdLink
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Update link information.
*
*	\param		pInst	Pointer to FB instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__hwUpdLink(
	ndrv__Inst *			pInst
) {
	ndrv__HwInst *			pHwInst;
	enum link_adapter_event	linkEvent;

	pHwInst = (ndrv__HwInst *) pInst;

	if (pHwInst->initEvent != LINKEVENT_UNCHANGED) {
		linkEvent = pHwInst->initEvent;
		pHwInst->initEvent = LINKEVENT_UNCHANGED;
	} else {
		linkEvent = link_adapter(pHwInst->packetAdapter);
	}

	if (linkEvent != LINKEVENT_UNCHANGED) {
		ndif_Status status;
		ndrv_ProtHandler const_P * pProtHandler;

		if (linkEvent == LINKEVENT_UP) {
			status.flags = NDIF_STAT_LINKUP;
			pHwInst->flags |= NDRV__HWFLG_LINKUP;

		} else if (linkEvent == LINKEVENT_DOWN) {
			status.flags = NDIF_STAT_LINKDOWN;
			pHwInst->flags &= ~NDRV__HWFLG_LINKUP;
		}

		/*
		 * Call the status update function of the input FBs.
		 */

		pProtHandler = pInst->pInit->pProtHan;
		do {
			pProtHandler->pRxIf->pStatusFn(*pProtHandler->ppUtil, &status);

			pProtHandler++;
		} while (pProtHandler->ppUtil != 0);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwTxDone
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Frame transmitted.
*
*	\param		pInst	Pointer to NDRV instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__hwTxDone(
	ndrv__Inst *			pInst
) {
	/* 
	 * The TX done callback is called directly from the output function in the
	 * windows port. Nothing needs to be done here.
	 */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwPollThread
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Thread polling for incoming frames.
*
*	\param		pVoidInst	Pointer to NDRV instance.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__hwPollThread(
	void *					pVoidInst
) {
	ndrv__HwInst *			pInst;

	pInst = (ndrv__HwInst *) pVoidInst;

	FOREVER {
		update_adapter(pInst->packetAdapter);
		Sleep(0);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv__hwFrameInput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Process incoming frame.
*
*	\param		arg		Pointer to NDRV instance.
*	\param		frame	Pointer to incoming frame.
*	\param		len		Size of incoming frame in bytes.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void ndrv__hwFrameInput(
	void *					arg,
	void *					frame,
	int						len
) {
	ndrv__HwInst *			pHwInst;
	Uint32					startIdx;
	Uint32					produceIdx;
	BYTE *					pEthFrame;

	DISABLE;

	pHwInst = (ndrv__HwInst *) arg;

	if (len <= 0) {
		/* Don't know if this will ever happen but checking it anyway. */
		ENABLE;
		return;
	}

	pEthFrame = (BYTE *) frame;

	if (pHwInst->flags & NDRV__HWFLG_LINKUP) {

		/*
		 * If you are missing receive packets, they may be dropped
		 * because of invalid IP packet checksum.
		 *
		 * Make sure that NIC driver has all these disabled:
		 * - IPv4 offload = Disabled
		 * - IPv4 checksum offload = Disabled
		 * - etc.
		 * 
		 * It is not the MAC address filters here that eat those packets,
		 * the packets will be dropped in ntcpip__ipInput (ntcpip_ip.c:243).
		 */

		/*
		 *	Drop frames sent by this NIC
		 */

		if (!memcmp(&pEthFrame[NDRV__ETH_SRCADDR_0], pHwInst->macAddr, 6)) {
			ENABLE;
			return;
		}

		/*
		 *	Receive all multicast frames but only unicast frames with matching
		 *	address
		 */

		if (
			pEthFrame[NDRV__ETH_DSTADDR_0] & 0x01
			|| !memcmp(&pEthFrame[NDRV__ETH_DSTADDR_0], pHwInst->macAddr, 6)
		) {
			Uint32 readyIdx;

			startIdx = pHwInst->hwEmac.RxProduceIndex;
			produceIdx = startIdx;

			if (pHwInst->hwEmac.RxConsumeIndex > 0) {
				readyIdx = pHwInst->hwEmac.RxConsumeIndex - 1;
			} else {
				readyIdx = NDRV__RX_DESC_COUNT - 1;
			}

			while (
				len
				&& produceIdx != readyIdx
				&& pHwInst->buffersAdded == TRUE
			) {
				Uint32 buffLen;
				BYTE * pBuffer;

				buffLen = pHwInst->RxDescr[produceIdx].control & 0x3FF;
				pBuffer = (BYTE *) pHwInst->RxDescr[produceIdx].packet;

				buffLen = MIN((Uint32)len, buffLen);
				memcpy(pBuffer, pEthFrame, buffLen);
				len -= buffLen;
				pEthFrame += buffLen;

				pHwInst->RxStatus[produceIdx].Info = buffLen - 1;
				if (len == 0) {
					pHwInst->RxStatus[produceIdx].Info |= NDRV__RXSTATUS_LAST;
				}

				produceIdx++;
				if (produceIdx == NDRV__RX_DESC_COUNT) {
					produceIdx = 0;
				}
			}

			if (len != 0) {
				/* 
				 * RX FIFO overflow. The frame did not fit on the buffer. 
				 * Just drop it.
				 */

				produceIdx = startIdx;

			} else {
				pHwInst->hwEmac.RxProduceIndex = produceIdx;

				pHwInst->parent.triggers |= NDRV__TRG_RXTRIG;
				osa_semaSetIsr(&pHwInst->parent.taskSema);
			}
			
		}
	}

	ENABLE;
}
