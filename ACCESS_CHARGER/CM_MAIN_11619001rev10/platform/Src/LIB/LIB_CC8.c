/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		Implementation of copy compare function.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "lib.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_memCmpCpy8
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Copies and compares data.
*
*	\param		pTarget		Pointer to target buffer.
*	\param		pSource		Pointer to source buffer.
*	\param		bytes 		Number of BYTEs to be copied.
*
*	\return		Number of bytes that has been copied. If the source and target
*				buffers contain the same data then zero is returned.
*				
*	\details	The function will copy data between two buffers. The existing
*				data in the target buffer will also be compared to the data in
*				the source buffer.
*
*				Data may have been copied even when zero is returned.
*	\note	
*
*******************************************************************************/

PUBLIC Uint8 lib_memCmpCpy8(
	ptrs_RegData_bp				pTarget,
	ptrs_RegData_bcDp			pSource,
	Uint8						bytes
) {
	Ufast8 diff;
	Ufast8 bytesLeft;

	diff = 0U;
	bytesLeft = bytes;

	{
		Ufast8 source;
		Ufast8 oddBytes;

		oddBytes = bytesLeft & 0x03;
		bytesLeft -= oddBytes;

		switch (oddBytes) {
			case 3:
				source = *pSource++;
				diff = *pTarget ^ source;
				*pTarget++ = (BYTE) source;
			case 2:
				source = *pSource++;
				diff |= *pTarget ^ source;
				*pTarget++ = (BYTE) source;
			case 1:
				source = *pSource++;
				diff |= *pTarget ^ source;
				*pTarget++ = (BYTE) source;
				break;
		}
	}

	while (bytesLeft) {
		Ufast8 source;

		source = *pSource++;
		diff |= pTarget[0] ^ source;
		pTarget[0] = (BYTE) source;

		source = *pSource++;
		diff |= pTarget[1] ^ source;
		pTarget[1] = (BYTE) source;

		source = *pSource++;
		diff |= pTarget[2] ^ source;
		pTarget[2] = (BYTE) source;

		source = *pSource++;
		diff |= pTarget[3] ^ source;
		pTarget[3] = (BYTE) source;

		pTarget += 4U;
		bytesLeft -= 4U;
	}

	return(diff ? bytes : 0);
}
