/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		Small CRC-32 implementation.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "lib.h"
#include "deb.h"

#include "LIB_CRC.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint32 lib__crc32EthernetSmall(Uint32,BYTE const_D *,Uint16);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Interface for small CRC-32 calculation.
 */

PUBLIC lib_Crc32If const_P lib_crc32EthSmallIf = {
	&lib__crc32EthernetInit,
	&lib__crc32EthernetSmall,
	&lib__crc32EthernetGet
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_crc32EthernetSmall
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate 32-bit Ethernet CRC.
*
*	\param		crc		Starting value.
*	\param		pData	Pointer to data to calculate CRC from.
*	\param		bytes	Size of the data.
*
*	\return		The calculated CRC.
*
*	\details	There are two versions of the Ethernet CRC calculation
*				functions. This one takes up less code memory but it is slower.
*
*	\note
*	
*	\see		lib_crc32Ethernet
*
*******************************************************************************/

PRIVATE Uint32 lib__crc32EthernetSmall(
	Uint32					crc,
	BYTE const_D *			pData,
	Uint16					bytes
) {
	Ufast16 				bytesLeft;

	deb_assert(bytes != 0);
	deb_assert(pData != NULL);

	bytesLeft = bytes;

	do {
		Ufast8 bits;
		Uint32 tableValue;
		
		tableValue = (crc ^ *pData++) & 0xFFU;

		bits = 8;
		do {
			if (tableValue & 0x01U) {
				tableValue >>= 1;
				tableValue ^= 0xEDB88320;
			} else {
				tableValue >>= 1;
			}
		} while (--bits);

		crc = tableValue ^ (crc >> 8U);
	} while (--bytesLeft);

	return crc;
}
