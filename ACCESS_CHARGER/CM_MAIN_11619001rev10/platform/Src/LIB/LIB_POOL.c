/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		Memory pool handling.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 4423 $ \n
*				\$Date: 2021-01-13 17:08:06 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "lib.h"
#include "mem.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_poolInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocates and initialize the memory pool.
*
*	\param		pPool		Pointer to the memory pool structure.
*	\param		ppCategory	Pointer to the memory category that the memory pool
*							should be allocated from.
*	\param		blockSize	The desired block size.
*	\param		blockCount	The desired number of blocks.
*
*	\details	Allocates memory for the memory pool and initializes the pool
*				structure.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void lib_poolInit(
	lib_MemPool *			pPool,
	void **					ppCategory,
	size_t					blockSize,
	Uint8					blockCount
) {
	lib_PoolInit poolInit;

	poolInit.ppCategory = ppCategory;
	poolInit.blockSize = blockSize;
	poolInit.blockCount = blockCount;
	poolInit.pInitCbFn = NULL;

	lib_poolInitCb(pPool, &poolInit);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_poolInitCb
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocates and initialize the memory pool.
*
*	\param		pPool		Pointer to the memory pool structure.
*	\param		pInit		Pointer to memory pool initializtion values.
*
*	\details	Allocates memory for the memory pool and initializes the pool
*				structure. A callback function is called for each block in the
*				memory pool so that the block can be initialized.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void lib_poolInitCb(
	lib_MemPool *			pPool,
	lib_PoolInit *			pInit
) {
	size_t blockSize;

	deb_assert(pInit->blockSize != 0);
	deb_assert(pInit->blockCount != 0);

	lib_fifoInit(pPool);

	/*
	 *	The block size must be padded to get proper alignment for each block.
	 */

	blockSize = (pInit->blockSize + TARGET_MEM_ALIGN - 1) & ~(TARGET_MEM_ALIGN - 1);
	
	{
		Ufast8 blocksLeft;
		BYTE * pBuffer;

		blocksLeft = pInit->blockCount;

		pBuffer = mem_reserve(pInit->ppCategory, blockSize * blocksLeft);
		deb_assert(pBuffer != NULL);

		do {
			/*
			 *	The resulting pointer to pointer should always be properly
			 *	aligned here because blockSize is increased to the target
			 *	alignment.
			 */

			if (pInit->pInitCbFn != NULL) {
				/*
				 *	Initialize each object if an init function has been
				 *	provided.
				 */
				pInit->pInitCbFn((void *) pBuffer);
			}

			lib_fifoPut(pPool, (void **) (void *) pBuffer);

			pBuffer += blockSize;
		} while (--blocksLeft);
	}
}
