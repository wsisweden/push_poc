/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		LIB_CRDS.c
*
*	\ingroup	LIB
*
*	\brief		Small CRC-8/MAXIM implementation.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "lib.h"
#include "deb.h"

#include "LIB_CRC.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint8 lib__crc8DallasMaximSmall(Uint8,BYTE const_D *,Uint16);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Interface for small CRC-8/MAXIM "Dow" CRC calculation.
 */

PUBLIC lib_Crc8If const_P lib_crc8MaximSmallIf = {
	&lib__crc32MaximInit,
	&lib__crc8DallasMaximSmall,
	&lib__crc32MaximGet
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib__crc8DallasMaximSmall
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate CRC-8/MAXIM "Dow" CRC. 
*
*	\param		crc		Starting CRC value.
*	\param		pData	Pointer to data to calculate CRC from.
*	\param		bytes	Size of data.
*
*   \return		The calculated CRC.
*
*	\details	This function calculates the CRC without any pre-calculated
*				table to make the code size as small as possible.
*	
*	\see		lib_crc8DallasMaxim
*
*******************************************************************************/

PRIVATE Uint8 lib__crc8DallasMaximSmall(
	Uint8					crc,
	BYTE const_D *			pData,
	Uint16					bytes
) {
	Ufast16 				bytesLeft;

	deb_assert(bytes != 0);
	deb_assert(pData != NULL);

	bytesLeft = bytes;

	do {
		Ufast8	ii;
		Uint8	dataByte;

		dataByte = *pData++;

		for (ii = 0; ii < 8; ii++) {
			Uint8 fb_bit;

			fb_bit = ((dataByte >> ii) ^ crc) & 0x01;

			crc = crc >> 1;

			if (fb_bit) {
				crc = crc ^ 0x8c;
			}
		}

	} while (--bytesLeft);

	return crc;
}
