/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2007, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		Local CRC calculation declarations.
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

#ifndef LIB_CRC_H_INCLUDED
#define LIB_CRC_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

Uint32 lib__crc32EthernetInit(void);
Uint32 lib__crc32EthernetGet(Uint32);

Uint8 lib__crc32MaximInit(void);
Uint8 lib__crc32MaximGet(Uint8);

Uint16 lib__crc16InitKermit(void);
Uint16 lib__crc16GetKermit(Uint16);

Uint16 lib__crc16InitMcrf4(void);
Uint16 lib__crc16GetMcrf4(Uint16);

Uint16 lib__crcUpd1021FastRev(Uint16,BYTE const_D *,Uint16);
Uint16 lib__crcUpd1021SmallRev(Uint16,BYTE const_D *,Uint16);
Uint16 lib__crcUpd1021HwRev(Uint16,BYTE const_D *,Uint16);

Uint16 lib__crcUpd1021Fast(Uint16,BYTE const_D *,Uint16);
Uint16 lib__crcUpd1021Small(Uint16,BYTE const_D *,Uint16);

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/******************************************************//** \endcond pub_decl */

#endif
