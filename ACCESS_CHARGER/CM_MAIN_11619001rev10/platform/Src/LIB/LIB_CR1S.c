/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		Small reverse CRC calculation with the 0x1021 polynomial.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "lib.h"
#include "deb.h"

#include "LIB_CRC.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib__crcUpd1021SmallRev
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Small reverse CRC calculation with the 0x1021 polynomial.
*
*	\param		crc		Starting value.
*	\param		pData	Pointer to data to calculate CRC from.
*	\param		bytes	Size of the data.
*
*	\return		The calculated CRC.
*
*	\details	There are two versions of the calculation functions. This one is
*				faster but it uses more code memory because of a large
*				pre-calculated table.
*				
*				The CRC implemented in this function is used for KERMIT, X-25,
*				CRC-A, CRC-16/TMS37157, CRC-16/RIELLO and CRC-16/MCRF4XX
*
*	\note
*	
*******************************************************************************/

PUBLIC Uint16 lib__crcUpd1021SmallRev(
	Uint16 					crc,
	BYTE const_D *			pData,
	Uint16					bytes
) {
	Ufast16 				bytesLeft;

	deb_assert(bytes != 0);
	deb_assert(pData != NULL);

	bytesLeft = bytes;

	do {
		Ufast8 bits;
		Uint16 c;

		c = *pData++;

		bits = 8;
		do {
			if ((crc ^ c) & 0x0001U) {
				crc = (crc >> 1) ^ 0x8408U;
			} else {
				crc = crc >> 1;
			}

			c = c >> 1;
		} while (--bits);
	} while (--bytesLeft);

	return crc;
}
