/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		Average filter implementation.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3986 $ \n
*				\$Date: 2019-11-07 16:25:05 +0200 (to, 07 marras 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E headers */
#include "tools.h"
#include "deb.h"
#include "lib.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_favgInit32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the 32-bit average filter.
*
*	\param		pFilter		Pointer to filter state information.
*	\param		initValue	The initial average value.
*	\param		valueCount	Number of elements in the value array.
*
*	\details	The size of the value array determines how many values will
*				be included in the filter.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void lib_favgInit32(
	lib_FavgInfo *			pFilter,
	Uint32					initValue,
	Uint16					valueCount
) {
	deb_assert(pFilter != NULL);

	pFilter->maxCount = valueCount;
	pFilter->counter = valueCount;
	pFilter->currSum = 0;
	pFilter->sum = initValue * valueCount;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_favgAddValue32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Add a new value to the 32-bit average filter.
*
*	\param		pFilter		Pointer to filter state information.
*	\param		newValue	The value that should be added.
*
*	\retval		TRUE		A new average value can be calculated.
*	\retval		FALSE		No new average value can be calculated yet.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Boolean lib_favgAddValue32(
	lib_FavgInfo *			pFilter,
	Uint32					newValue
) {
	Boolean newAverage;

	deb_assert(pFilter != NULL);

	/*
	 *	Adding each value to the sum.
	 */

	pFilter->currSum += newValue;

	newAverage = FALSE;
	if (--pFilter->counter == 0) {
		/*
		 *	The specified amount of values has been added to the sum. Store
		 *	the calculated sum so that the average value can be calculated
		 *	later.
		 *
		 *	The current sum is then reset so that new samples can be added.
		 */

		pFilter->counter = pFilter->maxCount;
		pFilter->sum = pFilter->currSum;
		pFilter->currSum = 0;

		newAverage = TRUE;
	}

	return(newAverage);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_favgGet32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get current output value of the filter.
*
*	\param		pFilter		Pointer to filter state information.
*
*	\return		Current output value (average of added values).
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Uint32 lib_favgGet32(
	lib_FavgInfo *			pFilter
) {
	deb_assert(pFilter != NULL);

	/*
	 *	Adding half of maxCount before division for proper rounding.
	 */

	return(
		(Uint32) (
			(pFilter->sum + (pFilter->maxCount >> 1))
			/ pFilter->maxCount
		)
	);
}
