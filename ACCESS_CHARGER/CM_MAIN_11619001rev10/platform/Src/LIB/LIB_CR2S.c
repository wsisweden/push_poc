/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		Small CRC calculation with the 0x1021 polynomial.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "TOOLS.H"
#include "lib.h"
#include "deb.h"

#include "LIB_CRC.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib__crcUpd1021Small
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Small CRC calculation with the 0x1021 polynomial.
*
*	\param		crc		Starting value.
*	\param		pData	Pointer to data to calculate CRC from.
*	\param		bytes	Size of the data.
*
*	\return		The calculated CRC.
*
*	\details	There are two versions of the calculation functions. This one is
*				smaller but it is not as fast as the table based version.
*				
*				The CRC implemented in this function is used for
*				CRC-16/AUG-CCITT, CRC-16/CCITT-FALSE, CRC-16/GENIBUS and XMODEM.
*
*	\note
*	
*******************************************************************************/

PUBLIC Uint16 lib__crcUpd1021Small(
	Uint16 					crc,
	BYTE const_D *			pData,
	Uint16					bytes
) {
	Ufast16 				bytesLeft;

	deb_assert(bytes != 0);
	deb_assert(pData != NULL);

	bytesLeft = bytes;

	do {
		Ufast8 bits;

		crc = crc ^ ((Uint16)*pData++ << 8);

		bits = 8;
		do {
			if (crc & 0x8000U) {
				crc = (crc << 1) ^ 0x1021U;
			} else {
				crc <<= 1;
			}
		} while (--bits);
	} while (--bytesLeft);

	return crc;
}
