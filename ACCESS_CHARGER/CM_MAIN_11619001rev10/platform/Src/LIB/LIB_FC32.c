/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		32-bit Combo average filter implementation.
*
*	\details	This file implements a filter that combines the 32-bit basic 
*				average filter with the 32-bit simple moving average filter. 
*
*				The advantage from this two stage filter is that the balance 
*				between RAM usage and latency can be adjusted.
*
*				RAM memory can be saved by adding a large portion of the samples
*				to the basic average filter. latency can be improved by adding
*				a large portion of the samples to the simple moving average.
*
*	\note		
*
*	\version	\$Rev: 3986 $ \n
*				\$Date: 2019-11-07 16:25:05 +0200 (to, 07 marras 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E headers */
#include "tools.h"
#include "deb.h"
#include "lib.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_fcombInit32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the 32-bit combo average filter.
*
*	\param		pFilter		Pointer to filter state information.
*	\param		initValue	The initial average value.
*	\param		pArray		Sample buffer for SMA filter. The buffer must
*							contain valueCount2 elements.
*	\param		valueCount1	Number of samples for stage 1 (basic average).
*	\param		valueCount2	Number of samples for stage 2 (SMA).
*
*	\details	The total amount of samples used for average calculation is
*				valueCount1 * valueCount2.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void lib_fcombInit32(
	lib_FcombInfo *			pFilter,
	Uint32					initValue,
	Uint32 *				pArray,
	Uint16					valueCount1,
	Uint16					valueCount2
) {
	deb_assert(pFilter != NULL);
	deb_assert(pArray != NULL);

	/*
	 *	Initialize both filtering stages.
	 */

	lib_favgInit32(&pFilter->stage1, initValue, valueCount1);
	lib_fsmaInit32(&pFilter->stage2, initValue, pArray, valueCount2);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_fcombAddValue32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Add a new value to the filter.
*
*	\param		pFilter		Pointer to filter state information.
*	\param		newValue	The value that should be added.
*
*	\retval		TRUE		A new average value can be calculated.
*	\retval		FALSE		No new average value can be calculated yet.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Boolean lib_fcombAddValue32(
	lib_FcombInfo *			pFilter,
	Uint32					newValue
) {
	Boolean newAverage;

	deb_assert(pFilter != NULL);

	/*
	 *	Filtering is done in two stages.
	 *
	 *	The first stage adds samples to a sum and calculates a new average
	 *	when a specified amount of samples has been added. The frequency at
	 *	which new averages are generated depends on the configured sample
	 *	count for the average. This stage can be bypassed by configuring the
	 *	1st stage sample count to one.
	 */

	newAverage = lib_favgAddValue32(&pFilter->stage1, newValue);
	if (newAverage) {
		Uint32 stage1Sum;

		/*
		 *	A new value is available from the basic average filter.
		 *
		 *	The second stage performs simple moving average filtering on the
		 *	values from the first stage. The simple moving average filter
		 *	outputs a new average each time a value is added.
		 */

		stage1Sum = lib_favgGetSum32(&pFilter->stage1);
		lib_fsmaAddValue32(&pFilter->stage2, stage1Sum);
	}

	return(newAverage);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_fcombGet32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get current output value of the filter.
*
*	\param		pFilter		Pointer to filter state information.
*
*	\return		Current output value (average of added values).
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Uint32 lib_fcombGet32(
	lib_FcombInfo *			pFilter
) {
	Uint32 sum;
	Uint32 count;

	deb_assert(pFilter != NULL);

	/*
	 *	Get the sum from the second stage.
	 */

	sum = lib_fsmaGetSum32(&pFilter->stage2);

	/*
	 *	Get the combined number of samples.
	 */

	count =
		(Uint32)lib_fsmaGetCount32(&pFilter->stage2)
		* (Uint32)lib_favgGetCount32(&pFilter->stage1);

	/*
	 *	Add half of the divisor for proper rounding.
	 */

	return((sum + (count>>1)) / count);
}
