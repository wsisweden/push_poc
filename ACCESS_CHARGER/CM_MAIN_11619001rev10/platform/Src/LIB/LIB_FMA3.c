/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	LIB
*
*	\brief		32-bit simple moving average filter implementation.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3986 $ \n
*				\$Date: 2019-11-07 16:25:05 +0200 (to, 07 marras 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E headers */
#include "tools.h"
#include "deb.h"
#include "lib.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_fsmaInit32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the simple moving average filter.
*
*	\param		pFilter		Pointer to filter state information.
*	\param		initValue	The initial average value.
*	\param		pArray		Pointer to the value array.
*	\param		valueCount	Number of elements in the value array.
*
*	\details	The size of the value array determines how many values will
*				be included in the filter.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void lib_fsmaInit32(
	lib_FsmaInfo32 *		pFilter,
	Uint32					initValue,
	Uint32 *				pArray,
	Uint16					valueCount
) {
	Ufast16 idx;

	deb_assert(pFilter != NULL);
	deb_assert(pArray != NULL);

	pFilter->maxCount = valueCount;
	pFilter->nextIdx = 0;
	pFilter->pValues = pArray;
	pFilter->sum = initValue * valueCount;

	idx = 0;
	do {
		pArray[idx] = initValue;
	} while (++idx < valueCount);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_fsmaAddValue32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Add a new value to the filter calculation.
*
*	\param		pFilter		Pointer to filter state information.
*	\param		newValue	The value that should be added.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Boolean lib_fsmaAddValue32(
	lib_FsmaInfo32 *		pFilter,
	Uint32					newValue
) {
	Uint32 * pValue;

	deb_assert(pFilter != NULL);

	/*
	 *	Get pointer to oldest value.
	 */

	pValue = &pFilter->pValues[pFilter->nextIdx];

	/*
	 *	Replace the oldest value.
	 */

	pFilter->sum -= *pValue;
	*pValue = newValue;
	pFilter->sum += newValue;

	/*
	 *	Move to the next value.
	 */

	if (++pFilter->nextIdx >= pFilter->maxCount) {
		pFilter->nextIdx = 0;
	}

	return(TRUE);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_fsmaGet32
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get current output value of the filter.
*
*	\param		pFilter		Pointer to filter state information.
*
*	\return		Current output value (average of added values).
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC Uint32 lib_fsmaGet32(
	lib_FsmaInfo32 *		pFilter
) {
	deb_assert(pFilter != NULL);

	/*
	 *	Adding half of valueCount before division for proper rounding.
	 */

	return(
		(Uint32) (
			(pFilter->sum + (pFilter->maxCount >> 1))
			/ pFilter->maxCount
		)
	);
}
