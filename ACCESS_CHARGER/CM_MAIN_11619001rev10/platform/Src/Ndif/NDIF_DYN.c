/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	NDIF
*
*	\brief		Dynamic buffer handler.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "ndif.h"
#include "mem.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type for dynamic buffer handling instance.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16				blockSize;		/**< Size of buffer blocks.			*/
} ndif__DynamicHandler;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void * ndif__dynamicInit(Uint16 blockSize,Uint8 blockCount);
PRIVATE Uint8 ndif__dynamicAlloc(void * pInst, ndif_Cbuff * pData,Uint8 count);
PRIVATE void ndif__dynamicFree(void * pInst,void * pUtil);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface constant for memory handler that uses MEM FB dynamic memory.
 * This memory handler may be used if the network layer protocols don't need an
 * own buffer format. For example the NETIF function block that handles the IP
 * protocol needs an own protocol handler.
 */

PUBLIC ndif_BuffHanIf const_P ndif_dynamicMemHandler = {
	&ndif__dynamicInit,
	&ndif__dynamicAlloc,
	&ndif__dynamicFree
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/

PRIVATE void * ndif__dynamicInit(
	Uint16					blockSize,
	Uint8					blockCount
) {
	ndif__DynamicHandler * pInst;

	pInst = (ndif__DynamicHandler *) mem_reserve(
		&mem_normal,
		sizeof(ndif__DynamicHandler)
	);

	pInst->blockSize = blockSize;

	return pInst;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndif__dynamicAlloc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate buffer blocks.
*
*	\param		pVoidInst	Pointer to memory handler instance.
*	\param		pData		Pointer to array of buffer block information.
*	\param		count		Number of blocks in the array.
*
*   \return		Number of blocks that could be allocated.
*
*	\details	
*
*******************************************************************************/

PRIVATE Uint8 ndif__dynamicAlloc(
	void *					pVoidInst,
	ndif_Cbuff *			pData,
	Uint8					count
) {
	Ufast8					buffCount;
	Uint16					blockSize;

	blockSize = ((ndif__DynamicHandler *) pVoidInst)->blockSize;
	buffCount = count;
	do {
		Uint16 currBuffSize;

		currBuffSize = blockSize;
		pData->pData = mem_alloc(&mem_normal, currBuffSize);

		if (pData->pData == NULL) {
			currBuffSize >>= 1;
			pData->pData = mem_alloc(&mem_normal, currBuffSize);
			
			if (pData->pData == NULL) {
				break;
			}
		}

		pData->size = currBuffSize;
		pData->pBlockInfo = pData->pData;

		pData++;
	} while (--buffCount);

	return (Uint8) (count - buffCount);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndif__dynamicFree
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Frees the buffer block.
*
*	\param		pVoidInst	Pointer to memory handler instance.
*	\param		pBlockInfo	Pointer to data needed to free the buffer block.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PRIVATE void ndif__dynamicFree(
	void *					pVoidInst,
	void *					pBlockInfo
) {
	mem_free(pBlockInfo);
}
