/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NDIF_POO.c
*
*	\ingroup	NDIF
*
*	\brief		Memory pool buffer handler.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "ndif.h"
#include "mem.h"
#include "lib.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 *	Type for buffer pool handling instance.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16				blockSize;		/**< Size of buffer blocks.			*/
	lib_MemPool *		pMemPool;		/**< The memory pool.				*/
} ndif__PoolHandler;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void * ndif__poolInit(Uint16 blockSize,Uint8 blockCount);
PRIVATE Uint8 ndif__poolAlloc(void * pInst, ndif_Cbuff * pData,Uint8 count);
PRIVATE void ndif__poolFree(void * pInst,void * pUtil);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface constant for memory handler that uses pre-allocated memory
 * pool. This memory handler may be used if the network layer protocols don't
 * need an own buffer format. For example the NETIF function block that handles
 * the IP and ARP protocols needs an own buffer handler.
 */

PUBLIC ndif_BuffHanIf const_P ndif_poolMemHandler = {
	&ndif__poolInit,
	&ndif__poolAlloc,
	&ndif__poolFree
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndif__poolInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize buffer pool handler.
*
*	\param		blockSize	Size of memory blocks.
*	\param		blockCount	Number of memory blocks.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PRIVATE void * ndif__poolInit(
	Uint16					blockSize,
	Uint8					blockCount
) {
	ndif__PoolHandler *		pBufferHandler;

	pBufferHandler = mem_reserve(&mem_normal, sizeof(ndif__PoolHandler));
	lib_poolInit(pBufferHandler->pMemPool, &mem_normal, blockSize, blockCount);
	
	return(pBufferHandler);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndif__poolAlloc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Allocate buffer blocks.
*
*	\param		pVoidInst	Pointer to memory handler instance.
*	\param		pData		Pointer to array of buffer block information.
*	\param		count		Number of blocks in the array.
*
*   \return		Number of blocks that could be allocated.
*
*	\details	
*
*******************************************************************************/

PRIVATE Uint8 ndif__poolAlloc(
	void *					pVoidInst,
	ndif_Cbuff *			pData,
	Uint8					count
) {
	Ufast8					buffCount;
	ndif__PoolHandler *		pInst;

	pInst = (ndif__PoolHandler *) pVoidInst;

	buffCount = count;
	do {
		BYTE * pBlock;

		pBlock = lib_poolAlloc(pInst->pMemPool);
		if (pBlock == NULL) {
			break;
		}

		pData->pData = pBlock;
		pData->size = pInst->blockSize;
		pData->pBlockInfo = pBlock;

		pData++;
	} while (--buffCount);

	return (Uint8) (count - buffCount);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndif__poolFree
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Frees the buffer block.
*
*	\param		pVoidInst	Pointer to memory handler instance.
*	\param		pBlockInfo	Pointer to data needed to free the buffer block.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PRIVATE void ndif__poolFree(
	void *					pInst,
	void *					pBlockInfo
) {
	lib_poolFree(((ndif__PoolHandler *) pInst)->pMemPool, pBlockInfo);
}
