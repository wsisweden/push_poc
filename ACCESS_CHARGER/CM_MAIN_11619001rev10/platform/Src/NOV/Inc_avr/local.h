/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NOV\Inc_avr\local.h
*
*	\ingroup	NOV_AVR
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	1.6.2004   15:42
*
*******************************************************************************/

#if (TARGET_SELECTED == TARGET_W32)
#define HW_EEPROM			512
#endif

/*-----------------------------------------------------------------------------
 *
 *	Includes.
 *
 *---------------------------------------------------------------------------*/

#include "tools.h"
#include "osa.h"
#include "nov.h"

/*-----------------------------------------------------------------------------
 *
 *	Defines.
 *
 *---------------------------------------------------------------------------*/

#define NOV_CHK_UNEVEN		1
 
 
#define NOV_BEGIN_ADDR		0
#define NOV_ERASE_CHAR		0x00

/*
 *	TODO:
 *	-	bank size / etc should depend on the nov size.
 */
#define NOV_SIZE			HW_EEPROM			/**< Size of eeprom         */
#define NOV_CHK_BLOCK		32					/**< Size of checksum block */
#define NOV_BANK_SIZE		241					/**< Size of a bank         */
/*
 *	Number of checksum bytes.
 */
#define NOV_CHK_SUMS														\
((NOV_BANK_SIZE / NOV_CHK_BLOCK) + (NOV_BANK_SIZE % NOV_CHK_BLOCK ? 1 : 0))

/*
 *	Start offsets of banks, checksum bytes and validity counter.
 */
#define NOV_BANK0_START		(0)		
#define NOV_BANK1_START		(NOV_BANK0_START + NOV_BANK_SIZE)
#define NOV_CHK_BANK0_START	(NOV_BANK1_START + NOV_BANK_SIZE)			
#define NOV_CHK_BANK1_START	(NOV_CHK_BANK0_START + NOV_CHK_SUMS)	
#define NOV_VLD_START		(NOV_CHK_BANK1_START + NOV_CHK_SUMS)
#define NOV_VLD_SW_SIZE		(NOV_SIZE - NOV_VLD_START)
#define NOV_VLD_SW_IDX_MAX	(NOV_VLD_SW_SIZE - 1)

/*
 *	Bits of nFlag.
 */
#define NOV_FLG_BANK1		(1<<0)	/**< Active bank bit                    */
#define NOV_FLG_COPYING		(1<<1)	/**< Currently copying                  */
#define NOV_FLG_WRITING		(1<<2)	/**< Currently writing                  */
#define NOV_FLG_BSWITCH		(1<<3)	/**< Bank needs switching               */
#define NOV_FLG_CHKUPD		(1<<4)	/**< Checksum byte needs reading        */
#define NOV_FLG_WRCNT		(1<<5)	/**< Validity counter needs to be written*/
#define NOV_FLG_WR_STOP		(1<<6)	/**< Validity counter needs to be written*/


#define NOV_FLG_MASK		0xFE	/**< Mask of flag bits                  */

/*
 *	Bits of validity counter
 */
#define NOV_CNT_FLG_B0		0x40	/**< Bank 0 validity                    */
#define NOV_CNT_FLG_B1		0x80	/**< Bank 1 validity                    */
#define NOV_CNT_STATE_MASK	(NOV_CNT_FLG_B0|NOV_CNT_FLG_B1)	/**< Validity bits*/
#define NOV_CNT_MAX			((BYTE)0x3F)	/**< Counter bits mask          */

#define NOV_IFLG_ERROR		(1 << 1)
#define NOV_IFLG_FATAL		(1 << 2)
#define NOV_IFLG_CMP1		(1 << 3)
#define NOV_IFLG_CMP2		(1 << 4)
#define NOV_IFLG_CMP3		(1 << 5)

#define NOV_IFLG_CMP		(NOV_IFLG_CMP1|NOV_IFLG_CMP2|NOV_IFLG_CMP3)

/*-----------------------------------------------------------------------------
 *
 *	Types.
 *
 *---------------------------------------------------------------------------*/

/*
 *	TODO:
 *	-	max bank size depends on eeprom size:
 *		-	ee_size <= 512 => 8 bit, else 16
 */

#if HW_EEPROM <= 512
typedef Uint16				nov__AddrSize;
typedef Uint8				nov__OffsSize;
#else
typedef Uint16				nov__AddrSize;
typedef Uint16				nov__OffsSize;
#endif

/*
 *	NOV structure.
 */
typedef struct {
	nov_Info_st *			pNext;			/**< Next write info            */
	nov__AddrSize			nCurCnt;		/**< Bytes to write             */

	Uint8					nFlags;			/**< State flags                */

	Uint8					nChk;			/**< Chksum counter             */
	nov__AddrSize			nChkBlock;		/**< Current chksum block       */
	nov__AddrSize			nAddr;			/**< Current write address      */

	Uint8					nValidCnt;		/**< Bank validity counter      */
	Uint8					nVldIdx;		/**< Index of validity counter  */

	osa_SemaType			semRead;		/**< Sema for reads             */
} nov_Instance_st;

/*-----------------------------------------------------------------------------
 *
 *	Macros.
 *
 *---------------------------------------------------------------------------*/

#define nov__setFlags(f_)	(nov__instance.nFlags |= (f_))
#define nov__chkFlags(f_)	(nov__instance.nFlags & (f_))
#define nov__clrFlags(f_)	(nov__instance.nFlags &= ~(f_))


#define nov__addrToByte0(a_)	(BYTE)((a_) > 1 ? (a_) - 2 : (NOV_VLD_SW_IDX_MAX - 1) + (a_))
#define nov__addrToByte1(a_)	(BYTE)((a_) ? (a_) - 1 : NOV_VLD_SW_IDX_MAX)
#define nov__addrToByte2(a_)	(BYTE)(a_)

#define nov__vldToCnt(v_)	((v_) & NOV_CNT_MAX)

#define nov__vldBank0(v_)	((v_) & NOV_CNT_FLG_B0)
#define nov__vldBank1(v_)	((v_) & NOV_CNT_FLG_B1)
#define nov__vldBankX(v_)	((v_) & (NOV_CNT_FLG_B0|NOV_CNT_FLG_B1))


/*
 *	Bank handling macros.
 *	nov__switchBank = switches bank from 0 to 1 or vice versa.
 *	nov__curBank	= return current bank (0 or 1)
 *  nov__revBank	= return not-current bank (0 or 1)
 */
#define nov__switchBank()	(nov__instance.nFlags ^= NOV_FLG_BANK1)
#define nov__curBank()		(nov__instance.nFlags & NOV_FLG_BANK1)
#define nov__revBank()		(nov__curBank() ^ NOV_FLG_BANK1)

/*
 *	Set, check and clear flags from nov__instance.nFlags.
 */
#define nov__setFlags(f_)	(nov__instance.nFlags |= (f_))
#define nov__chkFlags(f_)	(nov__instance.nFlags & (f_))
#define nov__clrFlags(f_)	(nov__instance.nFlags &= ~(f_))

/*
 *	Sets write flags.
 */
#define nov__isrStartWriting()												\
	{																		\
		nov__clrFlags(NOV_FLG_COPYING);										\
		nov__setFlags(NOV_FLG_WRITING|NOV_FLG_CHKUPD);						\
	}

/*
 *	Sets copy flags.
 */
#define nov__isrStartCopying()												\
	{																		\
		nov__clrFlags(NOV_FLG_WRITING);										\
		nov__setFlags(NOV_FLG_COPYING|NOV_FLG_BSWITCH);						\
	}

#define nov__chkBlockStart(a_)	(!((a_) & (NOV_CHK_BLOCK - 1)))


#define nov__chkCurToRevAddr(a_)											\
	(nov__curBank() ? (a_) - NOV_CHK_SUMS : (a_) + NOV_CHK_SUMS)


#define nov__chkIdxFromOffs(a_)	((a_) / NOV_CHK_BLOCK)
#define nov__chkFromCurOffs(a_)												\
	(NOV_CHK_BANK0_START + ((a_) / NOV_CHK_BLOCK) + (nov__curBank() * NOV_CHK_SUMS))
#define nov__chkFromRevOffs(a_)												\
	(NOV_CHK_BANK0_START + ((a_) / NOV_CHK_BLOCK) + (nov__revBank() * NOV_CHK_SUMS))


#define nov__offsToCurAddr(a_)												\
	((a_) + (nov__curBank() * NOV_BANK_SIZE))

#define nov__offsToRevAddr(a_)												\
	((a_) + (nov__revBank() * NOV_BANK_SIZE))



#if (TARGET_SELECTED & TARGET_AVR)

/*
 *	Read byte from address a_ and put it into place pointed by ptr b_
 */
#define nov_readByte(a_,b_)													\
	{																		\
		EEAR = (a_);														\
		EECR |= LBIT(EERE);													\
		*(b_) = EEDR;														\
	}

/*
 *	Write byte b_ to address a_.
 */
#define nov_writeByte(a_,b_)												\
	{																		\
		EEAR = a_;															\
		EEDR = b_;															\
		EECR |= LBIT(EEMWE);												\
		EECR |= LBIT(EEWE);													\
	}

/*
 *	Set eeprom ready isr either on or off.
 */
#define nov__readyIsrOn()	(EECR |= LBIT(EERIE))
#define nov__readyIsrOff()	(EECR &= ~LBIT(EERIE))
#define nov__writing()		(EECR & LBIT(EEWE))


#elif (TARGET_SELECTED == TARGET_W32)
#include <windows.h>
extern BYTE					eeprom[HW_EEPROM];
extern HANDLE				g_isrThread;
extern BOOL					g_suspend;


#define nov_readByte(a_,b_)		(*(b_) = eeprom[a_])
#define nov_writeByte(a_,b_)	(eeprom[a_] = b_)

#define nov__readyIsrOn()	(ResumeThread(g_isrThread))
#define nov__readyIsrOff()	(g_suspend = TRUE)
#define nov__writing()		(FALSE)


#else
# error	"Unsupported target type."
#endif

/*-----------------------------------------------------------------------------
 *
 *	Globals.
 *
 *---------------------------------------------------------------------------*/

extern nov_Instance_st		nov__instance;

/*-----------------------------------------------------------------------------
 *
 *	Locals.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Functions.
 *
 *---------------------------------------------------------------------------*/
