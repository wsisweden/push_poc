/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file
*
*	\ingroup	SNMPA
*
*	\brief		SNMPA initialization declarations.
*
*	\details
*
*	\note
*
*	\version
*
*******************************************************************************/

#ifndef SNMPA_INIT_H_INCLUDED
#define SNMPA_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

#include "tools.h"
#include "snmpa.h"
#include "pod.h"

//#define snmpa_reset		dummy_reset
#define snmpa_down		dummy_down
#define snmpa_read		dummy_read
#define snmpa_write		dummy_write
#define snmpa_ctrl		dummy_ctrl
#define snmpa_test		dummy_test

/**
 * Initialization parameters for the SNMP FB.
 */

typedef struct {
	/**
	 * Pointer to the POD.
	 */
	pod_Pod const_P *		pPod;

	/**
	 * Handle of a register containing a textual description of the device. This
	 * value should include the full name and version identification of the
	 * system's hardware type, software operating-system, and networking
	 * software.
	 */
	sys_RegHandle			sysDescr;
} snmpa_Init;

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_SNMPA
#elif defined(EXE_GEN_SNMPA)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_SNMPA
#endif

#define EXE_APPL(n)			n##snmpa

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

//#define EXE_USE_NN /**< Non-volatile numeric */
#define EXE_USE_NS /* Non-volatile string	*/
//#define EXE_USE_VN /* Volatile numeric		*/
//#define EXE_USE_VS /* Volatile string		*/
//#define EXE_USE_PN /* Process-point numeric	*/
//#define EXE_USE_PS /* Process-point string	*/
//#define EXE_USE_BL /* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name		Flags				Dim	Type	Def	Min	Max
            ---------	-------------------	---	-----	---	---	--*/
//EXE_REG_NN(ipAddr,	SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFFUL )
//EXE_REG_NN(gateway,	SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFFUL )
//EXE_REG_NN(submask,	SYS_REG_DEFAULT,	1,	Uint8,  24,	8,	30 )

EXE_REG_NS( sysContact, SYS_REG_DEFAULT,	20,			"Ari Suomi"    )
EXE_REG_NS( sysName, 	SYS_REG_DEFAULT,	20,			"EthKit"       )
EXE_REG_NS( sysLocation, SYS_REG_DEFAULT,	30,			"Tietolaite Oy, toinen kerros")
//EXE_REG_NS( sysDescr, 	SYS_REG_DEFAULT,	40,			"EthKit LPC2387, FreeRTOS, lwIP")

/*
EXE_MMI_NONE( myRName1 )
EXE_MMI_NONE( myRName2 )
EXE_MMI_NONE( myRName3 )
EXE_MMI_NONE( myRName4 )
*/

/* References to external registers */

//EXE_REG_EX( buttonStatusHandle, buttonStatus, button1 )


/* Write-indications */

//EXE_IND_ME( ipAddr, 	ipAddr )
//EXE_IND_ME( gateway, 	gateway )
//EXE_IND_ME( submask, 	submask )

//EXE_IND_EX( buttonStatusInd, buttonStatus, button1 )


/* Language-dependent texts */

//EXE_TXT( tHandle1, EXE_T_EN("Param descr 1") EXE_T_FI("Param kuvaus 1") "")

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
