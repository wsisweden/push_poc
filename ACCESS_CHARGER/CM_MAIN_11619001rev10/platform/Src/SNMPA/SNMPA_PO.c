/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SNMPA_PO.C
*
*	\ingroup	SNMPA
*
*	\brief		Glue functions for platform POD system and lwIP SNMP MIB.
*
*	\details
*
*	\note
*
*	\version	21-04-2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>

#include "tools.h"
#include "deb.h"
#include "snmpa.h"
#include "pod.h"
#include "sys.h"
#include "reg.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_getLeafCount
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the number of leafs a node has.
*
*	\param		pNodeNum	Pointer to leaf parent node number.
*	\param		level		Tree level.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 snmpa_getLeafCount(
	void * 					pNodeNum,
	Uint8 					level
) {
	Uint16					podIdx;
	pod_Pod const_P * 		pPod;
	Uint16					retVal = 0;

	pPod = snmpa__inst_p->pInit->pPod;

	/*
	 * Find the first leaf of the node.
	 */
	{
		pod_Search 	searchParams;
		Uint32		key[2];

		key[0] = *((Uint16 *) pNodeNum);

		searchParams.nameSize = 1;
		searchParams.pPod = pPod;
		searchParams.pProtName = key;

		podIdx = pPod->pTypeTools->searchFn(&searchParams);
	}

	if (podIdx != POD_NOTFOUND) {
		/*
		 * First leaf was found. Search forward in the pod until the nodeNumber
		 * changes. This will count how many leafs the node has.
		 */

		for(retVal = 1; (retVal + podIdx) < pPod->u16Size; retVal++) {
			snmpa_PodEntry const_P * 	pEntry;
			Uint8						elementSize;
			Uint32						podNodeNum;

			pEntry =
				&((snmpa_PodEntry const_P *) pPod->pAttrs)[podIdx + retVal];
			elementSize = pEntry->protName.size & 0xC0;

			if (elementSize == POD_ASIZE_8) {
				podNodeNum = *((Uint8 const_P *) pEntry->protName.pAddr);

			} else if (elementSize == POD_ASIZE_16) {
				podNodeNum = *((Uint16 const_P *) pEntry->protName.pAddr);

			} else { /* (elementSize == POD_ASIZE_32) */
				podNodeNum = *((Uint32 const_P *) pEntry->protName.pAddr);

			}

			if (podNodeNum != *((Uint16 *) pNodeNum)) {
				break;
			}
		}
	}

	snmpa__inst_p->firstEntryIdx = podIdx;

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_compareIdWithIdx
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Compare the leaf with the supplied identification number.
*
*	\param		pNodeNum	Pointer to leaf parent node number.
*	\param		level		Tree level.
*	\param		idx			Leaf index (not same as identification number).
*	\param		subIdent	Identification number to compare with.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Int32 snmpa_compareIdWithIdx(
	void * 					pNodeNum,
	Uint8 					level,
	Uint16 					idx,
	Int32 					subIdent
) {
	Int32					retVal = -1;

	if (snmpa__inst_p->firstEntryIdx != POD_NOTFOUND) {
		snmpa_PodEntry const_P * pEntry;
		Uint32					leafIdent;
		Uint8					elementSize;
		pod_Pod const_P * 		pPod;

		pPod = snmpa__inst_p->pInit->pPod;

		/*
		 * Compare the leaf with the supplied index with the supplied node
		 * identification number.
		 */

		pEntry = &((snmpa_PodEntry const_P *) pPod->pAttrs)[
			snmpa__inst_p->firstEntryIdx + idx
		];

		elementSize = pEntry->protName.size & 0xC0;

		if (elementSize == POD_ASIZE_8) {
			leafIdent = ((Uint8 const_P *) pEntry->protName.pAddr)[1];

		} else if (elementSize == POD_ASIZE_16) {
			leafIdent = ((Uint16 const_P *) pEntry->protName.pAddr)[1];

		} else { /* (elementSize == POD_ASIZE_32) */
			leafIdent = ((Uint32 const_P *) pEntry->protName.pAddr)[1];

		}

		retVal = leafIdent - subIdent;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_getId
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Fetches the object identification number for the supplied index.
*
*	\param		pNodeNum	Pointer to leaf parent node number.
*	\param		level		Tree level.
*	\param		idx			Leaf index (not same as identification number).
*	\param		pIdent		Pointer to object identification array.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_getId(
	void * 					pNodeNum,
	Uint8 					level,
	Uint16 					idx,
	Int32 *					pIdent
) {
	Uint16					podIdx;
	pod_Pod const_P * 		pPod;

	pPod = snmpa__inst_p->pInit->pPod;

	/*
	 * Find the first leaf of the node.
	 */
	{
		pod_Search 	searchParams;
		Uint32		key[2];

		key[0] = *((Uint16 *) pNodeNum);

		searchParams.nameSize = 1;
		searchParams.pPod = pPod;
		searchParams.pProtName = key;

		podIdx = pPod->pTypeTools->searchFn(&searchParams);
	}

	if (podIdx != POD_NOTFOUND) {
		snmpa_PodEntry const_P *	pEntry;
		Uint8						elementSize;

		/*
		 * First leaf was found. Compare the leaf with the supplied index with
		 * the supplied node identification number.
		 */

		pEntry = &((snmpa_PodEntry const_P *) pPod->pAttrs)[podIdx + idx];
		elementSize = pEntry->protName.size & 0xC0;

		if (elementSize == POD_ASIZE_8) {
			*pIdent = ((Uint8 const_P *) pEntry->protName.pAddr)[1];

		} else if (elementSize == POD_ASIZE_16) {
			*pIdent = ((Uint16 const_P *) pEntry->protName.pAddr)[1];

		} else { /* (elementSize == POD_ASIZE_32) */
			*pIdent = ((Uint32 const_P *) pEntry->protName.pAddr)[1];

		}

	//	snmpa__inst_p->currPodIdx = podIdx;
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_getObjDefQ
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start get object definition operation.
*
*	\param		pNodeNum	Pointer to leaf parent node number.
*	\param		reqId		Request ID.
*	\param		identLen	Object identification array length.
*	\param		pIdent		Pointer to object identification array.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_getObjDefQ(
	void * 					pNodeNum,
	Uint8 					reqId,
	Uint8 					identLen,
	Int32 * 				pIdent
) {
	/* return to object name, adding index depth (1) */
//	identLen += 1;
//	pIdent -= 1;

	/*
	 * Find the first leaf of the node.
	 */
	{
		pod_Search 			searchParams;
		Uint32				key[2];
		pod_Pod const_P * 	pPod;

		pPod = snmpa__inst_p->pInit->pPod;

		key[0] = *((Uint16 *) pNodeNum);
		key[1] = *pIdent;

		searchParams.nameSize = 2;
		searchParams.pPod = pPod;
		searchParams.pProtName = key;

		snmpa__inst_p->currPodIdx = pPod->pTypeTools->searchFn(&searchParams);
	}

	ntcpip_snmpMsgEvent(reqId);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_getObjDefA
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get object definition.
*
*	\param		reqId		Request ID.
*	\param		identLen	Object identification array length.
*	\param		pIdent		Pointer to object identification array.
*	\param		pObjDef		Pointer to object definition where the information
*							should be stored.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_getObjDefA(
	Uint8 					reqId,
	Uint8 					identLen,
	Int32 * 				pIdent,
	snmpa_ObjDef *			pObjDef
) {
	snmpa__Inst *			pInst;

	pInst = snmpa__inst_p;

	if (pInst->currPodIdx != POD_NOTFOUND) {
		snmpa_PodEntry const_P * pEntry;
		pod_Pod const_P * 		pPod;
		reg_StatType			stat;
		reg_Status				status;

		pPod = pInst->pInit->pPod;
		pEntry =
			&((snmpa_PodEntry const_P *) pPod->pAttrs)[pInst->currPodIdx];

		status = reg_stat(pEntry->nHandle, &stat);

		if (status == REG_OK) {
			Uint8 type;

			pObjDef->instance = pEntry->snmpaInst;
			pObjDef->access = pEntry->snmpaAccess;
			pObjDef->id_inst_ptr = (Int32 *) (Int32) pInst->currPodIdx;

			type = stat.status & REG_TYPE_MASK;

			switch (type) {
				case SYS_REG_T_Int8:
				case SYS_REG_T_Int16:
				case SYS_REG_T_Int32:
				case SYS_REG_T_Uint8:
				case SYS_REG_T_Uint16:
				case SYS_REG_T_Uint32:
					memset(pInst->buff, 0, 4);
					reg_aGet(
						pInst->buff,
						pEntry->nHandle,
						pEntry->nArrIndex
					);

					/*
					 * Convert the value if needed.
					 */

					if (type == SYS_REG_T_Int8) {
						*((Int32 *) (void *) pInst->buff) =
							*((Int8 *) pInst->buff);

					} else if (type == SYS_REG_T_Int16) {
						*((Int32 *) (void *) pInst->buff) =
							*((Int16 *) (void *) pInst->buff);
					}

					pInst->dataIdx = 0;
					pObjDef->v_len = 4;
					pObjDef->asn_type = SNMP_ASN1_INTEG;
					break;

				case SYS_REG_T_String:
					reg_get(
						pInst->buff,
						pEntry->nHandle
					);
					pInst->dataIdx = 1;
					pObjDef->v_len = pInst->buff[0];
					pObjDef->asn_type = SNMP_ASN1_OC_STR;
					break;

				case SYS_REG_T_File:
				case SYS_REG_T_Real:
				case SYS_REG_T_Typedef:
				default:
					pObjDef->instance = MIB_OBJECT_NONE;
					break;
			}

		} else {
			pObjDef->instance = MIB_OBJECT_NONE;

		}

	} else {
		pObjDef->instance = MIB_OBJECT_NONE;

	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_getObjDefPc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Panic close.
*
*	\param		reqId		Request ID.
*	\param		identLen	Object identification array length.
*	\param		pIdent		Pointer to object identification array.
*
*	\return
*
*	\details	Aborts the get object definition operation.
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_getObjDefPc(
	Uint8 					reqId,
	Uint8 					identLen,
	Int32 *					pIdent
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_getValueQ
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start get value operation.
*
*	\param		reqId		Request ID.
*	\param		pObjDef		Pointer to object definition.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_getValueQ(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef
) {
	ntcpip_snmpMsgEvent(reqId);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_getValueA
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get register value.
*
*	\param		reqId		Request ID.
*	\param		pObjDef		Pointer to object definition.
*	\param		len			Length of the buffer?
*	\param		pValue		Pointer to buffer where the value should be stored.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_getValueA(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef,
	Uint16 					len,
	void *					pValue
) {
	Uint16 					podIdx;

	podIdx = (Uint16) (Uint32) pObjDef->id_inst_ptr;

	if (podIdx != POD_NOTFOUND) {
		memcpy(pValue, &snmpa__inst_p->buff[snmpa__inst_p->dataIdx], len);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_setValuePc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Panic close.
*
*	\param		reqId		Request ID.
*	\param		pObjDef		Pointer to object definition.
*
*	\return
*
*	\details	Aborts the get value operation.
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_getValuePc(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_testValueQ
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start test value operation.
*
*	\param		reqId		Request ID.
*	\param		pObjDef		Pointer to object definition.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_testValueQ(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef
) {
	ntcpip_snmpMsgEvent(reqId);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_testValueA
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Test if data can be stored to the register.
*
*	\param		reqId	Request ID.
*	\param		pObjDef	Pointer to object definition.
*	\param		len		Size of data in bytes.
*	\param		pData	Pointer to data.
*
*	\return		1 If the value can be stored to the register. 0 if the value
*				cannot be stored to the register.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC Uint8 snmpa_testValueA(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef,
	Uint16 					len,
	void *					pData
) {
	Uint16 					podIdx;
	Uint8					retVal = 1;

	podIdx = (Uint16) (Uint32) pObjDef->id_inst_ptr;

	if (podIdx != POD_NOTFOUND) {
		pod_Pod const_P * 			pPod;
		snmpa_PodEntry const_P * 	pEntry;
		Uint8						asn1Type;

		podIdx = (Uint16) (Uint32) pObjDef->id_inst_ptr;
		pPod = snmpa__inst_p->pInit->pPod;
		pEntry = &((snmpa_PodEntry const_P *) pPod->pAttrs)[podIdx];
		asn1Type = pObjDef->asn_type & ~SNMP_ASN1_CONSTR;

		/*
		 * The test will be done differently for each data type.
		 */

		switch (asn1Type) {
			case SNMP_ASN1_UNIV | SNMP_ASN1_INTEG:
			{
				Int32 *		pValue;
				reg_Status 	status;

				deb_assert(len == 4);

				pValue = (Int32 *) pData;

				status = reg_i32ToRegType(
					*pValue,
					pEntry->nHandle,
					snmpa__inst_p->buff
				);

				if (status == REG_OK) {
					status = reg_test(snmpa__inst_p->buff, pEntry->nHandle);
				}

				if (status != REG_OK) {
					retVal = 0;
				}
				break;
			}

			case SNMP_ASN1_UNIV | SNMP_ASN1_OC_STR:
			{
				reg_StatType stat;

				reg_stat(pEntry->nHandle, &stat);

				if (len > stat.size) {
					/*
					 * String is longer than the register size.
					 */
					retVal = 0;
				}
				break;
			}

			default:
				retVal = 0;
				break;
		}


	} else {
		retVal = 0;

	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_testValuePc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Panic close.
*
*	\param		reqId	Request ID.
*	\param		pObjDef	Pointer to object definition.
*
*	\return
*
*	\details	Aborts the test value operation.
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_testValuePc(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_setValueQ
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start value test operation.
*
*	\param		reqId	Request ID.
*	\param		pObjDef	Pointer to object definition.
*	\param		len		Size of value in bytes.
*	\param		pValue	Pointer to value.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_setValueQ(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef,
	Uint16 					len,
	void *					pValue
) {
	ntcpip_snmpMsgEvent(reqId);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_setValueA
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set a new value to a register.
*
*	\param		reqId		Request ID.
*	\param		pObjDef		Pointer to object definition.
*	\param		len			Size of value in bytes.
*	\param		pData		Pointer to value.
*
*	\return		-
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_setValueA(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef,
	Uint16 					len,
	void *					pData
) {
	Uint16 					podIdx;
	snmpa__Inst *			pInst;

	pInst = snmpa__inst_p;
	podIdx = (Uint16) (Uint32) pObjDef->id_inst_ptr;

	if (podIdx != POD_NOTFOUND) {
		pod_Pod const_P * 			pPod;
		snmpa_PodEntry const_P * 	pEntry;
		Uint8						asn1Type;

		pPod = pInst->pInit->pPod;
		pEntry = &((snmpa_PodEntry const_P *) pPod->pAttrs)[podIdx];
		asn1Type = pObjDef->asn_type & ~SNMP_ASN1_CONSTR;

		switch (asn1Type) {
			case SNMP_ASN1_UNIV | SNMP_ASN1_INTEG:
				deb_assert(len == 4);
				reg_i32ToRegType(
					*((Int32 *) pData),
					pEntry->nHandle,
					pInst->buff
				);
				break;

			case SNMP_ASN1_UNIV | SNMP_ASN1_OC_STR:
				pInst->buff[0] = (Uint8) len;
				memcpy(&pInst->buff[1], pData, len);
				break;

			default:
				deb_assert(FALSE);
				break;
		}

		/*
		 * The data has been copied to the buffer by the test value function.
		 */

		reg_aPut(pInst->buff, pEntry->nHandle, pEntry->nArrIndex);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	snmpa_setValuePc
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Panic close.
*
*	\param		reqId	Request ID.
*	\param		pObjDef	Pointer to object definition.
*
*	\return
*
*	\details	Aborts the set value operation.
*
*	\note
*
*******************************************************************************/

PUBLIC void snmpa_setValuePc(
	Uint8 					reqId,
	snmpa_ObjDef *			pObjDef
) {

}
