/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SNMPA
*
*	\brief		Local declarations for the SNMPA FB.
*
*	\details		
*
*	\note		
*
*	\version	13.4.2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "snmpa.h"

#include "init.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Instance type for SNMPA FB.
 */

typedef struct snmpa__inst {
	snmpa_Init const_P *	pInit;		/**< Pointer to FB init values		*/
	Uint16					currPodIdx;	/**< Current POD entry index		*/
	Uint16					firstEntryIdx;/**< First POD index of node		*/
	sys_FBInstId			instId;		/**< FB instance id					*/
	char *					buff; 		/**< Data buffer					*/
	Uint8					dataIdx;	/**< Buffer data start index		*/
} snmpa__Inst;

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern snmpa_Interface const_P snmpa__interface;
extern snmpa__Inst *		snmpa__inst_p;

/******************************************************************************/
