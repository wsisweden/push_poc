/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SNMPA_MI.C
*
*	\ingroup	SNMPA
*
*	\brief		Tietolaite MIB.
*
*	\details
*
*	\note
*
*	\version	26-03-2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>

#include "tools.h"
#include "sys.h"
#include "reg.h"
#include "deb.h"
#include "snmpa.h"
#include "pod.h"
#include "ntcpip.h"

#include "local.h"

#if NTCPIP__LWIP_SNMP

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

#if 0
/* Development .1.3.6.1.4.1.35530.3 *******************************************/
PRIVATE Int32 const_P snmpa__productsIds[] = { 1, 2 };
PRIVATE snmpa_Node const_P * snmpa__productsNodes[] = {
	(snmpa_Node const_P *) &snmpa__test1,
	(snmpa_Node const_P *) &snmpa__test2
};
SNMPA_NOLEAF_PARENT_NODE(snmpa__products, PRIVATE);

/* Development .1.3.6.1.4.1.35530.2 *******************************************/
PRIVATE Int32 const_P snmpa__functionBlocksIds[] = { 1, 2 };
PRIVATE snmpa_Node const_P * snmpa__functionBlocksNodes[] = {
	(snmpa_Node const_P *) &snmpa__test1,
	(snmpa_Node const_P *) &snmpa__test2
};
SNMPA_NOLEAF_PARENT_NODE(snmpa__functionBlocks, PRIVATE);

#endif

/* sandbox2 .1.3.6.1.4.1.35530.1.2 ********************************************/
SNMPA_LEAF_PARENT_NODE(snmpa__sandbox2, SNMPA_NODENUM_TEST1);

/* sandbox1 .1.3.6.1.4.1.35530.1.1 ********************************************/
SNMPA_LEAF_PARENT_NODE(snmpa__sandbox1, SNMPA_NODENUM_TEST0);

/* Development .1.3.6.1.4.1.35530.1 *******************************************/
PRIVATE Int32 const_P snmpa__developmentIds[] = { 1, 2 };
PRIVATE snmpa_Node const_P * snmpa__developmentNodes[] = {
	(snmpa_Node const_P *) &snmpa__sandbox1,
	(snmpa_Node const_P *) &snmpa__sandbox2
};
SNMPA_NOLEAF_PARENT_NODE(snmpa__development, PRIVATE);

/* Tietolaite .1.3.6.1.4.1.35530 **********************************************/
PRIVATE Int32 const_P snmpa_tietolaiteMibIds[] = { 1 };
PRIVATE snmpa_Node const_P * snmpa_tietolaiteMibNodes[] = {
	(snmpa_Node const_P *) &snmpa__development,
/*	(snmpa_Node const_P *) &snmpa__functionBlocks, */
/*	(snmpa_Node const_P *) &snmpa__products */
};

/**
 * Root node of the Tietolaite MIB. This should be configured to the address
 * 1.3.6.1.4.1.35530 if the MIB is used in the project.
 */
SNMPA_NOLEAF_PARENT_NODE(snmpa_tietolaiteMib, PUBLIC);

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/

#endif



