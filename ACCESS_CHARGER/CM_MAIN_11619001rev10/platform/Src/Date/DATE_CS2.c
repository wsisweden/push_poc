/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Time conversion support.
*
*	\details
*
*	\version	\$Rev: 3081 $ \n
*				\$Date: 2016-11-10 16:57:02 +0200 (to, 10 marras 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
; D E C L A R A T I O N M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "date.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_timeToSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert the supplied time to milliseconds since midnight.
*
*	\param		pTime	Pointer to time structure containing the time to be
*						converted.
*
*	\details	Milliseconds from midnight is a more convenient format to do
*				time calculations on. It is also a more compact format for time
*				storage.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC date_TimeSerNr date_timeToSerNr(
	date_Time const_D *		pTime
) {
	return(
		pTime->hour * (60UL*60UL*1000UL)
		+ pTime->minute * (60UL*1000UL)
		+ pTime->second * 1000UL
		+ pTime->ms
	);
}
