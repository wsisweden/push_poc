/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		STM32F4xx RTC backup memory handling.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 4467 $ \n
*				\$Date:: 2021-03-18 16:11:20 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "DATE.H"
#include "hw.h"
#include "memdrv.h"

#include "DATE_HW.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void			date__bkpPut(void *,memdrv_Alloc *);
PRIVATE void			date__bkpGet(void *,memdrv_Alloc *);
PRIVATE void			date__bkpBusy(void *,memdrv_Alloc *);
PRIVATE void			date__bkpDoOp(void *,memdrv_Alloc *);

PRIVATE void 			date__bkpHandleRequest(memdrv_Alloc * pRequest);
PRIVATE memdrv_RetCode 	date__bkpWrite(memdrv_Alloc * pRequest);
PRIVATE memdrv_RetCode 	date__bkpRead(memdrv_Alloc * pRequest);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * 	Public interface for the RTC backup memory. The type of the interface object
 *	is specified by the MEMDRV interface.
 */

PUBLIC memdrv_Interface const_P date_bkpRegSyncIf = {
	&date__bkpPut,
	&date__bkpGet,
	&date__bkpBusy,
	&date__bkpDoOp,
	MEMDRV_FUNC_SYNCHRONOUS
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__bkpPut
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write data to flash.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to memory driver request.
*
*	\return		
*
*	\details
*
*	\note		The address must be erased before it can be written to.
*
*******************************************************************************/

PRIVATE void date__bkpPut(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	deb_assert(pRequest != NULL);
	deb_assert(pRequest->count != 0);

	pRequest->op = MEMDRV_OP_PUT;

	date__bkpHandleRequest(pRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__bkpGet
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read data from flash.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to memory driver request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void date__bkpGet(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	deb_assert(pRequest != NULL);
	deb_assert(pRequest->count != 0);

	pRequest->op = MEMDRV_OP_GET;

	date__bkpHandleRequest(pRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__bkpBusy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check if driver is busy.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to memory driver request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void date__bkpBusy(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	/* 
	 *	Do not use the busy function. The driver queues requests and the busy
	 *	response would be old information when the callback is called.
	 */

	deb_assert(FALSE);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__bkpDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform some other operation.
*
*	\param		pInstance	Pointer to driver instance.
*	\param		pRequest	Pointer to the request.
*
*	\return		
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void date__bkpDoOp(
	void *					pInstance,
	memdrv_Alloc *			pRequest
) {
	deb_assert(pRequest != NULL);

	date__bkpHandleRequest(pRequest);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__bkpHandleRequest
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the supplied request.
*
*	\param		pRequest	Pointer to the request.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE void date__bkpHandleRequest(
	memdrv_Alloc *			pRequest
) {
	memdrv_RetCode retCode;

	switch (pRequest->op) {
		case MEMDRV_OP_PUT:
			retCode = date__bkpWrite(pRequest);
			break;

		case MEMDRV_OP_GET:
			retCode = date__bkpRead(pRequest);
			break;

		case MEMDRV_OP_BUSY:
			retCode = MEMDRV_PARAM_ERROR;
			break;

		case MEMDRV_OP_SLEEP:
			retCode = MEMDRV_OK;
			break;

		case MEMDRV_OP_WAKEUP:
			retCode = MEMDRV_OK;
			break;

		case MEMDRV_OP_GET_INFO: {
			memdrv_MemInfo * pMemInfo;

			pMemInfo = (memdrv_MemInfo *) pRequest->pData;

			pMemInfo->flags = 0;
			pMemInfo->sectorSize = 1;
			pMemInfo->jedecId = 0;
			pMemInfo->deviceId[0] = 0;
			pMemInfo->deviceId[1] = 0;
			pMemInfo->deviceId[2] = 0;
			pMemInfo->deviceId[3] = 0;
			pMemInfo->sectorCount = 0;

			retCode = MEMDRV_OK;
			break;
		}

		case MEMDRV_OP_INIT_MEM:
			retCode = MEMDRV_OK;
			break;

		case MEMDRV_OP_MEDIA_PRESENT:
			retCode = MEMDRV_MEDIA_IS_PRESENT;
			break;

		case MEMDRV_OP_FLUSH:
			retCode = MEMDRV_OK;
			break;
			
		default:
			retCode = MEMDRV_NOT_SUPPORTED;
			break;
	}

	pRequest->count = (memdrv_SizeType) retCode;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__bkpRead
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the supplied read request.
*
*	\param		pRequest	Pointer to the request.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE memdrv_RetCode date__bkpRead(
	memdrv_Alloc *			pRequest
) {
	Uint32 regNumber;
	Uint32 offset;
	Uint32 bytesLeft;
	BYTE * pData;

	bytesLeft = pRequest->count;

	if (bytesLeft == 0) {
		return MEMDRV_PARAM_ERROR;
	}

	regNumber = pRequest->address / 4;
	offset = pRequest->address - (regNumber * 4);
	pData = pRequest->pData;

	do {
		Uint32 regData;

		regData = HAL_RTCEx_BKUPRead(&date__hrtInst.hwHandle, regNumber);

		switch (offset) {
			case 0:
				if (bytesLeft != 0) {
					*pData++ = (BYTE) ((regData >> 0) & 0xFFU);
					bytesLeft--;
				}
			case 1:
				if (bytesLeft != 0) {
					*pData++ = (BYTE) ((regData >> 8) & 0xFFU);
					bytesLeft--;
				}
			case 2:
				if (bytesLeft != 0) {
					*pData++ = (BYTE) ((regData >> 16) & 0xFFU);
					bytesLeft--;
				}
			case 3:
				if (bytesLeft != 0) {
					*pData++ = (BYTE) ((regData >> 24) & 0xFFU);
					bytesLeft--;
				}
				break;
		}

		offset = 0;
		regNumber++;

	} while (bytesLeft != 0 && regNumber <= RTC_BKP_DR19);

	return MEMDRV_OK;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__bkpWrite
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle the supplied write request.
*
*	\param		pRequest	Pointer to the request.
*
*	\return		
*
*	\details
*
*	\note		
*
*******************************************************************************/

PRIVATE memdrv_RetCode date__bkpWrite(
	memdrv_Alloc *			pRequest
) {
	Uint32 regNumber;
	Uint32 offset;
	Uint32 bytesLeft;
	BYTE * pData;

	bytesLeft = pRequest->count;

	if (bytesLeft == 0) {
		return MEMDRV_PARAM_ERROR;
	}

	regNumber = pRequest->address / 4;
	offset = pRequest->address - (regNumber * 4);
	pData = pRequest->pData;

	do {
		Uint32 regData;

		regData = HAL_RTCEx_BKUPRead(&date__hrtInst.hwHandle, regNumber);

		switch (offset) {
			case 0:
				if (bytesLeft != 0) {
					regData &= 0xFFFFFF00U;
					regData |= *pData++;
					bytesLeft--;
				}
			case 1:
				if (bytesLeft != 0) {
					regData &= 0xFFFF00FFU;
					regData |= ((Uint32)(*pData++)) << 8;
					bytesLeft--;
				}
			case 2:
				if (bytesLeft != 0) {
					regData &= 0xFF00FFFFU;
					regData |= ((Uint32)(*pData++)) << 16;
					bytesLeft--;
				}
			case 3:
				if (bytesLeft != 0) {
					regData &= 0x00FFFFFFU;
					regData |= ((Uint32)(*pData++)) << 24;
					bytesLeft--;
				}
				break;
		}

		HAL_RTCEx_BKUPWrite(&date__hrtInst.hwHandle, regNumber, regData);

		offset = 0;
		regNumber++;

	} while (bytesLeft != 0 && regNumber <= RTC_BKP_DR19);

	return MEMDRV_OK;
}