/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		STM32F4xx RTC handling.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 4484 $ \n
*				\$Date:: 2021-04-09 10:06:44 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "DATE.H"
#include "hw.h"

#include "DATE_HW.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Status flags
 */
/**@{*/
#define DATE__HRTFLG_INIT_DONE	(1<<0)
/**@}*/

/**
 * Random number stored to backup register 0 when the time has been set.
 */

#define DATE__HRTBKP0_SET		0x72364628U
#define DATE__HRTBKP0_INIT		0x42796175U

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE date_RtcStatus	date__hwRtcGetDt(date_DateTime *);
PRIVATE date_RtcStatus	date__hwRtcGetDtSerNr(date_DtSerNr *);
PRIVATE void			date__hwRtcSetDt(date_DateTime const_D *);
PRIVATE void			date__hwRtcSetDtSerNr(date_DtSerNr const_D *);
PRIVATE void			date__hwRtcDoOp(date_RtcOp,void *);

PRIVATE void 			date__hrtMspInit(RTC_HandleTypeDef *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Public interface constant for the internal RTC peripheral.
 *
 *	This RTC implementation requires an external RTC crystal. A backup battery
 *	is normally also used.
 *
 *	date_internalRtcInit must be called before any call through this interface.
 *
 *	The accuracy of this implementation depends on the RTC crystal. The
 *	resolution is one second.
 */

PUBLIC date_RtcIf const_P date_internalRtc = {
	&date__hwRtcGetDt,
	&date__hwRtcGetDtSerNr,
	&date__hwRtcSetDt,
	&date__hwRtcSetDtSerNr,
	&date__hwRtcDoOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PUBLIC date__HrtInst date__hrtInst = { 0 };

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_internalRtcInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the RTC.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details	This function will start the RTC if it hasn't been started yet.
*				The epoch date will be set when the RTC is started.
*
*	\note		Must be called before any call through the date_internalRtc 
*				interface.
*
*	\sa
*
*******************************************************************************/

PUBLIC date_RtcStatus date_internalRtcInit(
	void
) {
	date_RtcStatus 		rtcStatus;
	HAL_StatusTypeDef 	halStatus;
	Boolean				reinitRtc;

	reinitRtc = FALSE;
	if (
		(RTC->BKP0R != DATE__HRTBKP0_SET && RTC->BKP0R != DATE__HRTBKP0_INIT)
		|| (RCC->BDCR & RCC_BDCR_RTCEN) == 0
	) {
		reinitRtc = TRUE;
	}

	/* 
	 *	Do not re-initialize RTC in case it has already been to avoid loosing
	 *	seconds at each reset.
	 */

	date__hrtInst.hwHandle.Instance = RTC;

	date__hrtInst.hwHandle.Init.HourFormat = RTC_HOURFORMAT_24;
	date__hrtInst.hwHandle.Init.AsynchPrediv = 127;
	date__hrtInst.hwHandle.Init.SynchPrediv = 255;
	date__hrtInst.hwHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
	date__hrtInst.hwHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	date__hrtInst.hwHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

	if (reinitRtc) {
		HAL_RTC_RegisterCallback(
			&date__hrtInst.hwHandle,
			HAL_RTC_MSPINIT_CB_ID,
			date__hrtMspInit
		);

		halStatus = HAL_RTC_Init(&date__hrtInst.hwHandle);

	} else {
		__HAL_RCC_RTC_ENABLE();
		HAL_PWR_EnableBkUpAccess();
		HAL_RTC_WaitForSynchro(&date__hrtInst.hwHandle);
		halStatus = HAL_OK;
	}

	if (halStatus == HAL_OK) {
		if (RTC->BKP0R == DATE__HRTBKP0_SET) {
			/*
			 *	Time has been set and should be valid.
			 */

			rtcStatus = DATE_RTCSTAT_VALID;

		} else if (RTC->BKP0R == DATE__HRTBKP0_INIT) {
			/*
			 *	The time has been initialized to epoch time when the RTC
			 *	was initialized the first time. A valid time has not been
			 *	set.
			 */

			rtcStatus = DATE_RTCSTAT_NOT_SET;	

		} else {
			date_DtSerNr dtSerNr;

			date__hrtInst.flags |= DATE__HRTFLG_INIT_DONE;
			
			/*
			 *	This is the first time the RTC is initialized after power on. 
			 *	Initialize the RTC to epoch date.
			 */

			dtSerNr.dateSerNr = 0;
			dtSerNr.timeSerNr = 0;
			date__hwRtcSetDtSerNr(&dtSerNr);

			RTC->BKP0R = DATE__HRTBKP0_INIT;

			rtcStatus = DATE_RTCSTAT_NOT_SET;
		}

	} else {
		rtcStatus = DATE_RTCSTAT_ERROR;
	}

	date__hrtInst.flags |= DATE__HRTFLG_INIT_DONE;

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hrtMspInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Low level RTC init.
*
*	\param		rtcHandle	ST HAL handle.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hrtMspInit(
	RTC_HandleTypeDef * 	rtcHandle
) {
	/* RTC clock enable */
	__HAL_RCC_RTC_ENABLE();

	/* RTC interrupt Init */
	//hw_enableIrq(RTC_WKUP_IRQn);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcGetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC.
*
*	\param		pDateTime	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__hwRtcGetDt(
	date_DateTime *			pDateTime
) {
	date_RtcStatus 		rtcStatus;
	RTC_TimeTypeDef 	time;
	RTC_DateTypeDef 	date;
	HAL_StatusTypeDef 	halStatus;

	deb_assert(date__hrtInst.flags & DATE__HRTFLG_INIT_DONE);

	halStatus = HAL_RTC_GetTime(
		&date__hrtInst.hwHandle,
		&time,
		RTC_FORMAT_BIN
	);

	if (halStatus == HAL_OK) {
		halStatus = HAL_RTC_GetDate(
			&date__hrtInst.hwHandle,
			&date,
			RTC_FORMAT_BIN
		);
	}

	if (halStatus == HAL_OK) {

		pDateTime->date.base.year = date.Year + 2000U;
		pDateTime->date.base.month = date.Month;
		pDateTime->date.base.day = date.Date;
		pDateTime->date.dayOfWeek = date.WeekDay;
		pDateTime->time.hour = time.Hours;
		pDateTime->time.minute = time.Minutes;	
		pDateTime->time.second = time.Seconds;

		/*
		 *	TODO:	SubSeconds and SecondFraction could be used to get the
		 *			millisecond value.
		 */
		pDateTime->time.ms = 0;

		if (RTC->BKP0R == DATE__HRTBKP0_SET) {
			rtcStatus = DATE_RTCSTAT_VALID;
		} else {
			rtcStatus = DATE_RTCSTAT_NOT_SET;
		}

	} else {
		date_DtSerNr dtSerNr;

		/*
		 *	Something went wrong. Just return epoch date.
		 */

		dtSerNr.dateSerNr = 0;
		dtSerNr.timeSerNr = 0;
		date_dtFromSerNr(pDateTime, &dtSerNr);

		rtcStatus = DATE_RTCSTAT_ERROR;
	}

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcGetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC in serial format.
*
*	\param		pDtSerNr	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__hwRtcGetDtSerNr(
	date_DtSerNr *			pDtSerNr
) {
	date_RtcStatus	rtcStatus;
	date_DateTime	dateTime;

	/*
	 * The time and date is stored in struct format in the RTC. Just getting
	 * the struct format and converting it to serial format.
	 */

	rtcStatus = date__hwRtcGetDt(&dateTime);
	date_dtToSerNr(pDtSerNr, &dateTime);

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcSetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcSetDt(
	date_DateTime const_D * pDateTime
) {
	RTC_TimeTypeDef 	time;
	RTC_DateTypeDef 	date;
	HAL_StatusTypeDef 	halStatus;
	Uint8 				year;

	deb_assert(date__hrtInst.flags & DATE__HRTFLG_INIT_DONE);

	if (pDateTime->date.base.year >= 2000) {
		year = pDateTime->date.base.year - 2000U;
	} else {
		year = 0;
	}

	date.Year = year;
	date.Month = pDateTime->date.base.month;
	date.Date = pDateTime->date.base.day;
	date.WeekDay = pDateTime->date.dayOfWeek;

	time.Hours = pDateTime->time.hour;
	time.Minutes = pDateTime->time.minute;
	time.Seconds = pDateTime->time.second;
	time.SubSeconds = 0;
	time.SecondFraction = 0;
	time.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	time.StoreOperation = RTC_STOREOPERATION_SET;

	halStatus = HAL_RTC_SetDate(
		&date__hrtInst.hwHandle,
		&date,
		RTC_FORMAT_BIN
	);

	if (halStatus == HAL_OK) {
		halStatus = HAL_RTC_SetTime(
			&date__hrtInst.hwHandle,
			&time,
			RTC_FORMAT_BIN
		);

		if (halStatus == HAL_OK) {
			RTC->BKP0R = DATE__HRTBKP0_SET;
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcSetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time using serial format.
*
*	\param		pDtSerNr	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcSetDtSerNr(
	date_DtSerNr const_D *	pDtSerNr
) {
	date_DateTime dateTime;

	date_dtFromSerNr(&dateTime, pDtSerNr);
	date__hwRtcSetDt(&dateTime);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform additional RTC operations.
*
*	\param		op		Code specifying the desired operation.
*	\param		pParam	Pointer to operation parameters. The parameter type
*						depends on the requested operation.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcDoOp(
	date_RtcOp				op,
	void *					pParam
) {
	/*
	 *	Calibration and alarm setting should be handled here.
	 */
}
