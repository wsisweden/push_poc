/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Millisecond counter RTC implementation for hardware with no RTC.
*
*	\details	This implementation will maintain the current time as long as
*				the device is powered. The accuracy of the time is the same as
*				the millisecond ISR.
*
*	\note		
*
*	\version	\$Rev: 4423 $ \n
*				\$Date:: 2021-01-13 17:08:06 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "DATE.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * Milliseconds in one day.
 */

#define DATE__MSC_MS_DAY	(1000UL*60UL*60UL*24UL)

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE date_RtcStatus 	date__msCountRtcGetDt(date_DateTime *);
PRIVATE date_RtcStatus 	date__msCountRtcGetDtSer(date_DtSerNr *);
PRIVATE void			date__msCountRtcSetDt(date_DateTime const_D *);
PRIVATE void			date__msCountRtcSetDtSer(date_DtSerNr const_D*);
PRIVATE void			date__msCountRtcDoOp(date_RtcOp,void * pParam);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Public interface constant for RTC implementation that uses the 1ms
 *	ISR to increment the clock.
 *
 *	The date_msCountTick macro should be used in the 1ms ISR when this interface
 *	is in use. No init function call is needed for this RTC implementation.
 *
 *	The accuracy of this implementation depends on the main clock crystal. The
 *	resolution is one millisecond.
 */

PUBLIC date_RtcIf const_P date_msCountRtc = {
	&date__msCountRtcGetDt,
	&date__msCountRtcGetDtSer,
	&date__msCountRtcSetDt,
	&date__msCountRtcSetDtSer,
	&date__msCountRtcDoOp
};

/**
 *	Variable used to count current time. The timeSerNr field in this variable
 *	is inverted so that DATE__MSC_MS_DAY means zero milliseconds. The inversion
 *	is done because it is slightly faster to update the counter that way.
 */

PUBLIC date_DtSerNr date__msCountRtc = {0, DATE__MSC_MS_DAY};
PUBLIC Boolean		date__msCountDateSet = FALSE;

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__msCountRtcGetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC.
*
*	\param		pDateTime	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_INACCURATE		The time and date is valid but
*											inaccurate.
*	\retval		DATE_RTCSTAT_NOT_SET		Time has not been set.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__msCountRtcGetDt(
	date_DateTime *			pDateTime
) {
	date_DtSerNr	currentTime;
	date_RtcStatus	rtcStatus;

	rtcStatus = date__msCountRtcGetDtSer(&currentTime);

	/*
	 *	Converting the serial numbers to a date structure.
	 */

	date_dtFromSerNr(pDateTime, &currentTime);

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__msCountRtcGetDtSer
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC in serial format.
*
*	\param		pDateTime	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_INACCURATE		The time and date is valid but
*											inaccurate.
*	\retval		DATE_RTCSTAT_NOT_SET		Time has not been set.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__msCountRtcGetDtSer(
	date_DtSerNr *			pDateTime
) {
	date_RtcStatus rtcStatus;

	rtcStatus = DATE_RTCSTAT_INACCURATE;

	/*
	 *	Copying the current ms and second counters atomically.
	 */

	DISABLE;
	*pDateTime = date__msCountRtc;

	if (date__msCountDateSet == FALSE) {
		rtcStatus = DATE_RTCSTAT_NOT_SET;
	}
	ENABLE;

	pDateTime->timeSerNr = DATE__MSC_MS_DAY - pDateTime->timeSerNr;

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__msCountRtcSetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__msCountRtcSetDt(
	date_DateTime const_D * pDateTime
) {
	date_DtSerNr newDateTime;

	/*
	 *	Converting the date to serial format.
	 */

	date_dtToSerNr(&newDateTime, pDateTime);

	newDateTime.timeSerNr = DATE__MSC_MS_DAY - newDateTime.timeSerNr;

	/*
	 *	Copying the current ms and second counters atomically.
	 */

	atomic(
		date__msCountRtc = newDateTime;
		date__msCountDateSet = TRUE;
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__msCountRtcSetDtSer
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets RTC date and time using serial format.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__msCountRtcSetDtSer(
	date_DtSerNr const_D *	pDateTime
) {
	date_TimeSerNr timeSerNr;

	timeSerNr = DATE__MSC_MS_DAY - pDateTime->timeSerNr;

	/*
	 *	Copying the current ms and second counters atomically.
	 */

	atomic(
		date__msCountRtc.dateSerNr = pDateTime->dateSerNr;
		date__msCountRtc.timeSerNr = timeSerNr;
		date__msCountDateSet = TRUE;
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__msCountRtcDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform additional RTC operations.
*
*	\param		op		Code specifying the desired operation.
*	\param		pParam	Pointer to operation parameters. The parameter type
*						depends on the requested operation.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__msCountRtcDoOp(
	date_RtcOp				op,
	void *					pParam
) {

}
