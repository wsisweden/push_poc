/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		LPC17xx and LPC407x RTC handling.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 4434 $ \n
*				\$Date:: 2021-02-04 15:38:21 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "DATE.H"
#include "osa.h"
#include "hw.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define DATE__CCR_CLKEN			(1<<0)		/** Clock enable bitmask.		*/

#if TARGET_SELECTED & TARGET_CM3
#define DATE__RTC_SEC		LPC_RTC->SEC
#define DATE__RTC_MIN		LPC_RTC->MIN
#define DATE__RTC_HOUR		LPC_RTC->HOUR
#define DATE__RTC_DOW		LPC_RTC->DOW
#define DATE__RTC_DOM		LPC_RTC->DOM
#define DATE__RTC_MONTH		LPC_RTC->MONTH
#define DATE__RTC_YEAR		LPC_RTC->YEAR
#else
#define DATE__RTC_SEC		LPC_RTC->TIME[RTC_TIMETYPE_SECOND]
#define DATE__RTC_MIN		LPC_RTC->TIME[RTC_TIMETYPE_MINUTE]
#define DATE__RTC_HOUR		LPC_RTC->TIME[RTC_TIMETYPE_HOUR]
#define DATE__RTC_DOW		LPC_RTC->TIME[RTC_TIMETYPE_DAYOFWEEK]
#define DATE__RTC_DOM		LPC_RTC->TIME[RTC_TIMETYPE_DAYOFMONTH]
#define DATE__RTC_MONTH		LPC_RTC->TIME[RTC_TIMETYPE_MONTH]
#define DATE__RTC_YEAR		LPC_RTC->TIME[RTC_TIMETYPE_YEAR]
#endif

/**
 * \name	Status flags
 */
/**@{*/
#define DATE__HRTFLG_INIT_DONE	(1<<0)
#define DATE__HRTFLG_TIME_SET	(1<<1)
/**@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Type for driver instance.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	volatile Uint8			flags;		/**< Status flags.					*/
} date__HrtInst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE date_RtcStatus	date__hwRtcGetDt(date_DateTime *);
PRIVATE date_RtcStatus	date__hwRtcGetDtSerNr(date_DtSerNr *);
PRIVATE void			date__hwRtcSetDt(date_DateTime const_D *);
PRIVATE void			date__hwRtcSetDtSerNr(date_DtSerNr const_D *);
PRIVATE void			date__hwRtcDoOp(date_RtcOp,void *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Public interface constant for the internal RTC peripheral.
 *
 *	This RTC implementation requires an external RTC crystal. A backup battery
 *	is normally also used.
 *
 *	date_internalRtcInit must be called before any call through this interface.
 *
 *	The accuracy of this implementation depends on the RTC crystal. The
 *	resolution is one second.
 */

PUBLIC date_RtcIf const_P date_internalRtc = {
	&date__hwRtcGetDt,
	&date__hwRtcGetDtSerNr,
	&date__hwRtcSetDt,
	&date__hwRtcSetDtSerNr,
	&date__hwRtcDoOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE date__HrtInst date__hrtInst = { 0 };

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_internalRtcInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the RTC.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details	This function will start the RTC if it hasn't been started yet.
*				The epoch date will be set when the RTC is started.
*
*	\note		Must be called before any call through the date_internalRtc 
*				interface.
*
*	\sa
*
*******************************************************************************/

PUBLIC date_RtcStatus date_internalRtcInit(
	void
) {
	date_RtcStatus rtcStatus;

#if TARGET_SELECTED & TARGET_CM3
	SC->PCONP |= (1<<9);
#else
	LPC_SYSCTL->PCONP |= (1<<SYSCTL_CLOCK_RTC);
#endif

	rtcStatus = DATE_RTCSTAT_VALID;

	date__hrtInst.flags |= DATE__HRTFLG_INIT_DONE;

	if (LPC_RTC->RTC_AUX & (1<<4)) {
		date_DateTime dateTime;
		date_DtSerNr dtSerNr;

		/*
		 *	The RTC oscillator has stopped.
		 *
		 *	The oscillator can stop because of the following reasons:
		 *	- The RTC crystal is missing
		 *	- It is the first time the device is powered
		 *	- Power has been lost and there is no backup battery
		 *	- Power has been lost and the backup battery is empty
		 *	- The RTC is malfunctioning for some reason
		 *
		 *	Initializing and starting the RTC.
		 */

		LPC_RTC->AMR = 0;
		LPC_RTC->CIIR = 0;
		LPC_RTC->CCR = 0;
		LPC_RTC->CALIBRATION = 0;

		LPC_RTC->RTC_AUX |= (1<<4);

		/*
		 *	Get epoch date and initialize the RTC with that date.
		 */

		dtSerNr.dateSerNr = 0;
		dtSerNr.timeSerNr = 0;
		date_dtFromSerNr(&dateTime, &dtSerNr);
		date__hwRtcSetDt(&dateTime);

		rtcStatus = DATE_RTCSTAT_NOT_SET;

	} else {
		date_DtSerNr dtSerNr;

		/*
		 * 	The RTC is running. It is not possible to know for sure if the time
		 * 	has been set.
		 *
		 * 	Assuming the time has been set if the current time is at least two
		 * 	years from the epoch date.
		 */

		date__hwRtcGetDtSerNr(&dtSerNr);

		if (dtSerNr.dateSerNr > (365U * 2)) {
			date__hrtInst.flags |= DATE__HRTFLG_TIME_SET;
			rtcStatus = DATE_RTCSTAT_VALID;
		} else {
			rtcStatus = DATE_RTCSTAT_NOT_SET;
		}
	}

	LPC_RTC->CCR |= DATE__CCR_CLKEN;

	if (LPC_RTC->RTC_AUX & (1<<4)) {
		/*
		 * 	The RTC oscillator is still not running. Maybe there is no RTC
		 * 	crystal connected.
		 */

		rtcStatus = DATE_RTCSTAT_ERROR;
	}

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcGetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC.
*
*	\param		pDateTime	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__hwRtcGetDt(
	date_DateTime *			pDateTime
) {
	date_RtcStatus rtcStatus;
	
	deb_assert(date__hrtInst.flags & DATE__HRTFLG_INIT_DONE);

	if not(LPC_RTC->RTC_AUX & (1<<4)) {
		pDateTime->time.ms = 0;

		DISABLE;

		/*
		 *	The RTC oscillator is running.
		 *
		 *	Read the current date and time from the RTC. Read again if the
		 *	second counter has been incremented while reading
		 */

		do {
			pDateTime->time.second = DATE__RTC_SEC;
			pDateTime->time.minute = DATE__RTC_MIN;
			pDateTime->time.hour = DATE__RTC_HOUR;
			pDateTime->date.dayOfWeek = DATE__RTC_DOW + 1;
			pDateTime->date.base.day = DATE__RTC_DOM;
			pDateTime->date.base.month = DATE__RTC_MONTH;
			pDateTime->date.base.year = DATE__RTC_YEAR;
		} while (pDateTime->time.second != DATE__RTC_SEC);

		if (date__hrtInst.flags & DATE__HRTFLG_TIME_SET) {
			rtcStatus = DATE_RTCSTAT_VALID;
		} else {
			rtcStatus = DATE_RTCSTAT_NOT_SET;
		}
		ENABLE;

	} else {
		date_DtSerNr dtSerNr;

		/*
		 *	The RTC oscillator has stopped.
		 */

		dtSerNr.dateSerNr = 0;
		dtSerNr.timeSerNr = 0;
		date_dtFromSerNr(pDateTime, &dtSerNr);

		atomic(
			date__hrtInst.flags &= ~DATE__HRTFLG_TIME_SET;
		)

		rtcStatus = DATE_RTCSTAT_ERROR;
	}

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcGetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC in serial format.
*
*	\param		pDtSerNr	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__hwRtcGetDtSerNr(
	date_DtSerNr *			pDtSerNr
) {
	date_RtcStatus	rtcStatus;
	date_DateTime	dateTime;

	/*
	 * The time and date is stored in struct format in the RTC. Just getting
	 * the struct format and converting it to serial format.
	 */

	rtcStatus = date__hwRtcGetDt(&dateTime);
	date_dtToSerNr(pDtSerNr, &dateTime);

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcSetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcSetDt(
	date_DateTime const_D * pDateTime
) {
	Uint32					running;

	deb_assert(date__hrtInst.flags & DATE__HRTFLG_INIT_DONE);

	/*
	 * Protecting the follwing code from concurrent access from different task.
	 */

	DISABLE;

	/*
	 *	Stop the RTC.
	 */

	running = LPC_RTC->CCR & DATE__CCR_CLKEN;
	LPC_RTC->CCR &= ~DATE__CCR_CLKEN;

	/*
	 *	Set the new time and date.
	 */

	DATE__RTC_SEC = pDateTime->time.second;
	DATE__RTC_MIN = pDateTime->time.minute;
	DATE__RTC_HOUR = pDateTime->time.hour;

	DATE__RTC_DOM = pDateTime->date.base.day;
	DATE__RTC_DOW = pDateTime->date.dayOfWeek - 1;
	DATE__RTC_MONTH = pDateTime->date.base.month;
	DATE__RTC_YEAR = pDateTime->date.base.year;

	/*
	 *	Start the RTC again if it was running.
	 */

	LPC_RTC->CCR |= running;

	date__hrtInst.flags |= DATE__HRTFLG_TIME_SET;
	ENABLE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcSetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time using serial format.
*
*	\param		pDtSerNr	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcSetDtSerNr(
	date_DtSerNr const_D *	pDtSerNr
) {
	date_DateTime dateTime;

	date_dtFromSerNr(&dateTime, pDtSerNr);
	date__hwRtcSetDt(&dateTime);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform additional RTC operations.
*
*	\param		op		Code specifying the desired operation.
*	\param		pParam	Pointer to operation parameters. The parameter type
*						depends on the requested operation.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcDoOp(
	date_RtcOp				op,
	void *					pParam
) {
	/*
	 *	Calibration and alarm setting should be handled here.
	 */
}
