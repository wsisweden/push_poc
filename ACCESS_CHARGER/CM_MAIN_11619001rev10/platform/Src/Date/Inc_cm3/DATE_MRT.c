/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Millisecond estimation for internal RTC.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 4423 $ \n
*				\$Date:: 2021-01-13 17:08:06 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "osa.h"
#include "hw.h"
#include "DATE.H"
#include "TSTAMP.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define DATE__CCR_CLKEN			(1<<0)		/** Clock enable bitmask.		*/

#if TARGET_SELECTED & TARGET_CM3
#define DATE__RTC_SEC		LPC_RTC->SEC
#define DATE__RTC_MIN		LPC_RTC->MIN
#define DATE__RTC_HOUR		LPC_RTC->HOUR
#define DATE__RTC_DOW		LPC_RTC->DOW
#define DATE__RTC_DOM		LPC_RTC->DOM
#define DATE__RTC_MONTH		LPC_RTC->MONTH
#define DATE__RTC_YEAR		LPC_RTC->YEAR
#else
#define DATE__RTC_SEC		LPC_RTC->TIME[RTC_TIMETYPE_SECOND]
#define DATE__RTC_MIN		LPC_RTC->TIME[RTC_TIMETYPE_MINUTE]
#define DATE__RTC_HOUR		LPC_RTC->TIME[RTC_TIMETYPE_HOUR]
#define DATE__RTC_DOW		LPC_RTC->TIME[RTC_TIMETYPE_DAYOFWEEK]
#define DATE__RTC_DOM		LPC_RTC->TIME[RTC_TIMETYPE_DAYOFMONTH]
#define DATE__RTC_MONTH		LPC_RTC->TIME[RTC_TIMETYPE_MONTH]
#define DATE__RTC_YEAR		LPC_RTC->TIME[RTC_TIMETYPE_YEAR]
#endif

/**
 * \name	Status flags
 */
/**@{*/
#define DATE__MRTFLG_INIT_DONE	(1<<0)		/**< RTC Initialization done.	*/
#define DATE__MRTFLG_TIME_SET	(1<<1)		/**< Time has been set.			*/
/**@}*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Type for RTC driver instance data.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''*/
	volatile Uint8			flags;		/**< Status flags.					*/
	Uint8					dayOfWeek;	/**< Current day of week.			*/
	tstamp_Stamp			tickStamp;	/**< RTC second tick timestamp.		*/
	volatile Int32			offset;		/**< RTC and time variable offset.	*/
	date_DtSerNr			currTime;	/**< Current time.					*/
} rtc__MrtInst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE date_RtcStatus	date__mrtGetDt(date_DateTime *);
PRIVATE date_RtcStatus	date__mrtGetDtSerNr(date_DtSerNr *);
PRIVATE void			date__mrtSetDt(date_DateTime const_D *);
PRIVATE void			date__mrtSetDtSerNr(date_DtSerNr const_D *);
PRIVATE void			date__mrtDoOp(date_RtcOp,void * pParam);

PRIVATE void date__mrtSetDateTime(date_DateTime const_D*,date_DtSerNr const_D*);
PRIVATE date_RtcStatus date__mrtGetDateTime(date_DtSerNr *,Uint8 *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Interface constant for the hardware RTC driver with additional millisecond
 *	resolution.
 *
 *	This RTC implementation requires an external RTC crystal. A backup battery
 *	is normally also used.
 *
 *	date_internalMsRtcInit must be called before any call through this
 *	interface.
 *
 *	The accuracy of this implementation depends on the RTC crystal. The
 *	resolution is one millisecond.
 */

PUBLIC date_RtcIf const_P date_internalMsRtc = {
	&date__mrtGetDt,
	&date__mrtGetDtSerNr,
	&date__mrtSetDt,
	&date__mrtSetDtSerNr,
	&date__mrtDoOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE rtc__MrtInst date__mrtInst = { 0 };

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_internalMsRtcInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the RTC with millisecond resolution.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details	This function will start the RTC if it hasn't been started yet.
*				The epoch date will be set when the RTC is started.
*
*	\note		Must be called before any call through the date_internalMsRtc 
*				interface.
*
*	\sa
*
*******************************************************************************/

PUBLIC date_RtcStatus date_internalMsRtcInit(
	void
) {
	date_RtcStatus rtcStatus;

#if TARGET_SELECTED & TARGET_CM3
	SC->PCONP |= (1<<9);
#else
	LPC_SYSCTL->PCONP |= (1<<SYSCTL_CLOCK_RTC);
#endif

	date__mrtInst.flags = 0;
	date__mrtInst.offset = 0;

	date__mrtInst.flags |= DATE__MRTFLG_INIT_DONE;

	if (LPC_RTC->RTC_AUX & (1<<4)) {
		date_DtSerNr 	dtSerNr;

		/*
		 *	The RTC oscillator has stopped.
		 *
		 *	The oscillator can stop because of the following reasons:
		 *	- The RTC crystal is missing
		 *	- It is the first time the device is powered
		 *	- Power has been lost and there is no backup battery
		 *	- Power has been lost and the backup battery is empty
		 *	- The RTC is malfunctioning for some reason
		 *
		 *	Initializing and starting the RTC.
		 */

		LPC_RTC->AMR = 0;
		LPC_RTC->CIIR = 0;
		LPC_RTC->CCR = 0;
		LPC_RTC->CALIBRATION = 0;

		LPC_RTC->RTC_AUX |= (1<<4);

		hw_enableIrq(RTC_IRQn);
		LPC_RTC->CIIR = (1<<0); /* Isr on 1 sec */

		/*
		 *	Get epoch date and initialize the RTC with that date.
		 */

		dtSerNr.dateSerNr = 0;
		dtSerNr.timeSerNr = 0;
		date__mrtSetDtSerNr(&dtSerNr);

		rtcStatus = DATE_RTCSTAT_NOT_SET;

	} else {
		date_DateTime dateTime;
		
		/*
		 * 	The RTC is running. 
		 *
		 *	Read the current date and time from the RTC. Read again if the
		 *	second counter has been incremented while reading. This way we can
		 *	read the time even when the RTC is running.
		 */

		do {
			dateTime.time.second = DATE__RTC_SEC;
			dateTime.time.minute = DATE__RTC_MIN;
			dateTime.time.hour = DATE__RTC_HOUR;
			dateTime.date.dayOfWeek = DATE__RTC_DOW + 1;
			dateTime.date.base.day = DATE__RTC_DOM;
			dateTime.date.base.month = DATE__RTC_MONTH;
			dateTime.date.base.year = DATE__RTC_YEAR;
		} while (dateTime.time.second != DATE__RTC_SEC);

		date__mrtInst.dayOfWeek = dateTime.date.dayOfWeek;
		date_dtToSerNr(&date__mrtInst.currTime, &dateTime);
		tstamp_getStamp(&date__mrtInst.tickStamp);

		hw_enableIrq(RTC_IRQn);
		LPC_RTC->CIIR = (1<<0); /* Isr on 1 sec */

		/*
		 *	It is not possible to know for sure if the time has been set.
		 *
		 * 	Assuming the time has been set if the current time is at least two
		 * 	years from the epoch date.
		 */

		if (date__mrtInst.currTime.dateSerNr > (365U * 2)) {
			date__mrtInst.flags |= DATE__MRTFLG_TIME_SET;
			rtcStatus = DATE_RTCSTAT_VALID;
		} else {
			rtcStatus = DATE_RTCSTAT_NOT_SET;
		}
	}

	LPC_RTC->CCR |= DATE__CCR_CLKEN;

	if (LPC_RTC->RTC_AUX & (1<<4)) {
		/*
		 * 	The RTC oscillator is still not running. Maybe there is no RTC
		 * 	crystal connected.
		 */

		rtcStatus = DATE_RTCSTAT_ERROR;
	}

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__mrtGetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC.
*
*	\param		pDateTime	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__mrtGetDt(
	date_DateTime *			pDateTime
) {
	date_RtcStatus	rtcStatus;
	date_DtSerNr	dtSerNr;
	Uint8			dayOfWeek;
	
	rtcStatus = date__mrtGetDateTime(&dtSerNr, &dayOfWeek);
	date_dtFromSerNr(pDateTime, &dtSerNr);
	pDateTime->date.dayOfWeek = dayOfWeek;

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__mrtGetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC in serial format.
*
*	\param		pDtSerNr	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__mrtGetDtSerNr(
	date_DtSerNr *			pDtSerNr
) {
	date_RtcStatus	rtcStatus;
	Uint8			dayOfWeek;

	rtcStatus = date__mrtGetDateTime(pDtSerNr, &dayOfWeek);

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__mrtSetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__mrtSetDt(
	date_DateTime const_D * pDateTime
) {
	date_DtSerNr dtSerNr;
	
	date_dtToSerNr(&dtSerNr, pDateTime);
	date__mrtSetDateTime(pDateTime, &dtSerNr);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__mrtSetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time using serial format.
*
*	\param		pDtSerNr	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__mrtSetDtSerNr(
	date_DtSerNr const_D *	pDtSerNr
) {
	date_DateTime dateTime;

	date_dtFromSerNr(&dateTime, pDtSerNr);
	date__mrtSetDateTime(&dateTime, pDtSerNr);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__mrtDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform additional RTC operations.
*
*	\param		op		Code specifying the desired operation.
* 	\param		pParam	Pointer to operation parameters. The parameter type
* 						depends on the requested operation.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__mrtDoOp(
	date_RtcOp				op,
	void *					pParam
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	RTC_IRQHandler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		RTC 1s interrupt service routine.
*
*	\return		-
*
*	\details	
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PUBLIC osa_isrFastFn(NULL, RTC_IRQHandler) {
	osa_isrEnter();

	/*
	 *	Store timestamp so that we can calculate the millisecond part later.
	 */

	tstamp_getStamp(&date__mrtInst.tickStamp);

	date__mrtInst.currTime.timeSerNr += 1000UL;

	if (date__mrtInst.currTime.timeSerNr >= 86400000UL) {
		/*
		 *	One day has elapsed.
		 */

		date__mrtInst.currTime.timeSerNr -= 86400000UL;
		date__mrtInst.currTime.dateSerNr++;

		date__mrtInst.dayOfWeek++;
		if (date__mrtInst.dayOfWeek >= 8) {
			date__mrtInst.dayOfWeek = 1;
		}
	}

	LPC_RTC->ILR = 3;

	osa_isrLeave();
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__mrtSetTimeDate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*	\param		pDtSerNr	Pointer to structure containing the date and time
*							that will be configured to the RTC.
*
*	\details	The two parameters must contain the same date and time.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__mrtSetDateTime(
	date_DateTime const_D * pDateTime,
	date_DtSerNr const_D *	pDtSerNr
) {
	Uint32 running;
	Uint16 msSinceTick;

	deb_assert(date__mrtInst.flags & DATE__MRTFLG_INIT_DONE);

	DISABLE;

	/*
	 *	Stop the RTC. Updating the RTC while it is running could cause the date
	 *	to become invalid.
	 */

	running = LPC_RTC->CCR & DATE__CCR_CLKEN;
	LPC_RTC->CCR &= ~DATE__CCR_CLKEN;

	/*
	 *	Set the new time and date to the RTC.
	 */

	DATE__RTC_SEC = pDateTime->time.second;
	DATE__RTC_MIN = pDateTime->time.minute;
	DATE__RTC_HOUR = pDateTime->time.hour;

	DATE__RTC_DOM = pDateTime->date.base.day;
	DATE__RTC_DOW = pDateTime->date.dayOfWeek - 1;
	DATE__RTC_MONTH = pDateTime->date.base.month;
	DATE__RTC_YEAR = pDateTime->date.base.year;

	/*
	 *	Also store the date to the current time counter variable. The
	 *	millisecond part is not stored to the variable. Milliseconds are handled
	 *	by the offset variable.
	 */

	date__mrtInst.currTime = *pDtSerNr;
	date__mrtInst.currTime.timeSerNr -= pDateTime->time.ms;
	date__mrtInst.dayOfWeek = pDateTime->date.dayOfWeek;

	/*
	 *	Calculate offset and store it. This is the difference between the wanted
	 *	milliseconds and the milliseconds of the RTC.
	 */

	tstamp_getDiffCurr(&date__mrtInst.tickStamp, &msSinceTick);
	date__mrtInst.offset = (Int32)pDateTime->time.ms - (Int32)msSinceTick;

	/*
	 *	Start the RTC again if it was running.
	 */

	LPC_RTC->CCR |= running;

	date__mrtInst.flags |= DATE__MRTFLG_TIME_SET;

	ENABLE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__mrtGetDateTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC in serial format.
*
*	\param		pDtSerNr	Pointer to structure where the current date and time
*							will be stored.
*	\param		pDayOfWeek	Pointer to variable where the day of week will be 
*							stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_NOT_SET	Time has not been set.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.	
*
*	\details	
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PRIVATE date_RtcStatus date__mrtGetDateTime(
	date_DtSerNr *			pDtSerNr,
	Uint8 *					pDayOfWeek
) {
	date_RtcStatus	rtcStatus;
	Int32			offset;
	Uint16			msSinceTick;

	deb_assert(date__mrtInst.flags & DATE__MRTFLG_INIT_DONE);

	DISABLE;

	/*
	 *	The time is not read from the actual RTC here. The current time variable
	 *	is used instead. The current time variable is updated each second in the
	 *	ISR.
	 */

	*pDtSerNr = date__mrtInst.currTime;
	*pDayOfWeek = date__mrtInst.dayOfWeek;

	/*
	 *	Check how much time has elapsed since the last second interrupt.
	 *	Calculate the offset we have to add to the time to get the milliseconds
	 *	part correctly.
	 */

	tstamp_getDiffCurr(&date__mrtInst.tickStamp, &msSinceTick);
	offset = msSinceTick;
	offset += date__mrtInst.offset;

	/*
	 *	Determine if the time and date is correct.
	 */

	rtcStatus = DATE_RTCSTAT_ERROR;
	if ((LPC_RTC->RTC_AUX & (1<<4)) == 0) {
		/*
		 *	The RTC is still running.
		 */

		rtcStatus = DATE_RTCSTAT_NOT_SET;
		if (date__mrtInst.flags & DATE__MRTFLG_TIME_SET) {
			/*
			 *	Seems like the time has been set.
			 */

			rtcStatus = DATE_RTCSTAT_VALID;
		}
	}

	ENABLE;

	/*
	 *	The current time variable only contains the seconds part of the time.
	 *	The milliseconds part have to be added here.
	 */

	date_addOffset(pDtSerNr, offset);

	return(rtcStatus);
}
