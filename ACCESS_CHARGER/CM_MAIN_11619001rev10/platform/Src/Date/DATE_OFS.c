/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Add offset to time and date in serial format.
*
*	\details
*
*	\version	\$Rev: 3081 $ \n
*				\$Date: 2016-11-10 16:57:02 +0200 (to, 10 marras 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
; D E C L A R A T I O N M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "date.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * Number of milliseconds in one day.
 */

#define DATE__DAY_MS	(1000*60*60*24)

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_addOffset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Add the supplied offset to the date and Time.
*
*	\param		pDtSerNr	The dateTime structure to be modified.
*	\param		offset		The amount of milliseconds to add to the dateTime
*							structure.
*
*	\details	The offset may also be negative.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void date_addOffset(
	date_DtSerNr *			pDtSerNr,
	Int32					offset
) {
	Uint16 days;

	if ((offset < 0) && (pDtSerNr->timeSerNr < (Uint32)-offset)) {
		/*
		 *	The offset is negative and large enough to change the date.
		 */

		days = (Uint16)((Uint32)-offset / DATE__DAY_MS);
		offset = offset + days * DATE__DAY_MS;

		/*
		 *	Subtract whole days.
		 */

		pDtSerNr->dateSerNr -= days;

		if (pDtSerNr->timeSerNr < (Uint32)-offset) {
			/*
			 *	The offset is still large enough to change the date.
			 */

			pDtSerNr->dateSerNr--;
			pDtSerNr->timeSerNr += DATE__DAY_MS;
		}
	}

	pDtSerNr->timeSerNr += offset;

	if (pDtSerNr->timeSerNr >= DATE__DAY_MS) {
		/*
		 *	Adding the offset caused the millisecond counter to go past one
		 *	whole day.
		 */

		days = (Uint16)(pDtSerNr->timeSerNr / DATE__DAY_MS);
		pDtSerNr->dateSerNr += days;
		pDtSerNr->timeSerNr -= days * DATE__DAY_MS;
	}
}
