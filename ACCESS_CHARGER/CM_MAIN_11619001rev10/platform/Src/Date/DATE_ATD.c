/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		String to date and time conversion.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3530 $ \n
*				\$Date:: 2018-06-27 14:33:49 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "util.h"
#include "DATE.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_aToDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert string in ISO 8601 format to date and time structure.
*
*	\param		pDt		Pointer to date and time object where the parsed data 
*						will be stored.
*	\param		pDtStr	Pointer to date and time string.
*	\param		len		Length of the string.
*
*	\details	The function does not validate the date and time values in any
*				way. date_validateDt() may be used to validate the parsed
*				values.
*
*				Example strings:
*				2018-06-15T12:57:12.222
*				2018-06-15T12:57:12
*				2018-06-15T12:57
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC char const_D * date_aToDt(
	date_DateTime *			pDt,
	char const_D *			pDtStr,
	Uint8					len
) {
	char const_D * pParse;

	/*
	 *	Start by converting the date part.
	 */

	pDt->date.dayOfWeek = 0;
	pParse = date_aToDate(&pDt->date.base, pDtStr, len);

	if (pParse != pDtStr) {
		/*
		 *	The date part was parsed successfully.
		 */

		if (*pParse == 'T') {
			/*
			 * The string seems to also contain the time.
			 */

			pParse++;
			len = len - (pParse - pDtStr);

			pParse = date_aToTime(&pDt->time, pParse, len);

		} else {
			/*
			 *	No time separator found (T). Setting time to midnight.
			 */

			pDt->time.hour = 0;
			pDt->time.minute = 0;
			pDt->time.second = 0;
			pDt->time.ms = 0;	
		}
	}

	return(pParse);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_aToDate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert string in ISO 8601 format to date structure.
*
*	\param		pDate		Pointer to date object where the parsed data 
*							will be stored.
*	\param		pDateStr	Pointer to date string.
*	\param		len			Length of the string.
*
*	\return		Pointer to the next character in the string after the date.
*
*				The function does not validate the date values in any way.
*				date_validate() may be used to validate the parsed date.
*
*	\details	Example strings:
*				20180615		
*				2018-06-15		(Extended format)
*				2018-06			(Extended format without days)
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC char const_D * date_aToDate(
	date_Date *				pDate,
	char const_D *			pDateStr,
	Uint8					len
) {
	char const_D * pParse;

	pParse = pDateStr;

	pDate->year = 0;
	pDate->month = 1;
	pDate->day = 1;

	if (
		len >= 4
		&& util_isDecDigit(pParse[0])
		&& util_isDecDigit(pParse[1])
		&& util_isDecDigit(pParse[2])
		&& util_isDecDigit(pParse[3])
	) {
		/*
		 *	Four year digits found in the string. Proceeding by converting them
		 *	to a numeric value.
		 */

		pDate->year = (Uint16) util_atou32dec(pParse, 4, &pParse);

		if (
			len >= 1
			&& *pParse == '-'
		) {
			/*
			 *	The extended format '-' separator was found. Step over the 
			 *	separator.
			 */

			pParse++;
			len--;
		}

		if (
			len >= 2
			&& util_isDecDigit(pParse[0])
			&& util_isDecDigit(pParse[1])
		) {
			/*
			 *	Two month digits found in the string. Proceeding by converting
			 *	them to a numeric value.
			 */

			pDate->month = (Uint8) util_atou32dec(pParse, 2, &pParse);

			if (
				len >= 1
				&& *pParse == '-'
			) {
				/*
				 *	The extended format '-' separator was found. Step over the 
				 *	separator.
				 */

				pParse++;
				len--;
			}

			if (
				len >= 2
				&& util_isDecDigit(pParse[0])
				&& util_isDecDigit(pParse[1])
			) {
				/*
				 *	Two day digits found in the string. Proceeding by converting
				 *	them to a numeric value.
				 */

				pDate->day = (Uint8) util_atou32dec(pParse, 2, &pParse);
			}
		}
	}

	return(pParse);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_aToTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert string in ISO 8601 format to time structure.
*
*	\param		pTime		Pointer to time structure where the parsed data will
*							be stored.
*	\param		pTimeStr	Pointer to string.
*	\param		len			Length of the string.
*
*	\return		Pointer to the next character in the string after the time.
*
*	\details	The string can be in extended format (hh:mm:ss.fff) or without
*				separators (hhmmssfff).
*
*				The function does not validate the time values in any way.
*				date_validateTime() may be used to validate the parsed time.
*
*	\par		Example strings:
*				125722234
*				15:57:22.234		(Extended format)
*				12:57:22			(Extended format without milliseconds)
*				12:57				(Extended format without seconds)
*				
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC char const_D * date_aToTime(
	date_Time *				pTime,
	char const_D *			pTimeStr,
	Uint8					len
) {
	char const_D * pParse;

	pParse = pTimeStr;

	pTime->hour = 0;
	pTime->minute = 0;
	pTime->second = 0;
	pTime->ms = 0;

	if (
		len >= 2
		&& util_isDecDigit(pParse[0])
		&& util_isDecDigit(pParse[1])
	) {
		/*
		 *	Two hour digits found in the string. Proceeding by converting them
		 *	to a numeric value.
		 */

		pTime->hour = (Uint8) util_atou32dec(pTimeStr, 2, &pParse);
		len -= 2;

		if (len >= 1 && *pParse == ':') {
			/*
			 *	Colon found. Skip it.
			 */

			pParse++;
			len--;
		}

		if (
			len >= 2
			&& util_isDecDigit(pParse[0])
			&& util_isDecDigit(pParse[1])
		) {
			/*
			 *	Two minute digits found in the string. Proceeding by converting
			 *	them to a numeric value.
			 */

			pTime->minute = (Uint8) util_atou32dec(pParse, 2, &pParse);
			len -= 2;

			if (len >= 1 && *pParse == ':') {
				/*
				 *	Colon found. Skip it.
				 */
				pParse++;
				len--;
			}
		
			if (
				len >= 2
				&& util_isDecDigit(pParse[0])
				&& util_isDecDigit(pParse[1])
			) {
				/*
				 *	Two second digits found in the string. Proceeding by
				 *	converting them to a numeric value.
				 */

				pTime->second = (Uint8) util_atou32dec(pParse, 2, &pParse);
				len -= 2;

				if (len >= 1 && *pParse == '.') {
					/*
					 *	Decimal dot found. Skip it.
					 */
					pParse++;
					len--;
				}

				if (
					len >= 3
					&& util_isDecDigit(pParse[0])
					&& util_isDecDigit(pParse[1])
					&& util_isDecDigit(pParse[2])
				) {
					/*
					 *	Three millisecond digits was found. Proceeding by
					 *	converting them to a numeric value.
					 */

					pTime->ms = (Uint16) util_atou32dec(pParse, 3, &pParse);
				}
			}
		}
	}

	return(pParse);
}
