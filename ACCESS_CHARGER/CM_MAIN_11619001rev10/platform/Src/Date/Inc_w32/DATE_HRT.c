/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Windows RTC handling.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3530 $ \n
*				\$Date:: 2018-06-27 14:33:49 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <time.h>
#include <Windows.h>

#include "tools.h"
#include "deb.h"
#include "DATE.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/**
 * Type for RTC state information.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Boolean			initCalled;			/**< True if init has ben called.	*/
	Int32			dateOffset;			/**< Date offset from system time.	*/
	Int32			timeOffset;			/**< Time offset from system time.	*/
} date__HwRtcInst;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE date_RtcStatus	date__hwRtcGetDt(date_DateTime *);
PRIVATE date_RtcStatus	date__hwRtcGetDtSerNr(date_DtSerNr *);
PRIVATE void			date__hwRtcSetDt(date_DateTime const_D *);
PRIVATE void			date__hwRtcSetDtSerNr(date_DtSerNr const_D *);
PRIVATE void			date__hwRtcDoOp(date_RtcOp,void * pParam);

PRIVATE date_RtcStatus	date__hwRtcGetSystemTime(date_DateTime *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Interface constant for the internal RTC peripheral.
 */

PUBLIC date_RtcIf const_P date_internalRtc = {
	&date__hwRtcGetDt,
	&date__hwRtcGetDtSerNr,
	&date__hwRtcSetDt,
	&date__hwRtcSetDtSerNr,
	&date__hwRtcDoOp
};

/**
 *	Interface constant for the internal RTC peripheral with additional
 *	millisecond resolution.
 */

PUBLIC date_RtcIf const_P date_internalMsRtc = {
	&date__hwRtcGetDt,
	&date__hwRtcGetDtSerNr,
	&date__hwRtcSetDt,
	&date__hwRtcSetDtSerNr,
	&date__hwRtcDoOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE date__HwRtcInst date__hwRtcInst = {FALSE, 0, 0};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_internalRtcInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the RTC.
*
*	\return		Always returns DATE_RTCSTAT_VALID.
*
*	\details
*
*	\note		Must be called before any call through the date_internalRtc 
*				interface.
*
*	\sa
*
*******************************************************************************/

PUBLIC date_RtcStatus date_internalRtcInit(
	void
) {
	date__hwRtcInst.initCalled = TRUE;

	return(DATE_RTCSTAT_VALID);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_internalMsRtcInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize thee RTC with millisecond resolution.
*
*	\return		Always returns DATE_RTCSTAT_VALID.
*
*	\details
*
*	\note		Must be called before any call through the date_internalMsRtc 
*				interface.
*
*	\sa
*
*******************************************************************************/

PUBLIC date_RtcStatus date_internalMsRtcInit(
	void
) {
	return(date_internalRtcInit());
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcGetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC.
*
*	\param		pDateTime	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__hwRtcGetDt(
	date_DateTime *			pDateTime
) {
	date_RtcStatus	rtcStatus;
	date_DtSerNr	dtSerNr;

	rtcStatus = date__hwRtcGetDtSerNr(&dtSerNr);
	date_dtFromSerNr(pDateTime, &dtSerNr);

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcGetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC in serial format.
*
*	\param		pDtSerNr	Pointer to structure where the current date and time
*							will be stored.
*
*	\retval		DATE_RTCSTAT_VALID		The time and date is valid and accurate.
*	\retval		DATE_RTCSTAT_ERROR		RTC malfunction.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__hwRtcGetDtSerNr(
	date_DtSerNr *			pDtSerNr
) {
	date_DateTime	dateTime;
	date_RtcStatus	rtcStatus;
	Int32			dateOffset;
	Int32			timeOffset;

	/*
	 *	First get the system time.
	 */

	rtcStatus = date__hwRtcGetSystemTime(&dateTime);
	date_dtToSerNr(pDtSerNr, &dateTime);

	/*
	 *	Add the offset.
	 */

	DISABLE;
	dateOffset = date__hwRtcInst.dateOffset;
	timeOffset = date__hwRtcInst.timeOffset;
	ENABLE;

	pDtSerNr->dateSerNr = (date_SerNr)((Int32)pDtSerNr->dateSerNr + dateOffset);
	date_addOffset(pDtSerNr, timeOffset);

	return(rtcStatus);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcSetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcSetDt(
	date_DateTime const_D * pDateTime
) {
	date_DtSerNr dtSerNr;

	date_dtToSerNr(&dtSerNr, pDateTime);
	date__hwRtcSetDtSerNr(&dtSerNr);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcSetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Set RTC date and time using serial format.
*
*	\param		pDtSerNr	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcSetDtSerNr(
	date_DtSerNr const_D *	pDtSerNr
) {
	date_DtSerNr	systemDtSerNr;
	date_DateTime	systemDt;

	/*
	 *	Setting the system clock is not desired here. We just calculate an
	 *	offset from the system time and use that offset when the time is being
	 *	read.
	 */

	date__hwRtcGetSystemTime(&systemDt);
	date_dtToSerNr(&systemDtSerNr, &systemDt);

	DISABLE;
	date__hwRtcInst.dateOffset =
		(Int32)pDtSerNr->dateSerNr - (Int32)systemDtSerNr.dateSerNr;

	date__hwRtcInst.timeOffset =
		(Int32)pDtSerNr->timeSerNr - (Int32)systemDtSerNr.timeSerNr;
	ENABLE;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform additional RTC operations.
*
*	\param		op		Code specifying the desired operation.
*	\param		pParam	Pointer to operation parameters. The parameter type
*						depends on the requested operation.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__hwRtcDoOp(
	date_RtcOp				op,
	void *					pParam
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__hwRtcGetSystemTime
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get system time.
*
*	\param		pDateTime		Pointer to date and time object where the system
*								time will be stored.
*
*	\details	This function will fetch the system time from Windows and
*				convert it to T-Plat.E format.
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PRIVATE date_RtcStatus date__hwRtcGetSystemTime(
	date_DateTime *			pDateTime
) {
	date_RtcStatus	rtcStatus;
	SYSTEMTIME		time;

	GetLocalTime(&time);

	pDateTime->time.ms = (Uint16) time.wMilliseconds;
	pDateTime->time.second = (Uint8) time.wSecond;
	pDateTime->time.minute = (Uint8) time.wMinute;
	pDateTime->time.hour = (Uint8) time.wHour;
	pDateTime->date.dayOfWeek = (Uint8) time.wDayOfWeek;
	pDateTime->date.base.day = (Uint8) time.wDay;
	pDateTime->date.base.month = (Uint8) time.wMonth;
	pDateTime->date.base.year = (Uint16) time.wYear;

	if (pDateTime->date.dayOfWeek == 0) {
		pDateTime->date.dayOfWeek = 7U;
	}

	rtcStatus = DATE_RTCSTAT_ERROR;
	if (date__hwRtcInst.initCalled) {
		rtcStatus = DATE_RTCSTAT_VALID;
	}

	return(rtcStatus);
}