/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Time and date validation functions.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3530 $ \n
*				\$Date:: 2018-06-27 14:33:49 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "DATE.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Boolean date__isLeapYear(Uint16 year);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint8 const_P date__valMonthDays[] = {
	31,	/* January */
	29,	/* February (allowing leap years) */
	31,	/* March */
	30,	/* April */
	31,	/* May */
	30, /* June */
	31,	/* July */
	31,	/* August */
	30,	/* September */
	31,	/* October */
	30,	/* November */
	31	/* December */
};

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_validateDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Validate the supplied date and time.
*
*	\param		pDt		Pointer to date and time object.
*
*	\details	
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC Boolean date_validateDt(
	date_DateTime const_D *	pDt
) {
	Boolean isValid;

	isValid = date_validateTime(&pDt->time);

	if (isValid) {
		isValid = FALSE;

		if (pDt->date.dayOfWeek <= 7) {
			isValid = date_validate(&pDt->date.base);
		}
	}

	return(isValid);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_validate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Validate the supplied date.
*
*	\param		pDate		Pointer to the date that should be validated.
*
*	\details	The function checks that the month value is valid and that the
*				day value does not exceed the allowed days for that month. 29th
*				February is only allowed on leap years.
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC Boolean date_validate(
	date_Date const_D *		pDate
) {
	Boolean isValid;

	/*
	 *	First check that the month and day fields are valid.
	 */

	isValid = FALSE;

	if (pDate->month >= 1U && pDate->month <= 12U) {
		if (
			pDate->day >= 1U
			&& pDate->day <= date__valMonthDays[pDate->month-1]
		) {
			isValid = TRUE;

			if (pDate->month == 2 && pDate->day == 29) {
				Boolean isLeapYear;

				/*
				 *	This day is only valid for leap years. Verify that this is
				 *	a leap year.
				 */

				isLeapYear = date__isLeapYear(pDate->year);

				if (isLeapYear == FALSE) {
					isValid = FALSE;
				}
			}
		}
	}

	return(isValid);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_validateSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Check that the supplied date can be converted to the
*				serial format.
*
*	\param		pDate		The date to be checked.
*
*	\retval		TRUE		The date can be converted.
*	\retval		FALSE		The date can not be converted to the serial format.
*
*	\details	The serial format cannot be used to store dates earlier than the
*				epoch date. It also cannot store dates later than 179 years past
*				the epoch.
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC Boolean date_checkSerNr(
	date_Date const_D *		pDate
) {
	Boolean		isValid;
	date_Date	checkDate;
	date_SerNr	epochSerial;

	epochSerial = 0;
	date_fromSerNr(&checkDate, epochSerial);

	isValid = FALSE;

	if (pDate->year > checkDate.year) {
		date_SerNr lastSerial;

		/*
		 *	The supplied date is later than the epoch date. Get the last date
		 *	that can be represented with the serial format.
		 */

		lastSerial = 0xFFFFU;
		date_fromSerNr(&checkDate, lastSerial);

		if (pDate->year < checkDate.year) {
			isValid = TRUE;

		} else if (pDate->year == checkDate.year) {
			/*
			 *	This is the last supported year. We have to check the month
			 *	also.
			 */

			if (pDate->month < checkDate.month) {
				isValid = TRUE;

			} else if (pDate->month == checkDate.month) {
				/*
				 *	This is the last supported month. We have to check the day
				 *	also.
				 */

				if (pDate->day <= checkDate.day) {
					isValid = TRUE;
				} 
			}
		}

	} else if (pDate->year == checkDate.year) {
		/*
		 *	Same year as the epoch. We have to check the month also.
		 */

		if (pDate->month > checkDate.month) {
			isValid = TRUE;

		} else if (pDate->month == checkDate.month) {
			/*
			 *	Same month as the epoch. We have to check the day also.
			 */

			if (pDate->day >= checkDate.day) {
				isValid = TRUE;
			}
		}
	}

	return(isValid);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__isLeapYear
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Checks if the supplied year is a leap year.
*
*	\param		year		The year to be checked.
*
*	\retval		TRUE		The year is a leap year.
*	\retval		FALSE		The year is not a leap year.
*
*	\details	
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PRIVATE Boolean date__isLeapYear(
	Uint16					year
) {
	Boolean isLeapYear;

	isLeapYear = FALSE;

	if (year % 4 == 0) {
		if (year % 100 != 0 || year % 400 == 0) {
			isLeapYear = TRUE;
		}
	}

	return(isLeapYear);
}
