/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Time conversion support.
*
*	\details
*
*	\version	\$Rev: 3081 $ \n
*				\$Date: 2016-11-10 16:57:02 +0200 (to, 10 marras 2016) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
; D E C L A R A T I O N M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "date.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_timeFromSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert milliseconds since midnight to time structure.
*
*	\param		pTime		Pointer to time structure that will be filled.
*	\param		milliSecs	Milliseconds since midnight.
*
*	\return		Pointer to the filled structure.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC date_Time * date_timeFromSerNr(
	date_Time *				pTime,
	date_TimeSerNr			milliSecs
) {
	Ufast8	temp;
	Ufast16	u16milliSec;

	temp = (Ufast8) (milliSecs / (1000UL*60UL*60UL));
	milliSecs -= temp * (1000UL*60UL*60UL);
	pTime->hour = (Uint8) temp;

	temp = (Ufast8) (milliSecs / (1000UL*60UL));
	u16milliSec = (Ufast16) (milliSecs - temp * (1000UL*60UL));
	pTime->minute = (Uint8) temp;

	temp = (Ufast8) (u16milliSec / 1000U);
	u16milliSec -= temp * 1000U;
	pTime->second = (Uint8) temp;

	pTime->ms = (Uint16) u16milliSec;

	return(pTime);
}
