/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		date and time to string conversion.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3529 $ \n
*				\$Date:: 2018-06-19 14:53:01 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "DATE.H"
#include "util.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_dtToa
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert date and time object to a string in ISO 8601 format.
*
*	\param		pDtStr	Pointer to buffer where the string will be built.
*	\param		len		Length of the buffer.
*	\param		pDt		Pointer to date and time object.
*
*	\return		Pointer to the next character in the buffer after the date and
*				time string.
*
*	\details	The string will be in extended format. 2018-06-15T12:57:12.222.
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC char * date_dtToa(
	char *					pDtStr,
	Uint8					len,
	date_DateTime *			pDt
) {
	char * pEnd;

	pEnd = date_dateToa(pDtStr, len, &pDt->date.base);

	if (pEnd != pDtStr) {
		char const_D * pTemp;

		/*
		 *	The date was converted successfully. Add the 'T' delimiter.
		 */

		*pEnd++ = 'T';

		/*
		 *	Continue by adding the time string.
		 */

		len -= pEnd - pDtStr;
		pTemp = pEnd;
		pEnd = date_timeToa(pEnd, len, &pDt->time);

		if (pEnd == pTemp) {
			/*
			 *	Conversion failed. The pDtStr parameter should be returned.
			 */

			pEnd = pDtStr;
		}
	}

	return(pEnd);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_dateToa
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert date object to a string in ISO 8601 format.
*
*	\param		pDateStr	Pointer to buffer where the string will be built.
*	\param		len			Length of the buffer.
*	\param		pDate		Pointer to date object with the desired date 
*							information.
*
*	\return		Pointer to the next character in the buffer after the date 
*				string.
*
*	\details	The string will be in extended format (YYYY-MM-DD).
*
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC char * date_dateToa(
	char *					pDateStr,
	Uint8					len,
	date_Date *				pDate
) {
	char * pEnd;
	char * pStart;

	pEnd = pDateStr;

	if (len >= 4) {
		/*
		 *	The year will fit on the buffer. Add leading zeros and the year
		 *	digits.
		 */

		*pEnd++ = '0';
		*pEnd++ = '0';
		*pEnd++ = '0';
		pEnd++;
		util_u16toadec(pDate->year, pDateStr, pEnd);

		if (len >= 7) {
			/*
			 *	The month will fit on the buffer. Add delimiter and month
			 *	digits.
			 */

			*pEnd++ = '-';
			pStart = pEnd;

			/*
			 *	Add leading zero and the month digits.
			 */

			*pEnd++ = '0';
			pEnd++;
			util_u8toadec(pDate->month, pStart, pEnd);

			if (len >= 10) {
				/*
				 *	The day will fit on the buffer. Add delimiter and day
				 *	digits.
				 */

				*pEnd++ = '-';
				pStart = pEnd;

				/*
				 *	Add leading zero and the day digits.
				 */

				*pEnd++ = '0';
				pEnd++;
				util_u8toadec(pDate->day, pStart, pEnd);
			}
		}
	}

	return(pEnd);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date_timeToa
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert time object to a string in ISO 8601 format.
*
*	\param		pTimeStr	Pointer to buffer where the string will be built.
*	\param		len			Length of the buffer.
*	\param		pTime		Pointer to time object with the desired time 
*							information.
*
*	\return		Pointer to the next character in the buffer after the time 
*				string.
*
*	\details	The string will be in extended format (hh:mm:ss.fff).
*				
*	\note		
*
*	\sa			
*
*******************************************************************************/

PUBLIC char * date_timeToa(
	char *					pTimeStr,
	Uint8					len,
	date_Time *				pTime
) {
	char * pEnd;
	char * pStart;

	pEnd = pTimeStr;

	if (len >= 2) {
		/*
		 *	The hour digits will fit on the buffer. Add leading zero and the
		 *	hour digits.
		 */

		*pEnd++ = '0';
		pEnd++;
		util_u8toadec(pTime->hour, pTimeStr, pEnd);


		if (len >= 5) {
			/*
			 *	The minute digits will fit on the buffer. Add delimiter.
			 */

			*pEnd++ = ':';
			pStart = pEnd;

			/*
			 *	Add leading zero and the minute digits.
			 */

			*pEnd++ = '0';
			pEnd++;
			util_u8toadec(pTime->minute, pStart, pEnd);


			if (len >= 8) {
				/*
				 *	The second digits will fit on the buffer. Add delimiter.
				 */

				*pEnd++ = ':';
				pStart = pEnd;

				/*
				 *	Add leading zero and the second digits.
				 */

				*pEnd++ = '0';
				pEnd++;
				util_u8toadec(pTime->second, pStart, pEnd);


				if (len >= 12) {
					/*
					 *	The millisecond digits will fit on the buffer. Add
					 *	decimal dot.
					 */

					*pEnd++ = '.';
					pStart = pEnd;

					/*
					 *	Add leading zero and the millisecond digits.
					 */

					*pEnd++ = '0';
					*pEnd++ = '0';
					pEnd++;
					util_u16toadec(pTime->ms, pStart, pEnd);
				}
			}
		}
	}

	return(pEnd);
}
