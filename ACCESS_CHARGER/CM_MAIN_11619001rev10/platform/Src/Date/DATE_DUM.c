/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DATE
*
*	\brief		Dummy RTC interface for projects where time does not matter.
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: 3527 $ \n
*				\$Date:: 2018-06-15 14:12:59 #$ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "DATE.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE date_RtcStatus	date__dummyRtcGetDt(date_DateTime *);
PRIVATE date_RtcStatus	date__dummyRtcGetDtSerNr(date_DtSerNr *);
PRIVATE void			date__dummyRtcSetDt(date_DateTime const_D *);
PRIVATE void			date__dummyRtcSetDtSerNr(date_DtSerNr const_D *);
PRIVATE void			date__dummyRtcDoOp(date_RtcOp,void * pParam);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 *	Public interface constant for dummy RTC implementation. The implementation
 *	always returns epoch date and midnight time. The clock never
 *	increments.
 *	
 *	This is useful in projects where the date and time is not important but some
 *	function block is depending on a RTC implementation e.g. a file system.
 *
 *	No init function call is needed for this RTC implementation.
 */

PUBLIC date_RtcIf const_P date_dummyRtc = {
	&date__dummyRtcGetDt,
	&date__dummyRtcGetDtSerNr,
	&date__dummyRtcSetDt,
	&date__dummyRtcSetDtSerNr,
	&date__dummyRtcDoOp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__dummyRtcGetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC.
*
*	\param		pDateTime	Pointer to structure where the current date and time
*							will be stored.
*
*	\returns	The function always returns DATE_RTCSTAT_NOT_SET because this is
*				not the correct date and time.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__dummyRtcGetDt(
	date_DateTime *			pDateTime
) {
	date_DtSerNr dtSerNr;

	/*
	 *	Using epoch date.
	 */

	dtSerNr.dateSerNr = 0;
	dtSerNr.timeSerNr = 0;
	date_dtFromSerNr(pDateTime, &dtSerNr);

	return(DATE_RTCSTAT_NOT_SET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__dummyRtcGetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get date and time from RTC in serial format.
*
*	\param		pDtSerNr	Pointer to structure where the current date and time
*							will be stored.
*
*	\returns	The function always returns DATE_RTCSTAT_NOT_SET because this is
*				not the correct date and time.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE date_RtcStatus date__dummyRtcGetDtSerNr(
	date_DtSerNr *			pDtSerNr
) {
	pDtSerNr->dateSerNr = 0;
	pDtSerNr->timeSerNr = 0;

	return(DATE_RTCSTAT_NOT_SET);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__dummyRtcSetDt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details	Does nothing.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__dummyRtcSetDt(
	date_DateTime const_D * pDateTime
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__dummyRtcSetDtSerNr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets RTC date and time.
*
*	\param		pDateTime	Pointer to structure containing the date time that
*							will be configured to the RTC.
*
*	\details	Does nothing.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__dummyRtcSetDtSerNr(
	date_DtSerNr const_D *	pDateTime
) {

}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	date__dummyRtcDoOp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform additional RTC operations.
*
*	\param		op		Code specifying the desired operation.
*	\param		pParam	Pointer to operation parameters. The parameter type
*						depends on the requested operation.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void date__dummyRtcDoOp(
	date_RtcOp				op,
	void *					pParam
) {

}
