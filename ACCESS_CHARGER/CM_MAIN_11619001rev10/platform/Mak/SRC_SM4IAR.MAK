################################################################################
#
#	T-Plat.E makefile
#	Copyright (c) 2019, TJK Tietolaite Oy
#
#	Target:		SM4 (STM32F302xC)
#	Compiler:	IAR
#	Make:		Microsoft NMAKE
#
#	version:	$Rev$
#				$UtcDate$
#				$Author$
#
################################################################################

#
#	Check if override file exists. If it does include it.
#
!IF EXIST($(TLLIB_ROOT)\exe\mak\OVRD_$(TLLIB_T).inc)
! INCLUDE $(TLLIB_ROOT)\exe\mak\OVRD_$(TLLIB_T).inc
!ENDIF

#
#	Set paths.
#
PATH_CURRENT=$(TLLIB_R)
PATH_PLATFORM=$(TLLIB_L)
!ifdef TLLIB_ROOT
PATH_BLOCKS=$(TLLIB_ROOT)\blocks
PATH_EXE=$(TLLIB_ROOT)\exe
!endif

#
# CPU type
#
!ifndef CPU_TYPE
CPU_TYPE = --cpu Cortex-M4
!endif

#
# Endianness
#
!ifndef CPU_ENDIANNESS
CPU_ENDIANNESS = --endian little
!endif

#
# THUMB
#
!ifndef THUMB
THUMB = --thumb
!endif

#
# FPU flags
#
!ifndef FPUTYPE
FPUTYPE = --fpu VFPv4_sp
!endif

#
# WARNINGS
#
!ifndef WARNINGS
WARNINGS = --diag_suppress=Pe188
!endif

# -Wredundant-decls\
# -Wcast-qual\
# -Wmissing-prototypes\
# -Wmissing-declarations\
#-Wstrict-prototypes

#
# If no build set use DEBUG
#
!ifndef TLLIB_B
TLLIB_B=DEBUG
!endif

#
# Set default OS if no OS has been specified
#
!ifndef TLLIB_T_OS
TLLIB_T_OS = FRE09M4
!endif

#
# If no output format is set then the target name is used.
#
!ifndef TLLIB_OF
TLLIB_OF=$(TLLIB_T)
!endif

#
# Set build dependent flags:
# - output file name
# - debug options
# - optimization level
# - LIB version to link
#

!ifndef libfile
# Output file when compiling a lib
libfile	= $(PATH_CURRENT)\lib\lib_$(TLLIB_OF)_$(TLLIB_T)_$(TLLIB_HW)_$(TLLIB_B).a
!endif

!ifndef lnk_libs
lnk_libs = $(PATH_BLOCKS)\lib\lib_$(TLLIB_OF)_$(TLLIB_T)_$(TLLIB_HW)_$(TLLIB_B).a $(PATH_PLATFORM)\lib\lib_$(TLLIB_OF)_$(TLLIB_T)_$(TLLIB_HW)_$(TLLIB_B).a
!endif

!if "$(TLLIB_B)" == "DEBUG"

! ifndef debug_flags
# n=no, l=low, m=medium, h=high, hs=High speed, hz=high small size
debug_flags = -Ol
! endif

!elseif "$(TLLIB_B)" == "RELEASE"

! ifndef debug_flags
debug_flags = -DNDEBUG -Oh --no_tbaa
! endif

!else
! MESSAGE ---------------------------------------------------
! MESSAGE WARNING: UNRECOGNIZED BUILD $(TLLIB_B)
! MESSAGE ---------------------------------------------------
!endif

#
# Assume STM32F302xC family if not otherwise set
#
!ifndef TLLIB_F
TLLIB_F=STM32F302xC
!endif 

hw_flags = -DSUBTARGET_$(TLLIB_HW) -DTARGETFAMILY_$(TLLIB_F)

#
#	Defines
#
#	__st_cortex_m4__ 	Target info for tools.h
#	$(TLLIB_F)			This selects the correct ST device header.
#	USE_FULL_LL_DRIVER	This enables full code for STM32CUBEF3 LL (low level) drivers in HW FB.
#
!ifndef comp_defines
comp_defines = \
 -D__st_cortex_m4__\
 -D$(TLLIB_F)\
 -DTARGET_OS_$(TLLIB_T_OS)\
 -DUSE_FULL_LL_DRIVER
!endif

#
#	C compiler flags
#
!ifndef c_flags
c_flags	=\
 $(CPU_TYPE)\
 $(CPU_ENDIANNESS)\
 $(THUMB)\
 $(debug_flags)\
 $(FPUTYPE)\
 $(hw_flags)\
 $(comp_defines)\
 --debug\
 --silent\
 -e\
 $(WARNINGS)\
 -l Lst_sm4/iar/
!endif

#
# Assembler flags
#
!ifndef a_flags
a_flags	=\
 $(CPU_TYPE)\
 $(CPU_ENDIANNESS)\
 $(THUMB)\
 $(FPUTYPE)\
 $(hw_flags)\
 $(comp_defines)\
 -t4\
 -s+\
 -r\
 -LLst_sm4/iar/
!endif

#
# Include paths. Support mode for old way if TLLIB_ROOT is not set.
# 
!ifndef c_incs
c_incs =\
 $(INC_DIRS)\
 -I.\
 -I$(PATH_EXE)\
 -I$(PATH_EXE)/Inc/Inc_sm4/iar\
 -I$(PATH_EXE)/Inc/Inc_sm4\
 -I$(PATH_EXE)/inc\
 -I$(PATH_BLOCKS)\
 -I$(PATH_BLOCKS)/Inc/Inc_sm4/iar\
 -I$(PATH_BLOCKS)/Inc/Inc_sm4\
 -I$(PATH_BLOCKS)/inc\
 -I$(PATH_PLATFORM)\
 -I$(PATH_PLATFORM)/Inc/Inc_sm4/iar\
 -I$(PATH_PLATFORM)/Inc/Inc_sm4\
 -I$(PATH_PLATFORM)/Inc/OSA/$(TLLIB_T_OS)\
 -I$(PATH_PLATFORM)/inc

!endif

o		= $(TLLIB_O)
TLLIB_O	= $(TLLIB_B)_$(TLLIB_O)

#
# Output file name (for linker)
#
outfile	= project_$(TLLIB_HW)_$(TLLIB_OF)

#
# Linker flags
#
!ifndef lnk_flags
lnk_flags = \
 $(CPU_TYPE)\
 $(FPUTYPE)\
 --semihosting\
 --map $(outfile).map\
 --silent\
 --entry Reset_Handler
!endif

#
# Using IAR tools
#
CC			= iccarm.exe
LD			= ilinkarm.exe
AS			= iasmarm.exe
AR			= iarchive.exe
ELFTOOL		= ielftool.exe
ELFDUMP		= ielfdumparm.exe

.SUFFIXES	: .o .S .c
.PHONY		: create_dirs obj lib

#
# The linker script should be in the target folder.
#
LDSCRIPT=Inc_sm4\iar\$(TLLIB_HW)_flash.icf

###############################################################################

.s{Obj_$(TLLIB_T)}.$o:
	@echo Compiling assembly file $(<) to $(@)
	@$(AS) $(<) $(c_incs) $(a_flags) -o $(@)

.c{Obj_$(TLLIB_T)}.$o:
	@echo Compiling C file $(<) to $(@) ($o)
	@$(CC) $(<) $(c_incs) $(c_flags) -o $(@)

{inc_sm4}.s{Obj_$(TLLIB_T)}.$o:
	@echo Compiling assembly file $(<) to $(@)
	@$(AS) $(<) $(c_incs) $(a_flags) -o $(@)
	
{inc_sm4}.c{Obj_$(TLLIB_T)}.$o:
	@echo Compiling C file $(<) to $(@)
	@$(CC) $(<) $(c_incs) $(c_flags) -o $(@)

{inc_sm4\iar\}.s{Obj_$(TLLIB_T)}.$o:
	@echo Compiling assembly file $(<) to $(@)
	@$(AS) $(<) $(c_incs) $(a_flags) -o $(@)
	
{inc_sm4\iar\}.c{Obj_$(TLLIB_T)}.$o:
	@echo Compiling C file $(<) to $(@)
	@$(CC) $(<) $(c_incs) $(c_flags) -o $(@)
	
###############################################################################

!IF EXIST(inc_sm4\makefile.inc)
makIncPath = inc_sm4
! INCLUDE inc_sm4\makefile.inc
!ENDIF

!IF EXIST(inc_sm4\iar\makefile.inc)
makIncPath = inc_sm4\iar
! INCLUDE inc_sm4\iar\makefile.inc
!ENDIF

create_dirs :
!IF EXIST(Obj_$(TLLIB_T)) == 0
	@md Obj_$(TLLIB_T)
!ENDIF
!IF EXIST(Lst_sm4/iar) == 0
	@md Lst_sm4\iar
!ENDIF

!ifdef TLLIB_E
################################################################################
# This part is executed under the EXE directory only.
#

platclean :
	@del Obj_$(TLLIB_T)\*.$(TLLIB_O)
	@del $(outfile).elf 
	@del $(outfile).hex
	@del $(outfile).bin

lib : 

obj : create_dirs $(objects)
	@echo Linking to $(outfile).elf>con
!if exist($(TLLIB_E).elf)
	@del $(TLLIB_E).elf
!endif
#
	@$(LD) $(objects) $(lnk_libs) -o $(outfile).elf $(lnk_flags) --config $(LDSCRIPT) 
#
	@echo Creating hex load file $(outfile).hex for flash>con
	@$(ELFTOOL) --ihex --silent $(outfile).elf $(outfile).hex
	@echo Creating binary load file $(outfile).bin for flash>con
	@$(ELFTOOL) --bin --silent $(outfile).elf $(outfile).bin
	@$(ELFDUMP) $(outfile).elf --no_header
# No blank lines before end of file. This makes it possible to add project 
# specific stuff.
#
!else

################################################################################
# This part is executed under the SRC directory only.
#

platclean :
	@del Obj_$(TLLIB_T)\*.$(TLLIB_O)
	@del Lst_sm4\iar\*.$(TLLIB_O).lst
	@del Lst_sm4\*.$(TLLIB_O).lst
	@del /F /Q $(libfile)

obj : create_dirs $(objects)
# Create the lib directory if it doesn't exist
!IF EXIST($(PATH_CURRENT)\lib) == 0
	md $(PATH_CURRENT)\lib
!ENDIF

lib : create_dirs $(objects)
	@echo Adding objects to library $(libfile)
!IF EXIST($(libfile)) == 0
# 	The library file does not exist. Create it.
	@$(AR) --create $(libfile) $(objects)
!ELSE
#	The library file exists. Add or replace objects.
	@$(AR) -r $(libfile) $(objects)
!ENDIF
# No blank lines bofore end of file. This makes it possible to add project 
# specific stuff.
#
!endif