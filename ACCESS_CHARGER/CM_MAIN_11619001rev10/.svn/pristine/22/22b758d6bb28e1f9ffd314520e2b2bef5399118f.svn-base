/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		netif.c
*
*	\ingroup	NETIF
*
*	\brief		Main source file of the network interface FB.
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	NETIF	NETIF
*
*	\brief		Network Interface
*
********************************************************************************
*
*	\details	NETIF is used with the NTCPIP FB. Each instance of NETIF
*				creates one network interface with an own IP-address, subnet
*				mask and default gateway.
*
*				Multiple NETIF instances should be created if there are multiple
*				MAC-driver instances. There could for example be one Ethernet
*				MAC-driver and one serial modem driver (SLIP or PPP).
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>

#include "tools.h"
#include "deb.h"
#include "mem.h"
#include "sys.h"
#include "reg.h"
#include "ntcpip.h"
#include "ndif.h"
#include "netif.h"
#include "snmpa.h"
#include "err.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Task execution triggers
 */

/*@{*/
#define NETIF__TRG_ADDR		(1<<0)		/**< Address change pending			*/
#define NETIF__TRG_REG_RDY	(1<<1)		/**< REG is ready.					*/
#define NETIF__TRG_LINK_UP	(1<<2)		/**< Data layer ling went up.		*/
/*@}*/

#define NETIF__CHAINBUF_LEN 20

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void 		netif__warmInit(void *,ndif_Init const_D *);
PRIVATE Boolean		netif__input(void *,ndif_InData const_D *);
PRIVATE void		netif__activate(void *,ndif_Status const_D *);

PRIVATE ntcpip_Err	netif__netifInit(ntcpip_Netif *);
PRIVATE ntcpip_Err	netif__linkOutput(ntcpip_Netif *,ntcpip_Pbuf *);
PRIVATE void 		netif__updateIpAddr(netif__Inst *);
PRIVATE void 		netif__enableNetif(netif__Inst *);
PRIVATE void		netif__statusCallback(ntcpip_Netif *);
PRIVATE void		netif__regTimeout(swtimer_Timer_st *);
PRIVATE void		netif__regReadyCallback(void * pUtil);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * The public interface constant. A pointer to this constant should be provided
 * to the Ethernet driver. The driver will then call the input function for
 * each received packet. It will also call the status function to report
 * link status changes.
 */

PUBLIC ndif_RxIf const_P netif_rxIf = {
	&netif__warmInit,
	&netif__input,
	&netif__activate
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 * 	The complete send queue can probably be sent at once. The following check
 * 	ensures that the chain buffer is large enough for the whole send queue.
 */
#if NETIF__CHAINBUF_LEN <= NTCPIP__TCP_SND_QUEUELEN
#error "Chain buffer not large enough for the whole send queue!"
#endif

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		iid 	An index (0..) that is accepted by various functions to
*						identify the instance. Should be kept for later use, or
*						may be discarded if not required by the FB.
*	\param		pArgs 	Pointer to the initialization arguments.
*
*	\return		Pointer to the instance i.e. to its private data. This pointer
*				will be passed to all the other functions of the SYS interface
*				as it identifies the instance in question.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * netif_init(
	sys_FBInstId			iid,
	void const_P *			pArgs
) {
	netif__Inst * pInst;

	pInst = (netif__Inst *) mem_reserve(&mem_normal, sizeof(netif__Inst));
	deb_assert(pInst != NULL);

	pInst->pInit = (netif_Init const_P *) pArgs;
	pInst->instId = iid;
	pInst->status = 0;
	pInst->pNdifInit = NULL;
	pInst->confIp = 0;
	pInst->dhcpMode = NETIF_DHCPMODE_AUTO;

	swtimer_new(&pInst->bootTimer, netif__regTimeout, 10000, 0);
	swtimer_setUtilPtr(&pInst->bootTimer, pInst);
	swtimer_start(&pInst->bootTimer);

	pInst->regReadyCall.flags = OSA__SYNC_REG;
	pInst->regReadyCall.fn = netif__regReadyCallback;
	pInst->regReadyCall.pUtil = pInst;
	osa_syncCall(&pInst->regReadyCall);

	return(pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif_write
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write indication function used by an external program to
*				indicate and ask permission to write the value of a parameter
*				(not necessarily owned by this FB).
*
*	\param		instance 	Pointer to the data of the instance.
*	\param		rIndex		Register index.
*	\param		aIndex 		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC sys_IndStatus netif_write(
	void *					instance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex
) {
	sys_IndStatus retVal = REG_IND_OK;
	netif__Inst * pInst;

	DUMMY_VAR(aIndex);

	pInst = (netif__Inst *) instance;

	switch(rIndex) {
		case reg_indic_ipAddr:
		case reg_indic_gateway:
		case reg_indic_netmask:
		case reg_indic_dhcpMode:
			if (pInst->pNdifInit != NULL) {
				pInst->triggers |= NETIF__TRG_ADDR;

				/*
				 *	Request a function call from the MAC-driver task.
				 */

				pInst->pNdifInit->pTxIf->pGetActFn(pInst->pNdifInit->pMacInst);
			}
			break;

		default:
			retVal = REG_IND_ERR;
			break;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif_read
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read indication function used by an external program to read the
*				value of a parameter owned by this FB.
*
*	\param		pInstance 	Pointer to the data of the instance.
*	\param		rIndex 		Register index (0..) according to the indication
*							list defined for this FB.
*	\param		aIndex 		Array index. May be omitted unless the the register
*							is an array. (For non-arrays the value is zero.)
*	\param		pValue 		Pointer to the storage for the value.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC sys_IndStatus netif_read(
	void *					pInstance,
	sys_IndIndex			rIndex,
	sys_ArrIndex			aIndex,
	void *					pValue
) {
	netif__Inst * pInst;

	DUMMY_VAR(aIndex);

	pInst = (netif__Inst *) pInstance;

	switch (rIndex) {
		case reg_indic_ipStatus: {
			Uint32 currIpAddr;
			Uint32 confIpAddr;
			Uint8 dhcpMode;
			Uint8 * pTypedValue;

			/*
			 *	Reading the ip-address here even though it could be changed at
			 *	any time from tcpip task (It is changed very rarely). The
			 *	address in the netif struct is in network byte order. The
			 *	register should return the ip-address in host byte order.
			 *
			 *	If a more robust solution is needed then it could maybe be done
			 *	with net network interface status callback.
			 */

			atomic(
				currIpAddr = pInst->netif.ip_addr.addr;
				confIpAddr = pInst->confIp;
				dhcpMode = pInst->dhcpMode;
			);

			pTypedValue = (Uint8 *) pValue;

			*pTypedValue = 0;

			if (dhcpMode == NETIF_DHCPMODE_ENABLED || confIpAddr == 0) {

				if (dhcpMode == NETIF_DHCPMODE_ENABLED) {
					*pTypedValue = NETIF_IPSTAT_DHCPMODE;
				}

				if ((currIpAddr & 0x0000ffffUL) == 0x0000fea9UL) {
					*pTypedValue |= NETIF_IPSTAT_LINKLOCAL;
				} else {
					*pTypedValue |= NETIF_IPSTAT_DHCP;
				}
			}
			break;
		}

		case reg_indic_currIpAddr: {
			Uint32 ipAddr;
	
			atomic(
				ipAddr = pInst->netif.ip_addr.addr;
			);

			ipAddr = ntcpip_ntohl(ipAddr);
			*((Uint32 *) pValue) = ipAddr;
			break;
		}

		case reg_indic_currNetmask: {
			Uint32 mask;
			Ufast8 networkBits;

			atomic(
				mask = pInst->netif.netmask.addr;
			);

			mask = ntcpip_ntohl(mask);
			for (networkBits = 0; mask != 0; networkBits++) {
				mask <<= 1;
			}

			*((Uint8 *) pValue) = (Uint8) networkBits;
			break;
		}

		case reg_indic_currGateway: {
			Uint32 gateway;

			atomic(
				gateway = pInst->netif.gw.addr;
			);

			gateway = ntcpip_ntohl(gateway);
			*((Uint32 *) pValue) = gateway;
			break;
		}

		default:
			return REG_UNAVAIL;
	}

	return(REG_IND_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__warmInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create an instance of the FB.
*
*	\param		pVoidInst	Pointer to the FB instance.
*	\param		pInit		Pointer to initialization values.
*
*	\return		-
*
*	\details	This function is called by the MAC driver.
*
*	\note
*
*******************************************************************************/

PRIVATE void netif__warmInit(
	void *					pVoidInst,
	ndif_Init const_D *		pNdifInit
) {
	netif__Inst * 			pInst;
	struct ntcpip_ipAddr	dummyAddr;

	pInst = (netif__Inst *) pVoidInst;

	if (pInst != NULL) {

		atomic(
			pInst->pNdifInit = pNdifInit;
		);

		dummyAddr.addr = 0;

		ntcpip_netifapiNetifAdd(
			&pInst->netif,
			&dummyAddr,
			NULL,
			NULL,
			pInst,
			&netif__netifInit,
			&ntcpip_tcpipInput
		);

		if (pNdifInit->flags & NDIF_OPT_DEFAULT) {
			ntcpip_netifapiNetifSetDefault(&pInst->netif);
		}

	} else {
		deb_assert(0);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__statusCallback
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Handle netif status change.
*
*	\param		pNetif		Pointer to the netif structure.
*
*	\details
*
*	\note		Called from the TCP/IP-stack task when the network interface
*				status changes (is set "up" or "down"). IP-configuration is done
*				when the interface status is set to "up".
*
*	\sa
*
*******************************************************************************/

PRIVATE void netif__statusCallback(
	ntcpip_Netif *			pNetif
) {
	netif__Inst * pInst;

	pInst = (netif__Inst *) pNetif->state;

	if (ntcpip__netifIsUp(pNetif)) {
		if (ntcpip_ipaddrIsLinkLocal(&pNetif->ip_addr)) {
			err_error(pInst->instId, 0, NETIF__ERR_LINK_LOCAL);
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__updateIpAddr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Update the ip-address of the network interface.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		-
*
*	\details	The function will also update the default gateway and the
*				subnet mask.
*
*	\note		
*
*******************************************************************************/

PRIVATE void netif__updateIpAddr(
	netif__Inst *			pInst
) {
	/*
	 *	Make sure the DHCP-client is not running and the network interface
	 *	is down before reconfiguring IP-address.
	 *
	 *	If the link is down then ip-settings will be configured when the link
	 *	comes up.
	 */

	ntcpip_netifapiDhcpStop(&pInst->netif);
	ntcpip_netifapiNetifSetDown(&pInst->netif);
	pInst->status &= ~NETIF_STAT_ENABLED;

	if ((pInst->status & NETIF_STAT_LINKUP) != 0) {
		netif__enableNetif(pInst);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__enableNetif
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Update the ip-address of the network interface.
*
*	\param		pInst		Pointer to the FB instance.
*
*	\return		-
*
*	\details	The function will also update the default gateway and the
*				subnet mask.
*
*	\note		
*
*******************************************************************************/

PRIVATE void netif__enableNetif(
	netif__Inst *			pInst
) {
	sys_RegHandle	base;
	Uint32			tempAddr;
	Uint8			dhcpMode;

	base = reg_indexToHandle(pInst->instId, 0);
	reg_get(&tempAddr, base + reg_index_ipAddr);
	reg_get(&dhcpMode, base + reg_index_dhcpMode);

	atomic(
		pInst->confIp = tempAddr;
		pInst->dhcpMode = dhcpMode;
	);

	if (
		dhcpMode == NETIF_DHCPMODE_ENABLED
		|| (dhcpMode == NETIF_DHCPMODE_AUTO && tempAddr == 0)
	) {
		/*
		 * Ip-address should be automatically configured using DHCP or
		 * AUTOIP.
		 */

		deb_assert(pInst->pInit->pIpCfgFn);
		pInst->pInit->pIpCfgFn(pInst);

	} else {
		ntcpip_IpAddr 	ipAddr;
		ntcpip_IpAddr 	netmask;
		ntcpip_IpAddr	gateway;
		Uint8 			networkBits;

		/*
		 * Static ip-address is stored in REG. ip-addresses in REG are stored
		 * in host byte order and ip-addresses in ntcpip_IpAddr is stored in
		 * network byte order.
		 */

		ipAddr.addr = ntcpip_htonl(tempAddr);

		reg_get(&tempAddr, base + reg_index_gateway);
		gateway.addr = ntcpip_htonl(tempAddr);

		reg_get(&networkBits, base + reg_index_netmask);
		tempAddr = 0xFFFFFFFF << (32 - networkBits);
		netmask.addr = ntcpip_htonl(tempAddr);

		ntcpip_netifapiNetifSetAddr(
			&pInst->netif,
			&ipAddr,
			&netmask,
			&gateway
		);

		err_error(pInst->instId, 0, NETIF__ERR_STATIC_CONF);
		ntcpip_netifapiNetifSetUp(&pInst->netif);
	}

	pInst->status |= NETIF_STAT_ENABLED;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__input
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Send data to tcp/ip stack.
*
*	\param		pInstance 	Pointer to FB instance.
*	\param		pInData		Pointer to the inData structure.
*
*	\retval		TRUE	The frame was handled.
*	\retval		FALSE	The frame was not handled. Unknown Ethertype.
*
*	\details	The function will copy the data into the tcp/ip stack buffer
*				and call the tcp/ip stack input function.
*
*	\note
*
*******************************************************************************/

PRIVATE Boolean netif__input(
	void * 					pInstance,
	ndif_InData const_D *	pInData
) {
	netif__Inst * 	pInst;
	Boolean			retVal;

	pInst = (netif__Inst *) pInstance;

	retVal = FALSE;
	if (pInData->type == NDIF_TYPE_IP || pInData->type == NDIF_TYPE_ARP) {
		ntcpip_Pbuf *	pBuf;
		
		/*
		 * Make a properly chained pbuf from the buffers with received data.
		 */
		{
			Ufast8			buffCount;
			ndif_Cbuff *	pCBuff;

			buffCount = pInData->count;
			pCBuff = pInData->pData;
			pBuf = (ntcpip_Pbuf *) pCBuff->pBlockInfo;
			deb_assert(pBuf != NULL);
			deb_assert(pBuf->ref != 0);
			pBuf->len = pCBuff->size;
			pBuf->tot_len = pCBuff->size;

			while (--buffCount) {
				ntcpip_Pbuf * pNextBuf;
				pCBuff++;
				pNextBuf = (ntcpip_Pbuf *) pCBuff->pBlockInfo;
				deb_assert(pNextBuf != NULL);
				deb_assert(pNextBuf->ref != 0);
				pNextBuf->len = pCBuff->size;
				pNextBuf->tot_len = pCBuff->size;
				ntcpip_pbufCat(pBuf, pNextBuf);
			}
		}

		/*
		 *	TODO:	The memory of the last buffer in the chain should be trimmed
		 *			so that unused memory will be freed up. It can be done with
		 *			realloc.
		 */
		//ntcpip__pbufRealloc(pBuf, pBuf->tot_len);

		snmpa_addNetifInOctets(&pInst->netif, pBuf->tot_len);

		/*
		 * 	Pass the pbuf to NTCPIP input function. NTCPIP will free the buffer
		 * 	when it has been handled.
		 */
		{
			ntcpip_Err retCode;

			retCode = pInst->netif.input(pBuf, &pInst->netif);
			if (retCode != NTCPIP_ERR_OK) {
				ntcpip_pbufFree(pBuf);
			}
		}

		retVal = TRUE;
	}

	return retVal;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__netifInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization function called by LwIP.
*
*	\param		pNetif	Pointer to the lwip network interface structure for this
*						ethernet device.
*
*	\retval		NTCPIP_ERR_OK 	The loopif is initialized.
*	\retval		NTCPIP_ERR_MEM 	Private data couldn't be allocated.
*	\retval		.				Any other ntcpip_Err on error.
*
*	\details	Should be called at the beginning of the program to set up the
*				network interface. It calls the function low_level_init() to do
*				the actual setup of the hardware.
*
*				This function should be passed as a parameter to
*				ntcpip__netifapiNetifAdd().
*
*	\note
*
*******************************************************************************/

PRIVATE ntcpip_Err netif__netifInit(
	ntcpip_Netif *			pNetif
) {
	netif__Inst * pInst;

	pInst = (netif__Inst *) pNetif->state;

	/*
	 * Store MAC address to the netif struct.
	 */

	pInst->netif.hwaddr[0] = pInst->pNdifInit->macAddr[0];
	pInst->netif.hwaddr[1] = pInst->pNdifInit->macAddr[1];
	pInst->netif.hwaddr[2] = pInst->pNdifInit->macAddr[2];
	pInst->netif.hwaddr[3] = pInst->pNdifInit->macAddr[3];
	pInst->netif.hwaddr[4] = pInst->pNdifInit->macAddr[4];
	pInst->netif.hwaddr[5] = pInst->pNdifInit->macAddr[5];
	pInst->netif.hwaddr_len = 6;
	pInst->netif.mtu = pInst->pNdifInit->mtu;

	/* Initialize interface hostname */
//	pNetif->hostname = "lwip";

	/*
	 * Initialize the snmp variables and counters inside the struct eth1__netif.
	 * The last argument should be replaced with your link speed, in units
	 * of bits per second.
	 */

	NTCPIP_NETIF_INIT_SNMP(pNetif, snmp_ifType_ethernet_csmacd, 0);

	pNetif->name[0] = 'e';
	pNetif->name[1] = 'n';

	/*
	 * eth1__etharpOutput() used directly here to save a function call.
	 * An own output function could be made in this driver if some checks
	 * would be necessary before calling eth1__etharpOutput().
	 */

	pNetif->output = &ntcpip_etharpOutput;
	pNetif->linkoutput = &netif__linkOutput;
	
#if NTCPIP__LWIP_IGMP
	pNetif->igmp_mac_filter = &netif__macFilter;
#endif

	netif_set_status_callback(&pInst->netif, netif__statusCallback);

	/*
	 * Set device capabilities
	 */

	if (pInst->pNdifInit->flags & NDIF_CAP_BROADCAST) {
		pNetif->flags |= NTCPIP_NETIF_BROADCAST;
	}

	if (pInst->pNdifInit->flags & NDIF_OPT_ARP) {
		pNetif->flags |= NTCPIP_NETIF_ETHARP;
	}

	if (pInst->pNdifInit->flags & NDIF_CAP_IGMP) {
		pNetif->flags |= NTCPIP_NETIF_IGMP;
	}

	return(NTCPIP_ERR_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__frameSent
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Free up buffer of sent data.
*
*	\param		pCBuffer	Pointer to chain buffer.
*
*	\return
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void netif__frameSent(
	ndif_Cbuff const_D *	pCBuffer
) {
	/*
	 *	The pbuf chain can be freed now.
	 */

	do {
		ntcpip_pbufFree(pCBuffer->pBlockInfo);
	} while ((++pCBuffer)->size != 0);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__linkOutput
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Network device output function.
*
*	\param		pNetif 	The lwip network interface structure for this ethernetif
*	\param		pBuf	The MAC packet to send (e.g. IP packet including MAC
*						addresses and type)
*
*	\return		NTCPIP_ERR_OK if the packet could be sent an ntcpip_Err value if
*				the packet couldn't be sent
*
*	\details	This function should do the actual transmission of the packet.
*				The packet is contained in the pbuf that is passed to the
*				function. This pbuf might be chained.
*
*	\note		Returning NTCPIP_ERR_MEM here if a DMA queue of your MAC is full
*				can lead to strange results. You might consider waiting for
*				space in the DMA queue to become available since the stack
*				doesn't retry to send a packet dropped because of memory failure
*				(except for the TCP timers).
*
*******************************************************************************/

PRIVATE ntcpip_Err netif__linkOutput(
	ntcpip_Netif *			pNetif,
	ntcpip_Pbuf *			pBuf
) {
	netif__Inst *	pInst;
	ndif_Request 	request;
	ndif_Cbuff		chainBuffer[NETIF__CHAINBUF_LEN];

	pInst = (netif__Inst *) pNetif->state;

	{
		ntcpip_Pbuf * 		pTempBuf;
		Ufast8				cBuffIdx;
		
		cBuffIdx = 0;

		for(pTempBuf = pBuf; pTempBuf != NULL; pTempBuf = pTempBuf->next) {
			deb_assert(cBuffIdx < NETIF__CHAINBUF_LEN);
			chainBuffer[cBuffIdx].pData = pTempBuf->payload;
			chainBuffer[cBuffIdx].size = pTempBuf->len;
			chainBuffer[cBuffIdx].pBlockInfo = pTempBuf;

			/*
			 *	The pbuf cannot be freed directly. It can be done when the data
			 *	has been sent. Increase the reference count to leave the pbuf
			 *	allocated.
			 */
			ntcpip_pbufRef(pTempBuf);
			cBuffIdx++;
		}

		chainBuffer[cBuffIdx].size = 0;

		request.pCBuffer = chainBuffer;
		request.count = (Uint16) cBuffIdx;
		request.pUtil = pInst;
		request.pFrameSentFn = &netif__frameSent;
	}

	/*
	 *	Enqueue data for transmission.
	 */

	pInst->pNdifInit->pTxIf->pOutputFn(pInst->pNdifInit->pMacInst, &request);

	//snmpa_addNetifOutOctets(pNetif, pBuf->tot_len);

	return(NTCPIP_ERR_OK);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__activate
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Change Ethernet device status.
*
*	\param		pInstance	Pointer to FB instance.
*	\param		pStatus		Pointer to status change type.
*
*	\return		-
*
*	\details	Called by the Ethernet driver when the link status changes or
*				when a call has been requested through pGetActFn.
*
*	\note
*
*******************************************************************************/

PRIVATE void netif__activate(
	void *					pInstance,
	ndif_Status const_D *	pStatus
) {
	netif__Inst *			pInst;
	netif__Triggers			triggers;
	netif__Status			oldStatus;
	pInst = (netif__Inst *) pInstance;

	oldStatus = pInst->status;

	atomic(
		triggers = pInst->triggers;
		pInst->triggers = 0;
	);

	if (pStatus->flags & NDIF_STAT_LINKUP) {
		
		/*
		 *	Link is up.
		 */

		err_errorEx(pInst->instId, 0, NETIF__ERR_PHY_STATUS, 1);
		ntcpip_netifapiLinkUp(&pInst->netif);
		pInst->status |= NETIF_STAT_LINKUP;
		triggers |= NETIF__TRG_LINK_UP;
	}

	if (pStatus->flags & NDIF_STAT_LINKDOWN) {
		/*
		 *	Link went down.
		 */

		err_errorEx(pInst->instId, 0, NETIF__ERR_PHY_STATUS, 0);
		ntcpip_netifapiLinkDown(&pInst->netif);
		pInst->status &= ~NETIF_STAT_LINKUP;
	}

	/*
	 *	Handle trigger flags.
	 */

	if (triggers & NETIF__TRG_REG_RDY) {
		/*
		 *	REG is now ready.
		 */

		pInst->status |= NETIF_STAT_REGREADY;

		if ((pInst->status & NETIF_STAT_LINKUP) != 0) {
			triggers |= NETIF__TRG_LINK_UP;
		}
	}

	if (triggers & NETIF__TRG_LINK_UP) {
		if ((pInst->status & NETIF_STAT_REGREADY) != 0) {
			if ((pInst->status & NETIF_STAT_ENABLED) == 0) {
				/*
				 *	Link is up and REG is ready but the interface has not been
				 *	enabled yet. Enable it now.
				 */

				netif__enableNetif(pInst);
			}
		}
	}

	if (triggers & NETIF__TRG_ADDR) {
		/*
		 *	IP-settings registers have been changed.
		 */

		netif__updateIpAddr(pInst);
	}

	/*
	 * Check if ip-address, default gateway or the subnet mask has changed.
	 */

	if (oldStatus != pInst->status) {
		reg_putLocal((void *) &pInst->status, reg_index_status, pInst->instId);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__regReadyCallback
*
*--------------------------------------------------------------------------*//**
*
*	\brief		REG initialization is done.
*
*	\param		pUtil	Pointer to instance.
*
*	\details
*
*	\note		Called with scheduling disabled.
*
*	\sa
*
*******************************************************************************/

PRIVATE void netif__regReadyCallback(
	void *					pUtil
) {
	netif__Inst * pInst;

	pInst = (netif__Inst *) pUtil;

	/*
	 *	Stop REG timeout timer.
	 */

	swtimer_stop(&pInst->bootTimer);

	/*
	 *	The link is up and REG is ready but the interface has not been
	 *	enabled yet.
	 */

	if ((pInst->triggers & NETIF__TRG_REG_RDY) == 0) {
		pInst->triggers |= NETIF__TRG_REG_RDY;

		if (pInst->pNdifInit != NULL) {
			pInst->pNdifInit->pTxIf->pGetActFn(pInst->pNdifInit->pMacInst);
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	netif__regTimeout
*
*--------------------------------------------------------------------------*//**
*
*	\brief		REG init timeout timer has expired.
*
*	\param		pTimer		Pointer to timer that expired.
*
*	\details
*
*	\note		Called with scheduling disabled.
*
*	\sa
*
*******************************************************************************/

PRIVATE void netif__regTimeout(
	 swtimer_Timer_st *		pTimer
) {
	netif__Inst * pInst;

	pInst = (netif__Inst *) swtimer_getUtilPtr(pTimer);

	/*
	 *	There is probably something wrong with the REG non-volatile register
	 *	handler. Proceed with default network values. Request a function call
	 *	from the MAC-driver task.
	 */

	pInst->triggers |= NETIF__TRG_REG_RDY;
	if (pInst->pNdifInit != NULL) {
		pInst->pNdifInit->pTxIf->pGetActFn(pInst->pNdifInit->pMacInst);
	}
}
