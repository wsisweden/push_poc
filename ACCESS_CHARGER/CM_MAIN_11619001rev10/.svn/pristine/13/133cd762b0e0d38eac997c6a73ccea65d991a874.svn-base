/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		LIB_ITOA.C
*
*	\ingroup	LIB
*
*	\brief		Conversion functions.
*
*	\details	
*
*	\par		Module name:
*				lib_itoa
*
*	\par		Files:
*				lib_itoa.c lib.h
*
*	\note		
*
*	\version	dd-mm-yy / N N
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "lib.h"

#include <stdlib.h>
#include <string.h>

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_Uint16ToAscii
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert Uint16 to string
*
*	\param		nValue 	Number to be converted
*	\param		pStr 	Pointer to string where vaule will be stored
*	\param		nMaxLen Max length of the number
*
*	\return		The number of characters used for the number.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC Uint8 lib_Uint16ToAscii(
	Uint16					nValue,
	char *					pStr,
	Uint16					nMaxLen
) {
	Uint8					nLen;
	char *					pTmp;

	if (nMaxLen == 0) {
		return 0;
	}

	pTmp = &pStr[nMaxLen - 1];
	*pTmp = 0x00;

	/*
	 *	TODO:	util_u16toadec() could be called here instead having identical
	 *			code in this function.
	 */

	do {
		if (pTmp == pStr) {
			break;
		}

		*--pTmp = (Uint8)((Uint8)(nValue % 10) + '0');
		
	} while((nValue /= 10) > 0);

	nLen = (Uint8)(&pStr[nMaxLen - 1] - pTmp);
	memmove(pStr, pTmp, nLen + 1);

	return nLen;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_Uint32ToAscii
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert Uint32 to string
*
*	\param		nValue 	Number to be converted
*	\param		pStr 	Pointer to location where the string will be stored.
*	\param		nMaxLen Max number of characters to use for the string.
*
*	\return		Number of characters used for the number.
*
*	\details	
*
*	\note	
*
*******************************************************************************/
PUBLIC Uint8 lib_Uint32ToAscii(
	Uint32					nValue,
	char *					pStr,
	Uint16					nMaxLen
) {
	Uint8					nDigit;
	Uint8					nLen;
	char *					pBeg;

	if (nMaxLen < 11) {
		return 0;
	}

	pBeg = pStr;

	if (nValue >= 10000) {
		if (nValue >= 100000UL) {
			if (nValue >= 1000000UL) {
				if (nValue >= 10000000UL) {
					if (nValue >= 100000000UL) {
						if (nValue >= 1000000000UL) {

							/* 
							 *	1000000000 - Uint32 max 
							 */
							if (nValue >= 3000000000UL) { 
								nValue -= 3000000000UL;
								nDigit = '3';
							} else {
								nValue -= 1000000000UL;
								nDigit = '1';
							}

							if (nValue >= 1000000000UL) {
								nValue -= 1000000000UL;
								nDigit++;
							}

							*pStr++ = nDigit;
						}

						/* 
						 *	100000000 - 1000000000 
						 */
						if (nValue >= 500000000UL) {
							nValue -= 500000000UL;
							nDigit = '5';
						} else {
							nDigit = '0';
						}

						if (nValue >= 300000000UL) {
							nValue -= 300000000UL;
							nDigit += 3;
						} 

						if (nValue >= 100000000UL) {
							nValue -= 100000000UL;
							nDigit++;
						}

						if (nValue >= 100000000UL) {
							nValue -= 100000000UL;
							nDigit++;
						}

						*pStr++ = nDigit;

					}

					/* 
					 *	10000000 - 100000000 
					 */
					if (nValue >= 50000000UL) {
						nValue -= 50000000UL;
						nDigit = '5';
					} else {
						nDigit = '0';
					}

					if (nValue >= 30000000UL) {
						nValue -= 30000000UL;
						nDigit += 3;
					} 

					if (nValue >= 10000000UL) {
						nValue -= 10000000UL;
						nDigit++;
					}

					if (nValue >= 10000000UL) {
						nValue -= 10000000UL;
						nDigit++;
					}

					*pStr++ = nDigit;
				}

				/* 
				 *	1000000 - 10000000 
				 */
				if (nValue >= 5000000UL) {
					nValue -= 5000000UL;
					nDigit = '5';
				} else {
					nDigit = '0';
				}

				if (nValue >= 3000000UL) {
					nValue -= 3000000UL;
					nDigit += 3;
				} 

				if (nValue >= 1000000UL) {
					nValue -= 1000000UL;
					nDigit++;
				}

				if (nValue >= 1000000UL) {
					nValue -= 1000000UL;
					nDigit++;
				}

				*pStr++ = nDigit;
			}

			/* 
			 *	100000 - 1000000 
			 */
			if (nValue >= 500000UL) {
				nValue -= 500000UL;
				nDigit = '5';
			} else {
				nDigit = '0';
			}

			if (nValue >= 300000UL) {
				nValue -= 300000UL;
				nDigit += 3;
			} 

			if (nValue >= 100000UL) {
				nValue -= 100000UL;
				nDigit++;
			}

			if (nValue >= 100000UL) {
				nValue -= 100000UL;
				nDigit++;
			}

			*pStr++ = nDigit;

		}

		/* 
		 *	10000 - 100000 
		 */
		if (nValue >= 50000UL) {
			nValue -= 50000UL;
			nDigit = '5';
		} else {
			nDigit = '0';
		}

		if (nValue >= 30000UL) {
			nValue -= 30000UL;
			nDigit += 3;
		} 

		if (nValue >= 10000UL) {
			nValue -= 10000UL;
			nDigit++;
		}

		if (nValue >= 10000UL) {
			nValue -= 10000UL;
			nDigit++;
		}

		*pStr++ = nDigit;

		if (nValue < 1000) {
			*pStr++ = (Uint8)'0';

			if (nValue < 100) {
				*pStr++ = (Uint8)'0';

				if (nValue < 10) {
					*pStr++ = (Uint8)'0';
				}
			}
		}
	}

	nLen = (Uint8)(pStr - pBeg);

	nLen += lib_Uint16ToAscii((Uint16)nValue, pStr, (Uint8)(nMaxLen - nLen));

	return nLen;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_Int16ToAscii
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert Int16 to a string.
*
*	\param		nValue 	Number to be converted.
*	\param		pStr 	Pointer to location where the string will be stored.
*	\param		nMaxLen Max number of characters to be used for the number.
*
*	\return		Number of characters used for the number.
*
*	\details	
*
*	\note	
*
*******************************************************************************/
PUBLIC Uint8 lib_Int16ToAscii(
	Int16					nValue,
	char *					pStr,
	Uint16					nMaxLen
) {
	Uint16					nTmp;
	Uint8					nLen;
	Boolean					bSign;

	if (nMaxLen == 0) {
		return 0;
	}

	/*	
	 *  Handle as Uint16 and just put the sign separately if the value is 
	 *	below zero.
	 */
	if (nValue < 0) {
		*pStr++ = '-';
		nMaxLen--;
		nTmp = (Uint16)(nValue * -1);

		bSign = TRUE;
	} else {
		nTmp = nValue;
		bSign = FALSE;
	}

	nLen = lib_Uint16ToAscii(nTmp, pStr, nMaxLen);

	if (nLen && bSign) {
		nLen++;
	}

	return nLen;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_Int32ToAscii
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Convert Int32 to a string
*
*	\param		nValue 
*	\param		pStr 
*	\param		nMaxLen 
*
*	\return		PUBLIC Uint8
*
*	\details	
*
*	\note	
*
*******************************************************************************/
PUBLIC Uint8 lib_Int32ToAscii(
	Int32					nValue,
	char *					pStr,
	Uint16					nMaxLen
) {
	Uint32					nTmp;
	Uint8					nLen;
	Boolean					bSign;

	if (nMaxLen < 12) {
		return 0;
	}

	/*	
	 *  Handle as Uint32 and justa dd the sign if smaller than zero.
	 */
	if (nValue < 0) {

		*pStr++ = '-';
		nMaxLen--;

		/* todo: overflow if value is int32_min? */
		
		nTmp = nValue * -1;
		bSign = TRUE;

	} else {

		nTmp = nValue;
		bSign = FALSE;
	}

	nLen = lib_Uint32ToAscii(nTmp, pStr, nMaxLen);

	if (nLen && bSign) {
		nLen++;
	}

	return nLen;
}

