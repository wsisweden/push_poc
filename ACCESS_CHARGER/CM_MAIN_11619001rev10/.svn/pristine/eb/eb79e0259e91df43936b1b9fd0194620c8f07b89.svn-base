/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_fi.c
*
*	\ingroup	FLMEM
*
*	\brief		Main register write fifo handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"

/* local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#ifndef NDEBUG
#define FLMEM__FIFO_EMPTY_COUNTER_MAX		1000
#endif

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__fifoInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initializes the FIFO.
*
*	\param		pInst		Pointer to FB instance.
*
*	\details	
*
*	\return		-
*
*	\note		
*
*******************************************************************************/

PUBLIC void flmem__fifoInit(
	flmem__Inst *			pInst
) {
	pInst->fifoInfo.frontArrIdx = FLMEM__MEMORYINDEX_INDEX_FIFOEND;
	pInst->fifoInfo.backArrIdx = FLMEM__MEMORYINDEX_INDEX_FIFOEND;

#ifndef NDEBUG
	pInst->fifoInfo.fifoBusyCount = 0;
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__fifoPush
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function adds tag to FLMEM-fifo so that tag can be processed
*				and written to flash.
*
*	\param		pInst		Pointer to FB instance.
*	\param		tagIndex	Index of the register in the memoryindexes array.
*
*	\details	Tagindex is array position of tag at memoryindexes- or
*				flashtagsByTagsOrder array.
*
*	\return		True if value was added to FIFO. False if value was already
*				at FIFO.
*
*	\note		
*
*******************************************************************************/

PUBLIC Boolean flmem__fifoPush(
	flmem__Inst *			pInst,
	Uint16					tagIndex
) {
	flmem__MemoryIndex *	pMemoryIndexes;
	Boolean					wasPushed;
	Uint16					nextIdx;

	pMemoryIndexes = pInst->pMemoryIndexes;
	wasPushed = FALSE;

	deb_assert(tagIndex < *(pInst->pInit->tagCount));

	DISABLE;
	nextIdx = pMemoryIndexes[tagIndex].nextArrIdx & FLMEM__MEMORYINDEX_INDEX_MASK_LOW;

	if (nextIdx == FLMEM__MEMORYINDEX_INDEX_UNUSED) {
		/*
		 *	The register is not in the FIFO yet. It will be added. If the 
		 *	register would be in the FIFO then it would not need to be added.
		 */

		/*
		 * TODO:	Set the high bit?
		 */
		
		if (pInst->fifoInfo.backArrIdx == FLMEM__MEMORYINDEX_INDEX_FIFOEND) {
			/*
			 *	The FIFO is empty.
			 */

			pInst->fifoInfo.frontArrIdx = tagIndex;

		} else {
			pMemoryIndexes[pInst->fifoInfo.backArrIdx].nextArrIdx &=
				FLMEM__MEMORYINDEX_INDEX_MASK_HIGH;
			pMemoryIndexes[pInst->fifoInfo.backArrIdx].nextArrIdx |= tagIndex;
		}

		pMemoryIndexes[tagIndex].nextArrIdx = FLMEM__MEMORYINDEX_INDEX_FIFOEND;
		pInst->fifoInfo.backArrIdx = tagIndex;
		wasPushed = TRUE;
		pInst->triggers |= FLMEM__TRG_WRITE;
	}
	ENABLE;

	if (wasPushed) {
		osa_signalSend(&pInst->task);
	}

	return wasPushed;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__fifoPop
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Removes the item at the front of a non-empty FIFO.
*
*	\param		pInst		Pointer to FB instance.
*
*	\details	
*
*	\return		Index of the popped tag.
*
*	\note		
*
*******************************************************************************/

PUBLIC flmem__TagIdx flmem__fifoPop(
	flmem__Inst *			pInst
) {
	flmem__MemoryIndex *	pPoppedTag;
	flmem__TagIdx			poppedIdx;
	Boolean					kickTask;

	kickTask = FALSE;

	DISABLE;
	if (pInst->fifoInfo.frontArrIdx == FLMEM__MEMORYINDEX_INDEX_FIFOEND) {
		/*
		 *	The FIFO is empty.
		 */
		poppedIdx = FLMEM__MEMORYINDEX_INDEX_FIFOEND;

	} else {
		poppedIdx = pInst->fifoInfo.frontArrIdx;
		pPoppedTag = &pInst->pMemoryIndexes[pInst->fifoInfo.frontArrIdx];
		pInst->fifoInfo.frontArrIdx = pPoppedTag->nextArrIdx &
				FLMEM__MEMORYINDEX_INDEX_MASK_LOW;

		/* 
		 *	Clear the nextArrIdx field of the popped element but leave the
		 *	bit-flag untouched.
		 */

		pPoppedTag->nextArrIdx |= FLMEM__MEMORYINDEX_INDEX_UNUSED;

		if (pInst->fifoInfo.frontArrIdx == FLMEM__MEMORYINDEX_INDEX_FIFOEND) {
			/*
			 *	The FIFO is empty now.
			 */
			
			pInst->fifoInfo.backArrIdx = FLMEM__MEMORYINDEX_INDEX_FIFOEND;

#ifndef NDEBUG
			pInst->fifoInfo.fifoBusyCount = 0;
#endif

		} else {
			/*
			 *	There is still registers in the FIFO. Start task that will
			 *	handle next write
			 */

			pInst->triggers |= FLMEM__TRG_WRITE;
			kickTask = TRUE;

			/*
			 *	This makes sure that FIFO is empty even a once for a while.
			 *	If thats not a case then it would seem you might have a design
			 *	problem in you current program! Flash shouldn't be used so that
			 *	it's bombed with write-request excessively. Flash memory
			 *	will get worn out quite soon!
			 */

			deb_assert(
				++(pInst->fifoInfo.fifoBusyCount)<=FLMEM__FIFO_EMPTY_COUNTER_MAX
			);
		}
	}
	ENABLE;

	if (kickTask) {
		osa_signalSend(&pInst->task);
	}

	return poppedIdx;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__fifoFront
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Peeks at the front of the FIFO.
*
*	\param		pInst		Pointer to FB instance.
*
*	\details	This will provide the next index that the fifoPop will return.
*				This function will not modify the FIFO.
*
*	\return		Index of the element at the front of the non-empty FIFO.
*
*	\note		
*
*******************************************************************************/

PUBLIC Uint16 flmem__fifoFront(
	flmem__Inst *			pInst							   
) {
	Uint16					frontIdx;

	atomic(
		frontIdx = pInst->fifoInfo.frontArrIdx;
	);

	return frontIdx;
}
