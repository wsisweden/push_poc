/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		DEB_LOGN.C
*
*	\ingroup	DEB
*
*	\brief		Debug logging functions.
*
*	\details	
*
*	\note
*
*	\version	dd-mm-yy / author
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "util.h"
#include "local.h"

#include "deb.h"
#include <stdarg.h>

/******************************************************//** \cond priv_decl *//*
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define LOGN_ZERO_PAD		0x01			/**< Zero padding               */
#define LOGN_LEFT_ALIGN		0x02			/**< Left aligned output        */
#define LOGN_SIGN_PRFX		0x04			/**< Add sign                   */
#define LOGN_TYPE_PRFX		0x08			/**< Add type prefix            */
#define LOGN_BLANK_PRFX		0x10			/**< Add blanks                 */

#define LOGN_UHEX_PRFX		0x80			/**< Upper case hex prefix (0X) */
#define LOGN_LHEX_PRFX		0x40			/**< Lower case hex prefix (0x) */
#define LOGN_OCT_PRFX		0x20			/**< Octal prefix (0)           */

#define LOGN_T_SHORT		0				/**< Short value                */
#define LOGN_T_NORMAL		1				/**< Normal value               */
#define LOGN_T_LONG			2				/**< Long value                 */

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*************************************************************//** \endcond *//*
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	deb_logN
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Writes a log message with arguments. Arguments of the format
*				string are the same as the printf's format string with some
*				restrictions. This is a wrapper for deb_logNList.
*
*	\param		frmt 	null terminated format string
*	\param		...		argument list
*
*	\details
*
*	\note		Logger must be registered with deb_logRegLogger before calling
*				this or nothing will be logged.
*
*******************************************************************************/

PUBLIC void deb_logN(
	char const_P *			frmt,
	...
) {
	va_list					args;

	va_start(args, frmt);
	deb_logNXList(frmt, FALSE, args);
	va_end(args);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	deb_logNIsr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Writes a log message with arguments. Arguments of the format
*				string are the same as the printf's format string with some
*				restrictions. This can be called from an ISR as a last resort
*				debug output.
*
*	\param		frmt 	null terminated format string
*	\param		...		argument list
*
*	\details
*
*	\note		Logger must be registered with deb_logRegLogger before calling
*				this or nothing will be logged.
*
*******************************************************************************/

PUBLIC void deb_logNIsr(
	char const_P *			frmt,
	...
) {
	va_list					args;

	va_start(args, frmt);
	deb_logNXList(frmt, TRUE, args);
	va_end(args);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	deb_logNXList
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Writes a log message with arguments. Arguments of the format
*				string are the same as the printf's format string with some
*				restrictions.
*
*	\param		frmt 	null terminated format string.
*	\param		isr		Called from interrupt handler.
*	\param		args	argument list.
*
*	\details
*
*	\note		Logger must be registered with deb_logRegLogger before calling
*				this or nothing will be logged.
*
*******************************************************************************/

PUBLIC void deb_logNXList(
	char const_P *			frmt,
	Boolean					isr,
	va_list					args
) {
	Uint16					mask;

	deb_assert(deb__logInstance.logger != NULL);

	if (isr)
	{
		mask = DEB_LOG_ISR;
	}
	else
	{
		mask = 0;

		if (osa_isRunning)
		{
			osa_semaGet(&deb__logInstance.sema);
		}
	}

	deb__logInstance.logger(DEB_LOG_FIRST | mask);

	while(*frmt != '\0')
	{
		char *				pTmp = NULL;
		Uint8				nTmp = 0;
		Uint8				nFlags = 0;
		Uint8				nPad = 0;

		if (*frmt == '%')
		{
			frmt++;

			/*
			 *  Check flags
			 */
			while(nTmp == 0)
			{
				switch (*frmt)
				{
					case '-':	// Left align
						frmt++;
						nFlags |= LOGN_LEFT_ALIGN;
						break;

					case '+':	//Sign prefix
						frmt++;
						nFlags |= LOGN_SIGN_PRFX;
						break;

					case '#':	// Type prefix
						frmt++;
						nFlags |= LOGN_TYPE_PRFX;
						break;

					case ' ':	// Blank prefix
						frmt++;
						nFlags |= LOGN_BLANK_PRFX;
						break;;

					case '0':	// Zero padding
						frmt++;
						nFlags |= LOGN_ZERO_PAD;
						break;

					case '\0':	// Unecpected end of string
						/*
						 *  Just break, the switch down the path will catch this
						 */
					default:
						nTmp = 1;
						break;
				}
			}

			// Check width
			if (*frmt == '*')
			{
				// todo: unsupported
				deb_assert(FALSE);
				frmt++;
			}
			else if (util_isdigit(*frmt))
			{
				nPad = (Uint8)util_atolConst(frmt, &frmt);
			}

			// Check for precision
			if (*frmt == '.')
			{
				frmt++;

				if (*frmt == '*')
				{
					// todo: unsupported
					deb_assert(FALSE);
					frmt++;
				}
				else if (util_isdigit(*frmt))
				{
					do
					{
						frmt++;
					} while(util_isdigit(*frmt));
				}
			}

			// Check type prefix
			switch (*frmt)
			{
				case 'h':	// Short
					nTmp = LOGN_T_SHORT;
					frmt++;
					break;

				case 'l':	// Long
					nTmp = LOGN_T_LONG;
					frmt++;
					break;

				default:
					nTmp = LOGN_T_NORMAL;
					break;
			}

			deb__logInstance.buf[0] = '\0';

			// Get conversion type
			switch(*frmt)
			{
			case 'd':	// Signed integer
			case 'i':	// Signed integer
				{
					Int32	nVal = 0;

					switch(nTmp) {
					case LOGN_T_SHORT:
						nVal = (short)va_arg(args, int);
						break;

					case LOGN_T_NORMAL:
						nVal = (Int32)va_arg(args, int);
						break;

					case LOGN_T_LONG:
						nVal = (Int32)va_arg(args, long);
						break;

					default:
						break;
					}
					pTmp = util_ltoaEx(
						deb__logInstance.buf,
						&deb__logInstance.buf[sizeof(deb__logInstance.buf) - 1],
						nVal
					);

					nTmp = (Uint8) (pTmp - deb__logInstance.buf);
				}
				break;

			case 'u':	// Unsigned integer
				{
					Uint32	nVal = 0;

					switch(nTmp) {
					case LOGN_T_SHORT:
						nVal = (unsigned short)va_arg(args, unsigned int);
						break;

					case LOGN_T_NORMAL:
						nVal = (Int32)va_arg(args, unsigned int);
						break;

					case LOGN_T_LONG:
						nVal = (Int32)va_arg(args, unsigned long);
						break;

					default:
						break;
					}

					pTmp = util_ultoaEx(
						deb__logInstance.buf,
						&deb__logInstance.buf[sizeof(deb__logInstance.buf) - 1],
						nVal
					);

					nTmp = (Uint8) (pTmp - deb__logInstance.buf);
				}
				break;

			case 'x':	// Unsigned hex using "abcdef"
			case 'X':	// Unsigned hex using "ABCDEF"
				{
					Uint32	nVal = 0;

					switch(nTmp) {
					case LOGN_T_SHORT:
						nVal = (unsigned short)va_arg(args, unsigned int);
						break;

					case LOGN_T_NORMAL:
						nVal = (Int32)va_arg(args, unsigned int);
						break;

					case LOGN_T_LONG:
						nVal = (Int32)va_arg(args, unsigned long);
						break;

					default:
						break;
					}

					pTmp = &deb__logInstance.buf[
						sizeof(deb__logInstance.buf) - 1
					];

					if (*frmt == 'x')
					{
						// Use lower case characters
						pTmp = util_ultoaLHex(deb__logInstance.buf, pTmp, nVal);

						if (nFlags & LOGN_TYPE_PRFX)
						{
							nFlags |= LOGN_LHEX_PRFX;
						}
					}
					else
					{
						// Use upper case characters
						pTmp = util_ultoaUHex(deb__logInstance.buf, pTmp, nVal);

						if (nFlags & LOGN_TYPE_PRFX)
						{
							nFlags |= LOGN_UHEX_PRFX;
						}
					}

					nTmp = (Uint8) (pTmp - deb__logInstance.buf);
				}
				break;

			case 'c':	// Character
				{
					deb__logInstance.buf[0] = va_arg(args, int);
					deb__logInstance.buf[1] = '\0';
					nTmp = 1;
				}
				break;

			case 's':	// String
				{
					nTmp = 0;
					pTmp = va_arg(args, char *);

					if (pTmp != NULL)
					{
						while (*pTmp != '\0')
						{
							deb__logInstance.logger(*pTmp | mask);
							pTmp++;
							nTmp++;
						}
					}
				}
				break;

			case'p':	// Pointer
				{
					Uint32 nVal;

					nVal = (Uint32) va_arg(args, void *);

					pTmp = util_ultoaUHex(
						deb__logInstance.buf,
						&deb__logInstance.buf[sizeof(deb__logInstance.buf) - 1],
						nVal
					);

					nTmp = (Uint8) (pTmp - deb__logInstance.buf);
				}
				break;

			case '\0':	// Unexpected end of string
				continue;

#if 0 // Unnecessary ifs
			case 'o':	// Unsigned octal
			case 'e':
			case 'E':
			case 'f':
			case 'g':
			case 'G':
#endif
			default:
				// Unsupported
				deb_assert(FALSE);
				break;
			}

			/*
			 *  Add prefix if so demanded by flags
			 */
			if (nFlags & LOGN_UHEX_PRFX)
			{
				deb__logInstance.logger('0' | mask);
				deb__logInstance.logger('X' | mask);
			}
			else if (nFlags & LOGN_LHEX_PRFX)
			{
				deb__logInstance.logger('0' | mask);
				deb__logInstance.logger('x' | mask);
			}
			else if (nFlags & LOGN_OCT_PRFX)
			{
				deb__logInstance.logger('0' | mask);
			}

			/*
			 *  Add padding to beginning if necessary
			 */
			if ((nFlags & LOGN_LEFT_ALIGN) == 0)
			{
				if (nPad > nTmp)
				{
					nPad -= nTmp;

					if (nFlags & LOGN_ZERO_PAD)
					{
						nTmp = '0';
					}
					else
					{
						nTmp = ' ';
					}

					while(nPad--)
					{
						deb__logInstance.logger(nTmp | mask);
					}

					// Zero the value for after padding
					nPad = 0;
				}
			}
			/*
			 *  Output the characters
			 */
			pTmp = deb__logInstance.buf;

			while (*pTmp != '\0')
			{
				deb__logInstance.logger(*pTmp | mask);
				pTmp++;
			}

			/*
			 *  Add after padding if necessary
			 */
			if (nPad > nTmp)
			{
				nPad -= nTmp;

				if (nFlags & LOGN_ZERO_PAD)
				{
					nTmp = '0';
				}
				else
				{
					nTmp = ' ';
				}

				while(nPad--)
				{
					deb__logInstance.logger(nTmp | mask);
				}
			}
		}
		else
		{
			/*
			 *  Not a formatting flag
			 */
			deb__logInstance.logger(*frmt | mask);
		}

		frmt++;
	}

	deb__logInstance.logger(DEB_LOG_LAST | mask);

	if (osa_isRunning && !isr)
	{
		osa_semaSet(&deb__logInstance.sema);
	}
}
