/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_cm3/UART_COM.H
*
*	\ingroup	UART_CM3
*
*	\brief		Declarations of the common code module.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 349 $ \n
*				\$Date: 2010-06-01 14:45:18 +0300 (ti, 01 kes� 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef UART_COM_H_INCLUDED
#define UART_COM_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "../local.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * \name	Interrupt Identification Register
 */

/*@{*/
#define UART__UxIIR_NO_ISR	(1<<0)			/**< Interrupt status			*/
#define UART__UxIIR_RLS		((1<<1)|(1<<2))	/**< Interrupt identification	*/
#define UART__UxIIR_RDA		(1<<2)			/**< Receive Data Available		*/
#define UART__UxIIR_CTI		((1<<2)|(1<<3))	/**< Character Time-out Indicator*/
#define UART__UxIIR_THRE	(1<<1)			/**< THRE Interrupt				*/
/*@}*/

/**
 * \name	Line Status Register bits
 */

/*@{*/
#define UART__UxLSR_RDR		(1<<0)		/**< Receiver data ready			*/
#define UART__UxLSR_THRE	(1<<5)		/**< Transmitter Holding Register Empty*/
#define UART__UxLSR_RXE		(1<<7)		/**< Error in RX FIFO				*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Type used to store UART settings in HW register value format.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint32					lcr;		/**< Line control register contents	*/
	Uint8					fdr;		/**< Fractional divider register	*/
	Uint8					dlm;		/**< Divisor latch MSB register		*/
	Uint8					dll;		/**< Divisor latch LSB register		*/
} uart__Settings;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Type used to store baud rate temporarily during configuration.
 */

typedef union {
	Uint8					u8;
	Uint16					u16;
	Uint32					u32;
} uart__BaudRate;

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

extern void					uart__getSettings(uart_Inst *,uart__Settings *);

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
