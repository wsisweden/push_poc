/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\brief		
*
*	\details
*
*	\note
*
*	\version	
*
*******************************************************************************/

#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"


/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * 	\name	Mode flags.
 */
/*@{*/
#define DISPLAY_DRAW_INVERT	(1<<0)		/**< Inverted drawing				*/
#define DISPLAY_DRAW_TRANS	(1<<1)		/**< Transparent drawing			*/
#define DISPLAY_DRAW_VERT	(1<<2)		/**< 								*/
#define DISPLAY_DRAW_HOR	(1<<3)		/**<								*/
/*@}*/


/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/


/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

typedef Uint8				display_Mode;
typedef Uint8				display_Coord;


typedef struct							/*''''''''''''''''''''''''''' CONST */
{										/*									*/
	display_Coord			x;			/**< x coordinate					*/
	display_Coord			y;			/**< y coordinate					*/
} display_Pos;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef struct							/*''''''''''''''''''''''''''' CONST */
{										/*									*/
	display_Coord			x;			/**< x coordinate					*/
	display_Coord			y;			/**< y coordinate					*/
	display_Coord			width;		/**< Width							*/
	display_Coord			height;		/**< Height							*/
} display_Rect;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/


typedef void * (*			display_fnInitData)(void const_P *);
typedef void (*				display_fnInitDisplay)(void *);
typedef void (*				display_fnAbort)(void *);
typedef void (*				display_fnOnOff)(void *, Boolean);
typedef void (*				display_fnSleep)(void *, Boolean);
typedef void (*				display_fnBacklight)(void *, Boolean);
typedef void (*				display_fnContrast)(void *, Uint8);

typedef void (*				display_fnFlush)(void *);
typedef void (*				display_fnClear)(void *);
typedef void (*				display_fnMode)(void *, display_Mode);
typedef void (*				display_fnSetPos)(void *, display_Coord, display_Coord);
typedef void (*				display_fnDrawImage)(void *, BYTE const_P *, display_Coord, display_Coord);
typedef void (*				display_fnDrawFilledRect)(void *, display_Rect * pRect);


/**
 * \brief		Display driver interface.
 *
 * \details		
 */
typedef struct							/*''''''''''''''''''''''''''' CONST */
{										/*									*/
	display_fnInitData		initData;	/**< Initializes the data structures*/
	display_fnInitDisplay	initDisplay;/**< Initializes the actual display	*/
	display_fnAbort			abort;		/**< Abort current request			*/
	display_fnOnOff			onoff;		/**< Sets display on / off			*/
	display_fnOnOff			sleep;		/**< Display sleep on/ off			*/
	display_fnBacklight		backlight;	/**< Display backlight on / off		*/
	display_fnContrast		contrast;	/**< Display contrast				*/
										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/
	display_fnClear			clear;		/**< Clears the display				*/
	display_fnFlush			flush;		/**< Flushes RAM buffer to display	*/
	display_fnMode			setmode;	/**< Sets draw mode					*/
	display_fnSetPos		setpos;		/**< Sets current draw position		*/
	display_fnDrawImage		drawImage;	/**< Draws image to display buffer	*/
	display_fnDrawFilledRect drawFilledRect;	/**< Draws filled rectangle	*/
} display_Interface;					/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/



/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/


/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

/*****************************************************************************/

#endif
