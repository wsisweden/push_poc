/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[X]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		NDRV_INT.C
*
*	\ingroup	NDRV
*
*	\brief		PHY status interrupt control.
*
*	\details
*
*	\note
*
*	\version	24-03-2010 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "ndrv.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	ndrv_handleInterrupt
*
*--------------------------------------------------------------------------*//**
*
*	\brief		PHY interrupt handler.
*
*	\param		pVoidInst	Pointer to the driver instance.
*
*	\details	This function should be called from a ISR in project.c when a
*				PHY state change interrupt has been generated.
*
*				The interrupt indicates that the Ethernet link state has
*				changed.
*	\note
*
*******************************************************************************/

PUBLIC void ndrv_handleInterrupt(
	void *					pVoidInst
) {
	ndrv__Inst * 			pInst;

	pInst = (ndrv__Inst *) pVoidInst;

	pInst->triggers |= NDRV__TRG_POLLPHY;
	osa_semaSetIsr(&pInst->taskSema);
}
