/** @file
 */

#ifndef __LWIP_DHCP_H__
#define __LWIP_DHCP_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_DHCP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/netif.h"

/* tlarsu: 	Used forward declaration instead of include so that the include file
 * 			does not need to be public.
 */
//#include "lwip/udp.h"
struct ntcpip__udpPcb;

#ifdef __cplusplus
extern "C" {
#endif

/** period (in seconds) of the application calling ntcpip__dhcpCoarseTmr() */
#define DHCP_COARSE_TIMER_SECS 60 
/** period (in milliseconds) of the application calling ntcpip__dhcpCoarseTmr() */
#define DHCP_COARSE_TIMER_MSECS (DHCP_COARSE_TIMER_SECS * 1000UL)
/** period (in milliseconds) of the application calling ntcpip__dhcpFineTmr() */
#define DHCP_FINE_TIMER_MSECS 500 

struct dhcp
{
  /** transaction identifier of last sent request */ 
  Uint32 xid;
  /** our connection to the DHCP server */ 
  struct ntcpip__udpPcb *pcb;
  /** incoming msg */
  struct dhcp_msg *msg_in;
  /** incoming msg options */
  void *options_in; 
  /** ingoing msg options length */
  Uint16 options_in_len;
  /** current DHCP state machine state */
  Uint8 state;
  /** retries of current request */
  Uint8 tries;

  struct ntcpip_pbuf *p_out; /* pbuf of outcoming msg */
  struct dhcp_msg *msg_out; /* outgoing msg */
  Uint16 options_out_len; /* outgoing msg options length */
  Uint16 request_timeout; /* #ticks with period DHCP_FINE_TIMER_SECS for request timeout */
  Uint16 t1_timeout;  /* #ticks with period DHCP_COARSE_TIMER_SECS for renewal time */
  Uint16 t2_timeout;  /* #ticks with period DHCP_COARSE_TIMER_SECS for rebind time */
  struct ntcpip_ipAddr server_ip_addr; /* dhcp server address that offered this lease */
  struct ntcpip_ipAddr offered_ip_addr;
  struct ntcpip_ipAddr offered_sn_mask;
  struct ntcpip_ipAddr offered_gw_addr;
  struct ntcpip_ipAddr offered_bc_addr;
#define DHCP_MAX_DNS 2
  Uint32 dns_count; /* actual number of DNS servers obtained */
  struct ntcpip_ipAddr offered_dns_addr[DHCP_MAX_DNS]; /* DNS server addresses */
 
  Uint32 offered_t0_lease; /* lease period (in seconds) */
  Uint32 offered_t1_renew; /* recommended renew time (usually 50% of lease period) */
  Uint32 offered_t2_rebind; /* recommended rebind time (usually 66% of lease period)  */
#if NTCPIP__LWIP_DHCP_AUTOIP_COOP
  Uint8 autoip_coop_state;
#endif
/** Patch #1308
 *  TODO: See dhcp.c "TODO"s
 */
#if 0
  struct ntcpip_ipAddr offered_si_addr;
  Uint8 *boot_file_name;
#endif
};

/* MUST be compiled with "pack structs" or equivalent! */
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
/** minimum set of fields of any DHCP message */
struct dhcp_msg
{
  NTCPIP__PACK_STRUCT_FIELD(Uint8 op);
  NTCPIP__PACK_STRUCT_FIELD(Uint8 htype);
  NTCPIP__PACK_STRUCT_FIELD(Uint8 hlen);
  NTCPIP__PACK_STRUCT_FIELD(Uint8 hops);
  NTCPIP__PACK_STRUCT_FIELD(Uint32 xid);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 secs);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 flags);
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip_ipAddr ciaddr);
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip_ipAddr yiaddr);
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip_ipAddr siaddr);
  NTCPIP__PACK_STRUCT_FIELD(struct ntcpip_ipAddr giaddr);
#define DHCP_CHADDR_LEN 16U
  NTCPIP__PACK_STRUCT_FIELD(Uint8 chaddr[DHCP_CHADDR_LEN]);
#define DHCP_SNAME_LEN 64U
  NTCPIP__PACK_STRUCT_FIELD(Uint8 sname[DHCP_SNAME_LEN]);
#define DHCP_FILE_LEN 128U
  NTCPIP__PACK_STRUCT_FIELD(Uint8 file[DHCP_FILE_LEN]);
  NTCPIP__PACK_STRUCT_FIELD(Uint32 cookie);
#define DHCP_MIN_OPTIONS_LEN 68U
/** make sure user does not configure this too small */
#if ((defined(DHCP_OPTIONS_LEN)) && (DHCP_OPTIONS_LEN < DHCP_MIN_OPTIONS_LEN))
#  undef DHCP_OPTIONS_LEN
#endif
/** allow this to be configured in lwipopts.h, but not too small */
#if (!defined(DHCP_OPTIONS_LEN))
/** set this to be sufficient for your options in outgoing DHCP msgs */
#  define DHCP_OPTIONS_LEN DHCP_MIN_OPTIONS_LEN
#endif
  NTCPIP__PACK_STRUCT_FIELD(Uint8 options[DHCP_OPTIONS_LEN]);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

/** start DHCP configuration */
ntcpip_Err ntcpip__dhcpStart(struct ntcpip__netif *netif);
/** enforce early lease renewal (not needed normally)*/
ntcpip_Err ntcpip__dhcpRenew(struct ntcpip__netif *netif);
/** release the DHCP lease, usually called before ntcpip__dhcpStop()*/
ntcpip_Err ntcpip__dhcpRelease(struct ntcpip__netif *netif);
/** stop DHCP configuration */
void ntcpip__dhcpStop(struct ntcpip__netif *netif);
/** inform server of our manual IP address */
void ntcpip__dhcpInform(struct ntcpip__netif *netif);
/** Handle a possible change in the network configuration */
void ntcpip__dhcpNetworkChanged(struct ntcpip__netif *netif);

/** if enabled, check whether the offered IP address is not in use, using ARP */
#if NTCPIP__DHCP_DOES_ARP_CHECK
void ntcpip__dhcpArpReply(struct ntcpip__netif *netif, struct ntcpip_ipAddr *addr);
#endif

/** to be called every minute */
void ntcpip__dhcpCoarseTmr(void);
/** to be called every half second */
void ntcpip__dhcpFineTmr(void);
 
/** DHCP message item offsets and length */
#define DHCP_MSG_OFS (UDP_DATA_OFS)  
  #define DHCP_OP_OFS (DHCP_MSG_OFS + 0)
  #define DHCP_HTYPE_OFS (DHCP_MSG_OFS + 1)
  #define DHCP_HLEN_OFS (DHCP_MSG_OFS + 2)
  #define DHCP_HOPS_OFS (DHCP_MSG_OFS + 3)
  #define DHCP_XID_OFS (DHCP_MSG_OFS + 4)
  #define DHCP_SECS_OFS (DHCP_MSG_OFS + 8)
  #define DHCP_FLAGS_OFS (DHCP_MSG_OFS + 10)
  #define DHCP_CIADDR_OFS (DHCP_MSG_OFS + 12)
  #define DHCP_YIADDR_OFS (DHCP_MSG_OFS + 16)
  #define DHCP_SIADDR_OFS (DHCP_MSG_OFS + 20)
  #define DHCP_GIADDR_OFS (DHCP_MSG_OFS + 24)
  #define DHCP_CHADDR_OFS (DHCP_MSG_OFS + 28)
  #define DHCP_SNAME_OFS (DHCP_MSG_OFS + 44)
  #define DHCP_FILE_OFS (DHCP_MSG_OFS + 108)
#define DHCP_MSG_LEN 236

#define DHCP_COOKIE_OFS (DHCP_MSG_OFS + DHCP_MSG_LEN)
#define DHCP_OPTIONS_OFS (DHCP_MSG_OFS + DHCP_MSG_LEN + 4)

#define DHCP_CLIENT_PORT 68  
#define DHCP_SERVER_PORT 67

/** DHCP client states */
#define DHCP_REQUESTING 1
#define DHCP_INIT 2
#define DHCP_REBOOTING 3
#define DHCP_REBINDING 4
#define DHCP_RENEWING 5
#define DHCP_SELECTING 6
#define DHCP_INFORMING 7
#define DHCP_CHECKING 8
#define DHCP_PERMANENT 9
#define DHCP_BOUND 10
/** not yet implemented #define DHCP_RELEASING 11 */
#define DHCP_BACKING_OFF 12
#define DHCP_OFF 13

/** AUTOIP cooperatation flags */
#define DHCP_AUTOIP_COOP_STATE_OFF 0
#define DHCP_AUTOIP_COOP_STATE_ON 1
 
#define DHCP_BOOTREQUEST 1
#define DHCP_BOOTREPLY 2

#define DHCP_DISCOVER 1
#define DHCP_OFFER 2
#define DHCP_REQUEST 3
#define DHCP_DECLINE 4
#define DHCP_ACK 5
#define DHCP_NAK 6
#define DHCP_RELEASE 7
#define DHCP_INFORM 8

#define DHCP_HTYPE_ETH 1

#define DHCP_HLEN_ETH 6

#define DHCP_BROADCAST_FLAG 15
#define DHCP_BROADCAST_MASK (1 << DHCP_FLAG_BROADCAST)

/** BootP options */
#define DHCP_OPTION_PAD 0
#define DHCP_OPTION_SUBNET_MASK 1 /* RFC 2132 3.3 */
#define DHCP_OPTION_ROUTER 3
#define DHCP_OPTION_DNS_SERVER 6 
#define DHCP_OPTION_HOSTNAME 12
#define DHCP_OPTION_IP_TTL 23
#define DHCP_OPTION_MTU 26
#define DHCP_OPTION_BROADCAST 28
#define DHCP_OPTION_TCP_TTL 37
#define DHCP_OPTION_END 255

/** DHCP options */
#define DHCP_OPTION_REQUESTED_IP 50 /* RFC 2132 9.1, requested IP address */
#define DHCP_OPTION_LEASE_TIME 51 /* RFC 2132 9.2, time in seconds, in 4 bytes */
#define DHCP_OPTION_OVERLOAD 52 /* RFC2132 9.3, use file and/or sname field for options */

#define DHCP_OPTION_MESSAGE_TYPE 53 /* RFC 2132 9.6, important for DHCP */
#define DHCP_OPTION_MESSAGE_TYPE_LEN 1


#define DHCP_OPTION_SERVER_ID 54 /* RFC 2132 9.7, server IP address */
#define DHCP_OPTION_PARAMETER_REQUEST_LIST 55 /* RFC 2132 9.8, requested option types */

#define DHCP_OPTION_MAX_MSG_SIZE 57 /* RFC 2132 9.10, message size accepted >= 576 */
#define DHCP_OPTION_MAX_MSG_SIZE_LEN 2

#define DHCP_OPTION_T1 58 /* T1 renewal time */
#define DHCP_OPTION_T2 59 /* T2 rebinding time */
#define DHCP_OPTION_US 60
#define DHCP_OPTION_CLIENT_ID 61
#define DHCP_OPTION_TFTP_SERVERNAME 66
#define DHCP_OPTION_BOOTFILE 67

/** possible combinations of overloading the file and sname fields with options */
#define DHCP_OVERLOAD_NONE 0
#define DHCP_OVERLOAD_FILE 1
#define DHCP_OVERLOAD_SNAME  2
#define DHCP_OVERLOAD_SNAME_FILE 3

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_DHCP */

#endif /*__LWIP_DHCP_H__*/

