/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_SM4
*
*	\brief		Driver for STM32F3xx SPI peripheral.
*
*	\details	This file should not be added to the makefile. It should be
*				included into another file. Peripheral specific defines must
*				be set before including this file. This way the same driver code
*				can be used for all SSP peripherals in the MCU.
*
*	\note		
*
*	\version	\$Rev: 4420 $ \n
*				\$Date: 2021-01-13 15:28:24 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "osa.h"
#include "mem.h"

#include "../local.h"
#include "spim_com.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PUBLIC osa_isrOSDecl(SPIM__FN_ISR);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE spim__HwInst * SPIM__VAR_PINST;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__lowInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Low level SPI master initialization.
*
*	\details	The SPI interfaces are configured using the following
*				registers:
*
*				-# Power: 	
*
*				-# Clock: 	
*
*				-# Pins:  		
*
*				-# Interrupts:  
*
*				-# Setup:       
*
*	\note
*
*******************************************************************************/

PRIVATE spim__Inst * spim__lowInit(
	spim_Init const_P *		pInit
) {
	spim__HwInst *			pInst;

	pInst = (spim__HwInst *) mem_reserve(&mem_normal, sizeof(spim__HwInst));
	deb_assert(pInst != NULL);

	/*
	 * The ISR needs the instance pointer to be stored globally.
	 */

	SPIM__VAR_PINST = pInst;

	spim__pSetupPinsAndClock(pInit);

	spim__comInit(&pInst->parent, SPIM__PERIPH);
	spim__comEnableRx(SPIM__PERIPH);

	/*
	 * Initialize TX/RX info.
	 */

	pInst->pCurrData = NULL;
	pInst->txIndex = 0;
	pInst->rxIndex = 0;

	return(&pInst->parent);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__lowProcess
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Low level request handling.
*
*	\param		pInst 	Pointer to SPIM instance
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void spim__lowProcess(
	spim__Inst *			pInst
) {
	spim__HwInst *			pHwInst;
	Boolean					csLow;
	spim_SlaveInfo const_P *pSlave;

	pHwInst = (spim__HwInst *) pInst;

	pHwInst->pCurrData = &pInst->pCurrReq->data;

	deb_assert(pHwInst->pCurrData->size != 0);

	pSlave = (spim_SlaveInfo const_P *) pInst->pCurrReq->pTargetInfo;
	
	/*
	 * Configure for current slave
	 */
	spim__pConfigPinsForSlave(pInst->pInit, pSlave);
	spim__comBusSettings(pInst, SPIM__PERIPH, pSlave);

	/*
	 *	Set chip select pin to requested level.
	 */
	if ((pHwInst->pCurrData->flags & SPIM_REQ_CS_MASK) == SPIM_REQ_CS_ON) {
		csLow = TRUE;
	} else {
		csLow = FALSE;
	}

	spim__hwChipsel(pInst, csLow, pSlave);
	
	/*
	 * Setup data pointers and activate the TX interrupt. The ISR will take care
	 * of the rest.
	 */
	pHwInst->pCurrData = &pInst->pCurrReq->data;
	pHwInst->rxIndex = 0;
	pHwInst->txIndex = 0;
	pHwInst->rxOverFlCnt = 0;

	spim__hwEnableTx(SPIM__PERIPH);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__lowUp
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Activate hardware.
*
*	\param		pInst 	Pointer to SPIM instance
*
*	\details	Enables the SPI master.
*
*	\note
*
*******************************************************************************/

PRIVATE void spim__lowUp(
	spim__Inst * 			pInst
) {
	/*
	 * Enable SPI interrupt
	 */
	
	spim__hwDisableTx(SPIM__PERIPH);

	hw_enableIrq(SPIM__IRQN)
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	SPIM__FN_ISR
*
*--------------------------------------------------------------------------*//**
*
*	\brief		SSP interrupt service routine.
*
*	\details	
*
*	\note		A pointer to this ISR should be stored in the interrupt vector.
*
*******************************************************************************/

PUBLIC osa_isrOSFn(
	NULL,
	SPIM__FN_ISR
) {
	spim__HwInst *			pHwInst;
	Uint32					statusReg;
	osa_isrEnter();

	pHwInst = SPIM__VAR_PINST;

	/*
	 * The interrupt may be triggered from the following events:
	 * TX buffer empty interrupt (TXEIE)
	 * RX buffer not empty interrupt (RXNEIE)
	 * Error interrupt  (ERRIE)
	 */

	statusReg = SPIM__REG_SR;

	/*
	 *	RX fifo overflow. This should not happen.
	 */
	if (statusReg & SPI_SR_OVR) {
		deb_assert(FALSE);
		pHwInst->parent.flags |= SPIM__FLAG_ERROR;
		osa_coTaskRunIsr(&pHwInst->parent.coTask);
	}
	
	/*
	 * There is data on the HW RX FIFO
	 */
	if (statusReg & SPI_SR_RXNE) {
		spim_SlaveInfo const_P *	pSlave;
		Uint8						frameBits;

		pSlave = pHwInst->parent.pCurrReq->pTargetInfo;
		frameBits = pSlave->frameBits;

		do {
			/*
			 * Read frame from RX FIFO to data buffer
			 */
			if (frameBits <= 8) {

				Uint8 data8;

				data8 = *SPIM__REG_DR8;

				if (pHwInst->pCurrData->flags & PROTIF_FLAG_READ) {
					pHwInst->pCurrData->pData[pHwInst->rxIndex] = data8;
				}

				pHwInst->rxIndex++;

			} else {
				Uint16 data16;

				data16 = *SPIM__REG_DR16;

				if (pHwInst->pCurrData->flags & PROTIF_FLAG_READ) {
					pHwInst->pCurrData->pData[pHwInst->rxIndex++] = data16;
					pHwInst->pCurrData->pData[pHwInst->rxIndex++] = data16 >> 8;
				} else {
					pHwInst->rxIndex += 2;
				}
			}

			deb_assert(pHwInst->rxOverFlCnt != 0);
			pHwInst->rxOverFlCnt--;

			/*
			 * Current data block has been handled.
			 */
			if (pHwInst->rxIndex >= pHwInst->pCurrData->size) {
				/*
				 *	Get the next data block pointer either:
				 *	a) from the callback function or
				 *	b) from the next pointer.
				 */

				if (pHwInst->pCurrData->flags & PROTIF_FLAG_CB) {
					pHwInst->pCurrData = pHwInst->pCurrData->callback(
						pHwInst->parent.pCurrReq,
						pHwInst->pCurrData
					);

				} else {
					pHwInst->pCurrData = pHwInst->pCurrData->pNext;
					deb_assert(pHwInst->pCurrData->size !=0);
				}

				/*
				 * All data blocks have been handled.
				 */

				if (pHwInst->pCurrData == NULL) {
					/*
					 *	The RX buffer should be empty now.
					 *	Disable TX buffer empty interrupt and signal the task.
					 */

					statusReg = SPIM__REG_SR;
					deb_assert((statusReg & SPI_SR_RXNE) == 0);

					SPIM__REG_CR2 &= ~SPI_CR2_TXEIE;
					pHwInst->parent.flags |= SPIM__FLAG_DONE;
					osa_coTaskRunIsr(&pHwInst->parent.coTask);

				} else {
					Boolean csLow;

					pHwInst->rxIndex = 0;
					pHwInst->txIndex = 0;

					/*
					 *	Set chip select pin to requested level.
					 */

					if (
						(pHwInst->pCurrData->flags & SPIM_REQ_CS_MASK)
							== SPIM_REQ_CS_ON
					) {
						csLow = TRUE;
					} else {
						csLow = FALSE;
					}

					spim__hwChipsel(&pHwInst->parent, csLow, pSlave);
				}
			}

			statusReg = SPIM__REG_SR;

		} while (statusReg & SPI_SR_RXNE);
	}

	if (pHwInst->pCurrData != NULL) {
		/*
		 *	There is still data to be sent/received.
		 */

		if (statusReg & SPI_SR_TXE && (pHwInst->rxOverFlCnt == 0)) {
			spim_SlaveInfo const_P *	pSlave;
			Uint8						frameBits;

			/*
			 *	The TX buffer register is empty. Copy one byte to the buffer.
			 */

			pSlave = pHwInst->parent.pCurrReq->pTargetInfo;
			frameBits = pSlave->frameBits;

			if (pHwInst->txIndex < pHwInst->pCurrData->size) {
				/*
				 * There is still data to send/receive in the current request.
				 */

				if (frameBits <= 8) {
					Uint8 data8;

					if (pHwInst->pCurrData->flags & PROTIF_FLAG_WRITE) {
						data8 = pHwInst->pCurrData->pData[pHwInst->txIndex];

					} else {
						/*
						 *	This is a read request. Just write transmit dummy
						 *	data.
						 */
						data8 = 0xFF;
					}

					pHwInst->txIndex++;
					*SPIM__REG_DR8 = data8;
					pHwInst->rxOverFlCnt++;

				} else {
					Uint16 data16;

					if (pHwInst->pCurrData->flags & PROTIF_FLAG_WRITE) {
						data16 = pHwInst->pCurrData->pData[pHwInst->txIndex++];
						data16 |= pHwInst->pCurrData->pData[pHwInst->txIndex++] << 8;

					} else {
						/*
						 *	This is a read request. Just write transmit dummy
						 *	data.
						 */
						pHwInst->txIndex += 2;
						data16 = 0xFFFFU;
					}

					*SPIM__REG_DR16 = data16;
					pHwInst->rxOverFlCnt++;
				}
			}
		}
	}

	osa_isrLeave();
}
osa_endOSIsr
