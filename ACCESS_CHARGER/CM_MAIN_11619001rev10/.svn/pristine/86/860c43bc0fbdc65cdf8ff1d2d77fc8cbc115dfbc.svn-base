/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[x]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		st7565.h
*
*	\ingroup	ST7565
*
*	\brief		ST7565 display driver public declarations.
*
*	\details
*
*	\note
*
*	\version	
*
*	\todo		Most of the defines shold be moved to drivers local header.
*
*******************************************************************************/

#ifndef ST7565_H_INCLUDED
#define ST7565_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "protif.h"

#include "display.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/**
 * 	\name	ST7565 Display on / off for st7565_displayOnOff.
 */
/*@{*/
#define ST7565_DISP_ON			0x01	/**< Display on						*/
#define ST7565_DISP_OFF			0x00	/**< Display off					*/
/*@}*/

/**
 * 	\name	ST7565 segment driver direction for st7565_setSegDirection.
 */
/*@{*/
#define ST7565_SEG_REVERSE		0x01	/**< Reverse direction				*/
#define ST7565_SEG_NORMAL		0x00	/**< Normal direction				*/
/*@}*/

/**
 * 	\name	ST7565 segment driver direction for st7565_setInversion.
 */
/*@{*/
#define ST7565_DISP_NONINVERTED	0x00	/**< Normal display					*/
#define ST7565_DISP_INVERTED	0x01	/**< Inverted display				*/
/*@}*/

/**
 * 	\name	ST7565 all points on / off states for st7565_setAllPointsOn.
 */
/*@{*/
#define ST7565_POINTS_NORMAL	0x00	/**< Normal display					*/
#define ST7565_POINTS_ON		0x01	/**< All points on					*/
/*@}*/

/**
 * 	\name	ST7565 LCD bias for st7565_setBias.
 */
/*@{*/
#define ST7565_BIAS_1_9			0x00	/**< 1/9 bias						*/
#define ST7565_BIAS_1_7			0x01	/**< 1/7 bias						*/
/*@}*/

/**
 * 	\name	ST7565 common output mode select for st7565_setComMode.
 */
/*@{*/
#define ST7565_COM_NORMAL		0x00	/**< Normal mode					*/
#define ST7565_COM_REVERSE		0x08	/**< Reverse mode					*/
/*@}*/

/**
 * 	\name	ST7565 power controller setting bits for st7565_powerControl.
 */
/*@{*/
#define ST7565_BOOSTER_ON		(1<<2)	/**< Booster cicruit on				*/
#define ST7565_BOOSTER_OFF		0x00	/**< Booster cicruit off			*/
#define ST7565_REGU_ON			(1<<1)	/**< Voltage regulator on			*/
#define ST7565_REGU_OFF			0x00	/**< Voltage regulator off			*/
#define ST7565_FOL_ON			(1<<0)	/**< Voltage follower cicrcuit on	*/
#define ST7565_FOL_OFF			0x00	/**< Voltage follower cicrcuit off	*/
/*@}*/

/**
 * 	\name	ST7565 booster ration for st7565_setBoosterRatio.
 */
/*@{*/
#define ST7565_BOOSTER_2X		0x00	/**< Booster ratio 2x, 3x, 4x		*/
#define ST7565_BOOSTER_5X		0x01	/**< Booster ratio 5x				*/
#define ST7565_BOOSTER_6X		0x02	/**< Booster ratio 6x				*/
/*@}*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

typedef struct st7565_Instance		st7565_Instance;

typedef void (*				st7565_fnCmdData)(Boolean command);
typedef void (*				st7565_fnBacklight)(Boolean on);


/**
 * \brief		st7565 initialization structure.	
 *
 * \details		
 */
typedef struct							/*''''''''''''''''''''''''''' CONST */
{										/*									*/
	st7565_fnCmdData		fnCommand;	/**< Data / command selection func	*/
	st7565_fnBacklight		fnBacklight;/**< Backlight on / off				*/
	protif_Interface const_P *	pProtIf;/**< Bus master interface			*/
	sys_FBInstId			protInst;	/**< Bus master instance id			*/
	void const_P *			pTargetInfo;/**< Protocol specific target info  */
	Uint16					com_count;	/**< COM count						*/
	Uint16					seg_count;	/**< Segment count					*/
	Uint16					seg_offset;	/**< Segment offset					*/
	Uint8					startline;	/**< COM Start line					*/
	Uint8					segdir;		/**< Segment direction				*/
	Uint8					comdir;		/**< COM direction					*/
	Uint8					bias;		/**< LCD drive voltage bias			*/
	Uint8					power;		/**< Power supply operating mode	*/
	Uint8					resistor;	/**< Internal resistor ratio		*/
	Uint8					volume;		/**< Output voltage volume register	*/
	Uint8					booster;	/**< Booster ratio					*/
} st7565_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern const_P display_Interface st7564_interface;

/******************************************************************************/

#endif
