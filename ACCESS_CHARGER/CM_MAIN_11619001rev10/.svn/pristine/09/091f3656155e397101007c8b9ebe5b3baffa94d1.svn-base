/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	UART_S4F4
*
*	\brief		STM32F4xx UART1 driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4433 $ \n
*				\$Date: 2021-02-02 17:59:17 +0200 (ti, 02 helmi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "hw.h"
#include "drv1.h"
#include "uart.h"
#include "deb.h"

#include "..\local.h"
#include "UART_hw.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void * uart__init1(void const_P *pConfig,void * pUtil);
PRIVATE void uart__set1(void);
PRIVATE void uart__write1(void * pUart, Uint8 * pBytes, Uint16 nLen);

PRIVATE void uart__mspInitCb1(UART_HandleTypeDef * hUart);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface for USART1 with TX on pin PB6 and RX on PB7. This interface
 * should be given as a initialization parameter to the function block that will
 * be using the driver. The function block can then call the driver through this
 * interface.
 */

PUBLIC const_P drv1_Interface uart_if1TxPB6 = {
	uart__init1,
	uart__set1,
	uart__write1
};

/**
 * Public interface for USART1 without clock and pin initialization. This
 * interface should be given as a initialization parameter to the function block
 * that will be using the driver. The function block can then call the driver
 * through this interface.
 * 
 * The peripheral clock must be enabled before using this interface. The UART
 * pins must also be configured before using the interface.
 */

PUBLIC const_P drv1_Interface uart_ifNoInit1 = {
	uart__init1,
	uart__set1,
	uart__write1
};

/* These callbacks should be defined somewhere in the exe folder. */
extern uart_Callback const_P project_uart1Callback[];

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Instance of the USART1 driver.
 */

PRIVATE uart__HwUartInst 	uart__inst1;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__init1
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of UART1.
*
*	\param		pConfig Pointer to UART config structure.
*	\param		pUtil 	Pointer to protocol instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void * uart__init1(
	void const_P *			pConfig,
	void *					pUtil
) {
	uart__inst1.base.pConfig = (uart_Config const_P *) pConfig;
	uart__inst1.base.pUtil = pUtil;
	uart__inst1.base.flags = 0;
	uart__inst1.base.pSendPtr = NULL;
	
	uart__inst1.pCallbacks = project_uart1Callback;

	uart__drvInit(&uart__inst1, uart__mspInitCb1, USART1);

	return(&uart__inst1);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__set1
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Apply UART settings.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__set1(
	void
) {
	uart__drvSet(&uart__inst1);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__write1
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function for sending data to the UART.
*
*	\param		pUart 	Pointer to the uart instance
*	\param		pBytes 	Pointer to the data that is to be sent.
*	\param		nLen 	Length of the data
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__write1(
	void *					pUart,
	Uint8 *					pBytes,
	Uint16					nLen
) {
	/*
	 *	The supplied instance pointer should be valid.
	 */

	deb_assert(pUart == &uart__inst1);

	uart__drvWrite(&uart__inst1, pBytes, nLen);
}
/** \cond priv_decl */
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Interrupt service routine for uart1.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC osa_isrOSFn(NULL, USART1_IRQHandler) {
	osa_isrEnter();
	HAL_UART_IRQHandler(&uart__inst1.hwHandle);
	osa_isrLeave();
}
osa_endOSIsr
/** \endcond */
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__mspInitCb1
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Perform low level peripheral initialization.
*
*	\param		hUart		Pointer to UART driver handle.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void uart__mspInitCb1(
	UART_HandleTypeDef *	hUart
) {
	GPIO_InitTypeDef gpioInit;

	/*
	 *	Enable clocks for the peripheral device.
	 */

	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_USART1_CLK_ENABLE();

	/*
	 *	Configure the UART pins.
	 */

	/* UART TX pin configuration  */
	gpioInit.Pin       = GPIO_PIN_6;
	gpioInit.Mode      = GPIO_MODE_AF_PP;
	gpioInit.Pull      = GPIO_PULLUP;
	gpioInit.Speed     = GPIO_SPEED_FAST;
	gpioInit.Alternate = GPIO_AF7_USART1;

	HAL_GPIO_Init(GPIOB, &gpioInit);

	/* UART RX pin configuration  */
	gpioInit.Pin       = GPIO_PIN_7;
	gpioInit.Mode      = GPIO_MODE_AF_PP;
	gpioInit.Pull      = GPIO_PULLUP;
	gpioInit.Speed     = GPIO_SPEED_FAST;
	gpioInit.Alternate = GPIO_AF7_USART1;

	HAL_GPIO_Init(GPIOB, &gpioInit);

	/*
	 *	Enable the UART interrupt.
	 */

	hw_enableIrq(USART1_IRQn);
}
