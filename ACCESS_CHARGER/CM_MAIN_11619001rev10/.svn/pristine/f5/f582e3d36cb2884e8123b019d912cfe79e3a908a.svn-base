/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		REG_BUF2.C
*
*	\ingroup	REG
*
*	\brief		Supporting functions for ring buffers.
*
*	\details		
*
*	\par		Module name:
*				Register file i/o
*
*	\par		Files:
*				reg_buf2.c
*
*	\note		
*
*	\version	11-06-08 / Tommi Nakkila, Tero Kankaanpaa
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "osa.h"
#include "sys.h"
#define REG__IMPLEM /**< Declare the internal / implementation specific parts*/
#include "reg.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#if REG__INLINE_LEVEL < 50
# define REG__BUF_INLINE
#else
# define REG__BUF_INLINE		INLINE
#endif

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE REG__BUF_INLINE sys_ArrIndex *reg__bufInfoPtr(sys_RegHandle);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	*reg__bufInfoPtr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Calculate ptr to the index/size info of given ring buffer.
*
*	\param		handle 	Handle of the ring buffer.
*
*	\return		Ptr to index (REG__BUF_INDEX) and size (REG__BUF_SIZE) in RAM.
*
*	\details	
*
*	\note		May be inlined.
*
*******************************************************************************/

PRIVATE REG__BUF_INLINE sys_ArrIndex *reg__bufInfoPtr(
	sys_RegHandle			handle
) {
	sys_RegDescr const_P *pDescr;

	pDescr = reg__descrPtr(handle);		/* Ptr to ring buffer description..	*/
	pDescr++;							/*	and to index/size following it	*/

	deb_assert((pDescr->status & REG_TYPE_MASK) == SYS_REG_T__ArrIndex);

	return((sys_ArrIndex *) &sys_imageRAM[pDescr->value]);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_bufGetCount
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get current size of a ring buffer.
*
*	\param		handle 	Handle of the ring buffer.
*
*	\return		The number of elements in the ring buffer.
*
*	\details	
*
*	\note		If task scheduling is enabled over the call to this function,
*				another task may modify the ring buffer before the return value
*				is received and processed.
*
*******************************************************************************/

PUBLIC sys_ArrIndex reg_bufGetCount(
	sys_RegHandle			handle
) {
	sys_ArrIndex retVal;
	sys_ArrIndex *pBufStatus;

	pBufStatus = reg__bufInfoPtr(handle);

	atomic(
		retVal = pBufStatus[REG__BUF_SIZE];
	)
	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_bufGetIndex
*
*--------------------------------------------------------------------------*//**
*
*	\brief	
*
*	\param		handle 	Handle of the ring buffer.
*
*	\return		Returns the number of data elements in the ring buffer before
*				the operation. Zero indicates that the ring buffer was empty
*				thus no changes take place.
*
*	\details	
*
*	\note		If task scheduling is enabled over the call to this function,
*				another task may modify the ring buffer before the return value
*				is received and processed.
*
*******************************************************************************/

PUBLIC sys_ArrIndex reg_bufGetIndex(
	sys_RegHandle			handle
) {
	sys_ArrIndex retVal;
	sys_ArrIndex *pBufStatus;

	pBufStatus = reg__bufInfoPtr(handle);

	atomic(
		retVal = pBufStatus[REG__BUF_INDEX];
	)
	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_bufClear
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Clear the given ring buffer.
*
*	\param		handle 	Handle of the ring buffer.
*
*	\return		Returns the number of data elements in the ring buffer before
*				the operation. Zero indicates that the ring buffer was empty
*				thus no changes take place.
*
*	\details	Sets the size of given ring buffer to zero.
*
*	\note		If task scheduling is enabled over the call to this function,
*				another task may modify the ring buffer before the return value
*				is received and processed.
*	\par
*				NON-VOLATILE RING BUFFERS ARE NOT (YET) SUPPORTED!
*	\par
*				NO INDICATIONS TAKE PLACE WITH THIS OPERATION.
*
*******************************************************************************/

PUBLIC sys_ArrIndex reg_bufClear(
	sys_RegHandle			handle
) {
	sys_ArrIndex retVal;
	sys_ArrIndex *pBufStatus;

	pBufStatus = reg__bufInfoPtr(handle);

	atomic(
		retVal = pBufStatus[REG__BUF_SIZE];
		pBufStatus[REG__BUF_SIZE] = 0;
	)
	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_bufPopOldest
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Discard the oldest element from the given ring buffer.
*
*	\param		handle 	Handle of the ring buffer.
*
*	\return		Returns the number of data elements in the ring buffer before
*				the operation. Zero indicates that the ring buffer was empty
*				thus nothing was popped.
*
*	\details	
*
*	\note		If task scheduling is enabled over the call to this function,
*				another task may modify the ring buffer before the return value
*				is received and processed.
*	\par
*				NON-VOLATILE RING BUFFERS ARE NOT (YET) SUPPORTED!
*	\par
*				NO INDICATIONS TAKE PLACE WITH THIS OPERATION.
*
*******************************************************************************/

PUBLIC sys_ArrIndex reg_bufPopOldest(
	sys_RegHandle			handle
) {
	sys_ArrIndex retVal;
	sys_ArrIndex *pBufStatus;

	pBufStatus = reg__bufInfoPtr(handle);

	atomic(
		retVal = pBufStatus[REG__BUF_SIZE];
		if (retVal)
			pBufStatus[REG__BUF_SIZE] = retVal - 1;
	)
	return(retVal);
}
