/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	BMM
*
*	\brief		Block stream handling.
*
*	\details	
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include <string.h>

#include "TOOLS.H"
#include "BMM.H"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

#if BMM__STREAM_TESTED

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamInitRead
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize a read stream.
*
*	\param		pStream		Pointer to the stream.
*	\param		pPool		Pointer to the block pool that will be used by the
*							stream.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PUBLIC void bmm_streamInitRead(
	bmm_ReadStream *			pStream,
	bmm_BlockPool *				pPool
) {
	bmm_initList(&pStream->list, pPool);
	pStream->position = 0;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamInitWrite
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize a write stream.
*
*	\param		pStream		Pointer to the stream.
*	\param		pPool		Pointer to the block pool that will be used by the
*							stream.
*
*   \return		-
*
*	\details	
*
*******************************************************************************/

PUBLIC void bmm_streamInitWrite(
	bmm_WriteStream *			pStream,
	bmm_BlockPool *				pPool
) {
	bmm_initList(&pStream->list, pPool);
	pStream->pWriteBlock = NULL;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamRead
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read bytes from the block stream.
*
*	\param		pStream		Pointer to the stream.
*	\param		pData		Pointer to the destination buffer for the data.
*	\param		bytes		Number of bytes that should be copied.
*
*   \retval		TRUE		The data was read successfully.
*   \retval		FALSE		Not enough bytes in the block list.
*
*	\details	
*
*******************************************************************************/

PUBLIC Boolean bmm_streamRead(
	bmm_ReadStream * 		pStream,
	BYTE * 					pData,
	Uint8					bytes
) {
	bmm_BlockSize			blockBytes;	/* Number of bytes left in the block. */
	
	deb_assert(bytes > 0);
	deb_assert(pData != NULL);
	deb_assert(pStream != NULL);

	if (bmm_listIsEmpty(&pStream->list)) {
		/*
		 *	The list is empty.
		 */
		return FALSE;
	}
	
	blockBytes = pStream->list.pRead->usedSize - pStream->position;
	
	do {
		Ufast8 	copyCount;
		BYTE * 	pBuffer;
		
		pBuffer = bmm_getBuffer(pStream->list.pRead, NULL);
		pBuffer = &pBuffer[pStream->position];
		
		copyCount = MIN(blockBytes, bytes);

		memcpy(pData, pBuffer, copyCount);
		pData += copyCount;
		pBuffer += copyCount;

		pStream->position += (bmm_BlockSize) copyCount;

		bytes -= (Uint8) copyCount;
		blockBytes -= (bmm_BlockSize) copyCount;
		if (blockBytes == 0) {
			bmm_Block * pHandledBlock;
			
			/*
			 *	The whole block has been handled. Move to the next block.
			 */

			pHandledBlock = bmm_getFirst(&pStream->list);
			bmm_freeBlock(pStream->list.pPool, pHandledBlock);
			pStream->position = 0;
			
			if (bmm_listIsEmpty(&pStream->list)) {
				/*
				 *	No more blocks in the list.
				 */
				break;
			}

			blockBytes = pStream->list.pRead->usedSize;
		}

	} while (bytes);

	return(bytes == 0 ? TRUE : FALSE);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamWrite
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write data to block stream.
*
*	\param		pStream		Pointer to the stream.
*	\param		pData		Pointer to data to be written.
*	\param		bytes		Number of data bytes.
*
*   \retval		TRUE		The data was copied successfully.
*   \retval		FALSE		Could not allocate new blocks for the data. No data
*							was copied.
*
*	\details	
*
*******************************************************************************/

Boolean bmm_streamWrite(
	bmm_WriteStream *			pStream,
	BYTE *						pData,
	Uint8						bytes
) {
	BYTE * 						pBuffer;
	bmm_BlockSize				blockBytes;	/* Number of bytes left in the block. */
	
	/*
	 *	TODO:	There is a big problem if memory blocks cannot be allocated
	 *			in the middle of a copy. Some data may have been copied and
	 *			some still left to copy. The buffer will then be in messy state
	 *			and the user has no clue how much has been copied.
	 *			
	 *			Solution:
	 *			All needed blocks should be allocated before copying any data.
	 *			Call bmm_allocBlockList() to allocate the blocks.
	 *			
	 *			If allocation fails then all the allocated blocks are freed and
	 *			this function returns FALSE.
	 */

	if (pStream->pWriteBlock == NULL) {
		pStream->pWriteBlock = bmm_allocBlock(pStream->list.pPool);
		if (pStream->pWriteBlock == NULL) {
			return(FALSE);
		}
	}

	pBuffer = bmm_getBuffer(pStream->pWriteBlock, NULL);
	pBuffer = &pBuffer[pStream->pWriteBlock->usedSize];
	blockBytes =pStream->list.pPool->blockSize - pStream->pWriteBlock->usedSize;

	do {
		Ufast8 copyCount;

		copyCount = MIN(blockBytes, bytes);

		memcpy(pBuffer, pData, copyCount);
		pBuffer += copyCount;
		pData += copyCount;

		pStream->pWriteBlock->usedSize += (bmm_BlockSize) copyCount;

		bytes -= (Uint8) copyCount;
		blockBytes -= (bmm_BlockSize) copyCount;
		if (blockBytes == 0) {
			/*
			 *	The block has been filled.
			 */
			
			bmm_putLast(&pStream->list, pStream->pWriteBlock);

			/*
			 *	TODO:	Do not allocate new blocks here. Use the blocks that
			 *			have been allocated before copying was started.
			 */

			pStream->pWriteBlock = bmm_allocBlock(pStream->list.pPool);
			if (pStream->pWriteBlock == NULL) {
				break;
			}
			pBuffer = bmm_getBuffer(pStream->pWriteBlock, NULL);
			blockBytes = pStream->list.pPool->blockSize;
		}

	} while (bytes);

	return(bytes == 0 ? TRUE : FALSE);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamFlush
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Skip to the next write block.
*
*	\param		pStream		Pointer to the stream.
*
*   \return		-
*
*	\details	This will flush the current write block to the block list.
*
*******************************************************************************/

PUBLIC void bmm_streamFlush(
	bmm_WriteStream *			pStream
) {
	/*
	 *	There is no point in skipping the current block if no block has
	 *	been allocated.
	 */
	deb_assert(pStream->pWriteBlock != NULL);

	if (pStream->pWriteBlock->usedSize != 0) {
		/*
		 *	There is data in the current block. Flush the current block to the
		 *	block list and allocate a new block.
		 */

		bmm_putLast(&pStream->list, pStream->pWriteBlock);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamWriteSkip
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Increment the write pointer the specified amount of bytes.
*
*	\param		pStream		Pointer to the stream.
*	\param		skipBytes	Number of bytes that should be skipped.
*
*   \retval		TRUE		The bytes were skipped successfully.
*   \retval		FALSE		Attempt to skip more bytes than available in the
*							current block.
*
*	\details	This function should be used in conjunction with the
*				bmm_streamGetWriteStat function.
*	
*	\see		bmm_streamGetWriteStat
*
*******************************************************************************/

PUBLIC Boolean bmm_streamWriteSkip(
	bmm_WriteStream *			pStream,
	Uint8						skipBytes
) {
	Boolean						retVal;
	bmm_BlockSize				blockBytes;	/* Number of bytes left in the block. */
	
	deb_assert(pStream->list.pPool != NULL);

	/*
	 *	There is no point in skipping if no write block has been allocated.
	 *	bmm_streamGetWriteStat() should be called first to check how many bytes
	 *	can be written.
	 */
	deb_assert(pStream->pWriteBlock != NULL);
	
	blockBytes =pStream->list.pPool->blockSize - pStream->pWriteBlock->usedSize;

	retVal = FALSE;
	if (blockBytes >= skipBytes) {
		pStream->pWriteBlock->usedSize += (bmm_BlockSize) skipBytes;
		retVal = TRUE;
	}

	return(retVal);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamGetWriteStat
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get write information about the underlaying data buffer.
*
*	\param		pStream			Pointer to the stream.
*	\param		pFreeBytes		The remaining buffer space will be stored to
*								this variable.
*
*   \return		Pointer to the buffer at current position. NULL is returned if
*				no write buffer was allocated and a new one could not be
*				allocated.
*
*	\details	This function is needed when data needs to be copied directly
*				to the block buffer without using the BMM stream functions.
*				The bmm_streamWriteSkip function must be called after the data
*				has been copied to report the amount of data that has been
*				copied.
*	
*				If the available buffer space is not enough then
*				bmm_streamFlush can be called to skip to a new block with an
*				empty buffer.
*				
*	\see		bmm_streamFlush, bmm_streamWriteSkip
*
*******************************************************************************/

PUBLIC BYTE * bmm_streamGetWriteStat(
	bmm_WriteStream *			pStream,
	bmm_BlockSize *				pFreeBytes
) {
	BYTE * 						pBuffer;
	bmm_BlockSize				usedSize;

	if (pStream->pWriteBlock == NULL) {
		pStream->pWriteBlock = bmm_allocBlock(pStream->list.pPool);
		if (pStream->pWriteBlock == NULL) {
			*pFreeBytes = 0;
			return(NULL);
		}
	}

	pBuffer = bmm_getBuffer(pStream->pWriteBlock, &usedSize);
	pBuffer = &pBuffer[usedSize];

	*pFreeBytes =
		pStream->list.pPool->blockSize - usedSize;

	return(pBuffer);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	bmm_streamGetReadStat
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Get read information about the underlying data buffer.
*
*	\param		pStream			Pointer to the stream.
*	\param		pBytesLeft		The number of data bytes left in the buffer will
*								be stored here.
*
*   \return		Pointer to the buffer at current position.
*
*	\details	
*
*******************************************************************************/

PUBLIC BYTE * bmm_streamGetReadStat(
	bmm_ReadStream *			pStream,
	bmm_BlockSize *				pBytesLeft
) {
	BYTE * 						pBuffer;
	bmm_BlockSize				usedSize;

	pBuffer = bmm_getBuffer(pStream->list.pRead, &usedSize);
	pBuffer = &pBuffer[pStream->position];

	*pBytesLeft = usedSize - pStream->position;

	return(pBuffer);
}
#else
PUBLIC Uint8 bmm__dummy = 0;
#endif
