/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		REG_LOC.C
*
*	\ingroup	REG
*
*	\brief		Functions that use local indexes instead of parameter handles.
*
*	\details		
*
*	\par		Module name:
*				Register file i/o
*
*	\par		Files:
*				reg_loc.c
*
*	\note		
*
*	\version	Refer SVN.
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "sys.h"
#define REG__IMPLEM /**< Declare the internal / implementation specific parts*/
#include "reg.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_putLocal
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write a new value to a local non-array register.
*
*	\param		pData 	Ptr to the data value to write.
*	\param		index 	Local index (0..) of the register.
*	\param		id 		ID of the FB instance as given to its initialization
*						function.
*
*	\return		Status of the operation.
*
*	\details	
*
*	\note		This function is for "occasional use" only. For frequent use of
*				local parameters the base index should be obtained once (see the
*				reg_indexToHandle() macro) and added to each local index to
*				convert them to corresponding global handles for e.g. reg_put()
*				to use.
*
*******************************************************************************/

PUBLIC reg_Status reg_putLocal(
	void *					pData,
	sys_RegHandle			index,
	sys_FBInstId			id
) {
	index += reg__fbInstDescrPtr(id)->paramBase;

	return(reg_aPut(pData, index, 0));
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg_getLocal
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Read the value from a local non-array register.
*
*	\param		pData 	Ptr to the storage for the read data.
*	\param		index 	Local index (0..) of the register.
*	\param		id 		ID of the FB instance as given to its initialization
*						function.
*
*	\return		Status of the operation.
*
*	\details	
*
*	\note		This function is for "occasional use" only. For frequent use of
*				local parameters the base index should be obtained once (see the
*				reg_indexToHandle() macro) and added to each local index to
*				convert them to corresponding global handles for e.g. reg_get()
*				to use.
*
*******************************************************************************/

PUBLIC reg_Status reg_getLocal(
	void *					pData,
	sys_RegHandle			index,
	sys_FBInstId			id
) {
	index += reg__fbInstDescrPtr(id)->paramBase;

	return(reg_aGet(pData, index, 0));
}
