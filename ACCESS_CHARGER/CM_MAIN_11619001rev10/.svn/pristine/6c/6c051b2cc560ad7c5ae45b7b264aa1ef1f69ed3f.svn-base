/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		I16TOA.C
*
*	\ingroup	UTIL
*
*	\brief		Int16 to string conversion.
*
*	\note		
*
*	\version
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************
;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "util.h"
#include "deb.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	util_i16toa
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Converts signed 16 bit value to ASCII.
*
*	\param		nVal 	Value to convert.
*	\param		pBuf 	Target buffer.
*	\param		nSize 	Size of target buffer.
*	\param		nBase 	Base to use (2, 8, 10 or 16).
*
*	\return		Pointer to position just after string (the ending null).
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC char * util_i16toa(
	Int16					nVal,
	char *					pBuf,
	Uint8					nSize,
	Uint8					nBase
) {
	char *					pStart;
	Uint16					nTmp;

	if (nVal < 0)
	{
		*pBuf++ = '-';
		nSize -= 1;

		if (nVal == -32768)
		{
			nTmp = 32768;
		}
		else
		{
			nTmp = -nVal;
		}
	}
	else
	{
		nTmp = nVal;
	}

	pStart = &pBuf[nSize];
	*--pStart = '\0';

	switch(nBase) 
	{
	case 2:
		deb_assert(FALSE); // To be implemented
		break;

	case 8:
		deb_assert(FALSE); // To be implemented
		break;

	case 10:
		pStart = util_u16toadec(nTmp, pBuf, pStart);
		break;

	case 16:
		pStart = util_u16toahex(nTmp, pBuf, pStart);
		break;

	default:
		deb_assert(FALSE);
		break;
	}

	if (pStart > pBuf) 
	{
		while (*pStart) 
		{
			*pBuf++ = *pStart++;
		}

		*pBuf = '\0';

		return pBuf;
	}

	return &pBuf[nSize - 1];
}
