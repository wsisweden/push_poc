/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_cm3/IICMSTR2.C
*
*	\ingroup	IICMSTR_CM3
*
*	\brief		
*
*	\details	
*
*	\note		
*
*	\version	\$Rev: $ \n
*				\$Date: $ \n
*				\$Author: $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"
#include "deb.h"

#include "..\iicmstr_local.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define IICMSTR_PCONP		26					/**< Power bit for PCONP    */

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

#define IICMSTR_CONSET		LPC_I2C2->I2CONSET	/**< Control register       */
#define IICMSTR_CONCLR		LPC_I2C2->I2CONCLR	/**< Control clear register */
#define IICMSTR_STAT		LPC_I2C2->I2STAT	/**< Status register        */
#define IICMSTR_DAT			LPC_I2C2->I2DAT		/**< Data register          */
#define IICMSTR_ADDR		LPC_I2C2->I2ADR0	/**< Slave address register */

#define IICMSTR_SCLL		LPC_I2C2->I2SCLL	/**< SCL low duty register  */
#define IICMSTR_SCLH		LPC_I2C2->I2SCLH	/**< SCL high duty register */

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void				iicmstr__init2(iicmstr_Inst *);
PRIVATE void				iicmstr__noInit2(iicmstr_Inst * pInst);
PRIVATE void				iicmstr__process2(void);

PUBLIC						osa_isrOSDecl(I2C2_IRQHandler);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface constant for the I2C2 peripheral using P0.11 pin as SCL and
 * P0.10 as SDA.
 */
PUBLIC const_P iicmstr__If	iicmstr_if2 = {
	iicmstr__init2,
	iicmstr__process2
};

/**
 * Public interface constant for the I2C2 peripheral without any pin
 * configuration. The Pin configuration must be done outside of this FB when
 * this constant is used.
 */
PUBLIC const_P iicmstr__If	iicmstr_if2NoPinInit = {
	iicmstr__init2,
	iicmstr__process2
};

/**
 * Public interface constant for the I2C2 peripheral without any clock or pin
 * configuration. The clock and pins must be configured separately if this
 * constant is used.
 */
PUBLIC const_P iicmstr__If	iicmstr_if2NoInit = {
	iicmstr__noInit2,
	iicmstr__process2
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE iicmstr__Hw			iicmstr__hwInstance2;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__init2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		IICMSTR low level initialization function.
*
*	\param		pInst 	ptr to IICMSTR instance
*
*	\return
*
*	\details	Sets up the NXP I2C port and interrupt.
*
*	\note	
*
*******************************************************************************/

PRIVATE void iicmstr__init2(
	iicmstr_Inst *			pInst
) {
	iicmstr__hwInstance2.pMain = pInst;

	LPC_SC->PCONP |= (1<<IICMSTR_PCONP);			/* Power on to I2C */
	LPC_SC->PCLKSEL1 |= (1U<<20);					/* Clock divider 1 */

	if (pInst->pInit->fn == &iicmstr_if2) {
		/* Configure as I2c pins */
		LPC_PINCON->PINSEL0 &= ~((1U<<20)|(1U<<22));
		LPC_PINCON->PINSEL0 |= ((1U<<21)|(1U<<23));
	}

	/*
	 *	Enable I2C isr.
	 */
#if TARGET_SELECTED & TARGET_CM3
	hw_enableIrq(I2C0_IRQn);	

	IICMSTR_SCLL = pInst->pInit->settings.CLLL;
	IICMSTR_SCLH = pInst->pInit->settings.CLLH;
#endif

	/*
	 *	Enable master functions
	 */
	IICMSTR_CONSET = IICMSTR_CS_ENA;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__noInit2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		IICMSTR "init" when NoInit is selected. 
*
*	\param		pInst 	ptr to IICMSTR instance
*
*	\return
*
*	\details	Sets the instance pointer to be part of the static hwInstance.
*
*	\note	
*
*******************************************************************************/

PRIVATE void iicmstr__noInit2(
    iicmstr_Inst *          pInst
) {
    iicmstr__hwInstance2.pMain = pInst;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr__process2
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Processes I2C requests from common IICMSTR code.
*
*	\return
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PRIVATE void iicmstr__process2(
	void
) {
	iicmstr__Hw *			pInst;

	pInst = &iicmstr__hwInstance2;

	deb_assert(pInst->pMain != NULL);
	deb_assert(pInst->pMain->pReq != NULL);

	/*
	 * Initialize variables for ISR.
	 */
	pInst->pSubReq = &pInst->pMain->pReq->data;

	pInst->slave =
		((iicmstr_SlaveInfo const_P *)pInst->pMain->pReq->pTargetInfo)->address;
	pInst->slave <<= 1;
	pInst->pos = 0;

	if (pInst->pSubReq->flags & PROTIF_FLAG_READ)
	{
		pInst->slave |= IICMSTR_READ_BIT;
	}

	/*
	 * When state is set to BUSY then a request is active
	 * and ISR can modify request and instance variables.
	 */
	atomic ( pInst->pMain->state = IICMSTR__BUSY; )

	/*
	 * Forced access to the I2C bus (LPC17xx User Manual p. 463).
	 *
	 * If an uncontrolled source generates a superfluous START or masks a STOP condition,
	 * then the I2C-bus stays busy indefinitely. If the STA flag is set and bus access is not
	 * obtained within a reasonable amount of time, then a forced access to the I2C-bus is
	 * possible. This is achieved by setting the STO flag while the STA flag is still set. No STOP
	 * condition is transmitted. The I2C hardware behaves as if a STOP condition was received
	 * and is able to transmit a START condition. The STO flag is cleared by hardware
	 */
	if (IICMSTR_CONSET & IICMSTR_CS_START) {
		IICMSTR_CONSET = IICMSTR_CS_STOP;
	}

	IICMSTR_CONSET |= IICMSTR_CS_START; /* Generate start */
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	iicmstr2__isr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		I2C interrupt handler.
*
*	\return
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC osa_isrOSFn(
	NULL,
	I2C2_IRQHandler
) {
	Uint8					state;
	iicmstr__Hw *			pInst;

	osa_isrEnter();

	pInst = &iicmstr__hwInstance2;
	state = IICMSTR_STAT;

	/*
	 * Interrupt function may be triggered by garbage on SCL pin.
	 * If no request is active, just return.
	 *
	 * Instance variable 'state' is used to protect other instance
	 * variables.
	 */
	deb_assert(pInst->pMain != NULL);
	if (pInst->pMain->state != IICMSTR__BUSY) {

		/*
		 * Recover from error by transmitting stop.
		 */
		IICMSTR_CONSET = IICMSTR_CS_STOP | IICMSTR_CS_AA;
		IICMSTR_CONCLR = IICMSTR_CS_START;
		IICMSTR_CONCLR = IICMSTR_CS_SI;
		return;
	}

	/*
	 * Low level instance variable 'result' holds return code for
	 * active request.
	 *
	 * This result code is copied to upper level instance variable
	 * 'result' when request is complete.
	 */
	pInst->result = PROTIF_INVALID_RESULT;

	switch (state)
	{
	case 0x08:
		/*
		 *	A Start condition has been transmitted. The Slave Address + R/W
		 *	bit will be transmitted, an ACK bit will be received.
		 *
		 *	1. Write Slave Address with R/W bit to I2DAT.
		 *	2. Write 0x04 to I2CONSET to set the AA bit.
		 *	3. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	4. Set up Master Transmit mode data buffer.
		 *	5. Set up Master Receive mode data buffer.
		 *	6. Initialize Master data counter.
		 *	7. Exit
		 */

		IICMSTR_DAT = pInst->slave;
		IICMSTR_CONSET = IICMSTR_CS_AA;
		break;

	case 0x10:
		/*
		 *	A repeated Start condition has been transmitted. The Slave
		 *	Address + R/W bit will be transmitted, an ACK bit will be
		 *	received.
		 *
		 *	1. Write Slave Address with R/W bit to I2DAT.
		 *	2. Write 0x04 to I2CONSET to set the AA bit.
		 *	3. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	4. Set up Master Transmit mode data buffer.
		 *	5. Set up Master Receive mode data buffer.
		 *	6. Initialize Master data counter.
		 *	7. Exit
		 */

		if (pInst->pos < pInst->pSubReq->size)
		{
			IICMSTR_DAT = pInst->slave;
			IICMSTR_CONSET = IICMSTR_CS_AA;
		}
		else
		{
			IICMSTR_CONSET = IICMSTR_CS_STOP;
			IICMSTR_CONCLR = IICMSTR_CS_START;
			pInst->result = PROTIF_OK;
		}
		break;

	case 0x18:
		/*
		 *	Previous state was State 8 or State 10, Slave Address + Write
		 *	has been transmitted, ACK has been received. The first data
		 *	byte will be transmitted, an ACK bit will be received.
		 *
		 *	1. Load I2DAT with first data byte from Master Transmit buffer.
		 *	2. Write 0x04 to I2CONSET to set the AA bit.
		 *	3. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	4. Increment Master Transmit buffer pointer.
		 *	5. Exit
		 */

		if (pInst->pos < pInst->pSubReq->size)
		{
			IICMSTR_DAT = pInst->pSubReq->pData[pInst->pos++];
			IICMSTR_CONSET = IICMSTR_CS_AA;
			IICMSTR_CONCLR = IICMSTR_CS_START;
		}
		else
		{
			// todo: size 0 write => restart / stop
			IICMSTR_CONSET = IICMSTR_CS_START;
		}
		break;

	case 0x20:
		/*
		 *	Slave Address + Write has been transmitted, NOT ACK has been
		 *	received. A Stop condition will be transmitted.
		 *
		 *	1. Write 0x14 to I2CONSET to set the STO and AA bits.
		 *	2. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	3. Exit
		 */
		IICMSTR_CONSET = IICMSTR_CS_STOP|IICMSTR_CS_AA;
		IICMSTR_CONCLR = IICMSTR_CS_START;
		pInst->result = PROTIF_NACK;
		break;

	case 0x28:
		/*
		 *	Data has been transmitted, ACK has been received. If the
		 *	transmitted data was the last data byte then transmit a Stop
		 *	condition, otherwise transmit the next data byte.
		 *
		 *	1. Decrement the Master data counter, skip to step 5 if not the
		 *		last data byte.
		 *	2. Write 0x14 to I2CONSET to set the STO and AA bits.
		 *	3. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	4. Exit
		 *	5. Load I2DAT with next data byte from Master Transmit buffer.
		 *	6. Write 0x04 to I2CONSET to set the AA bit.
		 *	7. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	8. Increment Master Transmit buffer pointer
		 *	9. Exit
		 */

		if (pInst->pos < pInst->pSubReq->size)
		{
			/*
			 *	Send next byte
			 */
			IICMSTR_DAT = pInst->pSubReq->pData[pInst->pos++];
			IICMSTR_CONSET = IICMSTR_CS_AA;
		}
		else
		{
			/*
			 *	Request done, check if chained requests
			 */
			if (pInst->pSubReq->pNext)
			{
				pInst->pSubReq = pInst->pSubReq->pNext;

				if (pInst->pSubReq->flags & PROTIF_FLAG_CONT)
				{
					/*
					 *	Continue immediately with write
					 */
					deb_assert(!(pInst->slave & IICMSTR_READ_BIT));

					pInst->pos = 1;
					IICMSTR_DAT = pInst->pSubReq->pData[0];
					IICMSTR_CONSET = IICMSTR_CS_AA;
				}
				else
				{
					/*
					 *	Generate restart and proceed with next request
					 */

					if (pInst->pSubReq->flags & PROTIF_FLAG_WRITE)
					{
						/* Write operation */
						pInst->slave &= ~IICMSTR_READ_BIT;
					}
					else
					{
						/* Read operation */
						pInst->slave |= IICMSTR_READ_BIT;
					}

					pInst->pos = 0;
					IICMSTR_CONSET = IICMSTR_CS_START | IICMSTR_CS_AA;
				}
			}
			else
			{
				/*
				 *	All done
				 */
				IICMSTR_CONSET = IICMSTR_CS_STOP | IICMSTR_CS_AA;
				pInst->result = PROTIF_OK;
			}
		}
		break;

	case 0x30:
		/*
		 *	Data has been transmitted, NOT ACK received. A Stop condition
		 *	will be transmitted.
		 *
		 *	1. Write 0x14 to I2CONSET to set the STO and AA bits.
		 *	2. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	3. Exit
		 */
		IICMSTR_CONSET = IICMSTR_CS_STOP | IICMSTR_CS_AA;
		pInst->result = PROTIF_NACK;
		break;

	case 0x40:
		/*
		 *	Previous state was State 08 or State 10. Slave Address + Read has
		 *	been transmitted, ACK has been received. Data will be received and
		 *	ACK returned.
		 *
		 *	1. Write 0x04 to I2CONSET to set the AA bit.
		 *	2. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	3. Exit
		 */
		if (pInst->pos >= pInst->pSubReq->size)
		{
			IICMSTR_CONSET = IICMSTR_CS_STOP;
			IICMSTR_CONCLR = IICMSTR_CS_AA;
		}
		else if (pInst->pos == (pInst->pSubReq->size - 1))
		{
			/*
			 *	First byte is the last byte => reply with NACK
			 */
			IICMSTR_CONCLR = IICMSTR_CS_AA;
		}
		else
		{
			/*
			 *	Read byte reply with ACK
			 */
			IICMSTR_CONSET = IICMSTR_CS_AA;
		}

		IICMSTR_CONCLR = IICMSTR_CS_START;
		break;

	case 0x48:
		/*
		 *	Slave Address + Read has been transmitted, NOT ACK has been
		 *	received. A Stop condition will be transmitted.
		 *
		 *	1. Write 0x14 to I2CONSET to set the STO and AA bits.
		 *	2. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	3. Exit
		 */
		IICMSTR_CONSET = IICMSTR_CS_STOP | IICMSTR_CS_AA;
		IICMSTR_CONCLR = IICMSTR_CS_START;
		pInst->result = PROTIF_NACK;
		break;

	case 0x50:
		/*
		 *	Data has been received, ACK has been returned. Data will be read
		 *	from I2DAT. Additional data will be received. If this is the last
		 *	data byte then NOT ACK will be returned, otherwise ACK will be
		 *	returned.
		 *
		 *	1. Read data byte from I2DAT into Master Receive buffer.
		 *	2. Decrement the Master data counter, skip to step 5 if not the last
		 *		data byte.
		 *	3. Write 0x0C to I2CONCLR to clear the SI flag and the AA bit.
		 *	4. Exit
		 *	5. Write 0x04 to I2CONSET to set the AA bit.
		 *	6. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	7. Increment Master Receive buffer pointer
		 *	8. Exit
		 */
		pInst->pSubReq->pData[pInst->pos++] = IICMSTR_DAT;

		if (pInst->pos == (pInst->pSubReq->size - 1))
		{
			/*
			 *	Last byte => reply with NACK
			 */
			IICMSTR_CONCLR = IICMSTR_CS_AA;
		}
		else
		{
			/*
			 *	Read byte reply with ACK
			 */
			IICMSTR_CONSET = IICMSTR_CS_AA;
		}
		break;

	case 0x58:
		/*
		 *	Data has been received, NOT ACK has been returned. Data will be
		 *	read from I2DAT. A Stop condition will be transmitted.
		 *
		 *	1. Read data byte from I2DAT into Master Receive buffer.
		 *	2. Write 0x14 to I2CONSET to set the STO and AA bits.
		 *	3. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	4. Exit
		 */

		pInst->pSubReq->pData[pInst->pos++] = IICMSTR_DAT;
		deb_assert(pInst->pos == pInst->pSubReq->size);

		IICMSTR_CONSET = IICMSTR_CS_STOP | IICMSTR_CS_AA;
		pInst->result = PROTIF_OK;
		break;

	case 0x00:
		/*
		 *	Bus Error. Enter not addressed Slave mode and release bus.
		 *
		 *	1. Write 0x14 to I2CONSET to set the STO and AA bits.
		 *	2. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	3. Exit
		 */
		IICMSTR_CONSET = IICMSTR_CS_STOP | IICMSTR_CS_AA;
		pInst->result = PROTIF_ERROR;
		break;

	case 0x38:
		/*
		 *	Arbitration has been lost during Slave Address + Write or data.
		 *	The bus has been released and not addressed Slave mode is
		 *	entered. A new Start condition will be transmitted when the bus
		 *	is free again.
		 *
		 *	1. Write 0x24 to I2CONSET to set the STA and AA bits.
		 *	2. Write 0x08 to I2CONCLR to clear the SI flag.
		 *	3. Exit
		 */
		IICMSTR_CONSET = IICMSTR_CS_STOP | IICMSTR_CS_AA;
		pInst->result = PROTIF_ERROR;
		break;

	default:	/* Unexpected state											*/
		break;
	}

	/*
	 * If low level instance variable 'result' is not INVALID,
	 * then the request is complete.
	 *
	 * Copy result code to higher level instance variable 'result'
	 * and indicate that request is complete.
	 */
	if (pInst->result != PROTIF_INVALID_RESULT)
	{
		deb_assert(pInst->pMain->pReq);

		pInst->pMain->result = pInst->result;

		pInst->pMain->state = IICMSTR__DONE;
		osa_coTaskRunIsr(&pInst->pMain->task);
	}

	/* Clear SI flag */
	IICMSTR_CONCLR = IICMSTR_CS_SI;

	osa_isrLeave();
}
osa_endOSIsr
