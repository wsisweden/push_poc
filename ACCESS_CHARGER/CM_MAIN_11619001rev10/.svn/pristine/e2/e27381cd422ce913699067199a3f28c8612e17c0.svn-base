/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		LIB_SFE0.C
*
*	\ingroup	LIB
*
*	\brief		Functions to convert integer data to stream of bytes and vice
*				versa.
*
*	\details	By the conversion algorithm, two reserved byte values are not
*				written into the stream in order to allow e.g. a protocol to
*				use them in special meaning. 254 byte values remain available
*				and hence the name of the module "FE".
*
*				Data is written to and read from a stream as fields. A field
*				is an integer ranging from zero to a maximum value that can be
*				any integer value starting from one. Note that fields are not
*				limited to bit or byte boundaries of the stream.
*
*				The functions include:
*
*				lib_sFeInit()		Prepare a stream for writing/reading fields
*				lib_sFeWrite8()		Write a field of up to 8 bits into stream
*				lib_sFeWrite16()	Write a field of up to 16 bits into stream
*				lib_sFeRead8()		Read a field of up to 8 bits from stream
*				lib_sFeRead16()		Read a field of up to 16 bits from stream
*
*	\note		
*
*	\version	\$Rev: $ \n
*				\$Date:: $ \n
*				\$Author: $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "lib.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_sFeInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Prepare a stream for writing or reading fields.
*
*	\param		pInfo		Pointer to a structure to be initialized.
*	\param		pBuffer		Pointer to the start of the buffer for the stream.
*	\param		length		The maximum number of bytes to write or read.
*
*	\details	This function needs to be called before any of the related
*				conversion functions.
*
*	\note
*
*	\sa
*
*******************************************************************************/

PUBLIC void lib_sFeInit(
	lib_SFeInfo *			pInfo,
	Uint8 *					pBuffer,
	Uint8					length
) {
	pInfo->bufPtr = pBuffer;
	pInfo->bufLen = length;
	pInfo->bufByte = 0;
	pInfo->bufUsed = 0;
}
