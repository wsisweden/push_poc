/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2010, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Inc_cm3/SPIM_0.C
*
*	\ingroup	SPIM_CM3
*
*	\brief		SPI specific code.
*
*	\details
*
*	\note
*
*	\version	25-07-2008 / Ari Suomi
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "osa.h"
#include "mem.h"
#include "Cm3_Arm7.h"

#include "../local.h"
#include "spim_0.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

#define SPIM__SPI_

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE spim_Inst * 		spim__lowInit(void);
PRIVATE void 				spim__lowProcess(spim_Inst *);
PRIVATE void 				spim__lowUp(spim_Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
* Tools for physical bus
*/

PUBLIC spim__Tools const_P	SPIM__VAR_TOOLS = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE spim__HwInst * 		spim__pInst0;

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__lowInit
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Low level SPI master initialization.
*
*	\param		pInst
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE spim_Inst * spim__lowInit(
	void
) {
	spim__HwInst *			pInst;

	pInst = (spim__HwInst *) mem_reserve(&mem_normal, sizeof(spim__HwInst));

	/*
	 * The ISR needs the instance pointer to be stored globally.
	 */

	SPIM__VAR_PINST = pInst;

	/*
	 * The SPI is configured using the following registers:
	 *
	 * 1. 	Power: 		In the PCONP register, set bit PCSPI.
	 *
	 * 					Remark: 	On reset, the SPI is enabled (PCSPI = 1).
	 *
	 * 2. 	Clock: 		In PCLK_SEL0 select PCLK_SPI. In master mode, the clock
	 * 					must be scaled down.
	 *
	 * 3. 	Pins: 		Select SPI pins and their modes in PINSEL0 to PINSEL4
	 * 					and PINMODE0 to PINMODE4 (see Section 9�5).
	 *
	 * 4. 	Interrupts: Interrupts are enabled in the S0SPINT register.
	 * 					Interrupts are enabled in the VIC using the VICIntEnable
	 * 					register.
	 *
	 * 					Remark: 	In the VIC, the SPI shares its interrupts
	 * 								with the SSP0 interface.
	 */

	PCONP |= 1 << SPIM__PCONP_SPI;

	/*
	 * Set the SPI clock divider.
	 */

	SPIM__REG_PCLKSEL |= SPIM__PCLKSEL_DIV1 << SPIM__PCLKSEL_SPI;

	/*
	 * Set the pin functions to SPI
	 */

	PINSEL0 |= SPIM__PINSEL_FUNC << SPIM__PINSEL_SCK;
	PINSEL1 |= SPIM__PINSEL_FUNC << SPIM__PINSEL_MISO;
	PINSEL1 |= SPIM__PINSEL_FUNC << SPIM__PINSEL_MOSI;

	/*
	 * Enable interrupts.
	 */

	S0SPCR |= SPIM__S0SPCR_SPIE;

	/*
	 * set device to master mode.
	 */

	S0SPCR |= SPIM__S0SPCR_MSTR;

	return((spim_Inst *) pInst);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__lowProcess
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize the SPI module
*
*	\param		pInst
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void spim__lowProcess(
	spim_Inst *				pInst
) {
	spim_SlaveInfo const_P *pSlave;
	spim__HwInst * 			pHwInst;
	Uint32					temp;
	Uint32					temp2;

	pHwInst = (spim__HwInst *) pInst;
	pSlave = (spim_SlaveInfo const_P *) pInst->pCurrReq->pTargetInfo;

	/*
	 * Set SPI frame length. Only 8...16 bits are supported on this bus.
	 */

	deb_assert(pSlave->frameBits >= 8);
	deb_assert(pSlave->frameBits <= 16);

	S0SPCR |= SPIM__S0SPCR_BENA;

	S0SPCR |= (pSlave->frameBits & 0x0F) << SPIM__S0SPCR_BITS;

	/*
	 * Set the clock settings (polarity and phase).
	 */

	if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
		S0SPCR |= SPIM__S0SPCR_CPOL;

		if (pSlave->flags & SPIM_PHASE_RISING) {
			S0SPCR |= SPIM__S0SPCR_CPHA;
			
		} else {
			S0SPCR &= ~SPIM__S0SPCR_CPHA;
		}
		
	} else {
		S0SPCR &= ~SPIM__S0SPCR_CPOL;

		if (pSlave->flags & SPIM_PHASE_RISING) {
			S0SPCR &= ~SPIM__S0SPCR_CPHA;
			
		} else {
			S0SPCR |= SPIM__S0SPCR_CPHA;
		}
	}

	/*
	 * Select the SPI bus clock. In Master mode, this register must be an even
	 * number greater than or equal to 8. Violations of this can result in
	 * unpredictable behavior.
	 */

	deb_assert(pSlave->clkDiv >= 8);

	S0SPCCR = (pSlave->clkDiv / 2) * 2;

	/*
	 * Select slave by setting slave CS pin low.
	 */

	switch (pSlave->csPort) {
		case 0:
			FIO0MASK &= ~(1 << pSlave->csPin);
			FIO0CLR |= 1 << pSlave->csPin;
			break;

		case 1:
			FIO1MASK &= ~(1 << pSlave->csPin);
			FIO1CLR |= 1 << pSlave->csPin;
			break;

		case 2:
			FIO2MASK &= ~(1 << pSlave->csPin);
			FIO2CLR |= 1 << pSlave->csPin;
			break;

		default:
			deb_assert(FALSE);
			break;
	}

	/*
	 * Read the status register and data register to clear the SPIF flag and
	 * empty the data register.
	 */

	temp = S0SPCR;
	temp = S0SPSR;
	temp = S0SPDR;
	temp2 = S0SPINT;

	/*
	 * Copy the first byte to the data register. This will activate
	 * transmission. The interrupt will take care of the rest.
	 */

	pHwInst->rxData.pCurrData = &pInst->pCurrReq->data;
	pHwInst->txData.pCurrData = &pInst->pCurrReq->data;
	pHwInst->rxData.dataIndex = 0;
	pHwInst->txData.dataIndex = 1;

	S0SPDR = *pHwInst->txData.pCurrData->pData;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__lowUp
*
*--------------------------------------------------------------------------*//**
*
*	\brief
*
*	\param		pInst
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void spim__lowUp(
	spim_Inst *				pInst
) {	
	hw_enableIrq(SPI_IRQn);	
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	SPI_IRQHandler
*
*--------------------------------------------------------------------------*//**
*
*	\brief		SPI interrupt service routine.
*
*	\details	
*
*	\note		A pointer to this ISR should be stored in the interrupt vector.
*
*******************************************************************************/

PUBLIC osa_isrOSFn(
	NULL,
	SPI_IRQHandler
) {
	osa_isrEnter();

	spim__HwInst * 	pInst;

	pInst = spim__pInst0;

	/*
	 * Clear interrupt flag
	 */

	S0SPINT = 1;

	if (S0SPSR & SPIM__S0SPSR_SPIF) {
		Uint8 dummy;

		/*
		 * Transfer completed
		 */

		/*
		 * RX
		 */

		if (pInst->rxData.dataIndex >= pInst->rxData.pCurrData->size) {
			if (pInst->rxData.pCurrData->pNext != NULL) {
				/*
				 * Data block has been handled. Step to next data block.
				 */

				pInst->rxData.pCurrData = pInst->rxData.pCurrData->pNext;
				pInst->rxData.dataIndex = 0;
			}
		}

		if (pInst->rxData.pCurrData->flags & PROTIF_FLAG_READ) {
			pInst->rxData.pCurrData->pData[pInst->rxData.dataIndex] =
				S0SPDR;
				
		} else {
			dummy = S0SPDR;
		}
		pInst->rxData.dataIndex++;
	

		/*
		 * TX
		 */

		if (pInst->txData.dataIndex >= pInst->txData.pCurrData->size) {
			/*
			 * Data block has been handled. Step to next data block if there
			 * is one. Otherwise unset chip select and signal the task that the
			 * request has been handled.
			 */

			if (pInst->txData.pCurrData->pNext == NULL) {
				spim_SlaveInfo const_P * pSlave;

				pSlave = pInst->parent.pCurrReq->pTargetInfo;

				/*
				 * All request data has been handled.
				 * Deselect the slave by setting the CS pin high.
				 * signal the task.
				 */

				switch (pSlave->csPort) {
					case 0:
						FIO0MASK &= ~(1 << pSlave->csPin);
						FIO0SET |= 1 << pSlave->csPin;
						break;

					case 1:
						FIO1MASK &= ~(1 << pSlave->csPin);
						FIO1SET |= 1 << pSlave->csPin;
						break;

					case 2:
						FIO2MASK &= ~(1 << pSlave->csPin);
						FIO2SET |= 1 << pSlave->csPin;
						break;

					default:
						deb_assert(FALSE);
						break;
				}

				pInst->parent.flags |= SPIM__FLAG_DONE;
				osa_coTaskRunIsr(&pInst->parent.coTask);
				
			} else {
				/*
				 * Step to next data block.
				 */

				pInst->txData.pCurrData = pInst->txData.pCurrData->pNext;
				pInst->txData.dataIndex = 0;
			}
		}

		if not(pInst->parent.flags & SPIM__FLAG_DONE) {
			/*
			 * Copy the next byte to the data register if there is data left.
			 */

			if (pInst->txData.pCurrData->flags & PROTIF_FLAG_WRITE) {
				S0SPDR =
					pInst->txData.pCurrData->pData[pInst->txData.dataIndex];
					
			} else {
				S0SPDR = 0;
			}
			pInst->txData.dataIndex++;
		}
	}

	osa_isrLeave();
}
osa_endOSIsr
