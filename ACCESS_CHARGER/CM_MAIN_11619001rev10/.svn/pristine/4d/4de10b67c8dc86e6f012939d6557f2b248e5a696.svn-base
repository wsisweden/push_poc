/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	UART_CM3
*
*	\brief		LPC17xx UART0 driver interface.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 4423 $ \n
*				\$Date: 2021-01-13 17:08:06 +0200 (ke, 13 tammi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "drv1.h"
#include "hw.h"

#include "uart.h"

#include "..\local.h"
#include "UART_HW.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void * uart__init01(void const_P *,void *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Public interface for UART0 with TX on pin P0.2. This interface should be
 * given as a initialization parameter to the function block that will be using
 * the driver. The function block can then call the driver through this
 * interface.
 */

PUBLIC const_P drv1_Interface uart0_interface = {
	uart__init01,
	uart__set0,
	uart__write0
};

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__init01
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialization of UART0.
*
*	\param		pConfig Pointer to UART config structure.
*	\param		pUtil 	Pointer to protocol instance.
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE void * uart__init01(
	void const_P *			pConfig,
	void *					pUtil
) {
	LPC_SC->PCONP |= (1<<3);			/* Enable power to UART0			*/

#ifdef TARGETFAMILY_LPC178x_7x
	LPC_IOCON->P0_2 = (LPC_IOCON->P0_2 & ~0x3) | 0x1;	/* Mode U0_TXD		*/
	LPC_IOCON->P0_3 = (LPC_IOCON->P0_3 & ~0x3) | 0x1;	/* Mode U0_RXD		*/
#else
	LPC_PINCON->PINSEL0 |=
		(1U<<4)											/* TXD0 P0.2		*/
		| (1U<<6);										/* RXD0 P0.3		*/
#endif

	return(uart__init0(pConfig, pUtil));
}
