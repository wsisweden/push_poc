/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	NVMEMREG
*
*	\brief		NOV-tag table searching.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 3650 $ \n
*				\$Date: 2019-03-22 14:32:21 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"
#include "sys.h"
#include "novtag.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	novtag_findOffset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Find the element with the specified offset.
*
*	\param		pTagInfo		Pointer to offset/tag table information.
*	\param		offset			The offset value to search for.
*
*	\return		The table index where the specified offset was found. 0xFFFF is
*				returned if the offset could not be found.
*
*	\details	
*
*	\note
*
*******************************************************************************/

PUBLIC Uint16 novtag_findOffset(
	novtag_TagInfo const_P *pTagInfo,
	Uint16 					offset
) {
	Ufast16					mid;
	Ufast16					lower;
	Ufast16					upper;
	novtag_OtPair const_P * pTable;

	upper = pTagInfo->tagCount;
	pTable = pTagInfo->pTagsByOffs;

	for (
		mid = 0;
		lower = mid, lower != (mid += (upper - mid) / 2);
	) {
		if (offset < pTable[mid].offset) {
			upper = mid;
			mid = lower;
		}
	}

	if (offset == pTable[lower].offset) {
		return((Uint16) lower);
	}

	return(0xFFFFU);
}
