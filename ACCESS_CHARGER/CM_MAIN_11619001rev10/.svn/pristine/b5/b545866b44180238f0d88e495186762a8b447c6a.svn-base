#include "PhaseError.h"
#include "engines_types.h"
#include "meas.h"

#define AVERAGE_LEN 100															// Number of samples AC amplifier and dynamic hysteresis use for averages

static int32_t average(const int16_t* i, int len);								// Average values or more exact the sum without the division
static int schmitt_trigger(int32_t AC, int32_t hysteresis);						// Schmitt-trigger

int regu_DetectMissingPhase(const int16_t* i, int len)
{
	const int32_t DC = average(i, len);											// Store values internally and average or more exact the sum without the division

	{																			// Frequency counter
		int flank_cnt = 0;

		{
#define LEVEL_SHIFTS_LEN 10
			static uint32_t levelShifts[LEVEL_SHIFTS_LEN] = {[0 ... LEVEL_SHIFTS_LEN - 1] = 0}; // Store level shifts here, one per bit
			int n;

			for(n = 0; n < len; n++){
				static int levelOld = 0;
				static int pos = 0;												// Five lowest bits bits position, higher bits byte position
				const int32_t AC = AVERAGE_LEN*(i[n]) - DC;						// AC-amplifier, note multiplication on both sides to avoid division
				const int level = schmitt_trigger(AC, DC >> 3);					// Connect signal thru schmitt-trigger

				if(level != levelOld){											// Flank ?
					levelShifts[pos >> 5] |= (1 << (pos & 0b11111));			// Set position to flank occurred
				}
				else{
					levelShifts[pos >> 5] &= ~(uint32_t)(1 << (pos & 0b11111));	// Set position to not occurred
				}

				levelOld = level;

				if(pos < 32*LEVEL_SHIFTS_LEN - 1){								// Reach the end ?
					pos++;														// Increase position
				}
				else{
					pos = 0;													// Start from the beginning
				}
			}

			{																	// Count level shifts
				for(n = 0; n < LEVEL_SHIFTS_LEN; n++){							// For all stored values with level shifts
					uint32_t current = levelShifts[n];

					int k;
					for(k = 0; k < 32; k++){									// For all bits in stored value
						if((current >> k) & 1){									// Flank ?
							flank_cnt++;										// Count flank
						}
					}
				}
			}
		}

		{
			static int cnt = 0;
#define HZ_TO_PULSES(f) (int)(((2*f)*(LEVEL_SHIFTS_LEN*32/1000.0)) + 0.5)

			if(DC > (int32_t)Ampere2ADC(0.1*engineNominalCurrent()) &&			// Current sufficiently high for schmitt-trigger to work ?
			   flank_cnt > HZ_TO_PULSES(80) && flank_cnt < HZ_TO_PULSES(140)){	// Between 40 and 70 Hz input ?
				if(cnt < 90){
					cnt++;
				}
			}
			else{
				if(cnt){
					cnt--;
				}
			}

			if(cnt > 60){
// removed phase error detection because it triggered during BMU_INIT (alternative comm)
				return 0;
			}
			else{
				return 0;
			}
		}
	}
}

static int32_t average(const int16_t* i, int len){								// Average values
	static int16_t old[AVERAGE_LEN] = {[0 ... AVERAGE_LEN - 1] = 0};			// Values are stored internally here so that averaging is possible

	{																			// Store new values internally
		static int pos = 0;
		int n;

		for(n = 0; n < len; n++){												// For all new values
			old[pos] = i[n];													// Store value internally at position
			pos++;																// Increase storage position
			if(pos >= AVERAGE_LEN){												// Reached the end ?
				pos = 0;														// Start from beginning
			}
		}
	}
	{
		int32_t sum = 0;

		int n;
		for(n = 0; n < AVERAGE_LEN; n++){										// For all stored values
			sum += old[n];														// Calculate sum
		}

		return sum;
	}
}

static int schmitt_trigger(int32_t AC, int32_t hysteresis){
	static int level = 0;														// Store filtered level used to calculate frequency here, note remember value for the schmitt trigger
	if(level){																	// Level currently high ?
		if(AC < -hysteresis){													// AC value below 1/8 of the DC level ?
			level = 0;															// Set output to low
		}
	}
	else{																		// Level is currently low
		if(AC > hysteresis){													// AC value above 1/8 of the DC level ?
			level = 1;															// Set output to high
		}
	}

	return level;
}
