/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_TCP_H__
#define __LWIP_TCP_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_TCP /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/sys.h"
//#include "lwip/mem.h"		/* removed by tlarsu */
#include "ntcpip/pbuf.h"
#include "ntcpip/ip.h"
//#include "lwip/icmp.h"	/* removed by tlarsu */
#include "ntcpip/err.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ntcpip__tcpPcb;

/* Functions for interfacing with TCP: */

/* Lower layer interface to TCP: */
#define ntcpip__tcpInit() /* Compatibility define, not init needed. */
void             ntcpip__tcpTmr     (void);  /* Must be called every
                                         NTCPIP__TCP_TMR_INTERVAL
                                         ms. (Typically 250 ms). */
/* Application program's interface: */
struct ntcpip__tcpPcb * ntcpip_tcpNew     (void);
struct ntcpip__tcpPcb * ntcpip_tcpAlloc   (Uint8 prio);

void             ntcpip_tcpArg     (struct ntcpip__tcpPcb *pcb, void *arg);
void             ntcpip_tcpAccept  (struct ntcpip__tcpPcb *pcb,
                              ntcpip_Err (* accept)(void *arg, struct ntcpip__tcpPcb *newpcb,
                 ntcpip_Err err));
void             ntcpip_tcpRecv    (struct ntcpip__tcpPcb *pcb,
                              ntcpip_Err (* recv)(void *arg, struct ntcpip__tcpPcb *tpcb,
                              struct ntcpip_pbuf *p, ntcpip_Err err));
void             ntcpip_tcpSent    (struct ntcpip__tcpPcb *pcb,
                              ntcpip_Err (* sent)(void *arg, struct ntcpip__tcpPcb *tpcb,
                              Uint16 len));
void             ntcpip_tcpPoll    (struct ntcpip__tcpPcb *pcb,
                              ntcpip_Err (* poll)(void *arg, struct ntcpip__tcpPcb *tpcb),
                              Uint8 interval);
void             ntcpip_tcpErr     (struct ntcpip__tcpPcb *pcb,
                              void (* err)(void *arg, ntcpip_Err err));

#define          ntcpip_tcpMss(pcb)      ((pcb)->mss)
#define          ntcpip_tcpSndbuf(pcb)   ((pcb)->snd_buf)
#define          ntcpip_tcpNagleDisable(pcb)  ((pcb)->flags |= NTCPIP_TF_NODELAY)
#define          ntcpip_tcpNagleEnable(pcb) ((pcb)->flags &= ~NTCPIP_TF_NODELAY)
#define          ntcpip_tcpNagleDisabled(pcb) (((pcb)->flags & NTCPIP_TF_NODELAY) != 0)

#if NTCPIP__TCP_LISTEN_BACKLOG
#define          ntcpip_tcpAccepted(pcb) (((struct ntcpip__tcpPcbListen *)(pcb))->accepts_pending--)
#else  /* NTCPIP__TCP_LISTEN_BACKLOG */
#define          ntcpip_tcpAccepted(pcb)
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */

void             ntcpip_tcpRecved  (struct ntcpip__tcpPcb *pcb, Uint16 len);
ntcpip_Err       ntcpip_tcpBind(struct ntcpip__tcpPcb *pcb, const struct ntcpip_ipAddr *ipaddr,
                              Uint16 port);
ntcpip_Err       ntcpip_tcpConnect(struct ntcpip__tcpPcb *pcb, const struct ntcpip_ipAddr *ipaddr,
                              Uint16 port, ntcpip_Err (* connected)(void *arg,
                              struct ntcpip__tcpPcb *tpcb,
                              ntcpip_Err err));

struct ntcpip__tcpPcb * ntcpip_tcpListenWithBacklog(struct ntcpip__tcpPcb *pcb, Uint8 backlog);
#define          ntcpip_tcpListen(pcb) ntcpip_tcpListenWithBacklog(pcb, NTCPIP__TCP_DEFAULT_LISTEN_BACKLOG)

void             ntcpip__tcpAbandon (struct ntcpip__tcpPcb *pcb, int reset);
#define          ntcpip_tcpAbort(pcb) ntcpip__tcpAbandon((pcb), 1)
ntcpip_Err            ntcpip_tcpClose   (struct ntcpip__tcpPcb *pcb);

/* Flags for "apiflags" parameter in ntcpip_tcpWrite and ntcpip__tcpEnqueue */
#define NTCPIP_TCP_W_FLG_COPY 0x01
#define NTCPIP_TCP_W_FLAG_MORE 0x02

ntcpip_Err            ntcpip_tcpWrite   (struct ntcpip__tcpPcb *pcb, const void *dataptr, Uint16 len,
                              Uint8 apiflags);

void             ntcpip__tcpSetPrio (struct ntcpip__tcpPcb *pcb, Uint8 prio);

#define NTCPIP__TCP_PRIO_MIN    1
#define NTCPIP__TCP_PRIO_NORMAL 64
#define NTCPIP__TCP_PRIO_MAX    127

/* It is also possible to call these two functions at the right
   intervals (instead of calling ntcpip__tcpTmr()). */
void             ntcpip__tcpSlowTmr (void);
void             ntcpip__tcpFastTmr (void);


/* Only used by IP to pass a TCP segment to TCP: */
void             ntcpip__tcpInput   (struct ntcpip_pbuf *p, struct ntcpip__netif *inp);
/* Used within the TCP code only: */
ntcpip_Err            ntcpip__tcpSendEmptyAck(struct ntcpip__tcpPcb *pcb);
ntcpip_Err            ntcpip__tcpOutput  (struct ntcpip__tcpPcb *pcb);
void             ntcpip__tcpRexmit  (struct ntcpip__tcpPcb *pcb);
void             ntcpip__tcpRexmitRto  (struct ntcpip__tcpPcb *pcb);
void             ntcpip__tcpRexmitFast (struct ntcpip__tcpPcb *pcb);
Uint32            ntcpip__tcpUpdateRcvAnnWnd(struct ntcpip__tcpPcb *pcb);

/**
 * This is the Nagle algorithm: try to combine user data to send as few TCP
 * segments as possible. Only send if
 * - no previously transmitted data on the connection remains unacknowledged or
 * - the NTCPIP_TF_NODELAY flag is set (nagle algorithm turned off for this pcb) or
 * - the only unsent segment is at least pcb->mss bytes long (or there is more
 *   than one unsent segment - with lwIP, this can happen although unsent->len < mss)
 * - or if we are in fast-retransmit (NTCPIP_TF_INFR)
 */
#define ntcpip__tcpDoOutputNagle(tpcb) ((((tpcb)->unacked == NULL) || \
                            ((tpcb)->flags & (NTCPIP_TF_NODELAY | NTCPIP_TF_INFR)) || \
                            (((tpcb)->unsent != NULL) && (((tpcb)->unsent->next != NULL) || \
                              ((tpcb)->unsent->len >= (tpcb)->mss))) \
                            ) ? 1 : 0)
#define ntcpip__tcpOutputNagle(tpcb) (ntcpip__tcpDoOutputNagle(tpcb) ? ntcpip__tcpOutput(tpcb) : NTCPIP_ERR_OK)


#define NTCPIP__TCP_SEQ_LT(a,b)     ((Int32)((a)-(b)) < 0)
#define NTCPIP__TCP_SEQ_LEQ(a,b)    ((Int32)((a)-(b)) <= 0)
#define NTCPIP__TCP_SEQ_GT(a,b)     ((Int32)((a)-(b)) > 0)
#define NTCPIP__TCP_SEQ_GEQ(a,b)    ((Int32)((a)-(b)) >= 0)
/* is b<=a<=c? */
#if 0 /* see bug #10548 */
#define NTCPIP__TCP_SEQ_BETWEEN(a,b,c) ((c)-(b) >= (a)-(b))
#endif
#define NTCPIP__TCP_SEQ_BETWEEN(a,b,c) (NTCPIP__TCP_SEQ_GEQ(a,b) && NTCPIP__TCP_SEQ_LEQ(a,c))
#define NTCPIP__TCP_FIN 0x01U
#define NTCPIP__TCP_SYN 0x02U
#define NTCPIP__TCP_RST 0x04U
#define NTCPIP__TCP_PSH 0x08U
#define NTCPIP__TCP_ACK 0x10U
#define NTCPIP__TCP_URG 0x20U
#define NTCPIP__TCP_ECE 0x40U
#define NTCPIP__TCP_CWR 0x80U

#define NTCPIP__TCP_FLAGS 0x3fU

/* Length of the TCP header, excluding options. */
#define NTCPIP__TCP_HLEN 20

#ifndef NTCPIP__TCP_TMR_INTERVAL
#define NTCPIP__TCP_TMR_INTERVAL       250  /* The TCP timer interval in milliseconds. */
#endif /* NTCPIP__TCP_TMR_INTERVAL */

#ifndef NTCPIP__TCP_FAST_INTERVAL
#define NTCPIP__TCP_FAST_INTERVAL      NTCPIP__TCP_TMR_INTERVAL /* the fine grained timeout in milliseconds */
#endif /* NTCPIP__TCP_FAST_INTERVAL */

#ifndef NTCPIP__TCP_SLOW_INTERVAL
#define NTCPIP__TCP_SLOW_INTERVAL      (2*NTCPIP__TCP_TMR_INTERVAL)  /* the coarse grained timeout in milliseconds */
#endif /* NTCPIP__TCP_SLOW_INTERVAL */

#define NTCPIP__TCP_FIN_WAIT_TIMEOUT 20000 /* milliseconds */
#define NTCPIP__TCP_SYN_RCVD_TIMEOUT 20000 /* milliseconds */

#define NTCPIP__TCP_OOSEQ_TIMEOUT        6U /* x RTO */

#ifndef NTCPIP__TCP_MSL
#define NTCPIP__TCP_MSL 60000UL /* The maximum segment lifetime in milliseconds */
#endif

/* Keepalive values, compliant with RFC 1122. Don't change this unless you know what you're doing */
#ifndef  NTCPIP_TCP_KEEPIDLE_DEFAULT
#define  NTCPIP_TCP_KEEPIDLE_DEFAULT     7200000UL /* Default KEEPALIVE timer in milliseconds */
#endif

#ifndef  NTCPIP_TCP_KEEPINTVL_DEFAULT
#define  NTCPIP_TCP_KEEPINTVL_DEFAULT    75000UL   /* Default Time between KEEPALIVE probes in milliseconds */
#endif

#ifndef  NTCPIP_TCP_KEEPCNT_DEFAULT
#define  NTCPIP_TCP_KEEPCNT_DEFAULT      9U        /* Default Counter for KEEPALIVE probes */
#endif

#define  NTCPIP__TCP_MAXIDLE              NTCPIP_TCP_KEEPCNT_DEFAULT * NTCPIP_TCP_KEEPINTVL_DEFAULT  /* Maximum KEEPALIVE probe time */

/* Fields are (of course) in network byte order.
 * Some fields are converted to host byte order in ntcpip__tcpInput().
 */
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/bpstruct.h"
#endif
NTCPIP__PACK_STRUCT_BEGIN
struct ntcpip__tcpHdr {
  NTCPIP__PACK_STRUCT_FIELD(Uint16 src);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 dest);
  NTCPIP__PACK_STRUCT_FIELD(Uint32 seqno);
  NTCPIP__PACK_STRUCT_FIELD(Uint32 ackno);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 _hdrlen_rsvd_flags);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 wnd);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 chksum);
  NTCPIP__PACK_STRUCT_FIELD(Uint16 urgp);
} NTCPIP__PACK_STRUCT_STRUCT;
NTCPIP__PACK_STRUCT_END
#ifdef NTCPIP__PACK_STRUCT_USE_INCLUDES
#  include "arch/epstruct.h"
#endif

#define NTCPIP__TCPH_OFFSET(phdr) (ntcpip_ntohs((phdr)->_hdrlen_rsvd_flags) >> 8)
#define NTCPIP__TCPH_HDRLEN(phdr) (ntcpip_ntohs((phdr)->_hdrlen_rsvd_flags) >> 12)
#define NTCPIP__TCPH_FLAGS(phdr)  (ntcpip_ntohs((phdr)->_hdrlen_rsvd_flags) & NTCPIP__TCP_FLAGS)

#define NTCPIP__TCPH_OFFSET_SET(phdr, offset) (phdr)->_hdrlen_rsvd_flags = ntcpip_htons(((offset) << 8) | NTCPIP__TCPH_FLAGS(phdr))
#define NTCPIP__TCPH_HDRLEN_SET(phdr, len) (phdr)->_hdrlen_rsvd_flags = ntcpip_htons(((len) << 12) | NTCPIP__TCPH_FLAGS(phdr))
#define NTCPIP__TCPH_FLAGS_SET(phdr, flags) (phdr)->_hdrlen_rsvd_flags = (((phdr)->_hdrlen_rsvd_flags & ntcpip_htons((Uint16)(~(Uint16)(NTCPIP__TCP_FLAGS)))) | ntcpip_htons(flags))
#define NTCPIP__TCPH_SET_FLAG(phdr, flags ) (phdr)->_hdrlen_rsvd_flags = ((phdr)->_hdrlen_rsvd_flags | ntcpip_htons(flags))
#define NTCPIP__TCPH_UNSET_FLAG(phdr, flags) (phdr)->_hdrlen_rsvd_flags = ntcpip_htons(ntcpip_ntohs((phdr)->_hdrlen_rsvd_flags) | (NTCPIP__TCPH_FLAGS(phdr) & ~(flags)) )

#define NTCPIP__TCP_TCPLEN(seg) ((seg)->len + ((NTCPIP__TCPH_FLAGS((seg)->tcphdr) & (NTCPIP__TCP_FIN | NTCPIP__TCP_SYN)) != 0))

enum ntcpip__tcpState {
  NTCPIP_CLOSED      = 0,
  NTCPIP_LISTEN      = 1,
  NTCPIP_SYN_SENT    = 2,
  NTCPIP_SYN_RCVD    = 3,
  NTCPIP_ESTABLISHED = 4,
  NTCPIP_FIN_WAIT_1  = 5,
  NTCPIP_FIN_WAIT_2  = 6,
  NTCPIP_CLOSE_WAIT  = 7,
  NTCPIP_CLOSING     = 8,
  NTCPIP_LAST_ACK    = 9,
  NTCPIP_TIME_WAIT   = 10
};

/** Flags used on input processing, not on pcb->flags
*/
#define NTCPIP_TF_RESET     (Uint8)0x08U   /* Connection was reset. */
#define NTCPIP_TF_CLOSED    (Uint8)0x10U   /* Connection was sucessfully closed. */
#define NTCPIP_TF_GOT_FIN   (Uint8)0x20U   /* Connection was closed by the remote end. */


#if NTCPIP__LWIP_CALLBACK_API
  /* Function to call when a listener has been connected.
   * @param arg user-supplied argument (tcp_pcb.callback_arg)
   * @param pcb a new tcp_pcb that now is connected
   * @param err an error argument (TODO: that is current always NTCPIP_ERR_OK?)
   * @return NTCPIP_ERR_OK: accept the new connection,
   *                 any other ntcpip_Err abortsthe new connection
   */
#define NTCPIP__DEF_ACCEPT_CB  ntcpip_Err (* accept)(void *arg, struct ntcpip__tcpPcb *newpcb, ntcpip_Err err)
#else /* NTCPIP__LWIP_CALLBACK_API */
#define NTCPIP__DEF_ACCEPT_CB
#endif /* NTCPIP__LWIP_CALLBACK_API */

/**
 * members common to struct ntcpip__tcpPcb and struct tcp_listen_pcb
 */
#define NTCPIP__TCP_PCB_COMMON(type) \
  type *next; /* for the linked list */ \
  enum ntcpip__tcpState state; /* TCP state */ \
  Uint8 prio; \
  void *callback_arg; \
  /* ports are in host byte order */ \
  Uint16 local_port; \
  /* the accept callback for listen- and normal pcbs, if NTCPIP__LWIP_CALLBACK_API */ \
  NTCPIP__DEF_ACCEPT_CB


/* the TCP protocol control block */
struct ntcpip__tcpPcb {
/** common PCB members */
  NTCPIP__IP_PCB;
/** protocol specific PCB members */
  NTCPIP__TCP_PCB_COMMON(struct ntcpip__tcpPcb);

  /* ports are in host byte order */
  Uint16 remote_port;
  
  Uint8 flags;
#define NTCPIP_TF_ACK_DELAY   ((Uint8)0x01U)   /* Delayed ACK. */
#define NTCPIP_TF_ACK_NOW     ((Uint8)0x02U)   /* Immediate ACK. */
#define NTCPIP_TF_INFR        ((Uint8)0x04U)   /* In fast recovery. */
#define NTCPIP_TF_TIMESTAMP   ((Uint8)0x08U)   /* Timestamp option enabled */
#define NTCPIP_TF_FIN         ((Uint8)0x20U)   /* Connection was closed locally (FIN segment enqueued). */
#define NTCPIP_TF_NODELAY     ((Uint8)0x40U)   /* Disable Nagle algorithm */
#define NTCPIP_TF_NAGLEMEMERR ((Uint8)0x80U)   /* nagle enabled, memerr, try to output to prevent delayed ACK to happen */

  /* the rest of the fields are in host byte order
     as we have to do some math with them */
  /* receiver variables */
  Uint32 rcv_nxt;   /* next seqno expected */
  Uint16 rcv_wnd;   /* receiver window available */
  Uint16 rcv_ann_wnd; /* receiver window to announce */
  Uint32 rcv_ann_right_edge; /* announced right edge of window */

  /* Timers */
  Uint32 tmr;
  Uint8 polltmr, pollinterval;
  
  /* Retransmission timer. */
  Int16 rtime;
  
  Uint16 mss;   /* maximum segment size */
  
  /* RTT (round trip time) estimation variables */
  Uint32 rttest; /* RTT estimate in 500ms ticks */
  Uint32 rtseq;  /* sequence number being timed */
  Int16 sa, sv; /* @todo document this */

  Int16 rto;    /* retransmission time-out */
  Uint8 nrtx;    /* number of retransmissions */

  /* fast retransmit/recovery */
  Uint32 lastack; /* Highest acknowledged seqno. */
  Uint8 dupacks;
  
  /* congestion avoidance/control variables */
  Uint16 cwnd;  
  Uint16 ssthresh;

  /* sender variables */
  Uint32 snd_nxt;   /* next new seqno to be sent */
  Uint16 snd_wnd;   /* sender window */
  Uint32 snd_wl1, snd_wl2; /* Sequence and acknowledgement numbers of last
                             window update. */
  Uint32 snd_lbb;       /* Sequence number of next byte to be buffered. */

  Uint16 acked;
  
  Uint16 snd_buf;   /* Available buffer space for sending (in bytes). */
#define NTCPIP_TCP_SNDQUEUELEN_OF (0xffff-3)
  Uint16 snd_queuelen; /* Available buffer space for sending (in tcp_segs). */
  
  
  /* These are ordered by sequence number: */
  struct ntcpip__tcpSeg *unsent;   /* Unsent (queued) segments. */
  struct ntcpip__tcpSeg *unacked;  /* Sent but unacknowledged segments. */
#if NTCPIP__TCP_QUEUE_OOSEQ  
  struct ntcpip__tcpSeg *ooseq;    /* Received out of sequence segments. */
#endif /* NTCPIP__TCP_QUEUE_OOSEQ */

  struct ntcpip_pbuf *refused_data; /* Data previously received but not yet taken by upper layer */

#if NTCPIP__LWIP_CALLBACK_API
  /* Function to be called when more send buffer space is available.
   * @param arg user-supplied argument (tcp_pcb.callback_arg)
   * @param pcb the tcp_pcb which has send buffer space available
   * @param space the amount of bytes available
   * @return NTCPIP_ERR_OK: try to send some data by calling ntcpip__tcpOutput
   */
  ntcpip_Err (* sent)(void *arg, struct ntcpip__tcpPcb *pcb, Uint16 space);
  
  /* Function to be called when (in-sequence) data has arrived.
   * @param arg user-supplied argument (tcp_pcb.callback_arg)
   * @param pcb the tcp_pcb for which data has arrived
   * @param p the packet buffer which arrived
   * @param err an error argument (TODO: that is current always NTCPIP_ERR_OK?)
   * @return NTCPIP_ERR_OK: try to send some data by calling ntcpip__tcpOutput
   */
  ntcpip_Err (* recv)(void *arg, struct ntcpip__tcpPcb *pcb, struct ntcpip_pbuf *p, ntcpip_Err err);

  /* Function to be called when a connection has been set up.
   * @param arg user-supplied argument (tcp_pcb.callback_arg)
   * @param pcb the tcp_pcb that now is connected
   * @param err an error argument (TODO: that is current always NTCPIP_ERR_OK?)
   * @return value is currently ignored
   */
  ntcpip_Err (* connected)(void *arg, struct ntcpip__tcpPcb *pcb, ntcpip_Err err);

  /* Function which is called periodically.
   * The period can be adjusted in multiples of the TCP slow timer interval
   * by changing tcp_pcb.polltmr.
   * @param arg user-supplied argument (tcp_pcb.callback_arg)
   * @param pcb the tcp_pcb to poll for
   * @return NTCPIP_ERR_OK: try to send some data by calling ntcpip__tcpOutput
   */
  ntcpip_Err (* poll)(void *arg, struct ntcpip__tcpPcb *pcb);

  /* Function to be called whenever a fatal error occurs.
   * There is no pcb parameter since most of the times, the pcb is
   * already deallocated (or there is no pcb) when this function is called.
   * @param arg user-supplied argument (tcp_pcb.callback_arg)
   * @param err an indication why the error callback is called:
   *            NTCPIP_ERR_ABRT: aborted through ntcpip_tcpAbort or by a TCP timer
   *            NTCPIP_ERR_RST: the connection was reset by the remote host
   */
  void (* errf)(void *arg, ntcpip_Err err);
#endif /* NTCPIP__LWIP_CALLBACK_API */

#if LWIP_TCP_TIMESTAMPS
  Uint32 ts_lastacksent;
  Uint32 ts_recent;
#endif /* LWIP_TCP_TIMESTAMPS */

  /* idle time before KEEPALIVE is sent */
  Uint32 keep_idle;
#if NTCPIP__LWIP_TCP_KEEPALIVE
  Uint32 keep_intvl;
  Uint32 keep_cnt;
#endif /* NTCPIP__LWIP_TCP_KEEPALIVE */
  
  /* Persist timer counter */
  Uint32 persist_cnt;
  /* Persist timer back-off */
  Uint8 persist_backoff;

  /* KEEPALIVE counter */
  Uint8 keep_cnt_sent;
};

struct ntcpip__tcpPcbListen {  
/* Common members of all PCB types */
  NTCPIP__IP_PCB;
/* Protocol specific PCB members */
  NTCPIP__TCP_PCB_COMMON(struct ntcpip__tcpPcbListen);

#if NTCPIP__TCP_LISTEN_BACKLOG
  Uint8 backlog;
  Uint8 accepts_pending;
#endif /* NTCPIP__TCP_LISTEN_BACKLOG */
};

#if NTCPIP__LWIP_EVENT_API

enum ntcpip__event {
  LWIP_EVENT_ACCEPT,
  LWIP_EVENT_SENT,
  LWIP_EVENT_RECV,
  LWIP_EVENT_CONNECTED,
  LWIP_EVENT_POLL,
  LWIP_EVENT_ERR
};

ntcpip_Err lwip_tcp_event(void *arg, struct ntcpip__tcpPcb *pcb,
         enum lwip_event,
         struct ntcpip_pbuf *p,
         Uint16 size,
         ntcpip_Err err);

#define NTCPIP__TCP_EVENT_ACCEPT(pcb,err,ret)    ret = lwip_tcp_event((pcb)->callback_arg, (pcb),\
                LWIP_EVENT_ACCEPT, NULL, 0, err)
#define NTCPIP__TCP_EVENT_SENT(pcb,space,ret) ret = lwip_tcp_event((pcb)->callback_arg, (pcb),\
                   LWIP_EVENT_SENT, NULL, space, NTCPIP_ERR_OK)
#define NTCPIP__TCP_EVENT_RECV(pcb,p,err,ret) ret = lwip_tcp_event((pcb)->callback_arg, (pcb),\
                LWIP_EVENT_RECV, (p), 0, (err))
#define NTCPIP__TCP_EVENT_CONNECTED(pcb,err,ret) ret = lwip_tcp_event((pcb)->callback_arg, (pcb),\
                LWIP_EVENT_CONNECTED, NULL, 0, (err))
#define NTCPIP__TCP_EVENT_POLL(pcb,ret)       ret = lwip_tcp_event((pcb)->callback_arg, (pcb),\
                LWIP_EVENT_POLL, NULL, 0, NTCPIP_ERR_OK)
#define NTCPIP__TCP_EVENT_ERR(errf,arg,err)  lwip_tcp_event((arg), NULL, \
                LWIP_EVENT_ERR, NULL, 0, (err))
#else /* NTCPIP__LWIP_EVENT_API */

#define NTCPIP__TCP_EVENT_ACCEPT(pcb,err,ret)                          \
  do {                                                         \
    if((pcb)->accept != NULL)                                  \
      (ret) = (pcb)->accept((pcb)->callback_arg,(pcb),(err));  \
    else (ret) = NTCPIP_ERR_OK;                                       \
  } while (0)

#define NTCPIP__TCP_EVENT_SENT(pcb,space,ret)                          \
  do {                                                         \
    if((pcb)->sent != NULL)                                    \
      (ret) = (pcb)->sent((pcb)->callback_arg,(pcb),(space));  \
    else (ret) = NTCPIP_ERR_OK;                                       \
  } while (0)

#define NTCPIP__TCP_EVENT_RECV(pcb,p,err,ret)                           \
  do {                                                          \
    if((pcb)->recv != NULL) {                                   \
      (ret) = (pcb)->recv((pcb)->callback_arg,(pcb),(p),(err)); \
    } else {                                                    \
      (ret) = ntcpip__tcpRecvNull(NULL, (pcb), (p), (err));           \
    }                                                           \
  } while (0)

#define NTCPIP__TCP_EVENT_CONNECTED(pcb,err,ret)                         \
  do {                                                           \
    if((pcb)->connected != NULL)                                 \
      (ret) = (pcb)->connected((pcb)->callback_arg,(pcb),(err)); \
    else (ret) = NTCPIP_ERR_OK;                                         \
  } while (0)

#define NTCPIP__TCP_EVENT_POLL(pcb,ret)                                \
  do {                                                         \
    if((pcb)->poll != NULL)                                    \
      (ret) = (pcb)->poll((pcb)->callback_arg,(pcb));          \
    else (ret) = NTCPIP_ERR_OK;                                       \
  } while (0)

#define NTCPIP__TCP_EVENT_ERR(errf,arg,err)                            \
  do {                                                         \
    if((errf) != NULL)                                         \
      (errf)((arg),(err));                                     \
  } while (0)

#endif /* NTCPIP__LWIP_EVENT_API */

/* This structure represents a TCP segment on the unsent and unacked queues */
struct ntcpip__tcpSeg {
  struct ntcpip__tcpSeg *next;    /* used when putting segements on a queue */
  struct ntcpip_pbuf *p;          /* buffer containing data + TCP header */
  void *dataptr;           /* pointer to the TCP data in the pbuf */
  Uint16 len;               /* the TCP length of this segment */
  Uint8  flags;
#define TF_SEG_OPTS_MSS   (Uint8)0x01U   /* Include MSS option. */
#define TF_SEG_OPTS_TS    (Uint8)0x02U   /* Include timestamp option. */
  struct ntcpip__tcpHdr *tcphdr;  /* the TCP header */
};

#define NTCPIP__TCP_OPT_LENGTH(flags)              \
  (flags & TF_SEG_OPTS_MSS ? 4  : 0) +          \
  (flags & TF_SEG_OPTS_TS  ? 12 : 0)

/** This returns a TCP header option for MSS in an Uint32 */
#define NTCPIP__TCP_BLD_MSS_OPT(x) (x) = ntcpip_htonl(((Uint32)2 << 24) |          \
                                            ((Uint32)4 << 16) |          \
                                            (((Uint32)NTCPIP__TCP_MSS / 256) << 8) | \
                                            (NTCPIP__TCP_MSS & 255))

/* Internal functions and global variables: */
struct ntcpip__tcpPcb *tcp_pcb_copy(struct ntcpip__tcpPcb *pcb);
void ntcpip__tcpPcbPurge(struct ntcpip__tcpPcb *pcb);
void ntcpip__tcpPcbRemove(struct ntcpip__tcpPcb **pcblist, struct ntcpip__tcpPcb *pcb);

Uint8 ntcpip__tcpSegsFree(struct ntcpip__tcpSeg *seg);
Uint8 ntcpip__tcpSegFree(struct ntcpip__tcpSeg *seg);
struct ntcpip__tcpSeg *ntcpip__tcpSegCopy(struct ntcpip__tcpSeg *seg);

#define ntcpip__tcpAck(pcb)                               \
  do {                                             \
    if((pcb)->flags & NTCPIP_TF_ACK_DELAY) {              \
      (pcb)->flags &= ~NTCPIP_TF_ACK_DELAY;               \
      (pcb)->flags |= NTCPIP_TF_ACK_NOW;                  \
      ntcpip__tcpOutput(pcb);                             \
    }                                              \
    else {                                         \
      (pcb)->flags |= NTCPIP_TF_ACK_DELAY;                \
    }                                              \
  } while (0)

#define ntcpip__tcpAckNow(pcb)                           \
  do {                                             \
    (pcb)->flags |= NTCPIP_TF_ACK_NOW;                    \
    ntcpip__tcpOutput(pcb);                               \
  } while (0)

ntcpip_Err ntcpip__tcpSendCtrl(struct ntcpip__tcpPcb *pcb, Uint8 flags);
ntcpip_Err ntcpip__tcpEnqueue(struct ntcpip__tcpPcb *pcb, void *dataptr, Uint16 len,
                  Uint8 flags, Uint8 apiflags, Uint8 optflags);

void ntcpip__tcpRexmitSeg(struct ntcpip__tcpPcb *pcb, struct ntcpip__tcpSeg *seg);

void ntcpip__tcpRst(Uint32 seqno, Uint32 ackno,
       struct ntcpip_ipAddr *local_ip, struct ntcpip_ipAddr *remote_ip,
       Uint16 local_port, Uint16 remote_port);

Uint32 ntcpip__tcpNextIss(void);

void ntcpip__tcpKeepAlive(struct ntcpip__tcpPcb *pcb);
void ntcpip__tcpZeroWindowProbe(struct ntcpip__tcpPcb *pcb);

#if NTCPIP__TCP_CALCULATE_EFF_SEND_MSS
Uint16 ntcpip__tcpEffSendMss(Uint16 sendmss, const struct ntcpip_ipAddr *addr);
#endif /* NTCPIP__TCP_CALCULATE_EFF_SEND_MSS */

#if NTCPIP__LWIP_CALLBACK_API
ntcpip_Err ntcpip__tcpRecvNull(void *arg, struct ntcpip__tcpPcb *pcb, struct ntcpip_pbuf *p, ntcpip_Err err);
#endif /* NTCPIP__LWIP_CALLBACK_API */

extern struct ntcpip__tcpPcb *ntcpip__tcpInputPcb;
extern Uint32 ntcpip__tcpTicks;

const char* ntcpip__tcpDebugStateStr(enum ntcpip__tcpState s);
#if NTCPIP__TCP_DEBUG || NTCPIP__TCP_INPUT_DEBUG || NTCPIP__TCP_OUTPUT_DEBUG
void tcp_debug_print(struct tcp_hdr *tcphdr);
void tcp_debug_print_flags(Uint8 flags);
void tcp_debug_print_state(enum ntcpip__tcpState s);
void tcp_debug_print_pcbs(void);
Int16 tcp_pcbs_sane(void);
#else
#  define tcp_debug_print(tcphdr)
#  define tcp_debug_print_flags(flags)
#  define tcp_debug_print_state(s)
#  define tcp_debug_print_pcbs()
#  define tcp_pcbs_sane() 1
#endif /* NTCPIP__TCP_DEBUG */

#if NTCPIP__NO_SYS
#define ntcpip__tcpipTimerNeeded()
#else
void ntcpip__tcpipTimerNeeded(void);
#endif

/* The TCP PCB lists. */
union ntcpip__tcpListenPcbst { /* List of all TCP PCBs in NTCPIP_LISTEN state. */
  struct ntcpip__tcpPcbListen *listen_pcbs; 
  struct ntcpip__tcpPcb *pcbs;
};
extern union ntcpip__tcpListenPcbst ntcpip__tcpListenPcbs;
extern struct ntcpip__tcpPcb *ntcpip__tcpActivePcbs;  /* List of all TCP PCBs that are in a
              state in which they accept or send
              data. */
extern struct ntcpip__tcpPcb *ntcpip__tcpTwPcb;      /* List of all TCP PCBs in TIME-WAIT. */

extern struct ntcpip__tcpPcb *ntcpip__tcpTmpPcb;      /* Only used for temporary storage. */

/* Axioms about the above lists:   
   1) Every TCP PCB that is not NTCPIP_CLOSED is in one of the lists.
   2) A PCB is only in one of the lists.
   3) All PCBs in the ntcpip__tcpListenPcbs list is in NTCPIP_LISTEN state.
   4) All PCBs in the ntcpip__tcpTwPcb list is in TIME-WAIT state.
*/

/* Define two macros, NTCPIP__TCP_REG and NTCPIP__TCP_RMV that registers a TCP PCB
   with a PCB list or removes a PCB from a list, respectively. */
#if 0
#define NTCPIP__TCP_REG(pcbs, npcb) do {\
                            NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("NTCPIP__TCP_REG %p local port %d\n", npcb, npcb->local_port)); \
                            for(ntcpip__tcpTmpPcb = *pcbs; \
          ntcpip__tcpTmpPcb != NULL; \
        ntcpip__tcpTmpPcb = ntcpip__tcpTmpPcb->next) { \
                                NTCPIP__LWIP_ASSERT("NTCPIP__TCP_REG: already registered\n", ntcpip__tcpTmpPcb != npcb); \
                            } \
                            NTCPIP__LWIP_ASSERT("NTCPIP__TCP_REG: pcb->state != NTCPIP_CLOSED", npcb->state != NTCPIP_CLOSED); \
                            npcb->next = *pcbs; \
                            NTCPIP__LWIP_ASSERT("NTCPIP__TCP_REG: npcb->next != npcb", npcb->next != npcb); \
                            *(pcbs) = npcb; \
                            NTCPIP__LWIP_ASSERT("NTCPIP__TCP_RMV: tcp_pcbs sane", tcp_pcbs_sane()); \
              ntcpip__tcpipTimerNeeded(); \
                            } while(0)
#define NTCPIP__TCP_RMV(pcbs, npcb) do { \
                            NTCPIP__LWIP_ASSERT("NTCPIP__TCP_RMV: pcbs != NULL", *pcbs != NULL); \
                            NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("NTCPIP__TCP_RMV: removing %p from %p\n", npcb, *pcbs)); \
                            if(*pcbs == npcb) { \
                               *pcbs = (*pcbs)->next; \
                            } else for(ntcpip__tcpTmpPcb = *pcbs; ntcpip__tcpTmpPcb != NULL; ntcpip__tcpTmpPcb = ntcpip__tcpTmpPcb->next) { \
                               if(ntcpip__tcpTmpPcb->next == npcb) { \
                                  ntcpip__tcpTmpPcb->next = npcb->next; \
                                  break; \
                               } \
                            } \
                            npcb->next = NULL; \
                            NTCPIP__LWIP_ASSERT("NTCPIP__TCP_RMV: tcp_pcbs sane", tcp_pcbs_sane()); \
                            NTCPIP__LWIP_DEBUGF(NTCPIP__TCP_DEBUG, ("NTCPIP__TCP_RMV: removed %p from %p\n", npcb, *pcbs)); \
                            } while(0)

#else /* LWIP_DEBUG */

#define NTCPIP__TCP_REG(pcbs, npcb)                        \
  do {                                             \
    npcb->next = *pcbs;                            \
    *(pcbs) = npcb;                                \
    ntcpip__tcpipTimerNeeded();                            \
  } while (0)

#define NTCPIP__TCP_RMV(pcbs, npcb)                        \
  do {                                             \
    if(*(pcbs) == npcb) {                          \
      (*(pcbs)) = (*pcbs)->next;                   \
    }                                              \
    else {                                         \
      for(ntcpip__tcpTmpPcb = *pcbs;                                         \
          ntcpip__tcpTmpPcb != NULL;                                         \
          ntcpip__tcpTmpPcb = ntcpip__tcpTmpPcb->next) {                           \
        if(ntcpip__tcpTmpPcb->next == npcb) {   \
          ntcpip__tcpTmpPcb->next = npcb->next;          \
          break;                                   \
        }                                          \
      }                                            \
    }                                              \
    npcb->next = NULL;                             \
  } while(0)

#endif /* LWIP_DEBUG */

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_TCP */

#endif /* __LWIP_TCP_H__ */

