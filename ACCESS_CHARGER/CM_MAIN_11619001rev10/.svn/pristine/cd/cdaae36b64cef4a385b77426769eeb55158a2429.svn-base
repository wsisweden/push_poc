/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		inc_arm7/uart_0.C
*
*	\ingroup	UART_ARM7
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	\$Rev: 385 $ \n
*				\$Date: 2010-06-23 13:50:28 +0300 (ke, 23 kesä 2010) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	UART_ARM7	ARM7
*
*	\ingroup	UART
*
*	\brief		UART implementation fro NXP LPC23xx
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"
#include "deb.h"
#include "_hw.h"

#include "uart.h"
#include "drv1.h"
#include "..\local.h"

#include "UART_COM.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE osa_isrOSDecl(uart__isr);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

extern uart_Callback const_P project_uart0Callback;

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

PRIVATE uart_Inst			uart0;

/**
 * Public interface that should be used instead of calling the functions
 * directly
 */

PUBLIC const_P drv1_Interface	uart0_interface = {
	uart_init0,
	uart_set0,
	uart_write0
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/** \cond priv_decl */
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart_init0
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function for initialization of uart0
*
*	\param		pConfig Pointer to uart config structure
*	\param		pUtil 	Pointer to protocol instance
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void * uart_init0(
	void const_P *			pConfig,
	void *					pUtil
) {
	uart0.pConfig = (uart_Config const_P *) pConfig;
	uart0.pUtil = pUtil;

	PINSEL0 |= 0x00000050;			/* RxD0 and TxD0					*/
	PCONP |= (1<<3);				/* Enable power to UART0			*/

	uart_set0();

	return &uart0;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart_set0
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function for initialization of uart0
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void uart_set0(
	void
) {
	uart__BaudRate			baudRate;
	Uint16					parity;
	Uint16					dataBits;
	Uint16					stopBits;

	/*
	 *	Get baud rate from register or through config ptr
	 */

	if(uart0.pConfig->baudRateHandle != SYS_BAD_REGISTER) {
		reg_StatType		regStat;

		reg_stat(uart0.pConfig->baudRateHandle, &regStat);

		if((regStat.status & REG_TYPE_MASK) == SYS_REG_T_Uint8) {
			reg_get(&baudRate.u8, uart0.pConfig->baudRateHandle);

			switch (baudRate.u8)
			{
				case UART_BAUDRATE_1200:
					baudRate.u32 = 1200;
					break;

				case UART_BAUDRATE_2400:
					baudRate.u32 = 2400;
					break;

				case UART_BAUDRATE_4800:
					baudRate.u32 = 4800;
					break;

				case UART_BAUDRATE_9600:
					baudRate.u32 = 9600;
					break;

				case UART_BAUDRATE_19200:
					baudRate.u32 = 19200;
					break;

				case UART_BAUDRATE_38400:
					baudRate.u32 = 38400;
					break;

				case UART_BAUDRATE_57600:
					baudRate.u32 = 57600;
					break;

				case UART_BAUDRATE_115200:
					baudRate.u32 = 115200;
					break;

				default:
					deb_assert(FALSE);
					baudRate.u32 = 9600;
					break;
			}

		} else if((regStat.status & REG_TYPE_MASK) == SYS_REG_T_Uint16) {
			reg_get(&baudRate.u16, uart0.pConfig->baudRateHandle);
			baudRate.u32 = baudRate.u16;

		} else if((regStat.status & REG_TYPE_MASK) == SYS_REG_T_Uint32) {
			reg_get(&baudRate.u32, uart0.pConfig->baudRateHandle);

		} else {
			deb_assert(FALSE);
			baudRate.u32 = uart0.pConfig->baudRate;
		}

	} else {
		baudRate.u32 = uart0.pConfig->baudRate;
	}

	/*
	 *	Parity
	 */

	if(uart0.pConfig->parityHandle != SYS_BAD_REGISTER) {
		reg_get(&parity, uart0.pConfig->parityHandle);
	} else {
		parity = uart0.pConfig->parity;
	}

	/*
	 *	Databits
	 */

	if(uart0.pConfig->dataBitsHandle != SYS_BAD_REGISTER) {
		reg_get(&dataBits, uart0.pConfig->dataBitsHandle);
	} else {
		dataBits = uart0.pConfig->dataBits;
	}

	/*
	 *	Stopbits
	 */

	if(uart0.pConfig->stopBitsHandle != SYS_BAD_REGISTER) {
		reg_get(&stopBits, uart0.pConfig->stopBitsHandle);
	} else {
		stopBits = uart0.pConfig->stopBits;
	}

	U0IER = 0x00;						/* Disable all UART interrupts */
	U0LCR = (1<<7);						/* Enable latch	*/

	switch (baudRate.u32) {
		case 19200:
			U0DLL = (Uint8)23;			/* Set for baud low byte			*/
			U0DLM = (Uint8)0;			/* set for baud high byte			*/
			U0FDR = 3 | (11<<4);		/* DIVADDVAL = 3,MULVAL = 11		*/
			break;

		case 57600:
			U0DLL = (Uint8)6;      		/* Set for baud low byte			*/
			U0DLM = (Uint8)0;			/* set for baud high byte			*/
			U0FDR = 5 | (8<<4);			/* DIVADDVAL = 5,MULVAL = 8			*/
			break;

		case 115200:
			U0DLL = (Uint8)4;      		/* Set for baud low byte			*/
			U0DLM = (Uint8)0;			/* set for baud high byte			*/
			U0FDR = 2 | (9<<4);			/* DIVADDVAL = 2,MULVAL = 9			*/
			break;

		case 9600:
		default:
			U0DLL = (Uint8)37;			/* Set for baud low byte			*/
			U0DLM = (Uint8)0;			/* set for baud high byte			*/
			U0FDR = 7 | (12<<4);		/* DIVADDVAL = 7,MULVAL = 12		*/
			break;
	}

	/*
	 *	Databits
	 */

	if( dataBits == UART_DATABITS_5) {
		/* 5 data bits */

	} else if( dataBits == UART_DATABITS_6) {
		U0LCR |= (1<<0); /* 6 data bits */

	} else if( dataBits == UART_DATABITS_7) {
		U0LCR |= (1<<1); /* 7 data bits */

	} else if( dataBits == UART_DATABITS_8) {
		U0LCR |= (1<<0)|(1<<1); /* 8 data bits */

	} else {
		deb_assert(FALSE);
		U0LCR |= (1<<0)|(1<<1); /* 8 data bits */
	}

	/*
	 *	Parity
	 */

	if( parity == UART_PARITY_EVEN) {
		U0LCR |= (1<<3)|(1<<4); /* Even parity */

	} else if( parity == UART_PARITY_ODD) {
		U0LCR |= (1<<3); /* Odd parity */

	} else if( parity == UART_PARITY_NONE) {
		/* No parity */

	} else {
		deb_assert(FALSE);
		U0LCR |= (1<<3)|(1<<4); /* Even parity */
	}

	/*
	 *	Stopbits
	 */

	if( stopBits == UART_STOPBITS_1) {
		/* 1 stop bit */

	} else if( stopBits == UART_STOPBITS_2) {
		U0LCR |= (1<<2); /* Use 2 stop bits */

	} else {
		deb_assert(FALSE);
		U0LCR |= (1<<2); /* Use 2 stop bits */
	}

	U0LCR &= ~(1<<7);					/* Disable latch					*/
	U0FCR = (1<<0);						/* Enable FIFO						*/

	VICIntEnable = (1<<6);
	VICVectAddr6 = (long) uart__isr;
	VICVectCntl6 = 1;

	U0IER = (1<<0)|(1<<1);				/* Enable RBR / THRE interrupts		*/
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart_write0
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Function for sending data to the UART.
*
*	\param		pUart 	Pointer to the UART instance.
*	\param		pBytes 	Pointer to the data that is to be sent.
*	\param		nLen 	Length of the data.
*
*	\details
*
*	\note
*
*******************************************************************************/

PUBLIC void uart_write0(
	void *					pUart,
	Uint8 *					pBytes,
	Uint16					nLen
) {
	deb_assert(pUart == &uart0);

	if (nLen > 0) {
		uart0.pSendPtr = pBytes;
		uart0.nSendLen = --nLen;

		U0THR = *uart0.pSendPtr++;
	}
}
/** \endcond */
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	uart__isr
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Interrupt service routine for uart0
*
*	\details
*
*	\note
*
*******************************************************************************/

PRIVATE osa_isrOSFn(NULL, uart__isr)
{
	Uint32					nIid;
	Uint8					rxError = FALSE;

	osa_isrEnter();

	FOREVER {
		nIid = U0IIR;

		if (nIid & UART__UxIIR_NO_ISR) {
			break;
		}

		switch(nIid & 0x0E) {
		case UART__UxIIR_RDA:
		case UART__UxIIR_CTI:
			do {
				if(rxError || U0LSR & UART__UxLSR_RXE) {
					rxError = TRUE;
					U0RBR;

				} else {
					project_uart0Callback.fnDataReceived(U0RBR, uart0.pUtil);
				}

			} while (U0LSR & UART__UxLSR_RDR); /* Valid data available? */
			break;

		case UART__UxIIR_THRE:
			while (U0LSR & UART__UxLSR_THRE) {
				if (uart0.nSendLen == 0) {
					project_uart0Callback.fnDataSent(uart0.pUtil);
					break;
				}

				uart0.nSendLen--;
				U0THR = *uart0.pSendPtr++;
			}
			break;

		default:
			U0LSR;
			U0RBR;
			break;
		}
	}

	VICVectAddr = 0x00000000;

	osa_isrLeave();
}
osa_endOSIsr
