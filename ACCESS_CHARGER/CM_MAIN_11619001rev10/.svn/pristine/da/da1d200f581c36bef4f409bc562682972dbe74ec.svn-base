/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		amif.h
*
*	\ingroup	AMIF
*
*	\brief		Asynchronous MII Management bus (MDIO) Interface.
*
*	\details	This is the public header file of the interface.
*
*	\note
*
*	\version	\$Rev: 3657 $ \n
*				\$Date: 2019-03-22 15:21:40 +0200 (pe, 22 maalis 2019) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	AMIF	AMIF
*
*	\brief		Asynchronous MII Management bus (MDIO) Interface.
*
********************************************************************************
*
*	\details	This interface should be implemented by all Station Management
*				Entity (STA) drivers that needs an asynchronous interface. It is
*				based on the generic asynchronous interface (GAI). The STA is
*				the bus master for the MDIO bus.
*
*				This interface does the same thing as the synchronous MIIMIF
*				interface. AMIF should be preferred over MIIMIF because it is
*				possible to change an asynchronous interface into a synchronous
*				interface when needed but not the other way around.
*
*	\par		Request done callback
*				The callback function must be called from a task or a CoTask
*				with interrupts and tasking enabled. It must not be called from
*				an ISR.
*	\par
*				It should be possible to call the access function again from 
*				the callback function.
*
*******************************************************************************/

#ifndef AMIF_H_INCLUDED
#define AMIF_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "GAI.h"
#include "MDIO.h"

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Operation code for asynchronous requests.
 */
 
typedef enum {							/*''''''''''''''''''''''''''''''''''*/
	AMIF_OP_READ,						/**< Read register.					*/
	AMIF_OP_WRITE						/**< Write register.				*/
} amif_Op;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \brief		STA driver asynchronous interface type.
 *
 * \details		Each STA driver should have a public constant of this type. A
 *				pointer to the constant is supplied as an initialization
 *				parameter to every FB using the MDIO bus.
 *
 *				A pointer of type amif_Req should be supplied to the access
 *				function when using the interface.
 *
 *				The interface is based on the generic asynchronous interface
 *				(GAI). It uses the base GAI interface type without
 *				modifications.
 */

typedef gai_Interface amif_Interface;

/**
 *	\brief		Request type for asynchronous MDIO operations.
 *
 *	\details	This type should be used with the access function in the 
 *				amif_Interface. The GAI access function takes a pointer to a
 *				gai_Req but when this interface is used the pointer should point
 *				to a structure of this type. The STA driver can always cast the
 *				pointer to a amif_Req pointer.
 *
 *				The request must be statically or dynamically allocated by the
 *				FB that calls the access function. It must remain allocated and
 *				unchanged until the callback function is called.
 *
 *				The phyAddr, regNum, op, pDoneFn and pUtil fields should not be
 *				changed by the STA driver. They may temporarily be changed but
 *				the original value must be restored before the callback function
 *				is called.
 *
 *				The data field should only be modified by the STA if a
 *				AMIF_OP_READ operation has been requested. The pNext pointer in
 *				the baseReq may freely be modified by the STA.
 *
 *				The type can be safely casted to gai_Req type.
 *
 *	\extends	gai_Req
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	gai_Req					baseReq;	/**< Base request data.				*/
	Uint8					phyAddr;	/**< PHY-chip address.				*/
	Uint8					regNum;		/**< Register number/address.		*/
	amif_Op					op;			/**< Requested operation.			*/
	Uint16					data;		/**< Data.							*/
} amif_Req;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
