/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */
#ifndef __LWIP_RAW_H__
#define __LWIP_RAW_H__

#include "ntcpip/opt.h"

#if NTCPIP__LWIP_RAW /* don't build if not configured for use in lwipopts.h */

#include "ntcpip/pbuf.h"
#include "ntcpip/inet.h"
#include "lwip/ip.h"
#include "ntcpip/ip_addr.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ntcpip__rawPcb {
/* Common members of all PCB types */
  NTCPIP__IP_PCB;

  struct ntcpip__rawPcb *next;

  Uint8 protocol;

  /* receive callback function
   * @param arg user supplied argument (raw_pcb.recv_arg)
   * @param pcb the raw_pcb which received data
   * @param p the packet buffer that was received
   * @param addr the remote IP address from which the packet was received
   * @return 1 if the packet was 'eaten' (aka. deleted),
   *         0 if the packet lives on
   * If returning 1, the callback is responsible for freeing the pbuf
   * if it's not used any more.
   */
  Uint8 (* recv)(void *arg, struct ntcpip__rawPcb *pcb, struct ntcpip_pbuf *p,
    struct ntcpip_ipAddr *addr);
  /* user-supplied argument for the recv callback */
  void *recv_arg;
};

/* The following functions is the application layer interface to the
   RAW code. */
struct ntcpip__rawPcb * raw_new        (Uint8 proto);
void             raw_remove     (struct ntcpip__rawPcb *pcb);
ntcpip_Err            raw_bind       (struct ntcpip__rawPcb *pcb, struct ntcpip_ipAddr *ipaddr);
ntcpip_Err            raw_connect    (struct ntcpip__rawPcb *pcb, struct ntcpip_ipAddr *ipaddr);

void             raw_recv       (struct ntcpip__rawPcb *pcb,
                                 Uint8 (* recv)(void *arg, struct ntcpip__rawPcb *pcb,
                                              struct ntcpip_pbuf *p,
                                              struct ntcpip_ipAddr *addr),
                                 void *recv_arg);
ntcpip_Err            raw_sendto     (struct ntcpip__rawPcb *pcb, struct ntcpip_pbuf *p, struct ntcpip_ipAddr *ipaddr);
ntcpip_Err            raw_send       (struct ntcpip__rawPcb *pcb, struct ntcpip_pbuf *p);

/* The following functions are the lower layer interface to RAW. */
Uint8             raw_input      (struct ntcpip_pbuf *p, struct ntcpip__netif *inp);
#define raw_init() /* Compatibility define, not init needed. */

#ifdef __cplusplus
}
#endif

#endif /* NTCPIP__LWIP_RAW */

#endif /* __LWIP_RAW_H__ */

