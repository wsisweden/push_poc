/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		TXT_CPY.C
*
*	\ingroup	TXT
*
*	\brief		Functions to support use of language-dependent strings.
*
*	\details		
*
*	\par		Module name:
*				Text handling functions
*
*	\par		Files:
*				TXT_CPY.C TXT.H
*
*	\note		
*
*	\version	08-05-05 / TJK
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#define TXT_SKIP_INIT_H
#include "txt.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 *	The following symbols are defined by EXE.H when included from PROJECT.C.
 */

extern unsigned char const_P * const_P * const_P exe_textTables[];
extern Uint8 const_P exe_dimTextTables;

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	txt_localCpy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Copy indexed local text of currently selected language.
*
*	\param		target 	Ptr to the target buffer for the text.
*	\param		instId 	The FB instance in question.
*	\param		handle 	Local text handle i.e. index (0..) of the text to copy.
*
*	\return		Number of characters copied. No prefix (length) is copied and no
*				trailing NUL is appended.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC txt_LenType txt_localCpy(
	void *					target,
	sys_FBInstId			instId,
	sys_TxtIndex			handle
) {
	return(
		txt_cpy(
			target,
			txt_localHandle(instId, handle)
		)
	);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	txt_cpy
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Copy indexed text of currently selected language.
*
*	\param		target 	Ptr to the target buffer for the text.
*	\param		handle 	Project-wide text handle i.e. index (0..) of the text to
*						copy.
*
*	\return		Number of characters copied. No prefix (length) is copied and no
*				trailing NUL is appended.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC txt_LenType txt_cpy(
	void *					target,
	sys_TxtHandle			handle
) {
	txt_LenType retVal;
	unsigned char const_P *source;

	source = txt_text(handle);
	retVal = (txt_LenType) source[0];

	if (retVal) {

		txt_LenType ii;
		unsigned char *tt;

		ii = retVal;
		tt = target;

		do {
			source++;
			*tt = *source;
			tt++;
		} while (--ii);
	}
	return(retVal);
}
