/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		POD_STRU.C
*
*	\ingroup	POD
*
*	\brief		Search functions for struct type pods.
*
*	\details		
*
*	\note		
*
*	\version	05-03-08 / Ari Suomi
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "pod.h"
#include "lib.h"
#include "deb.h"

#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS 
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Uint16 pod__getIndexStruct(pod_Search *);
PRIVATE Uint16 pod__makeStructHash(void const_D *,Uint8);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for struct type PODs
 */
 
PUBLIC pod_TypeTools const_P pod_structTools = {
	&pod__getIndexStruct
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pod__getIndexStruct
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Returns the index of the pod register mapping.
*
*	\param		pParams 	The search parameters.
*
*	\return		The POD index of register. If the register could not be found,
*				the function returns POD_NOTFOUND.
*
*	\details	Step 1 The function searches the pod with a binary search. The
*				binary search only compares the lower 16-bits. If there are many
*				keys with the same lower 16 bits the binary search finds the
*				first matching key.
*
*				Step 2 A linear search is performed. The search starts from the
*				index provided by the binary search. The linear search compares
*				protocol register names until the correct register is found, the
*				lower 16 bits changes or the end of the table is reached.
*
*	\note	
*
*******************************************************************************/
			  
PRIVATE Uint16 pod__getIndexStruct(
	pod_Search *			pParams
) {
	Uint16					u16Idx;
	Uint32 const_P *		pTable;		/* The table to search in			  */
	Uint16					SearchKey;	/* Search key generated from the name */
	Uint16					TblSize;	/* Maximum table index				  */

	/*
	 *	Search setup.
	 */

	SearchKey = pod__makeStructHash(pParams->pProtName, pParams->nameSize);
	pTable =	pParams->pPod->pKeyTable;
	TblSize =	pParams->pPod->u16Size;

	/*
	 *	Binary search on lower 16 bits.
	 */

	u16Idx = pod__binarySearch(SearchKey, pTable, TblSize);
	
	if (u16Idx == POD_NOTFOUND) 
	{
		return u16Idx;
	}

	/*
	 *	Linear search (Comparing structure contents).
	 */

	{
		void const_P *pComp;
		
		/*
		 *	Store address of the first value to the comparison pointer.
		 */

		pComp = (unsigned char const_P *) pParams->pPod->pAttrs +
			u16Idx * pParams->pPod->u8AttrSize;

		do 
		{
			
			/*
			 *	Compares the searched protocol register name with the
			 *	corresponding name stored in the POD.
			 */

			if (
				lib_pMemcmp(
					pParams->pProtName, 
					pComp, 
					pParams->nameSize
				) == 0
			) {
				return u16Idx;
			}
			
			/*
			 *	Steps forward the comparison pointer.
			 */

			pComp = (Uint8 const_P *) pComp + pParams->pPod->u8AttrSize;

			/*
			 *	Steps forward the search index and checks that the lower 16 bits 
			 *	still match the search key and that the index is still inside 
			 *	the table.
			 */
			u16Idx++;

		} until ((Uint16) pTable[u16Idx] != SearchKey || u16Idx > TblSize);
	}

	/* 
	 *	if the register could not be found 
	 */

	return POD_NOTFOUND;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	pod__makeStructHash
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Makes hash from a struct
*
*	\param		pStruct Pointer to the struct.
*	\param		uSize 	Size of the struct in BYTEs (1..).
*
*	\return		A 16-bit hash value (Uint16).
*
*	\details	This hash function has to be used when hashing structs.
*
*				The endianness and the amount of padding structs depends on the
*				target. To always generate the same hash on all targets a few
*				things has to be considered.
*
*				This hash function does not change the hash on zero bytes.
*				Padding bytes in structs should be initialized to zero before
*				using this hash function. That way they won't affect the hash.
*
*				Endianness is countered by making the hash function indifferent
*				to byte ordering.
*
*	\note	
*
*******************************************************************************/

PRIVATE Uint16 pod__makeStructHash(
	void const_D *			pStruct,
	Uint8					uSize
) {
	Uint16 u16Hash;

	u16Hash = 0;
	
	do {
		Uint8 nextByte;

		nextByte = *(Uint8 const_D *) pStruct;
		u16Hash += nextByte ^ (Uint16) nextByte << 1 ^ (Uint16) nextByte << 3;
		pStruct = (Uint8 const_D *) pStruct + 1;

	} while (--uSize);

	return(u16Hash);
}
