/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		OSA\inc_w32\OSAW32.C
*
*	\ingroup	OSA_W32
*
*	\brief		uCOS-II	port for 32-bit Windows - OSA interface
*
*	\details		
*
*	\note		
*
*	\version	xx.xx.xxxx / Juha Ylinen
*
*******************************************************************************/

/*******************************************************************************
*
*	G R O U P   D O C U M E N T A T I O N
*
****************************************************************************//**
*
*	\defgroup	OSA_W32	W32
*
*	\ingroup	OSA
*
*	\brief		
*
********************************************************************************
*
*	\details	
*
*******************************************************************************/

/*
	0:	disabled
	1:	osa calls prod__swTimerIsr -function on every tick implemented by
		the application.
*/
#define	OSA__TIMER_CALLBACK_PROG		1

/*-----------------------------------------------------------------------------
 *
 *	Includes.
 *
 *---------------------------------------------------------------------------*/

#include <stdlib.h>

#include "TOOLS.H"
#include "osa.h"
#include "deb.h"

#include "os_cpu.h"
#include "os_cfg.h"
#include "ucos_ii.h"

/*-----------------------------------------------------------------------------
 *
 *	Defines.
 *
 *---------------------------------------------------------------------------*/

#define OSA__ISR_DISABLE	1

#define OSA__PRIORITY_1ST	(OSA_PRIORITY_LOW - 1)
#define OSA__PRIORITY_CNT	(OSA_PRIORITY_HIGH - OSA_PRIORITY_LOW + 3)

/*-----------------------------------------------------------------------------
 *
 *	Types.
 *
 *---------------------------------------------------------------------------*/
 
typedef struct {						
	osa_TaskType *			link;		
	void (OSA_TASK *		fn)(void);	
	size_t					stackSize;	
} osa__NewTaskInfo;						

/*-----------------------------------------------------------------------------
 *
 *	Macros.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Globals.
 *
 *---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *
 *	Locals.
 *
 *---------------------------------------------------------------------------*/

PUBLIC	osa_TaskType *			osa__tskPriorTbl[OS_MAX_TASKS];
PUBLIC	Uint8					osa_nTaskCount=0;
PUBLIC  unsigned				osa__taskingLockCount = 1;
//PUBLIC	Boolean					osa_isRunning = FALSE;

SYS_DEFINE_FILE_NAME;

/*-----------------------------------------------------------------------------
 *
 *	Functions.
 *
 *---------------------------------------------------------------------------*/
 
extern void OSInitHookBegin();

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa__w32tickCB
*
*--------------------------------------------------------------------------*//**
*
*	\brief		uCOS-II calls this function on every tick
*
*	\details	
*
*	\note	
*
*******************************************************************************/
PUBLIC  void osa__w32tickCB(
	void
) {
#if OSA__TIMER_CALLBACK_PROG == 1
	project_swTimerIsr();
#endif
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_init
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Initialize OSA and the Operating System.
*
*	\details	This function should be called before invoking any other
*				OSA-service.
*
*	\note	
*
*******************************************************************************/

PUBLIC void osa_init(
	void
) {
	OSInit();
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_newTask
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Create a new task.
*
*	\param		pTask 		Pointer to the scratch task-object to initialize.
*	\param		name 		Unique name for the task. The name must be a hard
*							constant that is valid and available throughout the
*							program execution.
*	\param		priority 	Task's priority, one of the predefined constants (see
*							below) optionally fine-tuned with +/-1.
*	\param		taskFn 		Task's "main" function.
*	\param		stackSize 	Size of the task's stack in bytes.
*
*	\details	Must be called before the OS is started i.e. osa_run() is
*				called.
*	<pre>
*				Priority            Description
*				OSA_PRIORITY_HIGH   Critical tasks should use this priority.
*				OSA_PRIORITY_NORMAL This should be the priority of each task,
*				                    unless there are reasons that force to raise
*				                    or lower it.
*				OSA_PRIORITY_LOW    Less-important tasks should use this
*				                    priority.
*	</pre>
*				All the three priority values can be fine-tuned, when necessary,
*				for higher or lower priority by adding or subtracting,
*				respectively, one to/from the constant value. How the actual
*				priorities are set is an implementation dependent matter, but
*				OSA supports up to nine different priority levels.
*
*				Task's "main" function should include the OSA_TASK type
*				qualifier. Tasks never return thus the compiler allowed optimize
*				such functions in a more efficient way.
*					
*	\note	
*
*******************************************************************************/

PUBLIC void osa_newTask(
	osa_TaskType *			pTask,
	const_P char *			name,
	Uint8					priority,
	void (OSA_TASK *		taskFn)(void),
	Uint16					stackSize
) {
	osa__NewTaskInfo*	pInfo;

	pTask->priority = --priority;
	pTask->pName	= name;

#if 0
	stackSize = 1 + MAX(stackSize, sizeof(osa__NewTaskInfo)) & ~(size_t) 1;

	pInfo = (osa__NewTaskInfo *) mem_reserve(&mem_fast, stackSize);

#else
	pInfo = (osa__NewTaskInfo*)malloc( MAX(
											stackSize, 
											sizeof(osa__NewTaskInfo))
										);
#endif
	deb_assert( pInfo );

	osa_newSemaTaken( &pTask->signal );

	pInfo->fn		= taskFn;
	pInfo->stackSize= stackSize;
	pInfo->link		= osa__tskPriorTbl[priority];
					  osa__tskPriorTbl[priority] = pTask;	 	

	pTask->info = (void*)pInfo;
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_run
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Start multitasking.
*
*	\return		(Never returns.)
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC void osa_run(
	void
) {
	osa_TaskType *			pTask;
	Uint8					nInd;
	osa_TaskType *			pTskList = NULL;
	unsigned				cnt;

	nInd = 0;

	OSDisableInterruptFlag();
	
	{
		void osa__run(void);
		osa__run();
	}

	do {
		pTask = osa__tskPriorTbl[nInd];

		while ( pTask ) {
			osa_TaskType *		pTmp;

			pTmp = ((osa__NewTaskInfo*)pTask->info)->link;

			((osa__NewTaskInfo*)pTask->info)->link = pTskList;
			pTskList = pTask;

			pTask = pTmp;
			osa_nTaskCount++;
		}

	} while ( nInd++ < OSA__PRIORITY_CNT );

	for ( nInd = 0; nInd < OSA__PRIORITY_CNT; nInd++ ) {
		osa__tskPriorTbl[nInd] = NULL;	
	}

	cnt = 0;
	pTask	= pTskList;

	deb_assert( pTask );

	do {
		osa__NewTaskInfo*	pInfo;

		pInfo = pTask->info;

		osa__tskPriorTbl[cnt] = pTask;
		pTask->priority = cnt;
		OSTaskCreate( (void (*)(void*))pInfo->fn, (void*)pTask, (OS_STK*)pInfo, (Uint8)cnt++ );

		pTask = pInfo->link;

	} while ( pTask );

	osa_isRunning = TRUE;
	osa__taskingLockCount = 0;
	OSEnableInterruptFlag();
	OSInitHookBegin();
	OSStart();    
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_semaSetFn
*
*--------------------------------------------------------------------------*//**
*
*	\brief	
*
*	\param		pSema 
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC void	osa_semaSetFn( 
	osa_SemaType *			pSema
) {
	OSSemPost( pSema->sema );
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_signalSendFn
*
*--------------------------------------------------------------------------*//**
*
*	\brief	
*
*	\param		pTask 
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC void	osa_signalSendFn( 
	osa_TaskType *		pTask
) {
	OSSemPost( pTask->signal.sema );
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_taskingDisable
*
*--------------------------------------------------------------------------*//**
*
*	\brief	
*
*	\details	
*
*	\note	
*
*******************************************************************************/
PUBLIC void	osa_taskingDisable(
	void
) {
#if OSA__ISR_DISABLE == 1
	OSSchedLock();
	osa__taskingLockCount++;

#else
	OSDisableInterruptFlag();

	osa__taskingLockCount++;
#endif
}

/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	osa_taskingEnable
*
*--------------------------------------------------------------------------*//**
*
*	\brief	
*
*	\details	
*
*	\note	
*
*******************************************************************************/
PUBLIC void	osa_taskingEnable(
	void
) {
#if OSA__ISR_DISABLE == 1
	OSSchedUnlock();
	osa__taskingLockCount--;
#else
	osa__taskingLockCount--;

	if ( !osa__taskingLockCount )
		OSEnableInterruptFlag();
#endif
}

PUBLIC void	osa__isrEnter(
	void
) {
}

PUBLIC void osa__isrLeave(
	void
) {
}

PUBLIC void osa_interruptsDisable(
	void
) {
#if OSA__ISR_DISABLE == 1
	OSDisableInterruptFlag();
#endif
}
			
PUBLIC void osa_interruptsEnable(
	void 
) {
#if OSA__ISR_DISABLE == 1
	OSEnableInterruptFlag();
#endif
}
