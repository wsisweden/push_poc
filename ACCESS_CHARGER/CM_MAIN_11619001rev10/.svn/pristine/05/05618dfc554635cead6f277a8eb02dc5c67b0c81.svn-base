/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	DP83848T
*
*	\brief		Public declarations of the DP83848T function block.
*
*	\details	DP83848T is a Ethernet PHY chip by TI.
*
*	\note		
*
*	\version	\$Rev: 4436 $ \n
*				\$Date: 2021-02-17 14:13:50 +0200 (ke, 17 helmi 2021) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

#ifndef DP83848T_H_INCLUDED
#define DP83848T_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "tools.h"
#include "apif.h"
#include "phyif.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

/**
 * PHY specific registers
 */

enum {									/*''''''''''''''''''''''''''''''''''*/
	DP83848T_REG_PHYSTS = 0x0010,		/**< PHY Status Register.			*/
	DP83848T_REG_FCSCR = 0x0014,		/**< False Carrier Sense Counter.	*/
	DP83848T_REG_RECR,					/**< Receiver Error Counter.		*/
	DP83848T_REG_PCSR,					/**< 100 Mb/s PCS Conf. and Status. */
	DP83848T_REG_RBR,					/**< RMII and Bypass Register.		*/
	DP83848T_REG_LEDCR,					/**< LED Direct Control Register.	*/
	DP83848T_REG_PHYCR,					/**< PHY Control Register.			*/
	DP83848T_REG_10BTSCR = 0x001A,		/**< 10Base-T Status/Control.		*/
	DP83848T_REG_CDCTRL1,				/**< CD Test and BIST Extensions.	*/
	DP83848T_REG_EDCR					/**< Energy Detect Control.			*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \name	PHY Status Register bits
 */

/*@{*/
#define DP83848T_STS_LSTATUS	(1<<0)	/**< Valid link established.		*/
#define DP83848T_STS_SPEED10	(1<<1)	/**< 10 Mb/s mode.					*/
#define DP83848T_STS_DUPLEX		(1<<2)	/**< Full duplex mode.				*/
#define DP83848T_STS_LOOPBACK	(1<<3)	/**< Loopback enabled.				*/
#define DP83848T_STS_AN_COMP	(1<<4)	/**< Auto-Negotiation complete.		*/
#define DP83848T_STS_JABBER		(1<<5)	/**< Jabber condition detected.		*/
#define DP83848T_STS_R_FAULT	(1<<6)	/**< Remote Fault condition detected.*/
#define DP83848T_STS_PAGE_R		(1<<8)	/**< Link Code Word Page received.	*/
#define DP83848T_STS_D_LOCK		(1<<9)	/**< 100Base-TX Descrambler Lock.	*/
#define DP83848T_STS_SIG_DET	(1<<10)	/**< 100Base-TX Signal Detect.		*/
#define DP83848T_STS_F_CARRIER	(1<<11)	/**< False Carrier event has occurred.*/
#define DP83848T_STS_POL		(1<<12)	/**< Inverted Polarity detected.	*/
#define DP83848T_STS_R_ERROR	(1<<13)	/**< Receive error event has occurred.*/
#define DP83848T_STS_MDI_X		(1<<14)	/**< MDI pairs swapped.				*/
/*@}*/

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/

extern apif_Interface const_P dp83848t_if;
extern phyif_Interface const_P dp83848t_syncIf;

/*****************************************************************************/

#endif

