/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	GUI
*
*	\brief		View handling functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-yyyy / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
//#include "reg.h"
#include "mem.h"
#include "global.h"
#include "deb.h"

#include "txt.h"

#include "gui.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE Boolean gui__viewBaseProc(gui__Inst * pInst, gui__View * pView, gui__ViewMessage * pMsg, void * pArgs);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/*
 *	View templates.
 */
PRIVATE gui__ViewTemplate gui__viewTemplates[] =
{
	{	/* GUI_VIEW_DEBUG */
		gui__viewDbgProc,
		0
	},
	{	/* GUI_VIEW_ERROR */
		gui__viewErrorProc,
		0
	},
	{	/* GUI_VIEW_MENU */
		gui__viewMenuProc,
		0
	},
	{	/* GUI_VIEW_BAY */
		gui__viewBayProc,
		0
	},
	{	/* GUI_VIEW_POPUP */
		gui__viewPopupProc,
		1
	},
	{	/* GUI_VIEW_TIMETABLE */
		gui__viewTimeProc,
		0
	},
	{	/* GUI_VIEW_LOG */
		gui__viewLogProc,
		0
	},
	{	/* GUI_VIEW_CS */
		gui__viewCsProc,
		0
	},
	{	/* GUI_VIEW_QUERY */
		gui__viewQueryProc,
		1
	},
	{	/* GUI_VIEW_SCP */
		gui__viewScpProc,
		0
	},
	{	/* GUI_VIEW_SRO */
		gui__viewSroProc,
		0
	},
	{	/* GUI_VIEW_CAN */
		gui__viewCanProc,
		0
	},
	{	/* GUI_VIEW_SRI */
		gui__viewSriProc,
		0
	},
};

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Initializes view handling data.
*
* \param		pInst		ptr to GUI instance
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__viewInit(
	gui__Inst *				pInst
) {
	pInst->view.pCurrent = NULL;
//	pInst->view.pPopup = NULL;

	(*(gui__View *)&pInst->view.views[0]).proc = NULL;
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Creates a view.
*
* \param		pInst		ptr to GUI instance
* \param		viewtype	view type to create
* \param		pArgs		view init message args
*
* \return		Ptr to created view.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC gui__View * gui__viewCreate(
	gui__Inst *				pInst,
	Uint8					viewtype,
	void *					pArgs
) {
	gui__ViewTemplate const_P *	pTmp;
	gui__View *				pView;

	deb_assert(viewtype < (Uint8)dim(gui__viewTemplates));

	if (viewtype >= dim(gui__viewTemplates))
	{
		/* Activate error view on fatal error. */
		viewtype = GUI_VIEW_ERROR;
	}

	pTmp = &gui__viewTemplates[viewtype];
	pView = (gui__View *)&pInst->view.views[pTmp->index];

	if (pTmp->proc(pInst, pView, gui_msg_create, pArgs) < 0)
	{
		return NULL;
	}

	if (pView->proc != NULL)
	{
		gui__viewSendMessage(pInst, pView, gui_msg_close, NULL);
	}

	pView->proc = pTmp->proc;
	gui__viewInvalidate(pView);

	gui__viewSendMessage(pInst, pView, gui_msg_init, pArgs);

	return pView;
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Opens (and creates) a view of given type.
*
* \param		pInst		ptr to GUI instance
* \param		viewtype	view type to open
* \param		pArgs		view arguments
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__viewOpen(
 	gui__Inst *				pInst,
	Uint8					viewtype,
	void *					pArgs
) {
	pInst->view.pCurrent = gui__viewCreate(pInst, viewtype, pArgs);
}


/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Closes a view.
*
* \param		pInst		ptr to GUI instance
* \param		pView		view to close
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC void gui__viewClose(
	gui__Inst *				pInst,
	gui__View *				pView
) {
	gui__viewSendMessage(pInst, pView, gui_msg_close, NULL);
	pView->proc = NULL;

	if (pView == pInst->view.pCurrent)
	{
		Uint8 view;

		/*
		 *	Find next view to show starting from last (highest priority) in the
		 *	view list.
		 */
		pInst->view.pCurrent = NULL;

		view = GUI_MAX_VIEW_TYPES;

		do 
		{
			gui__View * pNext;

			view--;

			pNext = (gui__View *)&pInst->view.views[view];

			if ((pNext != pView) && (pNext->proc != NULL)) 
			{
				pInst->view.pCurrent = (gui__View *)&pInst->view.views[view];
				break;
			}

		} while (view != 0);

		if (pInst->view.pCurrent != NULL)
		{
			gui__viewSendMessage(
				pInst, 
				pInst->view.pCurrent, 
				gui_msg_rebuild, 
				NULL
			);

			gui__viewInvalidate(pInst->view.pCurrent);
		}
	} 
}



/******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*-------------------------------------------------------------------------*//**
*
* \brief		Sends a message to a view.
*
* \param		pInst		ptr to GUI instance
* \param		pView		target view
* \param		msg			message type
* \param		pArgs		message args
*
* \return		View return code.
*
* \details		
*
* \note			
*
* \sa			
*
******************************************************************************/
PUBLIC Int8 gui__viewSendMessage(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage		msg,
	void *					pArgs
) {
	//Int8					ret = 0;

	if (gui__viewBaseProc(pInst, pView, &msg, pArgs))
	{
		/*ret = */pView->proc(pInst, pView, msg, pArgs);
	}

	return 0;
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Base view message handler called BEFORE calling the actual view
*				handler. This may prevent sending the message forward or 
*				transform the message into another message.
*
*	\param		pInst		ptr to GUI instance
*	\param		pView		target view
*	\param		pMsg			Ptr to message type
*	\param		pArgs		message args
*
*	\return		
*
*	\details		
*
*	\note		Create message is never routed here.
*
*	\sa			
*
*******************************************************************************/
PRIVATE Boolean gui__viewBaseProc(
	gui__Inst *				pInst,
	gui__View *				pView,
	gui__ViewMessage *		pMsg,
	void *					pArgs
) {
	Boolean					allow_to_proceed = TRUE;

	switch (*pMsg)
	{
	/*
	 *	Automatically validate view every time is painted.
	 */
	case gui_msg_paint:
		gui__viewValidate(pView);
		break;

	/*
	 *	Process tick message and transform it into a timeout message if
	 *	set tick count has expired.
	 */
	case gui_msg_tick:
		if (gui__viewTmrRunning(pView) && (--pView->delay == 0))
		{
			*pMsg = gui_msg_timeout;

			if (pView->reload)
			{
				pView->delay = pView->reload;
			}
			else
			{
				gui__viewTmrStop(pView);
			}
		}
		else
		{
			/* Timer not running or not expired yet */
			allow_to_proceed = FALSE;
		}
		break;

	default:
		break;
	}

	return allow_to_proceed;
}
