/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		flmem_vc.c
*
*	\ingroup	FLMEM
*
*	\brief		FLMEM configuration verifying.
*
*	\details	
*
*	\note
*
*	\version	\$Rev: 769 $ \n
*				\$Date: 2012-01-13 16:50:22 +0200 (pe, 13 tammi 2012) $ \n
*				\$Author: tlarsu $
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************/

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

/* T-Plat.E library */
#include "tools.h"
#include "deb.h"
#include "sys.h"
#define REG__IMPLEM
#include "reg.h"
#include "flmem.h"

/* local */
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void flmem__verifyOffset(Uint16);
PRIVATE void flmem__verifyReg(Uint16,Uint8,Uint16);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem_verifyConf
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Verifies that the FLMEM configuration is valid.
*
*	\return		-
*
*	\details	This function should be called in the main function before
*				osa_run but after sys_initAll.
*				
*				It should always be called in DEBUG build. In some cases it can
*				also be useful to call it in RELEASE build.
*
*	\note
*
*******************************************************************************/

PUBLIC void flmem_verifyConf(
	void
) {
	{
		flmem_TagOffset const_P *	pTags;
		Uint16						ii;
		Uint16						oldvalue;

		/*
		 *	Checking that the tag table in offset order is sorted correctly.
		 */

		oldvalue = Uint16_MAX;
		pTags = flmem__instance.pInit->flashtagsByOffsetOrder;
		for (ii = *flmem__instance.pInit->tagCount - 1; ii != 0; ii--) {
			if (oldvalue <= pTags[ii].offset) {
				deb_assert(FALSE);
				FOREVER {

				}
			}
			oldvalue = pTags[ii].offset;
		};

		/*
		 *	Checking that the tag table in tag order is sorted correctly.
		 */

		oldvalue = Uint16_MAX;
		pTags = flmem__instance.pInit->flashtagsByTagsOrder;
		for (ii = *flmem__instance.pInit->tagCount - 1; ii != 0; ii--) {
			if (oldvalue <= pTags[ii].tag) {
				deb_assert(FALSE);
				FOREVER {

				}
			}
			oldvalue = pTags[ii].tag;
		};
	}

	/*
	 *	Check that all non-volatile registers are included in the tag tables.
	 */

	{
		extern sys_RegDescr const_P sys_regFile[];
		extern sys_RegHandle const_P sys_dimRegFile;
		sys_RegDescr const_P *		pRegDescr;
		sys_RegHandle				regsLeft;

		pRegDescr = sys_regFile;
		regsLeft = sys_dimRegFile;
		do {
			Uint8					regType;
			Uint8					elementSize;
			Uint8					dimension;
			Uint16					baseOffset;

			if (pRegDescr->status & SYS_REG_RIND) {
				/* P-type registers will never be stored to flash memory. */
				pRegDescr++;
				continue;
			}

			{
				reg__Implem const_P *	pImp;
				extern reg_ImplemType	reg_typeImplem;

				regType = pRegDescr->status & REG_TYPE_MASK;
				pImp = &reg_typeImplem[regType];
				elementSize = pImp->attrib & REG_A_STORAGE;
				dimension = pRegDescr->size;
			}

			if (elementSize) {
				if (regType == SYS_REG_T_String) {
					/* String registers will only have one tag. */
					dimension = 1;
				}

				baseOffset = pRegDescr->value;
				flmem__verifyReg(baseOffset, dimension, elementSize);

			} else if (regType == SYS_REG_T_Typedef) {
				sys_TypedefType const_P *	pType;
				extern BYTE const_P			exe__types[];

				pType = (void const_P *) &exe__types[pRegDescr->value];
				elementSize = pType->reg_tHdr.reg_tTools->reg_tSize;
				deb_assert(elementSize);

				baseOffset = pType->reg_tHdr.reg_tValue;

				flmem__verifyReg(baseOffset, dimension, elementSize);
			}

			pRegDescr++;
		} while (--regsLeft);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__verifyReg
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Verify that each element of the supplied register is included
*				in the tag table.
*
*	\param		baseOffset		RAM image offset of the first register element.
*	\param		dimension		Number of elements in register.
*	\param		elementSize		Size of each element.
*	
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__verifyReg(
	Uint16					baseOffset,
	Uint8					dimension,
	Uint16					elementSize
) {
	extern Uint16 const_P	sys_sizeImageNOV;

	if (baseOffset < sys_sizeImageNOV) {
		while (dimension--) {
			Uint16 offset;

			offset = baseOffset + dimension * elementSize;
			flmem__verifyOffset(offset);
		}
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	flmem__verifyOffset
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Verify that the register with the supplied offset is included
*				in the tag table.
*
*	\param		offset		The RAM image offset of the register.
*	
*	\return		-
*
*	\details	
*
*	\note
*
*******************************************************************************/

PRIVATE void flmem__verifyOffset(
	Uint16					offset
) {
	Uint8					found;
	Uint16					tagIdx;
	Uint16					tagIdx2;

	/* Find register in flashtag table */
	found = flmem__binarySearchOffsetIndexByOffset(
		&flmem__instance,
		offset,
		&tagIdx
	);

	/*
	 *	If the code stops here then there is a non-volatile
	 *	register missing from the tag table sorted in offset
	 *	order.
	 *	
	 *	If the tag tables have been automatically generated then run
	 *	PodComposer again to update the tables.
	 *
	 *	If the tables have been manually constructed:
	 *	Look at the flmem_offsetTagMap[] array in the 
	 *	debugger and find the spot where the register should
	 *	be. Other registers close to the spot should give a clue for finding the
	 *	missing register. The offset variable will hold the offset
	 *	of the register that is missing from the table.
	 *
	 *	The offset hold the ram image offset. The ram image is a global variable
	 *	named "sys_imageRAM".
	 */

	if (found == 0) {
		deb_assert(FALSE);
		FOREVER {
			/* This loop is to prevent the program from
			 * running in RELEASE build.
			 */
		}
	}

	if (found) {
		found = flmem__binarySearchMemoryIndexByTag(
			&flmem__instance,
			flmem__instance.pInit->flashtagsByOffsetOrder[
				tagIdx
			].tag,
			&tagIdx2
		);
	}

	/*
	 *	If the code stops here then there is a non-volatile
	 *	register missing from the tag table sorted 
	 *	in tag order.
	 *	
	 *	The missing tag can be read from:
	 *	flmem__instance.pInit->flashtagsByOffsetOrder[tagIdx].tag
	 */

	if (found == 0) {
		deb_assert(FALSE);
		FOREVER {
			/* This loop is to prevent the program from
			 * running in RELEASE build.
			 */
		}
	}
}
