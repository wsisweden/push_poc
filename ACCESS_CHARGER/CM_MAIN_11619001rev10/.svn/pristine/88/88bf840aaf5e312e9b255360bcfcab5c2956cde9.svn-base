/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPA_UTIL.C
*
*	\ingroup	SPA
*
*	\brief		
*
*	\details		
*
*	\par		Module name:
*				SPA_UTIL.C
*
*	\par		Identification:
*				Utility function for SPA.
*
*	\note		
*
*	\version	30-08-07 / TJK
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "reg.h"

#include "mem.h"
#include "deb.h"
#include "util.h"

#include "spa.h"
#include "local.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

//SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spa__startFrame
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Prepares SPA frame by adding start characters and SPA slave
*				address.
*
*	\param		pInst 	ptr to SPA instance
*	\param		pFrm 	ptr to SPA output frame
*
*	\return		Uint8, SPA return code.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC Uint8 spa__startFrame(
	spa__Inst *				pInst,
	spa__FrmBuf *			pFrm
) {
	/*	
	 *  Check there is enough room in the buffer
	 */

	if (pFrm->nFree < 2)
	{
		return SPA_ERROR;
	}

	spa__appendFrame(pFrm, SPA_CHAR_LF); /* LF */
	spa__appendFrame(pFrm, '<');

	pFrm->pWritePos = (Uint8 *)util_u16toa(
		pInst->nAddress, 
		(char *)pFrm->pWritePos, 
		(Uint8)(pFrm->nFree > 255 ? 255 : pFrm->nFree),
		(Uint8)10
	);

	pFrm->nFree = pFrm->nSize - (Uint16) (pFrm->pWritePos - pFrm->pData);

	return SPA_OK;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spa__endFrame
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Ends SPA message frame by adding CRC and end characters.
*
*	\param		pInst 	ptr to SPA instance
*	\param		pFrm 	ptr to SPA output frame
*
*	\return		Uint8, SPA return code.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC Uint8 spa__endFrame(
	spa__Inst *				pInst,
	spa__FrmBuf *			pFrm
) {
	Uint16					nCrc = 0;
	Uint8 *					pPos = pFrm->pData + 1; /* Skip LF */

	/*	
	 *  Check there is enough room in the buffer
	 */

	if (pFrm->nFree < 5)
	{
		return SPA_ERROR;
	}

	/*	
	 *  Checksum
	 */
	while (pPos != pFrm->pWritePos) 
	{
		nCrc ^= *pPos++;
	}

	spa__appendFrame(pFrm, spa__toHex(nCrc >> 4));
	spa__appendFrame(pFrm, spa__toHex(nCrc & 0x0F));

	/*	
	 *  Ending CR LF
	 */
	spa__appendFrame(pFrm, SPA_CHAR_CR);	
	spa__appendFrame(pFrm, SPA_CHAR_LF);	

	return SPA_OK;
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spa__writeNack
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Adds NACK into to a output frame. Assumes frame has been
*				prepared with spa__startFrame.
*
*	\param		pInst 	ptr to SPA instance
*	\param		nNack 	NACK code to write
*	\param		pFrm 	ptr to SPA output frame
*
*	\return		Uint8, SPA return code.
*
*	\details	
*
*	\note	
*
*******************************************************************************/

PUBLIC Uint8 spa__writeNack(
	spa__Inst *				pInst,
	Uint8					nNack,
	spa__FrmBuf *			pFrm
) {
	/*	
	 *  Check there is enough room in the buffer
	 */
	if (pFrm->nFree < 4)
	{
		return SPA_ERROR;
	}

	spa__appendFrame(pFrm, 'N');
	spa__appendFrame(pFrm, ':');
	spa__appendFrame(pFrm, (nNack - SPA_NACK0) + '0');
	spa__appendFrame(pFrm, ':');	
	
	return SPA_OK;
}
