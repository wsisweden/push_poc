/*****************************************************************************
 *
 * LPC2378 System Memory Map
 * -------------------------
 * Compiler         : GNU ARM-ELF
 * Processor        : LPC2378
 *
 * ********************************************************************
 *
 *  Flash
 *   startup
 *    .startup   Vector table and startup code
 *   prog
 *    .text
 *    .rodata
 *    .rodata*
 *    .glue_7
 *    .glue_7t
 *               __end_of_text_
 *     .         __data_beg_src__
 *     .data
 *
 *  Ram
 *   .data
 *     __data_beg__
 *     .data
 *     __data_end__
 *   .bss
 *     __bss_beg__
 *     .bss
 *     __bss_end__
 *     _bss_end__
 *     __end__
 *     _end
 *     end
 *
 *  Ramstack
 *   stacklayout
 *    __stack_svc_bottom__
 *    __stack_irq_bottom__  __stack_svc_top__  
 *    __stack_fiq_bottom__  __stack_irq_top__
 *    __stack_und_bottom__  __stack_fiq_top__
 *    __stack_iap_bottom__  __stack_und_top__
 *
 *  USBRam
 *   usbramlayout
 *    __usbram_start__
 *    __usbram_size__
 *
 *  EthRam
 *   ethramlayout
 *    __ethram_start__
 *    __ethram_size__
 *
 ********************************************************************
 *
 *        +----------------------------------+  
 * 4.0 GB |                                  | 0xFFFF FFFF
 *        |  AHB Peripherals                 |
 *        |                                  | 
 * 3.75GB |----------------------------------|- 0xF000 0000 
 *        |                                  | 
 *        |  APB Peripherals                 |
 * 3.5 GB |                                  | 
 *        |----------------------------------|- 0xE000 0000
 *        |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |XXXXXXXXXXXX RESERVED XXXXXXXXXXXX|
 * 2.0 GB |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |----------------------------------|- 0x8000 0000
 *        |                                  | 
 *        |  Boot ROM and boot FLASH (remap) |  8192 bytes (0x2000) ??
 *        |                                  | 
 *        |----------------------------------|- 0x7FFE DFFF ??
 *        |XXXXXXXXXXXX RESERVED XXXXXXXXXXXX|
 *        |----------------------------------|- 
 *        |                                  | 0x7FE0 4FFF
 *        |  Ethernet RAM                    |   16384 bytes (0x4000)
 *        |                                  | 
 *        |----------------------------------|- 0x7FE0 0000 
 *        |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |XXXXXXXXXXXX RESERVED XXXXXXXXXXXX|
 *        |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |----------------------------------|- 0x7FD0 2000 
 *        |                                  | 
 *        |  USB RAM                         |    8192 bytes (0x2000)
 *        |                                  | 
 *        |----------------------------------|- 0x7FD0 0000 
 *        |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |XXXXXXXXXXXX RESERVED XXXXXXXXXXXX|
 *        |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |----------------------------------|- 0x4000 8000  
 *        |            |                     | 
 *        |            | Reserved for IAP    |     
 *        |            | 128 bytes (0x80)    | 
 *        |            |---------------------|- 0x4000 7F80  
 *        |            | UND Stack           |     
 *        |            | 128 bytes (0x80)    | 
 *        |  Internal  |---------------------|- 0x4000 7F00  
 *        |    RAM     | FIQ Stack           |     
 *        |    32KB    | 128 bytes (0x80)    | 
 *        |            |---------------------|- 0x4000 7E80  
 *        |            | IRQ Stack           |    
 *        |            | 1024 bytes (0x400)  | 
 *        |            |---------------------|- 0x4000 7A80  
 *        |            | SVC Stack           |    
 *        |            | 1024 bytes (0x400)  | 
 *        |            |---------------------|- 0x4000 7680
 *        |            |                     |
 *        |            |                     |
 *        |            | Variables RW and ZI |   
 *        |            | 30336 bytes (0x7680)|
 * 1.0 GB |            |                     | 
 *        |----------------------------------|- 0x4000 0000  
 *        |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |XXXXXXXXXXXX RESERVED XXXXXXXXXXXX| 
 *        |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX| 
 *        |----------------------------------|- 0x0008 0000
 *        |                                  | 
 *        |                                  |
 *        |  Flash                           |   
 *        |  512KB (0x80000)                 |   
 * 0.0 GB |                                  | 
 *        +----------------------------------+- 0x00000000
 *
 *  UND Stack is used by HaltUndef, HaltSWI, HaltPabort and HaldAabort
 *
 ********************************************************************/

/*
 * Defines memory layout
 */

MEMORY
{
  flash    (rx) : ORIGIN = 0x00000000, LENGTH = 512K
  ram      (rw) : ORIGIN = 0x40000000, LENGTH = 30336
  ramstack (rw) : ORIGIN = 0x40007680, LENGTH = 2432
  usbram   (rw) : ORIGIN = 0x7FD00000, LENGTH = 8K
  ethram   (rw) : ORIGIN = 0x7FE00000, LENGTH = 16K
}

/* 
 * Define entry point, found in startarm.s
 */
ENTRY(_reset_handler)

/*
 * Sections
 */
SECTIONS
{
  /* Place memory counter at address 0 */
  . = 0;

  /* Place startup code at start of flash */
  startup : 
  { 
    *(.startup)
  } > flash =0

  . = ALIGN(4);

  /* Place program code */
  prog :
  {
    *(.text)
    *(.rodata)
    *(.rodata*)
    *(glue_7)
    *(glue_7t)
  } > flash =0

  . = ALIGN(4);

  __end_of_text__  = .;
  __data_beg_src__ = .;
  PROVIDE(etext = .);

  /* Place initialized data */
  .data : AT(__data_beg_src__)
  {
    __data_beg__     = .;
    *(.data)
  } > ram

  . = ALIGN(4);
  __data_end__     = .;
  PROVIDE(edata = .);

  /* Place zero initialized data */
  .bss :
  {
    __bss_beg__ = .;
    *(.bss)
  } >ram


  /* Align here to ensure that the .bss section occupies space up to
     _end. Align after .bss to ensure correct alignment even if the
     .bss section disappears because there are no input sections. */
  . = ALIGN(32 / 8);

  . = ALIGN(32 / 8);

  /* Provide end markers */
  _end        = .;
  _bss_end__  = .;
  __bss_end__ = .;
  __end__     = .;
  PROVIDE(end = .);

  stacklayout :
  {
    __stack_scv_bottom__ = .;
    . = . + 0x0400;
    __stack_scv_top__    = .;
    __stack_irq_bottom__ = .;
    . = . + 0x0400;
    __stack_irq_top__    = .;
    __stack_fiq_bottom__ = .;
    . = . + 0x0080;
    __stack_fiq_top__    = .;
    __stack_und_bottom__ = .;
    . = . + 0x0080;
    __stack_und_top__    = .;
    __stack_iap_bottom__ = .;
  } >ramstack

  usbramlayout :
  {
    __usbram_start__ = .;
    __usbram_end__   = 0x2000;
  } >usbram

  ethramlayout :
  {
    __ethram_start__ = .;
    __ethram_end__   = 0x4000;
  } > ethram

  /* Stabs debugging sections.  */
  .stab          0 : { *(.stab) }
  .stabstr       0 : { *(.stabstr) }
  .stab.excl     0 : { *(.stab.excl) }
  .stab.exclstr  0 : { *(.stab.exclstr) }
  .stab.index    0 : { *(.stab.index) }
  .stab.indexstr 0 : { *(.stab.indexstr) }
  .comment       0 : { *(.comment) }
  /* DWARF debug sections.
     Symbols in the DWARF debugging sections are relative to the beginning
     of the section so we begin them at 0.  */
  /* DWARF 1 */
  .debug          0 : { *(.debug) }
  .line           0 : { *(.line) }
  /* GNU DWARF 1 extensions */
  .debug_srcinfo  0 : { *(.debug_srcinfo) }
  .debug_sfnames  0 : { *(.debug_sfnames) }
  /* DWARF 1.1 and DWARF 2 */
  .debug_aranges  0 : { *(.debug_aranges) }
  .debug_pubnames 0 : { *(.debug_pubnames) }
  /* DWARF 2 */
  .debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
  .debug_abbrev   0 : { *(.debug_abbrev) }
  .debug_line     0 : { *(.debug_line) }
  .debug_frame    0 : { *(.debug_frame) }
  .debug_str      0 : { *(.debug_str) }
  .debug_loc      0 : { *(.debug_loc) }
  .debug_macinfo  0 : { *(.debug_macinfo) }
  /* SGI/MIPS DWARF 2 extensions */
  .debug_weaknames 0 : { *(.debug_weaknames) }
  .debug_funcnames 0 : { *(.debug_funcnames) }
  .debug_typenames 0 : { *(.debug_typenames) }
  .debug_varnames  0 : { *(.debug_varnames) }
}
