/*
$Revision: 1.3 $ 
$Date: 2008/07/11 11:41:30 $
$Author: jj $
$Source: Z:\Software\CVSrep\bart\SRC\REGU/REGU/local.H,v $
*/
// Revision: 1.1 Titolaite Release 20080128

/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/
/*************************************************************************
 History:
Rev      Date      Sign  Description
1.1      20080410  bsi   First CVS-version.
1.2      20080523 jj/   redefinition of function regu__PIcontroller
						 Bsi   #001 - regu__PIcontroller changed to void instead of Uint32
-- This version -- jj    re-scale of pwm
						       #002 - pwm max value/time is increased by 4
								 Battery disconnection detection
								 #003 - Added new status register 
***************************************************************************/
/*******************************************************************************
;
;	H E A D E R   D E S C R I P T I O N
;
;-------------------------------------------------------------------------------
;
;	Name:		Local.h
;
;	Purpose:	
;
;	Notice:		
;
;******************************************************************************/

#ifndef REGU_LOCAL_H_INCLUDED
#define REGU_LOCAL_H_INCLUDED

/******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;----------------------------------------------------------------------------*/

#include "global.h"
#include "init.h"
#include "ControlLoop/Regulator/regu_StatusAndError.h"

/******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;----------------------------------------------------------------------------*/

#define REGU_FLG_TESTMODE			(1<<3)
#define REGU_FLG_DOWN				(1<<4)	/**< MEAS is down				*/

//States for in/out-side specification state machine
#define WITHIN_SPEC_STATE		0x01U
#define OUTSIDE_SPEC_STATE		0x02U
#define WAIT_SPEC_STATE			0x03U
#define SPEC_STATE_DELAY_TIME  (10U*100U)   //10 seconds

/******************************************************************************
;
;	MACROS
;
;----------------------------------------------------------------------------*/


/******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;----------------------------------------------------------------------------*/

typedef struct {						/*''''''''''''''''''''''''''''' RAM	*/
	osa_CoTaskType			coTask;		/* Task								*/
	regu_Init const_P * 	pInit;		/**< Initialization data			*/
	sys_FBInstId 			instId;		/**< FB instance ide.				*/
	Boolean					test;		/**< Internal test flag				*/
	Uint8					flags;		/**< State flags					*/
	int						NewSet;		/** New set values should be used	*/
	int						ReguWatchDog[ReguWatchDog_enum_size];
	int						Restarts;	/** Number of restarts last minute	*/
	int						RestartDelay;	/** Delay ENGINE ON signal afetr restart */
	Uint8					RegulatorErrorCnt;	/** Counter for number of errors during one cycle */
	Uint16 					triggers;
	ChargerStatusFields_type Status;
	Uint32 					outsideSpecState;
	Uint32                  outsideSpecDelayCnt;
} regu__Inst;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/******************************************************************************
;
;	FUNCTIONS
;
;----------------------------------------------------------------------------*/

/******************************************************************************
;
;	DATA
;
;----------------------------------------------------------------------------*/


/*****************************************************************************/

#endif
