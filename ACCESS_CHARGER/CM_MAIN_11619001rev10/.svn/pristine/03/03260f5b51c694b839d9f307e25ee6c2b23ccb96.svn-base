/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		TSTAMP.H
*
*	\ingroup	TSTAMP
*
*	\brief		Public header of the TSTAMP FB.
*
*	\details
*
*	\note
*
*	\version	14-08-2008 / Ari Suomi
*
*	\version	14-01-2009 / Ari Suomi / Added the macro tstamp_getStampCurr.
*
*	\version	01-01-2010 / TJK / Added tstamp_msGet() and tstamp_msPut().
*
*******************************************************************************/

#ifndef TSTAMP_H_INCLUDED
#define	TSTAMP_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

#if (TARGET_SELECTED & TARGET_AVR) == TARGET_AVR

#define TSTAMP__CONV_MAX	0x10000
#define	TSTAMP__CONV_SHIFT	0

#elif (TARGET_SELECTED & (TARGET_CM0 | TARGET_CM3 | TARGET_CM4 | TARGET_ARM7)) != 0

#define TSTAMP__CONV_MAX	0x100000000
#define	TSTAMP__CONV_SHIFT	16

#elif (TARGET_SELECTED & TARGET_MM7) != 0

#define TSTAMP__CONV_MAX	0x100000000
#define	TSTAMP__CONV_SHIFT	16

#elif (TARGET_SELECTED & TARGET_SM4) != 0

#define TSTAMP__CONV_MAX	0x100000000
#define	TSTAMP__CONV_SHIFT	16

#elif (TARGET_SELECTED & TARGET_S4F4) != 0

#define TSTAMP__CONV_MAX	0x100000000
#define	TSTAMP__CONV_SHIFT	16

#elif (TARGET_SELECTED & TARGET_S3F2) != 0

#define TSTAMP__CONV_MAX	0x100000000
#define	TSTAMP__CONV_SHIFT	16

#elif (TARGET_SELECTED & TARGET_DOXY) == TARGET_DOXY

/**
 * This define is used when converting the HW timer counter value to 1/65535
 * milliseconds. The value is selected for each target so that the full range of
 * an integer is used in the conversion.
 */

#define TSTAMP__CONV_MAX

/**
 * This define is used when converting the HW timer counter value to 1/65535
 * milliseconds. The value is selected for each target so that the full range of
 * an integer is used in the conversion.
 */

#define	TSTAMP__CONV_SHIFT

#elif (TARGET_SELECTED & TARGET_W32) == TARGET_W32

#define TSTAMP__CONV_MAX	0x100000000
#define	TSTAMP__CONV_SHIFT	16

#else
#error TSTAMP is not implemented for that target yet

#endif

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/**
 * 	\brief		Extracts the milliseconds count of a timestamp.
 *
 *	\param		pStamp_ 	Pointer to the timestamp of type either
 *							tstamp_Stamp or tstamp_StampEx.
 *
 *	\return		The time in milliseconds. Uint16.
 *
 * 	\details	Should be used along with tstamp_msPut() only.
 */

#define tstamp_msGet(pStamp_) ((pStamp_)->msStamp)

/**
 * 	\brief		Sets the milliseconds count of a timestamp.
 *
 *	\param		pStamp_ 	Pointer to the timestamp of type either
 *							tstamp_Stamp or tstamp_StampEx.
 *	\param		msCount_	The time in milliseconds.
 *
 * 	\details	Should be used along with tstamp_msGet() only.
 */

#define tstamp_msPut(pStamp_,msCount_) (pStamp_)->msStamp=(msCount_)

/**
 * 	\brief		Converts HW timer counter value to 1/65536 milliseconds.
 *
 * 	\param		value_	The HW timer counter value to be converted.
 * 	\param		cntMax_	The counter value that is equal to 1ms. This value
 * 						will never be on the counter as the counter is reseted
 * 						when this value is reached.
 *
 *	\return		The converted time in 1/65536 milliseconds.
 *
 * 	\details	This macro is meant to be used in the project_getStampEx()
 * 				function.
 */

#define tstamp_convert(value_, cntMax_) 								\
	(																	\
		(TSTAMP__CONV_MAX / (cntMax_)) * (value_)						\
	) >> TSTAMP__CONV_SHIFT

/**
 * 	\brief		Converts the difference of two extended timestamps to
 * 				microseconds.
 *
 * 	\param		exDiff_		The difference of two extended timestamps as Uint32.
 *
 *	\return		The time converted to microseconds. Uint32.
 *
 *	\details
 *
 *	\note
 *
 *	\internal	((exDiff * 125) >> 13) does the same but will overflow on big
 *				values.
 */

#define tstamp_diffEx2us(exDiff_) \
	((((exDiff_) >> 16) * 125) << 3) + ((((exDiff_) & 0xFFFF) * 125) >> 13);

/**
 *	\brief		This macro is used to increment the millisecond counter.
 *
 *	\param		v_		The time elapsed between each execution in milliseconds.
 *
 *	\details	The macro code should executed each millisecond if possible.
 *				There is usually a 1ms timer isr in project.c where the macro
 *				can be used. The macro code must be executed on some multiple of
 *				1ms!
 *
 *	\note		If the macro code cannot be executed each millisecond then the
 *				timestamp accuracy will not be 1ms!
 */

#define tstamp_incIsr(v_)	tstamp__msCounter += (v_)

/**
 * 	\brief		Returns the value of the millisecond counter.
 *
 *	\return		The value of the millisecond counter.
 *
 * 	\details	This macro is meant to be used in the project_getStampEx()
 * 				function with interrupts disabled.
 */

#define tstamp_getMsCount()	tstamp__msCounter

/**
 *
 *	\brief		Get standard timestamp.
 *
 *	\param		pStamp_ 	Pointer to the timestamp where the time will be
 *							copied. Has to be of the type tstamp_Stamp.
 *
 *	\return		-
 *
 *	\details	The macro will get a standard 16-bit timestamp with 1ms
 *				accuracy.
 *
 *	\note
 *
 */

#define tstamp_getStamp(pStamp_) 					\
	atomicintr(										\
		(pStamp_)->msStamp = tstamp__msCounter;		\
	);

/**
 *
 *	\brief		Get the difference of the supplied timestamps in milliseconds.
 *
 *	\param		pStamp1_	Pointer to the first timestamp.
 *	\param		pStamp2_	Pointer to the second timestamp.
 *
 *	\return		The difference in milliseconds.
 *
 *	\details	This macro can be used with any type of timestamp.
 */

#define tstamp_getDiff(pStamp1_,pStamp2_) \
	((pStamp2_)->msStamp - (pStamp1_)->msStamp)

/**
 *
 *	\brief		Add the supplied amount of milliseconds to the timestamp.
 *
 *	\param		pStamp_		Pointer to the timestamp.
 *	\param		ms_			Amount of milliseconds to be added to the timestamp.
 *
 *	\return		-
 *
 *	\details	The added time cannot be larger than 65535ms.
 */
 
#define tstamp_addTime(pStamp_, ms_)		(pStamp_)->msStamp + (ms_)

/**
 *
 *	\brief		Get the difference of the supplied timestamp to current time.
 *
 *	\param		pStamp_		Pointer to the timestamp.
 *	\param		pDiff_		Pointer to Uint16 variable where the difference will
 *							be stored. The difference will be in milliseconds.
 *
 *	\return		-
 *
 *	\details	This macro can be used with any type of timestamp.
 */

#define tstamp_getDiffCurr(pStamp_, pDiff_) 				\
	atomicintr(												\
		*(pDiff_) = tstamp__msCounter - (pStamp_)->msStamp;	\
	);

/**
 * 	\brief		Get extended timestamp.
 *
 * 	\param		pStamp_		Pointer to location where the timestamp will be
 * 							stored. Must be of type tstamp_StampEx.
 *
 * 	\return		-
 *
 * 	\details	project_getStampEx() function has to be implemented in project.c
 * 				if this macro is used.
 */

#define tstamp_getStampEx(pStamp_)		project_getStampEx(pStamp_)

/**
 *
 *	\brief		Calculate the difference of the supplied extended timestamps.
 *
 *	\param		pStampEx1_		Pointer to first extended stamp.
 *	\param		pStampEx2_		Pointer to second extended stamp.
 *
 *	\return		The difference between the supplied timestamps. The unit used in
 *				the result is 1/65536 milliseconds. Uint32 is required to store
 *				the returned value.
 *
 *	\details	The tstamp_diffEx2us macro can be used to convert the returned
 *				value to microseconds.
 *
 *	\note
 *
 */

#define tstamp_getDiffEx(pStampEx1_,pStampEx2_) 							\
	(((Uint32) ((pStampEx2_)->msStamp - (pStampEx1_)->msStamp)) << 16) +	\
	(pStampEx2_)->exStamp - (pStampEx1_)->exStamp;

/**
 *
 *	\brief		Get elapsed time between the supplied extended timestamp and 
 *				currint time.
 *
 *	\param		pStampEx_	Pointer to an extended timestamp.
 *	\param		pDiff_		Pointer to Uint32 variable where the elapsed time
 *							will be stored. The time will be in microseconds.
 *
 *	\return		-
 *
 *	\details	This macro can only be used with extended timestamps.
 */

#define tstamp_getDiffCurrEx(pStampEx_, pDiff_) {							\
	tstamp_StampEx 	tstamp__stamp2Ex;										\
	Uint32 			tstamp__diffEx;											\
	tstamp_getStampEx(&tstamp__stamp2Ex);									\
	tstamp__diffEx = tstamp_getDiffEx(pStamp_, &tstamp__stamp2Ex);			\
	*(pDiff_) = tstamp_diffEx2us(tstamp__diffEx);							\
}

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Standard timestamp with 1ms accuracy. Capable of calculating elapsed times up
 * to 65535ms. The fields should not be used directly, they are read and
 * modified only by services provided by TSTAMP.
 */

typedef	struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16					msStamp;	/**< Standard timestamp	(ms)		*/
} tstamp_Stamp;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Extended timestamp. Capable of calculating elapsed times up to 65536ms.
 *
 * The accuracy of the timestamp depends on the low level implementation.
 * However the accuracy is at least 1 millisecond and at most 1/65536
 * milliseconds. Usually the accuracy is more than 1ms but not quite 1/65536 ms.
 *
 * The fields should not be used directly, they are read and modified only by
 * services provided by TSTAMP.
 */

typedef struct {						/*''''''''''''''''''''''''''''' RAM */
	Uint16					msStamp;	/**< Standard timestamp	(ms)		*/
	Uint16					exStamp;	/**< Extended timestamp				*/
} tstamp_StampEx;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/**
 *	\brief		Fetch extended timestamp.
 *
 *	\details	This function should be implemented in project.c if extended
 *				timestamps are needed in the project. The function should store
 *				the extended timestamp to the provided timestamp pointer.
 *
 *				The function could be implemented like this:
 *	\code
 *				PUBLIC void project_getStampEx(
 *					tstamp_StampEx *		pStampEx
 *				) {
 *					atomicintr(
 *						pStampEx->msStamp = tstamp_getMsCount();
 *						pStampEx->exStamp = T0TC;
 *						if (T0IR & (1 << 0))
 *						{
 *							pStampEx->msStamp++;
 *							pStampEx->exStamp = T0TC;
 *						}
 *					);
 *					pStampEx->exStamp =
 *						tstamp_convert(pStampEx->exStamp, PRJ_TSTAMP_CNTMAX);
 *				}
 *	\endcode
 *				The HW timer counter is constantly changing and may be reseted
 *				any time. In the example the timer interrupt flag is checked
 *				to see if a reset has occurred while the timestamp was read.
 *
 *				The tstamp_convert() macro can be used to convert the timer
 *				value to 1/65536 milliseconds. PRJ_TSTAMP_CNTMAX define in the
 *				example is set to the HW timer counter max value.
 *
 *				The function may be called from an ISR. The atomicintr() macro
 *				makes this possible.
 */

extern void 				project_getStampEx(tstamp_StampEx *);

/*******************************************************//** \cond pub_decl *//*
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern Uint16				tstamp__msCounter;

/***************************************************************//** \endcond */

#endif
