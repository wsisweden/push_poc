/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		REG_U16.C
*
*	\ingroup	REG
*
*	\brief		
*
*	\details		
*
*	\par		Module name:
*				Register file i/o
*
*	\par		Files:
*				reg_u16.c
*
*	\note		
*
*	\version	dd-mm-yy / author
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "reg.h"
#include "ptrs.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg__cpyUint16
*
*--------------------------------------------------------------------------*//**
*
*	\brief		'Copy' a register value of type Uint16.
*
*	\param		pTarget Ptr to the storage where the value is copied to.
*	\param		pSource Ptr to the value (in RAM) to copy.
*
*	\return		Number of BYTEs copied.
*
*	\details	
*
*	\note		Used via the 'reg_typeImplem' table only. Called with interrupts
*				disabled!
*
*******************************************************************************/

PUBLIC Uint8 reg__cpyUint16(
	ptrs_RegData_p				pTarget,
	ptrs_RegData_cDp			pSource
) {
	Uint16 source = *(ptrs_RegData_u16cDp) pSource;
	Uint16 target = *(ptrs_RegData_u16p) pTarget;

	*(ptrs_RegData_u16p) pTarget = source;
	
	return(source == target	? 0	: sizeof(Uint16));
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	reg__cmpUint16
*
*--------------------------------------------------------------------------*//**
*
*	\brief		'Compare' two register values of type Uint16.
*
*	\param		pVal1 	Ptr to the first value (in RAM) to compare.
*	\param		pVal2 	Ptr to the second value (in program memory) to compare.
*
*	\return
*	<pre>
*				One of the following three values:
*				 -1 if the first value < the second value,
*				  0 if the two values are identical, or
*				 +1 if the first value > the second value.
*	</pre>
*
*	\details	
*
*	\note		Used via the 'reg_typeImplem' table only.
*
*******************************************************************************/

PUBLIC Int8 reg__cmpUint16(
	ptrs_RegData_cDp		pVal1,
	void const_P *			pVal2
) {
	Int32 diff;

	diff = (Int32) *(ptrs_RegData_u16cDp) pVal1 - *(Uint16 const_P *) pVal2;
	return(
		diff < 0
			? (Int8) -1
			: diff > 0 ? (Int8) +1 : (Int8) 0
	);
}
