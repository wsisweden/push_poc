/**************************************************************************
MODULE:    MCO
CONTAINS:  Main MicroCANopen implementation
COPYRIGHT: Embedded Systems Academy, Inc. 2002-2009.
           All rights reserved. www.microcanopen.com
DISCLAIM:  Read and understand our disclaimer before using this code!
           www.esacademy.com/disclaim.htm
           This software was written in accordance to the guidelines at
           www.esacademy.com/software/softwarestyleguide.pdf
LICENSE:   This file may be freely distributed.
VERSION:   5.01, ESA 24-SEP-09
           $LastChangedDate: 2009-09-24 21:22:11 -0300 (Thu, 24 Sep 2009) $
           $LastChangedRevision: 1447 $
***************************************************************************/ 

#ifndef _MCO_H
#define _MCO_H

// The following configuration files need to be supplied by the application
#include "../MCO_DS401_LPC1768/nodecfg.h"
#include "../MCO_DS401_LPC1768/procimg.h"


/**************************************************************************
CANopen NMT (Network Management) Master Msg and Slave States
**************************************************************************/
#define NMTMSG_OP 1
#define NMTMSG_STOP 2
#define NMTMSG_PREOP 128
#define NMTMSG_RESETAPP 129
#define NMTMSG_RESETCOM 130

#define NMTSTATE_BOOT 0
#define NMTSTATE_STOP 4
#define NMTSTATE_OP 5
#define NMTSTATE_PREOP 127
#define NMTSTATE_AUTO_ID 0xF1


/**************************************************************************
Error codes used when calling MCOUSER_FatalError
**************************************************************************/
#define ERR_WARN     0x4000 // Warning only, continue execution
#define ERROFL_EMCY  0x4810 // Transmit buffer overflow, TPDO message lost
#define ERROFL_PDO   0x4820 // Transmit buffer overflow, TPDO message lost
#define ERROFL_SDO   0x4830 // Transmit buffer overflow, SDO message lost
#define ERROFL_HBT   0x4840 // Transmit buffer overflow, Heartbeat message lost
#define ERR_FATAL    0x8000 // Fatal error, should abort/reset
#define ERRFT_INIT   0x8010 // MCO Init failed
#define ERRFT_RXFLTN 0x8021 // Out of CAN receive filters, NMT
#define ERRFT_RXFLTP 0x8022 // Out of CAN receive filters, PDO
#define ERRFT_RXFLTS 0x8023 // Out of CAN receive filters, SDO
#define ERRFT_IPP    0x8031 // Init PDO Parameters out of range
#define ERRFT_PIR    0x8032 // Process Image access out of range
#define ERRFT_TPDOR  0x8041 // Out of TPDOs
#define ERRFT_RPDOR  0x8042 // Out of RPDOs


/**************************************************************************
DEFINES FOR ACCESS TYPE TO OD ENRIES
Readable, Writable, Call-Back
**************************************************************************/
#define ODRD 0x10
#define ODWR 0x20
#define CALB 0x08


/**************************************************************************
MACROS FOR OBJECT DICTIONARY ENTRIES
**************************************************************************/
#define GETBYTE(val,pos) ((val >> pos) & 0xFF)
#define GETBYTES16(val) GETBYTE(val, 0), GETBYTE(val, 8)
#define GETBYTES32(val) GETBYTE(val, 0), GETBYTE(val, 8), GETBYTE(val,16), GETBYTE(val,24)
#define SDOREPLY(index,sub,len,val) 0x43 | ((4-len)<<2), GETBYTES16(index), sub, GETBYTES32(val)
#define SDOREPLY4(index,sub,len,d1,d2,d3,d4) 0x43 | ((4-len)<<2), GETBYTES16(index), sub, d1, d2, d3, d4
#define ODENTRY(index,sub,len,offset) {index, sub, len, offset}

#if USE_EXTENDED_SDO
#define ODGENTRYC(index,sub,acc,len,dat) { \
  (UNSIGNED8)(index & 0x00FF), (UNSIGNED8)(index >> 8), (UNSIGNED8)sub, (UNSIGNED8)acc, \
  (UNSIGNED16)len,  (UNSIGNED8 *) dat }
#define ODGENTRYP(index,sub,acc,len,dat) { \
  (UNSIGNED8)(index & 0x00FF), (UNSIGNED8)(index >> 8), (UNSIGNED8)sub, (UNSIGNED8)acc, \
  (UNSIGNED16)len,  (UNSIGNED8 *) &gProcImg[dat] }
#endif // USE_EXTENDED_SDO


/**************************************************************************
MACROS FOR PROCESS IMAGE ACCESS 8bit, 16bit and 32bit access
**************************************************************************/
#define PI8ACC(offset) (gProcImg[offset])
#define PI16ACC(offset) (*((UNSIGNED16 *) &(gProcImg[offset])))
#define PI32ACC(offset) (*((UNSIGNED32 *) &(gProcImg[offset])))


/**************************************************************************
MACROS FOR ACCESS TO INTERNAL VARIABLES SOMETIMES NEEDED BY APPLICATION
**************************************************************************/
#define MY_NODE_ID (gMCOConfig.Node_ID)
#define MY_NMT_STATE (gMCOConfig.heartbeat_msg.BUF.BUF[0])


/**************************************************************************
MACROS FOR CAN-ID DEFINITION
**************************************************************************/
// CAN-ID used for NMT Master message
#define NMT_MASTER_ID 0

// CAN-ID for LSS reception (slave) or transmission (master)
#define LSS_MASTER_ID 2021

// CAN-ID for LSS transmission (slave) or reception (master)
#define LSS_SLAVE_ID 2020


/**************************************************************************
MACROS FOR CAN-ID USAGE
**************************************************************************/
#define IS_CANID_LSS_RESPONSE(canid) (canid == LSS_SLAVE_ID)

#if USE_CiA447

// SDO Transfers: Server = j, Client = i
// Request from client node i to server node j
// (0x240 + ((i-1) & 0xC) << 6) + (((i-1) & 0x03) << 4) + j-1) 
// Response from server node j to client node i
// (0x1C0 + ((i-1) & 0xC) << 6) + (((i-1) & 0x03) << 4) + j-1) 
#define IS_CAN_ID_EMERGENCY(canid) ((canid >= 0x081) && (canid <= 0x090))
#define IS_CAN_ID_HEARTBEAT(canid) ((canid >= 0x701) && (canid <= 0x710))
#define IS_CAN_ID_ISO_TP(canid)   ( \
       (canid==0x251) || (canid==0x262) || (canid==0x273) \
    || (canid==0x344) || (canid==0x355) || (canid==0x366) \
    || (canid==0x377) || (canid==0x448) || (canid==0x459) \
    || (canid==0x46A) || (canid==0x47B) || (canid==0x54C) \
    || (canid==0x55D) || (canid==0x56D) || (canid==0x56E) \
    || (canid==0x57F) \
  )
#define IS_CAN_ID_SDOREQUEST(canid) ( \
       (canid == 0x600+MY_NODE_ID) \
    || (    (canid > 0x240) \
         && (canid < 0x580) \
         && ((canid & 0x00CF) == (0x040 + MY_NODE_ID - 1)) \
       ) \
  )
#define IS_CAN_ID_SDORESPONSE(canid) ( \
       (canid > 0x1C0) \
    && (canid < 0x4FF) \
    && ((canid & 0x07F0) \
         == 0x1C0 + (((MY_NODE_ID - 1) & 0x0C) << 6) + (((MY_NODE_ID - 1) & 0x03) << 4) \
       ) \
  ) 
#define CAN_ID_SDORESPONSE_FROM_RXID(canid) ( canid - 0x80 )
#define CAN_ID_SDOREQUEST(client_nodeid, server_nodeid) ( \
    0x240 +  (((client_nodeid-1) & 0x0C) << 6) \
          +  (((client_nodeid-1) & 0x03) << 4) \
          +  server_nodeid-1 \
  )
#define CAN_ID_SDORESPONSE(client_nodeid, server_nodeid) ( \
    0x1C0 +  (((client_nodeid-1) & 0x0C) << 6) \
          +  (((client_nodeid-1) & 0x03) << 4) \
          + server_nodeid-1 \
  )
#define SDOSERVER(tx_canid) ( \
    (tx_canid == 0x580 + MY_NODE_ID) \
      ? (MY_NODE_ID-1) \
      : ((((tx_canid-0x100) >> 6) & 0x0C) + ((tx_canid >> 4) & 0x03)) \
  )

#else

#if USE_SDOMESH

#define IS_CAN_ID_EMERGENCY(canid) ((canid >= 0x081) && (canid <= 0x0FF))
#define IS_CAN_ID_HEARTBEAT(canid) ((canid >= 0x701) && (canid <= 0x77F))

#define IS_CAN_ID_SDOREQUEST(canid) ( \
       ((canid & 0x700) == 0x600) \
    && ((canid & 0x00F) == (MY_NODE_ID - 1)) \
  )
#define IS_CAN_ID_SDORESPONSE(canid) ( \
       (   (MY_NODE_ID < 9) \
        && ((canid & 0x700) == 0x500) \
        && ((canid & 0x0F0) == ((MY_NODE_ID + 7) << 4)) \
       ) \
    ||   \
       (   (MY_NODE_ID > 8) \
        && ((canid & 0x700) == 0x100) \
        && ((canid & 0x0F0) == ((MY_NODE_ID - 9) << 4)) \
       ) \
  ) 
#define CAN_ID_SDORESPONSE_FROM_RXID(canid) ( \
    (((canid & 0x0F0) >> 4) < 8) \
      ? ( 0x500 +  ((((canid & 0x0F0) >> 4) + 8) << 4) \
                +  (canid & 0xF) \
        ) \
      : ( 0x100 +  ((((canid & 0x0F0) >> 4) - 8) << 4) \
                +  (canid & 0xF) \
        ) \
  )
#define CAN_ID_SDOREQUEST(client_nodeid, server_nodeid) ( \
    0x600 +  (((client_nodeid-1) & 0xF) << 4) \
          +  ((server_nodeid-1) & 0xF) \
  )
#define CAN_ID_SDORESPONSE(client_nodeid, server_nodeid) ( \
    (client_nodeid < 9) \
      ? 0x500 +  ((((client_nodeid-1) & 0xF) + 8) << 4) \
              +  ((server_nodeid-1) & 0xF) \
      : 0x100 +  ((((client_nodeid-1) & 0xF) - 8) << 4) \
              +  ((server_nodeid-1) & 0xF) \
  )
#define SDOSERVER(tx_canid) ( \
    ((gTxSDO.ID & 0x700) == 0x500) \
      ? (((gTxSDO.ID & 0x0F0) >> 4) - 8) \
      : (((gTxSDO.ID & 0x0F0) >> 4) + 8) \
  )

#else

#define IS_CAN_ID_EMERGENCY(canid) ((canid >= 0x081) && (canid <= 0x0FF))
#define IS_CAN_ID_HEARTBEAT(canid) ((canid >= 0x701) && (canid <= 0x77F))
#define IS_CAN_ID_SDOREQUEST(canid) (canid == (unsigned)0x600+MY_NODE_ID)
#define IS_CAN_ID_SDORESPONSE(canid) (canid >= 0x581) && (canid <= 0x5FF)
#define CAN_ID_SDORESPONSE_FROM_RXID(canid) (canid - 0x80)
#define CAN_ID_SDOREQUEST(client_nodeid, server_nodeid)  ( 0x600 + server_nodeid )
#define CAN_ID_SDORESPONSE(client_nodeid, server_nodeid) ( 0x580 + server_nodeid )
#define SDOSERVER(tx_canid) 0

#endif

#endif


/**************************************************************************
SDO ABORT MESSAGES
**************************************************************************/
#define SDO_ABORT_TOGGLE          0x05030000UL
#define SDO_ABORT_UNKNOWN_COMMAND 0x05040001UL
#define SDO_ABORT_INVALID_SEQ     0x05040003UL
#define SDO_ABORT_CRC             0x05040004UL
#define SDO_ABORT_UNSUPPORTED     0x06010000UL
#define SDO_ABORT_WRITEONLY       0x06010001UL
#define SDO_ABORT_READONLY        0x06010002UL
#define SDO_ABORT_NOT_EXISTS      0x06020000UL
#define SDO_ABORT_PARAMETER       0x06040043UL
#define SDO_ABORT_TYPEMISMATCH    0x06070010UL
#define SDO_ABORT_DATATOBIG       0x06070012UL
#define SDO_ABORT_UNKNOWNSUB      0x06090011UL
#define SDO_ABORT_VALUE_RANGE     0x06090030UL
#define SDO_ABORT_GENERAL         0x08000000UL
#define SDO_ABORT_TRANSFER        0x08000020UL



/**************************************************************************
Status bits for function MCOHW_GetStatus
**************************************************************************/
#define HW_INIT  0x01
#define HW_CERR  0x02
#define HW_ERPA  0x04
#define HW_RXOR  0x08
#define HW_TXOR  0x10
#define HW_TXBSY 0x40
#define HW_BOFF  0x80


/**************************************************************************
Defines for LED control
**************************************************************************/
#if USE_LEDS
// LED Flash Patterns
// Flickering directly implemented in lss.c
#define LED_OFF 0
#define LED_ON 0xFF
#define LED_BLINK 0x7F
// Single, double, triple and quadruple flashes
#define LED_FLASH1 1
#define LED_FLASH2 2
#define LED_FLASH3 3
#define LED_FLASH4 4
#endif // USE_LEDS


/**************************************************************************
GLOBAL TYPE DEFINITIONS
**************************************************************************/

// Data structure for a single CAN message 
typedef struct
{
  COBID_TYPE ID;                 // Message Identifier
  union
  {
    UNSIGNED8 BUF[8];            // Data buffer
    UNSIGNED16 BUF16[4];         // Data buffer
    UNSIGNED32 BUF32[2];         // Data buffer
  } BUF;
  UNSIGNED8 LEN;                 // Data length (0-8) 
} CAN_MSG;

// This structure holds all node specific configuration
typedef struct
{
  CAN_MSG heartbeat_msg;         // Heartbeat message contents
  UNSIGNED16 Baudrate;           // Current Baud rate in kbit
  UNSIGNED16 heartbeat_time;     // Heartbeat time in ms
  UNSIGNED16 heartbeat_timestamp;// Timestamp of last heartbeat
  UNSIGNED16 last_fatal;         // Last Fatal Error code
#if USE_SYNC
  UNSIGNED16 SYNCid;             // CAN ID used for SYNC
#endif
#if USE_LEDS
  UNSIGNED16 LED_timestamp;      // LED control timestamp
  UNSIGNED8 LEDtoggle;           // Toggler for blinking or flickering pattern
  UNSIGNED8 LEDRun;              // Current pattern on run led
  UNSIGNED8 LEDcntR;             // Current flash counter on run led
  UNSIGNED8 LEDErr;              // Current pattern on error led
  UNSIGNED8 LEDcntE;             // Current flash counter on error led
#endif // USE_LEDS
  UNSIGNED8 Node_ID;             // Current Node ID (1-126)
  UNSIGNED8 error_code;          // Bits: 0=RxQueue 1=TxQueue 3=CAN 7=first_op
  UNSIGNED8 error_register;      // Error regiter for OD entry [1001,00]
#if USE_NODE_GUARDING
  UNSIGNED8 NGtoggle;            // Toggle value for node guarding
#endif
  UNSIGNED8 HWStatus;            // CAN HW status
} MCO_CONFIG;

// This structure holds all the TPDO configuration data for one TPDO
typedef struct 
{
  CAN_MSG CAN;                 // Current/last CAN message to be transmitted
  UNSIGNED16 offset;           // Offest to application data in process image
  UNSIGNED16 PDONr;            // PDO number (1-512)
#if USE_EVENT_TIME
  UNSIGNED16 event_time;       // Event timer in ms (0 for COS only operation)
  UNSIGNED16 event_timestamp;  // If event timer is used, this is the 
                               // timestamp for the next transmission
#endif
#if USE_INHIBIT_TIME
  UNSIGNED16 inhibit_time;     // Inhibit timer in ms (0 if COS not used)
  UNSIGNED16 inhibit_timestamp;// If inhibit timer is used, this is the 
                               // timestamp for the next transmission
  UNSIGNED8 inhibit_status;    // 0: Inhibit timer not started or expired
                               // 1: Inhibit timer started
                               // 2: Transmit msg waiting for expiration of inhibit
#endif
#if USE_SYNC
  UNSIGNED8 SYNCcnt;           // SYNC counter for counting SYNC signals
#endif
  UNSIGNED8 TType;			// Transmission Type 0-240, 252 means that the transmission of the PDO shall be related to the SYNC object.
							// 252,253 means that the PDO is only transmitted on remote transmission request.
  int		Trigger;
} TPDO_CONFIG;

// This structure holds all the RPDO configuration data for one RPDO
typedef struct 
{
#if USE_SYNC
  UNSIGNED8 BUF[8];  // SYNC data buffer 
#endif
  COBID_TYPE CANID;  // Message Identifier 
  UNSIGNED16 offset; // Pointer to destination of data 
  UNSIGNED16 PDONr;  // PDO number (1-512)
  UNSIGNED8 len;     // Data length (0-8) 
  UNSIGNED8 TType;   // Transmission Type
} RPDO_CONFIG;

// This structure holds all data for one process data entry in the OD
typedef struct 
{
  UNSIGNED16 idx;    // Index of OD entry
  UNSIGNED8 subidx;  // Subindex of OD entry 
  UNSIGNED8 len;     // Data length in bytes (1-4), plus bits ODRD, ODWR, RMAP/WMAP
  UNSIGNED16 offset; // Offset to process data in process image
} OD_PROCESS_DATA_ENTRY;

#if USE_EXTENDED_SDO
// This structure holds all data for generic entries into the OD
typedef struct 
{
  UNSIGNED8  idx_lo;              // Index of OD entry
  UNSIGNED8  idx_hi;              // Index of OD entry
  UNSIGNED8  subidx;              // Subindex of OD entry 
  UNSIGNED8  access;              // Bits ODRD, ODWR
  UNSIGNED16 length;              // Length of data
  UNSIGNED8  *pData;              // Pointer to data
} OD_GENERIC_DATA_ENTRY;
#endif // USE_EXTENDED_SDO


/**************************************************************************
GLOBAL FUNCTIONS
**************************************************************************/

/**************************************************************************
DOES:    Initializes the MicroCANopen stack
         It must be called from within MCOUSER_ResetApplication
RETURNS: TRUE, if init OK, else FALSE (also when unconfigured and in LSS)
**************************************************************************/
UNSIGNED8 MCO_Init (
  UNSIGNED16 Baudrate,  // CAN baudrate in kbit(1000,800,500,250,125,50,25 or 10)
  UNSIGNED8 Node_ID,    // CANopen node ID (1-126)
  UNSIGNED16 Heartbeat  // Heartbeat time in ms (0 for none)
  );


/**************************************************************************
DOES:    This function initializes a transmit PDO. Once initialized, the 
         MicroCANopen stack automatically handles transmitting the PDO.
         The application can directly change the data at any time.
NOTE:    For data consistency, the application should not write to the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_InitTPDO (
  UNSIGNED16 PDO_NR,     // TPDO number (1-512)
  UNSIGNED32 CAN_ID,     // CAN identifier to be used (set to 0 to use default)
  UNSIGNED16 event_tim,  // Transmitted every event_tim ms 
                         // (set to 0 if ONLY inhibit_tim should be used)
  UNSIGNED16 inhibit_tim,// Inhibit time in ms for change-of-state transmit
                         // (set to 0 if ONLY event_tim should be used)
  UNSIGNED8 len,         // Number of data bytes in TPDO
  UNSIGNED16 offset      // Offset to data location in process image
  );


/**************************************************************************
DOES:    This function rest a transmit PDO.
NOTE:    For data consistency, the application should not write to the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_ResetTPDO (
  UNSIGNED16 PDO_NR      // TPDO number (1-512)
  );


/**************************************************************************
DOES:    This function initializes a receive PDO. Once initialized, the 
         MicroCANopen stack automatically updates the data at offset.
NOTE:    For data consistency, the application should not read the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_InitRPDO (
  UNSIGNED16 PDO_NR, // RPDO number (1-512)
  UNSIGNED32 CAN_ID, // CAN identifier to be used (set to 0 to use default)
  UNSIGNED8 len,     // Number of data bytes in RPDO
  UNSIGNED16 offset  // Offset to data location in process image
  );


/**************************************************************************
DOES:    This function reset a receive PDO.
NOTE:    For data consistency, the application should not read the data
         while function MCO_ProcessStack executes.
RETURNS: nothing
**************************************************************************/
void MCO_ResetRPDO (
  UNSIGNED16 PDO_NR  // RPDO number (1-512)
  );


/**************************************************************************
DOES:    This function implements the main MicroCANopen protocol stack. 
         It must be called frequently to ensure proper operation of the
         communication stack. 
         Typically it is called from the while(1) loop in main.
RETURNS: 0 if nothing was done, 1 if a CAN message was sent or received
**************************************************************************/
UNSIGNED8 MCO_ProcessStack (
  void
  );


/**************************************************************************
         ALTERNATE PROCESS FUNCTION TO BE USED WITH RTOS INTEGRATION
DOES:    This function processes the next CAN message from the CAN receive
         queue. When using an RTOS, this can be turned into a task
         triggered by a CAN receive event.
RETURNS: 0 if no message was processed, 
         1 if a CAN message received was processed
**************************************************************************/
UNSIGNED8 MCO_ProcessStackRx (
  void
  );


/**************************************************************************
         ALTERNATE PROCESS FUNCTION TO BE USED WITH RTOS INTEGRATION
DOES:    This function executes all sub functions required to keep the 
         CANopen stack operating. It should be called frequently. When used
         in an RTOS it should be called repeatedly every RTOS time tick
         until it returns zero.
RETURNS: 0 if there was nothing to process 
         1 if functions were
**************************************************************************/
UNSIGNED8 MCO_ProcessStackTick (
  void
  );

#if USECB_SDOResponse
/****************************************************************
DOES:    User callback
RETURNS:
*****************************************************************/
void MCOUSER_SDOResponse (
  COBID_TYPE ID,
  UNSIGNED8 Len,
  UNSIGNED8 *pDat
  );
#endif

#if USECB_Emergency
/****************************************************************
DOES:    User callback
RETURNS:
*****************************************************************/
void MCOUSER_EmergencyReceived (
  COBID_TYPE ID,
  UNSIGNED8 Len,
  UNSIGNED8 *pDat
  );
#endif

/**************************************************************************
USER CALL-BACK FUNCTIONS
These must be implemented by the application.
**************************************************************************/

/**************************************************************************
DOES:    Call-back function for reset application.
         Starts the watchdog and waits until watchdog causes a reset.
RETURNS: nothing
**************************************************************************/
void MCOUSER_ResetApplication (
  void
  );


/**************************************************************************
DOES:    This function both resets and initializes both the CAN interface
         and the CANopen protocol stack. It is called from within the
         CANopen protocol stack, if a NMT master message was received that
         demanded "Reset Communication".
         This function should call MCO_Init and MCO_InitTPDO/MCO_InitRPDO.
RETURNS: nothing
**************************************************************************/
void MCOUSER_ResetCommunication (
  int resetNodeId
  );

void MCOUSER_ResetPDO (
  void
  );


/**************************************************************************
DOES:    This function is called if a fatal error occurred. 
         Error codes of mcohwxxx.c are in the range of 0x8000 to 0x87FF.
         Error codes of mco.c are in the range of 0x8800 to 0x8FFF. 
         All other error codes may be used by the application.
RETURNS: nothing
**************************************************************************/
void MCOUSER_FatalError (
  UNSIGNED16 ErrCode // To debug, search source code for the ErrCode encountered
  );


#if USE_TIME_OF_DAY_RX
/**************************************************************************
DOES:    This function is called if the message with the time object has
         been received.
RETURNS: nothing
**************************************************************************/
void MCOSUSER_TimeOfDay (
  UNSIGNED32 millis, // Milliseconds since midnight
  UNSIGNED16 days  // Number of days since January 1st, 1984
  );
#endif


#if USECB_RPDORECEIVE
/**************************************************************************
DOES:    This function is called after an RPDO has been received and stored
         into the Process Image.
RETURNS: nothing
**************************************************************************/
void MCOUSER_RPDOReceived (
  UNSIGNED16 RPDONr, // RPDO Number
  UNSIGNED16 pRPDO,  // Offset to RPDO data in Process Image
  UNSIGNED8  len     // Length of RPDO
  );

int MCOUSER_ReceiveRaw(CAN_MSG MEM_FAR *pRPDO);

#endif // USECB_RPDORECEIVE


#if USECB_TPDORDY
/**************************************************************************
DOES:    This function is called before a TPDO is sent. For triggering
         modes that are outside of the application's doing (Event Timer,
         SYNC), it is called before the send data is retrieved from the
         Process Image. This allows the application to update the TPDO
         data if necessary.
NOTE:    This function is also called before a change-of-state or
         application-triggered TPDO is sent, but updating the Process Image
         will not have any effect on the TPDO data in this case.
RETURNS: nothing
**************************************************************************/
void MCOUSER_TPDOReady (
  UNSIGNED16 TPDONr,      // TPDO Number
  UNSIGNED8  TPDOTrigger  // Trigger for this TPDO's transmission:
                          // 0: Event Timer
                          // 1: SYNC
                          // 2: SYNC+COS
                          // 3: COS or application trigger
  );
#endif // USECB_TPDORDY


#if USECB_APPSDO_READ
/*******************************************************************************
DOES:    Call Back function to allow implementation of custom, application
         specific OD Read entries
RETURNS: 0x00 - OD entry not handled by this function
         0x01 - OD entry handled by this function
         0x05 - Abort with SDO_ABORT_WRITEONLY
*******************************************************************************/
UNSIGNED8 MCOUSER_AppSDOReadInit (
  UNSIGNED8 sdoserver, // The SDO server number on which the request came in
  UNSIGNED16 idx, // Index of OD entry
  UNSIGNED8 subidx, // Subindex of OD entry
  UNSIGNED32 *size, // RETURN: size of data
  UNSIGNED8 **pDat // RETURN: pointer to data buffer
  );
#endif // USECB_APPSDO_READ


#if USECB_APPSDO_WRITE
/*******************************************************************************
DOES:    Call Back function to allow implementation of custom, application
         specific OD Write entries
RETURNS: 0x00 - OD entry not handled by this function
         0x01 - OD entry handled by this function
         0x04 - Abort with SDO_ABORT_READONLY
*******************************************************************************/
UNSIGNED8 MCOUSER_AppSDOWriteInit (
  UNSIGNED8 sdoserver, // The SDO server number on which the request came in
  UNSIGNED16 idx, // Index of OD entry
  UNSIGNED8 subidx, // Subindex of OD entry
  UNSIGNED32 *size, // Data size, if known,RETURN: max size of data buffer
  UNSIGNED8 **pDat // RETURN: pointer to data buffer
  );

/*******************************************************************************
DOES:    Call Back function to allow implementation of custom, application
         specific OD Write entries, call at en of segment or transfer
RETURNS: Nothing
*******************************************************************************/
void MCOUSER_AppSDOWriteComplete (
  UNSIGNED8 sdoserver, // The SDO server number on which the request came in
  UNSIGNED16 idx, // Index of OD entry
  UNSIGNED8 subidx, // Subindex of OD entry
  UNSIGNED32 size, // number of bytes written
  UNSIGNED32 more // number of bytes still to come
  );
#endif // USECB_APPSDO_WRITE


#if USE_XSDOCB_WRITE
/**************************************************************************
DOES:    This function is called before a segmented SDO write access is 
         made. The application can use this to implement custom, segmented
		     SDO write transfers.
RETURNS: TRUE, if this access is supported by the application
**************************************************************************/
UNSIGNED8 MCOUSER_XSDOInitWrite (
  UNSIGNED16 index, 
  UNSIGNED8 subindex,
  UNSIGNED32 size	  // size in bytes
  );


/*******************************************************************************
DOES:    After an access was approved by the application by the 
		 MCOUSER_XSDOInitWrite function, this function is called with each
		 new segment received.
RETURNS: TRUE if no error occured and the data was processed
         FALSE if a major error occured and the transfer needs to be aborted
*******************************************************************************/
UNSIGNED8 MCOUSER_XSDOWriteSegment (
  UNSIGNED8 last, // Is set to 1 if this is the last segment
  UNSIGNED8 len, // length of segment (0-7)
  UNSIGNED8 *pDat // pointer to 'len' data bytes
  );
#endif // USE_XSDOCB_WRITE


#if USE_SLEEP
/**************************************************************************
DOES:    DRIVER LEVEL: Sets the processor into sleep or power down mode.
         Called when a sleep request was received and confirmed.
         Wakeup MUST be through a reset.
**************************************************************************/
void MCOHW_Sleep (
  void
  );

/*******************************************************************************
DOES:    APPLICATION LEVEL: Call back function for sleep/wakeup messages 
         received as specified in CiA447.
RETURNS: nothing
*******************************************************************************/
void MCOUSER_Sleep (
  UNSIGNED8 mode // 0: Wakeup message received
                 // 1: Query Sleep Objection message received
                 // 2: Set Sleep Mode message received
  );

/*******************************************************************************
DOES:    Functions to transmit sleep messages as defined by CiA 447
RETURNS: TRUE, if message was succesfully queued
*******************************************************************************/
UNSIGNED8 MCO_TransmitWakeup (void);
UNSIGNED8 MCO_TransmitSleepObjection (void);
UNSIGNED8 MCO_TransmitSleepRequest (void);

#endif // USE_SLEEP


/**************************************************************************
Plausability check for settings
**************************************************************************/


#if ! USE_EVENT_TIME
  #if ! USE_INHIBIT_TIME
#error At least one, USE_EVENT_TIME or USE_INHIBIT_TIME must be defined!
  #endif
#endif

#if (NR_OF_RPDOS == 0)
  #if (NR_OF_TPDOS == 0)
#error At least one PDO must be defined!
  #endif
#endif

#if USE_STORE_PARAMETERS
 #if ! USE_EVENT_TIME
  #error When using Save Parameters, USE_EVENT_TIME must be used, too
 #endif
 #if ! USE_INHIBIT_TIME
  #error When using Save Parameters, USE_INHIBIT_TIME must be used, too
 #endif
#endif

#if ((NR_OF_TPDOS > 512) || (NR_OF_RPDOS > 512))
#error Illegal number of PDOs
#endif

#if ERROR_FIELD_SIZE > 253
#error Illegal size of error field
#endif

UNSIGNED16 MCO_SearchODProcTable (
  UNSIGNED16 searchODProcTableIndex,   // Index of OD entry searched
  UNSIGNED8 subindex  // Subindex of OD entry searched
);

#endif // _MCO_H
// END OF FILE
