/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2008, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		SPIM.H
*
*	\ingroup	SPIM
*
*	\brief		Public declarations of the SPI master FB
*
*	\details
*
*	\note
*
*	\version	25-07-2008 / Ari Suomi
*
*******************************************************************************/

#ifndef SPIM_H_INCLUDED
#define SPIM_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "protif.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*
 * Following definition adds compatibility for older projects that have used
 * the PROJECT_CLOCK_HZ definition
 */
#ifdef PROJECT_CLOCK_HZ
# ifndef PROJECT_CPU_HZ
#  define PROJECT_CPU_HZ PROJECT_CLOCK_HZ
# endif
#endif


/**
 * 	SPI master special request flags
 */

/*@{*/
#define SPIM_REQ_CS_OFF		(1<<PROTIF_FLG_BITS)/**< Chip select should be off*/
#define SPIM_REQ_CS_ON		(0<<PROTIF_FLG_BITS)/**< Chip select should be on*/
#define SPIM_REQ_CS_MASK	(1<<PROTIF_FLG_BITS)/**< Chip select state mask	*/
/*@}*/


/**
 * 	\name	SPI master configuration flags
 *
 *	The following flags can be used in the flags field of the SlaveInfo
 * 	structure.
 */

/*@{*/
#define SPIM_PHASE_RISING	(1<<0)		/**< Operation on rising clock edge	*/
#define SPIM_PHASE_FALLING	0			/**< Operation on falling clock edge*/
#define SPIM_IDLE_LEVEL_H	(1<<1)		/**< Clock idle level high.			*/
#define SPIM_IDLE_LEVEL_L	0			/**< Clock idle level low.			*/
#define SPIM_CS_ACTIVE_HIGH	(1<<2)		/**< Chip select active level high.	*/
/*@}*/


 /**
 * 	\name	Physical bus selection
 *
 *	The following defines are used to select which physical bus the SPIM
 *	instance will use.
 *
 *	On lpc23xx:
 *	- SPIM_BUS0:	SPI				(SCK: P0.15)
 *	- SPIM_BUS1: 	SSP0 			(SCK: P1.20)
 * 	- SPIM_BUS2:	SSP1 			(SCK: P0.7)
 *
 *	On lpc17xx:
 *	- SPIM_BUS0:	SSP0 			(SCK: P1.20) 
 *	- SPIM_BUS1: 	SSP0 			(SCK: P0.15)
 * 	- SPIM_BUS2:	SSP1 			(SCK: P0.7		MISO: P0.8		MOSI: P0.9)
 *	- SPIM_DMABUS1:	SSP0 with DMA	(SCK: P1.20		MISO: P1.23		MOSI: P1.24)
 *	- SPIM_DMABUS2:	SSP1 with DMA	(SCK: P0.7		MISO: P0.8		MOSI: P0.9)
 *	- SPIM_DMABUS3:	SSP0 with DMA	(SCK: P0.15		MISO: P0.17		MOSI: P0.18)
 *	- SPIM_BI_BUS:	SSP0 bus, both IRQ & DMA (pins switchable)
 *	- SPIM_BUS1NOCONF: SSP0 without pin and clock configuration
 *	- SPIM_BUS2NOCONF: SSP1 without pin and clock configuration
 *
 *	On lpc177x/8x and lpc40xx:
 *	- SPIM_BUS0:	SSP0			(SCK: P1.20,	MISO: P1.23,	MOSI: P1.24)
 *	- SPIM_BUS1: 	SSP0			(SCK: P0.15,	MISO: P0.17,	MOSI: P0.18)
 *	- SPIM_BUS2: 	SSP1			(SCK: P0.7,		MISO: P0.8,		MOSI: P0.9)
 * 	- SPIM_BUS3:	SSP0			(SCK: P2.22,	MISO: P2.26,	MOSI: P2.27)
 * 	- SPIM_BUS4:	SSP1			(SCK: P1.19,	MISO: P1.18,	MOSI: P1.22)
 * 	- SPIM_BUS5:	SSP1			(SCK: P4.20,	MISO: P4.22,	MOSI: P4.23)
 * 	- SPIM_BUS6:	SSP1			(SCK: P1.31,	MISO: P0.12,	MOSI: P0.13)
 * 	- SPIM_BUS7:	SSP2			(SCK: P1.0,		MISO: P1.4,		MOSI: P1.1)
 *	- SPIM_DMABUS0:	SSP0 with DMA	(SCK: P2.22,	MISO: P2.26,	MOSI: P2.27)
 *	- SPIM_DMABUS1:	SSP0 with DMA	(SCK: P1.20,	MISO: P1.23,	MOSI: P1.24)
 *	- SPIM_DMABUS2:	SSP1 with DMA	(SCK: P0.7,		MISO: P0.8,		MOSI: P0.9)
 * 	- SPIM_DMABUS3:	SSP0 with DMA	(SCK: P0.15,	MISO: P0.17,	MOSI: P0.18)
 * 	- SPIM_DMABUS4:	SSP1 with DMA	(SCK: P1.19,	MISO: P1.18,	MOSI: P0.22)
 * 	- SPIM_DMABUS5:	SSP1 with DMA	(SCK: P4.20,	MISO: P4.22,	MOSI: P4.23)
 * 	- SPIM_DMABUS6:	SSP1 with DMA	(SCK: P1.31,	MISO: P0.12,	MOSI: P0.13)
 * 	- SPIM_DMABUS7:	SSP2 with DMA	(SCK: P1.0,		MISO: P1.4,		MOSI: P1.1)
 *
 *	On lpc11x6x:
 *	- SPIM_BUS0:	SSP0			(SCK: P1.29		MISO: P1.16		MOSI: P1.12)
 *	- SPIM_BUS1: 	SSP0 			(SCK: P0.6		MISO: P0.8		MOSI: P0.9)
 * 	- SPIM_BUS2:	SSP1 			(SCK: P1.20		MISO: P1.21		MOSI: P1.22)
 * 	- SPIM_BUS3:	SSP1			(SCK: P1.27		MISO: P0.22		MOSI: P0.21)
 * 	- SPIM_BUS4:	SSP1			(SCK: P1.20		MISO: P0.22		MOSI: P0.21)
 *
 *	On STM32F3xx:
 *	- SPIM_BUS1: 	SPI1 			(SCK: PA5		MISO: PA6		MOSI: PA7)
 * 	- SPIM_BUS2:	SPI2 			(SCK: PB13		MISO: PB14		MOSI: PB15)
 * 	- SPIM_BUS3:	SPI3			(SCK: PB3		MISO: PB4		MOSI: PB5)
 * 	- SPIM_BUS4:	SPI3			(SCK: PC10		MISO: PB4		MOSI: PC12)
 *
 *	On STM32F4xx:
 *	- SPIM1_BUS1: 	SPI1 			(SCK: PA5		MISO: PA6		MOSI: PA7)
 *	- SPIM2_BUS2:	SPI2 			(SCK: PB13		MISO: PB14		MOSI: PB15)
 *	- SPIM2_BUS3_1:	SPI3			(SCK: PC10		MISO: PC11		MOSI: PC12)
 *	- SPIM2_BUS3_2:	SPI3			(SCK: PB3		MISO: PB4		MOSI: PB5)
 *
 *	On STM32F2xx:
 *	- SPIM1_BUS1: 	SPI1 			(SCK: PA5		MISO: PA6		MOSI: PA7)
 *	- SPIM2_BUS2:	SPI2 			(SCK: PB13		MISO: PB14		MOSI: PB15)
 *	- SPIM2_BUS3_1:	SPI3			(SCK: PC10		MISO: PC11		MOSI: PC12)
 *	- SPIM2_BUS3_2:	SPI3			(SCK: PB3		MISO: PB4		MOSI: PB5)
 *
 *	On w32:
 *	- SPIM_BUS0:	Simulated bus (will call function in project.c)
 *	- SPIM_BUS1:	Simulated bus (will call function in project.c)
 *	- SPIM_BUS2:	Simulated bus (will call function in project.c)
 *	- SPIM_BUS4:	Simulated bus (will call function in project.c)
 *	- SPIM_DMABUS1:	Same as SPIM_BUS1
 *	- SPIM_DMABUS2:	Same as SPIM_BUS2
 *	- SPIM_DMABUS3:	Same as SPIM_BUS1
 */

#if TARGET_SELECTED & TARGET_ARM7
#define SPIM_BUS0			&spim__tools0
#define SPIM_BUS1			&spim__tools1
#define SPIM_BUS2			&spim__tools2

#elif TARGET_SELECTED & (TARGET_CM3|TARGET_CM4)

#  define SPIM_BUS0			&spim__tools1p1p20
#  define SPIM_BUS1			&spim__tools1p0p15
#  define SPIM_BUS1NOCONF	&spim__tools1NoConf
#  define SPIM_BUS2			&spim__tools2p0p7
#  define SPIM_BUS2NOCONF	&spim__tools2NoConf
#  define SPIM_DMABUS1		&spim__dmaTools1p1p20
#  define SPIM_DMABUS2		&spim__dmaTools2p0p7
#  define SPIM_DMABUS3		&spim__dmaTools1p0p15

# if defined(TARGETFAMILY_LPC178x_7x) || (TARGET_SELECTED & TARGET_CM4) != 0

#  define SPIM_BUS3			&spim__tools1p2p22
#  define SPIM_BUS4			&spim__tools2p1p19
#  define SPIM_BUS5			&spim__tools2p4p20
#  define SPIM_BUS6			&spim__tools2p1p31
#  define SPIM_BUS7			&spim__tools3p1p0

#  define SPIM_DMABUS0		&spim__dmaTools1p2p22
#  define SPIM_DMABUS4		&spim__dmaTools2p1p19
#  define SPIM_DMABUS5		&spim__dmaTools2p4p20
#  define SPIM_DMABUS6		&spim__dmaTools2p1p31

#  ifdef TARGETFAMILY_LPC178x_7x
#   define SPIM_DMABUS7		&spim__dmaTools3p1p0
#  endif

# else /* LPC17xx */

#  define SPIM_BI_BUS0		&spim__biTools1
#  define SPIM_BI_SETPINS0	spim_biSetPins1

# endif

#elif TARGET_SELECTED & TARGET_CM0

# define SPIM_BUS0			&spim__tools1p0p6
# define SPIM_BUS1			&spim__tools1p1p29
# define SPIM_BUS2			&spim__tools2p1p20
# define SPIM_BUS3			&spim__tools2p1p27
# define SPIM_BUS4			&spim__tools2p1p20v2

#elif TARGET_SELECTED & TARGET_SM4

/* TODO:	These need to be implemented. */
# define SPIM_BUS0			NULL
# define SPIM_BUS1			&spim__tools1pap5
# define SPIM_BUS2			&spim__tools2pbp13
# define SPIM_BUS3			&spim__tools3pbp3
# define SPIM_BUS4			&spim__tools3pcp10
# define SPIM_DMABUS1		NULL
# define SPIM_DMABUS2		NULL
# define SPIM_DMABUS3		NULL

#elif TARGET_SELECTED & TARGET_S4F4

# define SPIM_BUS1			&spim__tools1pap5
# define SPIM_BUS1NOCONF	&spim__tools1NoConf
# define SPIM_BUS2			&spim__tools2pbp13
# define SPIM_BUS3_1		&spim__tools3pcp10
# define SPIM_BUS3_2		&spim__tools3pbp3
# define SPIM_BUS3NOCONF	&spim__tools3NoConf

#elif TARGET_SELECTED & TARGET_S3F2

# define SPIM_BUS1			&spim__tools1pap5
# define SPIM_BUS1NOCONF	&spim__tools1NoConf
# define SPIM_BUS2			&spim__tools2pbp13
# define SPIM_BUS3_1		&spim__tools3pcp10
# define SPIM_BUS3_2		&spim__tools3pbp3
# define SPIM_BUS3NOCONF	&spim__tools3NoConf

#elif TARGET_SELECTED & TARGET_W32

# define SPIM_BUS0			&spim__tools0
# define SPIM_BUS1			&spim__tools1
# define SPIM_BUS2			&spim__tools2
# define SPIM_BUS3			&spim__tools3
# define SPIM_BUS4			&spim__tools4
# define SPIM_BUS7			&spim__tools7
# define SPIM_DMABUS1		&spim__tools1
# define SPIM_DMABUS2		&spim__tools2
# define SPIM_DMABUS3		&spim__tools1
# define SPIM_BI_BUS0		&spim__tools0 /* TODO: check if ok */
#define SPIM_BI_SETPINS0	spim_biSetPins1

#elif (TARGET_SELECTED & TARGET_DOXY) == TARGET_DOXY

/*@{*/
#define SPIM_BUS0		&spim__tools0	/**< Selects physical bus 1			*/
#define SPIM_BUS1		&spim__tools1	/**< Selects physical bus 2			*/
#define SPIM_BUS2		&spim__tools2	/**< Selects physical bus 3			*/
/*@}*/

#endif

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

/**
 * 	\name	Bus clock selection
 *
 *	The following macros are used to select the clock of the SPI bus. The init
 *	structure for SPIM takes the value that will be directly copied to the
 *	hardware SPI clock register. To make the clock selection easier there are
 *	macros that converts a value given in Hz to the hardware clock register
 *	value. Each physical bus clock may be calculated differently and that's why
 *	there are one macro for each physical bus.
 *
 *	The macro implementation depends on the target.
 */

#if TARGET_SELECTED & TARGET_ARM7

#define spim_hzToRegVal0(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))
#define spim_hzToRegVal1(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))
#define spim_hzToRegVal2(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))

#elif TARGET_SELECTED & TARGET_CM3

#ifdef TARGETFAMILY_LPC178x_7x /* LPC177x/8x */

#define spim_hzToRegVal0(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))	/* SSP0 */
#define spim_hzToRegVal1(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))	/* SSP1 */
#define spim_hzToRegVal2(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))	/* SSP2 */

#else /* TARGETFAMILY_LPC178x_7x  */
/* TODO: check clock calculation macroS */
#define spim_hzToRegVal0(n_)	MIN(255, PROJECT_CPU_HZ / (n_))
#define spim_hzToRegVal1(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))
#define spim_hzToRegVal2(n_)	MIN(255, PROJECT_CPU_HZ / (2 * (n_)))

#endif /* TARGETFAMILY_LPC178x_7x  */

#elif TARGET_SELECTED & TARGET_CM4

#define spim_hzToRegVal1(n_)	MIN(255, PROJECT_CPU_HZ / (2 * ((n_) + 1)))
#define spim_hzToRegVal2(n_)	MIN(255, PROJECT_CPU_HZ / (2 * ((n_) + 1)))

#elif TARGET_SELECTED & TARGET_CM0

/**
 * \brief Macro for calculating clock settings
 * \param	n_			The desired SPI clock in Hz.
 * \param	perClk_		The SSP peripheral clock in Hz.
 */
#define spim_freqToRegVal1(n_, perClk_)	MIN(255, (perClk_) / (2 * ((n_) + 1)))
#define spim_freqToRegVal2(n_, perClk_)	MIN(255, (perClk_) / (2 * ((n_) + 1)))

#elif TARGET_SELECTED & TARGET_SM4

/* 
 * Following defines must be in projects local.h:
 * - PROJ__SM4_APB1_HZ
 * - PROJ__SM4_APB2_HZ
 */

#define spim_hzToRegValAPB2(n_)								\
(															\
	((((PROJ__SM4_APB2_HZ)/2) < (n_) ? 1 : 0) << 7)			\
	|	((((PROJ__SM4_APB2_HZ)/4) < (n_) ? 1 : 0) << 6)		\
	|	((((PROJ__SM4_APB2_HZ)/8) < (n_) ? 1 : 0) << 5)		\
	|	((((PROJ__SM4_APB2_HZ)/16) < (n_) ? 1 : 0) << 4)	\
	|	((((PROJ__SM4_APB2_HZ)/32) < (n_) ? 1 : 0) << 3)	\
	|	((((PROJ__SM4_APB2_HZ)/64) < (n_) ? 1 : 0) << 2)	\
	|	((((PROJ__SM4_APB2_HZ)/128) < (n_) ? 1 : 0) << 1)	\
	|	((((PROJ__SM4_APB2_HZ)/256) < (n_) ? 1 : 0) << 0)	\
)

#define spim_hzToRegValAPB1(n_)								\
(															\
		((((PROJ__SM4_APB1_HZ)/2) < (n_) ? 1 : 0) << 7)		\
	|	((((PROJ__SM4_APB1_HZ)/4) < (n_) ? 1 : 0) << 6)		\
	|	((((PROJ__SM4_APB1_HZ)/8) < (n_) ? 1 : 0) << 5)		\
	|	((((PROJ__SM4_APB1_HZ)/16) < (n_) ? 1 : 0) << 4)	\
	|	((((PROJ__SM4_APB1_HZ)/32) < (n_) ? 1 : 0) << 3)	\
	|	((((PROJ__SM4_APB1_HZ)/64) < (n_) ? 1 : 0) << 2)	\
	|	((((PROJ__SM4_APB1_HZ)/128) < (n_) ? 1 : 0) << 1)	\
	|	((((PROJ__SM4_APB1_HZ)/256) < (n_) ? 1 : 0) << 0)	\
)

#define spim_hzToRegVal1(n_)	spim_hzToRegValAPB2((n_))
#define spim_hzToRegVal2(n_)	spim_hzToRegValAPB1((n_))
#define spim_hzToRegVal3(n_)	spim_hzToRegValAPB1((n_))

#elif TARGET_SELECTED & TARGET_S4F4

/* 
 * Following defines must be in projects local.h:
 * - PROJ__S4F4_APB1_HZ
 * - PROJ__S4F4_APB2_HZ
 */

#define spim_hzToRegValAPB2(n_)								\
(															\
	((((PROJ__S4F4_APB2_HZ)/2) < (n_) ? 1 : 0) << 7)		\
	|	((((PROJ__S4F4_APB2_HZ)/4) < (n_) ? 1 : 0) << 6)	\
	|	((((PROJ__S4F4_APB2_HZ)/8) < (n_) ? 1 : 0) << 5)	\
	|	((((PROJ__S4F4_APB2_HZ)/16) < (n_) ? 1 : 0) << 4)	\
	|	((((PROJ__S4F4_APB2_HZ)/32) < (n_) ? 1 : 0) << 3)	\
	|	((((PROJ__S4F4_APB2_HZ)/64) < (n_) ? 1 : 0) << 2)	\
	|	((((PROJ__S4F4_APB2_HZ)/128) < (n_) ? 1 : 0) << 1)	\
	|	((((PROJ__S4F4_APB2_HZ)/256) < (n_) ? 1 : 0) << 0)	\
)

#define spim_hzToRegValAPB1(n_)								\
(															\
		((((PROJ__S4F4_APB1_HZ)/2) < (n_) ? 1 : 0) << 7)	\
	|	((((PROJ__S4F4_APB1_HZ)/4) < (n_) ? 1 : 0) << 6)	\
	|	((((PROJ__S4F4_APB1_HZ)/8) < (n_) ? 1 : 0) << 5)	\
	|	((((PROJ__S4F4_APB1_HZ)/16) < (n_) ? 1 : 0) << 4)	\
	|	((((PROJ__S4F4_APB1_HZ)/32) < (n_) ? 1 : 0) << 3)	\
	|	((((PROJ__S4F4_APB1_HZ)/64) < (n_) ? 1 : 0) << 2)	\
	|	((((PROJ__S4F4_APB1_HZ)/128) < (n_) ? 1 : 0) << 1)	\
	|	((((PROJ__S4F4_APB1_HZ)/256) < (n_) ? 1 : 0) << 0)	\
)

#define spim_hzToRegVal1(n_)	spim_hzToRegValAPB2((n_))
#define spim_hzToRegVal2(n_)	spim_hzToRegValAPB1((n_))
#define spim_hzToRegVal3(n_)	spim_hzToRegValAPB1((n_))

#elif TARGET_SELECTED & TARGET_S3F2

/* 
 * Following defines must be in projects local.h:
 * - PROJ__S3F2_APB1_HZ
 * - PROJ__S3F2_APB2_HZ
 */

#define spim_hzToRegValAPB2(n_)								\
(															\
	((((PROJ__S3F2_APB2_HZ)/2) < (n_) ? 1 : 0) << 7)		\
	|	((((PROJ__S3F2_APB2_HZ)/4) < (n_) ? 1 : 0) << 6)	\
	|	((((PROJ__S3F2_APB2_HZ)/8) < (n_) ? 1 : 0) << 5)	\
	|	((((PROJ__S3F2_APB2_HZ)/16) < (n_) ? 1 : 0) << 4)	\
	|	((((PROJ__S3F2_APB2_HZ)/32) < (n_) ? 1 : 0) << 3)	\
	|	((((PROJ__S3F2_APB2_HZ)/64) < (n_) ? 1 : 0) << 2)	\
	|	((((PROJ__S3F2_APB2_HZ)/128) < (n_) ? 1 : 0) << 1)	\
	|	((((PROJ__S3F2_APB2_HZ)/256) < (n_) ? 1 : 0) << 0)	\
)

#define spim_hzToRegValAPB1(n_)								\
(															\
		((((PROJ__S3F2_APB1_HZ)/2) < (n_) ? 1 : 0) << 7)	\
	|	((((PROJ__S3F2_APB1_HZ)/4) < (n_) ? 1 : 0) << 6)	\
	|	((((PROJ__S3F2_APB1_HZ)/8) < (n_) ? 1 : 0) << 5)	\
	|	((((PROJ__S3F2_APB1_HZ)/16) < (n_) ? 1 : 0) << 4)	\
	|	((((PROJ__S3F2_APB1_HZ)/32) < (n_) ? 1 : 0) << 3)	\
	|	((((PROJ__S3F2_APB1_HZ)/64) < (n_) ? 1 : 0) << 2)	\
	|	((((PROJ__S3F2_APB1_HZ)/128) < (n_) ? 1 : 0) << 1)	\
	|	((((PROJ__S3F2_APB1_HZ)/256) < (n_) ? 1 : 0) << 0)	\
)

#define spim_hzToRegVal1(n_)	spim_hzToRegValAPB2((n_))
#define spim_hzToRegVal2(n_)	spim_hzToRegValAPB1((n_))
#define spim_hzToRegVal3(n_)	spim_hzToRegValAPB1((n_))


#elif TARGET_SELECTED & TARGET_W32

#define spim_hzToRegVal0(n_)	(n_)
#define spim_hzToRegVal1(n_)	(n_)
#define spim_hzToRegVal2(n_)	(n_)
#define spim_hzToRegVal3(n_)	(n_)
#define spim_freqToRegVal1(n_, perClk_)	(n_)
#define spim_freqToRegVal2(n_, perClk_)	(n_)

#elif (TARGET_SELECTED & TARGET_DOXY) == TARGET_DOXY

/*@{*/
#define spim_hzToRegVal0(n)				/**< Used for physical bus 1		*/
#define spim_hzToRegVal1(n)				/**< Used for physical bus 2		*/
#define spim_hzToRegVal2(n)				/**< Used for physical bus 3		*/
/*@}*/

#endif

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/**
 * Slave info type. All information about a slave is stored with this type. Each
 * slave has an own CS pin, clock settings and frame length.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	Uint8 					csPin;		/**< Slave CS pin                   */
	Uint8 					csPort; 	/**< Slave CS port                  */
	Uint32					clkDiv;		/**< SPI clock divider value        */
	Uint8 					flags;		/**< Option flags                   */
	Uint8					frameBits;	/**< Number of bits/frame			*/
} spim_SlaveInfo;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

struct spim__tools;
typedef struct spim__tools spim__Tools;

/**
 * initialization data type used in project.h
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	spim__Tools const_P *	pTools;		/**< Physical bus selection.		*/
	spim_SlaveInfo const_P * pSlaveList;/**< List of SPI slaves.			*/
	Uint8 					slaveNum;	/**< Number of slaves in list.		*/
	Uint8					tx_dma_chnl;/**< TX DMA channel number			*/
	Uint8					rx_dma_chnl;/**< RX DMA channel number			*/
	Uint8					switchCount;/**< Switchover byte count (irq/dma)*/
	Uint8					noCoTask;	/**< CoTask triggered if 0			*/
	Uint16					stackSize;	/**< The minimum CoTask stack size.	*/
} spim_Init;							/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

struct spim__inst;
typedef struct spim__inst spim__Inst;

/**
* Type of hw bus init function. The HW bus init function allocates memory for
* the SPIM instance. It is done in the HW init function because the instance
* type may differ depending on the target and used physical bus.
*/

typedef spim__Inst *		spim__HwInitFn(spim_Init const_P *);

/**
* Type of hw bus process function.
*/

typedef void				spim__HwProcFn(spim__Inst *);

/**
* Type of hw bus up function. The function is called when the FB goes up from
* down state.
*/

typedef void				spim__HwUpFn(spim__Inst *);

/**
* Type of hw bus up function. The function is called when chip select state
* needs to be changed.
*/

typedef void				spim__HwChipSelFn(spim__Inst *, Boolean);

/**
* Physical bus tools. Each physical bus has one variable of this type.
*/

struct spim__tools {					/*''''''''''''''''''''''''''' CONST */
	spim__HwInitFn * 		pLowInitFn;	/**< Low level init function        */
	spim__HwProcFn * 		pLowProcFn;	/**< Low level process of request   */
	spim__HwUpFn * 			pLowUpFn;	/**< Low level enable SPI function  */
	spim__HwChipSelFn *		pChipSel;	/**< Chip select state change		*/
};										/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

typedef void (*spim_setPinsFn) (Uint8);

/*******************************************************//** \cond pub_decl *//*
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

extern protif_Interface	const_P	spim_fastIf;
extern protif_Interface	const_P	spim_interface;
extern protif_Interface	const_P	spim_biInterface;

#if TARGET_SELECTED & TARGET_W32

extern spim__Tools const_P	spim__tools0;
extern spim__Tools const_P	spim__tools1;
extern spim__Tools const_P	spim__tools2;
extern spim__Tools const_P	spim__tools3;
extern spim__Tools const_P	spim__tools4;
extern spim__Tools const_P	spim__tools7;
extern void spim_biSetPins1(Uint8);

#elif TARGET_SELECTED & TARGET_ARM7
extern spim__Tools const_P	spim__tools0;
extern spim__Tools const_P	spim__tools1;
extern spim__Tools const_P	spim__tools2;

#elif TARGET_SELECTED & (TARGET_CM3|TARGET_CM4)

extern spim__Tools const_P	spim__tools1p1p20;
extern spim__Tools const_P	spim__tools1p0p15;
extern spim__Tools const_P	spim__tools1NoConf;
extern spim__Tools const_P	spim__tools2p0p7;
extern spim__Tools const_P	spim__tools2NoConf;
extern spim__Tools const_P	spim__dmaTools1p1p20;
extern spim__Tools const_P	spim__dmaTools2p0p7;
extern spim__Tools const_P	spim__dmaTools1p0p15;

# if defined(TARGETFAMILY_LPC178x_7x) || (TARGET_SELECTED & TARGET_CM4) != 0

extern spim__Tools const_P	spim__tools1p2p22;
extern spim__Tools const_P	spim__tools2p1p19;
extern spim__Tools const_P	spim__tools2p4p20;
extern spim__Tools const_P	spim__tools2p1p31;
extern spim__Tools const_P	spim__dmaTools1p2p22;
extern spim__Tools const_P	spim__dmaTools2p1p19;
extern spim__Tools const_P	spim__dmaTools2p4p20;
extern spim__Tools const_P	spim__dmaTools2p1p31;

#  ifdef TARGETFAMILY_LPC178x_7x
extern spim__Tools const_P	spim__tools3p1p0;
extern spim__Tools const_P	spim__dmaTools3p1p0;
#  endif

# else /* LPC17xx */

extern spim__Tools const_P	spim__biTools1;
extern void spim_biSetPins1(Uint8);

# endif

#elif TARGET_SELECTED & TARGET_CM0

extern spim__Tools const_P	spim__tools1p0p6;
extern spim__Tools const_P	spim__tools1p1p29;
extern spim__Tools const_P	spim__tools2p1p20;
extern spim__Tools const_P	spim__tools2p1p20v2;
extern spim__Tools const_P	spim__tools2p1p27;

#elif TARGET_SELECTED & TARGET_SM4

extern spim__Tools const_P	spim__tools1pap5;
extern spim__Tools const_P	spim__tools2pbp13;
extern spim__Tools const_P	spim__tools3pbp3;
extern spim__Tools const_P	spim__tools3pcp10;

#elif TARGET_SELECTED & TARGET_S4F4

extern spim__Tools const_P	spim__tools1pap5;
extern spim__Tools const_P	spim__tools1NoConf;
extern spim__Tools const_P	spim__tools2pbp13;
extern spim__Tools const_P	spim__tools3pcp10;
extern spim__Tools const_P	spim__tools3pbp3;
extern spim__Tools const_P 	spim__tools3NoConf;

#elif TARGET_SELECTED & TARGET_S3F2

extern spim__Tools const_P	spim__tools1pap5;
extern spim__Tools const_P	spim__tools1NoConf;
extern spim__Tools const_P	spim__tools2pbp13;
extern spim__Tools const_P	spim__tools3pcp10;
extern spim__Tools const_P	spim__tools3pbp3;
extern spim__Tools const_P 	spim__tools3NoConf;

#endif

/***************************************************************//** \endcond */

#endif
