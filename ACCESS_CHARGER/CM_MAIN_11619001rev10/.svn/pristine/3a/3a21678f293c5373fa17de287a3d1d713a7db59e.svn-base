/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		LIB_SFE0.C
*
*	\ingroup	LIB
*
*	\brief		Functions to convert integer data to stream of bytes and vice
*				versa.
*
*	\details	By the conversion algorithm, two reserved byte values are not
*				written into the stream in order to allow e.g. a protocol to
*				use them in special meaning. 254 byte values remain available
*				and hence the name of the module "FE".
*
*				Data is written to and read from a stream as fields. A field
*				is an integer ranging from zero to a maximum value that can be
*				any integer value starting from one. Note that fields are not
*				limited to bit or byte boundaries of the stream.
*
*				The functions include:
*
*				lib_sFeInit()		Prepare a stream for writing/reading fields
*				lib_sFeWrite8()		Write a field of up to 8 bits into stream
*				lib_sFeWrite16()	Write a field of up to 16 bits into stream
*				lib_sFeRead8()		Read a field of up to 8 bits from stream
*				lib_sFeRead16()		Read a field of up to 16 bits from stream
*
*	\note		
*
*	\version	\$Rev: $ \n
*				\$Date:: $ \n
*				\$Author: $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "lib.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	lib_sFeWrite8
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Write a field of up to 8 bits into stream.
*
*	\param		pInfo		Pointer to the structure previously initialized.
*	\param		value		The field value. From zero to 'maxVal' (including).
*	\param		maxVal		The maximum valid value for the field, up to 0xFF.
*
*	\details	Write a value into stream of bytes. The value occupies a "field"
*				in the stream and can range from zero to 'maxVal'. The two
*				reserved byte codes (LIB_SFE_RESERVED_L and LIB_SFE_RESERVED_H)
*				are never written to the stream.
*
*				After each call to a write function the stream get updated thus
*				no special actions are needed to terminate the stream.
*
*	\return		TRUE is returned if the value was successfully written into
*				the stream. FALSE is returned indicating premature end of space
*				for the stream so nothing was written and no changes took place.
*
*	\note		The number of bytes written into the stream can be obtained by
*				subtracting pInfo->bufLen from the length of the buffer
*				previously given to lib_sFeInit().
*
*	\sa			lib_sFeInit
*
*******************************************************************************/

PUBLIC Boolean lib_sFeWrite8(
	lib_SFeInfo *			pInfo,
	Uint8					value,
	Uint8					maxVal
) {
	/*
	 *	Here "range" is the number of integers between the minimum and
	 *	the maximum value (including). The range of 'value' = maxVal + 1.
	 *	A range can be expected to be 2 or more because there would be no idea
	 *	in a field having only one (or zero) valid value.
	 *
	 *	A byte (having range = 254) can contain up to seven independent
	 *	values i.e. fields.
	 *
	 *		                                                value
	 *		Contents (min 0 and less than 1.0) of a field = -----
	 *		                                                range
	 *
	 *	Fields are stored so that the first written value locates in the least
	 *	significant bits of the byte. Therefore a value added into a byte is
	 *	multiplied by the previously used range (pInfo->bufUsed + 1).
	 */

	Uint8 range;
	Uint16 newRnge;

	range = pInfo->bufUsed;
	if (range >= 254 / 2) {

		/*
		 *	The remaining range is less than 2 thus current byte is considered
		 *	as full. Cannot fit still another field into this byte. The stream
		 *	pointer needs to be advanced and the "used range" count reset for
		 *	the next byte.
		 */

		pInfo->bufPtr++;
		pInfo->bufByte = 0;
		range = 0;
	}
	if (range == 0) {

		if (pInfo->bufLen < 1)
			return(FALSE);

		pInfo->bufLen--;
	}

	newRnge = ++range * (maxVal + (Uint16) 1);
	if (newRnge < 255) {

		/*
		 *	The field fits into the "current" byte (that may have a previous
		 *	field written into it).
		 */

		if (pInfo->bufLen < 1)
			return(FALSE);

		maxVal = (Uint8) newRnge - 1;

		value *= range;					/* Shift the new field into MSBits	*/
		value += pInfo->bufByte;		/* Keep previous field(s) in LSBits	*/

	} else {

		/*
		 *	The value cannot be fitted into the free space of the byte in
		 *	process. Splitting is required. An 8-bit value cannot span over
		 *	three bytes thus a single splitting will be enough. In integers
		 *	this is:
		 *		                 value_lo           value_hi
		 *		Splitted value = -------- + range * --------
		 *		                  range             range_hi
		 *		                \________/         \________/
		 *		                 1st byte           2nd byte
		 *	where
		 *
		 *		range = the remaining range (2..254) of the 1st byte
		 *
		 *		           value
		 *		value_hi = -----
		 *		           range
		 *
		 *		           maxVal
		 *		range_hi = ------ + 1
		 *		           range
		 *
		 *		value_lo = value - range * value_hi
		 */

		Uint8 val_lo;
		Uint8 rem_rnge;

		if (pInfo->bufLen < 2)
			return(FALSE);

							val_lo = value;
							rem_rnge = 254 / range;
		/* value_hi */		value /= rem_rnge;
		/* value_lo */		val_lo -= rem_rnge * value;
		/* range_hi - 1 */	maxVal /= rem_rnge;

		val_lo *= range;				/* Shift the new field into MSBits	*/
		val_lo += pInfo->bufByte;		/* Keep previous field(s) in LSBits	*/

		                   val_lo += val_lo >= LIB_SFE_RESERVED_L;
		*pInfo->bufPtr++ = val_lo + (val_lo >= LIB_SFE_RESERVED_H);
		pInfo->bufLen--;
	}
	pInfo->bufUsed = maxVal;
	pInfo->bufByte = value;

	                 value += value >= LIB_SFE_RESERVED_L;
	*pInfo->bufPtr = value + (value >= LIB_SFE_RESERVED_H);

	return(TRUE);
}
