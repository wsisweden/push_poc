function PlotCluster(data)%filename)
	%data = readCanTrace(filename);
	
	PlotNodes(data)
	PlotStates(data)
endfunction

function PlotNodes(data)
	NodeIds = unique(rem([data.CobId], 0x80));
	
	for n = 1:length(NodeIds)
		if(NodeIds(n) == 0 || NodeIds(n) == 127)
			continue
		endif
		
		COB_TPDO1 = 0x180;
		COB_TPDO2 = 0x180;
		COB_RPDO1 = 0x200;
		COB_RPDO2 = 0x300;

		idx1 = find([data.CobId] == COB_TPDO1 + NodeIds(n));

		if(length(idx1) > 0)
			tMeas = [data(idx1).t]/1e3;
			Umeas = CanVect2single([data(idx1).data](1:4,:));
			Imeas = CanVect2single([data(idx1).data](5:8,:));
		endif

		idx2 = find([data.CobId] == COB_RPDO1 + NodeIds(n));
		if(length(idx2) > 0)
			tSet = [data(idx2).t]/1e3;
			Uset = CanVect2single([data(idx2).data](1:4,:));
			Iset = CanVect2single([data(idx2).data](5:8,:));
		endif

		if(length(idx1) > 0 || length(idx2) > 0)
			h = figure;
			maxLen = max(length(idx1), length(idx2));
			tMeas(end:maxLen) = NaN;
			tSet(end:maxLen) = NaN;
			Umeas(end:maxLen) = NaN;
			Uset(end:maxLen) = NaN;
			Imeas(end:maxLen) = NaN;
			Iset(end:maxLen) = NaN;
			
			if(length(idx1) > 0 && length(idx2) > 0)
				plot([tMeas' tSet'], [Umeas' Uset']);
				legend("Umeas in (V)", "Uset")
				title(sprintf("Charger node ID %u", NodeIds(n)))
				xlabel("t in (s)")
				ylabel("u in (V)")
				ylabel("i in (A)")
				
				figure
				plot([tMeas' tSet'], [Imeas' Iset']);
				legend("Imeas in (A)", "Iset")
				title(sprintf("Charger node ID %u", NodeIds(n)))
				xlabel("t in (s)")
				ylabel("u in (V)")
				ylabel("i in (A)")
			elseif(length(idx1) > 0)
				[ax h1 h2] = plotyy([tMeas'], [Umeas'], [tMeas'], [Imeas']);
				legend("Umeas", "Imeas")
			elseif(length(idx2) > 0)
				[ax h1 h2] = plotyy([tSet'], [Uset'], [tSet'], [Iset']);
				legend("Uset", "Iset")
			endif

		endif
	endfor
endfunction

function PlotStates(data)
	idx = find([data.CobId] == 0x27F);
	t = [data(idx).t]/1e3;
	states = [1 256]*[data(idx).data](1:2,:);

	h = figure;
	plot(t, states);
	legend("States")
	title("Charging curve state numbers")
	xlabel("t in (s)")
	ylabel("State number in (n)")
endfunction
