/*
 * NodesStatusAndMapping.h
 *
 *  Created on: Aug 13, 2010
 *      Author: nicka
 */

#ifndef NODESSTATUSANDMAPPING_H_
#define NODESSTATUSANDMAPPING_H_

#define NODES_STATUS_LEN 10

#include "MicroCANopenPlus/MCO_DS401_LPC1768/types.h"
/* Keep track of mapping between serial number and Node Id. Two nodes should never have the same node ID but it might happen they have the
 * same serial number */

struct NodeInformation_type {
	volatile UNSIGNED8 heartbeatStatus;
	volatile UNSIGNED8 NodeId;				// NodeId == 0 => NodeId unknown

	volatile UNSIGNED32 VendorId;			// Vendor identification assigned by CAN in Automation
	volatile UNSIGNED32 ProductCode;		// Product code
	volatile UNSIGNED32 RevisionNr;			// Revision number
	volatile UNSIGNED32 SerialNr;			// SerialNr == 0 => serial number unknown
};

/* Functions used to add and remove nodes */
int CAN_NodeStatusAndMapping_AddNode(UNSIGNED8 NodeId);										// Add a node to the list of known nodes. If the node is already known zero is returned and otherwise nonzero
int CAN_NodeStatusAndMapping_GetNext();														// Iterate thru the registered nodes
void CAN_NodeStatusAndMapping_RemoveNode(UNSIGNED8 NodeId);									// Remove a node
void CAN_NodeStatusAndMapping_Reset();														// Remove all nodes

/* Functions used to keep the information up to date, received via
 * SDO so node ID of sender is known. */
void CAN_NodeStatusAndMapping_Heartbeat(UNSIGNED8 NodeId, UNSIGNED8 Heartbeat);				// Should be called then vendor id received
void CAN_NodeStatusAndMapping_SetVendorId(UNSIGNED8 NodeId, UNSIGNED32 VendorId);			// Should be called then vendor id received
void CAN_NodeStatusAndMapping_SetProductCode(UNSIGNED8 NodeId, UNSIGNED32 ProductCode);		// Should be called then product code received
void CAN_NodeStatusAndMapping_SetRevisionNr(UNSIGNED8 NodeId, UNSIGNED32 RevisionNr);		// Should be called then revision number received
void CAN_NodeStatusAndMapping_SetSerialNumber(UNSIGNED8 NodeId, UNSIGNED32 SerialNr);		// Should be called then serial number received

/* Useful functions to get information, adjust according to need */
UNSIGNED32 CAN_NodeStatusAndMapping_GetSerialNr(UNSIGNED8 NodeId);
UNSIGNED8 CAN_NodeStatusAndMapping_GetNodeId(UNSIGNED32 SerialNr);
volatile struct NodeInformation_type* CAN_NodeStatusAndMapping_GetNodes();

#endif /* NODESSTATUSANDMAPPING_H_ */
