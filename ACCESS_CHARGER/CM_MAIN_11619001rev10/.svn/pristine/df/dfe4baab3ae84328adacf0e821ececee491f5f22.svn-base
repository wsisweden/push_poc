/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		perf.h
*
*	\ingroup	NTCPIP
*
*	\brief		Performance measuring macros.
*
*	\details
*
*	\note		Can be implemented with timestamps.
*
*	\version
*
*******************************************************************************/

#ifndef PERF_H_INCLUDED
#define PERF_H_INCLUDED

/*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "tstamp.h"
#include "deb.h"

/*******************************************************************************
;
;	COMPILER CONTROLS (etc.)
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	CONSTANTS / DEFINITIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	MACROS
;
;-----------------------------------------------------------------------------*/

#if 1
#define NTCPIP__PERF_START

#define NTCPIP__PERF_STOP(X_)

#else

#define NTCPIP__PERF_START						\
{												\
	tstamp_Stamp stamp1;						\
	Uint16 diff;								\
	tstamp_getStamp(&stamp1);


#define NTCPIP__PERF_STOP(X_)					\
	tstamp_getDiffCurr(&stamp1, &diff);			\
	deb_logN("%s took %u ms\n\r", X_, diff);	\
}

#endif

/*******************************************************************************
;
;	DATA TYPES / STRUCTURES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	FUNCTIONS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	DATA
;
;-----------------------------------------------------------------------------*/

/******************************************************************************/

#endif
