/*
 $Revision: 155 $
 $Date: 2010-05-17 10:04:04 +0200 (må, 17 maj 2010) $
 $Author: bsi $
 $HeadURL: https://subversion.mpi.local/svn/micropower/_user/bsi/alg/alg_types.h $
 */

/**************************************************************************
 Project:     ACCESS:CM
 Module:      algdef_type.h
 Contains:    Type definitions for constant defined in algorithm.
 Environment: Compiled and tested with GNU GCC.
 Copyright:   Micropower ED
 Version:
 ---------------------------------------------------------------------------
 History:
 Rev      Date      Sign  Description
 bsi   First version.cai_constparam.h
 ***************************************************************************/


#ifndef ALG_TYPES
#define ALG_TYPES

typedef signed char         int8;
typedef signed short        int16;
typedef signed int          int32;
typedef signed long long    int64;
typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned int 		uint32;
typedef unsigned long long  uint64;

// Just for lint
#if 0
typedef unsigned short      Uint16;
typedef unsigned int 		Uint32;
#endif

#include "cai_idmodule.h"
#include <stdbool.h>

#define FALSE 0
#define TRUE 1



// AlgDef
// ======
typedef struct {
  uint32 AlgDef_ConstPar_Addr;
  uint32 Identity_ConstPar_Addr;
  uint32 Mandatory_ConstPar_Addr;
  uint32 Module_ConstPar_Addr;
} AlgDef_ConstPar_Type;


// Identity
// ========
typedef struct {
  uint16 IdNumber;
  uint16 Version;
  uint16 InterpretVer;
  struct {
	uint32 Addr;
	uint16 Size;
  } Name;
} Identity_ConstPar_Type;


// Mandatory
// =========
typedef struct {
  struct {
	uint32 Addr;
	uint16 Points;
  } Power;
  struct {
	uint32 Addr;
	uint16 Points;
  } Iaccuracy;
  struct {
	uint32 Addr;
	uint16 Points;
  } Uaccuracy;
  uint32 IOsignals_Addr;
  struct {
	uint32 Addr;
	uint16 Size;
  } UserParam;
} Mandatory_ConstPar_Type;


// Module
// ======
typedef struct {
  void* Addr;
  uint16 Phases;
} Module_ConstPar_Type;

// All modules have the elements Phase and Ctrl in this order.
typedef struct {
  uint16 Phase;
  uint32 Ctrl;
} Module_PhaseCtrl_Type;


// Ctrl standard definition
// ------------------------
// Nibble   Description
// 0        Standard definition for modules
// 1..2     Standard definition for Break modules
// 3..7     Module specific

// ** Execute **
// Nibble 0,
#define EXE_CONT       0x1  // Continue
#define EXE_INIT       0x2  // Initialize
#define EXE_CHECK      0x4  // Check condition
#define EXE_BRANCH     0x8  // Branch Active

// ** Init ** (applicable only for break)
// Nibble 1, Comparator
#define CMP_MASK      0xF0
#define CMP_GR        0x00
#define CMP_GREQ      0x10
#define CMP_EQ        0x20
#define CMP_LEEQ      0x30
#define CMP_LE        0x40
#define CMP_NOEQ      0x50
// Nibble 2, Operator
#define OP_MASK      0x300
#define OP_NO        0x000  // Use Param1 direct
#define OP_PLU       0x100  // Plus
#define OP_MIN       0x200  // Minus
#define OP_MUL       0x300  // Multiplication


#define REGUMODE_ENUM_MASK             0xF
#define REGUMODE_VOLTAGE_DETECTION    0x10
#define REGUMODE_START_REMOTEIN       0x20
#define REGUMODE_START_MEAS           0x40
#define REGUMODE_STOP_REMOTEIN        0x80
#define REGUMODE_STOP_MEAS           0x100

// RegSet
// ------
// Ctrl.Nibble 3           76543210
#define REGSET_U_USER        0x1000
#define REGSET_I_USER        0x2000
#define REGSET_P_USER        0x4000
// Ctrl.Nibble 4           76543210
#define REGSET_U_ACT        0x10000
#define REGSET_I_ACT        0x20000
#define REGSET_P_ACT        0x40000
// Ctrl.Nibble 5
#define REGSET_MODE_LSB 20
// Ctrl.Nibble 6           76543210
#define REGSET_MODE_MASK   0xF00000
#define REGSET_MODE_OFF    0x100000
#define REGSET_MODE_ON     0x200000
#define REGSET_MODE_AUTO   0x300000
typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Uset;
  float Iset;
  float Pset;
} RegSet_ConstParam_Type;

typedef struct {
	float U;
	float I;
	float P;
} RegSetParam_Type;
RegSetParam_Type RegSetParam;

// RegWin_ConstParam_Type
// ----------------------
// Ctrl.Nibble 3           76543210
#define REGWIN_USER_UERR     0x1000
#define REGWIN_USER_IERR     0x2000
#define REGWIN_USER_PERR     0x4000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Uerr;
  float Ierr;
  float Perr;
} RegWin_ConstParam_Type;


// Ctrl for Break-module
// =====================
// UbattWinBreak
// -------------
// Ctrl:
// IcompTbatt_ConstParam_Type
// --------------------------
// Ctrl.Nibble 3                   76543210
#define UBATTWIN_USER_ULOW           0x1000
#define UBATTWIN_USER_UHIGH          0x2000
// Nibble 4 Check Condition        76543210
#define UBATTWIN_CHECK_BRVA_MASK    0x30000
#define UBATTWIN_CHECK_BRVA_FALS    0x00000
#define UBATTWIN_CHECK_BRVA_TRUE    0x10000
#define UBATTWIN_CHECK_BRVA_EGAL    0x20000

// UbattWinBreak_ConstParam_Type
// -----------------------------
typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Ulow;
  float Uhigh;
  uint16 BranchPhase;
} UbattWinBreak_ConstParam_Type;
typedef struct {
	idModule_enum id;
	float Ulow;
	float Uhigh;
} UbattWinBreak_Type;
extern UbattWinBreak_Type UbattWinBreak[];

// UbattBreak
// ----------
// Ctrl:
// Nibble 3 Param0                 76543210
#define UBB_P0_MASK                  0x3000
#define UBB_P0_BREA                  0x0000
#define UBB_P0_UBAT                  0x1000
#define UBB_P0_CELL                  0x2000
// Nibble 4 Param0                 76543210
#define UBB_USER_PARAM1             0x10000
// Nibble 5 Check Condition        76543210
#define UBB_CHECK_BRVA_MASK        0x300000
#define UBB_CHECK_BRVA_FALS        0x000000
#define UBB_CHECK_BRVA_TRUE        0x100000
#define UBB_CHECK_BRVA_EGAL        0x200000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} UbattBreak_ConstParam_Type;


// IbattBreak
// ----------
// Ctrl:
// Nibble 3 Param0                 76543210
#define IBB_P0_MASK                  0x3000
#define IBB_P0_BREA                  0x0000
#define IBB_P0_IBAT                  0x1000
#define IBB_P0_CAPA                  0x2000
// Nibble 4 Param0                 76543210
#define IBB_USER_PARAM1             0x10000
// Nibble 5 Check Condition        76543210
#define IBB_CHECK_BRVA_MASK        0x300000
#define IBB_CHECK_BRVA_FALS        0x000000
#define IBB_CHECK_BRVA_TRUE        0x100000
#define IBB_CHECK_BRVA_EGAL        0x200000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} IbattBreak_ConstParam_Type;


// PbattBreak
// ----------
// Ctrl:
// Nibble 3 Param0                 76543210
#define PBB_P0_MASK                  0x3000
#define PBB_P0_BREA                  0x0000
#define PBB_P0_IBAT                  0x1000
#define PBB_P0_CECA                  0x2000
// Nibble 4 Param0                 76543210
#define PBB_USER_PARAM1             0x10000
// Nibble 5 Check Condition        76543210
#define PBB_CHECK_BRVA_MASK        0x300000
#define PBB_CHECK_BRVA_FALS        0x000000
#define PBB_CHECK_BRVA_TRUE        0x100000
#define PBB_CHECK_BRVA_EGAL        0x200000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} PbattBreak_ConstParam_Type;


// Ctrl TbattBreak
// ---------------
// Ctrl:
// Nibble 3 Param0                 76543210
#define TBB_P0_MASK                  0x3000
#define TBB_P0_BREA                  0x0000
#define TBB_P0_TBAT                  0x1000
// Nibble 4 Param0                 76543210
#define TBB_USER_PARAM1             0x10000
// Nibble      Check Condition     76543210
#define TBB_CHECK_BRVA_MASK        0x300000
#define TBB_CHECK_BRVA_FALS        0x000000
#define TBB_CHECK_BRVA_TRUE        0x100000
#define TBB_CHECK_BRVA_EGAL        0x200000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} TbattBreak_ConstParam_Type;


// UbattDeltaBreak
// --------------
// Ctrl:
// Nibble 3 Param0                 76543210
#define UDB_P0_MASK                  0x3000
#define UDB_P0_BREA                  0x0000
#define UDB_P0_UBAT                  0x1000
// Nibble 4 Check Condition        76543210
#define UDB_CHECK_BRVA_MASK         0x30000 // Bit 1..0 in nibble 4
#define UDB_CHECK_BRVA_FALS         0x00000
#define UDB_CHECK_BRVA_TRUE         0x10000
#define UDB_CHECK_BRVA_EGAL         0x20000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} UbattDeltaBreak_type;

/*// IbattDeltaBreak			// dI/dt
// --------------
// Ctrl:
// Nibble 3 Param0                 76543210
#define IDB_P0_MASK                  0x3000
#define IDB_P0_BREA                  0x0000
#define IDB_P0_IBAT                  0x1000
#define IDB_P0_CAPA                  0x2000
// Nibble 4 Check Condition        76543210
#define IDB_CHECK_BRVA_MASK         0x30000
#define IDB_CHECK_BRVA_FALS         0x00000
#define IDB_CHECK_BRVA_TRUE         0x10000
#define IDB_CHECK_BRVA_EGAL         0x20000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} IbattDeltaBreak_type;*/

// Ctrl TimerBreak
// ---------------
// Nibble 3 Param0                 76543210
#define TIB_P0_MASK                  0xF000
#define TIB_P0_BREA                  0x0000
#define TIB_P0_TIM0                  0x1000
#define TIB_P0_TIM1                  0x2000
#define TIB_P0_TIM2                  0x3000
#define TIB_P0_AH0C                  0x4000
#define TIB_P0_AH1C                  0x5000
// Nibble 4 Param0                 76543210
#define TIB_USER_PARAM1             0x10000
// Nibble 5 Check Condition        76543210
#define TIB_CHECK_BRVA_MASK        0x300000
#define TIB_CHECK_BRVA_FALS        0x000000
#define TIB_CHECK_BRVA_TRUE        0x100000
#define TIB_CHECK_BRVA_EGAL        0x200000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} TimerBreak_ConstParam_Type;

// Ctrl AhBreak
// ---------------
// Nibble 3 Param0                 76543210
#define AHB_P0_MASK                  0xF000
#define AHB_P0_BREA                  0x0000
#define AHB_P0_AH0                   0x1000
#define AHB_P0_AH1                   0x2000
#define AHB_P0_CAPA                  0x3000
// Nibble 4 Param0                 76543210
#define AHB_USER_PARAM1             0x10000
// Nibble 5 Check Condition        76543210
#define AHB_CHECK_BRVA_MASK        0x300000
#define AHB_CHECK_BRVA_FALS        0x000000
#define AHB_CHECK_BRVA_TRUE        0x100000
#define AHB_CHECK_BRVA_EGAL        0x200000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} AhBreak_ConstParam_Type;

// Ctrl WhBreak
// ---------------
// Nibble 3 Param0                 76543210
#define WHB_P0_MASK                 0xF0000
#define WHB_P0_BREA                 0x00000
#define WHB_P0_WH0                  0x10000
#define WHB_P0_WH1                  0x20000
#define WHB_P0_CECA                 0x30000
// Nibble 4 Param0                 76543210
#define WHB_USER_PARAM1             0x10000
// Nibble 5 Check Condition        76543210
#define WHB_CHECK_BRVA_MASK        0x300000
#define WHB_CHECK_BRVA_FALS        0x000000
#define WHB_CHECK_BRVA_TRUE        0x100000
#define WHB_CHECK_BRVA_EGAL        0x200000

typedef struct WhBreak_tag {
  uint16 Phase;
  uint32 Ctrl;
  float Param1;
  uint16 BranchPhase;
} WhBreak_ConstParam_Type;

// Delta
// -----
// Nibble 4 Param0                 76543210
#define DELTA_USER_PERIOD           0x10000

typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  uint32 Period;
} Delta_ConstParam_Type;

// UcompTbatt_ConstParam_Type
// --------------------------
// Ctrl.Nibble 3                 76543210
#define UCOMPTBATT_USER_LOW        0x1000
#define UCOMPTBATT_USER_NEUTRAL    0x2000
#define UCOMPTBATT_USER_HIGH       0x4000
#define UCOMPTBATT_USER_SLOPE      0x8000
typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  float Low;
  float Neutral;
  float High;
  float Slope;
} UcompTbatt_ConstParam_Type;

// IcompTbatt_ConstParam_Type
// --------------------------
// Ctrl.Nibble 3                    76543210
#define ICOMPTBATT_USER_COLDLOW       0x1000
#define ICOMPTBATT_USER_COLDHIGH      0x2000
#define ICOMPTBATT_USER_WARMLOW       0x4000
#define ICOMPTBATT_USER_WARMHIGH      0x8000
typedef struct IcompTbatt_tag {
  uint16 Phase;
  uint32 Ctrl;
  float ColdLow;
  float ColdHigh;
  float WarmLow;
  float WarmHigh;
} IcompTbatt_ConstParam_Type;






// Counter_type
// ------------
// Nibble  4..1                    76543210
#define TI0_RES                        0x10
#define TI0_RUN                        0x20
#define TI1_RES                        0x40
#define TI1_RUN                        0x80
#define TI2_RES                       0x100
#define TI2_RUN                       0x200
#define AH0_RES                       0x400
#define AH0_RUN                       0x800
#define AH1_RES                      0x1000
#define AH1_RUN                      0x2000
#define WH0_RES                      0x4000
#define WH0_RUN                      0x8000
#define WH1_RES                     0x10000
#define WH1_RUN                     0x20000

typedef struct Counter_tag {
  uint16 Phase;
  uint32 Ctrl;
} Counter_type;







typedef struct Power_tag {
  float VpC;
  float Capacity;
} Power_type;

typedef struct Uaccuracy_t {
  float VpC;
  float Accuracy;
} Uaccuracy_type;

typedef struct {
  float Capacity;
  float Accuracy;
} Iaccuracy_type;

#define IO_Tbatt    0x1
#define IO_AirPump  0x2
#define IO_Water    0x4



// UbattBreak_type, IbattBreak_type, PbattBreak_type Ctrl
// -------------------------------------------------
// Nibble 6                         76543210
#define REGU_CHECK_UERR_MASK       0x3000000 // Bit 1..0
#define REGU_CHECK_UERR_FALS       0x0000000
#define REGU_CHECK_UERR_TRUE       0x1000000
#define REGU_CHECK_UERR_EGAL       0x2000000
#define REGU_CHECK_IERR_MASK       0xC000000 // Bit 3..2
#define REGU_CHECK_IERR_FALS       0x0000000
#define REGU_CHECK_IERR_TRUE       0x4000000
#define REGU_CHECK_IERR_EGAL       0x8000000
// Nibble 7                         76543210
#define REGU_CHECK_PERR_MASK      0x30000000 // Bit 3..2
#define REGU_CHECK_PERR_FALS      0x00000000
#define REGU_CHECK_PERR_TRUE      0x10000000
#define REGU_CHECK_PERR_EGAL      0x20000000


// ReguModeBreak_ConstParam_Type
// -----------------------------
// Nibble 3    Check Condition      76543210
#define REGU_VOLTAGE_MODE_MASK        0x3000 // Bit 1..0 in nibble 3
#define REGU_VOLTAGE_MODE_TRUE        0x0000
#define REGU_VOLTAGE_MODE_FALS        0x1000
#define REGU_VOLTAGE_MODE_EGAL        0x2000
#define REGU_CURRENT_MODE_MASK        0xC000 // Bit 3..2 in nibble 3
#define REGU_CURRENT_MODE_TRUE        0x0000
#define REGU_CURRENT_MODE_FALS        0x4000
#define REGU_CURRENT_MODE_EGAL        0x8000
// Nibble 4    Check Condition      76543210
#define REGU_POWER_MODE_MASK         0x30000 // Bit 1..0 in nibble 4
#define REGU_POWER_MODE_TRUE         0x00000
#define REGU_POWER_MODE_FALS         0x10000
#define REGU_POWER_MODE_EGAL         0x20000
#define REGU_UERRWIN_MASK            0xC0000 // Bit 3..2 in nibble 4
#define REGU_UERRWIN_TRUE            0x00000
#define REGU_UERRWIN_FALS            0x40000
#define REGU_UERRWIN_EGAL            0x80000
// Nibble 5    Check Condition      76543210
#define REGU_IERRWIN_MASK           0x300000 // Bit 1..0 in nibble 5
#define REGU_IERRWIN_TRUE           0x000000
#define REGU_IERRWIN_FALS           0x100000
#define REGU_IERRWIN_EGAL           0x200000
#define REGU_PERRWIN_MASK           0xC00000 // Bit 3..2 in nibble 5
#define REGU_PERRWIN_TRUE           0x000000
#define REGU_PERRWIN_FALS           0x400000
#define REGU_PERRWIN_EGAL           0x800000
typedef struct {
	  uint16 Phase;
	  uint32 Ctrl;
	  uint16 BranchPhase;
} ReguModeBreak_ConstParam_Type;

// ReguDerateBreak_ConstParam_Type
// -------------------------------
// Nibble 3    Check Condition      76543210
#define REGU_DERATE_TEMP_MASK         0x3000 // Bit 1..0 in nibble 3
#define REGU_DERATE_TEMP_TRUE         0x0000
#define REGU_DERATE_TEMP_FALS         0x1000
#define REGU_DERATE_TEMP_EGAL         0x2000
#define REGU_DERATE_USET_MASK         0xC000 // Bit 3..2 in nibble 3
#define REGU_DERATE_USET_TRUE         0x0000
#define REGU_DERATE_USET_FALS         0x4000
#define REGU_DERATE_USET_EGAL         0x8000
// Nibble 4    Check Condition      76543210
#define REGU_DERATE_ISET_MASK        0x30000 // Bit 1..0 in nibble 4
#define REGU_DERATE_ISET_TRUE        0x00000
#define REGU_DERATE_ISET_FALS        0x10000
#define REGU_DERATE_ISET_EGAL        0x20000
#define REGU_DERATE_PSET_MASK        0xC0000 // Bit 3..2 in nibble 4
#define REGU_DERATE_PSET_TRUE        0x00000
#define REGU_DERATE_PSET_FALS        0x40000
#define REGU_DERATE_PSET_EGAL        0x80000
// Nibble 5    Check Condition      76543210
#define REGU_DERATE_SUM_MASK        0x300000 // Bit 1..0 in nibble 5
#define REGU_DERATE_SUM_TRUE        0x000000
#define REGU_DERATE_SUM_FALS        0x100000
#define REGU_DERATE_SUM_EGAL        0x200000
typedef struct {
	  uint16 Phase;
	  uint32 Ctrl;
	  uint16 BranchPhase;
} ReguDerateBreak_ConstParam_Type;


// ReguEngineBreak
// ---------------
// Nibble 3    Check Condition           76543210
#define REGU_OVER_TEMP_MASK                0x3000 // Bit 1..0 in nibble 3
#define REGU_OVER_TEMP_TRUE                0x0000
#define REGU_OVER_TEMP_FALS                0x1000
#define REGU_OVER_TEMP_EGAL                0x2000
#define REGU_OVER_VOLT_MASK                0xC000 // Bit 3..2 in nibble 3
#define REGU_OVER_VOLT_TRUE                0x0000
#define REGU_OVER_VOLT_FALS                0x4000
#define REGU_OVER_VOLT_EGAL                0x8000
// Nibble 4    Check Condition           76543210
#define REGU_OVER_CURR_MASK               0x30000 // Bit 1..0 in nibble 4
#define REGU_OVER_CURR_TRUE               0x00000
#define REGU_OVER_CURR_FALS               0x10000
#define REGU_OVER_CURR_EGAL               0x20000
#define REGU_OVER_POWER_MASK              0xC0000 // Bit 3..2 in nibble 4
#define REGU_OVER_POWER_TRUE              0x00000
#define REGU_OVER_POWER_FALS              0x40000
#define REGU_OVER_POWER_EGAL              0x80000
// Nibble 5    Check Condition           76543210
#define REGU_NO_RATED_OUTPUT_MASK        0x300000 // Bit 1..0 in nibble 5
#define REGU_NO_RATED_OUTPUT_TRUE        0x000000
#define REGU_NO_RATED_OUTPUT_FALS        0x100000
#define REGU_NO_RATED_OUTPUT_EGAL        0x200000
#define REGU_NO_OUTPUT_MASK              0xC00000 // Bit 3..2 in nibble 5
#define REGU_NO_OUTPUT_TRUE              0x000000
#define REGU_NO_OUTPUT_FALS              0x400000
#define REGU_NO_OUTPUT_EGAL              0x800000
typedef struct {
	  uint16 Phase;
	  uint32 Ctrl;
	  uint16 BranchPhase;
} ReguEngineBreak_ConstParam_Type;


// InputBreak
// -----------
// spare
// Nibble 3    Check Condition          76543210
#define INPU_CHECK_STOP_MASK              0x3000 // Bit 1..0 in nibble 3
#define INPU_CHECK_STOP_FALS              0x0000
#define INPU_CHECK_STOP_TRUE              0x1000
#define INPU_CHECK_STOP_EGAL              0x2000
#define INPU_CHECK_F1_MASK                0xC000 // Bit 3..2 in nibble 3
#define INPU_CHECK_F1_FALS                0x0000
#define INPU_CHECK_F1_TRUE                0x4000
#define INPU_CHECK_F1_EGAL                0x8000
// Nibble 4    Check Condition          76543210
#define INPU_CHECK_F2_MASK               0x30000 // Bit 1..0 in nibble 4
#define INPU_CHECK_F2_FALS               0x00000
#define INPU_CHECK_F2_TRUE               0x10000
#define INPU_CHECK_F2_EGAL               0x20000
#define INPU_CHECK_REMOTEIN_MASK         0xC0000 // Bit 3..2 in nibble 4
#define INPU_CHECK_REMOTEIN_FALS         0x00000
#define INPU_CHECK_REMOTEIN_TRUE         0x40000
#define INPU_CHECK_REMOTEIN_EGAL         0x80000
// Nibble 5    Check Condition          76543210
#define INPU_CHECK_WATERLOW_MASK        0x300000 // Bit 1..0 in nibble 5
#define INPU_CHECK_WATERLOW_FALS        0x000000
#define INPU_CHECK_WATERLOW_TRUE        0x100000
#define INPU_CHECK_WATERLOW_EGAL        0x200000
#define INPU_CHECK_CAIIN_MASK           0xC00000 // Bit 3..2 in nibble 5
#define INPU_CHECK_CAIIN_FALS           0x000000
#define INPU_CHECK_CAIIN_TRUE           0x400000
#define INPU_CHECK_CAIIN_EGAL           0x800000
// Nibble 6    Check Condition          76543210
#define INPU_CHECK_MANEQUON_MASK       0x3000000 // Bit 1..0 in nibble 6
#define INPU_CHECK_MANEQUON_FALS       0x0000000
#define INPU_CHECK_MANEQUON_TRUE       0x1000000
#define INPU_CHECK_MANEQUON_EGAL       0x2000000
#define INPU_CHECK_MANEQUOFF_MASK       0xC000000 // Bit 3..2 in nibble 6
#define INPU_CHECK_MANEQUOFF_FALS       0x0000000
#define INPU_CHECK_MANEQUOFF_TRUE       0x4000000
#define INPU_CHECK_MANEQUOFF_EGAL       0x8000000
// Nibble 7    Check Condition          76543210
#define INPU_CHECK_IOSPARE3_MASK      0x30000000 // Bit 1..0 in nibble 7
#define INPU_CHECK_IOSPARE3_FALS      0x00000000
#define INPU_CHECK_IOSPARE3_TRUE      0x10000000
#define INPU_CHECK_IOSPARE3_EGAL      0x20000000
#define INPU_CHECK_IOSPARE4_MASK      0xC0000000 // Bit 3..2 in nibble 7
#define INPU_CHECK_IOSPARE4_FALS      0x00000000
#define INPU_CHECK_IOSPARE4_TRUE      0x40000000
#define INPU_CHECK_IOSPARE4_EGAL      0x80000000


typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  uint16 BranchPhase;
} InputBreak_ConstParam_Type;




// RegOn/Off
// ---------
// Nibble 3 spare
// Nibble 4 spare
#define REGOO_A_UBATT  0x10000
#define REGOO_A_TBATT  0x20000
#define REGOO_A_REMIN  0x40000
// Nibble 5 spare
// Nibble 6 spare
#define REGOO_V_REMIN  0x4000000
// Nibble 7 spare
typedef struct RegOnOff_tag {
  uint16 Phase;
  uint32 Ctrl;
  float Ulow;
  float Uhigh;
  float Tlow;
  float Thigh;
} RegOnOff_type;


#define AND_BM0_ACT  0x010000
#define AND_BM1_ACT  0x020000
#define AND_BM2_ACT  0x040000
#define AND_BM3_ACT  0x080000
typedef struct {
  uint16 Phase;
  uint32 Ctrl;
  uint8 BreakModule[4];
  uint16 BranchPhase;
} AndBreak_type;


// Output_ConstParam_Type
// ----------------------
// Bit definition for value, see CaiOutput.Output
typedef struct Output_tag {
  uint16 Phase;
  uint32 Ctrl;
  uint32 Value;
} Output_ConstParam_Type;


// ChargingStage_Param_type
// ------------------------
// Bit definition in Module_type.Status
#define CHARGING_STAGE_CTRL_VALIDATA       0x80000000   // To know when it is the first stage
#define CHARGING_STAGE_CTRL_STAGENO_MASK   0xFF         // Mask for Stage number

typedef struct ChargingStage_Param_tag {
  uint16 Phase;
  uint16 NextPhase;
  uint32 Ctrl;
} ChargingStage_Param_type;
// } ChargingStage_ConstParam_type;


// ReguStartStop_Type
// ------------------
// Ctrl.Nibble 3 UserParam                 76543210
#define REGUONOFF_USER_UBATT_LOW             0x1000
#define REGUONOFF_USER_UBATT_HIGH            0x2000
// Ctrl.Nibble 4 Condition                 76543210
#define REGUONOFF_COND_UACT_MASK            0xF0000
#define REGUONOFF_COND_UACT_NOTUSED         0x00000
#define REGUONOFF_COND_UACT_FALSE           0x10000
#define REGUONOFF_COND_UACT_TRUE            0x20000
// Ctrl.Nibble 5 Condition                 76543210
#define REGUONOFF_COND_REMOTEIN_MASK       0xF00000
#define REGUONOFF_COND_REMOTEIN_NOTUSED    0x000000
#define REGUONOFF_COND_REMOTEIN_FALSE      0x100000
#define REGUONOFF_COND_REMOTEIN_TRUE       0x200000
// Ctrl.Nibble 6 Condition                 76543210
#define REGUONOFF_COND_CAIN_MASK          0xF000000
#define REGUONOFF_COND_CAIN_NOTUSED       0x0000000
#define REGUONOFF_COND_CAIN_FALSE         0x1000000
#define REGUONOFF_COND_CAIN_TRUE          0x2000000

// ChargingInfo_type
// -----------------
// Nibble 5..3                      76543210
#define CHRG_INFO_MASK              0x1FF000
#define CHRG_INFO_BATT_CONN           0x1000
#define CHRG_INFO_PRE                 0x2000
#define CHRG_INFO_MAIN                0x4000
#define CHRG_INFO_COMP                0x8000
#define CHRG_INFO_TRIC               0x10000
#define CHRG_INFO_EQUA               0x20000
#define CHRG_INFO_REDY               0x40000
#define CHRG_INFO_TELO               0x80000
#define CHRG_INFO_TEHI              0x100000

typedef struct ChargingInfo_tag {
  uint16 Phase;
  uint32 Ctrl;
} ChargingInfo_Param_type;


// ChargingError_type
// ------------------
// Nibble 5..3                      76543210
#define CHRG_ERRO_MASK              0xFFF000	// Changed because of new error - it was 0x7FF000
#define CHRG_ERRO_VOLO                0x1000
#define CHRG_ERRO_VOHI                0x2000
#define CHRG_ERRO_TELO                0x4000
#define CHRG_ERRO_TEHI                0x8000
#define CHRG_ERRO_AHMA               0x10000
#define CHRG_ERRO_PHMA               0x20000
#define CHRG_ERRO_DU                 0x40000
#define CHRG_ERRO_DI                 0x80000
#define CHRG_ERRO_DP                0x100000
#define CHRG_ERRO_DT                0x200000
#define CHRG_ERRO_TM                0x400000
#define CHRG_ERRO_APF               0x800000	// Air pump failure added
typedef struct ChargingError_tag {
  uint16 Phase;
  uint32 Ctrl;
} ChargingError_Param_type;


typedef enum {
	UserParam_Unit_VoltPerCell,
	UserParam_Unit_Capacity,
	UserParam_Unit_CellMulCap,
	UserParam_Unit_Second,
	UserParam_Unit_Minute,
	UserParam_Unit_Hour,
	UserParam_Unit_Percent,
	UserParam_Unit_Integer,
	UserParam_Unit_Ah,

	UserParam_Unit_size
} UserParam_Unit_enum;

typedef enum {
	UserParam_Operator_No,
	UserParam_Operator_Lesser,
	UserParam_Operator_Greather,
	UserParam_Operator_Inside,

	UserParam_Operator_size
} UserParam_Operator_enum;

typedef struct {
	const char*  Name;
	uint32 NameSize;
	float Default;
	float Min;
	float Max;
	UserParam_Unit_enum Unit;
	UserParam_Operator_enum Operator;
	uint8 ParA;
	uint8 ParB;
} UserParam_ConstParam_Type;

typedef enum {
	UserParam_Error_NoParDefined,
	UserParam_Error_No,
	UserParam_Error_Min,
	UserParam_Error_Max,
	UserParam_Error_Inside,
	UserParam_Error_Fatal_ParA_NotInAlg,
	UserParam_Error_Fatal_ParB_NotInAlg,
	UserParam_Error_Fatal_Operator,
	UserParam_Error_Fatal_ToManyValidPar,

	UserParam_Error_size
} UserParam_Error_enum;

typedef struct {
	uint8 Param;
	UserParam_Error_enum Error;
} UserParam_Error_Type;




#endif  // #ifndef ALG_TYPES


/*******************************************************************************
END OF FILE
*******************************************************************************/






