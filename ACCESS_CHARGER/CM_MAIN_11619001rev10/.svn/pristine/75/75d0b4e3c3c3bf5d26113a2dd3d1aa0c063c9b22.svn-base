/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[x]Lib FB main source file		[ ]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		st7565_I.c
*
*	\ingroup	ST7565
*
*	\brief		ST7565 interface functions.
*
*	\details
*
*	\note
*
*	\version	dd-mm-2010 / Tommi Nakkila
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "sys.h"
#include "osa.h"
#include "mem.h"

#include "st7565.h"
#include "local.H"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/



/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/


/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/


/* End of declaration module **************************************************/



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets backlight on / off.
*
*	\param		pInst		ptr to st7565 instance
*	\param		on			if TRUE backlight is set on, else off	
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void st7565_ifBacklight(
	void *					pInst,
	Boolean					on
) {
	THIS->pInit->fnBacklight(on);
}


/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets display contrast.
*
*	\param		pInst		ptr to st7565 instance
*	\param		contrast	contrast to set (0 - 100%)	
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/

PUBLIC void st7565_ifContrast(
	void *					pInst,
	Uint8					contrast
) {
	Uint16					value;

	/* todo: scale to match with actual hardware */
	contrast = 60 + ((Uint16)contrast * 40) / 100;

	/*
	 *	Input range is 0..100, set range is 0..63.
	 */
	value = ((Uint16)contrast * (Uint16)63) / (Uint16)100;

	THIS->contrast = value;

	st7565_setElectronicVolume(THIS, (Uint8)value);
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Sets display on / off.
*
*	\param		pInst		ptr to st7565 instance
*	\param		on			if TRUE display is turned on, else off	
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void st7565_ifOnOff(
	void *					pInst,
	Boolean					on
) {
	if (on)
	{
		st7565_displayOnOff(THIS, ST7565_DISP_ON);
	}
	else
	{
		st7565_displayOnOff(THIS, ST7565_DISP_OFF);
	}
}



/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Enters / exits power save state.
*
*	\param		pInst		ptr to st7565 instance
*	\param		on	
*
*	\details		
*
*	\note			
*
*	\sa			
*
*******************************************************************************/
PUBLIC void st7565_ifPowerSave(
	void *					pInst,
	Boolean					on
) {
	if (on)
	{
		st7565_setSleepMode(pInst, TRUE);
		st7565_displayOnOff(pInst, ST7565_DISP_OFF);
		st7565_setAllPointsOn(pInst, ST7565_POINTS_ON);
	}
	else
	{
		st7565_setAllPointsOn(pInst, ST7565_POINTS_NORMAL);

	}
}
