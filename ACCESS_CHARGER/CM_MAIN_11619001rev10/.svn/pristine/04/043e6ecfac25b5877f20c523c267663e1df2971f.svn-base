/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_CM3
*
*	\brief		SSP0 with DMA
*
*	\details
*
*	\note
*
*	\version	\$Rev$ \n
*				\$Date$ \n
*				\$Author$		
*
*******************************************************************************/

/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "hw.h"

#include "../local.h"
#include "spim_1.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE void				spim__hwSetupPinsAndClock(spim_Init const_P *);

PRIVATE spim__Inst *		spim__lowInit(spim_Init const_P *);
PRIVATE void 				spim__lowProcess(spim__Inst *);
PRIVATE void 				spim__lowUp(spim__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for physical bus SSP0 with DMA (SCK on port 1 pin 20)
 */

PUBLIC spim__Tools const_P	spim__dmaTools1p1p20 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/**
 * Tools for physical bus SSP0 with DMA (SCK on port 0 pin 15)
 */

PUBLIC spim__Tools const_P	spim__dmaTools1p0p15 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

#ifdef TARGETFAMILY_LPC178x_7x 

/**
 * Tools for physical bus SSP0 with DMA (SCK on port 2 pin 22)
 */

PUBLIC spim__Tools const_P	spim__dmaTools1p2p22 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

#endif

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwSetupPinsAndClock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configures power and clock for the peripheral and Sets up the
*				fin functions.
*
*	\details	Select SSP pins and their modes in PINSEL0 to PINSEL4 and
*				PINMODE0 to PINMODE4.
*
*	\note
*
*******************************************************************************/

PRIVATE void spim__hwSetupPinsAndClock(
	spim_Init const_P *		pInit
) {
	/* Enable SSP0 */
	LPC_SC->PCONP |= 1 << 21;

#ifdef TARGETFAMILY_LPC178x_7x 
	
	/* 
	 *	LPC177x/8x has only one peripheral clock value so it needs no adjusting.
	 *
	 *	The MISO-line should not need the pull-up, but it is left enabled
	 *	to ensure full compatibility with older projects.
	 */

	if (pInit->pTools == &spim__dmaTools1p1p20) {
		/*
		 *	Pin type D
		 */

		LPC_IOCON->P1_20 = 0x05;					/* SSP0_SCK */
		LPC_IOCON->P1_23 = 0x05 | HW_PIN_PULLUP;	/* SSP0_MISO */
		LPC_IOCON->P1_24 = 0x05;					/* SSP0_MOSI */

	} else if (pInit->pTools == &spim__dmaTools1p0p15) {
		/*
		 *	Pin type D
		 */

		LPC_IOCON->P0_15 = 0x02;					/* SSP0_SCK */
		LPC_IOCON->P0_17 = 0x02 | HW_PIN_PULLUP;	/* SSP0_MISO */
		LPC_IOCON->P0_18 = 0x02;					/* SSP0_MOSI */

	} else {
		deb_assert(pInit->pTools == &spim__dmaTools1p2p22);
		/*
		 *	Pin type D
		 */

		LPC_IOCON->P2_22 = 0x02;					/* SSP0_SCK */
		LPC_IOCON->P2_26 = 0x02 | HW_PIN_PULLUP;	/* SSP0_MISO */
		LPC_IOCON->P2_27 = 0x02;					/* SSP0_MOSI */
	}

#else
	/*
	 * Set the SSP clock.
	 * 36MHz / 1 = 36Mhz
	 * 36MHz / 2 = 18MHz
	 */
	LPC_SC->PCLKSEL1 |= 1 << 10;

	/*
	 * Select pin functions for SSP
	 */
	
	if (pInit->pTools == &spim__dmaTools1p1p20) {
		LPC_PINCON->PINSEL3 |= 3U << 8;		/* P1.20 SCK0	*/
		LPC_PINCON->PINSEL3 |= 3U << 14;	/* P1.23 MISO0	*/
		LPC_PINCON->PINSEL3 |= 3U << 16;	/* P1.24 MOSI0	*/

	} else {
		deb_assert(pInit->pTools == &spim__dmaTools1p0p15);

		LPC_PINCON->PINSEL0 |= 2U << 30;	/* P0.15 SCK0	*/
		LPC_PINCON->PINSEL1 |= 2U << 2;		/* P0.17 MISO0	*/
		LPC_PINCON->PINSEL1 |= 2U << 4;		/* P0.18 MOSI0	*/
	}
#endif
}

/* Include generic SSP driver code */
#include "SPIM_SDD.c"
