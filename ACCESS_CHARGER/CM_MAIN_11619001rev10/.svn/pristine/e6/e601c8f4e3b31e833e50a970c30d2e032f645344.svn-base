/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Cc/INIT.H
*
*	\ingroup	CC
*
*	\brief
*
*	\details
*
*	\note
*
*	\version	??-??-???? / Ari Suomi
*
*******************************************************************************/

#ifndef CC_INIT_H_INCLUDED
#define CC_INIT_H_INCLUDED

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */

/*
#define name@lwc_reset		dummy_reset
#define name@lwc_down		dummy_down
#define name@lwc_read		dummy_read
#define name@lwc_write		dummy_write
#define name@lwc_ctrl		dummy_ctrl
#define name@lwc_test		dummy_test
*/


typedef struct {						
	Uint8					nDummy;
} cc_Init;							

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_CC
#elif defined(EXE_GEN_CC)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_CC
#endif

#define EXE_APPL(n)			n##cc

/********************************************************************/ EXE_BEGIN

/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

//#define EXE_USE_NN	/* Non-volatile numeric	*/
//#define EXE_USE_NS	/* Non-volatile string	*/
#define EXE_USE_VN	/* Volatile numeric		*/
//#define EXE_USE_VS	/* Volatile string		*/
//#define EXE_USE_PN	/* Process-point numeric*/
//#define EXE_USE_PS	/* Process-point string	*/
//#define EXE_USE_BL	/* Block of byte-data	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name		Flags				Dim	Type	Def	Min	Max
            ---------	---------------		---	-----	---	---	----*/
EXE_REG_VN( Status,					SYS_REG_DEFAULT,    1,	Uint32,	0,	0,	0xFFFFFFFFUL )
EXE_REG_VN( Sync,					SYS_REG_DEFAULT,    1,	Uint16,	0,	0,	0xFFFFUL )
EXE_REG_NN( ParallelSelect,			SYS_REG_DEFAULT,	1,	Uint16, 0,	0,	0xFFFF )			// Crosses shown in the parallel control sub menu. This list is updated by the GUI and Cluster control use it
EXE_REG_NN( ParallelAvail,			SYS_REG_DEFAULT,	16, Uint32, 0,	0,	0xFFFFFFFFUL )		// Serial number shown in the parallel control sub menu
EXE_REG_NN( ParallelControl_func,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	1 )
EXE_REG_VN( ParallelStatus,		 	SYS_REG_DEFAULT, 	16, Uint8, 	2, 	0, 		4 )				// Status shown in the parallel control sub menu

//EXE_REG_PN( SyncP,					SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Sync (to display) from CAN master ext/BMS
EXE_REG_PN( UsetP,					SYS_REG_DEFAULT,    1,	Uint32,		0,	0xFFFFFFFFUL )		// Uset (to display) from CAN master ext/BMS
EXE_REG_PN( IsetP,					SYS_REG_DEFAULT,    1,	Uint32,		0,	0xFFFFFFFFUL )		// Iset (to display) from CAN master ext/BMS
EXE_REG_PN( PsetP,					SYS_REG_DEFAULT,    1,	Uint32,		0,	0xFFFFFFFFUL )		// Pset (to display) from CAN master ext/BMS
EXE_REG_PN( SocP,					SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// SOC (to display) from CAN master ext/BMS
EXE_REG_PN( UmeasP,					SYS_REG_DEFAULT,    1,	Uint32,		0,	0xFFFFFFFFUL )		// Umeas (to display) from CAN slave or master(sum)
EXE_REG_PN( ImeasP,					SYS_REG_DEFAULT,    1,	Uint32,		0,	0xFFFFFFFFUL )		// Imeas (to display) from CAN slave or master(sum)
EXE_REG_PN( PmeasP,					SYS_REG_DEFAULT,    1,	Uint32,		0,	0xFFFFFFFFUL )		// Pmeas (to display) from CAN slave or master(sum)
EXE_REG_PN( DerateP,				SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Derate (to display) from CAN slave or master(sum)
EXE_REG_PN( WarningP,				SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Warning (to display) from CAN slave or master(sum)
EXE_REG_PN( ErrorP,					SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Error (to display) from CAN slave or master(sum)
EXE_REG_PN( DerateDisp,				SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Derate No/Yes
EXE_REG_PN( WarningDisp,			SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Warning No/Yes
EXE_REG_PN( ErrorDisp,				SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Error No/Yes
EXE_REG_PN( RemoteP,				SYS_REG_DEFAULT,    1,	Uint8,		0,	0xFF )				// Remote restricted No/Yes

/*******************************************************************************
 * HMI attributes
 */
EXE_MMI_NONE( Status )
EXE_MMI_NONE( Sync )
EXE_MMI_ENUM( ParallelSelect,		SYS_MMI_U_NONE, tOffOn,		SYS_MMI_RW, 	tOffOn)
EXE_MMI_INT( ParallelAvail,			SYS_MMI_U_NONE, tOffOn,		SYS_MMI_RW, 	SYS_Q_NONE)
EXE_MMI_ENUM( ParallelControl_func,	SYS_MMI_U_NONE, tFunction,	SYS_MMI_RW, 	tFunctionEnum )
EXE_MMI_NONE( ParallelStatus )

//EXE_MMI_ENUM( SyncP,	SYS_MMI_U_NONE, tSyncP,		SYS_MMI_READ, 	tNoYes)
EXE_MMI_REAL( UsetP,	SYS_MMI_U_NONE,	tUsetP,	SYS_MMI_READ, 0, 0xFFFFFFFF, 3, SYS_Q_NONE)
EXE_MMI_REAL( IsetP,	SYS_MMI_U_NONE,	tIsetP,	SYS_MMI_READ, 0, 0xFFFFFFFF, 3, SYS_Q_NONE)
EXE_MMI_INT( PsetP,		SYS_MMI_U_NONE,	tPsetP,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_INT( SocP,		SYS_MMI_U_NONE,	tSocP,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_REAL( UmeasP,	SYS_MMI_U_NONE,	tUmeasP,	SYS_MMI_READ, 0, 0xFFFFFFFF, 3, SYS_Q_NONE)
EXE_MMI_REAL( ImeasP,	SYS_MMI_U_NONE,	tImeasP,	SYS_MMI_READ, 0, 0xFFFFFFFF, 3, SYS_Q_NONE)
EXE_MMI_INT( PmeasP,	SYS_MMI_U_NONE,	tPmeasP,	SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_ENUM( DerateP,	SYS_MMI_U_NONE, tDerateP,	SYS_MMI_READ, 	tOffOn)
EXE_MMI_ENUM( WarningP,	SYS_MMI_U_NONE, tWarningP,	SYS_MMI_READ, 	tOffOn)
EXE_MMI_ENUM( ErrorP,	SYS_MMI_U_NONE, tErrorP,	SYS_MMI_READ, 	tOffOn)
EXE_MMI_ENUM( DerateDisp,	SYS_MMI_U_NONE, tNoYes,	SYS_MMI_READ, 	tNoYes)
EXE_MMI_ENUM( WarningDisp,	SYS_MMI_U_NONE, tNoYes,	SYS_MMI_READ, 	tNoYes)
EXE_MMI_ENUM( ErrorDisp,	SYS_MMI_U_NONE, tNoYes,	SYS_MMI_READ, 	tNoYes)
EXE_MMI_ENUM( RemoteP,	SYS_MMI_U_NONE, tRemoteP,	SYS_MMI_READ, 	tNoYes)

/*******************************************************************************
 * References to external registers
 */
EXE_REG_EX( Cc_Status,				Status,					cc1)
EXE_REG_EX( Cc_ParallelControl,		ParallelControl_func,	cc1)
EXE_REG_EX( Cc_can_tpdoSet,			tpdoSet,				can1 )
EXE_REG_EX( Cc_Uset,				Uset,					regu1 )
EXE_REG_EX( Cc_Iset,				Iset,					regu1 )
EXE_REG_EX( Cc_Pset,				Pset,					regu1 )
//EXE_REG_EX( Cc_RemoteInMode,		RemoteInMode,			regu1 )
EXE_REG_EX( Cc_UactMode,			UactMode,				regu1 )
EXE_REG_EX( Cc_ReguMode,			ReguMode,				regu1 )
EXE_REG_EX( Cc_IdcLimit,			IdcLimit,				regu1 )
EXE_REG_EX( Cc_ReguOffUactLow,		ReguOffUactLow,			regu1 )
EXE_REG_EX( Cc_ReguOffUactHigh,		ReguOffUactHigh,		regu1 )
EXE_REG_EX( Cc_ReguOnUactLow,		ReguOnUactLow,			regu1 )
EXE_REG_EX( Cc_ReguOnUactHigh,		ReguOnUactHigh,			regu1 )
EXE_REG_EX( Cc_SOC,					BmSOC,					radio1 )
EXE_REG_EX( Cc__statusFlags,		statusFlags,			sup1 )
EXE_REG_EX( Cc__SerialNo,			SerialNo,				sup1 )
EXE_REG_EX( Cc_CAN_CommProfile,		CAN_CommProfile,		can1 )
EXE_REG_EX( Cc_ParallelAvail,		ParallelAvail,			cc1 )
EXE_REG_EX( Cc_ParallelStatus,		ParallelStatus,			cc1 )
EXE_REG_EX( Cc_ChalgStatus,			ChalgStatus,			chalg1 )

/******************************************************************************* 
 * read/write indications
 * (sorted alphabetically by instance name and by declaration order)
 */
EXE_IND_ME( Sync,			Sync )
EXE_IND_ME( UsetP,			UsetP )
EXE_IND_ME( IsetP,			IsetP )
EXE_IND_ME( PsetP,			PsetP )
EXE_IND_ME( SocP,			SocP )
EXE_IND_ME( UmeasP,			UmeasP )
EXE_IND_ME( ImeasP,			ImeasP )
EXE_IND_ME( PmeasP,			PmeasP )
EXE_IND_ME( DerateP,		DerateP )
EXE_IND_ME( WarningP,		WarningP )
EXE_IND_ME( ErrorP,			ErrorP )
EXE_IND_ME( DerateDisp,		DerateDisp )
EXE_IND_ME( WarningDisp,	WarningDisp )
EXE_IND_ME( ErrorDisp,		ErrorDisp )
EXE_IND_ME( RemoteP,		RemoteP )

/******************************************************************************* 
 * Language-dependent texts 
 *  - tExampleTxtHandle1 is reserved handlename and it's discarded by 
 *		textparser tools.
 *  - you can add txthandle comment at the end of the texthandle line, and this
 *		is parsed by textparser tools.
 */

/**********************************************************************/ EXE_END
EXE_TXT( tFunction,		EXE_T_EN("Function") EXE_T_ES("Funci\363n") EXE_T_EN_US("Function") EXE_T_PT("Fun\347\343o") EXE_T_DE("Funktion") EXE_T_JP("") EXE_T_SE("Funktion") EXE_T_IT("Funzione") EXE_T_FI("") "") /* Menu:Service->Parallel control->Function (21-1-tFunctionEnum)*/
EXE_TXT( tFunctionEnum, EXE_T_EN("Disabled\nEnabled") EXE_T_ES("Inactivo\nActivo") EXE_T_EN_US("Disabled\nEnabled") EXE_T_PT("Desabilitado\nHabilitado") EXE_T_DE("Deaktiviert\nAktiviert") EXE_T_FI("") EXE_T_SE("Av\nP\345") EXE_T_IT("Disattivato\nAttivato") EXE_T_JP("") "") /*Menu:Service->Parallel control->Function values (21-1-tFunction)*/
EXE_TXT( tOffOn, EXE_T_EN("[ ]\n[X]") EXE_T_ES("[ ]\n[X]") EXE_T_EN_US("[ ]\n[X]") EXE_T_PT("[ ]\n[X]") EXE_T_DE("[ ]\n[X]") EXE_T_JP("") EXE_T_SE("[ ]\n[X]") EXE_T_IT("[ ]\n[X]") EXE_T_FI("") "") /*Menu:Service->Parallel control->Charger select*/
EXE_TXT( tNoYes, EXE_T_EN("No\nYes") EXE_T_ES("No\nYes") EXE_T_EN_US("No\nYes") EXE_T_PT("No\nYes") EXE_T_DE("Nein\nJa") EXE_T_JP("") EXE_T_SE("No\nYes") EXE_T_IT("No\nSi") EXE_T_FI("") "") /*Menu:Service->CAN->Network info (10)*/

EXE_TXT( tNetInfo, 		EXE_T_EN("Network info") EXE_T_ES("Info. de Red") EXE_T_EN_US("Network info") EXE_T_PT("Info de rede") EXE_T_DE("Netzwerkinfo") EXE_T_FI("") EXE_T_SE("N\344tverksinfo") EXE_T_IT("Network info") EXE_T_JP("")	"") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tSyncP,		EXE_T_EN("Sync") EXE_T_ES("Sync") EXE_T_EN_US("Sync") EXE_T_PT("Sync") EXE_T_DE("Sync") EXE_T_JP("") EXE_T_SE("Sync") EXE_T_IT("Sync") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tUsetP,		EXE_T_EN("Uset\050V\051") EXE_T_ES("Uset\050V\051") EXE_T_EN_US("Uset\050V\051") EXE_T_PT("Uset\050V\051") EXE_T_DE("Uset\050V\051") EXE_T_JP("") EXE_T_SE("Uset\050V\051") EXE_T_IT("Uset\050V\051") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tIsetP,		EXE_T_EN("Iset\050A\051") EXE_T_ES("Iset\050A\051") EXE_T_EN_US("Iset\050A\051") EXE_T_PT("Iset\050A\051") EXE_T_DE("Iset\050A\051") EXE_T_JP("") EXE_T_SE("Iset\050A\051") EXE_T_IT("Iset\050A\051") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tPsetP,		EXE_T_EN("Pset\050W\051") EXE_T_ES("Pset\050W\051") EXE_T_EN_US("Pset\050W\051") EXE_T_PT("Pset\050W\051") EXE_T_DE("Pset\050W\051") EXE_T_JP("") EXE_T_SE("Pset\050W\051") EXE_T_IT("Pset\050W\051") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tSocP,			EXE_T_EN("SOC\050%\051") EXE_T_ES("SOC\050%\051") EXE_T_EN_US("SOC\050%\051") EXE_T_PT("SOC\050%\051") EXE_T_DE("SOC\050%\051") EXE_T_JP("") EXE_T_SE("SOC\050%\051") EXE_T_IT("SOC\050%\051") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tUmeasP,		EXE_T_EN("Umeas\050V\051") EXE_T_ES("Umeas\050V\051") EXE_T_EN_US("Umeas\050V\051") EXE_T_PT("Umeas\050V\051") EXE_T_DE("Umeas\050V\051") EXE_T_JP("") EXE_T_SE("Umeas\050V\051") EXE_T_IT("Umeas\050V\051") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tImeasP,		EXE_T_EN("Imeas\050A\051") EXE_T_ES("Imeas\050A\051") EXE_T_EN_US("Imeas\050A\051") EXE_T_PT("Imeas\050A\051") EXE_T_DE("Imeas\050A\051") EXE_T_JP("") EXE_T_SE("Imeas\050A\051") EXE_T_IT("Imeas\050A\051") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tPmeasP,		EXE_T_EN("Pmeas\050W\051") EXE_T_ES("Pmeas\050W\051") EXE_T_EN_US("Pmeas\050W\051") EXE_T_PT("Pmeas\050W\051") EXE_T_DE("Pmeas\050W\051") EXE_T_JP("") EXE_T_SE("Pmeas\050W\051") EXE_T_IT("Pmeas\050W\051") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tDerateP,		EXE_T_EN("Derate") EXE_T_ES("Derate") EXE_T_EN_US("Derate") EXE_T_PT("Derate") EXE_T_DE("Verringern") EXE_T_JP("") EXE_T_SE("Derate") EXE_T_IT("Derate") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tWarningP,		EXE_T_EN("Warning") EXE_T_ES("Warning") EXE_T_EN_US("Warning") EXE_T_PT("Warning") EXE_T_DE("Warnung") EXE_T_JP("") EXE_T_SE("Warning") EXE_T_IT("Allarme") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tErrorP,		EXE_T_EN("Error") EXE_T_ES("Error") EXE_T_EN_US("Error") EXE_T_PT("Error") EXE_T_DE("Fehler") EXE_T_JP("") EXE_T_SE("Error") EXE_T_IT("Errore") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/
EXE_TXT( tRemoteP,		EXE_T_EN("Remote restricted") EXE_T_ES("Remote restricted") EXE_T_EN_US("Remote restricted") EXE_T_PT("Remote restricted") EXE_T_DE("Ferngesperrt") EXE_T_JP("") EXE_T_SE("Remote restricted") EXE_T_IT("Blocco da remoto") EXE_T_FI("") "") /* Menu:Service->CAN->Network info (15)*/

EXE_TXT( tDeratePhase, 	EXE_T_EN("Phase error") EXE_T_ES("Phase error") EXE_T_EN_US("Phase error") EXE_T_PT("Phase error") EXE_T_DE("Phasenfehler") EXE_T_FI("") EXE_T_SE("Phase error") EXE_T_IT("Errore di fase") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Derate (21-1-tOffOn)*/
EXE_TXT( tDerateUdef, 	EXE_T_EN("User def lim") EXE_T_ES("User def lim") EXE_T_EN_US("User def lim") EXE_T_PT("User def lim") EXE_T_DE("Benutzer limitiert") EXE_T_FI("") EXE_T_SE("User def lim") EXE_T_IT("Utente limitato") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Derate (21-1-tOffOn)*/
EXE_TXT( tDerateUIchar,	EXE_T_EN("Power lim") EXE_T_ES("Power lim") EXE_T_EN_US("Power lim") EXE_T_PT("Power lim") EXE_T_DE("Leistung Begrenzt") EXE_T_FI("") EXE_T_SE("Power lim") EXE_T_IT("Potenza limitata") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Derate (21-1-tOffOn)*/
EXE_TXT( tDerateTemp, 	EXE_T_EN("Temperature") EXE_T_ES("Temperature") EXE_T_EN_US("Temperature") EXE_T_PT("Temperature") EXE_T_DE("Temperatur") EXE_T_FI("") EXE_T_SE("Temperature") EXE_T_IT("Temperatura") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Derate (21-1-tOffOn)*/
EXE_TXT( tDerateNoLoad,	EXE_T_EN("No load") EXE_T_ES("No load") EXE_T_EN_US("No load") EXE_T_PT("No load") EXE_T_DE("Keine Belastung") EXE_T_FI("") EXE_T_SE("No load") EXE_T_IT("No carichi") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Derate (21-1-tOffOn)*/

EXE_TXT( tErrorPhase, 	EXE_T_EN("Phase error") EXE_T_ES("Phase error") EXE_T_EN_US("Phase error") EXE_T_PT("Phase error") EXE_T_DE("Phasenfehler") EXE_T_FI("") EXE_T_SE("Phase error") EXE_T_IT("Errore di fase") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Warning/Error (21-1-tOffOn)*/
EXE_TXT( tErrorRegu, 	EXE_T_EN("Regulator error") EXE_T_ES("Regulator error") EXE_T_EN_US("Regulator error") EXE_T_PT("Regulator error") EXE_T_DE("Reglerfehler") EXE_T_FI("") EXE_T_SE("Regulator error") EXE_T_IT("Errore DC Board.") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Warning/Error (21-1-tOffOn)*/
EXE_TXT( tErrorTempL,	EXE_T_EN("Low temperature") EXE_T_ES("Low temperature") EXE_T_EN_US("Low temperature") EXE_T_PT("Low temperature") EXE_T_DE("Niedrige Temp.") EXE_T_FI("") EXE_T_SE("Low temperature") EXE_T_IT("Bassa temperatura") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Warning/Error (21-1-tOffOn)*/
EXE_TXT( tErrorTempH, 	EXE_T_EN("High temperature") EXE_T_ES("High temperature") EXE_T_EN_US("High temperature") EXE_T_PT("High temperature") EXE_T_DE("Hohe Temperatur") EXE_T_FI("") EXE_T_SE("High temperature") EXE_T_IT("Alta temperatura") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Warning/Error (21-1-tOffOn)*/
EXE_TXT( tErrorTempHW,	EXE_T_EN("HW temperature") EXE_T_ES("HW temperature") EXE_T_EN_US("HW temperature") EXE_T_PT("HW temperature") EXE_T_DE("HW-Temperatur") EXE_T_FI("") EXE_T_SE("HW temperature") EXE_T_IT("HW temperatura") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Warning/Error (21-1-tOffOn)*/
EXE_TXT( tErrorCAN, 	EXE_T_EN("CAN timeout") EXE_T_ES("CAN timeout") EXE_T_EN_US("CAN timeout") EXE_T_PT("CAN timeout") EXE_T_DE("CAN-Zeitlimit") EXE_T_FI("") EXE_T_SE("CAN timeout") EXE_T_IT("CAN timeout") EXE_T_JP("") "") /* Menu:Service->CAN->Network info->Warning/Error (21-1-tOffOn)*/

#undef EXE_APPL
#endif
#undef EXE_INST
