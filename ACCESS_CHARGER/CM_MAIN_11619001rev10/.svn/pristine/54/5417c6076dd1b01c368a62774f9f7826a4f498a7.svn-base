/*************************************************************************
 **                                                                     **
 **     Copyright (c) 2005, TJK Tietolaite Oy. All rights reserved.     **
 **  This source file is made available solely for use by the customer  **
 **       of TJK Tietolaite Oy according to the signed agreement.       **
 **         ANY USE OF THE SOURCE FILE IS EXPRESSLY PROHIBITED,         **
 **       unless such use is expressly permitted by the agreement.      **
 **                                                                     **
 *************************************************************************/

/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[ ]Lib FB source file
*	[x]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		
*
*	\ingroup	SPIM_SM4
*
*	\brief		SPI2 driver.
*
*	\details
*
*	\note
*
*	\version	\$Rev: 3920 $ \n
*				\$Date: 2019-09-20 13:42:39 +0300 (pe, 20 syys 2019) $ \n
*				\$Author: tldati $
*
*******************************************************************************/
/*******************************************************************************
;
;	D E C L A R A T I O N   M O D U L E
;
;*******************************************************************************

;*******************************************************************************
;
;	HEADER / INCLUDE FILES
;
;-----------------------------------------------------------------------------*/

#include "tools.h"
#include "deb.h"
#include "hw.h"

#include "../local.h"
#include "SPIM_P2.h"

/*******************************************************************************
;
;	CONSTANTS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL MACROS
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	INTERNAL STRUCTURES AND DATA TYPES
;
;-----------------------------------------------------------------------------*/

/*******************************************************************************
;
;	STATIC FUNCTIONS
;
;-----------------------------------------------------------------------------*/

PRIVATE spim__Inst * 		spim__lowInit(spim_Init const_P *);
PRIVATE void 				spim__lowProcess(spim__Inst *);
PRIVATE void 				spim__lowUp(spim__Inst *);

/*******************************************************************************
;
;	PUBLIC DATA
;
;-----------------------------------------------------------------------------*/

/**
 * Tools for physical bus SPI2 with SCK on port B pin 13
 */

PUBLIC spim__Tools const_P	spim__tools2pbp13 = {
	&spim__lowInit,
	&spim__lowProcess,
	&spim__lowUp
};

/*******************************************************************************
;
;	STATIC DATA
;
;-----------------------------------------------------------------------------*/

SYS_DEFINE_FILE_NAME;

/* End of declaration module **************************************************/
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__hwSetupPinsAndClock
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure clock and setup pins for SSP1 peripheral.
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pSetupPinsAndClock(
	spim_Init const_P *			pInit
) {
	/*
	 * Enable clock for SPI2
	 */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);

	if (pInit->pTools == &spim__tools2pbp13) {

		GPIO_TypeDef *			GPIO;
		LL_GPIO_InitTypeDef		pinInit;

		GPIO = (GPIO_TypeDef *)GPIOB;

		/*
		 * SCK = PORTB, pin 13
		 */
		pinInit.Pin = 1<<13;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_5;
		LL_GPIO_Init(GPIO, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);

		/*
		 * MISO = PORTB, pin 14
		 */
		pinInit.Pin = 1<<14;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_5;
		LL_GPIO_Init(GPIO, &pinInit);

		/*
		 * MOSI = PORTB, pin 15
		 */
		pinInit.Pin = 1<<15;
		pinInit.Mode = LL_GPIO_MODE_ALTERNATE;
		pinInit.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		pinInit.Speed = LL_GPIO_SPEED_FREQ_HIGH;
		pinInit.Pull = LL_GPIO_PULL_UP;
		pinInit.Alternate = LL_GPIO_AF_5;
		LL_GPIO_Init(GPIO, &pinInit);
		//LL_GPIO_SetOutputPin (GPIO, pinInit.Pin);
	}
}
/*******************************************************************************
*
*	F U N C T I O N   D E S C R I P T I O N
*
*	spim__pConfigPinsForSlave
*
*--------------------------------------------------------------------------*//**
*
*	\brief		Configure pins for current slave before SPI request
*
*	\param		pInit	Pointer to init parameters.
*
*	\details
*
*	\note
*
*	\sa
*
*******************************************************************************/

PRIVATE void spim__pConfigPinsForSlave(
	spim_Init const_P *			pInit,
	spim_SlaveInfo const_P *	pSlave
) {
	GPIO_TypeDef *			GPIO;

	/*
	 * Change pull-up/pull-down for SCK depending in slave
	 */
	if (pInit->pTools == &spim__tools2pbp13) {

		GPIO = (GPIO_TypeDef *)GPIOB;

		if (pSlave->flags & SPIM_IDLE_LEVEL_H) {
			LL_GPIO_SetPinPull(GPIO, 1<<13, LL_GPIO_PULL_UP);
		} else {
			LL_GPIO_SetPinPull(GPIO, 1<<13, LL_GPIO_PULL_DOWN);
		}
	}
}

/* Include the generic SSP driver */
#include "SPIM_SSP.c"
