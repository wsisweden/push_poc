/* 07-10-08 TJK
********************************************************************* tabs:[5 9]
*
*	F I L E   D E S C R I P T I O N
*
*	[ ]Lib FB main source file		[x]Lib FB source file
*	[ ]Lib FB target source file	[ ]Project source file
*
*--------------------------------------------------------------------------*//**
*
*	\file		Cm/INIT.H
*
*	\ingroup	CM
*
*	\brief		Initialization declarations for CM FB.
*
*	\details
*
*	\note
*
*	\version	22-10-2009 / Ari Suomi
*
*******************************************************************************/

#ifndef CM_INIT_H_INCLUDED
#define CM_INIT_H_INCLUDED

#include "global.h"
#include "memdrv.h"

#include "Cm.h"

/*******************************************************************************
 *
 *	This header may be #included multiple times in building the configuration
 *	for a EXE, but the following need to be declared once only.
 */


//#define cm_reset		dummy_reset
//#define cm_down		dummy_down
//#define cm_read		dummy_read
//#define cm_write		dummy_write
#define cm_ctrl		dummy_ctrl
#define cm_test		dummy_test

/**
 * Type for CM initialization data.
 */

typedef struct {						/*''''''''''''''''''''''''''' CONST */
	sys_FBInstId			drvInstId;	/**< Driver instance ID				*/
	memdrv_Interface const_P * pDrvIf;	/**< Pointer to driver interface	*/
	cm_LogAreaInfo			histArea;	/**< Info about historical log area.*/
	cm_LogAreaInfo			eventArea;	/**< Info about event log area.		*/
	cm_LogAreaInfo			instArea;	/**< Info about instance log area.	*/
} cm_Init;								/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * Log info register data type.
 */

typedef struct {						/*''''''''''''''''''''''''''''''''''''''''' RAM */
	Uint32					nextAddr;	/**< Address of next free space.				*/
	Uint32					firstAddr;	/**< Address of first record.					*/
	Uint16					recCount;	/**< Number of records present in memory.		*/
	Uint32					recTotal;	/**< Number of records ever written.			*/
	Uint32					recReset;	/**< Number of records since log was cleared	*/
} cm__LogInfoType;						/*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*/

/**
 * \name	LogInfo values
 */

/*@{*/
#define CM__HISTINFO_DEF	{ 0x00000000, 0x00000000, 0x0000, 0x00000000, 0x00000000 }
#define CM__HISTINFO_MIN	{ 0x00000000, 0x00000000, 0x0000, 0x00000000, 0x00000000 }
#define CM__HISTINFO_MAX	{ 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF, 0xFFFFFFFF, 0xFFFFFFFF }

#define CM__EVENTINFO_DEF	{ 0x00000000, 0x00000000, 0x0000, 0x00000000, 0x00000000 }
#define CM__EVENTINFO_MIN	{ 0x00000000, 0x00000000, 0x0000, 0x00000000, 0x00000000 }
#define CM__EVENTINFO_MAX	{ 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF, 0xFFFFFFFF, 0xFFFFFFFF }

#define CM__INSTINFO_DEF	{ 0x00000000, 0x00000000, 0x0000, 0x00000000, 0x00000000 }
#define CM__INSTINFO_MIN	{ 0x00000000, 0x00000000, 0x0000, 0x00000000, 0x00000000 }
#define CM__INSTINFO_MAX	{ 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFF, 0xFFFFFFFF, 0xFFFFFFFF }
/*@}*/

extern sys_TypeTools		cm__LogInfoTools;

#endif
/******************************************************************************/

#ifndef EXE_MODE
#define EXE_GEN_CM
#elif defined(EXE_GEN_CM)
#if EXE_MODE /* Only once for each FB */
#undef EXE_GEN_CM
#endif

#define EXE_APPL(n)			n##cm

/********************************************************************/ EXE_BEGIN
/*
 *	Register file definitions for each instance.
 */

/* Which register types are used by this FB */

#define EXE_USE_NN		/* Non-volatile numeric	*/
//#define EXE_USE_NS	/* Non-volatile string	*/
#define EXE_USE_VN		/* Volatile numeric		*/
#define EXE_USE_VS		/* Volatile string		*/
#define EXE_USE_PN		/* Process-point numeric*/
#define EXE_USE_PS		/* Process-point string	*/
//#define EXE_USE_BL	/* Block of byte-data	*/
#define EXE_USE_NT		/* Non-volatile typedef	*/


/*
 *	The registers and the MMI attributes for each.
 *
 *		reg_flags (can be ORed together):
 *			SYS_REG_CHANGE_DET	=Change detection supported
 *			SYS_REG_DEFAULT		=None of above flags apply
 *
 *	EXE_MMI_NONE can be replaced with any one of the following:
 *
 *		EXE_MMI_INT(  rName, use, tHandle1, mmiF,                   qEnum )
 *		EXE_MMI_REAL( rName, use, tHandle2, mmiF, min, max, nDecim, qEnum )
 *		EXE_MMI_MASK( rName,      tHandle3, mmiF, numBits,          qEnum )
 *		EXE_MMI_STR(  rName,      tHandle4, mmiF                          )
 *		EXE_MMI_ENUM( rName, use, tHandle5, mmiF, tHandle6                )
 *
 *			rName =Register name as given to the EXE_REG_..() macro.
 *
 *			use =Purpose/use of the register, one of enum SYS_MMI_U_...
 *
 *			tHandle =Descriptive name/text of the register.
 *
 *			mmiF flags (can be ORed together):
 *				SYS_MMI_READ	=MMI can only read the register
 *				SYS_MMI_WRITE	=MMI can only write the register
 *				SYS_MMI_RW		=MMI can both read and write the register
 *				SYS_MMI_RT_ADJ	=Should be adjusted real-time, not only at
 *								 completion of editing (the default)
 *
 *			qEnum =Quantity string enumerator (SYS_Q_...)
 */

/*          Name			Flags				Dim	Type	Def	Min	Max
            ---------		---------------		--- -----	---	---	----*/

/* Log status registers */
EXE_REG_NT( histLogInfo,	SYS_REG_DEFAULT,	1,	cm__LogInfo,	CM__HISTINFO )
EXE_REG_NT( eventLogInfo,	SYS_REG_DEFAULT,	1,	cm__LogInfo,	CM__EVENTINFO )
EXE_REG_NT( instLogInfo,	SYS_REG_DEFAULT,	1,	cm__LogInfo,	CM__INSTINFO )
EXE_REG_PN( histLogCount,	SYS_REG_DEFAULT,	1,	Uint16,		0,	Uint16_MAX )
EXE_REG_PN( eventLogCount,	SYS_REG_DEFAULT,	1,	Uint16,		0,	Uint16_MAX )
EXE_REG_PN( instLogCount,	SYS_REG_DEFAULT,	1,	Uint16,		0,	Uint16_MAX )
EXE_REG_PN( histLogIndex,	SYS_REG_DEFAULT,	1,	Uint32,		0,	Uint32_MAX )
EXE_REG_PN( eventLogIndex,	SYS_REG_DEFAULT,	1,	Uint32,		0,	Uint32_MAX )
EXE_REG_PN( instLogIndex,	SYS_REG_DEFAULT,	1,	Uint32,		0,	Uint32_MAX )
EXE_REG_PN( histLogIndexReset,	SYS_REG_DEFAULT,	1,	Uint32,		0,	Uint32_MAX )
EXE_REG_PN( eventLogIndexReset,	SYS_REG_DEFAULT,	1,	Uint32,		0,	Uint32_MAX )
EXE_REG_PN( instLogIndexReset,	SYS_REG_DEFAULT,	1,	Uint32,		0,	Uint32_MAX )

/* Log reading (used by GUI) */
EXE_REG_VN( cycleNo,		SYS_REG_DEFAULT,	1,	Uint32,	1,	1,	0xFFFFFFFF )
EXE_REG_PN( chargedAh,		SYS_REG_DEFAULT,	1,	Uint32,		0,	0xFFFFFFFF ) // in (Ah)
EXE_REG_PN( chargedAhP,		SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF )
EXE_REG_PN( startDate,		SYS_REG_DEFAULT,	1,	Uint32,		0,	0xFFFFFFFF )
EXE_REG_PN( startTime,		SYS_REG_DEFAULT,	1,	Uint32,		0,	0xFFFFFFFF )
EXE_REG_PN( BID,			SYS_REG_DEFAULT,	1,	Uint32,		0,	0xFFFFFFFF )
EXE_REG_PS( BIDString,		SYS_REG_DEFAULT,	8 )
EXE_REG_PS( AlgNoName,		SYS_REG_DEFAULT,	8 )
EXE_REG_PN( capacity,		SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF ) // in (Ah)
EXE_REG_PN( cells,			SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF )
EXE_REG_PN( cableRi,		SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF ) // in (mohm)
EXE_REG_PN( baseload,		SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF ) // in (A)
EXE_REG_PN( ChalgErrorSum,	SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF )
EXE_REG_PN( ReguErrorSum,	SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF )
EXE_REG_PN( AlarmCount,		SYS_REG_DEFAULT,	1,	Uint8,		0,	0xFF )

EXE_REG_PN( startVoltage,	SYS_REG_DEFAULT,	1,	Uint32,		0,	0xFFFFFFFF )
EXE_REG_PN( endVoltage,		SYS_REG_DEFAULT,	1,	Uint32,		0,	0xFFFFFFFF )

EXE_REG_PN( ChargingMode,	SYS_REG_DEFAULT,	1,	Uint8,		0,	3 )
EXE_REG_PN( ChalgStatusSum,	SYS_REG_DEFAULT,	1,	Uint16,		0,	0xFFFF )
EXE_REG_VN( cycleNoTemp,		SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFF )

EXE_REG_VN( InstLogHdrIndex,	SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFF )
EXE_REG_VN( InstLogHdrTime,		SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFF )
EXE_REG_VN( InstLogHdrSamplePer,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,	0xFFFF )
EXE_REG_VN( InstLogHdrRecType,	SYS_REG_DEFAULT,	1,	Uint16,	0,	0,	0xFFFF )
EXE_REG_VN( InstLogHdrRecSize,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFF )

EXE_REG_VN( U32Spare,		SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFF )
EXE_REG_VN( U16Spare,		SYS_REG_DEFAULT,	1,	Uint16,	0,	0,	0xFFFF )
EXE_REG_VN( U8Spare,		SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	0xFF )

EXE_REG_VN( ChargingModeTemp,	SYS_REG_DEFAULT,	1,	Uint8,	0,	0,	3 )
EXE_REG_VN( BIDTemp,			SYS_REG_DEFAULT,	1,	Uint32,	0,	0,	0xFFFFFFFF )
EXE_REG_VS( AlgNoNameTemp,		SYS_REG_DEFAULT,	8,	"N/A"  )
EXE_REG_VN( capacityTemp,		SYS_REG_DEFAULT,	1,	Uint16,	0,	0,	0xFFFF ) // in (Ah)
EXE_REG_VN( cellsTemp,			SYS_REG_DEFAULT,	1,	Uint16,	0,	0,	0xFFFF )
EXE_REG_VN( cableRiTemp,		SYS_REG_DEFAULT,	1,	Uint16,	0,	0,	0xFFFF ) // in (mohm)
EXE_REG_VN( baseloadTemp,		SYS_REG_DEFAULT,	1,	Uint16,	0,	0,	0xFFFF ) // in (A)
/*******************************************************************************
 *  HMI attributes
 */
//EXE_MMI_NONE( InstlogIndex )
EXE_MMI_NONE( histLogInfo )
EXE_MMI_NONE( eventLogInfo )
EXE_MMI_NONE( instLogInfo )
EXE_MMI_NONE( histLogCount )
EXE_MMI_NONE( eventLogCount )
EXE_MMI_NONE( instLogCount )
EXE_MMI_NONE( histLogIndex )
EXE_MMI_NONE( eventLogIndex )
EXE_MMI_NONE( instLogIndex )
EXE_MMI_NONE( histLogIndexReset )
EXE_MMI_NONE( eventLogIndexReset )
EXE_MMI_NONE( instLogIndexReset )

/* Log reading (used by GUI) */
EXE_MMI_INT(cycleNo,		SYS_MMI_U_NONE,	tCycleNo,	SYS_MMI_RW, SYS_Q_NONE)	

EXE_MMI_INT( chargedAh,		SYS_MMI_U_NONE,	tChargedAh,	SYS_MMI_READ, SYS_Q_NONE)	
EXE_MMI_INT( chargedAhP,	SYS_MMI_U_NONE,	tChargedAhP,SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_DATE(startDate,		SYS_MMI_U_NONE,	tDate,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_TIME(startTime,		SYS_MMI_U_NONE,	tTime,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_INT( BID,			SYS_MMI_U_NONE,	tBID,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_NONE( BIDString )
EXE_MMI_STR( AlgNoName,						tAlgName,	SYS_MMI_READ )
EXE_MMI_INT( capacity,		SYS_MMI_U_NONE,	tCapacity,	SYS_MMI_READ, SYS_Q_NONE)	
EXE_MMI_INT( cells,			SYS_MMI_U_NONE,	tCells,		SYS_MMI_READ, SYS_Q_NONE)	
EXE_MMI_INT( cableRi,		SYS_MMI_U_NONE,	tCableRi,	SYS_MMI_READ, SYS_Q_NONE)	
EXE_MMI_INT( baseload,		SYS_MMI_U_NONE,	tBaseload,	SYS_MMI_READ, SYS_Q_NONE)	

EXE_MMI_NONE( ChalgErrorSum )
EXE_MMI_NONE( ReguErrorSum )
EXE_MMI_INT( AlarmCount,	SYS_MMI_U_NONE,	tDummy,	SYS_MMI_READ, SYS_Q_NONE)

EXE_MMI_REAL( startVoltage,	SYS_MMI_U_NONE,	tStartVoltage,	SYS_MMI_READ, 0, 0xFFFFFFFF, 3, SYS_Q_NONE)
EXE_MMI_REAL( endVoltage,	SYS_MMI_U_NONE,	tEndVoltage,	SYS_MMI_READ, 0, 0xFFFFFFFF, 3, SYS_Q_NONE)

EXE_MMI_ENUM( ChargingMode,	SYS_MMI_U_NONE,	tSource,	SYS_MMI_READ, tSourceEnum)
EXE_MMI_NONE( ChalgStatusSum )
EXE_MMI_NONE( cycleNoTemp )

EXE_MMI_NONE( InstLogHdrIndex )
EXE_MMI_NONE( InstLogHdrTime )
EXE_MMI_NONE( InstLogHdrSamplePer )
EXE_MMI_NONE( InstLogHdrRecType )
EXE_MMI_NONE( InstLogHdrRecSize )

EXE_MMI_NONE( U32Spare )
EXE_MMI_NONE( U16Spare )
EXE_MMI_NONE( U8Spare )

EXE_MMI_ENUM( ChargingModeTemp,	SYS_MMI_U_NONE,	tSource,	SYS_MMI_READ, tSourceEnum)
EXE_MMI_INT( BIDTemp,			SYS_MMI_U_NONE,	tBID,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_STR( AlgNoNameTemp,						tAlgName,	SYS_MMI_READ )
EXE_MMI_INT( capacityTemp,		SYS_MMI_U_NONE,	tCapacity,	SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_INT( cellsTemp,			SYS_MMI_U_NONE,	tCells,		SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_INT( cableRiTemp,		SYS_MMI_U_NONE,	tCableRi,	SYS_MMI_READ, SYS_Q_NONE)
EXE_MMI_INT( baseloadTemp,		SYS_MMI_U_NONE,	tBaseload,	SYS_MMI_READ, SYS_Q_NONE)

/*******************************************************************************
* References to external registers
*/
//EXE_REG_EX( cm__chargeIndex,			chargeIndex,				sup1 )

EXE_REG_EX( cm__LocalTime,				LocalTime,					sup1 )
EXE_REG_EX( cm__instLogInterval,		InstLogSamplePeriod,		sup1 )

//EXE_REG_EX( cm__chargeIndexReset,		chargeIndexReset,			sup1 )

/******************************************************************************* 
 * read/write indications
 * (sorted alphabetically by instance name and by declaration order)
 */

EXE_IND_ME( histLogInfo,	histLogInfo )
EXE_IND_ME( histLogCount,	histLogCount )
EXE_IND_ME( eventLogCount,	eventLogCount )
EXE_IND_ME( instLogCount,	instLogCount )
EXE_IND_ME( histLogIndex,	histLogIndex )
EXE_IND_ME( eventLogIndex,	eventLogIndex )
EXE_IND_ME( instLogIndex,	instLogIndex )
EXE_IND_ME( histLogIndexReset,	histLogIndexReset )
EXE_IND_ME( eventLogIndexReset,	eventLogIndexReset )
EXE_IND_ME( instLogIndexReset,	instLogIndexReset )
EXE_IND_ME( cycleNo,		cycleNo )
EXE_IND_ME( chargedAh,		chargedAh )
EXE_IND_ME( chargedAhP,		chargedAhP	)
EXE_IND_ME( startDate,		startDate )
EXE_IND_ME( startTime,		startTime )
EXE_IND_ME( BID,			BID )
EXE_IND_ME( BIDString,		BIDString )
EXE_IND_ME( AlgNoName,		AlgNoName )	
EXE_IND_ME( capacity,		capacity )
EXE_IND_ME( cells,			cells )
EXE_IND_ME( cableRi,		cableRi )
EXE_IND_ME( baseload,		baseload )
EXE_IND_ME( ChalgErrorSum,	ChalgErrorSum )
EXE_IND_ME( ReguErrorSum,	ReguErrorSum )
EXE_IND_ME( AlarmCount,		AlarmCount )
EXE_IND_ME( startVoltage,	startVoltage )
EXE_IND_ME( endVoltage,		endVoltage )
EXE_IND_ME( ChargingMode,	ChargingMode )
EXE_IND_ME( ChalgStatusSum,	ChalgStatusSum )

/******************************************************************************* 
 * Language-dependent texts 
 *  - tExampleTxtHandle1 is reserved handlename and it's discarded by 
 *		textparser tools.
 *  - you can add txthandle comment at the end of the texthandle line, and this
 *		is parsed by textparser tools.
 */

//EXE_TXT( tExampleTxtHandle1, EXE_T_("") EXE_T_FI("") EXE_T_SE("Beskrivning") "") /* txthandle comment */
EXE_TXT( tDummy,		EXE_T_EN("") EXE_T_ES("") EXE_T_EN_US("") EXE_T_PT("") EXE_T_DE("") EXE_T_JP("") EXE_T_SE("") EXE_T_IT("") EXE_T_FI("") "") /*text not used but needed for declaration of REG value for display*/
 /* Cycle log window texts */
EXE_TXT( tCycleNo,		EXE_T_EN("Cycle no") EXE_T_ES("Ciclo n\372mero") EXE_T_EN_US("Cycle no") EXE_T_PT("Numero de ciclos") EXE_T_DE("Zyklus Nr") EXE_T_JP("") EXE_T_SE("Cykel nr") EXE_T_IT("Ciclo nr.") EXE_T_FI("") "") /*Charging cycle number (16)*/
EXE_TXT( tChargedAh,	EXE_T_EN("Charged Ah") EXE_T_ES("Ah cargados") EXE_T_EN_US("Charged Ah") EXE_T_PT("Ah carregados") EXE_T_DE("Aufgeladene Ah") EXE_T_JP("") EXE_T_SE("Laddade Ah") EXE_T_IT("Ah caricati") EXE_T_FI("") "") /*Menu:Statistics->Charging cycles->Cycle log->Charged Ah (16)*/
EXE_TXT( tChargedAhP,	EXE_T_EN("Charged Ah\050%\051") EXE_T_ES("Ah cargados\050%\051") EXE_T_EN_US("Charged Ah\050%\051") EXE_T_PT("Ah carregados\050%\051") EXE_T_DE("Aufgeladene Ah\050%\051") EXE_T_JP("") EXE_T_SE("Laddade Ah\050%\051") EXE_T_IT("Ah caricati\050%\051") EXE_T_FI("")  "") /*Menu:Statistics->Charging cycles->Cycle log->Charged Ah(%) (17)*/
EXE_TXT( tDate,			EXE_T_EN("Start date") EXE_T_ES("Fecha arr.") EXE_T_EN_US("Start date") EXE_T_PT("Data inicio") EXE_T_DE("startdatum") EXE_T_JP("") EXE_T_SE("Startdatum") EXE_T_IT("Data iniz.") EXE_T_FI("") "") /*Menu:Charging status->Start date (10)*/
EXE_TXT( tTime,			EXE_T_EN("Start time") EXE_T_ES("Tiempo arr.") EXE_T_EN_US("Start time") EXE_T_PT("Horario inicio") EXE_T_DE("Startzeit") EXE_T_JP("") EXE_T_SE("Starttid") EXE_T_IT("Orario iniz.") EXE_T_FI("") "") /*Menu:Charging status->Start time (13)*/

EXE_TXT( tNone,			EXE_T_EN("None") EXE_T_ES("Ninguno") EXE_T_EN_US("None") EXE_T_PT("Nenhum ") EXE_T_DE("Keiner") EXE_T_JP("") EXE_T_SE("Inga") EXE_T_IT("Nessuno") EXE_T_FI("") "") /*Menu:Statistics->Cycle log / Cycle when no items/alarms in log (21-1-tAlarms)*/

/* Charger param. window texts */
EXE_TXT( tSource,		EXE_T_EN("Source") EXE_T_ES("Fuente") EXE_T_EN_US("Source") EXE_T_PT("Fonte") EXE_T_DE("Quelle") EXE_T_JP("") EXE_T_SE("K\344lla") EXE_T_IT("Fonte") EXE_T_FI("") "") /*Menu:Statistics->Charging param->Source row (21-1-tSourceEnum)*/
EXE_TXT( tSourceEnum, 	EXE_T_EN("User def\nBMU\nDual\nMulti") EXE_T_ES("Def usuario\nBMU\nDe dos\nMulti") EXE_T_EN_US("User def\nBMU\nDual\nMulti") EXE_T_PT("Def. usuario\nBMU\nDuplo\nMulti") EXE_T_DE("Benutzer def\nBMU\nDual\nMulti") EXE_T_FI("") EXE_T_SE("Anv def\nBMU\nDual\nMulti") EXE_T_IT("Utente\nBMU\nDual\nMulti") EXE_T_JP("") "") /*Menu:Statistics->Charging param->Source row enum (21-1-tSource)*/
EXE_TXT( tBID,			EXE_T_EN("B-ID") EXE_T_ES("ID bateria") EXE_T_EN_US("B-ID") EXE_T_PT("ID bateria") EXE_T_DE("B-ID") EXE_T_FI("") EXE_T_SE("B-ID") EXE_T_IT("ID batteria") EXE_T_JP("")  "") /*Menu:Statistics->Charging Cycles->Cycle log->Charger param (10)*/
EXE_TXT( tAlgName,		EXE_T_EN("Curve") EXE_T_ES("Curva") EXE_T_EN_US("Curve") EXE_T_PT("Curva") EXE_T_DE("Kurve") EXE_T_JP("") EXE_T_SE("Kurva") EXE_T_IT("Curva") EXE_T_FI("") "") /*Menu:Statistics->Charging param->Charging curve (12)*/
EXE_TXT( tCapacity,		EXE_T_EN("Capacity\050Ah\051") EXE_T_ES("Capacidad\050Ah\051") EXE_T_EN_US("Capacity\050Ah\051") EXE_T_PT("Capacidade\050Ah\051") EXE_T_DE("Kapazit\344t\050Ah\051") EXE_T_JP("") EXE_T_SE("Kapacitet\050Ah\051") EXE_T_IT("Capacit\341\050Ah\051") EXE_T_FI("") "") /*Menu:Statistics->Charging param->Capacity (16)*/
EXE_TXT( tCells,		EXE_T_EN("Cells") EXE_T_ES("Celdas") EXE_T_EN_US("Cells") EXE_T_PT("Elementos") EXE_T_DE("Zellen") EXE_T_JP("") EXE_T_SE("Celler") EXE_T_IT("Celle") EXE_T_FI("") "") /*Menu:Statistics->Charging param->Cells (17)*/
EXE_TXT( tCableRi,		EXE_T_EN("Cable Ri\050m\246\051") EXE_T_ES("Res. cables\050m\246\051") EXE_T_EN_US("Cable Ri\050m\246\051") EXE_T_PT("Res. cabos\050m\246\051 ") EXE_T_DE("Kabel R \050m\246\051") EXE_T_JP("") EXE_T_SE("Kabelresistans\050m\246\051") EXE_T_IT("Res.cavi Ri\050m\246\051") EXE_T_FI("") "") /*Menu:Statistics->Charging param->Cable Ri (18)*/
EXE_TXT( tBaseload,		EXE_T_EN("Base load\050mA\051") EXE_T_ES("Carga b\341sica\050mA\051") EXE_T_EN_US("Base load\050mA\051") EXE_T_PT("Carga basica\050mA\051") EXE_T_DE("Grundlast\050mA\051") EXE_T_JP("") EXE_T_SE("Grundlast\050mA\051") EXE_T_IT("Ass.parall.\050mA\051") EXE_T_FI("") "") /*Menu:Statistics->Charging param->Baseload (15)*/

EXE_TXT( tStartVoltage,	EXE_T_EN("Start VPC") EXE_T_ES("Arranque VPC") EXE_T_EN_US("Start VPC") EXE_T_PT("Partida VPC") EXE_T_DE("VPC beginnen") EXE_T_JP("") EXE_T_SE("Start VPC") EXE_T_IT("VPC inizio") EXE_T_FI("") "") /*Menu:Statistics->Charging cycles->Cycle log->Start V/c (16)*/
EXE_TXT( tEndVoltage,	EXE_T_EN("End VPC") EXE_T_ES("Final VPC") EXE_T_EN_US("End VPC") EXE_T_PT("Final VPC") EXE_T_DE("VPC beenden") EXE_T_JP("") EXE_T_SE("Slut VPC") EXE_T_IT("VPC finale") EXE_T_FI("") "") /*Menu:Statistics->Charging cycles->Cycle log->End V/c (16)*/

/**********************************************************************/ EXE_END

#undef EXE_APPL
#endif
#undef EXE_INST
