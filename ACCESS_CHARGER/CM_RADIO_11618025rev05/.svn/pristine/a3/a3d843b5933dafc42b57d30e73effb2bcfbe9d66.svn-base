/***************************************************************************
  MpNwk.h

  This file defines the compile-options for the MpNwk.c source code.

  Revision history:
  Rev   Date      Comment   Description
  this  100707              first revision
***************************************************************************/
#ifndef MPNWK_H_
#define MPNWK_H_

#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

#define MAX_MSG 40
#define MAX_PAYLOAD 91

/* Message type defines */
#define ReqMsg_c    0x00  // Request message
#define RspMsg_c    0x01  // Response message
#define CmdTypeMask_c 0x10  // Mask for message type detection

/* Payload*/
#define iCmdOffset_c 0
#define iMpSeqOffset_c 1
#define iCtrlOffset_c 2
#define aDataOffset_c 3

/* DeviceType */
#define BMU       (1<<0)
#define CM_MAIN   (1<<1)
#define CM_RADIO  (1<<2)

#define gRouteDiscoveryMask_c (1<<6)

/* message cmd */
// SM
#define gSmGetStatus_c         0x60
#define gSmGetConfig_c         0x62
#define gSmGetLog_c            0x63
#define gSmGetInfo_c           0x68
#define gSmGetAcknowledge_c    0x6A
#define gSmReset_c             0x6C
#define gSmDisableRoutingMode_c    0x6D
#define gSmSetServiceMode_c    0x6E
#define gSmInfo_c              0x70
// CM
#define gCmInfo_c              0x54
//BM
#define gBmInfo_c              0x36
// Common
#define gGetSegment_c          0x01
#define gGetAddrPoolAddr_c     0x03
#define gAcknowledge_c         0x10
#define gJoinIndication_c      0x11
#define gAddrPoolAddr_c        0x12
// Cirrus GW
#define gSetCirrusParamReq_c   0x80
#define gGetCirrusParamReq_c   0x81
#define gGetCirrusParamResp_c  0x90
//debug
#define gAppDebugDataReq_c        0xEC
#define gMessageTableDataReq_c    0xED
#define gAssertDataReq_c          0xEE
#define gDebugDataReq_c           0xEF
#define gAppDebugDataRsp_c        0xFC
#define gMessageTableDataRsp_c    0xFD
#define gAssertDataRsp_c          0xFE
#define gDebugDataRsp_c           0xFF

/* Ctrl byte iterpretation */
#define FirstSegMask_c  0x80  // First segment mask
#define ConSegMask_c    0x7E  // Consecutive segment mask
#define LastSeg_c       0x01  // Last segment
#define SingleMsg_c     0x00  // Single message

/* Timers */
#define gMpMsgTimerFirstPos_c gPopFirstTimerId_c
#define gPopAppTimerFirstPos_c (gMpMsgTimerFirstPos_c + MAX_MSG)

/* Network defines */
#define gMpSmNwkReservedAddr_c   0xFF00

/* Definitions */
typedef enum {
  NA,
  Free,
  Sending,
  WaitingToSend,
  WaitingForRsp,
  RspTimeout,
  Segment,
  WaitingForReRoute
} eMsgStatus_t;

// The structure of messages which are queued for sending again purposes
typedef struct {
  uint16_t iDstAddr;
  uint8_t iOptions;
  uint8_t iRadius;
  uint8_t iPayloadLength;
  uint8_t aPayload[MAX_PAYLOAD];
} sMessage_t;

enum eMessage {
  MESSAGE_INDEX,
  MESSAGE_STATUS,
  MESSAGE_DSTADDR_H,
  MESSAGE_DSTADDR_L,
  MESSAGE_OPTIONS,
  MESSAGE_RADIUS,
  MESSAGE_PAYLOADLEN,
  MESSAGE_PAYLOAD,
  MESSAGE_SIZE,
};

typedef struct sMpMsgHeader_tag
{
  //header
  uint8_t iCmd;
  uint8_t iMpSequence;
  uint8_t iControl;
}sMpMsgHeader_t;

typedef struct sMpMsgGetSegment_tag
{
  uint8_t iCtrl;
}sMpMsgGetSegment_t;

enum eMpMsgGetSegment
{
  GET_SEG_CTRL,
  GET_SEG_SIZE
};

#pragma pack(1)
typedef struct sMpMsgGetAcknowledge_tag
{
  sMpMsgHeader_t sHeader;
  uint16_t iTimeSlot;
}sMpMsgGetAcknowledge_t;
#pragma pack()

enum eMpMsgGetAcknowledge
{
  GET_ACK_TIMESLOT_H,
  GET_ACK_TIMESLOT_L,
  GET_ACK_SIZE
};

#pragma pack(1)
typedef struct sMpMsgAcknowledge_tag
{
  uint8_t iReqCmd;
  uint8_t iStatus;
  uint8_t type;
  uint32_t id;
}sMpMsgAcknowledge_t;
#pragma pack()

enum eMpMsgAcknowledge
{
  ACK_REQCMD,
  ACK_STATUS,
  ACK_SIZE
};

#pragma pack(1)
typedef struct
{
  uint32_t index;
  uint8_t data[82];
} sMpMsgSetCirrusParamReq_t;
#pragma pack()

#pragma pack(1)
typedef struct
{
  uint32_t index;
} sMpMsgGetCirrusParamReq_t;
#pragma pack()

#pragma pack(1)
typedef struct
{
  uint32_t index;
  uint8_t data[82];
} sMpMsgGetCirrusParamResp_t;
#pragma pack()

/* Function definitions*/
void MpNwkTaskInit(void);
void MpNwkTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
uint8_t MpNwkGetMsgType(uint8_t aPayload[]);
void MpNwkDataIndication(sPopNwkDataIndication_t *pIndication, bool *pSendThruGateway);
void MpNwkDataConfirm(popErr_t iDataConfirm);
bool MpNwkDataRequest(sMessage_t *pMessage);
void MpNwkInitiateRouteHandling(void);
void MpNwkEndRouteHandling(const uint16_t address);
void MpNwkMemInit(void);

extern uint8_t PopFindTimer(popTaskId_t iTaskId, popTimerId_t iTimerId, uint8_t *piFreeTimer);
extern void PopAppAcknowledge(uint16_t iDstAddr, uint8_t iMpSequence, uint8_t popNwkDataReqOpts, uint8_t iReqCmd, uint8_t iStatus);

#endif
