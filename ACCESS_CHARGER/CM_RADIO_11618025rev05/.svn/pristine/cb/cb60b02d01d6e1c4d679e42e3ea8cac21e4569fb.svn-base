
/* TI peripheral library */
#include "cpu.h"
#include "tiflash.h"
#include "interrupt.h"
#include "hw_nvic.h"
#include "rom.h"

/* PopNet */
#include "Popnet_AppSpecific/board_config.h"
#include "EmbeddedTypes.h"
#include "PopBsp.h"

/* MP */
#include "flash.h"

/**
 *  Location of the main application. This should be the address of the main
 *  application interrupt vector table.
 */
#define MAIN_APP_START  0x00202000

/**
 *  Last address in the main application space.
 */
#define MAIN_APP_END    0x0027E7FF

/**
 *  Maximum main application size
 */
#define MAIN_APP_SIZE   (MAIN_APP_END-MAIN_APP_START+1)

/**
 *  Size of signature.
 */
#define SIGNATURE_SIZE  4

/**
 *  Size of signature.
 */
#define IMAGESIZE_SIZE  4

/**
 *  Size of the image header in the storage.
 */
#define HEADER_SIZE     (SIGNATURE_SIZE + IMAGESIZE_SIZE + 1)
 
void BootMainApplication(void);
static void MPEraseBlockDoneCallback(uint32_t address);
void MPFlashEraseBlockBlocking(uint32_t address);
static void MPReadDoneCallback(uint32_t address);
void MPFlashReadBlocking(uint32_t address, void* data, int len);
uint32_t ParseUnsigned32Bit(byte_t * pBuffer);

// For Micropower external flash blocking control
volatile uint8_t MPWriteDoneFlag;
volatile uint8_t MPReadDoneFlag;
volatile uint8_t MPEraseBlockFlag;

byte_t dataBuffer[2*1024];

/*
  PopBootLoader

  Verifies if there is a validimage on the storage area ready to be use, if not start with
  the current imageon the boot section of the flash. if the stored ismage is valid then,
  verifies if there is a valid image already on the boot area, if so then proceeds to use it.
  If the image on the boot sectiondoe snot has the water mark "GOOD" on the first 4 bytes,
  then the propcess of loading the stored image into the boot section beggins, once is done
  copying the whole stored image then writes the "GOOD" water mark and reboots.

  Returns no value.
  Receives no value
*/

int main()
{
  byte_t aHeader[HEADER_SIZE];
    
#if (TARGET_BOARD == MC1322XNCB) || (TARGET_BOARD == MC1322XSRB)
  byte_t *pBaseAddress_GPIO = (void *)0x80000000;  // GPIO Base addresss.
  *((uint32_t *)(pBaseAddress_GPIO + 0x18)) = 0x00000000; // Normal mode GPIO.
  *((uint32_t *)(pBaseAddress_GPIO + 0x28)) = 0x07800000; // Set the source to read from the data register.
  *((uint32_t *)pBaseAddress_GPIO)          = 0x07800000; // Direction set to output
  *((uint32_t *)(pBaseAddress_GPIO + 0x08)) = 0x00000000; // Data to 0, all LEDs off.
#endif

  // use definitions to avoid synchronization problems with Micropower specific values
  //GPIO_REGS_P->DirLo =  gDirLoValue_c;                   //MBAR_GPIO + 0x00  Direccion Set
  //GPIO_REGS_P->FuncSel0 = gFuncSel0Value_c;              //MBAR_GPIO + 0x18 Micropower GPIO mode
  //GPIO_REGS_P->DataLo = 0x11800C00;                      // All LEDs on
  //GPIO_REGS_P->DataLo = 0x01800200;                      // 10101 LED pattern 
  
  IntMasterEnable();
  
  // Initialize external flash
  FlashInit();

  (void)MPFlashReadBlocking(gStorageStart_c, aHeader, SIGNATURE_SIZE + IMAGESIZE_SIZE);  
  
  // If any of the readed characters is different from the expected signature, stop and leave, the image is not valid.
  if ((aHeader[0] != 0x00) || (aHeader[1] != 0x00) || (aHeader[2] != 0x49) || (aHeader[3] != 0x46))
  {
    // Boot to main and keep the current image.
    BootMainApplication();
  }

#if (TARGET_BOARD == MC1322XNCB) || (TARGET_BOARD == MC1322XSRB)
  // We are about to start processing the new image file, turn LED1 ON
  *((uint32_t *)(pBaseAddress_GPIO + 0x08)) |= 0x00800000;
#endif

#if 0
  {// initliaze Wacth dog (COP). Avoid resetting while erasing the Flash.
    crmCopCntl_t CopCntl;  // Define the COP controls

    /* Initialize the Dog */
    CopCntl.bit.copOut     = 0x00; // Generates a reset on time out.
    CopCntl.bit.copTimeOut = 0x1C; // To get approximately 2.5 seconds for time out. No needed when off.
    CopCntl.bit.copEn      = 0x00; // Disable/Enables = (0/1) the cop. By default is turned off.
    CopCntl.bit.copWP      = 0x01; // Protect the control cop registers.

    /* Now load the structure on Cop registers */
    (void)CRM_CopCntl(CopCntl);
  }
#endif
  
  // Ok, we got a valid image into the storage area and we are ready to update.

  // From now and on, if the board gets reset it will try to load the new image from scratch everytime.

  // Start copying from the Storage place into the Flash used for the stack.
  {
    // The iStorageOffset will keep track of the code in the storage place.
    uint32_t iStorageOffset = HEADER_SIZE;
    uint32_t iFlashOffset = MAIN_APP_START; // Offset in the flash area, the area used to boot and run the stack
    uint32_t iBytesLeft;                    // Number of image bytes left to copy
    
    iBytesLeft = ParseUnsigned32Bit(&aHeader[4]);
    if (iBytesLeft > MAIN_APP_SIZE)
    {
      /*
       *  The image will not fit into the main application area in the internal
       *  flash memory. Just skip the update and boot the main application.
       */
      BootMainApplication(); 
    }
    
#if (TARGET_BOARD == MC1322XNCB) || (TARGET_BOARD == MC1322XSRB)
    // Boot Loader code already porcessed, turn LED2 ON.
    *((uint32_t *)(pBaseAddress_GPIO + 0x08)) |= 0x01000000;
#endif
    
#if (TARGET_BOARD == MC1322XNCB) || (TARGET_BOARD == MC1322XSRB)
    // Start writing the whole flash turn LED3 ON
    *((uint32_t *)(pBaseAddress_GPIO + 0x08)) |= 0x02000000;
#endif

    // Copy data from storage to main application area.
    do
    {
      uint32_t iChunk;

      // Copy max 2KiB
      iChunk = (iBytesLeft >= 0x800) ? 0x800 : iBytesLeft;

      // Start reading from external flash memory
      MPReadDoneFlag = FALSE;
      FlashRead(iStorageOffset, dataBuffer, iChunk, MPReadDoneCallback); 
  
      // Erase page that will be written.
      TiFlashMainPageErase(iFlashOffset);
      
      // Wait until data has been read from external flash memory
      while(!MPReadDoneFlag);
      
      // Write the image into FLASH, one chunk at the time (4096 bytes).
      TiFlashMainPageProgramExt(dataBuffer, iFlashOffset, iChunk);
      
      iStorageOffset += iChunk;
      iFlashOffset += iChunk;
      iBytesLeft -= iChunk;
    } while (iBytesLeft != 0);
    
  }// End of block of writing.

  /*
    Erasing the first sector of the new image makes the signature A7IF go away
  */
  
  // Erase Block in external Flash
  MPFlashEraseBlockBlocking(gStorageStart_c);
  
#if (TARGET_BOARD == MC1322XNCB) || (TARGET_BOARD == MC1322XSRB)
  *((uint32_t *)(pBaseAddress_GPIO + 0x08)) = 0x00000000; // Data to 0, all LEDs off.
#endif

  // That's all folks.. On reset the Watchdog backs to normal.
  SysCtrlReset();
}

/**
 *  Reads a 32-bit value from a buffer in little-endian format.
 */
uint32_t ParseUnsigned32Bit(byte_t * pBuffer)
{
  uint32_t retVal;
  
  retVal = (uint32_t)*pBuffer++;
  retVal |= (uint32_t)*pBuffer++ << 8;
  retVal |= (uint32_t)*pBuffer++ << 16;
  retVal |= (uint32_t)*pBuffer << 24;
  
  return retVal;
}

/**
 *  Jump to the main application.
 */
void BootMainApplication(void)
{
  // Boot to main and keep the current image.

  typedef void ResetISRFn(void);
  
  ResetISRFn * pMainAppResetIsrFn;
  pMainAppResetIsrFn = (ResetISRFn *) *((void **)(MAIN_APP_START + 4));
  
  if ((int)pMainAppResetIsrFn >= MAIN_APP_START && (int)pMainAppResetIsrFn < MAIN_APP_END)
  {
    IntMasterDisable();  
    HWREG(NVIC_VTABLE) = MAIN_APP_START;
    pMainAppResetIsrFn();
  }
  else
  {
    /*
     *  The main application interrupt vector table is invalid. There is nothing
     *  that can be done any more. The main application must be loaded via JTAG
     *  again.
     */
    
    for (;;)
    {
      
    }
  }
}


/*
  MPEraseDoneCallback
*/
static void MPEraseBlockDoneCallback(uint32_t address)
{
  MPEraseBlockFlag = TRUE;
}


/*
  MPReadDoneCallback
*/
static void MPReadDoneCallback(uint32_t address)
{
  MPReadDoneFlag = TRUE;
}


/*
 MPFlashEraseBlockBlocking
 uses the FlashWrite routine from Micropower as a blocking routine for compatibility
 with actual over the air upgrade code
*/
void MPFlashEraseBlockBlocking(uint32_t address)
{
  MPEraseBlockFlag = FALSE;
  FlashEraseBlock(address,MPEraseBlockDoneCallback); 
  
  while(!MPEraseBlockFlag);
}

/*
 MPFlashReadBlocking
 uses the FlashRead routine from Micropower as a blocking routine for compatibility
 with actual over the air upgrade coding
*/
void MPFlashReadBlocking(uint32_t address, void* data, int len)
{
  MPReadDoneFlag = FALSE;
  FlashRead(address,data,len,MPReadDoneCallback); 
  
  while(!MPReadDoneFlag);
}
