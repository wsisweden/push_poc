#include "spi.h"
#include "../inc/hw_ssi.h"
#include "../inc/hw_ints.h"
#include "../inc/hw_memmap.h"
#include "../inc/hw_gpio.h"
#include "../inc/hw_ioc.h"
#include "tissi.h"
#include "gpio.h"
#include "interrupt.h"
#include "sys_ctrl.h"
#include "ioc.h"
#include "systick.h"
#include <stddef.h>

static void spi_EmptyCallback(uint32_t value);
static void SPI_Isr(void);

void (* volatile spi_Callback1)(uint32_t) = spi_EmptyCallback;

static volatile uint32_t tispiTxBytesLeft = 0;
static volatile uint32_t tispiTotalBytesLeft = 0;
static volatile uint32_t tispiRxData;
static volatile uint32_t tispiReserved = false;

static spi_ReserveReq * volatile pFifoHead;
static spi_ReserveReq * volatile pFifoTail;

static bool spi1HwFault;

/**
 * @brief   Initialize SPI master.
 */
void spi_init1(void){
  /* Pin configuration in PortConfig.h */

  spi1HwFault = false;

  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_SSI1);

  /* SCK PC1 */
  IOCPinConfigPeriphOutput(GPIO_A_BASE, GPIO_PIN_2, IOC_MUX_OUT_SEL_SSI1_CLKOUT);
  GPIOPinTypeSSI(GPIO_A_BASE, GPIO_PIN_2);

  /* MOSI PD1 */
  //IOCPinConfigPeriphOutput(GPIO_D_BASE, GPIO_PIN_1, IOC_MUX_OUT_SEL_SSI0_TXD);
  //GPIOPinTypeSSI(GPIO_D_BASE, GPIO_PIN_1);

  /* MISO PA4 */
  IOCPinConfigPeriphInput(GPIO_A_BASE, GPIO_PIN_4, IOC_SSIRXD_SSI1);
  GPIOPinTypeSSI(GPIO_A_BASE, GPIO_PIN_4);

  SSIConfigSetExpClk(
    SSI1_BASE,
    SysCtrlClockGet(),
    SSI_FRF_MOTO_MODE_1, /* CLK low when idle. Data sampled on falling edge */
    SSI_MODE_MASTER,
    1000000,          /* 1 MHz SPI clock */
    8                 /* Using 8-bit frames. */
  );

  SSIIntRegister(SSI1_BASE, &SPI_Isr);
  IntPrioritySet(INT_SSI1, 2<<5);
  SSIIntEnable(
    SSI1_BASE,
    SSI_IM_RXIM
    | SSI_IM_RTIM
  );

  SSIEnable(SSI1_BASE);

  pFifoHead = NULL;
  pFifoTail = NULL;
}

/**
 * @brief   Reserve the SPI bus.
 *
 * @param   pReservedCb   Pointer to function that will be called when the
 *                        bus has been reserved.
 */
void spi_reserve1(spi_ReserveReq * pReserveReq){

  IntMasterDisable();

  if (tispiReserved == true){
    /*
     *  Some other driver has reserved the bus. Store the reservation request
     *  pointer so that we can inform the waiting driver when the bus is free.
     */

    if (pFifoTail == NULL){
      pFifoHead = pReserveReq;
    }
    else{
      pFifoTail->pNext = pReserveReq;
    }

    pFifoTail = pReserveReq;
    pReserveReq->pNext = NULL;
    IntMasterEnable();
  }
  else{
    /*
     *  The bus is currently not reserved. Just reserve the bus and call the
     *  callback function directly.
     */

    tispiReserved = true;
    IntMasterEnable();

    pReserveReq->pReservedCb();
  }
}

/**
 * @brief   Free the SPI bus.
 */
void spi_free1(void){
  spi_ReserveReq * pReserveReq;

  IntMasterDisable();

  /*
   *  Pop the next bus reservation request from the FIFO.
   */

  pReserveReq = pFifoHead;
  if (pReserveReq != NULL){
    pFifoHead = pReserveReq->pNext;
    if (pFifoHead == NULL){
      pFifoTail = NULL;
    }

    pReserveReq->pNext = NULL;

    /*
     *  There was a driver waiting. Call the reserved callback function to
     *  inform the driver that the bus has been reserved.
     */
    IntMasterEnable();
    pReserveReq->pReservedCb();
  }
  else{
    /*
     *  No driver was waiting for the bus. Mark the bus as free.
     */
    tispiReserved = false;
    IntMasterEnable();
  }
}

/**
 * @brief   Transfer data on SPI bus.
 *
 * @param   data        Left to be written (left-justified).
 * @param   txCount     Number of bits to be written.
 * @param   totalCount  Total number of bits to be transferred (read and write).
 */
void spi_command1(uint32_t data, int txCount, int totalCount) {
  uint32_t startTick = PopGetSystick();

  // If HW fault don't lock the state machine
  if (spi1HwFault)
    return;

  // Check for HW fault
  while((HWREG(SSI1_BASE + SSI_O_SR) & SSI_SR_BSY) != 0) {
    if (PopGetSystick() > (startTick + 1000)) {
      spi1HwFault = true;
      return;
    }
  }

  totalCount++;

#if DEBUG >= 1
  while((txCount % 8) != 0);
  while((totalCount % 8) != 0);
#endif

  /* Converting to bytes instead of bits */
  totalCount >>= 3;
  txCount >>= 3;

  tispiTotalBytesLeft = totalCount;
  tispiTxBytesLeft = txCount;
  tispiRxData = 0;

  // Copy data to the TX FIFO
  do {
    if(txCount != 0){
      HWREG(SSI1_BASE + SSI_O_DR) = data >> 24;
      data = data << 8;
      txCount--;
    }
    else {
      /*
       *  Copy dummy byte to the data register to generate clock for RX.
       */
      HWREG(SSI1_BASE + SSI_O_DR) = 0;
    }
  }
  while(--totalCount != 0);
}

/**
 * @brief   SSI peripheral interrupt service routine.
 */
static void SPI_Isr(void){
  uint32_t ssiMis;

  ssiMis = HWREG(SSI1_BASE + SSI_O_MIS);

  if(ssiMis & SSI_MIS_RTMIS){
    HWREG(SSI1_BASE + SSI_O_ICR) = SSI_ICR_RTIC;
  }

  /*
   *  Read bytes from the data register until it is empty.
   */

  while((HWREG(SSI1_BASE + SSI_O_SR) & SSI_SR_RNE) != 0){
    uint8_t rxByte;

    rxByte = HWREG(SSI1_BASE + SSI_O_DR);

    if(tispiTxBytesLeft != 0){
      tispiTxBytesLeft--;
    }
    else{
      tispiRxData <<= 8;
      tispiRxData |= rxByte;
    }

    if(--tispiTotalBytesLeft == 0){
      uint32_t value = tispiRxData;
      spi_Callback1(value);
    }
  }
}

/**
 * @brief   Dummy callback function.
 */
static void spi_EmptyCallback(uint32_t value){
}
