/******************************************************************************
*  Filename:       flash.c
*  Revised:        $Date: 2016-06-15 13:17:23 +0300 (ke, 15 kesä 2016) $
*  Revision:       $Revision: 831 $
*
*  Description:    Driver for programming the on-chip flash.
*
*  Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

//*****************************************************************************
//
//! \addtogroup flash_api
//! @{
//
//*****************************************************************************

#include <string.h>

#include "interrupt.h"
#include "../inc/hw_flash_ctrl.h"
#include "../inc/hw_memmap.h"
#include "debug.h"
#include "tiflash.h"
#include "rom.h"

//*****************************************************************************
//
//! Erases a flash main page with use of ROM function
//!
//! \param ui32Address is the start address of the flash main page to be erased.
//!
//! This function erases one 2 kB main page of the on-chip flash. After
//! erasing, the page is filled with 0xFF bytes. Locked pages cannot be
//! erased. The flash main pages do not include the upper page.
//!
//! This function does not return until the page is erased or an error
//! encountered.
//!
//! \return Returns 0 on success, -1 if erasing error is encountered,
//!         or -2 in case of illegal parameter use.
//
//*****************************************************************************
int32_t
TiFlashMainPageErase(uint32_t ui32Address)
{
    int32_t          i32Stat;               // 0 = pass, -1 = fail
    uint32_t ui32CurrentCacheMode;

    i32Stat = 0;

    //
    // Check the arguments.
    //
    ASSERT(!(ui32Address < FLASH_BASE));
    ASSERT(!(ui32Address >= (FLASH_BASE + (FlashSizeGet() * 1024) -
                             TIFLASH_ERASE_SIZE)));
    ASSERT(!(ui32Address & (TIFLASH_ERASE_SIZE - 1)));

    //
    // Save current cache mode since the ROM function will change it.
    //
    ui32CurrentCacheMode = TiFlashCacheModeGet();

    //
    // Erase the specified flash main page by calling ROM function.
    //
    IntMasterDisable();
    i32Stat = ROM_PageErase(ui32Address, TIFLASH_ERASE_SIZE);
    IntMasterEnable();
    
    //
    // Restore cache mode.
    //
    TiFlashCacheModeSet(ui32CurrentCacheMode);

    //
    // Return status pass or fail.
    //
    return(i32Stat);
}

//*****************************************************************************
//
//! Erases the upper flash page with use of ROM function
//!
//! This function erases the 2 kB upper page of the on-chip flash. After
//! erasing, the page is filled with 0xFF bytes. A locked page cannot
//! be erased.
//!
//! This function does not return until the flash page is erased or
//! an error encountered.
//!
//! \return Returns 0 on success, -1 if erasing error is encountered
//!         or, -2 in case of illegal parameter use.
//
//*****************************************************************************
int32_t
TiFlashUpperPageErase(void)
{
    uint32_t ui32UpperPageAddr;
    uint32_t ui32CurrentCacheMode;
    int32_t  i32Stat;                  // 0 = pass, -1 = fail, -2 = wrong param

    i32Stat = 0;

    //
    // Find start address of upper flash page
    //
    ui32UpperPageAddr = FLASH_BASE + (TiFlashSizeGet() * 1024) - TIFLASH_ERASE_SIZE;

    //
    // Save current cache mode since the ROM function will change it.
    //
    ui32CurrentCacheMode = TiFlashCacheModeGet();

    //
    // Erase the upper flash page by calling ROM function.
    //
    IntMasterDisable();
    i32Stat = ROM_PageErase(ui32UpperPageAddr, TIFLASH_ERASE_SIZE);
    IntMasterEnable();
    
    //
    // Restore cache mode.
    //
    TiFlashCacheModeSet(ui32CurrentCacheMode);

    //
    // Return status pass or fail.
    //
    return(i32Stat);
}

//*****************************************************************************
//
//! Programs the flash main pages by use of ROM function
//!
//! \param pui32Data is a pointer to the data to be programmed.
//! \param ui32Address is the starting address in flash to be programmed. Must
//! be a multiple of four and within the flash main pages.
//! \param ui32Count is the number of bytes to be programmed. Must be a multiple
//! of four.
//!
//! This function programs a sequence of words into the on-chip flash.
//! Programming each location consists of the result of an AND operation
//! of the new data and the existing data; in other words, bits that contain
//! 1 can remain 1 or be changed to 0, but bits that are 0 cannot be changed
//! to 1. Therefore, a word can be programmed multiple times as long as these
//! rules are followed; if a program operation attempts to change a 0 bit to
//! a 1 bit, that bit will not have its value changed.
//!
//! Because the flash is programmed one word at a time, the starting address and
//! byte count must both be multiples of four. The caller must
//! verify the programmed contents, if verification is required.
//!
//! This function does not return until the data is programmed or an
//! error encountered. Locked flash pages cannot be programmed.
//!
//! \return Returns 0 on success, -1 if a programming error is encountered
//!         or, -2 in case of illegal parameter use.
//
//*****************************************************************************
int32_t
TiFlashMainPageProgram(uint32_t *pui32Data, uint32_t ui32Address,
                     uint32_t ui32Count)
{
    uint32_t ui32CurrentCacheMode;
    int32_t  i32Stat;     // 0 = pass, -1 = fail, -2 = wrong param

    i32Stat = 0;            // Start out passing

    //
    // Check the arguments.
    //
    ASSERT(!(ui32Address             < FLASH_BASE));
    ASSERT(!((ui32Address + ui32Count) > (FLASH_BASE + (TiFlashSizeGet() * 1024) -
                                          TIFLASH_ERASE_SIZE)));
    ASSERT(!(ui32Address & 3));
    ASSERT(!(ui32Count   & 3));

    if (
      (ui32Address + ui32Count)
      > (FLASH_BASE + (TiFlashSizeGet() * 1024) - TIFLASH_ERASE_SIZE)
    ) {
      /*
       *  Not allowed to write to the upper sector with this function. CCA is
       *  in the upper sector.
       */
      return -2;
    }

    //
    // Save current cache mode since the ROM function will change it.
    //
    ui32CurrentCacheMode = TiFlashCacheModeGet();

    //
    // Program flash by executing function in ROM.
    //
    IntMasterDisable();
    i32Stat = ROM_ProgramFlash(pui32Data, ui32Address, ui32Count);
    IntMasterEnable();
    
    //
    // Restore cache mode.
    //
    TiFlashCacheModeSet(ui32CurrentCacheMode);

    //
    // Return status pass or fail.
    //
    return(i32Stat);
}

//*****************************************************************************
//
//! Programs the flash main pages by use of ROM function
//!
//! \param pui32Data    Pointer to the data to be programmed.
//! \param ui32Address  The starting address in flash to be programmed. Must
//!                     be within the flash main pages.
//! \param ui32Count    is the number of bytes to be programmed.
//!
//! This function does the same as TiFlashMainPageProgram except that this
//! function accepts a nonaligned flash address, nonaligned data pointer and
//! the size does not have to be multiples of four.
//!
//! \return Returns 0 on success, -1 if a programming error is encountered
//!         or, -2 in case of illegal parameter use.
//
//*****************************************************************************
int32_t TiFlashMainPageProgramExt(uint8_t *pData, uint32_t address, uint32_t iLen)
{
  int32_t retVal;
  uint32_t unalignedBytes;
  uint32_t alignedBuffer[10];

  unalignedBytes = address & 0x00000003U;

  /* 
   * The CC2538 can only write to word aligned addresses. Also the write size
   * must be multiples of four.
   */

  if (unalignedBytes != 0)
  {
    uint8_t * pTempData;
    uint32_t alignedAddress;

    /*
     *  The write address was not aligned. Build a four byte buffer that we
     *  can write to an aligned address. Writing 0xFF to bytes that should
     *  be left unchanged.
     */

    pTempData = (uint8_t *) alignedBuffer;
    alignedAddress = address - unalignedBytes;

    pTempData[0] = 0xFF;
    pTempData[1] = 0xFF;
    pTempData[2] = 0xFF;
    pTempData[3] = 0xFF;

    if (unalignedBytes < 2)
    {
      pTempData[1] = *pData++;
      iLen--;
    }

    if (iLen != 0 && unalignedBytes < 3)
    {
      pTempData[2] = *pData++;
      iLen--;
    }

    if (iLen)
    {
      pTempData[3] = *pData++;
      iLen--;
    }

    retVal = TiFlashMainPageProgram(alignedBuffer, alignedAddress, 4);
    if (retVal != 0)
    {
      return retVal;
    }

    address = alignedAddress + 4;
  }


  if (iLen > 3)
  {
    /*
     *  There is enough data left to write at least one full four byte word.
     */

    uint32_t bytesToCopy;

    if (((uint32_t)pData & 0x00000003U) == 0)
    {
      /*
       *  The source data pointer (pData) pointer is aligned. Write directly
       *  to flash memory without using a temporary buffer.
       */

      bytesToCopy = iLen & 0xFFFFFFFCU;
      retVal = TiFlashMainPageProgram((uint32_t *) pData, address, bytesToCopy);
      if (retVal != 0)
      {
        return retVal;
      }

      pData += bytesToCopy;
      address += bytesToCopy;
      iLen -= bytesToCopy;
    }
    else
    {
      /*
       *  The pData pointer is not aligned. The data must be copied to an
       *  aligned buffer before writing to flash memory.
       */

      do 
      {
        uint8_t * pBuffer;

        pBuffer = (uint8_t *) alignedBuffer;
        
        bytesToCopy = (iLen > 40 ? 40 : iLen) & 0xFFFFFFFCU;
        memcpy(pBuffer, pData, bytesToCopy);

        retVal = TiFlashMainPageProgram(alignedBuffer, address, bytesToCopy);
        if (retVal != 0)
        {
          return retVal;
        }

        pData += bytesToCopy;
        address += bytesToCopy;
        iLen -= bytesToCopy;
      } while (iLen > 3);
    }
  }

  if (iLen != 0)
  {
    uint8_t * pTempData;

    /*
     *  Still data left to write but not enough for a full word. Build a full
     *  word buffer with 0xFF in bytes that should be left unchanged.
     */

    pTempData = (uint8_t *) alignedBuffer;

    switch (iLen)
    {
      case 1:
        pTempData[0] = *pData++;
        pTempData[1] = 0xFF;
        pTempData[2] = 0xFF;
        break;

      case 2:
        pTempData[0] = *pData++;
        pTempData[1] = *pData++;
        pTempData[2] = 0xFF;
        break;

      case 3:
        pTempData[0] = *pData++;
        pTempData[1] = *pData++;
        pTempData[2] = *pData++;
        break;
      default:
        break;
    }
    
    pTempData[3] = 0xFF;

    retVal = TiFlashMainPageProgram(alignedBuffer, address, 4);
    if (retVal != 0)
    {
      return retVal;
    }  
  }
  
  return 0;
}

//*****************************************************************************
//
//! Programs the upper page of the flash by use of ROM function
//!
//! \param pui32Data is a pointer to the data to be programmed.
//! \param ui32Address is the starting address within the flash upper page to be
//! programmed. Must be a multiple of four and within the flash upper page.
//! \param ui32Count is the number of bytes to be programmed.  Must be a multiple
//! of four.
//!
//! This function programs a sequence of words into the on-chip flash.
//! Programming each location consists of the result of an AND operation
//! of the new data and the existing data; in other words, bits that contain
//! 1 can remain 1 or be changed to 0, but bits that are 0 cannot be changed
//! to 1. Therefore, a word can be programmed multiple times as long as these
//! rules are followed; if a program operation attempts to change a 0 bit to
//! a 1 bit, that bit will not have its value changed.
//!
//! Because the flash is programmed one word at a time, the starting address and
//! byte count must both be multiples of four. The caller must
//! verify the programmed contents, if such verification is required.
//!
//! This function does not return until the data is programmed or an
//! error encountered. A locked flash page cannot be programmed.
//!
//! \return Returns 0 on success, -1 if a programming error is encountered
//!         or, -2 in case of illegal parameter use.
//
//*****************************************************************************
int32_t
TiFlashUpperPageProgram(uint32_t *pui32Data, uint32_t ui32Address,
                      uint32_t ui32Count)
{
    uint32_t ui32CurrentCacheMode;
    int32_t  i32Stat;                // 0 = pass, -1 = fail, -2 = wrong param

    i32Stat = 0;                     // Start out passing

    //
    // Check the arguments.
    //
    ASSERT(!(ui32Address < (FLASH_BASE + (TiFlashSizeGet() * 1024) -
                            FLASH_ERASE_SIZE)));
    ASSERT(!((ui32Address + ui32Count) > (FLASH_BASE +
                                          (TiFlashSizeGet() * 1024))));
    ASSERT(!(ui32Address & 3));
    ASSERT(!(ui32Count   & 3));

    //
    // Save current cache mode since the ROM function will change it.
    //
    ui32CurrentCacheMode = TiFlashCacheModeGet();

    //
    // Program flash by executing function in ROM.
    //
    IntMasterDisable();
    i32Stat = ROM_ProgramFlash(pui32Data, ui32Address, ui32Count);
    IntMasterEnable();
    
    //
    // Clear flash controller register bit set by ROM function.
    //
    HWREG(FLASH_CTRL_FCTL) &= (~FLASH_CTRL_FCTL_UPPER_PAGE_ACCESS);

    //
    // Restore cache mode.
    //
    TiFlashCacheModeSet(ui32CurrentCacheMode);

    //
    // Return status pass or fail.
    //
    return(i32Stat);
}

//*****************************************************************************
//
//! Gets the current contents of the flash at the designated address
//!
//! \param ui32Addr is the desired address to be read within the flash.
//!
//! This function helps differentiate flash memory reads from flash
//! register reads.
//!
//! \return Returns the 32bit value as an uint32_t value.
//
//*****************************************************************************
uint32_t
TiFlashGet(uint32_t ui32Addr)
{
    return(HWREG(ui32Addr));
}

//*****************************************************************************
//
//! Gets the current state of the flash Cache Mode
//!
//! This function gets the current setting for the Cache Mode.
//!
//! \return Returns the CM bits. Return value should match one of the
//! FLASH_CACHE_MODE_<> macros defined in flash.h.
//
//*****************************************************************************
uint32_t
TiFlashCacheModeGet(void)
{
    //
    // Return a FLASH_CACHE_MODE_<> macro value.
    //
    return(HWREG(FLASH_CTRL_FCTL) & FLASH_CTRL_FCTL_CM_M);
}

//*****************************************************************************
//
//! Sets the flash Cache Mode state
//!
//! \param ui32CacheMode is the desired cache mode.
//!
//! This function sets the flash Cache Mode to the desired state and accepts
//! a right justified 2 bit setting for the Cachemode bits. The function waits
//! for the flash to be idle, reads the FCTL register contents, masks in the
//! requested setting, and writes it into the FCTL register.
//!
//! The parameter \e ui32CacheMode can have one of the following values:
//!
//! - \b FLASH_CTRL_CACHE_MODE_DISABLE
//! - \b FLASH_CTRL_CACHE_MODE_ENABLE
//! - \b FLASH_CTRL_CACHE_MODE_PREFETCH_ENABLE
//! - \b FLASH_CTRL_CACHE_MODE_REALTIME
//!
//! \return None
//
//*****************************************************************************
void
TiFlashCacheModeSet(uint32_t ui32CacheMode)
{
    uint32_t ui32Busy;
    uint32_t ui32TempValue;

    //
    // Check the arguments.
    //
    ASSERT((ui32CacheMode == FLASH_CTRL_CACHE_MODE_DISABLE) ||
           (ui32CacheMode == FLASH_CTRL_CACHE_MODE_ENABLE) ||
           (ui32CacheMode == FLASH_CTRL_CACHE_MODE_PREFETCH_ENABLE) ||
           (ui32CacheMode == FLASH_CTRL_CACHE_MODE_REALTIME));

    //
    // Wait until FLASH is not busy.
    //
    ui32Busy = 1;
    while(ui32Busy)
    {
        ui32TempValue = HWREG(FLASH_CTRL_FCTL);
        ui32Busy      = ui32TempValue & FLASH_CTRL_FCTL_BUSY;
    }

    //
    // Set desired cache mode.
    //
    ui32TempValue           &= ~FLASH_CTRL_FCTL_CM_M;
    HWREG(FLASH_CTRL_FCTL) = ui32TempValue | ui32CacheMode;
}

//*****************************************************************************
//
//! Returns the flash size in number of KBytes
//!
//! This function returns the size of the flash in KBytes as determined by
//! examining the FLASH_DIECFG0 register settings.
//!
//! \return Returns the flash size in KBytes
//
//*****************************************************************************
uint32_t
TiFlashSizeGet(void)
{
    uint32_t ui32RegValue;
    uint32_t ui32Size;

    ui32RegValue = HWREG(FLASH_CTRL_DIECFG0);
    ui32RegValue = (ui32RegValue & FLASH_CTRL_DIECFG0_FLASH_SIZE_M) >>
                   FLASH_CTRL_DIECFG0_FLASH_SIZE_S;

    switch(ui32RegValue)
    {
    case 0x04:
        ui32Size = 512;
        break;
    case 0x03:
        ui32Size = 384;
        break;
    case 0x02:
        ui32Size = 256;
        break;
    case 0x01:
        ui32Size = 128;
        break;
    case 0x00:
        ui32Size =  64;
        break;
    default:
        ui32Size =  64;
        break;
    }
    return(ui32Size);
}

//*****************************************************************************
//
//! Returns the SRAM size in number of KBytes
//!
//! This function returns the size of the SRAM in KBytes as determined by
//! examining the FLASH_DIECFG0 register settings.
//!
//! \return Returns the SRAM size in KBytes
//
//*****************************************************************************
uint32_t
TiFlashSramSizeGet(void)
{
    uint32_t ui32RegValue;
    uint32_t ui32Size;

    ui32RegValue = HWREG(FLASH_CTRL_DIECFG0);
    ui32RegValue = (ui32RegValue & FLASH_CTRL_DIECFG0_SRAM_SIZE_M) >>
                   FLASH_CTRL_DIECFG0_SRAM_SIZE_S;

    switch(ui32RegValue)
    {
    case 0x04:
        ui32Size = 32;
        break;
    case 0x01:
        ui32Size =  8;
        break;
    case 0x00:
        ui32Size = 16;
        break;
    default:
        ui32Size = 32;
        break;
    }
    return(ui32Size);
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
