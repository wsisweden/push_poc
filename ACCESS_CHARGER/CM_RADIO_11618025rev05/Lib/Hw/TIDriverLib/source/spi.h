#ifndef SPI_H
#define SPI_H

#include <stdint.h>

/* A transaction is started by calling spi_command(...) and then result
 * is receieved it is sent back by calling the set function callback function
 * from within an interrupt. There is no real dead line since only 32 data
 * bits is sent in each transaction.
 *
 * This software module use the SPI interrupt internally to do the callaback.
 */

typedef void (* spi_ReservedCb)(void);

typedef struct spi_reserveReq {
	struct spi_reserveReq * pNext;
	spi_ReservedCb pReservedCb;
} spi_ReserveReq;

extern void (* volatile spi_Callback)(uint32_t);                      // write this with function that should be called then program done

void spi_init(void);                                                  // Initialize SPI module
void spi_setup_SS_SETUP(uint16_t out);                                // Set SPI_OUT in SPI Setup register
void spi_reserve(spi_ReserveReq * pReserveReq);
void spi_free(void);

#define SPI_COMMAND(data, dataLen, Clocks) spi_command(data, dataLen, Clocks);

void spi_command(uint32_t data, int txCount, int totalCount);        // Call to send data



extern void (* volatile spi_Callback1)(uint32_t);                 // write this with function that should be called then program done

void spi_init1(void);                                             // Initialize SPI module
void spi_reserve1(spi_ReserveReq * pReserveReq);
void spi_free1(void);
void spi_command1(uint32_t data, int txCount, int totalCount);        // Call to send data

#endif
