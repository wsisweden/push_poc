/**************************************************************************************************
  Filename:       mac_csp_tx.c
  Revised:        $Date: 2015-10-30 18:18:05 +0200 (pe, 30 loka 2015) $
  Revision:       $Revision: 751 $

  Description:    Describe the purpose and contents of the file.


  Copyright 2006-2012 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */

/* hal */
#include "hal_types.h"
#include "hal_defs.h"
#include "../TIDriverLib/source/interrupt.h"
#include "hal_rf.h"

/* high-level */
//#include "mac_spec.h"
//#include "mac_pib.h"

/* exported low-level */
//#include "mac_low_level.h"

/* low-level specific */
#include "mac_csp_tx.h"
//#include "mac_tx.h"
//#include "mac_rx.h"
//#include "mac_rx_onoff.h"

/* target specific */
//#include "mac_radio_defs.h"

/* debug */
//#include "mac_assert.h"

#include "../TIDriverLib/inc/hw_rfcore_xreg.h"
#include "../TIDriverLib/inc/hw_rfcore_sfr.h"

/* ------------------------------------------------------------------------------------------------
 *                                       Interrupt Macros
 * ------------------------------------------------------------------------------------------------
 */

typedef bool halIntState_t;

#define HAL_ENTER_CRITICAL_SECTION(x)  \
  do { (x) = !IntMasterDisable(); } while (0)

#define HAL_EXIT_CRITICAL_SECTION(x) \
  do { if (x) { (void) IntMasterEnable(); } } while (0)

/* Hal Critical statement definition */
#define HAL_CRITICAL_STATEMENT(x)       st( halIntState_t s; HAL_ENTER_CRITICAL_SECTION(s); x; HAL_EXIT_CRITICAL_SECTION(s); )




/* Status */
#define MAC_SUCCESS                 0x00  /* Operation successful */
#define MAC_AUTOACK_PENDING_ALL_ON  0xFE  /* The AUTOPEND pending all is turned on */
#define MAC_AUTOACK_PENDING_ALL_OFF 0xFF  /* The AUTOPEND pending all is turned off */
#define MAC_BEACON_LOSS             0xE0  /* The beacon was lost following a synchronization request */
#define MAC_CHANNEL_ACCESS_FAILURE  0xE1  /* The operation or data request failed because of
                                             activity on the channel */
#define MAC_COUNTER_ERROR           0xDB  /* The frame counter puportedly applied by the originator of
                                             the received frame is invalid */
#define MAC_DENIED                  0xE2  /* The MAC was not able to enter low power mode. */
#define MAC_DISABLE_TRX_FAILURE     0xE3  /* Unused */
#define MAC_FRAME_TOO_LONG          0xE5  /* The received frame or frame resulting from an operation
                                             or data request is too long to be processed by the MAC */
#define MAC_IMPROPER_KEY_TYPE       0xDC  /* The key purportedly applied by the originator of the
                                             received frame is not allowed */
#define MAC_IMPROPER_SECURITY_LEVEL 0xDD  /* The security level purportedly applied by the originator of
                                             the received frame does not meet the minimum security level */
#define MAC_INVALID_ADDRESS         0xF5  /* The data request failed because neither the source address nor
                                             destination address parameters were present */
#define MAC_INVALID_GTS             0xE6  /* Unused */
#define MAC_INVALID_HANDLE          0xE7  /* The purge request contained an invalid handle */
#define MAC_INVALID_INDEX           0xF9  /* Unused */
#define MAC_INVALID_PARAMETER       0xE8  /* The API function parameter is out of range */
#define MAC_LIMIT_REACHED           0xFA  /* The scan terminated because the PAN descriptor storage limit
                                             was reached */
#define MAC_NO_ACK                  0xE9  /* The operation or data request failed because no
                                             acknowledgement was received */
#define MAC_NO_BEACON               0xEA  /* The scan request failed because no beacons were received or the
                                             orphan scan failed because no coordinator realignment was received */
#define MAC_NO_DATA                 0xEB  /* The associate request failed because no associate response was received
                                             or the poll request did not return any data */
#define MAC_NO_SHORT_ADDRESS        0xEC  /* The short address parameter of the start request was invalid */
#define MAC_ON_TIME_TOO_LONG        0xF6  /* Unused */
#define MAC_OUT_OF_CAP              0xED  /* Unused */
#define MAC_PAN_ID_CONFLICT         0xEE  /* A PAN identifier conflict has been detected and
                                             communicated to the PAN coordinator */
#define MAC_PAST_TIME               0xF7  /* Unused */
#define MAC_READ_ONLY               0xFB  /* A set request was issued with a read-only identifier */
#define MAC_REALIGNMENT             0xEF  /* A coordinator realignment command has been received */
#define MAC_SCAN_IN_PROGRESS        0xFC  /* The scan request failed because a scan is already in progress */
#define MAC_SECURITY_ERROR          0xE4  /* Cryptographic processing of the received secure frame failed */
#define MAC_SUPERFRAME_OVERLAP      0xFD  /* The beacon start time overlapped the coordinator transmission time */
#define MAC_TRACKING_OFF            0xF8  /* The start request failed because the device is not tracking
                                             the beacon of its coordinator */
#define MAC_TRANSACTION_EXPIRED     0xF0  /* The associate response, disassociate request, or indirect
                                             data transmission failed because the peer device did not respond
                                             before the transaction expired or was purged */
#define MAC_TRANSACTION_OVERFLOW    0xF1  /* The request failed because MAC data buffers are full */
#define MAC_TX_ACTIVE               0xF2  /* Unused */
#define MAC_UNAVAILABLE_KEY         0xF3  /* The operation or data request failed because the
                                             security key is not available */
#define MAC_UNSUPPORTED_ATTRIBUTE   0xF4  /* The set or get request failed because the attribute is not supported */
#define MAC_UNSUPPORTED_LEGACY      0xDE  /* The received frame was secured with legacy security which is
                                             not supported */
#define MAC_UNSUPPORTED_SECURITY    0xDF  /* The security of the received frame is not supported */
#define MAC_UNSUPPORTED             0x18  /* The operation is not supported in the current configuration */
#define MAC_BAD_STATE               0x19  /* The operation could not be performed in the current state */
#define MAC_NO_RESOURCES            0x1A  /* The operation could not be completed because no
                                             memory resources were available */
#define MAC_ACK_PENDING             0x1B  /* For internal use only */
#define MAC_NO_TIME                 0x1C  /* For internal use only */
#define MAC_TX_ABORTED              0x1D  /* For internal use only */
#define MAC_DUPLICATED_ENTRY        0x1E  /* For internal use only - A duplicated entry is added to the source matching table */





/* T2CTRL */
#define LATCH_MODE            BV(3)
#define TIMER2_STATE          BV(2)
#define TIMER2_SYNC           BV(1)
#define TIMER2_RUN            BV(0)

/* T2IRQF */
#define TIMER2_OVF_COMPARE2F  BV(5)
#define TIMER2_OVF_COMPARE1F  BV(4)
#define TIMER2_OVF_PERF       BV(3)
#define TIMER2_COMPARE2F      BV(2)
#define TIMER2_COMPARE1F      BV(1)
#define TIMER2_PERF           BV(0)

/* RFIRQF0 */
#define IRQ_SFD         BV(1)
#define IRQ_FIFOP       BV(2)

/* RFIRQF1 */
#define IRQ_TXACKDONE   BV(0)
#define IRQ_TXDONE      BV(1)
#define IRQ_CSP_MANINT  BV(3)
#define IRQ_CSP_STOP    BV(4)

/* RFIRQM0 */
#define IM_SFD          BV(1)
#define IM_FIFOP        BV(2)

/* RFIRQM1 */
#define IM_TXACKDONE    BV(0)
#define IM_TXDONE       BV(1)
#define IM_CSP_MANINT   BV(3)
#define IM_CSP_STOP     BV(4)





#define MAC_MCU_WRITE_RFIRQF0(x)      HAL_CRITICAL_STATEMENT( S1CON = 0x00; HWREG(RFCORE_SFR_RFIRQF0) = x; )
#define MAC_MCU_WRITE_RFIRQF1(x)      HAL_CRITICAL_STATEMENT( S1CON = 0x00; HWREG(RFCORE_SFR_RFIRQF1) = x; )
#define MAC_MCU_OR_RFIRQM0(x)         st( HWREG(RFCORE_XREG_RFIRQM0) |= x; )  /* compiler must use atomic ORL instruction */
#define MAC_MCU_AND_RFIRQM0(x)        st( HWREG(RFCORE_XREG_RFIRQM0) &= x; )  /* compiler must use atomic ANL instruction */
#define MAC_MCU_OR_RFIRQM1(x)         st( HWREG(RFCORE_XREG_RFIRQM1) |= x; )  /* compiler must use atomic ORL instruction */
#define MAC_MCU_AND_RFIRQM1(x)        st( HWREG(RFCORE_XREG_RFIRQM1) &= x; )  /* compiler must use atomic ANL instruction */

#define MAC_MCU_FIFOP_ENABLE_INTERRUPT()              MAC_MCU_OR_RFIRQM0(IM_FIFOP)
#define MAC_MCU_FIFOP_DISABLE_INTERRUPT()             MAC_MCU_AND_RFIRQM0((IM_FIFOP ^ 0xFF))
#define MAC_MCU_FIFOP_CLEAR_INTERRUPT()               MAC_MCU_WRITE_RFIRQF0((IRQ_FIFOP ^ 0xFF))

#define MAC_MCU_TXACKDONE_ENABLE_INTERRUPT()          MAC_MCU_OR_RFIRQM1(IM_TXACKDONE)
#define MAC_MCU_TXACKDONE_DISABLE_INTERRUPT()         MAC_MCU_AND_RFIRQM1((IM_TXACKDONE ^ 0xFF))
//#define MAC_MCU_TXACKDONE_CLEAR_INTERRUPT()           MAC_MCU_WRITE_RFIRQF1((IRQ_TXACKDONE ^ 0xFF))

#define MAC_MCU_CSP_STOP_ENABLE_INTERRUPT()           MAC_MCU_OR_RFIRQM1(IM_CSP_STOP)
#define MAC_MCU_CSP_STOP_DISABLE_INTERRUPT()          MAC_MCU_AND_RFIRQM1((IM_CSP_STOP ^ 0xFF))
#define MAC_MCU_CSP_STOP_CLEAR_INTERRUPT()            MAC_MCU_WRITE_RFIRQF1((IRQ_CSP_STOP ^ 0xFF))
#define MAC_MCU_CSP_STOP_INTERRUPT_IS_ENABLED()       (HWREG(RFCORE_XREG_RFIRQM1) & IM_CSP_STOP)

#define MAC_MCU_CSP_INT_ENABLE_INTERRUPT()            MAC_MCU_OR_RFIRQM1(IM_CSP_MANINT)
#define MAC_MCU_CSP_INT_DISABLE_INTERRUPT()           MAC_MCU_AND_RFIRQM1((IM_CSP_MANINT ^ 0xFF))
#define MAC_MCU_CSP_INT_CLEAR_INTERRUPT()             MAC_MCU_WRITE_RFIRQF1((IRQ_CSP_MANINT ^ 0xFF))
#define MAC_MCU_CSP_INT_INTERRUPT_IS_ENABLED()        (HWREG(RFCORE_XREG_RFIRQM1) & IM_CSP_MANINT)

#define MAC_MCU_RFERR_ENABLE_INTERRUPT()              st( RFERRM |=  RFERR_RXOVERF; )
#define MAC_MCU_RFERR_DISABLE_INTERRUPT()             st( RFERRM &= (RFERR_RXOVERF ^ 0xFF); )



/* ------------------------------------------------------------------------------------------------
 *                                       MAC Timer Macros
 * ------------------------------------------------------------------------------------------------
 */
#define T2M_OVF_BITS    (BV(6) | BV(5) | BV(4))
#define T2M_BITS        (BV(2) | BV(1) | BV(0))

#define T2M_OVFSEL(x)   ((x) << 4)
#define T2M_SEL(x)      (x)

#define T2M_T2OVF       T2M_OVFSEL(0UL)
#define T2M_T2OVF_CAP   T2M_OVFSEL(1UL)
#define T2M_T2OVF_PER   T2M_OVFSEL(2UL)
#define T2M_T2OVF_CMP1  T2M_OVFSEL(3UL)
#define T2M_T2OVF_CMP2  T2M_OVFSEL(4UL)

#define T2M_T2TIM       T2M_SEL(0UL)
#define T2M_T2_CAP      T2M_SEL(1UL)
#define T2M_T2_PER      T2M_SEL(2UL)
#define T2M_T2_CMP1     T2M_SEL(3UL)
#define T2M_T2_CMP2     T2M_SEL(4UL)

#define MAC_MCU_T2_ACCESS_OVF_COUNT_VALUE()   st( T2MSEL = T2M_T2OVF; )
#define MAC_MCU_T2_ACCESS_OVF_CAPTURE_VALUE() st( T2MSEL = T2M_T2OVF_CAP; )
#define MAC_MCU_T2_ACCESS_OVF_PERIOD_VALUE()  st( T2MSEL = T2M_T2OVF_PER; )
#define MAC_MCU_T2_ACCESS_OVF_CMP1_VALUE()    st( T2MSEL = T2M_T2OVF_CMP1; )
#define MAC_MCU_T2_ACCESS_OVF_CMP2_VALUE()    st( T2MSEL = T2M_T2OVF_CMP2; )

#define MAC_MCU_T2_ACCESS_COUNT_VALUE()       st( T2MSEL = T2M_T2TIM; )
#define MAC_MCU_T2_ACCESS_CAPTURE_VALUE()     st( T2MSEL = T2M_T2_CAP; )
#define MAC_MCU_T2_ACCESS_PERIOD_VALUE()      st( T2MSEL = T2M_T2_PER; )
#define MAC_MCU_T2_ACCESS_CMP1_VALUE()        st( T2MSEL = T2M_T2_CMP1; )
#define MAC_MCU_T2_ACCESS_CMP2_VALUE()        st( T2MSEL = T2M_T2_CMP2; )

#define MAC_MCU_CONFIG_CSP_EVENT1()           st( T2CSPCFG = 1UL; )






/* SFR registers */
#define T2CSPCFG                     HWREG(RFCORE_SFR_MTCSPCFG)
#define T2CTRL                       HWREG(RFCORE_SFR_MTCTRL)
#define T2IRQM                       HWREG(RFCORE_SFR_MTIRQM)
#define T2IRQF                       HWREG(RFCORE_SFR_MTIRQF)
#define T2MSEL                       HWREG(RFCORE_SFR_MTMSEL)
#define T2M0                         HWREG(RFCORE_SFR_MTM0)
#define T2M1                         HWREG(RFCORE_SFR_MTM1)
#define T2MOVF2                      HWREG(RFCORE_SFR_MTMOVF2)
#define T2MOVF1                      HWREG(RFCORE_SFR_MTMOVF1)
#define T2MOVF0                      HWREG(RFCORE_SFR_MTMOVF0)
#define RFD                          HWREG(RFCORE_SFR_RFDATA)       
#define RFERRF                       HWREG(RFCORE_SFR_RFERRF)      
#define RFIRQF1                      HWREG(RFCORE_SFR_RFIRQF1)      
#define RFIRQF0                      HWREG(RFCORE_SFR_RFIRQF0)       

/* XREG registers */
#define CSPX                         HWREG(RFCORE_XREG_CSPX)        
#define CSPY                         HWREG(RFCORE_XREG_CSPY)        
#define CSPZ                         HWREG(RFCORE_XREG_CSPZ)        
#define CSPT                         HWREG(RFCORE_XREG_CSPT)





/* bit value used to form values of macTxActive */
#define MAC_TX_ACTIVE_PHYSICALLY_BV      0x80

/* state values for macTxActive; note zero is reserved for inactive state */
#define MAC_TX_ACTIVE_NO_ACTIVITY           0x00 /* zero reserved for boolean use, e.g. !macTxActive */
#define MAC_TX_ACTIVE_INITIALIZE            0x01
#define MAC_TX_ACTIVE_QUEUED                0x02
#define MAC_TX_ACTIVE_GO                   (0x03 | MAC_TX_ACTIVE_PHYSICALLY_BV)
#define MAC_TX_ACTIVE_CHANNEL_BUSY          0x04
#define MAC_TX_ACTIVE_DONE                 (0x05 | MAC_TX_ACTIVE_PHYSICALLY_BV)
#define MAC_TX_ACTIVE_LISTEN_FOR_ACK       (0x06 | MAC_TX_ACTIVE_PHYSICALLY_BV)
#define MAC_TX_ACTIVE_POST_ACK             (0x07 | MAC_TX_ACTIVE_PHYSICALLY_BV)





#define MAC_ASSERT(condition_)  while (!(condition_));



/* The number of symbols forming a basic CSMA-CA time period */
#define MAC_A_UNIT_BACKOFF_PERIOD       20

/* Microseconds in one symbol */
#define MAC_SPEC_USECS_PER_SYMBOL           16

/* Microseconds in one backoff period */
#define MAC_SPEC_USECS_PER_BACKOFF          (MAC_SPEC_USECS_PER_SYMBOL * MAC_A_UNIT_BACKOFF_PERIOD)


#define HAL_CPU_CLOCK_MHZ     32

#define MAC_RADIO_TIMER_TICKS_PER_USEC()              HAL_CPU_CLOCK_MHZ /* never fractional */
#define MAC_RADIO_TIMER_TICKS_PER_BACKOFF()           (HAL_CPU_CLOCK_MHZ * MAC_SPEC_USECS_PER_BACKOFF)
#define MAC_RADIO_TIMER_TICKS_PER_SYMBOL()            (HAL_CPU_CLOCK_MHZ * MAC_SPEC_USECS_PER_SYMBOL)


uint8 macTxActive;
uint8 macTxType;
uint8 macTxBe;
uint8 macTxCsmaBackoffDelay;
uint8 macTxGpInterframeDelay;







/* ------------------------------------------------------------------------------------------------
 *                                   CSP Defines / Macros
 * ------------------------------------------------------------------------------------------------
 */
/* immediate strobe commands */
#define ISSTART     0xE1
#define ISSTOP      0xE2
#define ISCLEAR     0xFF

/* strobe processor instructions */
#define SKIP(s,c)   (0x00 | (((s) & 0x07) << 4) | ((c) & 0x0F))   /* skip 's' instructions if 'c' is true  */
#define WHILE(c)    SKIP(0,c)              /* pend while 'c' is true (derived instruction)        */
#define WAITW(w)    (0x80 | ((w) & 0x1F))  /* wait for 'w' number of MAC timer overflows          */
#define WEVENT1     (0xB8)                 /* wait for MAC timer compare                          */
#define WAITX       (0xBC)                 /* wait for CSPX number of MAC timer overflows         */
#define LABEL       (0xBB)                 /* set next instruction as start of loop               */
#define RPT(c)      (0xA0 | ((c) & 0x0F))  /* if condition is true jump to last label             */
#define INT         (0xBA)                 /* assert IRQ_CSP_INT interrupt                        */
#define INCY        (0xC1)                 /* increment CSPY                                      */
#define INCMAXY(m)  (0xC8 | ((m) & 0x07))  /* increment CSPY but not above maximum value of 'm'   */
#define DECX        (0xC3)                 /* decrement CSPX                                      */
#define DECY        (0xC4)                 /* decrement CSPY                                      */
#define DECZ        (0xC5)                 /* decrement CSPZ                                      */
#define RANDXY      (0xBD)                 /* load the lower CSPY bits of CSPX with random value  */

/* strobe processor command instructions */
#define SSTOP       (0xD2)    /* stop program execution                                      */
#define SNOP        (0xD0)    /* no operation                                                */
#define STXCAL      (0xDC)    /* enable and calibrate frequency synthesizer for TX           */
#define SRXON       (0xD3)    /* turn on receiver                                            */
#define STXON       (0xD9)    /* transmit after calibration                                  */
#define STXONCCA    (0xDA)    /* transmit after calibration if CCA indicates clear channel   */
#define SRFOFF      (0xDF)    /* turn off RX/TX                                              */
#define SFLUSHRX    (0xDD)    /* flush receive FIFO                                          */
#define SFLUSHTX    (0xDE)    /* flush transmit FIFO                                         */
#define SACK        (0xD6)    /* send ACK frame                                              */
#define SACKPEND    (0xD7)    /* send ACK frame with pending bit set                         */

/* conditions for use with instructions SKIP and RPT */
#define C_CCA_IS_VALID        0x00
#define C_SFD_IS_ACTIVE       0x01
#define C_CPU_CTRL_IS_ON      0x02
#define C_END_INSTR_MEM       0x03
#define C_CSPX_IS_ZERO        0x04
#define C_CSPY_IS_ZERO        0x05
#define C_CSPZ_IS_ZERO        0x06
#define C_RSSI_IS_VALID       0x07

/* negated conditions for use with instructions SKIP and RPT */
#define C_NEGATE(c)   ((c) | 0x08)
#define C_CCA_IS_INVALID      C_NEGATE(C_CCA_IS_VALID)
#define C_SFD_IS_INACTIVE     C_NEGATE(C_SFD_IS_ACTIVE)
#define C_CPU_CTRL_IS_OFF     C_NEGATE(C_CPU_CTRL_IS_ON)
#define C_NOT_END_INSTR_MEM   C_NEGATE(C_END_INSTR_MEM)
#define C_CSPX_IS_NON_ZERO    C_NEGATE(C_CSPX_IS_ZERO)
#define C_CSPY_IS_NON_ZERO    C_NEGATE(C_CSPY_IS_ZERO)
#define C_CSPZ_IS_NON_ZERO    C_NEGATE(C_CSPZ_IS_ZERO)
#define C_RSSI_IS_INVALID     C_NEGATE(C_RSSI_IS_VALID)


/* ------------------------------------------------------------------------------------------------
 *                                         Defines
 * ------------------------------------------------------------------------------------------------
 */

/* CSPZ return values from CSP program */
#define CSPZ_CODE_TX_DONE           0
#define CSPZ_CODE_CHANNEL_BUSY      1
#define CSPZ_CODE_TX_ACK_TIME_OUT   2


/* ------------------------------------------------------------------------------------------------
 *                                     Local Programs
 * ------------------------------------------------------------------------------------------------
 */
static void  cspPrepForTxProgram(void);
static void  cspWeventSetTriggerNow(void);
static void  cspWeventSetTriggerSymbols(uint8 symbols);
static uint8 cspReadCountSymbols(void);



/* ------------------------------------------------------------------------------------------------
 *                                          Macros
 * ------------------------------------------------------------------------------------------------
 */
#define CSP_STOP_AND_CLEAR_PROGRAM()          st( RFST = ISSTOP; RFST = ISCLEAR; )
#define CSP_START_PROGRAM()                   st( RFST = ISSTART; )

/*
 *  These macros improve readability of using T2CMP in conjunction with WEVENT.
 *
 *  The timer2 compare, T2CMP, only compares one byte of the 16-bit timer register.
 *  It is configurable and has been set to compare against the upper byte of the timer value.
 *  The CSP instruction WEVENT waits for the timer value to be greater than or equal
 *  the value of T2CMP.
 *
 *  Reading the timer value is done by reading the low byte first.  This latches the
 *  high byte.  A trick with the ternary operator is used by a macro below to force a
 *  read of the low byte when returning the value of the high byte.
 *
 *  CSP_WEVENT_SET_TRIGGER_NOW()      - sets the WEVENT1 trigger point at the current timer count
 *  CSP_WEVENT_SET_TRIGGER_SYMBOLS(x) - sets the WEVENT1 trigger point in symbols
 *  CSP_WEVENT_READ_COUNT_SYMBOLS()   - reads the current timer count in symbols
 */
#define T2THD_TICKS_PER_SYMBOL                (MAC_RADIO_TIMER_TICKS_PER_SYMBOL() >> 8)

#define CSP_WEVENT_CLEAR_TRIGGER()            st( T2IRQF = ~TIMER2_COMPARE1F; )
#define CSP_WEVENT_SET_TRIGGER_NOW()          cspWeventSetTriggerNow()
#define CSP_WEVENT_SET_TRIGGER_SYMBOLS(x)     cspWeventSetTriggerSymbols(x)
#define CSP_WEVENT_READ_COUNT_SYMBOLS()       cspReadCountSymbols()

/*
 *  Number of bits used for aligning a slotted transmit to the backoff count (plus
 *  derived values).  There are restrictions on this value.  Compile time integrity
 *  checks will catch an illegal setting of this value.  A full explanation accompanies
 *  this compile time check (see bottom of this file).
 */
#define SLOTTED_TX_MAX_BACKOFF_COUNTDOWN_NUM_BITS     4
#define SLOTTED_TX_MAX_BACKOFF_COUNTDOWN              (1 << SLOTTED_TX_MAX_BACKOFF_COUNTDOWN_NUM_BITS)
#define SLOTTED_TX_BACKOFF_COUNT_ALIGN_BIT_MASK       (SLOTTED_TX_MAX_BACKOFF_COUNTDOWN - 1)





static uint8 macRxOnFlag;
static uint8 macRxEnableFlags;
static uint8  rxResetFlag;
static uint8  rxIsrActiveFlag;

static uint8 nb;
static uint8 txSeqn;
static uint8 txAckReq;
static uint8 txRetransmitFlag;


/*=================================================================================================
 * @fn          txComplete
 *
 * @brief       Transmit has completed.  Perform needed maintenance and return status of
 *              the transmit via callback function.
 *
 * @param       status - status of the transmit that just went out
 *
 * @return      none
 *=================================================================================================
 */
static void txComplete(uint8 status)
{
  /* reset the retransmit flag */
  txRetransmitFlag = 0;

  /* update tx state; turn off receiver if nothing is keeping it on */
  macTxActive = MAC_TX_ACTIVE_NO_ACTIVITY;
  
  /* turn off receive if allowed */
  macRxOffRequest();

  /* return status of transmit via callback function */
  macTxCompleteCallback(status);
}




/**************************************************************************************************
 * @fn          macTxCollisionWithRxCallback
 *
 * @brief       Function called if transmit strobed on top of a receive.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
static void macTxCollisionWithRxCallback(void)
{
  macRxHaltCleanup();
}



/**************************************************************************************************
 * @fn          macTxDoneCallback
 *
 * @brief       This callback is executed when transmit completes.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
static void macTxDoneCallback(void)
{
  halIntState_t  s;

  /*
   *  There is a small chance this function could be called twice for a single transmit.
   *  To prevent logic from executing twice, the state variable macTxActive is used as
   *  a gating mechanism to guarantee single time execution.
   */
  HAL_ENTER_CRITICAL_SECTION(s);
  if (macTxActive == MAC_TX_ACTIVE_GO)
  {
    if (macRxActive)
    {
      /* RX was partly done just before TX. Reset the RX state. */
      macTxCollisionWithRxCallback();
    }

    macTxActive = MAC_TX_ACTIVE_DONE;
    HAL_EXIT_CRITICAL_SECTION(s);

    txComplete(MAC_SUCCESS);
  }
  else
  {
    HAL_EXIT_CRITICAL_SECTION(s);
  }
}


/**************************************************************************************************
 * @fn          macTxTimestampCallback
 *
 * @brief       This callback function records the timestamp into the receive data structure.
 *              It should be called as soon as possible after there is a valid timestamp.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
static void macTxTimestampCallback(void)
{
  //MAC_ASSERT(pMacDataTx != NULL); /* transmit structure must be there */

  //pMacDataTx->internal.timestamp  = macBackoffTimerCapture();
  //pMacDataTx->internal.timestamp2 = MAC_RADIO_TIMER_CAPTURE();
}


/**************************************************************************************************
 * @fn          macRxOn
 *
 * @brief       Turn on the receiver if it's not already on.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
static void macRxOn(void)
{
  halIntState_t  s;

  HAL_ENTER_CRITICAL_SECTION(s);
  if (!macRxOnFlag)
  {
    macRxOnFlag = 1;
    ISRXON();
  }
  HAL_EXIT_CRITICAL_SECTION(s);
}


/**************************************************************************************************
 * @fn          macCspTxReset
 *
 * @brief       Reset the CSP.  Immediately halts any running program.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void macCspTxReset(void)
{
  MAC_MCU_CSP_STOP_DISABLE_INTERRUPT();
  MAC_MCU_CSP_INT_DISABLE_INTERRUPT();
  CSP_STOP_AND_CLEAR_PROGRAM();
}


/*=================================================================================================
 * @fn          cspWeventSetTriggerNow
 *
 * @brief       sets the WEVENT1 trigger point at the current timer count
 *
 * @param       none
 *
 * @return      symbols
 *=================================================================================================
 */
static void cspWeventSetTriggerNow(void)
{
  halIntState_t  s;
  uint8          temp0, temp1;

  /* Clear the compare interrupt flag for debugging purpose. */
  CSP_WEVENT_CLEAR_TRIGGER();

  /* copy current timer count to compare */
  HAL_ENTER_CRITICAL_SECTION(s);
  MAC_MCU_T2_ACCESS_COUNT_VALUE();
  temp0 = T2M0;
  temp1 = T2M1;

#if 0
  /* MAC timer bug on the cc2530 PG1 made it impossible to use
   * compare = 0 for both the timer and the overflow counter.
   */
  if ((macChipVersion <= REV_B) && (temp0 == 0) && (temp1 == 0))
  {
    temp0++;
  }
#endif

  MAC_MCU_T2_ACCESS_CMP1_VALUE();
  T2M0 = temp0;
  T2M1 = temp1;
  HAL_EXIT_CRITICAL_SECTION(s);
}


/*=================================================================================================
 * @fn          cspWeventSetTriggerSymbols
 *
 * @brief       sets the WEVENT1 trigger point in symbols
 *
 * @param       symbols
 *
 * @return      none
 *=================================================================================================
 */
static void cspWeventSetTriggerSymbols(uint8 symbols)
{
  halIntState_t  s;
  uint16         cmp;

  MAC_ASSERT(symbols <= MAC_A_UNIT_BACKOFF_PERIOD);

  /* Clear the compare interrupt flag for debugging purpose. */
  CSP_WEVENT_CLEAR_TRIGGER();

  HAL_ENTER_CRITICAL_SECTION(s);
  MAC_MCU_T2_ACCESS_CMP1_VALUE();
  cmp  = (symbols) * MAC_RADIO_TIMER_TICKS_PER_SYMBOL();

#if 0
  /* MAC timer bug on the cc2530 PG1 made it impossible to use
   * compare = 0 for both the timer and the overflow counter.
   */
  if ((macChipVersion <= REV_B) && (cmp == 0))
  {
    cmp++;
  }
#endif

  T2M0 = (cmp & 0xFF);
  T2M1 = (cmp >> 8);
  HAL_EXIT_CRITICAL_SECTION(s);
}


/*=================================================================================================
 * @fn          cspReadCountSymbols
 *
 * @brief       reads the current timer count in symbols
 *
 * @param       none
 *
 * @return      symbols
 *=================================================================================================
 */
static uint8 cspReadCountSymbols(void)
{
  uint8          countHigh, countLow;
  uint16         interim;
  halIntState_t  s;

  HAL_ENTER_CRITICAL_SECTION(s);
  MAC_MCU_T2_ACCESS_COUNT_VALUE();
  countLow  = T2M0;
  countHigh = T2M1;
  HAL_EXIT_CRITICAL_SECTION(s);
  
  /* Use of interim variable is not required in the logic,
   * the following should have been good enough:
   *   return (((countHigh << 8) | countLow) / MAC_RADIO_TIMER_TICKS_PER_SYMBOL());
   * However, IAR compiler optimizes the code incorrectly and skips
   * T2M0 reading when MAC_RADIO_TIMER_TICKS_PER_SYMBOL() happens to be certain
   * constant values that makes countLow ignorable in the result value.
   * Skipping T2M0 register reading causes T2M1 reading to be incorrect because
   * T2M1 requires T2M0 reading to latch the values.
   */
  interim = (countHigh << 8) | countLow;
  interim /= MAC_RADIO_TIMER_TICKS_PER_SYMBOL();
  return interim;
}





#if 0
/**************************************************************************************************
 * @fn          macCspForceTxDoneIfPending
 *
 * @brief       The function clears out any pending TX done logic.  Used by receive logic
 *              to make sure its ISR does not prevent transmit from completing in a reasonable
 *              amount of time.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void macCspForceTxDoneIfPending(void)
{
  if ((CSPZ == CSPZ_CODE_TX_DONE) &&  MAC_MCU_CSP_STOP_INTERRUPT_IS_ENABLED())
  {
    MAC_MCU_CSP_STOP_DISABLE_INTERRUPT();
    if (MAC_MCU_CSP_INT_INTERRUPT_IS_ENABLED())
    {
      macCspTxIntIsr();
    }
    macTxDoneCallback();
  }
}
#endif

#if 0
/**************************************************************************************************
 * @fn          macCspTxRequestAckTimeoutCallback
 *
 * @brief       Requests a callback after the ACK timeout period has expired.  At that point,
 *              the function macCspTxStopIsr() is called via an interrupt.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void macCspTxRequestAckTimeoutCallback(void)
{
  uint8 startSymbol;
  uint8 symbols;
  uint8 rollovers;

  MAC_ASSERT(!(HWREG(RFCORE_XREG_RFIRQM1) & IM_CSP_STOP)); /* already an active CSP program */

  /* record current symbol count */
  startSymbol = CSP_WEVENT_READ_COUNT_SYMBOLS();

  /* set symbol timeout from PIB */
  symbols = pMacPib->ackWaitDuration;

  /* make sure delay value is not too small for logic to handle */
  MAC_ASSERT(symbols > MAC_A_UNIT_BACKOFF_PERIOD);  /* symbols timeout period must be great than a backoff */

  /* subtract out symbols left in current backoff period */
  symbols = symbols - (MAC_A_UNIT_BACKOFF_PERIOD - startSymbol);

  /* calculate rollovers needed for remaining symbols */
  rollovers = symbols / MAC_A_UNIT_BACKOFF_PERIOD;

  /* calculate symbols that still need counted after last rollover */
  symbols = symbols - (rollovers * MAC_A_UNIT_BACKOFF_PERIOD);

  /* add one to rollovers to account for symbols remaining in the current backoff period */
  rollovers++;

  /* set up parameters for CSP program */
  CSPZ = CSPZ_CODE_TX_ACK_TIME_OUT;
  CSPX = rollovers;
  CSP_WEVENT_SET_TRIGGER_SYMBOLS(symbols);

  /* clear the currently loaded CSP, this generates a stop interrupt which must be cleared */
  CSP_STOP_AND_CLEAR_PROGRAM();
  MAC_MCU_CSP_STOP_CLEAR_INTERRUPT();

  /*--------------------------
   * load CSP program
   */
  RFST = WAITX;
  RFST = WEVENT1;
  RFST = SSTOP;

  /*--------------------------
   */

  /* run CSP program */
  MAC_MCU_CSP_STOP_ENABLE_INTERRUPT();
  CSP_START_PROGRAM();

  /*
   *  For bullet proof operation, must account for the boundary condition
   *  where a rollover occurs after count was read but before CSP program
   *  was started.
   *
   *  If current symbol count is less that the symbol count recorded at the
   *  start of this function, a rollover has occurred.
   */
  if (CSP_WEVENT_READ_COUNT_SYMBOLS() < startSymbol)
  {
    /* a rollover has occurred, make sure it was accounted for */
    if (CSPX == rollovers)
    {
      /*
       *  Rollover event missed, manually decrement CSPX to adjust.
       *
       *  Note : there is a very small chance that CSPX does not
       *  get decremented.  This would occur if CSPX were written
       *  at exactly the same time a timer overflow is occurring (which
       *  causes the CSP instruction WAITX to decrement CSPX).  This
       *  would be extremely rare, but if it does happen, the only
       *  consequence is that the ACK timeout period is extended
       *  by one backoff.
       */
      CSPX--;
    }
  }
}
#endif

/**************************************************************************************************
 * @fn          macCspTxCancelAckTimeoutCallback
 *
 * @brief       Cancels previous request for ACK timeout callback.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void macCspTxCancelAckTimeoutCallback(void)
{
  MAC_MCU_CSP_STOP_DISABLE_INTERRUPT();
  CSP_STOP_AND_CLEAR_PROGRAM();
}


/**************************************************************************************************
 * @fn          macCspTxIntIsr
 *
 * @brief       Interrupt service routine for handling INT type interrupts from CSP.
 *              This interrupt happens when the CSP instruction INT is executed.  It occurs
 *              once the SFD signal goes high indicating that transmit has successfully
 *              started.  The timer value has been captured at this point and timestamp
 *              can be stored.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void macCspTxIntIsr(void)
{
  MAC_MCU_CSP_INT_DISABLE_INTERRUPT();

  /* execute callback function that records transmit timestamp */
  macTxTimestampCallback();
}


/**************************************************************************************************
 * @fn          macCspTxStopIsr
 *
 * @brief       Interrupt service routine for handling STOP type interrupts from CSP.
 *              This interrupt occurs when the CSP program stops by 1) reaching the end of the
 *              program, 2) executing SSTOP within the program, 3) executing immediate
 *              instruction ISSTOP.
 *
 *              The value of CSPZ indicates if interrupt is being used for ACK timeout or
 *              is the end of a transmit.
 *
 * @param       none
 *
 * @return      none
 **************************************************************************************************
 */
void macCspTxStopIsr(void)
{
  MAC_MCU_CSP_STOP_DISABLE_INTERRUPT();
  
  if (CSPZ == CSPZ_CODE_TX_DONE)
  {
    macTxDoneCallback();
  }
  else if (CSPZ == CSPZ_CODE_CHANNEL_BUSY)
  {
    macTxChannelBusyCallback();
  }
  else
  {
    MAC_ASSERT(CSPZ == CSPZ_CODE_TX_ACK_TIME_OUT); /* unexpected CSPZ value */
    macTxAckNotReceivedCallback();
  }
}


/**************************************************************************************************
 *                                  Compile Time Integrity Checks
 **************************************************************************************************
 */

#if ((CSPZ_CODE_TX_DONE != 0) || (CSPZ_CODE_CHANNEL_BUSY != 1))
#error "ERROR!  The CSPZ return values are very specific and tied into the actual CSP program."
#endif

#if (MAC_TX_TYPE_SLOTTED_CSMA != 0)
#error "WARNING!  This define value changed.  It was selected for optimum performance."
#endif

#if (T2THD_TICKS_PER_SYMBOL == 0)
#error "ERROR!  Timer compare will not work on high byte.  Clock speed is probably too slow."
#endif

#define BACKOFFS_PER_BASE_SUPERFRAME  (MAC_A_BASE_SLOT_DURATION * MAC_A_NUM_SUPERFRAME_SLOTS)
#if (((BACKOFFS_PER_BASE_SUPERFRAME - 1) & SLOTTED_TX_BACKOFF_COUNT_ALIGN_BIT_MASK) != SLOTTED_TX_BACKOFF_COUNT_ALIGN_BIT_MASK)
#error "ERROR!  The specified bit mask for backoff alignment of slotted transmit does not rollover 'cleanly'."
/*
 *  In other words, the backoff count for the number of superframe rolls over before the
 *  specified number of bits rollover.  For example, if backoff count for a superframe
 *  rolls over at 48, the binary number immediately before a rollover is 00101111.
 *  In this case four bits would work as an alignment mask.  Five would not work though as
 *  the lower five bits would go from 01111 to 00000 (instead of the value 10000 which
 *  would be expected) because it a new superframe is starting.
 */
#endif
#if (SLOTTED_TX_MAX_BACKOFF_COUNTDOWN_NUM_BITS < 2)
#error "ERROR!  Not enough backoff countdown bits to be practical."
#endif


/**************************************************************************************************
*/
