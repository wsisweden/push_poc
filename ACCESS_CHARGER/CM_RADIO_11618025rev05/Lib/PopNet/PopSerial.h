#ifndef POPSERIAL_H
#define POPSERIAL_H

#include "PopBsp.h"
#include "PopNet.h"
#include <board_config.h>

/************* Serial Interface ****************/

#define gSTX_c            0x02
#define gPopNetOpGroup_c  0x50

/***************************************************************************
  SERIAL INTERFACE
***************************************************************************/

#if gPopSerial_d

/***************************************************************************
  Types & Definitions
***************************************************************************/

#if gPopNeedPack_c
#pragma pack(1)
#endif
// header for gateway serial packet (both directions)
typedef struct sPopGatewayPacketHdr_tag
{
  uint8_t       iStx;       // STX (0x02) character
  uint8_t       iGroup;     // PopNet Group (0x50)
  popEvtId_t    iEvtId;     // event ID
  // length of payload
#if gPopMicroPowerLpcUpgrade_d == 1
  uint8_t       iLen;
#else
  uint16_t      iLen;
#endif
} sPopGatewayPacketHdr_t;
#if gPopNeedPack_c
#pragma pack()
#endif

/* the start of each gateway serial packet from PC to target and from target to PC */
#define gPopStx_c 0x02

// the XOR checksum at the end of the gateway packet
typedef uint8_t popChecksum_t;

#if gPopNeedPack_c
#pragma pack(1)
#endif
// a gateway packet includes an optional
typedef struct sPopGatewayPacket_tag
{
  sPopGatewayPacketHdr_t  header;
  uint8_t                  payload[1]; // variable sized payload
  // popChecksum_t                 CheckSum;
} sPopGatewayPacket_t;

/*New events that are need it for test tool*/

// Timers
#define gPopSerMinGapTimer_c                       0x0011     /* minimum inter-byte gap event */

#define POPSER_RX_BUFFER_WATERMARK 4

/* helpers for manipulating interrupts */
/* the following for example only; race condition caution. */
#define PopSerialDisableTxInts() 
#define PopSerialEnableTxInts() 
#define PopSerialDisableRxInts()
#define PopSerialEnableRxInts()
#define PopSerialDisableTxRxInts()
#define PopSerialEnableTxRxInts() 
#define PopSerialSaveAndDisableTxRxInts()
#define PopSerialRestoreTxRxInts()
#define PopSerialSaveAndDisableTxInts()
#define PopSerialRestoreTxInts()       

// internal events for serial module 
#define gPopSerByteReceivedEvt_c     0x0010     /* ISR received a byte */

// status flags 
#define gPopSerRxData_c         0x01
#define gPopSerRxOverflow_c     0x02
#define gPopSerTxDone_c         0x04
#define gPopSerTxOverflow_c     0x08
#define gPopSerTooBig_c         0x10  //application buffer size exceeds maximum tx buffer size.
#define gPopSerBusy_c            0x12  //no room in xmit buffer for data.
#define gPopSerInvalidArg_c     0x14  //check parameters

// set baud rate divisor based on 16MHz bus rate (PopNet runs at 16MHz)
#if gPopSerialBaudRate_c == 1200
#define POPSER_DIVISOR Baudrate_1200
#elif gPopSerialBaudRate_c == 2400
#define POPSER_DIVISOR Baudrate_2400
#elif gPopSerialBaudRate_c == 4800
#define POPSER_DIVISOR Baudrate_4800
#elif gPopSerialBaudRate_c == 19200
#define POPSER_DIVISOR Baudrate_19200
#elif gPopSerialBaudRate_c == 57600
#define POPSER_DIVISOR Baudrate_57600
#elif gPopSerialBaudRate_c == 115
#define POPSER_DIVISOR Baudrate_115200
#else
#define POPSER_DIVISOR Baudrate_38400
#endif

extern gPopBaudRate_t  giPopSerialBaudRate;       /* baud rate, can be set by app */

extern UartErr_t UartWriteData( uint8_t UartNumber, uint8_t* pBuf, uint16_t NumberBytes);

void PopSerialTxIsr(UartWriteCallbackArgs_t* args);
void PopSerialRxIsr(UartReadCallbackArgs_t* pArgs);
void PopSerialRestartModule(void);
void PopConvertEventFromNativeToGatewayEndian(uint8_t iEvtId, void *pBuffer);

extern uint8_t  gaPopSerialTxBuf[]; /* ISR tx buf */
extern uint8_t  *gpPopSerialTxBufHead;    /* buffer head */
extern uint8_t  *gpPopSerialTxBufTail;    /* buffer tail */
extern uint8_t   gaPopSerialRxBuf[]; /* ISR rx buf */
extern uint8_t  *gpPopSerialRxBufHead;    /* buffer head */
extern uint8_t  *gpPopSerialRxBufTail;    /* buffer tail */
extern uint16_t  giPopSerialMinGapMs; /* mingap timeout, in ms. can be set by app */
extern volatile uint8_t gfPopSerialStatus;     /* current status of xmit and recv */
extern UartReadStatus_t gu8SCIStatus;
extern volatile uint8_t gu8SCIDataFlag;
extern uint16_t gu16SCINumOfBytes;
#endif

#endif
