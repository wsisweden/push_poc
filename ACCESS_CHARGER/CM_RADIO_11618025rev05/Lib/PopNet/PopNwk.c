/***************************************************************************
  PopNwk.c
  Copyright (c) 2006-2010 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  PopNet Internal: contains functions not to be changed by applications.

  This module defines the networking functions of PopNet, including
  broadcasting, unicasting, route discovery and other network functions.
  Includes also forming and joining networks.

****************************************************************************/
#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"
#include "PopNwk.h"
#include <stddef.h>

/********************************************************
           Local Macros
*********************************************************/

// Security limit for the staleness, one for lite and one for AES security.
#if gPopBigEndian_c
#define gPopLiteSecurityMask_c 0x0000FFFF
#define gPopNwkLiteLimit_c  0x7fff
#define gPopNwkAESLimit_c   0x7fffffff
#else
#define gPopLiteSecurityMask_c 0xFFFF0000
#define gPopNwkLiteLimit_c  0xff7f
#define gPopNwkAESLimit_c   0xffffff7f
#endif


/********************************************************
                  Local Types
*********************************************************/
#if gPopNeedPack_c
#pragma pack(1)
#endif
// for association responses
typedef struct _tagPopNwkAssociationRsp_t
{
  uint16_t      iFrameControl;    // 0xcc63
  uint8_t       iSequence;        // 0x50 = 'P'
  popPanId_t    iPanId;           //
  aPopMacAddr_t aDstIeeeAddr;     // destination MAC address
  aPopMacAddr_t aSrcIeeeAddr;     // source MAC address
  uint8_t       iCmd;             // 0x02 - association response
  popNwkAddr_t  iNewAddr;         // our new address
  uint8_t       iStatus;          // success or failure
} sPopNwkAssociationRsp_t;
#if gPopNeedPack_c
#pragma pack()
#endif


/********************************************************
           Local Prototypes
*********************************************************/

void PopNwkTaskInit(void);
void PopNwkTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopNwkRouteFrame(sPopNwkDataIndication_t * pIndication);
void PopNwkCheckRetries(popTimeOut_t iTicksMs);
void PopNwkFreeRetryEntry(uint8_t i, popErr_t iErr);
bool PopNwkSendToHigherLayers(sPopNwkDataIndication_t *pIndication);
void PopNwkSendPerHopAck(sPopNwkDataIndication_t *pIndication);
void PopNwkCheckDuplicates(popTimeOut_t iDiffTime);
popErr_t PopNwkAddToDuplicateTable(sPopNwkDataIndication_t *pIndication);
uint16_t PopNwkGetWaitTime(bool fBroadcast);
bool PopNwkConvertIncomingOtaFrame(sPopNwkDataIndication_t *pOtaFrame);
uint8_t PopNwkFindRetryEntry(popSequence_t iSequence, popNwkAddr_t iNwkAddr, uint8_t *piFree);
void PopNwkConvertOutgoingOtaFrame(sPopNwkDataIndication_t *pOtaFrame);
void PopNwkFreeEntireRouteDiscoveryTable(void);
void PopNwkCreateBeaconIndication( sPopNwkDataIndication_t *pIndication);
bool PopNwkProcessIfIsABeaconFrame(sPopNwkDataIndication_t *pIndication);
bool PopNwkProcessIfIsAnAckFrame(sPopDataFrame_t* pOtaDataFrame, uint8_t iFrameLen);
bool PopNwkIsBeaconDuplicate(sPopNwkBeaconIndication_t *pBeaconIndication);
sPopNwkDataIndication_t *PopNwkMakeCopy(sPopNwkDataIndication_t *pIndication);
bool PopNwkProcessAssociationFrame(sPopDataFrame_t *pOtaFrame);
void PopNwkReceive(void);
bool PopNwkAddMsgToQueue(sPopNwkDataIndication_t *pOtaFrame);
sPopNwkDataIndication_t *PopNwkRemoveMsgFromQueue(void);
popPanId_t PopNwkGetNoConflictRandomPanId(void);
popChannel_t PopNwkFindBestChannel(void);
popEnergyLevel_t PopPhyScanForNoise(uint8_t iChannel);
popStatus_t PopNwkFindSuitableParentToJoin(uint8_t *piJoinIndex);
bool PopNwkIsOffNetwork(void);
bool PopNwkProcessMacPacket(sPopNwkDataIndication_t *pIndication);

popNwkLiteMic_t PopNwkApplyLiteSecurity(uint8_t iLength, sNwkDataFrame_t *pNwkFrame);
popNwkAESMic_t PopNwkApplyAESSecurity(uint8_t iLength, sNwkDataFrame_t *pNwkFrame);
void PopNwkLiteCypherKey(aPopNwkKey_t aKey, popNwkLiteMic_t  *pMic, popNwkLiteCounter_t iCounter);
void PopNwkGenerateMIC(uint8_t *pPayload, uint8_t iLength, popNwkLiteMic_t *pMic);
popNwkAESMic_t PopNwkGenerateAESMIC(uint8_t *pPayload, uint8_t iLength, uint8_t *pKey);
uint8_t PopNwkSearchNeighbor(popNwkAddr_t iNwkAddr);
bool PopNwkAddNewNeighbor(popNwkAddr_t iNwkAddr, popLargeCounter_t iCounter);
bool PopNwkUpdateNeighbor(uint8_t  iIndex, popLargeCounter_t iCounter);
bool PopNwkReplaceOldNeighbor(popNwkAddr_t iNwkAddr, popLargeCounter_t iCounter);


/********************************************************
           Global Variables And Externs
*********************************************************/
extern bool gfPopReportedOutOfMemory;

// Network sequence # for outgoing packets. Set to random sequence # on init.
popSequence_t giPopNwkSequence;

// the # of entries in the OTA MSG queue
extern const uint8_t cPopNwkOtaMsgQueueEntries;
extern sPopNwkDataIndication_t *maPopNwkMsgQueue[];


// for use by PopNwkStartNetworkStateMachine(). These must all be set up before calling state machine
popNwkState_t       giPopNwkState;        // what initial state to do next?
popNwkStateCause_t  giPopNwkStateCause;   // who started this (Start/Form/Join)?
popNwkAddr_t        giPopNwkStateParent;  // parent to join
popStatus_t               giPopNwkStateJoinConfirm;
popNwkStateCause_t  giPopNwkPreviousStateCause = 0xFF;


// more globals for use by PopNwkStartNetworkStateMachine()
uint8_t             giPopNwkAssociateTriesLeft;
uint8_t             giPopNwkStateJoinTriesLeft;   // how many tries are left on join?
popStatus_t         giPopNwkStateStatus;          // what status to report to user?
uint8_t             giPopNwkStateEntry;
sPopNwkScanForNoise_t gsPopNwkScanForNoise;       // results of a scan for noise

// incoming OTA message queue
// this allows us to go back into recieve mode immediately and process them at our leisure
sPopNwkDataIndication_t *gpPopNwkIncomingMsg;   // an interrupt fills in this incoming MSG (e.g. SMAC)
uint8_t miPopNwkMsgQueueHead;   // head of message queue (where to add)
uint8_t miPopNwkMsgQueueTail;   // tail of message queue (where to remove)
uint8_t miPopNwkMsgQueueCount;  // # of messages in message queue

// for creating the outgoing beacon from other global information
sPopNwkBeacon_t gPopNetBeacon;

//
// security fields
//

// The internal prime number ot be use on the secure module
const popLitePrime_t miLitePrime = 0x97; // 0x97 = 151 decimal.
const popAESPrime_t  miAESPrime = 0x7fffffff;
uPopSecureCounters_t  guPopNwkSecureCounters = {0};   // counters used for light security

// The size of the Auxiliar Frame to be use in security.
// The index control for the local Neighbor table.
uint8_t miNeighborEntry = 0;
popLargeCounter_t miIncomingCounter=0;

// for code instrumentation
uint8_t giPopNwkRetryEntriesWaterMark;
uint8_t giPopNwkRetryEntriesCount;

/* Allocate a frame  large enough for an ACK. Fill with mostly junk so it's clear if code didn't fill it in properly. */
sPopDataFrame_t gAckPacket =
{
  {
    gMacFrameControlAck_c, //popMacFrameControl_t iFrameControl;
    0,      //popNwkPathCost_t  iPathCost;
    0xBEBE, //popPanId_t iPanId;
    0xBEBE, //popNwkAddr_t iDstAddr;
    0xBEBE, //popNwkAddr_t iSrcAddr;
  },//sMac
  {
    0xBEBE, //popNwkFrameControl_t iFrameControl;
    0xBEBE, //popNwkAddr_t      iDstAddr;
    0xBEBE, //popNwkAddr_t      iSrcAddr;
    1,      //popNwkRadius_t    iRadius;
    0,     //popSequence_t     iSequence;
  },//sNwk
  {
	  gPopNwkMgmtAck_c
  } //uint8_t aPayload[1];
};

/******** scanning module state machine ***********/
sPopNwkScanForNetworks_t *gpPopNwkDiscoveryTable;   // if this is set, we're recording beacons, if NULL, we're not
popChannelList_t mPopNwkScanChannelList;            // remember channel list to scan
popScanOption_t mPopNwkScanOptions;                 // remember options to scan
popTimeOut_t    mPopNwkScanTimeout;                 // remember timeout to scan
popChannel_t    mPopNwkScanChannelToScan;           // which channel are we scanning? (11-26)
bool          gfPopNwkIsProcessingBeaconEnabled;  // allow beacon indications. Used also by node placement even when off the network

// for association requests
sPopNwkAssociationReq_t mPopNwkAssociationReq =
{
  gMacAssociationReqFrameControl_c, // 0xc823
  0x50,     // 0x50 = 'P'
  0xffff,   // pan ID, little endian
  0x0000,   // desintation address, little endian
  0xffff,   // 0xffff
  { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 },   // source MAC address
  0x01,     // 0x01 - association request
  0x8e      // 0x8e - router
};

// for association responses
sPopNwkAssociationRsp_t mPopNwkAssociationRsp =
{
  gMacAssociationRspFrameControl_c, // 0xcc63
  0x50,         // 0x50 = 'P'
  0xffff,       // PAN ID, little endian
  { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 },   // destination MAC address
  { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 },   // source MAC address
  0x02,         // 0x02 - association response
  0xffff,       // our new address
  0x00          // success or failure
};

// version of PopNet (not changeable by user)
const gPopVersion_t giPopNetVersion = gPopNetVersion_c;   // e.g. 0x0200 = 2.0


// the number of PHY packets received (for this node or not) since last calling PopPhyPacketCount();
uint32_t giPopPhyPacketCount;


/***************************************************************************
  Code
***************************************************************************/

/*
  PopNwkTaskInit

  (For Internal Nwk Layer Use Only)

  Initialize the network task. This task performs all routing and data requests.
*/
void PopNwkTaskInit(void)
{
  // start with a random sequence number
  giPopNwkSequence = PopRandom08();

  // Read Nwk data from NVM
  PopNvRetrieveNwkData();

  // set the initial security key. May be changed at run-time.
  PopNwkSetSecurityKey(PopNwkGetSecurityKeyPtr());

  // resolve random MAC address if needed
  PopNwkSetMacAddr(PopNwkGetMacAddrPtr());

  // set up the I-Can-Hear-You table
  PopNwkClearICanHearYou();

  // allow the radio to be used to receive packets
  // TODO: don't go into receive mode until user says to
  PopNwkReceive();
}

/*
  PopNwkTaskEventLoop

  (For Internal Nwk Layer Use Only)

  Note: Rather than sending a DATA_INDICATION when receiving an over-the-air
  packet, this layer will send NWK_MGMT commands through as events.

*/
void PopNwkTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
  sPopNwkDataIndication_t *pNewReceiveBuffer;

  switch(iEvtId)
  {
    // received something from the PHY layer. Go back into recieve mode as soon as possible.
    // this can only happen if the radio receives something.
    case gPopEvtPhyIndication_c:

      // record how many raw packets we get, whether for us or not...
      ++giPopPhyPacketCount;

      if(!gfPopReportedOutOfMemory){
        /* get memory for new recieve buffer, so we can keep incoming buffer and still go back into receive mode */
        pNewReceiveBuffer = PopMemAlloc(giPopPhySize + sizeof(sPopNwkPreHeader_t));

        /* only process packet if we can allocate another receieve buffer, otherwise drop it */
        if(pNewReceiveBuffer)
        {
          /*
            if queue is full, thow out message.
          */
          if (PopNwkAddMsgToQueue(gpPopNwkIncomingMsg))
          {
            // Since the packet was added to the queue, we'll get a gPopEvtNwkGotPacket_c event to dequeue it
            gpPopNwkIncomingMsg = pNewReceiveBuffer;
          }

          // if the queue (or event table) is full and the node could alloc memory for the packet then free it.
          else
            PopMemFree((void*)pNewReceiveBuffer);
        }
      }

      // set radio back into receive mode
      PopNwkReceive();
    break;

    // we've recieved a packet (either from the radio or from the app)
    // either pass it up, pass it on, or throw it out
    case gPopEvtNwkGotPacket_c:

      // get packet from message queue
      sEvtData.pData = (void *)PopNwkRemoveMsgFromQueue();

      // if this was a MAC packet, no need to process it as a network packet
      if(!PopNwkProcessMacPacket(sEvtData.pData))
      {
        // wasn't a MAC packet, check if it's a network packet
        PopNwkRouteFrame(sEvtData.pData);
      }
      (void)PopMemFree(sEvtData.pData);
      break;

    // a network management command has come in. Process it.
    case gPopEvtDataIndication_c:
      PopNwkMgmtIndication(sEvtData.pDataIndication);
      break;

    // a network timer has fired
    case gPopEvtTimer_c:
      if(sEvtData.iTimerId == gPopNwkReceiveTimer_c)
        PopNwkReceive();
      else
        PopNwkMgmtTimerHandler(sEvtData.iTimerId);
      break;

    case gPopEvtPwrWakeUp_c:
    // allocate a buffer (if needed) go back into SMAC receive mode
    case gPopEvtNwkReceive_c:
      PopNwkReceive();
      break;

    // a node wishes to join this node. Originally sent from NWK layer to app.
    // Now it's back in the NWK layer via PopAppDefaultHandler().
    // Allow the node to join the normal way
    case gPopEvtNwkJoinIndication_c:
      PopNwkJoinIndication(sEvtData.pNwkJoinIndication);
    break;

    // this goes to the next network state for startup
    case gPopEvtNwkNextStartState_c:
      PopNwkStartNetworkStateMachine();
    break;
  default:
    break;
  }

}

/*
  Process MAC packets.

  Association request, MAC ACK, Beacons and Beacon Requests. At this point, the frame is in OTA format.

  Returns true if this was a MAC-level packet, false if not.
*/
bool PopNwkProcessMacPacket(sPopNwkDataIndication_t *pIndication)
{
  sPopDataFrame_t *pFrame;

  // get a pointer to the OTA frame
  pFrame = &pIndication->sFrame;

  // received an ACK from a node we unicast to (or through)
  if(PopNwkProcessIfIsAnAckFrame(pFrame, pIndication->sPre.iFrameLen))
    return true;

  // process incoming beacon requests and beacon frames
  if(PopNwkProcessIfIsABeaconFrame(pIndication))
    return true;

  // process incoming association frames
  if(PopNwkProcessAssociationFrame(pFrame))
    return true;

  // if the frame is too small for a PopNet data frame or MAC framecontrol is wrong for data, throw it out
  if((pIndication->sPre.iFrameLen < (sizeof(sPopDataFrame_t) - 1)) || pFrame->sMac.iFrameControl != gMacOtaFrameControlData)
    return true;

  return false;
}

/*
  PopNwkRouteFrame

  (For Internal Nwk Layer Use Only)

  A packet has come in over the radio or was sent from a higher layer. Route it.

  By the time it reaches here, all MAC-only frames have been filtered (beacon requests,
  beacons, association requests, MAC ACK, etc...)

  RREQ
  RREP
  Data Request (both broadcast and unicast)
  Data Indication (both broadcast and unicast)

  Upon completion of this function, the application or NWK layer may receive various
  indications including gPopEvtDataIndication_c, and gPopEvtDataPassThru_c.

  Frames include the following fields:
                MacSrc  MacDst  NwkSrc  NwkDst  PAN Id
  Unicast       0-n     0-n     0-n     0-n       pan
  Unibroadcast  0-n     0xffff  0-n     0-n       pan
  Broadcast     0-n     0xffff  0-n     0xffffx   pan

  Returns to free the packet (throw out). Increments allocation count to keep the memory.
*/
void PopNwkRouteFrame(sPopNwkDataIndication_t *pIndication)
{
  sPopNwkDataIndication_t *pCopy;
  popNwkAddr_t iNextHop;
  popNwkFrameControl_t iNwkFrameControl;
  popNwkRetryFlags_t iRetryFlags = { 0 };
  bool fFromThisNode;         // did this node initiate the packet?
  bool fMacDstThisNode;       // is MAC dest for this node?
  bool fUnicastForThisNode;   // is NwkDst = our node
  bool fIsMacBroadcast;       // is MAC dest broadcast?
  bool fForHigherLayers;      // is packet for higher layers? (NwkDst = our node or broadcast)
  bool fAddToRetryTable;      // add this packet to the retry table?

  // if not on network, throw out packet
  if(!PopNwkIsOnNetwork())
    return;

  // note: packet size and MAC frame control was already checked in the MAC frame detection. See PopNwkProcessMacPacket().

  // is the packet originating on this node?
  fFromThisNode = (pIndication->sPre.iLqi == gPopNwkLqiFromThisNode_c) ? true : false;

  // convert endian from OTA to native and decode security
  // if security conversion fails, throw out packet
  if(!fFromThisNode)
  {
    if(!PopNwkConvertIncomingOtaFrame(pIndication))
      return;
  }

  // if frame not for our PAN, throw it out (only MAC commands support broadcast pan ID 0xffff)
  if(pIndication->sFrame.sMac.iPanId != PopNwkGetPanId())
    return;

  // can we hear this address? if not, throw it out
  if(!PopNwkCanIHearYou(pIndication->sFrame.sMac.iSrcAddr))
    return;

  // determine if this packet is for this node or another node
  fUnicastForThisNode = pIndication->sFrame.sNwk.iDstAddr == PopNwkGetNodeAddr();
  fMacDstThisNode = (pIndication->sFrame.sMac.iDstAddr == PopNwkGetNodeAddr()) ? true : false;
  fIsMacBroadcast = (pIndication->sFrame.sMac.iDstAddr == gPopNwkBroadcastAddr_c) ? true : false;

  // only pay attention to packets for us or from us or broadcast to us
  if(pIndication->sFrame.sMac.iDstAddr != PopNwkGetNodeAddr() && !fFromThisNode && !fIsMacBroadcast)
    return;

  // throw out packet if any reserved bits used in NWK frame
  iNwkFrameControl = pIndication->sFrame.sNwk.iFrameControl;
  if(!PopNwkIsValidDataFc(iNwkFrameControl))
    return;

  // if no radius, invalid packet
  if(!pIndication->sFrame.sNwk.iRadius)
    return;

  // determine to which tables this packet needs to be added
  fForHigherLayers = ( fUnicastForThisNode || (!fFromThisNode && pIndication->sFrame.sNwk.iDstAddr == gPopNwkBroadcastAddr_c)) ? true : false;
  fAddToRetryTable     = (!fUnicastForThisNode || fIsMacBroadcast) ? true : false;

  // some things only need to happen if this is an incoming OTA packet
  // (as opposed to one initiated by this node)
  if(!fFromThisNode)
  {
    // if frame (at MAC frame) not for us, throw it out
    if(!fIsMacBroadcast && !fMacDstThisNode)
      return;

    // If security is enabled then check the neighbor table to see if the current packet
    // is a new one or is a stale one. This prevents replay attacks.
    if(!PopNwkStalenessCheck(pIndication))
      return;

    // if needed, send the MAC ACK back
    if(fMacDstThisNode)
      PopNwkSendPerHopAck(pIndication);

    // deals with the false route problem (where a receiving node has a route through a node that no longer does)
    // see test case 2.9.3. Forgotten Route
    if( pIndication->sFrame.sNwk.iFrameControl & gPopNwkFcRouteForced_c )
      PopNwkDeleteRoute(pIndication->sFrame.sNwk.iSrcAddr);

    // Got a route reply (RREP) token. Process and throw away the packet. No need to add to duplicate or retry table.
    if((iNwkFrameControl & gPopNwkFcMgmt_c) && (pIndication->sFrame.aPayload[0] == gPopNwkMgmtRouteReply_c))
    {
      PopNwkProcessRouteReplyFrame(pIndication);
      return;
    }

    // decrement radius in case we're propogating the packet (modify frame), but only if we received this packet from the outside
    --pIndication->sFrame.sNwk.iRadius;
    if(!pIndication->sFrame.sNwk.iRadius)
      fAddToRetryTable = false;

    /*
      route request = unibroadcast with "gPopNwkFcRouteIfNeeded_c" bit set in NWK frame
      RREQs from this node are created in PopNwkCheckRetries() if failed to deliver the
      unicast. otherwise, process all route requests received to find best path(s)
    */
    if(iNwkFrameControl & gPopNwkFcRouteIfNeeded_c)
    {
      (void)PopNwkProcessRouteRequest(pIndication, NULL);

      // when receiving a route discovery, it will be sent to the higher layers through the retry table code...
      // see popNwkRetryFlags_t fWaitDataInd
      fForHigherLayers = false;
    }

  }

  // some things only happen if from this node....
  else
  {
    // get the user options retryflags field
    iRetryFlags = pIndication->sPre.iRetryFlags;
  }

  // don't participate in routing if we're a sleepy node
  if(!fFromThisNode && fIsMacBroadcast && PopNwkIsSleepyNode())
    fAddToRetryTable = false;

  // if user wants duplicates to come through, that's OK otherwise add it to the duplicate table
  // only add to duplicate tables the broadcast messages or the incoming unicast
  if( !(pIndication->sFrame.sNwk.iFrameControl & gPopNwkFcDuplicatesOk_c) && (fIsMacBroadcast || !fFromThisNode ))
  {
    // can't add to duplicate table (or already there) then throw out the packet
    // as it's a duplicate (or we have no room)
    if(PopNwkAddToDuplicateTable(pIndication) != gPopErrNone_c)
      return;
  }

  // if unicasting through this node, determine next hop (modify frame)
  // we don't need to figure out next hop
  if(!fIsMacBroadcast && !fUnicastForThisNode && !fFromThisNode)
  {
    // get next hop in route (if none)
    iNextHop = PopNwkNextHop(pIndication->sFrame.sNwk.iDstAddr);

    // no route, throw out packet
    if(iNextHop == gPopNwkInvalidAddr_c)
      return;

    // modify the packet and send it on to next hop in the chain...
    pIndication->sFrame.sMac.iDstAddr = iNextHop;
    pIndication->sFrame.sMac.iSrcAddr = PopNwkGetNodeAddr();

    // inform application data is passing through this node (indicate destination address)
    (void)PopSetAppEvent(gPopEvtDataPassThru_c, (void *)&(pIndication->sFrame.sNwk.iDstAddr));
  }

  // send it to the higher layers
  if(fForHigherLayers)
  {
    (void)PopNwkSendToHigherLayers(pIndication);
  }

  // we'll be sending it on to one or more other nodes, add to retry table
  if(fAddToRetryTable)
  {
    // create a copy of the packet for the retry table (prior to convertion to OTA frame format)
    // since the higher layers need an unsecured and native endian packet
    pCopy = pIndication;
    if( fForHigherLayers )
      pCopy = PopNwkMakeCopy(pIndication);

    if(pCopy)
    {
      // set up the flags field for retrying
      iRetryFlags.fIsValid = true;
      iRetryFlags.fIsBroadcast = fIsMacBroadcast;

      // add to the retry table
      // this will also process the route request
      if(!PopNwkAddRetryEntry(pCopy, iRetryFlags))
      {
        // failed to add to retry table, free the copy (if we made one)
        if(pCopy != pIndication)
          (void)PopMemFree(pCopy);
      }
    }
  }

}

/*
  PopNwkAddMsgToQueue

  Add this incoming message from the PHY layer to the NWK OTA message queue.

  Idea: rather than setting an event for each incoming packet, just have the queue set
  the event at each idle time (when doing timer checks, etc...)

  Returns true if added. Returns false if not.
*/
bool PopNwkAddMsgToQueue(sPopNwkDataIndication_t *pOtaFrame)
{
  // if there is room in the queue and we could send the event we got the packet, then it's added
  if((miPopNwkMsgQueueCount < cPopNwkOtaMsgQueueEntries) && (PopSetEventEx(cNwkTaskId, gPopEvtNwkGotPacket_c, NULL) == gPopErrNone_c))
  {
    // add message to queue (ring buffer style)
    maPopNwkMsgQueue[miPopNwkMsgQueueHead++] = pOtaFrame;
    if(miPopNwkMsgQueueHead >= cPopNwkOtaMsgQueueEntries)
      miPopNwkMsgQueueHead = 0;

    // indicate we;ve got one more item in the queue
    ++miPopNwkMsgQueueCount;

    // added to queue
    return true;
  }

  // not added to queue
  return false;
}

/*
  PopNwkRemoveMsgFromQueue

  Pull a message out of the queue from the tail. Returns NULL if no messages in
  the queue.
*/
sPopNwkDataIndication_t *PopNwkRemoveMsgFromQueue(void)
{
  sPopNwkDataIndication_t *pMsg = NULL;

  if(miPopNwkMsgQueueCount)
  {
    pMsg = maPopNwkMsgQueue[miPopNwkMsgQueueTail++];
    if(miPopNwkMsgQueueTail >= cPopNwkOtaMsgQueueEntries)
      miPopNwkMsgQueueTail = 0;

    // indicate we;ve got one more item in the queue
    --miPopNwkMsgQueueCount;
  }

  return pMsg;
}

/*
  Free the entire NWK message queue
*/
void PopNwkFreeMsgQueue(void)
{
  sPopNwkDataIndication_t *pMsg;

  // free all messages in the queue
  do
  {
    pMsg = PopNwkRemoveMsgFromQueue();
    if(pMsg)
      PopMemFree(pMsg);
  } while(pMsg);

  // start out everything from 0 again
  miPopNwkMsgQueueHead = miPopNwkMsgQueueTail = miPopNwkMsgQueueCount = 0;
}

/*
  PopNwkSetMacAddr

  Set the current MAC address. If the parameter is all 0xffs, will set it to a random
  value, beginning with "Pop".
*/
void PopNwkSetMacAddr(aPopMacAddr_t aMacAddr)
{
  uint8_t i;
  bool invalidData;

  // check for random MAC address (all 0xff or 0x00)
  invalidData = PopMemIsFilledWith(aMacAddr, 0xff, sizeof(aPopMacAddr_t));
  invalidData |= PopMemIsFilledWith(aMacAddr, 0x00, sizeof(aPopMacAddr_t));

  if(invalidData)
  {
    // create a random MAC address
    PopMemCpy(&PopNwkGetMacAddrPtr()[5], "Pop", 3);
    for(i=0; i<5; ++i)
      PopNwkGetMacAddrPtr()[i] = PopRandom08();

    PopNwkGetMacAddrPtr()[0] = 1;
    PopNwkGetMacAddrPtr()[1] = 0;

    // Store Nwk data to NVM
    PopNvStoreNwkData();
  }
  else
    PopMemCpy(gsPopNwkData.aMacAddr, aMacAddr, sizeof(aPopMacAddr_t));
}

/*
  PopNwkGetNetworkStatus

  Returns true if the current is on the network and false otherwise.
*/
bool PopNwkGetNetworkStatus(void)
{
  return (PopNwkIsValidUnicastAddress(PopNwkGetNodeAddr()))? true: false;
}

/*
  Turn sleepy mode on or off.
*/
void PopNwkSetRoutingSleepingMode(bool fOn)
{
  gsPopNwkData.fSleepyMode = fOn;
  // Inform the NVM engine that network data has changed
  PopNvmDataMarkAsChanged(gPopNvmNwkDataChanged_c);
/*
  #JJ do not disable join enable because not used for sleeping mode but for disabling
  routing and broadcasting
  // turn off joining for sleepy nodes
  if(fOn)
    PopNwkJoinEnable(false, false);
*/
}

/*
  PopNwkReceive

  Allocates memory for the receive buffer (if needed) and puts PHY into interrupt driven
  receive mode.

  Note: giPopPhySize defines the largest packet that can be received over-the-air. See
  also gPopPhySize_c.
*/
void PopNwkReceive(void)
{
  // if not already allocated, allocate
  if(!gpPopNwkIncomingMsg)
    gpPopNwkIncomingMsg = PopMemAlloc(giPopPhySize + sizeof(sPopNwkPreHeader_t));

  // if not enough memory, try again later (after 3ms)
  if(!gpPopNwkIncomingMsg)
    (void)PopStartTimerEx(cNwkTaskId, gPopNwkReceiveTimer_c, 3);

  // got the memory, tell SMAC data indications may now come in...
  else
    PopPhyReceive(gpPopNwkIncomingMsg);
}

/*
  PopNwkClearICanHearYou

  Clear the I-Can-Hear-You table
*/
void PopNwkClearICanHearYou(void)
{
  uint8_t i;

  for(i=0; i<giPopNwkICanHearYouEntries; ++i)
    gaPopNwkICanHearYouTable[i] = gPopNwkInvalidAddr_c;
}

/*
  PopNwkSetICanHearYou

  Set the I-Can-Hear-You Table
*/
void PopNwkSetICanHearYou(uint8_t iNum, popNwkAddr_t *pArray)
{
  // clear out the table
  PopNwkClearICanHearYou();

  // don't allow too long of an array
  if(iNum > PopNwkGetICanHearYouEntries())
    iNum = PopNwkGetICanHearYouEntries();
  if(!iNum)
    return;

  // add these to the table
  PopMemCpy(gaPopNwkICanHearYouTable, pArray, iNum * sizeof(popNwkAddr_t));
}

/*
  PopNwkCanIHearYou

  Can I hear this node? Used to build table-top networks that can route.
  Uses the MAC source address (the node that originated the message).

  Returns true if yes. Returns false if no.
*/
bool PopNwkCanIHearYou(popNwkAddr_t iMacSrcAddr)
{
  uint8_t i;

  // is the feature disabled or ourselves? then we heard the node.
  if(!PopNwkGetICanHearYouEntries() || gaPopNwkICanHearYouTable[0] == gPopNwkInvalidAddr_c || iMacSrcAddr == PopNwkGetNodeAddr())
    return true;

  // is the node in the table? If not, we can't hear it.
  for(i=0; i<giPopNwkICanHearYouEntries; ++i)
  {
    if(iMacSrcAddr == gaPopNwkICanHearYouTable[i])
      return true;
  }
  return false;
}

/*
  PopNwkMakeCopy

  Returns a copy of a data indication. Returns NULL if failed.
*/
sPopNwkDataIndication_t *PopNwkMakeCopy(sPopNwkDataIndication_t *pIndication)
{
  sPopNwkDataIndication_t *pCopy = NULL;
  uint8_t iLen;

  // return a copy of the packet
  iLen = pIndication->sPre.iFrameLen + sizeof(sPopNwkPreHeader_t) + giPopNwkAuxFrameSize;

  // Alloc memory for the copy
  pCopy = PopMemAlloc(iLen);

  // If there is no memory then do nothing and return null
  if(!pCopy)
   return NULL;

  // otherwise make the copy
  PopMemCpy(pCopy, pIndication, iLen);

  // And return the pointer to the copy
  return pCopy;
}

/*
  PopNwkConvertIncomingOtaFrame

  Converts from an over-the-air format to internal format, including removing security.
  Note: MAC frame is left in OTA endian always.

  Returns true if worked, false if failed.
*/
bool PopNwkConvertIncomingOtaFrame(sPopNwkDataIndication_t *pOtaFrame)
{
  // decrypt the packet
  if (!PopNwkRemoveSecurity(pOtaFrame))
    return false;

  // fix endian of frame (everything but MAC Framecontrol)
  OtaToNative16(pOtaFrame->sFrame.sMac.iPanId);
  OtaToNative16(pOtaFrame->sFrame.sMac.iDstAddr);
  OtaToNative16(pOtaFrame->sFrame.sMac.iSrcAddr);
  OtaToNative16(pOtaFrame->sFrame.sNwk.iFrameControl);
  OtaToNative16(pOtaFrame->sFrame.sNwk.iDstAddr);
  OtaToNative16(pOtaFrame->sFrame.sNwk.iSrcAddr);

  return true;
}

/*
  PopNwkConvertOutgoingOtaFrame

  Converts from an in-memory frame to an over-the-air format, adding security
  and handling endian-ness.

  Note: MAC frame is left in OTA endian always.
*/
void PopNwkConvertOutgoingOtaFrame(sPopNwkDataIndication_t *pOtaFrame)
{
  // convert endian (everything but MAC Framecontrol)
  NativeToOta16(pOtaFrame->sFrame.sMac.iPanId);
  NativeToOta16(pOtaFrame->sFrame.sMac.iDstAddr);
  NativeToOta16(pOtaFrame->sFrame.sMac.iSrcAddr);
  NativeToOta16(pOtaFrame->sFrame.sNwk.iFrameControl);
  NativeToOta16(pOtaFrame->sFrame.sNwk.iDstAddr);
  NativeToOta16(pOtaFrame->sFrame.sNwk.iSrcAddr);

  // Adjust the size for security.
  pOtaFrame->sPre.iFrameLen += giPopNwkAuxFrameSize;

  // apply security if requested.
  PopNwkSecureData(pOtaFrame);
}

/*
  Returns true if this packet is from this node.
*/
bool PopNwkIsFromThisNode(sPopNwkDataIndication_t *pIndication)
{
  return (pIndication->sFrame.sNwk.iSrcAddr == PopNwkGetNodeAddr());
}

/*
  Random 8-bit jitter with upper modulo limit
*/
uint8_t PopNwkJitter08(uint8_t iUpperLimit)
{
  return (uint8_t)(PopRandom08() % iUpperLimit);
}

/*
  Random 126-bit jitter with upper modulo limit
*/
uint16_t PopNwkJitter16(uint16_t iUpperLimit)
{
  return (PopRandom16() % iUpperLimit);
}

/*
  PopNwkGetWaitTime

  Return the wait time for a broadcast or unicast retry.
*/
uint16_t PopNwkGetWaitTime(bool fBroadcast)
{
  uint16_t iWaitTime;

  // broadcast, wait a longer time
  if(fBroadcast)
    iWaitTime = giPopNwkBroadcastTimeOut + PopNwkJitter16(giPopNwkBroadcastJitter); /* wait 30-150ms */

  // unicast, wait a short time
  else
    iWaitTime = giPopNwkUnicastTimeOut + PopNwkJitter16(giPopNwkUnicastJitter);    /* wait 8-50 ms */

  return iWaitTime;
}

/*
  PopNwkCheckRetries

  (For Internal Nwk Layer Use Only)

  Checks if any of the retries have expired for unicast or broadcast or acknowledgements.
  May initiate a new transmit on behalf of unicasts or broadcasts. May initiate a confirm
  to the app if requested.

  Parameters
  iTicksMs     the # of milliseconds that have passed since this was last called
*/
void PopNwkCheckRetries(popTimeOut_t iTicksMs)
{
  uint8_t  i;
  uint8_t  iRREQIndex;
  popErr_t iErr;
  sPopNwkRetryTable_t *pEntry;
  popNwkRetryFlags_t iFlags;
  popNwkRetryFlags2_t iFlags2;

  // check retry list for an entry that needs to be sent
  for(i = 0; i < cPopNwkRetryEntries; i++)
  {
    pEntry = &gaPopNwkRetryTable[i];

    // only check if the entry is valid
    if(!PopNwkIsValidRetryEntry(i))
      continue;

    // get local copy of flags (code size reduction)
    iFlags = pEntry->iFlags;
    iFlags2 = pEntry->iFlags2;

    // don't send a new packet (or data confirm) if waiting for a route reply
    // the timeout on the route request will eventually free this entry if no route reply occurs in a timely manner
    if(iFlags2.fWait4RREP)
      continue;

    // only check if jitter time has expired
    if((popTimeOut_t)pEntry->iTimeLeftMs > iTicksMs)
    {
      pEntry->iTimeLeftMs -= iTicksMs;
      continue;
    }

    // if this was a pending data indication, then send up the packet
    if(iFlags.fWaitDataInd)
    {
      (void)PopNwkSendToHigherLayers(pEntry->pOtaFrame);
      PopNwkFreeRetryEntry(i, gPopErrNone_c);
      continue;
    }

    //
    // AT this point, it's time to retry
    //

    // still need to retry?
    if(pEntry->iRetries)
    {
      if(iFlags2.fIsOtaFrameNative)
      {
        PopNwkConvertOutgoingOtaFrame(pEntry->pOtaFrame);
        pEntry->iFlags2.fIsOtaFrameNative = false;
      }

      // send data out the radio (pOtaFrame already secured and OTA endian)
      PopNwkRawRadioTransmit(pEntry->pOtaFrame);

      // Set up a new expiry time
      pEntry->iTimeLeftMs = PopNwkGetWaitTime(iFlags.fIsBroadcast);
      --pEntry->iRetries;
    }

    // done, no more retries
    else
    {
      // check if a confirm is needed, or if some other processing should happen
      iErr = gPopErrNone_c;
      if(iFlags.fConfirm)   // if we need a confirm, we must be the originating node...
      {
        // if unicasting or route discovery, indicate we couldn't deliver packet
        if(!iFlags.fIsBroadcast)
          iErr = gPopErrRouteNotFound_c;

        // only the originator shall generate a route request
        if (pEntry->iFlags.fRoute)
        {
          // if not in native form, convert to native
          if(!pEntry->iFlags2.fIsOtaFrameNative)
          {
            (void)PopNwkConvertIncomingOtaFrame(pEntry->pOtaFrame);
            pEntry->iFlags2.fIsOtaFrameNative = true;
          }

          // convert from unicast to a unibroadcast
          pEntry->iFlags.fIsBroadcast = true;
          pEntry->iFlags.fRoute = false;
          pEntry->iRetries = giPopNwkMaxRetries;
          pEntry->iTimeLeftMs = PopNwkGetWaitTime(true);

          // convert OTA frame to route discovery
          pEntry->pOtaFrame->sFrame.sMac.iDstAddr = 0xffff;
          pEntry->pOtaFrame->sFrame.sMac.iPathCost = 0;
          pEntry->pOtaFrame->sFrame.sNwk.iFrameControl |= gPopNwkFcRouteIfNeeded_c;
          pEntry->pOtaFrame->sFrame.sNwk.iFrameControl &= (~gPopNwkFcRouteForced_c);

          // inform the route request engine
          if(PopNwkProcessRouteRequest(pEntry->pOtaFrame, &iRREQIndex) == gPopErrNone_c)
          {
            // we startup up the route discovery process
            pEntry->iFlags2.fDidRouteDiscovery = true;
            pEntry->iFlags2.iRREQIndex = iRREQIndex;

            // tell to the proper task that a route request just started
            (void)PopSetEventTo(pEntry->iFlags.fIsItFromNwk, gPopEvtNwkInitiateRoute_c, NULL);
            continue;
          }

          // RouteDiscoveryEntry could not be added (too many route requests underway)
          // so indicate busy...
          else
          {
            // error code busy
            pEntry->iRetries = 0;
            iErr = gPopErrBusy_c;
          }
        }

      }

      // done with the retry entry. Send a confirm if needed, but free it in any case
      if(!pEntry->iRetries)
      {
        // if we've already attempted the route discovery, just wait until the route discovery is complete
        // only then free the entry and confirm.
        if(iFlags2.fDidRouteDiscovery && !iFlags.fRoute)
        {
          // indicate we're waiting for the route reply before sending the confirm
          // if the RREP times out, this entry will be freed by the route discovery logic. See PopNwkCheckRouteDiscovery()
          pEntry->iFlags2.fWait4RREP = true;

          // as we won't be repeating it anymore, we can free the memory for other stuff...
          (void)PopMemFree(pEntry->pOtaFrame);
          pEntry->pOtaFrame = NULL;
          continue;
        }

        // If this packet is a single broadcast message then free one time to decrement the alloc counter because this
        // packet was allocated 2 time, at least. One time when the copy was made and another when it was added to the
        // retry table.
        if( iFlags.fIsBroadcast && !iFlags.fRoute)
          (void)PopMemFree(pEntry->pOtaFrame);

        // free the entry and send the confirm
        PopNwkFreeRetryEntry(i, iErr);
      }
    }

  }
}

/*
  PopNwkSendAppDataConfirm

  Send data confirm to application.
*/
void PopNwkSendAppDataConfirm(popErr_t iErr)
{
  sPopEvtData_t sEvtData;
  sEvtData.iDataConfirm = iErr;
  (void)PopSetAppEvent(gPopEvtDataConfirm_c, sEvtData.pData);
}

/*
  PopNwkFreeRetryEntry

  (For Internal Nwk Layer Use Only)

  Delete unicast or broadcast from the retry list. Will also send
  gPopEvtDataConfirm_c to the application if this is the originating node.

*/
void PopNwkFreeRetryEntry(uint8_t i, popErr_t iErr)
{
  sPopEvtData_t sEvtData;
  sPopNwkRetryTable_t *pEntry;

  // validate the index range if is out then do nothing.
  if(i >= cPopNwkRetryEntries)
    return;

  // free only valid retry entries
  if (!PopNwkIsValidRetryEntry(i))
    return;

  // free the entry
  pEntry = &gaPopNwkRetryTable[i];

  // tell to the proper task that everything worked (or not)
  if(pEntry->iFlags.fConfirm)
  {
    sEvtData.iDataConfirm = iErr;
    (void)PopSetEventTo(pEntry->iFlags.fIsItFromNwk, gPopEvtDataConfirm_c, sEvtData.pData);
  }

  // free the entry
  if(pEntry->pOtaFrame)
    (void)PopMemFree(pEntry->pOtaFrame);
  PopMemSetZero(pEntry, sizeof(sPopNwkRetryTable_t));

  // keep the count update on each free, but avoid wrap
  // used for high-water mark checking
  if(giPopNwkRetryEntriesCount)
    giPopNwkRetryEntriesCount--;
}

/*
  PopNwkFreeEntireRetryTable

  This function frees all retry entries without data confirm to higher layers.
  Used when leaving the network.
*/
void PopNwkFreeEntireRetryTable( void )
{
  uint8_t i;

  // Free all entries, no data confirm
  for( i = 0; i < cPopNwkRetryEntries; i++)
  {
    gaPopNwkRetryTable[i].iFlags.fConfirm = false;
    PopNwkFreeRetryEntry(i, gPopErrNone_c);
  }
}

/*
  PopNwkFindRetryEntry

  Find the retry entry if it exists in the table. Don't

  Parameter   Dir   Description
  ---------   ----  ------------
  iSequence   in    nwk sequence # of entry
  iNwkAddr    in    nwk src addr of entry
  *piFree     in    if non-NULL, returns first free index

  Returns gPopEndOfIndex_c if not found, or the index number (0-n) if found.
  Returns the first free entry if piFree is not NULL (*piFree may be gPopEndOfIndex_c if no free entries).
*/
uint8_t PopNwkFindRetryEntry(popSequence_t iSequence, popNwkAddr_t iNwkAddr, uint8_t *piFree)
{
  uint8_t i;
  uint8_t iFree = gPopEndOfIndex_c;   // assume none free

  // look through table for this entry
  for (i=0; i < cPopNwkRetryEntries; i++)
  {
    // if this is the frame that's being ACKed, remove it
    if (PopNwkIsValidRetryEntry(i))
    {
      if (gaPopNwkRetryTable[i].iSequence == iSequence && gaPopNwkRetryTable[i].iSrcAddr == iNwkAddr)
      {
        break;
      }
    }

    // not a valid entry, must be free. Record first one.
    else if(iFree == gPopEndOfIndex_c)
      iFree = i;
  }

  // returns first free index if requested by caller
  if(piFree)
    *piFree = iFree;

  // entry not found
  if(i >= cPopNwkRetryEntries)
    i = gPopEndOfIndex_c;

  // return index (0-n) or gPopEndOfIndex_c
  return i;
}


/*
  PopNwkAddRetryEntry   (For Internal Nwk Layer Use Only)

  Add a packet to the table that retries the entry until it's delivered (or is retried
  too many times). It is assumed the pOTA

  Parameter   Dir   Description
  ---------   ---   -----------
  pOtaFrame   in    The over-the-air packet to repeat (must be unencrypted and in native endian)
  iFlags      in    indicates any special flags from the originating node
                    (lower 8-bits of the popNwkRetryFlags_t structure)

  The following things added to the table:
  1. Broadcasts (including unibroadcasts)
  2. Unicasts initiated from this node
  3. Unicasts passing through this node
  4. Unibroadcasts destined for this node with route discovery bit on

  Returns true if this entry is unique and was added.
  Returns false if this has already been heard or table is full (couldn't add).
*/
bool PopNwkAddRetryEntry(sPopNwkDataIndication_t *pOtaFrame, popNwkRetryFlags_t iFlags)
{
  uint8_t  i;               // various temporary indexes
  uint8_t  iFreeEntry;      // a slot for a new entry
  uint8_t  iRetries;        // # of retries
  popSequence_t iSequence;  // NwkSeq #
  popNwkAddr_t iSrcAddr;    // NwkAddr together with seq # uniquely identifies a packet
  sPopNwkRetryTable_t  *pEntry;
  popTimeOut_t  iTimeLeft;

  // look for this entry, starting with the current head of the list
  iSequence = pOtaFrame->sFrame.sNwk.iSequence;
  iSrcAddr  = pOtaFrame->sFrame.sNwk.iSrcAddr;
  i = PopNwkFindRetryEntry(iSequence, iSrcAddr, &iFreeEntry);

  // if entry already found, don't add again. If no free entries, then can't add it
  if((i != gPopEndOfIndex_c) || (iFreeEntry == gPopEndOfIndex_c))
    return false;

  // access the entry directly (code reduction)
  pEntry = &gaPopNwkRetryTable[iFreeEntry];

  // if the user sends another data request from this node to this same destination
  // have that packet wait in the retry table pending the RREP (or timeout).
  if(iFlags.fConfirm)
  {
    i = PopNwkFindPendingRouteDiscoveryEntry(pOtaFrame->sFrame.sNwk.iDstAddr);
    if( i != gPopEndOfIndex_c)
    {
      pEntry->iFlags2.fWait4RREP = true;
      pEntry->iFlags2.iRREQIndex = i;
    }
  }

  // indicate OTA frame is native
  pEntry->iFlags2.fIsOtaFrameNative = true;

  // determine # of retries (usually 3, but broadcasts may be truncated to only 1 if desired)
  iRetries = giPopNwkMaxRetries;
  if(iFlags.fIsBroadcast)
  {
    iRetries = giPopNwkBroadcastRetries;

    // update all broadcast frames to use our own SrcAddr
    pOtaFrame->sFrame.sMac.iSrcAddr = PopNwkGetNodeAddr();
  }
  if(iFlags.fNoRetry)
    iRetries = 1;

  // increment the allocation count on the memory
  (void)PopMemAllocAgain(pOtaFrame);

  // add the new entry
  pEntry->iFlags    = iFlags;
  pEntry->pOtaFrame = pOtaFrame;
  pEntry->iSequence = iSequence;
  pEntry->iSrcAddr  = iSrcAddr;
  pEntry->iRetries  = iRetries;

  // if from this node, don't wait on the first try (any data request from this node will have fConfirm set)
  if(iFlags.fConfirm)
    iTimeLeft = 0;
  else
    iTimeLeft = PopNwkGetWaitTime(iFlags.fIsBroadcast);
  pEntry->iTimeLeftMs = iTimeLeft;

  // if adding the RetryEntry AFTER we've added the RouteRequest, then update the MAC addr and path cost
  if(pOtaFrame->sFrame.sNwk.iFrameControl & gPopNwkFcRouteIfNeeded_c)
  {
    // is there a route request for this retry entry?
    i = PopNwkFindRouteReq(iSrcAddr, iSequence, NULL);
    if(i != gPopEndOfIndex_c)
    {
      // this was an incoming route request for this node. Don't send the data up to the app right away
      // instead, wait awhile
      if(pOtaFrame->sFrame.sNwk.iDstAddr == PopNwkGetNodeAddr())
      {
        pEntry->iFlags.fWaitDataInd = true;
        pEntry->iTimeLeftMs = giPopNwkWaitInd;
      }

      // update broadcast with new best path cost
      else
      {
        pOtaFrame->sFrame.sMac.iPathCost = gaPopNwkRouteDiscoveryTable[i].aLinkEntry[0].iPathCost;
      }
    }
  }

  // Always keep trac of the amount of retry entries.
  giPopNwkRetryEntriesCount++;

  // Register the high water mark.
  if (giPopNwkRetryEntriesCount > giPopNwkRetryEntriesWaterMark)
    giPopNwkRetryEntriesWaterMark = giPopNwkRetryEntriesCount;

  // Report the table full event if needed.
  if (giPopNwkRetryEntriesCount >= cPopNwkRetryEntries)
  {
    // Ignore the error status, if we fail to set the event, then, there is nothing to do.
    (void)PopSetTableFullEvent(gPopRetryTableFull_c);
  }

  // indicate it's been added
  return true;
}


/*
  PopNwkSendToHigherLayers

  Sends the frame to the next higher layer. This can be the application for data, or the
  networkwork layer for NWK management commands.

  Returns true if worked. false if couldn't (no events)
*/
bool PopNwkSendToHigherLayers(sPopNwkDataIndication_t *pIndication)
{
  popTaskId_t taskId = cAppTaskId;

  // network management
  if(pIndication->sFrame.sNwk.iFrameControl & gPopNwkFcMgmt_c)
    taskId = cNwkTaskId;

  // tell application about the data (unicast or broadcast)
  if(PopSetEventEx(taskId, gPopEvtDataIndication_c, pIndication) == gPopErrNone_c)
  {
    // allocate the pIndication again, so the APP (or NWK) layer can keep it
    (void)PopMemAllocAgain(pIndication);
    return true;
  }
  return false;
}

/*
  (NWK Internal Only)

  If unicast for this node, sends an ACK back to previous hop. Sent as early as possible
  (just after descrypting and checking the I-Can-Hear-You table).
*/
void PopNwkSendPerHopAck(sPopNwkDataIndication_t *pIndication)
{
  /* fill in ACK fields */
  gAckPacket.sMac.iPanId = pIndication->sFrame.sMac.iPanId;
  gAckPacket.sMac.iDstAddr = pIndication->sFrame.sMac.iSrcAddr;
  gAckPacket.sMac.iSrcAddr = pIndication->sFrame.sMac.iDstAddr;

  /* fill in NWK frame */
  gAckPacket.sNwk.iFrameControl = gPopNwkFrameControl_c | gPopNwkFcMgmt_c;
  gAckPacket.sNwk.iSequence = pIndication->sFrame.sNwk.iSequence;
  gAckPacket.sNwk.iDstAddr = pIndication->sFrame.sNwk.iDstAddr;
  gAckPacket.sNwk.iSrcAddr = pIndication->sFrame.sNwk.iSrcAddr;

  /* convert endian */
  NativeToOta16(gAckPacket.sMac.iPanId);
  NativeToOta16(gAckPacket.sMac.iDstAddr);
  NativeToOta16(gAckPacket.sMac.iSrcAddr);
  NativeToOta16(gAckPacket.sNwk.iFrameControl);
  NativeToOta16(gAckPacket.sNwk.iDstAddr);
  NativeToOta16(gAckPacket.sNwk.iSrcAddr);

  /* send the ACK packet */
  PopPhyDataRequest((void *)(&gAckPacket), sizeof(sPopDataFrame_t));

  /* put back into recieve mode */
  PopNwkReceive();
}

/*
  (NWK Internal Only)

  This function frees all the duplicated entries of the Duplicated Table
*/
void PopNwkFreeEntireDuplicateTable( void )
{
  uint8_t i;

  // look through duplicate list for expired entries
  for(i=0; i<cPopNwkDuplicateEntries; i++)
  {
    // mark entry as free
    if(PopNwkIsValidDuplicateEntry(i)){
      PopNwkFreeDuplicateEntry(i);
      giPopNwkDuplicateEntriesCount--;
    }
  }
}
/*
  (NWK Internal Only)

  Checks the duplicate table for any expired entries. This is only done at system idle
  time (no events)
*/
void PopNwkCheckDuplicates(popTimeOut_t iDiffTime)
{
  uint8_t i;

  // look through duplicate list for expired entries
  for(i=0; i<cPopNwkDuplicateEntries; ++i)
  {
    if(PopNwkIsValidDuplicateEntry(i))
    {
      // time has expiured, mark entry as free
      if(gaPopNwkDuplicateTable[i].iTimeLeftMs <= iDiffTime)
      {
        PopNwkFreeDuplicateEntry(i);
        giPopNwkDuplicateEntriesCount--;
      }

      // else count down
      else
        gaPopNwkDuplicateTable[i].iTimeLeftMs -= iDiffTime;
    }
  }
}

/*
  (NWK Internal Only)

  The duplicate table keeps track of received packets so duplicates aren't sent
  to higher layers. Used for unicasts and broadcasts.

  Duplicates expire after a little bit of time (depending on whether it's a
  unicast or broadcast).

  Parameter     Dir   Description
  ------------  ---   ----------------
  pIndication   in    OTA frame in native form (not secured)
  iType         in    broadcast or unicast (effects timeout)

  Returns gPopErrNone_c if added.
  Returns gPopErrFull_c if table is full.
  Returns gPopErrAlreadyThere_c if already in the table.
*/
popErr_t PopNwkAddToDuplicateTable(sPopNwkDataIndication_t *pIndication)
{
  uint8_t i;
  uint8_t iFreeEntry = gPopEndOfIndex_c;
  sPopNwkDuplicateTable_t  *pEntry;

  for(i=0; i < cPopNwkDuplicateEntries; ++i)
  {
    if(PopNwkIsValidDuplicateEntry(i))
    {
      if(gaPopNwkDuplicateTable[i].iSequence == pIndication->sFrame.sNwk.iSequence &&
        gaPopNwkDuplicateTable[i].iNwkSrc == pIndication->sFrame.sNwk.iSrcAddr)
      {
        return gPopErrAlreadyThere_c;  // already added, drop the packet
      }
    }
    else if(iFreeEntry == gPopEndOfIndex_c)
      iFreeEntry = i;
  }

  // no free entries left, drop the packet
  if(iFreeEntry == gPopEndOfIndex_c)
    return gPopErrFull_c;

  // add the entry
  pEntry = &gaPopNwkDuplicateTable[iFreeEntry];
  pEntry->iSequence = pIndication->sFrame.sNwk.iSequence;
  pEntry->iNwkSrc = pIndication->sFrame.sNwk.iSrcAddr;
  pEntry->iTimeLeftMs = ((pIndication->sFrame.sMac.iDstAddr == gPopNwkBroadcastAddr_c) ? giPopNwkBroadcastExpired : giPopNwkAckExpired);

  // indicate high water mark
  giPopNwkDuplicateEntriesCount++;
  if (giPopNwkDuplicateEntriesCount > giPopNwkDuplicateHighWaterMark)
    giPopNwkDuplicateHighWaterMark = giPopNwkDuplicateEntriesCount;

  // if we ever over run we must set the proper event.
  if(giPopNwkDuplicateEntriesCount >= cPopNwkDuplicateEntries)
    (void)PopSetTableFullEvent(gPopDuplicateTableFull_c);

  // tell caller we added it
  return gPopErrNone_c;
}

/*
  (NWK Internal Only)

  Free only those entries waiting on the results of a route discovery.

  fUnBlockOnly = true if unblocking pending packets (this happens on receipt of RREP)
*/
void PopNwkFreePendingRetryEntries( uint8_t iRouteDiscoveryIndex, bool fUnBlockOnly )
{
  uint8_t i;
  uint8_t iRetryEntry;
   sPopEvtData_t sEvtData;

  // got RREP, so free the entry that's waiting for our unblocking
  if(fUnBlockOnly)
  {
    iRetryEntry = gaPopNwkRouteDiscoveryTable[iRouteDiscoveryIndex].iRetryEntry;
    if(iRetryEntry < cPopNwkRetryEntries)
      PopNwkFreeRetryEntry(iRetryEntry, gPopErrNone_c);
  }

  // Free all entries
  for( i = 0; i < cPopNwkRetryEntries; i++)
  {
    if(gaPopNwkRetryTable[i].iFlags.fIsValid &&
        gaPopNwkRetryTable[i].iFlags2.iRREQIndex == iRouteDiscoveryIndex)
    {
      // unblock any pending unicast entries (got RREP)
      if(fUnBlockOnly)
        gaPopNwkRetryTable[i].iFlags2.fWait4RREP = false;

      // failed to deliver (no RREP), so give up
      else{
        // Set the invalid address
        sEvtData.iRouteConfirm = gPopNwkInvalidAddr_c;

        // Notify to the app layer the status of the route discovery
        (void)PopSetEventTo(false,gPopEvtNwkRouteConfirm_c,sEvtData.pData);

        PopNwkFreeRetryEntry(i, gPopErrRouteNotFound_c);
      }
    }
  }
}

/*
  (NWK Internal Only)

  Is there a pending route discovery from this node to a particular destination?
*/
uint8_t PopNwkFindPendingRouteDiscoveryEntry(popNwkAddr_t iDstAddr)
{
  uint8_t i;

  for(i=0; i < cPopNwkRouteDiscoveryEntries; ++i)
  {
    if( gaPopNwkRouteDiscoveryTable[i].iFlags.fIsInUse
        && (gaPopNwkRouteDiscoveryTable[i].iDstAddr == iDstAddr)
        && (gaPopNwkRouteDiscoveryTable[i].iSrcAddr == PopNwkGetNodeAddr()) )
    {
      // Entry was found
      return i;
    }

    if( gaPopNwkRouteDiscoveryTable[i].iFlags.fIsInUse
        && (gaPopNwkRouteDiscoveryTable[i].iSrcAddr == iDstAddr)
        && (gaPopNwkRouteDiscoveryTable[i].iDstAddr == PopNwkGetNodeAddr()) )
    {
      // Entry was found
      return i;
    }
  }

  // Entry was not found
  return gPopEndOfIndex_c;
}

/*
  Free the route discovery entry. Note: does not free an associated entries (such as those in the retry table).
*/
void PopNwkFreeRouteDiscoveryEntry(uint8_t i)
{
  // don't free invalid entries
  if(i >= cPopNwkRouteDiscoveryEntries)
    return;

  // clear the entry
  if(gaPopNwkRouteDiscoveryTable[i].iFlags.fIsInUse)
  {
    --giPopNwkRouteDiscoveryCount;
    PopMemSetZero(&gaPopNwkRouteDiscoveryTable[i], sizeof(sPopNwkRouteDiscovery_t));
  }
}

/*
  Free the route discovery table. Used during PopNwkLeaveNetwork().
*/
void PopNwkFreeEntireRouteDiscoveryTable(void)
{
  uint8_t i;

  // free the table
  for( i = 0; i < cPopNwkRouteDiscoveryEntries; i++)
    PopNwkFreeRouteDiscoveryEntry(i);
}

/*
  Check the discovery table for any timeouts. This is done on idle.
*/
void PopNwkCheckRouteDiscovery(popTimeOut_t iDiffTime)
{
  uint8_t i;
  sPopNwkRouteDiscovery_t *pEntry;

  // find route discovery entries
  for(i=0; i<cPopNwkRouteDiscoveryEntries; ++i)
  {
    if(gaPopNwkRouteDiscoveryTable[i].iFlags.fIsInUse)
    {
      pEntry = &gaPopNwkRouteDiscoveryTable[i];

      // has the time for this entry expired?
      if(iDiffTime < pEntry->iTimeLeftMs)
        pEntry->iTimeLeftMs -= iDiffTime;

      // route discovery entry has expired.
      else
      {
        // free pending retry entries (which includes sending a data confirm if needed)
        if(PopNwkGetNodeAddr() == pEntry->iSrcAddr || PopNwkGetNodeAddr() == pEntry->iDstAddr)
          PopNwkFreePendingRetryEntries(i, false);

        // free the route discovery entry
        PopNwkFreeRouteDiscoveryEntry(i);
      }

      // check the ACK timer for timeout
      if(iDiffTime < pEntry->iTimeAckWait)
        pEntry->iTimeAckWait -= iDiffTime;

      // ACK has timed out
      else
      {
        if(pEntry->iFlags.fWaitingOnAck || pEntry->iFlags.fWaitingToStart)
          PopNwkProcessNextRREPState(i, gPopNwkInvalidAddr_c);
      }

    }
  }
}

/*
  PopNwkDataRequest

  Application callable routine

  Send out some data over the air. This is the only routine an application needs to call
  to send data. All options are in the data request structure.

  Data requests make a COPY of the data, meaning the data can be in a local variable or
  array.

  Always returns error information through data confirm.
*/
void PopNwkDataRequest(sPopNwkDataRequest_t *pDataReq)
{
  popErr_t iErr;

  // don't allow invalid PAN IDs (or if not on network)
  iErr = gPopErrNone_c;
  if(!PopNwkIsOnNetwork())
    iErr = gPopErrNoNwk_c;

  // don't allow invalid options
  if(pDataReq->iOptions & gPopNwkDataReqOptsReserved_c)
    iErr = gPopErrBadParm_c;

  // something failed, give up
  if(iErr != gPopErrNone_c)
  {
    PopNwkSendAppDataConfirm(iErr);
    return;
  }

  // pass it on to the real data request handler
  PopNwkMgmtDataRequest(pDataReq, true);
}

/*
  PopNwkMgmtDataRequest

  (For Internal Nwk Layer Use Only)

  Used for both application data requests and internal PopNet commands. In both
  cases, retries are used on unicasts to neighbors and broadcasts.

  Returns gPopErrNone_c if worked.
*/
popErr_t PopNwkMgmtDataRequest(sPopNwkDataRequest_t *pDataReq, bool fFromApp)
{
  sPopNwkDataIndication_t  *pOtaFrame = 0;  // remove compiler warnings
  popErr_t                  iErr;
  popNwkRetryFlags_t        iFlags={0};
  popNwkDataReqOpts_t       iOptions;

  // this command is a NWK management command (not app data)
  if(!fFromApp)
  {
    pDataReq->iOptions |= gPopNwkDataReqOptsMgmt_c;
    iFlags.fIsItFromNwk = true;
  }

  // indicate the data request is from this node
  iFlags.fConfirm = true;

  // assume success
  iErr = gPopErrNone_c;

  // codesize optimization
  iOptions = pDataReq->iOptions;

  // reject sending too long of a packet
  if(pDataReq->iPayloadLength > PopNwkMaxPayload())
    iErr = gPopErrBadParm_c;

  // don't allow invalid destination addresses (broadcast OK)
  else if(!PopNwkIsValidNwkAddress(pDataReq->iDstAddr))
    iErr = gPopErrBadParm_c;

  // allocate and fill out the over-the-air frame (create the frame)
  else
  {
    // force route discovery (forget the route)
    if(iOptions & gPopNwkDataReqOptsForce_c)
      PopNwkDeleteRoute(pDataReq->iDstAddr);

    // Create the OTA frame from the data request, except for security and endianess which are set on radio transmit.
    pOtaFrame = PopNwkCreateDataFrame(pDataReq, iFlags);
    if(!pOtaFrame)
      iErr = gPopErrNoMem_c;

    // note: now the retry flags are in the overloaded pOtaFrame->sPre.iRetryFlags field, to communicate with PopNwkRouteFrame()
  }

  // if sending to ourselves, just do it now...
  if(iErr == gPopErrNone_c)
  {
    if(pDataReq->iDstAddr == PopNwkGetNodeAddr())
    {
      // send to higher layer (and free the frame because the network layer is done with it)
      // note: we don't do error checking, as the only reason why PopSendToHigherLayers() can fail
      // is if we're out of events, and if out of events, we need the memory freed. If sent to higher
      // layers, then the allocation count was increased, so we still need to free the  memory once.
      (void)PopNwkSendToHigherLayers(pOtaFrame);
      (void)PopMemFree(pOtaFrame);

      // tell app about success or failure
      PopNwkSendAppDataConfirm(iErr);
      return iErr;
    }

    // add to queue to send/route it
    // note: if from ourselves, then it will be native (not OTA) endian. No conversion needed at this point
    else
    {
      if(!PopNwkAddMsgToQueue(pOtaFrame))
      {
        (void)PopMemFree(pOtaFrame);
        iErr = gPopErrBusy_c;
      }
    }
  }

  // something failed, indicate the failure with the confirm
  if(iErr != gPopErrNone_c)
    PopNwkSendAppDataConfirm(iErr);

  return iErr;
}

/*
  PopNwkCreateDataFrame

  Converts a data request into an over-the-air ready data frame (with the exception of
  security and endian-ness). That is, no security is applied, and the packet is native endian.
  Security and endian are set when it it transmitted over-the-air.

  Communicates the appropriate retry flags to the NWK layer in the iLqi field in the
  preheader.

  Returns a pointer to the data indication, or NULL if failed (no memory).
*/
sPopNwkDataIndication_t *PopNwkCreateDataFrame(sPopNwkDataRequest_t *pDataReq, popNwkRetryFlags_t iFlags)
{
  sPopNwkDataIndication_t *pOtaFrame;
  uint8_t iLen;
  popNwkFrameControl_t iFrameControl;
  popNwkAddr_t iNextHop;
  popNwkAddr_t iDstAddr;
  popNwkDataReqOpts_t iOptions = pDataReq->iOptions;

  // reject if not enough memory for packet (header + payload + security).
  iLen = (sizeof(sPopNwkDataIndication_t) - 1) + pDataReq->iPayloadLength;
  pOtaFrame = PopMemAlloc(iLen + giPopNwkAuxFrameSize);
  if(!pOtaFrame)
    return NULL;

  // set up length of OTA frame (not indication structure).
  pOtaFrame->sPre.iFrameLen = iLen - sizeof(sPopNwkPreHeader_t);  // len does not include AUX header
  pOtaFrame->sFrame.sMac.iFrameControl = gMacOtaFrameControlData;
  pOtaFrame->sFrame.sMac.iPanId = PopNwkGetPanId();
  pOtaFrame->sFrame.sMac.iPathCost = 0;

  // say where this is coming from (from a NWK standpoint)
  pOtaFrame->sFrame.sMac.iSrcAddr =
  pOtaFrame->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();

  // broadcast
  iDstAddr = pDataReq->iDstAddr;
  if(iDstAddr == gPopNwkBroadcastAddr_c)
  {
    pOtaFrame->sFrame.sNwk.iDstAddr = gPopNwkBroadcastAddr_c;
    pOtaFrame->sFrame.sMac.iDstAddr = gPopNwkBroadcastAddr_c;
    iFlags.fIsBroadcast = true;
  }

  // unicast (or unibroadcast)
  else
  {
    // set final destination
    pOtaFrame->sFrame.sNwk.iDstAddr = iDstAddr;

    // unicast via broadcast mechanism (unibroadcast)
    if(iOptions & gPopNwkDataReqOptsUnibroadcast_c)
    {
      pOtaFrame->sFrame.sMac.iDstAddr = gPopNwkBroadcastAddr_c;
      iFlags.fIsBroadcast = true;
    }

    // true unicast
    else
    {
      iNextHop = PopNwkNextHop(iDstAddr);
      if(iNextHop == gPopNwkInvalidAddr_c)
        iNextHop = iDstAddr;
      pOtaFrame->sFrame.sMac.iDstAddr = iNextHop;
    }

  }

  // set up a unique sequence #, 0x00-0xff
  // increment the nwk sequence number. Always keep a new one for the next packet.
  pOtaFrame->sFrame.sNwk.iSequence = giPopNwkSequence++;

  // fill in NWK control portion of frame
  iFrameControl = gPopNwkFrameControl_c;

  // NWK management commands go to NWK layer, not APP layer
  if(iOptions & gPopNwkDataReqOptsMgmt_c)
    iFrameControl |= gPopNwkFcMgmt_c;

  // instruct final receiving node to forget route to this node
  if(iOptions & gPopNwkDataReqOptsDuplicatesOk_c)
    iFrameControl |= gPopNwkFcDuplicatesOk_c;

  // force route discovery solves forgotten route problem (only use on unicast)
  if(!iFlags.fIsBroadcast && (iOptions & gPopNwkDataReqOptsForce_c))
    iFrameControl |= gPopNwkFcRouteForced_c;

  // got all the options. set frame control
  pOtaFrame->sFrame.sNwk.iFrameControl = iFrameControl;

  // only route if unicast and user wants to discover a route if needed (not no discover)
  iFlags.fRoute = 0;
  if(!(iOptions & gPopNwkDataReqOptsNoDiscover_c) && pOtaFrame->sFrame.sMac.iDstAddr != gPopNwkBroadcastAddr_c)
    iFlags.fRoute = 1;

  // indicate 1 try instead of 3
  if(iOptions & gPopNwkDataReqOptsNoRetry_c)
    iFlags.fNoRetry = 1;

  // inform PopNwkRouteFrame() of the flags user has chosen
  pOtaFrame->sPre.iLqi = gPopNwkLqiFromThisNode_c;  // special flag indicates from this node
  pOtaFrame->sPre.iRetryFlags = iFlags;

  // radius 0 means default
  pOtaFrame->sFrame.sNwk.iRadius = (pDataReq->iRadius ? pDataReq->iRadius : PopNwkGetDefaultRadius());

  // copy the payload to the frame
  PopMemCpy(pOtaFrame->sFrame.aPayload, pDataReq->pPayload, pDataReq->iPayloadLength);

  // no worries (everything worked)
  return pOtaFrame;
}

/*
  PopNwkRawRadioTransmit

  This is used to resend already encrypted packets (such as on broadcast or unicast retries).
*/
void PopNwkRawRadioTransmit(sPopNwkDataIndication_t *pOtaFrame)
{
  // send the packet out the radio
  PopPhyDataRequest(&(pOtaFrame->sFrame), pOtaFrame->sPre.iFrameLen);

  // go back to receive mode, we're done transmitting
  PopNwkReceive();
}


/*
  PopNwkBeaconRequest

  Send out over the air a beacon request. Note: this permanently changes the channel.
*/
void PopNwkBeaconRequest( popChannel_t channel )
{
  // fix data to have a mac beacon alike
  static const sPopNwkBeaconReq_t beaconRequest =
  {
    gMacFrameControlBeaconRequest_c, // mac frame control
    gPopNetId_c,                      // mac sequence number
    0xFFFF,                           // iSrcPanId
    0xFFFF,                           // iDstPanId
    0x07                              // iCommandId
  };

  // Set the channel in which the scan will take place
  PopPhySetChannel(channel);

  // send beacon request out the radio
  PopPhyDataRequest((void *)(&beaconRequest), sizeof(beaconRequest));

  // go back to receive immediately
  PopNwkReceive();

}

/*
  PopNwkSendBeacon

  Send out over-the-air a beacon on the current channel. This can be in response to a
  beacon request, or sent by the local application. Some MACs cannot send directly from
  the application. Will not be sent if not on a network.
*/
void PopNwkSendBeacon( void )
{
  // do not send beacon if not on the network (or forming or joining)
  // note: this must work even during forming/joining in the NWK state machine
  if(PopNwkIsOffNetwork())
    return;

  // set up MAC portion of beacon
  gPopNetBeacon.iFrameControl = gMacFrameControlBeacon_c;
  gPopNetBeacon.iSequenceNumber = gPopNetId_c;
  gPopNetBeacon.iSrcPanId = PopNwkGetPanId();
  gPopNetBeacon.iSrcNwkAddr = PopNwkGetNodeAddr();
  gPopNetBeacon.iSuperFrame = gMacSuperFrame_c;
  gPopNetBeacon.iPendingAddrSpec = gMacPendingAddrSpec_c;
  gPopNetBeacon.iGtsSpec = gMacGtsSpec_c;

  // set up beacon payload
  gPopNetBeacon.sBeaconPayload.iProtocol = gPopNetId_c;
  gPopNetBeacon.sBeaconPayload.iProtocolVer = gPopNetProtocolVer_c;

  // indicate whether permit-joining is enabled or not, see PopNwkJoinEnable()
  if (PopNwkGetJoinStatus() && PopNwkGetAddrCount())
  {
    gPopNetBeacon.iSuperFrame |= gMacSuperFrameCapacityOn_c;
    gPopNetBeacon.sBeaconPayload.iProtocolVer |= gMacProtocolVersionCapacityOn_c;
  }
  else
  {
    gPopNetBeacon.iSuperFrame &= gMacSuperFrameCapacityOff_c;
    gPopNetBeacon.sBeaconPayload.iProtocolVer &= gMacProtocolVersionCapacityOff_c;
  }

  // set up the beacon payload
  PopNwkGetMacAddr(gPopNetBeacon.sBeaconPayload.aMacAddr);
  gPopNetBeacon.sBeaconPayload.iMfgId = PopNwkGetManufacturerId();
  gPopNetBeacon.sBeaconPayload.iAppId = PopNwkGetApplicationId();

  // convert to OTA format all multi-byte fields
  NativeToOta16(gPopNetBeacon.iSrcPanId);
  NativeToOta16(gPopNetBeacon.iSrcNwkAddr);
  NativeToOta16(gPopNetBeacon.sBeaconPayload.iMfgId);
  NativeToOta16(gPopNetBeacon.sBeaconPayload.iAppId);

  // send the packet out the radio
  PopPhyDataRequest(&gPopNetBeacon, sizeof(gPopNetBeacon));

  // go back to receive immediately
  PopNwkReceive();

}

/*
  PopNwkScanForNoise

  Scan for noise (energy) on a set of channels. Returns a ptr to an array of 16-bytes with
  energy level for each specified channel (the others will be 0x00). Cannot fail.

  Warning: this is a blocking function (no events occur while this runs).
*/
sPopNwkScanForNoise_t *PopNwkScanForNoise(popChannelList_t iChannelList)
{
  uint8_t  i;

  // set all channels to no noise
  PopMemSetZero(gsPopNwkScanForNoise.aEnergyLevels, sizeof(gsPopNwkScanForNoise));

  // Get all possible channels walking over the channel list.
  for(i=0; i < gPopNwkNumOfChannels_c; i++)
  {
    // Only register valid channels..
    if (iChannelList & ((popChannelList_t)1))
    {
      // Register the curretn noise.
      gsPopNwkScanForNoise.aEnergyLevels[i] = PopPhyScanForNoise(i + gPopNwkFirstChannel_c);
    }

    // Get the next channel in the list/
    iChannelList = (iChannelList >> 1);
  }

  // Set back the channel in which local device was working on, and put the device back into receive mode
  PopPhySetChannel(PopNwkGetChannel());
  PopNwkReceive();

  // return pointer to energy array
  return &gsPopNwkScanForNoise;
}

/*
  Helper function to PopNwkStartNetworkStateMachine().
*/
void PopNwkStatusFailed(popStatus_t iStatus)
{
  sPopEvtData_t sEvtData;

  // indicate we're off the network
  giPopNwkState = gPopStateOffTheNetwork_c;

  // tell the app the start failed
  sEvtData.iNwkStartConfirm = iStatus;
  (void)PopSetAppEvent(gPopEvtNwkStartConfirm_c, sEvtData.pData);

  // free the scan (if allocated) and stop watching beacons
  PopNwkScanComplete();
}

/*
  PopNwkStartNetworkStateMachine.

  This state machine is used to form/join the network. It is initiated by
  PopNwkStartNetwork(), PopNwkFormNetwork() and PopNwkJoinNetwork(). This routine has all
  the intelligence and does any retries needed.

  A high level view of the state machine is:
  ------------------------------------------
  1. Silent join if requested (if PanId, NwkAddr and Channel are all valid). Go to Now On Network.
  2. If joining a specific parent, go to joining
  3. Scan for Beacons
  4. Scan for Noise (form only, and only if requested)
  5. (if forming) Choose channel/PanID to form the network on
  6. (if joining) Choose channel/PanID/parent to join
  7. (if joining) Choose
  8. Set PAN ID, channel, NwkAddr
  9. Clean up allocated memory from scans
  10. Retry if didn't associate or form
  11. (if joining) Request addresses from addres server (if requested)
  12. Now on network
*/
void PopNwkStartNetworkStateMachine(void)
{
  popErr_t iErr = gPopErrNone_c;
  sPopNwkBeaconIndication_t *pParent;

  /*
    enter the new state. note: for future compatibility, do not change the values of
    gPopStateOffTheNetwork_c and gPopStateOnTheNetwork_c
  */
  switch(giPopNwkState)
  {
    // checks for silent form/join, or moves on to scanning state
    case gPopStateStart_c:

      // check if user wants to silent join (channel, PAN ID and NwkAddr must all be set)
      if (PopNwkIsValidChannel(PopNwkGetChannel()) &&
          PopNwkIsValidPanId(PopNwkGetPanId()) &&
          PopNwkIsValidUnicastAddress(PopNwkGetNodeAddr()))
      {
        // indicate silent join
        giPopNwkStateStatus = gPopStatusSilentStart_c;

        // we've silently joined, set channel and finish up
        giPopNwkState = gPopStateFinishUp_c;
      }

      // not silent starting, we will scan for networks
      else
      {

        // we will either form or join, so clear out our address pool
        PopNwkSetAddrCount(0);
        PopNwkSetNextAddrAvailable(0);

        // always start with an invalid nodeAddr
        PopNwkSetNodeAddr(gPopNwkInvalidAddr_c);

        // save the network cause (join, form, smart)
        giPopNwkPreviousStateCause = giPopNwkStateCause;

        // we will scan up to 3 times when trying to find a suitable parent
        giPopNwkStateJoinTriesLeft = PopNwkGetJoinRetries();

        // start scanning for networks (unless user is joining and has already chosen a parent)
        if(giPopNwkStateCause == gPopNwkCauseJoin_c && PopNwkIsValidUnicastAddress(giPopNwkStateParent))
          giPopNwkState = gPopStateJoinNetwork_c;
        else
        giPopNwkState = gPopStateScanningNetworks_c;
      }

      // on to next state
      (void)PopSetEventNoData(gPopEvtNwkNextStartState_c);
    break;

    // scanning for networks
    case gPopStateScanningNetworks_c:

      // inform scan for networks we're waiting for scan confirm
      giPopNwkState = gPopStateWaitingForScanConfirm_c;
      iErr = PopNwkScanForNetworks(PopNwkGetChannelList(), PopNwkGetJoinOptions(), PopNwkGetScanTimeMs());

      // not enough memory to scan (or bad options, such as an empty channel list). Give up.
      if(iErr)
      {
        PopNwkStatusFailed(iErr);
        return;
      }

      // note: when scan is complete, it will call the state machine. See PopNwkScanNextChannel().
    break;

    // ok, we now have the scan confirm.
    // now, choose whether we want to form or join.
    case gPopStateWaitingForScanConfirm_c:

      // record if there is a suitable parent to join. Useful for both form and join.
      // record the entry to join in the gpPopNwkDiscoveryTable in giPopNwkStateEntry
      // status will indicate success or failure
      giPopNwkStateStatus = PopNwkFindSuitableParentToJoin(&giPopNwkStateEntry);

      // user wants to join a network, choose the parent
      if(giPopNwkStateCause == gPopNwkCauseJoin_c)
        giPopNwkState = gPopStateJoinChoose_c;

      // user wants to form a network
      else if(giPopNwkStateCause == gPopNwkCauseForm_c)
        giPopNwkState = gPopStateFormNetwork_c;

      // user wants the stack to pick whether to form or join
      else
      {
        // there is a parent to join, go join it  - or -
        // if the error is a "join" type error, then let join handle the error
        if(giPopNwkStateStatus == gPopStatusJoinSuccess_c || giPopNwkStateStatus == gPopStatusJoinDisabled_c
          || giPopNwkStateStatus == gPopStatusWrongMfgId_c)
          giPopNwkState = gPopStateJoinChoose_c;

        // ok, we're forming then
        else
        {
          // depending on scan options, either scan for noise or not
          giPopNwkState = gPopStateFormNetwork_c;
        }
      }

      // go on to next state (forming or joining)
      (void)PopSetEventNoData(gPopEvtNwkNextStartState_c);
    break;

    // form the network
    case gPopStateFormNetwork_c:

      // if user wants a random PAN ID, pick a random PAN
      if(PopNwkGetPanId() == gPopNwkAnyPanId_c)
        PopNwkSetPanId(PopNwkGetNoConflictRandomPanId());

      // don't form if ZigBee or another PopNet PAN is there.
      else if(giPopNwkStateStatus != gPopStatusNoParent_c)
      {
        PopNwkStatusFailed(gPopStatusPanAlreadyExists_c);
        return;
      }

      // no (optionally) scan for noise to use on forming on the best channel
      // this is a blocking function... will wait until scanning is done
      // checks if user enabled scanning in both PopCfg.h: gPopScanForNoise_d and
      // in join options: PopNwkGetJoinOptions()
      PopNwkOptionallyScanForNoise();

      // choose quietest channel on which to form (if forming)
      PopNwkSetChannel(PopNwkFindBestChannel());

      // we're now done with the scan, free it up
      PopNwkScanComplete();

      // set node address to 0x0000, and the address pool to all addresses.
      PopNwkSetNodeAddr(0x0000);
      PopNwkSetNextAddrAvailable(0x0001);
      PopNwkSetAddrCount(gPopNwkReservedAddr_c - 1);

      // if we formed, send out a beacon, unless user said no
      if( !(PopNwkGetJoinOptions() & gPopNwkJoinOptsSilent_c) )
        PopNwkSendBeacon();

      // successfully formed
      giPopNwkStateStatus = gPopStatusFormSuccess_c;

      // done forming, tell user!
      giPopNwkState = gPopStateFinishUp_c;
      (void)PopSetEventNoData(gPopEvtNwkNextStartState_c);
    break;

    // choose channel/parent to join if joining
    // will retry the scan if no suitable parent
    case gPopStateJoinChoose_c:

      // Check join retries
      if(giPopNwkStateStatus != gPopStatusJoinSuccess_c)
      {
        // indicate we've got 1 less time to try (for next time)
        if(giPopNwkStateJoinTriesLeft != 0xff)
        --giPopNwkStateJoinTriesLeft;

        // if we didn't find a suitable parent, return the error to app
        if(!giPopNwkStateJoinTriesLeft)
        {
          // failed to join after all retries
          if(giPopNwkStateCause == gPopNwkCauseJoin_c)
            giPopNwkStateStatus = gPopStatusJoinFailure_c;

          PopNwkStatusFailed(giPopNwkStateStatus);
          return;
        }

        // we're now done with the scan, free it up
        PopNwkScanComplete();

        // we will either form or join, so clear out our address pool
        PopNwkSetAddrCount(0);
        PopNwkSetNextAddrAvailable(0);

        // always start with an invalid nodeAddr
        PopNwkSetNodeAddr(gPopNwkInvalidAddr_c);

        // Restore the network cause to its original cause
        giPopNwkStateCause = giPopNwkPreviousStateCause;

        // start scanning for networks again (after the random waiting period)
        giPopNwkState = gPopStateScanningNetworks_c;
        (void)PopStartTimerEx(cNwkTaskId, gPopNwkJoinTimer_c, giPopNwkJoinExpired + PopNwkJitter16(giPopNwkReJoinJitter));
        break;
      }

      // OK, suitable parent found...

      // setup parent/channel/pan from discovery table
      pParent = &gpPopNwkDiscoveryTable->aBeaconList[giPopNwkStateEntry];
      if(giPopNwkStateParent == gPopNwkAnyParentAddr_c)
        giPopNwkStateParent = pParent->iNwkAddr;
      PopNwkSetChannel(pParent->iChannel);
      PopNwkSetPanId(pParent->iPanId);

      // we're now done with the scan, free it up
      PopNwkScanComplete();

      giPopNwkState = gPopStateJoinNetwork_c;
      (void)PopSetEventNoData(gPopEvtNwkNextStartState_c);
    break;

    // now have a valid parent, join the network
    case gPopStateJoinNetwork_c:


      // assume success
      giPopNwkStateStatus = gPopStatusJoinSuccess_c;

      // MAC association request. If this fails after 3 tries, then it will call PopNwkStatusFailed() to
      // end the state machine.
      giPopNwkAssociateTriesLeft = giPopNwkMaxRetries;
      PopNwkMgmtJoinRequest();

      // next state will be successfully joined if all works (we get the association rsp)
      giPopNwkState = gPopStateFinishUp_c;
      break;

    // node is now on the fully on the network and can communicate.
    case gPopStateFinishUp_c:

      // We assume that node was formed/joined with success. So, we can clear the previous nwk cause
      giPopNwkPreviousStateCause = 0xFF;

      // we're now on the network
      giPopNwkState = gPopStateOnTheNetwork_c;

      // get some addresses
      if(giPopNwkStateStatus == gPopStatusJoinSuccess_c)
      {
        // if the proper options are set, request our own address pool.
        if (!(PopNwkGetJoinOptions() & gPopNwkJoinOptsDontGetAddr_c))
        {
          // Send the request OTA, this action is atomic.
          PopNwkAddrPoolRequest(PopNwkGetAddressServer(), PopNwkGetAddrReqSize());
        }
      }

      // Let the application know that the join/form was a success.
      // Give other things a chance to init in case user called PopNwkStartNetwork() in the init routine.
      (void)PopStartTimerEx(cNwkTaskId, gPopNwkJoinConfirmTimer_c, 10);

      // done with state machine (and don't come back!)
    break;
  default:
    break;
  }
}

/*
  PopNwkScanForNetworks

  Starts network discovery scanning for an specific period of time in the given channels.
  After scanning is complete a ScanConfirm will be sent to the application. This is scanning
  for beacons, and will return all 802.15.4 beacons in the list.

  This will fail if there is no memory or a bad channel list or options
*/
popErr_t PopNwkScanForNetworks
(
  popChannelList_t iChannelList,
  popScanOption_t iOptions,
  popTimeOut_t iTimeForEachScanMs
)
{
  // if the list is empty return the proper error code
  if (!iChannelList || (iOptions & gPopNwkScanOptsReserved_c))
    return gPopErrBadParm_c;

  // if already scanning, don't do it again (no reentry)
  if(gpPopNwkDiscoveryTable)
    return gPopErrBusy_c;

  /*
    Allocate memory to keep track of the beacons we hear. Note: We COULD decrease the # of
    beacons we record if not enough memory... but then the beacon list will not have as good
    quality.
  */
  gpPopNwkDiscoveryTable = PopMemAlloc(giMaxSizeDiscoveryTable * sizeof(sPopNwkBeaconIndication_t) + 1);

  // if there is no memory available return the proper error code
  if (!gpPopNwkDiscoveryTable)
    return gPopErrNoMem_c;

  // prepare to gather beacons
  gfPopNwkIsProcessingBeaconEnabled = true;

  // assume no beacons have been collected so far
  gpPopNwkDiscoveryTable->iNumberOfBeacons = 0;

  // save scan information to global variables for further use
  mPopNwkScanChannelList  = iChannelList;
  mPopNwkScanOptions      = iOptions;
  mPopNwkScanTimeout      = iTimeForEachScanMs;

  // find first channel in channel list
  mPopNwkScanChannelToScan = 0;

  // scan a channel (its a scan state machine)
  PopNwkScanNextChannel();

  // the confirm will came back from the state machine.
  return gPopErrNone_c;
}

/*
  PopNwkScanComplete [public function]

  Frees the discovery table and sets the global pointer to NULL. See also PopNwkScanForNetworks().
  Doesn't hurt if this is called more than once.
*/
void PopNwkScanComplete(void)
{
  if(gpPopNwkDiscoveryTable)
  {
    (void)PopMemFree(gpPopNwkDiscoveryTable);
    gpPopNwkDiscoveryTable = NULL;
  }

  // turn off gathering beacons
  gfPopNwkIsProcessingBeaconEnabled = false;
}


/*
  NWK Internal Function: PopNwkScanNextChannel

  This is a state machine to scan the set of channels the user wants. Initiated from
  PopNwkScanForNetworks().

  mPopNwkScanChannelToScan   // the channel to scan
  mPopNwkScanChannelList         // channel list
  mPopNwkScanTimeout             // time to wait for beacons
  mPopNwkScanOptions             // indicates silent or not
*/
void PopNwkScanNextChannel( void )
{
  // if not first time, go to next channel
  if (mPopNwkScanChannelToScan)
    ++mPopNwkScanChannelToScan;

  // any more channels to scan (will return 0 if no more channels)
  mPopNwkScanChannelToScan = PopNwkGetFirstChannel(mPopNwkScanChannelList, mPopNwkScanChannelToScan);

  // if we have another channel to scan, scan it
  if(mPopNwkScanChannelToScan)
  {
    // if active scan is required send out the beacon request
    if(!(mPopNwkScanOptions & gPopNwkScanOptsPassiveScan_c))
    {
      // send out a beacon request on this particular channel
      PopNwkBeaconRequest(mPopNwkScanChannelToScan);
    }
    else
    {
      // Set the channel in which the scan will take place
      PopPhySetChannel(mPopNwkScanChannelToScan);
    }

    // receive beacons during this period of time on this particular channel
    (void)PopStartTimerEx(cNwkTaskId,gPopNwkScanTimer_c, mPopNwkScanTimeout);
    return;
  }

  /*
    If this point is reached it means we're done scanning all the channels in the list, is
    time to generate the scan confirm
  */

  // set back the channel in which local device was working on
  PopPhySetChannel(PopNwkGetChannel());

  // generate scan confirm
  // Send the scan confirm to the network layer if it's in forming or joinin mode
  // or to the app layer if the user has called a PopNwkScanNetwork function directly
  if (PopNwkIsJoiningOrForming())
    (void)PopSetEventEx(cNwkTaskId, gPopEvtNwkNextStartState_c, gpPopNwkDiscoveryTable);
  else
    (void)PopSetAppEvent(gPopEvtNwkScanConfirm_c, gpPopNwkDiscoveryTable);
}

/*
  PopNwkIsValidChannel

  Returns true if the given channel Id is between 11 and 26, false otehrwise.

  Receives the channel id to compare.
*/
bool PopNwkIsValidChannel(popChannel_t iChannel)
{
  return ((iChannel >= gPopNwkFirstChannel_c) && (iChannel < (gPopNwkFirstChannel_c + gPopNwkNumOfChannels_c)))? true: false;
}

/*
  PopNwkIsValidNwkAddress

  Returns true if the given address is not a reserved one or broadcast, false otherwise.
*/
bool PopNwkIsValidNwkAddress(popNwkAddr_t iAddress)
{
  return (((iAddress) < gPopNwkReservedAddr_c) || ((iAddress) == gPopNwkBroadcastAddr_c))? true: false;
}


/*
  PopNwkCreateBeaconIndication

  Received a beacon over the air.  Generate a beacon indication that is passed to the NHL
*/
void PopNwkCreateBeaconIndication( sPopNwkDataIndication_t *pIndication)
{

  sPopNwkBeacon_t *pBeacon = (sPopNwkBeacon_t *)&pIndication->sFrame;

  sPopNwkBeaconIndication_t *pBeaconIndication;

  pBeaconIndication = PopMemAlloc(sizeof(sPopNwkBeaconIndication_t));

  if(!pBeaconIndication)
  {
    (void)PopMemFree(pBeacon);
    return;
  }

  // fill pBeaconIndication message
  pBeaconIndication->iChannel = giPhyChannel;
  pBeaconIndication->iLqi = pIndication->sPre.iLqi;
  pBeaconIndication->iNwkAddr = pBeacon->iSrcNwkAddr;
  pBeaconIndication->iPanId = pBeacon->iSrcPanId;
  pBeaconIndication->iProtocol = pBeacon->sBeaconPayload.iProtocol;
  pBeaconIndication->iProtocolVer = pBeacon->sBeaconPayload.iProtocolVer;
  pBeaconIndication->iMfgId = pBeacon->sBeaconPayload.iMfgId;
  pBeaconIndication->iAppId = pBeacon->sBeaconPayload.iAppId;
  PopMemCpy(pBeaconIndication->aMacAddr,pBeacon->sBeaconPayload.aMacAddr,8);

  // Convert to Native format required fields
  OtaToNative16(pBeaconIndication->iNwkAddr);
  OtaToNative16(pBeaconIndication->iPanId);
  OtaToNative16(pBeaconIndication->iMfgId);
  OtaToNative16(pBeaconIndication->iAppId);

  (void)PopSetAppEvent(gPopEvtNwkBeaconIndication_c, pBeaconIndication);

  (void)PopMemFree(pBeacon);
}

/*
  PopNwkJoinPotential

  Is this beacon one we could potentially join?
*/
bool PopNwkJoinPotential(sPopNwkBeaconIndication_t *pNwkBeaconIndication, popPanId_t iPanId)
{
  // we can potentially join this node?
  // yes, if it's PopNet, the right version, joining is enabled and the PAN we're looking for
  return ( PopNwkIsValidProtocol(pNwkBeaconIndication->iProtocol) &&
      PopNwkIsValidProtocolVer(pNwkBeaconIndication->iProtocolVer) &&
      PopNwkIsJoiningEnabled(pNwkBeaconIndication->iProtocolVer) &&
      (iPanId == gPopNwkAnyPanId_c || pNwkBeaconIndication->iPanId == iPanId));
}

/*
  Internal helper function.

  Uses the global gpPopNwkDiscoveryTable. Find a duplicate PAN ID entry.

  Returns NULL (no duplicates found), or a pointer to the highest PopNet beacon that
  duplicates a previous beacon.
*/
sPopNwkBeaconIndication_t *PopNwkPickDuplicatePopNetBeacon(sPopNwkBeaconIndication_t *pBestPopNetParent)
{
  uint8_t i;
  uint8_t j;
  sPopNwkBeaconIndication_t *pFound;            // a Temporary Beacon Indication

  // look through entire list, looking for duplicates.
  // will compare the ith entry to the jth entry (i is earlier in list)
  // only cares about Pan ID, not channel, as ZigBee, RF4CE, etc.. are cross channel.
  pFound = NULL;
  for(i=0; !pFound && i < giMaxSizeDiscoveryTable; ++i)
  {
    for(j = i + 1; !pFound && j < giMaxSizeDiscoveryTable; ++j)
    {
      // is it a duplicate PAN ID? then we can reuse this slot (but not if it's the best PopNet parent)
      if(gpPopNwkDiscoveryTable->aBeaconList[i].iPanId == gpPopNwkDiscoveryTable->aBeaconList[j].iPanId &&
        &gpPopNwkDiscoveryTable->aBeaconList[j] != pBestPopNetParent)
      {
          pFound = &gpPopNwkDiscoveryTable->aBeaconList[j];
      }
    }
  }

  return pFound;
}

/*
  PopNwkProcessBeaconIndication

  Potentially add the received beacon indication to the discovery table. If the table is full
  then it may overwrite an existing entry depending on its importance.

  Beacon order of importance:

  If looking for any beacons (forming or smart), then
    a. pick one PopNet, join enabled, right PanId, best LQI over all others (a parent to join)
    b. pick not-already-heard pan ID over all others... (for wide pan ID)
*/
void PopNwkProcessBeaconIndication(sPopNwkBeaconIndication_t *pNwkBeaconIndication)
{
  uint8_t i;
  sPopNwkBeaconIndication_t *pAvailableEntry; // an available entry in the discovery table
  sPopNwkBeaconIndication_t *pBestPopNetParent;  // an available entry in the discovery table
  sPopNwkBeaconIndication_t *pDuplicate;      // an available entry in the discovery table
  bool fHavePopNetParent;       // we have a join enabled beacon that matches Pan ID & MfgId

  // if discovery table has not been allocated ignore the beacon
  if(!gpPopNwkDiscoveryTable)
    return;

  // if there is available spots add the new entry, and we're done
  if(gpPopNwkDiscoveryTable->iNumberOfBeacons < giMaxSizeDiscoveryTable)
  {
    pAvailableEntry = gpPopNwkDiscoveryTable->aBeaconList + gpPopNwkDiscoveryTable->iNumberOfBeacons;
    gpPopNwkDiscoveryTable->iNumberOfBeacons++;
  }

  // Assume this incoming beacon is NOT more important any of the other beacons in the list
  else
  {
    pAvailableEntry = NULL;

    // find the highest (best LQI) PopNet beacon in the beacon list
    if(PopNwkFindSuitableParentToJoin(&i) == gPopStatusJoinSuccess_c)
      pBestPopNetParent = &gpPopNwkDiscoveryTable->aBeaconList[i];

    // is this incoming beacon a node we could join?
    fHavePopNetParent = PopNwkJoinPotential(pNwkBeaconIndication, PopNwkGetPanId());

    // if our incoming beacon is a PopNet parent with better LQI, then use the
    // PopNet slot of the previous best parent.
    if(fHavePopNetParent && pBestPopNetParent->iLqi > pNwkBeaconIndication->iLqi)
    {
      pAvailableEntry = pBestPopNetParent;
    }

    // don't have better PopNet parent, so just look for random dispursement of Pan IDs.
    if(!pAvailableEntry)
    {
      // pick a duplicate (any one but the best potention parent)
      pDuplicate = PopNwkPickDuplicatePopNetBeacon(pBestPopNetParent);

      // if we found a duplicate, replace it if the incoming packet is one we may want to join, or
      // incoming beacon has different pan ID than the duplicate.
      if(pDuplicate)
      {
        // if we don't yet have a PopNet parent && this one is a PopNet parent, put it in the list
        if(fHavePopNetParent && !pBestPopNetParent)
          pAvailableEntry = pDuplicate;

        // replace the duplicate if the incoming PAN ID is different
        else if(pDuplicate->iPanId != pNwkBeaconIndication->iPanId)
          pAvailableEntry = pDuplicate;
      }

      // no duplicate entries
      else
      {
        // if incoming beacon is PopNet a parent and we have no parents in the list, then use entry 0
        if(fHavePopNetParent && !pBestPopNetParent)
          pAvailableEntry = &gpPopNwkDiscoveryTable->aBeaconList[0];
      }
    }
  }

  // did we find an entry to overwrite? ok then, use it
  if (pAvailableEntry)
    PopMemCpy(pAvailableEntry, pNwkBeaconIndication, sizeof(sPopNwkBeaconIndication_t));
}

/*
  PopNwkIsBeaconDuplicate

  check the discovery table to see if the given beacon is already there, if so, return true
  otherwise return false

*/
bool PopNwkIsBeaconDuplicate(sPopNwkBeaconIndication_t *pBeaconIndication)
{
  uint8_t iIndex;
  // a Temporary Beacon Indication
  sPopNwkBeaconIndication_t *pTBI;

  for(iIndex=0; iIndex < giMaxSizeDiscoveryTable; iIndex++)
  {
    pTBI = gpPopNwkDiscoveryTable->aBeaconList + iIndex;
    if(pTBI->iChannel == pBeaconIndication->iChannel &&
       pTBI->iPanId == pBeaconIndication->iPanId &&
       pTBI->iMfgId == pBeaconIndication->iMfgId)
    {
      // beacon already in discovery table
      return true;
    }
  }
  //beacon was not found in discovery table
  return false;
}

/*
  PopNwkProcessIfIsABeaconFrame

  Check the incoming data indication to see if it is a beacon or a beacon request packet
  if so the indication is processed here and return true, if not return false

  Returns true if it was a beacon or beacon request
*/
bool PopNwkProcessIfIsABeaconFrame( sPopNwkDataIndication_t *pIndication )
{
  sPopNwkBeacon_t *pBeacon = (sPopNwkBeacon_t *)&pIndication->sFrame;

  if(pBeacon->iFrameControl == gMacFrameControlBeaconRequest_c)
  {
    if (PopNwkIsOnNetwork())
    {
      // If the node is on the network needs to send a beacon out, so we wait a time before
      // answering to avoid collisions. If Join enable then response is sent alomst directly
      if(PopNwkGetJoinStatus())
        (void)PopStartTimerEx(cNwkTaskId, gPopNwkSendBeaconTimer_c, PopNwkJitter08(giPopNwkBeaconJitterJoinEnable));
      else
        (void)PopStartTimerEx(cNwkTaskId, gPopNwkSendBeaconTimer_c, (giPopNwkBeaconJitterJoinEnable + PopNwkJitter08(giPopNwkBeaconJitter)));
    }

    // it is a Beacon and it's been processed
    return true;
  }
  else if(pBeacon->iFrameControl == gMacFrameControlBeacon_c)
  {
    if(gfPopNwkIsProcessingBeaconEnabled)
    {
      PopNwkCreateBeaconIndication(pIndication);
    }

    // it is a Beacon and it's been processed
    return true;
  }
  else
  {
    // it is not a beacon frame
    return false;
  }
}


/*
  This function is called if it's an ACK. Checks if there are any pending route requests
  that need a reply. If so, handle it.

  The NwkDstAddr == the originating NwkSrcAddr (where the RREQ came from)
*/
bool PopNwkProcessIfIsRouteReplyAck(sPopDataFrame_t* pOtaDataFrame)
{
  uint8_t i;
  popNwkAddr_t iNwkSrcAddr;
  popNwkAddr_t iMacSrcAddr;
  sPopNwkRouteDiscovery_t *pEntry;

  // convert NwkSrcAddr to native endian
  iNwkSrcAddr = pOtaDataFrame->sNwk.iDstAddr;
  OtaToNative16(iNwkSrcAddr);

  // from the NwkDst and sequence, find the RREQ structure
  i = PopNwkFindRouteReq(iNwkSrcAddr, pOtaDataFrame->sNwk.iSequence, NULL);
  if(i == gPopEndOfIndex_c)
    return false;

  // stop waiting for ACK.
  pEntry = &gaPopNwkRouteDiscoveryTable[i];
  pEntry->iFlags.fWaitingOnAck = false;

  // move on to next entry in case token comes back to us
  pEntry->iAckTries = 0;
  ++pEntry->iPrevHopIndex;

  // Add the route where it goes
  iMacSrcAddr = pOtaDataFrame->sMac.iSrcAddr;
  OtaToNative16(iMacSrcAddr);
  PopNwkAddRoute(pEntry->iSrcAddr, iMacSrcAddr);

  // if not the final destination, record route back to parent
  if(PopNwkGetNodeAddr() != pEntry->iDstAddr)
    PopNwkAddRoute(pEntry->iDstAddr, pEntry->iRRepSrcAddr);

  return true;
}

/*
  PopNwkProcessIfIsAnAckFrame

  Check the incoming data indication to see if it is an acknowledgement
  if so the indication is processed here and return true, if not return false.

  Note: ACK frames are NOT secured. That means we can process prior to unsecuring
  the frame and that endian is still OTA (not native).

  Returns true if this was an ACK frame
*/
bool PopNwkProcessIfIsAnAckFrame(sPopDataFrame_t* pOtaDataFrame, uint8_t iFrameLen)
{
  uint8_t i;
  uint16_t iTmp16;  // temporary 16-bit place holder
  popNwkAddr_t iNwkSrcAddr;   // source address

  // if not a MAC data frame or it does not contains the NWK command ACK
  // note: PopNet 2.0 ACK frames look like a data frame.. with a NWK mangement cmd == gPopNwkMgmtAck_c
  if((pOtaDataFrame->sMac.iFrameControl != gMacFrameControlAck_c) || (iFrameLen != sizeof(sPopDataFrame_t))
        || (pOtaDataFrame->aPayload[0] != gPopNwkMgmtAck_c) )
    return false;

  // make sure NWK frame control indicates its a NWK command.
  iTmp16 = pOtaDataFrame->sNwk.iFrameControl;
  OtaToNative16(iTmp16);
  if(iTmp16 != (gPopNwkFrameControl_c | gPopNwkFcMgmt_c))
    return false;

  // check the PAN ID (it's an ACK alright , but maybe not for us)
  iTmp16 = pOtaDataFrame->sMac.iPanId;
  OtaToNative16(iTmp16);
  if( iTmp16 != PopNwkGetPanId() )
    return true;

  // check the MAC Addr (it's an ACK alright, but maybe not for us)
  iTmp16 = pOtaDataFrame->sMac.iDstAddr;
  OtaToNative16(iTmp16);
  if( iTmp16 != PopNwkGetNodeAddr() )
    return true;

  // check if it's a route reply ACK
  if(PopNwkProcessIfIsRouteReplyAck(pOtaDataFrame))
    return true;

  // convert NwkSrcAddr to native endian
  iNwkSrcAddr = pOtaDataFrame->sNwk.iSrcAddr;
  OtaToNative16(iNwkSrcAddr);

  // if ACK is for this node, then free the entry
  i = PopNwkFindRetryEntry(pOtaDataFrame->sNwk.iSequence, iNwkSrcAddr, NULL);
  if(i != gPopEndOfIndex_c)
  {
    // got the ACK, quit retrying (which will also send a data confirm to the app if needed)
    PopNwkFreeRetryEntry(i, gPopErrNone_c);
  }

  // it is an Ack Frame (whether for us or not). We need process no further.
  return true;
}

/*
  PopNwkLeaveNetwork

  Gets the node off the network by chanching the current network state to be Off the network
  and by setting the node address to be an invalid address.

  Receives no values and returns nothing.
*/
void PopNwkLeaveNetwork(void)
{
  // ------------ Stop All NWK Task Timers -------------
  PopStopAllTimers(cNwkTaskId);

  // Free the scan buffers if the node was in the middle of  form/join/scan.
  PopNwkScanComplete();

  // indicate we're off the network
  giPopNwkState = gPopStateOffTheNetwork_c;
  PopNwkSetNodeAddr(gPopNwkInvalidAddr_c);

  // ------------ Clean all tables ------------

  // Clear network queue
  PopNwkFreeMsgQueue();

  // Clear retry table
  PopNwkFreeEntireRetryTable();

  // Clear duplicated table
  PopNwkFreeEntireDuplicateTable();

  // Clear I-Can-Hear-You Table
  PopNwkClearICanHearYou();

  // Clear Route Table
  PopNwkDeleteEntireRouteTable();

  // Clear both Route Discovery and Route Discovery Timer Id's Table
  PopNwkFreeEntireRouteDiscoveryTable();

  // Notify to App Layer
  (void)PopSetAppEvent(gPopEvtNwkLeaveConfirm_c, NULL);
}

/*
  Helper function.

  Called by PopNwkStartNetwork(), PopNwkJoinNetwork, and PopNwkFormNetwork(). This starts
  up the state machine if needed, or just returns a confirm to the application if one of
  the input paramters is bad. See PopNwkStartNetworkStateMachine().

  By the time it gets here, the global channel should have been converted to a valid channel
  from the channel list (if it was 0).

  Validates the following state machine globals:
  popNwkJoinOpts_t    giPopNwkStateJoinOpts;        // form/join options
  popChannel_t        giPopNwkStateChannel;         // channel on which to form. 0=use global channel list
  popPanId_t          giPopNwkStatePanId;           // pan ID on which to form/join, 0xffff=random pan ID on form, 0xffff=any pan ID on join
  popNwkAddr_t        giPopNwkStateParent;          // parent to join, 0xffff=any
*/
void PopNwkValidateAndStartStateMachine(void)
{
  sPopEvtData_t sEvtData;

  // don't startup state machine if already on the network or in the process
  sEvtData.iStatus = gPopStatusSuccess_c;
  if (PopNwkIsOnNetwork())
    sEvtData.iStatus = gPopStatusAlreadyOnTheNetwork_c;
  else if (giPopNwkState != gPopStateOffTheNetwork_c)
    sEvtData.iStatus = gPopStatusBusy_c;

  // validate the input parameters
  else if(PopNwkGetJoinOptions() & gPopNwkJoinOptsReserved_c)
    sEvtData.iStatus = gPopErrBadParm_c;
  else if(!PopNwkIsValidChannel(PopNwkGetChannel()))
    sEvtData.iStatus = gPopErrBadParm_c;
  else if(!(PopNwkIsValidPanId(PopNwkGetPanId()) || PopNwkIsRandomPanId(PopNwkGetPanId())))
    sEvtData.iStatus = gPopErrBadParm_c;
  else if(!PopNwkIsValidNwkAddress(giPopNwkStateParent))    /* can be valid NwkAddr or 0xffff (any parent) */
    sEvtData.iStatus = gPopErrBadParm_c;

  // if we failed, tell the user
  if(sEvtData.iStatus != gPopStatusSuccess_c)
  {
    (void)PopSetAppEvent(gPopEvtNwkStartConfirm_c, sEvtData.pData);
    return;
  }

  // parameters look ok, startup state machine
  giPopNwkState = gPopStateStart_c;
  (void)PopSetEventEx(cNwkTaskId, gPopEvtNwkNextStartState_c, NULL);
}

/*
  PopNwkIsOnNetwork [Public Command]

  Is this node on the network or not?
*/
bool PopNwkIsOnNetwork(void)
{
  return (PopNwkGetNetworkState() == gPopStateOnTheNetwork_c);
}

/*
  PopNwkIsOffNetwork [Public Command]

  Is this node definitely off the network (not on, or forming or joining)?
*/
bool PopNwkIsOffNetwork(void)
{
  return (PopNwkGetNetworkState() == gPopStateOffTheNetwork_c);
}

/*
  PopNwkIsJoiningOrForming [Public Command]

  Returns true if the network is in the state of forming or joining.
*/
bool PopNwkIsJoiningOrForming(void)
{
  return ((PopNwkGetNetworkState() != gPopStateOffTheNetwork_c) && (PopNwkGetNetworkState() != gPopStateOnTheNetwork_c) );
}

/*
  PopNwkSetChannelFromInput

  (internal) helper function. Set channel from input to PopNwkFormNetwork() or PopNwkJoinNetwork().
  1. Channel 0x00 used to be invalid prior to the change to the state machine. That's why the test procedure was wrong.
  2. Channel 0x00 now instructs form or join to use the channel list rather than a specific channel (similar to PopNwkStartNetwork()).
  3. Channels 11-26 (decimal) are also valid. Any other channel should be invalid.
*/
void PopNwkSetChannelFromInput(popChannel_t iChannel)
{
  popChannelList_t iChannelList;

  // If the channel is invalid and it is not zero
  if (iChannel != 0 && !PopNwkIsValidChannel(iChannel))
  {
    // Set the channel (only in nwk data structure not in phy layer) to use after
    PopNwkSetNwkDataChannel(iChannel);
    return;
  }

  if (!iChannel)
  {
    // if channel list is invalid (0), set to channel 11.
    iChannelList = PopNwkGetChannelList();
    if(!iChannelList)
      return;
  }

  // user has specified a single specific channel. Use it for the channel list.
  if (PopNwkIsValidChannel(iChannel))
    iChannelList = (1 << (uint8_t)(iChannel - gPopNwkFirstChannel_c));

  // just pick first bit in channnel list for the starting channel
  else
    iChannel = PopNwkGetFirstChannel(iChannelList, 0);

  // set the channel to the specified channel
  PopNwkSetChannel(iChannel);
  PopNwkSetChannelList(iChannelList);
}

/*
  PopNwkFormNetwork [Public Command]

  Forms a network on the given channel (or global channel list if channel is 0).

  iOptions define how the form will behave.
  iChannel may be a valid channel or 0 to use the global channel list.
  iPanId may be a valid Pan ID, or 0xffff to indicate form on a random Pan ID (that doesn't conflict with any PANs seen in the scan).

  Returns the form status via gPopEvtNwkStartConfirm_c. Possible status codes:
  gPopStatusBadParms_c              // one or more parameters were invalid
  gPopStatusBusy_c                  // already in a form/join process
  gPopStatusFormSuccess_c           // formed the network. NwkAddr now = 0x0000
  gPopStatusSilentStart_c           // silently formed the network (PAN ID, channel and NwkAddr preset)
  gPopStatusAlreadyOnTheNetwork_c   // can't form, already on a network
  gPopStatusPanAlreadyExists_c      // can't form, as this PAN ID already exists on one or more channels
*/
void PopNwkFormNetwork
(
  popNwkJoinOpts_t iOptions,  // The flag to indicates if the forming should be announced.
  popChannel_t iChannel,      // The channel where to form the network. 0=use global channel list.
  popPanId_t iPanId           // PAN ID for network (0xffff = random)
)
{

  // don't touch state machine globals if already in state machine
  if(!PopNwkIsJoiningOrForming())
  {
    // indicate PopNwkJoinNetwork() started the state machine
    giPopNwkStateCause   = gPopNwkCauseForm_c;
    giPopNwkStateParent  = gPopNwkAnyParentAddr_c;

    // silent form should not send a beacon request either, add that option
    if(iOptions  & gPopNwkJoinOptsSilent_c)
      iOptions |= gPopNwkJoinOptsPassiveScan_c;

    // record the basic parameters for state machine
    PopNwkSetJoinOptions(iOptions);
    PopNwkSetPanId(iPanId);
    PopNwkSetChannelFromInput(iChannel);
  }

  // validate parameters and perhaps start up state machine
  PopNwkValidateAndStartStateMachine();
}

/*
  PopNwkJoinNetwork [Public Command]

  This service joins a network on the given channel.

  IN: iOptions determine whether to get addresses after joining, or whether to to send beacon at the end
  IN: iChannel: The channel where is the network to which we will join. Use 0 for the global channel list
  IN: iPanId: The PAN ID for the network to which we will join. Use 0xffff to mean any.
  IN: iNwkAddr: The address of a specific node to join to. Use 0xffff if don't care.

  Returns nothing.
*/
void PopNwkJoinNetwork
(
  popNwkJoinOpts_t  iOptions,   // in: options
  popChannel_t      iChannel,   // in: channel
  popPanId_t        iPanId,     // in: PAN identifier
  popNwkAddr_t      iNwkAddr    // in: address of node to join
)
{
  // don't touch state machine globals if already in state machine
  if(!PopNwkIsJoiningOrForming())
  {
    // indicate PopNwkJoinNetwork() started the state machine
    giPopNwkStateCause   = gPopNwkCauseJoin_c;
    giPopNwkStateParent   = iNwkAddr;

    // record the basic parameters for state machine
    PopNwkSetJoinOptions(iOptions);
    PopNwkSetPanId(iPanId);
    PopNwkSetChannelFromInput(iChannel);
  }

  // validate parameters and perhaps start up state machine
  PopNwkValidateAndStartStateMachine();
}


/*
  PopNwkStartNetwork

  This function can be called from the application to form or join the network. See the
  PopNet Developer's Guide for a complete description of its behavior.

  It relies on the all the fields from the global gsPopNwkData.

  The results will be returned in gPopEvtNwkStartConfirm_c
*/
void PopNwkStartNetwork( void )
{
  // don't touch state machine globals if already in state machine
  if(!PopNwkIsJoiningOrForming())
  {
    // indicate PopNwkStartNetwork() started the state machine
    giPopNwkStateCause   = gPopNwkCauseStart_c;
    giPopNwkStateParent  = gPopNwkAnyParentAddr_c;

    // no need to set PAN ID, join options, they are already set.
    PopNwkSetChannelFromInput(PopNwkGetChannel());
  }

  // validate parameters and perhaps start up state machine
  PopNwkValidateAndStartStateMachine();
}

/*
  PopNwkGetFirstChannel [public function]

  Given a channel list, return the first channel found. Returns channel 11 if
  channel list is empty.

  Returns channel 11-26, or 0 if no channel bits.
*/
popChannel_t PopNwkGetFirstChannel(popChannelList_t iChannelList, popChannel_t iStartChannel)
{
  uint8_t i;
  uint8_t iStartBit;

  // if out of range, set start channel to the beginning
  if(iStartChannel < gPopNwkFirstChannel_c)
    iStartChannel = gPopNwkFirstChannel_c;

  // find the next channel bit
  iStartBit = iStartChannel - gPopNwkFirstChannel_c;
  for(i = iStartBit; i < gPopNwkNumOfChannels_c; ++i)
  {
    if(iChannelList & (1 << i))
      return gPopNwkFirstChannel_c + i;
  }

  // didn't find a channel
  return 0;
}

/*
  PopNwkCheckForPanIdConflict
  Always uses the results of the PopNwkScanForNetworks() (from an internal global).

  Returns true if the scan includes this PAN ID.
*/
bool PopNwkCheckForPanIdConflict(popPanId_t iPanId)
{
  uint8_t    i;
  sPopNwkBeaconIndication_t *pBeaconList;

  // no table, no conflict
  if(!gpPopNwkDiscoveryTable)
    return false;

  // check if it's in the list
  pBeaconList = gpPopNwkDiscoveryTable->aBeaconList;
  for(i=0; i<gpPopNwkDiscoveryTable->iNumberOfBeacons; ++i)
  {
    if(pBeaconList[i].iPanId == iPanId)
      break;
  }
  return (i < gpPopNwkDiscoveryTable->iNumberOfBeacons);
}

/*
  PopNwkGetNoConflictRandomPanId

  Returns a PAN ID that's not already in the list as reported by the scan confirm.
*/
popPanId_t PopNwkGetNoConflictRandomPanId(void)
{
  popPanId_t iPanId;

  // since the list is significantly smaller than 0xfff1 entries,
  // forever is ok to will pick a random one
  while(giPopForever)
  {
    // pick a PAN ID 0x0000 - 0xfff0
    iPanId = (PopRandom16() % gPopNwkReservedPan_c);

    // no PAN ID conflict? then we've found a good one
    if(!PopNwkCheckForPanIdConflict(iPanId))
      break;
  }

  return iPanId;
}

/*
  Find the best channel on which to form the network.

  Assumes both PopNwkScanForNetworks() and PopNwkScanForNoise() have already been called,
  and that the global gpPopNwkDiscovery.

  It uses the following inputs:
  1. gpPopNwkDiscoveryTable    - this was allocated by PopNwkScanForNetworks()
  2. gsPopNwkScanForNoise      - this was filled in by PopNwkScanForNoise()
  3. gsPopNwkData.iChannelList - this was set by the user to their preferred channel list

  Chooses
  1. The quietest channel with no networks on it.
  2. if all channels have networks on them, then the least noisy channel in active scan.
*/
popChannel_t PopNwkFindBestChannel(void)
{
  uint8_t i;
  uint8_t iLqiIndex;
  popChannelList_t iEmptyChannels;
  popChannelList_t iChannelBit;
  sPopNwkBeaconIndication_t *pBeaconList;

  // the channel list should have been set prior to this, but if there is
  // no channel list just use the first channel
  if(!PopNwkGetChannelList())
  {
    return gPopNwkFirstChannel_c;
  }

  // look on all the possible channels the user specified for an empty channel
  iEmptyChannels = PopNwkGetChannelList();

  // only use the network discovery table (beacons) if available)
  if(gpPopNwkDiscoveryTable)
  {
    // check for best LQI & empty channels
    iLqiIndex = 0xff;
    pBeaconList = gpPopNwkDiscoveryTable->aBeaconList;
    for(i = 0; i < gpPopNwkDiscoveryTable->iNumberOfBeacons; i++)
    {
      // turn off this channel as a potential (empty) channel
      iChannelBit = ((popChannel_t)1 << (uint8_t)(pBeaconList[i].iChannel - gPopNwkFirstChannel_c));
      iEmptyChannels &= (~iChannelBit);

      // find the quietest (least strong) LQI. The LQI # gets smaller as signal is stronger.
      if(iLqiIndex == 0xff)
        iLqiIndex = i;
      if(pBeaconList[i].iLqi > pBeaconList[iLqiIndex].iLqi)
        iLqiIndex = i;
    }

    // at this point, either there are NO empty channels, in which case we have an LQI index
    // or there are one or more empty channels, in which case we'll find the quietest one

    // no empty channels, just use quietest LQI (there MUST be an LQI if there are no empty channels)
    if(!iEmptyChannels)
    {
      return pBeaconList[iLqiIndex].iChannel;
    }
  }

  // get the first empty channel
  iLqiIndex = PopNwkGetFirstChannel(iEmptyChannels, 0) - gPopNwkFirstChannel_c;

  // choose quietest channel of the empty ones
  for(i=0; i < gPopNwkNumOfChannels_c; ++i)
  {
    // if the channel is an empty one, and the noise is less, use it
    if( ((1 << i) & iEmptyChannels) &&
      gsPopNwkScanForNoise.aEnergyLevels[i] < gsPopNwkScanForNoise.aEnergyLevels[iLqiIndex] )
      iLqiIndex = i;
  }

  return (popChannel_t)(iLqiIndex + gPopNwkFirstChannel_c);
}

/*
  Look through the scan confirm for a suitable parent to join. Does not actually join the parent!

  Checks:
  1. Is there a node with the proper PopNet protocol?
  2. Does parent have the right PAN ID?
  3. Does parent have the right Mfg ID?
  4. Is join enabled?

  Inputs:
    pScanConfirm    from gPopEvtNwkScanConfirm_c

  Outputs:
    piJoinIndex     index in scan confirm of suitable parent

  Returns
    gPopStatusJoinSuccess_c         Found a suitable parent to join (but haven't joined yet)
    gPopStatusJoinDisabled_c        Join disabled on all suitable parents
    gPopStatusWrongMfgId_c          Wrong Mfg ID on all suitable parents
    gPopStatusPanAlreadyExists_c    Same PAN ID exists on another protocol (could be ZigBee, etc...)
    gPopStatusNoParent_c            No suitable parent found
*/
popStatus_t PopNwkFindSuitableParentToJoin
  (
  uint8_t *piJoinIndex
  )
{
  uint8_t          i;
  uint8_t          iBestOne;        // found a parent (best one so far)
  popLqi_t         iBestLqi;        // to help pick best LQI
  popStatus_t      iStatus;         // status returned
  bool           fJoinDisabled;   //
  bool           fMfgIdWrong;
  bool           fOtherNetwork;
  sPopNwkBeaconIndication_t *pBeaconList;

  // assume we're able to join
  iStatus = gPopStatusJoinSuccess_c;

  if (!gpPopNwkDiscoveryTable)
  {
    return gPopStatusNoMem_c;
  }

  // search along the table for a parent to join.
  pBeaconList = (void *)gpPopNwkDiscoveryTable->aBeaconList;
  fJoinDisabled = fMfgIdWrong = fOtherNetwork = false;
  iBestOne = 0xff;          // indicate not found yet
  iBestLqi = 0xff;          // make sure to pick best LQI (lower LQI is better)
  for(i = 0; i < gpPopNwkDiscoveryTable->iNumberOfBeacons; i++)
  {
    /*
      if giPopNwkStateCause is gPopNwkCauseJoin_c that means that user wants to join to a specific parent and not any.
      However, we have an exception, here, becaue PopAppTracking uses PopNwkJoinNetwork to join nodes to any parent.
    */
    if( giPopNwkStateCause == gPopNwkCauseJoin_c )
      if( giPopNwkStateParent != gPopNwkAnyParentAddr_c ) /* Needed for PopAppTracking */
        if( giPopNwkStateParent != pBeaconList[i].iNwkAddr )
          continue;

    // don't join unless the right pan ID (or joining any PAN ID)
    if(!PopNwkIsRandomPanId(PopNwkGetPanId()) && pBeaconList[i].iPanId != PopNwkGetPanId())
      continue;

    // not PopNet, so we can't join
    if(!PopNwkIsValidProtocol(pBeaconList[i].iProtocol))
    {
      // if not using a random PAN ID, don't allow forming or joining on this PAN ID
      // if ZigBee or some other protocol exists.
      // remember, ZigBee or PopNet may change channels.
      if(!PopNwkIsRandomPanId(PopNwkGetPanId()))
        fOtherNetwork = true;
      continue;
    }

    // not the right PopNet protocol version, so we can't join
    if(!PopNwkIsValidProtocolVer(pBeaconList[i].iProtocolVer))
      continue;

    // only consider joining if join enabled
    if(!PopNwkIsJoiningEnabled(pBeaconList[i].iProtocolVer))
    {
      fJoinDisabled = true;
      continue;
    }

    // only consider joining if node matches manufacturer ID (0xffff means all)
    if(PopNwkGetManufacturerId() != 0xffff &&
        pBeaconList[i].iMfgId != PopNwkGetManufacturerId())
    {
      fMfgIdWrong = true;
      continue;
    }

    // only pick this one if it has a better LQI. Low LQI = better.
    // assumes that LQI will be something other than 0xff for the best one.
    else if(pBeaconList[i].iLqi >= iBestLqi)
      continue;

    // this is the best one (best LQI, PopNet, joining enabled, right PAN, etc...)
    iBestOne = i;
  }

  // found a suitable parent
  if(iBestOne != 0xff)
    *piJoinIndex = iBestOne;

  // didn't find suitable parent, return proper error code
  else
  {
    if(fJoinDisabled)
      iStatus = gPopStatusJoinDisabled_c;
    else if(fMfgIdWrong)
      iStatus = gPopStatusWrongMfgId_c;
    else if(fOtherNetwork)
      iStatus = gPopStatusPanAlreadyExists_c;
    else
      iStatus = gPopStatusNoParent_c;
  }

  // return status
  return iStatus;
}

/*
  PopNwkProcessAssociationFrame

  Processes an incoming association frame, either request or response.

  returns true if processed, false if not an association frame.
*/
bool PopNwkProcessAssociationFrame(sPopDataFrame_t *pOtaFrame)
{
  sPopNwkAssociationReq_t *pAssociationReq;
  sPopNwkAssociationRsp_t *pAssociationRsp;
  popPanId_t    iPanId;
  popNwkAddr_t  iNwkAddr;

  // if not from PopNet, ignore it
  pAssociationReq = (void *)pOtaFrame;
  if(pAssociationReq->iSequence != 0x50)
    return false;

  // another node is requesting to join this node
  if(pAssociationReq->iFrameControl == mPopNwkAssociationReq.iFrameControl)
  {
    // same pan?
    iPanId = pAssociationReq->iPanId;
    OtaToNative16(iPanId);
    if(iPanId != PopNwkGetPanId())
      return false;

    // same node addr?
    iNwkAddr = pAssociationReq->iDstAddr;
    OtaToNative16(iNwkAddr);
    if(iNwkAddr != PopNwkGetNodeAddr())
      return false;

    // correct command?
    if(pAssociationReq->iCmd != mPopNwkAssociationReq.iCmd)
      return false;

    // make the reply
    PopNwkMgmtJoinResponse(pAssociationReq->aSrcIeeeAddr);
    return true;
  }

  // assocation response back to this node.
  if(pAssociationReq->iFrameControl == mPopNwkAssociationRsp.iFrameControl)
  {
    // correct command?
    pAssociationRsp = (void *)pOtaFrame;
    if(pAssociationRsp->iCmd != mPopNwkAssociationRsp.iCmd)
      return false;

    // for our node?
    if(!PopNwkIsEqual8Bytes(pAssociationRsp->aDstIeeeAddr, PopNwkGetMacAddrPtr()))
      return false;

    // record the management
    iNwkAddr = pAssociationRsp->iNewAddr;
    OtaToNative16(iNwkAddr);
    PopNwkMgmtProcessJoinRsp(iNwkAddr);
    return true;
  }

  // we processed it
  return false;
}

/*
  PopNwkSendAssociationRequest

  Send out the association request
*/
void PopNwkSendAssociationRequest(popPanId_t iPanId, popNwkAddr_t iDstAddr)
{
  // send the assciation request
  mPopNwkAssociationReq.iPanId = iPanId;
  mPopNwkAssociationReq.iDstAddr = iDstAddr;
  NativeToOta16(mPopNwkAssociationReq.iPanId);
  NativeToOta16(mPopNwkAssociationReq.iDstAddr);
  PopNwkGetMacAddr(mPopNwkAssociationReq.aSrcIeeeAddr);

  // send the packet out the radio
  PopPhyDataRequest(&mPopNwkAssociationReq, sizeof(mPopNwkAssociationReq));

  // go back to receive immediately
  PopNwkReceive();
}


/*
  PopNwkSendAssociationResponse

  Got the request, send the response.
*/
void PopNwkSendAssociationResponse(aPopMacAddr_t aDstIeeeAddr, popNwkAddr_t iNewAddr)
{
  PopNwkGetMacAddr(mPopNwkAssociationRsp.aSrcIeeeAddr);
  PopMemCpy(mPopNwkAssociationRsp.aDstIeeeAddr, aDstIeeeAddr, sizeof(aPopMacAddr_t));
  mPopNwkAssociationRsp.iPanId = PopNwkGetPanId();
  NativeToOta16(mPopNwkAssociationRsp.iPanId);
  mPopNwkAssociationRsp.iNewAddr = iNewAddr;
  NativeToOta16(mPopNwkAssociationRsp.iNewAddr);

  // send the packet out the radio
  PopPhyDataRequest(&mPopNwkAssociationRsp, sizeof(mPopNwkAssociationRsp));

  // go back to receive immediately
  PopNwkReceive();
}

/*******************************************************************************
* Security Functions.
********************************************************************************/

/*
  Apply light security to the frame.
*/
void PopNwkLiteSecureData(sPopNwkDataIndication_t *pOtaFrame)
{
  /* Calculate the exact size for payload tobe encrypted. */
  uint8_t  iLength = (pOtaFrame->sPre.iFrameLen - PopSizeOfMember(sPopDataFrame_t, sMac));

  /* A pointer to the place where the Auxiliar data must go. */
  sNwkLiteAuxFrame_t  *pAuxFrame;

  /* The value for the transaction MIC. */
  popNwkLiteMic_t  iMic;

  /* The length  of the payload excludes the Auxiliar Frame. */
  iLength -= giPopNwkAuxFrameSize;

  /* cypher the payload and get the proper MIC */
  iMic = PopNwkApplyLiteSecurity(iLength, &pOtaFrame->sFrame.sNwk);

  /* The function should place the Auxiliar data at the end of the payload */
  pAuxFrame = (void *)((uint8_t *)&pOtaFrame->sFrame.sNwk + iLength);

  /* Set the curretn MIC */
  pAuxFrame->iMic = iMic;

  /* Set the out going secure counter. */
  pAuxFrame->iCounter = PopNwkGetLiteCounter();

  // Increment the counters by one every time we encript a packet.
  PopNwkIncreaseLiteCounter(1);

  /* Little to Big endian because HCS08 manage the mic in big endian */
  NativeToGateway16((popNwkLiteMic_t*)&pAuxFrame->iMic);
  NativeToGateway16((popNwkLiteCounter_t*)&pAuxFrame->iCounter);
}


/*
  Apply AES 128-bit security to the frame.
*/
void PopNwkAESSecureData(sPopNwkDataIndication_t *pOtaFrame)
{
  /* Calculate the exact size for payload tobe encrypted. */
  uint8_t  iLength = (pOtaFrame->sPre.iFrameLen - PopSizeOfMember(sPopDataFrame_t, sMac));

  /* A pointer to the place where the Auxiliar data must go. */
  sNwkAESAuxFrame_t  *pAuxFrame;

  /* The value for the transaction MIC. */
  popNwkAESMic_t  iMic;

  /* The length  of the payload excludes the Auxiliar Frame. */
  iLength -= giPopNwkAuxFrameSize;

  iMic = PopNwkApplyAESSecurity(iLength, &pOtaFrame->sFrame.sNwk);

  /* The function should place the Auxiliar data at the end of the payload */
  pAuxFrame = (void *)((uint8_t *)&pOtaFrame->sFrame.sNwk + iLength);

  /* Set the curretn MIC */
  pAuxFrame->iMic = iMic;

  /* Set the sending IEEE address. */
  PopNwkGetMacAddr(pAuxFrame->aIeeeAddr);

  /* Set the out going secure counter. */
  pAuxFrame->iCounter =  PopNwkGetAESCounter();

  // Increment the counters by one everytime we encrypt a packet.
  PopNwkIncreaseAESCounter(1);

  /* Due to HCS08 send OTA the MIC in Big endian it needs to change to little endian (just devices little endian)  */
  NativeToGateway32((popNwkAESMic_t*)&pAuxFrame->iMic);
  NativeToGateway32((popNwkAESCounter_t*)&pAuxFrame->iCounter);
}

void PopNwkLiteCypherKey(aPopNwkKey_t aKey, popNwkLiteMic_t  *pMic, popNwkLiteCounter_t iCounter)
{
  /* The for loop counter. */
  uint8_t i;

  /* The Xor value for a particular iteration. */
  uint8_t iXor;

  /* Cypher the key on a inter depended way. */
  for (i = 0; i < 16; i++)
  {
    /* Calculates the xor for each memeber of the key. */
    iXor = (uint8_t)(iCounter ^ (PopNwkGetSecurityKeyPtr()[i] ^ miLitePrime));

    if (!i)
      /* The first element does not has a previous element to be interdependent. */
      aKey[i] = iXor;
    else
      /* If the current element is not the first one cypher it using the previous one. */
      aKey[i] = (aKey[(i - 1)] ^ iXor);

    /* Use the cypher key to generate part of the MIC. */
    *pMic += aKey[i];
  }
}

void PopNwkGenerateMIC(uint8_t *pPayload, uint8_t iLength, popNwkLiteMic_t *pMic)
{
  uint8_t i;

  for (i = 0; i < iLength; i++)
    *pMic += pPayload[i];

  *pMic ^= 0xffff;
}

popNwkAESMic_t PopNwkGenerateAESMIC(uint8_t *pPayload, uint8_t iLength, uint8_t *pKey)
{
  uint8_t i;

  uint8_t iKeyElements = PopNumElements(PopNwkGetSecurityKeyPtr());

  popNwkAESMic_t iMic = 0;

  /*  */
  for (i = 0; i < iKeyElements; i++)
    iMic += pKey[i];

  for (i = 0; i < iLength; i++)
    iMic += pPayload[i];

  return (iMic ^ 0xffffffff);
}

/*
  PopNwkApplyLiteSecurity

  Applys the scheme of "Lite" security toa specific Frame. This function is
  divided in two modeules, the first one cyphers the key to be use, and the
  second one, cypher the whole payload using the cyphered key and calculates
  the MIC.

  Recieves the lenght of the payload to encrypt.

  Returns the MIC for this particular transaction.
*/
popNwkLiteMic_t PopNwkApplyLiteSecurity(uint8_t iLength, sNwkDataFrame_t *pNwkFrame)
{
  /* tmpKey is consider a temporary empty place to store the cyphered key. */
  aPopNwkKey_t aTmpKey;

  uint8_t iKeyElements = PopNumElements(PopNwkGetSecurityKeyPtr());

  /* A couple of loop counters. */
  uint8_t i;
  uint8_t j;

  /* The MIC or check sum for the message integrity check (2 bytes long). */
  popNwkLiteMic_t  iMic = 0;

  /* The pointer to walk through every single byte on the payload. */
  uint8_t *pPtr = (void *)pNwkFrame;

  /* Get the security key cypher, is needed to encrypt the payload. */
  PopNwkLiteCypherKey(aTmpKey, &iMic, PopNwkGetLiteCounter());//guPopNwkSecureCounters.iPopLiteCounter);

  /* Cypher the whole payload here. */
  for (i = 0; i < iLength; i++)
  {
    /* If the current element is not the first one cypher it using the previous one. */
    if (i)
      pPtr[i] = pPtr[i] ^ pPtr[i-1];

    /* Cypher the data with every single byte in the cypher key. */
    for (j = 0; j < iKeyElements; j++)
        pPtr[i] = pPtr[i] ^ aTmpKey[j];
  }

  /* Calculate the MIC to verify the message intergity. */
  PopNwkGenerateMIC((void *)(pPtr - sizeof(sMacDataFrame_t)),
                    iLength + sizeof(sMacDataFrame_t), &iMic);

  /* Return the complement of the check sum as the MIC. */
  return (iMic);

}

/*
  PopNwkLiteRemoveSecurity
*/
bool PopNwkLiteRemoveSecurity(sPopNwkDataIndication_t *pOtaFrame)
{
  /* Loop counters. */
  uint8_t i;
  uint8_t j;

  /* The elements of the current key. */
  uint8_t iKeyElements = PopNumElements(PopNwkGetSecurityKeyPtr());

  /* A pointer to the Auxiliar Frame (MIC + Counter) */
  sNwkLiteAuxFrame_t *pAuxFrame;

  /* The MIC to compare. */
  popNwkLiteMic_t iMic = 0;

  /* The Key to use during the decypher procedure.*/
  aPopNwkKey_t  aKey;

  /* A pointer to the payload to be decrypted. */
  uint8_t  *pPayload;

  /* The full payload to be decrypted. */
  uint8_t iPayLoadSize = (pOtaFrame->sPre.iFrameLen - PopSizeOfMember(sPopDataFrame_t, sMac));

  /* Exclude the size of the Auxiliar header from the total amount of byte to be
    decrypted. */
  iPayLoadSize -= giPopNwkAuxFrameSize;

  /* Point to the auxiliar header at the end of the real payload */
  pAuxFrame = (void *)(((uint8_t *)&pOtaFrame->sFrame.sNwk) + iPayLoadSize);

  /* Big endian to Little endian because HCS08 send OTA the mic as big endian */
  GatewayToNative16((popNwkLiteMic_t*)&pAuxFrame->iMic);
  GatewayToNative16((popNwkLiteCounter_t*)&pAuxFrame->iCounter);

  /* Pointer to the end of the payload. One byte before the Auxiliar header is where the
     payload ends. */
  pPayload = ((uint8_t *)&pOtaFrame->sFrame.sNwk);

  /* Get the cyphered key to be used during the decryption process. */
  PopNwkLiteCypherKey(aKey, &iMic, pAuxFrame->iCounter);

  /* Calculate the MIC to verify the message intergity. */
  PopNwkGenerateMIC((pPayload - sizeof(sMacDataFrame_t)),
                    (iPayLoadSize + sizeof(sMacDataFrame_t)),
                    &iMic);

  /* Condition of error: if the incoming MIC is different from the generated MIC, then,
     the packet must be rejected. The process of checking of the integrity of the message
     becomes in two steps procedure, the first one is when the key gets encrypted using
     its own bytes, and, The second one is add to the MIC each byte on the incoming payload.
     At the end a simple comparison between the MIC is enough to verify the integrity of
     the message. */
  if (pAuxFrame->iMic != iMic)
    return false;

  /* For each byte in the payload to be decrypted. */
  i = iPayLoadSize;
  while (i != 0)
  {
    i--;

    /* For each byte in the payload with every single byte in the key. */
    j = iKeyElements;
    while (j != 0)
    {
      j--;
      pPayload[i] = pPayload[i] ^ aKey[j];
    }

    /* If the current byte is not the first one then also decypher with the previous one. */
    if (i)
      pPayload[i] = pPayload[i] ^ pPayload[i - 1];
  }

  /* Reduce the size of the paylod extracting the size of the Auxiliar Frame. */
  pOtaFrame->sPre.iFrameLen -= giPopNwkAuxFrameSize;

  /* everything is ok, security has been removed or the payload decrypted. */
  return true;
}


/*
  Apply AES security to the frame. Includes both encyption and authentication.
*/
popNwkAESMic_t PopNwkApplyAESSecurity(uint8_t iLength, sNwkDataFrame_t *pNwkFrame)
{
  /* tmpKey is consider a temporary empty place to store the cyphered key. */
  aPopNwkKey_t aTmpKey;

  uint8_t iKeyElements = PopNumElements(PopNwkGetSecurityKeyPtr());

  /* A couple of loop counters. */
  uint8_t i;
  uint8_t j;

  /* The MIC or check sum for the message integrity check (2 bytes long). */
  popNwkAESMic_t  iMic = 0;

  /* 128 bits unique data block. */
  sNwkAESNonce_t  sNonce;

  /* The pointer to walk through every single byte on the payload. */
  uint8_t *pPtr = (void *)pNwkFrame;

  /* Get the Ieee Address of the sending node. */
  PopNwkGetMacAddr(sNonce.aIeeeAddr);

  /* Set the local Frame counter as a unique value to encrypt the key. */
  sNonce.iCounter = PopNwkGetAESCounter();//guPopNwkSecureCounters.iPopAESCounter;

  /* Set the private prime number as a value for the  */
  sNonce.iPrime = miAESPrime;

  /* We need to swap the AES prime to use equals to HCS08. if it isn't used equals then the temp key are
  generated wrong due to endianess when the PopNwkAes fills the state array. Furthermore, here, we need to
  swap the AES prime number.*/
  NativeToGateway32((popNwkAESCounter_t*)&sNonce.iCounter);
  NativeToGateway32((popAESPrime_t*)&sNonce.iPrime);

  /* Get the security key cypher, is needed to encrypt the payload. */
  PopNwkAES((void *)&sNonce, aTmpKey);

  /* Cypher the whole payload here. */
  for (i = 0; i < iLength; i++)
  {
    /* If the current element is not the first one cypher it using the previous one. */
    if (i)
      pPtr[i] = pPtr[i] ^ pPtr[i-1];

    /* Cypher the data with every single byte in the cypher key. */
    for (j = 0; j < iKeyElements; j++)
      pPtr[i] = pPtr[i] ^ aTmpKey[j];
  }

  /* Calculate the MIC to verify the message intergity. */
  //PopNwkGenerateMIC((void *)(pPtr - sizeof(sMacDataFrame_t)),
                    //iLength + sizeof(sMacDataFrame_t), &iMic);

  iMic = PopNwkGenerateAESMIC((pPtr - sizeof(sMacDataFrame_t)),
                              (iLength + sizeof(sMacDataFrame_t)),
                              aTmpKey);

  /* Return the complement of the check sum as the MIC. */
  return (iMic);

}

/*
  Remove AES security from the incoming frame. Includes both encyption and authentication.
*/
bool PopNwkAESRemoveSecurity(sPopNwkDataIndication_t *pOtaFrame)
{
  /* Loop counters. */
  uint8_t i;
  uint8_t j;

  /* The size ofthe current key. */
  uint8_t iKeyElements = PopNumElements(PopNwkGetSecurityKeyPtr());

  /* A pointer to the Auxiliar Frame (MIC + Counter) */
  sNwkAESAuxFrame_t *pAuxFrame;

  /* The MIC to compare. */
  popNwkAESMic_t iMic = 0;

  /* 128 bits unique data block. */
  sNwkAESNonce_t  sNonce;

  /* The Key to use during the decypher procedure.*/
  aPopNwkKey_t  aTmpKey;

  /* A pointer to the payload to be decrypted. */
  uint8_t  *pPayload;

  /* The full payload to be decrypted. */
  uint8_t iPayLoadSize = (pOtaFrame->sPre.iFrameLen - PopSizeOfMember(sPopDataFrame_t, sMac));

  /* Exclude the size of the Auxiliar header from the total amount of byte to be
    decrypted. */
  iPayLoadSize -= giPopNwkAuxFrameSize;

  /* Point to the auxiliar header at the end of the real payload */
  pAuxFrame = (void *)(((uint8_t *)&pOtaFrame->sFrame.sNwk) + iPayLoadSize);

  /* Pointer to the end of the payload. One byte before the Auxiliar header is where the
     payload ends. */
  pPayload = ((uint8_t *)&pOtaFrame->sFrame.sNwk);

  sNonce.iCounter = pAuxFrame->iCounter;

  sNonce.iPrime = miAESPrime;

  PopMemCpy(sNonce.aIeeeAddr, pAuxFrame->aIeeeAddr, sizeof(sNonce.aIeeeAddr));

  /* We need to swap the AES prime to use equals to HCS08. if it isn't used equals then the temp key are
  generated wrong due to endianess when the PopNwkAes fills the state array */
  NativeToGateway32((popAESPrime_t*)&sNonce.iPrime);

  /* Get the security key cypher, is needed to encrypt the payload. */
  PopNwkAES((void *)&sNonce, aTmpKey);

  iMic = PopNwkGenerateAESMIC((pPayload - sizeof(sMacDataFrame_t)),
                              (iPayLoadSize + sizeof(sMacDataFrame_t)), aTmpKey);

  /* Due to HCS08 send OTA the MIC in Big endian it needs to change to little endian (just devices little endian)  */
  NativeToGateway32((popNwkAESMic_t*)&iMic);

  /* Condition of error: if the incoming MIC is different from the generated MIC, then,
     the packet must be rejected. The process of checking of the integrity of the message
     becomes in two steps procedure, the first one is when the key gets encrypted using
     its own bytes, and, The second one is add to the MIC each byte on the incoming payload.
     At the end a simple comparison between the MIC is enough to verify the integrity of
     the message. */
  if (pAuxFrame->iMic != iMic)
    return false;

  /* For each byte in the payload to be decrypted. */
  i = iPayLoadSize;
  while (i != 0)
  {
    i--;

    /* For each byte in the payload with every single byte in the key. */
    j = iKeyElements;
    while (j != 0)
    {
      j--;
      pPayload[i] = pPayload[i] ^ aTmpKey[j];
    }

    /* If the current byte is not the first one then also decypher with the previous one. */
    if (i)
      pPayload[i] = pPayload[i] ^ pPayload[i - 1];
  }

  /* Reduce the size of the paylod extracting the size of the Auxiliar Frame. */
  pOtaFrame->sPre.iFrameLen -= giPopNwkAuxFrameSize;

  /* everything is ok, security has been removed or the payload decrypted. */
  return true;
}

/*
  PopNwkCheckNeighborTable

  Check the neighbor table for staleness. Returns false if stale, true if not.
*/
bool PopNwkCheckNeighborTable(popNwkAddr_t iMacDstAddr, popNwkAddr_t iNwkAddr, popLargeCounter_t iCounter)
{
  sNwkNeighborTable_t *pNeighborTable;
  uint8_t iIndex;

  /* If the Table is empty, then add this node into it and exit with success. */
  if (!miNeighborEntry)
  {
    /* Point to the current entry in the  */
    pNeighborTable = &gaNeighborTable[miNeighborEntry];

    /* Set this neighbor as the newest one. */
    pNeighborTable->iAge = 1;

    /* Set the address of the neighbor into the table. (the neighbor's source address) */
    pNeighborTable->iNwkAddr = iNwkAddr;

    /* The current counter of the newest neighbor. */
    pNeighborTable->iCounter = iCounter;

    /* Got one entry less to use. Table no longer empty */
    miNeighborEntry++;

    return true;
  }

  /* Search for the device into the neighbor table. */
  iIndex = PopNwkSearchNeighbor(iNwkAddr);

  /* The neigbor thable is not full. */
  if (miNeighborEntry < giPopNwkNeighborEntries)
  {
    /* If the neighbor table is not empty but the device was not found. */
    if (iIndex == gPopErrDeviceNotFound_c)
      /* Add the device to the current  */
      return PopNwkAddNewNeighbor(iNwkAddr, iCounter);
  }

  if (iIndex != gPopErrDeviceNotFound_c)
    /* The device is already in the table, we need to update it.*/
    return PopNwkUpdateNeighbor(iIndex, iCounter);

  /* If the device is not inthe table, and the message is not for me then throw  the packet. */
  if ((iMacDstAddr != PopNwkGetNodeAddr()) && (iMacDstAddr != gPopNwkBroadcastAddr_c))
    return true;  // Leave the function, do not add the node to the table but avoid throuwing the packet.

  /* neighbor table is full, replace it */
  return PopNwkReplaceOldNeighbor(iNwkAddr, iCounter);
}


/*
  Search the neighbor table
*/
uint8_t PopNwkSearchNeighbor(popNwkAddr_t iNwkAddr)
{
  uint8_t i;
  sNwkNeighborTable_t  *pEntry;

  for (i = 0; i < giPopNwkNeighborEntries; i++)
  {
    pEntry = &gaNeighborTable[i];

    if (pEntry->iAge && (iNwkAddr == pEntry->iNwkAddr))
      return i;
  }

  return gPopErrDeviceNotFound_c;
}

/*
  PopNwkAddNewNeighbor

  Add a new neigbor to the table
*/
bool PopNwkAddNewNeighbor(popNwkAddr_t iNwkAddr, popLargeCounter_t iCounter)
{
  sNwkNeighborTable_t *pEntry;
  uint8_t i;

  for (i = 0; i < giPopNwkNeighborEntries; i++)
  {
    pEntry = &gaNeighborTable[i];

    if (i == miNeighborEntry)
      continue;

    if (pEntry->iAge)
      pEntry->iAge++;
  }

  pEntry = &gaNeighborTable[miNeighborEntry];
  pEntry->iAge = 1;
  pEntry->iCounter = iCounter;
  pEntry->iNwkAddr = iNwkAddr;

  miNeighborEntry++;

  return true;
}

bool PopNwkUpdateNeighbor(uint8_t  iIndex, popLargeCounter_t iCounter)
{
  popIntLargeCounter_t  iDifference = 0;
  sNwkNeighborTable_t  *pEntry;
  sNwkNeighborTable_t  *pNeigborTable;
  uint8_t i;

  pEntry = &gaNeighborTable[iIndex];

  iDifference = (popIntLargeCounter_t)iCounter - (popIntLargeCounter_t)pEntry->iCounter;

  if (giPopSecurityLevel == gPopNwkLiteSecurity_c)
  {
    // For lite security take only the two less significant bytes.
    iDifference &= gPopLiteSecurityMask_c;
    if (iDifference > gPopNwkLiteLimit_c)
      return false;  // Reject the packet
  }

  if (giPopSecurityLevel == gPopNwkAESSecurity_c)
  {
    if (iDifference > gPopNwkAESLimit_c)
      return false;  // Reject the packet
  }

  for (i = 0; i < giPopNwkNeighborEntries; i++)
  {
    if (i == iIndex)
      continue;

    pNeigborTable = &gaNeighborTable[i];

    if (pEntry->iAge < pNeigborTable->iAge)
      continue;

    if (pNeigborTable->iAge)
      pNeigborTable->iAge++;
  }

  pEntry->iAge = 1;
  pEntry->iCounter = iCounter;

  return true;
}

/*
  Replace the counter for this neighbor.
*/
bool PopNwkReplaceOldNeighbor(popNwkAddr_t iNwkAddr, popLargeCounter_t iCounter)
{
  uint8_t i;
  uint8_t iIndex = 0;
  sNwkNeighborTable_t  *pEntry;

  for (i = 0; i < giPopNwkNeighborEntries; i++)
  {
    if (gaNeighborTable[i].iAge > gaNeighborTable[iIndex].iAge)
      iIndex = i;

    gaNeighborTable[i].iAge++;
  }

  pEntry = &gaNeighborTable[iIndex];
  pEntry->iAge = 1;
  pEntry->iCounter = iCounter;
  pEntry->iNwkAddr = iNwkAddr;

  return true;
}


/*
  the number of PHY packets received (for this node or not) since last calling PopPhyPacketCount()
*/
uint32_t PopPhyPacketCount(void)
{
  uint32_t iLastCount = giPopPhyPacketCount;
  giPopPhyPacketCount = 0;
  return iLastCount;
}


/* EOF */

