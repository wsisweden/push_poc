/***************************************************************************
  PopDefault.c
  Copyright (c) 2006-2010 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  This is the default event handler for the application's PopAppTaskEventLoop(). It frees memory on 
  appropriate events an handles unhandled events.

  In particular, it also implements:
  ---------------------------------
  Over-the-Air Upgrades (gPopOverTheAirUpgrades_d)
  Node Placement (gPopNwkNodePlacement_d)
  Manage Address Pool (gPopNwkManageAddrPool_d)
  Gateway (gPopGateway_d)

****************************************************************************/
#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"
#include "PopNwk.h"
#include "PopSerial.h"
#include <stddef.h>

#if DEBUG >= 1
#include "PortConfig.h"
#endif //DEBUG >= 1
// TODO: Clean Up Node Placement Code


/***************************************************************************
  Types & Definitions
***************************************************************************/
#define mPopNwkNodePlacementBlinkRate_c   500 // 500ms
#define mPopAppBlinkCountDownRate_c       300 // 1/3 second, 300 ms
#define mPopMemAllocFailTimeout_c         30000 // 30s

// for communication with NWK layer (must match, see PopNwk.h in full source kit)
#define gPopOtaUpgradeThrottleTimer_c       0x10    // moved to PopNet.h since they are shared with PopDefault.c
#define gPopOtaUpgradeCompleteTimer_c       0x11

// timer IDs used here in PopDefault.c. Note: these timer IDS are then not available to the application
#define gPopTimerIdOtaFwdlMode_c            0xF9
#define gPopTimerIdNodePlaceBlinkLed_c      0xFA
#define gPopTimerIdNodePlaceBeaconReq_c     0xFB
#define gPopTimerIdAppBlink_c               0xFC    // blink at application startup
#define gPopTimerIdLong_c                   0xFD    // long timer
#define gPopTimerIdNwkDataChanged_c         0xFE    // NWK data has been changed, save it

// if no NVM, then disable managing it
#if !gPopNumberOfNvPages_c
 #undef gPopManageNvm_d
 #define gPopManageNvm_d gDisabled_c 
#endif


/***************************************************************************
  Prototypes
***************************************************************************/
void PopHandleDefaultTimer(popTimerId_t iTimerId);

#if gPopNwkNodePlacement_d
void PopNwkNodePlaceProcessBeacon(sPopNwkBeaconIndication_t *pNwkBeaconIndication);
void PopNwkNodePlacementLeds(sPopNodePlacement_t *pPopNodePlacementResult);
#endif

#if gPopGateway_d
void PopGatewayProcessIncomingData(void);
#endif

void PopMemResetOutOfMemoryFlag(void);
void PopNwkReceive(void);

/* helper function to PopStartLongTimer() */
popErr_t PopDecrementLongTimeOut(void);


#if gPopOverTheAirUpgrades_d
/*----------------- OTA Upgrades -----------------------*/
void PopOtaUpgradeSendNextBlock(sPopOtauGwSubseqBlockPassThruSaveReq_t * pNotFirstImageBlock);
void PopOtaUpgradeMessageCreator(uint16_t blockNumber, sPopOtauGwFirstBlockPassThruSaveReq_t * pFirstBlock);
void PopNwkMgmtOtaUpgradeCompleteRequest(sPopGwUpgradeCompleteReq_t* pOtaUpgradeCompleteReq);
void PopOtaUpgradeSendCompleteRspToGw(popOtauCmdId_t RspType,sPopOtaUpgradeCompleteRsp_t  * pOtauCompleteRsp,popNwkAddr_t iSrcAddr);
popErr_t PopOtaUpgradeFirstBlockVerification(sPopOtauGwFirstBlockSaveReq_t  * pFirstImageBlock);
popErr_t PopOtaUpgradePassThruRequest(sPopOtauGwSubseqBlockPassThruSaveReq_t * pNotFirstImageBlock);
bool PopOtaUpgradeIsImageComplete(void);
extern sPopOtaUpgradeImageInfo_t gsPopOtaUpgradeInfo;
extern const uint16_t giPopNwkBroadcastExpired;
extern const popOtauHwId_t giPopHardwareId;
#endif


/***************************************************************************
  Globals & Externs
***************************************************************************/

// for use with the long timer. Only used if long timer used.
popLongTimeOut_t giPopLongTimeOut;
popTimerId_t     giPopLongTimerId;

// used to blink LED1 during application startup
// will only blink up to 15 times
uint8_t miPopAppBlinkCountDown;
bool mfPopAppBlinkLedOn;

// for use with NodePlacement. Only included if NodePlacement is included
uint8_t giPopNodePlaceBars;
sPopNodePlacement_t gsPopNodePlacementResult;
popChannel_t giPopNodePlaceChannel;
bool gfPopNwkInNodePlacementMode;   // not in node-placement to begin with

// OTA FWDL flag. True if receiving ota fwdl save commands
uint8_t otaFwdlModeFlag = false;              //  #JJ added to avoid gateway watchdog restart
uint8_t ispCommanderModeFlag=false;

popTimeOut_t giPopNodePlacementRate_c=1000;
popTimeOut_t giPopBlinkRate;

#if gPopNwkNodePlacement_d
extern const uint8_t gaPopNodePlacementStrongSignal[];
#endif

/* various globals for instrumentation, etc... */
#if (gPopAssertLevel_c >= 2)
extern uint16_t giHeapAddress;
#endif
extern const uint8_t cPopMaxEvents;
extern uint8_t giPopEventsHigWaterMark;  // The biggest # of events registered
extern uint8_t giPopTimerWaterMark;
extern const uint8_t cPopMaxTimers;
extern uint8_t giPopNwkRetryEntriesWaterMark;
extern const uint8_t cPopNwkRetryEntries;
extern const uint8_t cPopNwkRouteDiscoveryEntries;
extern uint8_t giPopNwkRouteDiscoveryHighWaterMark;
extern const uint8_t cPopNwkDuplicateEntries;
extern uint8_t giPopNwkDuplicateHighWaterMark;
extern sPopNwkDataIndication_t *gpPopNwkIncomingMsg;

sPopAssertCount_t gsPopAssert;
#if DEBUG >= 2
sMpDebugCount_t gsMpDebug;
uint8_t giUnknownEventMemFree;
extern uint8_t giUnknownEvent;
#endif
/***************************************************************************
  Code
***************************************************************************/


/*
  PopAppDefaultHandler

  The default handler performs the default behavior for messages, typically 
  freeing up any memory allocated by other functions, or continuing a state 
  machine. The application should nearly ALWAYS call the default handler on 
  every event.
  
*/
void PopAppDefaultHandler(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
  switch(iEvtId)
  {

 #if gPopManageNvm_d
    case gPopEvtNvmDataChanged_c:
      // Usually multiple changes to NWK data occur rapidly (changing AddrPool/PAN ID, etc...)
      // Save the Nwk data 3 seconds (3000ms) after it has changed for the last time (when things settle)
      if (PopNvmCheckBitOnMask(gPopNvmNwkDataChanged_c))
        (void)PopStartTimer(gPopTimerIdNwkDataChanged_c, 3000);
      break;
#endif

#if gPopNwkManageAddrPool_d
    case gPopEvtNwkAddrPoolChanged_c:
      // manage the address pool if user requested it (save to NVM and get more if needed)
      // if we've run out of addresses, ask for more from the address server
      if(PopNwkGetAddrCount() == 0 /*&& !PopNwkIsSleepyNode()*/)                // #JJ enable joining also if routing disabled
        PopNwkAddrPoolRequest(PopNwkGetAddressServer(), PopNwkGetAddrReqSize());
  #if gPopManageNvm_d
      PopNvmSetBitOnMask(gPopNvmNwkDataChanged_c);
  #endif
      break;
#endif

    // application has had a chance to process the data indication. Now free it.
    case gPopEvtDataIndication_c:
      (void)PopMemFree(sEvtData.pDataIndication);
    break;

    // a beacon indication has been received. Send it to the proper processing routine, then free it.
    case gPopEvtNwkBeaconIndication_c:
#if gPopNwkNodePlacement_d
      if (PopNwkInNodePlacementMode())
        PopNwkNodePlaceProcessBeacon(sEvtData.pNwkBeaconIndication);
      else
#endif
        PopNwkProcessBeaconIndication(sEvtData.pNwkBeaconIndication);

      // done with the beacon indication
      (void)PopMemFree(sEvtData.pNwkBeaconIndication);
    break;

    // The scan is complete, free the memory allocated by the scan
    case gPopEvtNwkScanConfirm_c:
      // Free the scan confirm, clear the global scan lists, and indicate we're no longer gathering beacons
      PopNwkScanComplete();
    break;

    // A node placement indication, set the LEDs accordingly
#if gPopNwkNodePlacement_d
    case gPopEvtNwkNodePlacement_c:
      PopNwkNodePlacementLeds(sEvtData.pPopNodePlacementResult);
    break;
#endif

    // a node wishes to join this node... tell the NWK layer
    case gPopEvtNwkJoinIndication_c:
      PopSetEventEx(cNwkTaskId, gPopEvtNwkJoinIndication_c, sEvtData.pNwkJoinIndication);
    break;

    // timers come here if the application hasn't handled them
    case gPopEvtTimer_c:
      PopHandleDefaultTimer(sEvtData.iTimerId);
    break;

    // blink the application startup
    case gPopEvtAppBlink_c:
      PopSetLed(gPopLed1_c, gPopLedOn_c);
      mfPopAppBlinkLedOn = true;
      miPopAppBlinkCountDown = PopNwkGetApplicationId() & 0xf;
      (void)PopStartTimer(gPopTimerIdAppBlink_c, mPopAppBlinkCountDownRate_c);
      break;

    // handled out of memory failure
    case gPopEvtMemoryAllocFailure_c:
      (void)PopStartTimerEx(cAppTaskId, gPopTimerIdMemAllocFail_c, mPopMemAllocFailTimeout_c);
#if DEBUG >= 1
      if(gsPopAssert.iAssertHeapCompromised < 255)
        gsPopAssert.iAssertHeapCompromised++;
#endif //DEBUG >= 1
      break;

#if gPopGateway_d
    // Serial data has come in from the PC (or Host processor). Handle it in the gateway if enabled.
    case gPopEvtSerialDataReady_c:
      PopGatewayProcessIncomingData();
    break;
#endif

    // just informative to the application
    case gPopEvtNwkRouteConfirm_c:
    case gPopEvtSerialTxDone_c:                                                 // #JJ
    case gPopEvtNwkJoinEnable_c:                                                // #JJ
    case gPopEvtNwkRoutePassThru_c:                                             // #JJ
      break;

    // do nothing on default
    default:
#if DEBUG >= 2
      if(giUnknownEvent){
        gsMpDebug.iEventCnt = iEvtId;
        if(iEvtId == gPopEvtTableFull_c){             
          gsMpDebug.iGwCmdCnt = sEvtData.iTableFullMask;                        // #JJ use GwCmdCnt for gPopEvtTableFull_c data
        }
        giUnknownEventMemFree = 1;                                              // #JJ Could maybe cause heap issues if memory freed when not intended
        PopMemFree(sEvtData.pData);
        giUnknownEventMemFree = 0;
      }
#endif
      break;
  }
}

/*
  Handle any default timers.
*/
void PopHandleDefaultTimer(popTimerId_t iTimerId)
{
  sPopEvtData_t sEvtData;

  switch(iTimerId)
  {

#if gPopManageNvm_d
    // Store changed NWK data into NVM after 3 seconds
    case gPopTimerIdNwkDataChanged_c:
      (void)PopNvStoreNwkData();
    break;
#endif

#if gPopNwkNodePlacement_d

    case gPopTimerIdNodePlaceBlinkLed_c:
      if(giPopBlinkRate==0)
        PopSetLed(gPopLed1_c, gPopLedOff_c);
      else
        PopSetLed(gPopLed1_c, gPopLedToggle_c);

      if(PopNwkInNodePlacementMode())
        (void)PopStartTimer(gPopTimerIdNodePlaceBlinkLed_c, giPopBlinkRate);    
      break;

    // time to send out the next beacon request for node-placement
    case gPopTimerIdNodePlaceBeaconReq_c:

      PopSetLed(gPopLed1_c | gPopLed2_c | gPopLed3_c | gPopLed4_c, gPopLedOff_c);
      gsPopNodePlacementResult.iNodeCount=0;
      giPopBlinkRate=0;

      // send out a beacon request on this particular channel
      (void)PopSetAppEvent(gPopEvtNwkNodePlacement_c, &gsPopNodePlacementResult);
      PopNwkBeaconRequest(giPopNodePlaceChannel);
  
      if(PopNwkInNodePlacementMode())
        (void)PopStartTimer(gPopTimerIdNodePlaceBeaconReq_c, giPopNodePlacementRate_c);
    break;

#endif //gPopNwkNodePlacement_d

    // toggle the LED on/off for the # of times in the count down
    case gPopTimerIdAppBlink_c:
      // count down the blink
      mfPopAppBlinkLedOn = !mfPopAppBlinkLedOn;
      if(mfPopAppBlinkLedOn)
      {
        --miPopAppBlinkCountDown;
        if(miPopAppBlinkCountDown)
          PopSetLed(gPopLed1_c, gPopLedOn_c);
      }
      else 
        PopSetLed(gPopLed1_c, gPopLedOff_c);

      // blink again
      if(miPopAppBlinkCountDown)
        (void)PopStartTimer(gPopTimerIdAppBlink_c, mPopAppBlinkCountDownRate_c);

    // count down the long timer
    case gPopTimerIdLong_c:
      // if done, then long timer is done, tell application about it
      if(!giPopLongTimeOut)
      {
        sEvtData.iTimerId = giPopLongTimerId;
        (void)PopSetEvent(gPopEvtTimer_c, sEvtData.pData);
      }

      // on to next section of long timer
      else
      {
        (void)PopDecrementLongTimeOut();
      }
    break;
    
    case gPopTimerIdOtaFwdlMode_c:  // #JJ added to avoid gateway watchdog restart
      otaFwdlModeFlag = false;
      break;
      
    case gPopTimerIdMemAllocFail_c: // Reset flag for MemAllocFail
      PopMemInit(); // reset heap if corrupted
      PopMemResetOutOfMemoryFlag();
      /* Re-allocate memory for pointer to receive message */
      gpPopNwkIncomingMsg = NULL;
      PopNwkReceive();
      break;
    default:
      break;
  }
}

/*
  PopDecrementLongTimeOut()

  Helper function. Decrements the long timer (that is, waits for the next increment using a normal short timer).
*/
popErr_t PopDecrementLongTimeOut(void)
{
  popTimeOut_t iTimeOut;

  // determine amount of time left
  iTimeOut = gPopMaxTimeout_c;
  if(giPopLongTimeOut < (popLongTimeOut_t)gPopMaxTimeout_c)
    iTimeOut = (popTimeOut_t)giPopLongTimeOut;

  // decrement timeout, and start the clock again
  giPopLongTimeOut -= (uint32_t)iTimeOut;
  return PopStartTimerEx(cAppTaskId, gPopTimerIdLong_c, iTimeOut);
}

/*
  PopStartLongTimer()

  Start the long timer. Long timer can be up to 4 billion (2^32) milliseconds, or about 49 days.
*/
popErr_t PopStartLongTimer(popTimerId_t iTimerId, popLongTimeOut_t iMilliseconds)
{
  giPopLongTimeOut = iMilliseconds;
  giPopLongTimerId = iTimerId;
  return PopDecrementLongTimeOut();
}

/*
  PopStopLongTimer()

  Stop a long timer
*/
popErr_t PopStopLongTimer(popTimerId_t iTimerId)
{
  // prevent compiler warning of unused parameter
  (void)iTimerId;

  // stop the long timer
  return PopStopTimerEx(cAppTaskId, gPopTimerIdLong_c);
}


/***************************************************************************
  Debug Code
***************************************************************************/

/*
  PopAssert

  Halts the processor because some catastrophic error has occurred.

  Current assert codes:
  gPopAssertStackOverRun_c     the C stack has been overrun. Use fewer nested functions or smaller locals.
  gPopAssertHeapCompromised_c  some errant pointer has overwritten part of the heap
  gPopAssertOutOfEvents_c      the system event queue is too small.
  gPopAssertBadReceivePtr_c    the system is attempting to receive with no memory. Bug in PopNet logic.
                               normal, the last incoming packet would be dropped.
*/
void PopAssert(popAssertCode_t iAssertCode)
{
  // <SJS: Added more assert code>
  // store number of asserts divided by code in NVM
#if DEBUG >= 1
  #if gPopNumberOfNvPages_c
  {
    extern uint32_t GetMcuTimer1();
    uint16_t iStartTime;
    
#warning debug set led depending on assert type   
    
    TUI_PIN_BATTERY_TEMPERATURE_OFF;
    TUI_PIN_ELECTROLYTIC_LEVEL_OFF;
    TUI_PIN_VOLTAGE_BALANCE_OFF;
    TUI_PIN_PROCESSOR_RUNNING_OFF;
    TUI_PIN_WIRELESS_INFO_OFF;
      
    switch(iAssertCode)
    {
      case gPopAssertStackOverRun_c:
        if(gsPopAssert.iAssertStackOverRun < 255)
          ++gsPopAssert.iAssertStackOverRun;
        TUI_PIN_BATTERY_TEMPERATURE_ON
        break;
       
      case gPopAssertHeapCompromised_c:
        if(gsPopAssert.iAssertHeapCompromised < 255)
          ++gsPopAssert.iAssertHeapCompromised;
        TUI_PIN_ELECTROLYTIC_LEVEL_ON
        break;
  
      case gPopAssertOutOfEvents_c:
        if(gsPopAssert.iAssertOutOfEvents < 255)
          ++gsPopAssert.iAssertOutOfEvents;
        TUI_PIN_BATTERY_TEMPERATURE_ON;
        TUI_PIN_ELECTROLYTIC_LEVEL_ON;
        break;
        
      case gPopAssertBadReceivePtr_c:
        if(gsPopAssert.iAssertBadReceivePtr < 255)
          ++gsPopAssert.iAssertBadReceivePtr;
        TUI_PIN_VOLTAGE_BALANCE_ON;
        break;
  
      case gPopAssertNwkWatchdog_c:
        if(gsPopAssert.iAssertNwkWatchdog < 255)
          ++gsPopAssert.iAssertNwkWatchdog;
        TUI_PIN_BATTERY_TEMPERATURE_ON;
        TUI_PIN_VOLTAGE_BALANCE_ON;
        break;
        
      case gPopAssertGwWatchdog_c:
        if(gsPopAssert.iAssertGwWatchdog < 255)
          ++gsPopAssert.iAssertGwWatchdog;
        TUI_PIN_ELECTROLYTIC_LEVEL_ON;
        TUI_PIN_VOLTAGE_BALANCE_ON;
        break;
    }
        
    while(1)
      ;/* wait forever */
    
    // Store to NVM
    (void)PopNvStore(gPopNvIdAssert_c, sizeof(gsPopAssert), &gsPopAssert);
    #if DEBUG >= 2
    (void)PopNvStore(gMpNvIdDebug_c, sizeof(gsMpDebug), &gsMpDebug);
    #endif
    // wait for 300ms just to let NVM hardware settle
    iStartTime = GetMcuTimer1();
    while((GetMcuTimer1() - iStartTime) < 300)
      /* wait */;
  }
  #endif
#endif  
  // </SJS:>
  // Reset the system 
  SysCtrlReset();
}

#if gPopAssertLevel_c
/*
  Fires an assert if the heap is not OK. Commented out if gPopAssertLevel_c is 0. 
  Sprinkle this liberally through code suspected of corrupting the heap.
*/
void DebugHeapCheck(void)
{
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopAssert(gPopAssertHeapCompromised_c);
}
#endif

/*
  PopEventsLeftAtHighWaterMark

  This function reports the maximum # of events available left in the worst case scenario.
  It does the difference between the event high water mark and the maximum number of events
  in sthe stack.
*/
uint8_t PopEventsLeftAtHighWaterMark(void)
{
  // In the case of an over run (giPopNumEvents < giPopEventsHigWaterMark) the code will assert.
  return (cPopMaxEvents - giPopEventsHigWaterMark);
}

#if !gPopBigEndian_c

#else

#endif
/*
  PopTimersLeftAtHighWaterMark

  Returns the amount of timers left on the highest water mark.
*/
uint8_t PopTimersLeftAtHighWaterMark(void)
{
  // If we run out of timers the system will inform through an event but it wont assert so
  // we need to consider that case.
  if (giPopTimerWaterMark >= cPopMaxTimers)
  {
    return 0;
  }

  // returns the amount of timers left at high water mark.
  return (cPopMaxTimers - giPopTimerWaterMark);
}

/*
  PopRetryEntriesLeftAtHighWaterMark

  Return the amount of entries left on the retry table at high water mark.
*/
uint8_t PopRetryEntriesLeftAtHighWaterMark(void)
{
  // If the table gets over run the system only sets an event to the application, does not
  // asserts, so, the overr run case need to be consider.
  if (giPopNwkRetryEntriesWaterMark >= cPopNwkRetryEntries)
    return 0;

  // Return the maximum number of retry table entries at high water mark.
  return (cPopNwkRetryEntries - giPopNwkRetryEntriesWaterMark);
}

/*
  PopRouteDiscoveryEntriesLeftAtHighWaterMark

  Returns the amount of route discovery table entries left at high water mark.
*/
uint8_t PopRouteDiscoveryEntriesLeftAtHighWaterMark(void)
{
  // running out of dicovery entries does not assert the system we should consider that case.
  if (giPopNwkRouteDiscoveryHighWaterMark >= cPopNwkRouteDiscoveryEntries)
    return 0;

  // return the maximum amount of dicovery entries available at high water mark.
  return (cPopNwkRouteDiscoveryEntries - giPopNwkRouteDiscoveryHighWaterMark);
}

/*
  PopDuplicateEntriesLeftAtHighWaterMark()

  Returns the maximum amount of entries in the duplicate table at high water mark.
*/
uint8_t PopDuplicateEntriesLeftAtHighWaterMark(void)
{
  // Handle the table full case.
  if (giPopNwkDuplicateHighWaterMark >= cPopNwkDuplicateEntries)
    return 0;

  // returns the amount of duplicate entries left at high water mark.
  return (cPopNwkDuplicateEntries - giPopNwkDuplicateHighWaterMark);
}

#if gPopNwkNodePlacement_d

/*
  PopNwkStartNodePlacement

  Start node placement on the given channel. Note: this function works whether on a network on not.
  PanId() should be set either to the desired pan, or to 0xffff. See PopNwkSetPanId().
*/
void PopNwkStartNodePlacement(popChannel_t iChannel)
{
  //Start Node Placement 
  gfPopNwkInNodePlacementMode = true;

  // if node placement on then permit joining should be off;
  PopNwkJoinEnable(false, false);

  // use the selected channel
  giPopNodePlaceChannel= iChannel;

  // now we're paying attention to beacons we hear
  gfPopNwkIsProcessingBeaconEnabled = true;

  // send out a beacon request on this particular channel
  PopNwkBeaconRequest(giPopNodePlaceChannel);

  // start timer for next beacon
  (void)PopStartTimerEx(cAppTaskId, gPopTimerIdNodePlaceBeaconReq_c, giPopNodePlacementRate_c);
 }


/*
  PopNwkEndNodePlacement

  End node placement (restores LEDs to former state)
*/
void PopNwkEndNodePlacement(void)
{
  //STOP Node Placement 
  gfPopNwkInNodePlacementMode = false;

  // turn On the ability to join
  // BUGBUG: but only if user wants joining on
  PopNwkJoinEnable(!gfPopNwkInNodePlacementMode, false);

  // Stop paying attention to beacons 
  gfPopNwkIsProcessingBeaconEnabled = false;

  // clean variables and turn leds  off 
  PopSetLed(gPopLed1_c | gPopLed2_c | gPopLed3_c | gPopLed4_c, gPopLedOff_c);
  PopMemSetZero(&gsPopNodePlacementResult, sizeof(gsPopNodePlacementResult));
}

/*
  PopNwkNodePlaceProcessBeacon

  Process beacon indications for node placment. In "node placement" mode.
*/
void PopNwkNodePlaceProcessBeacon(sPopNwkBeaconIndication_t *pNwkBeaconIndication)
{    
  // Verify ProtocolID, JoinEnable and PanID to know if new beacon is a good candidate 
  if(pNwkBeaconIndication->iProtocol == gPopNetId_c &&
    PopNwkIsJoiningEnabled(pNwkBeaconIndication->iProtocolVer) &&
      (pNwkBeaconIndication->iPanId == PopNwkGetPanId()))
  {
    // Update BeaconCount      
     gsPopNodePlacementResult.iNodeCount++;
  }

  // Update the Lqi
  gsPopNodePlacementResult.iLqi=pNwkBeaconIndication->iLqi;

  // If LQI under min value put a 1 in iBar
  if(pNwkBeaconIndication->iLqi < gaPopNodePlacementStrongSignal[0])
    gsPopNodePlacementResult.iBars=0x01;

  // If LQI between min and max values iBar = 2
  if(pNwkBeaconIndication->iLqi > gaPopNodePlacementStrongSignal[0] && pNwkBeaconIndication->iLqi <= gaPopNodePlacementStrongSignal[1]) 
    gsPopNodePlacementResult.iBars=0x02;

  // If LQI above max value iBar = 3        
  if( pNwkBeaconIndication->iLqi > gaPopNodePlacementStrongSignal[1] )
  {  
    gsPopNodePlacementResult.iBars=0x03;

      // If node count >1 then iBar=4
       if(gsPopNodePlacementResult.iNodeCount>1)
        gsPopNodePlacementResult.iBars=0x04;  
  }

  // Remember the memory will be freed after leaving this function.
}

/*
  PopNwkNodePlacementLeds

  Sets the node placement LEDs. Occurs on gPopEvtNwkNodePlacement_c app event.
*/
void PopNwkNodePlacementLeds(sPopNodePlacement_t *pPopNodePlacementResult)
{
/*Blink LED1 depending of the node placement result*/
  switch(pPopNodePlacementResult->iBars)
  {
    // Only 1 node and low LQI blink slowly
    case 1: giPopBlinkRate = mPopNwkNodePlacementBlinkRate_c;
#if (gPopNumLeds_c == 1)
      (void)PopStartTimerEx(cAppTaskId, gPopTimerIdNodePlaceBlinkLed_c, giPopBlinkRate);
#else
      PopSetLed(gPopLed1_c, gPopLedOn_c);
#endif
    break;

    // Blink faster better LQI          
    case 2: giPopBlinkRate = mPopNwkNodePlacementBlinkRate_c/2;
#if (gPopNumLeds_c == 1)
      (void)PopStartTimerEx(cAppTaskId, gPopTimerIdNodePlaceBlinkLed_c, giPopBlinkRate);
#else
      PopSetLed(gPopLed1_c, gPopLedOn_c);
      PopSetLed(gPopLed2_c, gPopLedOn_c);
#endif
    break;

    // Blink faster very strong LQI
    case 3: giPopBlinkRate = mPopNwkNodePlacementBlinkRate_c/10;
#if (gPopNumLeds_c == 1)
      (void)PopStartTimerEx(cAppTaskId, gPopTimerIdNodePlaceBlinkLed_c, giPopBlinkRate);
#else
      PopSetLed(gPopLed1_c, gPopLedOn_c);
      PopSetLed(gPopLed2_c, gPopLedOn_c);
      PopSetLed(gPopLed3_c, gPopLedOn_c);
#endif
    break;

    // Solid light when more than 1 node with good LQI
    case 4:
#if (gPopNumLeds_c == 1)
      PopSetLed(gPopLed1_c, gPopLedOn_c);
#else
      PopSetLed(gPopLed1_c, gPopLedOn_c);
      PopSetLed(gPopLed2_c, gPopLedOn_c);
      PopSetLed(gPopLed3_c, gPopLedOn_c);
      PopSetLed(gPopLed4_c, gPopLedOn_c);
#endif
    break;
  }
  //clear iBars for the next beacon request
  pPopNodePlacementResult->iBars = 0;
}
#endif //gPopNwkNodePlacement_d


/*
  Return true if the status was one of the successful ones.
*/
bool PopNwkFormJoinSuccess(popStatus_t iStatus)
{
  return (iStatus == gPopStatusJoinSuccess_c || iStatus == gPopStatusFormSuccess_c || iStatus == gPopStatusSilentStart_c) ? true : false;
}

/*
  PopNwkMgmtProcessOtaUpgrade

  Receives all over the air commands for OTA Upgrades and responds depending on the 
  command type
*/
void PopNwkMgmtProcessOtaUpgrade(sPopNwkDataIndication_t * pIndication)
{
  /* This #if was put inside to avoid the undefined compile error in the PopNetLib */  
#if gPopOverTheAirUpgrades_d
  sPopOtauOtaSubseqBlockPassThruSaveReq_t*  pPopSaveOtaUpgradeReq;
  sPopOtaUpgradeRsp_t*  pPopSaveOtaUpgradeRsp;
  sPopOtaUpgradeResetRsp_t * pUpgradeResetRsp;
  /* over the air response */
  sPopOtaUpgradeCompleteRsp_t * pOtaUpgradeCompleteRsp;
  popErr_t statusRsp;
  sPopNwkDataRequest_t sDataReq;
  sPopOtaUpgradeRsp_t upgradeRsp;

  uint8_t errLen; 
  pPopSaveOtaUpgradeReq = PopNwkPayload(pIndication);
  pPopSaveOtaUpgradeRsp = (sPopOtaUpgradeRsp_t*)pPopSaveOtaUpgradeReq;

  /* process a request */
  switch(pPopSaveOtaUpgradeReq->reqType){
    
  case gPopGwNwkMgmtOtaUpgradeFirstSaveReq_c:
  case gPopGwNwkMgmtOtaUpgradeSaveReq_c:
  case gPopGwNwkMgmtOtaUpgradeCancelReq_c:
  {
#if gPopMicroPowerLpcUpgrade_d
    statusRsp = PopLpcOtaUpgradeStorageModule((sPopOtauGwSubseqBlockSaveReq_t *)&pPopSaveOtaUpgradeReq->blockNumber,gPopOtaUpgradeReqSrcOTA_c);

    if(statusRsp == gPopErrBadHwId_c)
#endif
      /* store payload */
      statusRsp = PopOtaUpgradeStorageModule((sPopOtauGwSubseqBlockSaveReq_t *)&pPopSaveOtaUpgradeReq->blockNumber,gPopOtaUpgradeReqSrcOTA_c);

    /* create the status error payload, filling in with the correct information if an wrong HwId, AppId or Platform was used*/
    errLen = PopOtaUpgradeAddErrorInfo(statusRsp,&(upgradeRsp.otaUpgradeError), gPopOtaUpgradeReqSrcOTA_c);

    /* check if this is a broadcast or unicast transmission */
    if(pIndication->sFrame.sNwk.iDstAddr != gPopNwkBroadcastAddr_c)
    {
      upgradeRsp.otaUpgradeError.errType = statusRsp;
      upgradeRsp.blockNumber = pPopSaveOtaUpgradeReq->blockNumber;
      NativeToOta16(upgradeRsp.blockNumber);
      upgradeRsp.rspType = gPopGwNwkMgmtOtaUpgradeSaveRsp_c;
      sDataReq.iDstAddr = pIndication->sFrame.sNwk.iSrcAddr;
      sDataReq.iOptions = gPopNwkDataReqOptsDefault_c;
      /* the response contains response type(1 byte)+ BlockNumber + Error type plus some extra info if needed*/
      sDataReq.iPayloadLength = sizeof(uint8_t) + sizeof(uint16_t) + sizeof(popErr_t) + errLen;

      sDataReq.iRadius = PopNwkGetDefaultRadius();
      sDataReq.pPayload = &upgradeRsp;
      PopNwkMgmtDataRequest(&sDataReq,false);
    }
    otaFwdlModeFlag = true; // #JJ added to avoid gateway watchdog restart
    PopStartTimerEx(cAppTaskId, gPopTimerIdOtaFwdlMode_c, 5000); // #JJ added to avoid gateway watchdog restart
  }
  break;
  
  case gPopGwNwkMgmtOtaUpgradeSaveRsp_c:
    {

      /* if status is different than success save it to stop transmission later*/
      gsPopOtaUpgradeInfo.txStatus = pPopSaveOtaUpgradeRsp->otaUpgradeError.errType;
      if(!(gsPopOtaUpgradeInfo.txStatus))
      {
        /* increase the block number only if we got a response for that block */
        OtaToNative16(pPopSaveOtaUpgradeRsp->blockNumber);

        if(gsPopOtaUpgradeInfo.blockNumber == pPopSaveOtaUpgradeRsp->blockNumber){
          PopSetOtaUpgradeState(ePopOtaReceivedRsp);  
          gsPopOtaUpgradeInfo.blockNumber++;
        
          /* wait throttle or send block right now */
          if(gsPopOtaUpgradeInfo.throttleMilliseconds)
          {
            //set the timer to send the next message
            (void)PopStartTimerEx(cNwkTaskId,gPopOtaUpgradeThrottleTimer_c,gsPopOtaUpgradeInfo.throttleMilliseconds);
            //Update the state machine status
            PopSetOtaUpgradeState(ePopOtaWaitForThrottle);
          }
          else
          {
            /* send response to get next piece */
            (void)PopStopTimerEx(cNwkTaskId,gPopOtaUpgradeThrottleTimer_c);
            pPopSaveOtaUpgradeRsp->otaUpgradeError.errInfo.blockNumber = pPopSaveOtaUpgradeRsp->blockNumber;
            PopOtaUpgradeSendRspToSerial(gPopGwCmdOtaUpgradePassThruReq_c,gsPopOtaUpgradeInfo.txStatus,&(pPopSaveOtaUpgradeRsp->otaUpgradeError));
          }
        }
      }
      else
      {
        (void)PopStopTimerEx(cNwkTaskId,gPopOtaUpgradeThrottleTimer_c);
        PopOtaUpgradeSendRspToSerial(gPopGwCmdOtaUpgradePassThruReq_c,gsPopOtaUpgradeInfo.txStatus,&(pPopSaveOtaUpgradeRsp->otaUpgradeError));
      }
    }
    break;
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c:
      /* use the ota upgrade save rsp variable to send the reset response too */
      pUpgradeResetRsp = (sPopOtaUpgradeResetRsp_t *)&upgradeRsp;
      /* convert request to local */
      pPopSaveOtaUpgradeReq->reqType = 0;
      /* respond only if an error occurred */
      OtaToNative16(((sPopGwResetToNewImageReq_t *) pPopSaveOtaUpgradeReq)->handler);
      OtaToNative16(((sPopGwResetToNewImageReq_t *) pPopSaveOtaUpgradeReq)->appId);
#if gPopMicroPowerLpcUpgrade_d      
      statusRsp = PopNwkMgmtLpcUpgradeResetToNewImage((sPopGwResetToNewImageReq_t *) pPopSaveOtaUpgradeReq);
      if(statusRsp == gPopErrBadHwId_c)
#endif
        pUpgradeResetRsp->errType = PopNwkMgmtOtaUpgradeResetToNewImage((sPopGwResetToNewImageReq_t *) pPopSaveOtaUpgradeReq);
#if gPopMicroPowerLpcUpgrade_d
      else
        pUpgradeResetRsp->errType = statusRsp;
#endif
      // only respond if not a broadcast
      if(pIndication->sFrame.sNwk.iDstAddr == gPopNwkBroadcastAddr_c)
        break;
      else
      {
        pUpgradeResetRsp->rspType = gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c;
        sDataReq.iDstAddr = pIndication->sFrame.sNwk.iSrcAddr;
        sDataReq.iOptions = gPopNwkDataReqOptsDefault_c;
        sDataReq.iPayloadLength = sizeof(uint8_t) + sizeof(popErr_t);
        sDataReq.iRadius = PopNwkGetDefaultRadius();
        sDataReq.pPayload = pUpgradeResetRsp;
        PopNwkMgmtDataRequest(&sDataReq,false);
      }
    break;
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c:
      pUpgradeResetRsp = (sPopOtaUpgradeResetRsp_t *)pPopSaveOtaUpgradeReq;
      PopOtaUpgradeSendRspToSerial(pPopSaveOtaUpgradeReq->reqType,pUpgradeResetRsp->errType, NULL);
    break;
    case gPopGwNwkMgmtOtaUpgradeCompleteReq_c:
      /* reuse the reset response structure */
      pOtaUpgradeCompleteRsp = (sPopOtaUpgradeCompleteRsp_t *)&upgradeRsp;

      /* copy the handle and req type to check them */
      PopMemCpy(pOtaUpgradeCompleteRsp, pPopSaveOtaUpgradeReq, sizeof(sPopOtaUpgradeCompleteReq_t));

      /* if the handle is different then do not respond */
      OtaToNative16(pOtaUpgradeCompleteRsp->handler);
      if(!(pOtaUpgradeCompleteRsp->handler == gsPopOtaUpgradeInfo.handler))
        break;

      if(PopOtaUpgradeIsImageComplete())
      {
        /* if the image is complete and is a broadcast request do not respond */
        if(pIndication->sFrame.sNwk.iDstAddr == gPopNwkBroadcastAddr_c)
          break;

        pOtaUpgradeCompleteRsp->blockNumber = 0xFFFF;
      }
      else
      {
        /* the image is not complete respond with the last received block */
        pOtaUpgradeCompleteRsp->blockNumber = gsPopOtaUpgradeInfo.blockNumber;
        NativeToOta16(pOtaUpgradeCompleteRsp->blockNumber);
      }

      NativeToOta16(pOtaUpgradeCompleteRsp->handler);
      /* fill in the message to send */
      pOtaUpgradeCompleteRsp->reqType = gPopGwNwkMgmtOtaUpgradeCompleteRsp_c;
      sDataReq.iDstAddr = pIndication->sFrame.sNwk.iSrcAddr;
      sDataReq.iOptions = gPopNwkDataReqOptsDefault_c;
      /* the response contains response type(1 byte)+ BlockNumber + Error type plus some extra info if needed*/
      sDataReq.iPayloadLength = sizeof(sPopOtaUpgradeCompleteRsp_t);
      sDataReq.iRadius = PopNwkGetDefaultRadius();
      sDataReq.pPayload = (uint8_t *)&(gsPopOtaUpgradeInfo.imageCopy) + sizeof(sPopNwkDataRequest_t);
      /*save the response to send it after jitter*/
      PopMemCpy(&(gsPopOtaUpgradeInfo.imageCopy),&sDataReq,sizeof(sPopNwkDataRequest_t));
      /*copy payload right after*/
      PopMemCpy(sDataReq.pPayload, pOtaUpgradeCompleteRsp,sizeof(sPopOtaUpgradeCompleteRsp_t));
      /*set the jitter timer*/
      (void)PopStartTimerEx(cNwkTaskId,gPopOtaUpgradeCompleteTimer_c,(PopRandom16() % gPopOtaUpgradeCompleteRspJitter_c));
    break;
    case gPopGwNwkMgmtOtaUpgradeCompleteRsp_c:
      /* process only if we are waiting for it */
      if(PopGetOtaUpgradeState() != ePopOtaWaitForCompleteRsp)
        break;

      PopOtaUpgradeSendCompleteRspToGw(popOtauAnyCompleteRsp,(sPopOtaUpgradeCompleteRsp_t *)pPopSaveOtaUpgradeRsp,pIndication->sFrame.sNwk.iSrcAddr);
    break;
  default:
    break;
    }
#else
(void)pIndication;
#endif //#if gPopOverTheAirUpgrades_d
}

/*
  PopNwkMgmtOtaUpgradeResetToNewImage
  Receives a reset to new image either from UART or OTA
  If the reset is local then it just calls the switch image function to verify new image
  If the reset is going OTA it fills the message and sends it out

*/
#if gPopOverTheAirUpgrades_d
popErr_t PopNwkMgmtOtaUpgradeResetToNewImage(sPopGwResetToNewImageReq_t * pResetReq)
{
  sPopNwkDataRequest_t sDataReq;
  sPopOtaResetToNewImageReq_t payload;

  /* send OTA req */
  if(pResetReq->reqType)
  {
    NativeToOta16(pResetReq->handler);
    NativeToOta16(pResetReq->appId);
    sDataReq.iDstAddr = pResetReq->dstAddr;
    sDataReq.iOptions = gPopNwkDataReqOptsDefault_c;
    sDataReq.iPayloadLength = sizeof(sPopOtaResetToNewImageReq_t);
    sDataReq.iRadius = PopNwkGetDefaultRadius();
    pResetReq->reqType = gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c;
    PopMemCpy(&payload, pResetReq,sizeof(sPopOtaResetToNewImageReq_t));
    sDataReq.pPayload = &payload;
    PopNwkMgmtDataRequest(&sDataReq,false);
    return gPopErrNone_c;
  }
  /* local command */
  else
  {
#if gPopUsingStorageArea_c
    if((pResetReq->platform == gPopUpgradeReqMyHwType_c) &&
       ((pResetReq->appId == gPopAppId_c) || (pResetReq->appId == 0xFFFF)) &&
       (pResetReq->hwId == giPopHardwareId))
      return PopSwitchToNewImage(gsPopOtaUpgradeInfo.preserveNvm);
    else
      return gPopErrBusy_c;
#else
    return gPopErrNoImage_c;
#endif
  }
}
#endif //#if gPopOverTheAirUpgrades_d

/*
  PopNwkMgmtOtaUpgradeCompleteRequest
  Sends a message over the air to query nodes in the network about the status of the
  last over the air upgrade
*/
#if gPopOverTheAirUpgrades_d
void PopNwkMgmtOtaUpgradeCompleteRequest(sPopGwUpgradeCompleteReq_t* pOtaUpgradeCompleteReq)
{
  sPopNwkDataRequest_t sDataReq;
  uint16_t tempHandler;
  /* reuse the incoming packet as payload buffer */
  sPopOtaUpgradeCompleteReq_t * pPayload = (sPopOtaUpgradeCompleteReq_t*) pOtaUpgradeCompleteReq;
  /* fix endiannes */
  NativeToOta16(pOtaUpgradeCompleteReq->handler);
  /* save the handler temporarily */
  tempHandler = pOtaUpgradeCompleteReq->handler;
  /* fill in the data request */
  sDataReq.iDstAddr = pOtaUpgradeCompleteReq->dstAddr;
  sDataReq.iOptions = gPopNwkDataReqOptsDefault_c;
  sDataReq.iPayloadLength = sizeof(sPopOtaUpgradeCompleteReq_t);
  sDataReq.iRadius = PopNwkGetDefaultRadius();
  sDataReq.pPayload = pPayload;
  /* insert the request information */
  pPayload->reqType = gPopGwNwkMgmtOtaUpgradeCompleteReq_c;
  pPayload->handler = tempHandler;

  //set the timer to send the next message
  (void)PopStartTimerEx(cNwkTaskId,gPopOtaUpgradeCompleteTimer_c,giPopNwkBroadcastExpired);
  //set the state machine to wait for responses
  PopSetOtaUpgradeState(ePopOtaWaitForCompleteRsp);
  /* send out the message */
  PopNwkMgmtDataRequest(&sDataReq,false);
}
#endif

#if gPopOverTheAirUpgrades_d
bool PopOtaUpgradeIsImageComplete(void)
{
  uint32_t iSavedImageSize;
  /* calculate how much image we have */
  iSavedImageSize = gsPopOtaUpgradeInfo.blockNumber * gsPopOtaUpgradeInfo.blockSize;

  return (gsPopOtaUpgradeInfo.imageSize <= iSavedImageSize);
}
#endif
/*
     PopOtaUpgradeStateMachine
     Updates the state machine for the application retries
     and sends a response to gateway when needed
 */

void PopOtaUpgradeStateMachine(void)
{
/* This #if was put inside to avoid the undefined compile error in the PopNetLib */  
#if gPopOverTheAirUpgrades_d
  if( (PopGetOtaUpgradeState() == ePopOtaReceivedRsp) ||
      (PopGetOtaUpgradeState() == ePopOtaWaitForThrottle) ||
      (PopGetOtaUpgradeState() == ePopOtaLastRetry)
    ) {
      /* send response to get next piece */
      sPopOtaUpgradeErr_t info;
      info.errType = 0;
      info.errInfo.blockNumber = gsPopOtaUpgradeInfo.blockNumber - 1;
      PopOtaUpgradeSendRspToSerial(gPopGwCmdOtaUpgradePassThruReq_c,gsPopOtaUpgradeInfo.txStatus, &info);
  } else if(PopGetOtaUpgradeState() >= ePopOtaWaitForRsp)
  {
    /*if we are retrying and transmission was canceled, do not retry just send response back to the app */
    if(gsPopOtaUpgradeInfo.txStatus == gPopErrCanceled_c)
    {
      PopOtaUpgradeSendRspToSerial(gPopGwCmdOtaUpgradePassThruReq_c,gsPopOtaUpgradeInfo.txStatus, NULL);
      return;
    }
    //increase in 1 to reach the next state
    PopSetOtaUpgradeState(PopGetOtaUpgradeState()+1);
    /* send again the previous piece of image */
    gsPopOtaUpgradeInfo.txStatus = gPopErrNone_c;
    PopOtaUpgradeSendNextBlock((sPopOtauGwSubseqBlockPassThruSaveReq_t *)&(gsPopOtaUpgradeInfo.imageCopy));
  }
#endif
}


/*
 * PopOtaUpgradesAddErrorInfo
 *
 * Creates an error containing also the correct information
 */
#if gPopOverTheAirUpgrades_d
 uint8_t PopOtaUpgradeAddErrorInfo(popErr_t errType, sPopOtaUpgradeErr_t * pOtaUpgradeError, uint8_t sourceType)
{

  //set the error type
  pOtaUpgradeError->errType = errType;

  //If the error is not enough memory, tell the user how much he has
  if(errType == gPopErrNoMem_c)
  {
   pOtaUpgradeError->errInfo.availableMemory = gPopAvailableMemory_c;
   if (PopIsUpgradeReqSrcOTA(sourceType))
     //Make it little endian over the air if necessary
     NativeToOta32(pOtaUpgradeError->errInfo.availableMemory);
   else
     //if necessary make it big endian for the serial port
     NativeToGateway32(&(pOtaUpgradeError->errInfo.availableMemory));
   //return size of available memory
   return sizeof(uint32_t);
  }

  //Tell the user the correct Platform
  if(errType == gPopErrBadPlatform_c)
  {
    if (PopIsUpgradeReqSrcOTA(sourceType))
    {
      // it will be sent over the air, send only 1 byte
      pOtaUpgradeError->errInfo.platform = gPopUpgradeReqMyHwType_c;
      return sizeof(uint8_t);
    }
    else
    {
      //It will be sent over the serial port then send it in 4 bytes
      pOtaUpgradeError->errInfo.availableMemory = gPopUpgradeReqMyHwType_c;
      //Make it big endian if necessary
      NativeToGateway32(&(pOtaUpgradeError->errInfo.availableMemory));
      //Over the serial port we always send a 4 bytes value
      return sizeof(uint32_t);
    }

  }

  //Tell the user the correct Application Id
  if(errType == gPopErrBadAppId_c)
  {
    if (PopIsUpgradeReqSrcOTA(sourceType))
    {
      // it will be sent over the air send only 2 bytes
      pOtaUpgradeError->errInfo.appId = gPopAppId_c;
      //Make it little endian over the air if necessary
      NativeToOta16(pOtaUpgradeError->errInfo.appId);
      return sizeof(uint16_t);
    }
    else
    {
      //It will be sent over the serial port then send it in 4 bytes
      pOtaUpgradeError->errInfo.availableMemory = gPopAppId_c;
      //Make it big endian if necessary
      NativeToGateway32(&(pOtaUpgradeError->errInfo.availableMemory));
      //Over the serial port we always send a 4 bytes value
      return sizeof(uint32_t);
    }

  }


  //Tell the user the number of the last block
  if(errType == gPopErrWrongBlock_c)
  {
    if (PopIsUpgradeReqSrcOTA(sourceType))
    {
      // it will be sent over the air send only 2 bytes
      pOtaUpgradeError->errInfo.appId = gsPopOtaUpgradeInfo.blockNumber;
      //Make it little endian over the air if necessary
      NativeToOta16(pOtaUpgradeError->errInfo.appId);
      return sizeof(uint16_t);
    }
    else
    {
      //It will be sent over the serial port then send it in 4 bytes
      pOtaUpgradeError->errInfo.availableMemory = gsPopOtaUpgradeInfo.blockNumber;
      //Make it big endian if necessary
      NativeToGateway32(&(pOtaUpgradeError->errInfo.availableMemory));
      //Over the serial port we always send a 4 bytes value
      return sizeof(uint32_t);
    }

  }
//Tell the user the correct Hardware Id
  if(errType == gPopErrBadHwId_c)
  {
    if(PopIsUpgradeReqSrcOTA(sourceType))
    {
      //It will be sent over the air send only 1 byte
      pOtaUpgradeError->errInfo.hwId = giPopHardwareId;
      return sizeof(uint8_t);
    }
    else
    {
      //It will be sent over the serial port then send it in 4 bytes
      pOtaUpgradeError->errInfo.availableMemory = giPopHardwareId;
      //Make it big endian if necessary
      NativeToGateway32(&(pOtaUpgradeError->errInfo.availableMemory));
      //Over the serial port we always send a 4 bytes value
      return sizeof(uint32_t);
    }
  }

  //Tell the user the correct Number of Nv pages
    if(errType == gPopErrNvmSize_c)
    {
      if(PopIsUpgradeReqSrcOTA(sourceType))
      {
        //It will be sent over the air send only 1 byte
        pOtaUpgradeError->errInfo.nvPages = gPopNumberOfNvPages_c;
        return sizeof(uint8_t);
      }
      else
      {
        //It will be sent over the serial port then send it in 4 bytes
        pOtaUpgradeError->errInfo.availableMemory = gPopNumberOfNvPages_c;
        //Make it big endian if necessary
        NativeToGateway32(&(pOtaUpgradeError->errInfo.availableMemory));
        //Over the serial port we always send a 4 bytes value
        return sizeof(uint32_t);
      }
    }

 // do this if there is no error and the response will be sent over the serial port
  if(!PopIsUpgradeReqSrcOTA(sourceType))
  {
    //It will be sent over the serial port then send it in 4 bytes
    pOtaUpgradeError->errInfo.availableMemory = 0;
    //Over the serial port we always send a 4 bytes value
    return sizeof(uint32_t);
  }
  else
    // For any other command over the air no info bytes are sent
    return 0;

}
#endif


#if gPopOverTheAirUpgrades_d
 void PopOtaUpgradeSaveRspToGw(popOtauCmdId_t cmdId,popErr_t errType)
 {
   sPopEvtData_t  sEvtData;
   sPopOtaUpgradeErr_t   sPopOtaUpgradeError;

   (void)PopOtaUpgradeAddErrorInfo(errType, &sPopOtaUpgradeError,gPopOtaUpgradeReqSrcOTS_c);

   sEvtData.pPopOtaUpgradeError = &sPopOtaUpgradeError;
   /* send thru gateway */
    PopGatewayTargetToPC(cmdId,sEvtData);

 }

#endif
 /*

   PopOtaUpgradePassThruRequest

   Sends an image saved to make an OTA upgrade
 */
#if gPopOverTheAirUpgrades_d
 popErr_t PopOtaUpgradePassThruRequest(sPopOtauGwSubseqBlockPassThruSaveReq_t * pNotFirstImageBlock)
 {
   /* to access first block data */
   sPopOtauGwFirstBlockPassThruSaveReq_t * pfirstPassThruImageBlock;

   /* indicate where is this block coming from */
   PopSetOtaUpgradeState(ePopOtaGotBlockFromSerial);

   /* set the new block number */
   gsPopOtaUpgradeInfo.blockNumber = pNotFirstImageBlock->blockNumber;


   /* save the throttle to use it when response comes back */
   gsPopOtaUpgradeInfo.throttleMilliseconds = pNotFirstImageBlock->throttle;
   /* subsequent blocks process */
   if (pNotFirstImageBlock->blockNumber)
   {
     /* checks image id */
     if(!(gsPopOtaUpgradeInfo.handler))
       return gPopErrNoImage_c;
   }
   /* first block process */
   else
   {
     /* use the appropriate pointer */
     pfirstPassThruImageBlock = (sPopOtauGwFirstBlockPassThruSaveReq_t *)pNotFirstImageBlock;
     /* saves image information */
     gsPopOtaUpgradeInfo.handler = pfirstPassThruImageBlock->handler;
     gsPopOtaUpgradeInfo.imageSize = pfirstPassThruImageBlock->imageSize;
     /*blocksize + request type (1byte) + image id (2bytes)+ block number (2 bytes) + checksum (1byte)
     should be smaller than the maximum payload*/
     if(pfirstPassThruImageBlock->blockSize > (PopNwkMaxPayload()- 6))
       return gPopErrBlockSizeTooBig_c;
     else
       gsPopOtaUpgradeInfo.blockSize = pfirstPassThruImageBlock->blockSize;
     /* calculate how many blocks we'll pass thru*/
     gsPopOtaUpgradeInfo.totalBlocks = ((uint16_t)gsPopOtaUpgradeInfo.imageSize/gsPopOtaUpgradeInfo.blockSize)+1;

   }
   /*assume success to start */
   gsPopOtaUpgradeInfo.txStatus = gPopErrNone_c;

   PopOtaUpgradeSendNextBlock(pNotFirstImageBlock);
   return gPopErrNone_c;
  }

 /*

 */

 void PopOtaUpgradeSendNextBlock(sPopOtauGwSubseqBlockPassThruSaveReq_t * pNotFirstImageBlock)
 {

   popTimeOut_t responseTimeout;
   
   /* no errors continue sending messages */
   if((gsPopOtaUpgradeInfo.txStatus == gPopErrNone_c))
   {
     /* make a copy for the retries */
     if(PopGetOtaUpgradeState() == ePopOtaGotBlockFromSerial)
     {
       PopMemCpy(&(gsPopOtaUpgradeInfo.imageCopy), pNotFirstImageBlock, sizeof(sPopOtaUpgradeReq_t));
     }
     /* create and send message */
     PopOtaUpgradeMessageCreator(pNotFirstImageBlock->blockNumber,(sPopOtauGwFirstBlockPassThruSaveReq_t *)pNotFirstImageBlock);
     /*
       if it is a broadcast there will be no response then we need to set the throttle time here
     */
     if(pNotFirstImageBlock->dstAddr == gPopNwkBroadcastAddr_c)
     {
       /* Broadcast throttle can't be less than ms 4000 it will fill retry tables too quickly */
       if(gsPopOtaUpgradeInfo.throttleMilliseconds == 0)
         gsPopOtaUpgradeInfo.throttleMilliseconds = 4000;
       /* start throttle timer */
       if(gsPopOtaUpgradeInfo.throttleMilliseconds)
         (void)PopStartTimerEx(cNwkTaskId,gPopOtaUpgradeThrottleTimer_c,gsPopOtaUpgradeInfo.throttleMilliseconds);
       PopSetOtaUpgradeState(ePopOtaReceivedRsp);
     }
     /* unicast messages */
     else
     {
       /* set status as failed until confirm is received */
       gsPopOtaUpgradeInfo.txStatus = gPopErrTimeOut_c;
       /* change state only when block coms  */
       if(PopGetOtaUpgradeState() == ePopOtaGotBlockFromSerial)
         PopSetOtaUpgradeState(ePopOtaWaitForRsp);
       /* start timer to wait for response */
       if (pNotFirstImageBlock->blockNumber)
         responseTimeout = PopOtaWaitForRspTimeout;
       else
         responseTimeout = 10000;
       
       (void)PopStartTimerEx(cNwkTaskId,gPopOtaUpgradeThrottleTimer_c,/*gsPopOtaUpgradeInfo.throttleMilliseconds*/ /*PopOtaWaitForRspTimeout*/ responseTimeout);
               
     }

   }
   /*
     the transmission of a message failed, stop sending messages and respond
     over the serial port
   */
   else
   {
     PopOtaUpgradeSendRspToSerial(gPopGwCmdOtaUpgradePassThruReq_c, gPopErrFailed_c, NULL);
   }

 }
 /*

   PopOtaUpgradeMessageCreator

   creates the message and sends it OTA
 */
 void PopOtaUpgradeMessageCreator(uint16_t blockNumber,sPopOtauGwFirstBlockPassThruSaveReq_t * pFirstBlock)
 {
   sPopOtaUpgradeReq_t sendOtaReq;
   sPopNwkDataRequest_t sDataRequest;

   /* set up the request */
   sDataRequest.iDstAddr = pFirstBlock->dstAddr;
   if (PopGetOtaUpgradeState() == ePopOtaLastRetry)
     sDataRequest.iOptions = gPopNwkDataReqOptsForce_c;
   else
     sDataRequest.iOptions = gPopNwkDataReqOptsDefault_c;
   sDataRequest.iRadius = PopNwkGetDefaultRadius();
   /* the payload in the serial packet follows the header, so get the address of it */
   sDataRequest.pPayload = &sendOtaReq;
   sendOtaReq.reqType = gPopGwNwkMgmtOtaUpgradeSaveReq_c;

   if(!blockNumber)
   {
     NativeToOta32(pFirstBlock->imageSize);
     NativeToOta16(pFirstBlock->appId);
     PopMemCpy(&(sendOtaReq.blockNumber), &(pFirstBlock->blockNumber),sizeof(sPopOtauGwFirstBlockSaveReq_t));
     sDataRequest.iPayloadLength = sizeof(sPopOtauOtaFirstBlockPassThruSaveReq_t);
     /* put endianness back, in case we need to retry */
     OtaToNative32(pFirstBlock->imageSize);
     OtaToNative16(pFirstBlock->appId);
   }
   else
   {
     PopMemCpy(&(sendOtaReq.blockNumber), &(pFirstBlock->blockNumber),sizeof(sPopOtauOtaSubseqBlockPassThruSaveReq_t)-1 + gsPopOtaUpgradeInfo.blockSize);
     if((gsPopOtaUpgradeInfo.totalBlocks == blockNumber)  || (gsPopOtaUpgradeInfo.txStatus == gPopErrCanceled_c))
       sendOtaReq.reqType = gPopGwNwkMgmtOtaUpgradeCancelReq_c;
     sDataRequest.iPayloadLength = sizeof(sPopOtauOtaSubseqBlockPassThruSaveReq_t)-1+ gsPopOtaUpgradeInfo.blockSize;
   }
   /* set endiannes appropriately */
   NativeToOta16(sendOtaReq.blockNumber);
   NativeToOta16(sendOtaReq.handler);
   /* send OTA */
   PopNwkMgmtDataRequest(&sDataRequest,false);
 }
#endif

 /*
   PopOtaUpgradeStorageModule

   Verifies the image blocks arriving and calls storage routines to save it

   Possible result status:
   gPopErrNone_c =  success
   gPopErrNoMem_c = total image is bigger than available memory
   gPopErrBusy_c = another transfer is taking place
   gPopErrFailed_c = hw check or block or image checksum failed
   gPopErrNoImage_c = non block 0 received after 3xThrottle expired
   or without a previous block 0

 */

 void PopOtaUpgradeSendRspToSerial(uint8_t rspOpcode, popErr_t errStatus, sPopOtaUpgradeErr_t* pErrorInfo)
 {
#if gPopOverTheAirUpgrades_d
  sPopEvtData_t evtData;
  sPopOtaUpgradeErr_t sOtauError;

  evtData.pPopOtaUpgradeError = &sOtauError;

  // if no info error received set it to zeros
  if(!pErrorInfo)
    sOtauError.errInfo.availableMemory = 0;
  else
  {
    switch(errStatus)
    {

    case gPopErrNoMem_c:
      OtaToNative32(pErrorInfo->errInfo.availableMemory);
      NativeToGateway32(&(pErrorInfo->errInfo.availableMemory));
      sOtauError.errInfo.availableMemory = pErrorInfo->errInfo.availableMemory;
    break;
    case gPopErrBadPlatform_c:
    case gPopErrBadHwId_c:
    case gPopErrNvmSize_c:
      sOtauError.errInfo.availableMemory = (uint32_t)pErrorInfo->errInfo.platform;
      /* Copy the byte into the 32 and then do the native to gateway conversion */
      NativeToGateway32(&(sOtauError.errInfo.availableMemory));
    break;
    case gPopErrNone_c:
      OtaToNative16(pErrorInfo->errInfo.appId);
      sOtauError.errInfo.blockNumber = (uint32_t)(pErrorInfo->errInfo.blockNumber);
      /* Copy the 16bit into the 16 and then do the native to gateway conversion */
      NativeToGateway16(&(sOtauError.errInfo.blockNumber));
    break;
    case gPopErrBadAppId_c:
    case gPopErrWrongBlock_c: 
      OtaToNative16(pErrorInfo->errInfo.appId);
      sOtauError.errInfo.availableMemory = (uint32_t)(pErrorInfo->errInfo.appId);
      /* Copy the 16bit into the 32 and then do the native to gateway conversion */
      NativeToGateway32(&(sOtauError.errInfo.availableMemory));
    break;
    default:
      sOtauError.errInfo.availableMemory = 0;
    break;
    }
  }
  /* set the error type */
  sOtauError.errType = errStatus;
  /* send thru gateway */
  PopGatewayTargetToPC(rspOpcode, evtData);
#else
  (void)rspOpcode;
  (void)errStatus;
  (void)pErrorInfo;
#endif
 }

 popErr_t PopOtaUpgradeFirstBlockVerification(sPopOtauGwFirstBlockSaveReq_t  * pFirstImageBlock)
 {
 #if gPopOverTheAirUpgrades_d
#if gPopMicroPowerLpcUpgrade_d
    /* get swaped Application Id */
   gsPopOtaUpgradeInfo.appId = pFirstImageBlock->appId;
   /* get swaped Application Id */
   gsPopOtaUpgradeInfo.hwId = pFirstImageBlock->hwId;
   /* get swaped Image Size */
   gsPopOtaUpgradeInfo.imageSize = pFirstImageBlock->imageSize;
   
//   /* check Application Id if it indicates special upgrade only check size */
//   if(gsPopOtaUpgradeInfo.appId == gPopLpcUpgradeAppId_c)
   /* check Hardware Id if it indicates special upgrade only check size */
   if(gsPopOtaUpgradeInfo.hwId == gPopLpcUpgradeHwId_c)
   {
     /* checks available memory */
     if(gsPopOtaUpgradeInfo.imageSize > gPopLpcAvailableMemory_c)
       return gPopErrNoMem_c;
   
   }
   /* if app id is not for LPC special upgrade*/
   else
   {
     /* checks available memory */
     if(gsPopOtaUpgradeInfo.imageSize > gPopAvailableMemory_c)
       return gPopErrNoMem_c;

     if((gsPopOtaUpgradeInfo.appId != gPopAppId_c) && (gsPopOtaUpgradeInfo.appId != 0xFFFF))
       return gPopErrBadAppId_c;
     /* checks the Hw type */
     if((pFirstImageBlock->platform != gPopUpgradeReqMyHwType_c)&& (pFirstImageBlock->platform != 0xFF))
       return gPopErrBadPlatform_c;
     /* check Hardware Id */
     if((pFirstImageBlock->hwId != giPopHardwareId) && (pFirstImageBlock->hwId != 0xFF))
       return gPopErrBadHwId_c;
  
     // verify only for S08
     #if gPopBigEndian_c
       if(pFirstImageBlock->nvPages != gPopNumberOfNvPages_c)
             return gPopErrNvmSize_c;
     #endif
   }     
     
#else
   /* get swaped Image Size */
   gsPopOtaUpgradeInfo.imageSize = pFirstImageBlock->imageSize;
   /* checks available memory */
   if(gsPopOtaUpgradeInfo.imageSize > gPopAvailableMemory_c)
     return gPopErrNoMem_c;

   /* checks the Hw type */
   if((pFirstImageBlock->platform != gPopUpgradeReqMyHwType_c)&& (pFirstImageBlock->platform != 0xFF))
     return gPopErrBadPlatform_c;

   /* get swaped Application Id */
   gsPopOtaUpgradeInfo.appId = pFirstImageBlock->appId;
   /* check Application Id */
   if((gsPopOtaUpgradeInfo.appId != gPopAppId_c) && (gsPopOtaUpgradeInfo.appId != 0xFFFF))
     return gPopErrBadAppId_c;
   /* check Hardware Id */
   if((pFirstImageBlock->hwId != giPopHardwareId) && (pFirstImageBlock->hwId != 0xFF))
     return gPopErrBadHwId_c;

 // verify only for S08
 #if gPopBigEndian_c
   if(pFirstImageBlock->nvPages != gPopNumberOfNvPages_c)
         return gPopErrNvmSize_c;
 #endif
#endif
   return gPopErrNone_c;
 #else
   (void)pFirstImageBlock;
   return gPopErrNone_c;
 #endif
 }

 void PopOtaUpgradeSendCompleteRspToGw
 (
   popOtauCmdId_t RspType,
   sPopOtaUpgradeCompleteRsp_t  * pOtauCompleteRsp,
   popNwkAddr_t iSrcAddr
 )
 {
#if gPopOverTheAirUpgrades_d
   sPopEvtData_t sEvtData;
   sPopGwUpgradeCompleteRsp_t sPopGwOtauCompleteRsp;
   if(RspType == popOtauAnyCompleteRsp)
   {
      /* fix endianness if necessary */
     OtaToNative16(pOtauCompleteRsp->handler);
     NativeToGateway16(&(pOtauCompleteRsp->handler));
     OtaToNative16(pOtauCompleteRsp->blockNumber);
     NativeToGateway16(&(pOtauCompleteRsp->blockNumber));
     //OtaToNative16(iSrcAddr);
     NativeToGateway16(&iSrcAddr);
     /* copy the handle and block number */
     PopMemCpy(&(sPopGwOtauCompleteRsp.handler), &(pOtauCompleteRsp->handler) , sizeof(sPopGwUpgradeCompleteReq_t));
     /* add the short address of the source */
     sPopGwOtauCompleteRsp.dstAddr = iSrcAddr;

   }
   else
   {
     /* copy the handle and block number */
     PopMemSet(&(sPopGwOtauCompleteRsp), 0xFF , sizeof(sPopGwUpgradeCompleteRsp_t));
   }

   sEvtData.pPopOtaUpgradeGwCompleteRsp = &sPopGwOtauCompleteRsp;
 /* send thru gateway */
 PopGatewayTargetToPC(gPopGwNwkMgmtOtaUpgradeCompleteRsp_c,sEvtData);
#else
 (void)RspType;
 (void)pOtauCompleteRsp;
 (void)iSrcAddr;
#endif

 }

#if gPopMicroPowerLpcUpgrade_d
//extern uint8_t ispCommanderModeFlag;
extern uint8_t gaGatewayRxSerialData[];

eMPIspState_t MPIspState=eSendQnMark;
// User Manual LPC17XX  - Section 32.7.4 The length of any UU-encoded line 
// should not exceed 61 characteres (bytes) i.e. it can hold 45 data bytes
#define mMP_MaxBinImageLen_c 45
// User Manual LPC17XX - Table 567 (1765 have 21 sectors, 1768 have 29 sectors)
#define mMP_MaxSectorId_c 29 
// User Manual LPC17XX - Section 32.7.4 The checksum... is reset after 
// transmitting 20 UU-encodedl lines.
#define mMP_ResetChecksumLine_c 20
// Maximum lines to save in 1024 RAM in pieces of 45 bytes
#define mMP_Max45BytesLines_c 22
// Maximum size of uuencoded line 
#define mMP_MaxUueImageLen_c 64
// Maximum size of checksum string in ascii
#define mMP_MaxChecksumStr_c 11
// Total size of RAM
#define mMP_MaxRamSize_c 1024
// Maximum image size for LPC1765 (256 kB flash)
#define mMP_LPC1765MaxImageSize_c 0x40000

/* 
    MPUuencoder
    Uuencodes piece of binary image to send it to the LPC bootloader
*/
uint32_t MPUuencoder(uint8_t *pUueLine, uint8_t uueLen, uint8_t * pImageBinLine, uint8_t binLen)
{
  uint32_t checksum =0;
  uint8_t i = 0;
  // leave first uuencoded array byte to store size
  uint8_t x = 1;
  // Process every 3 binary image bytes to uuencode them into 4 bytes
  for(i = 0; ((i < binLen) && ((x+3) < uueLen)); i += 3 )
  {
    if((i+1)>=binLen)
      pImageBinLine[i+1]=0x00;
    if((i+2)>=binLen)
      pImageBinLine[i+2]=0x00;
    checksum += pImageBinLine[i] + pImageBinLine[i+1] + pImageBinLine[i+2];
    // uuencoding byte 1 = 6 bits of binary image byte 1 (then add 32)
    pUueLine[x] = (pImageBinLine[i] >> 2) + 32;
    // uuencoding byte 2 = 2 bits of binary image byte 1 + 4 bits of image byte 2  (then add 32)
    pUueLine[x+1] = (((pImageBinLine[i] & 0x03) << 4) | (pImageBinLine[i+1] >> 4)) + 32;
    // uuencoding byte 3 = 4 bits of binary image byte 2 + 2 bits of image byte 3  (then add 32)
    pUueLine[x+2] = (((pImageBinLine[i+1] & 0x0F) << 2) | (pImageBinLine[i+2] >> 6)) +32;
    // uuencoding byte 4 = 6 bits of binary image byte 3  (then add 32)
    pUueLine[x+3] = (pImageBinLine[i+2] & 0x3F) + 32;
    
    // if uuencoding value is 32 then change it to 0x60 
    if(pUueLine[x] == 32) pUueLine[x] = 0x60;
    if(pUueLine[x+1] == 32) pUueLine[x+1] = 0x60;
    if(pUueLine[x+2] == 32) pUueLine[x+2] = 0x60;
    if(pUueLine[x+3] == 32) pUueLine[x+3] = 0x60;
    
    // move pointer to uuencoded array 4 bytes
    x+=4;
  }

  // set uuencoded array size character
  pUueLine[0]= binLen + 32;
  // set new line characters
  if(uueLen > x+2)    
  {
   pUueLine[x]=0x0d;
   pUueLine[x+1]=0x0a;
  
  }

  return checksum;
}
/* 
  MP_Uint32ToASCII
  pAsciiStrAddr - IN/OUT - string receives start of string returns start of ascii value
  in that string
  integerVal - IN - integer value of string
  returns size of ascii value
*/
uint8_t MP_Uint32ToASCII(uint32_t integerVal, uint32_t * pAsciiStrAddr)
{
  uint8_t * pAsciiStr = (uint8_t *) *pAsciiStrAddr;
  // get index for max size of ascii string 
  // use volataile to avoid optimization mess with value of i
  volatile uint8_t i = mMP_MaxChecksumStr_c-1;
  do{
    i--;
    (pAsciiStr)[i] = ( integerVal % 10 ) + '0';
    integerVal /= 10;
  }while(integerVal);
  
  // use pAsciiStrAddr to indicate where the ascii string starts
  *pAsciiStrAddr = (uint32_t)(&(pAsciiStr)[i]);
  // return size of array
  i= mMP_MaxChecksumStr_c-i;
  return i;
}

void MPIspStateHandler(void)
{
  // string commands
  char aQuestion[] = {0x3F}; // "?"
  char aSync[] = {0x53, 0x79, 0x6E, 0x63, 0x68, 0x72, 0x6F, 0x6E, 0x69, 0x7A, 0x65, 0x64, 0x0D}; // "Synchronized "
  char aSyncRsp[] = {0x53, 0x79, 0x6E, 0x63, 0x68, 0x72, 0x6F, 0x6E, 0x69, 0x7A, 0x65, 0x64, 0x0D, 0x4F, 0x4B, 0x0D}; //"Synchronized OK "
  char aClkFreq[] = {0x31, 0x32, 0x30, 0x30, 0x30, 0x0D}; // "12000 "
  char aOK[] = {0x4F, 0x4B, 0x0D}; // "OK "
  char aEcho[] = {0x41, 0x20, 0x31, 0x0D}; //"A 1 "
  char aReadPartId[] = {0x4A, 0x0D}; // "J " 
  char aReadPartId1765Rsp[] = {0x36, 0x33, 0x37, 0x36, 0x31, 0x33, 0x38, 0x37, 0x35, 0x0D}; //LPC1765 "637613875 "
  char aReadPartId1768Rsp[] = {0x36, 0x33, 0x37, 0x36, 0x31, 0x35, 0x39, 0x32, 0x37, 0x0D}; //LPC1768 "637615927 "
  char aCmdUnlock[] = {0x55, 0x20, 0x32, 0x33, 0x31, 0x33, 0x30, 0x0D}; //"U 23130 "
  char aPrepareAllFlash[] = {0x50, 0x20, 0x30, 0x20, 0x32, 0x31, 0x0D}; //"P 0 21 "
  char aPrepareAllFlash1768[] = {0x50, 0x20, 0x30, 0x20, 0x32, 0x39, 0x0D}; //"P 0 29 "    
  char aEraseAllFlash[] = {0x45, 0x20, 0x30, 0x20, 0x32, 0x31, 0x0D}; //"E 0 21 "
  char aEraseAllFlash1768[] = {0x45, 0x20, 0x30, 0x20, 0x32, 0x39, 0x0D}; //"E 0 29 "    
  char aWriteRam[] = {0x57, 0x20, 0x32, 0x36, 0x38, 0x34, 0x33, 0x35, 0x39, 0x36, 0x38, 0x20, 0x31, 0x30, 0x32, 0x34, 0x0D}; //"W 268435968 1024 " = 0x1000 0200
  char aCopyToFlash[] = {0x43,0x20,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x20, 0x32, 0x36, 0x38, 0x34, 0x33, 0x35, 0x39, 0x36, 0x38, 0x20,0x31,0x30,0x32,0x34,0x0D}; //"C xxxxxxxxxx 268435968 1024 " allow maximum size because flash address varies
  char aRamAddrSize[] = {0x32, 0x36, 0x38, 0x34, 0x33, 0x35, 0x39, 0x36, 0x38, 0x20,0x31,0x30,0x32,0x34,0x0D};//"268435968 1024 
  // array for piece of binary image startas filled with example for debug but gets cleared in eSendDataToRam state
  uint8_t binLine[mMP_MaxBinImageLen_c] = {0x1e,0xfd,0x01,0x98,  0x00,0x38,0x18,0xbf, 0x01,0x20,0x0e,0xbd,  0x00,0xbf,0x07,0xb5,
                  0xca,0x21,0x0d,0xf1,  0x07,0x00,0x00,0x22, 0x24,0xf0,0x11,0xfd,  0x9d,0xf8,0x07,0x00,
                  0xd0,0xf1,0x01,0x00,  0x38,0xbf,0x00,0x20, 0x0e,0xbd,0x07,0xb5,  0x40};
  // array for uuencoded line result from binLine gets cleared in eSendDataToRam state
  uint8_t uueLine[mMP_MaxUueImageLen_c];
  
  // static variables to control image transmission
  static popOtauImageSize_t imageSent;  // how much of the bin file has been sent through SPI
  static popOtauImageSize_t imageSize;  // Total image size
  static popOtauImageSize_t imageInFlash; // Total image copied to Flash
  static uint8_t sectorId = 0;  // Sector Id, to get sectore size
  static uint8_t linesSent = 0;  // keep count of lines to send checksum
  static uint16_t savedRam;   // Total image saved in RAM
  static uint16_t savedSector; // Total image saved in Flash sector
  // to keep checksum of bin lines
  static uint32_t checksum = 0;
  // how many bytes to count to read the status of the LPC bootloader response
  static uint8_t statusOffset=0;
  //LPC1768 or 1765?
  static uint8_t TargetNodeType; //0 = unknown, 1 = it's a LPC1765 controller, 2 = it's a LPC 1768 controller   
  // variables 
  uint8_t LPC1765 = 1;
  uint8_t LPC1768 = 2;    
  uint16_t sectorSize; // gets calculated everytime the function is called
  uint8_t bytesToSend = 0;  // binary image bytes to send to the LPC bootloader through SPI
  uint8_t aChecksumStr[mMP_MaxChecksumStr_c]; // array for ASCII string of checksum value
  uint8_t *pChecksumStr = aChecksumStr; // pointer to get the start of the ASCII string  of checksum value
  uint8_t serialBytes;  // how many actual bytes are sent through SPI
  uint8_t sendDummyBytes=0; // dummy bytes to complete either a line or sector
  uint8_t status = gPopErrFailed_c; // to go back to initial status of variables when failure occurres

  
  
 

  // start sync with ?
  if(MPIspState == eSendQnMark){
    status = gPopErrNone_c;
    MPGatewaySendLpcCommand(sizeof(aQuestion),aQuestion); 
    MPIspState++;
  }
  // send synchronization message
  else if((MPIspState == eSendSynchronized) && (!PopMemCmp(aSync,&gaGatewayRxSerialData[0],sizeof(aSync))))
  {
    status = gPopErrNone_c;
    MPGatewaySendLpcCommand(sizeof(aSync),aSync);
    MPIspState++;
  }
  // send clock frequency
  else if((MPIspState ==eSendClkFreq) && (!PopMemCmp(aSyncRsp,(void *)gaGatewayRxSerialData,sizeof(aSyncRsp))))
  {
    status = gPopErrNone_c;
    MPGatewaySendLpcCommand(sizeof(aClkFreq),aClkFreq);
    MPIspState++;
  }
  // send echo on
  else if((MPIspState == eSendEcho) && (!PopMemCmp(aOK,(void *)(gaGatewayRxSerialData + sizeof(aClkFreq)),sizeof(aOK))))
  {    
    status = gPopErrNone_c;
    MPGatewaySendLpcCommand(sizeof(aEcho),aEcho);
    MPIspState++;
  }
  // read part id
  else if((MPIspState == eSendReadPartId) && (gaGatewayRxSerialData[sizeof(aEcho)] == 0x30))
  {    
    status = gPopErrNone_c;
    MPGatewaySendLpcCommand(sizeof(aReadPartId),aReadPartId);
    MPIspState++;
  }
  // send unlock
  else if(MPIspState == eSendUnlock) 
  {  
    status = gPopErrNone_c;     
    if(!PopMemCmp(aReadPartId1765Rsp,(void *)(gaGatewayRxSerialData + sizeof(aReadPartId)+sizeof(aOK)),sizeof(aReadPartId1765Rsp)))
    {       
      TargetNodeType = LPC1765; //LPC1765 (256kB flash)
      if(gsPopOtaUpgradeInfo.imageSize > mMP_LPC1765MaxImageSize_c)     
      {      
        //Image is to big for this microcontroller
        status = gPopErrNoMem_c;        
      }
    }
    else if(!PopMemCmp(aReadPartId1768Rsp,(void *)(gaGatewayRxSerialData + sizeof(aReadPartId)+sizeof(aOK)),sizeof(aReadPartId1768Rsp)))
    {      
      TargetNodeType = LPC1768; //LPC1768 (512kB flash)
    }
    if((TargetNodeType > 0) && (status == gPopErrNone_c))
    {     
      MPGatewaySendLpcCommand(sizeof(aCmdUnlock),aCmdUnlock);
      MPIspState++; 
    }           
  }
  // prepare all flash
  else if((MPIspState == eSendPrepAllFlash) && (gaGatewayRxSerialData[sizeof(aCmdUnlock)] == 0x30))
  {   
    status = gPopErrNone_c;
    if (TargetNodeType == LPC1765)
    {
      MPGatewaySendLpcCommand(sizeof(aPrepareAllFlash),aPrepareAllFlash);
    }
    else 
    { 
      MPGatewaySendLpcCommand(sizeof(aPrepareAllFlash1768),aPrepareAllFlash1768);
    }        
    MPIspState++;
  }
  // erase all flash
  else if((MPIspState == eSendEraseAllFlash) && (gaGatewayRxSerialData[sizeof(aPrepareAllFlash)] == 0x30))
  {   
    status = gPopErrNone_c;
    // get ready to start sending the image only in this state update the static value
    imageSize = gsPopOtaUpgradeInfo.imageSize;
    if(!imageSize){  
      MPIspState = eSendQnMark;// there is no image to copy abort
      return;
    }
    imageSent = 0;   
    imageInFlash = 0;
    sectorId = 0;
    linesSent = 0;    
    savedRam = 0;
    savedSector =0;
    checksum = 0;
    if (TargetNodeType == LPC1765)
    {
      statusOffset = sizeof(aEraseAllFlash);
      MPGatewaySendLpcCommand(sizeof(aEraseAllFlash),aEraseAllFlash);
    }
    else if (TargetNodeType == LPC1768)
    {
      statusOffset = sizeof(aEraseAllFlash1768);            
      MPGatewaySendLpcCommand(sizeof(aEraseAllFlash1768),aEraseAllFlash1768);      
    }
    MPIspState++;
  }
  else if((MPIspState == eSendWriteRam) && (gaGatewayRxSerialData[statusOffset] == 0x30))
  {   
    status = gPopErrNone_c;
    // start sending write RAM command        
    statusOffset = sizeof(aWriteRam);
    // wait a little to allow copy to flash finish correctly
    DelayMs(100);
    MPGatewaySendLpcCommand(sizeof(aWriteRam),aWriteRam);
    MPIspState++;
  }
  // loop  to send data
  else if((MPIspState == eSendDataToRam)   )//&& (PopMemCmp(aOK,(void *)(gaGatewayRxSerialData + sizeof(aWriteRam)),sizeof(aOK))))
  {
    status = gPopErrNone_c;
    // calculate sector size depending on its id see User Manual LPC17XX - Table 567 sectors 0x10 and up are 32kB
    statusOffset = 0;
    sectorSize = (sectorId & 0x10)? 0x8000: 0x1000;
    if(savedSector >= sectorSize)
    {
        savedSector = 0;
        sectorId++;
        sectorSize = (sectorId & 0x10)? 0x8000: 0x1000;
    }
        // if sector is stil valid
    if(sectorId <= mMP_MaxSectorId_c)
    {
      // if we have space to save 45 bytes and we have 45 bytes to send
      if((linesSent < mMP_Max45BytesLines_c ) && ((imageSize - imageSent) >= mMP_MaxBinImageLen_c)  && (( sectorSize - savedSector ) >= mMP_MaxBinImageLen_c) && ((mMP_MaxRamSize_c - savedRam ) >=mMP_MaxBinImageLen_c))
      {
        bytesToSend = mMP_MaxBinImageLen_c;
        serialBytes = mMP_MaxUueImageLen_c;
      }
      // we don't have 45 bytes free in RAM or we don't have 45 bytes to send
      else
      {
        // space is ram is less than 45 bytes
        if((mMP_MaxRamSize_c - savedRam) < (imageSize - imageSent))
            bytesToSend = (mMP_MaxRamSize_c - savedRam);
        else{
          // what is less? space in RAM or left image
          bytesToSend = (imageSize - imageSent);
          // add dummy bytes to complete 45 or sector or RAM
          sendDummyBytes = ((mMP_MaxRamSize_c - savedRam)< mMP_MaxBinImageLen_c)?(mMP_MaxRamSize_c - (savedRam + bytesToSend)):(mMP_MaxBinImageLen_c - (imageSize - imageSent));
        }
          
        // Get the real number of bytes to send trough serial
        serialBytes = (bytesToSend + sendDummyBytes)/3;
        serialBytes += ((bytesToSend + sendDummyBytes) % 3)?1:0;          
        serialBytes *= 4;
        serialBytes += 3;
        
      
      }
      // Clear binary line before reading it from storage
      PopMemSetZero(binLine,mMP_MaxBinImageLen_c);
      // Get piece to send
      if(!PopStorageReadBytes(binLine, imageSent+gPopLpcStorageStart_c, bytesToSend))
      {
          // clear uue line before encoding
          PopMemSetZero(uueLine,mMP_MaxUueImageLen_c);
          
          // uu encode line and get checksum
          checksum += MPUuencoder(uueLine, mMP_MaxUueImageLen_c,binLine,bytesToSend+sendDummyBytes);
          
          // update counters
          imageSent += bytesToSend;
          savedRam += bytesToSend+sendDummyBytes;
          savedSector += bytesToSend+sendDummyBytes;
          linesSent++;
          // if we just sent line 20 send checksum and reset it
          if((linesSent == mMP_ResetChecksumLine_c)  || ((bytesToSend + sendDummyBytes) < mMP_MaxBinImageLen_c)){
            // next time go to send checksum state
            MPIspState = eSendChecksum;
          }            
          // send line
          MPGatewaySendLpcCommand(serialBytes,uueLine);
      }
      else // we could not read storage
        status = gPopErrFailed_c;
    }
  }
  else if(MPIspState == eSendChecksum)// && (PopMemCmp(aOK,(void *)(gaGatewayRxSerialData + sizeof(aWriteRam)),sizeof(aOK))))
  {
     status = gPopErrNone_c;
     //DelayMs(100);
     // if end of image, end of sector or end of ram  prepare to copy to flash 
     if( (savedRam == mMP_MaxRamSize_c)) 
       MPIspState = eSendPrepSector;
     else
       // go back to send data after checksum
       MPIspState = eSendDataToRam;
     // get array pointer
     pChecksumStr = aChecksumStr;
     // transform checksum into string
     bytesToSend =  MP_Uint32ToASCII(checksum, (uint32_t *)(&pChecksumStr));
     // set last byte to CR character
     pChecksumStr[bytesToSend-1] = 0x0D;
     
     statusOffset = bytesToSend;
     // send checksum
     MPGatewaySendLpcCommand(bytesToSend,pChecksumStr);
     // clean lines 
     linesSent = 0;
     // clean checksum
     checksum = 0;
   
  }
  else if(MPIspState == eSendPrepSector)// && (PopMemCmp(aOK,(void *)(gaGatewayRxSerialData + sizeof(aWriteRam)),sizeof(aOK))))
  {
    status = gPopErrNone_c;
    //DelayMs(100);
    // send prepare sector
    if (TargetNodeType == LPC1765)
    {
      // send prepare sector    
      statusOffset = sizeof(aPrepareAllFlash);
      // go to copy to flash    
      MPGatewaySendLpcCommand(sizeof(aPrepareAllFlash),aPrepareAllFlash);
    }
    else 
    {
      // send prepare sector    
      statusOffset = sizeof(aPrepareAllFlash1768);
      // go to copy to flash    
      MPGatewaySendLpcCommand(sizeof(aPrepareAllFlash1768),aPrepareAllFlash1768);
    }     
    // go to copy to flash        
    MPIspState = eSendCopyRamToFlash;
  }
  else if(MPIspState == eSendCopyRamToFlash)// && (PopMemCmp(aOK,(void *)(gaGatewayRxSerialData + sizeof(aWriteRam)),sizeof(aOK))))
  {
    status = gPopErrNone_c;
    //DelayMs(100);
    // if we need to continue sending go back
    if( imageSize > imageSent)
      MPIspState = eSendWriteRam;
    else
      MPIspState = eEndOfTransmission;
    
    savedRam = 0;
    linesSent = 0;
    
    // get array pointer
    pChecksumStr = aChecksumStr;
    // transform checksum into string
    bytesToSend =  MP_Uint32ToASCII(imageInFlash, (uint32_t *)(&pChecksumStr));
    // set last byte to space character
    pChecksumStr[bytesToSend-1] = 0x20;
    // copy asciii value of flash address to copy from RAM
    PopMemCpy(&aCopyToFlash[2],pChecksumStr, bytesToSend );
    
    // copy address and size of RAM data
    PopMemCpy(&aCopyToFlash[2+bytesToSend],aRamAddrSize,sizeof(aRamAddrSize));
     
    statusOffset = 2+bytesToSend+sizeof(aRamAddrSize);
    // RAM has been saved into FLASH    
    MPGatewaySendLpcCommand(2+bytesToSend+sizeof(aRamAddrSize),aCopyToFlash);  
    imageInFlash = imageSent;
    
  }            
  // if we reached end of transmission of image or there was an error, reset 
  // state machine state and turn off isp commander
  else if(MPIspState == eEndOfTransmission )    
  {
    // Give it some time to complete last copy
    DelayMs(100);
    // don't change status it is err none, but we want to finish and reset
  }
  
  
  // if something goes wrong...
  if(status != gPopErrNone_c)
  { 
    // Reset
    ispCommanderModeFlag=false;
    MPIspState = eSendQnMark;
    // Reset LPC to allow user code take control
    MPResetLPC();  
  }
    
}

/*

  MP_IsLpcImageChecksumCorrect
  Verifies image in storage is valid

  User manual LPC17xx section 32.3.11 Criterion for Valid User Code:
  "The reserved Cortex-M3 exception vector location 7 ( offset 0x001c in the 
  vector table) should contain the 2's complement of the checksum of table 
  entries 0 through 6. This causes the checksum for the first 8 table entries 
  to be 0. The bootloader code checksums the first 8 locations in sector 0 of 
  the flash. If the result is 0, then execution control is transferred to the 
  user code."

  It is important to notice that the image doesn't contain the checksum by 
  default, it needs to be added by the PC application downloader.

*/
bool MP_IsLpcImageChecksumCorrect(void)
{
  uint8_t vectors[32];
  uint8_t dwordIndex=0;
  uint32_t checksum=0;
  uint32_t dword=0;
  
  if(!PopStorageReadBytes(vectors, gPopLpcStorageStart_c, sizeof(vectors)))
  {
    // calculate checksum
    for (dwordIndex = 0; dwordIndex < 28; dwordIndex+=4)
    {
      dword = (vectors[dwordIndex + 3] << (3 * 8)) | (vectors[dwordIndex + 2] << (2 * 8)) | (vectors[dwordIndex + 1] << (1 * 8)) | (vectors[dwordIndex] << (0 * 8));
      checksum += dword;
    }
    checksum = ~checksum + 1;
    dword = (vectors[dwordIndex + 3] << (3 * 8)) | (vectors[dwordIndex + 2] << (2 * 8)) | (vectors[dwordIndex + 1] << (1 * 8)) | (vectors[dwordIndex] << (0 * 8));

    return checksum == dword;
   }
 return false;
}

/*
  MP_LpcCheckStoredImage

  Verifies that the new image contains the appropriate signature and checksum

  Returns gPopErrNoIma
*/
popErr_t MP_LpcCheckStoredImage(void)
{
  uint16_t iSum = 0;      //store calculated checksum
  uint8_t iTemp[255];     //temporal data
  uint32_t iOffSet = gPopLpcStorageStart_c;  // offset after signature
  uint32_t iEndOfImage;   //store image length
  uint16_t iChecksum = 0; //stored checksum to compare with iSum
  uint8_t iCount;
  
  // Find the image length.
  iEndOfImage = gsPopOtaUpgradeInfo.imageSize-2;

  //If image > available memory something is wrong
  if (iEndOfImage > gPopLpcAvailableMemory_c)
    return gPopErrNoImage_c;  
  
  // Calculate checksum to validate what is in the storage
  while(iOffSet < (iEndOfImage + gPopLpcStorageStart_c))
  {
    //Get 255 bytes from storage
    if(gPopErrNone_c != PopStorageReadBytes((void *)iTemp, iOffSet,sizeof(iTemp)))
      return gPopErrNoImage_c;

    // keep generating the check sum
    for(iCount=0;iCount < (sizeof(iTemp));iCount++)
    {
      iSum += iTemp[iCount];
      if((iOffSet+iCount) == iEndOfImage+gPopLpcStorageStart_c-1)
        break;
    }
    //Increase offset
    iOffSet+=sizeof(iTemp);
  }

  //Get the stored checksum
  if(gPopErrNone_c != PopStorageReadBytes((void *)&iChecksum, iEndOfImage + gPopLpcStorageStart_c ,2))
    return gPopErrNoImage_c;

  // If the checksums are different the stored images has an error.
  if (iSum != iChecksum)
  {
    // Inform the current error.
    return gPopErrFailed_c;
  }

  /* everything ok */
  return gPopErrNone_c;
}

popErr_t PopNwkMgmtLpcUpgradeResetToNewImage(sPopGwResetToNewImageReq_t * pResetReq)
{
/* send OTA req */
  if(!pResetReq->reqType) 
    /* swap image id if necessary */
    OtaToNative16(pResetReq->appId);

  // check hw id is for LPC
  if(pResetReq->hwId != gPopLpcUpgradeHwId_c){
    if(!pResetReq->reqType)
      NativeToOta16(pResetReq->appId);
    return gPopErrBadHwId_c;   
  }    
  
  // check if image is valid
  if(!MP_IsLpcImageChecksumCorrect()){
    if(!pResetReq->reqType)
      NativeToOta16(pResetReq->appId);
    return gPopErrNoImage_c;
  }
  
  if(MP_LpcCheckStoredImage()){
    if(!pResetReq->reqType)
      NativeToOta16(pResetReq->appId);
    return gPopErrNoImage_c;
  }
  
  // Enter bootloader ISP
  MPEnterIspCommandHandler();
  // Clear Serial Rx data buffer
  PopSerialRestartModule();
  // Call LPC bootloader state machine
  MPIspStateHandler();

  return gPopErrNone_c;
}
#endif
