/************************************************************************************
* Includes the UART Utils.
*
* (c) Copyright 2008, Freescale Semiconductor, Inc. All rights reserved.
*
* No part of this document must be reproduced in any form - including copied,
* transcribed, printed or by any electronic means - without specific written
* permission from Freescale.
*
* Last Inspected:
* Last Tested:
*
************************************************************************************/
#include "board_config.h"
#include "UartUtil.h"
#include "PopCfg.h"
#include <Hw/TIDriverLib/source/Interrupt.h>
#include <Hw/TIDriverLib/inc/hw_ints.h>
#include <Hw/TIDriverLib/source/gpio.h>
#include <Hw/TIDriverLib/inc/hw_memmap.h>
#include <Hw/TIDriverLib/source/ioc.h>
#include <Hw/TIDriverLib/inc/hw_ioc.h>

/******************************************************************************/
/******************************************************************************/

#if gPopSerial_d
/************************************************************************************
*************************************************************************************
* Private macros
*************************************************************************************
************************************************************************************/

/************************************************************************************
*************************************************************************************
* Private prototypes
*************************************************************************************/
static uint8_t HexToAscii(uint8_t u8Hex);
void UartEventRead1(UartReadCallbackArgs_t* args);
void UartEventWrite2(UartWriteCallbackArgs_t* args);
void UartEventRead2(UartReadCallbackArgs_t* args);
void UartEventWrite1(UartWriteCallbackArgs_t* args);
void GpioUart1Init(void);
void GpioUart2Init(void);

/************************************************************************************
*************************************************************************************
* Private type definitions
*************************************************************************************
************************************************************************************/

/************************************************************************************
*************************************************************************************
* Public memory declarations
*************************************************************************************
************************************************************************************/
/* Variables */

/* Prototypes */
extern void PopSerialTxIsr(UartWriteCallbackArgs_t* args);
extern void PopSerialRxIsr(UartReadCallbackArgs_t* pArgs);
#endif
/************************************************************************************
*************************************************************************************
* Private memory declarations
*************************************************************************************
************************************************************************************/

UartReadStatus_t gu8SCIStatus = gUartReadStatusComplete_c;
volatile uint8_t gu8SCIDataFlag = false;
uint16_t gu16SCINumOfBytes = 0;

/************************************************************************************
*************************************************************************************
* Public functions
*************************************************************************************
************************************************************************************/
#if gPopSerial_d
/************************************************************************************  
* Uart_Init
*
* Initializate the Uart module.
*************************************************************************************/
UartErr_t res;    // made global to avoid compiler warnings if res not used.

void Uart_Init(uint8_t *mUARTRxBuffer) 
{
  UartConfig_t pConfig;
  UartCallbackFunctions_t pCallback;

  IntMasterDisable();

#if gUart_PortDefault_d == UART_1
  res = UartGetStatus(UART_1);
  //Consult the current status of this serial port
  if( res != gUartErrUartNotOpen_c )
  {
    /* if the uart is already open then do nothing */
	  IntMasterEnable();
	return;
  }

  //initialize GPIOs for UART1 and UART2 
  GpioUart1Init();  

  //configure the uart parameters 
  pConfig.UartParity = gUartParityNone_c;
  pConfig.UartStopBits = gUartStopBits1_c;
  pConfig.UartBaudrate = gUartDefaultBaud_c;
  pConfig.UartFlowControlEnabled = gUart1_EnableHWFlowControl_d;
  pConfig.UartRTSActiveHigh = false;

  
  //mount the interrupts corresponding to UART driver
  //IntRegister(INT_UART0, UartIsr1);
  IntPrioritySet(INT_UART1, 2<<5);

  //initialize the uart
  res = UartOpen(UART_1,gPlatformClock_c);  

  if( res != gUartErrNoError_c ){
	  IntMasterEnable();
    return;
  }

  res = UartSetConfig(UART_1, &pConfig);  

#if gUart1_EnableHWFlowControl_d == true
  UartSetCTSThreshold(UART_1, gUart_RxFlowControlSkew_d);
#endif

  //configure the Uart Rx and Tx Threshold
  res = UartSetTransmitterThreshold(UART_1, 1);
  res = UartSetReceiverThreshold(UART_1, 2);

  //set pCallback functions
  pCallback.pfUartWriteCallback = PopSerialTxIsr;
  pCallback.pfUartReadCallback = PopSerialRxIsr;
  res = UartSetCallbackFunctions(UART_1, &pCallback);

#endif

#if gUart_PortDefault_d == UART_2
  res = UartGetStatus(UART_2);
  //Consult the current status of this serial port
  if( res != gUartErrUartNotOpen_c )
  {
    /* if the uart is already open then do nothing */
	  IntMasterEnable();
    return;
  }
  //initialize GPIOs for UART1 and UART2
  GpioUart2Init();  

  //configure the uart parameters
  pConfig.UartParity = gUartParityNone_c;
  pConfig.UartStopBits = gUartStopBits1_c;
  pConfig.UartBaudrate = gUartDefaultBaud_c;
  pConfig.UartFlowControlEnabled = gUart2_EnableHWFlowControl_d;
  pConfig.UartRTSActiveHigh = false;

  // mount the interrupts corresponding to UART driver
  //IntRegister(INT_UART1, UartIsr2);
  IntPrioritySet(INT_UART0, 2<<5);

  //initialize the uart
  res = UartOpen(UART_2,gPlatformClock_c);

  if( res != gUartErrNoError_c ){
	  IntMasterEnable();
    return;
  }
  
  res = UartSetConfig(UART_2,&pConfig);

#if gUart2_EnableHWFlowControl_d == true
  UartSetCTSThreshold(UART_2, gUart_RxFlowControlSkew_d);
#endif

  //configure the Uart Rx and Tx Threshold
  res = UartSetTransmitterThreshold(UART_2,1);  // SJS: was 1
  res = UartSetReceiverThreshold(UART_2,2);   // SJS: was 2

  //set pCallback functions
  pCallback.pfUartWriteCallback = PopSerialTxIsr;
  pCallback.pfUartReadCallback = PopSerialRxIsr;
  res = UartSetCallbackFunctions(UART_2,&pCallback);
  
#endif /* gUart2_Enabled_d */
  
  // global enable interrupts in AITC driver
  IntMasterEnable();
  
  res = UartReadData(gUart_PortDefault_d,mUARTRxBuffer,sizeof(mUARTRxBuffer),true);
}                                       

/************************************************************************************  
* GpioUart1Init
*
* This function initializate the gpio�s for the Uart1 module
*************************************************************************************/
void GpioUart1Init(void)
{
  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_UART1);

  IOCPinConfigPeriphInput(GPIO_A_BASE, GPIO_PIN_0, IOC_UARTRXD_UART1);
  GPIOPinTypeUARTInput(GPIO_A_BASE, GPIO_PIN_0);

  IOCPinConfigPeriphOutput(GPIO_A_BASE, GPIO_PIN_1, IOC_MUX_OUT_SEL_UART1_TXD);
  GPIOPinTypeUARTOutput(GPIO_A_BASE, GPIO_PIN_1);

#if gUart1_EnableHWFlowControl_d == true
  IOCPinConfigPeriphOutput(GPIO_A_BASE, GPIO_PIN_3, IOC_MUX_OUT_SEL_UART1_RTS);
  GPIOPinTypeUARTOutput(GPIO_A_BASE, GPIO_PIN_3); /* RTS */

  IOCPinConfigPeriphInput(GPIO_A_BASE, GPIO_PIN_2, IOC_UARTCTS_UART1);
  GPIOPinTypeUARTInput(GPIO_A_BASE, GPIO_PIN_2);  /* CTS */
#endif
}

/************************************************************************************  
* GpioUart2Init
*
* This function initializate the gpio�s for the Uart2 module
*************************************************************************************/
void GpioUart2Init(void)
{
  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_UART0);

  IOCPinConfigPeriphOutput(GPIO_C_BASE, GPIO_PIN_6, IOC_MUX_OUT_SEL_UART0_TXD);
  GPIOPinTypeUARTOutput(GPIO_C_BASE, GPIO_PIN_6);

  IOCPinConfigPeriphInput(GPIO_C_BASE, GPIO_PIN_7, IOC_UARTRXD_UART0);
  GPIOPinTypeUARTInput(GPIO_C_BASE, GPIO_PIN_7);

#if gUart2_EnableHWFlowControl_d == true
# error This UART does not support HW flow control
#endif
}


/*******************************************************************************  
* UartEventRead1 
*
* Callabck function for the reception process in the Uart1
*******************************************************************************/
void UartEventRead1(UartReadCallbackArgs_t* args)
{
  gu8SCIDataFlag = true;
  gu16SCINumOfBytes = args->UartNumberBytesReceived;
  gu8SCIStatus = args->UartStatus;
}

/*******************************************************************************  
* UartEventWrite1
*
* Callback function for the transmission interruption in the Uart1.
*******************************************************************************/
void UartEventWrite1(UartWriteCallbackArgs_t* args)
{
}

/*******************************************************************************  
* UartEventRead12
*
* Callabck function for the reception process in the Uart2
*******************************************************************************/
void UartEventRead2(UartReadCallbackArgs_t* args)
{
  gu8SCIDataFlag = true;
  gu16SCINumOfBytes = args->UartNumberBytesReceived;
  gu8SCIStatus = args->UartStatus;
}

/*******************************************************************************  
* UartEventWrite2
*
* Callback function for the transmission interruption in the Uart1.
*******************************************************************************/
void UartEventWrite2(UartWriteCallbackArgs_t* args)
{
}

/*******************************************************************************  
* HexToAscii
*
* This function converts an hexadecimal value in ascii code.
*******************************************************************************/
static uint8_t HexToAscii(uint8_t u8Hex)
{
  u8Hex &= 0x0F;
  return u8Hex + ((u8Hex <= 9) ? '0' : ('A'-10));
}

/*******************************************************************************  
* Uart_putchar
*
* This function print a byte in the serial interface
*******************************************************************************/
void Uart_putchar(uint8_t u8Char)
{
  UartWriteData(gUart_PortDefault_d,&u8Char,1);
}


/*******************************************************************************  
* Uart_Print
*
* This function transmit a string to the initializated Uart. 
*******************************************************************************/
void Uart_Print(uint8_t* pString)
{
  uint8_t WriteBuffer[1];
  uint8_t length = 0;
  uint16_t u8Timeout;
  while(pString[length] != '\0'){
    WriteBuffer[0] = pString[length];
    length++;
    u8Timeout = 0;
    while((UartWriteData(gUart_PortDefault_d,WriteBuffer,1) != gUartErrNoError_c) && (u8Timeout < gUartTimeout_d))
    {
      u8Timeout ++;
    }
  }
}

/*******************************************************************************  
* Uart_getchar
*
* Waits until a character is received in the Uart. 
*******************************************************************************/
char Uart_getchar(uint8_t *u8Char)
{
  while(true != gu8SCIDataFlag){}
  gu8SCIDataFlag = false;
  UartGetByteFromRxBuffer(gUart_PortDefault_d, u8Char);
  
  return *u8Char;
}

/*******************************************************************************  
* Uart_Tx
*
* Transmits a buffer to the UART. 
*******************************************************************************/
void Uart_Tx(const uint8_t * msg, uint8_t length)
{
  uint8_t WriteBuffer[1];
  uint32_t i;
  uint16_t u8Timeout;

  for(i=0;i<length;i++)
  {
    if(*msg == '\n' ){
      WriteBuffer[0] = '\r';
      u8Timeout = 0;
      while((UartWriteData(gUart_PortDefault_d,WriteBuffer,1) != gUartErrNoError_c) && (u8Timeout < gUartTimeout_d))
      {
        u8Timeout ++;
      }
    }
    WriteBuffer[0] = *msg;
    
    u8Timeout = 0;
    while((UartWriteData(gUart_PortDefault_d,WriteBuffer,1) != gUartErrNoError_c) && u8Timeout < gUartTimeout_d )
    {
      u8Timeout ++;
    }
   
    msg++;
  }
}

void Uart_Poll(uint8_t *pRxBuffer)
{
  if (gu8SCIDataFlag) {
    gu8SCIDataFlag = false;
    UartGetByteFromRxBuffer(gUart_PortDefault_d, pRxBuffer);
  } 
}

/*******************************************************************************  
* AsciitoHex
*
* This function converts an ascii code in a hexadecimal value.
*******************************************************************************/
uint8_t AsciitoHex(uint8_t u8Ascii)
{
  if ((u8Ascii > 47) && (u8Ascii <= 57)){
    return u8Ascii - '0';
  }
  else{ 
    if((u8Ascii > 64) && (u8Ascii <= 70))
    { 
      return (u8Ascii - 'A' + 10);
    }
    else if((u8Ascii > 96) && (u8Ascii <= 102))
    {
      return (u8Ascii - 'a' + 10);
    } 
  }

  return 0;
}

/*******************************************************************************  
* Uart_PrintHex
*
* This function transmit a hexadecimal value to the initializated Uart. 
*******************************************************************************/
void Uart_PrintHex(uint8_t *pu8Hex, uint8_t u8len, uint8_t u8flags)
{
  uint8_t hexString[3];
  if(! (u8flags & gPrtHexBigEndian_c))
    pu8Hex = pu8Hex + (u8len-1);
  
  while(u8len)
  {
    hexString[2] = '\0';
    hexString[1] = HexToAscii( *pu8Hex );
    hexString[0] = HexToAscii((*pu8Hex)>>4);
    
    Uart_Print((uint8_t*) hexString);
    
    if(u8flags & gPrtHexCommas_c)
      Uart_Print((uint8_t*)",");
    pu8Hex = pu8Hex + (u8flags & gPrtHexBigEndian_c ? 1 : -1);
    u8len--;
  }
  if(u8flags & gPrtHexNewLine_c)
    Uart_Print((uint8_t*)"\n");
}

/*******************************************************************************  
* Uart_PrintByteDec
*
* This function transmit a decimal value to the initializated Uart
*******************************************************************************/
void Uart_PrintByteDec(uint8_t u8Dec)
{
  uint8_t decString[3];
  uint8_t  tem;
  tem = u8Dec & 0x0F;
  decString[2] = '\0';
  decString[0]= tem/10;
  decString[1] = tem%10;

  tem = ((u8Dec>>4) & 0x0F);
  tem = tem * 16;
  
  if(decString[1] + (tem%10) >= 10){
    decString[0] = ( decString[0] + (tem/10) + 1 + '0');
    decString[1] = ( decString[1] + (tem%10) - 10 +'0');
  }
  else{
    decString[0] = ( decString[0] + (tem/10) + '0');
    decString[1] = ( decString[1] + (tem%10) + '0');
  }
  Uart_Print((uint8_t*) decString);
}

/*******************************************************************************  
* Uart_PrintShortDec
*
* This function transmit a decimal value to the initializated Uart
*******************************************************************************/
void Uart_PrintShortDec(uint16_t u16Dec)
{
  uint8_t decString[6];
  uint16_t  temp;

  decString[5] = '\0';
  temp = u16Dec;

  decString[4] = (temp%10) + '0';
  temp = temp/10;
  decString[3] = (temp%10) + '0';
  temp = temp/10;
  decString[2] = (temp%10) + '0';
  temp = temp/10;
  decString[1] = (temp%10) + '0';
  temp = temp/10;

  decString[0] = temp + '0';

  Uart_Print((uint8_t*) decString);
}

/*******************************************************************************  
* mem_cmp 
*
* This function compare two strings, return false when these are diferents or
* true when are equals.
*******************************************************************************/
bool mem_cmp(uint8_t * str1, uint8_t * str2, uint8_t sz)
{
  uint8_t i;
  for(i=0; i<sz; i++)
  {
    if( (str1[i]) != (str2[i]))
    {
      return false;
    }
  }
  return true;
}
#endif
