/***************************************************************************
  PopPhy.h
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  Supports the radio PHY for PopNet. Generic interface. For implementation, see 
  PopPhy.c.

  03/31/2007 dg  Created by Drew Gislason
****************************************************************************/
#ifndef POPPHY_H
#define POPPHY_H

#include "PopBsp.h"
#include "PopNet.h"
#include <stdint.h>
#include <stdbool.h>

/***************************************************************************
  Types & Defines
***************************************************************************/

// this property allows for random LQI (needed for some tests)
#ifndef gPopRandomLqi_d
 #define gPopRandomLqi_d  gDisabled_c
#endif

// PHY has sent the NWK layer a packet (must be same EvtId in PopNwk.h)
#define gPopEvtPhyIndication_c            0x12


// Status for data indication. Used in PHY layer only.
#define gPopPhyIndicationSuccess_c 0x00
#define gPopPhyRadioBackToListen_c 0x01


/***************************************************************************
  Prototypes
***************************************************************************/

void PopInitPhy(void);
void data_indication_execute(void);

void MCPSDataIndicationCallback(void);
void PopPhyBackToListen(void);
void PopPhyProcessRadioMsg(void);
bool PopNwkFilterPackets(sPopNwkDataIndication_t *pFrame);
void PlatformPortInit(void);

#if gPopAppSniffer_d
void PopPhyAppSnifferReceive(void);
extern uint32_t PopGetCurrentTimeMs(void);
#endif

/***************************************************************************
  Externs
***************************************************************************/
extern uint8_t u8ScanValPerChann[];

#endif  // POPPHY_H

