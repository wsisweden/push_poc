/***************************************************************************
  PopMain.c
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  This module defines the main entry point to PopNet, as well as the high-level 
  task and timer code.

****************************************************************************/
#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"
#include <stdarg.h>


// only pick 1 unit test
#ifdef gPopMemUnitTest_d
void PopMemUnitTest(void);
#endif

#ifdef gPopNvUnitTest_d
void PopNvUnitTest(void);
#endif

#ifdef gPopFlashUnitTest_d
void PopFlashUnitTest(void);
#endif


/***************************************************************************
  Local Types & Defines
***************************************************************************/


/***************************************************************************
  Local Prototypes
***************************************************************************/
void PopInitializeTasks(void);
void PopRunTasks(void);


/***************************************************************************
  Globals & Externs
***************************************************************************/

// globals to prevent compiler warnings 
uint8_t giPopForever = 1; // to prevent compiler warnings on forever loops


/***************************************************************************
  Code
***************************************************************************/
/*
  Main entry point to PopNet. Note how simple!
*/
int main(void)
{
  // initialize the tasks
  PopInitializeTasks();

  // unit test the memory routines (must be AFTER initializing BSP so watchdog doesn't fire)
#ifdef gPopMemUnitTest_d
  PopMemUnitTest();
#endif

#ifdef gPopNvUnitTest_d
  PopNvUnitTest();
#endif

#ifdef gPopFlashUnitTest_d
  PopFlashUnitTest();
#endif

  // run the tasks
  PopRunTasks();

  return 0;
}

// for converting to hex or decimal
const char gszPopDigits[] = "0123456789ABCDEF";

/*
  Converts hex to ascii
*/
void PopHex2Ascii(char *psz, uint16_t iHex)
{
  uint8_t i;
 
  i = 4;
  psz[i] = 0;
  while(i)
  {
    --i;
    psz[i] = gszPopDigits[iHex & 0xf];
    iHex = iHex >> 4;
  }
}

/*
  Converts a 2 digit integer to ascii
*/
void PopInt2Ascii(char *psz, int_fast8_t iInt)
{
  uint8_t i;

  i = 2;
  psz[i] = 0;
  while(i)
  {
    --i;
    psz[i] = gszPopDigits[iInt % 10];
    iInt = (int_fast8_t)(iInt / 10);
  }
}

/*
  Converts ascii to hex. Can do one or 2 bytes.
*/
uint16_t PopAToHex(char *psz)
{
  char c;
  uint8_t  iLastDigit = 0;
  uint16_t iValue = 0;
  for(;;)
  {
    c = *psz;
    if(c >= 'a' && c <= 'f')
      iLastDigit = 10 + c - 'a';
    else if(c >= 'A' && c <= 'F')
      iLastDigit = 10 + c - 'A';
    else if(c >= '0' && c <= '9')
      iLastDigit = c - '0';
    else
      break;

    iValue = (iValue << 4) + iLastDigit;
    ++psz;
  }
  return iValue;
}

/*
  Is this a decimal digit?
*/
bool PopIsDigit(char c)
{
  return (c>='0' && c<='9') ? 1 : 0;  // compiler bug work-around, not all compilers return true/false
}

/*
  Is this a hex digit?
*/
bool PopIsXDigit(char c)
{
  if(PopIsDigit(c))
    return true;
  if((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))
    return true;
  return false;
}

/*
  Reverse the digits in this string.
*/
void PopReverseDigits(char *psz)
{
  uint8_t i;
  uint8_t iLen;
  char c;

  iLen = PopStrLen(psz);
  if(!iLen)
    return;
  --iLen;
  i = 0;
  while(i < iLen)
  {
    c = psz[i];
    psz[i] = psz[iLen];
    psz[iLen] = c;
    ++i;
    --iLen;
  }
}

/*
  PopIToA

  Convert from a value to a string.
*/
void PopIToA(char *szValue, uint16_t iValue, uint8_t iRadix)
{
  uint8_t i;

  // value 0 is always 1 digit
  if(!iValue)
  {
    szValue[0] = '0';
    szValue[1] = 0;
    return;
  }

  // convert from number to string
  i = 0;
  while(iValue)
  {
    szValue[i++] = gszPopDigits[iValue % iRadix];
    iValue = iValue / iRadix;
  }
  szValue[i] = 0;
  PopReverseDigits(szValue);
}

/*
  PopSprintf

  Small sprintf(), supports the following formats (szFmt):
  %c     print a character
  %s     print a string
  %x     print a hex number
  %d     print an (unsigned) decimal number
  %04x   print a 4-digit hex number, pad with 0s
  %3x    print lower 3 digits of the hex number, pad with spaces

  Returns the length of the resulting szDst string (not including '\0')
*/
uint8_t PopSprintf(char *szDst, char *szFmt, ...)
{
  va_list args;
  uint8_t i;
  uint8_t iStrLen; 
  uint8_t iSprintfLen;
  uint8_t iFillWidth;
  uint8_t iOrgPos;
  char iFillValue;
  char c = 0;         // remove compiler warnings
  char *psz = 0;      // remove compiler warnings
  uint16_t iValue;
  char szValue[6];   // can handle up to 65535 decimal (plus NULL)

  // start up the va list
  va_start(args, szFmt);

  // walk through format string
  iSprintfLen = 0;
  i = 0;
  while(szFmt[i])
  {
    if(szFmt[i] != '%')
    {
      szDst[iSprintfLen++] = szFmt[i++];
    }

    // must be a format character
    else
    {
      // remember original position in case of invalid formatting
      iOrgPos = i;

      // skip the '%'
      ++i;

      // see if the fill value should be ' ' or '0' (pad with 0s or spaces)
      iFillValue=' ';
      if(szFmt[i]=='0')
        iFillValue='0';

      // see if there is a fill width, or just whatever the # works out to be
      iFillWidth = 0;
      while(PopIsDigit(szFmt[i]))
      {
        iFillWidth = (uint8_t)(iFillWidth * 10) + (szFmt[i]-'0');
        ++i;
      }

      // calculate string length for the character
      iStrLen = 0xff;
      switch(szFmt[i])
      {
        // character, always length 1
        case 'c':
          c = va_arg (args, int);
          iStrLen = 1;
        break;

        // determine ptr to the string and width of string
        case 's':
          psz = va_arg (args, char *);
          iStrLen = PopStrLen(psz);
        break;

        // determine value for the # and the width of the #
        case 'x':
        case 'd':
          // determine value for the hext #, and 
          iValue = va_arg(args, int);
          PopIToA(szValue, iValue, (szFmt[i] == 'x') ? 16 : 10);
          psz = szValue;
          iStrLen = PopStrLen(psz);
        break;
      default:
        break;
      }

      // if not a valid format, then just copy the format string and continue;
      if(iStrLen == 0xff)
      {
        ++i;
        PopMemCpy(&szDst[iSprintfLen], &szFmt[iOrgPos], i - iOrgPos);
        continue;
      }

      // fill in the spaces
      if(iStrLen < iFillWidth)
      {
        PopMemSet(&szDst[iSprintfLen], iFillValue, iFillWidth - iStrLen);
        iSprintfLen += (iFillWidth - iStrLen);
      }

      // fill in the data (char, number, str) itself
      switch(szFmt[i])
      {
        case 'c':
          szDst[iSprintfLen++] = c;
        break;
        case 'x':
        case 'd':
        case 's':
          PopMemCpy(&szDst[iSprintfLen], psz, iStrLen);
          iSprintfLen += iStrLen;
        break;
      default:
        break;
      }

      // skip the format character
      ++i;
    }
  }

  // done with va list
  (void)va_end (args);

  // end the string
  szDst[iSprintfLen] = 0;

  // return string length
  return iSprintfLen;
}

#if gPopLcdEnabled_d
// TODO: determine proper place for helper functions (not PopMain.c?)
// a character string to hold the PopNet NWK ID, etc...
char gszPopLcdLine2[gPopLcdCols_c + 1];

/*
  Create a string (for display on LCD) that looks like:

  Pop 0F00 25 0001
*/
void PopDisplayLcdNetInfo(void)
{
  PopMemCpy(gszPopLcdLine2, "Pop ", 4);
  PopHex2Ascii(gszPopLcdLine2 + 4, PopNwkGetPanId());
  gszPopLcdLine2[8] = ' ';
  PopInt2Ascii(gszPopLcdLine2 + 9, PopNwkGetChannel());
  gszPopLcdLine2[11] = ' ';
  PopHex2Ascii(gszPopLcdLine2 + 12, PopNwkGetNodeAddr());
  PopLcdWriteString(2, gszPopLcdLine2);
}

#endif // gPopLcdEnabled_d

/*
  PopIsZeros

  iSize is the size of the array in bytes (not bits!)
  
  Verify array is all 0s. Returns true if so, false if not.
*/
bool PopIsZeros(uint8_t *pArray, uint8_t iSize)
{
  while(iSize--)
  {
    if(*(pArray + iSize))
      return false;
  }
  return true;
}

/*
  PopBitOr

  Or in a bit into an array.
*/
void PopBitOr(uint8_t *pArray, uint8_t iBit)
{
  pArray += (iBit / 8);
  *pArray |= (1 << (iBit & 0x7));
}

/*
  PopBitAnd

  And off a bit from an array.
*/
void PopBitAnd(uint8_t *pArray, uint8_t iBit)
{
  pArray += (iBit / 8);
  *pArray &= ~(1 << (iBit & 0x7));
}

