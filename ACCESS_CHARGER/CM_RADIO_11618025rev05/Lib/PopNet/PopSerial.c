/******************************************************************************
  PopSerial.c
  Copyright (c) 2006-2010, San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is released
  under Non-Disclosure and is not a public work. This notice is not to be
  removed.

  This module contains the high-level serial I/O, including passing the serial data
  to the application and the serial gateway. This module only supports one serial
  port at a time, that can either be used for raw serial I/O, or with the serial
  gateway.

  The interrupt handlers and defines are found in PopBsp.c and PopBsp.h.

  The properties (#defines) from PopCfg.h that affect serial I/O are:
  gPopSerial_d             - enable serial I/O
  gPopGateway_d            - enable serial gateway
  gPopSerialBaudRate_c     - baud rate (9600, 19200, 38400, etc...)
  gPopSerialMinGapMs_c     - time to wait after last character received before
                             sending packet to the application
  gPopSerialPort_c         - determines which serial port (COM1 or COM2) is supported
  gPopSerialRxBufferSize_c - sets the size of the receive ring buffer
  gPopSerialTxBufferSize_c - sets the size of the transmit ring buffer

******************************************************************************/
#include "PopSerial.h"
#include <Hw/TIDriverLib/source/interrupt.h>
#include <stddef.h>

/**********************************
  Data Types Definitions
***********************************/

/*
  Enable or disable blocking on gateway serial output. If enabled, then the gateway waits on
  each packet to complete. If disabled, then it will only wait if the Tx buffer is full.
*/
#ifndef gPopGatewayBlocking_d
 #define gPopGatewayBlocking_d  true
#endif

typedef struct _tagsIncomingTable
{
  uint8_t iLength;
  popNwkAddr_t aAddrArray[1];
} sIncomingTable_t;

// structure used by PopNwkInitCommonValues
typedef struct sPopGatewayInitValues_tag
{
  aPopMacAddr_t     aMacAddr;
  popNwkAddr_t      iNwkAddr;
  popChannel_t      iChannel;
  popChannelList_t  iChannelList; // channel list for PopNwkStartNetwork()
  popPanId_t        iPanId;
  popMfgId_t        iMfgId;
  popNwkJoinOpts_t  iJoinOptions; // form/join options
  popTimeOut_t      iScanTime;    // scan time for PopNwkStartNetwork()
  popAddrCount_t    iAddrCount;   // # of addresses to ask for when joining via PopNwkStartNetwork()
} sPopGatewayInitValues_t;

typedef struct sPopGatewayVersionRsp_tag
{
  uint16_t    iVersion;             // gPopNetVersion_c
  uint16_t    iApplicationId;   // gPopAppId_c
  uint16_t    iMfgId;                // gPopMfgId_c
  uint8_t      iHwId;                // TARGET_BOARD
} sPopGatewayVersionRsp_t;

// for returning the address pool
typedef struct sPopGatewayAddrPool_tag
{
  popAddrCount_t  iNextAvailable;
  popAddrCount_t  iAddrCount;
} sPopGatewayAddrPool_t;

/*
  PopNet contains instrumentation for debugging.
*/
typedef struct sPopGatewayGetHighWaterMarks_tag
{
  uint16_t      iStackLeftAtHWM;      /* at high water mark */
  uint8_t       iEventsLeftAtHWM;
  uint8_t       iTimersLeftAtHWM;
  uint8_t       iDuplicatesLeftAtHWM;
  uint8_t       iRetriesLeftAtHWM;
  uint8_t       iRouteDiscoveriesLeftAtHWM;
  uint16_t      iStackSize;           /* total size */
  uint8_t       iMaxEvents;
  uint8_t       iMaxTimers;
  uint8_t       iDuplicateEntries;
  uint8_t       iRetryEntries;
  uint8_t       iRouteDiscoveryEntries;
  uint16_t      iStackLeftCurrent;    /* current left */
  uint8_t       iEventsLeftCurrent;
  uint8_t       iTimersLeftCurrent;
  uint8_t       iDuplicatesLeftCurrent;
  uint8_t       iRetriesLeftCurrent;
  uint8_t       iRouteDiscoveriesLeftCurrent;
} sPopGatewayGetHighWaterMarks_t;

// this structure must match the sPopNwkDataRequest_t structure
typedef struct sPopGatewayDataRequest_tag
{
  popNwkAddr_t  iDstAddr;        // 0-n or 0xffff for broadcast
  uint8_t        iRadius;        // radius 0xff means forever. Usually set to
  popNwkDataReqOpts_t iOptions;  // discover route, security enable
  uint8_t        iPayloadLength;
  uint8_t        aPayload[1];
} sPopGatewayDataRequest_t;


// union of types received by the gateway
typedef union sPopGatewayData_tag
{
  sPopGatewayInitValues_t   sNwkInitValues; //  gPopEvtInitNwkValues_c
  sPopGatewayDataRequest_t  sNwkDataRequest;
} sPopGatewayData_t;


// structure for gPopGwCmdHeapCheck_c
typedef struct sPopGatewayHeapCheck_tag
{
  popHeapSize_t  iFree;           // total free bytes right now
  popHeapSize_t  iLargestFree;
  popHeapSize_t  iMinFree;        // minimum free (high water mark)
  popHeapSize_t  iBadBlockOffset; // offset in heap of bad block, 0xffff (gPopMemHeapOk_c) if everything is OK
} sPopGatewayHeapCheck_t;

// <SJS: Added Serial Tx Rx State Machines>
typedef uint8_t popGatewayState_t;
#define gPopGatewayState_WaitingStx_c   0
#define gPopGatewayState_GetGroupID_c   1
#define gPopGatewayState_GetCmdID_c     2
#define gPopGatewayState_GetLen_c       3
#define gPopGatewayState_Data_c         4
#define gPopGatewayState_GetXOR_c       5

#if DEBUG >= 1
typedef struct sPopAppDebugCount_tag
{
  uint8_t   iCmd;
  uint32_t  iCount;
} sPopAppDebugCount_t;
sPopAppDebugCount_t gsDebugCount;
extern void PopAppSendCountOTA(sPopAppDebugCount_t sDebugCount);
// </SJS:>
#endif //DEBUG >= 1

#if gPopNeedPack_c
#pragma pack()
#endif


/***************************************************************************
  Prototypes
***************************************************************************/
#if gNodeType_c == STATISTICS_MODULE
extern bool MpEventIsOutOfRange(popEvtId_t iEvtId);
#endif

#if gPopSerial_d
void PopSerialTimerHandler(popTimerId_t  iTimerId);

#if gPopGateway_d
void PopGatewaySendDataRequest(sPopNwkDataRequest_t *pBuffer);
bool PopEventIsOutOfRange(popEvtId_t iEvtId);
#endif

/***************************************************************************
  Globals & Externs
***************************************************************************/

/* globals for this module only */
uint8_t   giPopSerialActive;
uint8_t   gaPopSerialTxBuf[gPopSerialTxBufferSize_c]; /* ISR tx buf */
uint8_t  *gpPopSerialTxBufHead = gaPopSerialTxBuf;    /* buffer head */
uint8_t  *gpPopSerialTxBufTail = gaPopSerialTxBuf;    /* buffer tail */
uint8_t   gaPopSerialRxBuf[gPopSerialRxBufferSize_c]; /* ISR rx buf */
uint8_t  *gpPopSerialRxBufHead = gaPopSerialRxBuf;    /* buffer head */
uint8_t  *gpPopSerialRxBufTail = gaPopSerialRxBuf;    /* buffer tail */
volatile uint8_t gfPopSerialStatus;                          /* current status of xmit and recv */
uint8_t   giPopSerialUartCRegSave;                    /* used by save/restore macros above */

// TODO: Back these up in NVM if needed (or document they need to be backed up by the app)
gPopBaudRate_t giPopSerialBaudRate = gPopSerialBaudRate_c;       /* baud rate, can be set by app */
uint16_t  giPopSerialMinGapMs = gPopSerialMinGapMs_c; /* mingap timeout, in ms. can be set by app */
#endif

#if gPopGateway_d && gPopSerial_d

// for limiting which events are sent through gateway to PC
// does NOT limit which events are sent from PC to gateway
// set low and high limit to 0 to disable
uint8_t giEvtLowLimit  = gPopEvtKey_c;          // default 2
uint8_t giEvtHighLimit = 0xff;  // default 0xff, which includes application events

sPopGatewayHeapCheck_t gsGatewayHeapCheck;

#if (gPopAssertLevel_c >= 2)
extern popHeapSize_t iPopMemLowestFree;
#endif

// when receiving data from PC to Gateway, place data here for processing
// this means there is always enough memory for gateway
uint8_t  gaGatewayRxSerialData[gPopSerialRxBufferSize_c];
// uint8_t  gaGatewayTxSerialData[gPopSerialTxBufferSize_c];   // SJS: Removed, because now allocated from the heap

#if gPopOverTheAirUpgrades_d
/* save image information */
extern void PopNwkMgmtOtaUpgradeCompleteRequest(sPopGwUpgradeCompleteReq_t* pOtaUpgradeCompleteReq);
extern popErr_t PopOtaUpgradePassThruRequest(sPopOtauGwSubseqBlockPassThruSaveReq_t * pNotFirstImageBlock);
extern sPopOtaUpgradeImageInfo_t gsPopOtaUpgradeInfo;
#endif

#endif // gPopGateway_d

#if (gPopAssertLevel_c >= 5)
extern bool gfReportAlloc;
#endif

#if DEBUG >= 2
extern sMpDebugCount_t gsMpDebug;
#endif
/***************************************************************************
  Code
***************************************************************************/


/*
  PopSerialTaskInit

  Initialize the serial subsystem in PopNet
*/
void PopSerialTaskInit(void)
{
#if gPopSerial_d
  // enables serial input
  giPopSerialActive=1;

  /* start the serial module at the right baudrate */
  (void)PopSerialSettings(giPopSerialBaudRate, giPopSerialMinGapMs);
#endif
}

/*
  PopSerialEventLoop
  This routine process serial events before sending them on to the
  application. Takes care of things like keeping track of mingap.
*/

void PopSerialTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
#if !gPopSerial_d
  (void)iEvtId;
  (void)sEvtData.pData;
#else

  // not yet active, don't send messages
  if(!giPopSerialActive)
    return;

  // handle serial task event
  switch(iEvtId)
  {
  case gPopSerByteReceivedEvt_c:
    (void)PopStartTimerEx(cSerialTaskId,gPopSerMinGapTimer_c,gPopSerialMinGapMs_c);
    break;

  // time to inform the application of serial input
    case gPopEvtTimer_c:
      PopSerialTimerHandler(sEvtData.iTimerId);
    break;

    case gPopSerTxDone_c:
      (void)PopSetAppEvent(gPopEvtSerialTxDone_c, NULL);
      break;
    default:
      break;
  } // end of switch

#endif
}


#if gPopSerial_d

/*
  Serial driver timer handler.
*/
void PopSerialTimerHandler(popTimerId_t  iTimerId)
{
  // the min gap has occurred, send the data to the application
  if (iTimerId == gPopSerMinGapTimer_c)
  {
    // only set 1 app event for serial data ready (not many)
    (void)PopSetUniqueEventEx(cAppTaskId, gPopEvtSerialDataReady_c, NULL);
  }
}


/******************************
 * Application Public Interface
 ******************************/

// <SJS: Added Serial Tx Rx State Machines>
/*
  PopSerialGet1Byte()

  Gets 1 byte out of the serial Rx buffer.
  Returns
    gPopErrNone_c     successful receive of a byte
    gPopErrEmpty_c    buffer is empty: no bytes to receive
    gPopErrBadParm_c  NULL pointer for data
*/
popErr_t PopSerialGet1Byte(void *pByte)
{
  uint8_t * pRxHead;
  uint8_t * pRxTail;

  if(!pByte)
    return gPopErrBadParm_c;

  // critical section, disable ints when grabbing head and tail
  IntMasterDisable();
  pRxHead = gpPopSerialRxBufHead;
  pRxTail = gpPopSerialRxBufTail;

  IntMasterEnable();
  // no data in the serial buffer, return nothing
  if ( pRxTail == pRxHead )
    return gPopErrEmpty_c;

  // got the data
  *(uint8_t *)pByte = *pRxTail;

  // update tail
  IntMasterDisable();

  ++gpPopSerialRxBufTail;
  if( gpPopSerialRxBufTail >= gaPopSerialRxBuf + gPopSerialRxBufferSize_c )
    gpPopSerialRxBufTail = gaPopSerialRxBuf;

  IntMasterEnable();

  return gPopErrNone_c;
}

/*
  PopSerialReceive



  paraeters:
    piLen:   [in/out]   in: length of app's input buffer.
                        out: n. bytes received to buffer, or on error n. bytes required.
    pBuffer: [in]       pointer to application receive buffer.

    return values:
    gPopErrNone_c     successful receive to application buffer
    gPopErrBadParm_c  invalid parameter

    side effects:
    on successful receive to app buffer, clears the gPopSerRxData_c status flag.
*/
popErr_t PopSerialReceive(uint16_t *piLen, void *pBuffer)
{
  uint8_t* pRxHead;
  uint8_t* pRxTail;
  uint16_t iChunkData1;
  uint16_t iChunkData2;

  // critical section, disable ints
  IntMasterDisable();

  // no longer have data (must disable both tx and rx which can affect this flag
  gfPopSerialStatus &= ~gPopSerRxData_c;

  // grab head and tail for comparison (must disable ints because rx int will affect tail
  pRxHead = gpPopSerialRxBufHead;
  pRxTail = gpPopSerialRxBufTail;

  IntMasterEnable();

  // check input parameters (but only if not trying to keep size small
  if ( !piLen || !*piLen || !pBuffer )
    return gPopSerInvalidArg_c;

  // no data. this case should not actually happen...
  if ( pRxTail == pRxHead )
  {
    *piLen = 0;
    return gPopErrNone_c;
  }

  // data is in one chunk...
  iChunkData1 = (uint16_t)(pRxHead - pRxTail);
  iChunkData2 = 0;
  if(pRxTail >= pRxHead)  // or two
  {
      iChunkData1 = (uint16_t)(gaPopSerialRxBuf + gPopSerialRxBufferSize_c - pRxTail);
      iChunkData2 = (uint16_t)(pRxHead - gaPopSerialRxBuf);
  }

  // application may not want all of the data (if small buffer size)
  if ( *piLen < iChunkData1 )
  {
    iChunkData1 = *piLen;
    iChunkData2 = 0;
  }
  else if(*piLen < iChunkData1 + iChunkData2)
    iChunkData2 = *piLen - iChunkData1;

  // copy data to app buffer in one chunk...
  if ( iChunkData1 )
    PopMemCpy( pBuffer, pRxTail, iChunkData1 );
  if ( iChunkData2 )  // or two
    PopMemCpy( (uint8_t *)pBuffer + iChunkData1, gaPopSerialRxBuf, iChunkData2 );

  // tell application how many bytes received
  *piLen = iChunkData1 + iChunkData2;

  // disable rx ints during tail update...
  IntMasterDisable();

  // data added to tail, update tail
  if ( !iChunkData2 )
    gpPopSerialRxBufTail += iChunkData1;
  else
    gpPopSerialRxBufTail = gaPopSerialRxBuf + iChunkData2;

  // tail wrapped
  if ( gpPopSerialRxBufTail >= gaPopSerialRxBuf + gPopSerialRxBufferSize_c )
    gpPopSerialRxBufTail = gaPopSerialRxBuf;

  // enable interrupts for serial receive
  IntMasterEnable();

  // let caller know it was successful
  return gPopErrNone_c;
}

/*
  Returns true when the serial port is done transmitting (so a function can wait until
  transmitting done).
*/
bool PopSerialDoneTransmitting(void)
{
  return (gfPopSerialStatus & gPopSerTxDone_c);
}

/*
  PopSerialGetLength

  Returns the current length of the data in the serial input buffer.
*/
uint8_t PopSerialGetLength(void)
{
  uint8_t *pRxHead;
  uint8_t *pRxTail;
  uint8_t iLen;

  // grab head and tail for comparison
  // critical section, disable serial ints in case interrupt adds to head
  PopDisableInts();
  pRxHead = gpPopSerialRxBufHead;
  pRxTail = gpPopSerialRxBufTail;
  PopEnableInts();

  // all in 1 chunk
  if(pRxHead > pRxTail)
    iLen = ((uint8_t)(pRxHead - pRxTail));

  // in 2 chunks
  else
  {
    iLen = (uint8_t)(gaPopSerialRxBuf + gPopSerialRxBufferSize_c - pRxTail) +
            (uint8_t)(pRxHead - gaPopSerialRxBuf);
  }

  return iLen;
}

/*
  PopSerialDebugStr

  Send a debug string out the serial port. Does not return until the data is out the
  serial port.
*/
void PopSerialDebugStr(char *s)
{
  uint8_t iLen;
  iLen = PopStrLen(s);
  // <SJS: Added Serial Debug Code 0x90>
  (void)PopSerialSend(iLen, s, gPopSerialSendFlags_Normal_c);
  // </SJS:>
}

/*
  PopSerialDebugHex

  Displays an 8-bit hex #.
*/
void PopSerialDebugHex(uint8_t iValue)
{
  char szHex[5];

  // converts to 4-digit ascii (e.g. "1A3E")
  // but send only lower byte
  PopHex2Ascii(szHex, iValue);
  szHex[4] = 0;
  PopSerialDebugStr(&szHex[2]);
}

/***************************
 * Private Module Functions
 ***************************/
/*
  PopSerialStatusClearFlag

  Clears the sticky receive status flag. Written in a general way in case
  other flags are added later.
*/
void PopSerialStatusClearFlag( uint8_t flag )
{
  PopSerialSaveAndDisableTxRxInts();
  gfPopSerialStatus &= ~flag;
  PopSerialRestoreTxRxInts();
}
#endif  // gPopSerial_d




/*
  PopCheckSum

  Calculate XOR checksum (for gateway packets)
*/
uint8_t PopCheckSum(uint16_t iBufferLen, void* pBuffer)
{
#if gPopGateway_d || gPopOverTheAirUpgrades_d || gPopUsingStorageArea_c
  uint8_t  *piRawBytes;
  uint8_t   iCheckSum = 0;
  uint16_t   i;

  piRawBytes =(uint8_t *)pBuffer;
  for (i=0; i < iBufferLen; i++)
    iCheckSum ^= piRawBytes[i];

  return iCheckSum;
#else
  (void)iBufferLen;
  (void)pBuffer;
  return 0;
#endif
}

#if gPopGateway_d && gPopSerial_d
/*
  PopGatewayPacketIsValid

  Is this gateway packet valid? Tests the checksum and verifies the length.
*/
uint8_t PopGatewayPacketIsValid(uint16_t iBytesReceived, sPopGatewayPacket_t  *psGatewayPacket)
{
  uint16_t   iLen;
  uint8_t   iCheckSum;

  // packet limited to gPopSerialRxBufferSize_c bytes
  if(iBytesReceived >= gPopSerialRxBufferSize_c)
    return false;

  // verify packet is large enough for header + checksum
  if(iBytesReceived < sizeof(sPopGatewayPacketHdr_t) + sizeof(popChecksum_t))
    return false; // packet too small

  // verify length byte
  iLen = psGatewayPacket->header.iLen;
  if(iBytesReceived < sizeof(sPopGatewayPacketHdr_t) + sizeof(popChecksum_t) + iLen)
    return false; // packet too small for length byte

  // verify checksum (everything after the STX)
  iCheckSum = PopCheckSum(sizeof(sPopGatewayPacketHdr_t) - 1 + iLen, &psGatewayPacket->header.iGroup);
  if(iCheckSum != psGatewayPacket->payload[iLen])
    return false;

  return true;
}

/*
  Set the event range for gateway events.
*/
void PopGatewaySetEventRange(uint8_t iLow, uint8_t iHigh)
{
  giEvtLowLimit  = iLow;
  giEvtHighLimit = iHigh;
}

/*
  PopGatewayInitCommonValues

  Internal to the gateway.

  Initialize all the common network variables in one Test Tool packet.
*/
void PopGatewayInitCommonValues(sPopGatewayInitValues_t *pValues)
{
  popNwkAddr_t iNwkAddr;
  popChannelList_t iChannelList;
  popPanId_t iPanId;
  popMfgId_t iMfgId;
  popTimeOut_t iScanTime;
  popAddrCount_t iAddrCount;
  popNwkJoinOpts_t iJoinOpts;

  // Process it when the node is off the network only
  // TODO: indicate error when on network
  if(PopNwkIsOnNetwork())
    return;

  /*
    Some compilers have trouble with addresses to items in packed structures,
    so put them in locals instead.
  */
  iNwkAddr = pValues->iNwkAddr;
  iChannelList = pValues->iChannelList;
  iPanId = pValues->iPanId;
  iMfgId = pValues->iMfgId;
  iScanTime = pValues->iScanTime;
  iAddrCount = pValues->iAddrCount;
  iJoinOpts = pValues->iJoinOptions;

  // Convert from gateway (big) endian to native endian (code has no effect on big endian MCUs)
  GatewayToNative16(&iNwkAddr);
  GatewayToNative16(&iChannelList);
  GatewayToNative16(&iPanId);
  GatewayToNative16(&iMfgId);
  GatewayToNative16(&iScanTime);
  GatewayToNative16(&iAddrCount);

  // Set the various fields as user specified
  PopNwkSetMacAddr(pValues->aMacAddr);
  PopNwkSetNodeAddr(iNwkAddr);
  PopNwkSetNwkDataChannel(pValues->iChannel);
  PopNwkSetChannelList(iChannelList);
  PopNwkSetPanId(iPanId);
  PopNwkSetManufacturerId(iMfgId);
  PopNwkSetJoinOptions(iJoinOpts);
  PopNwkSetScanTimeMs(iScanTime);
  PopNwkSetAddrReqSize(iAddrCount);

  // turn off requesting an address if the count is 0
  if(!iAddrCount)
    PopNwkSetAddrPoolReqOff();
}


/*
  PopGatewayGetCommonValues

  Reads the initialization values from the stack globals and sends them back through the
  gateway (serial or what ever is configured). This function does not validate any of the
  data.

  Interface assumption:
     Receives no parameters or values.

  Return value:
     Returns no values.
*/
void PopGatewayGetCommonValues(void)
{
  sPopGatewayInitValues_t sInitValues;

  // Set the various fields as user specified
  PopNwkGetMacAddr(sInitValues.aMacAddr);             // 8 Bytes

  sInitValues.iNwkAddr = PopNwkGetNodeAddr();         // 2 Bytes
  NativeToGateway16((void *)&sInitValues.iNwkAddr);

  sInitValues.iChannel = PopNwkGetChannel();          // 1 Bytes
  sInitValues.iChannelList = PopNwkGetChannelList();  // 2 Bytes
  NativeToGateway16((void *)&sInitValues.iChannelList);

  sInitValues.iPanId = PopNwkGetPanId();              // 2 Bytes
  NativeToGateway16((void *)&sInitValues.iPanId);

  sInitValues.iMfgId = PopNwkGetManufacturerId();     // 2 Bytes
  NativeToGateway16((void *)&sInitValues.iMfgId);

  sInitValues.iJoinOptions = PopNwkGetJoinOptions();  // 1 Bytes
  sInitValues.iScanTime = PopNwkGetScanTimeMs();      // 2 Bytes
  NativeToGateway16((void *)&sInitValues.iScanTime);

  sInitValues.iAddrCount = PopNwkGetAddrReqSize();    // 2 Bytes
  NativeToGateway16((void *)&sInitValues.iAddrCount);
  //                                              -------------
  //                                                 22 Bytes

  // Send the packet back to the PC or the host using hte gateway, is an atomic operation.
  PopGatewaySendEventToPC(gPopGwCmdGetInitNwkValues_c, sizeof(sInitValues), &sInitValues);
}

/*
  Send the current addr pool through gateway
*/
void PopGatewayGetAddrPool(void)
{
  sPopGatewayAddrPool_t sAddrPool;

  sAddrPool.iNextAvailable = PopNwkGetNextAddrAvailable();
  sAddrPool.iAddrCount = PopNwkGetAddrCount();
  NativeToGateway16((void *)&sAddrPool.iNextAvailable);
  NativeToGateway16((void *)&sAddrPool.iAddrCount);
  PopGatewaySendEventToPC(gPopGwCmdGetAddrPool_c, sizeof(sAddrPool), &sAddrPool);
}

// no need to swap on gateway if already big endian
#if gPopGateway_d && (!gPopBigEndian_c) && gPopSerial_d

/*
  PopSwapBeaconIndication

  Used to swap the beacon indication from native endian to big endian, as required by the
  gateway.
*/
void PopSwapBeaconIndication( sPopNwkBeaconIndication_t *pPopNwkBeaconIndication)
{
  /* Swap sMAC fields */
  NativeToGateway16((popNwkAddr_t*)&pPopNwkBeaconIndication->iNwkAddr);
  NativeToGateway16((popPanId_t*)&pPopNwkBeaconIndication->iPanId);
  NativeToGateway16((popMfgId_t*)&pPopNwkBeaconIndication->iMfgId);
  NativeToGateway16((popAppId_t*)&pPopNwkBeaconIndication->iAppId);
}

/*
  PopConvertEventFromNativeToGatewayEndian

  Some events have
*/
void PopConvertEventFromNativeToGatewayEndian(uint8_t iEvtId, void *pBuffer)
{
  sPopNwkDataIndication_t *pPopNwkDataIndication;
  sPopNwkBeaconIndication_t *pPopNwkBeaconIndication;
  sPopNwkJoinIndication_t *pPopNwkJoinIndication;
  sPopGatewayHeapCheck_t *pPopGatewayHeapCheck;

  // the length of the event depends on the ID
  switch (iEvtId)
  {
    //
    // <UI>
    //

    // convert any fields in high-water mark structure to big endian.
    case gPopGwCmdGetHighWaterMarks_c:
      NativeToGateway16(&((sPopGatewayGetHighWaterMarks_t *)pBuffer)->iStackLeftAtHWM);
      NativeToGateway16(&((sPopGatewayGetHighWaterMarks_t *)pBuffer)->iStackSize);
      NativeToGateway16(&((sPopGatewayGetHighWaterMarks_t *)pBuffer)->iStackLeftCurrent);
      break;

    //
    // <NWK>
    //
    case gPopEvtMemoryAlloc_c:
    case gPopEvtMemoryFree_c:
    case gPopEvtDataPassThru_c:
    case gPopEvtNwkRoutePassThru_c:
    case gPopEvtNwkRouteConfirm_c:
    case gPopEvtNwkAddrPoolConfirm_c:
    case gPopEvtNwkAddrPoolChanged_c:
      NativeToGateway16( &( (sPopEvtData_t*) pBuffer )->iWord );
    break;

    // Note: scan confirm only shows the # of beacons over-the-gatweay (not the whole beacon list).
    // since the beacon
    case gPopEvtNwkScanConfirm_c:
    break;

    // beacon indication
    case gPopEvtNwkBeaconIndication_c:
      pPopNwkBeaconIndication = (sPopNwkBeaconIndication_t*)( (void*)pBuffer );

      /* Swap sMAC fields */
      PopSwapBeaconIndication(pPopNwkBeaconIndication);
    break;

    // join indication
    case gPopEvtNwkJoinIndication_c:
      pPopNwkJoinIndication = (sPopNwkJoinIndication_t*)( (void*)pBuffer );
      NativeToGateway16((popNwkAddr_t*)&pPopNwkJoinIndication->iNwkAddr);
    break;

    // data indication has a pointer to the entire frame
    case gPopEvtDataIndication_c:
      pPopNwkDataIndication = (sPopNwkDataIndication_t*)( (void*)pBuffer );

      /* Swap sMAC fields */
      NativeToGateway16((popMacFrameControl_t*)&pPopNwkDataIndication->sFrame.sMac.iFrameControl);
      NativeToGateway16((popNwkAddr_t*)&pPopNwkDataIndication->sFrame.sMac.iDstAddr);
      NativeToGateway16((popNwkAddr_t*)&pPopNwkDataIndication->sFrame.sMac.iSrcAddr);
      NativeToGateway16((popPanId_t*)&pPopNwkDataIndication->sFrame.sMac.iPanId);

      /* Swap NWK fields */
      NativeToGateway16((popNwkFrameControl_t*)&pPopNwkDataIndication->sFrame.sNwk.iFrameControl);
      NativeToGateway16((popNwkAddr_t*)&pPopNwkDataIndication->sFrame.sNwk.iDstAddr);
      NativeToGateway16((popNwkAddr_t*)&pPopNwkDataIndication->sFrame.sNwk.iSrcAddr);
    break;

    // heap check
    case gPopGwCmdHeapCheck_c:
      pPopGatewayHeapCheck = (sPopGatewayHeapCheck_t *)( (void*)pBuffer );
      NativeToGateway16(&pPopGatewayHeapCheck->iFree);
      NativeToGateway16(&pPopGatewayHeapCheck->iLargestFree);
      NativeToGateway16(&pPopGatewayHeapCheck->iMinFree);
      NativeToGateway16(&pPopGatewayHeapCheck->iBadBlockOffset);
    break;
  default:
    break;
  }


}

#endif // gPopGateway_d

/*
  PopGatewaySendDataRequest

  We've got a data request from the PC side of the gateway. Send it over the air.
*/
void PopGatewaySendDataRequest(sPopNwkDataRequest_t *pBuffer)
{
  sPopNwkDataRequest_t sDataRequest;

  /* set up the request */
  GatewayToNative16((popNwkAddr_t*)&pBuffer->iDstAddr);
  sDataRequest.iDstAddr = pBuffer->iDstAddr;
  sDataRequest.iOptions = pBuffer->iOptions;
  sDataRequest.iRadius = pBuffer->iRadius;
  sDataRequest.iPayloadLength = pBuffer->iPayloadLength;

  /* the payload in the serial packet follows the header, so get the address of it */
  sDataRequest.pPayload = &pBuffer->pPayload;

  PopNwkDataRequest(&sDataRequest);
}

#endif // gPopGateway_d

/*
  PopGatewayPCToTarget

  The PC Gateway App (or Test Tool) has sent something to this board.

  A properly formatted gateway packet looks like:

  [stx][0x50][evtId][len][data...][chksum]

  Each field above, except for the variable length data field, is 1 byte. The
  data may be 0 - 122 bytes. The entire packet must be 127 bytes or less.

  Note: an over-the-air PopNet data request is limited to 107 bytes of payload.

  Inputs:
    iLen            - length of the entire gateway packet
    psGatewayPacket - ptr to the gateway packet

  The length passed to this function is the length of the data in the gateway
  packet.
*/
void PopGatewayPCToTarget(uint16_t iLen, sPopGatewayPacket_t *psGatewayPacket)
{
#if !gPopGateway_d || !gPopSerial_d
  // stubbed. Just tell the compiler this variable is used.
  (void)iLen;
  (void)psGatewayPacket;
#else
  uint8_t       *pPayload;
  sPopEvtData_t  sEvtData;
  bool         fSendResponse;   // send the response now? (doesn't send event to app)
  popEvtId_t     iEvtId;          // command/event ID for this command
  popEvtId_t     iRspEvtId;       // response event ID

  // check if the gateway packet is valid
  if(!PopGatewayPacketIsValid(iLen, psGatewayPacket))
    return;
#if DEBUG >= 2
  gsMpDebug.iGwCmdCnt++;
#endif
  pPayload = (uint8_t *)&psGatewayPacket->payload;

  // by default, send the command completed with success
  fSendResponse = true;
  sEvtData.iStatus = gPopErrNone_c;
  iRspEvtId = gPopGwCmdCompleted_c;

  PopSetMessageEvent(gPopMessageReceived_c, psGatewayPacket);

  // the event ID identifies this packet
  iEvtId = psGatewayPacket->header.iEvtId;
  switch(iEvtId)
  {
    case gPopEvtKey_c:
    case gPopGwCmdSetLed_c:
    case gPopGwCmdPop_c:
    case gPopGwCmdLcdWriteString_c:
    case gPopGwCmdInitNwkValues_c:
    case gPopGwCmdGetInitNwkValues_c:
    case gPopGwCmdSetChannel_c:
    case gPopGwCmdSetChannelList_c:
    case gPopGwCmdSetPanId_c:
    case gPopGwCmdSetNwkAddr_c:
    case gPopGwCmdSetAddrPool_c:
    case gPopGwCmdGetAddrPool_c:
    case gPopGwCmdSetMacAddr_c:
    case gPopGwCmdSetManufactureId_c:
    case gPopGwCmdSetScanOptions_c:
    case gPopGwCmdSetJoinOptions_c:
    case gPopGwCmdSetScanTime_c:
    case gPopGwCmdNwkScanNetworks_c:
    case gPopGwCmdNwkScanForNoise_c:
    case gPopGwCmdNwkStartNetwork_c:
    case gPopEvtNwkJoinEnable_c:
    case gPopGwCmdNwkLeaveNetwork_c:
    case gPopGwCmdNwkDataRequest_c:
    case gPopGwCmdNwkAddrRequest_c:
    case gPopGwCmdSetNetworkKey_c:
    case gPopGwCmdSendBeacon_c:
    case gPopGwCmdBeaconRequest_c:
    case gPopEvtNwkNodePlacement_c:
    case gPopGwCmdNwkFormNetwork_c:
    case gPopGwCmdNwkJoinNetwork_c:
    case gPopGwCmdSetDefaultAddressPoolSize_c:
    case gPopGwCmdGetJoinStatus_c:
    case gPopGwCmdGetSecurityKey_c:
    case gPopGwCmdGetRoutingSleepingMode_c:
    case gPopGwCmdGetApplicationId_c:
    case gPopGwCmdGetNetworkStatus_c:
    case gPopGwCmdSetApplicationId_c:
    case gPopGwCmdGetPopNetVersion_c:
    case gPopGwCmdSetEvtRange_c:
    case gPopGwCmdHeapCheck_c:
    case gPopGwCmdPopNvStore_c:
    case gPopGwCmdPopNvRetrieve_c:
    case gPopGwCmdPopNvForget_c:
      fSendResponse = false;
    break;

/*~~~~~~~~~~~~~~PopNet OTA Upgrade Transmit commands~~~~~~~~~~~~*/
#if gPopOverTheAirUpgrades_d
    case gPopGwCmdOtaUpgradeFirstPassThruReq_c:
    case gPopGwCmdOtaUpgradePassThruReq_c:
      // Destiantion Address
      GatewayToNative16((uint16_t *)pPayload);
      //throttle
      GatewayToNative16((uint16_t *)(pPayload+2));
      //block number
      GatewayToNative16((uint16_t *)(pPayload+4));
      //image id
      GatewayToNative16((uint16_t *)(pPayload+6));
      if(!(*((uint16_t *)(pPayload+4))))
      {
        //image size
        GatewayToNative32((uint32_t *)(pPayload+10));
        //application id
        GatewayToNative16((uint16_t *)(pPayload+14));
      }
      sEvtData.iByte = PopOtaUpgradePassThruRequest((sPopOtauGwSubseqBlockPassThruSaveReq_t * )pPayload);
      // if an error occurred send response immediately
      if(sEvtData.iByte != gPopErrNone_c )
      {
        PopOtaUpgradeSaveRspToGw(gPopGwCmdOtaUpgradePassThruReq_c,sEvtData.iByte);
      }
      // no error wait for response from remote node
      fSendResponse = false;
    break;

    case gPopGwNwkMgmtOtaUpgradeCancelReq_c:
      if(*pPayload)
      {
        iRspEvtId = gPopGwNwkMgmtOtaUpgradeCancelRsp_c;
      }else
      {
        gsPopOtaUpgradeInfo.txStatus = gPopErrCanceled_c;
      }
      fSendResponse = true;
    break;
    case gPopGwNwkMgmtOtaUpgradeFirstSaveReq_c:
    case gPopGwNwkMgmtOtaUpgradeSaveReq_c:
      // save a piece of image in storage, thru serial port
      //block number
      GatewayToNative16((uint16_t *)pPayload);
      //image id
      GatewayToNative16((uint16_t *)(pPayload+2));
      if(!(*((uint16_t *)pPayload)))
      {
        //image size
        GatewayToNative32((uint32_t *)(pPayload+6));
        //application id
        GatewayToNative16((uint16_t *)(pPayload+10));
      }
#if gPopMicroPowerLpcUpgrade_d
      sEvtData.iByte = PopLpcOtaUpgradeStorageModule((sPopOtauGwSubseqBlockSaveReq_t *)pPayload,gPopOtaUpgradeReqSrcOTS_c);

      //if(sEvtData.iByte == gPopErrBadAppId_c)
      if(sEvtData.iByte == gPopErrBadHwId_c) // JJ debug
#endif
      sEvtData.iByte = PopOtaUpgradeStorageModule((sPopOtauGwSubseqBlockSaveReq_t *)pPayload,gPopOtaUpgradeReqSrcOTS_c);

      //get some error info if needed, always send 4 bytes as error info to the pc app
      PopOtaUpgradeSaveRspToGw(gPopGwNwkMgmtOtaUpgradeSaveRsp_c,sEvtData.iByte);
      fSendResponse = false;
    break;
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c:
      //image id
      GatewayToNative16((uint16_t *)(pPayload+1));
      //application id
      GatewayToNative16((uint16_t *)(pPayload+3));
      iLen = psGatewayPacket->header.iLen;
      if(iLen == sizeof(sPopGwResetToNewImageReq_t))
      {
        //application id
        GatewayToNative16((uint16_t *)(pPayload+7));
      }
#if gPopMicroPowerLpcUpgrade_d
      sEvtData.iByte = PopNwkMgmtLpcUpgradeResetToNewImage((sPopGwResetToNewImageReq_t *)pPayload);
      if(sEvtData.iByte == gPopErrBadHwId_c)
#endif
      sEvtData.iByte = PopNwkMgmtOtaUpgradeResetToNewImage((sPopGwResetToNewImageReq_t *)pPayload);
      // avodid sending response immediately if is an OTA command
      fSendResponse = false;
      if(!(*pPayload))
      {
        PopOtaUpgradeSendRspToSerial(gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c,sEvtData.iByte, NULL);
      }
    break;

    case gPopGwNwkMgmtOtaUpgradeCompleteReq_c:
      GatewayToNative16((uint16_t *)(pPayload));
      GatewayToNative16((uint16_t *)(pPayload + 2));
      PopNwkMgmtOtaUpgradeCompleteRequest((sPopGwUpgradeCompleteReq_t*)pPayload);
      fSendResponse = false;
    break;
#else
    case gPopGwCmdOtaUpgradePassThruReq_c:
      sEvtData.iByte = gPopErrNotSupported_c;
      iRspEvtId = iEvtId;
    break;
    case gPopGwNwkMgmtOtaUpgradeSaveReq_c:
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c:
    case gPopGwNwkMgmtOtaUpgradeCancelReq_c:
    case gPopGwNwkMgmtOtaUpgradeCompleteReq_c:
      sEvtData.iByte = gPopErrNotSupported_c;
      // add one to have the response event id
      iRspEvtId = iEvtId + 1;
    break;
#endif
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    case gPopGwCmdGetMaxPayload_c:
      sEvtData.iByte = PopNwkMaxPayload();
      iRspEvtId = iEvtId;
    break;

  case gPopGwCmdSetICanHearYouTable_c:
    case gPopGwCmdGetHighWaterMarks_c:
    case gPopGwCmdSetSleepyMode_c:
      fSendResponse = false;
    break;

#if DEBUG >= 1
    // <SJS: Added Debug OTA Debug Message 0x90>
#if gDebugCmdTest_c && (gNodeType_c == CHARGER_MODULE)
#if gDebugCmdTest_c != gDebugCmdTest_MpDebug_c
    case gPopAppAssertEvt_c:
    case gPopAppCountEvt_c:
      gsDebugCount.iCmd = gPopAppCountEvt_c;
      GatewayToNative32((uint32_t *)pPayload);
      gsDebugCount.iCount = *((uint32_t *)pPayload);
      PopAppSendCountOTA(gsDebugCount);
      fSendResponse = false;
    break;
#endif
#endif
    // </SJS:>
#endif //DEBUG >= 1

    // we don't know the command, so don't send the automated response.
    default:
      fSendResponse = false;
      break;
  }

  // These responses don't flow through the application (not app event),
  // but go straight back out the gateway.
  //
  // Used to handle simple commands processed directly by the gateway (e.g. GetVersion)
  if (fSendResponse)
  {
    PopGatewayTargetToPC(iRspEvtId, sEvtData);
  }

#endif // gPopGateway_d
}

#if gPopMicroPowerLpcUpgrade_d
extern uint8_t ispCommanderModeFlag;
/*
  PopGatewayProcessIncomingData

  Data has been recieved by the serial driver. Act on it in the gateway.
*/
extern void MPIspStateHandler(void);
#endif
void PopGatewayProcessIncomingData(void)
{
#if gPopGateway_d && gPopSerial_d
  uint16_t iLen;
  uint16_t i;

  // retrieve the data from the serial ring buffer to the gateway buffer
  iLen = sizeof(gaGatewayRxSerialData);
  (void)PopSerialReceive(&iLen, gaGatewayRxSerialData);

#if gPopMicroPowerLpcUpgrade_d
  if(ispCommanderModeFlag)
  {
    MPIspStateHandler();
  }
  else
  {
#endif
    // find the STX (if it doesn't already start with one)
    // that is, skip (throw away) any preceding junk before the STX
    for(i=0; i<iLen; ++i)
    {
      if(gaGatewayRxSerialData[i] == gPopStx_c)
        break;
    }
    iLen -= i;

    // if there was a packet, process it
    if(iLen)
      PopGatewayPCToTarget(iLen, (sPopGatewayPacket_t *)(&gaGatewayRxSerialData[i]));
#if gPopMicroPowerLpcUpgrade_d
  }
#endif
#endif  // gPopGateway_d
}

#if gPopGateway_d && gPopSerial_d
/*
  PopEventIsOutOfRange

  Returns true if the event ID is out of range.
*/
bool PopEventIsOutOfRange(popEvtId_t iEvtId)
{
  // if outside the limits, event is out of range
  if(iEvtId < giEvtLowLimit || iEvtId > giEvtHighLimit)
    return true;

  // never report serial done or data ready through gateway
  if(iEvtId == gPopEvtSerialTxDone_c || iEvtId == gPopEvtSerialDataReady_c)
    return true;

  return false;
}

#endif // gPopGateway_d

#if gPopMicroPowerLpcUpgrade_d
void MPGatewaySendLpcCommand(uint8_t iLen, void *pBuffer)
{
#if !gPopGateway_d || !gPopSerial_d
  // no gateway, remove compiler warning of unused variables
  (void)iEvtId;
  (void)iLen;
  (void)pBuffer;
#else

  uint8_t *pSerialPacket;
  uint8_t iSerialPacketLen;

  /* Just send what is received there is no STX or header for LPC */
  iSerialPacketLen = iLen;

  /* packet too large, trucate it... at least you see something on the serial port */
  if(iSerialPacketLen > gPopSerialTxBufferSize_c)
  {
    iSerialPacketLen = gPopSerialTxBufferSize_c;
    //iLen = iSerialPacketLen - sizeof(sPopGatewayPacket_t);
  }

  // <SJS: Added in Serial Tx Rx State Machines> */
  /* allocate the memory for the complete message sent to the gateway */
  pSerialPacket = PopMemAlloc(iSerialPacketLen);
  if(!pSerialPacket)
    return;   // no memory, cannot be sent
  // </SJS:>

  /* copy in the data */
  PopMemCpy(pSerialPacket, pBuffer, iLen);

 // <SJS: Added in Serial Tx Rx State Machines> */
  /* send the bytes out the serial port, slow down the system to keep the events and generate app events more slowly */
  (void)PopSerialSend(iSerialPacketLen, pSerialPacket, gPopSerialSendFlags_FreeOnTxDone_c);
  // </SJS:>

#endif
}
#endif

#ifdef gPopLargeSerialPacket_d
/*
  PopGatewaySendLargeEventToPC

  Sends a large event and data to the PC. Because the packet is larger than 255
  bytes the packet will be static allocated. Packets can only be sent one
  and one.
  The serial packet looks like:

  [stx][0x50][iEvtId][len][data...][chksum]

  Returns
  gPopErrNone_c
*/
popErr_t PopGatewaySendLargeEventToPC(popEvtId_t iEvtId, uint16_t iLen, void *pBuffer, void* pStaticBuffer)
{
#if !gPopGateway_d || !gPopSerial_d
  // no gateway, remove compiler warning of unused variables
  (void)iEvtId;
  (void)iLen;
  (void)pBuffer;
#else
  sPopGatewayPacket_t  *pSerialPacket;
  uint16_t iSerialPacketLen;

  /* [stx][0x50][iEvtId][len][data...][chksum] */
  iSerialPacketLen = sizeof(sPopGatewayPacket_t) + iLen;

  /* packet too large, trucate it... at least you see something on the serial port */
  if(iSerialPacketLen > gPopSerialTxBufferSize_c)
  {
    iSerialPacketLen = gPopSerialTxBufferSize_c;
    iLen = iSerialPacketLen - sizeof(sPopGatewayPacket_t);
  }

  // <SJS: Added in Serial Tx Rx State Machines> */
  /* allocate the memory for the complete message sent to the gateway */
  pSerialPacket = (sPopGatewayPacket_t*)pStaticBuffer;

  /* set up the packet header */
  pSerialPacket->header.iStx = gPopStx_c;
  pSerialPacket->header.iGroup = gPopNetOpGroup_c;
  pSerialPacket->header.iEvtId = iEvtId;
  pSerialPacket->header.iLen = iLen;

  /* copy in the data */
  PopMemCpy(pSerialPacket->payload, pBuffer, iLen);

  /* swap the information in the buffer to Big endian (only creates code if MCU is little endian) */
  PopConvertEventFromNativeToGatewayEndian(iEvtId, pSerialPacket->payload);

  /* checksum the packet (everything after STX) */
  pSerialPacket->payload[iLen] =
    PopCheckSum(iSerialPacketLen - (sizeof(uint8_t) + sizeof(popChecksum_t)),
    (void *)&(pSerialPacket->header.iGroup));

  // <SJS: Added in Serial Tx Rx State Machines> */
  /* send the bytes out the serial port, slow down the system to keep the events and generate app events more slowly */
  (void)PopSerialSend(iSerialPacketLen, pSerialPacket, 0);
  // </SJS:>

  PopSetMessageEvent(gPopMessageSent_c, pSerialPacket);

#endif
  return gPopErrNone_c;
}
#endif // gPopLargeSerialPacket_d

/*
  PopGatewaySendEventToPC

  Sends the event and data to the PC. Used by PopGatewayTargetToPc.
  The serial packet looks like:

  [stx][0x50][iEvtId][len][data...][chksum]

  Returns
  gPopErrNone_c
  gPopErrNoMem_c    Not enough memory to allocate serial packet
*/
popErr_t PopGatewaySendEventToPC(popEvtId_t iEvtId, uint16_t iLen, void *pBuffer)
{
#if !gPopGateway_d || !gPopSerial_d
  // no gateway, remove compiler warning of unused variables
  (void)iEvtId;
  (void)iLen;
  (void)pBuffer;
#else
  sPopGatewayPacket_t  *pSerialPacket;
  uint16_t iSerialPacketLen;

  /* [stx][0x50][iEvtId][len][data...][chksum] */
  iSerialPacketLen = sizeof(sPopGatewayPacket_t) + iLen;

  /* packet too large, trucate it... at least you see something on the serial port */
  if(iSerialPacketLen > gPopSerialTxBufferSize_c)
  {
    iSerialPacketLen = gPopSerialTxBufferSize_c;
    iLen = iSerialPacketLen - sizeof(sPopGatewayPacket_t);
  }

  // <SJS: Added in Serial Tx Rx State Machines> */
  /* allocate the memory for the complete message sent to the gateway */
  pSerialPacket = PopMemAlloc(iSerialPacketLen);
  if(!pSerialPacket)
    return gPopErrNoMem_c;   // no memory, cannot be sent
  // </SJS:>

  /* set up the packet header */
  pSerialPacket->header.iStx = gPopStx_c;
  pSerialPacket->header.iGroup = gPopNetOpGroup_c;
  pSerialPacket->header.iEvtId = iEvtId;
  pSerialPacket->header.iLen = iLen;

  /* copy in the data */
  PopMemCpy(pSerialPacket->payload, pBuffer, iLen);

  /* swap the information in the buffer to Big endian (only creates code if MCU is little endian) */
  PopConvertEventFromNativeToGatewayEndian(iEvtId, pSerialPacket->payload);

  /* checksum the packet (everything after STX) */
  pSerialPacket->payload[iLen] =
    PopCheckSum(iSerialPacketLen - (sizeof(uint8_t) + sizeof(popChecksum_t)),
    (void *)&(pSerialPacket->header.iGroup));

  // <SJS: Added in Serial Tx Rx State Machines> */
  /* send the bytes out the serial port, slow down the system to keep the events and generate app events more slowly */
  (void)PopSerialSend(iSerialPacketLen, pSerialPacket, gPopSerialSendFlags_FreeOnTxDone_c);
  // </SJS:>

  PopSetMessageEvent(gPopMessageSent_c, pSerialPacket);

#endif
  return gPopErrNone_c;
}

/*
  PopGatewayTargetToPC

  The PopNet app wants to send something to the PC Gateway (or Test Tool).

  This routine simply determines the size of the payload from the event ID and
  passes it on to PopGatewaySendEventToPC().

  Note: the event ID only works for PopNet stack events. For Application
  events, the application must use its own call to PopGatewaySendEventToPC()

  Will do nothing if the event ID is out of range.
*/
void PopGatewayTargetToPC(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
#if !gPopGateway_d || !gPopSerial_d
  // no gateway, remove compiler warning of unused variables
  (void)iEvtId;
  (void)sEvtData;
#else
  uint8_t  *pPayload;      // ptr to payload to send over serial connection
  uint8_t  iPayloadLen;    // length of payload

  // if the event ID is out of user selected range, don't report event to PC
#if gNodeType_c == STATISTICS_MODULE
  if(MpEventIsOutOfRange(iEvtId))
  {
    return;
  }
#else
  if(PopEventIsOutOfRange(iEvtId))
  {
    return;
  }
#endif

  // assume single byte payload
  iPayloadLen = 1;
  pPayload = &sEvtData.iByte;

  // the length of the event depends on the ID
  switch (iEvtId)
  {
    //
    // <UI>
    //

    // byte payloads
    case gPopEvtTableFull_c:
    case gPopEvtKey_c:
    case gPopEvtPwrWakeUp_c:
    case gPopGwCmdSetLed_c:
    case gPopGwCmdPop_c:
    case gPopGwCmdLcdWriteString_c:
      break;

    //
    // <NWK>
    //

    // scan confirm
    case gPopEvtNwkScanConfirm_c:
      // iPayloadLen is already 1 from above
      pPayload = (void *)(sEvtData.pNwkScanConfirm);
    break;

    // beacon indication
    case gPopEvtNwkBeaconIndication_c:
      iPayloadLen = sizeof(sPopNwkBeaconIndication_t);
      pPayload = (void *)(sEvtData.pNwkBeaconIndication);
    break;

    // join indication
    case gPopEvtNwkJoinIndication_c:
      iPayloadLen = sizeof(sPopNwkJoinIndication_t);
      pPayload = (void *)(sEvtData.pNwkJoinIndication);
    break;

    // data indication has a pointer to the entire frame
    case gPopEvtDataIndication_c:
      iPayloadLen = (sizeof(sPopNwkDataIndication_t) - 1) + PopNwkPayloadLen(sEvtData.pDataIndication);
      pPayload = (void *)(sEvtData.pDataIndication);
    break;

    // 1 byte payloads
    case gPopEvtDataConfirm_c:       // default length 1 is correct
    case gPopEvtNwkStartConfirm_c:   // default length 1 is correct
    case gPopEvtNwkJoinEnable_c:     // default length 1 is correct
    case gPopEvtNwkLeaveConfirm_c:   // default length 1 is correct
    break;

    // node placement
    case gPopEvtNwkNodePlacement_c:
      iPayloadLen = sizeof(sPopNodePlacement_t);
      pPayload = (void *)sEvtData.pPopNodePlacementResult;
    break;

    // Do we have join enable or disable.
    case gPopGwCmdGetJoinStatus_c:
      iPayloadLen = sizeof(bool);
      pPayload = (void *)&sEvtData.iByte;
      break;

    // Returns the current key.
    case gPopGwCmdGetSecurityKey_c:
      iPayloadLen = sizeof(aPopNwkKey_t);
      pPayload = (void *)(sEvtData.pData);
      break;

    // returns the status of the sleeping mode
    case gPopGwCmdGetRoutingSleepingMode_c:
      iPayloadLen = sizeof(bool);
      pPayload = (void *)&sEvtData.iByte;
      break;

    // returns or sets the application Id
    case gPopGwCmdGetApplicationId_c:
      iPayloadLen = sizeof(sEvtData.iWord);
      pPayload = (void *)&sEvtData.iWord;
      break;

    // returns true if the device is on the network or not
    case gPopGwCmdGetNetworkStatus_c:
      iPayloadLen = sizeof(bool);
      pPayload = (void *)&sEvtData.iByte;
      break;


    //
    // <GATEWAY>
    //

    // 0 byte payloads
    case gPopEvtMemoryAllocFailure_c:
    case gPopEvtSerialDataReady_c:
    case gPopEvtNwkInitiateRoute_c:
      iPayloadLen = 0;
    break;

    // 1 byte payloads
    case gPopGwCmdCompleted_c:
    case gPopEvtAssert_c:
    case gPopGwCmdGetMaxPayload_c:
    case gPopGwCmdPopNvStore_c:
    case gPopGwCmdPopNvRetrieve_c:
    case gPopGwCmdPopNvForget_c:
    case gPopEvtNwkScanConfirmError_c:
    break;

    // word payloads
    case gPopEvtMemoryAlloc_c:
    case gPopEvtMemoryFree_c:
    case gPopEvtDataPassThru_c:
    case gPopEvtNwkRoutePassThru_c:
    case gPopEvtNwkRouteConfirm_c:
    case gPopEvtNwkAddrPoolConfirm_c:
    case gPopEvtNwkAddrPoolChanged_c:
      iPayloadLen = sizeof(uint16_t);
      pPayload = (void *)(&sEvtData.iWord);
    break;

    // n-byte payloads
    case gPopGwCmdGetPopNetVersion_c:
      iPayloadLen = sizeof(sPopGatewayVersionRsp_t);
      pPayload = sEvtData.pData;
      break;

    case gPopGwCmdHeapCheck_c:
      iPayloadLen = sizeof(gsGatewayHeapCheck);
      pPayload = (void *)(&gsGatewayHeapCheck);
    break;

    // n-byte payload
    case gPopGwCmdGetHighWaterMarks_c:
      iPayloadLen = sizeof(sPopGatewayGetHighWaterMarks_t);
      pPayload = sEvtData.pData;
    break;

    // 1 byte payload OTA upgrade commands
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c:
      pPayload = sEvtData.pData;
    break;
    case gPopGwNwkMgmtOtaUpgradeCancelRsp_c:
    break;

    case gPopGwNwkMgmtOtaUpgradeSaveRsp_c:
    case gPopGwCmdOtaUpgradeFirstPassThruReq_c:
    case gPopGwCmdOtaUpgradePassThruReq_c:
      // always send the extra info as a 4byte value
      iPayloadLen = sizeof(sPopOtaUpgradeErr_t);
      pPayload = sEvtData.pData;
    break;

    case gPopGwNwkMgmtOtaUpgradeCompleteRsp_c:
      // always send the extra info as a 4byte value
      iPayloadLen = sizeof(sPopGwUpgradeCompleteRsp_t);
    break;

    case gPopGwCmdGetInitNwkValues_c:
      iPayloadLen = sizeof(gsPopNwkData);
      pPayload = sEvtData.pData;
      break;

    // don't know the payload size, so don't report the event
    default:
      iEvtId = 0;   // unkown packet
    break;
  }

  /* send it out the serial port */
  if(iEvtId)
    PopGatewaySendEventToPC(iEvtId, iPayloadLen, pPayload);

#endif // gPopGateway_d
}

