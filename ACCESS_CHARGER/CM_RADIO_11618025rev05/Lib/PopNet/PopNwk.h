/***************************************************************************
  PopNwk.h
  Copyright (c) 2006-2010 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  PopNet Internal: contains types and defines not to be changed by applications.

****************************************************************************/
#ifndef _POP_NWK_H_
#define _POP_NWK_H_

/***************************************************************************
  Types & Defines
***************************************************************************/

// NWK layer internal events
#define gPopEvtNwkReceive_c           0x10	// NWK: go into receive mode
#define gPopEvtNwkGotPacket_c         0x11	// NWK: ready to process next over-the-air packet

// PHY has sent the NWK layer a packet (must be same EvtId in PopPhy.h)


// network layer management commands
#define gPopNwkMgmtAck_c              0x13   // an ACK frame is coming back (unicast only)
#define gPopNwkMgmtAddrRequest_c      0x16
#define gPopNwkMgmtAddrResponse_c     0x17
#define gPopNwkMgmtJoinEnable_c       0x18
#define gPopNwkMgmtRouteReply_c       0x19   // route reply (unicast). Route Requests are a bit in unibroadcast
#define gPopNwkMgmtFindNodeRequest_c  0x1A  // find node request
#define gPopNwkMgmtFindNodeResponse_c 0x1B  // find node reply
#define gPopNwkMgmtRouteRequest_c         0x1C  // route request

// Network timers ids (check for any use of these in PopDefault.c)
#define gPopNwkScanTimer_c            0x01
#define gPopNwkJoinTimer_c            0x02
#define gPopNwkSendBeaconTimer_c      0x03
#define gPopNwkReceiveTimer_c         0x04
#define gPopNwkJoinConfirmTimer_c     0x05
#define gPopNwkJoinRspTimer_c         0x09

// timers shared with PopDefault.c
#define gPopOtaUpgradeThrottleTimer_c        0x10    // moved to PopNet.h since they are shared with PopDefault.c
#define gPopOtaUpgradeCompleteTimer_c      0x11

// bits in the NWK frame control field. See popNwkFrameControl_t in PopNet.h
// 
// xxxx xxxx xxxx xxTT  Frame type (00=data, 01=cmd)
// xxxx xxxx xxVV VVxx  Protocol version (always 0010 = 0x02)
// xxxx xxxx DDxx xxxx  Discover route (00=none, 01=route request, 10=forced)
// xxUx xxxx xxxx xxxx  Duplicates OK (don't use duplicate table)
// RRxR RRRR xxxx xxxx  Reserved
#define gPopNwkFrameControl_c     0x0008	// normal network frame for data (protocol ver 0x02).
#define gPopNwkFcData_c           0x0000  // bits 0:1  data frame
#define gPopNwkFcMgmt_c           0x0001  // bits 0:1  nwk management frame or app data frame?
#define gPopNwkFcNoDiscover_c     0x0000  // bits 6:7  don't discover route
#define gPopNwkFcRouteIfNeeded_c  0x0040  // bits 6:7  discover route if needed
#define gPopNwkFcRouteForced_c    0x0080  // bits 6:7  route discovery was forced
#define gPopNwkFcDuplicatesOk_c   0x2000  // bit  13   duplicates ok (don't add to duplicate table)
#define gPopNwkFcReserved_c       0xdf00  // reserved bits

// checks if the NWK frame control is valid
#define PopNwkIsValidDataFc(iFrameControl) (((iFrameControl) & (gPopNwkFcReserved_c | gPopNwkFrameControl_c)) == gPopNwkFrameControl_c)

// see popNwkDataReqOpts_t
#define gPopNwkDataReqOptsMgmt_c  0x80  // (nwk layer internal only)

#define gPopNwkThrowOutPacket_c   FALSE
#define gPopNwkKeepPacket_c       TRUE

// supported frame control (popMacFrameControl_t) configuration for emulating mac packets
// Mac Header Frame Control (from 7.2.1.1 Frame Control Field - IEEE 802.15.4 2006 spec)
// 
// xxxx xxxx xxxx xTTT  Frame type (000=beacon, 001=data, 010=ack, 011=cmd) 
// xxxx xxxx xxxx Sxxx  Security enabled
// xxxx xxxx xxxP xxxx  Pending frame (to inform sleepy node)
// xxxx xxxx xxAx xxxx  Ack requested
// xxxx xxxx xCxx xxxx  Pan ID compression (0=src & dst PANs, 1=dst PAN only)
// xxxx xxRR Rxxx xxxx  Reserved bits
// xxxx DDxx xxxx xxxx  Destination address mode (00=not present, 10=16-bit, 11=64-bit)
// xxVV xxxx xxxx xxxx  Frame version (00=2003, 01=2006)
// SSxx xxxx xxxx xxxx  Source address mode (00=not present, 10=16-bit, 11=64-bit)

// these fields are used prior to decoding the packet. The packet is always little endian (and possibly secure) at this stage).
#if ( !gPopBigEndian_c ) 

#define gMacDataFrame_c                  0x8841
#define gMacFrameControlAck_c            0x8841   // MAC frame control for PopNet ACK (looks like a MAC data frame)
#define gMacFrameControlBeacon_c         0x8000   // MAC frame control for Beacon Response
#define gMacFrameControlBeaconRequest_c  0x0803   // MAC frame control for Beacon Request
#define gMacAssociationReqFrameControl_c 0xc823
#define gMacAssociationRspFrameControl_c 0xcc63
#define gMacFrameControlSecurityEnable_c 0x0008   // MAC frame control
#else

#define gMacDataFrame_c                  0x4188
#define gMacFrameControlAck_c            0x4188   // MAC frame control for PopNet ACK (looks like a MAC data frame)
#define gMacFrameControlBeacon_c         0x0080   // MAC frame control for Beacon Response
#define gMacFrameControlBeaconRequest_c  0x0308   // MAC frame control for Beacon Request
#define gMacAssociationReqFrameControl_c 0x23c8
#define gMacAssociationRspFrameControl_c 0x63cc
#define gMacFrameControlSecurityEnable_c 0x0800   // MAC frame control
#endif

#define gPopNwkSignature_c		'P'	      // ascii P, signature in the MAC frame

// Nwk frame 

#define PopNwkIsValidDuplicateEntry(i) (gaPopNwkDuplicateTable[i].iTimeLeftMs)
#define PopNwkFreeDuplicateEntry(i)    (gaPopNwkDuplicateTable[i].iTimeLeftMs = 0)

// duplicate table types
typedef uint8_t  popNwkDuplicateType_t;
#define gPopNwkDuplicateTypeUnicast_c   1
#define gPopNwkDuplicateTypeBroadcast_c 2

// retry types
typedef uint8_t popNwkRetryTypeField_t;
#define gPopNwkRetryTypeUnused_c        0 // non used
#define gPopNwkRetryTypeUnicast_c       1 // unicast request
#define gPopNwkRetryTypeGotUnicast_c    2 // unicast pass-through
#define gPopNwkRetryTypeBroadcast_c     3 // broadcast request
#define gPopNwkRetryTypeGotBroadcast_c  4 // received broadcast

// Helper functions for broadcasting and unicasting (for flags field)
#define PopNwkIsValidRetryEntry(i) (gaPopNwkRetryTable[i].iFlags.fIsValid)

// the state of this node on the network. See PopNwkGetNetworkState()
// Returns the current network status. See popNwkState_t for states.
#define PopNwkGetNetworkState()              ((popNwkState_t)giPopNwkState)

extern popNwkState_t giPopNwkState;
#define gPopStateOffTheNetwork_c                  0   // not on the network
#define gPopStateStart_c                                  1   // checks for things like silent form/join
#define gPopStateScanningNetworks_c            2   // start the scan for networks process
#define gPopStateWaitingForScanConfirm_c    3   // waiting for scan confirm
#define gPopStateScanningForNoise_c             4   // scan for noise
#define gPopStateFormNetwork_c                    5   // choose channel on whiich to form (if forming)
#define gPopStateJoinChoose_c                        6   // choose channel/parent to join if joining 
#define gPopStateJoinNetwork_c                      7   // actually send out assocation request
#define gPopStateFreeScanMem_c                   8   // clean up any allocated memory
#define gPopStateSendBeacon_c                      9   // send out a beacon if requested
#define gPopStateFinishUp_c                          10  // done with pretty much everthing, now say we're on the network
#define gPopStateOnTheNetwork_c           42  // node is now on the fully on the network and can communicate.

// cause of the state machine
typedef uint8_t popNwkStateCause_t;
#define gPopNwkCauseJoin_c              1
#define gPopNwkCauseForm_c            2
#define gPopNwkCauseStart_c            3


// externals used by the broadcast/unicast engine
extern const uint8_t giPopNwkBroadcastTimeOut;
extern const uint8_t giPopNwkBroadcastJitter;
extern const uint16_t giPopNwkUnicastTimeOut;
extern const uint16_t giPopNwkUnicastJitter;
extern const uint16_t giPopNwkAckExpired;
extern const uint16_t giPopNwkBroadcastExpired;
extern const uint8_t giPopNwkMaxRetries;
extern const uint8_t giPopNwkBroadcastRetries;
extern sPopNwkRetryTable_t gaPopNwkRetryTable[];
extern const uint8_t cPopNwkRetryEntries;
extern uint8_t giPopNwkRetryEntriesCount;
extern sPopNwkDuplicateTable_t gaPopNwkDuplicateTable[];
extern const uint8_t cPopNwkDuplicateEntries;
extern uint8_t giPopNwkDuplicateEntriesCount;
extern uint8_t giPopNwkDuplicateHighWaterMark;
extern const uint16_t giPopNwkJoinExpired;
extern const uint8_t  giPopNwkBeaconJitter;
extern const uint8_t  giPopNwkBeaconJitterJoinEnable;
extern const uint16_t giPopNwkReJoinJitter;
extern const uint8_t giPopNwkAuxFrameSize;
extern const popMacFrameControl_t gMacOtaFrameControlData;
extern const uint8_t giPopNwkScanAttempts;
extern const popMemSize_t giMaxSizeDiscoveryTable;
extern bool gPopSendingRouteReply;
extern const uint8_t cPopNwkAsymmetricLinkEntries;;


#if gPopNeedPack_c  
#pragma pack(1)
#endif

/*
   Network layer: sPopNwkAck_t

   PopNet ACKs are not time-dependant like MAC ACKs, and so are a bit larger.

   Deprecated. PopNet now uses MAC data packets (NWK CMD packet) for ACK. See 
*/
typedef struct _tagPopNwkAck
{
  popMacFrameControl_t iFrameControl;  // fixed data to emulate MAC acks (0x8842)
  popSequence_t iSequenceNumber;    // MAC sequence number ingored (in popnet packet it's always 0x50)
  popPanId_t iDstPanId;             // what pan id we're sending to
  popNwkAddr_t iDstAddress;         // what address we're sending to
  popNwkAddr_t iSrcAddress;         // what address we're sending from
  popNwkAddr_t iNwkSrcAddress;      // source address from the nwk header
  popSequence_t iNwkSequenceNumber; // sequence number from the nwk header
} sPopNwkAck_t;


/*
  Network layer: sPopNwkBeaconReq_t

  Serves for sending out a beacon request over the air in a specific channel
*/
typedef struct _tagPopNwkBeaconReq
{
  popMacFrameControl_t iFrameControl;  // fixed data to emulate MAC packets
  popSequence_t iSequenceNumber;    // sequence number (in popnet packet it's always 0x50)
  popPanId_t iSrcPanId;             // what pan id we're sending From
  popPanId_t iDstPanId;             // what pan id we're sending to
  popCmd_t   iCommandId;            // identifier for this request
} sPopNwkBeaconReq_t;

/*
  Network layer: sPopNwkBeaconPayload_t

  Serves for formating the beacon payload
*/
typedef struct _tagPopNwkBeaconPayload_t
{
  popProtocol_t	    iProtocol;		    // 0x50=PopNet, 0x01=ZigBee
  popProtocolVer_t  iProtocolVer;	    // top bit indicates join enable
  aPopMacAddr_t	    aMacAddr;		      // 64-bit IEEE (MAC) address
  popMfgId_t		    iMfgId;		        // 16-bit manufacturer identifier
  popAppId_t		    iAppId;		        // 16-bit application identitifer
} sPopNwkBeaconPayload_t;


/*
  Network layer: sPopNwkBeacon_t

  Serves for sending out a beacon over the air
*/
typedef struct _tagPopNwkBeacon_t
{
  popMacFrameControl_t iFrameControl;      // fixed data to emulate MAC packets
  popSequence_t iSequenceNumber;        // sequence number (in popnet packet it's always 0x50)
  popPanId_t iSrcPanId;                 // what pan id we're sending from
  popNwkAddr_t iSrcNwkAddr;             // what nwk address we're sending from
  uint16_t iSuperFrame;                 // fixed data 0xcfff
  uint8_t iGtsSpec;                     // fixed data 0x00
  uint8_t iPendingAddrSpec;             // fixed data 0x00
  sPopNwkBeaconPayload_t sBeaconPayload;// Popnet beacon payload
} sPopNwkBeacon_t;

// MAC beacon fields
// 
#if ( !gPopBigEndian_c ) 
#define gMacSuperFrame_c                 0xCFFF
#define gMacSuperFrameCapacityOn_c       0x8000
#define gMacSuperFrameCapacityOff_c      0x7FFF

#else
#define gMacSuperFrame_c                 0xFFCF
#define gMacSuperFrameCapacityOn_c       0x0080
#define gMacSuperFrameCapacityOff_c      0xFF7F
#endif

// single bytes don't need endian control
#define gMacGtsSpec_c                     0x00
#define gMacPendingAddrSpec_c             0x00

// note: PopNet uses the high bit of the protocol version byte (first byte of beacon payload)
// to indicate join enable/disable, in addition to the MAC fields. Used for discovery table compression.
#define gMacProtocolVersionCapacityOn_c   0x80
#define gMacProtocolVersionCapacityOff_c  0x7F


#if gPopNeedPack_c  
#pragma pack()
#endif

/*
  Network Layer: sPopNwkMgmtJoinRequest_t

  Used to send a Join request to the previously selected node.
*/
typedef struct _tagPopNwkMgmtJoinRequest_t
{
  // The command Id to be send out OTA.
  popCmd_t  iCommandId;

  // The address of the requesting device.
  aPopMacAddr_t  aSrcMacAddr;
} sPopNwkMgmtJoinRequest_t;

/*
  Network Layer: sPopNwkMgmtJoinResponse_t

  This data type is used when issuing a Join response, if the join response is
  send back with success the error status send back is Error None, the next
  parameter is the IEEE address and the short address of the receiving node
  comes next.

  If the resposne got eny error the error status will have the proper error code
  and no address is send back.

  This type is variable lenght.
*/
typedef struct _tagPopNwkMgmtJoinResponse_t
{
  // The command Id to be send out OTA.
  popCmd_t  iCommandId;

  // The IEEE Address of the requesting node.
  aPopMacAddr_t  aIEEEAddr;

  // The new Nwk address of the joining device. This field may not be included
  // if the status is different of Succes.
  popNwkAddr_t   iDstAddress;
} sPopNwkMgmtJoinResponse_t;

/*
  NetWork Layer: sPopNwkMgmtAddrRequest_t

  This type is used to send out an Address request command, is used to ask for a
  pool of address form a node with capacity.

  This type is fixed size.
*/
#if gPopNeedPack_c  
#pragma pack(1)
#endif
typedef struct _tagPopNwkMgmtAddrRequest_t
{
  // The command Id to be send out OTA. Command Id gPopNwkMgmtAddrRequest_c
  popCmd_t  iCommandId;

  // the amount of addresses to request.
  popAddrCount_t  iAddrCount;
} sPopNwkMgmtAddrRequest_t;

/*
  NetWork Layer: sPopNwkMgmtAddrResponse_t

  The resposne send back to the requesting node, return zero int he address count
  if the node can not provide the reuqested set of address, also the first addres
  is set to 0xfffe.

  This type is fixed size.
*/
typedef struct _tagPopNwkMgmtAddrResponse_t
{
  // The command Id to be send out OTA. Command Id gPopNwkMgmtAddrResponse_c
  popCmd_t  iCommandId;

  // The number of addresses in the address pool.
  popAddrCount_t  iAddrCount;

  // The First address on the address pool.
  popNwkAddr_t    iFirstAddress;
} sPopNwkMgmtAddrResponse_t;
#if gPopNeedPack_c
#pragma pack()
#endif

// for the over-the-air join enable request. There is no response to this command.
typedef struct _tagPopNwkMgmtJoinEnableRequest_t
{
  popCmd_t            iCommandId; // command send OTA, gPopNwkMgmtJoinEnable_c
  bool  fEnable;                // join status.
} sPopNwkMgmtJoinEnableRequest_t;

// this is the over-the-air structure used internally by PopNwkFindNode()
// See also sPopNwkFindNode_t. Note: these fields MUST match the last 3 fields.
typedef struct _tagPopNwkMgmtFindNodeRequest_t
{
  popCmd_t             iCommandId; // command send OTA. Command Id gPopNwkMgmtFindNodeRequest_c
  popNwkFindOptions_t  iOptions;   // seach with any combination of options
  popAppId_t           iAppId;
  aPopMacAddr_t        aMacAddr;
} sPopNwkMgmtFindNodeRequest_t;

typedef struct _tagPopNwkMgmtFindNodeResponse_t
{
  popCmd_t            iCommandId; // command send OTA. Command Id gPopNwkMgmtFindNodeResponse_c
} sPopNwkMgmtFindNodeResponse_t;


// The union with all the possible commands to be used OTA by the network layer.
typedef union _tagPopNwkMgmtCmd_t
{
    // The command Id to be send out OTA.
    popCmd_t  iCommandId;

    // An address request to get a set of addresses form the a specific address
    // pool. Command Id gPopNwkMgmtAddrRequest_c
    sPopNwkMgmtAddrRequest_t   sAddressRequest;

    // A response to the address request which include the first address in the
    // set of addresses and amount of addresses to hold. Command Id gPopNwkMgmtAddrResponse_c
    sPopNwkMgmtAddrResponse_t  sAddressResponse;

    // join enable (or disable) across the network, command id gPopNwkMgmtJoinEnable_c
    sPopNwkMgmtJoinEnableRequest_t  sJoinEnable;

    // find node request. Command Id gPopNwkMgmtFindNodeRequest_c
    sPopNwkMgmtFindNodeRequest_t   sFindNodeRequest;

    // find node response. Command Id gPopNwkMgmtFindNodeResponse_c
    sPopNwkMgmtFindNodeResponse_t  sFindNodeResponse;

    // ota upgrade request
    sPopOtaUpgradeReq_t  sPopSaveOtaUpgradeReq;

    // response to ota upgrade request
    sPopOtaUpgradeRsp_t  sPopSaveOtaUpgradeRsp;
}sPopNwkMgmtCmd_t;

// route reply structre
typedef struct _tagPopNwkMgmtRouteReply_t
{
  uint8_t        iCmd;
  popSequence_t iSequence;
} sPopNwkMgmtRouteReply_t;

// A union for the secure counters
typedef union _tagPopSecureCounters_t
{
  popNwkLiteCounter_t iPopLiteCounter;
  popNwkAESCounter_t iPopAESCounter;
}uPopSecureCounters_t;

#define PopNwkGetAESCounter()      ((popNwkAESCounter_t)guPopNwkSecureCounters.iPopAESCounter)
#define PopNwkIncreaseAESCounter(increment)  (guPopNwkSecureCounters.iPopAESCounter += ( increment ))

#define PopNwkGetLiteCounter()      ((popNwkLiteCounter_t)guPopNwkSecureCounters.iPopLiteCounter)
#define PopNwkIncreaseLiteCounter(increment)  (guPopNwkSecureCounters.iPopLiteCounter += ( increment ))

// The next status is used by nwk layer only and they are by itself explanatory
typedef enum PopErrReturned_tag{
  gPopIndicationIsNull_c = 0,
  gPopIndexFreeIsNull_c,
  gPopRouteReqEntryNotFound_c,
  gPopAddressHeared_c,
  gPopAddressNotInList_c,
  gPopInvalidPacket_c
}PopErrReturned_t;

/***************************************************************************
  Prototypes
***************************************************************************/
bool PopNwkIsValidRouteDiscoveryIndex(uint8_t i );

/********* Prototypes Used by Nwk Layer *************/
void PopNwkMgmtIndication(sPopNwkDataIndication_t *pIndication);
void PopNwkConfirm(sPopEvtData_t sEvtData);
void PopNwkCheckRetries(popTimeOut_t iTicksMs);
void PopNwkCheckDuplicates(popTimeOut_t iDiffTime);
void PopNwkCheckRouteDiscovery(popTimeOut_t iDiffTime);
void PopNwkFreeEntireDuplicateTable( void );
popErr_t PopNwkMgmtDataRequest(sPopNwkDataRequest_t *pDataReq, bool fFromApp);
void PopNwkBeaconRequest( popChannel_t channel );
void PopNwkSendBeacon( void );
void PopNwkStartNetworkStateMachine(void);
void PopNwkMgmtTimerHandler(popTimerId_t iTimerId);
void PopNwkFreeRetryEntry(uint8_t i, popErr_t iErr);
void PopNwkFreeEntireRetryTable( void );
void PopNwkSendRouteReply(popNwkAddr_t iPrevHop, popNwkAddr_t iNwkSrcAddr, popNwkAddr_t iNwkDstAddr, popSequence_t iSequence);
void PopNwkProcessNextRREPState(uint8_t iDiscoveryTableIndex, popNwkAddr_t iMacSrcAddr);
bool PopNwkIsOffNetwork(void);
void PopNwkMgmtSetOtaEnabled(const bool enabled);
void PopNwkFreeMsgQueue(void);

// PHY layer prototypes
uint8_t PopPhyLqi(void);
void PopPhyDataRequest(void *pData, uint8_t iLen);
void PopPhyReceive(sPopNwkDataIndication_t *pFrame);
void PopPhySetChannel(uint8_t iChannel);


/*
  PopNwkMgmtGetAddrFromPool

  this function handles the address pool of the curretn device, it does not
  matter if it is the address server or any node with avaialbel addresses.

  Receives the amount of addresses requested.

  Returns the first address of the requested set.
*/
popNwkAddr_t PopNwkMgmtGetAddrFromPool(popAddrCount_t  *pNumOfAddress);

// internal address request/response routines
void PopNwkMgmtSendAddrPoolResponse(popNwkAddr_t iSrcAddr, sPopNwkMgmtAddrRequest_t  *pAddrReq);
void PopNwkMgmtProcessAddressPoolRsp(sPopNwkMgmtAddrResponse_t  *pAddressResponse);

/*
  PopNwkMgmtSetJoinEnabled

  Set the current status of the Join capability.

  Receives a boolean value to be set as the current join capability.

  Returns nothing.
*/
void PopNwkMgmtSetJoinEnable(bool fEnable);

void PopNwkScanNextChannel( void );

// assocation routines
void PopNwkMgmtJoinRequest(void);
void PopNwkMgmtJoinResponse(aPopMacAddr_t aDstIeeeAddr);
void PopNwkMgmtProcessJoinRsp(popNwkAddr_t iDstAddr);
void PopNwkSendAssociationRequest(popPanId_t iPanId, popNwkAddr_t iDstAddr);
void PopNwkSendAssociationResponse(aPopMacAddr_t aDstIeeeAddr, popNwkAddr_t iNewAddr);
void PopNwkMgmtFindNodeRequest(popNwkAddr_t iSrcAddr, sPopNwkMgmtFindNodeRequest_t *pFindReq);
void PopNwkMgmtFindNodeResponse(popNwkAddr_t iSrcAddr);

// routing function prototypes
bool PopNwkFilterPackets(sPopNwkDataIndication_t *pFrame);
void PopNwkSendAppDataConfirm(popErr_t iErr);
void PopNwkGotRouteReply(sPopNwkDataIndication_t *pIndication);//, popNwkAddr_t iPrevAddr);
void PopNwkMgmtRouteReply(sPopNwkDataIndication_t *pIndication);
uint8_t PopNwkFindRouteReq(popNwkAddr_t iSrcAddr, popSequence_t iSequence, uint8_t *piFree);
uint8_t PopNwkFindRetryEntry(popSequence_t iSequence, popNwkAddr_t iNwkAddr, uint8_t *piFree);
popNwkAddr_t PopNwkGetRouteReqPrevHop(sPopNwkRouteDiscovery_t *pRDEntry);
popNwkPathCost_t PopNwkLinkCostFromLqi(uint8_t iLqi);
popErr_t PopNwkProcessRouteRequest(sPopNwkDataIndication_t *pIndication, uint8_t *pIndex);
void PopNwkJoinIndication(sPopNwkJoinIndication_t *pJoinIndication);
void PopNwkAddRoute(popNwkAddr_t iDstAddr, popNwkAddr_t iNextHop);
void PopNwkDeleteEntireRouteTable(void);
void PopNwkDeleteRoute(popNwkAddr_t iDstAddr);
void PopNwkAgeRoutes(uint8_t iIndex);
popNwkAddr_t PopNwkNextHop(popNwkAddr_t iDstAddr);
bool PopNwkCanIHearYou(popNwkAddr_t iMacSrcAddr);
uint8_t PopNwkFindPendingRouteDiscoveryEntry(popNwkAddr_t iDstAddr);


// security functions
bool PopNwkConvertIncomingOtaFrame(sPopNwkDataIndication_t *pOtaFrame);
void PopNwkConvertOutgoingOtaFrame(sPopNwkDataIndication_t *pOtaFrame);

void PopNwkOptionallyScanForNoise(void);

/* Returns TRUE if the given channel Id is between 11 and 26, FALSE otehrwise. */
bool PopNwkIsValidChannel(popChannel_t iChannel);

/* Returns TRUE if the given address is not a reserved one or broadcast, FALSE otherwise. */
bool PopNwkIsValidNwkAddress(popNwkAddr_t iAddress);

bool PopNwkIsFromThisNode(sPopNwkDataIndication_t *pIndication);
sPopNwkDataIndication_t *PopNwkMakeCopy(sPopNwkDataIndication_t *pIndication);
bool PopNwkAddRetryEntry(sPopNwkDataIndication_t *pOtaFrame, popNwkRetryFlags_t iFlags);
void PopNwkFreePendingRetryEntries( uint8_t iRouteDiscoveryIndex, bool fUnBlockOnly );
void PopNwkForwardRouteReply(uint8_t iHoldIndex);
void PopNwkProcessRouteReplyFrame(sPopNwkDataIndication_t *pIndication);
PopErrReturned_t PopNwkAddressHearedOrNot(uint8_t iIndex, popNwkAddr_t nwkAddress);
PopErrReturned_t PopNwkSearchAndMarkAddressInPreviousHopList(uint8_t iRDEntry, popNwkAddr_t nwkAddress);
popErr_t PopNwkAddEntryToPreviousHopList(uint8_t iRDEntry, popNwkAddr_t nwkAddress);
void PopNwkFreeEntireRouteDiscoveryTable(void);
void PopNwkRawRadioTransmit(sPopNwkDataIndication_t *pOtaFrame);
sPopNwkDataIndication_t *PopNwkCreateDataFrame(sPopNwkDataRequest_t *pDataReq, popNwkRetryFlags_t iFlags);


/***************************************************************************
  Externals
***************************************************************************/

/********* Security Externs. *********/
// the Neighbor Table and index.
extern sNwkNeighborTable_t gaNeighborTable[];
extern const uint8_t giPopNwkNeighborEntries;
extern popNwkLiteCounter_t miPopLiteCounter;
extern popNwkAESCounter_t miPopAESCounter;

// Used to mark the index of the Retry Table as invalid.
#define gPopNwkInvalidRetryEntryIndex_c 0xFF

/********* Externs *********/

extern sPopNwkRouteEntry_t  gaPopNwkRouteTable[];
extern const uint8_t        giPopNwkRouteEntries;
extern sPopNwkRouteDiscovery_t gaPopNwkRouteDiscoveryTable[];
extern const uint8_t        cPopNwkRouteDiscoveryEntries;
extern uint8_t              giPopNwkRouteDiscoveryHighWaterMark;
extern uint8_t              giPopNwkRouteDiscoveryCount;
extern const uint8_t         gaPopNwkLqi[];                // table to convert LQI to path cost
extern const uint16_t       giPopNwkRouteReplyWait;       // wait for the route reply from sending end before giving up
extern const uint16_t       giPopNwkRouteReplyShortWait;  // wait a bit (jitter) in case a better route comes through later
extern popNwkJoinOpts_t     giJoinOptions;                // what join options to use on form/join?
extern const uint16_t       giPopNwkJoinExpired;          // how many ms to expire each join attempt
extern popNwkAddr_t         giPopNwkStateParent;          // parent to join, 0xffff=any
extern uint8_t              giPopNwkAssociateTriesLeft;   // # of tries left to associate
extern uint8_t              giPopNwkStateJoinTriesLeft;   // how many tries are left on join?
extern const uint16_t       giPopNwkWaitInd;              // how long to wait before sending the data indication up to app on receiving a RREQ
extern const uint16_t       giPopNwkWaitRREPStartup;      // how long to wait before sending out the 1st RREP on RREQ dest node

#endif //_POP_NWK_H_

