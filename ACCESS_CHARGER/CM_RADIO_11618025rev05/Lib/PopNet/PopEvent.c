/***************************************************************************
  PopEvent.c
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  Contains task and event handling for PopNet. Also includes high-level timer 
  code.

****************************************************************************/
#include "popcfg.h"
#include "popbsp.h"
#include "popnet.h"
#include <stddef.h>

/***************************************************************************
  Local Types & Defines
***************************************************************************/


/***************************************************************************
  Local Prototypes & Macros
***************************************************************************/

// local prototypes
void PopGetNextEvent(sPopEvt_t *pPopEvt);
void PopRemoveEvent(uint8_t i);
bool PopIsValidTask(popTaskId_t iTaskId);
void PopBspStartUpTimer(void);

// and event is queued
#define PopIsEventQueued() giPopNumEvents


/***************************************************************************
  Globals & Externals
***************************************************************************/
extern void PopPhyProcessRadioMsg(void);
extern const sPopTaskList_t gaPopNetTaskList[];
extern const uint8_t cPopNumTasks;
extern sPopEvt_t gaPopEvents[];
extern const uint8_t cPopMaxEvents;

// Set a global pointer to verify the integrity of the Stack address space.
#if !gPopBigEndian_c
/* The stack is filled from final to start.*/
extern uint32_t * __SSTACK; /* Points to begin of the stack */

#define SP_Starts  __SSTACK
#define gpSP_WaterMark  __SSTACK
#else
uint16_t *gpSP_WaterMark = ((uint16_t *)SP_Starts);
#endif
popTaskId_t giPopThisTask;  // current task ID
uint8_t giPopEventHead;     // where events are added
uint8_t giPopEventTail;     // where events are removed
uint8_t giPopNumEvents;     // total # of current events
uint8_t giPopEventsHigWaterMark;  // Highest # of events allocated.

bool gMemHeapChecked;
popTableFullMask_t giPopOldTableFullMask;


#if (gPopAssertLevel_c >= 2)
popHeapSize_t giHeapAddress;
#endif


/***************************************************************************
  Code
***************************************************************************/

/*
  PopInitializeTasks

  Initialize all the tasks.
*/
void PopInitializeTasks(void)
{
  uint8_t i;

  // disable interrupts
  PopDisableInts();

  // initialize memory subsystem
  PopMemInit();

  // initialize each task
  for(i = 0; i < cPopNumTasks; ++i)
  {
    // Keep the watch from firing
    PopFeedTheDog();

    giPopThisTask = i;

    // initialize the task
    (*gaPopNetTaskList[i].pfnInitTask)();
  }

  // done initializing tasks, enable interrupts
  PopEnableInts();
 
  // don't start timer subsystem until all init tasks have completed
  PopBspStartUpTimer();
}

/*
  PopRunTasks

  Run the tasks. Never exits.
*/
void PopRunTasks(void)
{
  popTaskId_t iTaskId;
  sPopEvt_t   sPopEvt;

  // run tasks forever, in the order they entered events
  for(;;)
  {
    // Check for the C stack to see if it has not being over run.
    if (*gpSP_WaterMark != SP_WaterMark_c)
      PopAssert(gPopAssertStackOverRun_c);

    // Check the integrity of the heap.
#if (gPopAssertLevel_c == 5)
    giHeapAddress = (uint16_t)PopMemHeapCheck(NULL,NULL);
    if(giHeapAddress != gPopMemHeapOk_c)
      PopAssert(gPopAssertHeapCompromised_c);
#endif

    // Reset the COP
    PopFeedTheDog();
    {
      PopPhyProcessRadioMsg();
    }
    // is there an event ready for any task?
    if(PopIsEventQueued())
    {
      // After MemHeapCheck has finised we mark as false the flag
       gMemHeapChecked = false;
       
      // get the next event
      PopGetNextEvent(&sPopEvt);
      giPopThisTask = iTaskId = sPopEvt.iTaskId;

      // Send the App events to through the gateway (if requested)
      if (iTaskId == cAppTaskId)
        PopGatewayTargetToPC(sPopEvt.iEvtId, sPopEvt.sEvtData);

      // take precaution so we don't crash with bad task ID
      if(giPopThisTask < cPopNumTasks)
      {
        (*(gaPopNetTaskList[iTaskId].pfnEventLoop))(sPopEvt.iEvtId, sPopEvt.sEvtData);
      }
    }

    // no events, tell BSP there is time for idle work
    else
    {
      // send NULL event to BSP task to allow for polled timers
      (void)PopSetEventEx(cBspTaskId, gPopEvtNull_c, NULL);
      
      // Check the integrity of the heap when the run task is idle. 
#if (gPopAssertLevel_c >= 2 && gPopAssertLevel_c < 5)
      
       if(!gMemHeapChecked)
       {
         //Mark the flag as true
        gMemHeapChecked = true;
        giHeapAddress = PopMemHeapCheck(NULL,NULL);
        if(giHeapAddress != gPopMemHeapOk_c)
         PopAssert(gPopAssertHeapCompromised_c);
       
       }
#endif
      // TODO: Check for entering low power here when timers are interrupt driven to save a little more power   
    }
  } // forever loop
}

/*
  PopIsValidTask

  Verify task is valid and in our task list.
*/
bool PopIsValidTask(popTaskId_t iTaskId)
{
  return (iTaskId < cPopNumTasks);
}

/*
  INTERNAL: PopGetNextEvent

  Retrieves the next event from the event queue and removes it.
*/
void PopGetNextEvent(sPopEvt_t *pPopEvt)
{
  // sanity check
  if(!PopIsEventQueued())
    return;

  // copy the event from the queue
  PopMemCpy(pPopEvt, &gaPopEvents[giPopEventTail], sizeof(sPopEvt_t));

  // remove the event at the tail
  PopRemoveEvent(giPopEventTail);
}

/*
  PopFindEvent

  Find an event based on the task ID and event id

  Returns index of event or gPopEndOfIndex_c if not found.
*/
uint8_t PopFindEvent(popTaskId_t iTaskId, popEvtId_t iEvtId)
{
  uint8_t i;

  // search from tail to head
  i = giPopEventTail;
  while(i != giPopEventHead)
  {
    // did we find this timer in the event queue? If so, remove it
    if(gaPopEvents[i].iEvtId == iEvtId && gaPopEvents[i].iTaskId == iTaskId)
      return i;

    // try next event
    ++i;
    if(i >= cPopMaxEvents)
      i = 0;
  }

  // not found
  return gPopEndOfIndex_c;
}

/*
  PopRemoveEvent

  Remove the event from the queue
*/
void PopRemoveEvent(uint8_t i)
{
  
  // update the queue
  PopDisableInts();

  // clear out the entry to indicate it's free (turns it into a NULL event for BSP task)
  gaPopEvents[giPopEventTail].iEvtId = gaPopEvents[giPopEventTail].iTaskId = 0;

  // if the event is at the tail, remove it from the queue
  if(i == giPopEventTail)
  {
    --giPopNumEvents;
    ++giPopEventTail;
    if(giPopEventTail >= cPopMaxEvents)
      giPopEventTail = 0;
  }

  PopEnableInts();
}


/*
  PopSetUniqueEventEx

  Only add the event if not already in the queue. Used for things like indicating serial input 
  (rather than getting an event for each input byte, it can get a single event for a set of bytes).
*/
popErr_t PopSetUniqueEventEx(popTaskId_t iTaskId, popEvtId_t iEvtId,void *pData)
{
  // if not already in the event queue, then add it
  if(PopFindEvent(iTaskId, iEvtId) == gPopEndOfIndex_c)
    return PopSetEventEx(iTaskId, iEvtId, pData);
  return gPopErrNone_c;
}

/*
  PopSetTableFullEvent

  Only add the event to the application if not already in the queue. If the event
  already exist in the event queue, then, set the bit for specific over run.
*/
popErr_t PopSetTableFullEvent(popTableFullMask_t iMask)
{
  uint8_t i;
  sPopEvtData_t sEvtData;

  // only set the event if the bits have changed (only the user program can clear them)
  iMask = iMask | giPopOldTableFullMask;
  if(iMask == giPopOldTableFullMask)
    return gPopErrNone_c;

  // don't flood an already busy system with table full events. allow only 1.
  i = PopFindEvent(cAppTaskId, gPopEvtTableFull_c);
  if(gPopEndOfIndex_c != i)
  {
    // if the event is already on the event queue, then, add the needed bit into the bit mask.
    gaPopEvents[i].sEvtData.iTableFullMask |= iMask;
    
    // At this point the event already exist and we are just addidng another over run into the mask.  
    return gPopErrNone_c;
  }
  // save the value properly as a byte before passing it through
  sEvtData.iTableFullMask = iMask;
  // if not already in the event queue, then add it.
  return PopSetAppEvent(gPopEvtTableFull_c, (void * )sEvtData.pData);
}


/*
  PopSetEventEx

  Set an event for a task.

  Returns one of:
  gPopErrNone_c       Worked
  gPopErrBadTask_c    Invalid task ID
  gPopErrFull_c       Event queue full (couldn't add it)
*/
popErr_t PopSetEventEx(popTaskId_t iTaskId, popEvtId_t iEvtId, void *pData)
{
  sPopEvt_t sPopEvt;

  // not a valid event
  if(!PopIsValidTask(iTaskId))
    return gPopErrBadTask_c;

  // no room to add the event
  if(giPopNumEvents >= cPopMaxEvents)
  {
    PopAssert(gPopAssertOutOfEvents_c);
    return gPopErrFull_c;
  }

  // set up the event itself
  sPopEvt.iTaskId = iTaskId;
  sPopEvt.iEvtId = iEvtId;
  sPopEvt.sEvtData.pData = pData;

  // add to the queue (must be atomic, as this can be called inside interrupt handlers)
  PopDisableInts();
  ++giPopNumEvents;
  PopMemCpy(&gaPopEvents[giPopEventHead], &sPopEvt, sizeof(sPopEvt_t));
  ++giPopEventHead;
  if(giPopEventHead >= cPopMaxEvents)
    giPopEventHead = 0;
  PopEnableInts();

  // Keep Track of the highest amount of events registered. Always after the event gets registered.
  if (giPopEventsHigWaterMark < giPopNumEvents)
    giPopEventsHigWaterMark = giPopNumEvents;

  // everything worked, event added
  return gPopErrNone_c;
}

/*
  PopSetAppEvent

  Set an application event
*/
popErr_t PopSetAppEvent(popEvtId_t iId, void *pData)
{
  return PopSetEventEx(cAppTaskId, iId, pData);
}

/*
  PopSetMessageEvent

  Set an message event
*/
popErr_t PopSetMessageEvent(popEvtId_t iId, void *pData)
{
  return PopSetEventEx(cMessageTaskId, iId, pData);
}

/*
  PopSetEventTo

  Set an event to either the app task or to the nwk task
*/
popErr_t PopSetEventTo(bool isNwkData, popEvtId_t iId, void *pData)
{
  if( isNwkData )
    return PopSetEventEx(cNwkTaskId, iId, pData);
  else
    return PopSetEventEx(cAppTaskId, iId, pData);
}

/*
  PopSetEvent

  Set an event for this task.
*/
popErr_t PopSetEvent(popEvtId_t iId, void *pData)
{
  return PopSetEventEx(giPopThisTask, iId, pData);
}

/*
  PopSetEventNoData

  Set an event for this task, but there is no data associated.
*/
popErr_t PopSetEventNoData(popEvtId_t iId)
{
  return PopSetEventEx(giPopThisTask, iId, NULL);
}

