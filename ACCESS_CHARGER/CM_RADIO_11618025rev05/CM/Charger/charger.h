#pragma once

#include <stdint.h>

// Public function declarations

void charger_Init(void);
uint32_t charger_GetId(void);
void charger_SetId(const uint32_t id);