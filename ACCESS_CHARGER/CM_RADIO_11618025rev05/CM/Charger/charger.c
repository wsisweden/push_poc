#include <Charger/charger.h>

// Private declarations
static uint32_t chargerId;

// Public functions

/**
 * Initialize the charger module
 */
void charger_Init(void) {
  chargerId = 0xffffffff;
}

/**
 * Get the charger id
 * @return The charger id
 */
uint32_t charger_GetId(void) {
  return chargerId;
}

/**
 * Set the charger id
 * @param id The id
 */
void charger_SetId(const uint32_t id) {
  chargerId = id;
}