/***************************************************************************
  PopAppCm.h

  Revision history:
  Rev   Date      Comment   Description
  this  110909              first revision

***************************************************************************/
#ifndef _POPAPPCM_H_
#define _POPAPPCM_H_

#include "PopCfg.h"
#include "MpCfg.h"

#include <MP/MpNwk.h>
#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

#define PART_OF_ADDRPOOL 10   //Divide addr pool by 10
#define ADDR_TO_SPARE_MAX 400
#define ADDR_TO_SPARE_MIN 10

// time (in milliseconds) between packets. Default 100.
#ifndef gPopAppInterval_c
 #define gPopAppInterval_c 1000
#endif

// size of the application data (up to 108 bytes). Range 5 - 108. Default 32.
#ifndef gPopAppDataSize_c
 #define gPopAppDataSize_c 90
#endif

// the node hard-coded address (so no join needed)
// if 0, then normal forming/joining using PopNwkStartNetwork() is done
#ifndef gPopAppNodeAddr_c
 #define gPopAppNodeAddr_c    1013
#endif
// the node hard-coded Pan id
#ifndef gPopAppPanId_c
 #define gPopAppPanId_c    12
#endif
// the node hard-coded Channel
#ifndef gPopAppChannel_c
 #define gPopAppChannel_c    12
#endif

typedef struct PopApp_tag
{
  uint8_t                  aPopMacAddr[8];     // Mac address
  uint8_t                  iXtalCTune;         // Oscillator coarse tune
  uint8_t                  iXtalFTune;         // Oscillator fine tune
}PopApp_t;

typedef struct sPopAppNwkInfo_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
}sPopAppNwkInfo_t;

typedef struct
{
  uint16_t iAddrPoolSize;
  uint16_t iFirstAddr;
}sMpMsgAddrPoolAddr_t;

enum ePopAppMsgAddrPoolAddrRsp
{
  ADDR_POOL_ADDR_ADDRPOOLSIZE_H,
  ADDR_POOL_ADDR_ADDRPOOLSIZE_L,
  ADDR_POOL_ADDR_FIRSTADDR_H,
  ADDR_POOL_ADDR_FIRSTADDR_L,
  ADDR_POOL_ADDR_SIZE
};

typedef struct sPopAppMsgCmInfo_tag
{
  uint32_t iSerialNo;
  uint32_t iHistLogIndex;
  uint32_t iHistLogDate;
  uint32_t iEvtLogIndex;
  uint32_t iEvtLogDate;
  uint16_t iMomLogIndex;
  uint16_t iChalgErrorSum;
  uint16_t iReguErrorSum;
}sPopAppMsgCmInfo_t;

#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct sPopAppHighWaterMarks_tag
{
  uint16_t      iStackLeftAtHWM;      /* at high water mark */
  uint8_t       iEventsLeftAtHWM;
  uint8_t       iTimersLeftAtHWM;
  uint8_t       iDuplicatesLeftAtHWM;
  uint8_t       iRetriesLeftAtHWM;
  uint8_t       iRouteDiscoveriesLeftAtHWM;
  uint16_t      iStackSize;           /* total size */
  uint8_t       iMaxEvents;
  uint8_t       iMaxTimers;
  uint8_t       iDuplicateEntries;
  uint8_t       iRetryEntries;
  uint8_t       iRouteDiscoveryEntries;
  uint16_t      iStackLeftCurrent;    /* current left */
  uint8_t       iEventsLeftCurrent;
  uint8_t       iTimersLeftCurrent;
  uint8_t       iDuplicatesLeftCurrent;
  uint8_t       iRetriesLeftCurrent;
  uint8_t       iRouteDiscoveriesLeftCurrent;
} sPopAppHighWaterMarks_t;
#if gPopNeedPack_c
#pragma pack()
#endif

typedef struct sPopAppMsgAckData_tag
{
  popNwkAddr_t iDstAddr;
  uint8_t iOptions;
  uint8_t iReqCmd;
  uint8_t iMpSequence;
  uint8_t iStatus;
}sPopAppMsgAckData_t;

typedef struct sPopAppBeaconIndication_tag
{
  popChannel_t      iChannel;      // channel 11-26
  popLqi_t          iLqi;          // link quality indicator
  popNwkAddr_t      iNwkAddr;      // address of responding device
  popPanId_t        iPanId;        // 16-bit pan identifier
  popProtocol_t     iProtocol;     // 0x50=PopNet, 0x01=ZigBee
  popProtocolVer_t  iProtocolVer;  // top bit indicates join enable
  aPopMacAddr_t     aMacAddr;      // 64-bit IEEE (MAC) address
  popMfgId_t        iMfgId;        // 16-bit manufacturer identifier
  popAppId_t        iAppId;        // 16-bit application identitifer
}sPopAppBeaconIndication_t;

#pragma pack(1)
typedef struct sPopAppReset_tag
{
  sMpMsgHeader_t sHeader;
  uint8_t iKey1;
  uint8_t iKey2;
  uint8_t iDeviceType;
}sPopAppReset_t;
#pragma pack()

enum ePopAppReset
{
  POPAPP_RESET_KEY1,
  POPAPP_RESET_KEY2,
  POPAPP_RESET_DEVICETYPE,
  POPAPP_RESET_SIZE
};

#if DEBUG == 1
#pragma pack(1)
typedef struct sPopAppAssert_tag
{
  sMpMsgHeader_t sHeader;
  uint8_t iCmd;
}sPopAppAssert_t;
#pragma pack()

enum ePopAppAssert
{
  POPAPP_ASSERT_CMD,
  POPAPP_ASSERT_SIZE
};
#endif

#pragma pack(1)
typedef struct sPopAppSetRoutingMode_tag
{
  sMpMsgHeader_t sHeader;
  uint8_t iDisableRoutingMode;
}sPopAppSetRoutingMode_t;
#pragma pack()

enum ePopAppRoutingMode
{
  POPAPP_ROUTINGMODE_ROUTINGMODE,
  POPAPP_ROUTINGMODE_SIZE
};

#pragma pack(1)
typedef struct sPopAppSetServiceMode_tag
{
  sMpMsgHeader_t sHeader;
  uint8_t iServiceMode;
  uint8_t iChannel;
  uint8_t iTimeout;
  uint32_t iU32Spare;
  uint16_t iU16Spare;
  uint8_t iU8Spare;
}sPopAppSetServiceMode_t;
#pragma pack()

enum ePopAppServiceMode
{
  POPAPP_SERVICEMODE_SERVICEMODE,
  POPAPP_SERVICEMODE_CHANNEL,
  POPAPP_SERVICEMODE_TIMEOUT,
  POPAPP_SERVICEMODE_U32SPARE,
  POPAPP_SERVICEMODE_U16SPARE,
  POPAPP_SERVICEMODE_U8SPARE,
  POPAPP_SERVICEMODE_SIZE
};

#endif // _POPAPPCM_H_



