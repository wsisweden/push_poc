/************************************************************************************
* Board Parameters Configuration
*
* (c) Copyright 2008, Freescale, Inc.  All rights reserved.
*
* No part of this document must be reproduced in any form - including copied,
* transcribed, printed or by any electronic means - without specific written
* permission from Freescale.
*
* Last Inspected:
* Last Tested:
************************************************************************************/

 #ifndef __BOARD_CONFIG_H__
 #define __BOARD_CONFIG_H__

#include "MpCfg.h"

/* ARM Targets */
#define MC1322XSRB    0
#define MC1322XNCB    1
#define MC1322XUSB    2
#define MC1322XLPB     3
#define MC1322XUSERDEF 4
#define CELFreeStarPro 24
#define MC1322XMPCMB   30       // 0x1E
#define CC2538SF53    46        // 0x2E

// Backwards capability macros. No effect on MC1322X code other than avoid compile time warnings.
#define MC1319XSARD   5
#define MC1319XEVB    6
#define MC1319XAXMC   7
#define MC1321XSRB    8
#define MC1321XNCB    9
#define MC1320XAXMC   10
#define MC1319XARD    11
#define MC1319XIBOARD 12
#define MC1320x_S08QE128_EVB 13

// SJS: For Popnet
#define PopNetBoard   20
#define ITRONBOARD    21
#define Ard2Board     22
#define BDMTechBoard  23
#define MC1319X_XBEE  25


// Target board is set depending of node type
#if gNodeType_c == CHARGER_MODULE
  #define TARGET_BOARD  CC2538SF53
#elif gNodeType_c == STATISTICS_MODULE
  #define TARGET_BOARD  MC1322XNCB
#endif

#ifndef TARGET_BOARD
#define TARGET_BOARD  MC1322XSRB
#endif

#if TARGET_BOARD == MC1322XSRB
  #define DEFAULT_COARSE_TRIM   0x08
  #define DEFAULT_FINE_TRIM     0x0F
#endif

#if TARGET_BOARD == MC1322XNCB
  #define DEFAULT_COARSE_TRIM   0x08
  #define DEFAULT_FINE_TRIM     0x0F
#endif

#if TARGET_BOARD == MC1322XUSB
  #define DEFAULT_COARSE_TRIM   0x0A
  #define DEFAULT_FINE_TRIM     0x0E
#endif

#if TARGET_BOARD == MC1322XLPB
  #define DEFAULT_COARSE_TRIM   0x0B
  #define DEFAULT_FINE_TRIM     0x12
#endif

#if TARGET_BOARD == CELFreeStarPro
  #define DEFAULT_COARSE_TRIM   0x04
  #define DEFAULT_FINE_TRIM     0x0E
#endif

#if TARGET_BOARD == MC1322XMPCMB
  #define DEFAULT_COARSE_TRIM   0x08
  #define DEFAULT_FINE_TRIM     0x0F
#endif

#if TARGET_BOARD == MC1322XUSERDEF
  #define DEFAULT_COARSE_TRIM_USER_DEF   0x18
  #define DEFAULT_FINE_TRIM_USER_DEF     0x0F
  #define DEFAULT_COARSE_TRIM DEFAULT_COARSE_TRIM_USER_DEF
  #define DEFAULT_FINE_TRIM DEFAULT_FINE_TRIM_USER_DEF
#endif

#include <Hw/TIDriverLib/source/sys_ctrl.h>
/*Clock definitions*/
#define PLATFORM_CLOCK (SYS_CTRL_32MHZ)

#endif /* __BOARD_CONFIG_H__ */
