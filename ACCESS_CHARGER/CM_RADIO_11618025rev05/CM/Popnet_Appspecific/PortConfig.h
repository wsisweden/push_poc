/************************************************************************************
* This is a template header file.
*
* Author(s): B02753
*
* (c) Copyright 2005, Freescale, Inc.  All rights reserved.
*
* No part of this document must be reproduced in any form - including copied,
* transcribed, printed or by any electronic means - without specific written
* permission from Freescale.
*
************************************************************************************/
#ifndef _PORT_CONFIG_H_
#define _PORT_CONFIG_H_
#include "board_config.h"

#if TARGET_BOARD == CC2538SF53
# include "hw_memmap.h"
# include "gpio.h"
#else
# include "PLM/LibInterface/GPIO_Interface.h"
#endif
/************************************************************************************
*************************************************************************************
* Public macros
*************************************************************************************
************************************************************************************/
/* BEGIN GPIO Register Values */
  #if TARGET_BOARD == CELFreeStarPro
    #define gDirLoValue_c               0x03c00000     //MBAR_GPIO + 0x00
    #define gDataLoValue_c              0x03c00000     //MBAR_GPIO + 0x08
    #define gDirHiValue_c               0x00000400     //MBAR_GPIO + 0x04
    #define gDataHiValue_c              0x00000400     //MBAR_GPIO + 0x0C
    #define gPuEnHiValue_c              0xfffffc3f     //MBAR_GPIO + 0x14
    #define gFuncSel0Value_c            0x00000000     //MBAR_GPIO + 0x18
    #define gFuncSel2Value_c            0x05500000     //MBAR_GPIO + 0x20
    #define gPuKeepHiValue_c            0x0000001f     //MBAR_GPIO + 0x44
  #elif TARGET_BOARD == MC1322XMPCMB
    #define gDirLoValue_c               0x00320004     //MBAR_GPIO + 0x00
    #define gDataLoValue_c              0x00000000     //MBAR_GPIO + 0x08
    #define gDirHiValue_c               0x00000000     //MBAR_GPIO + 0x04
    #define gDataHiValue_c              0x00000000     //MBAR_GPIO + 0x0C
    #define gPuEnHiValue_c              0xffffffff     //MBAR_GPIO + 0x14
    #define gFuncSel0Value_c            0x00055500     //MBAR_GPIO + 0x18
    #define gFuncSel2Value_c            0x01005000     //MBAR_GPIO + 0x20
    #define gPuKeepHiValue_c            0x00000000     //MBAR_GPIO + 0x44
  #else
    #define gDirLoValue_c               0x03c00000     //MBAR_GPIO + 0x00
    #define gDataLoValue_c              0x03c00000     //MBAR_GPIO + 0x08
    #define gDirHiValue_c               0x00000000     //MBAR_GPIO + 0x04
    #define gDataHiValue_c              0x00000000     //MBAR_GPIO + 0x0C
    #define gPuEnHiValue_c              0xffffffff     //MBAR_GPIO + 0x14
    #define gFuncSel0Value_c            0x00000000     //MBAR_GPIO + 0x18
    #define gFuncSel2Value_c            0x00000000     //MBAR_GPIO + 0x20
    #define gPuKeepHiValue_c            0x000000df     //MBAR_GPIO + 0x44
  #endif
  #define gPuEnLoValue_c              0xffffffff     //MBAR_GPIO + 0x10
  #define gFuncSel1Value_c            0x00000000     //MBAR_GPIO + 0x1C
  #define gFuncSel3Value_c            0x00000000     //MBAR_GPIO + 0x24
  #define gInputDataSelLoValue_c      0x00000000     //MBAR_GPIO + 0x28
  #define gInputDataSelHiValue_c      0x00000000     //MBAR_GPIO + 0x2C
  #define gPuSelLoValue_c             0x00003000     //MBAR_GPIO + 0x30
  #define gPuSelHiValue_c             0x8001c000     //MBAR_GPIO + 0x34
  #define gHystEnLoValue_c            0x00000000     //MBAR_GPIO + 0x38
  #define gHystEnHiValue_c            0x00000000     //MBAR_GPIO + 0x3C
  #define gPuKeepLoValue_c            0xc0000000     //MBAR_GPIO + 0x40
/* END GPIO Register Values */


#if TARGET_BOARD == CC2538SF53
# define FLASH_WRITE_PROTECT_LOW GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_2, 0)
# define FLASH_WRITE_PROTECT_HIGH GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_2, GPIO_PIN_2)
#else
# define FLASH_WRITE_PROTECT_LOW {GPIO.DataResetLo = (1<<2);}
# define FLASH_WRITE_PROTECT_HIGH {GPIO.DataSetLo = (1<<2);}
#endif
/************************************************************************************
*************************************************************************************
* Public prototypes
*************************************************************************************
************************************************************************************/

/************************************************************************************
*************************************************************************************
* Public type definitions
*************************************************************************************
************************************************************************************/

/************************************************************************************
*************************************************************************************
* Public memory declarations
*************************************************************************************
************************************************************************************/

/************************************************************************************
*************************************************************************************
* Public functions
*************************************************************************************
************************************************************************************/
#endif /* _PORT_CONFIG_H_ */

