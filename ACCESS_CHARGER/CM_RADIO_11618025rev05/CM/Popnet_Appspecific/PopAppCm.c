/***************************************************************************
  PopAppCm.c

  Revision history:
  Rev   Date      Comment   Description
  this  110909              First revision
***************************************************************************/

#include "PopAppCm.h"
#include "../MP/MpGw.h"
#include "../MP/MpNwk.h"
#include "../MP/MpSerial.h"
#include "../Message/MessageTask.h"

#include "PopCfg.h"
#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>
#include <PopNet/PopNwk.h>


#include <Hw/TIDriverLib/inc/hw_memmap.h>
#include <Hw/TIDriverLib/source/gpio.h>
#include <Hw/TIPhy/TiPhy.h>
#include <Hw/TIDriverLib/source/ioc.h>

#include "../Log/log_applicationParameters.h"
#include "../Flash/flash.h"
#include "../Log/Private/log_ProgramStarter.h"

#include <Charger/charger.h>

/***************************************************************************
  Local Types & Defines
 ***************************************************************************/
// must set gPopMicroPowerExternalFlash_d=1 in compiler and linker options
// Define any event IDs required by the application here...
#define gPopAppFormNetwork_c (gPopEvtUser_c + 32)
#define gPopAppJoinNetwork_c (gPopEvtUser_c + 33)

// Define any application timers here.
#define gPopAppFormNetworkTimerId_c      (gPopAppTimerFirstPos_c + 3)
#define gPopAppJoinNetworkTimerId_c      (gPopAppTimerFirstPos_c + 4)
#define gPopAppNwkDataChangeTimer_c      (gPopAppTimerFirstPos_c + 5)
#define gPopAppAckRspTimer_c             (gPopAppTimerFirstPos_c + 6)
#define gPopAppSoftwareResetTimerId_c    (gPopAppTimerFirstPos_c + 7)
#define gPopAppServiceModeDelayTimerId_c (gPopAppTimerFirstPos_c + 8)
#define gPopAppServiceModeTimerId_c      (gPopAppTimerFirstPos_c + 9)

// try retries if haven't received the ACK in 2 seconds
#define gPopAppLedBlinkTime_c             100
#define gPopAppBeaconIndicationTime_c      1000
#define gPopAppBusyTime_c                  5000
#define gPopAppFormNetworkTime_c           30000
#define gPopAppJoinNetworkTime_c           30000
#define gPopAppAckRspTime_c                500
#define gPopAppSoftwareResetTime_c         200
#define gPopAppServiceModeDelayTime_c      3000
#define gPopAppServiceModeTime_c           60000

/***************************************************************************
  Prototypes
 ***************************************************************************/
void PopAppHandleTimer(popTimerId_t iTimerId);
bool PopAppValidNwkData(void);
void PopAppSetNewState(popStatus_t iNwkStartConfirm);
void PopAppScanForParentHandling(
    sPopNwkBeaconIndication_t *pNwkBeaconIndication);
void PopAppScanForParentConfirm(sPopAppBeaconIndication_t *pBeacon);
void PopAppFormNetwork(sPopEvtData_t *pData);
void PopAppJoinNetwork(sPopEvtData_t *pData);
void PopAppJoinIndication(void);
void PopAppDataIndication(sPopNwkDataIndication_t *pIndication,
    bool *pSendThruGateway);
void PopAppGetAddrPoolAddr(void);
void PopAppAddrPoolAddr(uint16_t iDstAddr, uint8_t iMpSequence,
    uint16_t iAddrToSpare, uint16_t iFirstAvailableAddr);
void PopAppAcknowledge(uint16_t iDstAddr, uint8_t iMpSequence,
    uint8_t popNwkDataReqOpts, uint8_t iReqCmd, uint8_t iStatus);
void PopAppCrystalTuneInit(void);
void PopAppDefaultNodeAddrInit(void);
void PopAppReset(void);

#if DEBUG == 1
#warning DEBUG mode 1!!
void PopAppDebugDataReq(uint16_t iDstAddr, uint8_t iMpSequence);
void PopAppAssertDataReq(uint16_t iDstAddr, uint8_t iMpSequence);
#endif
// PopNet Task prototypes
void PopPhyTaskInit(void);
void PopPhyTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopBspTaskInit(void);
void PopBspTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopSerialTaskInit(void);
void PopSerialTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopNwkTaskInit(void);
void PopNwkTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopAppTaskInit(void);
void PopAppTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);

/***************************************************************************
  Globals and Externals
 ***************************************************************************/
/* Popnet */
// every application has a list of tasks called gaPopNetTasks[]
const sPopTaskList_t gaPopNetTaskList[] = {
// do NOT reorder these tasks without reordering the task ID
// constants below (cPhyTaskId)
    { PopPhyTaskInit, PopPhyTaskEventLoop },       // PopNet physical layer task
    { PopBspTaskInit, PopBspTaskEventLoop },       // PopNet BSP task
    { PopNwkTaskInit, PopNwkTaskEventLoop },       // PopNet networking layer task
    { PopSerialTaskInit, PopSerialTaskEventLoop }, // Serial I/O task
    { PopAppTaskInit, PopAppTaskEventLoop },  // application task
    { MpGwTaskInit, MpGwTaskEventLoop },   	       // Micropower gateway task
    { MpNwkTaskInit, MpNwkTaskEventLoop },   	   // Micropower network task
#if gUseMpSerial_d
    { MpSerialTaskInit, MpSerialTaskEventLoop },        // Mp Serial task
#endif
    { MessageTaskInit, MessageTaskEventLoop },     // Message interface task
};
const uint8_t cPopNumTasks = PopNumElements(gaPopNetTaskList);

// task ID constants (must match the above list)
const popTaskId_t cPhyTaskId = 0;
const popTaskId_t cBspTaskId = 1;
const popTaskId_t cNwkTaskId = 2;
const popTaskId_t gPopPhyIndicationTask = 2; // MUST be same as cNwkTaskId
const popTaskId_t cSerialTaskId = 3;
const popTaskId_t cAppTaskId = 4;
const popTaskId_t cGwTaskId = 5;
const popTaskId_t cMpNwkTaskId = 6;
const popTaskId_t cMpSerialTaskId = 7;
const popTaskId_t cMessageTaskId = 8;

/* Mp */
uint16_t giState; //  declaration of the global state
extern uint8_t giMpSequence; // Sequence number used for OTA data communication
bool gfMpIsProcessingScanConfirm = FALSE; // set if gPopEvtNwkScanConfirm_c should be handled
bool gfMpBeaconFound = FALSE; // number of beacons in list (with join enable)

uint32_t giFwType = 11618025;
uint32_t giFwVer = 501;
uint32_t giChkSum = 0x0A0B0C0D;
uint8_t giResetDeviceType = 0;

uint8_t giServiceMode = 0;
uint8_t giServiceChannel = 0;
uint8_t giServiceTimeout = 0;

popChannel_t giDefaultChannel = giDefaultChannel_c;                             // Default Channel
popPanId_t giDefaultPanId = giDefaultPanId_c;                                   // Default PanId
popNwkAddr_t giDefaultNodeAddr = giDefaultNodeAddr_c;                           // Default Node address

sPopAppNwkInfo_t gsPopAppParentNwkInfo; // global struct for saving nwk info of parent node
sMpJoinNetworkReq_t gsPopAppJoinReqNwkInfo; // global struct for saving nwk info from join request
sPopAppMsgAckData_t gsPopAppMsgAckData; // global struct for ack rsp data
sPopAppBeaconIndication_t gsPopAppBeaconIndication; // global struct for beacons with join enable

/***************************************************************************
  Code
 ***************************************************************************/

/*
  PopAppTaskInit
 */
void PopAppTaskInit(void) {
  //006 Set initial state
  giState = INIT;
  // set the event range for gateway
  PopGatewaySetEventRange(0xFF, 0xFF);
  // Init crystal trim values
  PopAppCrystalTuneInit();
  // Init join status
  PopNwkSetJoinStatus(gPopJoinEnabled_d);
  // Init default node address
  PopAppDefaultNodeAddrInit();
  // If valid network data stored in NVM
  if (PopAppValidNwkData()) {
    PopNwkStartNetwork();
  } else {
    giState = NOT_CONNECTED;
  }

  charger_Init();
}

/*
  PopAppTaskEventLoop

  All events from PopNet to the application occur here. The iEvtId is a number
  from 0x00-0xff (not a bit mask). The contents of sEvtData depends on the iEvtId.
  See type sPopEvtData_t in PopNet.h.
 */
void PopAppTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  switch (giState) {
    case INIT:
      switch (iEvtId) {
          case gPopEvtNwkStartConfirm_c:
            // check if succeeded and set state thereafter
            PopAppSetNewState(sEvtData.iNwkStartConfirm);
            break;
          default:
          // Nothing to do
            break;
        }
      break;

    case NOT_CONNECTED:
      switch (iEvtId) {
        case gPopEvtNwkScanConfirm_c:
          MpGwScanNetworkRsp(sEvtData.pNwkScanConfirm);
          break;
        default:
          // Nothing to do
          break;
        }
      break;

    case START:
      switch (iEvtId) {
        case gPopEvtNwkStartConfirm_c:
          // check if succeeded and set state thereafter
            PopAppSetNewState(sEvtData.iNwkStartConfirm);
            // answer with StartNetworkRsp since state = START
            if (sEvtData.iNwkStartConfirm != gPopStatusBusy_c) {
              MpGwStartNetworkRsp(sEvtData.iNwkStartConfirm);
            }
            break;
          case gPopAppFormNetwork_c:
            PopAppFormNetwork(sEvtData.pData);
            PopMemFree(sEvtData.pData);
            break;
          default:
            // Nothing to do
            break;
        }
        break;

    case JOIN:
      switch (iEvtId) {
        case gPopEvtNwkStartConfirm_c:
          // check if succeeded and set state thereafter
          PopAppSetNewState(sEvtData.iNwkStartConfirm);
          // answer with JoinNetworkRsp since state = JOIN
          if (sEvtData.iNwkStartConfirm != gPopStatusBusy_c) {
            // Send gateway response
            MpGwJoinNetworkRsp(sEvtData.iNwkStartConfirm);
            if (sEvtData.iNwkStartConfirm == gPopStatusJoinSuccess_c) {
              // Send data OTA to inform parent that joining was successfull
              PopAppGetAddrPoolAddr();
            }
          }
          break;
        case gPopEvtNwkScanConfirm_c:
          if (gfMpIsProcessingScanConfirm) {
            PopAppScanForParentConfirm(&gsPopAppBeaconIndication);
          }
          break;
        case gPopAppJoinNetwork_c:
          PopAppJoinNetwork(sEvtData.pData);
          break;
        default:
          // Nothing to do
          break;
      }
      break;

    case CONNECTED:
      switch (iEvtId) {
          // a message has come in on the radio
          case gPopEvtDataIndication_c: {
            extern uint8_t ispCommanderModeFlag;

            if (!ispCommanderModeFlag) {
              if (PopNwkPayloadLen(sEvtData.pDataIndication) <= MAX_PAYLOAD) {
                bool SendThruGateway = TRUE;

                // 002 Handle DataIndication in MpNwk
                MpNwkDataIndication(sEvtData.pDataIndication, &SendThruGateway);
                PopAppDataIndication(sEvtData.pDataIndication, &SendThruGateway);
                // Check if message should be sent thru gateway
                if (SendThruGateway) {
                  // send thru gateway
                  MpGwDataIndication(sEvtData.pDataIndication);
                }
              }
            }
          }
          break;
          // when data has been sent with PopNwkDataRequest(), this confirm indicates
          // the request has been sent out this radio (but not necessarily delivered
          // to the final destination).
          case gPopEvtDataConfirm_c:
            // 003 Handle DataConfirm in MpNwk
            MpNwkDataConfirm(sEvtData.iDataConfirm);
            break;
          // 006 Handle initiate route event
          case gPopEvtNwkInitiateRoute_c:
            MpNwkInitiateRouteHandling();
            break;
          // 006 Handle route onfirm event
          case gPopEvtNwkRouteConfirm_c:
            MpNwkEndRouteHandling(sEvtData.iWord);
            break;
          case gPopEvtNwkLeaveConfirm_c:
            // set state to NotConnected
            PopAppSetNewState(gMpLeaveNetworkReq_c);
            MpGwLeaveNetworkRsp();
            break;
          case gPopEvtNwkScanConfirm_c:
            MpGwScanNetworkRsp(sEvtData.pNwkScanConfirm);
            break;
        }
      break;

    case TEST:
      switch(iEvtId){
        // Test program start network handling
        case gPopEvtNwkStartConfirm_c:    // start network response
          PopAppSetNewState(sEvtData.iNwkStartConfirm);
          PopSetEventEx(cMpSerialTaskId, gPopEvtNwkStartConfirm_c, sEvtData.pData);
          break;
      }
      break;

    default:
      // No valid state. Reset MCU and store assert
      PopAssert(gPopAssertBadReceivePtr_c);
      break;
  }

  /******************************************************************************/
  // General events outside state machine
  switch (iEvtId) {
#if gUseMpSerial_d
    case gMpSerialDataReadyEvt_c: // 0x94
      // should be available in all states
      MpSerialHandler();
      break;
#endif
    // beacon indication  available in state = NOT_CONNECTED, CONNECTED, START, JOIN
    case gPopEvtNwkBeaconIndication_c:
        // process beacon indications
      if (gfMpGwIsProcessingBeacon_c) {
        MpGwScanNetworkHandling(sEvtData.pNwkBeaconIndication);
      } else if (gfMpIsProcessingScanConfirm) {
        PopAppScanForParentHandling(sEvtData.pNwkBeaconIndication);
      } else if(gfMpPlacementStart){
        MpGwNodePlaceProcessBeacon(sEvtData.pNwkBeaconIndication);
      }
      break;
    // Timer handling handled outside state machine
    case gPopEvtTimer_c:
       PopAppHandleTimer(sEvtData.iTimerId);
      break;
    default:
      // Corrupt data received, free memory allocated from heap to avoid loss of memory
      PopMemFree(sEvtData.pData);
      break;
  }

  /******************************************************************************/
  // always call the default handler to free messages and clean up.
  PopAppDefaultHandler(iEvtId, sEvtData);
}

/*
  PopAppHandleTimer

  A timer has expired. Handle it.
  o  The application may use up to 255 timers
  o  The maximum number of timers is set in PopCfg.h (gPopMaxTimers_c)
  o  The timers are not real-time
 */
void PopAppHandleTimer(popTimerId_t iTimerId) {
  switch (iTimerId) {
    case  gPopAppFormNetworkTimerId_c:
      // reset state to NOT_CONNECTED
      PopNwkScanComplete();
      PopAppSetNewState(gPopErrFailed_c);
      MpGwStartNetworkRsp(gPopErrFailed_c);
      break;
    case  gPopAppJoinNetworkTimerId_c:
      // reset state to NOT_CONNECTED
      PopNwkScanComplete();
      PopAppSetNewState(gPopErrFailed_c);
      MpGwJoinNetworkRsp(gPopErrFailed_c);
      break;
    case gPopAppNwkDataChangeTimer_c: {
      // Time to store network data
      (void) PopNvStoreNwkData();
      break;
    }
    case gPopAppAckRspTimer_c: {
      // Send ack response
      PopAppAcknowledge(gsPopAppMsgAckData.iDstAddr,
          gsPopAppMsgAckData.iMpSequence, gsPopAppMsgAckData.iOptions,
          gsPopAppMsgAckData.iReqCmd, gsPopAppMsgAckData.iStatus);
      break;
    }
    case  gPopAppSoftwareResetTimerId_c: {
      /* Call function to handle reset */
      PopAppReset();
      break;
    }
    case  gPopAppServiceModeDelayTimerId_c: {
      if (giServiceMode) {
        // Set the channel at the PHY level.
        PopPhySetChannel(giServiceChannel);

        if (giServiceTimeout) {
          // giTimeout is in minute resolution, start one minute timeout to start with
          PopStartTimerEx(cAppTaskId,gPopAppServiceModeTimerId_c, gPopAppServiceModeTime_c);
          giServiceTimeout--;
        }
      } else {
        // Get the configured channel
        uint8_t iChannel = PopNwkGetChannel();
        // Set the channel at the PHY level.
        PopPhySetChannel(iChannel);
        // Stop service mode timer
        PopStopTimerEx(cAppTaskId,gPopAppServiceModeTimerId_c);
      }
      break;
    }
    case  gPopAppServiceModeTimerId_c: {
      if (giServiceTimeout && giServiceMode) {
        // giTimeout is in minute resolution, restart one minute timeout
        PopStartTimerEx(cAppTaskId,gPopAppServiceModeTimerId_c, gPopAppServiceModeTime_c);
        giServiceTimeout--;
      } else {
        // Get the configured channel
        uint8_t iChannel = PopNwkGetChannel();
        // Set the channel at the PHY level.
        PopPhySetChannel(iChannel);
        // Reset service mode
        giServiceMode = FALSE;
      }
      break;
    }
    default:
      break;
  }
}

/*******************************************************************************
 ---------------- PopAppValidNwkData -----------------------------------------------
 Checks if Network data is within limits and returns TRUE if so.
 -------------------------------------------------------------------------------
 *******************************************************************************/
bool PopAppValidNwkData(void) {

  // retrieve any stored NWK data (e.g., PAN ID, channel, nwk key, etc...)
  // this must occur before starting the network
  if ((11 <= PopNwkGetChannel() && PopNwkGetChannel() <= 26)
      && (PopNwkGetPanId() < gPopNwkReservedPan_c)
      && (PopNwkGetNodeAddr() < gPopNwkReservedAddr_c)) {
        return TRUE;
       }

  return FALSE;
}

/*******************************************************************************
 ---------------- PopAppSetNewState ------------------------------------------
 Checks if Network has been successfully started or not and set state
 accordingly. If busy start a timer which will reset state to NOT_CONNECTED if
 no other event has been received
 -------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppSetNewState(popStatus_t iNwkStartConfirm) {

  // Check if network has started
  switch (iNwkStartConfirm) {
    case gPopStatusJoinSuccess_c:
      PopStopTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c);
      giState = CONNECTED;  // Set state to Connected
      (void) PopNvStoreNwkData();  // store network data
      break;

    case gPopStatusFormSuccess_c:
      PopStopTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c);
      giState = CONNECTED;  // Set state to Connected
      PopNwkSetAddrCount(gMpSmNwkReservedAddr_c - 1); // addr pool 0x0001 - 0xFEFF
      PopNwkSetNextAddrAvailable(0x0001); // First available addr
      (void) PopNvStoreNwkData();  // store network data
      break;

    case gPopStatusAlreadyOnTheNetwork_c:
    case gPopStatusSilentStart_c:
      PopStopTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c);
      PopStopTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c);
      giState = CONNECTED;
      break;

    case gPopErrFailed_c:
    case gPopStatusNoMem_c:
    case gPopStatusBadParms_c:
    case gPopStatusPanAlreadyExists_c:
    case gPopStatusJoinFailure_c:
    case gPopStatusJoinDisabled_c:
    case gPopStatusWrongMfgId_c:
    case gPopStatusNoParent_c:
      PopStopTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c);
      PopStopTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c);
      giState = NOT_CONNECTED;
      break;

    case gPopStatusBusy_c:
      ;
      break;

    case gMpStartNetworkReq_c:
      // start timer which will reset state to NOT_CONNECTED when fired
      PopStartTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c,
      gPopAppFormNetworkTime_c);
      giState = START;
      break;

    case gMpJoinNetworkReq_c:
      // start timer which will reset state to NOT_CONNECTED when fired
      PopStartTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c,
      gPopAppJoinNetworkTime_c);
      giState = JOIN;
      break;

    case gMpLeaveNetworkReq_c:
      giState = NOT_CONNECTED; // set state to NotConnected
      PopNwkSetAddressServer(0x0000); // set back to default
      PopNwkSetNextAddrAvailable(0x0000); // reset
      PopNwkSetAddrCount(0x0000); // reset
      (void) PopNvStoreNwkData();  // store network data
      break;

    default:
      giState = NOT_CONNECTED;
      break;
  }

  // Act on AppState
  switch(giState){
    case CONNECTED:
      // start Gw watchdog timer
      MpGwStartWatchdog();
      // start Nwk watchdog timer
#if NWK_WATCHDOG
      MpNwkStartWatchdog();
#endif
      break;

    case NOT_CONNECTED:
      // stop Gw watchdog timer
      MpGwStopWatchdog();
      // stop Nwk watchdog timer
#if NWK_WATCHDOG
      MpNwkStopWatchdog();
#endif
      break;

    default:
      break;
  }
}

/*******************************************************************************
 ---------------- PopAppScanForParentHandling ---------------------------------------
  popEvtNwkBeaconIndication event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppScanForParentHandling(
    sPopNwkBeaconIndication_t *pNwkBeaconIndication) {

  if ((pNwkBeaconIndication->iProtocolVer & 0x80)
      && (gfMpBeaconFound != true)) {
    // copy beacon to global struct
    gsPopAppBeaconIndication.iChannel = pNwkBeaconIndication->iChannel;
    gsPopAppBeaconIndication.iLqi = pNwkBeaconIndication->iLqi;
    gsPopAppBeaconIndication.iNwkAddr = pNwkBeaconIndication->iNwkAddr;
    gsPopAppBeaconIndication.iPanId = pNwkBeaconIndication->iPanId;
    gsPopAppBeaconIndication.iProtocol = pNwkBeaconIndication->iProtocol;
    gsPopAppBeaconIndication.iProtocolVer = pNwkBeaconIndication->iProtocolVer;
    gsPopAppBeaconIndication.aMacAddr[0] = pNwkBeaconIndication->aMacAddr[0];
    gsPopAppBeaconIndication.aMacAddr[1] = pNwkBeaconIndication->aMacAddr[1];
    gsPopAppBeaconIndication.aMacAddr[2] = pNwkBeaconIndication->aMacAddr[2];
    gsPopAppBeaconIndication.aMacAddr[3] = pNwkBeaconIndication->aMacAddr[3];
    gsPopAppBeaconIndication.aMacAddr[4] = pNwkBeaconIndication->aMacAddr[4];
    gsPopAppBeaconIndication.aMacAddr[5] = pNwkBeaconIndication->aMacAddr[5];
    gsPopAppBeaconIndication.aMacAddr[6] = pNwkBeaconIndication->aMacAddr[6];
    gsPopAppBeaconIndication.aMacAddr[7] = pNwkBeaconIndication->aMacAddr[7];
    gsPopAppBeaconIndication.iMfgId = pNwkBeaconIndication->iMfgId;
    gsPopAppBeaconIndication.iAppId = pNwkBeaconIndication->iAppId;
    // Set flag for beacon found
    gfMpBeaconFound = TRUE;
  }

}

/*******************************************************************************
 ---------------- PopAppScanForParentConfirm ---------------------------------------
  PopAppScanForParentConfirm event handling.
  If State = JOIN. Find a node with join enabled and call JoinNetwork
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppScanForParentConfirm(sPopAppBeaconIndication_t *pBeacon) {

  if (giState == JOIN) {

    bool ParentFound;
    ParentFound = FALSE;

    // find the network to join
    if (gfMpBeaconFound) {
      // verify this is PopNet, and our MfgId, and has join enabled and the right app ID
      if (pBeacon->iProtocol == 0x50 &&
        /*pBeacon->iAppId == 0x0002 &&*/
      pBeacon->iMfgId == gPopMfgId_c && pBeacon->iProtocolVer & 0x80) {
        // Check if specific channel was requested
        if (gsPopAppJoinReqNwkInfo.iChannel != 0) {
          // compare found channel with requested
          if (gsPopAppJoinReqNwkInfo.iChannel == pBeacon->iChannel) {
            // Check if specific PanId requested
            if (gsPopAppJoinReqNwkInfo.iPanId != 0xFFFF) {
              // Compare found PanId with requested
              if (gsPopAppJoinReqNwkInfo.iPanId == pBeacon->iPanId) {
                // Parent found
                ParentFound = TRUE;

              }  // Pan Id did not match requested
              else {
                // Parent not found (yet)
                ;
              }

            }  // PanId is don't care
            else {
                // Parent found
                ParentFound = TRUE;
            }

          }  // Channel did not match requested
          else {
            // Parent not found (yet)
            ;
          }

        }  // Channel is don't care
        else {
          // Check if specific PanId requested
          if (gsPopAppJoinReqNwkInfo.iPanId != 0xFFFF) {
            // Compare found PanId with requested
            if (gsPopAppJoinReqNwkInfo.iPanId == pBeacon->iPanId) {
              // Parent found
              ParentFound = TRUE;

            }  // Pan Id did not match requested
            else {
              // Parent not found (yet)
              ;
            }

          }  // PanId is don't care
          else {
              // Parent found
              ParentFound = TRUE;
          }

        }

      }

    }  // End if beacon found

    // Check if parent found
    if (ParentFound) {
      // set global parent info and try to join this node
      gsPopAppParentNwkInfo.iChannel = pBeacon->iChannel;
      gsPopAppParentNwkInfo.iPanId = pBeacon->iPanId;
      gsPopAppParentNwkInfo.iNwkAddr = pBeacon->iNwkAddr;
      PopSetEventEx(cAppTaskId, gPopAppJoinNetwork_c, &gsPopAppParentNwkInfo);

    }  // No network found, send response directly!
    else {
      PopAppSetNewState(gPopErrFailed_c);
      MpGwJoinNetworkRsp(gPopStatusNoParent_c);
    }

  }  // End JOIN
  gfMpIsProcessingScanConfirm = FALSE; // now it is taken care of
  gfMpBeaconFound = FALSE;
}

/*******************************************************************************
 ---------------- PopAppFormNetwork ----------------------------------------------
  PopAppFormNetwork event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppFormNetwork(sPopEvtData_t *pData) {
  sPopAppNwkInfo_t *pPopAppNwkParams = (sPopAppNwkInfo_t*) pData;
  // Form network using JoinOptions normal
  PopNwkFormNetwork(PopNwkGetJoinOptions(), pPopAppNwkParams->iChannel,
                    pPopAppNwkParams->iPanId);

}

/*******************************************************************************
 ---------------- PopAppJoinNetwork ----------------------------------------------
  PopAppJoinNetwork event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppJoinNetwork(sPopEvtData_t *pData) {
  sPopAppNwkInfo_t *pPopAppNwkParams = (sPopAppNwkInfo_t*) pData;
  // call Join network
  PopNwkJoinNetwork(PopNwkGetJoinOptions(), pPopAppNwkParams->iChannel,
      pPopAppNwkParams->iPanId, pPopAppNwkParams->iNwkAddr);
}

/*******************************************************************************
 ---------------- MpAppJoinIndication -------------------------------------------
 Send MpAppJoinIndication message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppJoinIndication(void) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMessage_t sMessage;
  uint8_t *pData;

  sMpMsgHeaderRsp.iCmd = gJoinIndication_c;
  sMpMsgHeaderRsp.iMpSequence = giMpSequence++;
  sMpMsgHeaderRsp.iControl = 0x00;

  // add JoinIndication data
  // no data

  // add network data
  sMessage.iDstAddr = gsPopAppParentNwkInfo.iNwkAddr;
  sMessage.iOptions = gPopNwkDataReqOptsDefault_c;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  // send the message
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppGetAddrPoolAddr ------------------------------------------
  Build and send PopAppGetAddrPoolAddr message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppGetAddrPoolAddr(void) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMessage_t sMessage;
  uint8_t *pData;

  sMpMsgHeaderRsp.iCmd = gGetAddrPoolAddr_c;
  sMpMsgHeaderRsp.iMpSequence = giMpSequence++;
  sMpMsgHeaderRsp.iControl = 0x00;

  // add GetAddrPoolAddr data
  // no data

  // add network data
  sMessage.iDstAddr = gsPopAppParentNwkInfo.iNwkAddr;
  sMessage.iOptions = gPopNwkDataReqOptsDefault_c;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  // send the message
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppAddrPoolAddr ---------------------------------------------
  Build and send PopAppAddrPoolAddr message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppAddrPoolAddr(uint16_t iDstAddr, uint8_t iMpSequence,
    uint16_t iAddrToSpare, uint16_t iFirstAvailableAddr) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMpMsgAddrPoolAddr_t sMpMsgAddrPoolAddr;
  sMessage_t sMessage;
  uint8_t *pData;

  // add header
  sMpMsgHeaderRsp.iCmd = gAddrPoolAddr_c;
  sMpMsgHeaderRsp.iMpSequence = iMpSequence;
  sMpMsgHeaderRsp.iControl = 0x00;

  // add AddrPoolAddr data
  sMpMsgAddrPoolAddr.iAddrPoolSize = iAddrToSpare;
  sMpMsgAddrPoolAddr.iFirstAddr = iFirstAvailableAddr;

  // add network data
  sMessage.iDstAddr = iDstAddr;
  sMessage.iOptions = gPopNwkDataReqOptsDefault_c;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t)
      + sizeof(sMpMsgAddrPoolAddr_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)), &sMpMsgAddrPoolAddr,
      sizeof(sMpMsgAddrPoolAddr_t));
  // send the response
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppAcknowledge ------------------------------------------
  Build and send PopAppAcknowledge message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppAcknowledge(uint16_t iDstAddr, uint8_t iMpSequence,
    uint8_t popNwkDataReqOpts, uint8_t iReqCmd, uint8_t iStatus) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMpMsgAcknowledge_t sMpMsgAcknowledge;
  sMessage_t sMessage;
  uint8_t *pData;

  sMpMsgHeaderRsp.iCmd = gAcknowledge_c;
  sMpMsgHeaderRsp.iMpSequence = iMpSequence;
  sMpMsgHeaderRsp.iControl = 0x00;

  // Add data
  sMpMsgAcknowledge.iReqCmd = iReqCmd;
  sMpMsgAcknowledge.iStatus = iStatus;
  sMpMsgAcknowledge.type = 3; // Charger type according to service tool
  sMpMsgAcknowledge.id = charger_GetId();

  // add network data
  sMessage.iDstAddr = iDstAddr;
  sMessage.iOptions = popNwkDataReqOpts;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t)
      + sizeof(sMpMsgAcknowledge_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)), &sMpMsgAcknowledge,
      sizeof(sMpMsgAcknowledge_t));
  // send the message
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppDataIndication -------------------------------------------
  PopAppDataIndication event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppDataIndication(sPopNwkDataIndication_t *pIndication,
    bool *pSendThruGateway) {

  sMpMsgHeader_t sMpMsgHeaderReq;
  sMpMsgHeader_t sMpMsgHeaderRsp;

  uint8_t *pData;
  uint8_t *pPayload;
  uint8_t iPayloadLen;
  uint8_t MsgType;

  pPayload = PopNwkPayload(pIndication);
  iPayloadLen = PopNwkPayloadLen(pIndication);

  MsgType = MpNwkGetMsgType(pPayload);   // get message type (request or response)

  switch (MsgType)    // Check if response or request
  {
    case RspMsg_c:
     sMpMsgHeaderRsp.iCmd = pPayload[iCmdOffset_c];
      sMpMsgHeaderRsp.iMpSequence = pPayload[iMpSeqOffset_c];

     switch(sMpMsgHeaderRsp.iCmd)
     {
       case gAddrPoolAddr_c:
       {
         uint16_t iAddrPoolSize = 0;
         uint16_t iFirstAvailableAddr = 0;

         // This message should not be sent thru gateway
         *pSendThruGateway = FALSE;

         if(iPayloadLen == (sizeof(sMpMsgHeader_t) + sizeof(sMpMsgAddrPoolAddr_t)))
         {
           // Get data from message
           pData = &pPayload[aDataOffset_c];

           iAddrPoolSize = (uint16_t)(pData[ADDR_POOL_ADDR_ADDRPOOLSIZE_H]);
           iAddrPoolSize |= (uint16_t)(pData[ADDR_POOL_ADDR_ADDRPOOLSIZE_L] << 8);

           iFirstAvailableAddr = (uint16_t)(pData[ADDR_POOL_ADDR_FIRSTADDR_H]);
           iFirstAvailableAddr |= (uint16_t)(pData[ADDR_POOL_ADDR_FIRSTADDR_L] << 8);

           //store Addr pool info
           PopNwkSetAddrCount(iAddrPoolSize);
           PopNwkSetNextAddrAvailable(iFirstAvailableAddr);
           // Inform the NVM engine that network data has changed
           PopNvmDataMarkAsChanged(gPopNvmNwkDataChanged_c);

           // Send gateway response
           MpGwGetRadioDataRsp();

           // Send data OTA to inform parent that joining was successfull
           PopAppJoinIndication();
         }
       }
       break;

     }
    break;

    case ReqMsg_c:
     sMpMsgHeaderReq.iCmd = pPayload[iCmdOffset_c];
     sMpMsgHeaderReq.iMpSequence = pPayload[iMpSeqOffset_c];
     switch(sMpMsgHeaderReq.iCmd)
     {
       case gGetAddrPoolAddr_c:
       {
         uint16_t iAddrToSpare = 0;
         uint16_t iFirstAvailableAddr = 0;

         // This message should not be sent thru gateway
         *pSendThruGateway = FALSE;

         if(iPayloadLen == (sizeof(sMpMsgHeader_t) + 0))
         {
           // Get own addr pool info
           iFirstAvailableAddr = PopNwkGetNextAddrAvailable(); /* Get first available addr in addr pool */
           iAddrToSpare = PopNwkGetAddrCount() / PART_OF_ADDRPOOL;  /* Get 10 % of addresses in the address pool */

           // Compare with limits
           if(iAddrToSpare > ADDR_TO_SPARE_MAX)
           {
             iAddrToSpare = ADDR_TO_SPARE_MAX;
           }
           else if(iAddrToSpare < ADDR_TO_SPARE_MIN)
           {
             if(PopNwkGetAddrCount() > ADDR_TO_SPARE_MIN)
             {
               iAddrToSpare = ADDR_TO_SPARE_MIN;
             }
             else
             {
               iAddrToSpare = 0;
             }
           }

           // update own addrs pool info
           PopNwkSetNextAddrAvailable(iFirstAvailableAddr + iAddrToSpare);
           PopNwkSetAddrCount(PopNwkGetAddrCount() - iAddrToSpare);
           // Inform the NVM engine that network data has changed
           PopNvmDataMarkAsChanged(gPopNvmNwkDataChanged_c);

           // Send gateway response
           MpGwGetRadioDataRsp();

           // Send AddrPoolAddr as response
           uint16_t iDstAddr = PopNwkSrcAddr(pIndication);
           uint8_t iMpSequence = sMpMsgHeaderReq.iMpSequence;
           PopAppAddrPoolAddr(iDstAddr, iMpSequence, iAddrToSpare, iFirstAvailableAddr);
         }
       }
       break;

       case gSmGetAcknowledge_c:
       {
         uint8_t iRandom;
         uint16_t iTimeSlot;
         popTimeOut_t iAckRspDelay;
         // This message should not be sent thru gateway
         *pSendThruGateway = FALSE;

         gsPopAppMsgAckData.iDstAddr = PopNwkSrcAddr(pIndication);
         gsPopAppMsgAckData.iOptions = gPopNwkDataReqOptsUnibroadcast_c;
         gsPopAppMsgAckData.iReqCmd = sMpMsgHeaderReq.iCmd;
         gsPopAppMsgAckData.iMpSequence = sMpMsgHeaderReq.iMpSequence;
         gsPopAppMsgAckData.iStatus = 0; // Ok

         if(iPayloadLen == sizeof(sMpMsgGetAcknowledge_t)){
          // Get data from message
          pData = &pPayload[aDataOffset_c];
          iTimeSlot = (uint16_t)(pData[GET_ACK_TIMESLOT_H]);
          iTimeSlot |= (uint16_t)(pData[GET_ACK_TIMESLOT_L] << 8);
         }
         else{
           iTimeSlot = 8000;  //ms
         }
         // Get random number 0-255
         iRandom = PopRandom08();
         // Get random delay within timeslot
         iAckRspDelay = (iRandom * (iTimeSlot/255));
         // start the timer for delay before sending response
         PopStartTimerEx(cAppTaskId, gPopAppAckRspTimer_c, iAckRspDelay);

       }
       break;

       case gSmReset_c:
       {
         popNwkAddr_t iDstAddr;
         uint8_t iOptions;
         uint8_t iReqCmd;
         uint8_t iMpSequence;
         uint8_t iStatus;
         uint8_t iDeviceType;

         // This message should not be sent thru gateway
         *pSendThruGateway = FALSE;

         if(iPayloadLen == sizeof(sPopAppReset_t))
         {
           // Get data from message
           pData = &pPayload[aDataOffset_c];

           if( (pData[0] == 'O') && (pData[1] == 'K'))
           {
             // Get device type
             iDeviceType = pData[POPAPP_RESET_DEVICETYPE];

             if((iDeviceType == CM_MAIN) || (iDeviceType == CM_RADIO) || (iDeviceType == (CM_MAIN|CM_RADIO))){
              // Wait for response message to be sent before reset.
              PopStartTimerEx(cAppTaskId,gPopAppSoftwareResetTimerId_c, gPopAppSoftwareResetTime_c);
              giResetDeviceType = iDeviceType;
              iStatus = 0; // OK
             }
             else{
              iStatus = 0xFF; // NOK, incorrect device type
             }
           }
           else{
            iStatus = 0xFF; // NOK, incorrect key
           }
         }
         else{
           iStatus = 0xFF; // NOK, incorrect length
         }

         // Send response
         iDstAddr = PopNwkSrcAddr(pIndication);
         iOptions = gPopNwkDataReqOptsDefault_c;
         iReqCmd = sMpMsgHeaderReq.iCmd;
         iMpSequence = sMpMsgHeaderReq.iMpSequence;

         PopAppAcknowledge(iDstAddr, iMpSequence, iOptions, iReqCmd, iStatus);
       }
       break;

       case gSmDisableRoutingMode_c:
       {
         popNwkAddr_t iDstAddr;
         uint8_t iOptions;
         uint8_t iReqCmd;
         uint8_t iMpSequence;
         uint8_t iStatus;
         uint8_t iDisableRoutingMode;

         // This message should not be sent thru gateway
         *pSendThruGateway = FALSE;

         if(iPayloadLen == sizeof(sPopAppSetRoutingMode_t))
         {
           // Get data from message
           pData = &pPayload[aDataOffset_c];

           // Get device type
           iDisableRoutingMode = pData[POPAPP_ROUTINGMODE_ROUTINGMODE];
           // Set routing mode
           PopNwkSetRoutingSleepingMode(iDisableRoutingMode);
           // Set status
           iStatus = 0; // OK
         }
         else{
           // Set status
           iStatus = 0xFF; // NOK
         }

         // Send response
         iDstAddr = PopNwkSrcAddr(pIndication);
         iOptions = gPopNwkDataReqOptsDefault_c;
         iReqCmd = sMpMsgHeaderReq.iCmd;
         iMpSequence = sMpMsgHeaderReq.iMpSequence;

         PopAppAcknowledge(iDstAddr, iMpSequence, iOptions, iReqCmd, iStatus);

       }
       break;

       case gSmSetServiceMode_c:
       {
         popNwkAddr_t iDstAddr;
         uint8_t iOptions;
         uint8_t iReqCmd;
         uint8_t iMpSequence;
         uint8_t iStatus;

         // This message should not be sent thru gateway
         *pSendThruGateway = FALSE;

         if(iPayloadLen == sizeof(sPopAppSetServiceMode_t))
         {
           // Get data from message
           pData = &pPayload[aDataOffset_c];

           // Get service mode enabled/disabled
           giServiceMode = pData[POPAPP_SERVICEMODE_SERVICEMODE];

           if(giServiceMode == TRUE){
            // Start service mode
            giServiceChannel = pData[POPAPP_SERVICEMODE_CHANNEL];
            giServiceTimeout = pData[POPAPP_SERVICEMODE_TIMEOUT];

            // Delay switch of channel until acknowledge has been sent
            PopStartTimerEx(cAppTaskId,gPopAppServiceModeDelayTimerId_c, gPopAppServiceModeDelayTime_c);
           }
           else{
            // End service mode
            // Delay switch of channel until acknowledge has been sent
            PopStartTimerEx(cAppTaskId,gPopAppServiceModeDelayTimerId_c, gPopAppServiceModeDelayTime_c);
           }

           // Set status
           iStatus = 0; // OK
         }
         else{
           // Set status
           iStatus = 0xFF; // NOK
         }

         // Send response
         iDstAddr = PopNwkSrcAddr(pIndication);
         iOptions = gPopNwkDataReqOptsDefault_c;
         iReqCmd = sMpMsgHeaderReq.iCmd;
         iMpSequence = sMpMsgHeaderReq.iMpSequence;

         PopAppAcknowledge(iDstAddr, iMpSequence, iOptions, iReqCmd, iStatus);

       }
       break;

#if DEBUG == 1
      case gAssertDataReq_c:
      {
        if(iPayloadLen == sizeof(sPopAppAssert_t))
        {
          // Get data from message
          pData = &pPayload[aDataOffset_c];

          if(pData[POPAPP_ASSERT_CMD] == 1)
          {
            extern sPopAssertCount_t gsPopAssert;

            gsPopAssert.iAssertStackOverRun = 0;
            gsPopAssert.iAssertHeapCompromised = 0;
            gsPopAssert.iAssertOutOfEvents = 0;
            gsPopAssert.iAssertBadReceivePtr = 0;
            gsPopAssert.iAssertNwkWatchdog = 0;
            gsPopAssert.iAssertGwWatchdog = 0;
            // Store to NVM
            (void)PopNvStore(gPopNvIdAssert_c, sizeof(gsPopAssert), &gsPopAssert);
          }
        }

        uint16_t iDstAddr = PopNwkSrcAddr(pIndication);
        uint8_t iMpSequence = sMpMsgHeaderReq.iMpSequence;
        // Send data back OTA
        PopAppAssertDataReq(iDstAddr, iMpSequence);
        // This message should not be sent thru gateway
        *pSendThruGateway = FALSE;
      }
      break;

      case gDebugDataReq_c:
      {
        uint16_t iDstAddr = PopNwkSrcAddr(pIndication);
        uint8_t iMpSequence = sMpMsgHeaderReq.iMpSequence;
        if(iPayloadLen == (sizeof(sMpMsgHeader_t) + 0))
        {
          // Send data back OTA
          PopAppDebugDataReq(iDstAddr, iMpSequence);
        }
        // This message should not be sent thru gateway
        *pSendThruGateway = FALSE;
      }
      break;
#endif	// DEBUG >= 1
     }
     break;
  }
  /* Test application code*/
  MpSerialTestReceivePacket(pIndication);
}

void PopAppCrystalTuneInit(void)
{
  uint8_t iCoarseTune;
  uint8_t iFineTune;

  if(PopNvRetrieve(gPopAppNvCoarseTune_c, &iCoarseTune) != gPopErrNone_c)
    iCoarseTune = 0x08;
  if(PopNvRetrieve(gPopAppNvFineTune_c, &iFineTune) != gPopErrNone_c)
    iFineTune = 0x0f;

#if TARGET_BOARD != CC2538SF53
  MLMEPHYXtalAdjust(iCoarseTune, iFineTune);
#else
  phyPLMEXtalAdjust(iCoarseTune, iFineTune);
#endif
}

void PopAppDefaultNodeAddrInit(void)
{
  popErr_t iErr;

  // Node address
  iErr = PopNvRetrieve(gPopAppNvDefaultNodeAddr_c, &giDefaultNodeAddr);

  if(iErr != gPopErrNone_c){
    giDefaultNodeAddr = (gPopNwkReservedAddr_c - 1) & PopRandom16();
    // Store to NVM to avoid new Node address next startup from reset
    PopNvStore(gPopAppNvDefaultNodeAddr_c, 2, &giDefaultNodeAddr);
  }

  // Channel
  iErr = PopNvRetrieve(gPopAppNvDefaultChannel_c, &giDefaultChannel);

  if(iErr != gPopErrNone_c){
    giDefaultChannel = giDefaultChannel_c;
    //PopNvStore(gPopAppNvDefaultChannel_c, 1, &giDefaultChannel);
  }

  // Pan Id
  iErr = PopNvRetrieve(gPopAppNvDefaultPanId_c, &giDefaultPanId);

  if(iErr != gPopErrNone_c){
    giDefaultPanId = giDefaultPanId_c;
    //PopNvStore(gPopAppNvDefaultPanId_c, 2, &giDefaultPanId);
  }
}

void PopAppReset(void)
{
  uint8_t iDeviceType;

  iDeviceType = giResetDeviceType;

  if(iDeviceType & CM_MAIN){
    /* Reset main processor */

#if TARGET_BOARD == CC2538SF53
    // Reset LPC (Set the pin high)
    GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, GPIO_PIN_5);
    // Hardware request for ISP command handler (Driving pin low)
    GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, 0);
    // keep values for some ms
    DelayMs(5);
    // Release Reset in LPC
    GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, 0);
#else
    Gpio_SetPinData(gGpioPin20_c, gGpioPinStateHigh_c);
    DelayMs(5);
    Gpio_SetPinData(gGpioPin20_c, gGpioPinStateLow_c);
#endif
  }

  if(iDeviceType & CM_RADIO){
    /* Reset radio now */
#if TARGET_BOARD == CC2538SF53
    SysCtrlReset();
#else
    CRM_SoftReset();
#endif
  }

  giResetDeviceType = 0;
}

