/***************************************************************************
  MpCfg.h

  Revision history:
  Rev   Date      Comment   Description
  this  110912              first revision

***************************************************************************/
#ifndef _MPCFG_H_
#define _MPCFG_H_

#define STATISTICS_MODULE 1
#define CHARGER_MODULE 2
#define BATTERY_MODULE 3

// define node type to build
// Target board is set depending of node type
#define gNodeType_c    CHARGER_MODULE 

// Define any application NVM (Non-Volatile Memory) Storage IDs here...
#define gPopAppNvCoarseTune_c     (gPopNvUserId_c)      // Not used in CC2538EM
#define gPopAppNvFineTune_c       (gPopNvUserId_c + 1)  // Not used in CC2538EM
#define gPopAppNvDefaultNodeAddr_c    (gPopNvUserId_c + 2)
#define gPopAppNvTxPowerLevel_c     (gPopNvUserId_c + 3)
#define gPopAppNvDefaultChannel_c    (gPopNvUserId_c + 4)
#define gPopAppNvDefaultPanId_c     (gPopNvUserId_c + 5)


// Define default network parameters
#define giDefaultChannel_c      26
#define giDefaultPanId_c        22222
#define giDefaultNodeAddr_c     0xFFFE

// Define DEBUG mode
// 0 = No debug code
// 1 = Store and read Assert data(cmd 0xEE)
#define DEBUG   0
#define NWK_WATCHDOG  0
#endif // _MPCFG_H_


