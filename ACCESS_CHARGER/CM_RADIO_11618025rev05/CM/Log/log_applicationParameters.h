#ifndef LOG_APPLICATION_PARAMETERS
#define LOG_APPLICATION_PARAMETERS

/* It take some time to write application parameters to flash and the
 * flash have a large but limited but limited number of write cycles before it
 * break down.
 */

/* It is good if only one copy of application parameters are stored in
 * memory so that all parts of the program always use the same parameters.
 * This copy of application parameters may be used while it is written to
 * flash memory.
 *
 * This copy of application parameters should only be updated by data read from
 * flash or functions calls in the module using the application parameters.
 * Only valid data read from flash should be written to this copy of
 * application parameters.
 *
 * Update of read parameters should be an atomic operation but it
 * should not interrupt any function in meas. If update of these
 * parameters are put in an ordinary popnet task these requirements should be
 * fulfilled.
 *
 * Write and read of program parameters will be much simpler if all parameterers
 * are stored in one memory location.
 * The basic problem is howto implement this in software while avoiding global
 * variables. It is implemented by storing the parameters for each module
 * in a seperate structure together in a large structure. A local constant
 * pointer is pointing to its own parameters. This provide an abstractin there
 * each module only access it's own variables while the parameters is stored in
 * the same memory location. Ideally a function should be called for each
 * module to update the variables then new values are read from flash but for
 * now it should be a good solution since it keep the program simple.
 */

#include "Private/ApplicationParameters/log_ApplicationParametersVariables.h"
#include <stdbool.h>

void EraseApplicationParameters(void);    // Erase application parameters stored in flash and set application parameters to default value
void WriteApplicationParameters(void);    // Write application parameters to flash
void ReadApplicationParameters(void);     // Read application parameters from flash and if valid parameters can not be read set to default parameters

bool WriteApplicationParametersCompleted(void);          // Returns true if write completed
bool ReadApplicationParametersCompleted(void);          // Returns true if read completed
bool EraseApplicationParametersCompleted(void);          // Returns true if erase completed
#endif
