#include "log_ProgramStarter.h"

volatile static struct {int called; void (* programStart)(); }
       log_CallFunctionQueue[Log_enum_size]
         = {{0, log_EraseApplicationParametersProgramStart},
            {0, log_WriteApplicationParametersProgramStart},
            {0, log_ReadApplicationParametersProgramStart}
         };
volatile static int LogBusy = 0;

void log_QueueStartNextProgram(enum log_CallFunctionQueue_enum pos){
  log_CallFunctionQueue[pos].called = 1;  // mark program as ready for execution
  
  if(!LogBusy)                            // not running ?
    log_QueueStartNextProgramInterrupt(); // start program
}

volatile void log_QueueStartNextProgramInterrupt(){
  int n;
  for(n = 0; n < Log_enum_size; n++){
    if(log_CallFunctionQueue[n].called){
      log_CallFunctionQueue[n].called = 0;
      LogBusy = 1;
      log_CallFunctionQueue[n].programStart();
      return;
    }
  }
  LogBusy = 0;
}
