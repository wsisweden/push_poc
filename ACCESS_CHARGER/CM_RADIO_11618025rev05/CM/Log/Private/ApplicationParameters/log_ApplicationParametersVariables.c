#include "log_ApplicationParametersVariables.h"
#include "log_EraseApplicationParametersProgram.h"
#include "log_ReadApplicationParametersProgram.h"
#include "log_WriteApplicationParametersProgram.h"

#define APPLICATION_PARAMETERS_DEFAULT /* default parameters */ \
    {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,  \
     0x08, \
     0x0f, \
     0} 

/* Application parameters function prototypes*/
void log_EraseApplicationParametersDone();
void log_WriteApplicationParametersDone();
void log_ReadApplicationParametersDone();

static ApplicationParameters_type ApplicationParameters = APPLICATION_PARAMETERS_DEFAULT;   // used parameters
volatile static ApplicationParameters_type ReadBuffer;                                      // read parameter buffer

PopApp_t* sPopApp  = &ApplicationParameters.sPopApp;                                   // should only be used by PopApp

bool_t fWriteDone = FALSE;                                                      // Flag set to true when write done 
bool_t fReadDone = FALSE;                                                       // Flag set to true when read done
bool_t fEraseDone = FALSE;                                                      // Flag set to true when erase done 

void EraseApplicationParameters(void){
  fEraseDone = FALSE;
  log_EraseApplicationParametersProgramInit(log_EraseApplicationParametersDone);
}

void WriteApplicationParameters(void){
  fWriteDone = FALSE;
  ReadBuffer.valid = -1;
  log_WriteApplicationParametersProgramInit(ApplicationParameters, log_WriteApplicationParametersDone);
}


void ReadApplicationParameters(void){
  fReadDone = FALSE;
  log_ReadApplicationParametersProgramInit(&ReadBuffer, log_ReadApplicationParametersDone);
}

void log_EraseApplicationParametersDone(){
  ApplicationParameters_type defaultValues = APPLICATION_PARAMETERS_DEFAULT;
  ApplicationParameters = defaultValues;
  fEraseDone = TRUE;
}

void log_WriteApplicationParametersDone(){
  fWriteDone = TRUE;
}

void log_ReadApplicationParametersDone(){
  if(ReadBuffer.valid == 0){
    ApplicationParameters = ReadBuffer;
  }
  else if(ReadBuffer.valid == -1){
  }
  else{
  }
  fReadDone = TRUE;
}

bool_t WriteApplicationParametersCompleted(void)
{
  return (fWriteDone & TRUE);
}

bool_t ReadApplicationParametersCompleted(void)
{
  return (fReadDone & TRUE);
}

bool_t EraseApplicationParametersCompleted(void)
{
  return (fEraseDone & TRUE);
}