#ifndef LOG_APPLICATION_PARAMETERS_VARIABLES_H
#define LOG_APPLICATION_PARAMETERS_VARIABLES_H

#include "PopAppCm.h"

typedef struct{
  PopApp_t                sPopApp;                  // Popnet hardware dependent parameters
  int                     valid;                    // indicate if data valid, should be written last
} ApplicationParameters_type;                       // Application parameter storage


/* Application parameters */
//volatile void log_EraseApplicationParametersDone();
//volatile void log_WriteApplicationParametersDone();
//volatile void log_ReadApplicationParametersDone();

#endif
