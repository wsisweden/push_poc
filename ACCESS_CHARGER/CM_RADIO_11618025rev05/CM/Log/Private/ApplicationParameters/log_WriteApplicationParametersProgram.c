#include "log_WriteApplicationParametersProgram.h"
#include "../../Flash/flash.h"
#include "../log_ProgramStarter.h"

volatile static ApplicationParameters_type parametersLocal;   // log that should be written
static void (* volatile callback)();                 // this function is called then program done

static void program0();                              // this is program state 0 and it is set to be called from flash then flash operation done
static void program1();                              // this is program state 1 and it is set to be called from flash then flash operation done
static void program2();                              // this is program state 12and it is set to be called from flash then flash operation done

void log_WriteApplicationParametersProgramInit(ApplicationParameters_type parameters, void (* volatile program)()){
  parametersLocal = parameters;
  callback = program;

  log_QueueStartNextProgram(WriteApplicationParametersLog_enum);
}

void log_WriteApplicationParametersProgramStart(){
  program0(0);
}

static void program0(uint32_t output){
  FlashEraseBlock(FLASH_APPLICATION_PARAMETERS_ADDRESS_FIRST, program1);
}

static void program1(uint32_t output){
  FlashWrite(FLASH_APPLICATION_PARAMETERS_ADDRESS_FIRST, (void *)&parametersLocal, sizeof(parametersLocal), program2);
}

static void program2(uint32_t output){
  log_QueueStartNextProgramInterrupt();
  callback();
}
