#include "log_EraseApplicationParametersProgram.h"
#include "../../Flash/flash.h"
#include "../log_ProgramStarter.h"


static void (* volatile callback)();                 // this function is called then program done

static void program0(uint32_t output);               // this is program state 0 and it is set to be called from flash then flash operation done
static void program1(uint32_t output);               // this is program state 1 and it is set to be called from flash then flash operation done


void log_EraseApplicationParametersProgramInit(void (* volatile program)()){
  callback = program;

  log_QueueStartNextProgram(EraseApplicationParameters_enum);
}

void log_EraseApplicationParametersProgramStart(){
  program0(0);
}


static void program0(uint32_t output){
  FlashEraseBlock(FLASH_APPLICATION_PARAMETERS_ADDRESS_FIRST, program1);
}

static void program1(uint32_t output){
  log_QueueStartNextProgramInterrupt();
  callback();
}
