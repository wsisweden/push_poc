#include "log_ReadApplicationParametersProgram.h"
#include "../../Flash/flash.h"
#include "../log_ProgramStarter.h"

volatile static ApplicationParameters_type* volatile parametersLocal; // log that should be written
static void (* volatile callback)();                         // this function is called then program done

static void program0(uint32_t output);                       // this is program state 0 and it is set to be called from flash then flash operation done
static void program1(uint32_t output);                       // this is program state 1 and it is set to be called from flash then flash operation done

void log_ReadApplicationParametersProgramInit(volatile ApplicationParameters_type* parameters, void (* volatile program)()){
  parametersLocal = parameters;
  callback = program;

  log_QueueStartNextProgram(ReadApplicationParametersLog_enum);
}

void log_ReadApplicationParametersProgramStart(){
  program0(0);
}

static void program0(uint32_t output){
  FlashRead(FLASH_APPLICATION_PARAMETERS_ADDRESS_FIRST, (void *)parametersLocal, sizeof(*parametersLocal), program1);
}

static void program1(uint32_t output){
  log_QueueStartNextProgramInterrupt();
  callback();
}
