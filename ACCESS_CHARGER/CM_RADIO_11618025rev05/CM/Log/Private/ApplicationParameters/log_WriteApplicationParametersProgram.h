#ifndef LOG_WRITE_APPLICATION_PARAMETERS_PROGRAM_H
#define LOG_WRITE_APPLICATION_PARAMETERS_PROGRAM_H

#include "../../log_applicationParameters.h"
#include "log_ApplicationParametersVariables.h"

void log_WriteApplicationParametersProgramInit(ApplicationParameters_type parameters, void (* volatile program)());   // called to initialize program but do not start, this function could be called then other program execute
void log_WriteApplicationParametersProgramStart();                                                           // start program, observe that init must be called first and this function should not be called then other program execute

#endif
