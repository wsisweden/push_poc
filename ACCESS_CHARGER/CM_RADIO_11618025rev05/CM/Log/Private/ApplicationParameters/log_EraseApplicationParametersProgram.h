#ifndef LOG_ERASE_APPLICATION_PARAMETERS_PROGRAMS_H
#define LOG_ERASE_APPLICATION_PARAMETERS_PROGRAMS_H

void log_EraseApplicationParametersProgramInit(void (* volatile program)());    // called to initialize program but do not start, this function could be called then other program execute
void log_EraseApplicationParametersProgramStart();                                       // start program, observe that init must be called first and this function should not be called then other program execute

#endif
