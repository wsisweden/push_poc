#ifndef LOG_PROGRAM_STARTER
#define LOG_PROGRAM_STARTER

/* It is possible to start several programs at once but each program will only
 * run once. The program is placed in a queue by calling the program init
 * function. 
 */

/* The circular buffers have four different functions: erase, write, find with
 * index and find next.
 *
 * Write must keep track of at which address next log should be written. This
 * address i set to start of buffer by erase and must be initialized then the
 * device is started.
 *
 * The find next function must know the address of the next log to read. This
 * address is updated at start of unit and by the find with index function. The
 * find function use the last written log to calculate the flash address of the
 * log to find
 *
 * Erase log use the update the address of next log to write.
 * Write log update the address of next log to find and next log to write
 * Find log use address of next log to write and update address of last find log
 * Find next use address of last find log and update it.
 *
 * There is two addresses write and find.
 */

#include "ApplicationParameters/log_EraseApplicationParametersProgram.h"
#include "ApplicationParameters/log_WriteApplicationParametersProgram.h"
#include "ApplicationParameters/log_ReadApplicationParametersProgram.h"

enum log_CallFunctionQueue_enum{EraseApplicationParameters_enum,
                                WriteApplicationParametersLog_enum,
                                ReadApplicationParametersLog_enum,
                                Log_enum_size};

void log_QueueStartNextProgram(enum log_CallFunctionQueue_enum);  // called after program initialized from a non interruptable process
volatile void log_QueueStartNextProgramInterrupt();               // called from interrupt then program done

#endif
