#ifndef FLASH_H
#define FLASH_H

#include <stdint.h>

/* Flash memory allocation
 *  0x0000 0000 - 0x0001 7FFF // MC1322x OTA binary image
 *  0x0001 8000 - 0x0001 FFFF // Application parameters
 *  0x0002 0000 - 0x0007 FFFF // LPC1765 OTA binary image
 */

#define FLASH_OTA_BINARY_ADDRESS_FIRST  0x00000000
#define FLASH_OTA_BINARY_ADDRESS_LAST   0x0007FFFF

#define FLASH_APPLICATION_PARAMETERS_ADDRESS_FIRST  0x00000000
#define FLASH_APPLICATION_PARAMETERS_ADDRESS_LAST   0x00000000

#define FLASH_LPC1765_OTA_BINARY_ADDRESS_FIRST  0x00000000
#define FLASH_LPC1765_OTA_BINARY_ADDRESS_LAST   0x0007FFFF

//#define FLASH_HISTORY_ADDRESS_FIRST                 0x00004000
//#define FLASH_HISTORY_ADDRESS_LAST                  0x0004FFFF

//#define FLASH_INSTANT_ADDRESS_FIRST                 0x00050000
//#define FLASH_INSTANT_ADDRESS_LAST                  0x0006FFFF

//#define FLASH_EVENT_ADDRESS_FIRST                   0x00070000
//#define FLASH_EVENT_ADDRESS_LAST                    0x0007FFFF

#define FLASH_ADDRESS_FIRST 0x00000000
#define FLASH_ADDRESS_LAST 0x0007FFFF

#define FLASH_ERASE_BLOCK_SIZE 4096
#define FLASH_PAGE_BLOCK_SIZE 256

/* Functions calls */
void FlashInit(void);

void FlashEraseSector(uint32_t address);                    // Erase the sector at address
void FlashEraseAll(volatile void (* volatile program)());   // Erase all flash

void FlashEraseBlock(uint32_t address, void (* volatile program)(uint32_t));                 // Erase block at address and return next uncleared address, this function is indenpendent of block size

void FlashWrite(uint32_t address, void* data, int len, void (* volatile program)(uint32_t)); // Write data at address, block must have been cleared beforehand

void FlashRead(uint32_t address, void* data, int len, void (* volatile program)(uint32_t));

#endif
