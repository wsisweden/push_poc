#include "flash.h"
#include "private/flash_memory_range_check.h"
#include "private/flash_erase_block_program.h"
#include "private/flash_write_program.h"
#include "private/flash_read_program.h"
#include "private/flash_init_program.h"
#include "Private/flash_erase_all_program.h"

#include <PortConfig.h>
#include <Hw/TIDriverLib/source/spi.h>
#include <stdbool.h>

volatile static int flash_InitProgramDone = 0;                           // Set then program start and reset then program reach end of execution
uint32_t gManufacturerAndDeviceId = 0xFFFFFFFF;

static void flash_InitDone(uint32_t ManufacturerAndDeviceId);

static void flash_InitDone(uint32_t ManufacturerAndDeviceId){
  gManufacturerAndDeviceId = ManufacturerAndDeviceId;
  flash_InitProgramDone = 1;
}

/*
  Returns TRUE when flash initialization is done
*/
bool flash_InitCompleted(void){
  return (flash_InitProgramDone & true);
}

void FlashInit(void){
  /* Pin configuration in PortConfig.h */
  FLASH_WRITE_PROTECT_LOW;    // Write protect chip

  spi_init();

  //flash_InitProgramDone = 1;
  flash_InitProgramDone = 0;
  flash_InitProgramStart(flash_InitDone);
  //while(!flash_InitProgramDone);
  while(!flash_InitCompleted());
}

void FlashEraseAll(volatile void (* volatile program)()){
  flash_EraseAllProgramStart(program);
}

void FlashEraseBlock(uint32_t address, void (* volatile program)(uint32_t)){
  flash_EraseBlockProgramStart(address, program);
}

void FlashWrite(uint32_t address, void* data, int len, void (* volatile program)(uint32_t)){
  flash_WriteDataProgramStart(data, len, address, program);
}

void FlashRead(uint32_t address, void* data, int len, void (* volatile program)(uint32_t)){
  flash_ReadDataProgramStart(data, len, address, program);
}
