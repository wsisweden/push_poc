#include "flash_init_program.h"
#include "flash_opcodes.h"

#include <Hw/TIDriverLib/source/spi.h>

static void (* volatile callback)(uint32_t);                          // callled with manufacturer and device id then program done

static void program0(uint32_t output);                                // this is program state 0 and it is set to be called from SPI then SPI operation done
static void program1(uint32_t output);                                // this is program state 1 and it is set to be called from SPI then SPI operation done
static void program2(uint32_t output);                                // this is program state 2 and it is set to be called from SPI then SPI operation done

void flash_InitProgramStart(void (* volatile program)(uint32_t)){
  callback = program;

  program0(0);
}

void program0(uint32_t output){
  spi_Callback = program1;
  SPI_COMMAND(FLASH_OPCODE_WRITE_DISABLE << 24, 16, 15);              // write disable, write 16 bits + read 0 bits = 16 bits
}

void program1(uint32_t output){
  spi_Callback = program2;
  SPI_COMMAND(FLASH_OPCODE_READ_MANUFACTURER_DEVICE_ID << 24, 8, 39); // Read manufacturer and device id, write 8 bits + read 32 bits = 40 bits
}

void program2(uint32_t output){
  callback(output);
}
