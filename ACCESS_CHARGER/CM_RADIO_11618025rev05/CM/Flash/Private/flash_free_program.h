#ifndef FLASH_FREE_PROGRAM_H
#define FLASH_FREE_PROGRAM_H

#include <EmbeddedTypes.h>

volatile void flash_FreeProgram(uint32_t output);   // empty function

#endif
