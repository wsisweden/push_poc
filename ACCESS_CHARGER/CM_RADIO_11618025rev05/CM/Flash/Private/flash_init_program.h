#ifndef FLASH_INIT_PROGRAM_H
#define FLASH_INIT_PROGRAM_H

#include <stdint.h>

void flash_InitProgramStart(void (* volatile program)(uint32_t));                  // should be called to reset init program

#endif
