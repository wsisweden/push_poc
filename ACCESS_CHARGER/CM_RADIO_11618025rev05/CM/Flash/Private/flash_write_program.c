#include "flash_write_program.h"
#include "flash_opcodes.h"
#include "../flash.h"

#include <PortConfig.h>
#include <Hw/TIDriverLib/source/spi.h>

volatile static uint8_t* bufferAddress;                   // address to start of buffer
volatile static unsigned int bufferSize;                  // size of buffer in bytes
volatile static uint32_t WriteFlashAddress;               // address in flash
static void (* volatile callback)(uint32_t);              // this function is called then program done

static void program0(uint32_t output);                    // this is program state 0 and it is set to be called from SPI then SPI operation done
static void program1(uint32_t output);                    // this is program state 1 and it is set to be called from SPI then SPI operation done
static void program2(uint32_t output);                    // this is program state 2 and it is set to be called from SPI then SPI operation done
static void program3(uint32_t output);                    // this is program state 3 and it is set to be called from SPI then SPI operation done
static void program4(uint32_t output);                    // this is program state 4 and it is set to be called from SPI then SPI operation done
static void program5(uint32_t output);                    // this is program state 5 and it is set to be called from SPI then SPI operation done
static void program6(uint32_t output);                    // this is program state 6 and it is set to be called from SPI then SPI operation done
static void program7(uint32_t output);                    // this is program state 7 and it is set to be called from SPI then SPI operation done
static void program8(uint32_t output);                    // this is program state 8 and it is set to be called from SPI then SPI operation done
static void program9(uint32_t output);                    // this is program state 9 and it is set to be called from SPI then SPI operation done
static void program10(uint32_t output);                   // this is program state 10 and it is set to be called from SPI then SPI operation done
static void program11(uint32_t output);                   // this is program state 11 and it is set to be called from SPI then SPI operation done
static void program12(uint32_t output);                   // this is program state 12 and it is set to be called from SPI then SPI operation done
static void program13(uint32_t output);                   // this is program state 13 and it is set to be called from SPI then SPI operation done


static int bufferPos = 0;

void flash_WriteDataProgramStart(uint8_t* buffer, int size, uint32_t flashAddress, void (* volatile program)(uint32_t)){
  bufferAddress = buffer;
  bufferSize = size;
  WriteFlashAddress = flashAddress;
  callback = program;

  program0(0);
}

static void program0(uint32_t output){
  if(bufferSize){
    spi_Callback = program1;
    FLASH_WRITE_PROTECT_HIGH;                             // allow write to chip
    SPI_COMMAND(FLASH_OPCODE_WRITE_STATUS << 24, 16, 15); // turn off sector protection lock, write 16 bits + read 0 bits = 16 bits
  }
  else{
    //spi_Callback = program13;
    uint32_t address = WriteFlashAddress;
    callback(address);                                    // No data to send, call callback function directly
  }
}

static void program1(uint32_t output){
  spi_setup_SS_SETUP(1);
  spi_Callback = program2;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program2(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program3;
    SPI_COMMAND(FLASH_OPCODE_WRITE_ENABLE << 24, 8, 7);   // set write enable, write 8 bits + read 0 bits = 8 bits
  }
}

static void program3(uint32_t output){
  spi_Callback = program4;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program4(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program5;
    SPI_COMMAND((FLASH_OPCODE_UNPROTECT_SECTOR << 24) | WriteFlashAddress, 32, 31); // unprotect sector, write 32 bits + read 0 bits = 32 bits
  }
}

static void program5(uint32_t output){
  spi_Callback = program6;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program6(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program7;
    SPI_COMMAND(FLASH_OPCODE_WRITE_ENABLE << 24, 8, 7);   // set write enable, write 8 bits + read 0 bits = 8 bits
  }
}

static void program7(uint32_t output){
  spi_Callback = program8;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program8(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_setup_SS_SETUP(2);
    spi_Callback = program9;
    SPI_COMMAND((FLASH_OPCODE_PROGRAM_1_TO_256_BYTES << 24) | WriteFlashAddress, 32, 31); // write 32 bits + read 0 bits = 32 bits
  }
}

static void program9(uint32_t output){
  SPI_COMMAND(bufferAddress[bufferPos] << 24, 8, 7);      // set write enable, write 8 bits + read 0 bits = 8 bits
  if(bufferPos == bufferSize - 1){
    bufferPos = 0;
    spi_Callback = program10;
  }
  else{
    bufferPos++;
    WriteFlashAddress++;
    if(WriteFlashAddress & 0xFF)
      spi_Callback = program9;
    else{
      spi_Callback = program1;                            // New page, need to write address to this page
    }
  }
}

static void program10(uint32_t output){
  spi_setup_SS_SETUP(1);
  spi_Callback = program11;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program11(uint32_t output){

  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program12;
    SPI_COMMAND(FLASH_OPCODE_WRITE_DISABLE << 24, 8, 7);  // write 16 bits + read 0 bits = 16 bits
  }
}

static void program12(uint32_t output){
  spi_Callback = program13;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program13(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    FLASH_WRITE_PROTECT_LOW;                              // Write protect chip
    {
      uint32_t address = WriteFlashAddress + 1;
      callback(address);
    }
  }
}

