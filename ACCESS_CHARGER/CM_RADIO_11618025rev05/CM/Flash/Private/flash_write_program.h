#ifndef FLASH_WRITE_PROGRAM_H
#define FLASH_WRITE_PROGRAM_H

#include <stdint.h>

void flash_WriteDataProgramStart(uint8_t* buffer, int size, uint32_t flashAddress, void (* volatile program)(uint32_t)); // should be called to reset flash_WriteDataProgram(...) before program is started

#endif
