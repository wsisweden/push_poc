#pragma once

/***************************************************************************
  MpSerial.h

  Revision history:
  Rev   Date      Comment   Description
  this  120503              first revision

***************************************************************************/
#if gUseMpSerial_d

#include <Hw/TIDriverLib/source/TiUart.h>
#include <board_config.h>
#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

// Timers
#define gMpSerMinGapTimer_c         0x01     /* minimum inter-byte gap event */
#define gMpSerialTimerToggleReply_c 0x02     /* Test application timer */
#define gMpSerialTimerTestTimeout_c 0x03     /* Test application timer */


// internal events for serial module
#define gMpSerByteReceivedEvt_c     0x91      /* ISR received a byte */
#define gMpSerialDataReadyEvt_c     0x94      // bsp: serial data coming in on port (no data)
#define gMpSerialTxDoneEvt_c        0x95      // bsp: transmit of last packet complete (no data)

// status flags
#define gMpSerRxData_c              0x01
#define gMpSerRxOverflow_c          0x02
#define gMpSerTxDone_c              0x04
#define gMpSerTxOverflow_c          0x08
#define gMpSerTooBig_c              0x10  //application buffer size exceeds maximum tx buffer size.
#define gMpSerBusy_c                0x12  //no room in xmit buffer for data.
#define gMpSerInvalidArg_c          0x14  //check parameters


#define gMpSerialPort_c             UART_1
#define gMpSerialBaudrate_c         38400
#define gMpUart_EnableHWFlowControl_d FALSE
#define gMpSerialMinGapMs_c         10

#define gMpSerialTxBufferSize_c     50
#define gMpSerialRxBufferSize_c     50

// Test application commands
#define gTestStart                   0x01     // Start test
#define gTestDone                    0x02     // End of test test
// Test application radio commands
#define gMpSerialCmdToggleLight_c    0x01     // on/off/toggle
#define gMpSerialCmdToggleAck_c      0x03     // end-to-end ACK
#define gMpSerialCmdResponse_c       0x04     // Delayed response

#if gPopSerial_d == gDisabled_c
#define PopSerialDisableRxInts()
#define PopSerialEnableRxInts()
#define PopSerialSaveAndDisableTxRxInts()
#endif

void MpSerialTaskInit(void);
void MpSerialTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);

void MpSerialTxIsr(UartWriteCallbackArgs_t* args);
void MpSerialRxIsr(UartReadCallbackArgs_t* args);

void MpUart_Init(uint8_t *mUARTRxBuffer);
void MpSerialHandler(void);
popErr_t MpSerialReceive(uint8_t *piLen, void *pBuffer);
popErr_t MpSerialSend( uint8_t iLen, void *pBuffer);

/* Test application function */
extern void MpSerialTestReceivePacket(sPopNwkDataIndication_t *pIndication);

extern uint8_t  gaMpSerialTxBuf[];        /* ISR tx buf */
extern uint8_t  *gpMpSerialTxBufHead;     /* buffer head */
extern uint8_t  *gpMpSerialTxBufTail;     /* buffer tail */
extern uint8_t   gaMpSerialRxBuf[];       /* ISR rx buf */
extern uint8_t  *gpMpSerialRxBufHead;     /* buffer head */
extern uint8_t  *gpMpSerialRxBufTail;     /* buffer tail */
extern uint16_t  giMpSerialMinGapMs;      /* mingap timeout, in ms. can be set by app */
extern volatile uint8_t gfMpSerialStatus; /* current status of xmit and recv */
extern uint8_t gfMpSerialActive;          /* Serial active flag */
extern volatile uint8_t gu8SCIDataFlag;
#endif //gUseMpSerial_d