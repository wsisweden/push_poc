/***************************************************************************
  MpGw.c

  This file includes functions for handling  resending and rerouting.
  of application messages

  Revision history:
  Rev   Date      Comment   Description
  this  100707              first revision
***************************************************************************/
#include "MpGw.h"

#include <Charger/charger.h>
#include <stddef.h>

// Local timers
#define gMpGwNodePlacementTimerId_c                gPopFirstTimerId_c
#define gMpGwBeaconIndicationTimerId_c            (gPopFirstTimerId_c + 1)
#define gMpGwResetCPUTimerId_c                    (gPopFirstTimerId_c + 2)
#define gMpGwTimerIdNodePlaceBeaconReq_c          (gPopFirstTimerId_c + 3)
#define gMpGwScanNetworkTimerId_c                 (gPopFirstTimerId_c + 4)
#define gMpGwScanForNodesTimerId_c                (gPopFirstTimerId_c + 5)
#define gMpGwWatchdogTimerId_c                    (gPopFirstTimerId_c + 6)

// Local definitions
#define gMpGwNodePlacementTimeoutTime_c  15000
#define gMpGwNodePlacementBeaconReqTime_c  10000
#define gMpGwBeaconIndicationTime_c  1000
#define gMpGwScanNetworkTime_c  5000
#define gMpGwScanForNodesTime_c  10000
#define gMpGwWatchdogTime_c  60000

/*Function prototypes */
void MpGwHandleTimer(popTimerId_t iTimerId);
bool MpGwValidNwkData(popChannel_t iChannel, popPanId_t iPanId, popNwkAddr_t iNwkAddr);
// node placement
void MpGwStartNodePlacement(void);
void MpGwEndNodePlacement(void);
void MpGwNodePlaceProcessBeacon(sPopNwkBeaconIndication_t *pNwkBeaconIndication);

// Gateway functions
//void MpGwStartNetworkReq(uint8_t *pPayload);
void MpGwStartNetworkReq(sPopEvtData_t *pData);
void MpGwStartNetworkRsp(popStatus_t iNwkStartConfirm);
void MpGwJoinEnableReq(uint16_t iWord);
void MpGwJoinEnableRsp();
void MpGwJoinNetworkReq(uint8_t *pPayload);
void MpGwJoinNetworkRsp(popStatus_t iNwkStartConfirm);
void MpGwLeaveNetworkReq(void);
void MpGwLeaveNetworkRsp(void);
void MpGwScanNetworkHandling(sPopNwkBeaconIndication_t *pNwkBeaconIndication);
void MpGwScanNetworkReq(uint8_t *pPayload);
void MpGwScanNetworkRsp(sPopNwkScanForNetworks_t *pNwkScanConfirm);
//void MpGwSetRadioParamReq(uint8_t *pPayload);
void MpGwSetRadioParamReq(sPopEvtData_t *pData);
void MpGwSetRadioParamRsp(uint8_t iStatus);
void MpGwGetRadioDataReq(uint8_t *pPayload);
//void MpGwNwkDataReq(sPopNwkDataRequest_t *pDataReq);
//void MpGwNwkDataReq(sMpNwkDataReq_t *pDataReq);  //100809
void MpGwNwkDataReq(sPopEvtData_t *pData);  //100810
void MpGwNwkDataRsp(uint8_t iStatus);
void MpGwDataIndication(sPopNwkDataIndication_t *pIndication);
//void MpGwSendPacket(sPopEvtData_t *pData);  //100819
void MpGwSendPacket(uint8_t *pData);  //120529
void MpGwNodePlacementReq();
void MpGwNodePlacementRsp(sPopNodePlacement_t *pPopNodePlacementResult);
uint8_t MpGwSetUserLevelReq(uint8_t *pByte);
void MpGwSetUserLevelRsp(uint8_t iStatus);
void MpGwGetUserLevelReq();
void MpGwGetUserLevelRsp(uint8_t iUserLevel);
void MpGwScanForNodesHandling(sPopNwkDataIndication_t *pDataIndication);
bool MpGwScanForNodesReq(uint8_t *pPayload);
void MpGwScanForNodesRsp(bool fValidNwkData);

/*Extern variables*/
extern const popTaskId_t cGwTaskId;

extern uint8_t giServiceMode;
extern uint8_t giServiceChannel;

/*Global variables */
sMpScanNetworkReq_t gsMpScanNetworkReq;
uint8_t pMpBeaconList[BEACONLIST_SIZE]; // global pointer (Array) to where beacon indications are stored
uint8_t giNodesInBeaconList = 0; // number of nodes in beacon list
bool gfMpGwIsProcessingBeacon_c = FALSE;
bool gfHeartbeat = FALSE; // Set heartbeat flag to zero as init value

// ScanForNodes
sMpScanForNodesReq_t gsMpScanForNodesReq;
sMpNodeList_t gsMpNodeList[NODELIST_SIZE]; // global structure to where node addresses from ScanForNodes are stored
uint8_t giNodesInList = 0; // number of nodes in NodeList

// Nodeplacement stuff
const popLqi_t gaMpNodePlacementStrongSignal[] = {0x40,0x80};
uint8_t giMpNodePlaceBars;
sPopNodePlacement_t gsMpNodePlacementResult;
popChannel_t giMpNodePlaceChannel;
bool gfMpPlacementStart = FALSE;

/*
  MpGwTaskInit
*/
void MpGwTaskInit(void) {
}

/*
  MpGwTaskEventLoop

  All events from PopNet to the application occur here. The iEvtId is a number
  from 0x00-0xff (not a bit mask). The contents of sEvtData depends on the iEvtId.
  See type sPopEvtData_t in PopNet.h.
*/
void MpGwTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
  switch(iEvtId)
  {
    case gPopEvtTimer_c:
      MpGwHandleTimer(sEvtData.iTimerId);
      break;
  }
}

void MpGwHandleTimer(popTimerId_t iTimerId)
{
  switch(iTimerId)
  {
    case gMpGwNodePlacementTimerId_c:
      MpGwEndNodePlacement();
      break;

    case gMpGwBeaconIndicationTimerId_c:
      PopStopTimerEx(cGwTaskId, gMpGwScanNetworkTimerId_c);
      MpGwScanNetworkRsp(NULL);
      break;

    case gMpGwScanForNodesTimerId_c:
      MpGwScanForNodesRsp(TRUE);
      break;

    case gMpGwResetCPUTimerId_c:
      // reset CPU
      PopReset();
      break;

    case  gMpGwTimerIdNodePlaceBeaconReq_c:
      {
        // Node placement timer has gone off
        if(gfMpPlacementStart){
          // Send response to gateway
          MpGwNodePlacementRsp(&gsMpNodePlacementResult);
          // use the selected channel
          if(giServiceMode){
            giMpNodePlaceChannel = giServiceChannel;
          }
          else{
            giMpNodePlaceChannel = PopNwkGetChannel();
          }
          // Clean Variables before next beacon request
          gsMpNodePlacementResult.iNodeCount=0;
          gsMpNodePlacementResult.iLqi=0;
          gsMpNodePlacementResult.iBars=0;
          // send out a beacon request on this particular channel
          PopNwkBeaconRequest(giMpNodePlaceChannel);
        }
      }
      break;

    case gMpGwScanNetworkTimerId_c:
        PopStopTimerEx(cGwTaskId, gMpGwBeaconIndicationTimerId_c);
        MpGwScanNetworkRsp(NULL);
      break;

    case  gMpGwWatchdogTimerId_c:
    {
      static uint8_t iMinuteCntr = 0;
      extern uint8_t ispCommanderModeFlag;
      extern uint8_t otaFwdlModeFlag;

      if((gfHeartbeat == TRUE) || (ispCommanderModeFlag == TRUE) || (otaFwdlModeFlag == TRUE))
      {
        // reset minute counter
        iMinuteCntr = 0;
        // reset heartbeat flag
        gfHeartbeat = FALSE;
      }
      else
      {
        iMinuteCntr ++;
      }

      if(iMinuteCntr == 7)
      {
        // reset CPU
        PopAssert(gPopAssertGwWatchdog_c);
      }
      else
      {
        // feed watchdog timer
        if(PopStartTimerEx(cGwTaskId,gMpGwWatchdogTimerId_c,gMpGwWatchdogTime_c) != gPopErrNone_c){
          PopAssert(gPopAssertGwWatchdog_c);
        }
      }
      break;
    }
  }
}
/*******************************************************************************
---------------- MpGwValidNwkData ----------------------------------------------
 Checks if Network data is within limits and returns TRUE if so.
--------------------------------------------------------------------------------
*******************************************************************************/
bool MpGwValidNwkData(popChannel_t iChannel, popPanId_t iPanId , popNwkAddr_t iNwkAddr)
{

  // compare input data with valid values
#if gNodeType_c == CHARGER_MODULE
  if((gPopNwkFirstChannel_c <= iChannel && iChannel <= (gPopNwkFirstChannel_c + gPopNwkNumOfChannels_c)) &&
     (iPanId < gPopNwkReservedPan_c) &&
     (iNwkAddr < gMpSmNwkReservedAddr_c)){
    return TRUE;
  }
#elif gNodeType_c == STATISTICS_MODULE
  if((gPopNwkFirstChannel_c <= iChannel && iChannel <= (gPopNwkFirstChannel_c + gPopNwkNumOfChannels_c)) &&
     (iPanId < gPopNwkReservedPan_c) &&
     (gMpSmNwkReservedAddr_c <= iNwkAddr && iNwkAddr < gPopNwkReservedAddr_c)){
    return TRUE;
  }
#endif
  return FALSE;
}

/*******************************************************************************
---------------- MpGwStartNodePlacement ---------------------------------------
  Start node placement on the given channel. Note: this function works whether on a network on not.
  PanId() should be set either to the desired pan, or to 0xffff. See PopNwkSetPanId().
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwStartNodePlacement()
{
  //Start Node Placement
  gfMpPlacementStart = TRUE;

  //if node placement on then permit joining should be off;
  PopNwkJoinEnable(!gfMpPlacementStart, FALSE);

  // use the selected channel
  if(giServiceMode){
    giMpNodePlaceChannel = giServiceChannel;
  }
  else{
    giMpNodePlaceChannel = PopNwkGetChannel();
  }

  // Clean Variables before next beacon request
  gsMpNodePlacementResult.iNodeCount=0;
  gsMpNodePlacementResult.iLqi=0;
  gsMpNodePlacementResult.iBars=0;

  // now we're paying attention to beacons we hear
  gfPopNwkIsProcessingBeaconEnabled = TRUE;

  // send out a beacon request on this particular channel
  PopNwkBeaconRequest(giMpNodePlaceChannel);

  // start timer for next beacon
  (void)PopStartTimerEx(cGwTaskId,gMpGwTimerIdNodePlaceBeaconReq_c,500);
}

/*******************************************************************************
---------------- MpGwEndNodePlacement ------------------------------------------
End node placement (restores LEDs to former state)
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwEndNodePlacement(void)
{
  //STOP Node Placement
  gfMpPlacementStart = FALSE;

  // Stop paying attention to beacons
  gfPopNwkIsProcessingBeaconEnabled = FALSE;

  // Clean Variables
  gsMpNodePlacementResult.iNodeCount=0;
  gsMpNodePlacementResult.iLqi=0;
  gsMpNodePlacementResult.iBars=0;
}

/*******************************************************************************
---------------- MpGwNodePlaceProcessBeacon ------------------------------------
Process beacon indications for node placment. In "node placement" mode.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNodePlaceProcessBeacon(sPopNwkBeaconIndication_t *pNwkBeaconIndication)
{
  // Verify ProtocolID and PanID before setting status
  if(pNwkBeaconIndication->iProtocol == gPopNetId_c &&
     (pNwkBeaconIndication->iPanId == PopNwkGetPanId()))
  {
    // Update BeaconCount
      gsMpNodePlacementResult.iNodeCount++;

    // Update the Lqi
    if(pNwkBeaconIndication->iLqi > gsMpNodePlacementResult.iLqi)
      gsMpNodePlacementResult.iLqi=pNwkBeaconIndication->iLqi;

    // If LQI under min value put a 1 in iBar
    if(gsMpNodePlacementResult.iLqi < gaMpNodePlacementStrongSignal[0])
      gsMpNodePlacementResult.iBars=0x01;

    // If LQI between min and max values iBar = 2
    if(gsMpNodePlacementResult.iLqi > gaMpNodePlacementStrongSignal[0] && gsMpNodePlacementResult.iLqi <= gaMpNodePlacementStrongSignal[1])
      gsMpNodePlacementResult.iBars=0x02;

    // If LQI above max value iBar = 3
    if( gsMpNodePlacementResult.iLqi > gaMpNodePlacementStrongSignal[1] )
    {
      gsMpNodePlacementResult.iBars=0x03;

        // If node count >1 then iBar=4
         if(gsMpNodePlacementResult.iNodeCount>1)
          gsMpNodePlacementResult.iBars=0x04;
    }

  }
  // Remember the memory will be freed after leaving this function.
}

/*******************************************************************************

---------------- MpGwStartNetworkReq -------------------------------------------
  MpGwStartNetwork gatewy request handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwStartNetworkReq(sPopEvtData_t *pData)
{
  sMpStartNetworkReq_t sMpStartNetworkReq;
  uint8_t *pPayload = (uint8_t*)pData;

  // place data into struct and transform PanId to native
  sMpStartNetworkReq.iChannel = (popChannel_t)pPayload[START_NWK_REQ_CHANNEL];
  sMpStartNetworkReq.iPanId = (popPanId_t)(pPayload[START_NWK_REQ_PANID_H] << 8);
  sMpStartNetworkReq.iPanId |= (popPanId_t)(pPayload[START_NWK_REQ_PANID_L]);

  // Check if Auto, default or user defined start of network
  if(sMpStartNetworkReq.iChannel == gPopNwkUseChannelList_c){
    // Auto. Form network on any Channel and PAN Id with energy scan
    popChannelList_t iChannelList = 0xFFFF; // all channels
    PopNwkSetChannelList(iChannelList);

    PopNwkFormNetwork(gPopNwkJoinOptsEnergyScan_c,
                      gPopNwkUseChannelList_c,
                      gPopNwkAnyPanId_c);
  }
  else if(sMpStartNetworkReq.iChannel == gPopNwkInvalidChannel_c){
    // Default. Form a network with default parameters
    extern popChannel_t giDefaultChannel;                                        // Default Channel (Const declared in PopAppCm)
    extern popPanId_t giDefaultPanId;                                            // Default PanId (Const declared in PopAppCm)
    extern popNwkAddr_t giDefaultNodeAddr;                                       // Default NodeAddr (from PopAppCm)

    // Perform silent start with default nwk data parameters
    PopNwkSetChannel(giDefaultChannel);
    PopNwkSetPanId(giDefaultPanId);
    PopNwkSetNodeAddr(giDefaultNodeAddr);
    PopNwkStartNetwork();
  }
  else{
    // User defined. Form network using JoinOptions normal
      PopNwkFormNetwork(gPopNwkJoinOptsNormal_c,
                        sMpStartNetworkReq.iChannel,
                        sMpStartNetworkReq.iPanId);
  }

}

/*******************************************************************************
---------------- MpGwStartNetworkRsp -------------------------------------------
  MpGwStartNetwork event response handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwStartNetworkRsp(popStatus_t iNwkStartConfirm)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpStartNetworkRsp_t sMpStartNetworkRsp;
  uint8_t pPayload[START_NWK_RSP_SIZE];

  gfPopNwkIsProcessingBeaconEnabled = FALSE;


  if(pPayload){
    // Get data and put to message
    // Event id
    iEvtId = gMpStartNetworkRsp_c;
    pPayload[START_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
    // Length
    iLen = START_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[START_NWK_RSP_LEN] = (uint8_t)(iLen);
    // Status
    sMpStartNetworkRsp.iStatus = iNwkStartConfirm;
    pPayload[START_NWK_RSP_STATUS] = (uint8_t)(sMpStartNetworkRsp.iStatus);
    // Channel
    sMpStartNetworkRsp.iChannel = PopNwkGetChannel();
    pPayload[START_NWK_RSP_CHANNEL] = (uint8_t)(sMpStartNetworkRsp.iChannel);
    // Pan id
    sMpStartNetworkRsp.iPanId = PopNwkGetPanId();
    pPayload[START_NWK_RSP_PANID_H] = (uint8_t)(sMpStartNetworkRsp.iPanId >> 8);
    pPayload[START_NWK_RSP_PANID_L] = (uint8_t)(sMpStartNetworkRsp.iPanId);
    // Node id
    sMpStartNetworkRsp.iNwkAddr = PopNwkGetNodeAddr();
    pPayload[START_NWK_RSP_NODEID_H] = (uint8_t)(sMpStartNetworkRsp.iNwkAddr >> 8);
    pPayload[START_NWK_RSP_NODEID_L] = (uint8_t)(sMpStartNetworkRsp.iNwkAddr);
    // transform from native to gateway

    // send response to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwJoinEnableReq --------------------------------------------
  MpGwJoinEnableReq event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinEnableReq(uint16_t iWord)
{
  sMpJoinEnableReq_t sMpJoinEnableReq;
  uint8_t *pPayload;
  GatewayToNative16(&iWord);                                                    // transform back to native, bytes was swaped in PopSerial because word assumption
  pPayload = (uint8_t *)&iWord;

  sMpJoinEnableReq.fEnable = (bool)(pPayload[JOIN_ENABLE_REQ_ENABLE]);
  sMpJoinEnableReq.fNetworkWide = (bool)(pPayload[JOIN_ENABLE_REQ_NETWORKWIDE]);
  PopNwkJoinEnable(sMpJoinEnableReq.fEnable, sMpJoinEnableReq.fNetworkWide);
  // call response function
  MpGwJoinEnableRsp();
}

/*******************************************************************************
---------------- MpGwJoinEnableRsp --------------------------------------------
  MpGwJoinEnableRsp event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinEnableRsp()
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpJoinEnableRsp_t sMpJoinEnableRsp;
  uint8_t pPayload[JOIN_ENABLE_RSP_SIZE];


  if(pPayload){
    //Get data & fill message with data
    // Event id
    iEvtId = gMpJoinEnableRsp_c;
    pPayload[JOIN_ENABLE_RSP_EVTID] = (uint8_t)(iEvtId);
    // Length
    iLen = JOIN_ENABLE_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[JOIN_ENABLE_RSP_LEN] = (uint8_t)(iLen);
    // Status
    sMpJoinEnableRsp.fStatus =  PopNwkGetJoinStatus();
    pPayload[JOIN_ENABLE_RSP_ENABLE] = (uint8_t)(sMpJoinEnableRsp.fStatus);
    // Send event to PC
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwJoinNetworkReq-----------------------------------------
  MpGwJoinNetwork gateway request handling. Any node on the specified Channel
  and Pan Id will do as long as it has Join enable set. Hopefully only one node
  will have join enable set.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinNetworkReq(uint8_t *pPayload)
{
  sMpJoinNetworkReq_t sMpJoinNetworkReq;
  popChannelList_t iChannelList = 0;

  // place data into struct and transform PanId to native
  sMpJoinNetworkReq.iChannel = (popChannel_t)(pPayload[JOIN_NWK_REQ_CHANNEL]);
  sMpJoinNetworkReq.iPanId = (popPanId_t)(pPayload[JOIN_NWK_REQ_PANID_H] << 8);
  sMpJoinNetworkReq.iPanId |= (popPanId_t)(pPayload[JOIN_NWK_REQ_PANID_L]);

    // Store join request info
  gsPopAppJoinReqNwkInfo.iChannel = sMpJoinNetworkReq.iChannel;
  gsPopAppJoinReqNwkInfo.iPanId = sMpJoinNetworkReq.iPanId;

    // Check if Auto or user defined start of network
  if(sMpJoinNetworkReq.iChannel == 0){
    // Set channel list to scan all channels
    iChannelList = 0xFFFF;
    PopNwkSetChannelList(iChannelList);
  }
  else{
    // set channel list to match requested channel
    iChannelList = 1 << (sMpJoinNetworkReq.iChannel - 11); // one channel
    PopNwkSetChannelList(iChannelList);
  }

  gfPopNwkIsProcessingBeaconEnabled = TRUE;
  gfMpIsProcessingScanConfirm = TRUE; // handle scanConfirm
  PopNwkScanForNetworks(iChannelList, // PopNwkGetChannelList()
                        PopNwkGetJoinOptions(),
                        PopNwkGetScanTimeMs());
}

/*******************************************************************************
---------------- MpGwSetMuiReq-----------------------------------------
  MpGwSetMuiReq gateway request handling.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSetMuiReq(uint8_t *pPayload)
{
  sMpSetMuiReq_t setMuiReq;
  popEvtId_t iEvtId;
  uint8_t iLen;
  uint8_t payload[SET_MUI_RSP_SIZE];

  setMuiReq.mui  = (((uint64_t)pPayload[SET_MUI_DATA + 7]) << 56);
  setMuiReq.mui |= (((uint64_t)pPayload[SET_MUI_DATA + 6]) << 48);
  setMuiReq.mui |= (((uint64_t)pPayload[SET_MUI_DATA + 5]) << 40);
  setMuiReq.mui |= (((uint64_t)pPayload[SET_MUI_DATA + 4]) << 32);
  setMuiReq.mui |= (((uint64_t)pPayload[SET_MUI_DATA + 3]) << 24);
  setMuiReq.mui |= (((uint64_t)pPayload[SET_MUI_DATA + 2]) << 16);
  setMuiReq.mui |= (((uint64_t)pPayload[SET_MUI_DATA + 1]) << 8 );
  setMuiReq.mui |= (((uint64_t)pPayload[SET_MUI_DATA    ])      );

  PopNwkSetMacAddr((uint8_t*)&setMuiReq.mui);

  iEvtId = gMpSetMuiRsp_c;
  payload[SET_MUI_RSP_EVTID] = iEvtId;
  iLen = SET_MUI_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  payload[SET_MUI_RSP_LEN] = (uint8_t)(iLen);
  payload[SET_MUI_RSP_STATUS] = 0;

  // Send message to gateway
  MpGwSendPacket(payload);
}

/*******************************************************************************
---------------- MpGwJoinNetworkRsp --------------------------------------------
  MpGwJoinNetwork event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinNetworkRsp(popStatus_t iNwkStartConfirm)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpJoinNetworkRsp_t sMpJoinNetworkRsp;
  uint8_t pPayload[JOIN_NWK_RSP_SIZE];

  gfPopNwkIsProcessingBeaconEnabled = FALSE;


  if(pPayload){
    // get data and put to message
    iEvtId = gMpJoinNetworkRsp_c;
    pPayload[JOIN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
    iLen = JOIN_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[JOIN_NWK_RSP_LEN] = (uint8_t)(iLen);
    // Status
    sMpJoinNetworkRsp.iStatus = iNwkStartConfirm;
    pPayload[JOIN_NWK_RSP_STATUS] = (uint8_t)(sMpJoinNetworkRsp.iStatus);
    // channel
    sMpJoinNetworkRsp.iChannel = PopNwkGetChannel();
    pPayload[JOIN_NWK_RSP_CHANNEL] = (uint8_t)(sMpJoinNetworkRsp.iChannel);
    // Pan id
    sMpJoinNetworkRsp.iPanId = PopNwkGetPanId();
    pPayload[JOIN_NWK_RSP_PANID_H] = (uint8_t)(sMpJoinNetworkRsp.iPanId >> 8);
    pPayload[JOIN_NWK_RSP_PANID_L] = (uint8_t)(sMpJoinNetworkRsp.iPanId);
    // Node id
    sMpJoinNetworkRsp.iNwkAddr = PopNwkGetNodeAddr();
    pPayload[JOIN_NWK_RSP_NODEID_H] = (uint8_t)(sMpJoinNetworkRsp.iNwkAddr >> 8);
    pPayload[JOIN_NWK_RSP_NODEID_L] = (uint8_t)(sMpJoinNetworkRsp.iNwkAddr);
    // Send message to gateway
    MpGwSendPacket(pPayload);
    //if(PopSetEventEx(cGwTaskId, gMpGwSendPacket_c, pPayload) != gPopErrNone_c){
      //(void)PopMemFree(pPayload);
    //}
  }
}

/*******************************************************************************
---------------- MpGwLeaveNetworkReq -------------------------------------------
  MpGwLeaveNetwork gateway request handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwLeaveNetworkReq(void)
{
  // leave network
  PopNwkLeaveNetwork();
}

/*******************************************************************************
---------------- MpGwLeaveNetworkRsp -------------------------------------------
  MpGwLeaveNetwork event handling. The PopNwkLeaveNetwork cannot fail so if
  the event gPopNekLeaveConfirm_c has arrived, which trigger this function
  it means that this node has left the network.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwLeaveNetworkRsp(void)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpLeaveNetworkRsp_t sMpLeaveNetworkRsp;
  uint8_t pPayload[LEAVE_NWK_RSP_SIZE];


  if(pPayload){
    // get data and put to message
    // Event id
    iEvtId = gMpLeaveNetworkRsp_c;
    pPayload[LEAVE_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
    // Length
    iLen = LEAVE_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[LEAVE_NWK_RSP_LEN] = (uint8_t)(iLen);
    // Status
    sMpLeaveNetworkRsp.iStatus = gPopErrNone_c;
    pPayload[LEAVE_NWK_RSP_STATUS] =  (uint8_t)(sMpLeaveNetworkRsp.iStatus);
    // Send message to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwScanNetworkHandling ---------------------------------------
  popEvtNwkBeaconIndication event handling.
  Collect all beacons. restart timer for timeout
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanNetworkHandling(sPopNwkBeaconIndication_t *pNwkBeaconIndication)
{
//  sMpJoinNetworkReq_t sMpJoinNetworkReq;
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;



//  iChannel = pNwkBeaconIndication->iChannel;  equals to zero bug?
  iChannel = gsMpScanNetworkReq.iChannel;
  iPanId = pNwkBeaconIndication->iPanId;
  iNwkAddr = pNwkBeaconIndication->iNwkAddr;

  if(/*(iChannel == gsMpScanNetworkReq.iChannel) &&*/ (iPanId == gsMpScanNetworkReq.iPanId)){
    if(!giNodesInBeaconList){           // if first BeaconIndication
        pMpBeaconList[BEACONLIST_CHANNEL] = (uint8_t)iChannel;
        pMpBeaconList[BEACONLIST_PANID_H] = (uint8_t)(iPanId >> 8);
        pMpBeaconList[BEACONLIST_PANID_L] = (uint8_t)(iPanId);
        pMpBeaconList[BEACONLIST_NODEID_H] = (uint8_t)(iNwkAddr >> 8);
        pMpBeaconList[BEACONLIST_NODEID_L] = (uint8_t)(iNwkAddr);
        giNodesInBeaconList++;
    }
    else{

      popNwkAddr_t iNwkAddrInList;
      uint8_t iListIdx;
      bool NwkAddrDoublett;

      iListIdx = 0;
      NwkAddrDoublett = FALSE;

      // loop through pMpBeaconList and search for doubletts
      while((iListIdx < giNodesInBeaconList) && !NwkAddrDoublett){
        // Get Nwkddr from list
        iNwkAddrInList = (popNwkAddr_t)(pMpBeaconList[BEACONLIST_NODEID_H + iListIdx * (sizeof(popNwkAddr_t))] << 8);
        iNwkAddrInList |= (popNwkAddr_t)(pMpBeaconList[BEACONLIST_NODEID_L + iListIdx * (sizeof(popNwkAddr_t))]);
        // Compare with new NwkAddr
        if(iNwkAddr == iNwkAddrInList){
          // NwkAddr already in table
          NwkAddrDoublett = TRUE;
        }
        iListIdx++;
      }

      // Add new NwkAddr to table if no doublett
      if(!NwkAddrDoublett && (giNodesInBeaconList < 100)){
        pMpBeaconList[BEACONLIST_NODEID_H + giNodesInBeaconList * (sizeof(popNwkAddr_t))] = (uint8_t)(iNwkAddr >> 8);
        pMpBeaconList[BEACONLIST_NODEID_L + giNodesInBeaconList * (sizeof(popNwkAddr_t))] = (uint8_t)(iNwkAddr);
        giNodesInBeaconList++;
      }
    }
  }
  // start/restart timer
  PopStartTimerEx(cGwTaskId, gMpGwBeaconIndicationTimerId_c, gMpGwBeaconIndicationTime_c);
}

/*******************************************************************************
---------------- MpGwScanNetworkReq --------------------------------------------
  MpGwScanNetwork gateway request handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanNetworkReq(uint8_t *pPayload)
{
  popChannelList_t iChannelList;
  popChannel_t iChannel;

  // transform PanId to native
  // place data into struct
  gsMpScanNetworkReq.iChannel = (popChannel_t)pPayload[SCAN_NWK_REQ_CHANNEL];
  gsMpScanNetworkReq.iPanId = (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_H] << 8);
  gsMpScanNetworkReq.iPanId |= (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_L]);

  // Enable beacon events in app
  // set channel list from channel
  // if channel = 0 scan all channels
  if(gsMpScanNetworkReq.iChannel == gPopNwkUseChannelList_c){
    iChannelList = 0xFFFF;
    // scan for network
    gfPopNwkIsProcessingBeaconEnabled = TRUE;
    PopNwkScanForNetworks(iChannelList,
                          0, //mPopDefaultScanOptions_c,  ///PopNwkGetScanOptions(), // popScanOption_t  iOptions;
                          PopNwkGetScanTimeMs()); // popTimeOut_t iTimeForEachScanMs;
  }
  // else set the corresponding bit in channellist
  else{
    if(gsMpScanNetworkReq.iPanId == gPopNwkAnyPanId_c){
      // else set the corresponding bit in channellist
      iChannelList = 1 << (gsMpScanNetworkReq.iChannel - 11);
      // scan for network
      gfPopNwkIsProcessingBeaconEnabled = TRUE;
      PopNwkScanForNetworks(iChannelList,
                            0, //mPopDefaultScanOptions_c,  ///PopNwkGetScanOptions(), // popScanOption_t  iOptions;
                            PopNwkGetScanTimeMs()); // popTimeOut_t iTimeForEachScanMs;
    }
    else{
      if(gsMpScanNetworkReq.iPanId < gPopNwkReservedPan_c){
      // set channel
      iChannel = gsMpScanNetworkReq.iChannel;
      // send beacon request
      gfPopNwkIsProcessingBeaconEnabled = TRUE;
      // Set flag for Beacon handling
      gfMpGwIsProcessingBeacon_c = TRUE;
      // start/restart timer
      PopStartTimerEx(cGwTaskId, gMpGwBeaconIndicationTimerId_c, gMpGwBeaconIndicationTime_c);
      PopStartTimerEx(cGwTaskId, gMpGwScanNetworkTimerId_c, gMpGwScanNetworkTime_c);
      // send out beacon request
      PopNwkBeaconRequest(iChannel);
      }
      else{
        // wrong param send response with no data
        MpGwScanNetworkRsp(NULL);
      }

    }
  }

}
/*******************************************************************************
---------------- MpGwScanNetworkRsp --------------------------------------------
  MpGwScanNetwork event handling.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanNetworkRsp(sPopNwkScanForNetworks_t *pNwkScanConfirm)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpScanNetworkRsp_t sMpScanNetworkRsp;
  sMpScanNetworkRsp_t sMpScanNetworkRspTable[MAXNWK];
  uint8_t pPayload[SCAN_NWK_RSP_SIZE];
  gfPopNwkIsProcessingBeaconEnabled = FALSE;

  if(pNwkScanConfirm){
    if(pNwkScanConfirm->iNumberOfBeacons){

      // reset table of networks
      for(int i = 0; i < MAXNWK; i++){
        sMpScanNetworkRspTable[i].iChannel = 0;
        sMpScanNetworkRspTable[i].iPanId = 0;
        sMpScanNetworkRspTable[i].iNwkAddr = 0;
      }

      popChannel_t iChannel;
      popPanId_t iPanId;
      popNwkAddr_t iNwkAddr;
      uint8_t iExclusiveNwks;

      iExclusiveNwks = 0;
      // loop through all new beacons and search for doubletts in rsp table
      for(int i = 0; i < pNwkScanConfirm->iNumberOfBeacons; i++){
        iChannel = pNwkScanConfirm->aBeaconList[i].iChannel;
        iPanId = pNwkScanConfirm->aBeaconList[i].iPanId;
        iNwkAddr = pNwkScanConfirm->aBeaconList[i].iNwkAddr;

        bool NwkDoublett;
        uint8_t iRspTableIdx;

        NwkDoublett = FALSE;
        iRspTableIdx = 0;

        while((iRspTableIdx < iExclusiveNwks) && !NwkDoublett){
          if(iChannel == sMpScanNetworkRspTable[iRspTableIdx].iChannel){
            if(iPanId == sMpScanNetworkRspTable[iRspTableIdx].iPanId){
              // Network already in table
              NwkDoublett = TRUE;
            }
          }
          iRspTableIdx++;
        }
        // fill table with exclusive networks, no doubletts
        if(!NwkDoublett && (iExclusiveNwks < MAXNWK)){
          sMpScanNetworkRspTable[iExclusiveNwks].iChannel = iChannel;
          sMpScanNetworkRspTable[iExclusiveNwks].iPanId = iPanId;
          sMpScanNetworkRspTable[iExclusiveNwks].iNwkAddr = iNwkAddr;
          iExclusiveNwks++;
        }
      }

      for(int i = 0; i < iExclusiveNwks; i++){

        if(pPayload){
          // Get data and put data to message buffer
          // Event id
          iEvtId = gMpScanNetworkRsp_c;
          pPayload[SCAN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
          // Length
          iLen = SCAN_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
          pPayload[SCAN_NWK_RSP_LEN] = (uint8_t)(iLen);
          // Channel
          iChannel = sMpScanNetworkRspTable[i].iChannel;
          pPayload[SCAN_NWK_RSP_CHANNEL] = (uint8_t)(iChannel);
          // Pan Id
          iPanId = sMpScanNetworkRspTable[i].iPanId;
          pPayload[SCAN_NWK_RSP_PANID_H] = (uint8_t)(iPanId >> 8);
          pPayload[SCAN_NWK_RSP_PANID_L] = (uint8_t)(iPanId);
          // Node Id
          iNwkAddr = sMpScanNetworkRspTable[i].iNwkAddr;
          pPayload[SCAN_NWK_RSP_NODEID_H] = (uint8_t)(iNwkAddr >> 8);
          pPayload[SCAN_NWK_RSP_NODEID_L] = (uint8_t)(iNwkAddr);

          // send data to gateway
          MpGwSendPacket(pPayload);
          //if(PopSetEventEx(cGwTaskId, gMpGwSendPacket_c, pPayload) != gPopErrNone_c){
            //(void)PopMemFree(pPayload);
          //}
        }
      }
    }
    else{

      if(pPayload){
        // Get data and put data to message buffer
        // Event id
        iEvtId = gMpScanNetworkRsp_c;
        pPayload[SCAN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
        // Length
        iLen = SCAN_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
        pPayload[SCAN_NWK_RSP_LEN] = (uint8_t)(iLen);
        // Channel
        sMpScanNetworkRsp.iChannel = 0x00;
        pPayload[SCAN_NWK_RSP_CHANNEL] = (uint8_t)(sMpScanNetworkRsp.iChannel);
        // Pan Id
        sMpScanNetworkRsp.iPanId = 0x0000;
        pPayload[SCAN_NWK_RSP_PANID_H] = (uint8_t)(sMpScanNetworkRsp.iPanId >> 8);
        pPayload[SCAN_NWK_RSP_PANID_L] = (uint8_t)(sMpScanNetworkRsp.iPanId);
        // Node Id
        sMpScanNetworkRsp.iNwkAddr = 0x0000;
        pPayload[SCAN_NWK_RSP_NODEID_H] = (uint8_t)(sMpScanNetworkRsp.iNwkAddr >> 8);
        pPayload[SCAN_NWK_RSP_NODEID_L] = (uint8_t)(sMpScanNetworkRsp.iNwkAddr);

        // send data to gateway
        MpGwSendPacket(pPayload);
        //if(PopSetEventEx(cGwTaskId, gMpGwSendPacket_c, pPayload) != gPopErrNone_c){
          //(void)PopMemFree(pPayload);
        //}
      }
    }
  }
  // function call from App with own beacon indication handling
  else{
    if(giNodesInBeaconList){
      // add Event Id and Length of BeaconList
      // Event id
      iEvtId = gMpScanNetworkRsp_c;
      pMpBeaconList[SCAN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
      // Length
      iLen = sizeof(popChannel_t) + sizeof(popPanId_t) + (sizeof(popNwkAddr_t) * giNodesInBeaconList);
      pMpBeaconList[SCAN_NWK_RSP_LEN] = (uint8_t)(iLen);
      // send data to gateway
      MpGwSendPacket(pMpBeaconList);
    }
    else{

      if(pPayload){
        // Get data and put data to message buffer
        // Event id
        iEvtId = gMpScanNetworkRsp_c;
        pPayload[SCAN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
        // Length
        iLen = SCAN_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
        pPayload[SCAN_NWK_RSP_LEN] = (uint8_t)(iLen);
        // Channel
        sMpScanNetworkRsp.iChannel = 0x00;
        pPayload[SCAN_NWK_RSP_CHANNEL] = (uint8_t)(sMpScanNetworkRsp.iChannel);
        // Pan Id
        sMpScanNetworkRsp.iPanId = 0x0000;
        pPayload[SCAN_NWK_RSP_PANID_H] = (uint8_t)(sMpScanNetworkRsp.iPanId >> 8);
        pPayload[SCAN_NWK_RSP_PANID_L] = (uint8_t)(sMpScanNetworkRsp.iPanId);
        // Node Id
        sMpScanNetworkRsp.iNwkAddr = 0x0000;
        pPayload[SCAN_NWK_RSP_NODEID_H] = (uint8_t)(sMpScanNetworkRsp.iNwkAddr >> 8);
        pPayload[SCAN_NWK_RSP_NODEID_L] = (uint8_t)(sMpScanNetworkRsp.iNwkAddr);
        // send data to gateway
        MpGwSendPacket(pPayload);

      }
    }

    // Reset beaconlist
    giNodesInBeaconList = 0;
    for(int i = 0; i < BEACONLIST_SIZE; i++){
      pMpBeaconList[i] = 0;
    }
    // reset flag for beacon procesing
    gfMpGwIsProcessingBeacon_c = FALSE;
  }

}

/*******************************************************************************
---------------- MpGwSetRadioParamReq -------------------------------------------
  MpGwSetRadioParamReq event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSetRadioParamReq(sPopEvtData_t *pData)
{
  uint8_t iStatus;
  sMpSetRadioParamReq_t sMpSetRadioParamReq;
  uint8_t *pPayload;
// #106  uint8_t NewRadioParam;

  pPayload = (uint8_t *)pData;

  iStatus = gPopErrFailed_c;
  sMpSetRadioParamReq.iChannel = (popChannel_t)pPayload[SET_RADIO_PARAM_REQ_CHANNEL];
  sMpSetRadioParamReq.iPanId = (popPanId_t)(pPayload[SET_RADIO_PARAM_REQ_PANID_H] << 8);
  sMpSetRadioParamReq.iPanId |= (popPanId_t)(pPayload[SET_RADIO_PARAM_REQ_PANID_L]);
  sMpSetRadioParamReq.iNwkAddr = (popNwkAddr_t)(pPayload[SET_RADIO_PARAM_REQ_NODEID_H] << 8);
  sMpSetRadioParamReq.iNwkAddr |= (popNwkAddr_t)(pPayload[SET_RADIO_PARAM_REQ_NODEID_L]);

  if(MpGwValidNwkData(sMpSetRadioParamReq.iChannel, sMpSetRadioParamReq.iPanId, sMpSetRadioParamReq.iNwkAddr)){
    // set channel
    //PopNwkSetNwkDataChannel(sMpSetRadioParamReq.iChannel);
    PopNwkSetChannel(sMpSetRadioParamReq.iChannel);
    // set PanId
    PopNwkSetPanId(sMpSetRadioParamReq.iPanId);
    // set NodeAddr
    PopNwkSetNodeAddr(sMpSetRadioParamReq.iNwkAddr);

    // store network data
    iStatus = PopNvStoreNwkData();
    // reset CPU so that new values can be initialized
    if(iStatus == gPopErrNone_c){
      (void)PopStartTimerEx(cGwTaskId,gMpGwResetCPUTimerId_c,500);
    }
  }
  else{
    // Invalid parameters
    iStatus = gPopErrBadParm_c;
  }
  // call response function
  MpGwSetRadioParamRsp(iStatus);
}

/*******************************************************************************
---------------- MpGwSetRadioParamRsp ------------------------------------------
  MpGwSetRadioParamRsp response handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSetRadioParamRsp(uint8_t iStatus)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpSetRadioParamRsp_t sMpSetRadioParamRsp;
  uint8_t pPayload[SET_RADIO_PARAM_RSP_SIZE];


  if(pPayload){
    //get data and put to message array
    // Event Id
    iEvtId = gMpSetRadioParamRsp_c;
    pPayload[SET_RADIO_PARAM_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = SET_RADIO_PARAM_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[SET_RADIO_PARAM_RSP_LEN] = (uint8_t)iLen;
    // Status
    sMpSetRadioParamRsp.iStatus = iStatus;
    pPayload[SET_RADIO_PARAM_RSP_STATUS] = (uint8_t)sMpSetRadioParamRsp.iStatus;
    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwGetRadioDataReq -------------------------------------------
  MpGwGetRadioData event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwGetRadioDataReq(uint8_t *pPayload)
{
  uint32_t chargerId;

  chargerId = ((uint32_t)pPayload[GET_RADIO_DATA_CHARGER_ID_L]);
  chargerId |= ((uint32_t)pPayload[GET_RADIO_DATA_CHARGER_ID_LM]) << 8;
  chargerId |= ((uint32_t)pPayload[GET_RADIO_DATA_CHARGER_ID_HM]) << 16;
  chargerId |= ((uint32_t)pPayload[GET_RADIO_DATA_CHARGER_ID_H]) << 24;
  charger_SetId(chargerId);

  // nothing to do but call the response function
  MpGwGetRadioDataRsp();
}

/*******************************************************************************
---------------- MpGwGetRadioDataRsp -------------------------------------------
  MpGwGetRadioData response handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwGetRadioDataRsp()
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpGetRadioDataRsp_t sMpGetRadioDataRsp;
  uint8_t pPayload[RADIO_DATA_RSP_SIZE];


  if(pPayload){
    // Get data and put to message payload
    // Event Id
    iEvtId = gMpGetRadioDataRsp_c;
    pPayload[RADIO_DATA_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = RADIO_DATA_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[RADIO_DATA_RSP_LEN] = (uint8_t)iLen;
    // Firmware type
    sMpGetRadioDataRsp.iFwType = giFwType;
    pPayload[RADIO_DATA_RSP_FWTYPE_H] = (uint8_t)(sMpGetRadioDataRsp.iFwType >> 24);
    pPayload[RADIO_DATA_RSP_FWTYPE_MH] = (uint8_t)(sMpGetRadioDataRsp.iFwType >> 16);
    pPayload[RADIO_DATA_RSP_FWTYPE_ML] =(uint8_t)(sMpGetRadioDataRsp.iFwType >> 8);
    pPayload[RADIO_DATA_RSP_FWTYPE_L] = (uint8_t)(sMpGetRadioDataRsp.iFwType);
    // Firmware version
    sMpGetRadioDataRsp.iFwVer = giFwVer;
    pPayload[RADIO_DATA_RSP_FWVER_H] = (uint8_t)(sMpGetRadioDataRsp.iFwVer >> 24);
    pPayload[RADIO_DATA_RSP_FWVER_MH] = (uint8_t)(sMpGetRadioDataRsp.iFwVer >> 16);
    pPayload[RADIO_DATA_RSP_FWVER_ML] =(uint8_t)(sMpGetRadioDataRsp.iFwVer >> 8);
    pPayload[RADIO_DATA_RSP_FWVER_L] = (uint8_t)(sMpGetRadioDataRsp.iFwVer);
    // Checksum
    sMpGetRadioDataRsp.iChkSum = giChkSum;
    pPayload[RADIO_DATA_RSP_CHKSUM_H] = (uint8_t)(sMpGetRadioDataRsp.iChkSum >> 24);
    pPayload[RADIO_DATA_RSP_CHKSUM_MH] = (uint8_t)(sMpGetRadioDataRsp.iChkSum >> 16);
    pPayload[RADIO_DATA_RSP_CHKSUM_ML] =(uint8_t)(sMpGetRadioDataRsp.iChkSum >> 8);
    pPayload[RADIO_DATA_RSP_CHKSUM_L] = (uint8_t)(sMpGetRadioDataRsp.iChkSum);
    // CM id (MAC address)
    PopNwkGetMacAddr(sMpGetRadioDataRsp.aCmId);
    pPayload[RADIO_DATA_RSP_CMID_0] = sMpGetRadioDataRsp.aCmId[0];
    pPayload[RADIO_DATA_RSP_CMID_1] = sMpGetRadioDataRsp.aCmId[1];
    pPayload[RADIO_DATA_RSP_CMID_2] = sMpGetRadioDataRsp.aCmId[2];
    pPayload[RADIO_DATA_RSP_CMID_3] = sMpGetRadioDataRsp.aCmId[3];
    pPayload[RADIO_DATA_RSP_CMID_4] = sMpGetRadioDataRsp.aCmId[4];
    pPayload[RADIO_DATA_RSP_CMID_5] = sMpGetRadioDataRsp.aCmId[5];
    pPayload[RADIO_DATA_RSP_CMID_6] = sMpGetRadioDataRsp.aCmId[6];
    pPayload[RADIO_DATA_RSP_CMID_7] = sMpGetRadioDataRsp.aCmId[7];
    // Manufacturer id
    sMpGetRadioDataRsp.iMfgId = PopNwkGetManufacturerId();
    pPayload[RADIO_DATA_RSP_MFGID_H] = (uint8_t)(sMpGetRadioDataRsp.iMfgId >> 8);
    pPayload[RADIO_DATA_RSP_MFGID_L] = (uint8_t)(sMpGetRadioDataRsp.iMfgId);
    // Application id
    sMpGetRadioDataRsp.iAppId = PopNwkGetApplicationId();
    pPayload[RADIO_DATA_RSP_APPID_H] = (uint8_t)(sMpGetRadioDataRsp.iAppId >> 8);
    pPayload[RADIO_DATA_RSP_APPID_L] = (uint8_t)(sMpGetRadioDataRsp.iAppId);
    // Channel id
    if(giServiceMode){
      sMpGetRadioDataRsp.iChannel = giServiceChannel;
    }
    else{
      sMpGetRadioDataRsp.iChannel = PopNwkGetChannel();
    }
    pPayload[RADIO_DATA_RSP_CHANNEL] = (uint8_t)(sMpGetRadioDataRsp.iChannel);
    // PAN id
    sMpGetRadioDataRsp.iPanId = PopNwkGetPanId();
    pPayload[RADIO_DATA_RSP_PANID_H] = (uint8_t)(sMpGetRadioDataRsp.iPanId >> 8);
    pPayload[RADIO_DATA_RSP_PANID_L] = (uint8_t)(sMpGetRadioDataRsp.iPanId);
    // Node id
    sMpGetRadioDataRsp.iNwkAddr = PopNwkGetNodeAddr();
    pPayload[RADIO_DATA_RSP_NODEID_H] = (uint8_t)(sMpGetRadioDataRsp.iNwkAddr >> 8);
    pPayload[RADIO_DATA_RSP_NODEID_L] = (uint8_t)(sMpGetRadioDataRsp.iNwkAddr);
    // Address pool
    sMpGetRadioDataRsp.AddrPool = PopNwkGetAddrCount();
    pPayload[RADIO_DATA_RSP_ADDRPOOL_H] = (uint8_t)(sMpGetRadioDataRsp.AddrPool >> 8);
    pPayload[RADIO_DATA_RSP_ADDRPOOL_L] = (uint8_t)(sMpGetRadioDataRsp.AddrPool);

    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwNwkDataReq ------------------------------------------------
  If it is a request message add the message to queue and
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNwkDataReq(sPopEvtData_t *pData)
{

  uint8_t *pPayload;
  sMessage_t sThisMessage;
  uint8_t iStatus;

  // set returm status to fail by default
  iStatus = gPopErrFailed_c;

  pPayload = (uint8_t *)pData;


  sThisMessage.iDstAddr = (popNwkAddr_t)(pPayload[DATA_REQ_DSTADDR_H] << 8);  // Get Destination address
  sThisMessage.iDstAddr |= (popNwkAddr_t)(pPayload[DATA_REQ_DSTADDR_L]);      // Get Destination address
  sThisMessage.iOptions = gPopNwkDataReqOptsDefault_c;                        // Get Options
  sThisMessage.iRadius = PopNwkGetDefaultRadius();                            // Get Radius
  sThisMessage.iPayloadLength = (uint8_t)pPayload[DATA_REQ_PAYLOADLEN];        // Get payload length

  if(sThisMessage.iPayloadLength <= MAX_PAYLOAD)
  {
    PopMemCpy(&(sThisMessage.aPayload), &pPayload[DATA_REQ_PAYLOAD], sThisMessage.iPayloadLength);    // copy payload to message

    //if app could handle the message set response status to true
    if(MpNwkDataRequest(&sThisMessage)){
      iStatus = gPopErrNone_c;
    }
  }
  else{
    // Do not handle message if > MAX_PAYLOAD
  }
  // send message to Nwk app
  MpGwNwkDataRsp(iStatus);

}

/*******************************************************************************
---------------- MpGwNwkDataRsp --------------------------------------------
  Response sent as soon as radio app has checked if it can handle the data request
  message (if it has enough free buffers)
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNwkDataRsp(uint8_t iStatus)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpNwkDataRsp_t sMpNwkDataRsp;
  uint8_t pPayload[DATA_RSP_SIZE];


  if(pPayload){
    // get data and put to message payload
    // Event Id
    iEvtId = gMpNwkDataRsp_c;
    pPayload[DATA_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = DATA_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[DATA_RSP_LEN] = (uint8_t)iLen;
    // Status
    sMpNwkDataRsp.iStatus = iStatus;
    pPayload[DATA_RSP_STATUS] = sMpNwkDataRsp.iStatus;
    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwDataIndication ---------------------------------------
  MpGwDataIndication event handling.

--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwDataIndication(sPopNwkDataIndication_t *pIndication)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpDataIndication_t sMpDataIndication;
  uint8_t pPayload[DATA_IND_SIZE + MAX_PAYLOAD];


  if(pPayload){
    //Get data and put into message array
    // Event Id
    iEvtId = gMpDataIndication_c;
    pPayload[DATA_IND_EVTID] = (uint8_t)(iEvtId);
    // Length
    iLen = sizeof(sMpDataIndication.iSrcAddr) + sizeof(sMpDataIndication.iPayloadLength) + PopNwkPayloadLen(pIndication);
    pPayload[DATA_IND_LEN] = (uint8_t)(iLen);
    // Source address
    sMpDataIndication.iSrcAddr = PopNwkSrcAddr(pIndication);
    pPayload[DATA_IND_SRCADDR_H] = (uint8_t)(sMpDataIndication.iSrcAddr >> 8);
    pPayload[DATA_IND_SRCADDR_L] = (uint8_t)(sMpDataIndication.iSrcAddr);
    // Payload length
    sMpDataIndication.iPayloadLength = PopNwkPayloadLen(pIndication);
    pPayload[DATA_IND_PAYLOADLEN] = sMpDataIndication.iPayloadLength;
    // Payload
    sMpDataIndication.pPayload = PopNwkPayload(pIndication);
    PopMemCpy(&pPayload[DATA_IND_PAYLOAD], sMpDataIndication.pPayload, sMpDataIndication.iPayloadLength);

    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwSendPacket ------------------------------------------------
  Gateway send packet event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSendPacket(uint8_t *pData)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  uint8_t *pPayload;



  iEvtId = (popEvtId_t)(pData[GW_PACKET_EVTID]);
  iLen = (uint8_t)(pData[GW_PACKET_LEN]);
  pPayload = &(pData[GW_PACKET_PAYLOAD]);

  PopGatewaySendEventToPC(iEvtId, iLen, pPayload);

}

/*******************************************************************************
---------------- MpGwNodePlacementReq ------------------------------------------
  MpGwNodePlacementReq event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNodePlacementReq()
{
  // start node placement
  if(!gfMpPlacementStart){
    MpGwStartNodePlacement();
  }
  else{
    // Call timer function directly
    (void)PopStartTimerEx(cGwTaskId,gMpGwTimerIdNodePlaceBeaconReq_c,0);
  }
  // start timer for function timeout
  (void)PopStartTimerEx(cGwTaskId,gMpGwNodePlacementTimerId_c,gMpGwNodePlacementTimeoutTime_c);
}

/*******************************************************************************
---------------- MpGwNodePlacementRsp ------------------------------------------
  MpGwNodePlacementRsp event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNodePlacementRsp(sPopNodePlacement_t *pPopNodePlacementResult)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpNodePlacementRsp_t sMpNodePlacementRsp;
  uint8_t pPayload[NODE_PLACEMENT_RSP_SIZE];


  if(pPayload){
    // get data and put to message payload
    // Event Id
    iEvtId = gMpNodePlacementRsp_c;
    pPayload[NODE_PLACEMENT_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = sizeof(sMpNodePlacementRsp.iStatus);
    pPayload[NODE_PLACEMENT_RSP_LEN] = (uint8_t)iLen;
    // Status
    sMpNodePlacementRsp.iStatus = pPopNodePlacementResult->iBars;
    pPayload[NODE_PLACEMENT_RSP_STATUS] = sMpNodePlacementRsp.iStatus;
    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwSetUserLevelReq -------------------------------------------
  Set user level
--------------------------------------------------------------------------------
*******************************************************************************/
uint8_t MpGwSetUserLevelReq(uint8_t *pByte)
{
  uint8_t *pPayload;
  uint8_t iUserLevel;

  pPayload = pByte;
  iUserLevel = pPayload[SET_USERLEVEL_REQ_USERLEVEL];

  return iUserLevel;
}

/*******************************************************************************
---------------- MpGwSetUserLevelRsp -------------------------------------------
  Response sent as soon as user level has been set
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSetUserLevelRsp(uint8_t iStatus)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpSetUserLevelRsp_t sMpSetUserLevelRsp;
  uint8_t pPayload[SET_USERLEVEL_RSP_SIZE];


  if(pPayload){
    // get data and put to message payload
    // Event Id
    iEvtId = gMpSetUserLevelRsp_c;
    pPayload[SET_USERLEVEL_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = SET_USERLEVEL_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[SET_USERLEVEL_RSP_LEN] = (uint8_t)iLen;
    // Status
    sMpSetUserLevelRsp.iStatus = iStatus;
    pPayload[SET_USERLEVEL_RSP_STATUS] = sMpSetUserLevelRsp.iStatus;
    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwGetUserLevelReq -------------------------------------------
  Set user level
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwGetUserLevelReq()
{
  ;  // not used
}

/*******************************************************************************
---------------- MpGwGetUserLevelRsp -------------------------------------------
  Response sent as soon as user level has been set
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwGetUserLevelRsp(uint8_t iUserLevel)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpGetUserLevelRsp_t sMpGetUserLevelRsp;
  uint8_t pPayload[GET_USERLEVEL_RSP_SIZE];


  if(pPayload){
    // get data and put to message payload
    // Event Id
    iEvtId = gMpGetUserLevelRsp_c;
    pPayload[GET_USERLEVEL_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = GET_USERLEVEL_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[GET_USERLEVEL_RSP_LEN] = (uint8_t)iLen;
    // Status
    sMpGetUserLevelRsp.iUserLevel = iUserLevel;
    pPayload[GET_USERLEVEL_RSP_USERLEVEL] = sMpGetUserLevelRsp.iUserLevel;
    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwScanForNodesHandling ---------------------------------------
  ScanForNodes event handling.
  Collect all node addreses. restart timer for timeout
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanForNodesHandling(sPopNwkDataIndication_t *pDataIndication)
{
//  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
  bool  fNodeListFull;


//iChannel =  ??;
  iPanId = pDataIndication->sFrame.sMac.iPanId;
  iNwkAddr = PopNwkSrcAddr(pDataIndication);

  if(/*(iChannel == gsMpScanForNodesReq.iChannel) &&*/ (iPanId == gsMpScanForNodesReq.iPanId))
  {
    // Add new node NwkAddr to table if list is not already full
    // or if iy is a doublett
    fNodeListFull = (giNodesInList >= NODELIST_SIZE) ? TRUE:FALSE;
    if(!fNodeListFull)
    {
      popNwkAddr_t iNwkAddrInList;
      uint8_t iListIdx;
      bool NwkAddrDoublett;

      iListIdx = 0;
      NwkAddrDoublett = FALSE;

      // loop through node list and search for doubletts
      while((iListIdx < giNodesInList) && !NwkAddrDoublett)
      {
        // Get NwkAddr from node list
        iNwkAddrInList = gsMpNodeList[iListIdx].iNwkAddr;
        // Compare with new NwkAddr
        if(iNwkAddr == iNwkAddrInList){
          // NwkAddr already in table
          NwkAddrDoublett = TRUE;
        }
        iListIdx++;
      }

      // Add new NwkAddr to node list if no doublett
      if(!NwkAddrDoublett)
      {
        gsMpNodeList[giNodesInList].iNwkAddr = iNwkAddr;
        giNodesInList++;
      }
    }
  }
}

/*******************************************************************************
---------------- MpGwScanForNodesReq --------------------------------------------
  MpGwForNodes gateway request handling
--------------------------------------------------------------------------------
*******************************************************************************/
bool MpGwScanForNodesReq(uint8_t *pPayload)
{
  popChannel_t iChannel;
  popPanId_t iPanId;
  bool fValidNwkData;

  // Assume correct parameters
  fValidNwkData = TRUE;

  // clear NodeList, in case this has not alredy been done
  if(giNodesInList != 0)
  {
    uint8_t iIndex;
    for(iIndex = 0; iIndex < NODELIST_SIZE; iIndex++)
    {
      gsMpNodeList[iIndex].iNwkAddr = gPopNwkInvalidAddr_c;
    }
    giNodesInList = 0;
  }

  // Store Channel and PanId in global structure for later use
  gsMpScanForNodesReq.iChannel = (popChannel_t)pPayload[SCAN_NWK_REQ_CHANNEL];
  gsMpScanForNodesReq.iPanId = (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_H] << 8);
  gsMpScanForNodesReq.iPanId |= (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_L]);

  // Get local network parameters
  iChannel = PopNwkGetChannel();
  iPanId = PopNwkGetPanId();

  // Check if valid network parameters
  if((gsMpScanForNodesReq.iChannel == iChannel) && (gsMpScanForNodesReq.iPanId == iPanId))
  {
    // start/restart timer
    if(PopStartTimerEx(cGwTaskId, gMpGwScanForNodesTimerId_c, gMpGwScanForNodesTime_c) != gPopErrNone_c){
      fValidNwkData = FALSE;
    }
  }
  else
  {
    // wrong param
    fValidNwkData = FALSE;
  }

  if(!fValidNwkData){
    // wrong param send response directly
    MpGwScanForNodesRsp(FALSE);
  }

  return fValidNwkData;
}
/*******************************************************************************
---------------- MpGwScanForNodesRsp --------------------------------------------
  Send MpGwScanForNodes message.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanForNodesRsp(bool fValidNwkData)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
  uint8_t pPayload[SCAN_NODE_RSP_SIZE + 2*NODELIST_SIZE];


  if(giNodesInList)
  {
    // Convert NwkAddr in node list to payload
    popNwkAddr_t iNwkAddrInList;



    if(pPayload){
      // Get data and put to message payload
      // Event id
      iEvtId = gMpScanForNodesRsp_c;
      pPayload[SCAN_NODE_RSP_EVTID] = (uint8_t)(iEvtId);
      // Length
      iLen = sizeof(popChannel_t) + sizeof(popPanId_t) + (sizeof(popNwkAddr_t) * giNodesInList);
      pPayload[SCAN_NWK_RSP_LEN] = (uint8_t)(iLen);
      // Channel
      iChannel = gsMpScanForNodesReq.iChannel;
      pPayload[SCAN_NODE_RSP_CHANNEL] = (uint8_t)(iChannel);
      // Pan Id
      iPanId = gsMpScanForNodesReq.iPanId;
      pPayload[SCAN_NODE_RSP_PANID_H] = (uint8_t)(iPanId >> 8);
      pPayload[SCAN_NODE_RSP_PANID_L] = (uint8_t)(iPanId);
      // Node Id
      // loop through node list and convert NwkAddr to payload data
      for(int i = 0; i < giNodesInList; i++)
      {
        // Get NwkAddr from node list
        iNwkAddrInList = gsMpNodeList[i].iNwkAddr;
        // Add to payload
        pPayload[SCAN_NODE_RSP_NODEID_H + i * (sizeof(popNwkAddr_t))] = (uint8_t)(iNwkAddrInList >> 8);
        pPayload[SCAN_NODE_RSP_NODEID_L + i * (sizeof(popNwkAddr_t))] = (uint8_t)(iNwkAddrInList);
      }
    }
  }
  else{

    if(pPayload){
      // Get data and put to message payload
      // Event id
      iEvtId = gMpScanForNodesRsp_c;
      pPayload[SCAN_NODE_RSP_EVTID] = (uint8_t)(iEvtId);
      // Length
      iLen = sizeof(popChannel_t) + sizeof(popPanId_t) + (sizeof(popNwkAddr_t));
      pPayload[SCAN_NODE_RSP_LEN] = (uint8_t)(iLen);
      // Channel
      if(fValidNwkData){
        iChannel = 0x00;
      }
      else{
        iChannel = 0xFF;
      }
      pPayload[SCAN_NODE_RSP_CHANNEL] = (uint8_t)(iChannel);
      // Pan Id
      iPanId = 0xFFFF;
      pPayload[SCAN_NODE_RSP_PANID_H] = (uint8_t)(iPanId >> 8);
      pPayload[SCAN_NODE_RSP_PANID_L] = (uint8_t)(iPanId);
      // Node Id, not used because of iLen!!
      iNwkAddr = 0xFFFF;
      pPayload[SCAN_NODE_RSP_NODEID_H] = (uint8_t)(iNwkAddr >> 8);
      pPayload[SCAN_NODE_RSP_NODEID_L] = (uint8_t)(iNwkAddr);
    }
  }

  if(pPayload){
    // send data to gateway
    MpGwSendPacket(pPayload);
    //if(PopSetEventEx(cGwTaskId, gMpGwSendPacket_c, pPayload) != gPopErrNone_c){
      //(void)PopMemFree(pPayload);
    //}
  }

  // clear NodeList if needed
  if(giNodesInList != 0)
  {
    uint8_t iIndex;
    for(iIndex = 0; iIndex < NODELIST_SIZE; iIndex++)
    {
      gsMpNodeList[iIndex].iNwkAddr = gPopNwkInvalidAddr_c;
    }
    giNodesInList = 0;
  }
}

/*******************************************************************************
---------------- MpGwHeartbeatReq -------------------------------------------
  Heartbeat request
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwHeartbeatReq(uint8_t iBBC)
{
  // Set heartbeat flag
  gfHeartbeat = TRUE;
  // Call function to send response
  MpGwHeartbeatRsp();
}

/*******************************************************************************
---------------- MpGwHeartbeatRsp -------------------------------------------
  Response sent as soon as heartbeat request has been received
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwHeartbeatRsp()
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpHeartbeatRsp_t sMpHeartbeatRsp;
  uint8_t pPayload[HEARTBEAT_RSP_SIZE];


  if(pPayload){
    // get data and put to message payload
    // Event Id
    iEvtId = gMpHeartbeatRsp_c;
    pPayload[HEARTBEAT_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = HEARTBEAT_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[HEARTBEAT_RSP_LEN] = (uint8_t)iLen;
    // Status
    sMpHeartbeatRsp.iStatus = gPopErrNone_c;
    pPayload[HEARTBEAT_RSP_STATUS] = sMpHeartbeatRsp.iStatus;
    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwRoutingReq -------------------------------------------
  Routing request
  Routing enable = TRUE equals setting PopNwkSetRoutingSleepingMode(FALSE)
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwRoutingReq(uint8_t iEnable)
{
  uint8_t iDisableRoutingMode;
  // Set routing/broadcast mode
  iDisableRoutingMode = !iEnable;
  PopNwkSetRoutingSleepingMode(iDisableRoutingMode);
  // Call function to send response
  MpGwRoutingRsp();
}

/*******************************************************************************
---------------- MpGwRoutingRsp -------------------------------------------
  Response sent as soon as routing request has been received
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwRoutingRsp()
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpRoutingRsp_t sMpRoutingRsp;
  uint8_t pPayload[ROUTING_RSP_SIZE];


  if(pPayload){
    // get data and put to message payload
    // Event Id
    iEvtId = gMpRoutingRsp_c;
    pPayload[ROUTING_RSP_EVTID] = (uint8_t)iEvtId;
    // Length
    iLen = ROUTING_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[ROUTING_RSP_LEN] = (uint8_t)iLen;
    // Enable
    sMpRoutingRsp.iEnable =!PopNwkIsSleepyNode();
    pPayload[ROUTING_RSP_ENABLE] = sMpRoutingRsp.iEnable;
    // send data to gateway
    MpGwSendPacket(pPayload);
  }
}

/*******************************************************************************
---------------- MpGwStartWatchdog ---------------------------------------------
  Start watchdog timer
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwStartWatchdog()
{
  // Start the watchdog timer
  if(PopStartTimerEx(cGwTaskId,gMpGwWatchdogTimerId_c,gMpGwWatchdogTime_c) != gPopErrNone_c){
    PopAssert(gPopAssertGwWatchdog_c);
  }
}

/*******************************************************************************
---------------- MpGwStopWatchdog ---------------------------------------------
  Stop watchdog timer
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwStopWatchdog()
{
  // Stop the watchdog timer
  PopStopTimerEx(cGwTaskId, gMpGwWatchdogTimerId_c);
}