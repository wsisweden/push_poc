/***************************************************************************
  MpSerial.c

  Revision history:
  Rev   Date      Comment   Description
  this  120503    000       First revision

***************************************************************************/
#if gUseMpSerial_d

#include "MpSerial.h"
#include "MpGw.h"

#if TARGET_BOARD == CC2538SF53
#include "Interrupt.h"
#include "hw_ints.h"
#include "gpio.h"
#include "hw_memmap.h"
#include "ioc.h"
#include "hw_ioc.h"
#include <Hw/TIPhy/TIPhy.h>
#endif

#include <board_config.h>
#include <PopCfg.h>
#include <MpCfg.h>
#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>
#include <stddef.h>

/***************************************************************************
  Local Types & Defines
***************************************************************************/

/***************************************************************************
  Prototypes
***************************************************************************/
void MpSerialTimerHandler(popTimerId_t  iTimerId);
void MpGpioUart1Init(void);
void MpGpioUart2Init(void);

/* Test application functions */
void MpSerialTestToggleLight(void);
void MpSerialTestSendAckedPacket(sPopNwkDataRequest_t *pDataReq);
void MpSerialTestRetryPacket(void);
void MpSerialTestReceivePacket(sPopNwkDataIndication_t *pIndication);
void MpSerialTestRadioTxRxTest(uint8_t cmd);
void MpSerialStartNetworkResponse(popStatus_t iNwkStartConfirm);

extern void GpioUart1Init(void);
/***************************************************************************
  Globals and Externals
***************************************************************************/
/* globals for this module only */
uint8_t   giMpSerialActive;
uint8_t   gaMpSerialTxBuf[gMpSerialTxBufferSize_c]; /* ISR tx buf */
uint8_t  *gpMpSerialTxBufHead = gaMpSerialTxBuf;    /* buffer head */
uint8_t  *gpMpSerialTxBufTail = gaMpSerialTxBuf;    /* buffer tail */
uint8_t   gaMpSerialRxBuf[gMpSerialRxBufferSize_c]; /* ISR rx buf */
uint8_t  *gpMpSerialRxBufHead = gaMpSerialRxBuf;    /* buffer head */
uint8_t  *gpMpSerialRxBufTail = gaMpSerialRxBuf;    /* buffer tail */

volatile uint8_t gfMpSerialStatus;                      /* current status of xmit and recv */
uint8_t gfMpSerialActive = 0;                           /* Serial active flag */

/* Test application variables */
void    *gpPopAppPacket;
uint8_t  giPopAppPacketLen;
uint8_t  gTestMode = 0;
popLqi_t gLQI = 0;
popNwkAddr_t giAppRemoteAddr = 0x0000;                  /* Address ogf "golden unit" used for RadioTxRxTest */

extern const popTaskId_t cMpSerialTaskId;
extern uint32_t gManufacturerAndDeviceId;
extern uint16_t giState;
/***************************************************************************
  Code
***************************************************************************/

/*
  MppSerialTaskInit

  Initialize the serial task
*/
void MpSerialTaskInit(void)
{
  // enables serial input
  gfMpSerialActive=1;

  gpMpSerialTxBufHead =
  gpMpSerialTxBufTail = gaMpSerialTxBuf;
  gpMpSerialRxBufHead =
  gpMpSerialRxBufTail = gaMpSerialRxBuf;

  /* Initialize the Mp serial port*/
  (void)MpUart_Init(gaMpSerialRxBuf);

  // nothing received yet
  gfMpSerialStatus = 0;

}

/*
  MpSerialEventLoop
  This routine process serial events before sending them on to the
  application. Takes care of things like keeping track of mingap.
*/

void MpSerialTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
  // not yet active, don't send messages
  if(!gfMpSerialActive)
    return;

  // handle serial task event
  switch(iEvtId)
  {
    case gMpSerByteReceivedEvt_c:
      (void)PopStartTimerEx(cMpSerialTaskId,gMpSerMinGapTimer_c,gMpSerialMinGapMs_c);
      break;

    // time to inform the application of serial input
    case gPopEvtTimer_c:
      MpSerialTimerHandler(sEvtData.iTimerId);
      break;

    case gMpSerTxDone_c:
      //(void)PopSetAppEvent(gMpSerialTxDoneEvt_c, NULL);
      break;

    case gPopEvtNwkStartConfirm_c:
      // Handle start network response
      if(gTestMode == 0x15){
        MpSerialStartNetworkResponse(sEvtData.iNwkStartConfirm);
        // Reset Test mode and state and stop timeout timer
        gTestMode = 0;
        (void)PopStopTimerEx(cMpSerialTaskId, gMpSerialTimerTestTimeout_c);
      }
      break;
  } // end of switch
}

/*
  Serial driver timer handler.
*/
void MpSerialTimerHandler(popTimerId_t  iTimerId)
{
  // the min gap has occurred, send the data to the application
  if (iTimerId == gMpSerMinGapTimer_c)
  {
    // only set 1 app event for serial data ready (not many)
    (void)PopSetUniqueEventEx(cAppTaskId, gMpSerialDataReadyEvt_c, NULL);
  }

/* Test application timers */
  // send the packet again, this time with end-to-end retries
  if(iTimerId == gMpSerialTimerToggleReply_c)
  {
    MpSerialTestRetryPacket();
  }
  // Test has somehow failed
  if(iTimerId == gMpSerialTimerTestTimeout_c)
  {
    uint8_t u8Temp;

    // Reset Test mode and state
    gTestMode = 0;
    // Send end cmd to PC
    u8Temp = 0xFF;
    MpSerialSend(1, &u8Temp);
  }
}

/************************************************************************************
* MpGpioUart1Init
*
* This function initializate the gpio�s for the Uart1 module
*************************************************************************************/
void MpGpioUart1Init(void)
{
#if TARGET_BOARD != CC2538SF53
  register uint32_t tmpReg;

# if gUart1_EnableHWFlowControl_d == TRUE
  GPIO.PuSelLo |= (GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit);  // Pull-up select: UP type
  GPIO.PuEnLo  |= (GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit);  // Pull-up enable
  GPIO.InputDataSelLo &= ~(GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit); // read from pads
  GPIO.DirResetLo = (GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit); // inputs
  GPIO.DirSetLo = (GPIO_UART1_CTS_bit | GPIO_UART1_TX_bit);  // outputs

  tmpReg = GPIO.FuncSel0 & ~((FN_MASK << GPIO_UART1_RX_fnpos) | (FN_MASK << GPIO_UART1_TX_fnpos));
  GPIO.FuncSel0 = tmpReg | ((FN_ALT << GPIO_UART1_RX_fnpos) | (FN_ALT << GPIO_UART1_TX_fnpos));
  tmpReg = GPIO.FuncSel1 & ~((FN_MASK << GPIO_UART1_CTS_fnpos) | (FN_MASK << GPIO_UART1_RTS_fnpos));
  GPIO.FuncSel1 = tmpReg | ((FN_ALT << GPIO_UART1_CTS_fnpos) | (FN_ALT << GPIO_UART1_RTS_fnpos));
# else
  GPIO.PuSelLo |= GPIO_UART1_RX_bit;  // Pull-up select: UP type
  GPIO.PuEnLo  |= GPIO_UART1_RX_bit;  // Pull-up enable
  GPIO.InputDataSelLo &= ~GPIO_UART1_RX_bit; // read from pads
  GPIO.DirResetLo = GPIO_UART1_RX_bit; // inputs
  GPIO.DirSetLo = GPIO_UART1_TX_bit;  // outputs

  tmpReg = GPIO.FuncSel0 & ~((FN_MASK << GPIO_UART1_RX_fnpos) | (FN_MASK << GPIO_UART1_TX_fnpos));
  GPIO.FuncSel0 = tmpReg | ((FN_ALT << GPIO_UART1_RX_fnpos) | (FN_ALT << GPIO_UART1_TX_fnpos));

# endif
#else
  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_UART1);

  IOCPinConfigPeriphInput(GPIO_A_BASE, GPIO_PIN_0, IOC_UARTRXD_UART1);
  GPIOPinTypeUARTInput(GPIO_A_BASE, GPIO_PIN_0);

  IOCPinConfigPeriphOutput(GPIO_A_BASE, GPIO_PIN_1, IOC_MUX_OUT_SEL_UART1_TXD);
  GPIOPinTypeUARTOutput(GPIO_A_BASE, GPIO_PIN_1);

# if gUart1_EnableHWFlowControl_d == TRUE
  IOCPinConfigPeriphOutput(GPIO_A_BASE, GPIO_PIN_3, IOC_MUX_OUT_SEL_UART1_RTS);
  GPIOPinTypeUARTOutput(GPIO_A_BASE, GPIO_PIN_3); /* RTS */

  IOCPinConfigPeriphInput(GPIO_A_BASE, GPIO_PIN_2, IOC_UARTCTS_UART1);
  GPIOPinTypeUARTInput(GPIO_A_BASE, GPIO_PIN_2);  /* CTS */
# endif
#endif
}

/************************************************************************************
* MpGpioUart2Init
*
* This function initializate the gpio�s for the Uart2 module
*************************************************************************************/
void MpGpioUart2Init(void)
{
#if TARGET_BOARD != CC2538SF53
  register uint32_t tmpReg;

#if gUart2_EnableHWFlowControl_d == TRUE
  GPIO.PuSelLo |= (GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit);  // Pull-up select: UP type
  GPIO.PuEnLo  |= (GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit);  // Pull-up enable
  GPIO.InputDataSelLo &= ~(GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit); // read from pads
  GPIO.DirResetLo = (GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit); // inputs
  GPIO.DirSetLo = (GPIO_UART2_CTS_bit | GPIO_UART2_TX_bit);  // outputs

  tmpReg = GPIO.FuncSel1 & ~((FN_MASK << GPIO_UART2_CTS_fnpos) | (FN_MASK << GPIO_UART2_RTS_fnpos)\
  | (FN_MASK << GPIO_UART2_RX_fnpos) | (FN_MASK << GPIO_UART2_TX_fnpos));
  GPIO.FuncSel1 = tmpReg | ((FN_ALT << GPIO_UART2_CTS_fnpos) | (FN_ALT << GPIO_UART2_RTS_fnpos)\
  | (FN_ALT << GPIO_UART2_RX_fnpos) | (FN_ALT << GPIO_UART2_TX_fnpos));
#else
  // Don't care about RTS and CTS pins
  GPIO.PuSelLo |= GPIO_UART2_RX_bit;  // Pull-up select: UP type
  GPIO.PuEnLo  |= GPIO_UART2_RX_bit;  // Pull-up enable
  GPIO.InputDataSelLo &= ~GPIO_UART2_RX_bit; // read from pads
  GPIO.DirResetLo = GPIO_UART2_RX_bit; // inputs
  GPIO.DirSetLo = GPIO_UART2_TX_bit;  // outputs

  tmpReg = GPIO.FuncSel1 & ~((FN_MASK << GPIO_UART2_RX_fnpos) | (FN_MASK << GPIO_UART2_TX_fnpos));
  GPIO.FuncSel1 = tmpReg | ((FN_ALT << GPIO_UART2_RX_fnpos) | (FN_ALT << GPIO_UART2_TX_fnpos));

# endif

#else
  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_UART0);

  IOCPinConfigPeriphOutput(GPIO_C_BASE, GPIO_PIN_6, IOC_MUX_OUT_SEL_UART0_TXD);
  GPIOPinTypeUARTOutput(GPIO_C_BASE, GPIO_PIN_6);

  IOCPinConfigPeriphInput(GPIO_C_BASE, GPIO_PIN_7, IOC_UARTRXD_UART0);
  GPIOPinTypeUARTInput(GPIO_C_BASE, GPIO_PIN_7);

# if gUart2_EnableHWFlowControl_d == TRUE
#  error This UART does not support HW flow control
# endif

#endif
}

/************************************************************************************
* MpUart_Init
*
* Initializate the Uart module.
*************************************************************************************/
UartErr_t result;
void MpUart_Init(uint8_t *mUARTRxBuffer)
{
  UartConfig_t pConfig;
  UartCallbackFunctions_t pCallback;

  IntMasterDisable();

#if gMpSerialPort_c == UART_1
    result = UartGetStatus(UART_1);
    //Consult the current status of this serial port
    if( result != gUartErrUartNotOpen_c )
    {
      /* if the uart is already open then do nothing */
      return;
    }

    //initialize GPIOs for UART1
    MpGpioUart1Init();

    //configure the uart parameters
    pConfig.UartParity = gUartParityNone_c; // 0
    pConfig.UartStopBits = gUartStopBits1_c;  // 1
    pConfig.UartBaudrate = gMpSerialBaudrate_c;  // 38400
    pConfig.UartFlowControlEnabled = gMpUart_EnableHWFlowControl_d;  // FALSE
    pConfig.UartRTSActiveHigh = FALSE;

#if TARGET_BOARD != CC2538SF53
    //mount the interrupts corresponding to UART driver
    IntAssignHandler(gUart1Int_c, (IntHandlerFunc_t)UartIsr1);
    ITC_SetPriority(gUart1Int_c, gItcNormalPriority_c);
    //enable the interrupts corresponding to UART driver
    ITC_EnableInterrupt(gUart1Int_c);
#else
    IntPrioritySet(INT_UART1, 2<<5);
#endif

    //initialize the uart
    result = UartOpen(UART_1,gPlatformClock_c);

    if( result != gUartErrNoError_c )
      return;

    result = UartSetConfig(UART_1, &pConfig);

  #if gMpUart_EnableHWFlowControl_d == TRUE
    UartSetCTSThreshold(UART_1, gUart_RxFlowControlSkew_d);
  #endif

    //configure the Uart Rx and Tx Threshold
    result = UartSetTransmitterThreshold(UART_1, 12);
    result = UartSetReceiverThreshold(UART_1, 2);

    //set pCallback functions
    pCallback.pfUartWriteCallback = MpSerialTxIsr;
    pCallback.pfUartReadCallback = MpSerialRxIsr;
    result = UartSetCallbackFunctions(UART_1, &pCallback);
#endif

#if gMpSerialPort_c == UART_2
    result = UartGetStatus(UART_2);
    //Consult the current status of this serial port
    if( result != gUartErrUartNotOpen_c )
    {
      /* if the uart is already open then do nothing */
      return;
    }

    //initialize GPIOs for UART2
    MpGpioUart2Init();

    //configure the uart parameters
    pConfig.UartParity = gUartParityNone_c;
    pConfig.UartStopBits = gUartStopBits1_c;
    pConfig.UartBaudrate = gMpSerialBaudrate_c;
    pConfig.UartFlowControlEnabled = gMpUart_EnableHWFlowControl_d;
    pConfig.UartRTSActiveHigh = FALSE;

#if TARGET_BOARD != CC2538SF53
    //mount the interrupts corresponding to UART driver
    IntAssignHandler(gUart2Int_c, (IntHandlerFunc_t)UartIsr2);
    ITC_SetPriority(gUart2Int_c, gItcNormalPriority_c);
    //enable the interrupts corresponding to UART driver
    ITC_EnableInterrupt(gUart2Int_c);
#else
    IntPrioritySet(INT_UART0, 2<<5);
#endif

    //initialize the uart
    result = UartOpen(UART_2,gPlatformClock_c);

    if( result != gUartErrNoError_c )
      return;

    result = UartSetConfig(UART_2,&pConfig);

  #if gMpUart_EnableHWFlowControl_d == TRUE
    UartSetCTSThreshold(UART_2, gUart_RxFlowControlSkew_d);
  #endif

    //configure the Uart Rx and Tx Threshold
    result = UartSetTransmitterThreshold(UART_2,12);
    result = UartSetReceiverThreshold(UART_2,2);

    //set pCallback functions
    pCallback.pfUartWriteCallback = MpSerialTxIsr;
    pCallback.pfUartReadCallback = MpSerialRxIsr;
    result = UartSetCallbackFunctions(UART_2,&pCallback);
#endif

  // global enable interrupts in AITC driver
#if TARGET_BOARD != CC2538SF53
  IntEnableIRQ();
#else
  IntMasterEnable();
#endif

  result = UartReadData(gMpSerialPort_c,mUARTRxBuffer,sizeof(mUARTRxBuffer),TRUE);
}

/*
  MpSerialTxIsr1

  UART Transmit ISR. Called on Tx interrupt - data register empty. Data added
  to head, removed from tail.
*/
void MpSerialTxIsr(UartWriteCallbackArgs_t* args)
{
  // send next byte to UART
  if(args->UartStatus == gUartWriteStatusComplete_c )
  {
    gu16SCINumOfBytes = args->UartNumberBytesSent;

    gfMpSerialStatus |= gMpSerTxDone_c;

    /* Tell the Serial Task that the transmission has been completed.
       This is optional */
   (void)PopSetEventEx(cMpSerialTaskId, gMpSerTxDone_c, NULL);
  }

}

/*
  MpSerialRxIsr

  UART Receive ISR. Called on Rx interrupt. Data added to head, removed from
  tail.
*/
void MpSerialRxIsr(UartReadCallbackArgs_t* args)
{
  uint8_t TestBuff;

  gu8SCIDataFlag = TRUE;

  gu8SCIStatus = args->UartStatus;

  if( gu8SCIStatus == gUartReadStatusComplete_c )
  {
    gu16SCINumOfBytes = args->UartNumberBytesReceived;

    UartGetByteFromRxBuffer(gMpSerialPort_c,&TestBuff );

    *gpMpSerialRxBufHead++ = TestBuff;

    // head has wrapped in ring buffer
    if ( gpMpSerialRxBufHead >= (gaMpSerialRxBuf + gMpSerialRxBufferSize_c) )
      gpMpSerialRxBufHead = gaMpSerialRxBuf;

    // if overrun, set (sticky) flag to indicate such
    if ( gpMpSerialRxBufHead == gpMpSerialRxBufTail )
      gfMpSerialStatus |= gMpSerRxOverflow_c;

    // indicate byte received to appropriate task
    gfMpSerialStatus |= gMpSerRxData_c;

  }

  // reenable interupts. We had an error
  else if( gu8SCIStatus == gUartReadStatusError_c)
  {
    /*
      Disabling Rx interrupt corresponding to UART driver. If we disable global interruptions then we
      will be affecting the other interrupts.
    */
    UartCloseReceiver(gMpSerialPort_c);

    // Clear uart status error
    UartClearErrors(gMpSerialPort_c);

    // Read the data stored in Rx buffer at the moment of the error.
    while( UartGetByteFromRxBuffer(gMpSerialPort_c,&TestBuff ) );

    /*
      Enabling Rx interrupt corresponding to UART driver. If we disable global interruptions then we
      will be affecting the other interrupts.
    */
    UartOpenReceiver(gMpSerialPort_c);

    // Clear the args status.
    args->UartReadError.UartReadOverrunError = 0;
    args->UartReadError.UartParityError = 0;
    args->UartReadError.UartFrameError = 0;
    args->UartReadError.UartStartBitError = 0;
    args->UartStatus = gUartReadStatusComplete_c;
    args->UartNumberBytesReceived = 0;

  }

}


popErr_t MpSerialSend( uint8_t iLen, void *pBuffer)
{
  // no data, success
  if( !iLen )
    return gPopErrNone_c;

  // no buffer, invalid argument
  if ( !pBuffer )
    return gMpSerInvalidArg_c;

  // data too large for transmit buffer, give up
  if ( iLen > gMpSerialTxBufferSize_c )
    return gMpSerTooBig_c;

  /* If there is a write ongoing return busy */
  if (UartGetStatus(gMpSerialPort_c)== gUartErrWriteOngoing_c)
    return gMpSerBusy_c;

  UartWriteData(gMpSerialPort_c,pBuffer,iLen);

  return gPopErrNone_c;
}

/*
  MpSerialReceive



  paraeters:
    piLen:   [in/out]   in: length of app's input buffer.
                        out: n. bytes received to buffer, or on error n. bytes required.
    pBuffer: [in]       pointer to application receive buffer.

    return values:
    gPopErrNone_c     successful receive to application buffer
    gPopErrBadParm_c  invalid parameter

    side effects:
    on successful receive to app buffer, clears the gMpSerRxData_c status flag.
*/
popErr_t MpSerialReceive(uint8_t *piLen, void *pBuffer)
{
  uint8_t * pRxHead;
  uint8_t * pRxTail;
  uint8_t  iChunkData1;
  uint8_t  iChunkData2;

  // critical section, disable ints
  PopSerialSaveAndDisableTxRxInts();

  // no longer have data (must disable both tx and rx which can affect this flag
  gfMpSerialStatus &= ~gMpSerRxData_c;

  // grab head and tail for comparison (must disable ints because rx int will affect tail
  pRxHead = gpMpSerialRxBufHead;
  pRxTail = gpMpSerialRxBufTail;

  PopSerialEnableRxInts();

  // check input parameters (but only if not trying to keep size small
  if ( !piLen || !*piLen || !pBuffer )
    return gMpSerInvalidArg_c;

  // no data. this case should not actually happen...
  if ( pRxTail == pRxHead )
  {
    *piLen = 0;
    return gPopErrNone_c;
  }

  // data is in one chunk...
  iChunkData1 = (uint8_t)(pRxHead - pRxTail);
  iChunkData2 = 0;
  if(pRxTail >= pRxHead)  // or two
  {
      iChunkData1 = (uint8_t)(gaMpSerialRxBuf + gMpSerialRxBufferSize_c - pRxTail);
      iChunkData2 = (uint8_t)(pRxHead - gaMpSerialRxBuf);
  }

  // application may not want all of the data (if small buffer size)
  if ( *piLen < iChunkData1 )
  {
    iChunkData1 = *piLen;
    iChunkData2 = 0;
  }
  else if(*piLen < iChunkData1 + iChunkData2)
    iChunkData2 = *piLen - iChunkData1;

  // copy data to app buffer in one chunk...
  if ( iChunkData1 )
    PopMemCpy( pBuffer, pRxTail, iChunkData1 );
  if ( iChunkData2 )  // or two
    PopMemCpy( (uint8_t *)pBuffer + iChunkData1, gaMpSerialRxBuf, iChunkData2 );

  // tell application how many bytes received
  *piLen = iChunkData1 + iChunkData2;

  // disable rx ints during tail update...
  PopSerialDisableRxInts();

  // data added to tail, update tail
  if ( !iChunkData2 )
    gpMpSerialRxBufTail += iChunkData1;
  else
    gpMpSerialRxBufTail = gaMpSerialRxBuf + iChunkData2;

  // tail wrapped
  if ( gpMpSerialRxBufTail >= gaMpSerialRxBuf + gMpSerialRxBufferSize_c )
    gpMpSerialRxBufTail = gaMpSerialRxBuf;

  // enable interrupts for serial receive
  PopSerialEnableRxInts();

  // let caller know it was successful
  return gPopErrNone_c;
}

void MpSerialTestRadioTxRxTest(uint8_t cmd)
{
  //static uint8_t repeatResponse = 0;
  uint8_t SerialResponse[12] = {0,0,0,0,0,0,0,0,0,0,0,0};

  switch(cmd)
  {
    case gTestStart:
      // Start test by sending toggle light command
      MpSerialTestToggleLight();
    break;

    case gTestDone:
      // Test succesfull
      // Build serial response
      SerialResponse[0] = 0x0C;   // Cmd
      for(uint8_t i = 1; i < 11; i++){
        SerialResponse[i] = gLQI; // LQI
      }
      SerialResponse[11] = 0xFF;  // End of Test
      // Send serial response
      MpSerialSend(12, &SerialResponse);

      // Reset Test mode and state and stop timeout timer
      gTestMode = 0;
      (void)PopStopTimerEx(cMpSerialTaskId, gMpSerialTimerTestTimeout_c);
    break;
  }
}

/*
  Toggle a remote light
*/
void MpSerialTestToggleLight(void)
{
  sPopNwkDataRequest_t sDataReq;
  uint8_t cCmdId = gMpSerialCmdToggleLight_c;

  // set up the data request
  sDataReq.iDstAddr = giAppRemoteAddr;
  sDataReq.iOptions = gPopNwkDataReqOptsNoDiscover_c;
  sDataReq.iRadius = PopNwkGetDefaultRadius();
  sDataReq.iPayloadLength = sizeof(cCmdId);
  sDataReq.pPayload = &cCmdId;

  // send the toggle light command over-the-air
  MpSerialTestSendAckedPacket(&sDataReq);
}

/*
  Send an end-to-end acknowledged data request
*/
void MpSerialTestSendAckedPacket(sPopNwkDataRequest_t *pDataReq)
{
  // in case user presses button twice quickly
  if(gpPopAppPacket)
    PopMemFree(gpPopAppPacket);

  // make a copy of the packet for resending
  giPopAppPacketLen = pDataReq->iPayloadLength;
  gpPopAppPacket = PopMemAlloc(giPopAppPacketLen);

  // can only resend if we can make a copy
  if(gpPopAppPacket)
  {
    PopMemCpy(gpPopAppPacket, pDataReq->pPayload, giPopAppPacketLen);

    // timer code and ACK hanlding code will free memory
    (void)PopStartTimerEx(cMpSerialTaskId, gMpSerialTimerToggleReply_c, PopNwkAppEndToEndAckTimeout());
  }

  // send the data out the radio
  PopNwkDataRequest(pDataReq);
}

/*
  timed out, time to retry
*/
void MpSerialTestRetryPacket(void)
{
  sPopNwkDataRequest_t sDataReq;

  // prepare the data request
  sDataReq.iDstAddr = giAppRemoteAddr;
  sDataReq.iOptions = gPopNwkDataReqOptsNoDiscover_c;
  sDataReq.iRadius = PopNwkGetDefaultRadius();
  sDataReq.iPayloadLength = giPopAppPacketLen;
  sDataReq.pPayload = gpPopAppPacket;

  // send the data over-the-air
  PopNwkDataRequest(&sDataReq);

  // free the packet
  (void)PopMemFree(gpPopAppPacket);
  gpPopAppPacket = NULL;
}

void MpSerialTestReceivePacket(sPopNwkDataIndication_t *pIndication)
{
  uint8_t *pPayload = PopNwkPayload(pIndication);
  uint8_t iPayloadLen = PopNwkPayloadLen(pIndication);
  uint8_t cCmd = *pPayload;

  // get pointer and size to application data payload
  if(!iPayloadLen)
    return;

  // got the application end-to-end acknowledgement, no need to retry
  if(cCmd == gMpSerialCmdToggleAck_c)
  {
    (void)PopStopTimerEx(cMpSerialTaskId, gMpSerialTimerToggleReply_c);
    (void)PopMemFree(gpPopAppPacket);
    gpPopAppPacket = NULL;
  }

  // got the delayed response,
  if(cCmd == gMpSerialCmdResponse_c)
  {
    if(gTestMode == 0x0C){
      // Get LQI and send via serial comm to test PC
      gLQI = PopNwkLqi(pIndication);
      MpSerialTestRadioTxRxTest(gTestDone);
    }
  }
}

void MpSerialStartNetworkResponse(popStatus_t iNwkStartConfirm)
{
  uint8_t aSerialTxData[gMpSerialTxBufferSize_c];
  uint8_t cmd = 0x15;
  uint8_t status = 0xFF;
  popChannel_t iChannel = gsPopNwkData.iChannel;
  popPanId_t iPanId = gsPopNwkData.iPanId;
  popNwkAddr_t iNodeAddr = gsPopNwkData.iNodeAddr;

  // Check if network has started
  switch(iNwkStartConfirm){
    case gPopStatusSilentStart_c:
    case gPopStatusAlreadyOnTheNetwork_c:
      status = 0; // Success
      break;

    default:
      status = 0xFF; // Fail
      break;
  }

  /* Send response */
  aSerialTxData[0] = cmd;
  PopMemCpy(&aSerialTxData[1], &status, 1);
  PopMemCpy(&aSerialTxData[2], &iChannel, 1);
  NativeToGateway16(&iPanId);
  PopMemCpy(&aSerialTxData[3], &iPanId, 2);
  NativeToGateway16(&iNodeAddr);
  PopMemCpy(&aSerialTxData[5], &iNodeAddr, 2);

  MpSerialSend(7, aSerialTxData);
}

void MpSerialHandler(void)
{
  uint8_t aSerialRxData[gMpSerialRxBufferSize_c];
  uint8_t aSerialTxData[gMpSerialTxBufferSize_c];
  uint8_t iLen;

  uint8_t cmd;
  uint8_t aMACAddr[8];
  uint8_t iCoarseTune;
  uint8_t iFineTune;
  uint8_t iTxPowerLevel;
  popErr_t iErr;
  popChannel_t iDefaultChannel;
  popPanId_t iDefaultPanId;
  popNwkAddr_t iDefaultNodeAddr;
  uint32_t iManufacturerAndDeviceId;

  iLen = sizeof(aSerialRxData);
  MpSerialReceive(&iLen,aSerialRxData);
  cmd = aSerialRxData[0];

  switch(cmd)
  {
        //Switch options
     case 0x01:
        // Set Q2 to ON (EINT0 on LPC)
        GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, GPIO_PIN_4);

        // Send response
        aSerialTxData[0] = cmd;
        MpSerialSend(1, aSerialTxData);
        break;

     case 0x02:
        // Set Q2 to OFF (EINT0 on LPC)
        GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, 0);

        // Send response
        aSerialTxData[0] = cmd;
        MpSerialSend(1, aSerialTxData);
        break;

     case 0x03:
        // Set Q3 to ON (RESET on LPC)
        GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, GPIO_PIN_5);

        // Send response
        aSerialTxData[0] = cmd;
        MpSerialSend(1, aSerialTxData);
        break;

     case 0x04:
        // Set Q3 to ON (RESET on LPC)
        GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, 0);

        // Send response
        aSerialTxData[0] = cmd;
        MpSerialSend(1, aSerialTxData);
        break;

     case 0x0A:
      /* Set crystal coarse tune */
      iCoarseTune = aSerialRxData[1];
#if TARGET_BOARD != CC2538SF53
      set_xtal_coarse_tune(iCoarseTune);
#else
      phyPLMEXtalAdjust(iCoarseTune, 0xFF);
#endif
      PopNvStore(gPopAppNvCoarseTune_c, 1, &iCoarseTune);

      /* Send response */
      aSerialTxData[0] = cmd;
      aSerialTxData[1] = iCoarseTune;
      MpSerialSend(2, aSerialTxData);
      break;

    case 0x0B:
      /* Set crystal fine tune */
      iFineTune = aSerialRxData[1];
#if TARGET_BOARD != CC2538SF53
      set_xtal_fine_tune(iFineTune);
#else
      phyPLMEXtalAdjust(0xFF, iFineTune);
#endif
      PopNvStore(gPopAppNvFineTune_c, 1, &iFineTune);

      /* Send response */
      aSerialTxData[0] = cmd;
      aSerialTxData[1] = iFineTune;
      MpSerialSend(2, aSerialTxData);
      break;

    case 0x0C:
      // LQI test
      // start test
      MpSerialTestRadioTxRxTest(gTestStart);
      // Set active test mode
      gTestMode = 0x0C;
      // Start timeout timer and wait for data indication response
      PopStartTimerEx(cMpSerialTaskId, gMpSerialTimerTestTimeout_c, 10000);
      break;

    case 0x0D:
      /* Set MAC address */
      /* Store data to NVM */
      PopMemCpy(&aMACAddr[0], &aSerialRxData[1], 8);
      PopNwkSetMacAddr(aMACAddr);
      (void)PopNvStoreNwkData();

      /* verify data stored to NVM */
      iErr = PopNvRetrieveNwkData();
      if(iErr == gPopErrNone_c){
        //copy MAC addr from global struct
          PopMemCpy(&aSerialTxData[1], PopNwkGetMacAddrPtr(), 8);
      }
      else{
        //fill response with 0xFFs
        for(int i=1;i<9;i++){
            aSerialTxData[i] = 0xFF;
          }
      }

      /* Send response */
      aSerialTxData[0] = cmd;
      MpSerialSend(9, aSerialTxData);
      break;

    case 0x10:
      /* Set default node address */
      PopMemCpy(&iDefaultNodeAddr, &aSerialRxData[1], 2);
      GatewayToNative16(&iDefaultNodeAddr);
      /* Check if valid data */
      if(iDefaultNodeAddr > 0 && iDefaultNodeAddr < 0xFE00){
        /* Store data to NVM */
        PopNvStore(gPopAppNvDefaultNodeAddr_c, 2, &iDefaultNodeAddr);

        /* verify data stored to NVM */
        iErr = PopNvRetrieve(gPopAppNvDefaultNodeAddr_c, &iDefaultNodeAddr);
        if(iErr != gPopErrNone_c){
          iDefaultNodeAddr = gPopNwkInvalidAddr_c;
        }
      }
      else if(iDefaultNodeAddr == 0){
        /* Read data stored in NVM */
        iErr = PopNvRetrieve(gPopAppNvDefaultNodeAddr_c, &iDefaultNodeAddr);
        if(iErr != gPopErrNone_c){
          iDefaultNodeAddr = gPopNwkInvalidAddr_c;
        }
      }
      else{
        iDefaultNodeAddr = gPopNwkInvalidAddr_c;
      }

      /* Send response */
      aSerialTxData[0] = cmd;
      NativeToGateway16(&iDefaultNodeAddr);
      PopMemCpy(&aSerialTxData[1], &iDefaultNodeAddr, 2);
      MpSerialSend(3, aSerialTxData);
      break;

    case 0x11:
      /* Set Tx power level */
      /* Check if valid data */
      PopMemCpy(&iTxPowerLevel, &aSerialRxData[1], 1);
      switch(iTxPowerLevel)
      {
        case gPopNwkTxPower_Maximum_c:
        case gPopNwkTxPower_Normal_c:
        case gPopNwkTxPower_Medium_c:
        case gPopNwkTxPower_Mininum_c:
          /* Store power level to NVM*/
          PopNvStore(gPopAppNvTxPowerLevel_c, 1, &iTxPowerLevel);

          /* verify data stored to NVM */
          iErr = PopNvRetrieve(gPopAppNvTxPowerLevel_c, &iTxPowerLevel);
          if(iErr == gPopErrNone_c){
            /* Set power level */
            PopNwkSetTransmitPower(iTxPowerLevel);
          }
          else{
            iTxPowerLevel = 0xFF;
          }
          break;

        case 0:
          /* Read data stored to NVM */
          iErr = PopNvRetrieve(gPopAppNvTxPowerLevel_c, &iTxPowerLevel);
          if(iErr != gPopErrNone_c){
            iTxPowerLevel = 0xFF;
          }
          break;

        default:
          iTxPowerLevel = 0xFF;
          break;
      }

      /* Send response */
      aSerialTxData[0] = cmd;
      PopMemCpy(&aSerialTxData[1], &iTxPowerLevel, 1);
      MpSerialSend(2, aSerialTxData);
      break;

    case 0x12:
        /* Read Device Id from external flash */
        /* data read out at startup initialization of external flash */
        iManufacturerAndDeviceId = gManufacturerAndDeviceId;

        // Send response
        aSerialTxData[0] = cmd;
        NativeToGateway32(&iManufacturerAndDeviceId);
        PopMemCpy(&aSerialTxData[1], &iManufacturerAndDeviceId, 4);
        MpSerialSend(5, aSerialTxData);
        break;

    case 0x13:
      /* Set default channel */
      PopMemCpy(&iDefaultChannel, &aSerialRxData[1], 1);

      /* Check if valid data */
      if(iDefaultChannel >= 11 && iDefaultChannel <= 26){
        /* Store data to NVM */
        PopNvStore(gPopAppNvDefaultChannel_c, 1, &iDefaultChannel);

        /* verify data stored to NVM */
        iErr = PopNvRetrieve(gPopAppNvDefaultChannel_c, &iDefaultChannel);
        if(iErr != gPopErrNone_c){
          iDefaultChannel = gPopNwkInvalidChannel_c;
        }
      }
      else if(iDefaultChannel == 0){
        /* Read data stored to NVM */
        iErr = PopNvRetrieve(gPopAppNvDefaultChannel_c, &iDefaultChannel);
        if(iErr != gPopErrNone_c){
          iDefaultChannel = gPopNwkInvalidChannel_c;
        }
      }
      else{
        iDefaultChannel = gPopNwkInvalidChannel_c;
      }

      /* Send response */
      aSerialTxData[0] = cmd;
      PopMemCpy(&aSerialTxData[1], &iDefaultChannel, 1);
      MpSerialSend(2, aSerialTxData);
      break;

    case 0x14:
      /* Set default PAN Id */
      PopMemCpy(&iDefaultPanId, &aSerialRxData[1], 2);
      GatewayToNative16(&iDefaultPanId);
      /* Check if valid data */
      if(iDefaultPanId > 0 && iDefaultPanId < gPopNwkReservedPan_c){
        /* Store data to NVM */
        PopNvStore(gPopAppNvDefaultPanId_c, 2, &iDefaultPanId);

        /* verify data stored to NVM */
        iErr = PopNvRetrieve(gPopAppNvDefaultPanId_c, &iDefaultPanId);
        if(iErr != gPopErrNone_c){
          iDefaultPanId = gPopNwkInvalidPan_c;
        }
      }
      else if(iDefaultPanId == 0){
        /* Read data stored to NVM */
        iErr = PopNvRetrieve(gPopAppNvDefaultPanId_c, &iDefaultPanId);
        if(iErr != gPopErrNone_c){
          iDefaultPanId = gPopNwkInvalidPan_c;
        }
      }
      else{
        iDefaultPanId = gPopNwkInvalidPan_c;
      }

      /* Send response */
      aSerialTxData[0] = cmd;
      NativeToGateway16(&iDefaultPanId);
      PopMemCpy(&aSerialTxData[1], &iDefaultPanId, 2);
      MpSerialSend(3, aSerialTxData);
      break;

    case 0x15:
      /* Start default network */
      PopNvRetrieve(gPopAppNvDefaultChannel_c, &iDefaultChannel);
      PopNvRetrieve(gPopAppNvDefaultPanId_c, &iDefaultPanId);
      PopNvRetrieve(gPopAppNvDefaultNodeAddr_c, &iDefaultNodeAddr);

      // Perform silent start with default nwk data parameters
      PopNwkSetChannel(iDefaultChannel);
      PopNwkSetPanId(iDefaultPanId);
      PopNwkSetNodeAddr(iDefaultNodeAddr);
      PopNwkStartNetwork();

      // Start timeout timer and wait for data indication response
      giState = TEST;
      gTestMode = 0x15;
      PopStartTimerEx(cMpSerialTaskId, gMpSerialTimerTestTimeout_c, 10000);
      break;
  }
}
#endif //gUseMpSerial_d