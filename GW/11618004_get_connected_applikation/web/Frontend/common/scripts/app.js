$(function() 
{
    'use strict';

    /* ECMAScript 6 is not supported by all browsers, so we "mock" that we create a real constant */
    var DEFAULT_PAGE_ON_LOAD = 'statistic';

    // Cache jQuery objects.
    var currentPage;
    var page  = $('ul#page');
    var pages = page.children();

    var wirelessLinkQualityGauge = new JustGage({id: "wirelessLinkQualityGauge", min: 0, max: 100, value: 0, label: "%"});
    var wirelessSignalStrengthGauge = new JustGage({id: "wirelessSignalStrengthGauge", min: -100, max: 0, value: -100, label: "dBm"});

    //var uri = 'ws://127.0.0.1:8080';
    var uri = 'ws://' + location.host + ':8080';

    var ethernetEditMode = "false";
    var ethernetStaticEdit = "false";

    function connectWebSocket()
    {
        var socket = new WebSocket(uri);
        socket.onclose = function()
        {
            console.error('Web channel closed, reconnecting in 5 seconds');
            setTimeout(function()
            {
                connectWebSocket();
            }, 5000);
        };

        socket.onerror = function(error)
        {
            console.error('Web channel error: ' + error);
        };

        socket.onopen = function()
        {
            window.channel = new QWebChannel(socket, function(channel)
            {
                //
                // Node backend.
                //
                channel.objects.node.foundNodesChanged.connect(function()
                {
                    $('#foundNodes tr:not(:first)').remove();
                    channel.objects.node.foundNodes.forEach(function(data)
                    {
                        var node = JSON.parse(data);
                        if ($('#foundNodes').find('#' + node.channel + "," + node.panId + "," + node.address).length === 0)
                        {
                            var productType;
                            if (node.productType === 1) // BMU
                                productType = '<em class="fa fa-battery-half"></em>'
                            else if (node.productType === 3) // Charger
                                productType = '<em class="fa fa-flash"></em>'
                            else // Unknown
                                productType = '';

                            $('#foundNodes tr:last').after('<tr id="' + node.channel + "," + node.panId + "," + node.address + '">' +
                                                               '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.nodeId + '</td>' +
                                                               '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.channel + '</td>' +
                                                               '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.panId + '</td>' +
                                                               '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.address + '</td>' +
                                                               '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.timestamp + '</td>' +
                                                               '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + productType + '</td>' +
                                                               //'<td class="h2" align="center" style="padding-right: 2em; padding-left: 2em; padding-top: 0.2em; padding-bottom: 0.5em;"><label class="switch"><input type="checkbox" checked /><span class="slider round"></span></label></td>' +
                                                           '</tr>');
                        }
                    });
                });
                channel.objects.node.foundNodesCountChanged.connect(function()
                {
                    $('#foundNodesCount').text(channel.objects.node.foundNodesCount);
                });

                $('#foundNodes tr:not(:first)').remove();
                channel.objects.node.foundNodes.forEach(function(data)
                {
                    var node = JSON.parse(data);
                    if ($('#foundNodes').find('#' + node.channel + "," + node.panId + "," + node.address).length === 0)
                    {
                        var productType;

                        if (node.productType === 1) // BMU
                            productType = '<em class="fa fa-battery-half"></em>'
                        else if (node.productType === 3) // Charger
                            productType = '<em class="fa fa-flash"></em>'
                        else // Unknown
                            productType = '';

                        $('#foundNodes tr:last').after('<tr id="' + node.channel + "," + node.panId + "," + node.address + '">' +
                                                           '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.nodeId + '</td>' +
                                                           '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.channel + '</td>' +
                                                           '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.panId + '</td>' +
                                                           '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.address + '</td>' +
                                                           '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + node.timestamp + '</td>' +
                                                           '<td class="h2" align="center" valign="top" style="padding-right: 2em; padding-left: 2em; padding-bottom: 0.5em;">' + productType + '</td>' +
                                                           //'<td class="h2" align="center" style="padding-right: 2em; padding-left: 2em; padding-top: 0.2em; padding-bottom: 0.5em;"><label class="switch"><input type="checkbox" checked /><span class="slider round"></span></label></td>' +
                                                       '</tr>');
                    }
                });
                $('#foundNodesCount').text(channel.objects.node.foundNodesCount);

                //
                // Network backend.
                //                
                channel.objects.network.ethernetMacChanged.connect(function()
                {                    
                    $('#ethernetMac').val(channel.objects.network.ethernetMac);
                    if (channel.objects.network.ethernetMac !== "")
                        //$('#ethernetStatic').show();
                        $('#editEthernetSettings').show();
                    else
                        //$('#ethernetStatic').hide();
                        $('#editEthernetSettings').hide();
                });
                channel.objects.network.ethernetAddressChanged.connect(function()
                {
                    if (ethernetEditMode === "false")
                    {
                        $('#ethernetAddress').prev('input').val(channel.objects.network.ethernetAddress);
                    }
                });
                channel.objects.network.ethernetNetmaskChanged.connect(function()
                {
                    if (ethernetEditMode === "false")
                    {
                        $('#ethernetNetmask').prev('input').val(channel.objects.network.ethernetNetmask);
                    }
                });
                channel.objects.network.ethernetGatewayChanged.connect(function()
                {
                    if (ethernetEditMode === "false")
                    {
                        $('#ethernetGateway').prev('input').val(channel.objects.network.ethernetGateway);
                    }
                });                
                channel.objects.network.ethernetStaticChanged.connect(function()
                {
                    if (ethernetEditMode === "false")
                    {
                        $('#ethernetStatic').prev('input').val(channel.objects.network.ethernetStatic);
                        //$('#editEthernetSettings').hide();
                    }
                });

                $('#ethernetMac').val(channel.objects.network.ethernetMac);
                $('#ethernetAddress').prev('input').val(channel.objects.network.ethernetAddress);
                $('#ethernetNetmask').prev('input').val(channel.objects.network.ethernetNetmask);
                $('#ethernetGateway').prev('input').val(channel.objects.network.ethernetGateway);

                //if (channel.objects.network.ethernetMac !== "")
                //    $('#ethernetStatic').show();
                //else
                    $('#ethernetStatic').hide();

                $('#ethernetAddress').hide();
                $('#ethernetNetmask').hide();
                $('#ethernetGateway').hide();

                $('#editEthernetSettingsCancel').hide();
                $('#editEthernetSettingsStore').hide();

                $('#ethernetStatic').prev('input').val(channel.objects.network.ethernetStatic);
//                if (channel.objects.network.ethernetStatic === "true")
//                {
//                    $('#ethernetAddress').show();
//                    $('#ethernetNetmask').show();
//                    $('#ethernetGateway').show();
//                    ethernetStaticEdit = "true";
//                }
//                else
//                {
//                    $('#ethernetAddress').hide();
//                    $('#ethernetNetmask').hide();
//                    $('#ethernetGateway').hide();
//                    ethernetStaticEdit = "false";
//                }

                channel.objects.network.wirelessMacChanged.connect(function()
                {
                    $('#wirelessMac').val(channel.objects.network.wirelessMac);
                    if (channel.objects.network.wirelessMac !== "")
                    {
                        $('#wirelessSsid').show();
                        $('#wirelessPassword').show();
                    }
                    else
                    {
                        $('#wirelessSsid').hide();
                        $('#wirelessPassword').hide();
                    }
                });
                channel.objects.network.wirelessLinkQualityChanged.connect(function()
                {
                    $('#wirelessLinkQuality').val(channel.objects.network.wirelessLinkQuality);
                    wirelessLinkQualityGauge.refresh(channel.objects.network.wirelessLinkQuality);
                });
                channel.objects.network.wirelessLinkQualityDescriptionChanged.connect(function()
                {
                    wirelessLinkQualityGauge.refreshTitle(channel.objects.network.wirelessLinkQualityDescription);
                });
                channel.objects.network.wirelessSignalStrengthChanged.connect(function()
                {
                    $('#wirelessSignalStrength').val(channel.objects.network.wirelessSignalStrength);
                    wirelessSignalStrengthGauge.refresh(channel.objects.network.wirelessSignalStrength);
                });
                channel.objects.network.wirelessSignalStrengthDescriptionChanged.connect(function()
                {
                    wirelessSignalStrengthGauge.refreshTitle(channel.objects.network.wirelessSignalStrengthDescription);
                });
                channel.objects.network.wirelessAddressChanged.connect(function()
                {
                    $('#wirelessAddress').val(channel.objects.network.wirelessAddress);
                });
                channel.objects.network.wirelessNetmaskChanged.connect(function()
                {
                    $('#wirelessNetmask').val(channel.objects.network.wirelessNetmask);
                });
                channel.objects.network.wirelessSsidChanged.connect(function()
                {
                    $('#wirelessSsid').prev('input').val(channel.objects.network.wirelessSsid);
                });
                channel.objects.network.wirelessPasswordChanged.connect(function()
                {
                    $('#wirelessPassword').prev('input').val(channel.objects.network.wirelessPassword);
                });

                $('#wirelessMac').val(channel.objects.network.wirelessMac);
                $('#wirelessLinkQuality').val(channel.objects.network.wirelessLinkQuality);
                $('#wirelessSignalStrength').val(channel.objects.network.wirelessSignalStrength);
                $('#wirelessAddress').val(channel.objects.network.wirelessAddress);
                $('#wirelessNetmask').val(channel.objects.network.wirelessNetmask);
                $('#wirelessSsid').prev('input').val(channel.objects.network.wirelessSsid);
                $('#wirelessPassword').prev('input').val(channel.objects.network.wirelessPassword);

                wirelessLinkQualityGauge.refresh(channel.objects.network.wirelessLinkQuality);
                wirelessLinkQualityGauge.refreshTitle(channel.objects.network.wirelessLinkQualityDescription);
                wirelessSignalStrengthGauge.refresh(channel.objects.network.wirelessSignalStrength);
                wirelessSignalStrengthGauge.refreshTitle(channel.objects.network.wirelessSignalStrengthDescription);

                if (channel.objects.network.wirelessMac !== "")
                {
                    $('#wirelessSsid').show();
                    $('#wirelessPassword').show();
                }
                else
                {
                    $('#wirelessSsid').hide();
                    $('#wirelessPassword').hide();
                }

                //
                // System backend.
                //
                channel.objects.system.deviceIdChanged.connect(function()
                {
                    $('#deviceId').prev('input').val(channel.objects.system.deviceId);
                });
                channel.objects.system.canBitRateChanged.connect(function()
                {
                    $('#canBitRate').prev('input').val(channel.objects.system.canBitRate);
                });
                channel.objects.system.serialNumberChanged.connect(function()
                {
                    $('#serialNumber').prev('input').val(channel.objects.system.serialNumber);
                });
                channel.objects.system.softwareTypeChanged.connect(function()
                {
                    $('#sowftwareType').val(channel.objects.system.softwareType);
                });
                channel.objects.system.softwareVersionChanged.connect(function()
                {
                    $('#vVersion').val(channel.objects.system.softwareVersion);
                });
                channel.objects.system.firmwareTypeChanged.connect(function()
                {
                    $('#firmwareType').val(channel.objects.system.firmwareType);
                });
                channel.objects.system.firmwareVersionChanged.connect(function()
                {
                    $('#firmwareVersion').val(channel.objects.system.firmwareVersion);
                });
                channel.objects.system.firmwareAddressChanged.connect(function()
                {
                    $('#firmwareAddress').val(channel.objects.system.firmwareAddress);
                });
                channel.objects.system.rootfsReleaseChanged.connect(function()
                {
                    $('#rootfsRelease').val(channel.objects.system.rootfsRelease);
                });
                channel.objects.system.muiChanged.connect(function()
                {
                    $('#mui').val(channel.objects.system.mui);
                });
                channel.objects.system.securityCodeChanged.connect(function()
                {
                    $('#securityCode').prev('input').val(channel.objects.system.securityCode);
                });
                channel.objects.system.factoryResetChanged.connect(function()
                {
                    $('#factoryReset').prev('input').val(channel.objects.system.factoryReset);
                });
                channel.objects.system.nameChanged.connect(function()
                {
                    $('#name').prev('input').val(channel.objects.system.name);
                });
                channel.objects.system.activityChanged.connect(function()
                {
                    $('#activity').text(channel.objects.system.activity);
                });
                channel.objects.system.buildCommitChanged.connect(function()
                {
                    $('#buildCommit').val(channel.objects.system.buildCommit);
                });
                channel.objects.system.dateChanged.connect(function()
                {
                    $('#date').prev('input').val(channel.objects.system.date);
                });
                channel.objects.system.timeChanged.connect(function()
                {
                    $('#time').prev('input').val(channel.objects.system.time);
                });

                channel.objects.system.cloudUrlChanged.connect(function()
                {
                    $('#cloudUrl').prev('input').val(channel.objects.system.cloudUrl);
                });
                channel.objects.system.cloudUsernameChanged.connect(function()
                {
                    $('#cloudUsername').prev('input').val(channel.objects.system.cloudUsername);
                });
                channel.objects.system.cloudPasswordChanged.connect(function()
                {
                    $('#cloudPassword').prev('input').val(channel.objects.system.cloudPassword);
                });

                channel.objects.system.freeStorageChanged.connect(function()
                {
                    $('#freeStorage').val(channel.objects.system.freeStorage + ' MB');
                });
                channel.objects.system.totalStorageChanged.connect(function()
                {
                    $('#totalStorage').val(channel.objects.system.totalStorage + ' MB');
                });
                channel.objects.system.freeStoragePercentChanged.connect(function()
                {
                    $('#freeStoragePercent').text(channel.objects.system.freeStoragePercent);
                });

                channel.objects.system.scanNetworkIntervalChanged.connect(function()
                {
                    $('#scanNetworkInterval').prev('input').val(channel.objects.system.scanNetworkInterval);
                });
                channel.objects.system.collectDataIntervalChanged.connect(function()
                {
                    $('#collectDataInterval').prev('input').val(channel.objects.system.collectDataInterval);
                });
                channel.objects.system.removeNodeIntervalChanged.connect(function()
                {
                    $('#removeNodeInterval').prev('input').val(channel.objects.system.removeNodeInterval);
                });

                channel.objects.system.networkChanged.connect(function()
                {
                    $('#network').prev('input').val(channel.objects.system.network);
                });
                channel.objects.system.nodeChanged.connect(function()
                {
                    $('#node').prev('input').val(channel.objects.system.node);
                });
                channel.objects.system.secureShellChanged.connect(function()
                {
                    $('#secureShell').val(channel.objects.system.secureShell);
                });
                channel.objects.system.debugLogChanged.connect(function()
                {
                    $('#debugLog').prev('input').val(channel.objects.system.debugLog);
                });
                channel.objects.system.debugMpaLogChanged.connect(function()
                {
                    $('#debugMpaLog').prev('input').val(channel.objects.system.debugMpaLog);
                });

                $('#name').prev('input').val(channel.objects.system.name);
                $('#activity').text(channel.objects.system.activity);
                $('#deviceId').prev('input').val(channel.objects.system.deviceId);
                $('#serialNumber').prev('input').val(channel.objects.system.serialNumber);
                $('#softwareType').val(channel.objects.system.softwareType);
                $('#softwareVersion').val(channel.objects.system.softwareVersion);
                $('#firmwareType').val(channel.objects.system.firmwareType);
                $('#firmwareVersion').val(channel.objects.system.firmwareVersion);
                $('#firmwareAddress').val(channel.objects.system.firmwareAddress);
                $('#rootfsRelease').val(channel.objects.system.rootfsRelease);
                $('#mui').val(channel.objects.system.mui);
                $('#securityCode').prev('input').val(channel.objects.system.securityCode);
                $('#factoryReset').prev('input').val(channel.objects.system.factoryReset);
                $('#buildCommit').val(channel.objects.system.buildCommit);
                $('#date').prev('input').val(channel.objects.system.date);
                $('#time').prev('input').val(channel.objects.system.time);
                $('#canBitRate').prev('input').val(channel.objects.system.canBitRate);

                $('#freeStorage').val(channel.objects.system.freeStorage + ' MB');
                $('#totalStorage').val(channel.objects.system.totalStorage + ' MB');
                $('#freeStoragePercent').text(channel.objects.system.freeStoragePercent);

                $('#cloudUrl').prev('input').val(channel.objects.system.cloudUrl);
                $('#cloudUsername').prev('input').val(channel.objects.system.cloudUsername);
                $('#cloudPassword').prev('input').val(channel.objects.system.cloudPassword);

                $('#scanNetworkInterval').prev('input').val(channel.objects.system.scanNetworkInterval);
                $('#collectDataInterval').prev('input').val(channel.objects.system.collectDataInterval);
                $('#removeNodeInterval').prev('input').val(channel.objects.system.removeNodeInterval);

                $('#network').prev('input').val(channel.objects.system.network);
                $('#node').prev('input').val(channel.objects.system.node);
                $('#secureShell').val(channel.objects.system.secureShell);
                $('#debugLog').prev('input').val(channel.objects.system.debugLog);
                $('#debugMpaLog').prev('input').val(channel.objects.system.debugMpaLog);

                //
                // Statistic backend.
                //
                channel.objects.statistic.failedMpaRequestPercentChanged.connect(function()
                {
                    $('#failedMpaRequestPercent').text(channel.objects.statistic.failedMpaRequestPercent);
                });
                channel.objects.statistic.failedHttpRequestPercentChanged.connect(function()
                {
                    $('#failedHttpRequestPercent').text(channel.objects.statistic.failedHttpRequestPercent);
                });

                $('#failedMpaRequestPercent').text(channel.objects.statistic.failedMpaRequestPercent);
                $('#failedHttpRequestPercent').text(channel.objects.statistic.failedHttpRequestPercent);
            });
        }
    }
   
    $(document).on('ready', function() 
    {
        // Register IDLE refresh timer
        /*
        var time = new Date().getTime();
        $(document.body).bind("mousemove click keypress", function(e)
        {
            time = new Date().getTime();
        });

        setInterval(function()
        {
            if (new Date().getTime() - time >= 300000)
                window.location.reload(true);
        }, 60000);
        */

        pages.not('.' + DEFAULT_PAGE_ON_LOAD).hide({complete: function() {currentPage = DEFAULT_PAGE_ON_LOAD}});

        window.onload = connectWebSocket();
    });

    $('nav a').on('click', function(e) 
    {
        // Prevent default anchor behavior.
        e.preventDefault();

        // Get selected page.
        var selectedPage = $(this).data('page').toLowerCase();

        // Check if same page was selected, if true, exit because we do not need to filter it.
        if (selectedPage === currentPage)
        {
            return;
        }
        else
        {
            /*if (selectedPage === DEFAULT_PAGE_ON_LOAD)
            {
                // Show all pages available.
                pages.show();
            }
            else 
            {*/
                // Show pages by the selected page.
                pages.show().not('.' + selectedPage).hide();
            /*}*/

            // Save current selected page.
            currentPage = selectedPage;

            return;
        }
    });

    $('#restartWiFi').on('click', function()
    {
//        channel.objects.network.restartNetwork();
//        channel.objects.network.loadConfiguration();
        channel.objects.network.restartNetworkAndLoadConfiguration();
    });

    $('#restartEthernet').on('click', function()
    {
//        channel.objects.network.restartNetwork();
//        channel.objects.network.loadConfiguration();
        channel.objects.network.restartNetworkAndLoadConfiguration();
    });

    $('#deviceId').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.deviceId = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#canBitRate').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.canBitRate = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#securityCode').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.securityCode = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#serialNumber').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.serialNumber = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#name').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

    	if (readonly)
	    {
	        input.focus();
            input.select();
	    }
	    else
        {
            channel.objects.system.name = input.val();
        }

	    input.prop('readonly', !readonly);
    });

    $('#date').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

    	if (readonly)
	    {
	        input.focus();
            input.select();
	    }
	    else
        {
            channel.objects.system.date = input.val();
        }

	    input.prop('readonly', !readonly);
    });

    $('#time').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

    	if (readonly)
	    {
	        input.focus();
            input.select();
	    }
	    else
        {
            channel.objects.system.time = input.val();
        }

	    input.prop('readonly', !readonly);
    });

    $('#editEthernetSettings').on('click', function()
    {
        ethernetEditMode = "true";

        $('#editEthernetSettingsCancel').show();
        $('#editEthernetSettingsStore').show();
        $('#editEthernetSettings').hide();

        $('#ethernetStatic').show();
        //Show all edit pens
        if (channel.objects.network.ethernetStatic === "true")
        {
            $('#ethernetAddress').show();
            $('#ethernetNetmask').show();
            $('#ethernetGateway').show();
        }
        else
        {
            $('#ethernetAddress').hide();
            $('#ethernetNetmask').hide();
            $('#ethernetGateway').hide();
        }
    });

    $('#editEthernetSettingsStore').on('click', function()
    {
        $('#editEthernetSettingsCancel').hide();
        $('#editEthernetSettingsStore').hide();
        $('#editEthernetSettings').show();
        $('#ethernetStatic').hide();
        $('#ethernetAddress').hide();
        $('#ethernetNetmask').hide();
        $('#ethernetGateway').hide();

        channel.objects.network.ethernetStatic = $('#ethernetStatic').prev('input').val();
        channel.objects.network.ethernetAddress = $('#ethernetAddress').prev('input').val();
        channel.objects.network.ethernetNetmask = $('#ethernetNetmask').prev('input').val();
        channel.objects.network.ethernetGateway = $('#ethernetGateway').prev('input').val();

        ethernetEditMode = "false";
    });

    $('#editEthernetSettingsCancel').on('click', function()
    {
        var tmpNotify = channel.objects.network.ethernetStatic;
        $('#editEthernetSettingsCancel').hide();
        $('#editEthernetSettingsStore').hide();
        $('#editEthernetSettings').show();
        $('#ethernetStatic').hide();
        $('#ethernetAddress').hide();
        $('#ethernetNetmask').hide();
        $('#ethernetGateway').hide();
        ethernetEditMode = "false";
        channel.objects.network.ethernetStatic = tmpNotify;
    });

    $('#ethernetStatic').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {            
            //channel.objects.network.ethernetStatic = input.val();
            ethernetStaticEdit = input.val();
            if (ethernetStaticEdit === "true")
            {
                $('#ethernetAddress').show();
                $('#ethernetNetmask').show();
                $('#ethernetGateway').show();
            }
            else
            {
                $('#ethernetAddress').hide();
                $('#ethernetNetmask').hide();
                $('#ethernetGateway').hide();
            }
        }

        input.prop('readonly', !readonly);

    });

    $('#ethernetAddress').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

	    if (readonly)
	    {
	        input.focus();
            input.select();
	    }
	    else
        {
            channel.objects.network.ethernetAddress = input.val();
        }

	    input.prop('readonly', !readonly);
    });

    $('#ethernetNetmask').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

	    if (readonly)
	    {
	        input.focus();
            input.select();
	    }
	    else
        {
            channel.objects.network.ethernetNetmask = input.val();
        }

	    input.prop('readonly', !readonly);
    });

    $('#ethernetGateway').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.network.ethernetGateway = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#wirelessSsid').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.network.wirelessSsid = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#wirelessPassword').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.network.wirelessPassword = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#cloudUrl').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.cloudUrl = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#cloudUsername').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.cloudUsername = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#cloudPassword').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.cloudPassword = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#scanNetworkInterval').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.scanNetworkInterval = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#collectDataInterval').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.collectDataInterval = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#removeNodeInterval').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.removeNodeInterval = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#network').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.network = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#node').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.node = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#factoryReset').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.factoryReset = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#debugLog').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.debugLog = input.val();
        }

        input.prop('readonly', !readonly);
    });

    $('#debugMpaLog').on('click', function()
    {
        var input    = $(this).prev('input'),
            readonly = input.prop('readonly');

        if (readonly)
        {
            input.focus();
            input.select();
        }
        else
        {
            channel.objects.system.debugMpaLog = input.val();
        }

        input.prop('readonly', !readonly);
    });
});
