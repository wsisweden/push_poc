TEMPLATE = aux

DISTFILES += \
    index.html \
    common/images/logo.png \
    common/images/no-graph.png \
    common/images/percentage-1.png \
    common/images/percentage-2.png \
    common/scripts/app.js \
    common/stylesheets/master.css \
    vendors/animate/animate.css \
    vendors/jquery/jquery.js \
    vendors/knob/jquery.knob.min.js \
    vendors/qt/qwebchannel.js \
    vendors/font-awesome/fonts/fontawesome-webfont.woff \
    vendors/font-awesome/fonts/fontawesome-webfont.eot \
    vendors/font-awesome/fonts/fontawesome-webfont.woff2 \
    vendors/font-awesome/fonts/FontAwesome.otf \
    vendors/font-awesome/fonts/fontawesome-webfont.ttf \
    vendors/font-awesome/fonts/fontawesome-webfont.svg \
    vendors/font-awesome/css/font-awesome.css \
    vendors/font-awesome/css/font-awesome.min.css \
    vendors/font-awesome/less/font-awesome.less \
    vendors/font-awesome/less/icons.less \
    vendors/font-awesome/less/path.less \
    vendors/font-awesome/less/animated.less \
    vendors/font-awesome/less/bordered-pulled.less \
    vendors/font-awesome/less/core.less \
    vendors/font-awesome/less/fixed-width.less \
    vendors/font-awesome/less/larger.less \
    vendors/font-awesome/less/list.less \
    vendors/font-awesome/less/mixins.less \
    vendors/font-awesome/less/rotated-flipped.less \
    vendors/font-awesome/less/screen-reader.less \
    vendors/font-awesome/less/stacked.less \
    vendors/font-awesome/less/variables.less \
    vendors/font-awesome/scss/_animated.scss \
    vendors/font-awesome/scss/_bordered-pulled.scss \
    vendors/font-awesome/scss/_core.scss \
    vendors/font-awesome/scss/_fixed-width.scss \
    vendors/font-awesome/scss/_icons.scss \
    vendors/font-awesome/scss/_larger.scss \
    vendors/font-awesome/scss/_list.scss \
    vendors/font-awesome/scss/_mixins.scss \
    vendors/font-awesome/scss/_path.scss \
    vendors/font-awesome/scss/_rotated-flipped.scss \
    vendors/font-awesome/scss/_screen-reader.scss \
    vendors/font-awesome/scss/_stacked.scss \
    vendors/font-awesome/scss/_variables.scss \
    vendors/font-awesome/scss/font-awesome.scss \
    vendors/justgage-1.2.2/justgage.js \
    vendors/justgage-1.2.2/raphael-2.1.4.min.js \
    favicon.ico \
    common/fonts/montserrat-v12-latin-700.woff \
    common/fonts/montserrat-v12-latin-700.eot \
    common/fonts/montserrat-v12-latin-700.woff2 \
    common/fonts/montserrat-v12-latin-regular.eot \
    common/fonts/montserrat-v12-latin-700.ttf \
    common/fonts/montserrat-v12-latin-regular.ttf \
    common/fonts/montserrat-v12-latin-700.svg \
    common/fonts/montserrat-v12-latin-regular.svg \
    common/fonts/montserrat-v12-latin-regular.woff \
    common/fonts/montserrat-v12-latin-regular.woff2
