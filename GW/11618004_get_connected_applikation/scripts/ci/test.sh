#!/usr/bin/env bash

# Execute from this directory
cd "`dirname "$0"`"

# Setup variables
BUILD_PARALLEL_THREADS=2
BUILD_DIR="../../build/release"
BUILD_QT_PATH="/opt/x86/qt/5.9.1/gcc_64/bin"

fail()
{
	echo "$1" 1>&2
	exit 1
}

export MAKE_COMMAND="make -j $BUILD_PARALLEL_THREADS"

# Clean and enter shadow build directory
echo Cleaning...
if [ -e $BUILD_DIR ]; then
	rm -Rf $BUILD_DIR || fail "Could not delete $BUILD_DIR"
fi

mkdir -p $BUILD_DIR || fail "Could not create $BUILD_DIR"
cd $BUILD_DIR || fail "Could not enter $BUILD_DIR"

# Run qmake
echo Running qmake...
$BUILD_QT_PATH/qmake ../../src/Gateway.pro -r -spec linux-g++ "CONFIG+=release" || fail "qmake failed"

# Run make using the number of hardware threads in BUILD_PARALLEL_THREADS
echo Building...
/usr/bin/time -f 'Build time %E' make || fail "make failed"

# Run qmake
echo Running qmake...
$BUILD_QT_PATH/qmake ../../test/Test.pro -r -spec linux-g++ "CONFIG+=release" || fail "qmake failed"

# Run make using the number of hardware threads in BUILD_PARALLEL_THREADS
echo Building...
/usr/bin/time -f 'Build time %E' make || fail "make failed"

# Running all tests
for i in *; do
	if [ -d $i ]; then
		$i/${i,,} -o $i/${i,,}.xml,xunitxml
	fi
done

exit 0
