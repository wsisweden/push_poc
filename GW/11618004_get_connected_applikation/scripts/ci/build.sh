#!/usr/bin/env bash

# Execute from this directory
cd "`dirname "$0"`"

# Setup variables
if [ -z "$SCM_COMMIT" ]; then
    SCM_COMMIT=$(git svn find-rev $(git log --max-count 1 --pretty=format:%H))
fi

BUILD_ARCHIVE_NAME="gateway"
BUILD_PARALLEL_THREADS=2
BUILD_DIR="../../build"
BUILD_QT_PATH="/opt/micropower-cgw-sdk-1.0/bin"
PACKAGE_DIR=$BUILD_ARCHIVE_NAME-$SCM_COMMIT

fail()
{
	echo "$1" 1>&2
	exit 1
}

export MAKE_COMMAND="make -j $BUILD_PARALLEL_THREADS"

# Clean and enter shadow build directory
echo Cleaning...
if [ -e $BUILD_DIR ]; then
	rm -Rf $BUILD_DIR || fail "Could not delete $BUILD_DIR"
fi

mkdir $BUILD_DIR || fail "Could not create $BUILD_DIR"
cd $BUILD_DIR || fail "Could not enter $BUILD_DIR"

# Run qmake
echo Running qmake...
$BUILD_QT_PATH/qmake ../src/Gateway.pro -r -spec devices/linux-beagleboard-g++ "CONFIG+=release" || fail "qmake failed"

# Run make using the number of hardware threads in BUILD_PARALLEL_THREADS
echo Building...
/usr/bin/time -f 'Build time %E' make || fail "make failed"

# Create package directory to later archive
if [ -f $PACKAGE_DIR ]; then
	rm -Rf $PACKAGE_DIR || fail "Could not delete $PACKAGE_DIR"
fi
mkdir $PACKAGE_DIR || fail "Could not create $PACKAGE_DIR"
mkdir $PACKAGE_DIR/bin || fail "Could not create $PACKAGE_DIR/bin"

# Copy compiled executable
echo Copying binaries...
#find . -name \*.so\* -exec cp -fP {} $PACKAGE_DIR/bin/ \; || fail "Could not copy libraries"
cp -f Shell/gateway $PACKAGE_DIR/bin/ || fail "Could not copy executable"

# Create tar.gz file
echo Creating tar.gz...
tar -cvzf $PACKAGE_DIR.tar.gz $PACKAGE_DIR || fail "Could not create archive"
