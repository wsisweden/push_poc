#pragma once

#include "Shared.h"
#include "Node.h"
#include "IDevice.h"
#include "IReciever.h"
#include "Command/Handler/CommandHandlerFactory.h"
#include "../Common/Context.h"
#include "../Common/Event/SyncNodeEvent.h"
#include "../Common/Event/ConfigurationEvent.h"
#include "../Common/Event/ScanNetworkIntervalEvent.h"
#include "../Common/Event/CollectDataIntervalEvent.h"
#include "../Common/Event/RemoveNodeIntervalEvent.h"
#include "../Common/Event/AuthorizationEvent.h"
#include "../Common/Event/UplinkEvent.h"
#include "../Common/Event/IndexEvent.h"
#include "../Common/Event/InvalidIndexEvent.h"
#include "../Common/Event/NodeTimeoutEvent.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QVariant>
#include <QtCore/QMap>
#include <QtCore/QList>

class MPACCESS_EXPORT NodeWorker : public IReciever
{
    Q_OBJECT

    public:
        explicit NodeWorker(Context* context, QObject* parent = nullptr);
        ~NodeWorker();

        void setRadioAddress(const quint8 channel, const quint16 panId, const quint16 address, const quint32 firmwareType, const quint32 firmwareVersion);
        void addNetwork(const quint8 channel, const quint16 panId);
        void addNode(const quint32 id, const quint8 channel, const quint16 panId, const quint16 address, const quint8 productType);
        void addNodeInfo(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void addNodeMeas(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void addNodeStatus(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void addNodeConfig(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void addNodeHistoryLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void addNodeEventLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void addNodeInstantLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void updateParam(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);

    private:
        bool uplink = false;
        bool authorized = false;
        bool useNetworkRestriction = false;
        bool useNodeRestriction = false;
        bool firmwareAddressReceived = false;

        quint16 firmwareAddress;
        quint8 channelRestriction;
        quint16 panIdRestriction;
        quint32 nodeAddressRestriction;

        int scanNetworkInterval;
        int collectDataInterval;
        int removeNodeInterval;

        QMap<QString, Node*> nodes;
        QList<QString> blacklistedNodes;
        QMap<quint8, QVector<quint16>> network;

        int configComplete = 0;
        int initRadioModuleAttempts = 0;

        const int MAX_RADIO_MODULE_RETRY = 3;
        const quint8 MAX_HISTORY_LOG_REQUEST = 10;
        const quint8 MAX_EVENT_LOG_REQUEST = 50;
        const quint8 MAX_INSTANT_LOG_REQUEST = 100;

        IDevice* device = nullptr;
        Context* context = nullptr;
        QTimer* scanNetworkTimer = nullptr;
        QTimer* collectDataTimer = nullptr;
        QTimer* removeNodeTimer = nullptr;
        CommandHandlerFactory* commandFactory = nullptr;

        QList<Node*> getSortedNodes();
        QString translateLogType(const int logType);
        QString translateProductType(const quint8 value);

        void setupWorker();
        void removeBlacklist();

        void scanForNodes(const quint8 channel, const quint16 panId);
        void setRadioParam(const quint8 channel, const quint16 panId);

        void getNodeInfo(const quint8 channel, const quint16 panId, const quint16 address);
        void getNodeMeas(const quint8 channel, const quint16 panId, const quint16 address);
        void getNodeStatus(const quint8 channel, const quint16 panId, const quint16 address);
        void getNodeConfig(const quint8 channel, const quint16 panId, const quint16 address);
        void getNodeLogs(const quint8 channel, const quint16 panId, const quint16 address);
        void getNodeLog(const quint8 channel, const quint16 panId, const quint16 address, const quint32 index, const quint8 number, const quint8 type);
        void setNodeSync(const quint8 channel, const quint16 panId, const quint16 address, const quint32 identifier, const quint8 productType);

        Q_SLOT void doFactoryReset();
        Q_SLOT void scanNetwork();
        Q_SLOT void collectData();
        Q_SLOT void removeNodes();
        Q_SLOT void initializeComplete();
        Q_SLOT void configurationComplete();
        Q_SLOT void initializeRadioModule();
        Q_SLOT void syncNode(const SyncNodeEvent& event);
        Q_SLOT void configurationChanged(const ConfigurationEvent& event);
        Q_SLOT void scanNetworkIntervalChanged(const ScanNetworkIntervalEvent& event);
        Q_SLOT void collectDataIntervalChanged(const CollectDataIntervalEvent& event);
        Q_SLOT void removeNodeIntervalChanged(const RemoveNodeIntervalEvent& event);
        Q_SLOT void authorizationChanged(const AuthorizationEvent& event);
        Q_SLOT void uplinkChanged(const UplinkEvent& event);
        Q_SLOT void nodeRestrictionChanged(const NodeRestrictionEvent& event);
        Q_SLOT void networkRestrictionChanged(const NetworkRestrictionEvent& event);
        Q_SLOT void nodeTimeout(const NodeTimeoutEvent& event);
        Q_SLOT void indexReceived(const IndexEvent& event);
        Q_SLOT void invalidIndex(const InvalidIndexEvent& event);
};
