#pragma once

#include "Shared.h"
#include "Message/Model/IMpModel.h"
#include "../Cloud/Request/RequestPackage.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtCore/QStringList>
#include <QtCore/QList>
#include <QtCore/QVariant>
#include <QtCore/QJsonArray>

class MPACCESS_EXPORT Node : public QObject
{
    Q_OBJECT

    public:
        explicit Node(const quint32 nodeId, const quint8 channel, const quint16 panId, const quint16 address, const quint8 productType, QObject* parent = nullptr);

        bool hasInfoChanged();
        void clearLogsAndIndexes();

        RequestPackage* makeDevicePingRequest();
        RequestPackage* makeStatusRequest();
        RequestPackage* makeConfigRequest();
        RequestPackage* makeHistoryLogRequest();
        RequestPackage* makeEventLogRequest();
        RequestPackage* makeInstantLogRequest();

        void updateNodeId(quint8 id);
        void updateProductType(quint8 type);

        void resetTimeout() { this->timeout = 0; }
        void increaseTimeout() { this->timeout++; }

        void setInfoModel(IMpModel* model);
        void setMeasModel(IMpModel* model);
        void setStatusModel(IMpModel* model);
        void setConfigModel(IMpModel* model);

        void setQueried(const bool value) { this->queried = value; }
        void setInvalidIndex(const bool value) { this->invalidIndex = value; }

        void setHistoryIndexReceived(const bool value) { this->historyIndexReceived = value; }
        void setEventIndexReceived(const bool value) { this->eventIndexReceived = value; }
        void setInstantIndexReceived(const bool value) { this->instantIndexReceived = value; }

        void setHistoryRequested(const int value)  { this->historyRequested = value; }
        void setEventRequested(const int value)  { this->eventRequested = value; }
        void setInstantRequested(const int value) { this->instantRequested = value; }

        IMpModel* getInfoModel() { return this->infoModel; }

        void addHistoryLogModel(IMpModel* model);
        void addEventLogModel(IMpModel* model);
        void addInstantLogModel(IMpModel* model);

        quint16 getPanId() const { return this->panId; }
        quint8 getChannel() const { return this->channel; }
        quint16 getAddress() const { return this->address; }
        quint32 getNodeId() const { return this->nodeId; }
        quint8 getProductType() const { return this->productType; }
        quint64 getMUI() { return this->mui; }
        quint8  getTimeout() { return this->timeout; }

        quint8 getHistoryRequested() const { return this->historyRequested; }
        quint8 getEventRequested() const { return this->eventRequested; }
        quint8 getInstantRequested() const { return this->instantRequested; }

        bool isQueried() const { return this->queried; }
        bool isInvalidIndex() const { return this->invalidIndex; }
        bool isIndexesReady() const { return this->historyIndexReceived == true && this->eventIndexReceived == true && this->instantIndexReceived == true; }

        bool isInfoReady() const { return this->infoModel != nullptr; }
        bool isConfigReady() const { return isInfoReady() && this->configModel != nullptr; }
        bool isStatusReady() const { return isConfigReady() && this->measModel != nullptr && this->statusModel != nullptr; }
        bool isLogsReady() const { return this->historyLogModel.count() == this->historyRequested && this->eventLogModel.count() == this->eventRequested && this->instantLogModel.count() == this->instantRequested; }

    private:
        quint8 channel;
        quint16 panId;
        quint16 address;
        quint32 nodeId;
        quint8 productType;
        quint64 mui;

        quint8 timeout = 0;

        quint8 historyRequested;
        quint8 eventRequested;
        quint8 instantRequested;

        QString utcOffset;
        QString previousInfoHash;

        QList<IMpModel*> historyLogModel;
        QList<IMpModel*> eventLogModel;
        QList<IMpModel*> instantLogModel;

        IMpModel* infoModel = nullptr;
        IMpModel* measModel = nullptr;
        IMpModel* statusModel = nullptr;
        IMpModel* configModel = nullptr;

        bool queried = false;
        bool invalidIndex = false;
        bool historyIndexReceived = false;
        bool eventIndexReceived = false;
        bool instantIndexReceived = false;

        QString translateEventId(const quint16 id);
        QString translateCalibrationType(const quint8 type);

        void calculateUtcOffset(quint32 deviceTime);
        double convertToCelsius(const quint16 temperature);

        QList<QVariant> translateSupAlarms(const quint16 error);

        QList<QVariant> translateBMAlarms(const quint16 error);
        QList<QVariant> translateBMStatus(const quint16 status);

        QList<QVariant> translateReguAlarms(const quint16 error);
        QList<QVariant> translateReguStatus(const quint16 status);

        QList<QVariant> translateChalgAlarms(const quint16 error);
        QList<QVariant> translateChalgStatus(const quint16 status);

        QString getChargingRestrictionDay(const int index);
        QJsonArray parsePinInSelect(QVector<quint32> pinSelect);
        QJsonArray parsePinOutSelect(QVector<quint32> pinSelect);
        QJsonArray parseChargingRestrictions(QVector<quint8> chargingRestrictions);
};
