QT += core serialport sql webchannel websockets

CONFIG += c++11 staticlib

TARGET = mpaccess
TEMPLATE = lib

DEFINES += MPACCESS_LIBRARY

HEADERS += \
    Shared.h \
    IDevice.h \
    Message/Model/IMpModel.h \
    Message/Model/IMpModel.h \
    IReciever.h \
    Node.h \
    NodeWorker.h \
    Device/UartDevice.h \
    Command/Request/CommandRequest.h \
    Command/Request/GetMaxPayloadRequest.h \
    Command/Request/GetRadioParamRequest.h \
    Command/Request/NwkDataRequest.h \
    Command/Request/ScanForNodesRequest.h \
    Command/Request/SetRadioParamRequest.h \
    Command/Request/ScanNetworkRequest.h \
    Message/Request/GetConfigRequest.h \
    Message/Request/GetInfoRequest.h \
    Message/Request/GetLogRequest.h \
    Message/Request/GetMeasRequest.h \
    Message/Request/GetStatusRequest.h \
    Message/Request/MessageRequest.h \
    Message/Handler/BmConfigHandler.h \
    Message/Handler/BmInfoHandler.h \
    Message/Handler/BmLogHandler.h \
    Message/Handler/BmMeasHandler.h \
    Message/Handler/BmStatusHandler.h \
    Message/Handler/CmConfigHandler.h \
    Message/Handler/CmInfoHandler.h \
    Message/Handler/CmLogHandler.h \
    Message/Handler/CmMeasHandler.h \
    Message/Handler/CmStatusHandler.h \
    Message/Handler/MessageHandler.h \
    Message/Handler/MessageHandlerFactory.h \
    Command/Handler/CommandHandler.h \
    Command/Handler/CommandHandlerFactory.h \
    Command/Handler/DataIndicationHandler.h \
    Command/Handler/GetMaxPayloadHandler.h \
    Command/Handler/GetRadioParamHandler.h \
    Command/Handler/NwkDataHandler.h \
    Command/Handler/ScanForNodesHandler.h \
    Command/Handler/ScanNetworkHandler.h \
    Command/Handler/SetRadioParamHandler.h \
    Message/Model/BmConfigModel.h \
    Message/Model/BmEventLogModel.h \
    Message/Model/BmHistoryLogModel.h \
    Message/Model/BmInfoModel.h \
    Message/Model/BmInstantLogModel.h \
    Message/Model/BmMeasModel.h \
    Message/Model/BmStatusModel.h \
    Message/Model/CmConfigModel.h \
    Message/Model/CmEventLogModel.h \
    Message/Model/CmHistoryLogModel.h \
    Message/Model/CmInfoModel.h \
    Message/Model/CmInstantLogModel.h \
    Message/Model/CmMeasModel.h \
    Message/Model/CmStatusModel.h \
    Message/Request/SyncNodeRequest.h \
    Message/Request/SetParamRequest.h \
    UniqueKey.h \
    Message/Handler/SetParamHandler.h \
    Message/Model/SetParamModel.h

SOURCES += \
    Node.cpp \
    NodeWorker.cpp \
    Device/UartDevice.cpp \
    Command/Request/CommandRequest.cpp \
    Command/Request/GetMaxPayloadRequest.cpp \
    Command/Request/GetRadioParamRequest.cpp \
    Command/Request/NwkDataRequest.cpp \
    Command/Request/ScanForNodesRequest.cpp \
    Command/Request/ScanNetworkRequest.cpp \
    Command/Request/SetRadioParamRequest.cpp \
    Message/Request/GetConfigRequest.cpp \
    Message/Request/GetInfoRequest.cpp \
    Message/Request/GetLogRequest.cpp \
    Message/Request/GetMeasRequest.cpp \
    Message/Request/GetStatusRequest.cpp \
    Message/Request/MessageRequest.cpp \
    Message/Handler/BmConfigHandler.cpp \
    Command/Handler/CommandHandler.cpp \
    Command/Handler/CommandHandlerFactory.cpp \
    Command/Handler/DataIndicationHandler.cpp \
    Command/Handler/GetMaxPayloadHandler.cpp \
    Command/Handler/GetRadioParamHandler.cpp \
    Command/Handler/NwkDataHandler.cpp \
    Command/Handler/ScanForNodesHandler.cpp \
    Command/Handler/ScanNetworkHandler.cpp \
    Command/Handler/SetRadioParamHandler.cpp \
    Message/Handler/BmInfoHandler.cpp \
    Message/Model/BmConfigModel.cpp \
    Message/Model/BmEventLogModel.cpp \
    Message/Model/BmHistoryLogModel.cpp \
    Message/Model/BmInfoModel.cpp \
    Message/Model/BmInstantLogModel.cpp \
    Message/Model/BmMeasModel.cpp \
    Message/Model/BmStatusModel.cpp \
    Message/Model/CmConfigModel.cpp \
    Message/Model/CmEventLogModel.cpp \
    Message/Model/CmHistoryLogModel.cpp \
    Message/Model/CmInfoModel.cpp \
    Message/Model/CmInstantLogModel.cpp \
    Message/Model/CmMeasModel.cpp \
    Message/Model/CmStatusModel.cpp \
    Message/Handler/BmLogHandler.cpp \
    Message/Handler/BmMeasHandler.cpp \
    Message/Handler/BmStatusHandler.cpp \
    Message/Handler/CmConfigHandler.cpp \
    Message/Handler/CmInfoHandler.cpp \
    Message/Handler/CmLogHandler.cpp \
    Message/Handler/CmMeasHandler.cpp \
    Message/Handler/CmStatusHandler.cpp \
    Message/Handler/MessageHandler.cpp \
    Message/Handler/MessageHandlerFactory.cpp \
    Message/Request/SyncNodeRequest.cpp \
    Message/Request/SetParamRequest.cpp \
    Message/Handler/SetParamHandler.cpp \
    Message/Model/SetParamModel.cpp

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:QMAKE_LFLAGS += -Wl,--start-group

DEPENDPATH += $$OUT_PWD/../Common $$PWD/../Common
INCLUDEPATH += $$OUT_PWD/../Common $$PWD/../Common
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Common/release/ -lcommon
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Common/debug/ -lcommon
else:macx:LIBS += -L$$OUT_PWD/../Common/ -lcommon
else:unix:LIBS += -L$$OUT_PWD/../Common/ -lcommon

DEPENDPATH += $$OUT_PWD/../Storage $$PWD/../Storage
INCLUDEPATH += $$OUT_PWD/../Storage $$PWD/../Storage
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Storage/debug/ -lstorage
else:macx:LIBS += -L$$OUT_PWD/../Storage/ -lstorage
else:unix:LIBS += -L$$OUT_PWD/../Storage/ -lstorage
