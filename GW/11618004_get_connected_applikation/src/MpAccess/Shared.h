#pragma once

#include <QtCore/QtGlobal>

#if defined(MPACCESS_LIBRARY)
    #define MPACCESS_EXPORT //Q_DECL_EXPORT
#else
    #define MPACCESS_EXPORT //Q_DECL_IMPORT
#endif
