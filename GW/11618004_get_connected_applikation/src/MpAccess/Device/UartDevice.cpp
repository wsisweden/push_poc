#include "UartDevice.h"
#include "../../Common/Event/SyncNodeEvent.h"
#include "../Message/Request/SyncNodeRequest.h"
#include "../Message/Request/MessageRequest.h"
#include "../Command/Request/SetRadioParamRequest.h"

#include <QtCore/QLoggingCategory>

/**
 * @brief UartDevice::UartDevice Construct a new UART device
 */
UartDevice::UartDevice(Context* context, QObject* parent)
    : IDevice(parent)
{
    qInfo().noquote() << "Initialize UART device";

    this->context = context;
    this->command = new CommandRequest();

    this->device.setBaudRate(115200);
    this->device.setPortName(this->context->getCommandLineParser()->value("devicename"));
    this->device.setDataBits(QSerialPort::Data8);
    this->device.setParity(QSerialPort::NoParity);
    this->device.setStopBits(QSerialPort::OneStop);
    this->device.setFlowControl(QSerialPort::NoFlowControl);

    if (!this->device.open(QIODevice::ReadWrite))
        qCritical().noquote() << "Could not open UART device";

    this->timeoutTimer = new QTimer(this);
    this->transmitTimer = new QTimer(this);
    this->receiveTimer = new QTimer(this);

    connect(this->timeoutTimer, &QTimer::timeout, this, &UartDevice::timeout);
    connect(this->transmitTimer, &QTimer::timeout, this, &UartDevice::transmit);
    connect(this->receiveTimer, &QTimer::timeout, this, &UartDevice::receive);

    this->timeoutTimer->setSingleShot(true);
    this->transmitTimer->setSingleShot(true);
    this->receiveTimer->start(10);

    loadConfiguration();
}

/**
 * @brief UartDevice::~UartDevice Tear down UART device
 */
UartDevice::~UartDevice()
{
    qInfo().noquote() << "Tearing down UART device";

    this->device.close();

    this->receiveTimer->stop();
    this->transmitTimer->stop();
    this->timeoutTimer->stop();
}

/**
 * @brief UartDevice::setRadioAddress Set radio address
 */
void UartDevice::setFirmwareAddress(const quint16 address)
{
    this->firmwareAddress = address;
}

/**
 * @brief UartDevice::loadConfiguration Load configration
 */
void UartDevice::loadConfiguration()
{
    clearReceivedCommand();
    if (!this->sendQueue.isEmpty())
    {
        qDebug().noquote() << "Clear command queue";
        this->sendQueue.clear();
    }
}

/**
 * @brief UartDevice::send Prioritize set a command over send
 * @param command The command to send
 */
void UartDevice::set(CommandRequest* command)
{
    if (this->queue.isEmpty())
    {
        this->queue.append(command);
        writeCommand(command);
    }
    else
        this->setQueue.append(command);
}

/**
 * @brief UartDevice::send Send a command
 * @param command The command to send
 */
void UartDevice::send(CommandRequest* command)
{
    if (this->queue.isEmpty())
    {
        this->queue.append(command);
        writeCommand(command);
    }
    else
        this->sendQueue.append(command);
}

/**
 * @brief UartDevice::setCommandFactory Set the command factory
 * @param factory The command factory
 */
void UartDevice::setCommandFactory(CommandHandlerFactory* factory)
{
    this->factory = factory;
}

/**
 * @brief UartDevice::isSendQueueEmpty Check if send queue is empty
 */
bool UartDevice::isSendQueueEmpty()
{
    return this->sendQueue.isEmpty();
}

/**
 * @brief UartDevice::sendQueue Return send queue count
 */
int UartDevice::sendQueueCount()
{
    return this->sendQueue.count();
}

/**
 * @brief UartDevice::isOpen Check if device is open
 */
bool UartDevice::isOpen()
{
    return this->device.isOpen();
}

/**
 * @brief UartDevice::writeCommand Write a command to the device
 * @param command The command to write
 * @return Number of bytes written
 */
bool UartDevice::writeCommand(CommandRequest* command)
{
    if (!this->device.isOpen())
        return false;

    SyncNodeRequest* syncNodeRequest = dynamic_cast<SyncNodeRequest*>(command);
    if (syncNodeRequest != nullptr)
    {
        this->context->getEventManager()->emitSyncNodeEvent(SyncNodeEvent(syncNodeRequest->getNodeId(), syncNodeRequest->getChannel(),
                                                                          syncNodeRequest->getPanId(), syncNodeRequest->getAddress(), syncNodeRequest->getProductType()));
        this->queue.removeFirst();
        delete command;

        // Inform others that the command is complete.
        this->context->getEventManager()->emitCommandCompleteEvent();

        this->transmitTimer->start(10); // Process next command.

        return false;
    }

    // Increase request counter.
    this->context->getEventManager()->emitMpaRequestEvent();

    // Inform others that we are going to transmit command.
    this->context->getEventManager()->emitCommandTransmitEvent();

    QVector<quint8> buffer;
    command->toBuffer(buffer);

    QByteArray data = QByteArray(reinterpret_cast<const char*>(buffer.data()), buffer.size());
    qint64 bytesWritten = this->device.write(data);

    if (bytesWritten > 0)
        this->timeoutTimer->start(command->getTimeout());

    MessageRequest* message = dynamic_cast<MessageRequest*>(command);
    if (message == nullptr)
        qCDebug(QLoggingCategory("mpa")).noquote() << QString("Sent command 0x%1 (Channel: %2, PanId: %3, Size: %4 bytes)").arg(QString::number(command->getEventId(), 16).toUpper())
                                                                                                                           .arg(command->getChannel())
                                                                                                                           .arg(command->getPanId())
                                                                                                                           .arg(QString::number(buffer.size()));
    else
    {
        if (message->getAddress() == this->firmwareAddress)
            qCDebug(QLoggingCategory("mpa")).noquote() << QString("Sent parameter (Address %1, Size: %2 bytes)").arg(message->getAddress())
                                                                                                                .arg(QString::number(buffer.size()));
        else
            qCDebug(QLoggingCategory("mpa")).noquote() << QString("Sent message 0x%1 (Channel: %2, PanId: %3, Address %4, Size: %5 bytes)").arg(QString::number(message->getEventId(), 16).toUpper())
                                                                                                                                           .arg(message->getChannel())
                                                                                                                                           .arg(message->getPanId())
                                                                                                                                           .arg(message->getAddress())
                                                                                                                                           .arg(QString::number(buffer.size()));
    }

    this->currentChannel = command->getChannel();
    this->currentPanId = command->getPanId();

    return (bytesWritten > 0);
}

/**
 * @brief UartDevice::receive Receive timer event triggered
 */
void UartDevice::receive()
{
    if (this->device.bytesAvailable() > 0)
    {
        qCDebug(QLoggingCategory("mpa")).noquote() << "Received" << QString::number(this->device.bytesAvailable()) << "bytes";

        QByteArray data = this->device.readAll();
        for (int i = 0; i < data.size(); ++i)
        {
            this->command->addToCommand(data.at(i));
            if (!this->command->isValid())
            {
                qCDebug(QLoggingCategory("mpa")).noquote() << QString("Deleted command (%1 bytes)").arg(QString::number(this->command->getData().size()));

                clearReceivedCommand();
            }
            else if (this->command->isComplete())
            {
                qCDebug(QLoggingCategory("mpa")).noquote() << QString("Received command 0x%1 (%2 bytes)").arg(QString::number(this->command->getEventId(), 16).toUpper())
                                                                                                         .arg(QString::number(this->command->getData().size()));

                CommandHandler* handler = this->factory->getHandler(this->command->getEventId());
                if (handler != nullptr)
                {
                    // The handler returns true if the last segement is received.
                    if (handler->handle(this->currentChannel, this->currentPanId, this->command->getData()))
                    {
                        // Should we pop the command?
                        if (!this->queue.isEmpty() && this->queue.first()->isResponse(handler))
                        {
                            CommandRequest* first = this->queue.first();
                            this->transmitTimer->start(first->getPostTimeout());
                            this->timeoutTimer->stop();

                            this->queue.removeFirst();
                            delete first;

                            // Inform others that the command is complete.
                            // Note! Do not use command complete event for push messages, i.e. parameters.
                            this->context->getEventManager()->emitCommandCompleteEvent();
                        }
                    }
                    else
                    {
                        // Make sure we have requested the segment.
                        if (!this->queue.isEmpty() && this->queue.first()->isResponse(handler))
                        {
                            // We know we have more segments to come. Do not resend command, just wait for more segments.
                            CommandRequest* first = this->queue.first();
                            first->clearRetries();

                            this->timeoutTimer->start(); // Restart timeout, there are more segments.
                        }
                    }
                }

                clearReceivedCommand();
            }
        }
    }
}

/**
 * @brief UartDevice::transmit Transmit timer event triggered
 */
void UartDevice::transmit()
{
    if (populateQueue())
        writeCommand(this->queue.first());
}

/**
 * @brief UartDevice::timeout Command timeout
 */
void UartDevice::timeout()
{
    clearReceivedCommand();

    if (this->queue.isEmpty())
        return;

    CommandRequest* first = (CommandRequest*)this->queue.first();
    MessageRequest* message = dynamic_cast<MessageRequest*>(first);
    if (message == nullptr)
        qCWarning(QLoggingCategory("mpa")).noquote() << QString("Command timeout occured 0x%1, retires %2").arg(QString::number(first->getEventId(), 16).toUpper()).arg(first->getRetries() - 1);
    else
        qCWarning(QLoggingCategory("mpa")).noquote() << QString("Message timeout occured 0x%1, retires %2").arg(QString::number(message->getEventId(), 16).toUpper()).arg(message->getRetries() - 1);

    if (!first->isRetry())
    {    
        // Is the request of type message and it's not a parameter request, then inform others about the node timeout.
        if (message != nullptr && message->getAddress() != this->firmwareAddress)
            this->context->getEventManager()->emitNodeTimeoutEvent(NodeTimeoutEvent(message->getChannel(), message->getPanId(), message->getAddress()));

        this->queue.removeFirst();
        delete first;

        // Increase request fail counter.
        this->context->getEventManager()->emitMpaRequestFailedEvent();
    }

    this->transmitTimer->start(10);
}

/**
 * @brief UartDevice::pushQueueItem Push new item to queue.
 * @return True if queue is not emtpy.
 */
bool UartDevice::populateQueue()
{        
    // Set commands (parameters) have prio.
    if (!this->setQueue.isEmpty())
        this->queue.append(this->setQueue.takeFirst()); // Move first command from the set queue to the queue.
    else if (!this->sendQueue.isEmpty())
    {
        CommandRequest* command = this->sendQueue.first();

        // Do we need to change channel or pan id, if so append new radio param request and let the original command stay on the send queue to the next round.
        if (command->getChannel() >= MIN_CHANNEL && command->getChannel() <= MAX_CHANNEL && (command->getChannel() != this->currentChannel || command->getPanId() != this->currentPanId))
            this->queue.append(new SetRadioParamRequest(command->getChannel(), command->getPanId(), this->firmwareAddress)); // Append a new radio param command with the new channel and pan.
        else
            this->queue.append(this->sendQueue.takeFirst()); // Move first command from the send queue to the queue.
    }

    return !this->queue.isEmpty();
}

/**
 * @brief UartDevice::clearReceivedCommand Delete received data and start over with at fresh buffer.
 */
void UartDevice::clearReceivedCommand()
{
    delete this->command;
    this->command = new CommandRequest();
}

/**
 * @brief UartDevice::reset Reset device.
 */
void UartDevice::reset()
{
    loadConfiguration();
}
