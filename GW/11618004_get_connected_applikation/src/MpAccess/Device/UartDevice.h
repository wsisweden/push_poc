#pragma once

#include "../Shared.h"
#include "../IDevice.h"
#include "../Command/Request/CommandRequest.h"
#include "../Command/Handler/CommandHandlerFactory.h"
#include "../../Common/Context.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QQueue>

#include <QtSerialPort/QSerialPort>

class MPACCESS_EXPORT UartDevice : public IDevice
{
    Q_OBJECT

    public:
        explicit UartDevice(Context* context, QObject* parent = nullptr);
        ~UartDevice();

        void reset();
        void setFirmwareAddress(const quint16 address);

        bool isOpen();
        int sendQueueCount();
        bool isSendQueueEmpty();

        void set(CommandRequest* command);
        void send(CommandRequest* command);
        void setCommandFactory(CommandHandlerFactory* factory);

    private:
        quint8 currentChannel;
        quint16 currentPanId;
        quint16 firmwareAddress;

        QSerialPort device;

        QQueue<CommandRequest*> queue;
        QQueue<CommandRequest*> setQueue;
        QQueue<CommandRequest*> sendQueue;

        QTimer* timeoutTimer = nullptr;
        QTimer* receiveTimer = nullptr;
        QTimer* transmitTimer = nullptr;

        Context* context = nullptr;
        CommandRequest* command = nullptr;
        CommandHandlerFactory* factory = nullptr;

        static const quint8 MIN_CHANNEL = 11;
        static const quint8 MAX_CHANNEL = 26;

        void loadConfiguration();

        void clearReceivedCommand();
        bool populateQueue();
        bool writeCommand(CommandRequest* command);

        Q_SLOT void timeout();
        Q_SLOT void receive();
        Q_SLOT void transmit();
};
