#include "Node.h"
#include "Message/Model/BmInfoModel.h"
#include "Message/Model/CmInfoModel.h"
#include "Message/Model/BmMeasModel.h"
#include "Message/Model/CmMeasModel.h"
#include "Message/Model/BmStatusModel.h"
#include "Message/Model/CmStatusModel.h"
#include "Message/Model/BmConfigModel.h"
#include "Message/Model/CmConfigModel.h"
#include "Message/Model/BmHistoryLogModel.h"
#include "Message/Model/CmHistoryLogModel.h"
#include "Message/Model/BmEventLogModel.h"
#include "Message/Model/CmEventLogModel.h"
#include "Message/Model/BmInstantLogModel.h"
#include "Message/Model/CmInstantLogModel.h"
#include "Message/Handler/BmInfoHandler.h"
#include "Message/Handler/CmInfoHandler.h"
#include "Message/Handler/BmStatusHandler.h"
#include "Message/Handler/CmStatusHandler.h"
#include "../Common/Constant.h"

#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QTimeZone>
#include <QtCore/QDebug>
#include <QtCore/QCryptographicHash>

/**
 * @brief Node::Node Construct a new node
 */
Node::Node(const quint32 nodeId, const quint8 channel, const quint16 panId, const quint16 address, const quint8 productType, QObject* parent)
    : QObject(parent)
{
    this->panId = panId;
    this->channel = channel;
    this->address = address;
    this->nodeId = nodeId;
    this->productType = productType;
}

/**
 * @brief Node::clearLogs Clear log models
 */
void Node::clearLogsAndIndexes()
{
    qDeleteAll(this->historyLogModel);
    qDeleteAll(this->eventLogModel);
    qDeleteAll(this->instantLogModel);

    this->historyLogModel.clear();
    this->eventLogModel.clear();
    this->instantLogModel.clear();

    this->previousInfoHash.clear();

    setHistoryIndexReceived(false);
    setEventIndexReceived(false);
    setInstantIndexReceived(false);
}

/**
 * @brief Node::updateNodeId Update node id
 * @param nodeId The node id
 */
void Node::updateNodeId(quint8 nodeId)
{
    this->nodeId = nodeId;
}

/**
 * @brief Node::updateProductTypeId Update product type id
 * @param id The id
 */
void Node::updateProductType(quint8 type)
{
    this->productType = type;
}

/**
 * @brief Node::setInfoModel Set info model
 * @param model The model
 */
void Node::setInfoModel(IMpModel* model)
{
    if (this->infoModel != nullptr)
        delete this->infoModel;

    this->infoModel = model;
    if (this->infoModel->getId() == BmInfoHandler::ID)
        this->mui = ((BmInfoModel*)this->infoModel)->getMUI();
    else if (this->infoModel->getId() == CmInfoHandler::ID)
        this->mui = ((CmInfoModel*)this->infoModel)->getMUI();
}

/**
 * @brief Node::setInfoModel Set meas model
 * @param model The model
 */
void Node::setMeasModel(IMpModel* model)
{
    if (this->measModel != nullptr)
        delete this->measModel;

    this->measModel = model;
}

/**
 * @brief Node::setInfoModel Set status model
 * @param model The model
 */
void Node::setStatusModel(IMpModel* model)
{
    if (this->statusModel != nullptr)
        delete this->statusModel;

    this->statusModel = model;
}

/**
 * @brief Node::setInfoModel Set config model
 * @param model The model
 */
void Node::setConfigModel(IMpModel* model)
{
    if (this->configModel != nullptr)
        delete this->configModel;

    this->configModel = model;
}

/**
 * @brief Node::addHistoryLogModel Add history model
 * @param model The model
 */
void Node::addHistoryLogModel(IMpModel* model)
{
    this->historyLogModel.append(model);
}

/**
 * @brief Node::addEventLogModel Add event model
 * @param model The model
 */
void Node::addEventLogModel(IMpModel* model)
{
    this->eventLogModel.append(model);
}

/**
 * @brief Node::addInstantLogModel Add instant model
 * @param model The model
 */
void Node::addInstantLogModel(IMpModel* model)
{
    this->instantLogModel.append(model);
}

/**
 * @brief Node::hasInfoChanged Has info changed
 */
bool Node::hasInfoChanged()
{
    if (this->infoModel == nullptr)
        return false;

    QJsonObject payload;
    if (this->infoModel->getId() == BmInfoHandler::ID)
    {
        BmInfoModel* infoModel = (BmInfoModel*)this->infoModel;

        payload.insert("mui", QString::number(infoModel->getMUI()));
        payload.insert("bId", (qint64)infoModel->getBId());
        payload.insert("bmType", infoModel->getBMType());
        payload.insert("bmStatus", infoModel->getBMStatus());
        payload.insert("bmError", infoModel->getBMError());
        payload.insert("histLogInMem", infoModel->getHistLogInMem());
        payload.insert("histLogIndex", (qint64)infoModel->getHistLogIndex());
        payload.insert("histLogDate", (qint64)infoModel->getHistLogDate());
        payload.insert("evtLogInMem", (qint64)infoModel->getEvtLogInMem());
        payload.insert("evtLogIndex", (qint64)infoModel->getEvtLogIndex());
        payload.insert("evtLogDate", (qint64)infoModel->getEvtLogDate());
        payload.insert("instLogInMem", (qint64)infoModel->getInstLogInMem());
        payload.insert("instLogIndex", (qint64)infoModel->getInstLogIndex());
        payload.insert("fwVer", (qint64)infoModel->getFwVer());
        payload.insert("fwType", (qint64)infoModel->getFwType());
    }
    else if (this->infoModel->getId() == CmInfoHandler::ID)
    {
        CmInfoModel* infoModel = (CmInfoModel*)this->infoModel;

        payload.insert("mui", QString::number(infoModel->getMUI()));
        payload.insert("cId", (qint64)infoModel->getCId());
        payload.insert("histLogInMem", infoModel->getHistLogInMem());
        payload.insert("histLogIndex", (qint64)infoModel->getHistLogIndex());
        payload.insert("histLogDate", (qint64)infoModel->getHistLogDate());
        payload.insert("evtLogInMem", infoModel->getEvtLogInMem());
        payload.insert("evtLogIndex", (qint64)infoModel->getEvtLogIndex());
        payload.insert("evtLogDate", (qint64)infoModel->getEvtLogDate());
        payload.insert("instLogInMem", infoModel->getInstLogInMem());
        payload.insert("instLogIndex", (qint64)infoModel->getInstLogIndex());
        payload.insert("chalgErrorSum", infoModel->getChalgErrorSum());
        payload.insert("reguErrorSum", infoModel->getReguErrorSum());
        payload.insert("fwVer", (qint64)infoModel->getFwVer());
        payload.insert("fwType", (qint64)infoModel->getFwType());
        payload.insert("u16Spare1", infoModel->getU16Spare1());
        payload.insert("u8Spare1", infoModel->getU8Spare1());
    }

    QString hash = QString(QCryptographicHash::hash(QJsonDocument(payload).toJson(), QCryptographicHash::Md5).toHex());
    if (this->previousInfoHash == hash)
        return false;

    this->previousInfoHash = hash;

    return true;
}

/**
 * @brief Node::makeDevicePingRequest Make device ping request
 * @return The request package
 */
RequestPackage* Node::makeDevicePingRequest()
{
    if (!isInfoReady())
        return nullptr;

    return new RequestPackage(QString("/api/v1/devices/%1/ping").arg(QString::number(this->mui)), QJsonObject());
}

/**
 * @brief Node::makeStatusRequest Make status request
 * @return The request package
 */
RequestPackage* Node::makeStatusRequest()
{
    if (!isStatusReady())
        return nullptr;

    QJsonObject payload;
    if (this->statusModel->getId() == BmStatusHandler::ID)
    {
        BmInfoModel* infoModel = (BmInfoModel*)this->infoModel;
        BmMeasModel* measModel = (BmMeasModel*)this->measModel;
        BmConfigModel* configModel = (BmConfigModel*)this->configModel;
        BmStatusModel* statusModel = (BmStatusModel*)this->statusModel;

        calculateUtcOffset(statusModel->getTime());

        payload.insert("panId", this->panId);
        payload.insert("channel", this->channel);
        payload.insert("stateOfCharge", statusModel->getSOC());
        payload.insert("deviceTime", QDateTime::fromSecsSinceEpoch(statusModel->getTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
        payload.insert("batteryCurrent", measModel->getIBatt() / 1000.0);
        payload.insert("batteryVoltage", (qint64)measModel->getUBatt() / 1000.0);
        payload.insert("batteryTemperature", ((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(statusModel->getTBatt() / 10.0) : statusModel->getTBatt() / 10.0);
        payload.insert("batteryTemperatureAverage", ((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(statusModel->getAvgTBatt() / 10.0) : statusModel->getAvgTBatt() / 10.0);
        payload.insert("actualCapacity", statusModel->getActualCapacity());
        payload.insert("ahLeft", (qint64)statusModel->getAhLeft());
        payload.insert("timeLeft", statusModel->getTimeLeft());
        payload.insert("cycles2To25", statusModel->getCycles2To25());
        payload.insert("cycles26To50", statusModel->getCycles26To50());
        payload.insert("cycles51To80", statusModel->getCycles51To80());
        payload.insert("cycles81To90", statusModel->getCycles81To90());
        payload.insert("cyclesAbove90", statusModel->getCyclesAbove90());
        payload.insert("stoppedCycles", statusModel->getStoppedCycles());
        payload.insert("cyclesLeft", statusModel->getCyclesLeft());
        payload.insert("totalDischargeTime", (qint64)statusModel->getDTimeTotal());
        payload.insert("totalDischargeAh", (qint64)statusModel->getDAhTotal());
        payload.insert("totalDischargeWh", (qint64)statusModel->getDWhTotal());
        payload.insert("totalChargeTime", (qint64)statusModel->getCTimeTotal());
        payload.insert("totalChargeAh", (qint64)statusModel->getCAhTotal());
        payload.insert("totalChargeWh", (qint64)statusModel->getCWhTotal());
        payload.insert("totalChargeWhAc", (qint64)statusModel->getCWhACTotal());
        payload.insert("ebUnits", statusModel->getEBUnits());
        payload.insert("historyLogCount", infoModel->getHistLogInMem());
        payload.insert("historyLogIndex", (qint64)infoModel->getHistLogIndex());
        payload.insert("historyLogDate", QDateTime::fromSecsSinceEpoch(infoModel->getHistLogDate(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
        payload.insert("eventLogCount", (qint64)infoModel->getEvtLogInMem());
        payload.insert("eventLogIndex", (qint64)infoModel->getEvtLogIndex());
        payload.insert("instantLogCount", (qint64)infoModel->getInstLogInMem());
        payload.insert("instantLogIndex", (qint64)infoModel->getInstLogIndex());
        payload.insert("middleVoltage", (qint64)measModel->getUBattCenter() / 1000.0);
        payload.insert("electrolyteVoltage", (qint64)measModel->getUElectrolyte() / 1000.0);
        payload.insert("activeAlarms", QJsonArray::fromVariantList(translateBMAlarms(statusModel->getBMError())));
        payload.insert("eventLogDate", QDateTime::fromSecsSinceEpoch(infoModel->getEvtLogDate(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
        payload.insert("deviceStatus", QJsonArray::fromVariantList(translateBMStatus(statusModel->getBMStatus())));
        payload.insert("serialNumber", QString::number(infoModel->getSerialNumber()));
        payload.insert("timestamp", QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ"));

        QJsonObject main;
        main.insert("type", (qint64)infoModel->getFwType());
        main.insert("version", (qint64)infoModel->getFwVer());

        QVariantMap map;
        map.insert("main", main);

        payload.insert("firmware", QJsonObject::fromVariantMap(map));
    }
    else if (this->statusModel->getId() == CmStatusHandler::ID)
    {
        CmInfoModel* infoModel = (CmInfoModel*)this->infoModel;
        CmMeasModel* measModel = (CmMeasModel*)this->measModel;
        CmConfigModel* configModel = (CmConfigModel*)this->configModel;
        CmStatusModel* statusModel = (CmStatusModel*)this->statusModel;

        calculateUtcOffset(statusModel->getTime());

        payload.insert("deviceTime", QDateTime::fromSecsSinceEpoch(statusModel->getTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
        payload.insert("cycles2To25", statusModel->getCycklesSum2To25());
        payload.insert("cycles26To50", statusModel->getCycklesSum26To50());
        payload.insert("cycles51To80", statusModel->getCycklesSum51To80());
        payload.insert("cycles81To90", statusModel->getCycklesSum81To90());
        payload.insert("cyclesAbove90", statusModel->getCycklesSumAbove90());
        payload.insert("batteryId", (qint64)statusModel->getBId());
        payload.insert("algorithmNumber", statusModel->getAlgNo());
        payload.insert("algorithmName", statusModel->getAlgNoName());
        payload.insert("algorithmVersion", statusModel->getVersion());
        payload.insert("cells", statusModel->getCell());
        payload.insert("capacity", statusModel->getCapacity());
        payload.insert("cableResistance", statusModel->getCableRes() / 1000.0);
        payload.insert("baseLoad", statusModel->getBaseLoad() / 1000.0);
        payload.insert("activeAlarms", QJsonArray::fromVariantList(QList<QVariant>() << translateChalgAlarms(statusModel->getChalgError()) << translateReguAlarms(statusModel->getReguError())));
        payload.insert("chargerTemperature", (configModel->getLanguage() == 0x02)  ? convertToCelsius(measModel->getThs() / 10.0) : measModel->getThs() / 10.0);
        payload.insert("chargerCurrent", (qint64)measModel->getIChalg() / 1000.0);
        payload.insert("chargerVoltage", (qint64)measModel->getUChalg() / 1000.0);
        payload.insert("historyLogCount", infoModel->getHistLogInMem());
        payload.insert("historyLogIndex", (qint64)infoModel->getHistLogIndex());
        payload.insert("historyLogDate", QDateTime::fromSecsSinceEpoch(infoModel->getHistLogDate(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
        payload.insert("eventLogCount", infoModel->getEvtLogInMem());
        payload.insert("eventLogIndex", (qint64)infoModel->getEvtLogIndex());
        payload.insert("eventLogDate", QDateTime::fromSecsSinceEpoch(infoModel->getEvtLogDate(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
        payload.insert("instantLogCount", infoModel->getInstLogInMem());
        payload.insert("instantLogIndex", (qint64)infoModel->getInstLogIndex());
        payload.insert("totalChargeTime", (qint64)statusModel->getChargeTimeTotal());
        payload.insert("totalChargeAh", (qint64)statusModel->getChargedAhTotal());
        payload.insert("deviceStatus", QJsonArray::fromVariantList(QList<QVariant>() << translateChalgStatus(statusModel->getChalgStatus()) << translateReguStatus(statusModel->getReguStatus())));
        payload.insert("timestamp", QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ"));
        payload.insert("totalAcPowerConsumption", (qint64)statusModel->getPACTotal());
        payload.insert("overVoltageCount", (qint64)statusModel->getACOVP());
        payload.insert("acPowerConsumption", (qint64)measModel->getPAC());
        payload.insert("serialNumber", QString::number(infoModel->getSerialNumber()));

        QJsonObject main;
        main.insert("type", (qint64)infoModel->getFwType());
        main.insert("version", (qint64)infoModel->getFwVer());

        QJsonObject radio;
        radio.insert("type", (qint64)infoModel->getFwType());
        radio.insert("version", (qint64)infoModel->getFwVer());

        QVariantMap map;
        map.insert("main", main);
        map.insert("radio", radio);

        payload.insert("firmware", QJsonObject::fromVariantMap(map));
    }

    return new RequestPackage(QString("/api/v1/devices/%1/status").arg(QString::number(this->mui)), payload);
}

/**
 * @brief Node::makeConfigRequest Make config request
 * @return The request package
 */
RequestPackage* Node::makeConfigRequest()
{
    if (!isConfigReady())
        return nullptr;

    QJsonObject payload;
    if (this->configModel->getId() == BmConfigHandler::ID)
    {
        BmConfigModel* configModel = (BmConfigModel*)this->configModel;

        payload.insert("deviceId", (qint64)configModel->getBId());
        payload.insert("batteryType", configModel->getBatteryType());
        payload.insert("batteryTag", QString(configModel->getBMFgId().toUtf8()));
        payload.insert("bmuType", configModel->getBMType());
        payload.insert("cells", configModel->getCells());
        payload.insert("capacity", configModel->getCapacity() / 10.0);
        payload.insert("baseLoad", configModel->getBaseLoad() / 1000.0);
        payload.insert("cableResistance", configModel->getCableRes() / 1000.0);
        payload.insert("algorithmNumber", configModel->getAlgNo());
        payload.insert("cyclesAvailable", configModel->getCyclesAvailable());
        payload.insert("ahAvailable", (qint64)configModel->getAhAvailable());
        payload.insert("temperatureMax", ((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(configModel->getTMax() / 10.0) : configModel->getTMax() / 10.0);
        payload.insert("fahrenheitScale", (configModel->getBitConfig() & 0x01) == 0x01);
        payload.insert("stateOfChargeLow", configModel->getSOCThreshold());
        payload.insert("voltageBalanceLimit", configModel->getVoltageBalanceLimit() / 100.0);
        payload.insert("cellTap", configModel->getCellTap());
        payload.insert("acidSensor", (configModel->getAcidSensor() == 1) ? true : false);
        payload.insert("equalizeFunction", configModel->getEquFunc());
        payload.insert("equalizeParameter", configModel->getEquParam());
        payload.insert("wateringFunction", configModel->getWaterFunc());
        payload.insert("wateringParameter", configModel->getWaterParam());
        payload.insert("instantLogPeriod", configModel->getInstPeriod());
        payload.insert("automaticCalibration", (configModel->getBitConfig() & 0x02) == 0x02);
        payload.insert("everyDayUpdateHistoryLog", (configModel->getBitConfig() & 0x04) == 0x04);
        payload.insert("highHistoryCurrentIdleLimit", (configModel->getBitConfig() & 0x08) == 0x08);
        payload.insert("chargeEfficiency", configModel->getCEfficiency());
        payload.insert("dischargeStateThreshold", configModel->getDEl());
        payload.insert("chargeStateThreshold", configModel->getCEl());
        payload.insert("securityCode1", configModel->getSecurityCode1());
        payload.insert("securityCode2", configModel->getSecurityCode2());
        payload.insert("panId", configModel->getPanId());
        payload.insert("channel", configModel->getChannel());
        payload.insert("nodeAddress", configModel->getNwkId());
        payload.insert("ahUsed", (qint64)configModel->getAhUsed());
        payload.insert("timestamp", QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ"));
    }
    else if (this->configModel->getId() == CmConfigHandler::ID)
    {
        CmInfoModel* infoModel = (CmInfoModel*)this->infoModel;
        CmConfigModel* configModel = (CmConfigModel*)this->configModel;

        payload.insert("deviceId", QString::number(infoModel->getCId()));
        payload.insert("engineCode", (qint64)configModel->getEngineCode());
        payload.insert("algorithmNumber", configModel->getAlgNo());
        payload.insert("chargingCurves", QJsonArray()); // Implemented later...
        payload.insert("cableResistance", configModel->getCableRes() / 1000.0);
        payload.insert("capacity", configModel->getCapacity());
        payload.insert("cells", configModel->getCells());
        payload.insert("baseLoad", configModel->getBaseLoad() / 1000.0);
        payload.insert("iDcLimit", configModel->getIDCLimit());
        payload.insert("batteryType", configModel->getBatteryType());
        payload.insert("panId", configModel->getPanId());
        payload.insert("channel", configModel->getChannel());
        payload.insert("nodeAddress", configModel->getNwkId());
        payload.insert("equalizeFunction", configModel->getEqualizeFunc());
        payload.insert("equalizeParameter", configModel->getEqualizeVar());
        payload.insert("wateringFunction", configModel->getWaterFunction());
        payload.insert("wateringParameter", configModel->getWaterVar());
        payload.insert("timestamp", QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ"));
        payload.insert("securityCode2", (qint64)configModel->getSecurityCode2());
        payload.insert("securityCode1", (qint64)configModel->getSecurityCode1());
        payload.insert("powerGroup", configModel->getPowerGroup());
        payload.insert("mainsCurrentLimit", configModel->getIACLimit());
        payload.insert("mainsPowerLimit", configModel->getPACLimit());
        payload.insert("chargingVoltageLimit", configModel->getUDCLimit());
        payload.insert("displayContrast", configModel->getDisplayContrast());
        payload.insert("ledBrightnessMax", configModel->getLEDBrightnessMax());
        payload.insert("ledBrightnessDim", configModel->getLEDBrightnessDim());
        payload.insert("language", configModel->getLanguage());
        payload.insert("timeDateFormat", configModel->getTimeDateFormat());
        payload.insert("buttonF1Func", configModel->getButtonF1Func());
        payload.insert("buttonF2Func", configModel->getButtonF2Func());
        payload.insert("remoteInFunc", configModel->getRemoteInFunc());
        payload.insert("remoteOutFunc", configModel->getRemoteOutFunc());
        payload.insert("remoteOutAlarmVar", configModel->getRemoteOutAlarmVar());
        payload.insert("remoteOutPhaseVar", configModel->getRemoteOutPhaseAlarmVar());
        payload.insert("airPumpVar", configModel->getAirPumpVar());
        payload.insert("airPumpVar2", configModel->getAirPumpVar2());
        payload.insert("parallelControlFunc", configModel->getParallelControlFunc());
        payload.insert("timeRestriction", (configModel->getTimeRestriction() & 0x01) == 0x01);
        payload.insert("chargingRestrictions", parseChargingRestrictions(configModel->getChargingRestriction()));
        payload.insert("canMode", configModel->getCANMode());
        payload.insert("canNetworkControl", configModel->getCANNwkCtrl());
        payload.insert("radioMode", configModel->getRadioMode());
        payload.insert("radioNetworkSettings", configModel->getNwkSettings());
        payload.insert("backlightTime", configModel->getBacklightTime());
        payload.insert("extraChargeFunc", configModel->getExtraChargeFunc());
        payload.insert("extraChargeVar", configModel->getExtraChargeVar());
        payload.insert("extraChargeVar2", configModel->getExtraChargeVar2());
        payload.insert("extraChargeVar3", configModel->getExtraChargeVar3());
        payload.insert("airPumpAlarmLow", configModel->getAirPumpAlarmLow());
        payload.insert("airPumpAlarmHigh", configModel->getAirPumpAlarmHigh());
        payload.insert("canNodeId", configModel->getCANNodeId());
        payload.insert("pinOutSelect", parsePinOutSelect(configModel->getPinOutSelect()));
        payload.insert("pinInSelect", parsePinInSelect(configModel->getPinInSelect()));
        payload.insert("chargingMode", configModel->getChargingMode());
        payload.insert("instantLogPeriod", configModel->getInstLogSamplePeriod());
        payload.insert("bbcFunction", configModel->getBBCFunc());
        payload.insert("bbcGroup", configModel->getBBCVar());
        payload.insert("equalizeVar2", configModel->getEqualizeVar2());
        payload.insert("routing", configModel->getRouting());
        payload.insert("remoteOutBbcVar", configModel->getRemoteOutBBCVar());
        payload.insert("batteryTemperatureDefault", (configModel->getLanguage() == 0x02) ? convertToCelsius(configModel->getBatteryTemp() / 10.0) : configModel->getBatteryTemp() / 10.0);
        payload.insert("cecMode", (configModel->getBitConfig() & 0x01) == 0x01);
        payload.insert("userParamMode", (configModel->getBitConfig() & 0x02) == 0x02);
        payload.insert("quietDerate", (configModel->getBitConfig() & 0x04) == 0x04);
        payload.insert("dplMode", (configModel->getBitConfig() & 0x08) == 0x08);
        payload.insert("canBitPerSecond", configModel->getCANBps());
        payload.insert("dplPowerLimitTotal", (qint64)configModel->getDPLPacTot());
        payload.insert("dplFunction", configModel->getDPLFunc());
        payload.insert("dplPriorityFactor", configModel->getDPLPrio());
        payload.insert("dplPowerLimitDefault", configModel->getDPLPacDef());
    }

    return new RequestPackage(QString("/api/v1/devices/%1/config").arg(QString::number(this->mui)), payload);
}

/**
 * @brief Node::makeHistoryLogRequest Make history log request
 * @return The request package
 */
RequestPackage* Node::makeHistoryLogRequest()
{
    if (!isConfigReady() || this->historyLogModel.isEmpty())
        return nullptr;

    // Description:
    // Time is specified in extended ISO 8601 format. One or more <value> under values
    // can be arrays, e.g. alarm codes is an array of errors.
    //
    // Format:
    // {
    //     fields:[
    //         "field1",
    //         "field2",
    //         ...
    //     ],
    //     rows:[{
    //         index:<value>
    //         samplePeriod:<value>
    //         time:<value>
    //         values:[
    //             <value>,
    //             <value>,
    //             ...
    //         ]
    //     }],
    // }

    QJsonObject payload;
    if (this->historyLogModel.at(0)->getId() == BmLogHandler::ID)
    {
        BmConfigModel* configModel = (BmConfigModel*)this->configModel;

        QStringList fields;
        fields << "algorithmNumber"
               << "algorithmName"
               << "algorithmVersion"
               << "cableResistance"
               << "baseLoad"
               << "capacity"
               << "cells"
               << "dischargeBatteryTemperatureStart"
               << "dischargeBatteryTemperatureEnd"
               << "dischargeBatteryTemperatureMax"
               << "dischargeBatteryTemperatureMin"
               << "dischargeVpcStart"
               << "dischargeVpcEnd"
               << "dischargeAh"
               << "dischargeWh"
               << "dischargeCurrentMax"
               << "dischargeVpcMin"
               << "dischargeAhI25"
               << "dischargeTimeI25"
               << "dischargeAhI15"
               << "dischargeTimeI15"
               << "dischargeAhI0"
               << "dischargeTimeI0"
               << "dischargeRegeneratedAh"
               << "dischargeRegeneratedWh"
               << "dischargeStateOfChargeStart"
               << "dischargeStateOfChargeEnd"
               << "dischargeRegeneratingTime"
               << "chargeTimeStart"
               << "chargeTimeEnd"
               << "chargeVpcStart"
               << "chargeVpcEnd"
               << "chargeBatteryTemperatureStart"
               << "chargeBatteryTemperatureEnd"
               << "chargeBatteryTemperatureMax"
               << "chargeBatteryTemperatureMin"
               << "chargeAh"
               << "chargeWh"
               << "chargeCurrentMax"
               << "chargeVpcMax"
               << "chargeAhI25"
               << "chargeTimeI25"
               << "chargeAhI15"
               << "chargeTimeI15"
               << "chargeAhI0"
               << "chargeTimeI0"
               << "chargeStateOfChargeStart"
               << "chargeStateOfChargeEnd"
               << "equalizeTime"
               << "equalizeAh"
               << "equalizeWh"
               << "activeAlarms"
               << "deviceStatus";

        QJsonArray rows;
        for (IMpModel* model : this->historyLogModel)
        {
            BmHistoryLogModel* historyLogModel = dynamic_cast<BmHistoryLogModel*>(model);

            QJsonArray values;
            values.append(historyLogModel->getAlgNo());
            values.append(historyLogModel->getAlgNoName());
            values.append(historyLogModel->getAlgNoVer());
            values.append(historyLogModel->getCableRes() / 1000.0);
            values.append(historyLogModel->getBaseLoad() / 1000.0);
            values.append(historyLogModel->getCapacity() / 10.0);
            values.append(historyLogModel->getCells());
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getDStartTBatt() / 10.0) : historyLogModel->getDStartTBatt() / 10.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getCStartTBatt() / 10.0) : historyLogModel->getCStartTBatt() / 10.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getDMaxTBatt() / 10.0) : historyLogModel->getDMaxTBatt() / 10.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getDMinTBatt() / 10.0) : historyLogModel->getDMinTBatt() / 10.0);
            values.append(historyLogModel->getDStartVPC() / 100.0);
            values.append(historyLogModel->getCStartVPC() / 100.0);
            values.append(historyLogModel->getDAh());
            values.append((qint64)historyLogModel->getDWh());
            values.append(historyLogModel->getDIMax() / 10.0);
            values.append(historyLogModel->getDUMin() / 100.0);
            values.append(historyLogModel->getDAhI25());
            values.append((qint64)historyLogModel->getDTimeI25());
            values.append(historyLogModel->getDAhI15());
            values.append((qint64)historyLogModel->getDTimeI15());
            values.append(historyLogModel->getDAhI0());
            values.append((qint64)historyLogModel->getDTimeI0());
            values.append(historyLogModel->getDRegAh());
            values.append(historyLogModel->getDRegWh());
            values.append(historyLogModel->getDStartSOC());
            values.append(historyLogModel->getCStartSOC());
            values.append((qint64)historyLogModel->getDRegTime());
            values.append(QDateTime::fromSecsSinceEpoch(historyLogModel->getCStartTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            values.append(QDateTime::fromSecsSinceEpoch(historyLogModel->getCEndTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            values.append(historyLogModel->getCStartVPC() / 100.0);
            values.append(historyLogModel->getCEndVPC() / 100.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getCStartTBatt() / 10.0) : historyLogModel->getCStartTBatt() / 10.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getCEndTBatt() / 10.0) : historyLogModel->getCEndTBatt() / 10.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getCMaxTBatt() / 10.0) : historyLogModel->getCMaxTBatt() / 10.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius(historyLogModel->getCMinTBatt() / 10.0) : historyLogModel->getCMinTBatt() / 10.0);
            values.append(historyLogModel->getCAh());
            values.append((qint64)historyLogModel->getCWh());
            values.append(historyLogModel->getCIMax() / 10.0);
            values.append(historyLogModel->getCUMax() / 100.0);
            values.append(historyLogModel->getCAhI25());
            values.append((qint64)historyLogModel->getCTimeI25());
            values.append(historyLogModel->getCAhI15());
            values.append((qint64)historyLogModel->getCTimeI15());
            values.append(historyLogModel->getCAhI0());
            values.append((qint64)historyLogModel->getCTimeI0());
            values.append(historyLogModel->getCStartSOC());
            values.append(historyLogModel->getCEndSOC());
            values.append((qint64)historyLogModel->getETime());
            values.append(historyLogModel->getEAh());
            values.append(historyLogModel->getEWh());
            values.append(QJsonArray::fromVariantList(translateBMAlarms(historyLogModel->getBMErrorSum())));
            values.append(QJsonArray::fromVariantList(QList<QVariant>() << translateBMStatus(historyLogModel->getBMStatusSum()) << translateChalgStatus(historyLogModel->getChalgStatusSum())));

            QJsonObject object;
            object.insert("index", (qint64)historyLogModel->getCycleIndex());
            object.insert("start", QDateTime::fromSecsSinceEpoch(historyLogModel->getDStartTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("end", QDateTime::fromSecsSinceEpoch(historyLogModel->getCStartTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("values", values);

            rows.append(object);
        }

        payload.insert("fields", QJsonArray::fromStringList(fields));
        payload.insert("rows", rows);
    }
    else if (this->historyLogModel.at(0)->getId() == CmLogHandler::ID)
    {
        CmConfigModel* configModel = (CmConfigModel*)this->configModel;

        QStringList fields;
        fields << "algorithmNumber"
               << "algorithmName"
               << "algorithmVersion"
               << "cableResistance"
               << "baseLoad"
               << "capacity"
               << "cells"
               << "chargerTemperatureStart"
               << "chargerTemperatureEnd"
               << "chargerTemperatureMax"
               << "chargeVpcStart"
               << "chargeVpcEnd"
               << "chargeTime"
               << "chargeAh"
               << "chargeWh"
               << "chargeAhP"
               << "eventIndexStart"
               << "eventIndexEnd"
               << "forkLiftId"
               << "batteryId"
               << "batterySerialNumber"
               << "batteryManufacturerId"
               << "batteryType"
               << "serialNumber"
               << "chargingMode"
               << "equalizeTime"
               << "equalizeAh"
               << "equalizeWh"
               << "activeAlarms"
               << "deviceStatus";

        QJsonArray rows;
        for (IMpModel* model : this->historyLogModel)
        {
            CmHistoryLogModel* historyLogModel = dynamic_cast<CmHistoryLogModel*>(model);

            QJsonArray values;
            values.append(historyLogModel->getAlgNo());
            values.append(historyLogModel->getAlgNoName());
            values.append(QJsonValue::Null); // Not implemented in MP-ACCESS for history log.
            values.append(historyLogModel->getRi() / 1000.0);
            values.append(historyLogModel->getBaseload() / 1000.0);
            values.append(historyLogModel->getCapacity());
            values.append(historyLogModel->getCells());
            values.append((configModel->getLanguage() == 0x02) ? convertToCelsius(historyLogModel->getThsStart() / 10.0) : historyLogModel->getThsStart() / 10.0);
            values.append((configModel->getLanguage() == 0x02) ? convertToCelsius(historyLogModel->getThsEnd() / 10.0) : historyLogModel->getThsEnd() / 10.0);
            values.append((configModel->getLanguage() == 0x02) ? convertToCelsius(historyLogModel->getThsMax() / 10.0) : historyLogModel->getThsMax() / 10.0);
            values.append((qint64)historyLogModel->getStartVoltage() / 1000.0);
            values.append((qint64)historyLogModel->getEndVoltage() / 1000.0);
            values.append((qint64)historyLogModel->getChargeTime());
            values.append((qint64)historyLogModel->getChargedAh());
            values.append((qint64)historyLogModel->getChargedWh());
            values.append(historyLogModel->getChargedAhP());
            values.append((qint64)historyLogModel->getEvtIdxStart());
            values.append((qint64)historyLogModel->getEvtIdxStop());
            values.append((qint64)historyLogModel->getFId());
            values.append((qint64)historyLogModel->getBId());
            values.append((qint64)historyLogModel->getBSN());
            values.append(historyLogModel->getBMFgId());
            values.append(historyLogModel->getBatteryType());
            values.append(QString::number(historyLogModel->getSerialNo()));
            values.append(historyLogModel->getChargingMode());
            values.append((qint64)historyLogModel->getEquTime());
            values.append((qint64)historyLogModel->getEquAh());
            values.append((qint64)historyLogModel->getEquWh());
            values.append(QJsonArray::fromVariantList(QList<QVariant>() << translateChalgAlarms(historyLogModel->getChalgErrorSum()) << translateReguAlarms(historyLogModel->getReguErrorSum())));
            values.append(QJsonArray::fromVariantList(translateChalgStatus(historyLogModel->getChalgStatusSum())));

            QJsonObject object;
            object.insert("index", (qint64)historyLogModel->getChargeIndex());
            object.insert("start", QDateTime::fromSecsSinceEpoch(historyLogModel->getChargeStartTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("end", QDateTime::fromSecsSinceEpoch(historyLogModel->getChargeEndTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("values", values);

            rows.append(object);
        }

        payload.insert("fields", QJsonArray::fromStringList(fields));
        payload.insert("rows", rows);
    }

    return new RequestPackage(QString("/api/v1/devices/%1/logs/history").arg(QString::number(this->mui)), payload);
}

/**
 * @brief Node::makeEventLogRequest Make event log request
 * @return The request package
 */
RequestPackage* Node::makeEventLogRequest()
{
    if (!isInfoReady() || this->eventLogModel.isEmpty())
        return nullptr;

    // Description:
    // Time is specified in extended ISO 8601 format.
    //
    // Format:
    // {
    //     events:[{
    //         index:<value>
    //         time:<value>
    //         type:<value>
    //         values:[{
    //             <key>:<value>,
    //             <key>:<value>
    //         },{
    //             ...
    //         }]
    //     },{
    //         index:<value>
    //         time:<value>
    //         type:<value>
    //         values:[{
    //             <key>:<value>,
    //             <key>:<value>
    //         },{
    //             ...
    //         }]
    //     },{
    //     ...
    //     }]
    // }

    QJsonObject payload;
    if (this->eventLogModel.at(0)->getId() == BmLogHandler::ID)
    {
        QJsonArray events;
        for (IMpModel* model : this->eventLogModel)
        {
            BmEventLogModel* eventLogModel = dynamic_cast<BmEventLogModel*>(model);

            QJsonArray values;
            if (eventLogModel->getEventId() == 101) // BM error
            {
                QList<QVariant> alarms = translateBMAlarms((quint16)eventLogModel->getData().at(0).toUInt());

                QJsonObject object;
                object.insert("code", (alarms.count() > 0) ? (qint32)alarms.at(0).toUInt() : 0);
                object.insert("active", ((quint16)eventLogModel->getData().at(1).toUInt() > 0) ? true : false);

                // Unknown alarm, add the untranslated alarm as raw code.
                if (alarms.count() == 0)
                    object.insert("rawCode", (quint16)eventLogModel->getData().at(0).toUInt());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 102) // Startup
            {
                QJsonObject main;
                main.insert("type", (qint64)eventLogModel->getData().at(0).toUInt());
                main.insert("version", (qint64)eventLogModel->getData().at(1).toUInt());

                QVariantMap map;
                map.insert("main", main);

                QJsonObject object;
                object.insert("firmware", QJsonObject::fromVariantMap(map));

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 103) // Calibration
            {
                QJsonObject object;
                object.insert("type", translateCalibrationType((quint8)eventLogModel->getData().at(0).toUInt()));

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 104) // Parameter changed
            {
                if (eventLogModel->getData().at(0).toUInt() != eventLogModel->getData().at(1).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 26); // Algorithm number
                    object.insert("newValue", (quint8)eventLogModel->getData().at(0).toUInt());
                    object.insert("oldValue", (quint8)eventLogModel->getData().at(1).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(2).toUInt() != eventLogModel->getData().at(3).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 6); // Battery capacity
                    object.insert("newValue", (quint16)eventLogModel->getData().at(2).toUInt());
                    object.insert("oldValue", (quint16)eventLogModel->getData().at(3).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(4).toUInt() != eventLogModel->getData().at(5).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 5); // Number of cells
                    object.insert("newValue",(quint8)eventLogModel->getData().at(4).toUInt());
                    object.insert("oldValue", (quint8)eventLogModel->getData().at(5).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(6).toUInt() != eventLogModel->getData().at(7).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 25); // Cable resistance
                    object.insert("newValue", (quint8)eventLogModel->getData().at(6).toUInt());
                    object.insert("oldValue", (quint8)eventLogModel->getData().at(7).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(8).toUInt() != eventLogModel->getData().at(9).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 24); // Base load
                    object.insert("newValue", (quint16)eventLogModel->getData().at(8).toUInt());
                    object.insert("oldValue", (quint16)eventLogModel->getData().at(9).toUInt());

                    values.append(object);
                }
            }

            QJsonObject object;
            object.insert("index", (qint64)eventLogModel->getIndex());
            object.insert("timestamp", QDateTime::fromSecsSinceEpoch(eventLogModel->getTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("type", translateEventId(eventLogModel->getEventId()));
            object.insert("values", values);

            events.append(object);
        }

        payload.insert("events", events);
    }
    else if (this->eventLogModel.at(0)->getId() == CmLogHandler::ID)
    {
        QJsonArray events;
        for (IMpModel* model : this->eventLogModel)
        {
            CmEventLogModel* eventLogModel = dynamic_cast<CmEventLogModel*>(model);

            QJsonArray values;
            if (eventLogModel->getEventId() == 1) // Time set
            {
                QJsonObject object;
                object.insert("oldTime", (qint64)eventLogModel->getData().at(0).toUInt());
                object.insert("source", (quint8)eventLogModel->getData().at(1).toUInt());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 2) // Charge started - Not implemented
            {
                qWarning().noquote() << QString("Event is not supported (Id: %1)").arg(eventLogModel->getEventId());
                return nullptr;
            }
            else if (eventLogModel->getEventId() == 3) // Charge ended - Not implemented
            {
                qWarning().noquote() << QString("Event is not supported (Id: %1)").arg(eventLogModel->getEventId());
                return nullptr;
            }
            else if (eventLogModel->getEventId() == 4) // Chalg error
            {
                QList<QVariant> alarms = translateChalgAlarms((quint16)eventLogModel->getData().at(0).toUInt());

                QJsonObject object;
                object.insert("code", (alarms.count() > 0) ? (qint32)alarms.at(0).toUInt() : 0);
                object.insert("active", ((quint16)eventLogModel->getData().at(1).toUInt() > 0) ? true : false);

                // Unknown alarm, add the untranslated alarm as raw code.
                if (alarms.count() == 0)
                    object.insert("rawCode", (quint16)eventLogModel->getData().at(0).toUInt());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 5) // Regu error
            {
                QList<QVariant> alarms = translateReguAlarms((quint16)eventLogModel->getData().at(0).toUInt());

                QJsonObject object;
                object.insert("code", (alarms.count() > 0) ? (qint32)alarms.at(0).toUInt() : 0);
                object.insert("active", ((quint16)eventLogModel->getData().at(1).toUInt() > 0) ? true : false);

                // Unknown alarm, add the untranslated alarm as raw code.
                if (alarms.count() == 0)
                    object.insert("rawCode", (quint16)eventLogModel->getData().at(0).toUInt());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 6) // Parameter changed
            {
                if (eventLogModel->getData().at(0).toUInt() != eventLogModel->getData().at(1).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 26); // Algorithm number
                    object.insert("newValue", (quint8)eventLogModel->getData().at(0).toUInt());
                    object.insert("oldValue", (quint8)eventLogModel->getData().at(1).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(2).toUInt() != eventLogModel->getData().at(3).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 6); // Capacity
                    object.insert("newValue", (quint16)eventLogModel->getData().at(2).toUInt());
                    object.insert("oldValue", (quint16)eventLogModel->getData().at(3).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(4).toUInt() != eventLogModel->getData().at(5).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 5); // Cells
                    object.insert("newValue",(quint8)eventLogModel->getData().at(4).toUInt());
                    object.insert("oldValue", (quint8)eventLogModel->getData().at(5).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(6).toUInt() != eventLogModel->getData().at(7).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 25); // Cable resistance
                    object.insert("newValue", (quint8)eventLogModel->getData().at(6).toUInt());
                    object.insert("oldValue", (quint8)eventLogModel->getData().at(7).toUInt());

                    values.append(object);
                }
                if (eventLogModel->getData().at(8).toUInt() != eventLogModel->getData().at(9).toUInt())
                {
                    QJsonObject object;
                    object.insert("index", 24); // Base load
                    object.insert("newValue", (quint16)eventLogModel->getData().at(8).toUInt());
                    object.insert("oldValue", (quint16)eventLogModel->getData().at(9).toUInt());

                    values.append(object);
                }
            }
            else if (eventLogModel->getEventId() == 7) // VCC - Not implemented
            {
                qWarning().noquote() << QString("Event is not supported (Id: %1)").arg(eventLogModel->getEventId());
                return nullptr;
            }
            else if (eventLogModel->getEventId() == 8) // Voltage calibration
            {
                QJsonObject object;
                object.insert("type", "voltage"); // Charger voltage
                object.insert("newSlope", eventLogModel->getData().at(0).toFloat());
                object.insert("oldSlope", eventLogModel->getData().at(1).toFloat());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 9) // Current calibration
            {
                QJsonObject object;
                object.insert("type", "current"); // Charger current
                object.insert("newSlope", eventLogModel->getData().at(0).toFloat());
                object.insert("oldSlope", eventLogModel->getData().at(1).toFloat());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 10) // Start network - Not implemented
            {
                qWarning().noquote() << QString("Event is not supported (Id: %1)").arg(eventLogModel->getEventId());
                return nullptr;
            }
            else if (eventLogModel->getEventId() == 11) // Join network - Not implemented
            {
                qWarning().noquote() << QString("Event is not supported (Id: %1)").arg(eventLogModel->getEventId());
                return nullptr;
            }
            else if (eventLogModel->getEventId() == 12) // Startup
            {
                QJsonObject main;
                main.insert("type", (qint64)eventLogModel->getData().at(0).toUInt());
                main.insert("version", (qint64)eventLogModel->getData().at(1).toUInt());

                QJsonObject radio;
                radio.insert("type", (qint64)eventLogModel->getData().at(2).toUInt());
                radio.insert("version", (qint64)eventLogModel->getData().at(3).toUInt());

                QVariantMap map;
                map.insert("main", main);
                map.insert("radio", radio);

                QJsonObject object;
                object.insert("firmware", QJsonObject::fromVariantMap(map));

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 13) // Settings - Not implemented
            {
                qWarning().noquote() << QString("Event is not supported (Id: %1)").arg(eventLogModel->getEventId());
                return nullptr;
            }
            else if (eventLogModel->getEventId() == 14) // Assert
            {
                QJsonObject object;
                object.insert("index", eventLogModel->getEventId());
                object.insert("id", (quint16)eventLogModel->getData().at(0).toUInt());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 15) // Sup error
            {
                QList<QVariant> alarms = translateSupAlarms((quint16)eventLogModel->getData().at(0).toUInt());

                QJsonObject object;
                object.insert("code", (alarms.count() > 0) ? (qint32)alarms.at(0).toUInt() : 0);
                object.insert("active", ((quint16)eventLogModel->getData().at(1).toUInt() > 0) ? true : false);

                // Unknown alarm, add the untranslated alarm as raw code.
                if (alarms.count() == 0)
                    object.insert("rawCode", (quint16)eventLogModel->getData().at(0).toUInt());

                values.append(object);
            }
            else if (eventLogModel->getEventId() == 16) // Ri slope
            {
                QJsonObject object;
                object.insert("newSlope", (qint64)eventLogModel->getData().at(0).toUInt());
                object.insert("oldSlope", (qint64)eventLogModel->getData().at(1).toUInt());

                values.append(object);
            }

            QJsonObject object;
            object.insert("index", (qint64)eventLogModel->getIndex());
            object.insert("timestamp", QDateTime::fromSecsSinceEpoch(eventLogModel->getTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("type", translateEventId(eventLogModel->getEventId()));
            object.insert("values", values);

            events.append(object);
        }

        payload.insert("events", events);
    }

    return new RequestPackage(QString("/api/v1/devices/%1/logs/event").arg(QString::number(this->mui)), payload);
}

/**
 * @brief Node::makeInstantLogRequest Make instant log request
 * @return The request package
 */
RequestPackage* Node::makeInstantLogRequest()
{
    if (!isConfigReady() || this->instantLogModel.isEmpty())
        return nullptr;

    // Description:
    // Time is specified in extended ISO 8601 format. One or more <value> under values
    // can be arrays, e.g. alarm codes is an array of errors.
    //
    // Format:
    // {
    //     fields:[
    //         "field1",
    //         "field2",
    //         ...
    //     ],
    //     rows:[{
    //         index:<value>
    //         samplePeriod:<value>
    //         time:<value>
    //         values:[
    //             <value>,
    //             <value>,
    //             ...
    //         ]
    //     }],
    // }

    QJsonObject payload;
    if (this->instantLogModel.at(0)->getId() == BmLogHandler::ID)
    {
        BmConfigModel* configModel = (BmConfigModel*)this->configModel;

        QStringList fields;
        fields << "currentAverage"
               << "currentMin"
               << "currentMax"
               << "voltageAverage"
               << "voltageMin"
               << "voltageMax"
               << "temperatureAverage"
               << "temperatureMin"
               << "temperatureMax"
               << "middleVoltageAverage"
               << "stateOfCharge";

        QJsonArray rows;
        for (IMpModel* model : this->instantLogModel)
        {
            BmInstantLogModel* instantLogModel = dynamic_cast<BmInstantLogModel*>(model);

            QJsonArray values;
            values.append(instantLogModel->getData().at(1).toInt() / 10.0);
            values.append(instantLogModel->getData().at(2).toInt() / 10.0);
            values.append(instantLogModel->getData().at(3).toInt() / 10.0);
            values.append(instantLogModel->getData().at(4).toInt() / 100.0);
            values.append(instantLogModel->getData().at(5).toInt() / 100.0);
            values.append(instantLogModel->getData().at(6).toInt() / 100.0);
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius((qint8)instantLogModel->getData().at(7).toInt()) : (qint8)instantLogModel->getData().at(7).toInt());
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius((qint8)instantLogModel->getData().at(8).toInt()) : (qint8)instantLogModel->getData().at(8).toInt());
            values.append(((configModel->getBitConfig() & 0x01) == 0x01) ? convertToCelsius((qint8)instantLogModel->getData().at(9).toInt()) : (qint8)instantLogModel->getData().at(9).toInt());
            values.append(instantLogModel->getData().at(10).toInt() / 100.0);
            values.append((quint8)instantLogModel->getData().at(11).toUInt());

            QJsonObject object;
            object.insert("index", (qint64)instantLogModel->getIndex());
            object.insert("samplePeriod", instantLogModel->getInstLogSamplePeriod());
            object.insert("timestamp", QDateTime::fromSecsSinceEpoch(instantLogModel->getTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("values", values);

            rows.append(object);
        }

        payload.insert("fields", QJsonArray::fromStringList(fields));
        payload.insert("rows", rows);
    }
    else if (this->instantLogModel.at(0)->getId() == CmLogHandler::ID)
    {
        CmConfigModel* configModel = (CmConfigModel*)this->configModel;

        QStringList fields;
        fields << "chargerVoltage"
               << "chargerCurrent"
               << "chargerTemperature"
               << "activeAlarms";

        QJsonArray rows;
        for (IMpModel* model : this->instantLogModel)
        {
            CmInstantLogModel* instantLogModel = dynamic_cast<CmInstantLogModel*>(model);

            QJsonArray values;
            values.append(instantLogModel->getData().at(3).toInt() / 1000.0);
            values.append(instantLogModel->getData().at(4).toInt() / 1000.0);
            values.append((configModel->getLanguage() == 0x02)  ? convertToCelsius((qint16)instantLogModel->getData().at(6).toInt() / 10.0) : (qint16)instantLogModel->getData().at(6).toInt() / 10.0);
            values.append(QJsonArray::fromVariantList(QList<QVariant>() << translateChalgAlarms((quint16)instantLogModel->getData().at(1).toUInt()) << translateReguAlarms((quint16)instantLogModel->getData().at(2).toUInt())));

            QJsonObject object;
            object.insert("index", (qint32)instantLogModel->getIndex());
            object.insert("samplePeriod", instantLogModel->getInstLogSamplePeriod());
            object.insert("timestamp", QDateTime::fromSecsSinceEpoch(instantLogModel->getTime(), Qt::OffsetFromUTC).toString("yyyy-MM-ddThh:mm:ss.zzz") + this->utcOffset);
            object.insert("values", values);

            rows.append(object);
        }

        payload.insert("fields", QJsonArray::fromStringList(fields));
        payload.insert("rows", rows);
    }

    return new RequestPackage(QString("/api/v1/devices/%1/logs/instant").arg(QString::number(this->mui)), payload);
}

/**
 * @brief Node::translateCalibrationType Translate calibration type to human readable form
 * @param id The event id
 */
QString Node::translateCalibrationType(const quint8 type)
{
    if (type == 1) return "voltage";
    else if (type == 2) return "middleVoltage";
    else if (type == 3) return "electrolyte";
    else if (type == 4) return "current";
    else
    {
        qWarning().noquote() << QString("Calibration type is not supported for translation (Type: %1)").arg(type);
        return "";
    }
}

/**
 * @brief Node::translateEventId Translate event id to human readable form
 * @param id The event id
 */
QString Node::translateEventId(const quint16 id)
{
    if (id == 1) return "3";            // Time set
    else if (id == 2) return "7";       // Charge started - Not implemented
    else if (id == 3) return "8";       // Charge ended - Not implemented
    else if (id == 4) return "0";       // Chalg error
    else if (id == 5) return "0";       // Regu error
    else if (id == 6) return "2";       // Curve data
    else if (id == 7) return "9";       // VCC - Not implemented
    else if (id == 8) return "4";       // Voltage calibration
    else if (id == 9) return "4";       // Current calibration
    else if (id == 10) return "10";     // Start network - Not implemented
    else if (id == 11) return "11";     // Join network - Not implemented
    else if (id == 12) return "1";      // Startup
    else if (id == 13) return "2";      // Settings
    else if (id == 14) return "12";     // Assert
    else if (id == 15) return "0";      // Sup error
    else if (id == 16) return "5";      // Ri slope
    else if (id == 101) return "0";     // BM error
    else if (id == 102) return "1";     // Startup
    else if (id == 103) return "4";     // Calibration
    else if (id == 104) return "2";     // Parameter changed
    else
    {
        qWarning().noquote() << QString("Event is not supported for translation (Id: %1)").arg(id);
        return "";
    }
}

/**
 * @brief Node::translateSupAlarms Translate active alarms
 * @param errors The error
 * @return The active error list
 */
QList<QVariant> Node::translateSupAlarms(const quint16 error)
{
    QList<QVariant> list;

    if ((error & 0x0010) == 0x0010) list << 34;  // Battery stage 1 initialization fail
    if ((error & 0x0100) == 0x0100) list << 36;  // Firmware malfunction
    if ((error & 0x0200) == 0x0200) list << 37;  // Radio error
    if ((error & 0x0400) == 0x0400) list << 34;  // Battery initialization fail
    if ((error & 0x0800) == 0x0800) list << 33;  // Battery algorithm error
    if ((error & 0x1000) == 0x1000) list << 35;  // Charger address conflict
    if ((error & 0x2000) == 0x2000) list << 106; // BM address conflict
    if ((error & 0x4000) == 0x4000) list << 43;  // DPL no master connection
    if ((error & 0x8000) == 0x8000) list << 44;  // DPL master conflict

    return list;
}

/**
 * @brief Node::translateBMAlarms Translate active alarms
 * @param errors The error
 * @return The active error list
 */
QList<QVariant> Node::translateBMAlarms(const quint16 error)
{
    QList<QVariant> list;

    if ((error & 0x0001) == 0x0001) list << 101;  // High battery temp
    if ((error & 0x0002) == 0x0002) list << 102;  // Low electrolyte level
    if ((error & 0x0004) == 0x0004) list << 103;  // Voltage balancing error
    if ((error & 0x0008) == 0x0008) list << 104;  // Time not set
    if ((error & 0x0010) == 0x0010) list << 105;  // Low SOC
    if ((error & 0x0010) == 0x0020) list << 106;  // Address confilict
    if ((error & 0x8000) == 0x8000) list << 116;  // HW or SW error

    return list;
}

/**
 * @brief Node::translateBMStatus Translate device status
 * @param status The status
 * @return The device status list
 */
QList<QVariant> Node::  translateBMStatus(const quint16 status)
{
    QList<QVariant> list;

    if ((status & 0x0002) == 0x0002) list << 1;   // Charging
    if ((status & 0x0004) == 0x0004) list << 7;   // Connected to a radio network
    if ((status & 0x0008) == 0x0008) list << 8;   // Radio joining
    if ((status & 0x0010) == 0x0010) list << 9;   // Radio starting
    if ((status & 0x0020) == 0x0020) list << 10;  // Radio join enable
    if ((status & 0x0040) == 0x0040) list << 2;   // Equalize function
    if ((status & 0x0080) == 0x0080) list << 3;   // Need equalizing
    if ((status & 0x0100) == 0x0100) list << 4;   // Watering
    if ((status & 0x0200) == 0x0200) list << 5;   // Need watering
    if ((status & 0x0400) == 0x0400) list << 6;   // Charger connected
    if ((status & 0x0800) == 0x0800) list << 28;  // Need calibration
    if ((status & 0x1000) == 0x1000) list << 29;  // Shunt temperature connected

    return list;
}

/**
 * @brief Node::translateChalgAlarms Translate active alarms
 * @param errors The error
 * @return The active error list
 */
QList<QVariant> Node::translateChalgAlarms(const quint16 error)
{
    QList<QVariant> list;

    if ((error & 0x0001) == 0x0001) list << 1;    // Low battery voltage
    if ((error & 0x0002) == 0x0002) list << 2;    // High battery voltage
    if ((error & 0x0004) == 0x0004) list << 3;    // Charge time limit
    if ((error & 0x0008) == 0x0008) list << 4;    // Charge Ah limit
    if ((error & 0x0010) == 0x0010) list << 5;    // Incorrect charge algorithm
    if ((error & 0x0020) == 0x0020) list << 6;    // Tilt sensor error
    if ((error & 0x0040) == 0x0040) list << 7;    // High battery voltage, poCharge endedwer unit shut off
    if ((error & 0x0080) == 0x0080) list << 8;    // Battery error (delta U/I/P)
    if ((error & 0x0100) == 0x0100) list << 101;  // BM high battery temp
    if ((error & 0x0200) == 0x0200) list << 102;  // BM low electrolyte level
    if ((error & 0x0400) == 0x0400) list << 103;  // BM battery voltage not in balance
    if ((error & 0x0800) == 0x0800) list << 9;    // Low air pump pressure
    if ((error & 0x1000) == 0x1000) list << 10;   // High air pump pressure
    if ((error & 0x2000) == 0x2000) list << 105;  // BM low State Of Charge

    return list;
}

/**
 * @brief Node::translateChalgStatus Translate device status
 * @param status The status
 * @return The device status list
 */
QList<QVariant> Node::translateChalgStatus(const quint16 status)
{
    QList<QVariant> list;

    if ((status & 0x0001) == 0x0001) list << 16;  // Mains connected
    if ((status & 0x0002) == 0x0002) list << 13;  // Battery connected
    if ((status & 0x0004) == 0x0004) list << 17;  // BM connected
    if ((status & 0x0008) == 0x0008) list << 18;  // Main charging
    if ((status & 0x0010) == 0x0010) list << 19;  // Additional chargingCharge ended
    if ((status & 0x0020) == 0x0020) list << 2;   // Equalize charging
    if ((status & 0x0040) == 0x0040) list << 20;  // Maintenance charging
    if ((status & 0x0080) == 0x0080) list << 21;  // Charing completed
    if ((status & 0x0100) == 0x0100) list << 14;  // Pause
    if ((status & 0x0200) == 0x0200) list << 22;  // Pre Charging
    if ((status & 0x0400) == 0x0400) list << 23;  // Force start
    if ((status & 0x0800) == 0x0800) list << 27;  // BBC
    if ((status & 0x1000) == 0x1000) list << 24;  // Low battery temp
    if ((status & 0x2000) == 0x2000) list << 25;  // High battery temp
    if ((status & 0x4000) == 0x4000) list << 26;  // Charging restricted

    return list;
}

/**
 * @brief Node::translateReguAlarms Translate active alarms
 * @param errors The error
 * @return The active error list
 */
QList<QVariant> Node::translateReguAlarms(const quint16 error)
{
    QList<QVariant> list;

    // Bit 0-7 are errors, bit 8-15 are warnings. Warnings are
    // errors sent from slaves to master via CAN.

    if ((error & 0x0001) == 0x0001) list << 17;   // Phase error
    if ((error & 0x0002) == 0x0002) list << 18;   // Regulator errorCharge ended
    if ((error & 0x0004) == 0x0004) list << 19;   // Low heat sink temperature
    if ((error & 0x0008) == 0x0008) list << 20;   // High heat sink temperature
    if ((error & 0x0010) == 0x0010) list << 21;   // Low board temperature
    if ((error & 0x0020) == 0x0020) list << 22;   // High board temperature
    if ((error & 0x0040) == 0x0040) list << 23;   // High trafo temperature
    if ((error & 0x0080) == 0x0080) list << 24;   // CAN timeout

    if ((error & 0x0100) == 0x0100) list << 25;   // Phase error
    if ((error & 0x0200) == 0x0200) list << 26;   // Regulator error
    if ((error & 0x0400) == 0x0400) list << 27;   // Low heat sink temperature
    if ((error & 0x0800) == 0x0800) list << 28;   // High heat sink temperature
    if ((error & 0x1000) == 0x1000) list << 29;   // Low board temperature
    if ((error & 0x2000) == 0x2000) list << 30;   // High board temperature
    if ((error & 0x4000) == 0x4000) list << 31;   // High trafo temperature
    if ((error & 0x8000) == 0x8000) list << 32;   // CAN timeout

    return list;
}

/**
 * @brief Node::translateReguStatus Translate device status
 * @param status The status
 * @return The device status list
 */
QList<QVariant> Node::translateReguStatus(const quint16 status)
{
    QList<QVariant> list;

    if ((status & 0x0001) == 0x0001) list << 37;  // Voltage is near set value
    if ((status & 0x0002) == 0x0002) list << 38;  // Current is near set value
    if ((status & 0x0004) == 0x0004) list << 39;  // Power is near set value

    quint8 limit = (status & 0x00FF) >> 3;
    if (limit == 1) list << 40;  // Limit set voltage
    if (limit == 2) list << 41;  // Limit set current
    if (limit == 3) list << 42;  // Limit set power
    if (limit == 4) list << 43;  // Limit temperature
    if (limit == 5) list << 44;  // Limit engine voltage
    if (limit == 6) list << 45;  // Limit engine current
    if (limit == 7) list << 46;  // Limit engine power
    if (limit == 8) list << 47;  // Limit charging current
    if (limit == 9) list << 48;  // Limit mains current
    if (limit == 10) list << 49;  // Limit mains power
    if (limit == 11) list << 50;  // Limit phase

    if ((status & 0x0100) == 0x0100) list << 30;  // Phase derate
    if ((status & 0x0200) == 0x0200) list << 31;  // User param derate
    if ((status & 0x0400) == 0x0400) list << 32;  // Engine derate
    if ((status & 0x0800) == 0x0800) list << 33;  // Temperature derate
    if ((status & 0x1000) == 0x1000) list << 34;  // No load derate
    if ((status & 0x2000) == 0x2000) list << 35;  // Low voltage derate
    if ((status & 0x4000) == 0x4000) list << 36;  // High voltage derate

    return list;
}

/**
 * @brief Node::parseChargingRestrictions Parse charging restrictions
 * @param chargingRestrictions Active charging restrictions
 */
QJsonArray Node::parseChargingRestrictions(QVector<quint8> chargingRestrictions)
{
    QJsonArray restrictions;

    // Format description per day:
    //
    // chargingRestrictions[i + 0] => Ctrl, indicate if charging restriction is active for this block.
    // chargingRestrictions[i + 1] => From hour, indicate from which hour this charging restriction applies.
    // chargingRestrictions[i + 2] => From minute, indicate from which minute this charging restriction applies.
    // chargingRestrictions[i + 3] => To hour, indicate to which hour this charging restriction applies.
    // chargingRestrictions[i + 4] => To minute, indicate to which minute this charging restriction applies.
    //
    // chargingRestrictions[i + 5] => Ctrl, indicate if charging restriction is active for this block.
    // chargingRestrictions[i + 6] => From hour, indicate from which hour this charging restriction applies.
    // chargingRestrictions[i + 7] => From minute, indicate from which minute this charging restriction applies.
    // chargingRestrictions[i + 8] => To hour, indicate to which hour this charging restriction applies.
    // chargingRestrictions[i + 9] => To minute, indicate to which minute this charging restriction applies.
    //
    // chargingRestrictions[i + 10] => Ctrl, indicate if charging restriction is active for this block.
    // chargingRestrictions[i + 11] => From hour, indicate from which hour this charging restriction applies.
    // chargingRestrictions[i + 12] => From minute, indicate from which minute this charging restriction applies.
    // chargingRestrictions[i + 13] => To hour, indicate to which hour this charging restriction applies.
    // chargingRestrictions[i + 14] => To minute, indicate to which minute this charging restriction applies.
    int day = 1;
    for (int i = 0; i < chargingRestrictions.count(); i += 15)
    {
        if ((chargingRestrictions[i] & 0x00001) == 0x00001)
        {
            QJsonObject restriction;
            restriction.insert("day", getChargingRestrictionDay(day));
            restriction.insert("from", QTime(chargingRestrictions[i + 1], chargingRestrictions[i + 2]).toString("hh:mm"));
            restriction.insert("to", QTime(chargingRestrictions[i + 3], chargingRestrictions[i + 4]).toString("hh:mm"));

            restrictions.append(restriction);
        }

        if ((chargingRestrictions[i + 5] & 0x00001) == 0x00001)
        {
            QJsonObject restriction;
            restriction.insert("day", getChargingRestrictionDay(day));
            restriction.insert("from", QTime(chargingRestrictions[i + 6], chargingRestrictions[i + 7]).toString("hh:mm"));
            restriction.insert("to", QTime(chargingRestrictions[i + 8], chargingRestrictions[i + 9]).toString("hh:mm"));

            restrictions.append(restriction);
        }

        if ((chargingRestrictions[i + 10] & 0x00001) == 0x00001)
        {
            QJsonObject restriction;
            restriction.insert("day", getChargingRestrictionDay(day));
            restriction.insert("from", QTime(chargingRestrictions[i + 11], chargingRestrictions[i + 12]).toString("hh:mm"));
            restriction.insert("to", QTime(chargingRestrictions[i + 13], chargingRestrictions[i + 14]).toString("hh:mm"));

            restrictions.append(restriction);
        }

        day = (day < 7) ? day + 1 : 1;
    }

    return restrictions;
}

/**
 * @brief Node::getChargingRestrictionDay Translate restriction day to human readable form
 * @param day The restriction day index
 * @return Human readable restriction day
 */
QString Node::getChargingRestrictionDay(const int day)
{
    if (day == 1) return "monday";
    else if (day == 2) return "tuesday";
    else if (day == 3) return "wednesday";
    else if (day == 4) return "thursday";
    else if (day == 5) return "friday";
    else if (day == 6) return "saturday";
    else if (day == 7) return "sunday";
    else return "Unknown";
}

/**
 * @brief Node::parsePinInSelect Parse pin in selects
 * @param pinSelect The pin selects
 */
QJsonArray Node::parsePinInSelect(QVector<quint32> pinSelect)
{
    QJsonArray pins;
    for (quint8 i = 0; i < pinSelect.size(); i++)
    {
        // Function:
        // 0 = Disabled
        // 1 = Start / Stop
        // 2 = Stop
        // 3 = High / Low
        quint8 function = static_cast<quint8>(pinSelect[i] >> 24);
        if (function > 0)
        {
            QJsonObject pin;
            pin.insert("pin", i);
            pin.insert("function", function);
            pins.append(pin);
        }
    }

    return pins;
}

/**
 * @brief Node::parsePinOutSelect Parse pin out selects
 * @param pinSelect The pin selects
 */
QJsonArray Node::parsePinOutSelect(QVector<quint32> pinSelect)
{
    QJsonArray pins;
    for (quint8 i = 0; i < pinSelect.size(); i++)
    {
        // Function:
        // 0 = Disabled
        // 1 = Start / Stop
        // 2 = Stop
        // 3 = High / Low
        quint8 function = static_cast<quint8>(pinSelect[i] >> 24);
        if (function > 0)
        {
            quint32 variable = (pinSelect[i] & 0xffffff);

            QJsonObject pin;
            pin.insert("pin", i);
            pin.insert("function", function);
            pin.insert("variable", (qint64)variable);
            pins.append(pin);
        }
    }

    return pins;
}

/**
 * @brief Node::calculateUtcOffset Calculate device time offset against UTC
 */
void Node::calculateUtcOffset(quint32 deviceTime)
{
    // We can not calculate offset when device time is zero.
    if (deviceTime == 0)
    {
        this->utcOffset = "Z";
        return;
    }

    QDateTime currentDateTime = QDateTime().currentDateTimeUtc();
    QDateTime deviceDateTime = QDateTime::fromSecsSinceEpoch(deviceTime, Qt::OffsetFromUTC);

    // Calculate the hour diff.
    qint64 diff = qRound64(currentDateTime.secsTo(deviceDateTime) / 3600.0);

    // Validate the difference.
    if (diff == 0 || diff > 14 || diff < -12) // Max offset, see for more info: https://en.wikipedia.org/wiki/List_of_UTC_time_offsets
    {
        this->utcOffset = "Z";
        return;
    }

    // Zero padding if necessary
    if (diff > 0)
        this->utcOffset = QString("+%1").arg((diff < 10) ? QString::number(diff).rightJustified(2, '0') : QString::number(diff));
    else
        this->utcOffset = QString("-%1").arg((diff > -10) ? QString::number(qAbs(diff)).rightJustified(2, '0') : QString::number(qAbs(diff)));

    // Add zero suffix.
    this->utcOffset.append("00");
}

/**
 * @brief Node::convertToCelsius Convert farenheight to celsius
 */
double Node::convertToCelsius(const quint16 temperature)
{
    return (temperature - 32) / 1.8;
}
