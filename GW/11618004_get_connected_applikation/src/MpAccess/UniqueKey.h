#pragma once

#include "Shared.h"

#include <QtCore/QString>

class MPACCESS_EXPORT UniqueKey
{
    public:
        static QString makeKey(const quint8 channel, const quint16 panId, const quint16 address) { return QString("%1,%2,%3").arg(channel).arg(panId).arg(address); }
};
