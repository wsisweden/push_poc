#include "BmEventLogModel.h"

/**
 * @brief BmEventLogModel::BmEventLogModel Construct a new BM event log model
 */
BmEventLogModel::BmEventLogModel(const quint32 logType,
                                 const quint32 index,
                                 const quint32 indexReset,
                                 const quint32 time,
                                 const quint16 eventId,
                                 const quint8 dataSize,
                                 const QList<QVariant>& data)
    : IMpModel()
{
    this->logType = logType;
    this->index = index;
    this->indexReset = indexReset;
    this->time = time;
    this->eventId = eventId;
    this->dataSize = dataSize;
    this->data = data;
}
