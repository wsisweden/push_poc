#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/BmMeasHandler.h"

class MPACCESS_EXPORT BmMeasModel : public IMpModel
{
    public:
        explicit BmMeasModel(const qint16 tBatt,
                             const qint32 iBatt,
                             const quint32 uBatt,
                             const quint8 soc,
                             const quint16 error,
                             const quint32 uBattCenter,
                             const quint16 status,
                             const quint32 uElectrolyte,
                             const quint32 u32Spare,
                             const quint16 u16Spare,
                             const quint8 unitScale);

        quint8 getId() const { return BmMeasHandler::ID; }

        qint16 getTBatt() { return this->tBatt; }
        qint32 getIBatt() { return this->iBatt; }
        quint32 getUBatt() { return this->uBatt; }
        quint8 getSOC() { return this->soc; }
        quint16 getError() { return this->error; }
        quint32 getUBattCenter() { return this->uBattCenter; }
        quint16 getStatus() { return this->status; }
        quint32 getUElectrolyte() { return this->uElectrolyte; }
        quint32 getU32Spare() { return this->u32Spare; }
        quint16 getU16Spare() { return this->u16Spare; }
        quint8 getUnitScale() { return this->unitScale; }

    private:
        qint16 tBatt;
        qint32 iBatt;
        quint32 uBatt;
        quint8 soc;
        quint16 error;
        quint32 uBattCenter;
        quint16 status;
        quint32 uElectrolyte;
        quint32 u32Spare;
        quint16 u16Spare;
        quint8 unitScale;
};
