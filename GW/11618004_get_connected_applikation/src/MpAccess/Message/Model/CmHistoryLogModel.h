#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/CmLogHandler.h"

class MPACCESS_EXPORT CmHistoryLogModel : public IMpModel
{
    public:
        explicit CmHistoryLogModel(const quint8 logType,
                                   const quint32 chargeIndex,
                                   const quint32 chargeIndexReset,
                                   const quint16 recordType,
                                   const quint8 recordSize,
                                   const quint32 chargeStartTime,
                                   const quint32 chargeEndTime,
                                   const quint16 algNo,
                                   const quint16 capacity,
                                   const quint16 cells,
                                   const quint16 ri,
                                   const quint16 baseload,
                                   const quint16 thsStart,
                                   const quint16 thsEnd,
                                   const quint16 thsMax,
                                   const quint32 startVoltage,
                                   const quint32 endVoltage,
                                   const quint32 chargeTime,
                                   const quint32 chargedAh,
                                   const quint32 chargedWh,
                                   const quint16 chargedAhP,
                                   const quint32 equTime,
                                   const quint32 equAh,
                                   const quint32 equWh,
                                   const quint16 chalgErrorSum,
                                   const quint16 reguErrorSum,
                                   const quint32 evtIdxStart,
                                   const quint32 evtIdxStop,
                                   const quint32 bid,
                                   const quint8 bmFgId,
                                   const quint8 batteryType,
                                   const quint32 bsn,
                                   const quint32 engineCode,
                                   const quint32 serialNo,
                                   const QString& algNoName,
                                   const quint8 chargingMode,
                                   const quint16 chalgStatusSum,
                                   const quint32 fId,
                                   const quint32 u32Spare1,
                                   const quint32 u32Spare2,
                                   const quint16 u16Spare1,
                                   const quint16 u16Spare2,
                                   const quint8 u8Spare1,
                                   const quint8 u8Spare2);

        quint8 getId() const { return CmLogHandler::ID; }

        quint8 getLogType() { return this->logType; }
        quint32 getChargeIndex() { return this->chargeIndex; }
        quint32 getChargeIndexReset() { return this->chargeIndexReset; }
        quint16 getRecordType() { return this->recordType; }
        quint8 getRecordSize() { return this->recordSize; }
        quint32 getChargeStartTime() { return this->chargeStartTime; }
        quint32 getChargeEndTime() { return this->chargeEndTime; }
        quint16 getAlgNo() { return this->algNo; }
        quint16 getCapacity() { return this->capacity; }
        quint16 getCells() { return this->cells; }
        quint16 getRi() { return this->ri; }
        quint16 getBaseload() { return this->baseload; }
        quint16 getThsStart() { return this->thsStart; }
        quint16 getThsEnd() { return this->thsEnd; }
        quint16 getThsMax() { return this->thsMax; }
        quint32 getStartVoltage() { return this->startVoltage; }
        quint32 getEndVoltage() { return this->endVoltage; }
        quint32 getChargeTime() { return this->chargeTime; }
        quint32 getChargedAh() { return this->chargedAh; }
        quint32 getChargedWh() { return this->chargedWh; }
        quint16 getChargedAhP() { return this->chargedAhP; }
        quint32 getEquTime() { return this->equTime; }
        quint32 getEquAh() { return this->equAh; }
        quint32 getEquWh() { return this->equWh; }
        quint16 getChalgErrorSum() { return this->chalgErrorSum; }
        quint16 getReguErrorSum() { return this->reguErrorSum; }
        quint32 getEvtIdxStart() { return this->evtIdxStart; }
        quint32 getEvtIdxStop() { return this->evtIdxStop; }
        quint32 getBId() { return this->bid; }
        quint8 getBMFgId() { return this->bmFgId; }
        quint8 getBatteryType() { return this->batteryType; }
        quint32 getBSN() { return this->bsn; }
        quint32 getEngineCode() { return this->engineCode; }
        quint32 getSerialNo() { return this->serialNo; }
        QString getAlgNoName() { return this->algNoName; }
        quint8 getChargingMode() { return this->chargingMode; }
        quint16 getChalgStatusSum() { return this->chalgStatusSum; }
        quint32 getFId() { return this->fId; }
        quint32 getU32Spare1() { return this->u32Spare1; }
        quint32 getU32Spare2() { return this->u32Spare2; }
        quint16 getU16Spare1() { return this->u16Spare1; }
        quint16 getU16Spare2() { return this->u16Spare2; }
        quint8 getU8Spare1() { return this->u8Spare1; }
        quint8 getU8Spare2() { return this->u8Spare2; }

    private:
        quint8 logType;
        quint32 chargeIndex;
        quint32 chargeIndexReset;
        quint16 recordType;
        quint8 recordSize;
        quint32 chargeStartTime;
        quint32 chargeEndTime;
        quint16 algNo;
        quint16 capacity;
        quint16 cells;
        quint16 ri;
        quint16 baseload;
        quint16 thsStart;
        quint16 thsEnd;
        quint16 thsMax;
        quint32 startVoltage;
        quint32 endVoltage;
        quint32 chargeTime;
        quint32 chargedAh;
        quint32 chargedWh;
        quint16 chargedAhP;
        quint32 equTime;
        quint32 equAh;
        quint32 equWh;
        quint16 chalgErrorSum;
        quint16 reguErrorSum;
        quint32 evtIdxStart;
        quint32 evtIdxStop;
        quint32 bid;
        quint8 bmFgId;
        quint8 batteryType;
        quint32 bsn;
        quint32 engineCode;
        quint32 serialNo;
        QString algNoName;
        quint8 chargingMode;
        quint16 chalgStatusSum;
        quint32 fId;
        quint32 u32Spare1;
        quint32 u32Spare2;
        quint16 u16Spare1;
        quint16 u16Spare2;
        quint8 u8Spare1;
        quint8 u8Spare2;
};
