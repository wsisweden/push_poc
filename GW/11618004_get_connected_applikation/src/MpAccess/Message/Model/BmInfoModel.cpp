#include "BmInfoModel.h"

/**
 * @brief BmInfoModel::BmInfoModel Construct a new BM info model
 */
BmInfoModel::BmInfoModel(const quint32 bId,
                         const quint64 mui,
                         const quint8 bmType,
                         const quint16 bmStatus,
                         const quint16 bmError,
                         const quint16 histLogInMem,
                         const quint32 histLogIndex,
                         const quint32 histLogDate,
                         const quint32 evtLogInMem,
                         const quint32 evtLogIndex,
                         const quint32 evtLogDate,
                         const quint32 instLogInMem,
                         const quint32 instLogIndex,
                         const quint32 fwVer,
                         const quint32 fwType,
                         const quint32 productType)
    : IMpModel()
{
    this->bId = bId;
    this->mui = mui;
    this->bmType = bmType;
    this->bmStatus = bmStatus;
    this->bmError = bmError;
    this->histLogInMem = histLogInMem;
    this->histLogIndex = histLogIndex;
    this->histLogDate = histLogDate;
    this->evtLogInMem = evtLogInMem;
    this->evtLogIndex = evtLogIndex;
    this->evtLogDate = evtLogDate;
    this->instLogInMem = instLogInMem;
    this->instLogIndex = instLogIndex;
    this->fwVer = fwVer;
    this->fwType = fwType;
    this->productType = productType;
}

/**
 * @brief BmInfoModel::translateMUI Translate MUI according to product id specification
 * @param mui The mui
 */
quint64 BmInfoModel::translateMUI(const quint64 mui)
{
    quint16 productId = (quint16)(mui >> 48);
    quint64 serialNumber = parseSerialNumber(mui);

    if (productId == 0)         // Generation 2.
        productId = 1;
    else if (productId > 255)   // Generation 1.
        productId = 8;

    return ((((quint64)productId) << 48) | serialNumber);
}

/**
 * @brief Node::parseSerialNumber Parse serial number from MUI
 * @param mui The mui
 */
quint64 BmInfoModel::parseSerialNumber(const quint64 mui = 0)
{
    return (mui & 0xffffffffffffL);
}
