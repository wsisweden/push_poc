#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/SetParamHandler.h"

#include <QtCore/QVariant>

class MPACCESS_EXPORT SetParamModel : public IMpModel
{
    public:
        explicit SetParamModel(const quint16 index, const QVariant& data);

        quint8 getId() const { return SetParamHandler::ID; }

        quint16 getIndex() const { return this->index; }
        QVariant& getData() { return this->data; }

    private:
        quint16 index;
        QVariant data;
};
