#include "CmInstantLogModel.h"

/**
 * @brief CmInstantLogModel::CmInstantLogModel Construct a new CM instant log model
 */
CmInstantLogModel::CmInstantLogModel(const quint8 logType,
                                         const quint32 index,
                                         const quint8 noOfRecords,
                                         const quint32 time,
                                         const quint16 instLogSamplePeriod,
                                         const quint16 recordType,
                                         const quint8 recordSize,
                                         const QList<QVariant>& data)
    : IMpModel()
{
    this->logType = logType;
    this->index = index;
    this->noOfRecords = noOfRecords;
    this->time = time;
    this->instLogSamplePeriod = instLogSamplePeriod;
    this->recordType = recordType;
    this->recordSize = recordSize;
    this->data = data;
}
