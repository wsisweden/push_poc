#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/CmLogHandler.h"

#include <QtCore/QList>
#include <QtCore/QVariant>

class MPACCESS_EXPORT CmInstantLogModel : public IMpModel
{
    public:
        explicit CmInstantLogModel(const quint8 logType,
                                   const quint32 index,
                                   const quint8 noOfRecords,
                                   const quint32 time,
                                   const quint16 instLogSamplePeriod,
                                   const quint16 recordType,
                                   const quint8 recordSize,
                                   const QList<QVariant>& data);

        quint8 getId() const { return CmLogHandler::ID; }

        quint32 getLogType() { return this->logType; }
        quint32 getIndex() { return this->index; }
        quint32 getNoOfRecords() { return this->noOfRecords; }
        quint32 getTime() { return this->time; }
        quint16 getInstLogSamplePeriod() { return this->instLogSamplePeriod; }
        quint8 getRecordType() { return this->recordType; }
        quint8 getRecordSize() { return this->recordSize; }
        QList<QVariant>& getData() { return this->data  ; }

    private:
        quint8 logType;
        quint32 index;
        quint8 noOfRecords;
        quint32 time;
        quint16 instLogSamplePeriod;
        quint16 recordType;
        quint8 recordSize;
        QList<QVariant> data;
};
