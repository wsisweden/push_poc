#include "SetParamModel.h"

/**
 * @brief SetParamModel::SetParamModel Construct a new set param model
 */
SetParamModel::SetParamModel(const quint16 index, const QVariant& data)
    : IMpModel()
{
    this->index = index;
    this->data = data;
}
