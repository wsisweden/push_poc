#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/BmLogHandler.h"

class MPACCESS_EXPORT BmHistoryLogModel : public IMpModel
{
    public:
        explicit BmHistoryLogModel(const quint8 logType,
                                   const quint32 cycleIndex,
                                   const quint32 cycleIndexReset,
                                   const quint16 recordType,
                                   const quint8 recordSize,
                                   const quint32 bid,
                                   const quint16 algNo,
                                   const QString& algNoName,
                                   const quint16 algNoVer,
                                   const quint16 capacity,
                                   const quint16 cableRes,
                                   const quint16 cells,
                                   const quint16 baseLoad,
                                   const quint32 dStartTime,
                                   const quint16 dStartVPC,
                                   const quint32 dRegTime,
                                   const qint16 dStartTBatt,
                                   const qint16 dMaxTBatt,
                                   const qint16 dMinTBatt,
                                   const quint16 dAh,
                                   const quint16 dRegAh,
                                   const quint32 dWh,
                                   const quint16 dRegWh,
                                   const quint8 dStartSOC,
                                   const quint16 dAhI25,
                                   const quint32 dTimeI25,
                                   const quint16 dAhI15,
                                   const quint32 dTimeI15,
                                   const quint16 dAhI0,
                                   const quint32 dTimeI0,
                                   const quint16 dIMax,
                                   const quint16 dUMin,
                                   const quint32 cStartTime,
                                   const quint32 cEndTime,
                                   const quint16 cStartVPC,
                                   const quint16 cEndVPC,
                                   const qint16 cStartTBatt,
                                   const qint16 cEndTBatt,
                                   const qint16 cMaxTBatt,
                                   const qint16 cMinTBatt,
                                   const quint16 cAh,
                                   const quint32 cWh,
                                   const quint8 cStartSOC,
                                   const quint8 cEndSOC,
                                   const quint16 cAhI25,
                                   const quint32 cTimeI25,
                                   const quint16 cAhI15,
                                   const quint32 cTimeI15,
                                   const quint16 cAhI0,
                                   const quint32 cTimeI0,
                                   const quint16 cIMax,
                                   const quint16 cUMax,
                                   const quint32 eTime,
                                   const quint16 eAh,
                                   const quint16 eWh,
                                   const quint16 bmStatusSum,
                                   const quint16 bmErrorSum,
                                   const quint32 u32Spare1,
                                   const quint32 u32Spare2,
                                   const quint16 chalgStatusSum,
                                   const quint16 u16Spare2,
                                   const quint8 u8Spare1,
                                   const quint8 u8Spare2);

        quint8 getId() const { return BmLogHandler::ID; }

        quint8 getLogType() { return this->logType; }
        quint32 getCycleIndex() { return this->cycleIndex; }
        quint32 getCycleIndexReset() { return this->cycleIndexReset; }
        quint16 getRecordType() { return this->recordType; }
        quint8 getRecordSize() { return this->recordSize; }
        quint32 getBId() { return this->bid; }
        quint16 getAlgNo() { return this->algNo; }
        QString getAlgNoName() { return this->algNoName; }
        quint16 getAlgNoVer() { return this->algNoVer; }
        quint16 getCapacity() { return this->capacity; }
        quint16 getCableRes() { return this->cableRes; }
        quint16 getCells() { return this->cells; }
        quint16 getBaseLoad() { return this->baseLoad; }
        quint32 getDStartTime() { return this->dStartTime; }
        quint16 getDStartVPC() { return this->dStartVPC; }
        quint32 getDRegTime() { return this->dRegTime; }
        qint16 getDStartTBatt() { return this->dStartTBatt; }
        qint16 getDMaxTBatt() { return this->dMaxTBatt; }
        qint16 getDMinTBatt() { return this->dMinTBatt; }
        quint16 getDAh() { return this->dAh; }
        quint16 getDRegAh() { return this->dRegAh; }
        quint32 getDWh() { return this->dWh; }
        quint16 getDRegWh() { return this->dRegWh; }
        quint8 getDStartSOC() { return this->dStartSOC; }
        quint16 getDAhI25() { return this->dAhI25; }
        quint32 getDTimeI25() { return this->dTimeI25; }
        quint16 getDAhI15() { return this->dAhI15; }
        quint32 getDTimeI15() { return this->dTimeI15; }
        quint16 getDAhI0() { return this->dAhI0; }
        quint32 getDTimeI0() { return this->dTimeI0; }
        quint16 getDIMax() { return this->dIMax; }
        quint16 getDUMin() { return this->dUMin; }
        quint32 getCStartTime() { return this->cStartTime; }
        quint32 getCEndTime() { return this->cEndTime; }
        quint16 getCStartVPC() { return this->cStartVPC; }
        quint16 getCEndVPC() { return this->cEndVPC; }
        qint16 getCStartTBatt() { return this->cStartTBatt; }
        qint16 getCEndTBatt() { return this->cEndTBatt; }
        qint16 getCMaxTBatt() { return this->cMaxTBatt; }
        qint16 getCMinTBatt() { return this->cMinTBatt; }
        quint16 getCAh() { return this->cAh; }
        quint32 getCWh() { return this->cWh; }
        quint8 getCStartSOC() { return this->cStartSOC; }
        quint8 getCEndSOC() { return this->cEndSOC; }
        quint16 getCAhI25() { return this->cAhI25; }
        quint32 getCTimeI25() { return this->cTimeI25; }
        quint16 getCAhI15() { return this->cAhI15; }
        quint32 getCTimeI15() { return this->cTimeI15; }
        quint16 getCAhI0() { return this->cAhI0; }
        quint32 getCTimeI0() { return this->cTimeI0; }
        quint16 getCIMax() { return this->cIMax; }
        quint16 getCUMax() { return this->cUMax; }
        quint32 getETime() { return this->eTime; }
        quint16 getEAh() { return this->eAh; }
        quint16 getEWh() { return this->eWh; }
        quint16 getBMStatusSum() { return this->bmStatusSum; }
        quint16 getBMErrorSum() { return this->bmErrorSum; }
        quint32 getU32Spare1() { return this->u32Spare1; }
        quint32 getU32Spare2() { return this->u32Spare2; }
        quint16 getChalgStatusSum() { return this->chalgStatusSum; }
        quint16 getU16Spare2() { return this->u16Spare2; }
        quint8 getU8Spare1() { return this->u8Spare1; }
        quint8 getU8Spare2() { return this->u8Spare2; }
    private:
        quint8 logType;
        quint32 cycleIndex;
        quint32 cycleIndexReset;
        quint16 recordType;
        quint8 recordSize;
        quint32 bid;
        quint16 algNo;
        QString algNoName;
        quint16 algNoVer;
        quint16 capacity;
        quint16 cableRes;
        quint16 cells;
        quint16 baseLoad;
        quint32 dStartTime;
        quint16 dStartVPC;
        quint32 dRegTime;
        qint16 dStartTBatt;
        qint16 dMaxTBatt;
        qint16 dMinTBatt;
        quint16 dAh;
        quint16 dRegAh;
        quint32 dWh;
        quint16 dRegWh;
        quint8 dStartSOC;
        quint16 dAhI25;
        quint32 dTimeI25;
        quint16 dAhI15;
        quint32 dTimeI15;
        quint16 dAhI0;
        quint32 dTimeI0;
        quint16 dIMax;
        quint16 dUMin;
        quint32 cStartTime;
        quint32 cEndTime;
        quint16 cStartVPC;
        quint16 cEndVPC;
        qint16 cStartTBatt;
        qint16 cEndTBatt;
        qint16 cMaxTBatt;
        qint16 cMinTBatt;
        quint16 cAh;
        quint32 cWh;
        quint8 cStartSOC;
        quint8 cEndSOC;
        quint16 cAhI25;
        quint32 cTimeI25;
        quint16 cAhI15;
        quint32 cTimeI15;
        quint16 cAhI0;
        quint32 cTimeI0;
        quint16 cIMax;
        quint16 cUMax;
        quint32 eTime;
        quint16 eAh;
        quint16 eWh;
        quint16 bmStatusSum;
        quint16 bmErrorSum;
        quint32 u32Spare1;
        quint32 u32Spare2;
        quint16 chalgStatusSum;
        quint16 u16Spare2;
        quint8 u8Spare1;
        quint8 u8Spare2;
};
