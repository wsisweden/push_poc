#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/CmLogHandler.h"

#include <QtCore/QList>
#include <QtCore/QVariant>

class MPACCESS_EXPORT CmEventLogModel : public IMpModel
{
    public:
        explicit CmEventLogModel(const quint32 logType,
                                 const quint32 index,
                                 const quint32 indexReset,
                                 const quint32 time,
                                 const quint16 eventId,
                                 const quint8 dataSize,
                                 const QList<QVariant>& data);

        quint8 getId() const { return CmLogHandler::ID; }

        quint32 getLogType() { return this->logType; }
        quint32 getIndex() { return this->index; }
        quint32 getIndexReset() { return this->indexReset; }
        quint32 getTime() { return this->time; }
        quint16 getEventId() { return this->eventId; }
        quint8 getDataSize() { return this->dataSize; }
        QList<QVariant>& getData() { return this->data  ; }

    private:
        quint32 logType;
        quint32 index;
        quint32 indexReset;
        quint32 time;
        quint16 eventId;
        quint8 dataSize;
        QList<QVariant> data;
};
