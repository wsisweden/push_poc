#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/CmInfoHandler.h"

class MPACCESS_EXPORT CmInfoModel : public IMpModel
{
    public:
        explicit CmInfoModel(const quint32 cId,
                             const quint16 histLogInMem,
                             const quint32 histLogIndex,
                             const quint32 histLogDate,
                             const quint16 evtLogInMem,
                             const quint32 evtLogIndex,
                             const quint32 evtLogDate,
                             const quint16 instLogInMem,
                             const quint32 instLogIndex,
                             const quint16 chalgErrorSum,
                             const quint16 reguErrorSum,
                             const quint64 mui,
                             const quint32 fwVer,
                             const quint32 fwType,
                             const quint16 u16Spare1,
                             const quint8 u8Spare1,
                             const quint32 productType);

        quint8 getId() const { return CmInfoHandler::ID; }

        quint32 getCId() const { return this->cId; }
        quint64 getMUI() { return translateMUI(this->mui); }
        quint16 getHistLogInMem() const { return this->histLogInMem; }
        quint32 getHistLogIndex() const { return this->histLogIndex; }
        quint32 getHistLogDate() const { return this->histLogDate; }
        quint16 getEvtLogInMem() const { return this->evtLogInMem; }
        quint32 getEvtLogIndex() const { return this->evtLogIndex; }
        quint32 getEvtLogDate() const { return this->evtLogDate; }
        quint16 getInstLogInMem() const { return this->instLogInMem; }
        quint32 getInstLogIndex() const { return this->instLogIndex; }
        quint16 getChalgErrorSum() const { return this->chalgErrorSum; }
        quint16 getReguErrorSum() const { return this->reguErrorSum; }
        quint32 getFwVer() const { return this->fwVer; }
        quint32 getFwType() const { return this->fwType; }
        quint16 getU16Spare1() const { return this->u16Spare1; }
        quint8 getU8Spare1() const { return this->u8Spare1; }
        quint32 getProductType() const { return this->productType; }

        quint64 getSerialNumber() { return parseSerialNumber(this->mui); }

    private:
        quint32 cId;
        quint16 histLogInMem;
        quint32 histLogIndex;
        quint32 histLogDate;
        quint16 evtLogInMem;
        quint32 evtLogIndex;
        quint32 evtLogDate;
        quint16 instLogInMem;
        quint32 instLogIndex;
        quint16 chalgErrorSum;
        quint16 reguErrorSum;
        quint64 mui;
        quint32 fwVer;
        quint32 fwType;
        quint16 u16Spare1;
        quint8 u8Spare1;
        quint32 productType;

        quint64 translateMUI(const quint64 mui);
        quint64 parseSerialNumber(const quint64 mui);
};
