#include "CmInfoModel.h"

/**
 * @brief CmInfoModel::CmInfoModel Construct a new CM info model
 */
CmInfoModel::CmInfoModel(const quint32 cId,
                         const quint16 histLogInMem,
                         const quint32 histLogIndex,
                         const quint32 histLogDate,
                         const quint16 evtLogInMem,
                         const quint32 evtLogIndex,
                         const quint32 evtLogDate,
                         const quint16 instLogInMem,
                         const quint32 instLogIndex,
                         const quint16 chalgErrorSum,
                         const quint16 reguErrorSum,
                         const quint64 mui,
                         const quint32 fwVer,
                         const quint32 fwType,
                         const quint16 u16Spare1,
                         const quint8 u8Spare1,
                         const quint32 productType)
    : IMpModel()
{
    this->cId = cId;
    this->histLogInMem = histLogInMem;
    this->histLogIndex = histLogIndex;
    this->histLogDate = histLogDate;
    this->evtLogInMem = evtLogInMem;
    this->evtLogIndex = evtLogIndex;
    this->evtLogDate = evtLogDate;
    this->instLogInMem = instLogInMem;
    this->instLogIndex = instLogIndex;
    this->chalgErrorSum = chalgErrorSum;
    this->reguErrorSum = reguErrorSum;
    this->mui = mui;
    this->fwVer = fwVer;
    this->fwType = fwType;
    this->u16Spare1 = u16Spare1;
    this->u8Spare1 = u8Spare1;
    this->productType = productType;
}

/**
 * @brief BmInfoModel::translateMUI Translate MUI according to product id specification
 * @param mui The mui
 */
quint64 CmInfoModel::translateMUI(const quint64 mui)
{
    quint16 productId = (quint16)(mui >> 48);
    quint64 serialNumber = parseSerialNumber(mui);

    if (productId > 255)        // Generation 1.
        productId = 9;
    else                        // Generation 2.
    {
        if (productId == 80)
            productId = 9;
    }

    return ((((quint64)productId) << 48) | serialNumber);
}

/**
 * @brief Node::parseSerialNumber Parse serial number from MUI
 * @param mui The mui
 */
quint64 CmInfoModel::parseSerialNumber(const quint64 mui = 0)
{
    return (mui & 0xffffffffffffL);
}
