#include "BmStatusModel.h"

/**
 * @brief BmStatusModel::BmStatusModel Construct a new BM status model
 */
BmStatusModel::BmStatusModel(const quint32 bId,
                             const QString& bmFgId,
                             const quint8 batteryType,
                             const quint16 capacity,
                             const quint16 cells,
                             const quint16 cycles2to25,
                             const quint16 cycles26to50,
                             const quint16 cycles51to80,
                             const quint16 cycles81to90,
                             const quint16 cyclesAbove90,
                             const quint16 stoppedCycles,
                             const quint16 cyclesLeft,
                             const quint32 ahLeft,
                             const quint32 time,
                             const quint16 timeLeft,
                             const quint32 dTimeTotal,
                             const quint32 dAhTotal,
                             const quint32 dWhTotal,
                             const quint32 cTimeTotal,
                             const quint32 cAhTotal,
                             const quint32 cWhTotal,
                             const quint32 cWhACTotal,
                             const qint16 tBatt,
                             const quint8 soc,
                             const quint16 bmError,
                             const quint16 bmStatus,
                             const qint16 avgTBatt,
                             const quint32 u32Spare1,
                             const quint16 actualCapacity,
                             const quint16 u16Spare2,
                             const quint8 ebUnits,
                             const quint8 u8Spare2)
    : IMpModel()
{
    this->bId = bId;
    this->bmFgId = bmFgId;
    this->batteryType = batteryType;
    this->capacity = capacity;
    this->cells = cells;
    this->cycles2to25 = cycles2to25;
    this->cycles26to50 = cycles26to50;
    this->cycles51to80 = cycles51to80;
    this->cycles81to90 = cycles81to90;
    this->cyclesAbove90 = cyclesAbove90;
    this->stoppedCycles = stoppedCycles;
    this->cyclesLeft = cyclesLeft;
    this->ahLeft = ahLeft;
    this->time = time;
    this->timeLeft = timeLeft;
    this->dTimeTotal = dTimeTotal;
    this->dAhTotal = dAhTotal;
    this->dWhTotal = dWhTotal;
    this->cTimeTotal = cTimeTotal;
    this->cAhTotal = cAhTotal;
    this->cWhTotal = cWhTotal;
    this->cWhACTotal = cWhACTotal;
    this->tBatt = tBatt;
    this->soc = soc;
    this->bmError = bmError;
    this->bmStatus = bmStatus;
    this->avgTBatt = avgTBatt;
    this->u32Spare1 = u32Spare1;
    this->actualCapacity = actualCapacity;
    this->u16Spare2 = u16Spare2;
    this->ebUnits = ebUnits;
    this->u8Spare2 = u8Spare2;
}
