#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/CmMeasHandler.h"

class MPACCESS_EXPORT CmMeasModel : public IMpModel
{
    public:
        explicit CmMeasModel(const quint32 pac,
                             const quint32 uChalg,
                             const quint32 iChalg,
                             const qint16 tBoard,
                             const qint16 ths,
                             const quint32 u32Spare,
                             const quint16 u16Spare);

        quint8 getId() const { return CmMeasHandler::ID; }

        quint32 getPAC() const { return this->pac; }
        quint32 getUChalg() const { return this->uChalg; }
        quint32 getIChalg() const { return this->iChalg; }
        qint16 getTBoard() const { return this->tBoard; }
        qint16 getThs() const { return this->ths; }
        quint32 getU32Spare() const { return this->u32Spare; }
        quint16 getU16Spare() const { return this->u16Spare; }

    private:
        quint32 pac;
        quint32 uChalg;
        quint32 iChalg;
        qint16 tBoard;
        qint16 ths;
        quint32 u32Spare;
        quint16 u16Spare;
};
