#include "BmMeasModel.h"

/**
 * @brief BmMeasModel::BmMeasModel Construct a new BM meassure model
 */
BmMeasModel::BmMeasModel(const qint16 tBatt,
                         const qint32 iBatt,
                         const quint32 uBatt,
                         const quint8 soc,
                         const quint16 error,
                         const quint32 uBattCenter,
                         const quint16 status,
                         const quint32 uElectrolyte,
                         const quint32 u32Spare,
                         const quint16 u16Spare,
                         const quint8 unitScale)
    : IMpModel()
{
    this->tBatt = tBatt;
    this->iBatt = iBatt;
    this->uBatt = uBatt;
    this->soc = soc;
    this->error = error;
    this->uBattCenter = uBattCenter;
    this->status = status;
    this->uElectrolyte = uElectrolyte;
    this->u32Spare = u32Spare;
    this->u16Spare = u16Spare;
    this->unitScale = unitScale;
}
