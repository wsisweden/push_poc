#include "CmMeasModel.h"

/**
 * @brief CmMeasModel::CmMeasModel Construct a new BM meassure model
 */
CmMeasModel::CmMeasModel(const quint32 pac,
                         const quint32 uChalg,
                         const quint32 iChalg,
                         const qint16 tBoard,
                         const qint16 ths,
                         const quint32 u32Spare,
                         const quint16 u16Spare)
    : IMpModel()
{
    this->pac = pac;
    this->uChalg = uChalg;
    this->iChalg = iChalg;
    this->tBoard = tBoard;
    this->ths = ths;
    this->u32Spare = u32Spare;
    this->u16Spare = u16Spare;
}
