#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/CmConfigHandler.h"

#include <QtCore/QVector>

class MPACCESS_EXPORT CmConfigModel : public IMpModel
{
    public:
        explicit CmConfigModel(const quint16 panId,
                               const quint8 channel,
                               const quint16 nwkId,
                               const quint32 serialNo,
                               const quint32 engineCode,
                               const quint8 batteryType,
                               const quint16 algNo,
                               const quint16 capacity,
                               const quint16 cableRes,
                               const quint16 cells,
                               const quint16 baseLoad,
                               const quint8 powerGroup,
                               const qint32 iacLimit,
                               const qint32 pacLimit,
                               const qint32 idcLimit,
                               const qint32 udcLimit,
                               const quint32 u32Spare6,
                               const quint32 u32Spare7,
                               const quint32 u32Spare8,
                               const quint32 u32Spare9,
                               const quint32 u32Spare10,
                               const quint8 displayContrast,
                               const quint8 ledBrightnessMax,
                               const quint8 ledBrightnessDim,
                               const quint8 language,
                               const quint8 timeDateFormat,
                               const quint8 buttonF1Func,
                               const quint8 buttonF2Func,
                               const quint8 remoteInFunc,
                               const quint8 remoteOutFunc,
                               const quint8 remoteOutAlarmVar,
                               const quint8 remoteOutPhaseAlarmVar,
                               const quint8 waterFunction,
                               const quint8 waterVar,
                               const quint8 airPumpVar,
                               const quint8 airPumpVar2,
                               const quint8 parallelControlFunc,
                               const quint8 timeRestriction,
                               const quint8 canMode,
                               const quint16 canNwkCtrl,
                               const QVector<quint32>& pinInSelect,
                               const QVector<quint32>& pinOutSelect,
                               const quint8 radioMode,
                               const quint8 chargingMode,
                               const quint8 nwkSettings,
                               const quint32 securityCode1,
                               const quint32 securityCode2,
                               const quint8 backlightTime,
                               const quint16 instLogSamplePeriod,
                               const quint32 cId,
                               const quint8 bbcFunc,
                               const quint8 bbcVar,
                               const quint8 equalizeFunc,
                               const quint8 equalizeVar,
                               const quint8 equalizeVar2,
                               const quint8 routing,
                               const quint8 bitConfig,
                               const quint8 remoteOutBBCVar,
                               const quint8 canBps,
                               const quint8 extraChargeFunc,
                               const quint8 extraChargeVar,
                               const quint8 extraChargeVar2,
                               const quint8 extraChargeVar3,
                               const quint32 hardwareType,
                               const quint32 hardwareVersion,
                               const quint32 dplPacTot,
                               const quint32 u32Spare4,
                               const qint16 batteryTemp,
                               const quint16 airPumpAlarmLow,
                               const quint16 airPumpAlarmHigh,
                               const quint16 u16Spare4,
                               const quint8 canNodeId,
                               const quint8 dplFunc,
                               const quint8 dplPrio,
                               const quint8 dplPacDef,
                               const QVector<quint8>& chargingRestriction);

        quint8 getId() const { return CmConfigHandler::ID; }

        quint16 getPanId() const { return this->panId; }
        quint8 getChannel() const { return this->channel; }
        quint16 getNwkId() const { return this->nwkId; }
        quint32 getSerialNo() const { return this->serialNo; }
        quint32 getEngineCode() const { return this->engineCode; }
        quint8 getBatteryType() const { return this->batteryType; }
        quint16 getAlgNo() const { return this->algNo; }
        quint16 getCapacity() const { return this->capacity; }
        quint16 getCableRes() const { return this->cableRes; }
        quint16 getCells() const { return this->cells; }
        quint16 getBaseLoad() const { return this->baseLoad; }
        quint8 getPowerGroup() const { return this->powerGroup; }
        qint32 getIACLimit() const { return this->iacLimit; }
        qint32 getPACLimit() const { return this->pacLimit; }
        qint32 getIDCLimit() const { return this->idcLimit; }
        qint32 getUDCLimit() const { return this->udcLimit; }
        quint32 getU32Spare6() const { return this->u32Spare6; }
        quint32 getU32Spare7() const { return this->u32Spare7; }
        quint32 getU32Spare8() const { return this->u32Spare8; }
        quint32 getU32Spare9() const { return this->u32Spare9; }
        quint32 getU32Spare10() const { return this->u32Spare10; }
        quint8 getDisplayContrast() const { return this->displayContrast; }
        quint8 getLEDBrightnessMax() const { return this->ledBrightnessMax; }
        quint8 getLEDBrightnessDim() const { return this->ledBrightnessDim; }
        quint8 getLanguage() const { return this->language; }
        quint8 getTimeDateFormat() const { return this->timeDateFormat; }
        quint8 getButtonF1Func() const { return this->buttonF1Func; }
        quint8 getButtonF2Func() const { return this->buttonF2Func; }
        quint8 getRemoteInFunc() const { return this->remoteInFunc; }
        quint8 getRemoteOutFunc() const { return this->remoteOutFunc; }
        quint8 getRemoteOutAlarmVar() const { return this->remoteOutAlarmVar; }
        quint8 getRemoteOutPhaseAlarmVar() const { return this->remoteOutPhaseAlarmVar; }
        quint8 getWaterFunction() const { return this->waterFunction; }
        quint8 getWaterVar() const { return this->waterVar; }
        quint8 getAirPumpVar() const { return this->airPumpVar; }
        quint8 getAirPumpVar2() const { return this->airPumpVar2; }
        quint8 getParallelControlFunc() const { return this->parallelControlFunc; }
        quint8 getTimeRestriction() const { return this->timeRestriction; }
        quint8 getCANMode() const { return this->canMode; }
        quint16 getCANNwkCtrl() const { return this->canNwkCtrl; }
        QVector<quint32> getPinInSelect() const { return this->pinInSelect; }
        QVector<quint32> getPinOutSelect() const { return this->pinOutSelect; }
        quint8 getRadioMode() const { return this->radioMode; }
        quint8 getChargingMode() const { return this->chargingMode; }
        quint8 getNwkSettings() const { return this->nwkSettings; }
        quint32 getSecurityCode1() const { return this->securityCode1; }
        quint32 getSecurityCode2() const { return this->securityCode2; }
        quint8 getBacklightTime() const { return this->backlightTime; }
        quint16 getInstLogSamplePeriod() const { return this->instLogSamplePeriod; }
        quint32 getCId() const { return this->cId; }
        quint8 getBBCFunc() const { return this->bbcFunc; }
        quint8 getBBCVar() const { return this->bbcVar; }
        quint8 getEqualizeFunc() const { return this->equalizeFunc; }
        quint8 getEqualizeVar() const { return this->equalizeVar; }
        quint8 getEqualizeVar2() const { return this->equalizeVar2; }
        quint8 getRouting() const { return this->routing; }
        quint8 getBitConfig() const { return this->bitConfig; }
        quint8 getRemoteOutBBCVar() const { return this->remoteOutBBCVar; }
        quint8 getCANBps() const { return this->canBps; }
        quint8 getExtraChargeFunc() const { return this->extraChargeFunc; }
        quint8 getExtraChargeVar() const { return this->extraChargeVar; }
        quint8 getExtraChargeVar2() const { return this->extraChargeVar2; }
        quint8 getExtraChargeVar3() const { return this->extraChargeVar3; }
        quint32 getHardwareType() const { return this->hardwareType; }
        quint32 getHardwareVersion() const { return this->hardwareVersion; }
        quint32 getDPLPacTot() const { return this->dplPacTot; }
        quint32 getU32Spare4() const { return this->u32Spare4; }
        qint16 getBatteryTemp() const { return this->batteryTemp; }
        quint16 getAirPumpAlarmLow() const { return this->airPumpAlarmLow; }
        quint16 getAirPumpAlarmHigh() const { return this->airPumpAlarmHigh; }
        quint16 getU16Spare4() const { return this->u16Spare4; }
        quint8 getCANNodeId() const { return this->canNodeId; }
        quint8 getDPLFunc() const { return this->dplFunc; }
        quint8 getDPLPrio() const { return this->dplPrio; }
        quint8 getDPLPacDef() const { return this->dplPacDef; }
        QVector<quint8> getChargingRestriction() const { return this->chargingRestriction; }

    private:
        quint16 panId;
        quint8 channel;
        quint16 nwkId;
        quint32 serialNo;
        quint32 engineCode;
        quint8 batteryType;
        quint16 algNo;
        quint16 capacity;
        quint16 cableRes;
        quint16 cells;
        quint16 baseLoad;
        quint8 powerGroup;
        qint32 iacLimit;
        qint32 pacLimit;
        qint32 idcLimit;
        qint32 udcLimit;
        quint32 u32Spare6;
        quint32 u32Spare7;
        quint32 u32Spare8;
        quint32 u32Spare9;
        quint32 u32Spare10;
        quint8 displayContrast;
        quint8 ledBrightnessMax;
        quint8 ledBrightnessDim;
        quint8 language;
        quint8 timeDateFormat;
        quint8 buttonF1Func;
        quint8 buttonF2Func;
        quint8 remoteInFunc;
        quint8 remoteOutFunc;
        quint8 remoteOutAlarmVar;
        quint8 remoteOutPhaseAlarmVar;
        quint8 waterFunction;
        quint8 waterVar;
        quint8 airPumpVar;
        quint8 airPumpVar2;
        quint8 parallelControlFunc;
        quint8 timeRestriction;
        quint8 canMode;
        quint16 canNwkCtrl;
        QVector<quint32> pinInSelect;
        QVector<quint32> pinOutSelect;
        quint8 radioMode;
        quint8 chargingMode;
        quint8 nwkSettings;
        quint32 securityCode1;
        quint32 securityCode2;
        quint8 backlightTime;
        quint16 instLogSamplePeriod;
        quint32 cId;
        quint8 bbcFunc;
        quint8 bbcVar;
        quint8 equalizeFunc;
        quint8 equalizeVar;
        quint8 equalizeVar2;
        quint8 routing;
        quint8 bitConfig;
        quint8 remoteOutBBCVar;
        quint8 canBps;
        quint8 extraChargeFunc;
        quint8 extraChargeVar;
        quint8 extraChargeVar2;
        quint8 extraChargeVar3;
        quint32 hardwareType;
        quint32 hardwareVersion;
        quint32 dplPacTot;
        quint32 u32Spare4;
        qint16 batteryTemp;
        quint16 airPumpAlarmLow;
        quint16 airPumpAlarmHigh;
        quint16 u16Spare4;
        quint8 canNodeId;
        quint8 dplFunc;
        quint8 dplPrio;
        quint8 dplPacDef;
        QVector<quint8> chargingRestriction;
};
