#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/CmStatusHandler.h"

class MPACCESS_EXPORT CmStatusModel : public IMpModel
{
    public:
        explicit CmStatusModel(const quint32 cmType,
                               const quint64 cmId,
                               const quint32 time,
                               const quint32 chargeTimeTotal,
                               const quint32 chargedAhTotal,
                               const quint32 pACTotal,
                               const quint16 cycklesSum2to25,
                               const quint16 cycklesSum26to50,
                               const quint16 cycklesSum51to80,
                               const quint16 cycklesSum81to90,
                               const quint16 cycklesSumAbove90,
                               const quint8 chargingMode,
                               const quint32 bId,
                               const QString& algNoName,
                               const quint16 algNo,
                               const quint16 version,
                               const quint16 capacity,
                               const quint16 cableRes,
                               const quint16 cell,
                               const quint16 baseLoad,
                               const quint16 chalgError,
                               const quint16 reguError,
                               const quint16 chalgStatus,
                               const quint16 reguStatus,
                               const qint16 tBoard,
                               const qint16 ths,
                               const quint16 acOVP,
                               const quint32 u32Spare1,
                               const quint32 u32Spare2,
                               const quint16 u16Spare1);

        quint8 getId() const { return CmStatusHandler::ID; }

        quint32 getCMType() const { return this->cmType; }
        quint64 getCMId() const { return this->cmId; }
        quint32 getTime() const { return this->time; }
        quint32 getChargeTimeTotal() const { return this->chargeTimeTotal; }
        quint32 getChargedAhTotal() const { return this->chargedAhTotal; }
        quint32 getPACTotal() const { return this->pACTotal; }
        quint16 getCycklesSum2To25() const { return this->cycklesSum2to25; }
        quint16 getCycklesSum26To50() const { return this->cycklesSum26to50; }
        quint16 getCycklesSum51To80() const { return this->cycklesSum51to80; }
        quint16 getCycklesSum81To90() const { return this->cycklesSum81to90; }
        quint16 getCycklesSumAbove90() const { return this->cycklesSumAbove90; }
        quint8 getChargingMode() const { return this->chargingMode; }
        quint32 getBId() const { return this->bId; }
        QString getAlgNoName() const { return this->algNoName; }
        quint16 getAlgNo() const { return this->algNo; }
        quint16 getVersion() const { return this->version; }
        quint16 getCapacity() const { return this->capacity; }
        quint16 getCableRes() const { return this->cableRes; }
        quint16 getCell() const { return this->cell; }
        quint16 getBaseLoad() const { return this->baseLoad; }
        quint16 getChalgError() const { return this->chalgError; }
        quint16 getReguError() const { return this->reguError; }
        quint16 getChalgStatus() const { return this->chalgStatus; }
        quint16 getReguStatus() const { return this->reguStatus; }
        qint16 getTBoard() const { return this->tBoard; }
        qint16 getThs() const { return this->ths; }
        quint16 getACOVP() const { return this->acOVP; }
        quint32 getU32Spare1() const { return this->u32Spare1; }
        quint32 getU32Spare2() const { return this->u32Spare2; }
        quint16 getU16Spare1() const { return this->u16Spare1; }

    private:
        quint32 cmType;
        quint64 cmId;
        quint32 time;
        quint32 chargeTimeTotal;
        quint32 chargedAhTotal;
        quint32 pACTotal;
        quint16 cycklesSum2to25;
        quint16 cycklesSum26to50;
        quint16 cycklesSum51to80;
        quint16 cycklesSum81to90;
        quint16 cycklesSumAbove90;
        quint8 chargingMode;
        quint32 bId;
        QString algNoName;
        quint16 algNo;
        quint16 version;
        quint16 capacity;
        quint16 cableRes;
        quint16 cell;
        quint16 baseLoad;
        quint16 chalgError;
        quint16 reguError;
        quint16 chalgStatus;
        quint16 reguStatus;
        qint16 tBoard;
        qint16 ths;
        quint16 acOVP;
        quint32 u32Spare1;
        quint32 u32Spare2;
        quint16 u16Spare1;
};
