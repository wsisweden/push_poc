#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/BmStatusHandler.h"

class MPACCESS_EXPORT BmStatusModel : public IMpModel
{
    public:
        explicit BmStatusModel(const quint32 bId,
                               const QString& bmFgId,
                               const quint8 batteryType,
                               const quint16 capacity,
                               const quint16 cells,
                               const quint16 cycles2to25,
                               const quint16 cycles26to50,
                               const quint16 cycles51to80,
                               const quint16 cycles81to90,
                               const quint16 cyclesAbove90,
                               const quint16 stoppedCycles,
                               const quint16 cyclesLeft,
                               const quint32 ahLeft,
                               const quint32 time,
                               const quint16 timeLeft,
                               const quint32 dTimeTotal,
                               const quint32 dAhTotal,
                               const quint32 dWhTotal,
                               const quint32 cTimeTotal,
                               const quint32 cAhTotal,
                               const quint32 cWhTotal,
                               const quint32 cWhACTotal,
                               const qint16 tBatt,
                               const quint8 soc,
                               const quint16 bmError,
                               const quint16 bmStatus,
                               const qint16 avgTBatt,
                               const quint32 u32Spare1,
                               const quint16 actualCapacity,
                               const quint16 u16Spare2,
                               const quint8 ebUnits,
                               const quint8 u8Spare2);

        quint8 getId() const { return BmStatusHandler::ID; }

        quint32 getBId() const { return this->bId; }
        QString getBMFgId() const { return this->bmFgId; }
        quint8 getBatteryType() const { return this->batteryType; }
        quint16 getCapacity() const { return this->capacity; }
        quint16 getCells() const { return this->cells; }
        quint16 getCycles2To25() const { return this->cycles2to25; }
        quint16 getCycles26To50() const { return this->cycles26to50; }
        quint16 getCycles51To80() const { return this->cycles51to80; }
        quint16 getCycles81To90() const { return this->cycles81to90; }
        quint16 getCyclesAbove90() const { return this->cyclesAbove90; }
        quint16 getStoppedCycles() const { return this->stoppedCycles; }
        quint16 getCyclesLeft() const { return this->cyclesLeft; }
        quint32 getAhLeft() const { return this->ahLeft; }
        quint32 getTime() const { return this->time; }
        quint16 getTimeLeft() const { return this->timeLeft; }
        quint32 getDTimeTotal() const { return this->dTimeTotal; }
        quint32 getDAhTotal() const { return this->dAhTotal; }
        quint32 getDWhTotal() const { return this->dWhTotal; }
        quint32 getCTimeTotal() const { return this->cTimeTotal; }
        quint32 getCAhTotal() const { return this->cAhTotal; }
        quint32 getCWhTotal() const { return this->cWhTotal; }
        quint32 getCWhACTotal() const { return this->cWhACTotal; }
        qint16 getTBatt() const { return this->tBatt; }
        quint8 getSOC() const { return this->soc; }
        quint16 getBMError() const { return this->bmError; }
        quint16 getBMStatus() const { return this->bmStatus; }
        qint16 getAvgTBatt() const { return this->avgTBatt; }
        quint32 getU32Spare1() const { return this->u32Spare1; }
        quint16 getActualCapacity() const { return this->actualCapacity; }
        quint16 getU16Spare2() const { return this->u16Spare2; }
        quint8 getEBUnits() const { return this->ebUnits; }
        quint8 getU8Spare2() const { return this->u8Spare2; }

    private:
        quint32 bId;
        QString bmFgId;
        quint8 batteryType;
        quint16 capacity;
        quint16 cells;
        quint16 cycles2to25;
        quint16 cycles26to50;
        quint16 cycles51to80;
        quint16 cycles81to90;
        quint16 cyclesAbove90;
        quint16 stoppedCycles;
        quint16 cyclesLeft;
        quint32 ahLeft;
        quint32 time;
        quint16 timeLeft;
        quint32 dTimeTotal;
        quint32 dAhTotal;
        quint32 dWhTotal;
        quint32 cTimeTotal;
        quint32 cAhTotal;
        quint32 cWhTotal;
        quint32 cWhACTotal;
        qint16 tBatt;
        quint8 soc;
        quint16 bmError;
        quint16 bmStatus;
        qint16 avgTBatt;
        quint32 u32Spare1;
        quint16 actualCapacity;
        quint16 u16Spare2;
        quint8 ebUnits;
        quint8 u8Spare2;
};
