#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/BmInfoHandler.h"

class MPACCESS_EXPORT BmInfoModel : public IMpModel
{
    public:
        explicit BmInfoModel(const quint32 bId,
                             const quint64 mui,
                             const quint8 bmType,
                             const quint16 bmStatus,
                             const quint16 bmError,
                             const quint16 histLogInMem,
                             const quint32 histLogIndex,
                             const quint32 histLogDate,
                             const quint32 evtLogInMem,
                             const quint32 evtLogIndex,
                             const quint32 evtLogDate,
                             const quint32 instLogInMem,
                             const quint32 instLogIndex,
                             const quint32 fwVer,
                             const quint32 fwType,
                             const quint32 productType);

        quint8 getId() const { return BmInfoHandler::ID; }

        quint32 getBId() const { return this->bId; }
        quint64 getMUI() { return translateMUI(this->mui); }
        quint8 getBMType() const { return this->bmType; }
        quint16 getBMStatus() const { return this->bmStatus; }
        quint16 getBMError() const { return this->bmError; }
        quint16 getHistLogInMem() const { return this->histLogInMem; }
        quint32 getHistLogIndex() const { return this->histLogIndex; }
        quint32 getHistLogDate() const { return this->histLogDate; }
        quint32 getEvtLogInMem() const { return this->evtLogInMem; }
        quint32 getEvtLogIndex() const { return this->evtLogIndex; }
        quint32 getEvtLogDate() const { return this->evtLogDate; }
        quint32 getInstLogInMem() const { return this->instLogInMem; }
        quint32 getInstLogIndex() const { return this->instLogIndex; }
        quint32 getFwVer() const { return this->fwVer; }
        quint32 getFwType() const { return this->fwType; }
        quint32 getProductType() const { return this->productType; }
        quint64 getSerialNumber() { return parseSerialNumber(this->mui); }

    private:
        quint32 bId;
        quint64 mui;
        quint8 bmType;
        quint16 bmStatus;
        quint16 bmError;
        quint16 histLogInMem;
        quint32 histLogIndex;
        quint32 histLogDate;
        quint32 evtLogInMem;
        quint32 evtLogIndex;
        quint32 evtLogDate;
        quint32 instLogInMem;
        quint32 instLogIndex;
        quint32 fwVer;
        quint32 fwType;
        quint32 productType;

        quint64 translateMUI(const quint64 mui);
        quint64 parseSerialNumber(const quint64 mui);
};
