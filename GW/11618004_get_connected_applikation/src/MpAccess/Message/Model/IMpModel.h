#pragma once

#include "../../Shared.h"

#include <QtCore/QMetaType>

class MPACCESS_EXPORT IMpModel
{
    public:
        virtual ~IMpModel() {}
        virtual quint8 getId() const = 0;
};
