#include "CmStatusModel.h"

/**
 * @brief CmStatusModel::CmStatusModel Construct a new BM status model
 */
CmStatusModel::CmStatusModel(const quint32 cmType,
                             const quint64 cmId,
                             const quint32 time,
                             const quint32 chargeTimeTotal,
                             const quint32 chargedAhTotal,
                             const quint32 pACTotal,
                             const quint16 cycklesSum2to25,
                             const quint16 cycklesSum26to50,
                             const quint16 cycklesSum51to80,
                             const quint16 cycklesSum81to90,
                             const quint16 cycklesSumAbove90,
                             const quint8 chargingMode,
                             const quint32 bId,
                             const QString& algNoName,
                             const quint16 algNo,
                             const quint16 version,
                             const quint16 capacity,
                             const quint16 cableRes,
                             const quint16 cell,
                             const quint16 baseLoad,
                             const quint16 chalgError,
                             const quint16 reguError,
                             const quint16 chalgStatus,
                             const quint16 reguStatus,
                             const qint16 tBoard,
                             const qint16 ths,
                             const quint16 acOVP,
                             const quint32 u32Spare1,
                             const quint32 u32Spare2,
                             const quint16 u16Spare1)
    : IMpModel()
{
    this->cmType = cmType;
    this->cmId = cmId;
    this->time = time;
    this->chargeTimeTotal = chargeTimeTotal;
    this->chargedAhTotal = chargedAhTotal;
    this->pACTotal = pACTotal;
    this->cycklesSum2to25 = cycklesSum2to25;
    this->cycklesSum26to50 = cycklesSum26to50;
    this->cycklesSum51to80 = cycklesSum51to80;
    this->cycklesSum81to90 = cycklesSum81to90;
    this->cycklesSumAbove90 = cycklesSumAbove90;
    this->chargingMode = chargingMode;
    this->bId = bId;
    this->algNoName = algNoName;
    this->algNo = algNo;
    this->version = version;
    this->capacity = capacity;
    this->cableRes = cableRes;
    this->cell = cell;
    this->baseLoad = baseLoad;
    this->chalgError = chalgError;
    this->reguError = reguError;
    this->chalgStatus = chalgStatus;
    this->reguStatus = reguStatus;
    this->tBoard = tBoard;
    this->ths = ths;
    this->acOVP = acOVP;
    this->u32Spare1 = u32Spare1;
    this->u32Spare2 = u32Spare2;
    this->u16Spare1 = u16Spare1;
}
