#pragma once

#include "../../Shared.h"
#include "IMpModel.h"
#include "../Handler/BmConfigHandler.h"

class MPACCESS_EXPORT BmConfigModel : public IMpModel
{
    public:
        explicit BmConfigModel(const quint16 panId,
                               const quint8 channel,
                               const quint16 nwkId,
                               const quint8 nwkSettings,
                               const quint32 serialNo,
                               const quint8 bmType,
                               const quint32 bId,
                               const quint8 equFunc,
                               const quint8 equParam,
                               const quint8 waterFunc,
                               const quint8 waterParam,
                               const QString& bmFgId,
                               const quint16 cells,
                               const quint16 capacity,
                               const quint16 cyclesAvailable,
                               const quint32 ahAvailable,
                               const quint8 batteryType,
                               const quint16 baseLoad,
                               const quint16 cableRes,
                               const quint16 algNo,
                               const quint16 u16Spare5,
                               const quint16 bbCgroup,
                               const quint16 instPeriod,
                               const quint8 cellTap,
                               const quint8 voltageBalanceLimit,
                               const quint8 acidSensor,
                               const qint16 tMax,
                               const quint8 rOutFunc,
                               const quint16 dEl,
                               const quint16 cEl,
                               const quint8 cEfficiency,
                               const quint8 clearStatistics,
                               const quint32 ahUsed,
                               const quint32 u32Spare2,
                               const quint32 u32Spare3,
                               const quint16 securityCode1,
                               const quint16 securityCode2,
                               const quint16 u16Spare3,
                               const quint16 u16Spare4,
                               const quint8 bitConfig,
                               const quint8 socThreshold,
                               const quint8 u8Spare3,
                               const quint8 u8Spare4);

        quint8 getId() const { return BmConfigHandler::ID; }

        quint16 getPanId() const { return this->panId; }
        quint8 getChannel() const { return this->channel; }
        quint16 getNwkId() const { return this->nwkId; }
        quint8 getNwkSettings() const { return this->nwkSettings; }
        quint32 getSerialNo() const { return this->serialNo; }
        quint8 getBMType() const { return this->bmType; }
        quint32 getBId() const { return this->bId; }
        quint8 getEquFunc() const { return this->equFunc; }
        quint8 getEquParam() const { return this->equParam; }
        quint8 getWaterFunc() const { return this->waterFunc; }
        quint8 getWaterParam() const { return this->waterParam; }
        QString getBMFgId() const { return this->bmFgId; }
        quint16 getCells() const { return this->cells; }
        quint16 getCapacity() const { return this->capacity; }
        quint16 getCyclesAvailable() const { return this->cyclesAvailable; }
        quint32 getAhAvailable() const { return this->ahAvailable; }
        quint8 getBatteryType() const { return this->batteryType; }
        quint16 getBaseLoad() const { return this->baseLoad; }
        quint16 getCableRes() const { return this->cableRes; }
        quint16 getAlgNo() const { return this->algNo; }
        quint16 getU16Spare5() const { return this->u16Spare5; }
        quint16 getBBCgroup() const { return this->bbCgroup; }
        quint16 getInstPeriod() const { return this->instPeriod; }
        quint8 getCellTap() const { return this->cellTap; }
        quint8 getVoltageBalanceLimit() const { return this->voltageBalanceLimit; }
        quint8 getAcidSensor() const { return this->acidSensor; }
        qint16 getTMax() const { return this->tMax; }
        quint8 getROutFunc() const { return this->rOutFunc; }
        quint16 getDEl() const { return this->dEl; }
        quint16 getCEl() const { return this->cEl; }
        quint8 getCEfficiency() const { return this->cEfficiency; }
        quint8 getClearStatistics() const { return this->clearStatistics; }
        quint32 getAhUsed() const { return this->ahUsed; }
        quint32 getU32Spare2() const { return this->u32Spare2; }
        quint32 getU32Spare3() const { return this->u32Spare3; }
        quint16 getSecurityCode1() const { return this->securityCode1; }
        quint16 getSecurityCode2() const { return this->securityCode2; }
        quint16 getU16Spare3() const { return this->u16Spare3; }
        quint8 getBitConfig() const { return this->bitConfig; }
        quint8 getSOCThreshold() const { return this->socThreshold; }
        quint8 getU8Spare3() const { return this->u8Spare3; }
        quint8 getU8Spare4() const { return this->u8Spare4; }

    private:
        quint16 panId;
        quint8 channel;
        quint16 nwkId;
        quint8 nwkSettings;
        quint32 serialNo;
        quint8 bmType;
        quint32 bId;
        quint8 equFunc;
        quint8 equParam;
        quint8 waterFunc;
        quint8 waterParam;
        QString bmFgId;
        quint16 cells;
        quint16 capacity;
        quint16 cyclesAvailable;
        quint32 ahAvailable;
        quint8 batteryType;
        quint16 baseLoad;
        quint16 cableRes;
        quint16 algNo;
        quint16 u16Spare5;
        quint16 bbCgroup;
        quint16 instPeriod;
        quint8 cellTap;
        quint8 voltageBalanceLimit;
        quint8 acidSensor;
        qint16 tMax;
        quint8 rOutFunc;
        quint16 dEl;
        quint16 cEl;
        quint8 cEfficiency;
        quint8 clearStatistics;
        quint32 ahUsed;
        quint32 u32Spare2;
        quint32 u32Spare3;
        quint16 securityCode1;
        quint16 securityCode2;
        quint16 u16Spare3;
        quint16 u16Spare4;
        quint8 bitConfig;
        quint8 socThreshold;
        quint8 u8Spare3;
        quint8 u8Spare4;
};
