#include "BmMeasHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/BmMeasModel.h"

#include <QtCore/QLoggingCategory>

const quint8 BmMeasHandler::ID;

/**
 * @brief BmMeasHandler::BmMeasHandler Construct a new BM meassure handler
 * @param context The context
 * @param reciever The reciever
 */
BmMeasHandler::BmMeasHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &BmMeasHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &BmMeasHandler::reset);
}

/**
 * @brief BmMeasHandler::reset Reset handler
 */
void BmMeasHandler::reset()
{
    this->parseCompleted = false;
}

/**
 * @brief BmMeasHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool BmMeasHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < (30 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    qCDebug(QLoggingCategory("mpa")).noquote() << "BM meas";

    qint16 tBatt = (qint16)parseU16(data);
    qint32 iBatt = (qint32)parseU32(data);
    quint32 uBatt = parseU32(data);
    quint8 soc = parseU8(data);
    quint16 error = parseU16(data);
    quint32 uBattCenter = parseU32(data);
    quint16 status = parseU16(data);
    quint32 uElectrolyte = parseU32(data);
    quint32 u32Spare = parseU32(data);
    quint16 u16Spare = parseU16(data);
    quint8 unitScale = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   TBatt: " + QString::number(tBatt);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   IBatt: " + QString::number(iBatt);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   UBatt: " + QString::number(iBatt);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   SOC: " + QString::number(soc);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Error 0x: " + QString::number(error, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   UBatt Center: " + QString::number(uBattCenter);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Status 0x: " + QString::number(status, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   UElectrolyte: " + QString::number(uElectrolyte);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare: 0x" + QString::number(u32Spare, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare: 0x" + QString::number(u16Spare, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Unit scale: 0x" + QString::number(unitScale, 16).toUpper();

    IMpModel* model = new BmMeasModel(tBatt,
                                      iBatt,
                                      uBatt,
                                      soc,
                                      error,
                                      uBattCenter,
                                      status,
                                      uElectrolyte,
                                      u32Spare,
                                      u16Spare,
                                      unitScale);

    this->reciever.addNodeMeas(channel, panId, address, model);

    this->parseCompleted = true;

    return true;
}
