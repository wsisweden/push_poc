#pragma once

#include "MessageHandler.h"
#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../../Common/Context.h"

#include <QtCore/QMap>
#include <QtCore/QObject>

class MPACCESS_EXPORT MessageHandlerFactory : public QObject
{
    Q_OBJECT

    public:
        MessageHandlerFactory(Context* context, IReciever& reciever, QObject* parent = nullptr);

        MessageHandler* getHandler(const quint8 eventId);

    private:
        QMap<quint8, MessageHandler*> handlers;
};
