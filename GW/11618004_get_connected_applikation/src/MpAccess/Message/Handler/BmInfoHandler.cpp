#include "BmInfoHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/BmInfoModel.h"
#include "../../../Common/Constant.h"

#include <QtCore/QLoggingCategory>

const quint8 BmInfoHandler::ID;

/**
 * @brief BmInfoHandler::BmInfoHandler Construct a new BM information handler
 * @param context The context
 * @param reciever The reciever
 */
BmInfoHandler::BmInfoHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &BmInfoHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &BmInfoHandler::reset);
}

/**
 * @brief BmInfoHandler::reset Reset handler
 */
void BmInfoHandler::reset()
{
    this->parseCompleted = false;
}

/**
 * @brief BmInfoHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool BmInfoHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < (57 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    qCDebug(QLoggingCategory("mpa")).noquote() << "BM info";

    quint32 bId = parseU32(data);
    quint64 mui = parseU64(data);
    quint8 bmType = parseU8(data);
    quint16 bmStatus = parseU16(data);
    quint16 bmError = parseU16(data);
    quint16 histLogInMem = parseU16(data);
    quint32 histLogIndex = parseU32(data);
    quint32 histLogDate = parseU32(data);
    quint32 evtLogInMem = parseU16(data);
    quint32 evtLogIndex = parseU32(data);
    quint32 evtLogDate = parseU32(data);
    quint32 instLogInMem = parseU16(data);
    quint32 instLogIndex = parseU32(data);
    quint32 fwVer = parseU32(data);
    quint32 fwType = parseU32(data);
    quint16 u16Spare1 = parseU16(data);
    quint16 u16Spare2 = parseU16(data);
    quint8 u8Spare1 = parseU8(data);
    quint8 u8Spare2 = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   BID: " + QString::number(bId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   MUI: 0x" + QString::number(mui, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BM type: 0x" + QString::number(bmType, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BM status: 0x" + QString::number(bmStatus, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BM error: 0x" + QString::number(bmError, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   History log in memory: " + QString::number(histLogInMem);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   History log index: " + QString::number(histLogIndex);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   History log date: " + QString::number(histLogDate);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Event log in memory: " + QString::number(evtLogInMem);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Event log index: " + QString::number(evtLogIndex);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Event log date: " + QString::number(evtLogDate);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Instant log in memory: " + QString::number(instLogInMem);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Instant log index: " + QString::number(instLogIndex);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware type: " + QString::number(fwType);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware version: " + QString::number(fwVer);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: 0x" + QString::number(u16Spare1, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 2: 0x" + QString::number(u16Spare2, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: 0x" + QString::number(u8Spare1, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 2: 0x" + QString::number(u8Spare2, 16).toUpper();

    IMpModel* model = new BmInfoModel(bId,
                                      mui,
                                      bmType,
                                      bmStatus,
                                      bmError,
                                      histLogInMem,
                                      histLogIndex,
                                      histLogDate,
                                      evtLogInMem,
                                      evtLogIndex,
                                      evtLogDate,
                                      instLogInMem,
                                      instLogIndex,
                                      fwVer,
                                      fwType,
                                      ProductType::BMU);

    this->reciever.addNodeInfo(channel, panId, address, model);

    this->parseCompleted = true;

    return true;
}
