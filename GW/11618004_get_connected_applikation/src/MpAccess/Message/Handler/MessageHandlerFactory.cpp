#include "MessageHandlerFactory.h"
#include "BmInfoHandler.h"
#include "CmInfoHandler.h"
#include "BmMeasHandler.h"
#include "CmMeasHandler.h"
#include "BmStatusHandler.h"
#include "CmStatusHandler.h"
#include "BmConfigHandler.h"
#include "CmConfigHandler.h"
#include "BmLogHandler.h"
#include "CmLogHandler.h"
#include "SetParamHandler.h"

/**
 * @brief MessageHandlerFactory::MessageHandlerFactory Construct a new message handler factory
 * @param context The context
 * @param reciever The reciever
 */
MessageHandlerFactory::MessageHandlerFactory(Context* context, IReciever& reciever, QObject* parent)
    : QObject(parent)
{
    this->handlers[BmInfoHandler::ID] = new BmInfoHandler(context, reciever);
    this->handlers[CmInfoHandler::ID] = new CmInfoHandler(context, reciever);
    this->handlers[BmMeasHandler::ID] = new BmMeasHandler(context, reciever);
    this->handlers[CmMeasHandler::ID] = new CmMeasHandler(context, reciever);
    this->handlers[BmStatusHandler::ID] = new BmStatusHandler(context, reciever);
    this->handlers[CmStatusHandler::ID] = new CmStatusHandler(context, reciever);
    this->handlers[BmConfigHandler::ID] = new BmConfigHandler(context, reciever);
    this->handlers[CmConfigHandler::ID] = new CmConfigHandler(context, reciever);
    this->handlers[BmLogHandler::ID] = new BmLogHandler(context, reciever);
    this->handlers[CmLogHandler::ID] = new CmLogHandler(context, reciever);
    this->handlers[SetParamHandler::ID] = new SetParamHandler(context, reciever);
}

/**
 * @brief MessageHandlerFactory::getHandler Get the message to handle the event
 * @param eventId The ID of the event
 * @return The message
 */
MessageHandler* MessageHandlerFactory::getHandler(const quint8 eventId)
{
    if (this->handlers.contains(eventId))
    {
        MessageHandler* handler = this->handlers.value(eventId);
        handler->rewindPosition();

        return handler;
    }

    return nullptr;
}
