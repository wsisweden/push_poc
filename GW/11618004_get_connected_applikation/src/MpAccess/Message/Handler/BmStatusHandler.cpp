#include "BmStatusHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/BmStatusModel.h"

#include <QtCore/QLoggingCategory>

const quint8 BmStatusHandler::ID;

/**
 * @brief BmStatusHandler::BmStatusHandler Construct a new BM status handler
 * @param context The context
 * @param reciever The reciever
 */
BmStatusHandler::BmStatusHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &BmStatusHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &BmStatusHandler::reset);
}

/**
 * @brief BmStatusHandler::reset Reset handler
 */
void BmStatusHandler::reset()
{
    this->parseCompleted = false;
}

/**
 * @brief BmStatusHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool BmStatusHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < (88 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    qCDebug(QLoggingCategory("mpa")).noquote() << "BM status";

    quint32 bId = parseU32(data);
    QString bmFgId = parseString(data, 8);
    quint8 batteryType = parseU8(data);
    quint16 capacity = parseU16(data);
    quint16 cells = parseU16(data);
    quint16 cycles2to25 = parseU16(data);
    quint16 cycles26to50 = parseU16(data);
    quint16 cycles51to80 = parseU16(data);
    quint16 cycles81to90 = parseU16(data);
    quint16 cyclesAbove90 = parseU16(data);
    quint16 stoppedCycles = parseU16(data);
    quint16 cyclesLeft = parseU16(data);
    quint32 ahLeft = parseU32(data);
    quint32 time = parseU32(data);
    quint16 timeLeft = parseU16(data);
    quint32 dTimeTotal = parseU32(data);
    quint32 dAhTotal = parseU32(data);
    quint32 dWhTotal = parseU32(data);
    quint32 cTimeTotal = parseU32(data);
    quint32 cAhTotal = parseU32(data);
    quint32 cWhTotal = parseU32(data);
    quint32 cWhACTotal = parseU32(data);
    qint16 tBatt = (qint16)parseU16(data);
    quint8 soc = parseU8(data);
    quint16 bmError = parseU16(data);
    quint16 bmStatus = parseU16(data);
    qint16 avgTBatt = (qint16)parseU16(data);
    quint32 u32Spare1 = parseU32(data);
    quint16 actualCapacity = parseU16(data);
    quint16 u16Spare2 = parseU16(data);
    quint8 ebUnits = parseU8(data);
    quint8 u8Spare2 = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   BID: " + QString::number(bId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery tag: " + bmFgId;
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery type: " + QString::number(batteryType);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity: " + QString::number(capacity);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cells:" + QString::number(cells);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 2 to 25:" + QString::number(cycles2to25);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 26 to 50: " + QString::number(cycles26to50);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 51 to 80: " + QString::number(cycles51to80);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 81 to 90: " + QString::number(cycles81to90);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles above 90: " + QString::number(cyclesAbove90);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Stopped cyckles: " + QString::number(stoppedCycles);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles left: " + QString::number(cyclesLeft);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Ah left: " + QString::number(ahLeft);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Time: " + QString::number(time);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Time left: " + QString::number(timeLeft);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   dTime total: " + QString::number(dTimeTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   dAh total: " + QString::number(dAhTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   dWh total: " + QString::number(dWhTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   cTime total: " + QString::number(cTimeTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   cAh total: " + QString::number(cAhTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   cWh total: " + QString::number(cWhTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   cWh AC  total: " + QString::number(cWhACTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   TBatt: " + QString::number(tBatt);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   SOC: " + QString::number(soc);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BM Error 0x: " + QString::number(bmError, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BM Status 0x: " + QString::number(bmStatus, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Avg TBatt: " + QString::number(avgTBatt);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: 0x" + QString::number(u32Spare1, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Actual capacity: " + QString::number(actualCapacity);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 2: 0x" + QString::number(u16Spare2, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   EB units: " + QString::number(ebUnits);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 2: 0x" + QString::number(u8Spare2, 16).toUpper();

    IMpModel* model = new BmStatusModel(bId,
                                          bmFgId,
                                          batteryType,
                                          capacity,
                                          cells,
                                          cycles2to25,
                                          cycles26to50,
                                          cycles51to80,
                                          cycles81to90,
                                          cyclesAbove90,
                                          stoppedCycles,
                                          cyclesLeft,
                                          ahLeft,
                                          time,
                                          timeLeft,
                                          dTimeTotal,
                                          dAhTotal,
                                          dWhTotal,
                                          cTimeTotal,
                                          cAhTotal,
                                          cWhTotal,
                                          cWhACTotal,
                                          tBatt,
                                          soc,
                                          bmError,
                                          bmStatus,
                                          avgTBatt,
                                          u32Spare1,
                                          actualCapacity,
                                          u16Spare2,
                                          ebUnits,
                                          u8Spare2);

    this->reciever.addNodeStatus(channel, panId, address, model);

    this->parseCompleted = true;

    return true;
}
