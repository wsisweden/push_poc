#include "SetParamHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/SetParamModel.h"
#include "../../../Common/Constant.h"

#include <QtCore/QVariant>
#include <QtCore/QLoggingCategory>

const quint8 SetParamHandler::ID;

/**
 * @brief SetParamHandler::SetParamHandler Construct a new set param handler
 * @param context The context
 * @param reciever The reciever
 */
SetParamHandler::SetParamHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
}

/**
 * @brief SetParamHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool SetParamHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() <= (1 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return true;
    }

    parseHeader(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "Param";

    quint32 index = parseU32(data);

    QVariant value;
    switch (index)
    {
        case Param::SOFTWARE_TYPE:
        case Param::SOFTWARE_VERSION:
        case Param::BUILDCOMMIT:
        case Param::DEVICE_ID:
        case Param::DATETIME:
        case Param::SCAN_NETWORK_INTERVAL:
        case Param::COLLECT_DATA_INTERVAL:
        case Param::REMOVE_NODE_INTERVAL:
        case Param::WIRELESS_LINK_QUALITY:
        case Param::WIRELESS_SIGNAL_STRENGTH:
        case Param::SECURITY_CODE:
            value = parseU32(data);
            break;
        case Param::MUI:
        case Param::FREE_STORAGE:
        case Param::SERIAL_NUMBER:
            value = parseU64(data);
            break;
        default:
            value = parseByteArray(data);
            break;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Index: " + QString::number(index);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Data: " + value.toString();

    IMpModel* model = new SetParamModel(index, value);

    this->reciever.updateParam(channel, panId, address, model);

    return true;
}
