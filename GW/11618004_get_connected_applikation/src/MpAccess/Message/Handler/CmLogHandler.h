#pragma once

#include "MessageHandler.h"
#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../../Common/Context.h"
#include "../../../Common/Event/NodeRemovedEvent.h"

#include <QtCore/QMap>
#include <QtCore/QVector>
#include <QtCore/QObject>

class MPACCESS_EXPORT CmLogHandler : public MessageHandler
{
    Q_OBJECT

    public:
        explicit CmLogHandler(Context* context, IReciever& reciever, QObject* parent = nullptr);

        bool handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data);

        static const quint8 ID = 0x53;

    private:
        typedef struct
        {
            quint8 logType;
            quint8 expextedSequence;
            quint8 expectedSegments;

            QMap<quint8, QVector<quint8>> data;
        } Node;

        QMap<quint16, Node> nodes;

        QVector<quint8> copyData(QVector<quint8> data);

        void dropPackage(const quint16 address, const QString& message);

        bool parseEventLog(quint8 channel, quint16 panId, quint16 address, Node& node);
        bool parseInstantLog(quint8 channel, quint16 panId, quint16 address, Node& node);
        bool parseHistoryLog(quint8 channel, quint16 panId, quint16 address, Node& node);

        Q_SLOT void reset();
        Q_SLOT void nodeRemoved(const NodeRemovedEvent& event);
};
