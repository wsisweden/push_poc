#include "CmLogHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/CmHistoryLogModel.h"
#include "../Model/CmEventLogModel.h"
#include "../Model/CmInstantLogModel.h"
#include "../../../Common/EventManager.h"
#include "../../../Common/Event/InvalidIndexEvent.h"

#include <QtCore/QLoggingCategory>

const quint8 CmLogHandler::ID;

/**
 * @brief CmLogHandler::CmLogHandler Construct a new CM log handler
 * @param context The context
 * @param reciever The reciever
 */
CmLogHandler::CmLogHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &CmLogHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &CmLogHandler::reset);
    connect(this->context->getEventManager(), &EventManager::nodeRemoved, this, &CmLogHandler::nodeRemoved);
}

/**
 * @brief CmLogHandler::nodeRemoved Node removed handler
 * @param event The event
 */
void CmLogHandler::nodeRemoved(const NodeRemovedEvent& event)
{
    if (this->nodes.contains(event.getAddress()))
        reset();
}

/**
 * @brief CmLogHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool CmLogHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() <= (1 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "CM invalid index";
        this->context->getEventManager()->emitInvalidIndexEvent(InvalidIndexEvent(channel, panId, address));
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    if (this->firstSegment)
    {
        Node node;
        node.expectedSegments = this->segment;
        node.expextedSequence = this->sequence;

        this->nodes[address] = node;
    }

    if (!this->nodes.contains(address))
        return false;

    Node& node = this->nodes[address];

    // Is this package ours?
    if (node.expextedSequence != this->sequence)
        return false;

    // We expect zero segments but becouse we iterate backwards when
    // we parse the message we need to insert at position one.
    if (node.expectedSegments == 0)
        node.data.insert(1, copyData(data));
    else
        node.data.insert(this->segment, copyData(data));

    if (node.expectedSegments == 0)
        qCDebug(QLoggingCategory("mpa")).noquote() << "CM log segment received";
    else
        qCDebug(QLoggingCategory("mpa")).noquote() << QString("CM log segment received (%1/%2)").arg(node.data.count()).arg(node.expectedSegments);

    if (this->firstSegment)
    {
        node.logType = parseU8(data);
        qCDebug(QLoggingCategory("mpa")).noquote() << "CM log type: 0x" + QString::number(node.logType, 16).toUpper();
    }

    // Have we all segments, otherwise append data and wait for more.
    if (node.expectedSegments > 1 && node.data.count() != node.expectedSegments)
        return false;

    if (node.logType == 0)
        return parseHistoryLog(channel, panId, address, node);
    else if (node.logType == 1)
        return parseEventLog(channel, panId, address, node);
    else if (node.logType == 2)
        return parseInstantLog(channel, panId, address, node);
    else
        return false; // Probably wrong logtype
}

/**
 * @brief CmLogHandler::copyData Copy remaining data
 * @param data The data
 */
QVector<quint8> CmLogHandler::copyData(QVector<quint8> data)
{
    QVector<quint8> result;
    for (int i = this->position; i < data.size(); i++)
       result.append(data[i]);

    return result;
}

/**
 * @brief CmLogHandler::parseHistoryLog Handle history log data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param node The node
 * @return True if last segment
 */
bool CmLogHandler::parseHistoryLog(quint8 channel, quint16 panId, quint16 address, Node& node)
{
    for (int i = node.data.count(); i > 0; i -= 2)
    {
        qCDebug(QLoggingCategory("mpa")).noquote() << QString("CM history log segment (%1/%2)").arg(node.data.count() - 1).arg(node.expectedSegments);

        QVector<quint8>& data = node.data[i];
        rewindPosition();

        // Header
        quint8 logType = parseU8(data);
        quint32 chargeIndex = parseU32(data);
        quint32 chargeIndexReset = parseU32(data);
        quint16 recordType = parseU16(data);
        quint8 recordSize = parseU8(data);

        // Segment 1
        quint32 chargeStartTime = parseU32(data);
        quint32 chargeEndTime = parseU32(data);
        quint16 algNo = parseU16(data);
        quint16 capacity = parseU16(data);
        quint16 cells = parseU16(data);
        quint16 ri = parseU16(data);
        quint16 baseload = parseU16(data);
        quint16 thsStart = parseU16(data);
        quint16 thsEnd = parseU16(data);
        quint16 thsMax = parseU16(data);
        quint32 startVoltage = parseU32(data);
        quint32 endVoltage = parseU32(data);
        quint32 chargeTime = parseU32(data);
        quint32 chargedAh = parseU32(data);
        quint32 chargedWh = parseU32(data);
        quint16 chargedAhP  = parseU16(data);
        quint32 equTime = parseU32(data);
        quint32 equAh = parseU32(data);
        quint32 equWh = parseU32(data);
        quint16 chalgErrorSum = parseU16(data);
        quint16 reguErrorSum = parseU16(data);
        quint32 evtIdxStart = parseU32(data);
        quint32 evtIdxStop  = parseU32(data);
        quint32 bid = parseU32(data);
        quint8 bmFgId = parseU8(data);
        quint8 batteryType = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Log type: " + QString::number(logType);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge index: " + QString::number(chargeIndex);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge index reset: " + QString::number(chargeIndexReset);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Record type: " + QString::number(recordType);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Record size: " + QString::number(recordSize);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge start time: " + QString::number(chargeStartTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge end time: " + QString::number(chargeEndTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number: " + QString::number(algNo);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity: " + QString::number(capacity);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cells: " + QString::number(cells);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance: " + QString::number(ri);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load: " + QString::number(baseload);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Temperature hs start: " + QString::number(thsStart);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Temperature hs end: " + QString::number(thsEnd);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Temperature hs max: " + QString::number(thsMax);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Start voltage: " + QString::number(startVoltage);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   End voltage: " + QString::number(endVoltage);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge time: " + QString::number(chargeTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charged Ah: " + QString::number(chargedAh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charged Wh: " + QString::number(chargedWh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charged AhP: " + QString::number(chargedAhP);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalize time: " + QString::number(equTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalize Ah: " + QString::number(equAh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalize Wh: " + QString::number(equWh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Chalg error sum: " + QString::number(chalgErrorSum);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Regu error sum: " + QString::number(reguErrorSum);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Event idx start: " + QString::number(evtIdxStart);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Event idx stop: " + QString::number(evtIdxStop);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery id: " + QString::number(bid);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery manufacturer id: " + QString::number(bmFgId);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery type: " + QString::number(batteryType);

        qCDebug(QLoggingCategory("mpa")).noquote() << QString("CM history log segment (%1/%2)").arg(node.data.count()).arg(node.expectedSegments);

        data = node.data[i - 1];
        rewindPosition();

        // Segment 2
        quint32 bsn = parseU32(data);
        quint32 engineCode = parseU32(data);
        quint32 serialNo = parseU32(data);
        QString algNoName = parseString(data, 8);
        quint8 chargingMode = parseU8(data);
        quint16 chalgStatusSum = parseU16(data);
        quint32 fId = parseU32(data);
        quint32 u32Spare1 = parseU32(data);
        quint32 u32Spare2 = parseU32(data);
        quint16 u16Spare1 = parseU16(data);
        quint16 u16Spare2 = parseU16(data);
        quint8 u8Spare1 = parseU8(data);
        quint8 u8Spare2 = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery serial number: " + QString::number(bsn);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Engine code: " + QString::number(engineCode);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Serial number: " + QString::number(serialNo);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number name: " + algNoName;
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charging mode: " + QString::number(chargingMode);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Chalg status sum: " + QString::number(chalgStatusSum);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Fork lift id: " + QString::number(fId);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: 0x" + QString::number(u32Spare1, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 2: 0x" + QString::number(u32Spare2, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: 0x" + QString::number(u16Spare1, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 2: 0x" + QString::number(u16Spare2, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: 0x" + QString::number(u8Spare1, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 2: 0x" + QString::number(u8Spare2, 16).toUpper();

        IMpModel* model = new CmHistoryLogModel(logType,
                                                chargeIndex,
                                                chargeIndexReset,
                                                recordType,
                                                recordSize,
                                                chargeStartTime,
                                                chargeEndTime,
                                                algNo,
                                                capacity,
                                                cells,
                                                ri,
                                                baseload,
                                                thsStart,
                                                thsEnd,
                                                thsMax,
                                                startVoltage,
                                                endVoltage,
                                                chargeTime,
                                                chargedAh,
                                                chargedWh,
                                                chargedAhP,
                                                equTime,
                                                equAh,
                                                equWh,
                                                chalgErrorSum,
                                                reguErrorSum,
                                                evtIdxStart,
                                                evtIdxStop,
                                                bid,
                                                bmFgId,
                                                batteryType,
                                                bsn,
                                                engineCode,
                                                serialNo,
                                                algNoName,
                                                chargingMode,
                                                chalgStatusSum,
                                                fId,
                                                u32Spare1,
                                                u32Spare2,
                                                u16Spare1,
                                                u16Spare2,
                                                u8Spare1,
                                                u8Spare2);

        this->reciever.addNodeHistoryLog(channel, panId, address, model);
    }

    this->parseCompleted = true;

    return true;
}

/**
 * @brief CmLogHandler::parseEventLog Handle event log data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param node The node
 * @return True if last segment
 */
bool CmLogHandler::parseEventLog(quint8 channel, quint16 panId, quint16 address, Node& node)
{
    for (int i = node.data.count(); i > 0; i--)
    {
        if (node.expectedSegments == 0)
            qCDebug(QLoggingCategory("mpa")).noquote() << "CM event log";
        else
            qCDebug(QLoggingCategory("mpa")).noquote() << QString("CM event log segment (%1/%2)").arg((node.data.count() - i) + 1).arg(node.expectedSegments);

        QVector<quint8>& data = node.data[i];
        rewindPosition();

        // Header
        quint8 logType = parseU8(data);
        quint32 index = parseU32(data);
        quint32 indexReset = parseU32(data);
        quint32 time = parseU32(data);
        quint16 eventId = parseU16(data);
        quint8 dataSize = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Log type: 0x" + QString::number(logType, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Index: " + QString::number(index);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Index reset: " + QString::number(indexReset);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Time: " + QString::number(time);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Event id: " + QString::number(eventId);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Data size: " + QString::number(dataSize);

        QList<QVariant> list;
        if (eventId == 1) // Time set
        {
            list.append(parseU32(data));    // Old time
            list.append(parseU8(data));     // Source
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Old time: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Source: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(4).toUInt());
        }
        else if (eventId == 2) // Charge started
        {
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(2).toUInt());
        }
        else if (eventId == 3) // Charge ended
        {
            list.append(parseU32(data));    // U32 spare 1node
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(2).toUInt());
        }
        else if (eventId == 4) // Chalg error
        {
            list.append(parseU16(data));    // Chalg error
            list.append(parseU16(data));    // Active
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Chalg error: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Active: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(4).toUInt());
        }
        else if (eventId == 5) // Regu error
        {
            list.append(parseU16(data));    // Regu error
            list.append(parseU16(data));    // Active
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Regu error: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Active: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(4).toUInt());
        }
        else if (eventId == 6) // Parameter changed
        {
            list.append(parseU16(data));    // Algorithm number new
            list.append(parseU16(data));    // Algorithm number old
            list.append(parseU16(data));    // Capacity new
            list.append(parseU16(data));    // Capacity old
            list.append(parseU16(data));    // Cell new
            list.append(parseU16(data));    // Cell old
            list.append(parseU16(data));    // Cable resistance new
            list.append(parseU16(data));    // Cable resistance old
            list.append(parseU16(data));    // Base load new
            list.append(parseU16(data));    // Base load old
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number new: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number old: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity new: " + QString::number((quint16)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity old: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cell new: " + QString::number((quint16)list.at(4).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cell old: " + QString::number((quint16)list.at(5).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance new: " + QString::number((quint16)list.at(6).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance old: " + QString::number((quint16)list.at(7).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load new: " + QString::number((quint16)list.at(8).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load old: " + QString::number((quint16)list.at(9).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(10).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(11).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(12).toUInt());
        }
        else if (eventId == 7) // VCC
        {
            list.append(parseU8(data));     // VCC status
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   VCC status: " + QString::number((quint8)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(3).toUInt());
        }
        else if (eventId == 8) // Voltage calibration
        {
            list.append(parseF32(data));    // U slope new
            list.append(parseF32(data));    // U slope old
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   U slope new: " + QString::number(list.at(0).toFloat());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U slope old: " + QString::number(list.at(1).toFloat());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(4).toUInt());
        }
        else if (eventId == 9) // Current calibration
        {
            list.append(parseF32(data));    // I slope new
            list.append(parseF32(data));    // I slope old
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   I slope new: " + QString::number(list.at(0).toFloat());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   I slope old: " + QString::number(list.at(1).toFloat());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(4).toUInt());
        }
        else if (eventId == 10) // Start network
        {
            list.append(parseU8(data));     // Channel
            list.append(parseU16(data));    // PanId
            list.append(parseU16(data));    // Network Id
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Channel: " + QString::number((quint8)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   PanId: 0x" + QString::number((quint16)list.at(1).toUInt(), 16).toUpper();
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Network Id: 0x" + QString::number((quint16)list.at(2).toUInt(), 16).toUpper();
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(4).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(5).toUInt());
        }
        else if (eventId == 11) // Join network
        {
            list.append(parseU8(data));     // Channel
            list.append(parseU16(data));    // PanId
            list.append(parseU16(data));    // Network Id
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Channel: " + QString::number((quint8)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   PanId: 0x" + QString::number((quint16)list.at(1).toUInt(), 16).toUpper();
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Network Id: 0x" + QString::number((quint16)list.at(2).toUInt(), 16).toUpper();
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(4).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(5).toUInt());
        }
        else if (eventId == 12) // Startup
        {
            list.append(parseU32(data));    // Firmware type
            list.append(parseU32(data));    // Firmware version
            list.append(parseU32(data));    // Radio firmware type
            list.append(parseU32(data));    // Radio firmware version
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware type: " + QString::number((quint32)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware version: " + QString::number((quint32)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Radio firmware type: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Radio firmware version: " + QString::number((quint32)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(4).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(5).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(6).toUInt());
        }
        else if (eventId == 13) // Settings
        {
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(2).toUInt());
        }
        else if (eventId == 14) // Assert
        {
            list.append(parseU8(data));     // AssertId
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   AssertId: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(3).toUInt());
        }
        else if (eventId == 15) // Sup error
        {
            list.append(parseU16(data));    // Status flags
            list.append(parseU16(data));    // Active
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Status flags: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Active: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(4).toUInt());
        }
        else if (eventId == 16) // Ri slope
        {
            list.append(parseU32(data));    // Ri slope new
            list.append(parseU32(data));    // Ri slope old
            list.append(parseU32(data));    // U32 spare 1
            list.append(parseU16(data));    // U16 spare 1
            list.append(parseU8(data));     // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Ri slope new: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Ri slope old: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number((quint8)list.at(4).toUInt());
        }

        IMpModel* model = new CmEventLogModel(logType,
                                              index,
                                              indexReset,
                                              time,
                                              eventId,
                                              dataSize,
                                              list);

        this->reciever.addNodeEventLog(channel, panId, address, model);

        list.clear();
    }

    this->parseCompleted = true;

    return true;
}

/**
 * @brief CmLogHandler::parseInstantLog Handle instant log data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param node The node
 * @return True if last segment
 */
bool CmLogHandler::parseInstantLog(quint8 channel, quint16 panId, quint16 address, Node& node)
{
    quint32 index = 0, time = 0;
    quint16 instLogSamplePeriod = 0, recordType = 0;
    quint8 logType = 0, recordSize = 0, noOfRecords = 0;

    int offset = 0; // NOTE! We can not use record offset from the package because older firmware have issue with correct value.
    bool headerExpected = true;
    for (int i = node.data.count(); i > 0; i--)
    {
        if (node.expectedSegments == 0)
            qCDebug(QLoggingCategory("mpa")).noquote() << "CM instant log";
        else
            qCDebug(QLoggingCategory("mpa")).noquote() << QString("CM instant log segment (%1/%2)").arg((node.data.count() - i) + 1).arg(node.expectedSegments);

        QVector<quint8>& data = node.data[i];
        rewindPosition();

        if (headerExpected)
        {
            // Header
            logType = parseU8(data);
            index = parseU32(data);
            noOfRecords = parseU8(data);
            time = parseU32(data);
            instLogSamplePeriod = parseU16(data);
            recordType = parseU16(data);
            recordSize = parseU8(data);

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Log type: 0x" + QString::number(logType, 16).toUpper();
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Index: " + QString::number(index);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   No of records: " + QString::number(noOfRecords);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Time: " + QString::number(time);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Sample period: " + QString::number(instLogSamplePeriod);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Record type: " + QString::number(recordType);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Record size: " + QString::number(recordSize);

            headerExpected = false;
        }

        QList<QVariant> list;
        while (this->position + 21 <= data.size())
        {
            // Data
            list.append(parseU8(data));             // Record offset
            list.append(parseU16(data));            // Chalg error
            list.append(parseU16(data));            // Regu error
            list.append((qint32)parseU32(data));    // U chalg
            list.append((qint32)parseU32(data));    // I chalg
            list.append((qint16)parseU16(data));    // Temperature board
            list.append((qint16)parseU16(data));    // Temperature heat sink
            list.append(parseU32(data));            // U32 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "CM instant data " + QString::number(offset + 1); //QString::number((quint8)list.at(0).toUInt() + 1);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Record offset: " + QString::number(offset); //QString::number((quint8)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Chalg error: " + QString::number((quint16)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Regu error: " + QString::number((quint16)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U chalg: " + QString::number((qint32)list.at(3).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   I chalg: " + QString::number((qint32)list.at(4).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Temperature board: " + QString::number((qint16)list.at(5).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Temperature heat sink: " + QString::number((qint16)list.at(6).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number((quint32)list.at(7).toUInt());

            offset++;
        }

        IMpModel* model = new CmInstantLogModel(logType,
                                                index + offset, //list.at(0).toUInt(), // Index + record offset.
                                                noOfRecords,
                                                time,
                                                instLogSamplePeriod,
                                                recordType,
                                                recordSize,
                                                list);

        this->reciever.addNodeInstantLog(channel, panId, address, model);

        list.clear();
    }

    this->parseCompleted = true;

    return true;
}

/**
 * @brief CmLogHandler::reset Reset handler
 */
void CmLogHandler::reset()
{
    this->parseCompleted = false;

    for (Node& node : this->nodes)
        node.data.clear();

    this->nodes.clear();

    qCDebug(QLoggingCategory("mpa")).noquote() << "Clear CM log cache";
}

/**
 * @brief CmLogHandler::dropPackage Drop package
 * @param address The node address
 */
void CmLogHandler::dropPackage(const quint16 address, const QString& message)
{
    Q_UNUSED(address);

    qCWarning(QLoggingCategory("mpa")).noquote() << message;
    reset();
}
