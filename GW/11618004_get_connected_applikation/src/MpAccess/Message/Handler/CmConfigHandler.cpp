#include "CmConfigHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/CmConfigModel.h"

#include <QtCore/QLoggingCategory>

const quint8 CmConfigHandler::ID;

/**
 * @brief CmConfigHandler::CmConfigHandler Construct a new CM configuration handler
 * @param context The context
 * @param reciever The reciever
 */
CmConfigHandler::CmConfigHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &CmConfigHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &CmConfigHandler::reset);
    connect(this->context->getEventManager(), &EventManager::nodeRemoved, this, &CmConfigHandler::nodeRemoved);
}

/**
 * @brief CmConfigHandler::nodeRemoved Node removed handler
 * @param event The event
 */
void CmConfigHandler::nodeRemoved(const NodeRemovedEvent& event)
{
    if (this->nodes.contains(event.getAddress()))
        reset();
}

/**
 * @brief CmConfigHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool CmConfigHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < (45 + MESSAGE_HEADER)) // || data.size() != (88 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "CM config error";
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    if (this->firstSegment)
    {
        Node node;
        node.expectedSegments = this->segment;
        node.expextedSequence = this->sequence;

        this->nodes[address] = node;
    }

    if (!this->nodes.contains(address))
        return false;

    Node& node = this->nodes[address];

    // Is this package ours?
    if (node.expextedSequence != this->sequence)
        return false;

    // Do we have all segments, otherwise append data and wait for more.
    node.data.insert(this->segment, copyData(data));

    qCDebug(QLoggingCategory("mpa")).noquote() << QString("CM config segment received (%1/%2)").arg(node.data.count()).arg(node.expectedSegments);

    if (node.data.count() != node.expectedSegments)
        return false;

    for (int i = node.data.count(); i > 0; i -= 4)
    {
        qCDebug(QLoggingCategory("mpa")).noquote() << "CM config segment 1";

        QVector<quint8>& data = node.data[i];
        rewindPosition();

        quint16 panId2 = parseU16(data);
        quint8 channel2 = parseU8(data);
        quint16 nwkId = parseU16(data);
        quint32 serialNo = parseU32(data);
        quint32 engineCode = parseU32(data);
        quint8 batteryType = parseU8(data);
        quint16 algNo = parseU16(data);
        quint16 capacity = parseU16(data);
        quint16 cableRes = parseU16(data);
        quint16 cells = parseU16(data);
        quint16 baseLoad = parseU16(data);
        quint8 powerGroup = parseU8(data);
        quint32 iacLimit = (qint32)parseU32(data);
        quint32 pacLimit = (qint32)parseU32(data);
        quint32 idcLimit = (qint32)parseU32(data);
        quint32 udcLimit = (qint32)parseU32(data);
        quint32 u32Spare6 = parseU32(data);
        quint32 u32Spare7 = parseU32(data);
        quint32 u32Spare8 = parseU32(data);
        quint32 u32Spare9 = parseU32(data);
        quint32 u32Spare10 = parseU32(data);
        quint8 displayContrast = parseU8(data);
        quint8 ledBrightnessMax = parseU8(data);
        quint8 ledBrightnessDim = parseU8(data);
        quint8 language = parseU8(data);
        quint8 timeDateFormat = parseU8(data);
        quint8 buttonF1Func = parseU8(data);
        quint8 buttonF2Func = parseU8(data);
        quint8 remoteInFunc = parseU8(data);
        quint8 remoteOutFunc = parseU8(data);
        quint8 remoteOutAlarmVar = parseU8(data);
        quint8 remoteOutPhaseAlarmVar = parseU8(data);
        quint8 waterFunction = parseU8(data);
        quint8 waterVar = parseU8(data);
        quint8 airPumpVar = parseU8(data);
        quint8 airPumpVar2 = parseU8(data);
        quint8 parallelControlFunc = parseU8(data);
        quint8 timeRestriction = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   PanId: 0x" + QString::number(panId2, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Channel: " + QString::number(channel2);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Network Address: " + QString::number(nwkId);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Serial Number: " + QString::number(serialNo);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Engine code: " + QString::number(engineCode);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery type: " + QString::number(batteryType);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Alg no: " + QString::number(algNo);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity: " + QString::number(capacity);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance: " + QString::number(cableRes);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cells: " + QString::number(cells);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load: " + QString::number(baseLoad);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Power group: " + QString::number(powerGroup);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   I AC limit: " + QString::number(iacLimit);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   P AC limit: " + QString::number(pacLimit);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   I AC limit: " + QString::number(idcLimit);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U DC limit0x53: " + QString::number(udcLimit);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 Spare 6: " + QString::number(u32Spare6);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 Spare 7: " + QString::number(u32Spare7);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 Spare 8: " + QString::number(u32Spare8);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 Spare 9: " + QString::number(u32Spare9);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 Spare 10: " + QString::number(u32Spare10);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Display contrast: " + QString::number(displayContrast);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   LED brightness max: " + QString::number(ledBrightnessMax);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   LED brightness dim: " + QString::number(ledBrightnessDim);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Language: " + QString::number(language);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Time date format: " + QString::number(timeDateFormat);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Button F1 function: " + QString::number(buttonF1Func);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Button F2 function: " + QString::number(buttonF2Func);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Remote in function: " + QString::number(remoteInFunc);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Remote out function: " + QString::number(remoteOutFunc);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Remote out alarm param: " + QString::number(remoteOutAlarmVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Remote out phase alarm param: " + QString::number(remoteOutPhaseAlarmVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Water function: " + QString::number(waterFunction);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Water param: " + QString::number(waterVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Air pump param: " + QString::number(airPumpVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Air pump param 2: " + QString::number(airPumpVar2);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Parallel control function: " + QString::number(parallelControlFunc);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Time restriction: " + QString::number(timeRestriction);

        QVector<Restriction> chargingRestrictions1;
        for (int j = 0; j <= 1; j++)
        {
            chargingRestrictions1.push_back(Restriction());
            chargingRestrictions1[j].ctrl = parseU8(data);
            chargingRestrictions1[j].fromHour = parseU8(data);
            chargingRestrictions1[j].fromMin = parseU8(data);
            chargingRestrictions1[j].toHour = parseU8(data);
            chargingRestrictions1[j].toMin = parseU8(data);

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Ctrl " + QString::number(j) + ": " + QString::number(chargingRestrictions1[j].ctrl);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   From hour " + QString::number(j) + ": " + QString::number(chargingRestrictions1[j].fromHour);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   From min " + QString::number(j) + ": " + QString::number(chargingRestrictions1[j].fromMin);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   To hour " + QString::number(j) + ": " + QString::number(chargingRestrictions1[j].toHour);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   To min " + QString::number(j) + ": " + QString::number(chargingRestrictions1[j].toMin);
        }

        qCDebug(QLoggingCategory("mpa")).noquote() << "CM config segment 2";

        data = node.data[i - 1];
        rewindPosition();

        QVector<Restriction> chargingRestrictions2;
        for (int k = 0; k <= 17; k++)
        {
            chargingRestrictions2.push_back(Restriction());
            chargingRestrictions2[k].ctrl = parseU8(data);
            chargingRestrictions2[k].fromHour = parseU8(data);
            chargingRestrictions2[k].fromMin = parseU8(data);

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Ctrl " + QString::number(k) + ": " + QString::number(chargingRestrictions2[k].ctrl);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   From hour " + QString::number(k) + ": " + QString::number(chargingRestrictions2[k].fromHour);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   From min " + QString::number(k) + ": " + QString::number(chargingRestrictions2[k].fromMin);

            if (this->position == data.size())
                break;

            chargingRestrictions2[k].toHour = parseU8(data);
            chargingRestrictions2[k].toMin = parseU8(data);

            qCDebug(QLoggingCategory("mpa")).noquote() << "   To hour " + QString::number(k) + ": " + QString::number(chargingRestrictions2[k].toHour);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   To min " + QString::number(k) + ": " + QString::number(chargingRestrictions2[k].toMin);
        }

        qCDebug(QLoggingCategory("mpa")).noquote() << "CM config segment 3";

        data = node.data[i - 2];
        rewindPosition();

        chargingRestrictions2[17].toHour = parseU8(data);
        chargingRestrictions2[17].toMin = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   To hour 19: " + QString::number(chargingRestrictions2[17].toHour);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   To min 19: " + QString::number(chargingRestrictions2[17].toMin);

        QVector<Restriction> chargingRestrictions3;
        chargingRestrictions3.push_back(Restriction());
        chargingRestrictions3[0].ctrl = parseU8(data);
        chargingRestrictions3[0].fromHour = parseU8(data);
        chargingRestrictions3[0].fromMin = parseU8(data);
        chargingRestrictions3[0].toHour = parseU8(data);
        chargingRestrictions3[0].toMin = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Ctrl 20: " + QString::number(chargingRestrictions3[0].ctrl);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   From hour 20: " + QString::number(chargingRestrictions3[0].fromHour);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   From min 20: " + QString::number(chargingRestrictions3[0].fromMin);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   To hour 20: " + QString::number(chargingRestrictions3[0].toHour);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   To min 20: " + QString::number(chargingRestrictions3[0].toMin);

        quint8 canMode = parseU8(data);
        quint16 canNwkCtrl = parseU16(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   CAN Mode: " + QString::number(canMode);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   CAN Nwk Ctrl: " + QString::number(canNwkCtrl);

        QVector<quint32> pinOutSelect;
        for (int l = 0; l <= 7; l++)
        {
            pinOutSelect.push_back(parseU32(data));
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Pin out select " + QString::number(l) + ": " + QString::number(pinOutSelect[l]);
        }

        QVector<quint32> pinInSelect;
        for (int m = 0; m <= 7; m++)
        {
            pinInSelect.push_back(parseU32(data));
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Pin in select " + QString::number(m) + ": " + QString::number(pinInSelect[m]);
        }

        quint8 radioMode = parseU8(data);
        quint8 chargingMode = parseU8(data);
        quint8 nwkSettings = parseU8(data);
        quint32 securityCode1 = parseU32(data);
        quint32 securityCode2 = parseU32(data);
        quint8 backlightTime = parseU8(data);
        quint16 instLogSamplePeriod = parseU16(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Radio mode: " + QString::number(radioMode);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charging mode: " + QString::number(chargingMode);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Network Settings: " + QString::number(nwkSettings);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Security code 1: " + QString::number(securityCode1);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Security code 2: " + QString::number(securityCode2);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Backlight time: " + QString::number(backlightTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Instant log sample period: " + QString::number(instLogSamplePeriod);

        qCDebug(QLoggingCategory("mpa")).noquote() << "CM config segment 4";

        data = node.data[i - 3];
        rewindPosition();

        quint32 cId = parseU32(data);
        quint8 bbcFunc = parseU8(data);
        quint8 bbcVar = parseU8(data);
        quint8 equalizeFunc = parseU8(data);
        quint8 equalizeVar = parseU8(data);
        quint8 equalizeVar2 = parseU8(data);
        quint8 routing = parseU8(data);
        quint8 bitConfig = parseU8(data);
        quint8 remoteOutBBCVar = parseU8(data);
        quint8 canBps = parseU8(data);
        quint8 extraChargeFunc = parseU8(data);
        quint8 extraChargeVar = parseU8(data);
        quint8 extraChargeVar2 = parseU8(data);
        quint8 extraChargeVar3 = parseU8(data);
        quint32 hardwareType = parseU32(data);
        quint32 hardwareVersion = parseU32(data);
        quint32 dplPacTot = parseU32(data);
        quint32 u32Spare4 = parseU32(data);
        quint16 batteryTemp = (qint16)parseU16(data);
        quint16 airPumpAlarmLow = parseU16(data);
        quint16 airPumpAlarmHigh = parseU16(data);
        quint16 u16Spare4 = parseU16(data);
        quint8 canNodeId = parseU8(data);
        quint8 dplFunc = parseU8(data);
        quint8 dplPrio = parseU8(data);
        quint8 dplPacDef = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   CID: " + QString::number(cId);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   BBC function: " + QString::number(bbcFunc);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   BBC param: " + QString::number(bbcVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalizing function: " + QString::number(equalizeFunc);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalizing param: " + QString::number(equalizeVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalizing param 2: " + QString::number(equalizeVar2);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Routing: " + QString::number(routing);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Bit config: " + QString::number(bitConfig);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Remote out BBC param: " + QString::number(remoteOutBBCVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   CAN bps: " + QString::number(canBps);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Extra charge function: " + QString::number(extraChargeFunc);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Extra charge param: " + QString::number(extraChargeVar);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Extra charge param 2: " + QString::number(extraChargeVar2);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Extra charge param 3: " + QString::number(extraChargeVar3);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Hardware type: " + QString::number(hardwareType);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Hardware version: " + QString::number(hardwareVersion);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   DPL PAC total: " + QString::number(dplPacTot);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 4: " + QString::number(u32Spare4);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery temp: " + QString::number(batteryTemp);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Air pump alarm low: " + QString::number(airPumpAlarmLow);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Air pump alarm high: " + QString::number(airPumpAlarmHigh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 4: " + QString::number(u16Spare4);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   CAN node id: " + QString::number(canNodeId);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   DPL function: " + QString::number(dplFunc);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   DPL priority: " + QString::number(dplPrio);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   DPL PAC default limit: " + QString::number(dplPacDef);

        QVector<quint8> chargingRestrictions;
        for (Restriction restriction : chargingRestrictions1)
        {
            chargingRestrictions.push_back(restriction.ctrl);
            chargingRestrictions.push_back(restriction.fromHour);
            chargingRestrictions.push_back(restriction.fromMin);
            chargingRestrictions.push_back(restriction.toHour);
            chargingRestrictions.push_back(restriction.toMin);
        }

        for (Restriction restriction : chargingRestrictions2)
        {
            chargingRestrictions.push_back(restriction.ctrl);
            chargingRestrictions.push_back(restriction.fromHour);
            chargingRestrictions.push_back(restriction.fromMin);
            chargingRestrictions.push_back(restriction.toHour);
            chargingRestrictions.push_back(restriction.toMin);
        }

        for (Restriction restriction : chargingRestrictions3)
        {
            chargingRestrictions.push_back(restriction.ctrl);
            chargingRestrictions.push_back(restriction.fromHour);
            chargingRestrictions.push_back(restriction.fromMin);
            chargingRestrictions.push_back(restriction.toHour);
            chargingRestrictions.push_back(restriction.toMin);
        }

        IMpModel* model = new CmConfigModel(panId2,
                                            channel2,
                                            nwkId,
                                            serialNo,
                                            engineCode,
                                            batteryType,
                                            algNo,
                                            capacity,
                                            cableRes,
                                            cells,
                                            baseLoad,
                                            powerGroup,
                                            iacLimit,
                                            pacLimit,
                                            idcLimit,
                                            udcLimit,
                                            u32Spare6,
                                            u32Spare7,
                                            u32Spare8,
                                            u32Spare9,
                                            u32Spare10,
                                            displayContrast,
                                            ledBrightnessMax,
                                            ledBrightnessDim,
                                            language,
                                            timeDateFormat,
                                            buttonF1Func,
                                            buttonF2Func,
                                            remoteInFunc,
                                            remoteOutFunc,
                                            remoteOutAlarmVar,
                                            remoteOutPhaseAlarmVar,
                                            waterFunction,
                                            waterVar,
                                            airPumpVar,
                                            airPumpVar2,
                                            parallelControlFunc,
                                            timeRestriction,
                                            canMode,
                                            canNwkCtrl,
                                            pinInSelect,
                                            pinOutSelect,
                                            radioMode,
                                            chargingMode,
                                            nwkSettings,
                                            securityCode1,
                                            securityCode2,
                                            backlightTime,
                                            instLogSamplePeriod,
                                            cId,
                                            bbcFunc,
                                            bbcVar,
                                            equalizeFunc,
                                            equalizeVar,
                                            equalizeVar2,
                                            routing,
                                            bitConfig,
                                            remoteOutBBCVar,
                                            canBps,
                                            extraChargeFunc,
                                            extraChargeVar,
                                            extraChargeVar2,
                                            extraChargeVar3,
                                            hardwareType,
                                            hardwareVersion,
                                            dplPacTot,
                                            u32Spare4,
                                            batteryTemp,
                                            airPumpAlarmLow,
                                            airPumpAlarmHigh,
                                            u16Spare4,
                                            canNodeId,
                                            dplFunc,
                                            dplPrio,
                                            dplPacDef,
                                            chargingRestrictions);

        this->reciever.addNodeConfig(channel, panId, address, model);

        this->parseCompleted = true;

        return true;
    }

    return false;
}

/**
 * @brief CmConfigHandler::copyData Copy remaining data
 * @param data The data
 */
QVector<quint8> CmConfigHandler::copyData(QVector<quint8> data)
{
    QVector<quint8> result;
    for (int i = this->position; i < data.size(); i++)
       result.append(data[i]);

    return result;
}

/**
 * @brief CmConfigHandler::reset Reset handler
 */
void CmConfigHandler::reset()
{
    this->parseCompleted = false;

    for (Node& node : this->nodes)
        node.data.clear();

    this->nodes.clear();

    qCDebug(QLoggingCategory("mpa")).noquote() << "Clear CM config cache";
}

/**
 * @brief CmConfigHandler::dropPackage Drop package
 * @param address The node address
 */
void CmConfigHandler::dropPackage(const quint16 address, const QString& message)
{
    Q_UNUSED(address);

    qCWarning(QLoggingCategory("mpa")).noquote() << message;
    reset();
}
