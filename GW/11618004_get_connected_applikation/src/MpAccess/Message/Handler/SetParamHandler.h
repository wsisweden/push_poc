#pragma once

#include "MessageHandler.h"
#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../../Common/Context.h"

#include <QtCore/QMap>
#include <QtCore/QVector>
#include <QtCore/QObject>

class MPACCESS_EXPORT SetParamHandler : public MessageHandler
{
    Q_OBJECT

    public:
        explicit SetParamHandler(Context* context, IReciever& reciever, QObject* parent = nullptr);

        bool handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data);

        static const quint8 ID = 0x80;
};
