#include "MessageHandler.h"

#include <QtCore/QLoggingCategory>

/**
 * @brief MessageHandler::MessageHandler Construct a new message handler
 * @param context The context
 * @param reciever The reciever
 */
MessageHandler::MessageHandler(Context* context, IReciever& reciever, QObject* parent)
    : QObject(parent)
    , reciever(reciever)
{
    this->context = context;
}

/**
 * @brief MessageHandler::rewindPosition Rewind position
 */
void MessageHandler::rewindPosition()
{
    this->segment = 0;
    this->position = 0;
    this->firstSegment = false;
    this->lastSegment = false;
}

/**
 * @brief MessageHandler::parseHeader Parse data
 * @param data The data
 * @return Parsed data
 */
void MessageHandler::parseHeader(QVector<quint8>& data)
{
    this->sequence = parseU8(data);

    quint8 control = parseU8(data);
    this->segment = control & 0x7f;
    this->firstSegment = (control & 0x80) == 0x80 || this->segment == 0;
    this->lastSegment = (this->segment == 1);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Segment: " + QString::number(this->segment);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Sequence: " + QString::number(this->sequence);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   First Segment: " + QString("%1").arg((this->firstSegment == true) ? "true" : "false");
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Last Segment: " + QString("%1").arg((this->lastSegment == true) ? "true" : "false");
}

/**
 * @brief MessageHandler::parseU8 Parse data
 * @param data The data
 * @return Parsed data
 */
quint8 MessageHandler::parseU8(QVector<quint8>& data)
{
    quint8 temp = data[this->position];
    this->position += 1;

    return temp;
}

/**
 * @brief MessageHandler::parseU16 Parse data
 * @param data The data
 * @return Parsed data
 */
quint16 MessageHandler::parseU16(QVector<quint8>& data)
{
    quint16 temp = ((quint16)data[this->position] << 8) + data[this->position + 1];
    this->position += 2;

    return temp;
}

/**
 * @brief MessageHandler::parseU32 Parse data
 * @param data The data
 * @return Parsed data
 */
quint32 MessageHandler::parseU32(QVector<quint8>& data)
{
    quint32 temp = ((quint32)data[this->position] << 24) +
                   ((quint32)data[this->position + 1] << 16) +
                   ((quint32)data[this->position + 2] << 8) + data[this->position + 3];
    this->position += 4;

    return temp;
}

/**
 * @brief MessageHandler::parseF32 Parse data
 * @param data The data
 * @return Parsed data
 */
float MessageHandler::parseF32(QVector<quint8>& data)
{
    quint32 temp = ((quint32)data[this->position] << 24) +
                   ((quint32)data[this->position + 1] << 16) +
                   ((quint32)data[this->position + 2] << 8) + data[this->position + 3];
    this->position += 4;

    return  *reinterpret_cast<float*>(&temp);
}

/**
 * @brief MessageHandler::parseU64 Parse data
 * @param data The data
 * @return Parsed data
 */
quint64 MessageHandler::parseU64(QVector<quint8>& data)
{
    quint64 temp = ((quint64)data[this->position] << 56) +
                   ((quint64)data[this->position + 1] << 48) +
                   ((quint64)data[this->position + 2] << 40) +
                   ((quint64)data[this->position + 3] << 32) +
                   ((quint64)data[this->position + 4] << 24) +
                   ((quint64)data[this->position + 5] << 16) +
                   ((quint64)data[this->position + 6] << 8) + data[this->position + 7];
    this->position += 8;

    return temp;
}

/**
 * @brief MessageHandler::parseString Parse string data
 * @param data The data
 * @param length The number of bytes to parse
 * @return The string
 */
QString MessageHandler::parseString(QVector<quint8>& data, quint8 length)
{
    QString temp;
    int i = 0;
    while (i < length)
    {
        i++;

        QChar c = parseU8(data);
        if (c == '\0')
            break;

        temp.append(c);
    }

    this->position += length - i;

    return temp;
}

/**
 * @brief MessageHandler::parseByteArray Parse data
 * @param data The data
 * @return Parsed data
 */
QByteArray MessageHandler::parseByteArray(QVector<quint8>& data)
{
    return QByteArray(reinterpret_cast<const char*>(data.data() + this->position), data.size() - this->position);
}
