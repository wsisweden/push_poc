#include "BmLogHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/BmHistoryLogModel.h"
#include "../Model/BmEventLogModel.h"
#include "../Model/BmInstantLogModel.h"
#include "../../../Common/EventManager.h"
#include "../../../Common/Event/InvalidIndexEvent.h"

#include <QtCore/QLoggingCategory>

const quint8 BmLogHandler::ID;

/**
 * @brief BmLogHandler::BmLogHandler Construct a new BM log handler
 * @param context The context
 * @param reciever The reciever
 */
BmLogHandler::BmLogHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &BmLogHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &BmLogHandler::reset);
    connect(this->context->getEventManager(), &EventManager::nodeRemoved, this, &BmLogHandler::nodeRemoved);
}

/**
 * @brief BmLogHandler::nodeRemoved Node removed handler
 * @param event The event
 */
void BmLogHandler::nodeRemoved(const NodeRemovedEvent& event)
{
    if (this->nodes.contains(event.getAddress()))
        reset();
}

/**
 * @brief BmLogHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool BmLogHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() <= (1 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "BM invalid index";
        this->context->getEventManager()->emitInvalidIndexEvent(InvalidIndexEvent(channel, panId, address));
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    if (this->firstSegment)
    {
        Node node;
        node.expectedSegments = this->segment;
        node.expextedSequence = this->sequence;

        this->nodes[address] = node;
    }

    if (!this->nodes.contains(address))
        return false;

    Node& node = this->nodes[address];

    // Is this package ours?
    if (node.expextedSequence != this->sequence)
        return false;

    // We expect zero segments but becouse we iterate backwards when
    // we parse the message we need to insert at position one.
    if (node.expectedSegments == 0)
        node.data.insert(1, copyData(data));
    else
        node.data.insert(this->segment, copyData(data));

    if (node.expectedSegments == 0)
        qCDebug(QLoggingCategory("mpa")).noquote() << "BM log segment received";
    else
        qCDebug(QLoggingCategory("mpa")).noquote() << QString("BM log segment received (%1/%2)").arg(node.data.count()).arg(node.expectedSegments);

    if (this->firstSegment)
    {
        node.logType = parseU8(data);
        qCDebug(QLoggingCategory("mpa")).noquote() << "BM log type: 0x" + QString::number(node.logType, 16).toUpper();
    }

    // Have we all segments, otherwise append data and wait for more.
    if (node.expectedSegments > 1 && node.data.count() != node.expectedSegments)
        return false;

    if (node.logType == 0)
        return parseHistoryLog(channel, panId, address, node);
    else if (node.logType == 1)
        return parseEventLog(channel, panId, address, node);
    else if (node.logType == 2)
        return parseInstantLog(channel, panId, address, node);
    else
        return false; // Probably wrong logtype
}

/**
 * @brief BmLogHandler::copyData Copy remaining data
 * @param data The data
 */
QVector<quint8> BmLogHandler::copyData(QVector<quint8> data)
{
    QVector<quint8> result;
    for (int i = this->position; i < data.size(); i++)
       result.append(data[i]);

    return result;
}

/**
 * @brief BmLogHandler::parseHistoryLog Handle history log response
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param node The node
 * @return True if last segment
 */
bool BmLogHandler::parseHistoryLog(quint8 channel, quint16 panId, quint16 address, Node& node)
{
    for (int i = node.data.count(); i > 0; i -= 2)
    {
        qCDebug(QLoggingCategory("mpa")).noquote() << QString("BM history log segment (%1/%2)").arg(node.data.count() - 1).arg(node.expectedSegments);

        QVector<quint8>& data = node.data[i];
        rewindPosition();

        // Header
        quint8 logType = parseU8(data);
        quint32 cycleIndex = parseU32(data);
        quint32 cycleIndexReset = parseU32(data);
        quint16 recordType = parseU16(data);
        quint8 recordSize = parseU8(data);

        // Segment 1
        quint32 bid = parseU32(data);
        quint16 algNo = parseU16(data);
        QString algNoName = parseString(data, 8);
        quint16 algNoVer = parseU16(data);
        quint16 capacity = parseU16(data);
        quint16 cableRes = parseU16(data);
        quint16 cells = parseU16(data);
        quint16 baseLoad = parseU16(data);
        quint32 dStartTime = parseU32(data);
        quint16 dStartVPC = parseU16(data);
        quint32 dRegTime = parseU32(data);
        quint16 dStartTBatt = (qint16)parseU16(data);
        quint16 dMaxTBatt = (qint16)parseU16(data);
        quint16 dMinTBatt = (qint16)parseU16(data);
        quint16 dAh  = parseU16(data);
        quint16 dRegAh = parseU16(data);
        quint32 dWh = parseU32(data);
        quint16 dRegWh = parseU16(data);
        quint8 dStartSOC = parseU8(data);
        quint16 dAhI25 = parseU16(data);
        quint32 dTimeI25 = parseU32(data);
        quint16 dAhI15  = parseU16(data);
        quint32 dTimeI15 = parseU32(data);
        quint16 dAhI0 = parseU16(data);
        quint32 dTimeI0 = parseU32(data);
        quint16 dIMax = parseU16(data);
        quint16 dUMin  = parseU16(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Log type: 0x" + QString::number(logType, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cycle index: " + QString::number(cycleIndex);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cycle index reset: " + QString::number(cycleIndexReset);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Record type: " + QString::number(recordType);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Record size: " + QString::number(recordSize);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery id: " + QString::number(bid);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number: " + QString::number(algNo);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number name: " + algNoName;
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity: " + QString::number(capacity);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance: " + QString::number(cableRes);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Cells: " + QString::number(cells);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load: " + QString::number(baseLoad);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge start time: " + QString::number(dStartTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge start VPC: " + QString::number(dStartVPC);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge regenerated time: " + QString::number(dRegTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge battery temperature start: " + QString::number(dStartTBatt);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge battery temperature max: " + QString::number(dMaxTBatt);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge battery temperature min: " + QString::number(dMinTBatt);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge regenerated Ah: " + QString::number(dRegAh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge Wh: " + QString::number(dWh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge regenerated Wh: " + QString::number(dRegWh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge state of charge start: " + QString::number(dStartSOC);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge Ah I25: " + QString::number(dAhI25);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge time I25: " + QString::number(dTimeI25);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge Ah I15: " + QString::number(dAhI15);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge time I15: " + QString::number(dTimeI15);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge AhI0: " + QString::number(dAhI0);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge time I0: " + QString::number(dTimeI0);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge current max: " + QString::number(dIMax);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Discharge VPC min: " + QString::number(dUMin);

        qCDebug(QLoggingCategory("mpa")).noquote() << QString("BM history log segment (%1/%2)").arg(node.data.count()).arg(node.expectedSegments);

        data = node.data[i - 1];
        rewindPosition();

        // Segment 2
        quint32 cStartTime = parseU32(data);
        quint32 cEndTime = parseU32(data);
        quint16 cStartVPC = parseU16(data);
        quint16 cEndVPC  = parseU16(data);
        quint16 cStartTBatt = (qint16)parseU16(data);
        quint16 cEndTBatt = (qint16)parseU16(data);
        quint16 cMaxTBatt = (qint16)parseU16(data);
        quint16 cMinTBatt = (qint16)parseU16(data);
        quint16 cAh  = parseU16(data);
        quint32 cWh  = parseU32(data);
        quint8 cStartSOC  = parseU8(data);
        quint8 cEndSOC  = parseU8(data);
        quint16 cAhI25  = parseU16(data);
        quint32 cTimeI25  = parseU32(data);
        quint16 cAhI15  = parseU16(data);
        quint32 cTimeI15  = parseU32(data);
        quint16 cAhI0  = parseU16(data);
        quint32 cTimeI0  = parseU32(data);
        quint16 cIMax  = parseU16(data);
        quint16 cUMax  = parseU16(data);
        quint32 eTime  = parseU32(data);
        quint16 eAh  = parseU16(data);
        quint16 eWh  = parseU16(data);
        quint16 bmStatusSum  = parseU16(data);
        quint16 bmErrorSum  = parseU16(data);
        quint32 u32Spare1  = parseU32(data);
        quint32 u32Spare2  = parseU32(data);
        quint16 chalgStatusSum  = parseU16(data);
        quint16 u16Spare2  = parseU16(data);
        quint8 u8Spare1  = parseU8(data);
        quint8 u8Spare2  = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge time start: " + QString::number(cStartTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge time end: " + QString::number(cEndTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge VPC start: " + QString::number(cStartVPC);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge VPC end: " + QString::number(cEndVPC);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge battery temperature start: " + QString::number(cStartTBatt);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge battery temperature end: " + QString::number(cEndTBatt);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge battery temperature max: " + QString::number(cMaxTBatt);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge battery temperature min: " + QString::number(cMinTBatt);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge Ah: " + QString::number(cAh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge Wh: " + QString::number(cWh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge state of charge start: " + QString::number(cStartSOC);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge state of charge end: " + QString::number(cEndSOC);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge Ah I25: " + QString::number(cAhI25);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge time I25: " + QString::number(cTimeI25);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge Ah I15: " + QString::number(cAhI15);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge time I15: " + QString::number(cTimeI15);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge Ah I0: " + QString::number(cAhI0);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge time I0: " + QString::number(cTimeI0);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge current max: " + QString::number(cIMax);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge VPC max: " + QString::number(cUMax);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalize time: " + QString::number(eTime);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalize Ah: " + QString::number(eAh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalize Wh: " + QString::number(eWh);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   BM status sum: " + QString::number(bmStatusSum);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   BM error sum: " + QString::number(bmErrorSum);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: " + QString::number(u32Spare1);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 2: " + QString::number(u32Spare2);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Chalg status sum: " + QString::number(chalgStatusSum);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 2: " + QString::number(u16Spare2);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 1: " + QString::number(u8Spare1);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 2: " + QString::number(u8Spare2);

        IMpModel* model = new BmHistoryLogModel(logType,
                                                cycleIndex,
                                                cycleIndexReset,
                                                recordType,
                                                recordSize,
                                                bid,
                                                algNo,
                                                algNoName,
                                                algNoVer,
                                                capacity,
                                                cableRes,
                                                cells,
                                                baseLoad,
                                                dStartTime,
                                                dStartVPC,
                                                dRegTime,
                                                dStartTBatt,
                                                dMaxTBatt,
                                                dMinTBatt,
                                                dAh,
                                                dRegAh,
                                                dWh,
                                                dRegWh,
                                                dStartSOC,
                                                dAhI25,
                                                dTimeI25,
                                                dAhI15,
                                                dTimeI15,
                                                dAhI0,
                                                dTimeI0,
                                                dIMax,
                                                dUMin,
                                                cStartTime,
                                                cEndTime,
                                                cStartVPC,
                                                cEndVPC,
                                                cStartTBatt,
                                                cEndTBatt,
                                                cMaxTBatt,
                                                cMinTBatt,
                                                cAh,
                                                cWh,
                                                cStartSOC,
                                                cEndSOC,
                                                cAhI25,
                                                cTimeI25,
                                                cAhI15,
                                                cTimeI15,
                                                cAhI0,
                                                cTimeI0,
                                                cIMax,
                                                cUMax,
                                                eTime,
                                                eAh,
                                                eWh,
                                                bmStatusSum,
                                                bmErrorSum,
                                                u32Spare1,
                                                u32Spare2,
                                                chalgStatusSum,
                                                u16Spare2,
                                                u8Spare1,
                                                u8Spare2);

        this->reciever.addNodeHistoryLog(channel, panId, address, model);
    }

    this->parseCompleted = true;

    return true;
}

/**
 * @brief BmLogHandler::parseEventLog Handle event log data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param node The node
 * @return True if last segment
 */
bool BmLogHandler::parseEventLog(quint8 channel, quint16 panId, quint16 address, Node& node)
{
    for (int i = node.data.count(); i > 0; i--)
    {
        if (node.expectedSegments == 0)
            qCDebug(QLoggingCategory("mpa")).noquote() << "BM event log";
        else
            qCDebug(QLoggingCategory("mpa")).noquote() << QString("BM event log segment (%1/%2)").arg((node.data.count() - i) + 1).arg(node.expectedSegments);

        QVector<quint8>& data = node.data[i];
        rewindPosition();

        // Header
        quint8 logType = parseU8(data);
        quint32 index = parseU32(data);
        quint32 indexReset = parseU32(data);
        quint32 time = parseU32(data);
        quint16 eventId = parseU16(data);
        quint8 dataSize = parseU8(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "   Log type: 0x" + QString::number(logType, 16).toUpper();
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Index: " + QString::number(index);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Index reset: " + QString::number(indexReset);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Time: " + QString::number(time);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Event id: " + QString::number(eventId);
        qCDebug(QLoggingCategory("mpa")).noquote() << "   Data size: " + QString::number(dataSize);

        QList<QVariant> list;
        if (eventId == 101) // BM error
        {
            list.append(parseU16(data)); // BMError
            list.append(parseU16(data)); // Active

            qCDebug(QLoggingCategory("mpa")).noquote() << "   BM error: " + QString::number((quint16)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Active: " + QString::number((quint16)list.at(1).toUInt());
        }
        else if (eventId == 102) // Startup
        {
            list.append(parseU32(data)); // Firmware type
            list.append(parseU32(data)); // Firmware version

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware type: " + QString::number((quint32)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware version: " + QString::number((quint32)list.at(1).toUInt());
        }
        else if (eventId == 103) // Calibration
        {
            list.append(parseU8(data)); // Type

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Type: " + QString::number((quint8)list.at(0).toUInt());
        }
        else if (eventId == 104) // Parameter changed
        {
            list.append(parseU8(data));     // Algorithm number new
            list.append(parseU8(data));     // Algorithm number old
            list.append(parseU16(data));    // Battery capacity new
            list.append(parseU16(data));    // Battery capacity old
            list.append(parseU8(data));     // Cells new
            list.append(parseU8(data));     // Cells old
            list.append(parseU8(data));     // Cable resistance new
            list.append(parseU8(data));     // Cable resistance old
            list.append(parseU16(data));    // Base load new
            list.append(parseU16(data));    // Base load old

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number new: " + QString::number((quint8)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Algorithm number old: " + QString::number((quint8)list.at(1).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity new: " + QString::number((quint16)list.at(2).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity old: " + QString::number((quint16)list.at(3).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cell new: " + QString::number((quint8)list.at(4).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cell old: " + QString::number((quint8)list.at(5).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance new: " + QString::number((quint8)list.at(6).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance old: " + QString::number((quint8)list.at(7).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load new: " + QString::number((quint16)list.at(8).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load old: " + QString::number((quint16)list.at(9).toUInt());
        }

        IMpModel* model = new BmEventLogModel(logType,
                                              index,
                                              indexReset,
                                              time,
                                              eventId,
                                              dataSize,
                                              list);

        this->reciever.addNodeEventLog(channel, panId, address, model);

        list.clear();
    }

    this->parseCompleted = true;

    return true;
}

/**
 * @brief BmLogHandler::parseInstantLog Handle instant log data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param node The node
 * @return True if last segment
 */
bool BmLogHandler::parseInstantLog(quint8 channel, quint16 panId, quint16 address, Node& node)
{
    quint32 index = 0, time = 0;
    quint16 instLogSamplePeriod = 0, recordType = 0;
    quint8 logType = 0, recordSize = 0, noOfRecords = 0;

    bool headerExpected = true;
    for (int i = node.data.count(); i > 0; i--)
    {
        if (node.expectedSegments == 0)
            qCDebug(QLoggingCategory("mpa")).noquote() << "BM instant log";
        else
            qCDebug(QLoggingCategory("mpa")).noquote() << QString("BM instant log segment (%1/%2)").arg((node.data.count() - i) + 1).arg(node.expectedSegments);

        QVector<quint8>& data = node.data[i];
        rewindPosition();

        if (headerExpected)
        {
            // Header
            logType = parseU8(data);
            index = parseU32(data);
            noOfRecords = parseU8(data);
            time = parseU32(data);
            instLogSamplePeriod = parseU16(data);
            recordType = parseU16(data);
            recordSize = parseU8(data);

            qCDebug(QLoggingCategory("mpa")).noquote() << "   Log type: 0x" + QString::number(logType, 16).toUpper();
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Index: " + QString::number(index);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   No of records: " + QString::number(noOfRecords);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Time: " + QString::number(time);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Sample period: " + QString::number(instLogSamplePeriod);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Record type: " + QString::number(recordType);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Record size: " + QString::number(recordSize);

            headerExpected = false;
        }

        QList<QVariant> list;
        while (this->position + 20 <= data.size())
        {
            // Data
            list.append(parseU8(data));             // Record offset
            list.append((qint16)parseU16(data));    // Average instant log current
            list.append((qint16)parseU16(data));    // Min instant log current
            list.append((qint16)parseU16(data));    // Max instant log current
            list.append((qint16)parseU16(data));    // Average instant log voltage
            list.append((qint16)parseU16(data));    // Min instant log voltage
            list.append((qint16)parseU16(data));    // Max instant log voltage
            list.append((qint8)parseU8(data));      // Average instant log battery temperature
            list.append((qint8)parseU8(data));      // Min instant log battery temperature
            list.append((qint8)parseU8(data));      // Max instant log battery temperature
            list.append((qint16)parseU16(data));    // Average center voltage
            list.append(parseU8(data));             // SOC
            list.append(parseU8(data));             // U8 spare 1

            qCDebug(QLoggingCategory("mpa")).noquote() << "BM instant data " + QString::number((quint8)list.at(0).toUInt() + 1);
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Record offset: " + QString::number((quint8)list.at(0).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Average instant log current: " + QString::number((qint16)list.at(1).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Min instant log current: " + QString::number((qint16)list.at(2).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Max instant log current: " + QString::number((qint16)list.at(3).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Average instant log voltage: " + QString::number((qint16)list.at(4).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Min instant log voltage: " + QString::number((qint16)list.at(5).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Max instant log voltage: " + QString::number((qint16)list.at(6).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Average instant log battery temperature: " + QString::number((qint8)list.at(7).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Min instant log battery temperature: " + QString::number((qint8)list.at(8).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Max instant log battery temperature: " + QString::number((qint8)list.at(9).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   Average center voltage: " + QString::number((qint16)list.at(10).toInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   SOC: " + QString::number((quint8)list.at(11).toUInt());
            qCDebug(QLoggingCategory("mpa")).noquote() << "   U8Spare: " + QString::number((quint8)list.at(12).toUInt());

            IMpModel* model = new BmInstantLogModel(logType,
                                                    index + list.at(0).toUInt(),                        // index + record offset
                                                    noOfRecords,
                                                    time + (list.at(0).toUInt() * instLogSamplePeriod), // time + (record offset * sample period)
                                                    instLogSamplePeriod,
                                                    recordType,
                                                    recordSize,
                                                    list);

            this->reciever.addNodeInstantLog(channel, panId, address, model);

            list.clear();
        }
    }

    this->parseCompleted = true;

    return true;
}

/**
 * @brief BmLogHandler::reset Reset handler
 */
void BmLogHandler:: reset()
{
    this->parseCompleted = false;

    for (Node& node : this->nodes)
        node.data.clear();

    this->nodes.clear();

    qCDebug(QLoggingCategory("mpa")).noquote() << "Clear BM log cache";
}

/**
 * @brief BmLogHandler::dropPackage Drop package
 * @param address The node address
 */
void BmLogHandler::dropPackage(const quint16 address, const QString& message)
{
    Q_UNUSED(address);

    qCWarning(QLoggingCategory("mpa")).noquote() << message;
    reset();
}
