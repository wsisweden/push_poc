#include "BmConfigHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/BmConfigModel.h"

#include <QtCore/QLoggingCategory>

const quint8 BmConfigHandler::ID;

/**
 * @brief BmConfigHandler::BmConfigHandler Construct a new BM configuration handler
 * @param context The context
 * @param reciever The reciever
 */
BmConfigHandler::BmConfigHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &BmConfigHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &BmConfigHandler::reset);
}

/**
 * @brief BmConfigHandler::reset Reset handler
 */
void BmConfigHandler::reset()
{
    this->parseCompleted = false;
}

/**
 * @brief BmConfigHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool BmConfigHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < (86 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    qCDebug(QLoggingCategory("mpa")).noquote() << "BM config";

    panId = parseU16(data);
    channel = parseU8(data) + 11;
    quint16 nwkId = parseU16(data);
    quint8 nwkSettings = parseU8(data);
    quint32 serialNo = parseU32(data);
    quint8 bmType = parseU8(data) + 1;
    quint32 bId = parseU32(data);
    quint8 equFunc = parseU8(data);
    quint8 equParam = parseU8(data);
    quint8 waterFunc = parseU8(data);
    quint8 waterParam = parseU8(data);
    QString bmFgId = parseString(data, 8);
    quint16 cells = parseU16(data);
    quint16 capacity = parseU16(data);
    quint16 cyclesAvailable = parseU16(data);
    quint32 ahAvailable = parseU32(data);
    quint8 batteryType = parseU8(data);
    quint16 baseLoad = parseU16(data);
    quint16 cableRes = parseU16(data);
    quint16 algNo = parseU16(data);
    quint16 u16Spare5 = parseU16(data);
    quint16 bbCgroup = parseU16(data);
    quint16 instPeriod = parseU16(data);
    quint8 cellTap = parseU8(data);
    quint8 voltageBalanceLimit = parseU8(data);
    quint8 acidSensor = parseU8(data);
    qint16 tMax = (qint16)parseU16(data);
    quint8 rOutFunc = parseU8(data);
    quint16 dEl = parseU16(data);
    quint16 cEl = parseU16(data);
    quint8 cEfficiency = parseU8(data);
    quint8 clearStatistics = parseU8(data);
    quint32 ahUsed = parseU32(data);
    quint32 u32Spare2 = parseU32(data);
    quint32 u32Spare3 = parseU32(data);
    quint16 securityCode1 = parseU16(data);
    quint16 securityCode2 = parseU16(data);
    quint16 u16Spare3 = parseU16(data);
    quint16 u16Spare4 = parseU16(data);
    quint8 bitConfig = parseU8(data);
    quint8 socThreshold = parseU8(data);
    quint8 u8Spare3 = parseU8(data);
    quint8 u8Spare4 = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   PanId: 0x" + QString::number(panId, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Channel:" + QString::number(channel);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Network Address: " + QString::number(nwkId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Network Settings: " + QString::number(nwkSettings);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Serial Number: " + QString::number(serialNo);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BM type: 0x" + QString::number(bmType, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BID: " + QString::number(bId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalizing function: " + QString::number(equFunc);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Equalize param: " + QString::number(equParam);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Watering function: " + QString::number(waterFunc);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Watering param: " + QString::number(waterParam);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery tag: " + bmFgId;
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cells:" + QString::number(cells);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity: " + QString::number(capacity);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles available:" + QString::number(cyclesAvailable);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Ah available: " + QString::number(ahAvailable);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Battery type: " + QString::number(batteryType);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load: " + QString::number(baseLoad);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance: " + QString::number(cableRes);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Alg no: " + QString::number(algNo);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 5: 0x" + QString::number(u16Spare5, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BBC group: " + QString::number(bbCgroup);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Instant period: " + QString::number(instPeriod);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cell tap: " + QString::number(cellTap);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Voltage balance limit: " + QString::number(voltageBalanceLimit);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Acid sensor: " + QString::number(acidSensor);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   TMax: " + QString::number(tMax);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   R out function: " + QString::number(rOutFunc);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   dEl: " + QString::number(dEl);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   cEl: " + QString::number(cEl);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   cEfficiency: " + QString::number(cEfficiency);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Clear statistics: " + QString::number(clearStatistics);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Ah Used: " + QString::number(ahUsed);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 2: 0x" + QString::number(u32Spare2, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 3: 0x" + QString::number(u32Spare3, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Security code 1: 0x" + QString::number(securityCode1);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Security code 2: 0x" + QString::number(securityCode2);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: 0x" + QString::number(u16Spare3, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 2: 0x" + QString::number(u16Spare4, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Bit config: " + QString::number(bitConfig);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   SOC threshold: " + QString::number(socThreshold);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 3: 0x" + QString::number(u8Spare3, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U8 spare 4: 0x" + QString::number(u8Spare4, 16).toUpper();

    IMpModel* model = new BmConfigModel(panId,
                                        channel,
                                        nwkId,
                                        nwkSettings,
                                        serialNo,
                                        bmType,
                                        bId,
                                        equFunc,
                                        equParam,
                                        waterFunc,
                                        waterParam,
                                        bmFgId,
                                        cells,
                                        capacity,
                                        cyclesAvailable,
                                        ahAvailable,
                                        batteryType,
                                        baseLoad,
                                        cableRes,
                                        algNo,
                                        u16Spare5,
                                        bbCgroup,
                                        instPeriod,
                                        cellTap,
                                        voltageBalanceLimit,
                                        acidSensor,
                                        tMax,
                                        rOutFunc,
                                        dEl,
                                        cEl,
                                        cEfficiency,
                                        clearStatistics,
                                        ahUsed,
                                        u32Spare2,
                                        u32Spare3,
                                        securityCode1,
                                        securityCode2,
                                        u16Spare3,
                                        u16Spare4,
                                        bitConfig,
                                        socThreshold,
                                        u8Spare3,
                                        u8Spare4);

    this->reciever.addNodeConfig(channel, panId, address, model);

    this->parseCompleted = true;

    return true;
}
