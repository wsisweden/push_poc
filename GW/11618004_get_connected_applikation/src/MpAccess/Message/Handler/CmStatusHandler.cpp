#include "CmStatusHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/CmStatusModel.h"

#include <QtCore/QLoggingCategory>

const quint8 CmStatusHandler::ID;

/**
 * @brief CmStatusHandler::CmStatusHandler Construct a new CM status handler
 * @param context The context
 * @param reciever The reciever
 */
CmStatusHandler::CmStatusHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &CmStatusHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &CmStatusHandler::reset);
}

/**
 * @brief CmStatusHandler::reset Reset handler
 */
void CmStatusHandler::reset()
{
    this->parseCompleted = false;
}

/**
 * @brief CmStatusHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool CmStatusHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < (87 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    qCDebug(QLoggingCategory("mpa")).noquote() << "CM status";

    quint32 cmType = parseU32(data);
    quint64 cmId = parseU64(data);
    quint32 time = parseU32(data);
    quint32 chargeTimeTotal = parseU32(data);
    quint32 chargedAhTotal = parseU32(data);
    quint32 pACTotal = parseU32(data);
    quint16 cycklesSum2to25 = parseU16(data);
    quint16 cycklesSum26to50 = parseU16(data);
    quint16 cycklesSum51to80 = parseU16(data);
    quint16 cycklesSum81to90 = parseU16(data);
    quint16 cycklesSumAbove90 = parseU16(data);
    quint8 chargingMode = parseU8(data);
    quint32 bId = parseU32(data);
    QString algNoName = parseString(data, 8);
    quint16 algNo = parseU16(data);
    quint16 version = parseU16(data);
    quint16 capacity = parseU16(data);
    quint16 cableRes = parseU16(data);
    quint16 cell = parseU16(data);
    quint16 baseLoad = parseU16(data);
    quint16 chalgError = parseU16(data);
    quint16 reguError = parseU16(data);
    quint16 chalgStatus = parseU16(data);
    quint16 reguStatus = parseU16(data);
    qint16 tBoard = (qint16)parseU16(data);
    qint16 tHs = (qint16)parseU16(data);
    quint16 acOVP = parseU16(data);
    quint32 u32Spare1 = parseU32(data);
    quint32 u32Spare2 = parseU32(data);
    quint16 u16Spare1 = parseU16(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Charger type: " + QString::number(cmType);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   CMID 0x: " + QString::number(cmId, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Time: " + QString::number(time);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Charge time left: " + QString::number(chargeTimeTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Charged Ah total: " + QString::number(chargedAhTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   PAC total: " + QString::number(pACTotal);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 2 to 25:" + QString::number(cycklesSum2to25);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 26 to 50: " + QString::number(cycklesSum26to50);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 51 to 80: " + QString::number(cycklesSum51to80);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles 81 to 90: " + QString::number(cycklesSum81to90);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cyckles above 90: " + QString::number(cycklesSumAbove90);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Charging mode: " + QString::number(chargingMode);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   BID: " + QString::number(bId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Alg no name: " + algNoName;
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Alg no: " + QString::number(algNo);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Version: " + QString::number(version);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Capacity: " + QString::number(capacity);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cable resistance: " + QString::number(cableRes);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Cells: " + QString::number(cell);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Base load: " + QString::number(baseLoad);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Chalg error: " + QString::number(chalgError);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Regu error: " + QString::number(reguError);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Chalg staus: " + QString::number(chalgStatus);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Regu staus: " + QString::number(reguStatus);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   TBoard: " + QString::number(tBoard);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   THs: " + QString::number(tHs);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   AC OVP: " + QString::number(acOVP);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 1: 0x" + QString::number(u32Spare1, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare 2: 0x" + QString::number(u32Spare2, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare 1: 0x" + QString::number(u16Spare1, 16).toUpper();

    IMpModel* model = new CmStatusModel(cmType,
                                        cmId,
                                        time,
                                        chargeTimeTotal,
                                        chargedAhTotal,
                                        pACTotal,
                                        cycklesSum2to25,
                                        cycklesSum26to50,
                                        cycklesSum51to80,
                                        cycklesSum81to90,
                                        cycklesSumAbove90,
                                        chargingMode,
                                        bId,
                                        algNoName,
                                        algNo,
                                        version,
                                        capacity,
                                        cableRes,
                                        cell,
                                        baseLoad,
                                        chalgError,
                                        reguError,
                                        chalgStatus,
                                        reguStatus,
                                        tBoard,
                                        tHs,
                                        acOVP,
                                        u32Spare1,
                                        u32Spare2,
                                        u16Spare1);

    this->reciever.addNodeStatus(channel, panId, address, model);

    this->parseCompleted = true;

    return true;
}
