#pragma once

#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../../Common/Context.h"

#include <QtCore/QVector>
#include <QtCore/QObject>
#include <QtCore/QByteArray>

class MPACCESS_EXPORT MessageHandler : public QObject
{
    Q_OBJECT

    public:
        explicit MessageHandler(Context* context, IReciever& reciever, QObject* parent = nullptr);

        virtual bool handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data) = 0;

        void rewindPosition();
        void parseHeader(QVector<quint8>& data);

        bool isLastSegment() { return this->segment == 0x0; }

    protected:
        quint8 segment;
        quint8 sequence;
        quint16 position;
        IReciever& reciever;

        bool firstSegment = false;
        bool lastSegment = false;
        bool parseCompleted = false;

        Context* context = nullptr;
        const int MESSAGE_HEADER = 2;

        quint8 parseU8(QVector<quint8>& data);
        quint16 parseU16(QVector<quint8>& data);
        quint32 parseU32(QVector<quint8>& data);
        quint64 parseU64(QVector<quint8>& data);
        QString parseString(QVector<quint8>& data, quint8 length);
        QByteArray parseByteArray(QVector<quint8>& data);
        float parseF32(QVector<quint8>& data);
};
