#include "CmMeasHandler.h"
#include "../Model/IMpModel.h"
#include "../Model/CmMeasModel.h"

#include <QtCore/QLoggingCategory>

const quint8 CmMeasHandler::ID;

/**
 * @brief MpCmMeasHandler::MpCmMeasHandler Construct a new CM meassure handler
 * @param context The context
 * @param reciever The reciever
 */
CmMeasHandler::CmMeasHandler(Context* context, IReciever& reciever, QObject* parent)
    : MessageHandler(context, reciever, parent)
{
    connect(this->context->getEventManager(), &EventManager::commandTransmit, this, &CmMeasHandler::reset);
    connect(this->context->getEventManager(), &EventManager::commandComplete, this, &CmMeasHandler::reset);
}


/**
 * @brief CmMeasHandler::reset Reset handler
 */
void CmMeasHandler::reset()
{
    this->parseCompleted = false;
}

/**
 * @brief MpCmMeasHandler::handle Handle data
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param data The response data
 * @return True if last segment
 */
bool CmMeasHandler::handle(quint8 channel, quint16 panId, quint16 address, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < (23 + MESSAGE_HEADER))
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return true;
    }

    parseHeader(data);

    if (this->parseCompleted)
        return true;

    qCDebug(QLoggingCategory("mpa")).noquote() << "CM meas";

    quint32 pac = parseU32(data);
    quint32 uChalg = parseU32(data);
    quint32 iChalg = parseU32(data);
    qint16 tBoard = (qint16)parseU16(data);
    qint16 ths = (qint16)parseU16(data);
    quint32 u32Spare = parseU32(data);
    quint16 u16Spare = parseU16(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   PAC: " + QString::number(pac);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   UChalg: " + QString::number(uChalg);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   IChalg: " + QString::number(iChalg);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   TBoard: " + QString::number(tBoard);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Ths: " + QString::number(ths);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U32 spare: 0x" + QString::number(u32Spare, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   U16 spare: 0x" + QString::number(u16Spare, 16).toUpper();

    IMpModel* model = new CmMeasModel(pac,
                                      uChalg,
                                      iChalg,
                                      tBoard,
                                      ths,
                                      u32Spare,
                                      u16Spare);


    this->reciever.addNodeMeas(channel, panId, address, model);

    this->parseCompleted = true;

    return true;
}
