#pragma once

#include "../../Shared.h"
#include "MessageRequest.h"
#include "../../Command/Handler/DataIndicationHandler.h"

#include <QtCore/QVariant>

class MPACCESS_EXPORT SetParamRequest : public MessageRequest
{
    public:
        explicit SetParamRequest(const quint16 address, const quint32 index, const QVariant& data);

        bool isResponse(DataIndicationHandler* handler) override;
};
