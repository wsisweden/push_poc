#include "GetStatusRequest.h"

/**
 * @brief GetStatusRequest::GetStatusRequest Construct a new get status request
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 */
GetStatusRequest::GetStatusRequest(const quint8 channel, const quint16 panId, const quint16 address)
    : MessageRequest(channel, panId, address, 0x60)
{
    pack();
}

/**
 * @brief GetStatusRequest::isResponse Check message for valid response
 * @param handler The handler
 */
bool GetStatusRequest::isResponse(DataIndicationHandler* handler)
{
    return (handler->getId() == 0x30 || handler->getId() == 0x50);
}
