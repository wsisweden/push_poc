#pragma once

#include "../../Shared.h"
#include "MessageRequest.h"
#include "../../Command/Handler/DataIndicationHandler.h"

class MPACCESS_EXPORT GetStatusRequest : public MessageRequest
{
    public:
        explicit GetStatusRequest(const quint8 channel, const quint16 panId, const quint16 address);

        bool isResponse(DataIndicationHandler* handler) override;
};
