#pragma once

#include "../../Shared.h"
#include "../../Command/Request/NwkDataRequest.h"
#include "../../Command/Handler/DataIndicationHandler.h"

#include <QtCore/QVector>
#include <QtCore/QString>
#include <QtCore/QVariant>

class MPACCESS_EXPORT MessageRequest : public NwkDataRequest
{
    public:
        explicit MessageRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint8 eventId);
        explicit MessageRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint8 eventId, const quint16 timeout);
        explicit MessageRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId, const quint8 productType);
        virtual ~MessageRequest() {}

        quint8 getEventId() override;

        virtual bool isResponse(DataIndicationHandler* handler) = 0;

        quint16 getAddress() const { return this->address; }
        quint32 getNodeId() const { return this->nodeId; }
        quint8 getProductType() const { return this->productType; }

    protected:
        void pack();

        void addData(const bool value);
        void addData(const quint8 value);
        void addData(const quint16 value);
        void addData(const quint32 value);
        void addData(const quint64 value);
        void addData(const QString& data);
        void addData(const QVariant& data);
        void addData(const QVector<quint8>& data);

        QVector<quint8> data;

        static quint8 sequenceNumber;

    private:
        quint8 eventId;
        quint16 address;
        quint32 nodeId;
        quint8 productType;
};
