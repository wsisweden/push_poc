#pragma once

#include "../../Shared.h"
#include "MessageRequest.h"
#include "../../Command/Handler/DataIndicationHandler.h"

class MPACCESS_EXPORT GetLogRequest : public MessageRequest
{
    public:
        explicit GetLogRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint32 index, const quint8 number, const quint8 type);

        bool isResponse(DataIndicationHandler* handler) override;
};
