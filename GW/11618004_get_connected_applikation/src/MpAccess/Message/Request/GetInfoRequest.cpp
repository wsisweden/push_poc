#include "GetInfoRequest.h"

/**
 * @brief GetInfoRequest::GetInfoRequest Construct a new get information request
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 */
GetInfoRequest::GetInfoRequest(const quint8 channel, const quint16 panId, const quint16 address)
    : MessageRequest(channel, panId, address, 0x68)
{
    pack();
}

/**
 * @brief GetInfoRequest::isResponse Check message for valid response
 * @param handler The handler
 */
bool GetInfoRequest::isResponse(DataIndicationHandler* handler)
{
    return (handler->getId() == 0x36 || handler->getId() == 0x54);
}
