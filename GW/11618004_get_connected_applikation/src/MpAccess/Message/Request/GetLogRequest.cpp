#include "GetLogRequest.h"

/**
 * @brief GetLogRequest::GetLogRequest Construct a new get log request
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 * @param index The index
 * @param number The number
 * @param type The type (0 = History, 1 = Event, 2 = Instant)
 */
GetLogRequest::GetLogRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint32 index, const quint8 number, const quint8 type)
    : MessageRequest(channel, panId, address, 0x63)
{
    addData(type);
    addData(index);
    addData(number);
    pack();
}

/**
 * @brief GetLogRequest::isResponse Check message for valid response
 * @param handler The handler
 */
bool GetLogRequest::isResponse(DataIndicationHandler* handler)
{
    return (handler->getId() == 0x33 || handler->getId() == 0x53);
}
