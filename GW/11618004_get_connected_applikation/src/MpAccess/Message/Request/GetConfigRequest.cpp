#include "GetConfigRequest.h"

/**
 * @brief GetConfigRequest::GetConfigRequest Construct a new get configuration request
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 */
GetConfigRequest::GetConfigRequest(const quint8 channel, const quint16 panId, const quint16 address)
    : MessageRequest(channel, panId, address, 0x62)
{
    pack();
}

/**
 * @brief GetConfigRequest::isResponse Check message for valid response
 * @param handler The handler
 */
bool GetConfigRequest::isResponse(DataIndicationHandler* handler)
{
    return (handler->getId() == 0x32 || handler->getId() == 0x52);
}

