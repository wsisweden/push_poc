#pragma once

#include "../../Shared.h"
#include "MessageRequest.h"
#include "../../Command/Handler/DataIndicationHandler.h"

class MPACCESS_EXPORT SyncNodeRequest : public MessageRequest
{
    public:
        explicit SyncNodeRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId, const quint8 productType);

        bool isResponse(DataIndicationHandler* handler) override { Q_UNUSED(handler); return false; }
};
