#include "SetParamRequest.h"

/**
 * @brief SetParamRequest::SetParamRequest Construct a new set param request
 * @param address The node address
 * @param index The index
 * @param data The data
 */
SetParamRequest::SetParamRequest(const quint16 address, const quint32 index, const QVariant& data)
    : MessageRequest(0, 0, address, 0x80)
{
    addData(index);
    addData(data);
    pack();
}

/**
 * @brief SetParamRequest::isResponse Check message for valid response
 * @param handler The handler
 */
bool SetParamRequest::isResponse(DataIndicationHandler* handler)
{
    return (handler->getId() == 0x10);
}
