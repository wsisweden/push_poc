#include "SyncNodeRequest.h"

/**
 * @brief SyncNodeRequest::SyncNodeRequest Construct a new sync node request
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 * @param nodeId The node id
 * @param productType The product type
 */
SyncNodeRequest::SyncNodeRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId, const quint8 productType)
    : MessageRequest(channel, panId, address, nodeId, productType)
{
}
