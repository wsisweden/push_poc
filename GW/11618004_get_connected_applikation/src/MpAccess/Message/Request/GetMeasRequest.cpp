#include "GetMeasRequest.h"

/**
 * @brief GetMeasRequest::GetMeasRequest Construct a new get meassure request
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 */
GetMeasRequest::GetMeasRequest(const quint8 channel, const quint16 panId, const quint16 address)
    : MessageRequest(channel, panId, address, 0x61)
{
    pack();
}

/**
 * @brief GetMeasRequest::isResponse Check message for valid response
 * @param handler The handler
 */
bool GetMeasRequest::isResponse(DataIndicationHandler* handler)
{
    return (handler->getId() == 0x31 || handler->getId() == 0x51);
}
