#include "MessageRequest.h"

quint8 MessageRequest::sequenceNumber = 0;

/**
 * @brief MessageRequest::MessageRequest Construct a new message
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 * @param eventId The event ID
 */
MessageRequest::MessageRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId, const quint8 productType)
{
    this->channel = channel;
    this->panId = panId;

    this->address = address;
    this->nodeId = nodeId;
    this->productType = productType;
}

/**
 * @brief MessageRequest::MessageRequest Construct a new message
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 * @param eventId The event ID
 */
MessageRequest::MessageRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint8 eventId)
{
    this->channel = channel;
    this->panId = panId;

    this->address = address;
    this->eventId = eventId;

    NwkDataRequest::addData(address);

    addData(eventId);
    addData(this->sequenceNumber++);
    addData((quint8)0); // Not segmented message.
}

/**
 * @brief MessageRequest::MessageRequest Construct a new message
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 * @param eventId The event ID
 * @param timeout The message timeout
 */
MessageRequest::MessageRequest(const quint8 channel, const quint16 panId, const quint16 address, const quint8 eventId, const quint16 timeout)
    : NwkDataRequest(timeout)
{
    this->channel = channel;
    this->panId = panId;

    this->address = address;
    this->eventId = eventId;

    NwkDataRequest::addData(address);

    addData(eventId);
    addData(this->sequenceNumber++);
    addData((quint8)0); // Not segmented message.
}

/**
 * @brief MessageRequest::getEventId Get the event id
 * @return The event id
 */
quint8 MessageRequest::getEventId()
{
    return this->eventId;
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param value The value
 */
void MessageRequest::addData(const bool value)
{
    if (value)
        this->data.push_back(1);
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param value The data
 */
void MessageRequest::addData(const quint8 value)
{
    this->data.push_back(value);
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param value The data
 */
void MessageRequest::addData(const quint16 value)
{
    this->data.push_back((quint8)(value >> 8));
    this->data.push_back((quint8)(value & 0xff));
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param value The data
 */
void MessageRequest::addData(const quint32 value)
{
    this->data.push_back((quint8)(value >> 24));
    this->data.push_back((quint8)(value >> 16));
    this->data.push_back((quint8)(value >> 8));
    this->data.push_back((quint8)(value & 0xff));
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param value The data
 */
void MessageRequest::addData(const quint64 value)
{
    this->data.push_back((quint8)(value >> 56));
    this->data.push_back((quint8)(value >> 48));
    this->data.push_back((quint8)(value >> 40));
    this->data.push_back((quint8)(value >> 32));
    this->data.push_back((quint8)(value >> 24));
    this->data.push_back((quint8)(value >> 16));
    this->data.push_back((quint8)(value >> 8));
    this->data.push_back((quint8)(value & 0xff));
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param data The data
 */
void MessageRequest::addData(const QString& data)
{
    for (quint8 value : data.toUtf8())
        this->data.append(value);
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param data The data
 */
void MessageRequest::addData(const QVariant& data)
{
    if (data.userType() == QMetaType::Bool)
        addData((quint8)data.toBool());
    else if (data.userType() == QMetaType::Char)
        addData((quint8)data.toInt());
    else if (data.userType() == QMetaType::Short)
        addData((quint16)data.toInt());
    else if (data.userType() == QMetaType::Int)
        addData((quint32)data.toInt());
    else if (data.userType() == QMetaType::UShort)
        addData((quint16)data.toUInt());
    else if (data.userType() == QMetaType::UInt)
        addData((quint32)data.toUInt());
    else if (data.userType() == QMetaType::LongLong)
        addData((quint64)data.toLongLong());
    else if (data.userType() == QMetaType::ULongLong)
        addData((quint64)data.toULongLong());
    else if (data.userType() == QMetaType::QString)
        addData(data.toString() + '\0');
}

/**
 * @brief MessageRequest::addData Add data to message
 * @param data The data
 */
void MessageRequest::addData(const QVector<quint8>& data)
{
    this->data.append(data);
}

/**
 * @brief MessageRequest::pack Pack the message
 */
void MessageRequest::pack()
{
    NwkDataRequest::addData((quint8)this->data.size());
    NwkDataRequest::addData(this->data);
}
