#pragma once

#include "CommandHandler.h"
#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../../Common/Context.h"

#include <QtCore/QVector>
#include <QtCore/QObject>

class MPACCESS_EXPORT NwkDataHandler : public CommandHandler
{
    Q_OBJECT

    public:
        explicit NwkDataHandler(Context* context, IReciever& reciever, QObject* parent = nullptr);

        bool handle(quint8 channel, quint16 panId, QVector<quint8>& data);

        quint8 getStatus() { return this->status; }

        static const quint8 ID = 0x87;
        static const quint8 STATUS_OK = 0;
        static const quint8 STATUS_NO_ROUTE = 0x22;

    private:
        quint8 status;
};
