#include "GetMaxPayloadHandler.h"

#include <QtCore/QLoggingCategory>

const quint8 GetMaxPayloadHandler::ID;

/**
 * @brief GetMaxPayloadHandler::GetMaxPayloadHandler Construct a new get max payload handler
 * @param context The context
 * @param reciever The reciever
 */
GetMaxPayloadHandler::GetMaxPayloadHandler(Context* context, IReciever& reciever, QObject* parent)
    : CommandHandler(context, reciever, parent) {}

/**
 * @brief GetMaxPayloadHandler::handle Handle data
 * @param data The response data
 * @return True if last segment
 */
bool GetMaxPayloadHandler::handle(quint8 channel, quint16 panId, QVector<quint8>& data)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);

    // Check data length.
    if (data.size() < 1)
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return false;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "Get Max payload";

    quint8 length = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Length: " + QString::number(length);

    //this->reciever->setMaxPayload(length);

    return true;
}
