#include "DataIndicationHandler.h"
#include "../Request/CommandRequest.h"

#include <QtCore/QLoggingCategory>

const quint8 DataIndicationHandler::ID;

/**
 * @brief DataIndicationHandler::DataIndicationHandler Construct a new data indication handler
 * @param context The context
 * @param reciever The reciever
 * @param factory The message handler factory
 */
DataIndicationHandler::DataIndicationHandler(Context* context, IReciever& reciever, MessageHandlerFactory& messageFactory, QObject* parent)
    : CommandHandler(context, reciever, parent)
    , messageFactory(messageFactory) {}

/**
 * @brief DataIndicationHandler::handle Handle data
 * @param data The response data
 * @return True if last segment
 */
bool DataIndicationHandler::handle(quint8 channel, quint16 panId, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < 3)
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return false;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "Data indication";

    quint16 address = parseU16(data);
    quint8 length = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Address: " + QString::number(address);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Length: " + QString::number(length);

    // Check data length again.
    if (data.size() < (3 + (quint8)length) || length == 0)
        return true;

    this->id = parseU8(data);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Command: 0x" + QString::number(this->id, 16).toUpper();

    MessageHandler* handler = messageFactory.getHandler(this->id);
    if (handler != nullptr)
    {
        QVector<quint8> buffer;
        for (int i = this->position; i < data.size(); i++)
            buffer.push_back(data[i]);

        return handler->handle(channel, panId, address, buffer);
    }

    return true;
}
