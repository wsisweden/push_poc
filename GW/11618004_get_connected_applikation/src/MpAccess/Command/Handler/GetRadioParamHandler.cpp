#include "GetRadioParamHandler.h"

#include <QtCore/QLoggingCategory>

const quint8 GetRadioParamHandler::ID;

/**
 * @brief GetRadioParamHandler::GetRadioParamHandler Construct a new get radio param handler
 * @param context The context
 * @param reciever The reciever
 */
GetRadioParamHandler::GetRadioParamHandler(Context* context, IReciever& reciever, QObject* parent)
    : CommandHandler(context, reciever, parent) {}

/**
 * @brief GetRadioParamHandler::handle Handle data
 * @param data The response data
 * @return True if last segment
 */
bool GetRadioParamHandler::handle(quint8 channel, quint16 panId, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < 31)
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return false;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "Get radio param";

    quint32 fwType = parseU32(data);
    quint32 fwVer = parseU32(data);
    quint32 chkSum = parseU32(data);
    quint64 cmId = parseU64(data);
    quint16 mfgId = parseU16(data);
    quint16 appId = parseU16(data);
    quint8 channel2 = parseU8(data);
    quint16 panId2 = parseU16(data);
    quint16 address = parseU16(data);
    quint16 addrPool = parseU16(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware type: " + QString::number(fwType);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Firmware version: " + QString::number(fwVer);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Checksum: 0x" + QString::number(chkSum, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   CM Id: 0x" + QString::number(cmId, 16).toUpper();
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Mfg Id: " + QString::number(mfgId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   App Id: " + QString::number(appId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Channel: " + QString::number(channel2);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Pan Id: " + QString::number(panId2);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Address: " + QString::number(address);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Address pool: " + QString::number(addrPool);

    this->reciever.setRadioAddress(channel, panId, address, fwType, fwVer);

    return true;
}
