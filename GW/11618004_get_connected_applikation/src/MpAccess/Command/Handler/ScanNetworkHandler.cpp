#include "ScanNetworkHandler.h"

#include <QtCore/QLoggingCategory>

const quint8 ScanNetworkHandler::ID;

/**
 * @brief ScanNetworkHandler::ScanNetworkHandler Construct a new scan network handler
 * @param context The context
 * @param reciever The reciever
 */
ScanNetworkHandler::ScanNetworkHandler(Context* context, IReciever& reciever, QObject* parent)
    : CommandHandler(context, reciever, parent) {}

/**
 * @brief ScanNetworkHandler::handle Handle data
 * @param data The response data
 * @return True if last segment
 */
bool ScanNetworkHandler::handle(quint8 channel, quint16 panId, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < 5)
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return false;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "Scan network";

    channel = parseU8(data);
    panId = parseU16(data);
    quint16 address = parseU16(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Channel: " + QString::number(channel);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   PanId: " + QString::number(panId);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   Address: " + QString::number(address);

    this->reciever.addNetwork(channel, panId);

    return true;
}
