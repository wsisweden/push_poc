#pragma once

#include "CommandHandler.h"
#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../../Common/Context.h"

#include <QtCore/QVector>
#include <QtCore/QObject>

class MPACCESS_EXPORT SetRadioParamHandler : public CommandHandler
{
    Q_OBJECT

    public:
        explicit SetRadioParamHandler(Context* context, IReciever& reciever, QObject* parent = nullptr);

        bool handle(quint8 channel, quint16 panId, QVector<quint8>& data);

        static const quint8 ID = 0x84;
};
