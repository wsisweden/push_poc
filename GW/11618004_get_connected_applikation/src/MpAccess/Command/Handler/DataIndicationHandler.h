#pragma once

#include "CommandHandler.h"
#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../Message/Handler/MessageHandlerFactory.h"
#include "../../../Common/Context.h"

#include <QtCore/QVector>
#include <QtCore/QObject>

class MPACCESS_EXPORT DataIndicationHandler : public CommandHandler
{
    Q_OBJECT

    public:
        explicit DataIndicationHandler(Context* context, IReciever& reciever, MessageHandlerFactory& messageFactory, QObject* parent = nullptr);

        bool handle(quint8 channel, quint16 panId, QVector<quint8>& data);

        quint8 getId() { return this->id; }

        static const quint8 ID = 0x88;

    private:
        quint8 id;
        MessageHandlerFactory& messageFactory;
};
