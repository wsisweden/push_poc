#pragma once

#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../../Common/Context.h"

#include <QtCore/QVector>
#include <QtCore/QObject>
#include <QtCore/QByteArray>

class MPACCESS_EXPORT CommandHandler : public QObject
{
    Q_OBJECT

    public:
        explicit CommandHandler(Context* context, IReciever& reciever, QObject* parent = nullptr);

        virtual bool handle(quint8 channel, quint16 panId, QVector<quint8>& data) = 0;

        void rewindPosition();

    protected:
        quint16 position;
        IReciever& reciever;

        Context* context = nullptr;

        quint8 parseU8(QVector<quint8>& data);
        quint16 parseU16(QVector<quint8>& data);
        quint32 parseU32(QVector<quint8>& data);
        quint64 parseU64(QVector<quint8>& data);
        QString parseString(QVector<quint8>& data, quint8 length);
        QByteArray parseByteArray(QVector<quint8>& data);
        float parseF32(QVector<quint8>& data);
};
