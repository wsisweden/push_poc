#include "CommandHandlerFactory.h"
#include "NwkDataHandler.h"
#include "ScanNetworkHandler.h"
#include "ScanForNodesHandler.h"
#include "GetRadioParamHandler.h"
#include "SetRadioParamHandler.h"
#include "DataIndicationHandler.h"
#include "GetMaxPayloadHandler.h"

/**
 * @brief CommandHandlerFactory::CommandHandlerFactory Construct a new command handler factory
 * @param context The context
 * @param reciever The reciever
 */
CommandHandlerFactory::CommandHandlerFactory(Context* context, IReciever& reciever, QObject* parent)
    : QObject(parent)
    , messageFactory(context, reciever, this)
{
    this->handlers[NwkDataHandler::ID] = new NwkDataHandler(context, reciever);
    this->handlers[DataIndicationHandler::ID] = new DataIndicationHandler(context, reciever, messageFactory);
    this->handlers[ScanNetworkHandler::ID] = new ScanNetworkHandler(context, reciever);
    this->handlers[ScanForNodesHandler::ID] = new ScanForNodesHandler(context, reciever);
    this->handlers[GetRadioParamHandler::ID] = new GetRadioParamHandler(context, reciever);
    this->handlers[SetRadioParamHandler::ID] = new SetRadioParamHandler(context, reciever);
    this->handlers[GetMaxPayloadHandler::ID] = new GetMaxPayloadHandler(context, reciever);
}

/**
 * @brief CommandHandlerFactory::getHandler Get the command to handle the event
 * @param eventId The ID of the event
 * @return The command
 */
CommandHandler* CommandHandlerFactory::getHandler(const quint8 eventId)
{
    if (this->handlers.contains(eventId))
    {
        CommandHandler* handler = this->handlers.value(eventId);
        handler->rewindPosition();

        return handler;
    }

    return nullptr;
}
