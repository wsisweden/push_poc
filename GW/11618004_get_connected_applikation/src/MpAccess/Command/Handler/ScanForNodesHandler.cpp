#include "ScanForNodesHandler.h"

#include <QtCore/QLoggingCategory>

const quint8 ScanForNodesHandler::ID;

/**
 * @brief ScanForNodesHandler::ScanForNodesHandler Construct a new scan for nodes handler
 * @param context The context
 * @param reciever The reciever
 */
ScanForNodesHandler::ScanForNodesHandler(Context* context, IReciever& reciever, QObject* parent)
    : CommandHandler(context, reciever, parent) {}

/**
 * @brief ScanForNodesHandler::handle Handle data
 * @param data The response data
 * @return True if last segment
 */
bool ScanForNodesHandler::handle(quint8 channel, quint16 panId, QVector<quint8>& data)
{
    // Check data length.
    if (data.size() < 3)
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return false;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "Scan for nodes";

    channel = parseU8(data);
    panId = parseU16(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Channel: " + QString::number(channel);
    qCDebug(QLoggingCategory("mpa")).noquote() << "   PanId: " + QString::number(panId);

    while (data.size() >= (this->position + 7))
    {
        quint16 address = parseU16(data);
        quint8 type = parseU8(data);
        quint32 id = parseU32(data);

        qCDebug(QLoggingCategory("mpa")).noquote() << "      Address: " + QString::number(address);
        qCDebug(QLoggingCategory("mpa")).noquote() << "      Type: " + QString::number(type);
        qCDebug(QLoggingCategory("mpa")).noquote() << "      Id: " + QString::number(id);

        this->reciever.addNode(id, channel, panId, address, type);
    }

    return true;
}
