#include "CommandHandler.h"

/**
 * @brief CommandHandler::MpCommandResponse Construct a new command handler
 * @param context The context
 * @param reciever The reciever
 */
CommandHandler::CommandHandler(Context* context, IReciever& reciever, QObject* parent)
    : QObject(parent)
    , reciever(reciever)
{
    this->context = context;
}

/**
 * @brief CommandHandler::rewindPosition Rewind position
 */
void CommandHandler::rewindPosition()
{
    this->position = 0;
}

/**
 * @brief CommandHandler::parseU8 Parse data
 * @param data The data
 * @return Parsed data
 */
quint8 CommandHandler::parseU8(QVector<quint8>& data)
{
    quint8 temp = data[this->position];
    this->position += 1;

    return temp;
}

/**
 * @brief CommandHandler::parseU16 Parse data
 * @param data The data
 * @return Parsed data
 */
quint16 CommandHandler::parseU16(QVector<quint8>& data)
{
    quint16 temp = ((quint16)data[this->position] << 8) + data[this->position + 1];
    this->position += 2;

    return temp;
}

/**
 * @brief CommandHandler::parseU32 Parse data
 * @param data The data
 * @return Parsed data
 */
quint32 CommandHandler::parseU32(QVector<quint8>& data)
{
    quint32 temp = ((quint32)data[this->position] << 24) +
                   ((quint32)data[this->position + 1] << 16) +
                   ((quint32)data[this->position + 2] << 8) + data[this->position + 3];
    this->position += 4;

    return temp;
}

/**
 * @brief MessageHandler::parseF32 Parse data
 * @param data The data
 * @return Parsed data
 */
float CommandHandler::parseF32(QVector<quint8>& data)
{
    quint32 temp = ((quint32)data[this->position] << 24) +
                   ((quint32)data[this->position + 1] << 16) +
                   ((quint32)data[this->position + 2] << 8) + data[this->position + 3];
    this->position += 4;

    return  *reinterpret_cast<float*>(&temp);
}

/**
 * @brief CommandHandler::parseU64 Parse data
 * @param data The data
 * @return Parsed data
 */
quint64 CommandHandler::parseU64(QVector<quint8>& data)
{
    quint64 temp = ((quint64)data[this->position] << 56) +
                   ((quint64)data[this->position + 1] << 48) +
                   ((quint64)data[this->position + 2] << 40) +
                   ((quint64)data[this->position + 3] << 32) +
                   ((quint64)data[this->position + 4] << 24) +
                   ((quint64)data[this->position + 5] << 16) +
                   ((quint64)data[this->position + 6] << 8) + data[this->position + 7];
    this->position += 8;

    return temp;
}

/**
 * @brief CommandHandler::parseString Parse string data
 * @param data The data
 * @param length The number of bytes to parse
 * @return The string
 */
QString CommandHandler::parseString(QVector<quint8>& data, quint8 length)
{
    QString temp;
    for (int i = 0; i < length; i++)
    {
        QChar c = parseU8(data);
        if (c == '\0')
            break;

        temp.append(c);
    }

    return temp;
}

/**
 * @brief CommandHandler::parseByteArray Parse data
 * @param data The data
 * @return Parsed data
 */
QByteArray CommandHandler::parseByteArray(QVector<quint8>& data)
{
    return QByteArray(reinterpret_cast<const char*>(data.data() + this->position), data.size() - this->position);
}
