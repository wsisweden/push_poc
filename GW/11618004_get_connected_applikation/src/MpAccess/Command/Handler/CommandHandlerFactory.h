#pragma once

#include "CommandHandler.h"
#include "../../Shared.h"
#include "../../IReciever.h"
#include "../../Message/Handler/MessageHandlerFactory.h"
#include "../../../Common/Context.h"

#include <QtCore/QMap>
#include <QtCore/QObject>

class MPACCESS_EXPORT CommandHandlerFactory : public QObject
{
    Q_OBJECT

    public:
        explicit CommandHandlerFactory(Context* context, IReciever& reciever, QObject* parent = nullptr);

        CommandHandler* getHandler(const quint8 eventId);

    private:
        MessageHandlerFactory messageFactory;
        QMap<quint8, CommandHandler*> handlers;
};
