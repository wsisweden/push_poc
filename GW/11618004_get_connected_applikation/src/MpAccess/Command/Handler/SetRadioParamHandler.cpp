#include "SetRadioParamHandler.h"

#include <QtCore/QLoggingCategory>

const quint8 SetRadioParamHandler::ID;

/**
 * @brief SetRadioParamHandler::SetRadioParamHandler Construct a new set radio param handler
 * @param context The context
 * @param reciever The reciever
 */
SetRadioParamHandler::SetRadioParamHandler(Context* context, IReciever& reciever, QObject* parent)
    : CommandHandler(context, reciever, parent) {}

/**
 * @brief SetRadioParamHandler::handle Handle data
 * @param data The response data
 * @return True if last segment
 */
bool SetRadioParamHandler::handle(quint8 channel, quint16 panId, QVector<quint8>& data)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);

    // Check data length.
    if (data.size() < 1)
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return false;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "Set radio param";

    quint8 status = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Status: " + QString::number(status);

    return true;
}
