#include "NwkDataHandler.h"

#include <QtCore/QLoggingCategory>

const quint8 NwkDataHandler::ID;

/**
 * @brief NwkDataResp::NwkDataResp Construct a new nwk data response
 * @param context The context
 * @param reciever The reciever
 */
NwkDataHandler::NwkDataHandler(Context* context, IReciever& reciever, QObject* parent)
    : CommandHandler(context, reciever, parent) {}

/**
 * @brief NwkDataResp::handle Handle data
 * @param data The response data
 * @return True if last segment
 */
bool NwkDataHandler::handle(quint8 channel, quint16 panId, QVector<quint8>& data)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);

    // Check data length.
    if (data.size() < 1)
    {
        qCWarning(QLoggingCategory("mpa")).noquote() << "Invalid data";
        return false;
    }

    qCDebug(QLoggingCategory("mpa")).noquote() << "Data response";

    this->status = parseU8(data);

    qCDebug(QLoggingCategory("mpa")).noquote() << "   Status: " + QString::number(this->status);

    return true;
}
