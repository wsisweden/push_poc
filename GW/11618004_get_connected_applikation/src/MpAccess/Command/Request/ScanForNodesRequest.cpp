#include "ScanForNodesRequest.h"
#include "../Handler/ScanForNodesHandler.h"

/**
 * @brief ScanForNodesRequest::ScanForNodesRequest Construct a new scan for nodes request
 */
ScanForNodesRequest::ScanForNodesRequest(const quint8 channel, const quint16 panId)
    : CommandRequest(0x8c, 14000)
{
    // The radio device saves memory by sending 25 nodes each time insead of cacheing all found nodes.
    // We want to prevet resending the command therefore sets the retries to one. We also do not know
    // when the device is done. We let the timeout decide when we remove the command from the queue.
    this->retries = 0;

    this->channel = channel;
    this->panId = panId;

    addData(channel);
    addData(panId);
}

/**
 * @brief ScanForNodesRequest::isResponse Check handler for valid response
 * @param handler The handler
 */
bool ScanForNodesRequest::isResponse(CommandHandler* handler)
{
    ScanForNodesHandler* scanForNodesHandler = dynamic_cast<ScanForNodesHandler*>(handler);
    if (scanForNodesHandler == nullptr)
        return false;

    return true;
}
