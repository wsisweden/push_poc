#pragma once

#include "../../Shared.h"
#include "CommandRequest.h"
#include "../Handler/CommandHandler.h"

class MPACCESS_EXPORT ScanForNodesRequest : public CommandRequest
{
    public:
        explicit ScanForNodesRequest(const quint8 channel, const quint16 panId);

        bool isResponse(CommandHandler* handler) override;
};
