#include "SetRadioParamRequest.h"
#include "../Handler/SetRadioParamHandler.h"

/**
 * @brief SetRadioParamRequest::SetRadioParamRequest Construct a new set radio parameters request
 * @param channel The channel to set
 * @param panId The PAN ID to set
 */
SetRadioParamRequest::SetRadioParamRequest(const quint8 channel, const quint16 panId, const quint16 address)
    : CommandRequest(0x84, 500)
{
    this->channel = channel;
    this->panId = panId;

    addData(channel);
    addData(panId);
    addData(address);
}

/**
 * @brief SetRadioParamRequest::isResponse Check handler for valid response
 * @param handler The handler
 */
bool SetRadioParamRequest::isResponse(CommandHandler* handler)
{
    SetRadioParamHandler* setRadioParamHandler = dynamic_cast<SetRadioParamHandler*>(handler);
    if (setRadioParamHandler == nullptr)
        return false;

    return true;
}
