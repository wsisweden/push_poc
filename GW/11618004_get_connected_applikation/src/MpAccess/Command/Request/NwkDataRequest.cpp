#include "NwkDataRequest.h"

/**
 * @brief NwkDataRequest::NwkDataRequest Construct a new data request
 */
NwkDataRequest::NwkDataRequest()
    : CommandRequest(0x87, 6000)
{
}

/**
 * @brief NwkDataRequest::NwkDataRequest Construct a new data request
 * @param timeout The timeout
 */
NwkDataRequest::NwkDataRequest(const quint16 timeout)
    : CommandRequest(0x87, timeout)
{
}

/**
 * @brief NwkDataRequest::isResponse Check handler for valid response
 * @param handler The handler
 */
bool NwkDataRequest::isResponse(CommandHandler* handler)
{
    NwkDataHandler* nwkDataHandler = dynamic_cast<NwkDataHandler*>(handler);
    if (nwkDataHandler != nullptr)
        return isResponse(nwkDataHandler);

    DataIndicationHandler* dataIndicationHandler = dynamic_cast<DataIndicationHandler*>(handler);
    if (dataIndicationHandler != nullptr)
        return isResponse(dataIndicationHandler);

    return false;
}

/**
 * @brief NwkDataRequest::isResponse Check handler for valid response
 * @param handler The handler
 */
bool NwkDataRequest::isResponse(NwkDataHandler* handler)
{
    if (handler->getStatus() == NwkDataHandler::STATUS_NO_ROUTE)
        return true;

    return false;
}
