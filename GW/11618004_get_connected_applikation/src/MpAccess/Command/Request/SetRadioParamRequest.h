#pragma once

#include "../../Shared.h"
#include "CommandRequest.h"
#include "../Handler/CommandHandler.h"

class MPACCESS_EXPORT SetRadioParamRequest : public CommandRequest
{
    public:
        explicit SetRadioParamRequest(const quint8 channel, const quint16 panId, const quint16 address);

        bool isResponse(CommandHandler* handler) override;
};
