#pragma once

#include "../../Shared.h"
#include "CommandRequest.h"
#include "../Handler/CommandHandler.h"

class MPACCESS_EXPORT GetRadioParamRequest : public CommandRequest
{
    public:
        explicit GetRadioParamRequest();

        bool isResponse(CommandHandler* handler) override;
};
