#pragma once

#include "../../Shared.h"
#include "CommandRequest.h"
#include "../Handler/CommandHandler.h"

class MPACCESS_EXPORT GetMaxPayloadRequest : public CommandRequest
{
    public:
        explicit GetMaxPayloadRequest();

        bool isResponse(CommandHandler* handler) override;
};
