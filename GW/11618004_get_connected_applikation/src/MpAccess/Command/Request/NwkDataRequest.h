#pragma once

#include "../../Shared.h"
#include "CommandRequest.h"
#include "../Handler/CommandHandler.h"
#include "../Handler/NwkDataHandler.h"
#include "../Handler/DataIndicationHandler.h"

class MPACCESS_EXPORT NwkDataRequest : public CommandRequest
{
    public:
        explicit NwkDataRequest();
        explicit NwkDataRequest(const quint16 timeout);
        virtual ~NwkDataRequest() {}

        bool isResponse(NwkDataHandler* handler);
        bool isResponse(CommandHandler* handler) override;

        virtual bool isResponse(DataIndicationHandler* handler) = 0;

};
