#pragma once

#include "../../Shared.h"
#include "../Handler/CommandHandler.h"

#include <QtCore/QVector>
#include <QtCore/QString>
#include <QtCore/QVariant>

class MPACCESS_EXPORT CommandRequest
{
    public:
        explicit CommandRequest();
        explicit CommandRequest(const quint8 eventId);
        explicit CommandRequest(const quint8 eventId, const quint16 timeout);
        explicit CommandRequest(const quint8 eventId, const quint16 timeout, const quint16 postTimeout);
        virtual ~CommandRequest() {}

        virtual quint8 getChannel() { return this->channel; }
        virtual quint16 getPanId() { return this->panId; }

        virtual bool isResponse(CommandHandler* handler) { Q_UNUSED(handler); return false; }

        virtual quint8 getEventId();
        virtual void toBuffer(QVector<quint8>& buffer);

        bool isRetry();
        bool isValid();
        bool isComplete();

        void clearRetries() { this->retries = 0; }

        quint8 getRetries();
        quint8 getResponseEventId();
        quint16 getTimeout();
        quint16 getPostTimeout();
        QVector<quint8>& getData();

        void addToCommand(const quint8 value);

        static const int MAX_BUFF_SIZE = 0xff + 5;

    protected:
        void addData(const bool value);
        void addData(const quint8 value);
        void addData(const quint16 value);
        void addData(const quint32 value);
        void addData(const quint64 value);
        void addData(const QString& data);
        void addData(const QVariant& data);
        void addData(const QVector<quint8>& data);

        quint8 calcChecksum(quint8 value, quint8 sum);

        void appendToBuffer(QVector<quint8>& buffer, quint8 value);
        quint8 appendToBuffer(QVector<quint8>& buffer, quint8 value, quint8 sum);
        quint8 appendToBuffer(QVector<quint8>& buffer, quint16 value, quint8 sum);
        quint8 appendToBuffer(QVector<quint8>& buffer, QVector<quint8>& data, quint8 sum);

        static const int STX_POS = 0;
        static const int POP_NET_GROUP_POS = 1;
        static const int EVENT_ID_POS = 2;
        static const int DATA_LENGTH_POS = 3;
        static const int COMMAND_TIMEOUT = 7000;
        static const int COMMAND_RETRIES = 2;
        static const int COMMAND_POST_TIMEOUT = 10;
        static const quint8 STX = 0x02;
        static const quint8 POP_NET_GROUP = 0x50;

        quint8 eventId;
        quint8 retries;
        quint8 responseEventId;
        quint16 timeout;
        quint16 postTimeout;
        QVector<quint8> data;
        quint8 channel = 0;
        quint16 panId = 0;
};
