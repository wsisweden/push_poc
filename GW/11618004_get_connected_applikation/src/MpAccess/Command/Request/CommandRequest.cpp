#include "CommandRequest.h"

/**
 * @brief CommandRequest::CommandRequest Construct a new command
 * @param eventId The event id
 */
CommandRequest::CommandRequest()
{
    this->eventId = 0;
    this->responseEventId = 0;
}

/**
 * @brief CommandRequest::CommandRequest Construct a new command
 * @param eventId The event id
 */
CommandRequest::CommandRequest(const quint8 eventId)
{
    this->eventId = eventId;
    this->responseEventId = eventId;

    this->retries = COMMAND_RETRIES;
    this->timeout = COMMAND_TIMEOUT;
    this->postTimeout = COMMAND_POST_TIMEOUT;
}

/**
 * @brief CommandRequest::CommandRequest Construct a new command
 * @param eventId The event id
 * @param timeout The command timeout
 */
CommandRequest::CommandRequest(const quint8 eventId, const quint16 timeout)
{
    this->eventId = eventId;
    this->timeout = timeout;
    this->responseEventId = eventId;

    this->retries = COMMAND_RETRIES;
    this->postTimeout = COMMAND_POST_TIMEOUT;
}

/**
 * @brief CommandRequest::CommandRequest Construct a new command
 * @param eventId The event id
 * @param timeout The command timeout
 * @param timeout The command post timeout
 */
CommandRequest::CommandRequest(const quint8 eventId, const quint16 timeout, const quint16 postTimeout)
{
    this->eventId = eventId;
    this->timeout = timeout;
    this->postTimeout = postTimeout;
    this->responseEventId = eventId;

    this->retries = COMMAND_RETRIES;
}

/**
 * @brief CommandRequest::toMessage Write command to buffer
 * @param buf The buffer
 */
void CommandRequest::toBuffer(QVector<quint8>& buffer)
{
    quint8 sum = 0;

    appendToBuffer(buffer, STX);
    sum = appendToBuffer(buffer, POP_NET_GROUP, sum);
    sum = appendToBuffer(buffer, this->eventId, sum);
    sum = appendToBuffer(buffer, (quint16)this->data.size(), sum);
    sum = appendToBuffer(buffer, this->data, sum);
    appendToBuffer(buffer, sum);
}

/**
 * @brief CommandRequest::addToCommand Add data to command
 * @param value The value
 */
void CommandRequest::addToCommand(const quint8 value)
{
    this->data.push_back(value);
}

/**
 * @brief CommandRequest::isValid Check if command is valid
 * @return If command is valid
 */
bool CommandRequest::isValid()
{
    quint8 sum = 0;
    quint16 length = 0;

    if (this->eventId != 0)
        return true;

    for (int i = 0; i < this->data.size(); ++i)
    {
        switch (i)
        {
            case STX_POS:
                if (this->data[i] != STX)
                    return false;
                break;
            case POP_NET_GROUP_POS:
                if (this->data[i] != POP_NET_GROUP)
                    return false;
                sum = calcChecksum(this->data[i], sum);
                break;
            case DATA_LENGTH_POS:
                length = this->data[i];
                sum = calcChecksum(this->data[i], sum);
                break;
            case DATA_LENGTH_POS + 1:
                length |= this->data[i] << 8;
                sum = calcChecksum(this->data[i], sum);
                break;
            default:
                sum = calcChecksum(this->data[i], sum);
                break;
        }
    }

    if (this->data.size() == DATA_LENGTH_POS + 2 + (int)length + 1)
    {
        if (sum != 0)
            return false;

        this->eventId = this->data[EVENT_ID_POS];
        this->data.erase(this->data.begin(), this->data.begin() + DATA_LENGTH_POS + 2);
        this->data.erase(this->data.begin() + this->data.size() - 1);
    }

    return true;
}

/**
 * @brief CommandRequest::isComplete Check if command is complete
 * @return If command is complete
 */
bool CommandRequest::isComplete()
{
    if (this->eventId != 0)
        return true;

    return false;
}

/**
 * @brief CommandRequest::getEventId Get the event id
 * @return The event id
 */
quint8 CommandRequest::getEventId()
{
    return this->eventId;
}

/**
 * @brief CommandRequest::getResponseEventId Get the response event id
 * @return The response event id
 */
quint8 CommandRequest::getResponseEventId()
{
    return this->responseEventId;
}

/**
 * @brief CommandRequest::getData Get the data
 * @return The data
 */
QVector<quint8>& CommandRequest::getData()
{
    return this->data;
}

/**
 * @brief CommandRequest::getTimeout Get the command timeout
 * @return The timeout
 */
quint16 CommandRequest::getTimeout()
{
    return this->timeout;
}

/**
 * @brief CommandRequest::getRetries Get the retry count
 * @return The retry count
 */
quint8 CommandRequest::getRetries()
{
    return this->retries;
}

/**
 * @brief CommandRequest::isRetry Check for retries left and count down count
 * @return If retry is left
 */
bool CommandRequest::isRetry()
{
    if (this->retries > 0)
        this->retries--;

    return this->retries;
}

/**
 * @brief CommandRequest::getPostTimeout Get the command post timeout
 * @return The timeout
 */
quint16 CommandRequest::getPostTimeout()
{
    return this->postTimeout;
}

/**
 * @brief CommandRequest::calcChecksum Calculate checksum
 * @param data The data
 * @param sum The checksum
 * @return The new checksum
 */
quint8 CommandRequest::calcChecksum(quint8 value, quint8 sum)
{
    sum ^= value;
    return sum;
}

/**
 * @brief CommandRequest::appendToBuffer Append to buffer
 * @param buffer The buffer
 * @param value The value
 */
void CommandRequest::appendToBuffer(QVector<quint8>& buffer, quint8 value)
{
    buffer.append(value);
}

/**
 * @brief CommandRequest::appendToBuffer Append to buffer with checksum
 * @param buffer The buffer
 * @param value The value
 * @param sum The checksum
 * @return The new checksum
 */
quint8 CommandRequest::appendToBuffer(QVector<quint8>& buffer, quint8 value, quint8 sum)
{
    appendToBuffer(buffer, value);

    return calcChecksum(value, sum);
}

/**
 * @brief CommandRequest::appendToBuffer Append to buffer with checksum
 * @param buffer The buffer
 * @param value The value
 * @param sum The checksum
 * @return The new checksum
 */
quint8 CommandRequest::appendToBuffer(QVector<quint8>& buffer, quint16 value, quint8 sum)
{
    appendToBuffer(buffer, (quint8)(value & 0xff));
    quint8 temp = calcChecksum((quint8)(value & 0xff), sum);
    appendToBuffer(buffer, (quint8)((value >> 8) & 0xff));

    return calcChecksum((quint8)((value >> 8) & 0xff), temp);
}

/**
 * @brief CommandRequest::appendToBuffer Append to buffer with checksum
 * @param buffer The buffer
 * @param data The data
 * @param sum The checksum
 * @return The new checksum
 */
uint8_t CommandRequest::appendToBuffer(QVector<quint8>& buffer, QVector<quint8>& data, quint8 sum)
{
    for (int i = 0; i < data.size(); ++i)
        sum = appendToBuffer(buffer, data[i], sum);

    return sum;
}

/**
 * @brief CommandRequest::addData Add data to command
 * @param value The data
 */
void CommandRequest::addData(const bool value)
{
    quint8 temp = 0;

    if (value)
        temp = 1;

    this->data.append(temp);
}

/**
 * @brief CommandRequest::addData Add data to command
 * @param value The data
 */
void CommandRequest::addData(const quint8 value)
{
    this->data.append(value);
}

/**
 * @brief CommandRequest::addData Add data to command
 * @param value The data
 */
void CommandRequest::addData(const quint16 value)
{
    this->data.append((quint8)(value >> 8));
    this->data.append((quint8)(value & 0xff));
}

/**
 * @brief CommandRequest::addData Add data to command
 * @param value The data
 */
void CommandRequest::addData(const quint32 value)
{
    this->data.append((quint8)(value >> 24));
    this->data.append((quint8)(value >> 16));
    this->data.append((quint8)(value >> 8));
    this->data.append((quint8)(value & 0xff));
}

/**
 * @brief CommandRequest::addData Add data to message
 * @param value The data
 */
void CommandRequest::addData(const quint64 value)
{
    this->data.push_back((quint8)(value >> 56));
    this->data.push_back((quint8)(value >> 48));
    this->data.push_back((quint8)(value >> 40));
    this->data.push_back((quint8)(value >> 32));
    this->data.push_back((quint8)(value >> 24));
    this->data.push_back((quint8)(value >> 16));
    this->data.push_back((quint8)(value >> 8));
    this->data.push_back((quint8)(value & 0xff));
}

/**
 * @brief CommandRequest::addData Add data to message
 * @param data The data
 */
void CommandRequest::addData(const QString& data)
{
    for (quint8 value : data.toUtf8())
        this->data.append(value);
}

/**
 * @brief CommandRequest::addData Add data to message
 * @param data The data
 */
void CommandRequest::addData(const QVariant& data)
{
    if (data.userType() == QMetaType::Bool)
        addData((quint8)data.toBool());
    else if (data.userType() == QMetaType::Char)
        addData((quint8)data.toInt());
    else if (data.userType() == QMetaType::Short)
        addData((quint16)data.toInt());
    else if (data.userType() == QMetaType::Int)
        addData((quint32)data.toInt());
    else if (data.userType() == QMetaType::UShort)
        addData((quint16)data.toUInt());
    else if (data.userType() == QMetaType::UInt)
        addData((quint32)data.toUInt());
    else if (data.userType() == QMetaType::LongLong)
        addData((quint64)data.toLongLong());
    else if (data.userType() == QMetaType::ULongLong)
        addData((quint64)data.toULongLong());
    else if (data.userType() == QMetaType::QString)
        addData(data.toString() + '\0');
}

/**
 * @brief CommandRequest::addData Add data to command
 * @param value The data
 */
void CommandRequest::addData(const QVector<quint8>& value)
{
    this->data.append(value);
}
