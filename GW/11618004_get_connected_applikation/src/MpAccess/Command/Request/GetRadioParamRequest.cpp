#include "GetRadioParamRequest.h"
#include "../Handler/GetRadioParamHandler.h"

/**
 * @brief GetRadioParamRequest::GetRadioParamRequest Construct a new get radio parameters request
 */
GetRadioParamRequest::GetRadioParamRequest()
    : CommandRequest(0x85, 500) {}

/**
 * @brief GetRadioParamRequest::isResponse Check handler for valid response
 * @param handler The handler
 */
bool GetRadioParamRequest::isResponse(CommandHandler* handler)
{
    GetRadioParamHandler* radioParamHandler = dynamic_cast<GetRadioParamHandler*>(handler);
    if (radioParamHandler == nullptr)
        return false;

    return true;
}
