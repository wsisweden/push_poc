#include "GetMaxPayloadRequest.h"
#include "../Handler/GetMaxPayloadHandler.h"

/**
 * @brief GetMaxPayloadRequest::GetMaxPayloadRequest Construct a new get max payload request
 */
GetMaxPayloadRequest::GetMaxPayloadRequest()
    : CommandRequest(0x51, 500) {}

/**
 * @brief GetMaxPayloadRequest::isResponse Check handler for valid response
 * @param handler The handler
 */
bool GetMaxPayloadRequest::isResponse(CommandHandler* handler)
{
    GetMaxPayloadHandler* maxPayloadHandler = dynamic_cast<GetMaxPayloadHandler*>(handler);
    if (maxPayloadHandler == nullptr)
        return false;

    return true;
}
