#pragma once

#include "../../Shared.h"
#include "CommandRequest.h"
#include "../Handler/CommandHandler.h"

#include <QtCore/QQueue>

class MPACCESS_EXPORT ScanNetworkRequest : public CommandRequest
{
    public:
        explicit ScanNetworkRequest();

        bool isResponse(CommandHandler* handler) override;
};
