#include "ScanNetworkRequest.h"
#include "../Handler/ScanNetworkHandler.h"

/**
 * @brief ScanNetworkRequest::ScanNetworkRequest Construct a new scan network request
 */
ScanNetworkRequest::ScanNetworkRequest()
    : CommandRequest(0x83, 6000)
{
    addData((quint8)0);
    addData((quint8)0xff);
    addData((quint8)0xff);
}

/**
 * @brief ScanNetworkRequest::isResponse Check handler for valid response
 * @param handler The handler
 */
bool ScanNetworkRequest::isResponse(CommandHandler* handler)
{
    ScanNetworkHandler* scanNetworkHandler = dynamic_cast<ScanNetworkHandler*>(handler);
    if (scanNetworkHandler == nullptr)
        return false;

    return true;
}
