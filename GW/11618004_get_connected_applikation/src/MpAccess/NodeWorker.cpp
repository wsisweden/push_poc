#include "NodeWorker.h"
#include "UniqueKey.h"
#include "Device/UartDevice.h"
#include "Command/Request/ScanNetworkRequest.h"
#include "Command/Request/SetRadioParamRequest.h"
#include "Command/Request/GetRadioParamRequest.h"
#include "Command/Request/ScanForNodesRequest.h"
#include "Message/Request/SyncNodeRequest.h"
#include "Message/Request/GetInfoRequest.h"
#include "Message/Request/GetMeasRequest.h"
#include "Message/Request/GetStatusRequest.h"
#include "Message/Request/GetConfigRequest.h"
#include "Message/Request/GetLogRequest.h"
#include "Message/Request/SetParamRequest.h"
#include "Message/Handler/BmInfoHandler.h"
#include "Message/Handler/CmInfoHandler.h"
#include "Message/Handler/BmStatusHandler.h"
#include "Message/Handler/CmStatusHandler.h"
#include "Message/Handler/BmLogHandler.h"
#include "Message/Handler/CmLogHandler.h"
#include "Message/Model/BmInfoModel.h"
#include "Message/Model/CmInfoModel.h"
#include "Message/Model/BmHistoryLogModel.h"
#include "Message/Model/CmHistoryLogModel.h"
#include "Message/Model/BmEventLogModel.h"
#include "Message/Model/CmEventLogModel.h"
#include "Message/Model/BmInstantLogModel.h"
#include "Message/Model/CmInstantLogModel.h"
#include "Message/Model/SetParamModel.h"
#include "../Cloud/Request/RequestPackage.h"
#include "../Common/Random.h"
#include "../Common/Constant.h"
#include "../Common/EventManager.h"
#include "../Common/Event/NodeAddedEvent.h"
#include "../Common/Event/NodeRemovedEvent.h"
#include "../Common/Event/NodeUpdatedEvent.h"
#include "../Common/Event/RequestEvent.h"
#include "../Common/Event/ActivityEvent.h"
#include "../Storage/IStorage.h"
#include "../Storage/Model/ProductTypeModel.h"

#include <QtCore/QDebug>
#include <QtCore/QThread>
#include <QtCore/QDateTime>
#include <QtCore/QStringList>
#include <QtCore/QLoggingCategory>
#include <QtCore/QList>
#include <QtCore/QVariant>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QByteArray>

/**
 * @brief NodeWorker::NodeWorker Construct a new parser
 * @param context The context
 */
NodeWorker::NodeWorker(Context* context, QObject* parent)
    : IReciever(parent)
{
    qInfo().noquote() << "Initialize node worker";

    this->context = context;

    this->scanNetworkTimer = new QTimer(this);
    this->collectDataTimer = new QTimer(this);
    this->removeNodeTimer = new QTimer(this);

    QString networkRestriction = this->context->getStorage()->getConfigurationByName("Network").getValue();
    if (this->context->getCommandLineParser()->isSet("network") &&
        this->context->getCommandLineParser()->value("network").contains(":") && networkRestriction != this->context->getCommandLineParser()->value("network"))
    {
        qInfo().noquote() << "Override network restriction using command line option";

        this->useNetworkRestriction = true;
        this->channelRestriction = (quint8)this->context->getCommandLineParser()->value("network").split(":").at(0).toUInt();
        this->panIdRestriction = (quint16)this->context->getCommandLineParser()->value("network").split(":").at(1).toUInt();

        qInfo().noquote() << QString("Network restriction (Channel: %1, PanId: %2)").arg(this->channelRestriction).arg(this->panIdRestriction);
    }
    else if (!networkRestriction.isEmpty() && networkRestriction.contains(':'))
    {
        this->useNetworkRestriction = true;
        this->channelRestriction = (quint8)networkRestriction.split(":").at(0).toUInt();
        this->panIdRestriction = (quint16)networkRestriction.split(":").at(1).toUInt();

        qInfo().noquote() << QString("Network restriction (Channel: %1, PanId: %2)").arg(this->channelRestriction).arg(this->panIdRestriction);
    }

    QString nodeAddressRestriction = this->context->getStorage()->getConfigurationByName("Node").getValue();
    if (this->context->getCommandLineParser()->isSet("node") &&
        nodeAddressRestriction != this->context->getCommandLineParser()->value("node"))
    {
        qInfo().noquote() << "Override node restriction using command line option";

        this->useNodeRestriction = true;
        this->nodeAddressRestriction = (quint32)this->context->getCommandLineParser()->value("node").toUInt();

        qInfo().noquote() << QString("Node restriction (Address: %1)").arg(this->nodeAddressRestriction);
    }
    else if (!nodeAddressRestriction.isEmpty())
    {
        this->useNodeRestriction = true;
        this->nodeAddressRestriction = (quint32)nodeAddressRestriction.toUInt();

        qInfo().noquote() << QString("Node restriction (Address: %1)").arg(this->nodeAddressRestriction);
    }

    if (this->context->getCommandLineParser()->isSet("devicetype") && this->context->getCommandLineParser()->value("devicetype") == "uart")
        this->device = (IDevice*)new UartDevice(this->context, this);

    if (this->device == nullptr || !this->device->isOpen())
        return;

    this->commandFactory = new CommandHandlerFactory(this->context, *this, this);
    this->device->setCommandFactory(this->commandFactory);

    connect(this->scanNetworkTimer, &QTimer::timeout, this, &NodeWorker::scanNetwork);
    connect(this->collectDataTimer, &QTimer::timeout, this, &NodeWorker::collectData);
    connect(this->removeNodeTimer, &QTimer::timeout, this, &NodeWorker::removeNodes);
    connect(this->context->getEventManager(), &EventManager::syncNode, this, &NodeWorker::syncNode);
    connect(this->context->getEventManager(), &EventManager::nodeTimeout, this, &NodeWorker::nodeTimeout);
    connect(this->context->getEventManager(), &EventManager::invalidIndex, this, &NodeWorker::invalidIndex);
    connect(this->context->getEventManager(), &EventManager::initializeComplete, this, &NodeWorker::initializeComplete);
    connect(this->context->getEventManager(), &EventManager::factoryReset, this, &NodeWorker::doFactoryReset, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::uplinkChanged, this, &NodeWorker::uplinkChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::indexReceived, this, &NodeWorker::indexReceived, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationChanged, this, &NodeWorker::configurationChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::authorizationChanged, this, &NodeWorker::authorizationChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationComplete, this, &NodeWorker::configurationComplete, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::nodeRestrictionChanged, this, &NodeWorker::nodeRestrictionChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::networkRestrictionChanged, this, &NodeWorker::networkRestrictionChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::scanNetworkIntervalChanged, this, &NodeWorker::scanNetworkIntervalChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::collectDataIntervalChanged, this, &NodeWorker::collectDataIntervalChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::removeNodeIntervalChanged, this, &NodeWorker::removeNodeIntervalChanged, Qt::QueuedConnection);

    QTimer::singleShot(10000, this, [this]()
    {
        setupWorker();
    });
}

/**
 * @brief NodeWorker::~NodeWorker Teardown worker
 */
NodeWorker::~NodeWorker()
{
    qInfo().noquote() << "Teardown node worker";

    this->removeNodeTimer->stop();
    this->scanNetworkTimer->stop();
    this->collectDataTimer->stop();

    removeBlacklist();
}

/**
 * @brief NodeWorker::removeBlacklist Remove the blacklist
 */
void NodeWorker::removeBlacklist()
{
    QString path = QString("%1/logs").arg(this->context->getCommandLineParser()->isSet("logpath") ? this->context->getCommandLineParser()->value("logpath") : qApp->applicationDirPath());
    QFile file(QString("%1/blacklist.log").arg(path));
    if (file.exists())
        file.remove();
}

/**
 * @brief NodeWorker::setupWorker Setup worker
 */
void NodeWorker::setupWorker()
{
    this->configComplete = 0;
    this->initRadioModuleAttempts = 0;
    this->firmwareAddressReceived = false;

    this->nodes.clear();
    this->device->reset();

    // Populate nodes from storage.
    for (const NodeModel& model : this->context->getStorage()->getNodes().values())
    {
        if (this->useNetworkRestriction && (model.getChannel() != this->channelRestriction || model.getPanId() != this->panIdRestriction))
        {
            qInfo().noquote() << QString("Dropping node due to network restriction (Channel: %1, PanId: %2)").arg(model.getChannel()).arg(model.getPanId());
            continue;
        }

        if (this->useNodeRestriction && model.getAddress() != this->nodeAddressRestriction)
        {
            qInfo().noquote() << QString("Dropping node due to address restriction (Address: %1)").arg(model.getAddress());
            continue;
        }

        QString key = UniqueKey::makeKey(model.getChannel(), model.getPanId(), model.getAddress());
        this->nodes[key] = new Node(model.getNodeId(), model.getChannel(), model.getPanId(), model.getAddress(), model.getProductTypeValue(), this);

        this->context->getEventManager()->emitNodeAddedEvent(NodeAddedEvent(model.getId(), model.getNodeId(), model.getChannel(), model.getPanId(),
                                                                            model.getAddress(), model.getHistoryLogIndex(), model.getEventLogIndex(),
                                                                            model.getInstantLogIndex(), model.getTimestamp(), model.getProductTypeValue()));

        qInfo().noquote() << QString("Found cached node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, Nodes: %6)").arg(QString::number(model.getNodeId()))
                                                                                                                            .arg(QString::number(model.getChannel()))
                                                                                                                            .arg(QString::number(model.getPanId()))
                                                                                                                            .arg(QString::number(model.getAddress()))
                                                                                                                            .arg(translateProductType(model.getProductTypeValue()))
                                                                                                                            .arg(this->nodes.count());
    }

    this->removeNodeInterval = this->context->getStorage()->getConfigurationByName("RemoveNodeInterval").getValue().toInt();
    this->scanNetworkInterval = this->context->getStorage()->getConfigurationByName("ScanNetworkInterval").getValue().toInt();
    this->collectDataInterval = this->context->getStorage()->getConfigurationByName("CollectDataInterval").getValue().toInt();

    initializeRadioModule();
}

/**
 * @brief NodeWorker::initializeRadioModule Initialize radio module.
 */
void NodeWorker::initializeRadioModule()
{
    if (this->firmwareAddressReceived)
        return;

    this->initRadioModuleAttempts++;
    if (this->initRadioModuleAttempts <= MAX_RADIO_MODULE_RETRY)
    {
        GetRadioParamRequest* request = new GetRadioParamRequest();
        this->device->set(request);

        QTimer::singleShot(1000, this, &NodeWorker::initializeRadioModule);
    }
    else
    {
        qCritical().noquote() << QString("Radio address not received");
    }
}

/**
 * @brief NodeWorker::setRadioAddress Set radio address
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 * @param firmwareType The firmware type
 * @param firmwareVersion The firmware version
 */
void NodeWorker::setRadioAddress(const quint8 channel, const quint16 panId, const quint16 address, const quint32 firmwareType, const quint32 firmwareVersion)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);

    this->firmwareAddressReceived = true;

    this->firmwareAddress = address;
    this->device->setFirmwareAddress(this->firmwareAddress);

    qInfo().noquote() << QString("Received radio address (Address: %1)").arg(this->firmwareAddress);

    this->context->getEventManager()->emitFirmwareUpdatedEvent(FirmwareUpdatedEvent(firmwareType, firmwareVersion, this->firmwareAddress));

    // Let all know we are done => backend send config updates to the device.
    this->context->getEventManager()->emitConfigurationReadyEvent();
}

/**
 * @brief NodeWorker::initializeComplete Initialize complete handler
 */
void NodeWorker::initializeComplete()
{
    this->removeNodeTimer->start(this->removeNodeInterval);
    this->scanNetworkTimer->start((int)(Random::minutes(this->scanNetworkInterval / 60000) * 60000));

    QTimer::singleShot(1000, this, &NodeWorker::collectData);
}

/**
 * @brief NodeWorker::scanNetwork Scan for networks
 */
void NodeWorker::scanNetwork()
{
    // Scan for nodes more rarely the more nodes we already discovered.
    this->scanNetworkTimer->start((int)(Random::minutes(this->scanNetworkInterval / 60000) * 60000));

    qInfo().noquote() << QString("Scan network triggered (Authorized: %1, Nodes: %2, Scheduled: %3)").arg((this->authorized == true) ? "Yes" : "No")
                                                                                                     .arg(this->nodes.count())
                                                                                                     .arg(QDateTime::currentDateTimeUtc().addMSecs(this->scanNetworkTimer->interval()).toString("yyyy-MM-ddThh:mm:ss"));

    if (!this->authorized)
    {
        this->context->getEventManager()->emitAuthorizeEvent();
        return;
    }

    this->context->getEventManager()->emitActivityChangedEvent(ActivityEvent("Scanning"));

    for (quint8 channel : this->network.keys())
        this->network[channel].clear();

    this->network.clear();

    ScanNetworkRequest* request = new ScanNetworkRequest();
    this->device->send(request);
}

/**
 * @brief NodeWorker::configurationComplete Configuration complete handler
 * @param event The event
 */
void NodeWorker::configurationComplete()
{
    this->configComplete++;
    if (this->configComplete == 3)
    {
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::READY, true));
        this->context->getEventManager()->emitInitializeCompleteEvent();
    }
}

/**
 * @brief NodeWorker::authorizationChanged Authorization changed handler
 * @param event The event
 */
void NodeWorker::authorizationChanged(const AuthorizationEvent& event)
{
    bool authorized = event.getAuthorized();
    if (this->authorized != authorized)
    {
        this->authorized = authorized;
        if (!this->authorized)
            this->device->reset();
        else
            this->context->getEventManager()->emitActivityChangedEvent(ActivityEvent(""));
    }
}

/**
 * @brief NodeWorker::uplinkChanged Uplink changed handler
 * @param event The event
 */
void NodeWorker::uplinkChanged(const UplinkEvent& event)
{
    bool uplink = event.getUplink();
    if (this->uplink != uplink)
    {
        this->uplink = uplink;
        if (!this->uplink)
            this->device->reset();
    }
}

/**
 * @brief NodeWorker::invalidIndex Invalid index handler
 * @param event The event
 */
void NodeWorker::invalidIndex(const InvalidIndexEvent& event)
{
    QString key = UniqueKey::makeKey(event.getChannel(), event.getPanId(), event.getAddress());
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->setInvalidIndex(true);

    qWarning().noquote() << QString("Invalid index occured (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6)").arg(QString::number(node->getNodeId()))
                                                                                                                             .arg(QString::number(node->getChannel()))
                                                                                                                             .arg(QString::number(node->getPanId()))
                                                                                                                             .arg(QString::number(node->getAddress()))
                                                                                                                             .arg(translateProductType(node->getProductType()))
                                                                                                                             .arg(QString::number(node->getMUI()));
}

/**
 * @brief NodeWorker::indexReceived Index updated handler
 * @param event The event
 */
void NodeWorker::indexReceived(const IndexEvent& event)
{
    QString key = UniqueKey::makeKey(event.getChannel(), event.getPanId(), event.getAddress());
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];

    const NodeModel& model = this->context->getStorage()->getNode(event.getChannel(), event.getPanId(), event.getAddress());

    quint32 tailIndex = 0;
    quint32 storedIndex = 0;
    if (node->getInfoModel()->getId() == BmInfoHandler::ID)
    {
        BmInfoModel* infoModel = (BmInfoModel*)node->getInfoModel();

        switch (event.getLogType())
        {
            case LogType::HISTORY:
                node->setHistoryIndexReceived(true);
                storedIndex = model.getHistoryLogIndex();
                tailIndex = infoModel->getHistLogIndex() - infoModel->getHistLogInMem();
                break;
            case LogType::EVENT:
                node->setEventIndexReceived(true);
                storedIndex = model.getEventLogIndex();
                tailIndex = infoModel->getEvtLogIndex() - infoModel->getEvtLogInMem();
                break;
            case LogType::INSTANT:
                node->setInstantIndexReceived(true);
                storedIndex = model.getInstantLogIndex();
                tailIndex = infoModel->getInstLogIndex() - infoModel->getInstLogInMem();
                break;
            default:
                break;
        }
    }
    else if (node->getInfoModel()->getId() == CmInfoHandler::ID)
    {
        CmInfoModel* infoModel = (CmInfoModel*)node->getInfoModel();

        switch (event.getLogType())
        {
            case LogType::HISTORY:
                node->setHistoryIndexReceived(true);
                storedIndex = model.getHistoryLogIndex();
                tailIndex = infoModel->getHistLogIndex() - infoModel->getHistLogInMem();
                break;
            case LogType::EVENT:
                node->setEventIndexReceived(true);
                storedIndex = model.getEventLogIndex();
                tailIndex = infoModel->getEvtLogIndex() - infoModel->getEvtLogInMem();
                break;
            case LogType::INSTANT:
                node->setInstantIndexReceived(true);
                storedIndex = model.getInstantLogIndex();
                tailIndex = infoModel->getInstLogIndex() - infoModel->getInstLogInMem();
                break;
            default:
                break;
        }
    }

    if (event.getData().empty())
    {
        quint32 index = 0;

        this->context->getStorage()->updateNodeLogIndex(event.getChannel(), event.getPanId(), event.getAddress(), index, event.getLogType());
        qInfo().noquote() << QString("Updated %1 index (Id: %2, Channel: %3, PanId: %4, Address: %5 Type: %6, MUI: %7, Index: %8)").arg(translateLogType(event.getLogType()))
                                                                                                                                   .arg(QString::number(node->getNodeId()))
                                                                                                                                   .arg(QString::number(node->getChannel()))
                                                                                                                                   .arg(QString::number(node->getPanId()))
                                                                                                                                   .arg(QString::number(node->getAddress()))
                                                                                                                                   .arg(translateProductType(node->getProductType()))
                                                                                                                                   .arg(QString::number(node->getMUI()))
                                                                                                                                   .arg(index);
    }
    else
    {
        bool found = false;
        quint32 index = qMax(tailIndex, storedIndex);
        for (const QJsonValue& value : event.getData())
        {
            int start = value.toObject().value("start").toInt();
            int end = value.toObject().value("end").toInt();

            // Look for index in start - end range.
            if (index >= (quint32)start || index <= (quint32)end)
            {
                index = end;
                found = true;
            }
        }

        if (found && index > storedIndex)
        {
            this->context->getStorage()->updateNodeLogIndex(event.getChannel(), event.getPanId(), event.getAddress(), index, event.getLogType());
            qInfo().noquote() << QString("Updated %1 index (Id: %2, Channel: %3, PanId: %4, Address: %5 Type: %6, MUI: %7, Index: %8)").arg(translateLogType(event.getLogType()))
                                                                                                                                       .arg(QString::number(node->getNodeId()))
                                                                                                                                       .arg(QString::number(node->getChannel()))
                                                                                                                                       .arg(QString::number(node->getPanId()))
                                                                                                                                       .arg(QString::number(node->getAddress()))
                                                                                                                                       .arg(translateProductType(node->getProductType()))
                                                                                                                                       .arg(QString::number(node->getMUI()))
                                                                                                                                       .arg(index);
        }
    }

    if (node->isIndexesReady())
    {
        // Send request if info has changed.
        if (node->hasInfoChanged())
        {
            getNodeMeas(node->getChannel(), node->getPanId(), node->getAddress());
            getNodeConfig(node->getChannel(), node->getPanId(), node->getAddress());
            getNodeStatus(node->getChannel(), node->getPanId(), node->getAddress());
            getNodeLogs(node->getChannel(), node->getPanId(), node->getAddress());
            setNodeSync(node->getChannel(), node->getPanId(), node->getAddress(), node->getNodeId(), node->getProductType());
        }
        else
        {
            qInfo().noquote() << QString("No info change detected (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6)").arg(QString::number(node->getNodeId()))
                                                                                                                                    .arg(QString::number(node->getChannel()))
                                                                                                                                    .arg(QString::number(node->getPanId()))
                                                                                                                                    .arg(QString::number(node->getAddress()))
                                                                                                                                    .arg(translateProductType(node->getProductType()))
                                                                                                                                    .arg(QString::number(node->getMUI()));
        }
    }
}

/**
 * @brief NodeWorker::configurationChanged Configuration changed handler
 * @param event The event
 */
void NodeWorker::configurationChanged(const ConfigurationEvent& event)
{
    if (this->configComplete < 3)
        qInfo().noquote() << QString("Set parameter (Index: %1, Data: \"%2\")").arg(event.getIndex()).arg(event.getData().toString());
    else if (event.getIndex() != 28) // Do not print wifi password.
        qDebug().noquote() << QString("Set parameter (Index: %1, Data: \"%2\")").arg(event.getIndex()).arg(event.getData().toString());

    SetParamRequest* request = new SetParamRequest(this->firmwareAddress, event.getIndex(), event.getData());
    this->device->set(request);
}

/**
 * @brief NodeWorker::scanNetworkIntervalChanged Scan network interval changed handler
 * @param event The event
 */
void NodeWorker::scanNetworkIntervalChanged(const ScanNetworkIntervalEvent& event)
{
    if (this->scanNetworkInterval == event.getInterval())
        return;

    this->scanNetworkInterval = event.getInterval();

    // Schedule new interval and restart the timer.
    this->scanNetworkTimer->start((int)(Random::minutes(this->scanNetworkInterval / 60000) * 60000));
    qInfo().noquote() << QString("Rescheduled scan network (Scheduled: %1)").arg(QDateTime::currentDateTimeUtc().addMSecs(this->scanNetworkTimer->interval()).toString("yyyy-MM-ddThh:mm:ss"));
}

/**
 * @brief NodeWorker::collectDataIntervalChanged Collect data interval changed handler
 * @param event The event
 */
void NodeWorker::collectDataIntervalChanged(const CollectDataIntervalEvent& event)
{
    if (this->collectDataInterval == event.getInterval())
        return;

    this->collectDataInterval = event.getInterval();

    // Schedule new interval and restart the timer.
    this->collectDataTimer->start((int)(Random::minutes(this->collectDataInterval / 60000) * 60000));
    qInfo().noquote() << QString("Rescheduled collect data (Scheduled: %1)").arg(QDateTime::currentDateTimeUtc().addMSecs(this->collectDataTimer->interval()).toString("yyyy-MM-ddThh:mm:ss"));
}

/**
 * @brief NodeWorker::removeNodeIntervalChanged Remove node interval changed handler
 * @param event The event
 */
void NodeWorker::removeNodeIntervalChanged(const RemoveNodeIntervalEvent& event)
{
    if (this->removeNodeInterval == event.getInterval())
        return;

    this->removeNodeInterval = event.getInterval();

    // Schedule new interval and restart the timer.
    this->removeNodeTimer->start(this->removeNodeInterval);
    qInfo().noquote() << QString("Rescheduled remove nodes (Scheduled: %1)").arg(QDateTime::currentDateTimeUtc().addMSecs(this->removeNodeTimer->interval()).toString("yyyy-MM-ddThh:mm:ss"));
}

/**
 * @brief NodeWorker::networkRestrictionChanged Network restriction changed handler
 * @param event The event
 */
void NodeWorker::networkRestrictionChanged(const NetworkRestrictionEvent& event)
{
    if (event.getNetworkRestriction().isEmpty())
    {
        this->useNetworkRestriction = false;
        return;
    }

    if (event.getNetworkRestriction().contains(':'))
    {
        quint8 channelRestriction = (quint8)event.getNetworkRestriction().split(":").at(0).toUInt();
        quint16 panIdRestriction = (quint16)event.getNetworkRestriction().split(":").at(1).toUInt();

        if (this->channelRestriction == channelRestriction && this->panIdRestriction == panIdRestriction)
            return;

        this->useNetworkRestriction = true;
        this->channelRestriction = channelRestriction;
        this->panIdRestriction = panIdRestriction;

        QList<QVariant> list;
        for (const NodeModel& model : this->context->getStorage()->getNodes().values())
        {
            if (model.getChannel() == this->channelRestriction && model.getPanId() == this->panIdRestriction)
                continue;

            list.append(model.getId());
            QString key = UniqueKey::makeKey(model.getChannel(), model.getPanId(), model.getAddress());
            if (this->nodes.contains(key))
            {
                delete this->nodes[key];
                this->nodes.remove(key);
            }

            this->context->getEventManager()->emitNodeRemovedEvent(NodeRemovedEvent(model.getId(), model.getNodeId(), model.getChannel(), model.getPanId(),
                                                                                    model.getAddress(), model.getHistoryLogIndex(), model.getEventLogIndex(),
                                                                                    model.getInstantLogIndex(), model.getTimestamp(), model.getProductTypeValue()));

            qInfo().noquote() << QString("Removed node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, Nodes: %6)").arg(QString::number(model.getNodeId()))
                                                                                                                           .arg(QString::number(model.getChannel()))
                                                                                                                           .arg(QString::number(model.getPanId()))
                                                                                                                           .arg(QString::number(model.getAddress()))
                                                                                                                           .arg(translateProductType(model.getProductTypeValue()))
                                                                                                                           .arg(this->nodes.count());
        }

        if (list.count() > 0)
            this->context->getStorage()->deleteNodes(list);

        this->device->reset();
    }
}

/**
 * @brief NodeWorker::nodeRestrictionChanged Node restriction changed handler
 * @param event The event
 */
void NodeWorker::nodeRestrictionChanged(const NodeRestrictionEvent& event)
{
    if (event.getNodeRestriction().isEmpty())
    {
        this->useNodeRestriction = false;
        return;
    }

    quint32 nodeAddressRestriction = (quint32)event.getNodeRestriction().toUInt();
    if (this->nodeAddressRestriction == nodeAddressRestriction)
        return;

    this->useNodeRestriction = true;
    this->nodeAddressRestriction = nodeAddressRestriction;

    QList<QVariant> list;
    for (const NodeModel& model : this->context->getStorage()->getNodes().values())
    {
        if (this->useNetworkRestriction)
        {
            if (model.getChannel() == this->channelRestriction && model.getPanId() == this->panIdRestriction && model.getAddress() == this->nodeAddressRestriction)
                continue;
        }
        else
        {
            if (model.getAddress() == this->nodeAddressRestriction)
                continue;
        }

        list.append(model.getId());
        QString key = UniqueKey::makeKey(model.getChannel(), model.getPanId(), model.getAddress());
        if (this->nodes.contains(key))
        {
            delete this->nodes[key];
            this->nodes.remove(key);
        }

        this->context->getEventManager()->emitNodeRemovedEvent(NodeRemovedEvent(model.getId(), model.getNodeId(), model.getChannel(), model.getPanId(),
                                                                                model.getAddress(), model.getHistoryLogIndex(), model.getEventLogIndex(),
                                                                                model.getInstantLogIndex(), model.getTimestamp(), model.getProductTypeValue()));

        qInfo().noquote() << QString("Removed node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, Nodes: %6)").arg(QString::number(model.getNodeId()))
                                                                                                                       .arg(QString::number(model.getChannel()))
                                                                                                                       .arg(QString::number(model.getPanId()))
                                                                                                                       .arg(QString::number(model.getAddress()))
                                                                                                                       .arg(translateProductType(model.getProductTypeValue()))
                                                                                                                       .arg(this->nodes.count());
    }

    if (list.count() > 0)
        this->context->getStorage()->deleteNodes(list);

    this->device->reset();
}

/**
 * @brief NodeWorker::removeNodse Remove unseen nodes
 */
void NodeWorker::removeNodes()
{
    qInfo().noquote() << "Remove nodes triggered";

    qint64 interval = this->context->getStorage()->getConfigurationByName("RemoveNodeInterval").getValue().toLongLong();

    QList<QVariant> list;
    for (const NodeModel& model : this->context->getStorage()->getNodes().values())
    {
        qint64 diff = QDateTime::fromString(model.getTimestamp(), "yyyy-MM-dd hh:mm:ss.zzz").msecsTo(QDateTime::currentDateTimeUtc());
        if (diff >= interval)
        {
            QString key = UniqueKey::makeKey(model.getChannel(), model.getPanId(), model.getAddress());
            if (this->nodes.contains(key))
            {
                delete this->nodes[key];
                this->nodes.remove(key);

                list.append(model.getId());

                this->context->getEventManager()->emitNodeRemovedEvent(NodeRemovedEvent(model.getId(), model.getNodeId(), model.getChannel(), model.getPanId(),
                                                                                        model.getAddress(), model.getHistoryLogIndex(), model.getEventLogIndex(),
                                                                                        model.getInstantLogIndex(), model.getTimestamp(), model.getProductTypeValue()));

                qInfo().noquote() << QString("Removed node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, Nodes: %6)").arg(QString::number(model.getNodeId()))
                                                                                                                               .arg(QString::number(model.getChannel()))
                                                                                                                               .arg(QString::number(model.getPanId()))
                                                                                                                               .arg(QString::number(model.getAddress()))
                                                                                                                               .arg(translateProductType(model.getProductTypeValue()))
                                                                                                                               .arg(this->nodes.count());
            }
        }
    }

    if (list.count() > 0)
        this->context->getStorage()->deleteNodes(list);
}

/**
 * @brief NodeWorker::addNetwork Add network
 * @param channel The channel
 * @param pan The PAN ID
 */
void NodeWorker::addNetwork(const quint8 channel, const quint16 panId)
{
    if (this->useNetworkRestriction && (channel != this->channelRestriction || panId != this->panIdRestriction))
    {
        qInfo().noquote() << QString("Dropping network due to restriction (Channel: %1, PanId: %2)").arg(channel).arg(panId);
        return;
    }

    qInfo().noquote() << QString("Found network (Channel: %1, PanId: %2)").arg(channel).arg(panId);
    this->network[channel].append(panId);

    setRadioParam(channel, panId);
    scanForNodes(channel, panId);
}

/**
 * @brief NodeWorker::setRadioParam Set SM radio parameters
 * @param channel The channel
 * @param panId The PAN ID
 */
void NodeWorker::setRadioParam(const quint8 channel, quint16 panId)
{
    SetRadioParamRequest* request = new SetRadioParamRequest(channel, panId, this->firmwareAddress);
    this->device->send(request);
}

/**
 * @brief NodeWorker::scanForNodes Scan for nodes in PAN and channel
 * @param channel The channel to request
 * @param panId The PAN ID to request
 */
void NodeWorker::scanForNodes(const quint8 channel, quint16 panId)
{
    ScanForNodesRequest* request = new ScanForNodesRequest(channel, panId);
    this->device->send(request);
}

/**
 * @brief NodeWorker::addNode Add node to list
 * @param nodeId The node id
 * @param channel The channel
 * @param panId The pad id
 * @param address The node address
 * @param productType Product type (0 = Unknown, 1 = BMU, 2 = Vehicle, 3 = Charger, 4 = PC)
 */
void NodeWorker::addNode(const quint32 nodeId, quint8 channel, quint16 panId, const quint16 address, const quint8 productType)
{
    // Filter gateways.
    if ((address & 0xff00) == 0xff00)
        return;

    if (this->useNetworkRestriction && (channel != this->channelRestriction || panId != this->panIdRestriction))
    {
        qInfo().noquote() << QString("Dropping node due to network restriction (Channel: %1, PanId: %2)").arg(channel).arg(panId);
        return;
    }

    if (this->useNodeRestriction && (address != this->nodeAddressRestriction))
    {
        qInfo().noquote() << QString("Dropping node due to restriction (Address: %1)").arg(address);
        return;
    }

    QString key = UniqueKey::makeKey(channel, panId, address);
    if (this->nodes.contains(key))
    {
        this->context->getStorage()->updateNodeTimestamp(channel, panId, address, QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"));

        const NodeModel& nodeModel = this->context->getStorage()->getNode(channel, panId, address);
        this->context->getEventManager()->emitNodeUpdatedEvent(NodeUpdatedEvent(nodeModel.getId(), nodeModel.getNodeId(), nodeModel.getChannel(), nodeModel.getPanId(),
                                                                                nodeModel.getAddress(), nodeModel.getHistoryLogIndex(), nodeModel.getEventLogIndex(),
                                                                                nodeModel.getInstantLogIndex(), nodeModel.getTimestamp(), nodeModel.getProductTypeValue()));

        qInfo().noquote() << QString("Found seen node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5)").arg(QString::number(nodeModel.getNodeId()))
                                                                                                               .arg(QString::number(nodeModel.getChannel()))
                                                                                                               .arg(QString::number(nodeModel.getPanId()))
                                                                                                               .arg(QString::number(nodeModel.getAddress()))
                                                                                                               .arg(translateProductType(nodeModel.getProductTypeValue()));
    }
    else
    {
        this->nodes[key] = new Node(nodeId, channel, panId, address, productType, this);

        NodeModel model(0, nodeId, channel, panId, address, 0, 0, 0, QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"), productType);
        this->context->getStorage()->insertNode(model);

        this->context->getEventManager()->emitNodeAddedEvent(NodeAddedEvent(model.getId(), model.getNodeId(), model.getChannel(), model.getPanId(),
                                                                            model.getAddress(), model.getHistoryLogIndex(), model.getEventLogIndex(),
                                                                            model.getInstantLogIndex(), model.getTimestamp(), model.getProductTypeValue()));

        qInfo().noquote() << QString("Found new node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, Nodes: %6)").arg(QString::number(nodeId))
                                                                                                                         .arg(QString::number(channel))
                                                                                                                         .arg(QString::number(panId))
                                                                                                                         .arg(QString::number(address))
                                                                                                                         .arg(translateProductType(productType))
                                                                                                                         .arg(this->nodes.count());
    }
}

/**
 * @brief NodeWorker::translateLogType Translate log type to human readable form
 * @param logType The log type
 */
QString NodeWorker::translateLogType(const int logType)
{
    if (logType == LogType::HISTORY)
        return "history";
    else if (logType == LogType::EVENT)
        return "event";
    else if (logType == LogType::INSTANT)
        return "instant";
    else
        return "unknown";
}

/**
 * @brief NodeWorker::translateProductType Translate product type to human readable form
 * @param value The value
 * @return Human readable product type
 */
QString NodeWorker::translateProductType(const quint8 value)
{
    const ProductTypeModel& model = this->context->getStorage()->getProductTypeByValue(value);
    if (model.getId() == 0)
        return "Unknown";

    return model.getName();
}

/**
 * @brief NodeWorker::getSortedNodes Sort nodes on channel to minimize calls to setRadioParam()
 * @return List of nodes sorted on channel
 */
QList<Node*> NodeWorker::getSortedNodes()
{
    QList<Node*> nodes = this->nodes.values();
    qSort(nodes.begin(), nodes.end(), [](Node* n1, Node* n2){return n1->getChannel() < n2->getChannel();});

    return nodes;
}

/**
 * @brief NodeWorker::nodeTimeout Node timeout
 */
void NodeWorker::nodeTimeout(const NodeTimeoutEvent& event)
{
    QString key = UniqueKey::makeKey(event.getChannel(), event.getPanId(), event.getAddress());
    if (!this->nodes.contains(key) || this->blacklistedNodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->increaseTimeout();

    qWarning().noquote() << QString("Timeout occured (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6, Timeout: %7)").arg(QString::number(node->getNodeId()))
                                                                                                                                    .arg(QString::number(node->getChannel()))
                                                                                                                                    .arg(QString::number(node->getPanId()))
                                                                                                                                    .arg(QString::number(node->getAddress()))
                                                                                                                                    .arg(translateProductType(node->getProductType()))
                                                                                                                                    .arg(QString::number(node->getMUI()))
                                                                                                                                    .arg(QString::number(node->getTimeout()));

    // Postpone collect data.
    this->collectDataTimer->start();
    qInfo().noquote() << QString("Postponed collect data (Scheduled: %1)").arg(QDateTime::currentDateTimeUtc().addMSecs(this->collectDataTimer->interval()).toString("yyyy-MM-ddThh:mm:ss"));

    if (node->getTimeout() == 3)
    {
        QString path = QString("%1/logs").arg(this->context->getCommandLineParser()->isSet("logpath") ? this->context->getCommandLineParser()->value("logpath") : qApp->applicationDirPath());

        QDir directory(path);
        if (!directory.exists())
            directory.mkpath(".");

        QFile file(QString("%1/blacklist.log").arg(path));
        file.open(QIODevice::WriteOnly | QIODevice::Append);
        file.write(QString("Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6\n").arg(QString::number(node->getNodeId()))
                                                                                              .arg(QString::number(node->getChannel()))
                                                                                              .arg(QString::number(node->getPanId()))
                                                                                              .arg(QString::number(node->getAddress()))
                                                                                              .arg(translateProductType(node->getProductType()))
                                                                                              .arg(QString::number(node->getMUI()))
                                                                                              .toUtf8());
        file.close();
        this->blacklistedNodes.append(key);

        qWarning().noquote() << "Added node to blacklist (max timeout reached)";
    }

    node->clearLogsAndIndexes();
}

/**
 * @brief NodeWorker::collectData Collect data from the nodes
 */
void NodeWorker::collectData()
{
    // Schedule new interval and restart the timer.
    this->collectDataTimer->start((int)(Random::minutes(this->collectDataInterval / 60000) * 60000));

    qInfo().noquote() << QString("Collect data triggered (Status: %1, Authorized: %2, Uplink: %3, Nodes: %4, Queue: %5, Scheduled: %6)").arg((this->device->sendQueueCount() >= 5 || this->nodes.count() == 0 || this->uplink == false) ? "Sleeping" : "Running")
                                                                                                                                        .arg((this->authorized == true) ? "Yes" : "No")
                                                                                                                                        .arg((this->uplink == true) ? "Yes" : "No")
                                                                                                                                        .arg(this->nodes.count())
                                                                                                                                        .arg(this->device->sendQueueCount())
                                                                                                                                        .arg(QDateTime::currentDateTimeUtc().addMSecs(this->collectDataTimer->interval()).toString("yyyy-MM-ddThh:mm:ss"));

    if (!this->authorized)
    {
        this->context->getEventManager()->emitActivityChangedEvent(ActivityEvent("Authorizing"));
        this->context->getEventManager()->emitAuthorizeEvent();
        return;
    }

    if (!this->uplink)
        return;

    if (this->nodes.count() == 0 && this->device->sendQueueCount() < 5)
    {
        scanNetwork();
        return;
    }

    if (this->device->sendQueueCount() >= 5)
        return;

    this->context->getEventManager()->emitActivityChangedEvent(ActivityEvent("Collecting"));

    removeBlacklist();
    this->blacklistedNodes.clear();

    int counter = 0;
    QList<Node*> nodes = getSortedNodes();
    for (Node* node : nodes)
    {
        if (node->isQueried())
            continue;

        node->resetTimeout();
        node->setQueried(true);
        getNodeInfo(node->getChannel(), node->getPanId(), node->getAddress());

        counter++;
        if (counter == 10) // Number of nodes to query.
            break;
    }

    // Check if all nodes is queried, start over next round.
    bool allNodeQueried = true;
    for (Node* node : nodes)
    {
        if (!node->isQueried())
            allNodeQueried = false;
    }

    if (allNodeQueried)
    {
        for (Node* node : nodes)
            node->setQueried(false);
    }
}

/**
 * @brief NodeWorker::getNodeInfo Get node info
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 */
void NodeWorker::getNodeInfo(const quint8 channel, const quint16 panId, const quint16 address)
{
    GetInfoRequest* request = new GetInfoRequest(channel, panId, address);
    this->device->send(request);
}

/**
 * @brief NodeWorker::getNodeMeas Get node meas
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 */
void NodeWorker::getNodeMeas(const quint8 channel, const quint16 panId, const quint16 address)
{
    GetMeasRequest* request = new GetMeasRequest(channel, panId, address);
    this->device->send(request);
}

/**
 * @brief NodeWorker::getNodeStatus Get node status
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 */
void NodeWorker::getNodeStatus(const quint8 channel, const quint16 panId, const quint16 address)
{
    GetStatusRequest* request = new GetStatusRequest(channel, panId, address);
    this->device->send(request);
}

/**
 * @brief NodeWorker::getNodeConfig Get node config
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 */
void NodeWorker::getNodeConfig(const quint8 channel, const quint16 panId, const quint16 address)
{
    GetConfigRequest* request = new GetConfigRequest(channel, panId, address);
    this->device->send(request);
}

/**
 * @brief NodeWorker::getNodeLogs Get node logs
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 */
void NodeWorker::getNodeLogs(const quint8 channel, const quint16 panId, const quint16 address)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];

    qInfo().noquote() << QString("Request logs from node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6)").arg(QString::number(node->getNodeId()))
                                                                                                                           .arg(QString::number(node->getChannel()))
                                                                                                                           .arg(QString::number(node->getPanId()))
                                                                                                                           .arg(QString::number(node->getAddress()))
                                                                                                                           .arg(translateProductType(node->getProductType()))
                                                                                                                           .arg(QString::number(node->getMUI()));

    // The log is stored in a cirular buffer on the node with index I and count C.
    // To get the log index K we need to calculate I - C = K. Check if our saved
    // index J is larger than calculated K, qMax(K, J) = L.
    // The number of record to request R, need to be calculated in the same way,
    // R = qMin(I - L, S) where S is the number of request logs.

    const NodeModel& nodeModel = this->context->getStorage()->getNode(node->getChannel(), node->getPanId(), node->getAddress());
    quint32 storedHistoryIndex = nodeModel.getHistoryLogIndex();                                                                                // J
    quint32 storedEventIndex = nodeModel.getEventLogIndex();                                                                                    // J
    quint32 storedInstantIndex = nodeModel.getInstantLogIndex();                                                                                // J

    qDebug().noquote() << QString("Read index (History: %1, Event: %2, Instant: %3)").arg(QString::number(storedHistoryIndex))
                                                                                     .arg(QString::number(storedEventIndex))
                                                                                     .arg(QString::number(storedInstantIndex));  

    if (node->getInfoModel()->getId() == BmInfoHandler::ID)
    {
        BmInfoModel* infoModel = (BmInfoModel*)node->getInfoModel();

        quint32 headHistoryIndex = infoModel->getHistLogIndex();
        quint32 headEventIndex = infoModel->getEvtLogIndex();
        quint32 headInstantIndex = infoModel->getInstLogIndex();

        quint32 tailHistoryIndex = infoModel->getHistLogIndex() - infoModel->getHistLogInMem();                                                 // K
        quint32 tailEventIndex = infoModel->getEvtLogIndex() - infoModel->getEvtLogInMem();                                                     // K
        quint32 tailInstantIndex = infoModel->getInstLogIndex() - infoModel->getInstLogInMem();                                                 // K

        quint32 currentHistoryIndex = qMax(tailHistoryIndex, storedHistoryIndex);                                                               // L
        quint32 currentEventIndex = qMax(tailEventIndex, storedEventIndex);                                                                     // L
        quint32 currentInstantIndex = qMax(tailInstantIndex, storedInstantIndex);                                                               // L

        quint8 historyRequests = qMin(infoModel->getHistLogIndex() - currentHistoryIndex, (quint32)MAX_HISTORY_LOG_REQUEST);                                          // R, S
        quint8 eventRequests = qMin(infoModel->getEvtLogIndex() - currentEventIndex, (quint32)MAX_EVENT_LOG_REQUEST);                                              // R, S
        quint8 instantRequests = qMin(infoModel->getInstLogIndex() - currentInstantIndex, (quint32)MAX_INSTANT_LOG_REQUEST);                                         // R, S

        qInfo().noquote() << QString("Indexes (C:%1|H:%2|T:%3|S:%4, C:%5|H:%6|T:%7|S:%8, C:%9|H:%10|T:%11|S:%12)").arg(QString::number((currentHistoryIndex <= headHistoryIndex) ? currentHistoryIndex : headHistoryIndex))
                                                                                                                  .arg(QString::number(headHistoryIndex))
                                                                                                                  .arg(QString::number(tailHistoryIndex))
                                                                                                                  .arg(QString::number(storedHistoryIndex))
                                                                                                                  .arg(QString::number((currentEventIndex <= headEventIndex) ? currentEventIndex : headEventIndex))
                                                                                                                  .arg(QString::number(headEventIndex))
                                                                                                                  .arg(QString::number(tailEventIndex))
                                                                                                                  .arg(QString::number(storedEventIndex))
                                                                                                                  .arg(QString::number((currentInstantIndex <= headInstantIndex) ? currentInstantIndex : headInstantIndex))
                                                                                                                  .arg(QString::number(headInstantIndex))
                                                                                                                  .arg(QString::number(tailInstantIndex))
                                                                                                                  .arg(QString::number(storedInstantIndex));

        node->setHistoryRequested(0);
        if (infoModel->getHistLogInMem() > 0 && historyRequests > 0 && currentHistoryIndex < headHistoryIndex)
        {
            qInfo().noquote() << QString("New history log request (Id: %1, Channel: %2, PanId: %3, Address: %4, Index: %5, Quantity: %6, Count: %7/%8)").arg(QString::number(node->getNodeId()))
                                                                                                                                                        .arg(QString::number(node->getChannel()))
                                                                                                                                                        .arg(QString::number(node->getPanId()))
                                                                                                                                                        .arg(QString::number(node->getAddress()))
                                                                                                                                                        .arg(QString::number(currentHistoryIndex + 1))
                                                                                                                                                        .arg(QString::number(historyRequests))
                                                                                                                                                        .arg(QString::number(storedHistoryIndex))
                                                                                                                                                        .arg(QString::number(headHistoryIndex));

            getNodeLog(node->getChannel(), node->getPanId(), node->getAddress(), currentHistoryIndex + 1, historyRequests, LogType::HISTORY);
            node->setHistoryRequested(historyRequests);
        }

        node->setEventRequested(0);
        if (infoModel->getEvtLogInMem() > 0 && eventRequests > 0 && currentEventIndex < headEventIndex)
        {
            qInfo().noquote() << QString("New event log request (Id: %1, Channel: %2, PanId: %3, Address: %4, Index: %5, Quantity: %6, Count: %7/%8)").arg(QString::number(node->getNodeId()))
                                                                                                                                                      .arg(QString::number(node->getChannel()))
                                                                                                                                                      .arg(QString::number(node->getPanId()))
                                                                                                                                                      .arg(QString::number(node->getAddress()))
                                                                                                                                                      .arg(QString::number(currentEventIndex + 1))
                                                                                                                                                      .arg(QString::number(eventRequests))
                                                                                                                                                      .arg(QString::number(storedEventIndex))
                                                                                                                                                      .arg(QString::number(headEventIndex));

            getNodeLog(node->getChannel(), node->getPanId(), node->getAddress(), currentEventIndex + 1, eventRequests, LogType::EVENT);
            node->setEventRequested(eventRequests);
        }

        node->setInstantRequested(0);
        if (infoModel->getInstLogInMem() > 0 && instantRequests > 0 && currentInstantIndex < headInstantIndex)
        {
            qInfo().noquote() << QString("New instant log request (Id: %1, Channel: %2, PanId: %3, Address: %4, Index: %5, Quantity: %6, Count: %7/%8)").arg(QString::number(node->getNodeId()))
                                                                                                                                                        .arg(QString::number(node->getChannel()))
                                                                                                                                                        .arg(QString::number(node->getPanId()))
                                                                                                                                                        .arg(QString::number(node->getAddress()))
                                                                                                                                                        .arg(QString::number(currentInstantIndex + 1))
                                                                                                                                                        .arg(QString::number(instantRequests))
                                                                                                                                                        .arg(QString::number(storedInstantIndex))
                                                                                                                                                        .arg(QString::number(headInstantIndex));

            getNodeLog(node->getChannel(), node->getPanId(), node->getAddress(), currentInstantIndex + 1, instantRequests, LogType::INSTANT);
            node->setInstantRequested(instantRequests);
        }
    }
    else if (node->getInfoModel()->getId() == CmInfoHandler::ID)
    {
        CmInfoModel* infoModel = (CmInfoModel*)node->getInfoModel();

        quint32 headHistoryIndex = infoModel->getHistLogIndex();
        quint32 headEventIndex = infoModel->getEvtLogIndex();
        quint32 headInstantIndex = infoModel->getInstLogIndex();

        quint32 tailHistoryIndex = infoModel->getHistLogIndex() - infoModel->getHistLogInMem();                                                 // K
        quint32 tailEventIndex = infoModel->getEvtLogIndex() - infoModel->getEvtLogInMem();                                                     // K
        quint32 tailInstantIndex = infoModel->getInstLogIndex() - infoModel->getInstLogInMem();                                                 // K

        quint32 currentHistoryIndex = qMax(tailHistoryIndex, storedHistoryIndex);                                                               // L
        quint32 currentEventIndex = qMax(tailEventIndex, storedEventIndex);                                                                     // L
        quint32 currentInstantIndex = qMax(tailInstantIndex, storedInstantIndex);                                                               // L

        quint8 historyRequests = qMin(infoModel->getHistLogIndex() - currentHistoryIndex, (quint32)1);                                          // R, S
        quint8 eventRequests = qMin(infoModel->getEvtLogIndex() - currentEventIndex, (quint32)20);                                              // R, S
        quint8 instantRequests = qMin(infoModel->getInstLogIndex() - currentInstantIndex, (quint32)50);                                         // R, S

        qDebug().noquote() << QString("Indexes: C:%1|H:%2|T:%3|S:%4, C:%5|H:%6|T:%7|S:%8, C:%9|H:%10|T:%11|S:%12").arg(QString::number(currentHistoryIndex))
                                                                                                                  .arg(QString::number(headHistoryIndex))
                                                                                                                  .arg(QString::number(tailHistoryIndex))
                                                                                                                  .arg(QString::number(storedHistoryIndex))
                                                                                                                  .arg(QString::number(currentEventIndex))
                                                                                                                  .arg(QString::number(headEventIndex))
                                                                                                                  .arg(QString::number(tailEventIndex))
                                                                                                                  .arg(QString::number(storedEventIndex))
                                                                                                                  .arg(QString::number(currentInstantIndex))
                                                                                                                  .arg(QString::number(headInstantIndex))
                                                                                                                  .arg(QString::number(tailInstantIndex))
                                                                                                                  .arg(QString::number(storedInstantIndex));

        node->setHistoryRequested(0);
        if (infoModel->getHistLogInMem() > 0 && historyRequests > 0 && currentHistoryIndex < headHistoryIndex)
        {
            qInfo().noquote() << QString("New history log request (Id: %1, Channel: %2, PanId: %3, Address: %4, Index: %5, Quantity: %6, Count: %7/%8)").arg(QString::number(node->getNodeId()))
                                                                                                                                                        .arg(QString::number(node->getChannel()))
                                                                                                                                                        .arg(QString::number(node->getPanId()))
                                                                                                                                                        .arg(QString::number(node->getAddress()))
                                                                                                                                                        .arg(QString::number(currentHistoryIndex + 1))
                                                                                                                                                        .arg(QString::number(historyRequests))
                                                                                                                                                        .arg(QString::number(storedHistoryIndex))
                                                                                                                                                        .arg(QString::number(headHistoryIndex));

            getNodeLog(node->getChannel(), node->getPanId(), node->getAddress(), currentHistoryIndex + 1, historyRequests, LogType::HISTORY);
            node->setHistoryRequested(historyRequests);
        }

        node->setEventRequested(0);
        if (infoModel->getEvtLogInMem() > 0 && eventRequests > 0 && currentEventIndex < headEventIndex)
        {
            qInfo().noquote() << QString("New event log request (Id: %1, Channel: %2, PanId: %3, Address: %4, Index: %5, Quantity: %6, Count: %7/%8)").arg(QString::number(node->getNodeId()))
                                                                                                                                                      .arg(QString::number(node->getChannel()))
                                                                                                                                                      .arg(QString::number(node->getPanId()))
                                                                                                                                                      .arg(QString::number(node->getAddress()))
                                                                                                                                                      .arg(QString::number(currentEventIndex + 1))
                                                                                                                                                      .arg(QString::number(eventRequests))
                                                                                                                                                      .arg(QString::number(storedEventIndex))
                                                                                                                                                      .arg(QString::number(headEventIndex));

            getNodeLog(node->getChannel(), node->getPanId(), node->getAddress(), currentEventIndex + 1, eventRequests, LogType::EVENT);
            node->setEventRequested(eventRequests);
        }

        node->setInstantRequested(0);
        if (infoModel->getInstLogInMem() > 0 && instantRequests > 0 && currentInstantIndex < headInstantIndex)
        {
            qInfo().noquote() << QString("New instant log request (Id: %1, Channel: %2, PanId: %3, Address: %4, Index: %5, Quantity: %6, Count: %7/%8)").arg(QString::number(node->getNodeId()))
                                                                                                                                                        .arg(QString::number(node->getChannel()))
                                                                                                                                                        .arg(QString::number(node->getPanId()))
                                                                                                                                                        .arg(QString::number(node->getAddress()))
                                                                                                                                                        .arg(QString::number(currentInstantIndex + 1))
                                                                                                                                                        .arg(QString::number(instantRequests))
                                                                                                                                                        .arg(QString::number(storedInstantIndex))
                                                                                                                                                        .arg(QString::number(headInstantIndex));

            getNodeLog(node->getChannel(), node->getPanId(), node->getAddress(), currentInstantIndex + 1, instantRequests, LogType::INSTANT);
            node->setInstantRequested(instantRequests);
        }
    }
}

/**
 * @brief NodeWorker::getNodeLog Get node log
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param index The index
 * @param number The number
 * @param type The log type (0 = History, 1 = Event, 2 = Instant)
 */
void NodeWorker::getNodeLog(const quint8 channel, const quint16 panId, const quint16 address, const quint32 index, const quint8 number, const quint8 type)
{
    GetLogRequest* request = new GetLogRequest(channel, panId, address, index, number, type);
    this->device->send(request);
}

/**
 * @brief NodeWorker::addNodeInfo Add node info
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::addNodeInfo(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->setInvalidIndex(false);
    node->setInfoModel(model);

    RequestPackage* devicePingRequest = node->makeDevicePingRequest();
    if (devicePingRequest != nullptr)
    {
        this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(devicePingRequest->getEndpoint(), devicePingRequest->getPayload()));
        delete devicePingRequest;
    }

    quint64 mui = 0;
    quint32 nodeId = 0;
    quint8 productTypeValue = 0;
    if (model->getId() == BmInfoHandler::ID)
    {
        productTypeValue = ProductType::BMU;

        mui = ((BmInfoModel*)model)->getMUI();
        nodeId = ((BmInfoModel*)model)->getBId();
    }
    else if (model->getId() == CmInfoHandler::ID)
    {
        productTypeValue = ProductType::CHARGER;

        mui = ((CmInfoModel*)model)->getMUI();
        nodeId = ((CmInfoModel*)model)->getCId();
    }

    const NodeModel& nodeModel = this->context->getStorage()->getNode(node->getChannel(), node->getPanId(), node->getAddress());
    if (nodeModel.getNodeId() != nodeId || nodeModel.getProductTypeValue() != productTypeValue)
    {
        node->updateNodeId(nodeId);
        node->updateProductType(productTypeValue);

        this->context->getStorage()->updateNodeId(node->getChannel(), node->getPanId(), node->getAddress(), nodeId);
        this->context->getStorage()->updateProductType(node->getChannel(), node->getPanId(), node->getAddress(), productTypeValue);

        this->context->getEventManager()->emitNodeUpdatedEvent(NodeUpdatedEvent(nodeModel.getId(), nodeId, node->getChannel(), node->getPanId(), node->getAddress(),
                                                                                nodeModel.getHistoryLogIndex(), nodeModel.getEventLogIndex(),
                                                                                nodeModel.getInstantLogIndex(), nodeModel.getTimestamp(), productTypeValue));

        qInfo().noquote() << QString("Updated node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6)").arg(QString::number(nodeId))
                                                                                                                     .arg(QString::number(node->getChannel()))
                                                                                                                     .arg(QString::number(node->getPanId()))
                                                                                                                     .arg(QString::number(node->getAddress()))
                                                                                                                     .arg(translateProductType(productTypeValue))
                                                                                                                     .arg(QString::number(node->getMUI()));
    }

    qInfo().noquote() << QString("Query indexes for node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6)").arg(QString::number(nodeId))
                                                                                                                           .arg(QString::number(node->getChannel()))
                                                                                                                           .arg(QString::number(node->getPanId()))
                                                                                                                           .arg(QString::number(node->getAddress()))
                                                                                                                           .arg(translateProductType(node->getProductType()))
                                                                                                                           .arg(QString::number(node->getMUI()));

    QJsonObject payload;
    payload.insert("channel", QString::number(node->getChannel()));
    payload.insert("panId", QString::number(node->getPanId()));
    payload.insert("address", QString::number(node->getAddress()));

    // Request the latest indexes.
    this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(QString("/api/v1/devices/%1/logs/history/indexes").arg(mui), payload));
    this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(QString("/api/v1/devices/%1/logs/event/indexes").arg(mui), payload));
    this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(QString("/api/v1/devices/%1/logs/instant/indexes").arg(mui), payload));
}

/**
 * @brief NodeWorker::setNodeSync Set node sync
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 * @param nodeId The node id
 * @param productType The product type
 */
void NodeWorker::setNodeSync(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId, const quint8 productType)
{
    SyncNodeRequest* request = new SyncNodeRequest(channel, panId, address, nodeId, productType);
    this->device->send(request);
}

/**
 * @brief NodeWorker::syncNode Synchronize node
 * @param event The event
 */
void NodeWorker::syncNode(const SyncNodeEvent& event)
{
    QString key = UniqueKey::makeKey(event.getChannel(), event.getPanId(), event.getAddress());
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];

    qInfo().noquote() << QString("Synchronizing node (Id: %1, Channel: %2, PanId: %3, Address: %4, Type: %5, MUI: %6)").arg(QString::number(node->getNodeId()))
                                                                                                                       .arg(QString::number(node->getChannel()))
                                                                                                                       .arg(QString::number(node->getPanId()))
                                                                                                                       .arg(QString::number(node->getAddress()))
                                                                                                                       .arg(translateProductType(node->getProductType()))
                                                                                                                       .arg(QString::number(node->getMUI()));

    if (this->blacklistedNodes.contains(key))
    {
        qWarning().noquote() << "Detected blacklisted node, aborting synchronizing node";
        node->clearLogsAndIndexes();

        return;
    }

    // Is index valid?
    if (node->isInvalidIndex())
    {
        qWarning().noquote() << "Detected invalid index, aborting synchronizing node";
        node->clearLogsAndIndexes();

        return;
    }

    // Do we have all models we depends on?
    if (!node->isStatusReady())
    {
        qWarning().noquote() << "Detected missing data, aborting synchronizing node";
        node->clearLogsAndIndexes();

        return;
    }

    // Postpone collect data.
    this->collectDataTimer->start();
    qInfo().noquote() << QString("Postponed collect data (Scheduled: %1)").arg(QDateTime::currentDateTimeUtc().addMSecs(this->collectDataTimer->interval()).toString("yyyy-MM-ddThh:mm:ss"));

    const NodeModel& model = this->context->getStorage()->getNode(node->getChannel(), node->getPanId(), node->getAddress());
    quint32 storedHistoryIndex = model.getHistoryLogIndex();
    quint32 storedEventIndex = model.getEventLogIndex();
    quint32 storedInstantIndex = model.getInstantLogIndex();

    quint32 headHistoryIndex = 0, tailHistoryIndex = 0;
    quint32 headEventIndex = 0, tailEventIndex = 0;
    quint32 headInstantIndex = 0, tailInstantIndex = 0;
    if (node->getInfoModel()->getId() == BmInfoHandler::ID)
    {
        BmInfoModel* infoModel = (BmInfoModel*)node->getInfoModel();

        headHistoryIndex = infoModel->getHistLogIndex();
        headEventIndex = infoModel->getEvtLogIndex();
        headInstantIndex = infoModel->getInstLogIndex();

        tailHistoryIndex = infoModel->getHistLogIndex() - infoModel->getHistLogInMem();
        tailEventIndex = infoModel->getEvtLogIndex() - infoModel->getEvtLogInMem();
        tailInstantIndex = infoModel->getInstLogIndex() - infoModel->getInstLogInMem();
    }
    else if (node->getInfoModel()->getId() == CmInfoHandler::ID)
    {
        CmInfoModel* infoModel = (CmInfoModel*)node->getInfoModel();

        headHistoryIndex = infoModel->getHistLogIndex();
        headEventIndex = infoModel->getEvtLogIndex();
        headInstantIndex = infoModel->getInstLogIndex();

        tailHistoryIndex = infoModel->getHistLogIndex() - infoModel->getHistLogInMem();
        tailEventIndex = infoModel->getEvtLogIndex() - infoModel->getEvtLogInMem();
        tailInstantIndex = infoModel->getInstLogIndex() - infoModel->getInstLogInMem();
    }

    quint32 currentHistoryIndex = qMax(tailHistoryIndex, storedHistoryIndex) + node->getHistoryRequested();
    quint32 currentEventIndex = qMax(tailEventIndex, storedEventIndex) + node->getEventRequested();
    quint32 currentInstantIndex = qMax(tailInstantIndex, storedInstantIndex) + node->getInstantRequested();

    RequestPackage* historyLogRequest = node->makeHistoryLogRequest();
    if (historyLogRequest != nullptr)
    {
        this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(historyLogRequest->getEndpoint(), historyLogRequest->getPayload()));
        qInfo().noquote() << QString("Sent history data to the cloud (%1 bytes)").arg(QJsonDocument(historyLogRequest->getPayload()).toJson().size());
        delete historyLogRequest;

        if (currentHistoryIndex > storedHistoryIndex && node->getHistoryRequested() > 0)
        {
            this->context->getStorage()->updateNodeLogIndex(node->getChannel(), node->getPanId(), node->getAddress(), currentHistoryIndex, LogType::HISTORY);
            storedHistoryIndex = currentHistoryIndex;

            qInfo().noquote() << QString("Stored new history index (%1)").arg(currentHistoryIndex);
        }
    }

    RequestPackage* eventLogRequest = node->makeEventLogRequest();
    if (eventLogRequest != nullptr)
    {
        this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(eventLogRequest->getEndpoint(), eventLogRequest->getPayload()));
        qInfo().noquote() << QString("Sent event data to the cloud (%1 bytes)").arg(QJsonDocument(eventLogRequest->getPayload()).toJson().size());
        delete eventLogRequest;

        if (currentEventIndex > storedEventIndex && node->getEventRequested() > 0)
        {
            this->context->getStorage()->updateNodeLogIndex(node->getChannel(), node->getPanId(), node->getAddress(), currentEventIndex, LogType::EVENT);
            storedEventIndex = currentEventIndex;

            qInfo().noquote() << QString("Stored new event index (%1)").arg(storedEventIndex);
        }
    }

    RequestPackage* instantLogRequest = node->makeInstantLogRequest();
    if (instantLogRequest != nullptr)
    {
        this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(instantLogRequest->getEndpoint(), instantLogRequest->getPayload()));
        qInfo().noquote() << QString("Sent instant data to the cloud (%1 bytes)").arg(QJsonDocument(instantLogRequest->getPayload()).toJson().size());
        delete instantLogRequest;

        if (currentInstantIndex > storedInstantIndex && node->getInstantRequested() > 0)
        {
            this->context->getStorage()->updateNodeLogIndex(node->getChannel(), node->getPanId(), node->getAddress(), currentInstantIndex, LogType::INSTANT);
            storedInstantIndex = currentInstantIndex;

            qInfo().noquote() << QString("Stored new instant index (%1)").arg(storedInstantIndex);
        }
    }

    qInfo().noquote() << QString("Indexes (C:%1|H:%2|T:%3|S:%4, C:%5|H:%6|T:%7|S:%8, C:%9|H:%10|T:%11|S:%12)").arg(QString::number(currentHistoryIndex))
                                                                                                              .arg(QString::number(headHistoryIndex))
                                                                                                              .arg(QString::number(tailHistoryIndex))
                                                                                                              .arg(QString::number(storedHistoryIndex))
                                                                                                              .arg(QString::number(currentEventIndex))
                                                                                                              .arg(QString::number(headEventIndex))
                                                                                                              .arg(QString::number(tailEventIndex))
                                                                                                              .arg(QString::number(storedEventIndex))
                                                                                                              .arg(QString::number(currentInstantIndex))
                                                                                                              .arg(QString::number(headInstantIndex))
                                                                                                              .arg(QString::number(tailInstantIndex))
                                                                                                              .arg(QString::number(storedInstantIndex));

    // Should we read more log chunks?
    if (currentHistoryIndex < headHistoryIndex || currentEventIndex < headEventIndex || currentInstantIndex < headInstantIndex)
    {
        getNodeLogs(node->getChannel(), node->getPanId(), node->getAddress());
        setNodeSync(node->getChannel(), node->getPanId(), node->getAddress(), node->getNodeId(), node->getProductType());
    }

    node->clearLogsAndIndexes();
}

/**
 * @brief NodeWorker::addNodeMeas Add node meas
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::addNodeMeas(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->setMeasModel(model);
}

/**
 * @brief NodeWorker::addNodeConfig Add node config
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::addNodeConfig(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->setConfigModel(model);

    if (!node->isConfigReady())
        return;

    RequestPackage* package = node->makeConfigRequest();
    if (package != nullptr)
    {
        this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(package->getEndpoint(), package->getPayload()));
        delete package;
    }
}

/**
 * @brief NodeWorker::addNodeStatus Add node status
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::addNodeStatus(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->setStatusModel(model);

    if (!node->isStatusReady())
        return;

    RequestPackage* package = node->makeStatusRequest();
    if (package != nullptr)
    {
        this->context->getEventManager()->emitTransmitRequestEvent(RequestEvent(package->getEndpoint(), package->getPayload()));
        delete package;
    }
}

/**
 * @brief NodeWorker::addNodeHistoryLog Add node config
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::addNodeHistoryLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->addHistoryLogModel(model);
}

/**
 * @brief NodeWorker::addNodeEventLog Add node config
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::addNodeEventLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->addEventLogModel(model);
}

/**
 * @brief NodeWorker::addNodeInstantLog Add node config
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::addNodeInstantLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    QString key = UniqueKey::makeKey(channel, panId, address);
    if (!this->nodes.contains(key))
        return;

    Node* node = this->nodes[key];
    node->addInstantLogModel(model);
}

/**
 * @brief NodeWorker::updateParam Update parameters
 * @param channel The channel
 * @param panId The pan id
 * @param address The node address
 * @param model The model
 */
void NodeWorker::updateParam(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);
    Q_UNUSED(address);

    SetParamModel* setParamModel = (SetParamModel*)model;
    this->context->getEventManager()->emitConfigurationReceivedEvent(ConfigurationEvent(setParamModel->getIndex(), setParamModel->getData()));
}

/**
 * @brief NodeWorker::doFactoryReset Factory reset handler
 */
void NodeWorker::doFactoryReset()
{
    setupWorker();
}
