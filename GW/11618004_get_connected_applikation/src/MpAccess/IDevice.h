#pragma once

#include "Shared.h"
#include "Command/Request/CommandRequest.h"
#include "Command/Handler/CommandHandlerFactory.h"

#include <QtCore/QObject>

class MPACCESS_EXPORT IDevice : public QObject
{
    public:
        virtual void reset() = 0;
        virtual bool isOpen() = 0;
        virtual int sendQueueCount() = 0;
        virtual bool isSendQueueEmpty() = 0;
        virtual void set(CommandRequest* command) = 0;
        virtual void send(CommandRequest* command) = 0;
        virtual void setFirmwareAddress(const quint16 address) = 0;
        virtual void setCommandFactory(CommandHandlerFactory* factory) = 0;

    protected:
        IDevice(QObject* parent = nullptr) : QObject(parent) {}
};
