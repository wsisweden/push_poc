#pragma once

#include "Shared.h"
#include "Message/Model/IMpModel.h"

#include <QtCore/QObject>

class MPACCESS_EXPORT IReciever : public QObject
{
    public:
        virtual void setRadioAddress(const quint8 channel, const quint16 panId, const quint16 address, const quint32 firmwareType, const quint32 firmwareVersion) = 0;
        virtual void addNetwork(const quint8 channel, const quint16 panId) = 0;
        virtual void addNode(const quint32 id, const quint8 channel, const quint16 panId,const quint16 address, const quint8 type) = 0;
        virtual void addNodeInfo(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;
        virtual void addNodeMeas(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;
        virtual void addNodeStatus(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;
        virtual void addNodeConfig(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;
        virtual void addNodeHistoryLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;
        virtual void addNodeEventLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;
        virtual void addNodeInstantLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;
        virtual void updateParam(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) = 0;

    protected:
        IReciever(QObject* parent = nullptr) : QObject(parent) {}
};
