TEMPLATE = subdirs

SUBDIRS += \
    Common \
    Storage \
    Cloud \
    Config \
    MpAccess \
    Gui \
    Shell

Storage.depends = Common
Cloud.depends = Common Storage
Config.depends = Common Storage
MpAccess.depends = Common Storage
Gui.depends = Common MpAccess
Shell.depends = Common Storage Cloud Config MpAccess Gui
