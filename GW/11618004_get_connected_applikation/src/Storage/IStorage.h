#pragma once

#include "Shared.h"
#include "Model/NodeModel.h"
#include "Model/LogTypeModel.h"
#include "Model/ProductTypeModel.h"
#include "Model/ConfigurationModel.h"
#include "Model/StatisticModel.h"

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QVariant>

class STORAGE_EXPORT IStorage: public QObject
{
    public:
        virtual void factoryReset() = 0;

        virtual ConfigurationModel getConfigurationByName(const QString& name) = 0;
        virtual void updateConfiguration(const ConfigurationModel& model) = 0;

        virtual QMap<int, LogTypeModel> getLogType() = 0;
        virtual LogTypeModel getLogTypeByName(const QString& name) = 0;

        virtual QMap<int, ProductTypeModel> getProductType() = 0;
        virtual ProductTypeModel getProductTypeByName(const QString& name) = 0;
        virtual ProductTypeModel getProductTypeByValue(const quint8 value) = 0;

        virtual QMap<QString, NodeModel> getNodes() = 0;
        virtual NodeModel getNode(const quint8 channel, const quint16 panId, const quint16 address) = 0;
        virtual void insertNode(const NodeModel& model) = 0;
        virtual void updateNodeId(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId) = 0;
        virtual void updateProductType(const quint8 channel, const quint16 panId, const quint16 address, const quint8 productTypeValue) = 0;
        virtual void updateNodeTimestamp(const quint8 channel, const quint16 panId, const quint16 address, const QString& timestamp) = 0;
        virtual void updateNodeLogIndex(const quint8 channel, const quint16 panId, const quint16 address, const quint32 index, const quint8 type) = 0;
        virtual void deleteNodes(const QList<QVariant>& list) = 0;
        virtual void resetNodeIndexes() = 0;

    protected:
        IStorage(QObject* parent = NULL) : QObject(parent) {}
};
