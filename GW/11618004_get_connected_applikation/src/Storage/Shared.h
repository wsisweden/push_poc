#pragma once

#include <QtCore/QtGlobal>

#if defined(STORAGE_LIBRARY)
    #define STORAGE_EXPORT //Q_DECL_EXPORT
#else
    #define STORAGE_EXPORT //Q_DECL_IMPORT
#endif
