CREATE TABLE Configuration (Id INTEGER PRIMARY KEY, Name TEXT, DefaultValue TEXT, Value TEXT);
CREATE TABLE ProductType (Id INTEGER PRIMARY KEY, Name TEXT, Value INTEGER);
CREATE TABLE Node (Id INTEGER PRIMARY KEY, NodeId INTEGER, Channel INTEGER, PanId INTEGER, Address INTEGER, HistoryLogIndex INTEGER, EventLogIndex INTEGER, InstantLogIndex INTEGER, ProductTypeId INTEGER, Timestamp TEXT);
CREATE TABLE LogType (Id INTEGER PRIMARY KEY, Name TEXT, Value INTEGER);

INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('DatabaseVersion', '0', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('ListenerPort', '8080', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('Name', 'Get Connected', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('CloudUrl', '', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('CloudUsername', '', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('CloudPassword', '', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('CloudPingInterval', '30000', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('EthernetLinkInterval', '1000', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('HeartbeatInterval', '1000', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('ScanNetworkInterval', '3600000', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('CollectDataInterval', '300000', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('PeriodicUpdateInterval', '10000', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('RemoveNodeInterval', '86400000', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('DeviceId', '', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('SerialNumber', '', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('SecurityCode', '3562', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('FactoryReset', 'true', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('SecureShell', 'true', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('DebugLog', 'false', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('DebugMpaLog', 'false', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('Node', '', '');
INSERT INTO Configuration (Name, DefaultValue, Value) VALUES ('Network', '', '');

INSERT INTO ProductType (Name, Value) VALUES ('Unknown', 0);
INSERT INTO ProductType (Name, Value) VALUES ('BMU', 1);
INSERT INTO ProductType (Name, Value) VALUES ('Vehicle', 2);
INSERT INTO ProductType (Name, Value) VALUES ('Charger', 3);
INSERT INTO ProductType (Name, Value) VALUES ('PC', 4);
INSERT INTO ProductType (Name, Value) VALUES ('Unknown', 255);

INSERT INTO LogType (Name, Value) VALUES ('History', 0);
INSERT INTO LogType (Name, Value) VALUES ('Event', 1);
INSERT INTO LogType (Name, Value) VALUES ('Instant', 2);
