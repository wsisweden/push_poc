#pragma once

#include "Shared.h"
#include "IStorage.h"
#include "Model/NodeModel.h"
#include "Model/LogTypeModel.h"
#include "Model/ProductTypeModel.h"
#include "Model/ConfigurationModel.h"
#include "Model/StatisticModel.h"

#include <QtCore/QMap>
#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QVariant>

class STORAGE_EXPORT Database : public IStorage
{
    Q_OBJECT

    public:
        explicit Database(QObject* parent = nullptr);
        ~Database();

        void factoryReset();

        ConfigurationModel getConfigurationByName(const QString& name);
        void updateConfiguration(const ConfigurationModel& model);

        QMap<int, LogTypeModel> getLogType();
        LogTypeModel getLogTypeByName(const QString& name);

        QMap<int, ProductTypeModel> getProductType();
        ProductTypeModel getProductTypeByName(const QString& name);
        ProductTypeModel getProductTypeByValue(const quint8 value);

        QMap<QString, NodeModel> getNodes();
        NodeModel getNode(const quint8 channel, const quint16 panId, const quint16 address);
        void insertNode(const NodeModel& model);
        void updateNodeId(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId);
        void updateProductType(const quint8 channel, const quint16 panId, const quint16 address, const quint8 productTypeValue);
        void updateNodeTimestamp(const quint8 channel, const quint16 panId, const quint16 address, const QString& timestamp);
        void updateNodeLogIndex(const quint8 channel, const quint16 panId, const quint16 address, const quint32 index, const quint8 type);
        void deleteNodes(const QList<QVariant>& list);
        void resetNodeIndexes();

    private:
        QMutex* mutex = nullptr;

        void createDatabase();
        void upgradeDatabase();
        QString makeUniqeKey(const quint8 channel, const quint16 panId, const quint16 address);
};
