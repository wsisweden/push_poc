#include "StatisticModel.h"

/**
 * @brief StatisticModel::StatisticModel Construct a new statistic model
 * @param id The id
 * @param name The name
 * @param value The value
 */
StatisticModel::StatisticModel(const int id, const QString& name, const QString& value)
{
    this->id = id;
    this->name = name;
    this->value = value;
}

/**
 * @brief StatisticModel::setValue Set the configuration value
 * @param value The value
 */
void StatisticModel::setValue(const QString& value)
{
    if (this->value == value)
        return;

    this->value = value;
}
