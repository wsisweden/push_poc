#include "LogTypeModel.h"

/**
 * @brief LogTypeModel::LogTypeModel Construct a new log type model
 * @param id The id
 * @param name The name
 * @param value The value
 */
LogTypeModel::LogTypeModel(const int id, const QString& name, const quint8 value)
{
    this->id = id;
    this->name = name;
    this->value = value;
}
