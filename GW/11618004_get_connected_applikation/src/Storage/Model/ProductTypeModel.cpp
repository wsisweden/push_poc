#include "ProductTypeModel.h"

/**
 * @brief ProductTypeModel::ProductTypeModel Construct a new product type model
 * @param id The id
 * @param name The name
 * @param value The value
 */
ProductTypeModel::ProductTypeModel(const int id, const QString& name, const quint8 value)
{
    this->id = id;
    this->name = name;
    this->value = value;
}
