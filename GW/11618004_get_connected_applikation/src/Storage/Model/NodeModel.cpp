#include "NodeModel.h"

/**
 * @brief NodeModel::NodeModel Construct a new node model
 * @param id The id
 * @param nodeId The node id
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 * @param historyLogIndex The history log index
 * @param eventLogIndex The event log index
 * @param instantLogIndex The instant log index
 * @param timestamp The timestamp
 * @param productTypeValue The product type value
 */
NodeModel::NodeModel(const int id, const quint32 nodeId, const quint8 channel, const quint16 panId,
                     const quint16 address, const quint32 historyLogIndex, const quint32 eventLogIndex,
                     const quint32 instantLogIndex, const QString& timestamp, const quint8 productTypeValue)
{
    this->id = id;
    this->nodeId = nodeId;
    this->channel = channel;
    this->panId = panId;
    this->address = address;
    this->historyLogIndex = historyLogIndex;
    this->eventLogIndex = eventLogIndex;
    this->instantLogIndex = instantLogIndex;
    this->timestamp = timestamp;
    this->productTypeValue = productTypeValue;
}
