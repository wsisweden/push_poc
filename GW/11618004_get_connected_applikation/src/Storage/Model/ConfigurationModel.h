#pragma once

#include "../Shared.h"

#include <QtCore/QString>

class STORAGE_EXPORT ConfigurationModel
{
    public:
        explicit ConfigurationModel() {}
        explicit ConfigurationModel(const int id, const QString& name, const QString& value);

        void setValue(const QString& value);

        int getId() const { return this->id; }
        const QString& getName() const { return this->name; }
        const QString& getValue() const { return this->value; }

    private:
        int id;
        QString name;
        QString value;
};
