#pragma once

#include "../Shared.h"

#include <QtCore/QString>

class STORAGE_EXPORT NodeModel
{
    public:
        explicit NodeModel() {}
        explicit NodeModel(const int id, const quint32 nodeId, const quint8 channel, const quint16 panId,
                           const quint16 address, const quint32 historyLogIndex, const quint32 eventLogIndex,
                           const quint32 instantLogIndex, const QString& timestamp, const quint8 productTypeValue);

        int getId() const { return this->id; }
        quint8 getChannel() const { return this->channel; }
        quint8 getProductTypeValue() const { return this->productTypeValue; }
        quint16 getPanId() const { return this->panId; }
        quint16 getAddress() const { return this->address; }
        quint32 getNodeId() const { return this->nodeId; }
        quint32 getHistoryLogIndex() const { return this->historyLogIndex; }
        quint32 getEventLogIndex() const { return this->eventLogIndex; }
        quint32 getInstantLogIndex() const { return this->instantLogIndex; }
        QString getTimestamp() const { return this->timestamp; }

    private:
        int id;
        quint8 channel;
        quint8 productTypeValue;
        quint16 panId;
        quint16 address;
        quint32 nodeId;
        quint32 historyLogIndex;
        quint32 eventLogIndex;
        quint32 instantLogIndex;
        QString timestamp;
};
