#include "ConfigurationModel.h"

/**
 * @brief ConfigurationModel::ConfigurationModel Construct a new configuration model
 * @param id The id
 * @param name The name
 * @param value The value
 */
ConfigurationModel::ConfigurationModel(const int id, const QString& name, const QString& value)
{
    this->id = id;
    this->name = name;
    this->value = value;
}

/**
 * @brief ConfigurationModel::setValue Set the configuration value
 * @param value The value
 */
void ConfigurationModel::setValue(const QString& value)
{
    if (this->value == value)
        return;

    this->value = value;
}
