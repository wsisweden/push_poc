#pragma once

#include "../Shared.h"

#include <QtCore/QString>

class STORAGE_EXPORT LogTypeModel
{
    public:
        explicit LogTypeModel() {}
        explicit LogTypeModel(const int id, const QString& name, const quint8 value);

        int getId() const { return this->id; }
        quint8 getValue() const { return this->value; }
        const QString& getName() const { return this->name; }

    private:
        int id;
        quint8 value;
        QString name;
};
