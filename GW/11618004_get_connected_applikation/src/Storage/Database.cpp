#include "Database.h"

#include "../Common/Version.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QMutexLocker>

#include <QtSql/QSqlDriver>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

Database::Database(QObject* parent)
    : IStorage(parent)
{
    this->mutex = new QMutex(QMutex::Recursive);

    if (QSqlDatabase::database().tables().count() == 0)
        createDatabase();
    else
        upgradeDatabase();
}

Database::~Database()
{
    qInfo().noquote() << "Tearing down database";

    if (this->mutex != nullptr)
        delete this->mutex;
}

/**
 * @brief Database::createDatabase Create the databse based on the schema.
 */
void Database::createDatabase()
{
    QFile file(":/Sql/Schema.sql");
    if (file.open(QFile::ReadOnly))
    {
        QStringList queries = QString(file.readAll()).split(";");

        file.close();

        QSqlQuery sql;
        for (QString query : queries)
        {
            if (query.trimmed().isEmpty())
                continue;

            if (!sql.exec(query))
                qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());
        }

        sql.prepare("UPDATE Configuration SET Value = :Value "
                    "WHERE Name = 'DatabaseVersion'");

        sql.bindValue(":Value", DATABASE_VERSION);

        if (!sql.exec())
            qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

        qInfo().noquote() << "Successfully created database schema";
    }
}

/**
 * @brief Database::upgradeDatabase Upgrade the database and apply changeset accordingly
 */
void Database::upgradeDatabase()
{
    QSqlQuery sql;
    if (!sql.exec("SELECT c.Id, c.Name, c.Value FROM Configuration c WHERE c.Name = 'DatabaseVersion'"))
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    sql.first();

    int version = sql.value(2).toInt();
    while (version + 1 <= QString("%1").arg(DATABASE_VERSION).toInt())
    {
        QFile file(QString(":/Sql/ChangeSet-%1.sql").arg(version + 1));
        if (file.open(QFile::ReadOnly))
        {
            QStringList queries = QString(file.readAll()).split(";");

            file.close();

            for (const QString& query : queries)
            {
                 if (query.trimmed().isEmpty())
                     continue;

                 if (!sql.exec(query))
                    qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());
            }

            sql.prepare("UPDATE Configuration SET Value = :Value "
                        "WHERE Name = 'DatabaseVersion'");

            sql.bindValue(":Value", version + 1);

            if (!sql.exec())
                qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

            qInfo().noquote() << QString("Successfully upgraded database to ChangeSet-%1").arg(version + 1);
        }

        version++;
    }
}

/**
 * @brief Database::factoryReset Factory reset
 */
void Database::factoryReset()
{
    QMutexLocker locker(this->mutex);

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("UPDATE Configuration SET Value = :Value "
                "WHERE Name != 'SerialNumber'");

    sql.bindValue(":Value", "");

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    if (!sql.exec("DELETE FROM Node"))
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::getConfigurationByName Get configuration model by name
 * @param name Configuration name
 * @return Configuration model
 */
ConfigurationModel Database::getConfigurationByName(const QString& name)
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT c.Id, c.Name, c.DefaultValue, c.Value FROM Configuration c "
                "WHERE c.Name = :Name");

    sql.bindValue(":Name", name);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    if (sql.first())
        return ConfigurationModel(sql.value(0).toInt(), sql.value(1).toString(), (sql.value(3).toString().isEmpty() ?
                                                                                  sql.value(2).toString() :
                                                                                  sql.value(3).toString()));

    return ConfigurationModel(0, "", "");
}

/**
 * @brief Database::updateConfiguration Update configuration
 * @param model Configuration model
 */
void Database::updateConfiguration(const ConfigurationModel& model)
{
    QMutexLocker locker(this->mutex);

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("UPDATE Configuration SET Value = :Value "
                "WHERE Name = :Name");

    sql.bindValue(":Value", model.getValue());
    sql.bindValue(":Name", model.getName());

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::getLogType Get log type models
 * @return Log type models
 */
QMap<int, LogTypeModel> Database::getLogType()
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT l.Id, l.Name, l.Value FROM LogType l");

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QMap<int, LogTypeModel> models;
    while (sql.next())
        models.insert(sql.value(0).toInt(), LogTypeModel(sql.value(0).toInt(), sql.value(1).toString(), sql.value(2).toInt()));

    return models;
}

/**
 * @brief Database::getLogTypeByName Get log type model by name
 * @param name Log type name
 * @return Log type model
 */
LogTypeModel Database::getLogTypeByName(const QString& name)
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT l.Id, l.Name, l.Value FROM LogType l "
                "WHERE l.Name = :Name");

    sql.bindValue(":Name", name);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    if (sql.first())
        return LogTypeModel(sql.value(0).toInt(), sql.value(1).toString(), sql.value(2).toInt());

    return LogTypeModel(0, "", 0);
}

/**
 * @brief Database::getProductType Get product type models
 * @return Product type models
 */
QMap<int, ProductTypeModel> Database::getProductType()
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT t.Id, t.Name, t.Value FROM ProductType t");

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QMap<int, ProductTypeModel> models;
    while (sql.next())
        models.insert(sql.value(0).toInt(), ProductTypeModel(sql.value(0).toInt(), sql.value(1).toString(), sql.value(2).toInt()));

    return models;
}

/**
 * @brief Database::getProductTypeByName Get product type model by name
 * @param name Product type name
 * @return Node type model
 */
ProductTypeModel Database::getProductTypeByName(const QString& name)
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT t.Id, t.Name, t.Value FROM ProductType t "
                "WHERE t.Name = :Name");

    sql.bindValue(":Name", name);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    if (sql.first())
        return ProductTypeModel(sql.value(0).toInt(), sql.value(1).toString(), sql.value(2).toUInt());

    return ProductTypeModel(0, "", 0);
}

/**
 * @brief Database::getProductTypeByValue Get product type model by value
 * @param name Product type value
 * @return Product type model
 */
ProductTypeModel Database::getProductTypeByValue(const quint8 value)
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT t.Id, t.Name, t.Value FROM ProductType t "
                "WHERE t.Value = :Value");

    sql.bindValue(":Value", value);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    if (sql.first())
        return ProductTypeModel(sql.value(0).toInt(), sql.value(1).toString(), sql.value(2).toUInt());

    return ProductTypeModel(0, "", 0);
}

/**
 * @brief Database::getNodes Get node models
 * @return Node models
 */
QMap<QString, NodeModel> Database::getNodes()
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT n.Id, n.NodeId, n.Channel, n.PanId, n.Address, n.HistoryLogIndex, n.EventLogIndex, n.InstantLogIndex, n.Timestamp, t.Value FROM Node n "
                "INNER JOIN ProductType t ON n.ProductTypeId = t.Id");

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QMap<QString, NodeModel> models;
    while (sql.next())
        models.insert(makeUniqeKey(sql.value(2).toUInt(), sql.value(3).toUInt(), sql.value(4).toUInt()), NodeModel(sql.value(0).toInt(), sql.value(1).toUInt(),
                                                                                                                   sql.value(2).toUInt(), sql.value(3).toUInt(),
                                                                                                                   sql.value(4).toUInt(), sql.value(5).toUInt(),
                                                                                                                   sql.value(6).toUInt(), sql.value(7).toUInt(),
                                                                                                                   sql.value(8).toString(), sql.value(9).toUInt()));

    return models;
}

/**
 * @brief Database::getNode Get node model
 * @param channel The node channel
 * @param pan The node pan
 * @param address The node address
 * @return Node model
 */
NodeModel Database::getNode(const quint8 channel, const quint16 panId, const quint16 address)
{
    QMutexLocker locker(this->mutex);

    QSqlQuery sql;
    sql.prepare("SELECT n.Id, n.NodeId, n.Channel, n.PanId, n.Address, n.HistoryLogIndex, n.EventLogIndex, n.InstantLogIndex, n.Timestamp, t.Value FROM Node n "
                "INNER JOIN ProductType t ON n.ProductTypeId = t.Id "
                "WHERE n.Channel = :Channel AND n.PanId = :PanId AND n.Address = :Address");

    sql.bindValue(":Channel", channel);
    sql.bindValue(":PanId", panId);
    sql.bindValue(":Address", address);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    if (sql.first())
        return NodeModel(sql.value(0).toInt(), sql.value(1).toUInt(),
                         sql.value(2).toUInt(), sql.value(3).toUInt(),
                         sql.value(4).toUInt(), sql.value(5).toUInt(),
                         sql.value(6).toUInt(), sql.value(7).toUInt(),
                         sql.value(8).toString(), sql.value(9).toUInt());

    return NodeModel(0, 0, 0, 0, 0, 0, 0, 0, "", 0);
}

/**
 * @brief Database::insertNode Insert node
 * @param model Node model
 */
void Database::insertNode(const NodeModel& model)
{
    QMutexLocker locker(mutex);

    int productTypeId = getProductTypeByValue(model.getProductTypeValue()).getId();

    if (getNode(model.getNodeId(), model.getChannel(), model.getPanId()).getId() != 0)
        return; // The node already exists.

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("INSERT INTO Node (NodeId, Channel, PanId, Address, HistoryLogIndex, EventLogIndex, InstantLogIndex, ProductTypeId, Timestamp) "
                "VALUES(:NodeId, :Channel, :PanId, :Address, :HistoryLogIndex, :EventLogIndex, :InstantLogIndex, :ProductTypeId, :Timestamp)");

    sql.bindValue(":NodeId", model.getNodeId());
    sql.bindValue(":Channel", model.getChannel());
    sql.bindValue(":PanId", model.getPanId());
    sql.bindValue(":Address", model.getAddress());
    sql.bindValue(":HistoryLogIndex", model.getHistoryLogIndex());
    sql.bindValue(":EventLogIndex", model.getEventLogIndex());
    sql.bindValue(":InstantLogIndex", model.getInstantLogIndex());
    sql.bindValue(":ProductTypeId", productTypeId);
    sql.bindValue(":Timestamp", model.getTimestamp());

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::updateNodeId Update node id
 * @param channel The node channel
 * @param pan The node pan
 * @param address The node address
 * @param nodeId The node id
 */
void Database::updateNodeId(const quint8 channel, const quint16 panId, const quint16 address, const quint32 nodeId)
{
    QMutexLocker locker(this->mutex);

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("UPDATE Node SET NodeId = :NodeId "
                "WHERE Channel = :Channel AND PanId = :PanId AND Address = :Address");

    sql.bindValue(":NodeId", nodeId);
    sql.bindValue(":Channel", channel);
    sql.bindValue(":PanId", panId);
    sql.bindValue(":Address", address);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::updateProductType Update product type
 * @param channel The node channel
 * @param pan The node pan
 * @param address The node address
 * @param productTypeIdValue The product type id value
 */
void Database::updateProductType(const quint8 channel, const quint16 panId, const quint16 address, const quint8 productTypeValue)
{
    QMutexLocker locker(this->mutex);

    int productTypeId = getProductTypeByValue(productTypeValue).getId();

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("UPDATE Node SET ProductTypeId = :ProductTypeId "
                "WHERE Channel = :Channel AND PanId = :PanId AND Address = :Address");

    sql.bindValue(":ProductTypeId", productTypeId);
    sql.bindValue(":Channel", channel);
    sql.bindValue(":PanId", panId);
    sql.bindValue(":Address", address);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::updateNodeTimestamp Update node timestamp
 * @param channel The node channel
 * @param pan The node pan
 * @param address The node address
 * @param timestamp The timestamp
 */
void Database::updateNodeTimestamp(const quint8 channel, const quint16 panId, const quint16 address, const QString& timestamp)
{
    QMutexLocker locker(this->mutex);

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("UPDATE Node SET Timestamp = :Timestamp "
                "WHERE Channel = :Channel AND PanId = :PanId AND Address = :Address");

    sql.bindValue(":Timestamp", timestamp);
    sql.bindValue(":Channel", channel);
    sql.bindValue(":PanId", panId);
    sql.bindValue(":Address", address);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::updateNodeLogIndex Update node log index
 * @param channel The node channel
 * @param pan The node pan
 * @param address The node address
 * @param index The log index (0 = History, 1 = Event, 2 = Instant)
 * @param type The log type
 */
 void Database::updateNodeLogIndex(const quint8 channel, const quint16 panId, const quint16 address, const quint32 index, const quint8 type)
 {
    QMutexLocker locker(this->mutex);

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    if (type == 0) // History
    {
        sql.prepare("UPDATE Node SET HistoryLogIndex = :HistoryLogIndex "
                    "WHERE Channel = :Channel AND PanId = :PanId AND Address = :Address");

        sql.bindValue(":HistoryLogIndex", index);
        sql.bindValue(":Channel", channel);
        sql.bindValue(":PanId", panId);
        sql.bindValue(":Address", address);
    }
    else if (type == 1) // Event
    {
        sql.prepare("UPDATE Node SET EventLogIndex = :EventLogIndex "
                    "WHERE Channel = :Channel AND PanId = :PanId AND Address = :Address");

        sql.bindValue(":EventLogIndex", index);
        sql.bindValue(":Channel", channel);
        sql.bindValue(":PanId", panId);
        sql.bindValue(":Address", address);
    }
    else if (type == 2) // Instant
    {
        sql.prepare("UPDATE Node SET InstantLogIndex = :InstantLogIndex "
                    "WHERE Channel = :Channel AND PanId = :PanId AND Address = :Address");

        sql.bindValue(":InstantLogIndex", index);
        sql.bindValue(":Channel", channel);
        sql.bindValue(":PanId", panId);
        sql.bindValue(":Address", address);
    }

    if (!sql.exec())
        qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::deleteNodes Delete nodes
 * @param list The list of nod id
 */
void Database::deleteNodes(const QList<QVariant>& list)
{
    QMutexLocker locker(this->mutex);

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("DELETE FROM Node "
                "WHERE Id = ?");

    sql.addBindValue(list);

    if (!sql.execBatch())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::resetNodeIndexes Reset node indexes
 */
void Database::resetNodeIndexes()
{
    QMutexLocker locker(this->mutex);

    QSqlDatabase::database().transaction();

    QSqlQuery sql;
    sql.prepare("UPDATE Node SET HistoryLogIndex = :HistoryIndex, EventLogIndex = :EventLogIndex, InstantLogIndex = :InstantLogIndex");

    sql.bindValue(":HistoryLogIndex", 0);
    sql.bindValue(":EventLogIndex", 0);
    sql.bindValue(":InstantLogIndex", 0);

    if (!sql.exec())
       qCritical().noquote() << QString("Failed to execute sql query: %1, Error: %2").arg(sql.lastQuery()).arg(sql.lastError().text());

    QSqlDatabase::database().commit();
}

/**
 * @brief Database::makeUniqe Make unique key
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 */
QString Database::makeUniqeKey(const quint8 channel, const quint16 panId, const quint16 address)
{
    return QString("%1,%2,%3").arg(channel).arg(panId).arg(address);
}
