QT += core sql

CONFIG += c++11 staticlib

TARGET = storage
TEMPLATE = lib

DEFINES += STORAGE_LIBRARY

HEADERS += \
    Shared.h \
    IStorage.h \
    Database.h \
    Model/ConfigurationModel.h \
    Model/LogTypeModel.h \
    Model/NodeModel.h \
    Model/ProductTypeModel.h \
    Model/StatisticModel.h

SOURCES += \
    Database.cpp \
    Model/ConfigurationModel.cpp \
    Model/LogTypeModel.cpp \
    Model/NodeModel.cpp \
    Model/ProductTypeModel.cpp \
    Model/StatisticModel.cpp

RESOURCES += Storage.qrc

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:QMAKE_LFLAGS += -Wl,--start-group

DEPENDPATH += $$OUT_PWD/../Common $$PWD/../Common
INCLUDEPATH += $$OUT_PWD/../Common $$PWD/../Common
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Common/release/ -lcommon
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Common/debug/ -lcommon
else:macx:LIBS += -L$$OUT_PWD/../Common/ -lcommon
else:unix:LIBS += -L$$OUT_PWD/../Common/ -lcommon
