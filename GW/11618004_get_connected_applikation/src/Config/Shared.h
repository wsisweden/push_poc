#pragma once

#include <QtCore/QtGlobal>

#if defined(CONFIG_LIBRARY)
    #define CONFIG_EXPORT //Q_DECL_EXPORT
#else
    #define CONFIG_EXPORT //Q_DECL_IMPORT
#endif
