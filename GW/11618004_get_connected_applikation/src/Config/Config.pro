QT += core

CONFIG += c++11 staticlib

TARGET = config
TEMPLATE = lib

DEFINES += CONFIG_LIBRARY

HEADERS += \
    Shared.h \
    ListenerWorker.h \
    Backend/ChannelTransport.h \
    Backend/NetworkBackend.h \
    Backend/NodeBackend.h \
    Backend/StatisticBackend.h \
    Backend/SystemBackend.h

SOURCES += \
    ListenerWorker.cpp \
    Backend/ChannelTransport.cpp \
    Backend/NetworkBackend.cpp \
    Backend/NodeBackend.cpp \
    Backend/StatisticBackend.cpp \
    Backend/SystemBackend.cpp

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:QMAKE_LFLAGS += -Wl,--start-group

DEPENDPATH += $$OUT_PWD/../Common $$PWD/../Common
INCLUDEPATH += $$OUT_PWD/../Common $$PWD/../Common
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Common/release/ -lcommon
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Common/debug/ -lcommon
else:macx:LIBS += -L$$OUT_PWD/../Common/ -lcommon
else:unix:LIBS += -L$$OUT_PWD/../Common/ -lcommon

DEPENDPATH += $$OUT_PWD/../Storage $$PWD/../Storage
INCLUDEPATH += $$OUT_PWD/../Storage $$PWD/../Storage
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Storage/debug/ -lstorage
else:macx:LIBS += -L$$OUT_PWD/../Storage/ -lstorage
else:unix:LIBS += -L$$OUT_PWD/../Storage/ -lstorage

