#pragma once

#include "Shared.h"
#include "Backend/NodeBackend.h"
#include "Backend/SystemBackend.h"
#include "Backend/NetworkBackend.h"
#include "Backend/StatisticBackend.h"
#include "Backend/ChannelTransport.h"
#include "../Common/Context.h"

#include <QtCore/QObject>

#include <QtWebChannel/QWebChannel>

#include <QtWebSockets/QWebSocketServer>

class CONFIG_EXPORT ListenerWorker : public QObject
{
    Q_OBJECT

    public:
        explicit ListenerWorker(Context* context, QObject* parent = nullptr);
        ~ListenerWorker();

    private:
        Context* context = nullptr;
        QWebChannel* channel = nullptr;
        QWebSocketServer* server = nullptr;

        NodeBackend* node = nullptr;
        SystemBackend* system = nullptr;
        NetworkBackend* network = nullptr;
        StatisticBackend* statistic = nullptr;

        void setupWorker();

        Q_SLOT void newConnection();
};
