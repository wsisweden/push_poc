#include "ListenerWorker.h"
#include "../Storage/IStorage.h"
#include "../Storage/Model/ConfigurationModel.h"

#include <QtCore/QDebug>

#include <QtWebSockets/QWebSocket>

/**
 * @brief ListenerWorker::ListenerWorker Construct a new listener worker
 * @param context The context
 */
ListenerWorker::ListenerWorker(Context* context, QObject* parent)
    : QObject(parent)
{
    qInfo().noquote() << "Initialize listener worker";

    this->context = context;

    setupWorker();
}

/**
 * @brief ListenerWorker::~ListenerWorker Teardown listener worker
 */
ListenerWorker::~ListenerWorker()
{
    qInfo().noquote() << "Teardown listener worker";
}

/**
 * @brief ListenerWorker::setupWorker Setup worker
 */
void ListenerWorker::setupWorker()
{
    quint16 port = this->context->getStorage()->getConfigurationByName("ListenerPort").getValue().toInt();

    this->server = new QWebSocketServer("", QWebSocketServer::NonSecureMode, this);
    if (this->server->listen(QHostAddress::AnyIPv4, port))
    {
        connect(this->server, &QWebSocketServer::newConnection, this, &ListenerWorker::newConnection);

        qInfo().noquote() << QString("Listening for incoming connections on ws://%1:%2").arg(this->server->serverAddress().toString()).arg(this->server->serverPort());
    }
    else
    {
       qCritical().noquote() << QString("Unable to listen on port: %1").arg(port);
    }

    this->node = new NodeBackend(this->context, this);
    this->system = new SystemBackend(this->context, this);
    this->network = new NetworkBackend(this->context, this);
    this->statistic = new StatisticBackend(this->context, this);

    this->channel = new QWebChannel(this);
    this->channel->registerObject("node", this->node);
    this->channel->registerObject("system", this->system);
    this->channel->registerObject("network", this->network);
    this->channel->registerObject("statistic", this->statistic);
}

/**
 * @brief ListenerWorker::newConnection New connection handler
 */
void ListenerWorker::newConnection()
{
    QWebSocket* socket = this->server->nextPendingConnection();

    this->channel->connectTo(new ChannelTransport(socket));

    qInfo().noquote() << QString("Accepted connection from %1").arg(socket->peerAddress().toString());
}
