#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"
#include "../../Common/Event/ConfigurationEvent.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QTimer>

class CONFIG_EXPORT NetworkBackend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString ethernetMac READ getEthernetMac NOTIFY ethernetMacChanged)
    Q_PROPERTY(QString ethernetStatic READ getEthernetStatic WRITE setEthernetStatic NOTIFY ethernetStaticChanged)
    Q_PROPERTY(QString ethernetAddress READ getEthernetAddress WRITE setEthernetAddress NOTIFY ethernetAddressChanged)
    Q_PROPERTY(QString ethernetNetmask READ getEthernetNetmask WRITE setEthernetNetmask NOTIFY ethernetNetmaskChanged)
    Q_PROPERTY(QString ethernetGateway READ getEthernetGateway WRITE setEthernetGateway NOTIFY ethernetGatewayChanged)

    Q_PROPERTY(QString wirelessMac READ getWirelessMac NOTIFY wirelessMacChanged)
    Q_PROPERTY(int wirelessLinkQuality READ getWirelessLinkQuality NOTIFY wirelessLinkQualityChanged)
    Q_PROPERTY(QString wirelessLinkQualityDescription READ getWirelessLinkQualityDescription NOTIFY wirelessLinkQualityDescriptionChanged)
    Q_PROPERTY(int wirelessSignalStrength READ getWirelessSignalStrength NOTIFY wirelessSignalStrengthChanged)
    Q_PROPERTY(QString wirelessSignalStrengthDescription READ getWirelessSignalStrengthDescription NOTIFY wirelessSignalStrengthDescriptionChanged)
    Q_PROPERTY(QString wirelessAddress READ getWirelessAddress NOTIFY wirelessAddressChanged)
    Q_PROPERTY(QString wirelessNetmask READ getWirelessNetmask NOTIFY wirelessNetmaskChanged)
    Q_PROPERTY(QString wirelessSsid READ getWirelessSsid WRITE setWirelessSsid NOTIFY wirelessSsidChanged)
    Q_PROPERTY(QString wirelessPassword READ getWirelessPassword WRITE setWirelessPassword NOTIFY wirelessPasswordChanged)

    public:
        explicit NetworkBackend(Context* context, QObject* parent = nullptr);
        ~NetworkBackend();

        Q_SIGNAL void ethernetMacChanged();
        Q_SIGNAL void ethernetStaticChanged();
        Q_SIGNAL void ethernetAddressChanged();
        Q_SIGNAL void ethernetNetmaskChanged();
        Q_SIGNAL void ethernetGatewayChanged();

        Q_SIGNAL void wirelessMacChanged();
        Q_SIGNAL void wirelessLinkQualityChanged();
        Q_SIGNAL void wirelessLinkQualityDescriptionChanged();
        Q_SIGNAL void wirelessSignalStrengthChanged();
        Q_SIGNAL void wirelessSignalStrengthDescriptionChanged();
        Q_SIGNAL void wirelessAddressChanged();
        Q_SIGNAL void wirelessNetmaskChanged();
        Q_SIGNAL void wirelessSsidChanged();
        Q_SIGNAL void wirelessPasswordChanged();

        void setEthernetStatic(const QString& value);
        void setEthernetAddress(const QString& value);
        void setEthernetNetmask(const QString& value);
        void setEthernetGateway(const QString& value);

        void setWirelessSsid(const QString& value);
        void setWirelessPassword(const QString& value);

        QString getEthernetMac() const { return this->ethernetMac; }
        QString getEthernetAddress() const { return this->ethernetAddress; }
        QString getEthernetStatic() const { return this->ethernetStatic; }
        QString getEthernetNetmask() const { return this->ethernetNetmask; }
        QString getEthernetGateway() const { return this->ethernetGateway; }

        QString getWirelessMac() const { return this->wirelessMac; }
        int getWirelessLinkQuality() const { return this->wirelessLinkQuality; }
        QString getWirelessLinkQualityDescription() const { return this->wirelessLinkQualityDescription; }
        int getWirelessSignalStrength() const { return this->wirelessSignalStrength; }
        QString getWirelessSignalStrengthDescription() const { return this->wirelessSignalStrengthDescription; }
        QString getWirelessAddress() const { return this->wirelessAddress; }
        QString getWirelessNetmask() const { return this->wirelessNetmask; }
        QString getWirelessSsid() const { return this->wirelessSsid; }
        QString getWirelessPassword() const { return this->wirelessPassword; }

public slots:
        Q_INVOKABLE void restartNetworkAndLoadConfiguration();

    private: 
        QString ethernetMac;
        QString ethernetAddress;
        QString ethernetStatic;
        QString ethernetNetmask;
        QString ethernetGateway;

        QString wirelessMac;
        QString wirelessAddress;
        QString wirelessNetmask;
        QString wirelessSsid;
        QString wirelessPassword;
        QString wirelessLinkQualityDescription;
        QString wirelessSignalStrengthDescription;

        QString factoryReset;
        QString ethernetLinkState;

        int wirelessLinkQuality = 0;
        int ethernetLinkInterval = 0;
        int periodicUpdateInterval = 0;
        int wirelessSignalStrength = -100;

        bool restartingNetwork = false;

        Context* context = nullptr;
        QTimer* wirelessTimer = nullptr;
        QTimer* ethernetTimer = nullptr;
        QTimer* ethernetLinkTimer = nullptr;

        void restartNetwork();
        void loadConfiguration();
        void writeEthernetConfig();
        void writeWirelessConfig();

        Q_SLOT void doFactoryReset();
        Q_SLOT void updateEthernet();
        Q_SLOT void updateWireless();
        Q_SLOT void updateEthernetLink();
        Q_SLOT void configurationReady();
        Q_SLOT void doRestartNetwork();
        Q_SLOT void configurationReceived(const ConfigurationEvent& event);
        Q_SLOT void initializeComplete();
};
