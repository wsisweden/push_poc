#include "NetworkBackend.h"
#include "../../Storage/IStorage.h"
#include "../../Common/Constant.h"
#include "../../Common/EventManager.h"
#include "../../Common/Event/AuthorizationEvent.h"

#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QProcess>

#include <QtNetwork/QHostAddress>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QNetworkAddressEntry>

/**
 * @brief NetworkBackend::NetworkBackend Construct a new network backend
 */
NetworkBackend::NetworkBackend(Context* context, QObject* parent)
    : QObject(parent)
{
    this->context = context;

    this->wirelessTimer = new QTimer(this);
    this->ethernetTimer = new QTimer(this);
    this->ethernetLinkTimer = new QTimer(this);

    connect(this->wirelessTimer, &QTimer::timeout, this, &NetworkBackend::updateWireless);
    connect(this->ethernetTimer, &QTimer::timeout, this, &NetworkBackend::updateEthernet);
    connect(this->ethernetLinkTimer, &QTimer::timeout, this, &NetworkBackend::updateEthernetLink);
    connect(this->context->getEventManager(), &EventManager::factoryReset, this, &NetworkBackend::doFactoryReset);
    connect(this->context->getEventManager(), &EventManager::initializeComplete, this, &NetworkBackend::initializeComplete, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationReady, this, &NetworkBackend::configurationReady, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationReceived, this, &NetworkBackend::configurationReceived, Qt::QueuedConnection);

    loadConfiguration();
}

/**
 * @brief NetworkBackend::~NetworkBackend Teardown network backend
 */
NetworkBackend::~NetworkBackend()
{
    this->ethernetLinkTimer->stop();
    this->ethernetTimer->stop();
    this->wirelessTimer->stop();
}

/**
 * @brief NetworkBackend::loadConfiguration Load configration
 */
void NetworkBackend::loadConfiguration()
{
    this->factoryReset = this->context->getStorage()->getConfigurationByName("FactoryReset").getValue();
    this->ethernetLinkInterval = this->context->getStorage()->getConfigurationByName("EthernetLinkInterval").getValue().toInt();
    this->periodicUpdateInterval = this->context->getStorage()->getConfigurationByName("PeriodicUpdateInterval").getValue().toInt();
}

/**
 * @brief NetworkBackend::initializeComplete Initialize complete handler
 */
void NetworkBackend::initializeComplete()
{
    this->ethernetLinkTimer->start(ethernetLinkInterval);
    this->wirelessTimer->start(this->periodicUpdateInterval);
    this->ethernetTimer->start(this->periodicUpdateInterval);
}

/**
 * @brief NetworkBackend::setEthernetStatic Set the ethernet static
 * @param value The ethernet static
 */
void NetworkBackend::setEthernetStatic(const QString& value)
{
    if (value != "true" && value != "false")
        return;

    this->ethernetStatic = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Ethernet static changed (%1)").arg(this->ethernetStatic);

    this->ethernetAddress.clear();
    this->ethernetNetmask.clear();
    this->ethernetGateway.clear();

    emit ethernetAddressChanged();
    emit ethernetNetmaskChanged();
    emit ethernetGatewayChanged();

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    if (this->ethernetStatic == "false")
        writeEthernetConfig();

    emit ethernetStaticChanged();
}

/**
 * @brief NetworkBackend::setEthernetAddress Set the ethernet address
 * @param value The ethernet address
 */
void NetworkBackend::setEthernetAddress(const QString& value)
{
    QRegExp regEx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
    if (value.isEmpty() || !regEx.exactMatch(value) || this->ethernetAddress == value)
        return;

    this->ethernetAddress = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Ethernet address changed (%1)").arg(this->ethernetAddress);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    if (!this->ethernetAddress.isEmpty() && !this->ethernetNetmask.isEmpty() && !this->ethernetGateway.isEmpty())
        writeEthernetConfig();

    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_ADDRESS, this->ethernetAddress));

    emit ethernetAddressChanged();
}

/**
 * @brief NetworkBackend::setEthernetNetmask Set the ethernet netmask
 * @param value The ethernet netmask
 */
void NetworkBackend::setEthernetNetmask(const QString& value)
{
    QRegExp regEx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
    if (value.isEmpty() || !regEx.exactMatch(value) || this->ethernetNetmask == value)
        return;

    this->ethernetNetmask = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Ethernet netmask changed (%1)").arg(this->ethernetNetmask);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    if (!this->ethernetAddress.isEmpty() && !this->ethernetNetmask.isEmpty() && !this->ethernetGateway.isEmpty())
        writeEthernetConfig();

    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_NETMASK, this->ethernetNetmask));

    emit ethernetNetmaskChanged();
}

/**
 * @brief NetworkBackend::setEthernetGateway Set the ethernet gateway
 * @param value The ethernet gateway
 */
void NetworkBackend::setEthernetGateway(const QString& value)
{
    QRegExp regEx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
    if (value.isEmpty() || !regEx.exactMatch(value) || this->ethernetGateway == value)
        return;

    this->ethernetGateway = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Ethernet gateway changed (%1)").arg(this->ethernetGateway);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    if (!this->ethernetAddress.isEmpty() && !this->ethernetNetmask.isEmpty() && !this->ethernetGateway.isEmpty())
        writeEthernetConfig();

    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_GATEWAY, this->ethernetGateway));

    emit ethernetNetmaskChanged();
}

/**
 * @brief NetworkBackend::setWirelessSsid Set the wireless ssid
 * @param value The wireless ssid
 */
void NetworkBackend::setWirelessSsid(const QString& value)
{
    if (value.isEmpty() || this->wirelessSsid == value)
        return;

    this->wirelessSsid = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Wireless SSID changed (%1)").arg(this->wirelessSsid);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    if (!this->wirelessSsid.isEmpty() && !this->wirelessPassword.isEmpty())
        writeWirelessConfig();

    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_SSID, this->wirelessSsid));

    emit wirelessSsidChanged();
}

/**
 * @brief NetworkBackend::setWirelessPassword Set the wireless password
 * @param value The wireless password
 */
void NetworkBackend::setWirelessPassword(const QString& value)
{
    if (value.isEmpty() || this->wirelessPassword == value)
        return;

    this->wirelessPassword = value;
    this->factoryReset = "false";

    qInfo().noquote() << "Wireless password changed";

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    if (!this->wirelessSsid.isEmpty() && !this->wirelessPassword.isEmpty())
        writeWirelessConfig();

    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_PASSWORD, this->wirelessPassword));

    emit wirelessPasswordChanged();
}

/**
 * @brief NetworkBackend::updateEthernet Update ethernet
 */
void NetworkBackend::updateEthernet()
{
    QProcess process;
    QNetworkInterface eth0 = QNetworkInterface::interfaceFromName("eth0");
    if (eth0.isValid())
    {
        QString ethernetMac = eth0.hardwareAddress();
        if (this->ethernetMac != ethernetMac)
        {
            this->ethernetMac = ethernetMac;
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_MAC, this->ethernetMac));
            emit ethernetMacChanged();
        }

        if (!eth0.addressEntries().isEmpty())
        {
            QString ethernetAddress = eth0.addressEntries().first().ip().toString();
            if (this->ethernetAddress != ethernetAddress)
            {
                this->ethernetAddress = ethernetAddress;
                this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_ADDRESS, this->ethernetAddress));
                emit ethernetAddressChanged();
            }

            QString ethernetNetmask = eth0.addressEntries().first().netmask().toString();
            if (this->ethernetNetmask != ethernetNetmask)
            {
                this->ethernetNetmask = ethernetNetmask;
                this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_NETMASK, this->ethernetNetmask));
                emit ethernetNetmaskChanged();
            }
        }
        else
        {
            // Turn off the cloud led if we lost ethernet address and netmask.
            if (!this->ethernetAddress.isEmpty() && !this->ethernetNetmask.isEmpty())
                this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

            this->ethernetAddress.clear();
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_ADDRESS, this->ethernetAddress));
            emit ethernetAddressChanged();

            this->ethernetNetmask.clear();
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_NETMASK, this->ethernetNetmask));
            emit ethernetNetmaskChanged();
        }
    }

    process.start("sh", QStringList() << "-c" << "cat /etc/network/interfaces | grep 'iface eth0 inet static'");
    process.waitForFinished();
    QString ethernetStatic = process.readAllStandardOutput().trimmed().isEmpty() ? "false" : "true";
    process.close();

    if (this->ethernetStatic != ethernetStatic)
    {
        this->ethernetStatic = ethernetStatic;
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_STATIC, QVariant(this->ethernetStatic).toBool()));
        emit ethernetStaticChanged();
    }

    //process.start("sh", QStringList() << "-c" << "route -n | grep eth0 | tail -n 1 | cut -d ' ' -f 1");
    process.start("sh", QStringList() << "-c" << "ip route | grep 'default' | grep 'eth0' | cut -d ' ' -f 3");
    process.waitForFinished();
    QString ethernetGateway = process.readAllStandardOutput().trimmed();
    process.close();

    if (this->ethernetGateway != ethernetGateway)
    {
        this->ethernetGateway = ethernetGateway;
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_GATEWAY, this->ethernetGateway));
        emit ethernetGatewayChanged();
    }
}

/**
 * @brief NetworkBackend::updateWireless Update wireless
 */
void NetworkBackend::updateWireless()
{
    QNetworkInterface wlan0 = QNetworkInterface::interfaceFromName("wlan0");
    if (wlan0.isValid())
    {
        QString wirelessMac = wlan0.hardwareAddress();
        if (this->wirelessMac != wirelessMac)
        {
            this->wirelessMac = wirelessMac;
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_MAC, this->wirelessMac));
            emit wirelessMacChanged();
        }

        if (!wlan0.addressEntries().isEmpty())
        {
            QString wirelessAddress = wlan0.addressEntries().first().ip().toString();
            if (this->wirelessAddress != wirelessAddress)
            {
                this->wirelessAddress = wirelessAddress;
                this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_ADDRESS, this->wirelessAddress));
                emit wirelessAddressChanged();
            }

            QString wirelessNetmask = wlan0.addressEntries().first().netmask().toString();
            if (this->wirelessNetmask != wirelessNetmask)
            {
                this->wirelessNetmask = wirelessNetmask;
                this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_NETMASK, this->wirelessNetmask));
                emit wirelessNetmaskChanged();
            }
        }
        else
        {
            // Turn off the cloud led if we lost wireless address and netmask.
            if (!this->wirelessAddress.isEmpty() && !this->wirelessNetmask.isEmpty())
                this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

            this->wirelessAddress.clear();
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_ADDRESS, this->wirelessAddress));
            emit wirelessAddressChanged();

            this->wirelessNetmask.clear();
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_NETMASK, this->wirelessNetmask));
            emit wirelessNetmaskChanged();
        }
    }

    QProcess process;
    process.start("sh", QStringList() << "-c" << "cat /etc/wpa_supplicant.conf | grep 'ssid'");
    process.waitForFinished();
    QString ssid = process.readAllStandardOutput().trimmed();
    if (!ssid.isEmpty())
    {
        // Substring on first occurrence of '='.
        QString wirelessSsid = ssid.mid(ssid.indexOf('=') + 1, ssid.length());

        // Remove leading / trailing '"'.
        wirelessSsid = wirelessSsid.left(wirelessSsid.length() - 1);
        wirelessSsid = wirelessSsid.right(wirelessSsid.length() - 1);

        if (this->wirelessSsid != wirelessSsid)
        {
            this->wirelessSsid = wirelessSsid;
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_SSID, this->wirelessSsid));
            emit wirelessSsidChanged();
        }
    }
    process.close();

    process.start("sh", QStringList() << "-c" << "cat /etc/wpa_supplicant.conf | grep 'psk'");
    process.waitForFinished();
    QString psk = process.readAllStandardOutput().trimmed();
    if (!psk.isEmpty())
    {
        // Substring on first occurrence of '='.
        QString wirelessPassword = psk.mid(psk.indexOf('=') + 1, psk.length());

        // Remove leading / trailing '"'.
        wirelessPassword = wirelessPassword.left(wirelessPassword.length() - 1);
        wirelessPassword = wirelessPassword.right(wirelessPassword.length() - 1);

        if (this->wirelessPassword != wirelessPassword)
        {
            this->wirelessPassword = wirelessPassword;
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_PASSWORD, this->wirelessPassword));
            emit wirelessPasswordChanged();
        }
    }
    process.close();

    process.start("sh", QStringList() << "-c" << "iwconfig wlan0 | grep 'Quality' | sed 's/^ *//g' | cut -d '=' -f 2 | cut -d ' ' -f 1");
    process.waitForFinished();
    QString quality = process.readAllStandardOutput().trimmed();
    process.close();

    process.start("sh", QStringList() << "-c" << "iwconfig wlan0 | grep 'Signal' | sed 's/^ *//g' | cut -d '=' -f 3 | cut -d ' ' -f 1");
    process.waitForFinished();
    QString strength = process.readAllStandardOutput().trimmed();
    process.close();

    if (!strength.isEmpty())
    {
        this->wirelessSignalStrength = strength.toInt();

        // Generally,
        //    db >= -50 db = 100% quality
        //    db <= -100 db = 0% quality
        //
        // For example:
        //     High quality: 90% ~= -55db
        //     Good quality: 50% ~= -75db
        //     Low quality: 30% ~= -85db
        //     Bad quality: 30% ~= -95db
        //     Unusable: 8% ~= -96db
        //
        // See: http://www.speedguide.net/faq/how-does-rssi-dbm-relate-to-signal-quality-percent-439
        if (this->wirelessSignalStrength >= -55) this->wirelessSignalStrengthDescription = "High";
        else if (this->wirelessSignalStrength >= -75) this->wirelessSignalStrengthDescription = "Good";
        else if (this->wirelessSignalStrength >= -85) this->wirelessSignalStrengthDescription = "Low";
        else if (this->wirelessSignalStrength >= -95) this->wirelessSignalStrengthDescription = "Bad";
        else this->wirelessSignalStrengthDescription = "Unusable";

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_SIGNAL_STRENGTH, this->wirelessSignalStrength));

        emit wirelessSignalStrengthChanged();
        emit wirelessSignalStrengthDescriptionChanged();
    }

    if (!quality.isEmpty())
    {
        this->wirelessLinkQuality = qRound(((float)quality.split('/')[0].toInt() / (float)quality.split('/')[1].toInt()) * 100);

        if (this->wirelessLinkQuality >= 45) this->wirelessLinkQualityDescription = "High";
        else if (this->wirelessLinkQuality >= 35) this->wirelessLinkQualityDescription = "Good";
        else if (this->wirelessLinkQuality >= 25) this->wirelessLinkQualityDescription = "Low";
        else if (this->wirelessLinkQuality >= 15) this->wirelessLinkQualityDescription = "Bad";
        else this->wirelessLinkQualityDescription = "Unusable";

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_LINK_QUALITY, this->wirelessLinkQuality));

        emit wirelessLinkQualityChanged();
        emit wirelessLinkQualityDescriptionChanged();
    }
}

/**
 * @brief NetworkBackend::writeEthernetConfig Write ethernet config
 */
void NetworkBackend::writeEthernetConfig()
{
    qInfo().noquote() << "Writing ethernet config";

    if (this->ethernetStatic == "true")
    {
        QString config("auto lo\n"
                       "iface lo inet loopback\n\n"
                       "auto eth0\n"
                       "iface eth0 inet static\n"
                       "    address #ADDRESS#\n"
                       "    netmask #NETMASK#\n"
                       "    gateway #GATEWAY#\n\n"
                       "auto wlan0\n"
                       "iface wlan0 inet dhcp\n"
                       "    udhcpc_opts -b\n"
                       "    pre-up wpa_supplicant -B -D wext -i wlan0 -c /etc/wpa_supplicant.conf\n"
                       "    post-down killall -q wpa_supplicant\n");

        config.replace("#ADDRESS#", this->ethernetAddress);
        config.replace("#NETMASK#", this->ethernetNetmask);
        config.replace("#GATEWAY#", this->ethernetGateway);

        QFile file("/etc/network/interfaces");
        if (file.open(QIODevice::WriteOnly))
        {
            QTextStream stream(&file);
            stream << config;
            stream.flush();
        }
        file.close();
    }
    else
    {
        QString config("auto lo\n"
                       "iface lo inet loopback\n\n"
                       "auto eth0\n"
                       "iface eth0 inet dhcp\n\n"
                       "auto wlan0\n"
                       "iface wlan0 inet dhcp\n"
                       "    udhcpc_opts -b\n"
                       "    pre-up wpa_supplicant -B -D wext -i wlan0 -c /etc/wpa_supplicant.conf\n"
                       "    post-down killall -q wpa_supplicant\n");

        QFile file("/etc/network/interfaces");
        if (file.open(QIODevice::WriteOnly))
        {
            QTextStream stream(&file);
            stream << config;
            stream.flush();
        }
        file.close();
    }
}

/**
 * @brief NetworkBackend::writeWirelessConfig Write wireless config
 */
void NetworkBackend::writeWirelessConfig()
{
    qInfo().noquote() << "Writing wireless config";

    QString config("ap_scan=1\n\n"
                   "network={\n"
                   "    ssid=\"#SSID#\"\n"
                   "    psk=\"#PSK#\"\n"
                   "}\n");

    config.replace("#SSID#", this->wirelessSsid);
    config.replace("#PSK#", this->wirelessPassword);

    QFile file("/etc/wpa_supplicant.conf");
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        stream << config;
        stream.flush();
    }
    file.close();
}

/**
 * @brief NetworkBackend::restartNetwork Restart network
 */
void NetworkBackend::restartNetwork()
{
    this->ethernetMac.clear();
    this->ethernetAddress.clear();
    this->ethernetStatic.clear();
    this->ethernetNetmask.clear();
    this->ethernetGateway.clear();

    emit ethernetMacChanged();
    emit ethernetAddressChanged();
    emit ethernetNetmaskChanged();
    emit ethernetStaticChanged();
    emit ethernetGatewayChanged();

    this->wirelessMac.clear();
    this->wirelessAddress.clear();
    this->wirelessNetmask.clear();
    this->wirelessSsid.clear();
    this->wirelessPassword.clear();

    emit wirelessMacChanged();
    emit wirelessAddressChanged();
    emit wirelessNetmaskChanged();
    emit wirelessSsidChanged();
    emit wirelessPasswordChanged();

    this->wirelessLinkQuality = 0;
    this->wirelessSignalStrength = -100;

    emit wirelessLinkQualityChanged();
    emit wirelessSignalStrengthChanged();

    this->wirelessLinkQualityDescription.clear();
    this->wirelessSignalStrengthDescription.clear();

    emit wirelessLinkQualityDescriptionChanged();
    emit wirelessSignalStrengthDescriptionChanged();

    QTimer::singleShot(1000, this, &NetworkBackend::doRestartNetwork);
}

/**
 * @brief NetworkBackend::doRestartNetwork Do restart network
 */
void NetworkBackend::doRestartNetwork()
{
    qInfo().noquote() << "Restarting network";

    // Force heartbeat led off.
    this->context->getEventManager()->emitForceHeartbeatEvent(HeartbeatEvent(false));

    this->context->getEventManager()->emitUplinkChangedEvent(UplinkEvent(false));
    this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

    // Force application to process events.
    qApp->processEvents();

    QProcess process;
    process.start("sh", QStringList() << "-c" << "/etc/init.d/S40network restart");
    process.waitForFinished();
    process.close();

    process.start("sh", QStringList() << "-c" << "/etc/init.d/S40network restart");
    process.waitForFinished();
    process.close();

    // Force normal heartbeat led.
    this->context->getEventManager()->emitForceHeartbeatEvent(HeartbeatEvent(true));

    this->context->getEventManager()->emitAuthorizeEvent();

    // Force application to process events.
    qApp->processEvents();
}

/**
 * @brief NetworkBackend::configurationReady Configuration ready handler
 */
void NetworkBackend::configurationReady()
{
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_MAC, this->ethernetMac));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_ADDRESS, this->ethernetAddress));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_NETMASK, this->ethernetNetmask));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_GATEWAY, this->ethernetGateway));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_STATIC, QVariant(this->ethernetStatic).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_MAC, this->wirelessMac));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_SSID, this->wirelessSsid));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_PASSWORD, this->wirelessPassword));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_ADDRESS, this->wirelessAddress));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_NETMASK, this->wirelessNetmask));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_LINK_QUALITY, this->wirelessLinkQuality));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_SIGNAL_STRENGTH, this->wirelessSignalStrength));
    this->context->getEventManager()->emitConfigurationCompleteEvent();
}

/**
 * @brief NetworkBackend::configurationReceived Configuration handler
 */
void NetworkBackend::configurationReceived(const ConfigurationEvent& event)
{
    if (event.getIndex() == Param::ETHERNET_ADDRESS)
    {
        QString value = event.getData().toString();
        QRegExp regEx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
        if (value.isEmpty() || !regEx.exactMatch(value) || this->ethernetAddress == value)
            return;

        this->ethernetAddress = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Ethernet address changed (%1)").arg(this->ethernetAddress);

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_ADDRESS, this->ethernetAddress));
        this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));

        if (!this->ethernetAddress.isEmpty() && !this->ethernetNetmask.isEmpty() && !this->ethernetGateway.isEmpty())
        {
            writeEthernetConfig();

            // Schedule network restart.
            QTimer::singleShot(1000, this, &NetworkBackend::restartNetworkAndLoadConfiguration);
        }

        emit ethernetAddressChanged();
    }
    else if (event.getIndex() == Param::ETHERNET_STATIC)
    {
        bool value = QVariant(event.getData().toByteArray().at(0)).toBool();
        if (this->ethernetStatic == QVariant(value).toString())
            return;

        this->ethernetStatic = QVariant(value).toString();
        this->factoryReset = "false";

        qInfo().noquote() << QString("Ethernet static changed (%1)").arg(this->ethernetStatic);

        if (this->ethernetStatic == "false")
        {
            this->ethernetAddress.clear();
            this->ethernetNetmask.clear();
            this->ethernetGateway.clear();

            emit ethernetAddressChanged();
            emit ethernetNetmaskChanged();
            emit ethernetGatewayChanged();
        }

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_STATIC, QVariant(this->ethernetStatic).toBool()));
        this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));

//        if (this->ethernetStatic == "false")
//        {
            writeEthernetConfig();

            // Schedule network restart.
            QTimer::singleShot(1000, this, &NetworkBackend::restartNetworkAndLoadConfiguration);
//        }

        emit ethernetStaticChanged();
    }
    else if (event.getIndex() == Param::ETHERNET_NETMASK)
    {
        QString value = event.getData().toString();
        QRegExp regEx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
        if (value.isEmpty() || !regEx.exactMatch(value) || this->ethernetNetmask == value)
            return;

        this->ethernetNetmask = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Ethernet netmask changed (%1)").arg(this->ethernetNetmask);

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_NETMASK, this->ethernetNetmask));
        this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));

        if (!this->ethernetAddress.isEmpty() && !this->ethernetNetmask.isEmpty() && !this->ethernetGateway.isEmpty())
        {
            writeEthernetConfig();

            // Schedule network restart.
            QTimer::singleShot(1000, this, &NetworkBackend::restartNetworkAndLoadConfiguration);
        }

        emit ethernetNetmaskChanged();
    }
    else if (event.getIndex() == Param::ETHERNET_GATEWAY)
    {
        QString value = event.getData().toString();
        QRegExp regEx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
        if (value.isEmpty() || !regEx.exactMatch(value) || this->ethernetGateway == value)
            return;

        this->ethernetGateway = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Ethernet gateway changed (%1)").arg(this->ethernetGateway);

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ETHERNET_GATEWAY, this->ethernetGateway));
        this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));

        if (!this->ethernetAddress.isEmpty() && !this->ethernetNetmask.isEmpty() && !this->ethernetGateway.isEmpty())
        {
            writeEthernetConfig();

            // Schedule network restart.
            QTimer::singleShot(1000, this, &NetworkBackend::restartNetworkAndLoadConfiguration);
        }

        emit ethernetGatewayChanged();
    }
    else if (event.getIndex() == Param::WIRELESS_SSID)
    {
        QString value = event.getData().toString();
        if (value.isEmpty() || this->wirelessSsid == value)
            return;

        this->wirelessSsid = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Wireless SSID changed (%1)").arg(this->wirelessSsid);

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_SSID, this->wirelessSsid));
        this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));

        if (!this->wirelessSsid.isEmpty() && !this->wirelessPassword.isEmpty())
        {
            writeWirelessConfig();

            // Schedule network restart.
            QTimer::singleShot(1000, this, &NetworkBackend::restartNetworkAndLoadConfiguration);
        }

        emit wirelessSsidChanged();
    }
    else if (event.getIndex() == Param::WIRELESS_PASSWORD)
    {
        QString value = event.getData().toString();
        if (value.isEmpty() || this->wirelessPassword == value)
            return;

        this->wirelessPassword = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Wireless password changed (%1)").arg(this->wirelessPassword);

        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::WIRELESS_PASSWORD, this->wirelessPassword));
        this->context->getEventManager()->emitFactoryResetChangedEvent(FactoryResetEvent(QVariant(this->factoryReset).toBool()));

        if (!this->wirelessSsid.isEmpty() && !this->wirelessPassword.isEmpty())
        {
            writeWirelessConfig();

            // Schedule network restart.
            QTimer::singleShot(1000, this, &NetworkBackend::restartNetworkAndLoadConfiguration);
        }

        emit wirelessPasswordChanged();
    }
}

/**
 * @brief NetworkBackend::doFactoryReset Factory reset handler
 */
void NetworkBackend::doFactoryReset()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "echo "" > /etc/wpa_supplicant.conf");
    process.waitForFinished();
    process.close();

    writeEthernetConfig();
    restartNetworkAndLoadConfiguration();
}

/**
 * @brief NetworkBackend::ethernetLink Ethernet link state handler
 */
void NetworkBackend::updateEthernetLink()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "cat /sys/class/net/eth0/operstate");
    process.waitForFinished();
    QString state = process.readAllStandardOutput().trimmed();
    process.close();

    if (!this->ethernetLinkState.isEmpty() && this->ethernetLinkState != state)
    {
        this->ethernetLinkState = state;

        restartNetworkAndLoadConfiguration();
    }
    else
    {
        // First we just want to set our default state.
        this->ethernetLinkState = state;
    }
}

/**
 * @brief NetworkBackend::restartNetworkAndLoadConfiguration Restart network and load configuration handler
 */
void NetworkBackend::restartNetworkAndLoadConfiguration()
{
    if (this->restartingNetwork)
        return;

    this->restartingNetwork = true;

    restartNetwork();
    loadConfiguration();

    this->restartingNetwork = false;
}
