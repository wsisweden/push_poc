#pragma once

#include "../Shared.h"

#include <QtCore/QObject>
#include <QtCore/QStringList>

#include <QtWebSockets/QWebSocket>

#include <QtWebChannel/QWebChannelAbstractTransport>

class CONFIG_EXPORT ChannelTransport : public QWebChannelAbstractTransport
{
    Q_OBJECT

    public:
        explicit ChannelTransport(QWebSocket* socket = nullptr);

        Q_SIGNAL void finished();

        Q_SLOT void sendMessage(const QJsonObject& message) override;

    private:
        QWebSocket* socket = nullptr;

        Q_SLOT void disconnected();
        Q_SLOT void textMessageReceived(const QString& message);
};
