#include "SystemBackend.h"
#include "../Common/Version.h"
#include "../../Common/Constant.h"
#include "../../Common/Event/ConfigurationEvent.h"
#include "../../Common/Event/AuthorizationEvent.h"
#include "../../Common/Event/HeartbeatEvent.h"
#include "../../Common/Event/ScanNetworkIntervalEvent.h"
#include "../../Common/Event/CollectDataIntervalEvent.h"
#include "../../Common/Event/RemoveNodeIntervalEvent.h"
#include "../../Common/Event/NetworkRestrictionEvent.h"
#include "../../Common/Event/NodeRestrictionEvent.h"
#include "../../Storage/IStorage.h"

#include <QtCore/QDebug>
#include <QtCore/QRegExp>
#include <QtCore/QVariant>
#include <QtCore/QDateTime>
#include <QtCore/QLoggingCategory>
#include <QtCore/QStorageInfo>
#include <QtCore/QProcess>

/**
 * @brief SystemBackend::SystemBackend Construct a new system backend
 */
SystemBackend::SystemBackend(Context* context, QObject* parent)
    : QObject(parent)
{
    this->context = context;

    qsrand(QDateTime::currentDateTimeUtc().toMSecsSinceEpoch());

    this->storageTimer = new QTimer(this);
    this->dateTimeTimer = new QTimer(this);
    this->secureShellTimer = new QTimer(this);

    connect(this->storageTimer, &QTimer::timeout, this, &SystemBackend::updateStorage);
    connect(this->dateTimeTimer, &QTimer::timeout, this, &SystemBackend::updateDateTime);
    connect(this->secureShellTimer, &QTimer::timeout, this, &SystemBackend::updateSecureShell);
    connect(this->context->getEventManager(), &EventManager::factoryResetChanged, this, &SystemBackend::doFactoryResetChanged);
    connect(this->context->getEventManager(), &EventManager::configurationReceived, this, &SystemBackend::configurationReceived, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::firmwareUpdated, this, &SystemBackend::firmwareUpdated, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::activityChanged, this, &SystemBackend::updateActivity, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationReady, this, &SystemBackend::configurationReady, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::initializeComplete, this, &SystemBackend::initializeComplete, Qt::QueuedConnection);

    loadConfiguration();
}

/**
 * @brief SystemBackend::~SystemBackend Teardown a new system backend
 */
SystemBackend::~SystemBackend()
{
    this->secureShellTimer->stop();
    this->dateTimeTimer->stop();
    this->storageTimer->stop();
}

/**
 * @brief SystemBackend::loadConfiguration Load configration
 */
void SystemBackend::loadConfiguration()
{
    this->activity = "Initializing";

    this->node = this->context->getStorage()->getConfigurationByName("Node").getValue();
    this->network = this->context->getStorage()->getConfigurationByName("Network").getValue();
    this->debugLog = this->context->getStorage()->getConfigurationByName("DebugLog").getValue();
    this->debugMpaLog = this->context->getStorage()->getConfigurationByName("DebugMpaLog").getValue();

    // Override network restriction with command line option if specified.
    if (this->context->getCommandLineParser()->isSet("network") && this->context->getCommandLineParser()->value("network").contains(":") && this->network != this->context->getCommandLineParser()->value("network"))
    {
        qInfo().noquote() << "Override network restriction using command line option";

        this->network = this->context->getCommandLineParser()->value("network");
        this->context->getEventManager()->emitNetworkRestrictionChangedEvent(NetworkRestrictionEvent(this->network));
    }

    // Override node restriction with command line option if specified.
    if (this->context->getCommandLineParser()->isSet("node") && this->node != this->context->getCommandLineParser()->value("node"))
    {
        qInfo().noquote() << "Override node restriction using command line option";

        this->node = this->context->getCommandLineParser()->value("node");
        this->context->getEventManager()->emitNodeRestrictionChangedEvent(NodeRestrictionEvent(this->node));
    }

    // Override debug log with command line option if specified.
    if (this->context->getCommandLineParser()->isSet("debuglog") && this->debugLog != this->context->getCommandLineParser()->value("debuglog"))
    {
        qInfo().noquote() << "Override debug log setting using command line option";

        this->debugLog = this->context->getCommandLineParser()->value("debuglog");
    }

    // Override debug MP-ACCESS log with command line option if specified.
    if (this->context->getCommandLineParser()->isSet("debugmpalog") && this->debugMpaLog != this->context->getCommandLineParser()->value("debugmpalog"))
    {
        qInfo().noquote() << "Override debug MP-ACCESS log setting using command line option";

        this->debugMpaLog = this->context->getCommandLineParser()->value("debugmpalog");
    }

    this->softwareType = "11618004";
    this->softwareVersion = QString("%1").arg(qApp->applicationVersion().split(',').at(0).split('.').at(0).trimmed());
    this->buildCommit = qApp->applicationVersion().split(',').at(0).split('.').at(3).trimmed();
    this->qtVersion = qApp->applicationVersion().split(',').at(1).trimmed();

    this->name = this->context->getStorage()->getConfigurationByName("Name").getValue();
    this->cloudUrl = this->context->getStorage()->getConfigurationByName("CloudUrl").getValue();
    this->cloudUsername = this->context->getStorage()->getConfigurationByName("CloudUsername").getValue();
    this->cloudPassword = this->context->getStorage()->getConfigurationByName("CloudPassword").getValue();
    this->factoryReset = this->context->getStorage()->getConfigurationByName("FactoryReset").getValue();
    this->securityCode = this->context->getStorage()->getConfigurationByName("SecurityCode").getValue();

    this->serialNumber = this->context->getStorage()->getConfigurationByName("SerialNumber").getValue();
    if (this->serialNumber.isEmpty())
        setSerialNumber(QString::number(generateSerialNumber()));

    this->deviceId = this->context->getStorage()->getConfigurationByName("DeviceId").getValue();
    if (this->deviceId.isEmpty())
        setDeviceId(QString::number(this->serialNumber.toULongLong() % 1000000000)); // Parse maximal number from serial number that are contained in an quint32.

    this->scanNetworkInterval = this->context->getStorage()->getConfigurationByName("ScanNetworkInterval").getValue().toInt() / 60000; // msec -> min
    this->collectDataInterval = this->context->getStorage()->getConfigurationByName("CollectDataInterval").getValue().toInt() / 60000; // msec -> min
    this->removeNodeInterval = this->context->getStorage()->getConfigurationByName("RemoveNodeInterval").getValue().toInt() / 60000; // msec -> min
    this->periodicUpdateInterval = this->context->getStorage()->getConfigurationByName("PeriodicUpdateInterval").getValue().toInt();

    generateMUI();
    setLogFilter();
    writePasswordConfig();
    writeHostnameConfig();
    readCanBitRateConfig();
    readRootfsRelease();

    emit softwareTypeChanged();
    emit softwareVersionChanged();
    emit serialNumberChanged();
    emit activityChanged();
    emit buildCommitChanged();
    emit qtVersionChanged();
    emit securityCodeChanged();
    emit serialNumberChanged();
    emit factoryResetChanged();
    emit nameChanged();
    emit dateChanged();
    emit timeChanged();
    emit deviceIdChanged();
    emit cloudUrlChanged();
    emit cloudUsernameChanged();
    emit cloudPasswordChanged();
    emit scanNetworkIntervalChanged();
    emit collectDataIntervalChanged();
    emit removeNodeIntervalChanged();
    emit secureShellChanged();
    emit debugLogChanged();
    emit debugMpaLogChanged();
    emit rootfsReleaseChanged();
}

/**
 * @brief SystemBackend::initializeComplete Initialize complete handler
 */
void SystemBackend::initializeComplete()
{
    this->storageTimer->start(this->periodicUpdateInterval);
    this->dateTimeTimer->start(this->periodicUpdateInterval);
    this->secureShellTimer->start(this->periodicUpdateInterval);
}

/**
 * @brief SystemBackend::generateMUI Generate MUI
 */
void SystemBackend::generateMUI()
{
    quint64 mui = ((quint64)ProductId::GATEWAY) << 48;
    if (!this->serialNumber.isEmpty())
        mui |= this->serialNumber.toULongLong();

    this->mui = QString::number(mui);
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::MUI, this->mui.toULongLong()));

    emit muiChanged();
}

/**
 * @brief SystemBackend::generateSerialNumber Generate serial number
 */
quint64 SystemBackend::generateSerialNumber()
{
    return ((((quint64)qrand()) << 32) | qrand()) & 0xffffffffffffL;
}

/**
 * @brief SystemBackend::setSecurityCode Set the security code
 * @param value The value
 */
void SystemBackend::setSecurityCode(const QString& value)
{
    if (value.isEmpty() || this->securityCode == value)
        return;

    this->securityCode = value;
    writePasswordConfig();

    this->factoryReset = "false";

    qInfo().noquote() << QString("Security code changed (%1)").arg(this->securityCode);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "SecurityCode", this->securityCode));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SECURITY_CODE, this->securityCode.toUInt()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit securityCodeChanged();
}

/**
 * @brief SystemBackend::setSerialNumber Set the serial number
 * @param value The value
 */
void SystemBackend::setSerialNumber(const QString& value)
{
    if (value.isEmpty() || this->serialNumber == value)
        return;

    this->serialNumber = value;
    generateMUI();

    qInfo().noquote() << QString("Serial number changed (%1)").arg(this->serialNumber);
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "SerialNumber", this->serialNumber));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SERIAL_NUMBER, this->serialNumber.toULongLong()));

    emit serialNumberChanged();
}

/**
 * @brief SystemBackend::setCanBitRate Set CAN bit rate
 * @param value The value
 */
void SystemBackend::setCanBitRate(const QString& value)
{
    if (value.isEmpty() || this->canBitRate == value)
        return;

    this->canBitRate = value;

    qInfo().noquote() << QString("CAN bit rate changed (%1)").arg(this->canBitRate);

    writeCanBitRateConfig();
    restartCan();

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CAN_BITRATE, this->canBitRate.toUInt()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit canBitRateChanged();
}

/**
 * @brief SystemBackend::setDeviceId Set the device id
 * @param value The value
 */
void SystemBackend::setDeviceId(const QString& value)
{
    if (value.isEmpty() || value.toInt() <= 0 || this->deviceId == value)
        return;

    this->deviceId = value;

    qInfo().noquote() << QString("Device id changed (%1)").arg(this->deviceId);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "DeviceId", this->deviceId));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEVICE_ID, this->deviceId.toUInt()));

    emit deviceIdChanged();
}

/**
 * @brief SystemBackend::setName Set the name
 * @param value The name
 */
void SystemBackend::setName(const QString& value)
{
    if (value.isEmpty() || this->name == value)
        return;

    this->name = value;
    writeHostnameConfig();

    this->factoryReset = "false";

    qInfo().noquote() << QString("Name changed (%1)").arg(this->name);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "Name", this->name));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NAME, this->name));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit nameChanged();
}

/**
 * @brief SystemBackend::setDate Set the date
 * @param value The date
 */
void SystemBackend::setDate(const QString& value)
{
    QRegExp regEx("[0-9]{4}-[0-9]{2}-[0-9]{2}");
    if (value.isEmpty() || !regEx.exactMatch(value) || this->date == value)
        return;

    this->date = value;
    writeDate();

    this->factoryReset = "false";

    qDebug().noquote() << QString("Date changed (%1)").arg(this->date);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DATETIME, (quint32)QDateTime::currentDateTimeUtc().toSecsSinceEpoch()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit dateChanged();
}

/**
 * @brief SystemBackend::setTime Set the time
 * @param value The time
 */
void SystemBackend::setTime(const QString& value)
{
    QRegExp regEx("[0-9]{2}:[0-9]{2}:[0-9]{2}");
    if (value.isEmpty() || !regEx.exactMatch(value) || this->time == value)
        return;

    this->time = value;
    writeTime();

    this->factoryReset = "false";

    qDebug().noquote() << QString("Time changed (%1)").arg(this->time);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DATETIME, (quint32)QDateTime::currentDateTimeUtc().toSecsSinceEpoch()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit timeChanged();
}

/**
 * @brief SystemBackend::setCloudUrl Set the cloud url
 * @param value The cloud url
 */
void SystemBackend::setCloudUrl(const QString& value)
{
    if (value.isEmpty() || this->cloudUrl == value)
        return;

    this->cloudUrl = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Cloud url changed (%1)").arg(this->cloudUrl);

    this->context->getStorage()->resetNodeIndexes();
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CloudUrl", this->cloudUrl));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_URL, this->cloudUrl));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

    emit factoryResetChanged();
    emit cloudUrlChanged();
}

/**
 * @brief SystemBackend::setCloudUsername Set the cloud username
 * @param value The cloud username
 */
void SystemBackend::setCloudUsername(const QString& value)
{
    if (value.isEmpty() || this->cloudUsername == value)
        return;

    this->cloudUsername = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Cloud username changed (%1)").arg(this->cloudUsername);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CloudUsername", this->cloudUsername));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_USERNAME, this->cloudUsername));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

    emit factoryResetChanged();
    emit cloudUsernameChanged();
}

/**
 * @brief SystemBackend::setCloudPassword Set the cloud password
 * @param value The cloud password
 */
void SystemBackend::setCloudPassword(const QString& value)
{
    if (value.isEmpty() || this->cloudPassword == value)
        return;

    this->cloudPassword = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Cloud password changed (%1)").arg(this->cloudPassword);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CloudPassword", this->cloudPassword));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_PASSWORD, this->cloudPassword));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

    emit factoryResetChanged();
    emit cloudPasswordChanged();
}

/**
 * @brief SystemBackend::setScanNetworkInterval Set the scan network interval
 * @param value The scan network interval
 */
void SystemBackend::setScanNetworkInterval(const int value)
{
    if (value < 1 || value > 30000 || this->scanNetworkInterval == value)
        return;

    this->scanNetworkInterval = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Scan network interval changed (%1)").arg(this->scanNetworkInterval);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "ScanNetworkInterval", QString::number(this->scanNetworkInterval * 60000))); // min -> msec
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitScanNetworkIntervalChangedEvent(ScanNetworkIntervalEvent(this->scanNetworkInterval * 60000)); // min -> msec
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SCAN_NETWORK_INTERVAL, this->scanNetworkInterval));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit scanNetworkIntervalChanged();
}

/**
 * @brief SystemBackend::setCollectDataInterval Set the collect data interval
 * @param value The interval
 */
void SystemBackend::setCollectDataInterval(const int value)
{
    if (value < 1 || value > 30000 || this->collectDataInterval == value)
        return;

    this->collectDataInterval = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Collect data interval changed (%1)").arg(this->collectDataInterval);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CollectDataInterval", QString::number(this->collectDataInterval * 60000))); // min -> msec
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitCollectDataIntervalChangedEvent(CollectDataIntervalEvent(this->collectDataInterval * 60000)); // min -> msec
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::COLLECT_DATA_INTERVAL, this->collectDataInterval));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit collectDataIntervalChanged();
}

/**
 * @brief SystemBackend::setRemoveNodeInterval Set the remove node interval
 * @param value The interval
 */
void SystemBackend::setRemoveNodeInterval(const int value)
{
    if (value < 1 || value > 30000 || this->removeNodeInterval == value)
        return;

    this->removeNodeInterval = value;
    this->factoryReset = "false";

    qInfo().noquote() << QString("Remove node interval changed (%1)").arg(this->removeNodeInterval);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "RemoveNodeInterval", QString::number(this->removeNodeInterval * 60000))); // min -> msec
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitRemoveNodeIntervalChangedEvent(RemoveNodeIntervalEvent(this->removeNodeInterval * 60000)); // min -> msec
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::REMOVE_NODE_INTERVAL, this->removeNodeInterval));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit removeNodeIntervalChanged();
}

/**
 * @brief SystemBackend::setDebugLog Set the debug log
 * @param value The value
 */
void SystemBackend::setDebugLog(const QString& value)
{
    if (value.isEmpty() || this->debugLog == value)
        return;

    this->debugLog = value;
    setLogFilter();

    this->factoryReset = "false";

    qInfo().noquote() << QString("Debug log changed (%1)").arg(value);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "DebugLog", this->debugLog));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEBUG_LOG, QVariant(this->debugLog).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit debugLogChanged();
}

/**
 * @brief SystemBackend::setDebugMpaLog Set the debug MP-ACCESS log
 * @param value The value
 */
void SystemBackend::setDebugMpaLog(const QString& value)
{
    if (value.isEmpty() || this->debugMpaLog == value)
        return;

    this->debugMpaLog = value;
    setLogFilter();

    this->factoryReset = "false";

    qInfo().noquote() << QString("Debug MP-ACCESS log changed (%1)").arg(value);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "DebugMpaLog", this->debugMpaLog));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEBUG_MPA_LOG, QVariant(this->debugMpaLog).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit debugMpaLogChanged();
}

/**
 * @brief SystemBackend::setFactoryReset Set the factory reset
 * @param value The value
 */
void SystemBackend::setFactoryReset(const QString &value)
{
    if (value.isEmpty() || this->factoryReset == value)
        return;

    this->factoryReset = value;

    qInfo().noquote() << QString("Factory reset changed (%1)").arg(this->factoryReset);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    if (this->factoryReset == "true")
        this->context->getEventManager()->emitFactoryResetEvent();

    emit factoryResetChanged();
}

/**
 * @brief SystemBackend::setNetwork Set network
 * @param value The value
 */
void SystemBackend::setNetwork(const QString& value)
{
    if (this->network == value)
        return;

    this->network = value;

    this->factoryReset = "false";

    qInfo().noquote() << QString("Network restriction changed (%1)").arg(this->network);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "Network", this->network));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitNetworkRestrictionChangedEvent(NetworkRestrictionEvent(this->network));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NETWORK, this->network));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit networkChanged();
}

/**
 * @brief SystemBackend::setNode Set node
 * @param value The value
 */
void SystemBackend::setNode(const QString& value)
{
    if (this->node == value)
        return;

    this->node = value;

    this->factoryReset = "false";

    qInfo().noquote() << QString("Node restriction changed (%1)").arg(this->node);

    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "Node", this->node));
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
    this->context->getEventManager()->emitNodeRestrictionChangedEvent(NodeRestrictionEvent(this->node));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NODE, this->node));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
    emit nodeChanged();
}

/**
 * @brief SystemBackend::setLogFilter Set log filter
 */
void SystemBackend::setLogFilter()
{
    QLoggingCategory::setFilterRules(QString("mpa.debug=%1\n").arg(this->debugMpaLog) +
                                     QString("default.debug=%1\n").arg(this->debugLog));
}

/**
 * @brief SystemBackend::updateActivity Activity changed handler
 */
void SystemBackend::updateActivity(const ActivityEvent& event)
{
    if (this->activity == event.getActivity())
        return;

    this->activity = event.getActivity();
    emit activityChanged();
}

/**
 * @brief SystemBackend::updateStorage Update storage value
 */
void SystemBackend::updateStorage()
{
    QStorageInfo storageInfo;
    storageInfo.setPath("/");

    this->freeStorage = storageInfo.bytesFree() / 1000 / 1000; // MB
    this->totalStorage = storageInfo.bytesTotal() / 1000 / 1000; // MB
    this->freeStoragePercent = qRound64(((float)this->freeStorage / (float)this->totalStorage) * 100);

    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FREE_STORAGE, this->freeStorage));

    emit freeStorageChanged();
    emit totalStorageChanged();
    emit freeStoragePercentChanged();
}

/**
 * @brief SystemBackend::updateDateTime Update date and time
 */
void SystemBackend::updateDateTime()
{
    this->date = QDateTime::currentDateTimeUtc().toString("yyyy-MM-dd");
    this->time = QDateTime::currentDateTimeUtc().toString("hh:mm:ss");

    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DATETIME, (quint32)QDateTime::currentDateTimeUtc().toSecsSinceEpoch()));

    emit dateChanged();
    emit timeChanged();
}

/**
 * @brief SystemBackend::updateSecureShell Update secure shell
 */
void SystemBackend::updateSecureShell()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "ps | grep /usr/sbin/sshd | wc -l");
    process.waitForFinished();
    QString secureShell = (process.readAllStandardOutput().trimmed().toInt() <= 2) ? "false" : "true";
    process.close();

    if (this->secureShell != secureShell)
    {
        this->secureShell = secureShell;
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SECURE_SHELL, QVariant(this->secureShell).toBool()));
        emit secureShellChanged();
    }
}

/**
 * @brief SystemBackend::firmwareUpdated Firmware updated handler
 * @param event The event
 */
void SystemBackend::firmwareUpdated(const FirmwareUpdatedEvent& event)
{
    if (this->firmwareType != event.getFirmwareType())
    {
        this->firmwareType = QString::number(event.getFirmwareType());
        emit firmwareTypeChanged();
    }

    if (this->firmwareVersion != event.getFirmwareVersion())
    {
        this->firmwareVersion = QString::number(event.getFirmwareVersion());
        emit firmwareVersionChanged();
    }

    if (this->firmwareAddress != event.getFirmwareAddress())
    {
        this->firmwareAddress = QString::number(event.getFirmwareAddress());
        emit firmwareAddressChanged();
    }
}

/**
 * @brief SystemBackend::configurationReceived Configuration handler
 * @param event The event
 */
void SystemBackend::configurationReceived(const ConfigurationEvent& event)
{
    if (event.getIndex() == Param::DEVICE_ID)
    {
        QString value = QString::number(event.getData().toUInt());
        if (value.isEmpty() || this->deviceId == value)
            return;

        this->deviceId = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Device id changed (%1)").arg(this->deviceId);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "DeviceId", this->deviceId));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEVICE_ID, this->deviceId.toUInt()));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit deviceIdChanged();
    }
    else if (event.getIndex() == Param::CAN_BITRATE)
    {
        QString value = QString::number(event.getData().toUInt());
        if (value.isEmpty() || this->canBitRate == value)
            return;

        this->canBitRate = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("CAN bit rate changed (%1)").arg(this->canBitRate);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CAN_BITRATE, this->canBitRate.toUInt()));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        writeCanBitRateConfig();

        // Schedule network restart.
        QTimer::singleShot(1000, this, &SystemBackend::restartCan);

        emit factoryResetChanged();
        emit canBitRateChanged();
    }
    else if (event.getIndex() == Param::NAME)
    {
        QString value = event.getData().toString();
        if (value.isEmpty() || this->name == value)
            return;

        this->name = value;
        writeHostnameConfig();

        this->factoryReset = "false";

        qInfo().noquote() << QString("Name changed (%1)").arg(this->name);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "Name", this->name));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NAME, this->name));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit nameChanged();
    }
    else if (event.getIndex() == Param::CLOUD_URL)
    {
        QString value = event.getData().toString();
        if (value.isEmpty() || this->cloudUrl == value)
            return;

        this->cloudUrl = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Cloud url changed (%1)").arg(this->cloudUrl);

        this->context->getStorage()->resetNodeIndexes();
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CloudUrl", this->cloudUrl));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_URL, this->cloudUrl));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));
        this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

        // Schedule authorization.
        QTimer::singleShot(1000, this, &SystemBackend::sendAuthorization);

        emit factoryResetChanged();
        emit cloudUrlChanged();
    }
    else if (event.getIndex() == Param::CLOUD_USERNAME)
    {
        QString value = event.getData().toString();
        if (value.isEmpty() || this->cloudUsername == value)
            return;

        this->cloudUsername = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Cloud username changed (%1)").arg(this->cloudUsername);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CloudUsername", this->cloudUsername));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_USERNAME, this->cloudUsername));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));
        this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

        // Schedule authorization.
        QTimer::singleShot(1000, this, &SystemBackend::sendAuthorization);

        emit factoryResetChanged();
        emit cloudUsernameChanged();
    }
    else if (event.getIndex() == Param::CLOUD_PASSWORD)
    {
        QString value = event.getData().toString();
        if (value.isEmpty() || this->cloudPassword == value)
            return;

        this->cloudPassword = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Cloud password changed (%1)").arg(this->cloudPassword);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CloudPassword", this->cloudPassword));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_PASSWORD, this->cloudPassword));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));
        this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, false));

        // Schedule authorization.
        QTimer::singleShot(1000, this, &SystemBackend::sendAuthorization);

        emit factoryResetChanged();
        emit cloudPasswordChanged();
    }
    else if (event.getIndex() == Param::SECURITY_CODE)
    {
        QString value = QString::number(event.getData().toUInt());
        if (value.isEmpty() || this->securityCode == value)
            return;

        this->securityCode = value;
        writePasswordConfig();

        this->factoryReset = "false";

        qInfo().noquote() << QString("Security code changed (%1)").arg(this->securityCode);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "SecurityCode", this->securityCode));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SECURITY_CODE, this->securityCode.toUInt()));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit securityCodeChanged();
    }
    else if (event.getIndex() == Param::SCAN_NETWORK_INTERVAL)
    {
        int value = event.getData().toInt();
        if (value < 1 || value > 30000 || this->scanNetworkInterval == value)
            return;

        this->scanNetworkInterval = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Scan network interval changed (%1)").arg(this->scanNetworkInterval);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "ScanNetworkInterval", QString::number(this->scanNetworkInterval * 60000))); // min -> msec
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitScanNetworkIntervalChangedEvent(ScanNetworkIntervalEvent(this->scanNetworkInterval * 60000)); // min -> msec
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SCAN_NETWORK_INTERVAL, this->scanNetworkInterval));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit scanNetworkIntervalChanged();
    }
    else if (event.getIndex() == Param::COLLECT_DATA_INTERVAL)
    {
        int value = event.getData().toInt();
        if (value < 1 || value > 30000 || this->collectDataInterval == value)
            return;

        this->collectDataInterval = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Collect data interval changed (%1)").arg(this->collectDataInterval);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "CollectDataInterval", QString::number(this->collectDataInterval * 60000))); // min -> msec
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitCollectDataIntervalChangedEvent(CollectDataIntervalEvent(this->collectDataInterval * 60000)); // min -> msec
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::COLLECT_DATA_INTERVAL, this->collectDataInterval));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit collectDataIntervalChanged();
    }
    else if (event.getIndex() == Param::REMOVE_NODE_INTERVAL)
    {
        int value = event.getData().toInt();
        if (value < 1 || value > 30000 || this->removeNodeInterval == value)
            return;

        this->removeNodeInterval = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Remove node interval changed (%1)").arg(this->removeNodeInterval);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "RemoveNodeInterval", QString::number(this->removeNodeInterval * 60000)));// min -> msec
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitRemoveNodeIntervalChangedEvent(RemoveNodeIntervalEvent(this->removeNodeInterval * 60000)); // min -> msec
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::REMOVE_NODE_INTERVAL, this->removeNodeInterval));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit removeNodeIntervalChanged();
    }
    else if (event.getIndex() == Param::DEBUG_LOG)
    {
        bool value = QVariant(event.getData().toByteArray().at(0)).toBool();
        if (this->debugLog == QVariant(value).toString())
            return;

        this->debugLog = QVariant(value).toString();
        setLogFilter();

        this->factoryReset = "false";

        qInfo().noquote() << QString("Debug log changed (%1)").arg(this->debugLog);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "DebugLog", this->debugLog));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEBUG_LOG, QVariant(this->debugLog).toBool()));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit debugLogChanged();
    }
    else if (event.getIndex() == Param::DEBUG_MPA_LOG)
    {
        bool value = QVariant(event.getData().toByteArray().at(0)).toBool();
        if (this->debugMpaLog == QVariant(value).toString())
            return;

        this->debugMpaLog = QVariant(value).toString();
        setLogFilter();

        this->factoryReset = "false";

        qInfo().noquote() << QString("Debug MP-ACCESS log changed (%1)").arg(this->debugMpaLog);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "DebugMpaLog", this->debugMpaLog));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEBUG_MPA_LOG, QVariant(this->debugMpaLog).toBool()));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit debugMpaLogChanged();
    }
    else if (event.getIndex() == Param::NETWORK)
    {
        QString value = event.getData().toString();
        if (this->network == value)
            return;

        this->network = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Network restriction changed (%1)").arg(this->network);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "Network", this->network));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitNetworkRestrictionChangedEvent(NetworkRestrictionEvent(this->network));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NETWORK, this->network));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit networkChanged();
    }
    else if (event.getIndex() == Param::NODE)
    {
        QString value = event.getData().toString();
        if (this->node == value)
            return;

        this->node = value;
        this->factoryReset = "false";

        qInfo().noquote() << QString("Node restriction changed (%1)").arg(this->node);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "Node", this->node));
        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitNodeRestrictionChangedEvent(NodeRestrictionEvent(this->node));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NODE, this->node));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        emit factoryResetChanged();
        emit nodeChanged();
    }
    else if (event.getIndex() == Param::DATETIME)
    {
        QString date = QDateTime::fromSecsSinceEpoch(event.getData().toLongLong()).currentDateTimeUtc().toString("yyyy-MM-dd");
        QRegExp regExDate("[0-9]{4}-[0-9]{2}-[0-9]{2}");
        if (!date.isEmpty() && regExDate.exactMatch(date) && this->date != date)
        {
            this->date = date;
            writeDate();

            this->factoryReset = "false";

            qDebug().noquote() << QString("Date changed (%1)").arg(this->date);

            this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DATETIME, (quint32)QDateTime::currentDateTimeUtc().toSecsSinceEpoch()));
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

            emit factoryResetChanged();
            emit dateChanged();
        }

        QString time = QDateTime::fromSecsSinceEpoch(event.getData().toLongLong()).currentDateTimeUtc().toString("hh:mm:ss");
        QRegExp regExTime("[0-9]{2}:[0-9]{2}:[0-9]{2}");
        if (!time.isEmpty() && regExTime.exactMatch(time) && this->time != time)
        {
            this->time = time;
            writeTime();

            this->factoryReset = "false";

            qDebug().noquote() << QString("Time changed (%1)").arg(this->time);

            this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DATETIME, (quint32)QDateTime::currentDateTimeUtc().toSecsSinceEpoch()));
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

            emit factoryResetChanged();
            emit timeChanged();
        }
    }
    else if (event.getIndex() == Param::SERIAL_NUMBER)
    {
        QString value = QString::number(event.getData().toULongLong());
        if (value == 0 || this->serialNumber == value)
            return;

        this->serialNumber = value;
        generateMUI();

        qInfo().noquote() << QString("Serial number changed (%1)").arg(this->serialNumber);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "SerialNumber", this->serialNumber));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SERIAL_NUMBER, this->serialNumber.toULongLong()));

        emit serialNumberChanged();
    }
    else if (event.getIndex() == Param::FACTORY_RESET)
    {
        bool value = QVariant(event.getData().toByteArray().at(0)).toBool();
        if (this->factoryReset == QVariant(value).toString())
            return;

        this->factoryReset = QVariant(value).toString();

        qInfo().noquote() << QString("Factory reset changed (%1)").arg(this->factoryReset);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "FactoryReset", this->factoryReset));
        this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

        if (this->factoryReset == "true")
        {
            // Reset database to default before we notify others.
            doFactoryReset();

            // Notify others about the factory reset.
            this->context->getEventManager()->emitFactoryResetEvent();
        }

        emit factoryResetChanged();
    }
}

/**
 * @brief SystemBackend::sendAuthorization Send authorization request
 */
void SystemBackend::sendAuthorization()
{
    if (!this->authorizationSent)
    {
        this->context->getEventManager()->emitAuthorizeEvent();
        this->authorizationSent = true;

        // Schedule reset of the authorization sent flag.
        QTimer::singleShot(1000, this, [this]()
        {
            this->authorizationSent = false;
        });
    }
}

/**
 * @brief SystemBackend::configurationReady Configuration ready handler
 */
void SystemBackend::configurationReady()
{
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::MUI, this->mui.toULongLong()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::BUILDCOMMIT, this->buildCommit));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::QTVERSION, this->qtVersion));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SOFTWARE_TYPE, this->softwareType.toUInt()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SOFTWARE_VERSION, this->softwareVersion.toUInt()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEVICE_ID, this->deviceId.toUInt()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NAME, this->name));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DATETIME, (quint32)QDateTime::currentDateTimeUtc().toSecsSinceEpoch()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_URL, this->cloudUrl));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_USERNAME, this->cloudUsername));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CLOUD_PASSWORD, this->cloudPassword));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SCAN_NETWORK_INTERVAL, this->scanNetworkInterval));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::COLLECT_DATA_INTERVAL, this->collectDataInterval));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::PERIODIC_UPDATE_INTERVAL, this->periodicUpdateInterval));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::REMOVE_NODE_INTERVAL, this->removeNodeInterval));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SECURE_SHELL, this->secureShell));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEBUG_LOG, QVariant(this->debugLog).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::DEBUG_MPA_LOG, QVariant(this->debugMpaLog).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FREE_STORAGE, this->freeStorage));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SECURITY_CODE, this->securityCode.toUInt()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::SERIAL_NUMBER, this->serialNumber.toULongLong()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NETWORK, this->network));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::NODE, this->node));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::CAN_BITRATE, this->canBitRate.toUInt()));
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::ROOTFS_RELEASE, this->rootfsRelease));
    this->context->getEventManager()->emitConfigurationCompleteEvent();
}

/**
 * @brief SystemBackend::doFactoryResetChanged Factory reset handler
 */
void SystemBackend::doFactoryResetChanged(const FactoryResetEvent& event)
{
    if (this->factoryReset == event.getFactoryReset())
        return;

    this->factoryReset = QVariant(event.getFactoryReset()).toString();
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FACTORY_RESET, QVariant(this->factoryReset).toBool()));

    emit factoryResetChanged();
}

/**
 * @brief SystemBackend::doFactoryReset Factory reset handler
 */
void SystemBackend::doFactoryReset()
{
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::READY, false));

    // Force application to process events.
    qApp->processEvents();

    this->storageTimer->stop();
    this->dateTimeTimer->stop();
    this->secureShellTimer->stop();

    this->canBitRate = Can::DEFAULT_BITRATE;
    writeCanBitRateConfig();

    this->context->getStorage()->factoryReset();

    loadConfiguration();
}

/**
 * @brief SystemBackend::writePassword Write password config
 */
void SystemBackend::writePasswordConfig()
{
    qInfo().noquote() << "Writing lighttpd password config";

    QProcess process;
    process.start("sh", QStringList() << "-c" << QString("echo 'root:%1' > /etc/lighttpd/passwd").arg(this->securityCode));
    process.waitForFinished();
    process.close();
}

/**
 * @brief SystemBackend::writeHostnameConfig Write hostname config
 */
void SystemBackend::writeHostnameConfig()
{
    qInfo().noquote() << "Writing hostname config";

    QProcess process;
    process.start("sh", QStringList() << "-c" << QString("echo '%1' > /etc/hostname && avahi-set-host-name %2").arg(QString(this->name).toLower().replace(' ', '-')).arg(QString(this->name).toLower().replace(' ', '-')));
    process.waitForFinished();
    process.close();
}

/**
 * @brief SystemBackend::writeDate Write date
 */
void SystemBackend::writeDate()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "date -s '" + this->date + "'");
    process.waitForFinished();
    process.close();

    process.start("sh", QStringList() << "-c" << "hwclock -w");
    process.waitForFinished();
    process.close();
}

/**
 * @brief SystemBackend::writeTime Write time
 */
void SystemBackend::writeTime()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "date -s '" + this->time + "'");
    process.waitForFinished();
    process.close();

    process.start("sh", QStringList() << "-c" << "hwclock -w");
    process.waitForFinished();
    process.close();
}

/**
 * @brief SystemBackend::readCanBitRateConfig Read CAN bit rate
 */
void SystemBackend::readCanBitRateConfig()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "cat /etc/can/bitrate");
    process.waitForFinished();
    QString canBitRate = process.readAllStandardOutput().trimmed();
    process.close();

    if (this->canBitRate != canBitRate)
    {
        this->canBitRate = canBitRate;
        emit canBitRateChanged();
    }
}

/**
 * @brief SystemBackend::writeCanBitRate Write CAN bit rate
 */
void SystemBackend::writeCanBitRateConfig()
{
    qInfo().noquote() << "Writing CAN bit rate config";

    QProcess process;
    process.start("sh", QStringList() << "-c" << QString("echo '%1' > /etc/can/bitrate").arg(this->canBitRate));
    process.waitForFinished();
    process.close();
}

/**
 * @brief NetworkBackend::restartCan Restart CAN
 */
void SystemBackend::restartCan()
{
    qInfo().noquote() << "Restarting CAN";

    QProcess process;
    process.start("sh", QStringList() << "-c" << "/etc/init.d/S40can restart");
    process.waitForFinished();
    process.close();
}

/**
 * @brief SystemBackend::readRootfsRelease Read rootfs release
 */
void SystemBackend::readRootfsRelease()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "cat /etc/rootfs-release");
    process.waitForFinished();
    QString rootfsRelease = process.readAllStandardOutput().trimmed();
    process.close();

    if (this->rootfsRelease != rootfsRelease)
    {
        this->rootfsRelease = rootfsRelease;
        emit rootfsReleaseChanged();
    }
}
