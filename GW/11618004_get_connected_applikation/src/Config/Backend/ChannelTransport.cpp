#include "ChannelTransport.h"

#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>

/**
 * @brief ChannelTransport::ChannelTransport Construct a new channel transport
 * @param socket The socket
 */
ChannelTransport::ChannelTransport(QWebSocket* socket)
    : QWebChannelAbstractTransport(socket)
    , socket(socket)
{
    connect(this->socket, &QWebSocket::disconnected, this, &ChannelTransport::disconnected);
    connect(this->socket, &QWebSocket::textMessageReceived, this, &ChannelTransport::textMessageReceived);
}

/**
 * @brief ChannelTransport::disconnected Disconnected handler
 */
void ChannelTransport::disconnected()
{
    qInfo().noquote() << QString("Client %1 was disconnected").arg(this->socket->peerAddress().toString());

    this->socket->deleteLater();

    emit finished();
}

/**
 * @brief ChannelTransport::sendMessage Send message
 * @param message The JSON message
 */
void ChannelTransport::sendMessage(const QJsonObject& message)
{
    QJsonDocument document(message);
    this->socket->sendTextMessage(QString(document.toJson(QJsonDocument::Compact)));
}

/**
 * @brief ChannelTransport::textMessageReceived Deserialize the stringified JSON message
 * @param message The message
*/
void ChannelTransport::textMessageReceived(const QString& message)
{
    emit messageReceived(QJsonDocument::fromJson(message.toUtf8()).object(), this);
}
