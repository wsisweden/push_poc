#include "NodeBackend.h"
#include "../../Common/Constant.h"
#include "../../Storage/IStorage.h"
#include "../../Storage/Model/NodeModel.h"

#include <QtCore/QDebug>
#include <QtCore/QVariantMap>
#include <QtCore/QByteArray>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>

/**
 * @brief NodeBackend::NodeBackend Construct a new node backend
 */
NodeBackend::NodeBackend(Context* context, QObject* parent)
    : QObject(parent)
{
    this->context = context;

    connect(this->context->getEventManager(), &EventManager::factoryReset, this, &NodeBackend::doFactoryReset);
    connect(this->context->getEventManager(), &EventManager::nodeAdded, this, &NodeBackend::nodeAdded, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::nodeRemoved, this, &NodeBackend::nodeRemoved, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::nodeUpdated, this, &NodeBackend::nodeUpdated, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::initializeComplete, this, &NodeBackend::initializeComplete, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationReady, this, &NodeBackend::configurationReady, Qt::QueuedConnection);
}

/**
 * @brief NodeBackend::initializeComplete Initialize complete handler
 */
void NodeBackend::initializeComplete()
{
    this->initComplete = true;
}

/**
 * @brief NodeBackend::configurationReady Configuration ready handler
 */
void NodeBackend::configurationReady()
{
    this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FOUND_NODES, this->foundNodesCount));
    this->context->getEventManager()->emitConfigurationCompleteEvent();
}

/**
 * @brief NodeBackend::nodeAdded Node added handler
 * @param model The node model
 */
void NodeBackend::nodeAdded(const NodeAddedEvent& event)
{
    if (lookupNode(event.getChannel(), event.getPanId(), event.getAddress()) == -1)
    {
        QJsonObject node;
        node.insert("nodeId", (qint64)event.getNodeId());
        node.insert("address", event.getAddress());
        node.insert("channel", event.getChannel());
        node.insert("panId", event.getPanId());
        node.insert("productType", event.getProductTypeValue());
        node.insert("timestamp", event.getTimestamp());

        QJsonDocument document(node);
        this->foundNodes.append(QString(document.toJson(QJsonDocument::Compact)));
        this->foundNodesCount = this->foundNodes.count();

        if (this->initComplete)
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FOUND_NODES, this->foundNodesCount));

        emit foundNodesChanged();
        emit foundNodesCountChanged();
    }
}

/**
 * @brief NodeBackend::nodeRemoved Node removed handler
 * @param model The node model
 */
void NodeBackend::nodeRemoved(const NodeRemovedEvent& event)
{
    int index = -1;
    if ((index = lookupNode(event.getChannel(), event.getPanId(), event.getAddress())) != -1)
    {
        this->foundNodes.removeAt(index);
        this->foundNodesCount = this->foundNodes.count();

        if (this->initComplete)
            this->context->getEventManager()->emitConfigurationChangedEvent(ConfigurationEvent(Param::FOUND_NODES, this->foundNodesCount));

        emit foundNodesChanged();
        emit foundNodesCountChanged();
    }
}

/**
 * @brief NodeBackend::nodeUpdated Node updated handler
 * @param model The node model
 */
void NodeBackend::nodeUpdated(const NodeUpdatedEvent& event)
{
    int index = -1;
    if ((index = lookupNode(event.getChannel(), event.getPanId(), event.getAddress())) != -1)
    {
        QByteArray data = this->foundNodes.at(index).toUtf8();
        QVariantMap map = QJsonDocument::fromJson(data).object().toVariantMap();
        map["nodeId"] = event.getNodeId();
        map["timestamp"] = event.getTimestamp();
        map["productType"] = event.getProductTypeValue();

        this->foundNodes[index] = QJsonDocument::fromVariant(map).toJson(QJsonDocument::Compact);

        emit foundNodesChanged();
    }
}

/**
 * @brief NodeBackend::lookupNode Lookup if node allready exists.
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 * @returns Returns the index position of the first match, otherwise -1
 */
int NodeBackend::lookupNode(quint8 channel, quint16 panId, quint16 address)
{
    for (int i = 0; i < this->foundNodes.count(); i++)
    {
        QByteArray data = this->foundNodes.at(i).toUtf8();
        QVariantMap map = QJsonDocument::fromJson(data).object().toVariantMap();
        if (map.value("channel").toString() == QString::number(channel) && map.value("panId").toString() == QString::number(panId) && map.value("address").toString() == QString::number(address))
            return i;
    }

    return -1;
}

/**
 * @brief NodeBackend::doFactoryReset Factory reset handler
 */
void NodeBackend::doFactoryReset()
{
    this->foundNodes.clear();
    this->foundNodesCount = 0;
    this->initComplete = false;

    emit foundNodesChanged();
    emit foundNodesCountChanged();
}
