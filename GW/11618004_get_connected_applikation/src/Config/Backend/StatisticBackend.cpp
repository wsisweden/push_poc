#include "StatisticBackend.h"
#include "../../Storage/IStorage.h"
#include "../../Common/Event/ConfigurationEvent.h"

#include <QtCore/QByteArray>

/**
 * @brief StatisticBackend::StatisticBackend Construct a new statistic backend
 * @param socket The socket
 */
StatisticBackend::StatisticBackend(Context* context, QObject* parent)
    : QObject(parent)
{
    this->context = context;

    connect(this->context->getEventManager(), &EventManager::factoryReset, this, &StatisticBackend::doFactoryReset);
    connect(this->context->getEventManager(), &EventManager::mpaRequest, this, &StatisticBackend::mpaRequest, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::mpaRequestFailed, this, &StatisticBackend::mpaRequestFailed, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::httpRequest, this, &StatisticBackend::httpRequest, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::httpRequestFailed, this, &StatisticBackend::httpRequestFailed, Qt::QueuedConnection);
}

/**
 * @brief StatisticBackend::mpaRequest MP-ACCESS request success handler
 */
void StatisticBackend::mpaRequest()
{
    this->totalMpaRequest += 1;

    int failedPercent = qRound(((float)this->failedMpaRequest / (float)this->totalMpaRequest) * 100);
    if (this->failedMpaRequestPercent != failedPercent)
    {
        this->failedMpaRequestPercent = failedPercent;
        emit failedMpaRequestPercentChanged();
    }

    emit totalMpaRequestChanged();
}

/**
 * @brief StatisticBackend::mpaRequestFailed MP-ACCESS request failed handler
 */
void StatisticBackend::mpaRequestFailed()
{
    this->failedMpaRequest += 1;

    int failedPercent = (this->totalMpaRequest > 0) ? qRound(((float)this->failedMpaRequest / (float)this->totalMpaRequest) * 100) : 0;
    if (this->failedMpaRequestPercent != failedPercent)
    {
        this->failedMpaRequestPercent = failedPercent;
        emit failedMpaRequestPercentChanged();
    }

    emit failedMpaRequestChanged();
}

/**
 * @brief StatisticBackend::httpRequestSuccess HTTP request handler
 */
void StatisticBackend::httpRequest()
{
    this->totalHttpRequest += 1;

    int failedPercent = qRound(((float)this->failedHttpRequest / (float)this->totalHttpRequest) * 100);
    if (this->failedHttpRequest != failedPercent)
    {
        this->failedHttpRequest = failedPercent;
        emit failedHttpRequestPercentChanged();
    }

    emit totalHttpRequestChanged();
}

/**
 * @brief StatisticBackend::httpRequestFailed HTTP request failed handler
 */
void StatisticBackend::httpRequestFailed()
{
    this->failedHttpRequest += 1;

    int failedPercent = (this->totalHttpRequest > 0) ? qRound(((float)this->failedHttpRequest / (float)this->totalHttpRequest) * 100) : 0;
    if (this->failedHttpRequest != failedPercent)
    {
        this->failedHttpRequest = failedPercent;
        emit failedHttpRequestPercentChanged();
    }

    emit failedHttpRequestChanged();
}

/**
 * @brief StatisticBackend::doFactoryReset Factory reset handler
 */
void StatisticBackend::doFactoryReset()
{
    this->totalMpaRequest = 0;
    this->failedMpaRequest = 0;
    this->failedMpaRequestPercent = 0;
    this->totalHttpRequest = 0;
    this->failedHttpRequest = 0;
    this->failedHttpRequestPercent = 0;

    emit totalMpaRequestChanged();
    emit failedMpaRequestChanged();
    emit failedMpaRequestPercentChanged();
    emit totalHttpRequestChanged();
    emit failedHttpRequestChanged();
    emit failedHttpRequestPercentChanged();
}
