#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"

#include <QtCore/QObject>

class CONFIG_EXPORT StatisticBackend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int totalMpaRequest READ getTotalMpaRequest NOTIFY totalMpaRequestChanged)
    Q_PROPERTY(int failedMpaRequest READ getFailedMpaRequest NOTIFY failedMpaRequestChanged)
    Q_PROPERTY(int failedMpaRequestPercent READ getFailedMpaRequestPercent NOTIFY failedMpaRequestPercentChanged)

    Q_PROPERTY(int totalHttpRequest READ getTotalHttpRequest NOTIFY totalHttpRequestChanged)
    Q_PROPERTY(int failedHttpRequest READ getFailedHttpRequest NOTIFY failedHttpRequestChanged)
    Q_PROPERTY(int failedHttpRequestPercent READ getFailedHttpRequestPercent NOTIFY failedHttpRequestPercentChanged)

    public:
        explicit StatisticBackend(Context* context, QObject* parent = nullptr);

        Q_SIGNAL void totalMpaRequestChanged();
        Q_SIGNAL void failedMpaRequestChanged();
        Q_SIGNAL void failedMpaRequestPercentChanged();

        Q_SIGNAL void totalHttpRequestChanged();
        Q_SIGNAL void failedHttpRequestChanged();
        Q_SIGNAL void failedHttpRequestPercentChanged();

        int getTotalMpaRequest() const { return this->totalMpaRequest; }
        int getFailedMpaRequest() const { return this->failedMpaRequest; }
        int getFailedMpaRequestPercent() const { return this->failedMpaRequestPercent; }

        int getTotalHttpRequest() const { return this->totalHttpRequest; }
        int getFailedHttpRequest() const { return this->failedHttpRequest; }
        int getFailedHttpRequestPercent() const { return this->failedHttpRequestPercent; }

    private:
        int totalMpaRequest = 0;
        int failedMpaRequest = 0;
        int failedMpaRequestPercent = 0;

        int totalHttpRequest = 0;
        int failedHttpRequest = 0;
        int failedHttpRequestPercent = 0;

        Context* context = nullptr;

        Q_SLOT void doFactoryReset();
        Q_SLOT void mpaRequest();
        Q_SLOT void mpaRequestFailed();
        Q_SLOT void httpRequest();
        Q_SLOT void httpRequestFailed();
};
