#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"
#include "../../Common/Event/ActivityEvent.h"
#include "../../Common/Event/ConfigurationEvent.h"
#include "../../Common/Event/FactoryResetEvent.h"
#include "../../Common/Event/FirmwareUpdatedEvent.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QTimer>

class CONFIG_EXPORT SystemBackend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int freeStorage READ getFreeStorage NOTIFY freeStorageChanged)
    Q_PROPERTY(int totalStorage READ getTotalStorage NOTIFY totalStorageChanged)
    Q_PROPERTY(int freeStoragePercent READ getFreeStoragePercent NOTIFY freeStoragePercentChanged)

    Q_PROPERTY(QString mui READ getMUI NOTIFY muiChanged)
    Q_PROPERTY(QString softwareType READ getSoftwareType NOTIFY softwareTypeChanged)
    Q_PROPERTY(QString softwareVersion READ getSoftwareVersion NOTIFY softwareVersionChanged)
    Q_PROPERTY(QString activity READ getActivity NOTIFY activityChanged)
    Q_PROPERTY(QString qtVersion READ getQtVersion NOTIFY qtVersionChanged)
    Q_PROPERTY(QString buildCommit READ getBuildCommit NOTIFY buildCommitChanged)
    Q_PROPERTY(QString firmwareType READ getFirmwareType NOTIFY firmwareTypeChanged)
    Q_PROPERTY(QString firmwareVersion READ getFirmwareVersion NOTIFY firmwareVersionChanged)
    Q_PROPERTY(QString firmwareAddress READ getFirmwareAddress NOTIFY firmwareAddressChanged)
    Q_PROPERTY(QString serialNumber READ getSerialNumber WRITE setSerialNumber NOTIFY serialNumberChanged)
    Q_PROPERTY(QString factoryReset READ getFactoryReset WRITE setFactoryReset NOTIFY factoryResetChanged)

    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString date READ getDate WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QString time READ getTime WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(QString rootfsRelease READ getRootfsRelease NOTIFY rootfsReleaseChanged)
    Q_PROPERTY(QString securityCode READ getSecurityCode WRITE setSecurityCode NOTIFY securityCodeChanged)
    Q_PROPERTY(QString deviceId READ getDeviceId WRITE setDeviceId NOTIFY deviceIdChanged)
    Q_PROPERTY(QString canBitRate READ getCanBitRate WRITE setCanBitRate NOTIFY canBitRateChanged)

    Q_PROPERTY(QString cloudUrl READ getCloudUrl WRITE setCloudUrl NOTIFY cloudUrlChanged)
    Q_PROPERTY(QString cloudUsername READ getCloudUsername WRITE setCloudUsername NOTIFY cloudUsernameChanged)
    Q_PROPERTY(QString cloudPassword READ getCloudPassword WRITE setCloudPassword NOTIFY cloudPasswordChanged)

    Q_PROPERTY(int scanNetworkInterval READ getScanNetworkInterval WRITE setScanNetworkInterval NOTIFY scanNetworkIntervalChanged)
    Q_PROPERTY(int collectDataInterval READ getCollectDataInterval WRITE setCollectDataInterval NOTIFY collectDataIntervalChanged)
    Q_PROPERTY(int removeNodeInterval READ getRemoveNodeInterval WRITE setRemoveNodeInterval NOTIFY removeNodeIntervalChanged)

    Q_PROPERTY(QString network READ getNetwork WRITE setNetwork NOTIFY networkChanged)
    Q_PROPERTY(QString node READ getNode WRITE setNode NOTIFY nodeChanged)
    Q_PROPERTY(QString secureShell READ getSecureShell NOTIFY secureShellChanged)
    Q_PROPERTY(QString debugLog READ getDebugLog WRITE setDebugLog NOTIFY debugLogChanged)
    Q_PROPERTY(QString debugMpaLog READ getDebugMpaLog WRITE setDebugMpaLog NOTIFY debugMpaLogChanged)

    public:
        explicit SystemBackend(Context* context, QObject* parent = nullptr);
        ~SystemBackend();

        Q_SIGNAL void muiChanged();
        Q_SIGNAL void firmwareTypeChanged();
        Q_SIGNAL void firmwareVersionChanged();
        Q_SIGNAL void firmwareAddressChanged();
        Q_SIGNAL void serialNumberChanged();
        Q_SIGNAL void activityChanged();
        Q_SIGNAL void buildCommitChanged();
        Q_SIGNAL void qtVersionChanged();
        Q_SIGNAL void softwareTypeChanged();
        Q_SIGNAL void softwareVersionChanged();

        Q_SIGNAL void freeStorageChanged();
        Q_SIGNAL void totalStorageChanged();
        Q_SIGNAL void freeStoragePercentChanged();

        Q_SIGNAL void nameChanged();
        Q_SIGNAL void dateChanged();
        Q_SIGNAL void timeChanged();
        Q_SIGNAL void deviceIdChanged();
        Q_SIGNAL void securityCodeChanged();
        Q_SIGNAL void factoryResetChanged();
        Q_SIGNAL void canBitRateChanged();
        Q_SIGNAL void rootfsReleaseChanged();

        Q_SIGNAL void cloudUrlChanged();
        Q_SIGNAL void cloudUsernameChanged();
        Q_SIGNAL void cloudPasswordChanged();

        Q_SIGNAL void scanNetworkIntervalChanged();
        Q_SIGNAL void collectDataIntervalChanged();
        Q_SIGNAL void removeNodeIntervalChanged();

        Q_SIGNAL void secureShellChanged();
        Q_SIGNAL void debugLogChanged();
        Q_SIGNAL void debugMpaLogChanged();
        Q_SIGNAL void networkChanged();
        Q_SIGNAL void nodeChanged();

        void setName(const QString& value);
        void setDate(const QString& value);
        void setTime(const QString& value);
        void setSecurityCode(const QString& value);
        void setFactoryReset(const QString& value);

        void setCloudUrl(const QString& value);
        void setCloudUsername(const QString& value);
        void setCloudPassword(const QString& value);

        void setScanNetworkInterval(const int value);
        void setCollectDataInterval(const int value);
        void setRemoveNodeInterval(const int value);

        void setDebugLog(const QString& value);
        void setDebugMpaLog(const QString& value);
        void setNetwork(const QString& value);
        void setNode(const QString& value);

        void setDeviceId(const QString& value);
        void setSerialNumber(const QString& value);
        void setCanBitRate(const QString& value);

        int getFreeStorage() const { return this->freeStorage; }
        int getTotalStorage() const { return this->totalStorage; }
        int getFreeStoragePercent() const { return this->freeStoragePercent; }

        int getScanNetworkInterval() const { return this->scanNetworkInterval; }
        int getCollectDataInterval() const { return this->collectDataInterval; }
        int getRemoveNodeInterval() const { return this->removeNodeInterval; }

        QString getName() const { return this->name; }
        QString getDeviceId() const { return this->deviceId; }
        QString getSecurityCode() const { return this->securityCode; }
        QString getDate() const { return QDateTime::currentDateTimeUtc().toString("yyyy-MM-dd"); }
        QString getTime() const { return QDateTime::currentDateTimeUtc().toString("hh:mm:ss"); }

        QString getMUI() const { return this->mui; }
        QString getSoftwareType() const { return this->softwareType; }
        QString getSoftwareVersion() const { return this->softwareVersion; }
        QString getFirmwareType() const { return this->firmwareType; }
        QString getFirmwareVersion() const { return this->firmwareVersion; }
        QString getFirmwareAddress() const { return this->firmwareAddress; }
        QString getSerialNumber() const { return this->serialNumber; }
        QString getActivity() const { return this->activity; }
        QString getBuildCommit() const { return this->buildCommit; }
        QString getQtVersion() const { return this->qtVersion; }
        QString getFactoryReset() const { return this->factoryReset; }
        QString getCanBitRate() const { return this->canBitRate; }
        QString getRootfsRelease() const { return this->rootfsRelease; }

        QString getCloudUrl() const { return this->cloudUrl; }
        QString getCloudUsername() const { return this->cloudUsername; }
        QString getCloudPassword() const { return this->cloudPassword; }

        QString getSecureShell() const { return this->secureShell; }
        QString getDebugLog() const { return this->debugLog; }
        QString getDebugMpaLog() const { return this->debugMpaLog; }
        QString getNetwork() const { return this->network; }
        QString getNode() const { return this->node; }

    private:
        int scanNetworkInterval;
        int collectDataInterval;
        int periodicUpdateInterval;
        int removeNodeInterval;

        QString name;
        QString date;
        QString time;
        QString deviceId;

        QString activity;
        QString buildCommit;
        QString qtVersion;
        QString firmwareType;
        QString firmwareVersion;
        QString firmwareAddress;
        QString serialNumber;
        QString securityCode;
        QString factoryReset;
        QString secureShell;
        QString debugLog;
        QString debugMpaLog;
        QString network;
        QString node;
        QString mui;
        QString softwareType;
        QString softwareVersion;
        QString canBitRate;
        QString rootfsRelease;

        QString cloudUrl;
        QString cloudUsername;
        QString cloudPassword;

        qint64 freeStorage = 0;
        qint64 totalStorage = 0;
        qint64 freeStoragePercent = 0;

        Context* context = nullptr;
        QTimer* storageTimer = nullptr;
        QTimer* dateTimeTimer = nullptr;
        QTimer* secureShellTimer = nullptr;

        bool initComplete = false;
        bool authorizationSent = false;

        void writeDate();
        void writeTime();
        void generateMUI();
        void setLogFilter();
        void loadConfiguration();
        void writePasswordConfig();
        void writeHostnameConfig();
        void readCanBitRateConfig();
        void writeCanBitRateConfig();
        void readRootfsRelease();

        quint64 generateSerialNumber();

        Q_SLOT void restartCan();
        Q_SLOT void sendAuthorization();
        Q_SLOT void doFactoryReset();
        Q_SLOT void updateStorage();
        Q_SLOT void updateDateTime();
        Q_SLOT void updateSecureShell();
        Q_SLOT void configurationReady();
        Q_SLOT void firmwareUpdated(const FirmwareUpdatedEvent& event);
        Q_SLOT void updateActivity(const ActivityEvent& event);
        Q_SLOT void configurationReceived(const ConfigurationEvent& event);
        Q_SLOT void doFactoryResetChanged(const FactoryResetEvent& event);
        Q_SLOT void initializeComplete();
};
