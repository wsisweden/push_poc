#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"
#include "../../Common/Process.h"
#include "../../Common/Event/NodeAddedEvent.h"
#include "../../Common/Event/NodeRemovedEvent.h"
#include "../../Common/Event/NodeUpdatedEvent.h"

#include <QtCore/QObject>
#include <QtCore/QStringList>
#include <QtCore/QMap>

#include <QtQml/QQmlListProperty>

class CONFIG_EXPORT NodeBackend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QStringList foundNodes READ getFoundNodes NOTIFY foundNodesChanged)
    Q_PROPERTY(int foundNodesCount READ getFoundNodesCount NOTIFY foundNodesCountChanged)

    public:
        explicit NodeBackend(Context* context, QObject* parent = nullptr);

        Q_SIGNAL void foundNodesChanged();
        Q_SIGNAL void foundNodesCountChanged();

        QStringList getFoundNodes() const { return this->foundNodes; }
        int getFoundNodesCount() const { return this->foundNodesCount; }

    private:
        Process process;
        QStringList foundNodes;

        int foundNodesCount = 0;
        bool initComplete = false;

        Context* context = nullptr;

        int lookupNode(quint8 channel, quint16 panId, quint16 address);

        Q_SLOT void doFactoryReset();
        Q_SLOT void initializeComplete();
        Q_SLOT void configurationReady();
        Q_SLOT void nodeAdded(const NodeAddedEvent& event);
        Q_SLOT void nodeRemoved(const NodeRemovedEvent& event);
        Q_SLOT void nodeUpdated(const NodeUpdatedEvent& event);
};
