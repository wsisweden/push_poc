#pragma once

#include <QtCore/QtGlobal>

#if defined(CLOUD_LIBRARY)
    #define CLOUD_EXPORT //Q_DECL_EXPORT
#else
    #define CLOUD_EXPORT //Q_DECL_IMPORT
#endif
