#include "CloudWorker.h"
#include "Request/StatusRequest.h"
#include "Request/CloudPingRequest.h"
#include "Request/AuthorizationRequest.h"
#include "../Common/Constant.h"
#include "../Common/EventManager.h"
#include "../Common/Event/UplinkEvent.h"
#include "../Common/Event/AuthorizationEvent.h"
#include "../Storage/IStorage.h"
#include "../Storage/Model/ConfigurationModel.h"

#include <QtCore/QDebug>
#include <QtCore/QThread>
#include <QtCore/QProcess>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>

/**
 * @brief CloudWorker::CloudWorker Construct a new worker
 * @param context The context
 */
CloudWorker::CloudWorker(Context* context, QObject* parent)
    : QObject(parent)
{
    qInfo().noquote() << "Initialize cloud worker";

    this->system.qtVersion = qApp->applicationVersion().split(',').at(1).trimmed();
    this->system.buildCommit = qApp->applicationVersion().split(',').at(0).split('.').at(3).trimmed();

    this->system.softwareType = "11618004";
    this->system.softwareVersion = QString("%1").arg(qApp->applicationVersion().split(',').at(0).split('.').at(0).trimmed());

    this->context = context;

    connect(this->context->getEventManager(), &EventManager::authorize, this, &CloudWorker::authorize, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::factoryReset, this, &CloudWorker::doFactoryReset, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::transmitRequest, this, &CloudWorker::processRequest, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::firmwareUpdated, this, &CloudWorker::firmwareUpdated, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::nodeRestrictionChanged, this, &CloudWorker::nodeRestrictionChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::networkRestrictionChanged, this, &CloudWorker::networkRestrictionChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationChanged, this, &CloudWorker::configurationChanged, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::configurationComplete, this, &CloudWorker::configurationComplete, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::authorizationChanged, this, &CloudWorker::authorizationChanged, Qt::QueuedConnection);

    this->cloudPingTimer = new QTimer(this);
    connect(this->cloudPingTimer, &QTimer::timeout, this, &CloudWorker::cloudPing);

    this->uploadParamTimer = new QTimer(this);
    connect(this->uploadParamTimer, &QTimer::timeout, this, &CloudWorker::uploadParam);

    setupWorker();
}

/**
 * @brief CloudWorker::~CloudWorker Teardown cloud worker
 */
CloudWorker::~CloudWorker()
{
    qInfo().noquote() << "Teardown cloud worker";

    this->cloudPingTimer->stop();
    this->uploadParamTimer->stop();
}

/**
 * @brief CloudWorker::setupWorker Setup worker
 */
void CloudWorker::setupWorker()
{
    this->cloudPingTimer->stop();
    this->uploadParamTimer->stop();

    int cloudPingInterval = this->context->getStorage()->getConfigurationByName("CloudPingInterval").getValue().toInt();
    this->cloudPingTimer->setInterval(cloudPingInterval);
    this->uploadParamTimer->setInterval(cloudPingInterval * 20);
}

/**
 * @brief CloudWorker::authorize Authorize handler
 * @param authToken The authorization token
 * @param userId The user id
 */
void CloudWorker::authorize()
{
    AuthorizationRequest* request = new AuthorizationRequest();
    connect(request, &AuthorizationRequest::authorizationFinished, this, &CloudWorker::authorizationFinished);
    request->request(this->context, RequestEvent("/api/v1/login", QJsonObject()));
}

/**
 * @brief CloudWorker::authorizationFinished Authorization finished handler
 * @param authToken The authorization token
 * @param userId The user id
 */
void CloudWorker::authorizationFinished(const QString& authToken, const QString& userId)
{
    this->userId = userId;
    this->authToken = authToken;

    qInfo().noquote() << QString("Authorization received (UserId: %1, AuthToken: %2)").arg(this->userId).arg(this->authToken);

    this->cloudPingTimer->start();
}

/**
 * @brief CloudWorker::authorizationChanged Authorization changed handler
 * @param event The event
 */
void CloudWorker::authorizationChanged(const AuthorizationEvent& event)
{
    this->authorized = event.getAuthorized();
    if (!this->authorized)
    {
        this->userId.clear();
        this->authToken.clear();
    }
}

/**
 * @brief CloudWorker::cloudPing Cloud ping handler
 */
void CloudWorker::cloudPing()
{
    if (!this->authorized)
    {
        authorize();
        return;
    }

    CloudPingRequest* request = new CloudPingRequest();
    connect(request, &CloudPingRequest::cloudPingError, this, &CloudWorker::cloudPingError);
    connect(request, &CloudPingRequest::cloudPingFinished, this, &CloudWorker::cloudPingFinished);
    request->request(this->context, RequestEvent("/api/v1/ping", QJsonObject()), this->userId, this->authToken);
}

/**
 * @brief CloudWorker::cloudPingError Cloud ping error handler
 */
void CloudWorker::cloudPingError()
{
    this->cloudPingErrors++;

    qWarning() << "Cloud ping errors:" << this->cloudPingErrors;

    if (this->cloudPingErrors == MAX_CLOUD_PING_ERROR)
        qCritical() << "Max cloud ping errors reached";

    this->context->getEventManager()->emitUplinkChangedEvent(UplinkEvent(false));
}

/**
 * @brief CloudWorker::cloudPingFinished Cloud ping finished handler
 * @param uplink The uplink
 */
void CloudWorker::cloudPingFinished(const bool uplink)
{
    this->cloudPingErrors = 0;
    if (this->cloudPingTimer->isActive())
        this->context->getEventManager()->emitUplinkChangedEvent(UplinkEvent(uplink));
}

/**
 * @brief CloudWorker::processRequest Process request
 * @param package The request package
 */
void CloudWorker::processRequest(const RequestEvent& event)
{
    AbstractRequest* request = this->requestFactory.getRequest(event);
    request->request(this->context, event, this->userId, this->authToken);
}

/**
 * @brief CloudWorker::doFactoryReset Factory reset handler
 */
void CloudWorker::doFactoryReset()
{
    setupWorker();
}

/**
 * @brief CloudWorker::configurationChanged Configuration changed handler
 * @param event The event
 */
void CloudWorker::configurationChanged(const ConfigurationEvent& event)
{
    if (event.getIndex() == Param::DEVICE_ID) this->system.deviceId = event.getData().toString();
    else if (event.getIndex() == Param::FREE_STORAGE) this->system.freeStorage = event.getData().toInt();
    else if (event.getIndex() == Param::CAN_BITRATE) this->system.canBitRate = event.getData().toUInt();
    else if (event.getIndex() == Param::ROOTFS_RELEASE) this->system.rootFsRelease = event.getData().toInt();
    else if (event.getIndex() == Param::NAME) this->system.name = event.getData().toString();
    else if (event.getIndex() == Param::CLOUD_URL) this->system.cloudUrl = event.getData().toString();
    else if (event.getIndex() == Param::CLOUD_USERNAME) this->system.cloudUsername = event.getData().toString();
    else if (event.getIndex() == Param::SECURITY_CODE) this->system.securityCode = event.getData().toUInt();
    else if (event.getIndex() == Param::SCAN_NETWORK_INTERVAL) this->system.scanNetworkInterval = event.getData().toInt();
    else if (event.getIndex() == Param::COLLECT_DATA_INTERVAL) this->system.collectDataInterval = event.getData().toInt();
    else if (event.getIndex() == Param::REMOVE_NODE_INTERVAL) this->system.removeNodeInterval = event.getData().toInt();
    else if (event.getIndex() == Param::PERIODIC_UPDATE_INTERVAL) this->system.periodicUpdateInterval = event.getData().toInt();
    else if (event.getIndex() == Param::DEBUG_LOG) this->system.debugLog = event.getData().toString();
    else if (event.getIndex() == Param::DEBUG_MPA_LOG) this->system.debugMpaLog = event.getData().toString();
    else if (event.getIndex() == Param::SECURE_SHELL) this->system.secureShell = event.getData().toString();
    else if (event.getIndex() == Param::NETWORK) this->system.network = event.getData().toString();
    else if (event.getIndex() == Param::NODE) this->system.node = event.getData().toString();
    else if (event.getIndex() == Param::SERIAL_NUMBER) this->system.serialNumber = event.getData().toString();
    else if (event.getIndex() == Param::MUI) this->system.mui = event.getData().toString();
    else if (event.getIndex() == Param::FACTORY_RESET) this->system.factoryReset = event.getData().toString();
    else if (event.getIndex() == Param::FOUND_NODES) this->node.foundNodesCount = event.getData().toInt();
    else if (event.getIndex() == Param::ETHERNET_MAC) this->network.ethernetMac = event.getData().toString();
    else if (event.getIndex() == Param::ETHERNET_ADDRESS) this->network.ethernetAddress = event.getData().toString();
    else if (event.getIndex() == Param::ETHERNET_STATIC) this->network.ethernetStatic = event.getData().toString();
    else if (event.getIndex() == Param::ETHERNET_NETMASK) this->network.ethernetNetmask = event.getData().toString();
    else if (event.getIndex() == Param::ETHERNET_GATEWAY) this->network.ethernetGateway = event.getData().toString();
    else if (event.getIndex() == Param::WIRELESS_MAC) this->network.wirelessMac = event.getData().toString();
    else if (event.getIndex() == Param::WIRELESS_SSID) this->network.wirelessSsid = event.getData().toString();
    else if (event.getIndex() == Param::WIRELESS_LINK_QUALITY) this->network.wirelessLinkQuality = event.getData().toInt();
    else if (event.getIndex() == Param::WIRELESS_SIGNAL_STRENGTH) this->network.wirelessSignalStrength = event.getData().toInt();
    else if (event.getIndex() == Param::DATETIME)
    {
        this->system.date = QDateTime::fromSecsSinceEpoch(event.getData().toLongLong()).currentDateTimeUtc().toString("yyyy-MM-dd");
        this->system.time = QDateTime::fromSecsSinceEpoch(event.getData().toLongLong()).currentDateTimeUtc().toString("hh:mm:ss");
    }
}

/**
 * @brief CloudWorker::firmwareUpdated Firmware updated handler
 * @param event The event
 */
void CloudWorker::firmwareUpdated(const FirmwareUpdatedEvent& event)
{
    this->system.firmwareType = QString::number(event.getFirmwareType());
    this->system.firmwareVersion = QString::number(event.getFirmwareVersion());
    this->system.firmwareAddress = QString::number(event.getFirmwareAddress());
}

/**
 * @brief CloudWorker::networkRestrictionChanged Network restriction changed handler
 * @param event The event
 */
void CloudWorker::networkRestrictionChanged(const NetworkRestrictionEvent& event)
{
    this->node.networkRestriction = event.getNetworkRestriction();
}

/**
 * @brief CloudWorker::nodeRestrictionChanged Node restriction changed handler
 * @param event The event
 */
void CloudWorker::nodeRestrictionChanged(const NodeRestrictionEvent& event)
{
    this->node.nodeRestriction = event.getNodeRestriction();
}

/**
 * @brief CloudWorker::configurationComplete Configuration complete handler
 * @param event The event
 */
void CloudWorker::configurationComplete()
{
    this->uploadParamTimer->start();
}

/**
 * @brief CloudWorker::uploadParam Upload paramters to the cloud handler
 */
void CloudWorker::uploadParam()
{
    uploadConfigParam();
    uploadStatusParam();
}

/**
 * @brief CloudWorker::uploadConfigParam Upload configuration paramters to the cloud handler
 */
void CloudWorker::uploadConfigParam()
{
    QJsonObject payload;

    QJsonObject device;
    device.insert("deviceName", this->system.name);
    device.insert("deviceId", this->system.deviceId);
    device.insert("securityCode2", this->system.securityCode);
    payload.insert("device", device);

    QJsonObject credential;
    credential.insert("cloudUrl", this->system.cloudUrl);
    credential.insert("cloudUsername", this->system.cloudUsername);
    payload.insert("credential", credential);

    QJsonObject interval;
    interval.insert("scanNetworkInterval", this->system.scanNetworkInterval);
    interval.insert("collectDataInterval", this->system.collectDataInterval);
    interval.insert("periodicUpdateInterval", this->system.periodicUpdateInterval);
    interval.insert("removeNodeInterval", this->system.removeNodeInterval);
    payload.insert("interval", interval);

    QJsonObject log;
    log.insert("debugLog", QVariant(this->system.debugLog).toBool());
    log.insert("debugMpaLog", QVariant(this->system.debugMpaLog).toBool());
    payload.insert("log", log);

    QJsonObject ethernet;
    ethernet.insert("ethernetAddress", this->network.ethernetAddress);
    ethernet.insert("ethernetStatic", QVariant(this->network.ethernetStatic).toBool());
    ethernet.insert("ethernetNetmask", this->network.ethernetNetmask);
    ethernet.insert("ethernetGateway", this->network.ethernetGateway);

    QJsonObject wireless;
    wireless.insert("wirelessAddress", this->network.wirelessAddress);
    wireless.insert("wirelessNetmask", this->network.wirelessNetmask);
    wireless.insert("wirelessSsid", this->network.wirelessSsid);

    QJsonObject can;
    can.insert("canBitRate", (qint64)this->system.canBitRate);

    QVariantMap network;
    network.insert("ethernet", ethernet);
    network.insert("wireless", wireless);
    network.insert("can", can);
    payload.insert("network", QJsonObject::fromVariantMap(network));

    QJsonObject restriction;
    restriction.insert("networkRestriction", this->node.networkRestriction);
    restriction.insert("nodeRestriction", this->node.nodeRestriction);
    payload.insert("restriction", restriction);

    payload.insert("timestamp", QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ"));

    StatusRequest* request = new StatusRequest();
    request->request(this->context, RequestEvent(QString("/api/v1/devices/%1/config").arg(this->system.mui), payload), this->userId, this->authToken);
}

/**
 * @brief CloudWorker::uploadStatusParam Upload status paramters to the cloud handler
 */
void CloudWorker::uploadStatusParam()
{
    QJsonObject payload;

    QJsonObject device;
    device.insert("deviceStatus", QJsonArray());
    device.insert("activeAlarms", QJsonArray());
    device.insert("serialNumber", this->system.serialNumber);
    device.insert("freeStorage", this->system.freeStorage);
    device.insert("foundNodes", this->node.foundNodesCount);
    device.insert("sshActive", QVariant(this->system.secureShell).toBool());
    device.insert("buildCommit", this->system.buildCommit);
    device.insert("qtVersion", this->system.qtVersion);
    device.insert("rootFsRelease", this->system.rootFsRelease);
    payload.insert("device", device);

    QJsonObject ethernet;
    ethernet.insert("ethernetMac", this->network.ethernetMac);

    QJsonObject wireless;
    wireless.insert("wirelessMac", this->network.wirelessMac);
    wireless.insert("wirelessSignalStrength", this->network.wirelessSignalStrength);
    wireless.insert("wirelessLinkQuality", this->network.wirelessLinkQuality);

    QVariantMap network;
    network.insert("ethernet", ethernet);
    network.insert("wireless", wireless);
    payload.insert("network", QJsonObject::fromVariantMap(network));

    QJsonObject software;
    software.insert("softwareType", (qint64)this->system.softwareType.toULongLong());
    software.insert("softwareVersion", (qint64)this->system.softwareVersion.toULongLong());
    payload.insert("software", software);

    QJsonObject firmware;
    firmware.insert("firmwareType", (qint64)this->system.firmwareType.toULongLong());
    firmware.insert("firmwareVersion", (qint64)this->system.firmwareVersion.toULongLong());
    payload.insert("firmware", firmware);

    payload.insert("timestamp", QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddThh:mm:ss.zzzZ"));

    StatusRequest* request = new StatusRequest();
    request->request(this->context, RequestEvent(QString("/api/v1/devices/%1/status").arg(this->system.mui), payload), this->userId, this->authToken);
}
