QT += core

CONFIG += c++11 staticlib

TARGET = cloud
TEMPLATE = lib

DEFINES += CLOUD_LIBRARY

HEADERS += \
    Shared.h \
    CloudWorker.h \
    Request/AbstractRequest.h \
    Request/AuthorizationRequest.h \
    Request/EventLogRequest.h \
    Request/HistoryLogRequest.h \
    Request/InstantLogRequest.h \
    Request/RequestFactory.h \
    Request/RequestPackage.h \
    Request/StatusRequest.h \
    Request/ConfigRequest.h \
    Request/EventLogIndexRequest.h \
    Request/HistoryLogIndexRequest.h \
    Request/InstantLogIndexRequest.h \
    Request/DevicePingRequest.h \
    Request/CloudPingRequest.h

SOURCES += \
    CloudWorker.cpp \
    Request/AbstractRequest.cpp \
    Request/AuthorizationRequest.cpp \
    Request/EventLogRequest.cpp \
    Request/HistoryLogRequest.cpp \
    Request/InstantLogRequest.cpp \
    Request/RequestFactory.cpp \
    Request/RequestPackage.cpp \
    Request/ConfigRequest.cpp \
    Request/StatusRequest.cpp \
    Request/EventLogIndexRequest.cpp \
    Request/HistoryLogIndexRequest.cpp \
    Request/InstantLogIndexRequest.cpp \
    Request/DevicePingRequest.cpp \
    Request/CloudPingRequest.cpp

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:QMAKE_LFLAGS += -Wl,--start-group

DEPENDPATH += $$OUT_PWD/../Common $$PWD/../Common
INCLUDEPATH += $$OUT_PWD/../Common $$PWD/../Common
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Common/release/ -lcommon
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Common/debug/ -lcommon
else:macx:LIBS += -L$$OUT_PWD/../Common/ -lcommon
else:unix:LIBS += -L$$OUT_PWD/../Common/ -lcommon

DEPENDPATH += $$OUT_PWD/../Storage $$PWD/../Storage
INCLUDEPATH += $$OUT_PWD/../Storage $$PWD/../Storage
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Storage/debug/ -lstorage
else:macx:LIBS += -L$$OUT_PWD/../Storage/ -lstorage
else:unix:LIBS += -L$$OUT_PWD/../Storage/ -lstorage
