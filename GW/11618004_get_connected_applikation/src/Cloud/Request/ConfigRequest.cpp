#include "ConfigRequest.h"
#include "../../Storage/IStorage.h"

/**
 * @brief ConfigRequest::ConfigRequest Construct a new config request
 */
ConfigRequest::ConfigRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief ConfigRequest::request Request device config
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void ConfigRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    this->context = context;

    post(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), event.getPayload(), userId, authToken);
}

/**
 * @brief ConfigRequest::requestError Request error handler
 * @param reply The reply
 */
void ConfigRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief ConfigRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void ConfigRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    Q_UNUSED(reply);
    Q_UNUSED(data);
}
