#include "EventLogIndexRequest.h"
#include "../../Storage/IStorage.h"
#include "../../Common/Constant.h"
#include "../../Common/Event/IndexEvent.h"

#include "QtCore/QJsonArray"
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>

/**
 * @brief EventLogIndexRequest::EventLogIndexRequest Construct a new event log request
 */
EventLogIndexRequest::EventLogIndexRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief EventLogIndexRequest::request Request event log
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void EventLogIndexRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    this->context = context;
    this->channel = (quint8)event.getPayload().value("channel").toString().toUInt();
    this->panId = (quint16)event.getPayload().value("panId").toString().toUInt();
    this->address = (quint16)event.getPayload().value("address").toString().toUInt();

    get(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), userId, authToken);
}

/**
 * @brief EventLogIndexRequest::requestError Request error handler
 * @param reply The reply
 */
void EventLogIndexRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief EventLogIndexRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void EventLogIndexRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    if (reply->request().url().toString().endsWith("/logs/event/indexes"))
    {
        QJsonObject json = QJsonDocument::fromJson(data).object();
        if (json.value("status").toString() == "success")
            this->context->getEventManager()->emitIndexReceivedEvent(IndexEvent(this->channel, this->panId, this->address, json.value("data").toArray(), LogType::EVENT));
        else
        {
            // If node do not exists, then emit an empty array which will reset index to zero.
            if (json.value("message").toString() == "Item not found")
                this->context->getEventManager()->emitIndexReceivedEvent(IndexEvent(this->channel, this->panId, this->address, QJsonArray(), LogType::EVENT));
        }
    }
}
