#include "CloudPingRequest.h"
#include "../../Storage/IStorage.h"

#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QCryptographicHash>
#include <QtCore/QProcess>

/**
 * @brief CloudPingRequest::CloudPingRequest Construct a new cloud ping request

 */
CloudPingRequest::CloudPingRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief CloudPingRequest::request Request clound ping
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void CloudPingRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    Q_UNUSED(event);

    this->context = context;

    post(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), event.getPayload(), userId, authToken);
}

/**
 * @brief CloudPingRequest::requestError Request error handler
 * @param reply The reply
 */
void CloudPingRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);

    emit cloudPingError();
}

/**
 * @brief CloudPingRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void CloudPingRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    if (reply->request().url().toString().endsWith("/api/v1/ping"))
    {
        QJsonObject json = QJsonDocument::fromJson(data).object();
        emit cloudPingFinished((json.value("status").toString() == "success") ? true : false);
    }
}
