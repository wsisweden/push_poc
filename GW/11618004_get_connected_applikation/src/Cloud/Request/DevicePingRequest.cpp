#include "DevicePingRequest.h"
#include "../../Storage/IStorage.h"

#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QCryptographicHash>
#include <QtCore/QProcess>

/**
 * @brief DevicePingRequest::DevicePingRequest Construct a new device ping request

 */
DevicePingRequest::DevicePingRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief DevicePingRequest::request Request device ping
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void DevicePingRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    Q_UNUSED(event);

    this->context = context;

    get(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), userId, authToken);
}

/**
 * @brief DevicePingRequest::requestError Request error handler
 * @param reply The reply
 */
void DevicePingRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief DevicePingRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void DevicePingRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    Q_UNUSED(reply);
    Q_UNUSED(data);
}
