#include "AuthorizationRequest.h"
#include "../../Storage/IStorage.h"
#include "../../Common/EventManager.h"
#include "../../Common/Event/AuthorizationEvent.h"

#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QCryptographicHash>

/**
 * @brief AuthorizationRequest::AuthorizationRequest Construct a new authorization request

 */
AuthorizationRequest::AuthorizationRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief AuthorizationRequest::request Request authorization
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void AuthorizationRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    Q_UNUSED(event);

    this->context = context;

    QJsonObject payload;
    payload.insert("username", this->context->getStorage()->getConfigurationByName("CloudUsername").getValue());
    payload.insert("password", QString(QCryptographicHash::hash(this->context->getStorage()->getConfigurationByName("CloudPassword").getValue().toUtf8(), QCryptographicHash::Sha256).toHex()));
    payload.insert("hashed", true);

    post(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), payload, userId, authToken);
}

/**
 * @brief AuthorizationRequest::requestError Request error handler
 * @param reply The reply
 */
void AuthorizationRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief AuthorizationRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void AuthorizationRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    if (reply->request().url().toString().endsWith("/login"))
    {
        QString userId;
        QString authToken;
        bool response = !data.isEmpty();

        QJsonObject json = QJsonDocument::fromJson(data).object();
        if (json.value("status").toString() == "success")
        {
            userId = json.value("data").toObject().value("userId").toString();
            authToken = json.value("data").toObject().value("authToken").toString();

            emit authorizationFinished(authToken, userId, this);
        }

        this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(!userId.isEmpty() && !authToken.isEmpty(), response));
    }
}
