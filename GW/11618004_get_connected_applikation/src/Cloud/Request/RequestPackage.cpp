#include "RequestPackage.h"

/**
 * @brief RequestPackage::RequestPackage Construct a new request package
 * @param other The other
 */
RequestPackage::RequestPackage(const RequestPackage& other)
{
    this->payload = other.getPayload();
    this->endpoint = other.getEndpoint();
}

/**
 * @brief RequestPackage::RequestPackage Construct a new request package
 * @param endpoint The endpoint
 * @param payload The payload
 */
RequestPackage::RequestPackage(const QString& endpoint, const QJsonObject& payload)
{
    this->payload = payload;
    this->endpoint = endpoint;
}
