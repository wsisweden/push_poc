#include "AbstractRequest.h"

#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QJsonDocument>
#include <QtCore/QUrl>
#include <QtCore/QTextStream>

/**
 * @brief AbstractRequest::AbstractRequest Construct a new abstract request
 */
AbstractRequest::AbstractRequest(QObject* parent)
    : QObject(parent) {}

/**
 * @brief AbstractRequest::post Send a POST request
 * @param url The URL
 * @param endpoint The endpoint
 * @param payload The payload
 * @param userId The user id
 * @param authToken The authorization token
 */
void AbstractRequest::post(const QString& url, const QString& endpoint, const QJsonObject& payload, const QString& userId, const QString& authToken)
{
    this->payload = payload;

    QNetworkRequest request(QUrl(url + endpoint));
    prepareRequest(&request, userId, authToken);

    this->networkManager = new QNetworkAccessManager(this);
    connect(this->networkManager, &QNetworkAccessManager::finished, this, &AbstractRequest::finished);
    connect(this->networkManager, &QNetworkAccessManager::sslErrors, this, &AbstractRequest::sslErrors);

    printRequestDebug(request);

    this->networkManager->post(request, QJsonDocument(this->payload).toJson());
    this->context->getEventManager()->emitHttpRequestEvent();
}

/**
 * @brief AbstractRequest::send Send a GET request
 * @param url The URL
 * @param endpoint The endpoint
 * @param userId The user id
 * @param authToken The authorization token
 */
void AbstractRequest::get(const QString& url, const QString& endpoint, const QString& userId, const QString& authToken)
{
    this->payload = QJsonObject();

    QNetworkRequest request(QUrl(url + endpoint));
    prepareRequest(&request, userId, authToken);

    this->networkManager = new QNetworkAccessManager(this);
    connect(this->networkManager, &QNetworkAccessManager::finished, this, &AbstractRequest::finished);
    connect(this->networkManager, &QNetworkAccessManager::sslErrors, this, &AbstractRequest::sslErrors);

    printRequestDebug(request);

    this->networkManager->get(request);
    this->context->getEventManager()->emitHttpRequestEvent();
}

/**
 * @brief AbstractRequest::prepareRequest Prepare network request
 * @param request The request
 */
void AbstractRequest::prepareRequest(QNetworkRequest* request, const QString& userId, const QString& authToken)
{
    request->setRawHeader("Content-Type", "application/json; charset=utf-8");

    if (!userId.isEmpty())
        request->setRawHeader("X-User-Id", userId.toUtf8());

    if (!authToken.isEmpty())
        request->setRawHeader("X-Auth-Token", authToken.toUtf8());
}

/**
 * @brief AbstractRequest::sslErrors SSL error handler
 */
void AbstractRequest::sslErrors(QNetworkReply* reply, const QList<QSslError>& errors)
{
    Q_UNUSED(reply);
    Q_UNUSED(errors);

    //reply->ignoreSslErrors();
}

/**
 * @brief AbstractRequest::finished POST request handler
 * @param reply The reply
 */
void AbstractRequest::finished(QNetworkReply* reply)
{
    QByteArray data = reply->readAll();

    QJsonDocument document = QJsonDocument::fromJson(data);
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    QVariant reasonPhrase = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute);

    if (reply->error() == QNetworkReply::NetworkError::TimeoutError ||
        reply->error() == QNetworkReply::NetworkError::HostNotFoundError)
    {
        qWarning().noquote() << "Network error:" << reply->errorString();

        QFile file("/var/log/gateway.network-error");
        if (file.open(QIODevice::ReadWrite))
        {
            QString errors = file.readAll();
            if (errors.isEmpty())
                errors = "1\n";

            errors = QString::number(errors.toInt() + 1) + "\n";

            file.seek(0);
            file.write(errors.toLocal8Bit());
            file.close();
        }

        requestError(reply);
    }
    else
    {
        if (statusCode.toInt() >= 400)
            if (statusCode.toInt() >= 400)
        {
            qWarning().noquote() << "Request failed:" << statusCode.toInt() << reasonPhrase.toString();

            if (!document.isEmpty())
                qWarning().noquote() << "Response:" << QString(document.toJson(QJsonDocument::Compact));

            printRequestWarning(reply->request());

            if (reasonPhrase.toString() == "Unauthorized")
                this->context->getEventManager()->emitAuthorizationChangedEvent(AuthorizationEvent(false, true));

            if (!reply->request().url().toString().endsWith("/login") && !reply->request().url().toString().endsWith("/ping"))
                this->context->getEventManager()->emitHttpRequestFailedEvent();
        }

        requestFinished(reply, data);
    }

    reply->deleteLater();
    this->networkManager->deleteLater();
    this->deleteLater();
}

/**
 * @brief AbstractRequest::printRequestDebug Print request to debug channel
 * @param request The request
 * @param data The request data
 */
void AbstractRequest::printRequestDebug(QNetworkRequest request)
{
    qDebug().noquote() << "Url:" << request.url().toString();

    foreach (QByteArray rawHeader, request.rawHeaderList())
        qDebug().noquote() << rawHeader.trimmed().append(":") << request.rawHeader(rawHeader).trimmed();

    if (!this->payload.isEmpty())
        qDebug().noquote() << QString(QJsonDocument(this->payload).toJson(QJsonDocument::Compact));
}

/**
 * @brief AbstractRequest::printRequestWarning Print request to debug channel
 * @param request The request
 * @param data The request data
 */
void AbstractRequest::printRequestWarning(QNetworkRequest request)
{
    qWarning().noquote() << "Url:" << request.url().toString();

    foreach (QByteArray rawHeader, request.rawHeaderList())
        qWarning().noquote() << rawHeader.trimmed().append(":") << request.rawHeader(rawHeader).trimmed();

    if (!this->payload.isEmpty())
        qWarning().noquote() << QString(QJsonDocument(this->payload).toJson(QJsonDocument::Compact));
}
