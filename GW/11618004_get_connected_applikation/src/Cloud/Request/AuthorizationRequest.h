#pragma once

#include "../Shared.h"
#include "AbstractRequest.h"
#include "../../Common/Context.h"
#include "../../Common/Event/RequestEvent.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QByteArray>

#include <QtNetwork/QNetworkReply>

class CLOUD_EXPORT AuthorizationRequest : public AbstractRequest
{
    Q_OBJECT

    public:
        explicit AuthorizationRequest(QObject* parent = nullptr);
        ~AuthorizationRequest() {}

        void request(Context* context, const RequestEvent& event, const QString& userId = "", const QString& authToken = "");

        Q_SIGNAL void authorizationFinished(const QString& authToken, const QString& userId, QObject* sender);

    protected:
        void requestError(QNetworkReply* reply);
        void requestFinished(QNetworkReply* reply, const QByteArray data);
};
