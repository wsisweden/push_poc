#include "EventLogRequest.h"
#include "../../Storage/IStorage.h"

/**
 * @brief EventLogRequest::EventLogRequest Construct a new event log request
 */
EventLogRequest::EventLogRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief EventLogRequest::request Request event log
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void EventLogRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    this->context = context;

    // CGW-166: There is a known issue with corrupted log index.
    quint32 index = (quint32)event.getPayload().value("index").toString().toUInt();
    if (index != 0xffffffff)
        post(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), event.getPayload(), userId, authToken);
}

/**
 * @brief EventLogRequest::requestError Request error handler
 * @param reply The reply
 */
void EventLogRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief EventLogRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void EventLogRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    Q_UNUSED(reply);
    Q_UNUSED(data);
}
