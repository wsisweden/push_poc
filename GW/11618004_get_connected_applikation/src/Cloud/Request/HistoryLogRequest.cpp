#include "HistoryLogRequest.h"
#include "../../Storage/IStorage.h"

/**
 * @brief HistoryLogRequest::HistoryLogRequest Construct a new history log request
 * @param storage The storage interface
 */
HistoryLogRequest::HistoryLogRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief HistoryLogRequest::request Request history log
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void HistoryLogRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    this->context = context;

    // CGW-166: There is a known issue with corrupted log index.
    quint32 index = (quint32)event.getPayload().value("index").toString().toUInt();
    if (index != 0xffffffff)
        post(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), event.getPayload(), userId, authToken);
}

/**
 * @brief HistoryLogRequest::requestError Request error handler
 * @param reply The reply
 */
void HistoryLogRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief HistoryLogRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void HistoryLogRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    Q_UNUSED(reply);
    Q_UNUSED(data);
}
