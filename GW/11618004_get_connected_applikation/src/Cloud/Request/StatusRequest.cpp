#include "StatusRequest.h"
#include "../../Storage/IStorage.h"

/**
 * @brief StatusRequest::StatusRequest Construct a new status request
 */
StatusRequest::StatusRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief StatusRequest::request Request device status
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void StatusRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    this->context = context;

    post(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), event.getPayload(), userId, authToken);
}


/**
 * @brief StatusRequest::requestError Request error handler
 * @param reply The reply
 */
void StatusRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief StatusRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void StatusRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    Q_UNUSED(reply);
    Q_UNUSED(data);
}
