#include "InstantLogRequest.h"
#include "../../Storage/IStorage.h"

/**
 * @brief InstantLogRequest::InstantLogRequest Construct a new instant log request
 * @param storage The storage interface
 */
InstantLogRequest::InstantLogRequest(QObject* parent)
    : AbstractRequest(parent) {}

/**
 * @brief InstantLogRequest::request Request instant log
 * @param context The context
 * @param event The request event
 * @param userId The user id
 * @param authToken The authorize token
 */
void InstantLogRequest::request(Context* context, const RequestEvent& event, const QString& userId, const QString& authToken)
{
    this->context = context;

    // CGW-166: There is a known issue with corrupted log index.
    quint32 index = (quint32)event.getPayload().value("index").toString().toUInt();
    if (index != 0xffffffff)
        post(this->context->getStorage()->getConfigurationByName("CloudUrl").getValue(), event.getEndpoint(), event.getPayload(), userId, authToken);
}

/**
 * @brief InstantLogRequest::requestError Request error handler
 * @param reply The reply
 */
void InstantLogRequest::requestError(QNetworkReply* reply)
{
    Q_UNUSED(reply);
}

/**
 * @brief InstantLogRequest::requestFinished Request finished handler
 * @param reply The reply
 * @param data The data
 */
void InstantLogRequest::requestFinished(QNetworkReply* reply, const QByteArray data)
{
    Q_UNUSED(reply);
    Q_UNUSED(data);
}
