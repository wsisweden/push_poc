#pragma once

#include "../Shared.h"
#include "AbstractRequest.h"
#include "../../Common/Context.h"
#include "../../Common/Event/RequestEvent.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QByteArray>

#include <QtNetwork/QNetworkReply>

class CLOUD_EXPORT EventLogRequest : public AbstractRequest
{
    Q_OBJECT

    public:
        explicit EventLogRequest(QObject* parent = nullptr);
        ~EventLogRequest() {}

        void request(Context* context, const RequestEvent& event, const QString& userId = "", const QString& authToken = "");

    protected:
        void requestError(QNetworkReply* reply);
        void requestFinished(QNetworkReply* reply, const QByteArray data);
};
