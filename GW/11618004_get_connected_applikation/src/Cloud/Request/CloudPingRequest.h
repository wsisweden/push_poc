#pragma once

#include "../Shared.h"
#include "AbstractRequest.h"
#include "../../Common/Context.h"
#include "../../Common/Event/RequestEvent.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QByteArray>

#include <QtNetwork/QNetworkReply>

class CLOUD_EXPORT CloudPingRequest : public AbstractRequest
{
    Q_OBJECT

    public:
        explicit CloudPingRequest(QObject* parent = nullptr);
        ~CloudPingRequest() {}

        void request(Context* context, const RequestEvent& event, const QString& userId = "", const QString& authToken = "");

        Q_SIGNAL void cloudPingError();
        Q_SIGNAL void cloudPingFinished(const bool uplink);

    protected:
        void requestError(QNetworkReply* reply);
        void requestFinished(QNetworkReply* reply, const QByteArray data);
};
