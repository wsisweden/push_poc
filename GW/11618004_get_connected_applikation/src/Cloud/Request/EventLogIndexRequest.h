#pragma once

#include "../Shared.h"
#include "AbstractRequest.h"
#include "../../Common/Context.h"
#include "../../Common/Event/RequestEvent.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QByteArray>

#include <QtNetwork/QNetworkReply>

class CLOUD_EXPORT EventLogIndexRequest : public AbstractRequest
{
    Q_OBJECT

    public:
        explicit EventLogIndexRequest(QObject* parent = nullptr);
        ~EventLogIndexRequest() {}

        void request(Context* context, const RequestEvent& event, const QString& userId = "", const QString& authToken = "");

    protected:
        void requestError(QNetworkReply* reply);
        void requestFinished(QNetworkReply* reply, const QByteArray data);

    private:
        quint8 channel;
        quint16 panId;
        quint16 address;
};
