#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QJsonObject>
#include <QtCore/QMetaType>

class CLOUD_EXPORT RequestPackage
{
    public:
        explicit RequestPackage() {}
        explicit RequestPackage(const RequestPackage& other);
        explicit RequestPackage(const QString& endpoint, const QJsonObject& payload);
        ~RequestPackage() {}

        QString getEndpoint() const { return this->endpoint; }
        QJsonObject getPayload() const { return this->payload; }

    private:
        QString endpoint;
        QJsonObject payload;
};

Q_DECLARE_METATYPE(RequestPackage)
