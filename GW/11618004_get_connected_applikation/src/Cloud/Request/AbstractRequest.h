#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"
#include "../../Common/Event/RequestEvent.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QJsonObject>
#include <QtCore/QByteArray>

#include <QtNetwork/QSslError>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkAccessManager>

class CLOUD_EXPORT AbstractRequest : public QObject
{
    Q_OBJECT

    public:
        explicit AbstractRequest(QObject* parent = nullptr);
        virtual ~AbstractRequest() {}

        virtual void requestError(QNetworkReply* reply) = 0;
        virtual void requestFinished(QNetworkReply* reply, const QByteArray data) = 0;
        virtual void request(Context* context, const RequestEvent& event, const QString& userId = "", const QString& authToken = "") = 0;

    protected:
        Context* context = nullptr;

        void get(const QString& url, const QString& endpoint, const QString& userId, const QString& authToken);
        void post(const QString& url, const QString& endpoint, const QJsonObject& payload, const QString& userId, const QString& authToken);

    private:
        QJsonObject payload;
        QNetworkAccessManager* networkManager;

        void printRequestDebug(QNetworkRequest request);
        void printRequestWarning(QNetworkRequest request);
        void prepareRequest(QNetworkRequest* request, const QString& userId, const QString& authToken);

        Q_SLOT void finished(QNetworkReply*);
        Q_SLOT void sslErrors(QNetworkReply* reply, const QList<QSslError>& errors);
};
