#pragma once

#include "../Shared.h"
#include "AbstractRequest.h"
#include "../../Common/Event/RequestEvent.h"

#include <QtCore/QObject>

class CLOUD_EXPORT RequestFactory : public QObject
{
    Q_OBJECT

    public:
        explicit RequestFactory(QObject* parent = nullptr);
        ~RequestFactory() {}

        AbstractRequest* getRequest(const RequestEvent& event);
};
