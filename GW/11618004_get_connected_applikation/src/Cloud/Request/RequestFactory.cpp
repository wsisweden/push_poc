#include "RequestFactory.h"
#include "DevicePingRequest.h"
#include "StatusRequest.h"
#include "ConfigRequest.h"
#include "EventLogRequest.h"
#include "InstantLogRequest.h"
#include "HistoryLogRequest.h"
#include "EventLogIndexRequest.h"
#include "InstantLogIndexRequest.h"
#include "HistoryLogIndexRequest.h"

/**
 * @brief RequestFactory::RequestFactory Construct a new abstract request
 */
RequestFactory::RequestFactory(QObject* parent)
    : QObject(parent) {}

/**
 * @brief RequestFactory::getRequest Get request with specific id
 * @param package The request package
 */
AbstractRequest* RequestFactory::getRequest(const RequestEvent& event)
{
    if (event.getEndpoint().endsWith("/ping")) return new DevicePingRequest();
    else if (event.getEndpoint().endsWith("/status")) return new StatusRequest();
    else if (event.getEndpoint().endsWith("/config")) return new ConfigRequest();
    else if (event.getEndpoint().endsWith("/logs/event")) return new EventLogRequest();
    else if (event.getEndpoint().endsWith("/logs/instant")) return new InstantLogRequest();
    else if (event.getEndpoint().endsWith("/logs/history")) return new HistoryLogRequest();
    else if (event.getEndpoint().endsWith("/logs/history/indexes")) return new HistoryLogIndexRequest();
    else if (event.getEndpoint().endsWith("/logs/event/indexes")) return new EventLogIndexRequest();
    else if (event.getEndpoint().endsWith("/logs/instant/indexes")) return new InstantLogIndexRequest();
    else return nullptr;
}
