#pragma once

#include "Shared.h"
#include "Request/RequestFactory.h"
#include "../Common/Context.h"
#include "../Common/Event/RequestEvent.h"
#include "../Common/Event/ConfigurationEvent.h"
#include "../Common/Event/FirmwareUpdatedEvent.h"
#include "../Common/Event/NodeRestrictionEvent.h"
#include "../Common/Event/NetworkRestrictionEvent.h"
#include "../Common/Event/AuthorizationEvent.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>

class CLOUD_EXPORT CloudWorker : public QObject
{
    Q_OBJECT

    public:
        explicit CloudWorker(Context* context, QObject* parent = nullptr);
        ~CloudWorker();

    private:
        struct Node
        {
            int foundNodesCount;

            QString networkRestriction;
            QString nodeRestriction;
        } node;

        struct Network
        {
            int wirelessLinkQuality;
            int wirelessSignalStrength;

            QString ethernetMac;
            QString ethernetAddress;
            QString ethernetStatic;
            QString ethernetNetmask;
            QString ethernetGateway;

            QString wirelessMac;
            QString wirelessAddress;
            QString wirelessNetmask;
            QString wirelessSsid;
        } network;

        struct System
        {
            quint16 securityCode;
            quint32 canBitRate;
            qint64 freeStorage;

            int scanNetworkInterval;
            int collectDataInterval;
            int periodicUpdateInterval;
            int removeNodeInterval;
            int rootFsRelease;

            QString name;
            QString date;
            QString time;
            QString deviceId;
            QString buildCommit;
            QString qtVersion;
            QString firmwareType;
            QString firmwareVersion;
            QString firmwareAddress;
            QString serialNumber;
            QString factoryReset;
            QString secureShell;
            QString debugLog;
            QString debugMpaLog;
            QString network;
            QString node;
            QString mui;
            QString softwareType;
            QString softwareVersion;
            QString cloudUrl;
            QString cloudUsername;
        } system;

        QString userId;
        QString authToken;
        RequestFactory requestFactory;

        int cloudPingErrors = 0;
        bool authorized = false;

        Context* context = nullptr;
        QTimer* cloudPingTimer = nullptr;
        QTimer* uploadParamTimer = nullptr;

        const int MAX_CLOUD_PING_ERROR = 60;

        void setupWorker();
        void uploadConfigParam();
        void uploadStatusParam();

        Q_SLOT void cloudPing();
        Q_SLOT void authorize();
        Q_SLOT void uploadParam();
        Q_SLOT void doFactoryReset();
        Q_SLOT void configurationComplete();
        Q_SLOT void cloudPingError();
        Q_SLOT void cloudPingFinished(const bool uplink);
        Q_SLOT void processRequest(const RequestEvent& event);
        Q_SLOT void configurationChanged(const ConfigurationEvent& event);
        Q_SLOT void firmwareUpdated(const FirmwareUpdatedEvent& event);
        Q_SLOT void authorizationFinished(const QString& authToken, const QString& userId);
        Q_SLOT void networkRestrictionChanged(const NetworkRestrictionEvent& event);
        Q_SLOT void nodeRestrictionChanged(const NodeRestrictionEvent& event);
        Q_SLOT void authorizationChanged(const AuthorizationEvent& event);
};
