#include "Process.h"

#include <QtCore/QDebug>

/**
 * @brief Process::Process Construct a new process
 */
Process::Process(bool autoRestart, QObject* parent)
    : QObject(parent)
{
    this->autoRestart = autoRestart;

    connect(&this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), this, &Process::finished);
}

/**
 * @brief Process::close Terminate process
 */
void Process::terminate()
{
    this->process.disconnect(); // Disconnect all events.
    this->process.close();
}

/**
 * @brief Process::run Run process
 * @param program The program to run
 */
void Process::run(const QString& program)
{
    qDebug().noquote() << QString("Running command: %1").arg(program);

    this->process.start(program);
}

/**
 * @brief Process::finished Process finished handler
 * @param exitCode The exit code
 * @param exitStatus The exit status
 */
void Process::finished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitCode);

    if (exitStatus == QProcess::CrashExit)
    {
        qCritical().noquote() << QString("The application crashed with error: %1").arg(this->process.errorString());

        // Application crashed, restart and carry on as usual.
        if (this->autoRestart && this->tries-- > 0)
            run(this->process.program());

        return;
    }
}
