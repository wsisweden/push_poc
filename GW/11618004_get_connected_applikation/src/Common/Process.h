#pragma once

#include "Shared.h"

#include <QtCore/QObject>
#include <QtCore/QProcess>

class COMMON_EXPORT Process : public QObject
{
    Q_OBJECT

    public:
        explicit Process(bool autoRestart = false, QObject* parent = 0);

        void terminate();
        void run(const QString& program);

    private:
        int tries = 5;
        QProcess process;
        bool autoRestart = false;

        Q_SLOT void finished(int exitCode, QProcess::ExitStatus exitStatus);
};
