#include "FactoryResetEvent.h"

/**
 * @brief FactoryResetEvent::FactoryResetEvent Construct a new factory reset event
 * @param other The other
 */
FactoryResetEvent::FactoryResetEvent(const FactoryResetEvent& other)
{
    this->factoryReset = other.getFactoryReset();
}

/**
 * @brief FactoryResetEvent::FactoryResetEvent Construct a new factory reset event
 * @param uplink The uplink
 */
FactoryResetEvent::FactoryResetEvent(const bool factoryReset)
{
    this->factoryReset = factoryReset;
}
