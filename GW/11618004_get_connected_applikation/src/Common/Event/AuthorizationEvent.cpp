#include "AuthorizationEvent.h"

/**
 * @brief AuthorizationEvent::AuthorizationEvent Construct a new authorization event
 * @param other The other
 */
AuthorizationEvent::AuthorizationEvent(const AuthorizationEvent& other)
{
    this->authorized = other.getAuthorized();
    this->serverResponse = other.getServerResponse();
}

/**
 * @brief AuthorizationEvent::AuthorizationEvent Construct a new authorization event
 * @param authorized The authorized
 * @param serverResponse Is server response
 */
AuthorizationEvent::AuthorizationEvent(const bool authorized, const bool serverResponse)
{
    this->authorized = authorized;
    this->serverResponse = serverResponse;
}
