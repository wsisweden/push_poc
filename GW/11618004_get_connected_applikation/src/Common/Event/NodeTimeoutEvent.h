#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT NodeTimeoutEvent
{
    public:
        explicit NodeTimeoutEvent() {}
        explicit NodeTimeoutEvent(const NodeTimeoutEvent& other);
        explicit NodeTimeoutEvent(const quint8 channel, const quint16 panId, const quint16 address);
        ~NodeTimeoutEvent() {}

        quint8 getChannel() const { return this->channel; }
        quint16 getPanId() const { return this->panId; }
        quint16 getAddress() const { return this->address; }

    private:
        quint8 channel;
        quint16 panId;
        quint16 address;
};

Q_DECLARE_METATYPE(NodeTimeoutEvent)
