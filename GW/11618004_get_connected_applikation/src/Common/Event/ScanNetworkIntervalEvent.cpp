#include "ScanNetworkIntervalEvent.h"

/**
 * @brief ScanNetworkIntervalEvent::ScanNetworkIntervalEvent Construct a new interval event
 * @param other The other
 */
ScanNetworkIntervalEvent::ScanNetworkIntervalEvent(const ScanNetworkIntervalEvent& other)
{
    this->interval = other.getInterval();
}

/**
 * @brief ScanNetworkIntervalEvent::ScanNetworkIntervalEvent Construct a new interval event
 * @param interval The interval
 */
ScanNetworkIntervalEvent::ScanNetworkIntervalEvent(const int interval)
{
    this->interval = interval;
}
