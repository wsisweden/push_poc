#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT NodeRestrictionEvent
{
    public:
        explicit NodeRestrictionEvent() {}
        explicit NodeRestrictionEvent(const NodeRestrictionEvent& other);
        explicit NodeRestrictionEvent(const QString& nodeRestriction);
        ~NodeRestrictionEvent() {}

        QString getNodeRestriction() const { return this->nodeRestriction; }

    private:
        QString nodeRestriction;
};

Q_DECLARE_METATYPE(NodeRestrictionEvent)
