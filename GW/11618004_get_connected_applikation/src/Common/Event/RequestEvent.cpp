#include "RequestEvent.h"

/**
 * @brief RequestEvent::RequestEvent Construct a new request event
 * @param other The other
 */
RequestEvent::RequestEvent(const RequestEvent& other)
{
    this->payload = other.getPayload();
    this->endpoint = other.getEndpoint();
}

/**
 * @brief RequestEvent::RequestEvent Construct a new request event
 * @param endpoint The endpoint
 * @param payload The payload
 */
RequestEvent::RequestEvent(const QString& endpoint, const QJsonObject& payload)
{
    this->payload = payload;
    this->endpoint = endpoint;
}
