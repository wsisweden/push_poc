#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT AuthorizationEvent
{
    public:
        explicit AuthorizationEvent() {}
        explicit AuthorizationEvent(const AuthorizationEvent& other);
        explicit AuthorizationEvent(const bool authorized, const bool serverResponse);
        ~AuthorizationEvent() {}

        bool getAuthorized() const { return this->authorized; }
        bool getServerResponse() const { return this->serverResponse; }

    private:
        bool authorized;
        bool serverResponse;
};

Q_DECLARE_METATYPE(AuthorizationEvent)
