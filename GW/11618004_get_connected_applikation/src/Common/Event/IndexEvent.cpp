#include "IndexEvent.h"

/**
 * @brief IndexEvent::IndexEvent Construct a new index event
 * @param other The other
 */
IndexEvent::IndexEvent(const IndexEvent& other)
{
    this->channel = other.getChannel();
    this->panId = other.getPanId();
    this->address = other.getAddress();
    this->data = other.getData();
    this->logType = other.getLogType();
}

/**
 * @brief IndexEvent::IndexEvent Construct a new index event
 * @param channel The channel
 * @param panId The panId
 * @param address The address
 * @param index The index
 * @param logType The logType
 */
IndexEvent::IndexEvent(const quint8 channel, const quint16 panId, const quint16 address, const QJsonArray& data, const int logType)
{
    this->channel = channel;
    this->panId = panId;
    this->address = address;
    this->data = data;
    this->logType = logType;
}
