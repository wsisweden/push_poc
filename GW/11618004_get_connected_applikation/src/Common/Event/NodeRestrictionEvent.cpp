#include "NodeRestrictionEvent.h"

/**
 * @brief NodeRestrictionEvent::NodeRestrictionEvent Construct a new node restriction event
 * @param other The other
 */
NodeRestrictionEvent::NodeRestrictionEvent(const NodeRestrictionEvent& other)
{
    this->nodeRestriction = other.getNodeRestriction();
}

/**
 * @brief NodeRestrictionEvent::NodeRestrictionEvent Construct a new node restriction event
 * @param nodeRestriction The node restriction
 */
NodeRestrictionEvent::NodeRestrictionEvent(const QString& nodeRestriction)
{
    this->nodeRestriction = nodeRestriction;
}
