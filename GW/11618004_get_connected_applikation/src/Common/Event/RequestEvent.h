#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QJsonObject>
#include <QtCore/QMetaType>

class COMMON_EXPORT RequestEvent
{
    public:
        explicit RequestEvent() {}
        explicit RequestEvent(const RequestEvent& other);
        explicit RequestEvent(const QString& endpoint, const QJsonObject& payload);
        ~RequestEvent() {}

        QString getEndpoint() const { return this->endpoint; }
        QJsonObject getPayload() const { return this->payload; }

    private:
        QString endpoint;
        QJsonObject payload;
};

Q_DECLARE_METATYPE(RequestEvent)
