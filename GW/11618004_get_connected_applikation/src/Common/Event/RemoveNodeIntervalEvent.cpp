#include "RemoveNodeIntervalEvent.h"

/**
 * @brief RemoveNodeIntervalEvent::RemoveNodeIntervalEvent Construct a new interval event
 * @param other The other
 */
RemoveNodeIntervalEvent::RemoveNodeIntervalEvent(const RemoveNodeIntervalEvent& other)
{
    this->interval = other.getInterval();
}

/**
 * @brief RemoveNodeIntervalEvent::RemoveNodeIntervalEvent Construct a new interval event
 * @param interval The interval
 */
RemoveNodeIntervalEvent::RemoveNodeIntervalEvent(const int interval)
{
    this->interval = interval;
}
