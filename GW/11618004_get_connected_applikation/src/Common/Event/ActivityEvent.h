#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT ActivityEvent
{
    public:
        explicit ActivityEvent() {}
        explicit ActivityEvent(const ActivityEvent& other);
        explicit ActivityEvent(const QString& activity);
        ~ActivityEvent() {}

        QString getActivity() const { return this->activity; }

    private:
        QString activity;
};

Q_DECLARE_METATYPE(ActivityEvent)
