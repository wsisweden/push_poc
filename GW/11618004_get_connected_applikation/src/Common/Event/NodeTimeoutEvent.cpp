#include "NodeTimeoutEvent.h"

/**
 * @brief NodeTimeoutEvent::NodeTimeoutEvent Construct a new node timeout event
 * @param other The other
 */
NodeTimeoutEvent::NodeTimeoutEvent(const NodeTimeoutEvent& other)
{
    this->channel = other.getChannel();
    this->panId = other.getPanId();
    this->address = other.getAddress();
}

/**
 * @brief NodeTimeoutEvent::NodeTimeoutEvent Construct a new node timeout event
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 */
NodeTimeoutEvent::NodeTimeoutEvent(const quint8 channel, const quint16 panId, const quint16 address)
{
    this->channel = channel;
    this->panId = panId;
    this->address = address;
}
