#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT FactoryResetEvent
{
    public:
        explicit FactoryResetEvent() {}
        explicit FactoryResetEvent(const FactoryResetEvent& other);
        explicit FactoryResetEvent(const bool factoryReset);
        ~FactoryResetEvent() {}

        bool getFactoryReset() const { return this->factoryReset; }

    private:
        bool factoryReset;
};

Q_DECLARE_METATYPE(FactoryResetEvent)
