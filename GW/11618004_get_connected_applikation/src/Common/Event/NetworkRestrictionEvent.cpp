#include "NetworkRestrictionEvent.h"

/**
 * @brief NetworkRestrictionEvent::NetworkRestrictionEvent Construct a new network restriction event
 * @param other The other
 */
NetworkRestrictionEvent::NetworkRestrictionEvent(const NetworkRestrictionEvent& other)
{
    this->networkRestriction = other.getNetworkRestriction();
}

/**
 * @brief NetworkRestrictionEvent::NetworkRestrictionEvent Construct a new network restriction event
 * @param networkRestriction The network restriction
 */
NetworkRestrictionEvent::NetworkRestrictionEvent(const QString& networkRestriction)
{
    this->networkRestriction = networkRestriction;
}
