#include "HeartbeatEvent.h"

/**
 * @brief HeartbeatEvent::HeartbeatEvent Construct a new heartbeat event
 * @param other The other
 */
HeartbeatEvent::HeartbeatEvent(const HeartbeatEvent& other)
{
    this->heartbeat = other.getHeartbeat();
}

/**
 * @brief HeartbeatEvent::HeartbeatEvent Construct a new heartbeat event
 * @param uplink The uplink
 */
HeartbeatEvent::HeartbeatEvent(const bool heartbeat)
{
    this->heartbeat = heartbeat;
}
