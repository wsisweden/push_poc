#include "NodeUpdatedEvent.h"

/**
 * @brief NodeUpdatedEvent::NodeUpdatedEvent Construct a new node updated event
 * @param other The other
 */
NodeUpdatedEvent::NodeUpdatedEvent(const NodeUpdatedEvent& other)
{
    this->id = other.getId();
    this->nodeId = other.getNodeId();
    this->channel = other.getChannel();
    this->panId = other.getPanId();
    this->address = other.getAddress();
    this->historyLogIndex = other.getHistoryLogIndex();
    this->eventLogIndex = other.getEventLogIndex();
    this->instantLogIndex = other.getInstantLogIndex();
    this->timestamp = other.getTimestamp();
    this->productTypeValue = other.getProductTypeValue();
}

/**
 * @brief NodeUpdatedEvent::NodeUpdatedEvent Construct a new node updated event
 * @param id The id
 * @param nodeId The node id
 * @param channel The channel
 * @param panId The panId
 * @param address The node address
 * @param historyLogIndex The history log index
 * @param eventLogIndex The event log index
 * @param instantLogIndex The instant log index
 * @param timestamp The timestamp
 * @param productTypeValue The product type value
 */
NodeUpdatedEvent::NodeUpdatedEvent(const int id, const quint32 nodeId, const quint8 channel, const quint16 panId,
                                   const quint16 address, const quint32 historyLogIndex, const quint32 eventLogIndex,
                                   const quint32 instantLogIndex, const QString& timestamp, const quint8 productTypeValue)
{
    this->id = id;
    this->nodeId = nodeId;
    this->channel = channel;
    this->panId = panId;
    this->address = address;
    this->historyLogIndex = historyLogIndex;
    this->eventLogIndex = eventLogIndex;
    this->instantLogIndex = instantLogIndex;
    this->timestamp = timestamp;
    this->productTypeValue = productTypeValue;
}
