#pragma once

#include "../Shared.h"

#include <QtCore/QMetaType>

class COMMON_EXPORT ScanNetworkIntervalEvent
{
    public:
        explicit ScanNetworkIntervalEvent() {}
        explicit ScanNetworkIntervalEvent(const ScanNetworkIntervalEvent& other);
        explicit ScanNetworkIntervalEvent(const int interval);
        ~ScanNetworkIntervalEvent() {}

        int getInterval() const { return this->interval; }

    private:
        int interval;
};

Q_DECLARE_METATYPE(ScanNetworkIntervalEvent)
