#include "UplinkEvent.h"

/**
 * @brief UplinkEvent::UplinkEvent Construct a new uplink event
 * @param other The other
 */
UplinkEvent::UplinkEvent(const UplinkEvent& other)
{
    this->uplink = other.getUplink();
}

/**
 * @brief UplinkEvent::UplinkEvent Construct a new uplink event
 * @param uplink The uplink
 */
UplinkEvent::UplinkEvent(const bool uplink)
{
    this->uplink = uplink;
}
