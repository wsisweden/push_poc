#include "ConfigurationEvent.h"

/**
 * @brief ConfigurationEvent::ConfigurationEvent Construct a new configuration event
 * @param other The other
 */
ConfigurationEvent::ConfigurationEvent(const ConfigurationEvent& other)
{
    this->index = other.getIndex();
    this->data = other.getData();
}

/**
 * @brief ConfigurationEvent::ConfigurationEvent Construct a new configuration event
 * @param index The index
 * @param data The data
 */
ConfigurationEvent::ConfigurationEvent(const quint16 index, const QVariant& data)
{
    this->index = index;
    this->data = data;
}
