#include "FirmwareUpdatedEvent.h"

/**
 * @brief RadioUpdatedEvent::RadioUpdatedEvent Construct a new firmware updated event
 * @param other The other
 */
FirmwareUpdatedEvent::FirmwareUpdatedEvent(const FirmwareUpdatedEvent& other)
{
    this->firmwareType = other.getFirmwareType();
    this->firmwareVersion = other.getFirmwareVersion();
    this->firmwareAddress = other.getFirmwareAddress();
}

/**
 * @brief RadioUpdatedEvent::RadioUpdatedEvent Construct a new firmware updated event
 * @param firmwareType The firmware type
 * @param firmwareVersion The firmware version
 * @param firmwareAddress The firmware address
 */
FirmwareUpdatedEvent::FirmwareUpdatedEvent(const quint32 firmwareType, const quint32 firmwareVersion, const quint16 firmwareAddress)
{
    this->firmwareType = firmwareType;
    this->firmwareVersion = firmwareVersion;
    this->firmwareAddress = firmwareAddress;
}
