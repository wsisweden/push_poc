#include "CollectDataIntervalEvent.h"

/**
 * @brief CollectDataIntervalEvent::CollectDataIntervalEvent Construct a new interval event
 * @param other The other
 */
CollectDataIntervalEvent::CollectDataIntervalEvent(const CollectDataIntervalEvent& other)
{
    this->interval = other.getInterval();
}

/**
 * @brief CollectDataIntervalEvent::CollectDataIntervalEvent Construct a new interval event
 * @param interval The interval
 */
CollectDataIntervalEvent::CollectDataIntervalEvent(const int interval)
{
    this->interval = interval;
}
