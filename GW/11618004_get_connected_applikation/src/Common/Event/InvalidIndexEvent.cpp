#include "InvalidIndexEvent.h"

/**
 * @brief InvalidIndexEvent::InvalidIndexEvent Construct a new invalid index event
 * @param other The other
 */
InvalidIndexEvent::InvalidIndexEvent(const InvalidIndexEvent& other)
{
    this->channel = other.getChannel();
    this->panId = other.getPanId();
    this->address = other.getAddress();
}

/**
 * @brief InvalidIndexEvent::InvalidIndexEvent Construct a new invalid index event
 * @param channel The channel
 * @param panId The panId
 * @param address The address
 */
InvalidIndexEvent::InvalidIndexEvent(const quint8 channel, const quint16 panId, const quint16 address)
{
    this->channel = channel;
    this->panId = panId;
    this->address = address;
}
