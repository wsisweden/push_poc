#pragma once

#include "../Shared.h"

#include <QtCore/QMetaType>

class COMMON_EXPORT RemoveNodeIntervalEvent
{
    public:
        explicit RemoveNodeIntervalEvent() {}
        explicit RemoveNodeIntervalEvent(const RemoveNodeIntervalEvent& other);
        explicit RemoveNodeIntervalEvent(const int interval);
        ~RemoveNodeIntervalEvent() {}

        int getInterval() const { return this->interval; }

    private:
        int interval;
};

Q_DECLARE_METATYPE(RemoveNodeIntervalEvent)
