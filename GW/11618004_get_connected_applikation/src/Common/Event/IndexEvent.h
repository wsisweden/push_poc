#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>
#include "QtCore/QJsonArray"

class COMMON_EXPORT IndexEvent
{
    public:
        explicit IndexEvent() {}
        explicit IndexEvent(const IndexEvent& other);
        explicit IndexEvent(const quint8 channel, const quint16 panId, const quint16 address, const QJsonArray& data, const int logType);
        ~IndexEvent() {}

        quint8 getChannel() const { return this->channel; }
        quint16 getPanId() const { return this->panId; }
        quint16 getAddress() const { return this->address; }
        QJsonArray getData() const { return this->data; }
        int getLogType() const { return this->logType; }

    private:
        quint8 channel;
        quint16 panId;
        quint16 address;
        QJsonArray data;
        int logType;
};

Q_DECLARE_METATYPE(IndexEvent)
