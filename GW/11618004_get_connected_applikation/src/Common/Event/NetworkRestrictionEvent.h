#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT NetworkRestrictionEvent
{
    public:
        explicit NetworkRestrictionEvent() {}
        explicit NetworkRestrictionEvent(const NetworkRestrictionEvent& other);
        explicit NetworkRestrictionEvent(const QString& networkRestriction);
        ~NetworkRestrictionEvent() {}

        QString getNetworkRestriction() const { return this->networkRestriction; }

    private:
        QString networkRestriction;
};

Q_DECLARE_METATYPE(NetworkRestrictionEvent)
