#pragma once

#include "../Shared.h"

#include <QtCore/QMetaType>

class COMMON_EXPORT CollectDataIntervalEvent
{
    public:
        explicit CollectDataIntervalEvent() {}
        explicit CollectDataIntervalEvent(const CollectDataIntervalEvent& other);
        explicit CollectDataIntervalEvent(const int interval);
        ~CollectDataIntervalEvent() {}

        int getInterval() const { return this->interval; }

    private:
        int interval;
};

Q_DECLARE_METATYPE(CollectDataIntervalEvent)
