#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>
#include "QtCore/QJsonArray"

class COMMON_EXPORT InvalidIndexEvent
{
    public:
        explicit InvalidIndexEvent() {}
        explicit InvalidIndexEvent(const InvalidIndexEvent& other);
        explicit InvalidIndexEvent(const quint8 channel, const quint16 panId, const quint16 address);
        ~InvalidIndexEvent() {}

        quint8 getChannel() const { return this->channel; }
        quint16 getPanId() const { return this->panId; }
        quint16 getAddress() const { return this->address; }

    private:
        quint8 channel;
        quint16 panId;
        quint16 address;
};

Q_DECLARE_METATYPE(InvalidIndexEvent)
