#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT FirmwareUpdatedEvent
{
    public:
        explicit FirmwareUpdatedEvent() {}
        explicit FirmwareUpdatedEvent(const FirmwareUpdatedEvent& other);
        explicit FirmwareUpdatedEvent(const quint32 firmwareType, const quint32 firmwareVersion, const quint16 firmwareAddress);
        ~FirmwareUpdatedEvent() {}

        quint32 getFirmwareType() const { return this->firmwareType; }
        quint32 getFirmwareVersion() const { return this->firmwareVersion; }
        quint16 getFirmwareAddress() const { return this->firmwareAddress; }

    private:
        quint32 firmwareType;
        quint32 firmwareVersion;
        quint16 firmwareAddress;
};

Q_DECLARE_METATYPE(FirmwareUpdatedEvent)
