#include "ActivityEvent.h"

/**
 * @brief ActivityEvent::ActivityEvent Construct a new activity event
 * @param other The other
 */
ActivityEvent::ActivityEvent(const ActivityEvent& other)
{
    this->activity = other.getActivity();
}

/**
 * @brief ActivityEvent::ActivityEvent Construct a new activity event
 * @param activity The activity
 */
ActivityEvent::ActivityEvent(const QString& activity)
{
    this->activity = activity;
}
