#include "SyncNodeEvent.h"

/**
 * @brief SyncNodeEvent::SyncNodeEvent Construct a new sync node event
 * @param other The other
 */
SyncNodeEvent::SyncNodeEvent(const SyncNodeEvent& other)
{
    this->identifier = other.getIdentifier();
    this->channel = other.getChannel();
    this->panId = other.getPanId();
    this->address = other.getAddress();
    this->productType = other.getProductType();
}

/**
 * @brief SyncNodeEvent::SyncNodeEvent Construct a new sync node event
 * @param identifier The identifier
 * @param channel The channel
 * @param panId The pan id
 * @param address The address
 * @param productType The product type
 */
SyncNodeEvent::SyncNodeEvent(const quint32 identifier, const quint8 channel, const quint16 panId, const quint16 address, const quint8 productType)
{
    this->identifier = identifier;
    this->channel = channel;
    this->panId = panId;
    this->address = address;
    this->productType = productType;
}
