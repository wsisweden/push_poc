#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT SyncNodeEvent
{
    public:
        explicit SyncNodeEvent() {}
        explicit SyncNodeEvent(const SyncNodeEvent& other);
        explicit SyncNodeEvent(const quint32 identifier, const quint8 channel, const quint16 panId, const quint16 address, const quint8 productType);
        ~SyncNodeEvent() {}

        quint32 getIdentifier() const { return this->identifier; }
        quint8 getChannel() const { return this->channel; }
        quint16 getPanId() const { return this->panId; }
        quint16 getAddress() const { return this->address; }
        quint8 getProductType() const { return this->productType; }

    private:
        quint32 identifier;
        quint8 channel;
        quint16 panId;
        quint16 address;
        quint8 productType;
};

Q_DECLARE_METATYPE(SyncNodeEvent)
