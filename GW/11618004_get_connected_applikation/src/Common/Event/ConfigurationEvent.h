#pragma once

#include "../Shared.h"

#include <QtCore/QVariant>
#include <QtCore/QJsonObject>
#include <QtCore/QMetaType>

class COMMON_EXPORT ConfigurationEvent
{
    public:
        explicit ConfigurationEvent() {}
        explicit ConfigurationEvent(const ConfigurationEvent& other);
        explicit ConfigurationEvent(const quint16 index, const QVariant& data);
        ~ConfigurationEvent() {}

        quint16 getIndex() const { return this->index; }
        QVariant getData() const { return this->data; }

    private:
        quint16 index;
        QVariant data;
};

Q_DECLARE_METATYPE(ConfigurationEvent)
