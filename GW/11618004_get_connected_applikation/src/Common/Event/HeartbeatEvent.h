#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT HeartbeatEvent
{
    public:
        explicit HeartbeatEvent() {}
        explicit HeartbeatEvent(const HeartbeatEvent& other);
        explicit HeartbeatEvent(const bool heartbeat);
        ~HeartbeatEvent() {}

        bool getHeartbeat() const { return this->heartbeat; }

    private:
        bool heartbeat;
};

Q_DECLARE_METATYPE(HeartbeatEvent)
