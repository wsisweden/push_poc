#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QMetaType>

class COMMON_EXPORT UplinkEvent
{
    public:
        explicit UplinkEvent() {}
        explicit UplinkEvent(const UplinkEvent& other);
        explicit UplinkEvent(const bool uplink);
        ~UplinkEvent() {}

        bool getUplink() const { return this->uplink; }

    private:
        bool uplink;
};

Q_DECLARE_METATYPE(UplinkEvent)
