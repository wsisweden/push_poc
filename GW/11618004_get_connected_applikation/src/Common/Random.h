#pragma once

#include "Shared.h"

#include <QtGlobal>
#include <QtCore/QDebug>

#include <math.h>

class COMMON_EXPORT Random
{
    public:
        explicit Random();

    static double minutes(const double min)
    {
        const double max = (min / 2) + min; // min > 0.

        double random = (double)qrand() / RAND_MAX;
        double result = min + random * (max - min);

        if (result < 2)
            result = 2;

        qDebug().noquote() << QString("Random generator (Min: %1, Max: %2, Random: %3, Result: %4)").arg(min).arg(max).arg(random).arg(result);

        return result; // result >= 2.
    }

    static int number(const int min, const int max)
    {
        int random = qrand();
        int result = min + (random % ((max + 1) - min));

        qDebug().noquote() << QString("Random generator (Min: %1, Max: %2, Random: %3, Result: %4)").arg(min).arg(max).arg(random).arg(result);

        return result;
    }
};
