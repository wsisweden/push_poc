#pragma once

#include <QtCore/QString>

namespace ProductId
{
    static const int GATEWAY = 16;
}

namespace Can
{
    static const QString DEFAULT_BITRATE = "125000";
}

namespace ProductType
{
    static const int UNKNOWN = 0;
    static const int BMU = 1;
    static const int VEHICLE = 2;
    static const int CHARGER = 3;
    static const int PC = 4;
}

namespace LogType
{
    static const int HISTORY = 0;
    static const int EVENT = 1;
    static const int INSTANT = 2;
}

namespace Param
{
    static const int SOFTWARE_TYPE = 0;
    static const int SOFTWARE_VERSION = 1;
    static const int MUI = 2;
    static const int BUILDCOMMIT = 3;
    static const int QTVERSION = 4;
    static const int ETHERNET_MAC = 5;
    static const int WIRELESS_MAC = 6;
    static const int DEVICE_ID = 7;
    static const int NAME = 8;
    static const int CLOUD_URL = 9;
    static const int CLOUD_USERNAME = 10;
    static const int CLOUD_PASSWORD = 11;
    static const int SECURITY_CODE = 12;
    static const int SCAN_NETWORK_INTERVAL = 13;
    static const int COLLECT_DATA_INTERVAL = 14;
    static const int SECURE_SHELL = 16;
    static const int PERIODIC_UPDATE_INTERVAL = 17;
    static const int DEBUG_LOG = 18;
    static const int DEBUG_MPA_LOG = 19;
    static const int ETHERNET_ADDRESS = 20;
    static const int ETHERNET_STATIC = 21;
    static const int ETHERNET_NETMASK = 22;
    static const int ETHERNET_GATEWAY = 23;
    static const int WIRELESS_ADDRESS = 24;
    static const int FOUND_NODES = 25;
    static const int WIRELESS_NETMASK = 26;
    static const int WIRELESS_SSID = 27;
    static const int WIRELESS_PASSWORD = 28;
    static const int WIRELESS_SIGNAL_STRENGTH = 30;
    static const int WIRELESS_LINK_QUALITY = 31;
    static const int FREE_STORAGE = 32;
    static const int DATETIME = 33;
    static const int SERIAL_NUMBER = 34;
    static const int FACTORY_RESET = 35;
    static const int REMOVE_NODE_INTERVAL = 36;
    static const int NETWORK = 37;
    static const int NODE = 38;
    static const int FIRMWARE_TYPE = 39;
    static const int FIRMWARE_VERSION = 40;
    static const int READY = 41;
    static const int CAN_BITRATE = 42;
    static const int ROOTFS_RELEASE = 43;
}
