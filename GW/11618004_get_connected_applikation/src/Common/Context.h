#pragma once

#include "Shared.h"
#include "../Common/EventManager.h"
#include "../Storage/IStorage.h"

#include <QtCore/QObject>
#include <QtCore/QCommandLineParser>

#include <QtQml/QQmlContext>

class COMMON_EXPORT Context : public QObject
{
    Q_OBJECT

    public:
        explicit Context(QObject* parent = nullptr);
        ~Context() {}

        void setStorage(IStorage* storage) { this->storage = storage; }
        void setQmlContext(QQmlContext* qmlContext) { this->qmlContext = qmlContext; }
        void setEventManager(EventManager* eventManager) { this->eventManager = eventManager; }
        void setCommandLineParser(QCommandLineParser* commandLineParser) { this->commandLineParser = commandLineParser; }

        IStorage* getStorage() const { return this->storage; }
        QQmlContext* getContext() const { return this->qmlContext; }
        EventManager* getEventManager() const { return this->eventManager; }
        QCommandLineParser* getCommandLineParser() const { return this->commandLineParser; }

    private:
        IStorage* storage = nullptr;
        QQmlContext* qmlContext = nullptr;
        EventManager* eventManager = nullptr;
        QCommandLineParser* commandLineParser = nullptr;
};
