QT += core

CONFIG += c++11 staticlib

TARGET = common
TEMPLATE = lib

DEFINES += COMMON_LIBRARY

HEADERS += \
    Shared.h \
    Process.h \
    EventManager.h \
    Event/CollectDataIntervalEvent.h \
    Event/NodeAddedEvent.h \
    Event/NodeRemovedEvent.h \
    Event/RequestEvent.h \
    Event/ScanNetworkIntervalEvent.h \
    Event/ActivityEvent.h \
    Constant.h \
    Random.h \
    Event/SyncNodeEvent.h \
    Event/AuthorizationEvent.h \
    Context.h \
    Event/HeartbeatEvent.h \
    Event/NodeUpdatedEvent.h \
    Event/RemoveNodeIntervalEvent.h \
    Event/FactoryResetEvent.h \
    Event/NetworkRestrictionEvent.h \
    Event/NodeRestrictionEvent.h \
    Event/FirmwareUpdatedEvent.h \
    Event/IndexEvent.h \
    Event/ConfigurationEvent.h \
    Event/InvalidIndexEvent.h \
    Event/NodeTimeoutEvent.h \
    Event/UplinkEvent.h

SOURCES += \
    Process.cpp \
    Event/CollectDataIntervalEvent.cpp \
    Event/NodeAddedEvent.cpp \
    Event/NodeRemovedEvent.cpp \
    Event/RequestEvent.cpp \
    Event/ScanNetworkIntervalEvent.cpp \
    Event/ActivityEvent.cpp \
    EventManager.cpp \
    Event/SyncNodeEvent.cpp \
    Event/AuthorizationEvent.cpp \
    Context.cpp \
    Event/HeartbeatEvent.cpp \
    Event/NodeUpdatedEvent.cpp \
    Event/RemoveNodeIntervalEvent.cpp \
    Event/FactoryResetEvent.cpp \
    Event/NetworkRestrictionEvent.cpp \
    Event/NodeRestrictionEvent.cpp \
    Event/FirmwareUpdatedEvent.cpp \
    Event/IndexEvent.cpp \
    Event/ConfigurationEvent.cpp \
    Event/InvalidIndexEvent.cpp \
    Event/NodeTimeoutEvent.cpp \
    Event/UplinkEvent.cpp

OTHER_FILES += \
    Version.h.in

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:QMAKE_LFLAGS += -Wl,--start-group

#GIT_TIMESTAMP = $$system($$quote(git log -n 1 --format=format:"%ai"))
#GIT_VERSION = $$system($$quote(git rev-parse --verify --short=10 HEAD))

SCM_COMMIT=$$(SCM_COMMIT)
isEmpty(SCM_COMMIT) {
    SCM_COMMIT = $$system($$quote(git show -s --format=%cd --date=format:%y%m%d%H%M HEAD))
}

QMAKE_SUBSTITUTES += $$PWD/Version.h.in
