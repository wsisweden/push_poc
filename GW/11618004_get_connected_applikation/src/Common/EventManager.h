#pragma once

#include "Shared.h"
#include "Event/SyncNodeEvent.h"
#include "Event/ActivityEvent.h"
#include "Event/RequestEvent.h"
#include "Event/NodeAddedEvent.h"
#include "Event/NodeRemovedEvent.h"
#include "Event/NodeUpdatedEvent.h"
#include "Event/FirmwareUpdatedEvent.h"
#include "Event/ConfigurationEvent.h"
#include "Event/CollectDataIntervalEvent.h"
#include "Event/ScanNetworkIntervalEvent.h"
#include "Event/RemoveNodeIntervalEvent.h"
#include "Event/NetworkRestrictionEvent.h"
#include "Event/NodeRestrictionEvent.h"
#include "Event/AuthorizationEvent.h"
#include "Event/UplinkEvent.h"
#include "Event/HeartbeatEvent.h"
#include "Event/FactoryResetEvent.h"
#include "Event/IndexEvent.h"
#include "Event/InvalidIndexEvent.h"
#include "Event/NodeTimeoutEvent.h"

#include <QtCore/QObject>

class COMMON_EXPORT EventManager : public QObject
{
    Q_OBJECT

    public:
        explicit EventManager(QObject* parent = nullptr);

        Q_SIGNAL void mpaRequest();
        Q_SIGNAL void mpaRequestFailed();
        Q_SIGNAL void httpRequest();
        Q_SIGNAL void httpRequestFailed();

        Q_SIGNAL void commandTransmit();
        Q_SIGNAL void commandComplete();

        Q_SIGNAL void indexReceived(const IndexEvent& event);

        Q_SIGNAL void authorize();
        Q_SIGNAL void authorizationChanged(const AuthorizationEvent& event);

        Q_SIGNAL void factoryReset();
        Q_SIGNAL void uplinkChanged(const UplinkEvent& event);
        Q_SIGNAL void forceHeartbeat(const HeartbeatEvent& event);
        Q_SIGNAL void factoryResetChanged(const FactoryResetEvent& event);

        Q_SIGNAL void initializeComplete();
        Q_SIGNAL void configurationReady();
        Q_SIGNAL void configurationComplete();
        Q_SIGNAL void configurationReceived(const ConfigurationEvent& event);
        Q_SIGNAL void configurationChanged(const ConfigurationEvent& event);

        Q_SIGNAL void transmitRequest(const RequestEvent& event);
        Q_SIGNAL void activityChanged(const ActivityEvent& event);

        Q_SIGNAL void syncNode(const SyncNodeEvent& event);
        Q_SIGNAL void nodeAdded(const NodeAddedEvent& event);
        Q_SIGNAL void nodeRemoved(const NodeRemovedEvent& event);
        Q_SIGNAL void nodeUpdated(const NodeUpdatedEvent& event);
        Q_SIGNAL void nodeTimeout(const NodeTimeoutEvent& event);
        Q_SIGNAL void invalidIndex(const InvalidIndexEvent& event);

        Q_SIGNAL void firmwareUpdated(const FirmwareUpdatedEvent& event);
        Q_SIGNAL void networkRestrictionChanged(const NetworkRestrictionEvent& event);
        Q_SIGNAL void nodeRestrictionChanged(const NodeRestrictionEvent& event);

        Q_SIGNAL void scanNetworkIntervalChanged(const ScanNetworkIntervalEvent& event);
        Q_SIGNAL void collectDataIntervalChanged(const CollectDataIntervalEvent& event);
        Q_SIGNAL void removeNodeIntervalChanged(const RemoveNodeIntervalEvent& event);

        void emitMpaRequestEvent() { emit mpaRequest(); }
        void emitMpaRequestFailedEvent() { emit mpaRequestFailed(); }
        void emitHttpRequestEvent() { emit httpRequest(); }
        void emitHttpRequestFailedEvent() { emit httpRequestFailed(); }

        void emitCommandTransmitEvent() { emit commandTransmit(); }
        void emitCommandCompleteEvent() { emit commandComplete(); }

        void emitIndexReceivedEvent(const IndexEvent& event) { emit indexReceived(event); }

        void emitAuthorizeEvent() { emit authorize(); }
        void emitAuthorizationChangedEvent(const AuthorizationEvent& event) { emit authorizationChanged(event); }

        void emitFactoryResetEvent() { emit factoryReset(); }
        void emitUplinkChangedEvent(const UplinkEvent& event) { emit uplinkChanged(event); }
        void emitForceHeartbeatEvent(const HeartbeatEvent& event) { emit forceHeartbeat(event); }
        void emitFactoryResetChangedEvent(const FactoryResetEvent& event) { emit factoryResetChanged(event); }

        void emitInitializeCompleteEvent() { emit initializeComplete(); }
        void emitConfigurationReadyEvent() { emit configurationReady(); }
        void emitConfigurationCompleteEvent() { emit configurationComplete(); }
        void emitConfigurationChangedEvent(const ConfigurationEvent& event) { emit configurationChanged(event); }
        void emitConfigurationReceivedEvent(const ConfigurationEvent& event) { emit configurationReceived(event); }

        void emitTransmitRequestEvent(const RequestEvent& event) { emit transmitRequest(event); }
        void emitActivityChangedEvent(const ActivityEvent& event) { emit activityChanged(event); }

        void emitSyncNodeEvent(const SyncNodeEvent& event) { emit syncNode(event); }
        void emitNodeAddedEvent(const NodeAddedEvent& event) { emit nodeAdded(event); }
        void emitNodeRemovedEvent(const NodeRemovedEvent& event) { emit nodeRemoved(event); }
        void emitNodeUpdatedEvent(const NodeUpdatedEvent& event) { emit nodeUpdated(event); }
        void emitNodeTimeoutEvent(const NodeTimeoutEvent& event) { emit nodeTimeout(event); }
        void emitInvalidIndexEvent(const InvalidIndexEvent& event) { emit invalidIndex(event); }

        void emitFirmwareUpdatedEvent(const FirmwareUpdatedEvent& event) { emit firmwareUpdated(event); }
        void emitNetworkRestrictionChangedEvent(const NetworkRestrictionEvent& event) { emit networkRestrictionChanged(event); }
        void emitNodeRestrictionChangedEvent(const NodeRestrictionEvent& event) { emit nodeRestrictionChanged(event); }

        void emitScanNetworkIntervalChangedEvent(const ScanNetworkIntervalEvent& event) { emit scanNetworkIntervalChanged(event); }
        void emitCollectDataIntervalChangedEvent(const CollectDataIntervalEvent& event) { emit collectDataIntervalChanged(event); }
        void emitRemoveNodeIntervalChangedEvent(const RemoveNodeIntervalEvent& event) { emit removeNodeIntervalChanged(event); }
};
