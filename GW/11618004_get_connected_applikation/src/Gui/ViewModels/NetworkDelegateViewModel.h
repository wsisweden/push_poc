#pragma once

#include "../Shared.h"
#include "AbstractViewModel.h"
#include "../Models/WifiHardwareModel.h"
#include "../Models/WifiFirmwareModel.h"
#include "../Models/NetworkInterfaceModel.h"
#include "../Models/NetworkAddressModel.h"
#include "../Models/WifiSignalQualityModel.h"
#include "../Models/WifiSignalStrengthModel.h"

class GUI_EXPORT NetworkDelegateViewModel : public AbstractViewModel
{
    Q_OBJECT

    Q_PROPERTY(QString hardwareDetected READ getHardwareDetected NOTIFY hardwareDetectedChanged)
    Q_PROPERTY(QString hardwareDetectedDescription READ getHardwareDetectedDescription NOTIFY hardwareDetectedDescriptionChanged)

    Q_PROPERTY(QString firmwareLoaded READ getFirmwareLoaded NOTIFY firmwareLoadedChanged)
    Q_PROPERTY(QString firmwareLoadedDescription READ getFirmwareLoadedDescription NOTIFY firmwareLoadedDescriptionChanged)

    Q_PROPERTY(QString interfaceDetected READ getInterfaceDetected NOTIFY interfaceDetectedChanged)
    Q_PROPERTY(QString interfaceDetectedDescription READ getInterfaceDetectedDescription NOTIFY interfaceDetectedDescriptionChanged)

    Q_PROPERTY(QString addressDetected READ getAddressDetected NOTIFY addressDetectedChanged)
    Q_PROPERTY(QString addressDetectedDescription READ getAddressDetectedDescription NOTIFY addressDetectedDescriptionChanged)

    Q_PROPERTY(QString signalQuality READ getSignalQuality NOTIFY signalQualityChanged)
    Q_PROPERTY(QString signalQualityDescription READ getSignalQualityDescription NOTIFY signalQualityDescriptionChanged)

    Q_PROPERTY(QString signalStrength READ getSignalStrength NOTIFY signalStrengthChanged)
    Q_PROPERTY(QString signalStrengthDescription READ getSignalStrengthDescription NOTIFY signalStrengthDescriptionChanged)

    public:
        explicit NetworkDelegateViewModel(Context* context, QObject* parent = 0);
        ~NetworkDelegateViewModel() {}

        Q_INVOKABLE void runTests() override;

        Q_SIGNAL void hardwareDetectedChanged(const QString& value);
        Q_SIGNAL void hardwareDetectedDescriptionChanged(const QString& value);

        Q_SIGNAL void firmwareLoadedChanged(const QString& value);
        Q_SIGNAL void firmwareLoadedDescriptionChanged(const QString& value);

        Q_SIGNAL void interfaceDetectedChanged(const QString& value);
        Q_SIGNAL void interfaceDetectedDescriptionChanged(const QString& value);

        Q_SIGNAL void addressDetectedChanged(const QString& value);
        Q_SIGNAL void addressDetectedDescriptionChanged(const QString& value);

        Q_SIGNAL void signalQualityChanged(const QString& value);
        Q_SIGNAL void signalQualityDescriptionChanged(const QString& value);

        Q_SIGNAL void signalStrengthChanged(const QString& value);
        Q_SIGNAL void signalStrengthDescriptionChanged(const QString& value);

        QString getHardwareDetected() { return this->hardwareDetected; }
        QString getHardwareDetectedDescription() { return this->hardwareDetectedDescription; }

        QString getFirmwareLoaded() { return this->firmwareLoaded; }
        QString getFirmwareLoadedDescription() { return this->firmwareLoadedDescription; }

        QString getInterfaceDetected() { return this->interfaceDetected; }
        QString getInterfaceDetectedDescription() { return this->interfaceDetectedDescription; }

        QString getAddressDetected() { return this->addressDetected; }
        QString getAddressDetectedDescription() { return this->addressDetectedDescription; }

        QString getSignalQuality() { return this->signalQuality; }
        QString getSignalQualityDescription() { return this->signalQualityDescription; }

        QString getSignalStrength() { return this->signalStrength; }
        QString getSignalStrengthDescription() { return this->signalStrengthDescription; }

    private:
        WifiHardwareModel* hardware;
        WifiFirmwareModel* firmware;
        NetworkInterfaceModel* interfaces;
        NetworkAddressModel* address;
        WifiSignalQualityModel* quality;
        WifiSignalStrengthModel* strength;

        QString hardwareDetected;
        QString hardwareDetectedDescription;

        QString firmwareLoaded;
        QString firmwareLoadedDescription;

        QString interfaceDetected;
        QString interfaceDetectedDescription;

        QString addressDetected;
        QString addressDetectedDescription;

        QString signalQuality;
        QString signalQualityDescription;

        QString signalStrength;
        QString signalStrengthDescription;
};
