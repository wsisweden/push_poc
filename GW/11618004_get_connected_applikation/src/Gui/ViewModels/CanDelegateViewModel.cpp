#include "CanDelegateViewModel.h"
#include "../../Common/Context.h"
#include "../../Common/Random.h"

#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QTimer>

CanDelegateViewModel::CanDelegateViewModel(Context* context, QObject* parent)
    : AbstractViewModel(context, parent)
{
    this->hardware = new CanHardwareModel(context, this);
    connect(this->hardware, &CanHardwareModel::hardwareDetectedChanged, [this](bool value)
    {
        this->hardwareDetected = QVariant(value).toString();
        emit hardwareDetectedChanged(this->hardwareDetected);
    });
    connect(this->hardware, &CanHardwareModel::hardwareDetectedDescriptionChanged, [this](const QString& value)
    {
        this->hardwareDetectedDescription = value;
        emit hardwareDetectedChanged(this->hardwareDetectedDescription);
    });

    this->interface = new CanInterfaceModel(context, this);
    connect(this->interface, &CanInterfaceModel::interfaceDetectedChanged, [this](bool value)
    {
        this->interfaceDetected = QVariant(value).toString();
        emit interfaceDetectedChanged(this->interfaceDetected);
    });
    connect(this->interface, &CanInterfaceModel::interfaceDetectedDescriptionChanged, [this](const QString& value)
    {
        this->interfaceDetectedDescription = value;
        emit interfaceDetectedChanged(this->interfaceDetectedDescription);
    });
}

void CanDelegateViewModel::runTests()
{
    QTimer::singleShot(Random::number(2000, 4000), this, [this]()
    {
        this->hardware->runTests();

        QTimer::singleShot(Random::number(1000, 2000), this, [this]()
        {
            this->interface->runTests();
        });
    });
}
