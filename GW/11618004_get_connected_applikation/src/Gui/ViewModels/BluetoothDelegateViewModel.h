#pragma once

#include "../Shared.h"
#include "AbstractViewModel.h"
#include "../Models/BluetoothHardwareModel.h"
#include "../Models/BluetoothDeviceModel.h"

class GUI_EXPORT BluetoothDelegateViewModel : public AbstractViewModel
{
    Q_OBJECT

    Q_PROPERTY(QString hardwareDetected READ getHardwareDetected NOTIFY hardwareDetectedChanged)
    Q_PROPERTY(QString hardwareDetectedDescription READ getHardwareDetectedDescription NOTIFY hardwareDetectedDescriptionChanged)

    Q_PROPERTY(QString deviceDetected READ getDeviceDetected NOTIFY deviceDetectedChanged)
    Q_PROPERTY(QString deviceDetectedDescription READ getDeviceDetectedDescription NOTIFY deviceDetectedDescriptionChanged)

    public:
        explicit BluetoothDelegateViewModel(Context* context, QObject* parent = 0);
        ~BluetoothDelegateViewModel() {}

        Q_INVOKABLE void runTests() override;

        Q_SIGNAL void hardwareDetectedChanged(const QString& value);
        Q_SIGNAL void hardwareDetectedDescriptionChanged(const QString& value);

        Q_SIGNAL void deviceDetectedChanged(const QString& value);
        Q_SIGNAL void deviceDetectedDescriptionChanged(const QString& value);

        QString getHardwareDetected() { return this->hardwareDetected; }
        QString getHardwareDetectedDescription() { return this->hardwareDetectedDescription; }

        QString getDeviceDetected() { return this->deviceDetected; }
        QString getDeviceDetectedDescription() { return this->deviceDetectedDescription; }

    private:
        BluetoothHardwareModel* hardware;
        BluetoothDeviceModel* device;

        QString hardwareDetected;
        QString hardwareDetectedDescription;

        QString deviceDetected;
        QString deviceDetectedDescription;
};
