#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"

#include <QtCore/QObject>

class GUI_EXPORT RootViewModel : public QObject
{
    Q_OBJECT

    public:
        explicit RootViewModel(Context* context, QObject* parent = 0);
        ~RootViewModel() {}

    private:
        Context* context = nullptr;
};
