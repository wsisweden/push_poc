#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"

#include <QtCore/QObject>

class GUI_EXPORT AbstractViewModel : public QObject
{
    Q_OBJECT

    public:
        explicit AbstractViewModel(Context* context, QObject* parent = 0);
        ~AbstractViewModel();

        virtual void runTests() = 0;

    protected:
        Context* context = nullptr;
};
