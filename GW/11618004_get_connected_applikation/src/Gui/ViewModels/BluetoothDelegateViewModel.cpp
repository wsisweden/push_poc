#include "BluetoothDelegateViewModel.h"
#include "../../Common/Context.h"
#include "../../Common/Random.h"

#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QTimer>

BluetoothDelegateViewModel::BluetoothDelegateViewModel(Context* context, QObject* parent)
    : AbstractViewModel(context, parent)
{
    this->hardware = new BluetoothHardwareModel(context, this);
    connect(this->hardware, &BluetoothHardwareModel::hardwareDetectedChanged, [this](bool value)
    {
        this->hardwareDetected = QVariant(value).toString();
        emit hardwareDetectedChanged(this->hardwareDetected);
    });
    connect(this->hardware, &BluetoothHardwareModel::hardwareDetectedDescriptionChanged, [this](const QString& value)
    {
        this->hardwareDetectedDescription = value;
        emit hardwareDetectedChanged(this->hardwareDetectedDescription);
    });

    this->device = new BluetoothDeviceModel(context, this);
    connect(this->device, &BluetoothDeviceModel::deviceDetectedChanged, [this](bool value)
    {
        this->deviceDetected = QVariant(value).toString();
        emit deviceDetectedChanged(this->deviceDetected);
    });
    connect(this->device, &BluetoothDeviceModel::deviceDetectedDescriptionChanged, [this](const QString& value)
    {
        this->deviceDetectedDescription = value;
        emit deviceDetectedDescriptionChanged(this->deviceDetectedDescription);
    });
}

void BluetoothDelegateViewModel::runTests()
{
    QTimer::singleShot(Random::number(2000, 4000), this, [this]()
    {
        this->hardware->runTests();

        QTimer::singleShot(Random::number(1000, 2000), this, [this]()
        {
            this->device->runTests();
        });
    });
}
