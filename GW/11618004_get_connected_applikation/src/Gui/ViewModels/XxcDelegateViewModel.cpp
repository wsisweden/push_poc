#include "XxcDelegateViewModel.h"
#include "../../Common/Context.h"
#include "../../Common/Random.h"

#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QTimer>

XxcDelegateViewModel::XxcDelegateViewModel(Context* context, QObject* parent)
    : AbstractViewModel(context, parent)
{
    this->hardware = new RtcHardwareModel(context, this);
    connect(this->hardware, &RtcHardwareModel::rtcHardwareDetectedChanged, [this](bool value)
    {
        this->hardwareDetected = QVariant(value).toString();
        emit hardwareDetectedChanged(this->hardwareDetected);
    });
    connect(this->hardware, &RtcHardwareModel::rtcHardwareDetectedDescriptionChanged, [this](const QString& value)
    {
        this->hardwareDetectedDescription = value;
        emit hardwareDetectedChanged(this->hardwareDetectedDescription);
    });

    this->serialNumber = new NfcSerialNumberModel(context, this);
    connect(this->serialNumber, &NfcSerialNumberModel::serialNumberDetectedChanged, [this](bool value)
    {
        this->serialNumberDetected = QVariant(value).toString();
        emit serialNumberDetectedChanged(this->serialNumberDetected);
    });
    connect(this->serialNumber, &NfcSerialNumberModel::serialNumberDetectedDescriptionChanged, [this](const QString& value)
    {
        this->serialNumberDetectedDescription = value;
        emit serialNumberDetectedDescriptionChanged(this->serialNumberDetectedDescription);
    });
}

void XxcDelegateViewModel::runTests()
{
    QTimer::singleShot(Random::number(2000, 4000), this, [this]()
    {
        this->hardware->runTests();
        this->serialNumber->runTests();
    });
}
