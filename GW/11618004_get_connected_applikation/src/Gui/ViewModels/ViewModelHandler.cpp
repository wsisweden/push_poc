#include "ViewModelHandler.h"
#include "NetworkDelegateViewModel.h"
#include "BluetoothDelegateViewModel.h"
#include "CanDelegateViewModel.h"
#include "XxcDelegateViewModel.h"
#include "RadioDelegateViewModel.h"

ViewModelHandler::ViewModelHandler(Context* context, QObject* parent)
    : QObject(parent)
{
    this->context = context;
}

/**
 * @brief ViewModelHandler::addViewModel Add view model
 * @param name Model name
 * @param parent View parent
 */
void ViewModelHandler::addViewModel(const QString& name, QObject* parent)
{
    qInfo("Adding view model: %s", qPrintable(name));
    if (name == NetworkDelegateViewModel::staticMetaObject.className())
        this->context->getContext()->setContextProperty(uncapitalize(name), new NetworkDelegateViewModel(this->context, parent));
    else if (name == BluetoothDelegateViewModel::staticMetaObject.className())
        this->context->getContext()->setContextProperty(uncapitalize(name), new BluetoothDelegateViewModel(this->context, parent));
    else if (name == CanDelegateViewModel::staticMetaObject.className())
        this->context->getContext()->setContextProperty(uncapitalize(name), new CanDelegateViewModel(this->context, parent));
    else if (name == XxcDelegateViewModel::staticMetaObject.className())
        this->context->getContext()->setContextProperty(uncapitalize(name), new XxcDelegateViewModel(this->context, parent));
    else if (name == RadioDelegateViewModel::staticMetaObject.className())
        this->context->getContext()->setContextProperty(uncapitalize(name), new RadioDelegateViewModel(this->context, parent));
    else
        qInfo("Requested view model is not implemented: %s", qPrintable(name));
}

/**
 * @brief ViewModelHandler::removeViewModel Remove view model
 * @param name Model name
 */
void ViewModelHandler::removeViewModel(const QString& name)
{
    qInfo("Removing view model: %s", qPrintable(name));
    QVariant variant = this->context->getContext()->contextProperty(uncapitalize(name));
    if (!variant.isNull())
        this->context->getContext()->setContextProperty(uncapitalize(name), nullptr);
}

/**
 * @brief ViewModelHandler::uncapitalize Uncapitalize string
 * @param name Model name
 */
QString ViewModelHandler::uncapitalize(QString name)
{
    name[0] = name[0].toLower();
    return name;
}
