#include "AbstractViewModel.h"

#include <QtCore/QDebug>

AbstractViewModel::AbstractViewModel(Context* context, QObject* parent)
    : QObject(parent)
{
    this->context = context;
}

AbstractViewModel::~AbstractViewModel()
{
}
