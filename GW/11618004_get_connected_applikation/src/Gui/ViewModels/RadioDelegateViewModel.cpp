#include "RadioDelegateViewModel.h"
#include "../../Common/Context.h"
#include "../../Common/Random.h"

#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QTimer>

RadioDelegateViewModel::RadioDelegateViewModel(Context* context, QObject* parent)
    : AbstractViewModel(context, parent)
{
    this->device = new RadioDeviceModel(context, this);
    connect(this->device, &RadioDeviceModel::hardwareDetectedChanged, [this](bool value)
    {
        this->hardwareDetected = QVariant(value).toString();
        emit hardwareDetectedChanged(this->hardwareDetected);
    });
    connect(this->device, &RadioDeviceModel::hardwareDetectedDescriptionChanged, [this](const QString& value)
    {
        this->hardwareDetectedDescription = value;
        emit hardwareDetectedChanged(this->hardwareDetectedDescription);
    });
    connect(this->device, &RadioDeviceModel::deviceDetectedChanged, [this](bool value)
    {
        this->deviceDetected = QVariant(value).toString();
        emit deviceDetectedChanged(this->deviceDetected);
    });
    connect(this->device, &RadioDeviceModel::deviceDetectedDescriptionChanged, [this](const QString& value)
    {
        this->deviceDetectedDescription = value;
        emit deviceDetectedDescriptionChanged(this->deviceDetectedDescription);
    });
}

void RadioDelegateViewModel::runTests()
{
    QTimer::singleShot(Random::number(2000, 4000), this, [this]()
    {
        this->device->runTests();
   });
}
