#pragma once

#include "../Shared.h"
#include "AbstractViewModel.h"
#include "../Models/CanHardwareModel.h"
#include "../Models/CanInterfaceModel.h"

class GUI_EXPORT CanDelegateViewModel : public AbstractViewModel
{
    Q_OBJECT

    Q_PROPERTY(QString hardwareDetected READ getHardwareDetected NOTIFY hardwareDetectedChanged)
    Q_PROPERTY(QString hardwareDetectedDescription READ getHardwareDetectedDescription NOTIFY hardwareDetectedDescriptionChanged)

    Q_PROPERTY(QString interfaceDetected READ getInterfaceDetected NOTIFY interfaceDetectedChanged)
    Q_PROPERTY(QString interfaceDetectedDescription READ getInterfaceDetectedDescription NOTIFY interfaceDetectedDescriptionChanged)

    public:
        explicit CanDelegateViewModel(Context* context, QObject* parent = 0);
        ~CanDelegateViewModel() {}

        Q_INVOKABLE void runTests() override;

        Q_SIGNAL void hardwareDetectedChanged(const QString& value);
        Q_SIGNAL void hardwareDetectedDescriptionChanged(const QString& value);

        Q_SIGNAL void interfaceDetectedChanged(const QString& value);
        Q_SIGNAL void interfaceDetectedDescriptionChanged(const QString& value);

        QString getHardwareDetected() { return this->hardwareDetected; }
        QString getHardwareDetectedDescription() { return this->hardwareDetectedDescription; }

        QString getInterfaceDetected() { return this->hardwareDetected; }
        QString getInterfaceDetectedDescription() { return this->hardwareDetectedDescription; }

    private:
        CanHardwareModel* hardware;
        CanInterfaceModel* interface;

        QString hardwareDetected;
        QString hardwareDetectedDescription;

        QString interfaceDetected;
        QString interfaceDetectedDescription;
};
