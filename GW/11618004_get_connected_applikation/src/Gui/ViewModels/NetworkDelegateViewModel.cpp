#include "NetworkDelegateViewModel.h"
#include "../../Common/Context.h"
#include "../../Common/Random.h"

#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QTimer>

NetworkDelegateViewModel::NetworkDelegateViewModel(Context* context, QObject* parent)
    : AbstractViewModel(context, parent)
{
    this->hardware = new WifiHardwareModel(context, this);
    connect(this->hardware, &WifiHardwareModel::hardwareDetectedChanged, [this](bool value)
    {
        this->hardwareDetected = QVariant(value).toString();
        emit hardwareDetectedChanged(this->hardwareDetected);
    });
    connect(this->hardware, &WifiHardwareModel::hardwareDetectedDescriptionChanged, [this](const QString& value)
    {
        this->hardwareDetectedDescription = value;
        emit hardwareDetectedChanged(this->hardwareDetectedDescription);
    });

    this->firmware = new WifiFirmwareModel(context, this);
    connect(this->firmware, &WifiFirmwareModel::firmwareLoadedChanged, [this](bool value)
    {
        this->firmwareLoaded = QVariant(value).toString();
        emit firmwareLoadedChanged(this->firmwareLoaded);
    });
    connect(this->firmware, &WifiFirmwareModel::firmwareLoadedDescriptionChanged, [this](const QString& value)
    {
        this->firmwareLoadedDescription = value;
        emit firmwareLoadedChanged(this->firmwareLoadedDescription);
    });

    this->interfaces = new NetworkInterfaceModel(context, this);
    connect(this->interfaces, &NetworkInterfaceModel::interfaceDetectedChanged, [this](bool value)
    {
        this->interfaceDetected = QVariant(value).toString();
        emit interfaceDetectedChanged(this->interfaceDetected);
    });
    connect(this->interfaces, &NetworkInterfaceModel::interfaceDetectedDescriptionChanged, [this](const QString& value)
    {
        this->interfaceDetectedDescription = value;
        emit interfaceDetectedChanged(this->interfaceDetectedDescription);
    });

    this->address = new NetworkAddressModel(context, this);
    connect(this->address, &NetworkAddressModel::addressDetectedChanged, [this](bool value)
    {
        this->addressDetected = QVariant(value).toString();
        emit addressDetectedChanged(this->addressDetected);

        if (this->addressDetected == "true")
        {
            this->quality->runTests();
            this->strength->runTests();
        }
        else
        {
            this->signalQuality = "false";
            emit signalQualityChanged(this->signalQuality);

            this->signalQualityDescription = "";
            emit signalQualityDescriptionChanged(this->signalQualityDescription);

            this->signalStrength = "false";
            emit signalStrengthChanged(this->signalStrength);

            this->signalStrengthDescription = "";
            emit signalStrengthDescriptionChanged(this->signalStrengthDescription);
        }
    });
    connect(this->address, &NetworkAddressModel::addressDetectedDescriptionChanged, [this](const QString& value)
    {
        this->addressDetectedDescription = value;
        emit addressDetectedDescriptionChanged(this->addressDetectedDescription);
    });

    this->quality = new WifiSignalQualityModel(context, this);
    connect(this->quality, &WifiSignalQualityModel::signalQualityChanged, [this](bool value)
    {
        this->signalQuality = QVariant(value).toString();
        emit signalQualityChanged(this->signalQuality);
    });
    connect(this->quality, &WifiSignalQualityModel::signalQualityDescriptionChanged, [this](const QString& value)
    {
        this->signalQualityDescription = value;
        emit signalQualityDescriptionChanged(this->signalQualityDescription);
    });

    this->strength = new WifiSignalStrengthModel(context, this);
    connect(this->strength, &WifiSignalStrengthModel::signalStrengthChanged, [this](bool value)
    {
        this->signalStrength = QVariant(value).toString();
        emit signalStrengthChanged(this->signalStrength);
    });
    connect(this->strength, &WifiSignalStrengthModel::signalStrengthDescriptionChanged, [this](const QString& value)
    {
        this->signalStrengthDescription = value;
        emit signalStrengthDescriptionChanged(this->signalStrengthDescription);
    });
}

void NetworkDelegateViewModel::runTests()
{
    QTimer::singleShot(Random::number(2000, 4000), this, [this]()
    {
        this->hardware->runTests();
        this->interfaces->runTests();

        QTimer::singleShot(Random::number(1000, 2000), this, [this]()
        {
            this->firmware->runTests();
            this->address->runTests();
        });
    });
}
