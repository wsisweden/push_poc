#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"

#include <QtCore/QObject>

class GUI_EXPORT ViewModelHandler : public QObject
{
    Q_OBJECT

    public:
        explicit ViewModelHandler(Context* context, QObject* parent = 0);
        ~ViewModelHandler() {}

        Q_INVOKABLE void addViewModel(const QString& name, QObject* parent = 0);
        Q_INVOKABLE void removeViewModel(const QString& name);

    private:
        Context* context = nullptr;

        QString uncapitalize(QString name);
};
