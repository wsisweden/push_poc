#pragma once

#include "../Shared.h"
#include "AbstractViewModel.h"
#include "../Models/RtcHardwareModel.h"
#include "../Models/NfcSerialNumberModel.h"

class GUI_EXPORT XxcDelegateViewModel : public AbstractViewModel
{
    Q_OBJECT

    Q_PROPERTY(QString hardwareDetected READ getHardwareDetected NOTIFY hardwareDetectedChanged)
    Q_PROPERTY(QString hardwareDetectedDescription READ getHardwareDetectedDescription NOTIFY hardwareDetectedDescriptionChanged)

    Q_PROPERTY(QString serialNumberDetected READ getSerialNumberDetected NOTIFY serialNumberDetectedChanged)
    Q_PROPERTY(QString serialNumberDetectedDescription READ getSerialNumberDetectedDescription NOTIFY serialNumberDetectedDescriptionChanged)

    public:
        explicit XxcDelegateViewModel(Context* context, QObject* parent = 0);
        ~XxcDelegateViewModel() {}

        Q_INVOKABLE void runTests();

        Q_SIGNAL void hardwareDetectedChanged(const QString& value);
        Q_SIGNAL void hardwareDetectedDescriptionChanged(const QString& value);

        Q_SIGNAL void serialNumberDetectedChanged(const QString& value);
        Q_SIGNAL void serialNumberDetectedDescriptionChanged(const QString& value);

        QString getHardwareDetected() { return this->hardwareDetected; }
        QString getHardwareDetectedDescription() { return this->hardwareDetectedDescription; }

        QString getSerialNumberDetected() { return this->serialNumberDetected; }
        QString getSerialNumberDetectedDescription() { return this->serialNumberDetectedDescription; }

    private:
        RtcHardwareModel* hardware;
        NfcSerialNumberModel* serialNumber;

        QString hardwareDetected;
        QString hardwareDetectedDescription;

        QString serialNumberDetected;
        QString serialNumberDetectedDescription;
};
