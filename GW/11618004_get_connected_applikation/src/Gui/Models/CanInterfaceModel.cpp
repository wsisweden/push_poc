#include "CanInterfaceModel.h"
#include "../../Common/Context.h"

CanInterfaceModel::CanInterfaceModel(Context* context, QObject* parent)
    : AbstractModel(context, parent) {}

void CanInterfaceModel::runTests()
{
    this->process = new QProcess(this);
    connect(this->process, &QProcess::readyRead, [this]()
    {
        this->interfaceDetected = this->process->readAllStandardOutput().trimmed().isEmpty() ? false : true;
    });
    connect(this->process, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->interfaceDetected = false;
        emit interfaceDetectedChanged(this->interfaceDetected);
    });
    connect(this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        emit interfaceDetectedChanged(this->interfaceDetected);

        this->interfaceDetectedDescription = "";
        emit interfaceDetectedDescriptionChanged(this->interfaceDetectedDescription);

        this->process->deleteLater();
    });
    this->process->start("sh", QStringList() << "-c" << "ifconfig | grep 'can0'");
}
