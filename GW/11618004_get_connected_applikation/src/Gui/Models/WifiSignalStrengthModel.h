#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QTimer>
#include <QtCore/QProcess>

class GUI_EXPORT WifiSignalStrengthModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool signalStrength READ getSignalStrength NOTIFY signalStrengthChanged)
    Q_PROPERTY(QString signalStrengthDescription READ getSignalStrengthDescription NOTIFY signalStrengthDescriptionChanged)

    public:
        explicit WifiSignalStrengthModel(Context* context, QObject* parent = 0);
        ~WifiSignalStrengthModel() {}

        void runTests() override;

        Q_SIGNAL void signalStrengthChanged(bool value);
        Q_SIGNAL void signalStrengthDescriptionChanged(const QString& value);

        bool getSignalStrength() { return this->signalStrength; }
        QString getSignalStrengthDescription() { return this->signalStrengthDescription; }

    private:
        QTimer* timer = nullptr;
        QProcess* process = nullptr;

        bool signalStrength = false;

        QString signalStrengthDescription;

        Q_SLOT void timeout();
};
