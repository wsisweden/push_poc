#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QProcess>

class GUI_EXPORT BluetoothDeviceModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool deviceDetected READ getDeviceDetected NOTIFY deviceDetectedChanged)
    Q_PROPERTY(QString deviceDetectedDescription READ getDeviceDetectedDescription NOTIFY deviceDetectedDescriptionChanged)

    public:
        explicit BluetoothDeviceModel(Context* context, QObject* parent = 0);
        ~BluetoothDeviceModel() {}

        void runTests() override;

        Q_SIGNAL void deviceDetectedChanged(bool value);
        Q_SIGNAL void deviceDetectedDescriptionChanged(const QString& value);

        bool getDeviceDetected() { return this->deviceDetected; }
        QString getDeviceDetectedDescription() { return this->deviceDetectedDescription; }

    private:
        bool deviceDetected = false;

        QProcess* process = nullptr;

        QString deviceDetectedDescription;
};
