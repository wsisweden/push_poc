#include "WifiSignalQualityModel.h"
#include "../../Common/Context.h"

WifiSignalQualityModel::WifiSignalQualityModel(Context* context, QObject* parent)
    : AbstractModel(context, parent)
{
    this->timer = new QTimer(this);
    connect(this->timer, &QTimer::timeout, this, &WifiSignalQualityModel::timeout);
}

void WifiSignalQualityModel::runTests()
{
    this->process = new QProcess(this);
    connect(this->process, &QProcess::readyRead, [this]()
    {    
        // Generally,
        //    quality >= 45 db = 100% quality
        //    quality <= 15 db = 0% quality
        //
        // For example:
        //     High quality: 45
        //     Good quality: 35
        //     Low quality: 25
        //     Bad quality: 15
        QString quality = this->process->readAllStandardOutput().trimmed();
        if (!quality.isEmpty())
            this->signalQuality = (qRound(((float)quality.split('/')[0].toInt() / (float)quality.split('/')[1].toInt()) * 100) >= 45) ? true : false;
    });
    connect(this->process, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->signalQuality = false;
        emit signalQualityChanged(this->signalQuality);
    });
    connect(this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        this->timer->stop();

        emit signalQualityChanged(this->signalQuality);

        this->signalQualityDescription = "";
        emit signalQualityDescriptionChanged(this->signalQualityDescription);

        this->process->deleteLater();
    });

    this->timer->start(2000);
}

void WifiSignalQualityModel::timeout()
{
    if (this->process == nullptr || this->process->state() == QProcess::Running)
        return;

    this->process->start("sh", QStringList() << "-c" << "iwconfig wlan0 | grep 'Quality' | sed 's/^ *//g' | cut -d '=' -f 2 | cut -d ' ' -f 1");
}
