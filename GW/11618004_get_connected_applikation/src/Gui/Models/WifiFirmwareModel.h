#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QProcess>

class GUI_EXPORT WifiFirmwareModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool firmwareLoaded READ getFirmwareLoaded NOTIFY firmwareLoadedChanged)
    Q_PROPERTY(QString firmwareLoadedDescription READ getFirmwareLoadedDescription NOTIFY firmwareLoadedDescriptionChanged)

    public:
        explicit WifiFirmwareModel(Context* context, QObject* parent = 0);
        ~WifiFirmwareModel() {}

        void runTests() override;

        Q_SIGNAL void firmwareLoadedChanged(bool value);
        Q_SIGNAL void firmwareLoadedDescriptionChanged(const QString& value);

        bool getFirmwareLoaded() { return this->firmwareLoaded; }
        QString getFirmwareLoadedDescription() { return this->firmwareLoadedDescription; }

    private:
        QProcess* process = nullptr;

        bool firmwareLoaded = false;

        QString firmwareLoadedDescription;
};
