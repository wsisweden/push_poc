#include "WifiSignalStrengthModel.h"
#include "../../Common/Context.h"

WifiSignalStrengthModel::WifiSignalStrengthModel(Context* context, QObject* parent)
    : AbstractModel(context, parent)
{
    this->timer = new QTimer(this);
    connect(this->timer, &QTimer::timeout, this, &WifiSignalStrengthModel::timeout);
}

void WifiSignalStrengthModel::runTests()
{
    this->process = new QProcess(this);
    connect(this->process, &QProcess::readyRead, [this]()
    {
        // Generally,
        //    db >= -50 db = 100% quality
        //    db <= -100 db = 0% quality
        //
        // For example:
        //     High quality: 90% ~= -55db
        //     Good quality: 50% ~= -75db
        //     Low quality: 30% ~= -85db
        //     Bad quality: 30% ~= -95db
        //     Unusable: 8% ~= -96db
        //
        // See: http://www.speedguide.net/faq/how-does-rssi-dbm-relate-to-signal-quality-percent-439
        QString strength = this->process->readAllStandardOutput().trimmed();
        if (!strength.isEmpty())
            this->signalStrength = (strength.toInt() >= -55) ? true : false;
    });
    connect(this->process, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->timer->stop();

        this->signalStrength = false;
        emit signalStrengthChanged(this->signalStrength);
    });
    connect(this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        this->timer->stop();

        emit signalStrengthChanged(this->signalStrength);

        this->signalStrengthDescription = "";
        emit signalStrengthDescriptionChanged(this->signalStrengthDescription);

        this->process->deleteLater();
    });

    this->timer->start(2000);
}

void WifiSignalStrengthModel::timeout()
{
    if (this->process == nullptr || this->process->state() == QProcess::Running)
        return;

    this->process->start("sh", QStringList() << "-c" << "iwconfig wlan0 | grep 'Signal' | sed 's/^ *//g' | cut -d '=' -f 3 | cut -d ' ' -f 1");
}
