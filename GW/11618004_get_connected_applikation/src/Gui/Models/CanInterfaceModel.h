#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QProcess>

class GUI_EXPORT CanInterfaceModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool interfaceDetected READ getInterfaceDetected NOTIFY interfaceDetectedChanged)
    Q_PROPERTY(QString interfaceDetectedDescription READ getInterfaceDetectedDescription NOTIFY interfaceDetectedDescriptionChanged)

    public:
        explicit CanInterfaceModel(Context* context, QObject* parent = 0);
        ~CanInterfaceModel() {}

        void runTests() override;

        Q_SIGNAL void interfaceDetectedChanged(bool value);
        Q_SIGNAL void interfaceDetectedDescriptionChanged(const QString& value);

        bool getInterfaceDetected() { return this->interfaceDetected; }
        QString getInterfaceDetectedDescription() { return this->interfaceDetectedDescription; }

    private:
        QProcess* process = nullptr;

        bool interfaceDetected = false;

        QString interfaceDetectedDescription;
};
