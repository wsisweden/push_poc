#include "AbstractModel.h"

#include <QtCore/QDebug>

AbstractModel::AbstractModel(Context* context, QObject* parent)
    : IReciever(parent)
{
    this->context = context;
}

/**
 * @brief AbstractModel::getConfigurationValue Get stored value
 * @param name The name associated with the configuration value
 */
QVariant AbstractModel::getConfigurationValue(const QString& name)
{
    return QVariant::fromValue(this->context->getStorage()->getConfigurationByName(name).getValue());
}

/**
 * @brief AbstractModel::setConfigurationValue Set configuration value
 * @param name The name associated with the configuration value
 * @param data The attribute data
 */
void AbstractModel::setConfigurationValue(const QString& name, const QVariant& data)
{
    this->context->getStorage()->updateConfiguration(ConfigurationModel(0, name, data.toString()));
}
