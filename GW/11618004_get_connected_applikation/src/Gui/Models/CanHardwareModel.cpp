#include "CanHardwareModel.h"
#include "../../Common/Context.h"

CanHardwareModel::CanHardwareModel(Context* context, QObject* parent)
    : AbstractModel(context, parent) {}

void CanHardwareModel::runTests()
{
    this->process = new QProcess(this);
    connect(this->process, &QProcess::readyRead, [this]()
    {
        this->hardwareDetected = this->process->readAllStandardOutput().trimmed().isEmpty() ? false : true;
    });
    connect(this->process, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->hardwareDetected = false;
        emit hardwareDetectedChanged(this->hardwareDetected);
    });
    connect(this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        emit hardwareDetectedChanged(this->hardwareDetected);

        this->hardwareDetectedDescription = "";
        emit hardwareDetectedDescriptionChanged(this->hardwareDetectedDescription);

        this->process->deleteLater();
    });
    this->process->start("sh", QStringList() << "-c" << "dmesg | grep 'can0: link becomes ready'");
}
