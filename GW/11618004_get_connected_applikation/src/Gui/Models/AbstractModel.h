#pragma once

#include "../Shared.h"
#include "../../Common/Context.h"
#include "../../MpAccess/IReciever.h"
#include "../../MpAccess/Message/Model/IMpModel.h"

#include <QtCore/QObject>
#include <QtCore/QVariant>

class GUI_EXPORT AbstractModel : public IReciever
{
    Q_OBJECT

    public:
        explicit AbstractModel(Context* context, QObject* parent = 0);
        ~AbstractModel() {}

        virtual void runTests() = 0;

        void setRadioAddress(const quint8 channel, const quint16 panId, const quint16 address, const quint32 firmwareType, const quint32 firmwareVersion) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(firmwareType); Q_UNUSED(firmwareVersion) }
        void addNetwork(const quint8 channel, const quint16 panId) { Q_UNUSED(channel); Q_UNUSED(panId) }
        void addNode(const quint32 id, const quint8 channel, const quint16 panId, const quint16 address, const quint8 productType) { Q_UNUSED(id); Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(productType) }
        void addNodeInfo(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }
        void addNodeMeas(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }
        void addNodeStatus(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }
        void addNodeConfig(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }
        void addNodeHistoryLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }
        void addNodeEventLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }
        void addNodeInstantLog(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }
        void updateParam(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model) { Q_UNUSED(channel); Q_UNUSED(panId); Q_UNUSED(address); Q_UNUSED(model) }

        QVariant getConfigurationValue(const QString& name);
        void setConfigurationValue(const QString& name, const QVariant& data);

    protected:
        Context* context = nullptr;
};
