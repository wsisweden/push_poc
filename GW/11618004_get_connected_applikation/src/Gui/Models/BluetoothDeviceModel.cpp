#include "BluetoothDeviceModel.h"
#include "../../Common/Context.h"

BluetoothDeviceModel::BluetoothDeviceModel(Context* context, QObject* parent)
    : AbstractModel(context, parent) {}

void BluetoothDeviceModel::runTests()
{
    this->deviceDetectedDescription = "Scanning for bluetooth device";
    emit deviceDetectedDescriptionChanged(this->deviceDetectedDescription);

    this->process = new QProcess(this);
    connect(this->process, &QProcess::readyRead, [this]()
    {
        this->deviceDetected = this->process->readAllStandardOutput().trimmed().isEmpty() ? false : true;
    });
    connect(this->process, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->deviceDetected = false;
        emit deviceDetectedChanged(this->deviceDetected);
    });
    connect(this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        emit deviceDetectedChanged(this->deviceDetected);

        this->deviceDetectedDescription = this->deviceDetected ? "" : "Timeout occured";
        emit deviceDetectedDescriptionChanged(this->deviceDetectedDescription);

        this->process->deleteLater();
    });
    this->process->start("sh", QStringList() << "-c" << "hcitool scan | grep 'MP_TEST'");
}
