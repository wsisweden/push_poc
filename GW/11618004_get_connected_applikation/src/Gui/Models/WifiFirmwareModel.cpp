#include "WifiFirmwareModel.h"
#include "../../Common/Context.h"

WifiFirmwareModel::WifiFirmwareModel(Context* context, QObject* parent)
    : AbstractModel(context, parent) {}

void WifiFirmwareModel::runTests()
{
    this->process = new QProcess(this);
    connect(this->process, &QProcess::readyRead, [this]()
    {
        this->firmwareLoaded = this->process->readAllStandardOutput().trimmed().isEmpty() ? false : true;
    });
    connect(this->process, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->firmwareLoaded = false;
        emit firmwareLoadedChanged(this->firmwareLoaded);
    });
    connect(this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        emit firmwareLoadedChanged(this->firmwareLoaded);

        this->firmwareLoadedDescription = "";
        emit firmwareLoadedDescriptionChanged(this->firmwareLoadedDescription);

        this->process->deleteLater();
    });
    this->process->start("sh", QStringList() << "-c" << "dmesg | grep 'wlcore: firmware booted'");
    //this->process->start("sh", QStringList() << "-c" << "dmesg | grep 'wlcore: loaded'");
}
