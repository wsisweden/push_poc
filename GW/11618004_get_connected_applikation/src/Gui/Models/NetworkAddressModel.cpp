#include "NetworkAddressModel.h"
#include "../../Common/Context.h"

#include <QtCore/QFile>
#include <QtCore/QTextStream>

#include <QtNetwork/QHostAddress>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QNetworkAddressEntry>

NetworkAddressModel::NetworkAddressModel(Context* context, QObject* parent)
    : AbstractModel(context, parent)
{
    this->timer = new QTimer(this);
    connect(this->timer, &QTimer::timeout, this, &NetworkAddressModel::timeout);
}

NetworkAddressModel::~NetworkAddressModel()
{
    resetWirelessConfig();
}

void NetworkAddressModel::runTests()
{
    this->addressDetectedDescription = "Acquiring IP from the router";
    emit addressDetectedDescriptionChanged(this->addressDetectedDescription);

    writeWirelessConfig();

    this->address = new QProcess(this);
    connect(this->address, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        this->timer->start(2000);

        this->address->deleteLater();
    });
    this->address->start("sh", QStringList() << "-c" << "/etc/init.d/S40network restart");
}

void NetworkAddressModel::writeWirelessConfig()
{
    qInfo().noquote() << "Writing wireless config";

    QString config("ap_scan=1\n\n"
                   "network={\n"
                   "    ssid=\"#SSID#\"\n"
                   "    psk=\"#PSK#\"\n"
                   "}\n");

    config.replace("#SSID#", "MP_TEST");
    config.replace("#PSK#", "&Yf=!3tz%%kq");

    QFile file("/etc/wpa_supplicant.conf");
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        stream << config;
        stream.flush();
    }
    file.close();
}

void NetworkAddressModel::resetWirelessConfig()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "echo "" > /etc/wpa_supplicant.conf");
    process.waitForFinished();
    process.close();
}

void NetworkAddressModel::timeout()
{
    if (this->ttl == 15)
    {
        this->timer->stop();

        resetWirelessConfig();

        this->addressDetected = false;
        emit addressDetectedChanged(this->addressDetected);

        this->addressDetectedDescription = "Timeout occured";
        emit addressDetectedDescriptionChanged(this->addressDetectedDescription);
    }
    else
    {
        QNetworkInterface eth = QNetworkInterface::interfaceFromName("eth0");
        QNetworkInterface wlan = QNetworkInterface::interfaceFromName("wlan0");
        if (eth.isValid() && wlan.isValid())
        {
            if (!eth.addressEntries().isEmpty() && !wlan.addressEntries().isEmpty())
            {
                this->timer->stop();

                resetWirelessConfig();

                this->addressDetected = true;
                emit addressDetectedChanged(this->addressDetected);

                this->addressDetectedDescription = "";
                emit addressDetectedDescriptionChanged(this->addressDetectedDescription);
            }
        }

        this->ttl++;
    }
}
