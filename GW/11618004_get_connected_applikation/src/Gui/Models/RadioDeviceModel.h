#pragma once

#include "../Shared.h"
#include "AbstractModel.h"
#include "../../Common/Context.h"
#include "../../MpAccess/IDevice.h"
#include "../../Common/Event/NodeTimeoutEvent.h"
#include "../../MpAccess/Message/Model/IMpModel.h"
#include "../../MpAccess/Command/Handler/CommandHandlerFactory.h"

#include <QtCore/QTimer>

class GUI_EXPORT RadioDeviceModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool hardwareDetected READ getHardwareDetected NOTIFY hardwareDetectedChanged)
    Q_PROPERTY(QString hardwareDetectedDescription READ getHardwareDetectedDescription NOTIFY hardwareDetectedDescriptionChanged)

    Q_PROPERTY(bool deviceDetected READ getDeviceDetected NOTIFY deviceDetectedChanged)
    Q_PROPERTY(QString deviceDetectedDescription READ getDeviceDetectedDescription NOTIFY deviceDetectedDescriptionChanged)

    public:
        explicit RadioDeviceModel(Context* context, QObject* parent = 0);
        ~RadioDeviceModel() {}

        void runTests() override;

        void setRadioAddress(const quint8 channel, const quint16 panId, const quint16 address, const quint32 firmwareType, const quint32 firmwareVersion);
        void addNodeInfo(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);
        void updateParam(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model);

        Q_SIGNAL void hardwareDetectedChanged(bool value);
        Q_SIGNAL void hardwareDetectedDescriptionChanged(const QString& value);

        Q_SIGNAL void deviceDetectedChanged(bool value);
        Q_SIGNAL void deviceDetectedDescriptionChanged(const QString& value);

        bool getHardwareDetected() { return this->hardwareDetected; }
        QString getHardwareDetectedDescription() { return this->hardwareDetectedDescription; }

        bool getDeviceDetected() { return this->deviceDetected; }
        QString getDeviceDetectedDescription() { return this->deviceDetectedDescription; }

    private:
        bool hardwareDetected = false;
        bool deviceDetected = false;

        IDevice* device = nullptr;
        QTimer* hardwareTimer = nullptr;
        QTimer* deviceTimer = nullptr;
        CommandHandlerFactory* commandFactory = nullptr;

        QString hardwareDetectedDescription;
        QString deviceDetectedDescription;

        Q_SLOT void hardwareTimeout();
        Q_SLOT void deviceTimeout();
};
