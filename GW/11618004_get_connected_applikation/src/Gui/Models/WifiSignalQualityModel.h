#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QTimer>
#include <QtCore/QProcess>

class GUI_EXPORT WifiSignalQualityModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool signalQuality READ getSignalQuality NOTIFY signalQualityChanged)
    Q_PROPERTY(QString signalQualityDescription READ getSignalQualityDescription NOTIFY signalQualityDescriptionChanged)

    public:
        explicit WifiSignalQualityModel(Context* context, QObject* parent = 0);
        ~WifiSignalQualityModel() {}

        void runTests() override;

        Q_SIGNAL void signalQualityChanged(bool value);
        Q_SIGNAL void signalQualityDescriptionChanged(const QString& value);

        bool getSignalQuality() { return this->signalQuality; }
        QString getSignalQualityDescription() { return this->signalQualityDescription; }

    private:
        QTimer* timer = nullptr;
        QProcess* process = nullptr;

        bool signalQuality = false;

        QString signalQualityDescription;

        Q_SLOT void timeout();
};
