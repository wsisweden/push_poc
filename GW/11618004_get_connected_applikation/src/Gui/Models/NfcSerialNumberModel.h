#pragma once

#include "../Shared.h"
#include "AbstractModel.h"
#include "../../Common/Event/ConfigurationEvent.h"

#include <QtCore/QTimer>

class GUI_EXPORT NfcSerialNumberModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool serialNumberDetected READ getSerialNumbereDetected NOTIFY serialNumberDetectedChanged)
    Q_PROPERTY(QString serialNumberDetectedDescription READ getSerialNumberDetectedDescription NOTIFY serialNumberDetectedDescriptionChanged)

    public:
        explicit NfcSerialNumberModel(Context* context, QObject* parent = 0);
        ~NfcSerialNumberModel() {}

        void runTests() override;

        Q_SIGNAL void serialNumberDetectedChanged(bool value);
        Q_SIGNAL void serialNumberDetectedDescriptionChanged(const QString& value);

        bool getSerialNumbereDetected() { return this->serialNumberDetected; }
        QString getSerialNumberDetectedDescription() { return this->serialNumberDetectedDescription; }

    private:
        int ttl = 0;

        QTimer* timer = nullptr;

        bool serialNumberDetected = false;

        QString serialNumber;
        QString serialNumberDetectedDescription;

        Q_SLOT void timeout();
        Q_SLOT void configurationReceived(const ConfigurationEvent& event);
};
