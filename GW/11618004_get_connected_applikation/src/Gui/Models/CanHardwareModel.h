#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QProcess>

class GUI_EXPORT CanHardwareModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool hardwareDetected READ getHardwareDetected NOTIFY hardwareDetectedChanged)
    Q_PROPERTY(QString hardwareDetectedDescription READ getHardwareDetectedDescription NOTIFY hardwareDetectedDescriptionChanged)

    public:
        explicit CanHardwareModel(Context* context, QObject* parent = 0);
        ~CanHardwareModel() {}

        void runTests() override;

        Q_SIGNAL void hardwareDetectedChanged(bool value);
        Q_SIGNAL void hardwareDetectedDescriptionChanged(const QString& value);

        bool getHardwareDetected() { return this->hardwareDetected; }
        QString getHardwareDetectedDescription() { return this->hardwareDetectedDescription; }

    private:
        QProcess* process = nullptr;

        bool hardwareDetected = false;

        QString hardwareDetectedDescription;
};
