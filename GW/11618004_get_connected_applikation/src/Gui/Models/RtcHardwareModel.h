#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QProcess>

class GUI_EXPORT RtcHardwareModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool rtcHardwareDetected READ getRtcHardwareDetected NOTIFY rtcHardwareDetectedChanged)
    Q_PROPERTY(QString rtcHardwareDetectedDescription READ getRtcHardwareDetectedDescription NOTIFY rtcHardwareDetectedDescriptionChanged)

    public:
        explicit RtcHardwareModel(Context* context, QObject* parent = 0);
        ~RtcHardwareModel() {}

        void runTests() override;

        Q_SIGNAL void rtcHardwareDetectedChanged(bool value);
        Q_SIGNAL void rtcHardwareDetectedDescriptionChanged(const QString& value);

        bool getRtcHardwareDetected() { return this->rtcHardwareDetected; }
        QString getRtcHardwareDetectedDescription() { return this->rtcHardwareDetectedDescription; }

    private:
        QProcess* process = nullptr;

        bool rtcHardwareDetected = false;

        QString rtcHardwareDetectedDescription;
};
