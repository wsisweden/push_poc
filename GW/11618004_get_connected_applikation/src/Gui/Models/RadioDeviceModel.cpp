#include "RadioDeviceModel.h"
#include "../../Common/Context.h"
#include "../../MpAccess/Device/UartDevice.h"
#include "../../MpAccess/Message/Model/SetParamModel.h"
#include "../../MpAccess/Message/Request/GetInfoRequest.h"
#include "../../MpAccess/Command/Request/GetRadioParamRequest.h"

#include <QtCore/QDebug>

RadioDeviceModel::RadioDeviceModel(Context* context, QObject* parent)
    : AbstractModel(context, parent)
{
    this->hardwareTimer = new QTimer(this);
    connect(this->hardwareTimer, &QTimer::timeout, this, &RadioDeviceModel::hardwareTimeout);

    this->deviceTimer = new QTimer(this);
    connect(this->deviceTimer, &QTimer::timeout, this, &RadioDeviceModel::deviceTimeout);
}

void RadioDeviceModel::runTests()
{
    if (this->context->getCommandLineParser()->isSet("devicetype") && this->context->getCommandLineParser()->value("devicetype") == "uart")
        this->device = (IDevice*)new UartDevice(this->context, this);

    if (this->device == nullptr || !this->device->isOpen())
        return;

    this->commandFactory = new CommandHandlerFactory(this->context, *this, this);
    this->device->setCommandFactory(this->commandFactory);

    GetRadioParamRequest* request = new GetRadioParamRequest();
    this->device->set(request);

    this->hardwareTimer->start(500);
}

void RadioDeviceModel::setRadioAddress(const quint8 channel, const quint16 panId, const quint16 address, const quint32 firmwareType, const quint32 firmwareVersion)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);
    Q_UNUSED(firmwareType);
    Q_UNUSED(firmwareVersion);

    this->hardwareTimer->stop();
    this->device->setFirmwareAddress(address);

    this->hardwareDetected = true;
    emit hardwareDetectedChanged(this->hardwareDetected);

    this->hardwareDetectedDescription = "";
    emit hardwareDetectedDescriptionChanged(this->hardwareDetectedDescription);

    GetInfoRequest* request = new GetInfoRequest(12, 12345, 123);
    this->device->send(request);

    this->deviceTimer->start(7000);
}

void RadioDeviceModel::addNodeInfo(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);
    Q_UNUSED(address);
    Q_UNUSED(model);

    this->deviceTimer->stop();

    this->deviceDetected = true;
    emit deviceDetectedChanged(this->deviceDetected);

    this->deviceDetectedDescription = "";
    emit deviceDetectedDescriptionChanged(this->deviceDetectedDescription);
}

void RadioDeviceModel::updateParam(const quint8 channel, const quint16 panId, const quint16 address, IMpModel* model)
{
    Q_UNUSED(channel);
    Q_UNUSED(panId);
    Q_UNUSED(address);

    SetParamModel* setParamModel = (SetParamModel*)model;
    this->context->getEventManager()->emitConfigurationReceivedEvent(ConfigurationEvent(setParamModel->getIndex(), setParamModel->getData()));
}

void RadioDeviceModel::hardwareTimeout()
{
    this->hardwareTimer->stop();

    this->hardwareDetected = false;
    emit hardwareDetectedChanged(this->hardwareDetected);

    this->hardwareDetectedDescription = "Timeout occured";
    emit hardwareDetectedDescriptionChanged(this->hardwareDetectedDescription);
}

void RadioDeviceModel::deviceTimeout()
{
    this->deviceTimer->stop();

    this->deviceDetected = false;
    emit deviceDetectedChanged(this->deviceDetected);

    this->deviceDetectedDescription = "Timeout occured";
    emit deviceDetectedDescriptionChanged(this->deviceDetectedDescription);
}
