#include "NfcSerialNumberModel.h"
#include "../../Common/Constant.h"
#include "../../Common/Context.h"

#include <QtCore/QDebug>

NfcSerialNumberModel::NfcSerialNumberModel(Context* context, QObject* parent)
    : AbstractModel(context, parent)
{
    this->timer = new QTimer(this);
    connect(this->timer, &QTimer::timeout, this, &NfcSerialNumberModel::timeout);
    connect(this->context->getEventManager(), &EventManager::configurationReceived, this, &NfcSerialNumberModel::configurationReceived, Qt::QueuedConnection);
}

void NfcSerialNumberModel::runTests()
{
    this->timer->start(2000);
}

void NfcSerialNumberModel::configurationReceived(const ConfigurationEvent& event)
{
    if (event.getIndex() == Param::SERIAL_NUMBER)
    {
        QString value = QString::number(event.getData().toULongLong());
        if (value.isEmpty())
            return;

        this->timer->stop();

        this->serialNumber = value;

        qInfo().noquote() << QString("Serial number changed (%1)").arg(this->serialNumber);

        this->context->getStorage()->updateConfiguration(ConfigurationModel(0, "SerialNumber", this->serialNumber));

        this->serialNumberDetected = true;
        emit serialNumberDetectedChanged(this->serialNumberDetected);

        this->serialNumberDetectedDescription = "";
        emit serialNumberDetectedDescriptionChanged(this->serialNumberDetectedDescription);
    }
}

void NfcSerialNumberModel::timeout()
{
    if (this->ttl == 150)
    {
        this->timer->stop();

        this->serialNumberDetected = false;
        emit serialNumberDetectedChanged(this->serialNumberDetected);

        this->serialNumberDetectedDescription = "Timeout occured";
        emit serialNumberDetectedDescriptionChanged(this->serialNumberDetectedDescription);
    }

    this->ttl++;
}
