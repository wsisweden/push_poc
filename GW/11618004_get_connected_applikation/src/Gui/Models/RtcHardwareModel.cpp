#include "RtcHardwareModel.h"
#include "../../Common/Context.h"

RtcHardwareModel::RtcHardwareModel(Context* context, QObject* parent)
    : AbstractModel(context, parent) {}

void RtcHardwareModel::runTests()
{
    this->process = new QProcess(this);
    connect(this->process, &QProcess::readyRead, [this]()
    {
        this->rtcHardwareDetected = this->process->readAllStandardOutput().trimmed().isEmpty() ? false : true;
    });
    connect(this->process, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->rtcHardwareDetected = false;
        emit rtcHardwareDetectedChanged(this->rtcHardwareDetected);
    });
    connect(this->process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        emit rtcHardwareDetectedChanged(this->rtcHardwareDetected);

        this->rtcHardwareDetectedDescription = "";
        emit rtcHardwareDetectedDescriptionChanged(this->rtcHardwareDetectedDescription);

        this->process->deleteLater();
    });
    this->process->start("sh", QStringList() << "-c" << "dmesg | grep 'rtc-pcf2123 spi1.0: chip found'");
}
