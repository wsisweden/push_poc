#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QProcess>

class GUI_EXPORT NetworkInterfaceModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool interfaceDetected READ getInterfaceDetected NOTIFY interfaceDetectedChanged)
    Q_PROPERTY(QString interfaceDetectedDescription READ getInterfaceDetectedDescription NOTIFY interfaceDetectedDescriptionChanged)

    public:
        explicit NetworkInterfaceModel(Context* context, QObject* parent = 0);
        ~NetworkInterfaceModel() {}

        void runTests() override;

        Q_SIGNAL void interfaceDetectedChanged(bool value);
        Q_SIGNAL void interfaceDetectedDescriptionChanged(const QString& value);

        bool getInterfaceDetected() { return this->interfaceDetected; }
        QString getInterfaceDetectedDescription() { return this->interfaceDetectedDescription; }

    private:
        QProcess* wlanProcess = nullptr;
        QProcess* ethernetProcess = nullptr;

        bool wlanDetected = false;
        bool ethernetDetected = false;
        bool interfaceDetected = false;

        QString interfaceDetectedDescription;
};
