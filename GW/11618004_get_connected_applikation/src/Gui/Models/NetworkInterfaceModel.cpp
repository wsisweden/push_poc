#include "NetworkInterfaceModel.h"
#include "../../Common/Context.h"

NetworkInterfaceModel::NetworkInterfaceModel(Context* context, QObject* parent)
    : AbstractModel(context, parent) {}

void NetworkInterfaceModel::runTests()
{
    this->wlanProcess = new QProcess(this);
    connect(this->wlanProcess, &QProcess::readyRead, [this]()
    {
        this->wlanDetected = this->wlanProcess->readAll().trimmed().isEmpty() ? false : true;
    });
    connect(this->wlanProcess, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->interfaceDetected = false;
        emit interfaceDetectedChanged(this->interfaceDetected);
    });
    connect(this->wlanProcess, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        this->interfaceDetected = this->ethernetDetected && this->wlanDetected;

        emit interfaceDetectedChanged(this->interfaceDetected);

        this->interfaceDetectedDescription = "";
        emit interfaceDetectedDescriptionChanged(this->interfaceDetectedDescription);

        this->wlanProcess->deleteLater();
    });

    this->ethernetProcess = new QProcess(this);
    connect(this->ethernetProcess, &QProcess::readyRead, [this]()
    {
        this->ethernetDetected = this->ethernetProcess->readAll().trimmed().isEmpty() ? false : true;
    });
    connect(this->ethernetProcess, &QProcess::errorOccurred, [this](QProcess::ProcessError error)
    {
        Q_UNUSED(error);

        this->interfaceDetected = false;
        emit interfaceDetectedChanged(this->interfaceDetected);
    });
    connect(this->ethernetProcess, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus exitStatus)
    {
        Q_UNUSED(exitCode);
        Q_UNUSED(exitStatus);

        this->ethernetProcess->deleteLater();

        this->wlanProcess->start("sh", QStringList() << "-c" << "ifconfig | grep 'wlan0'");
    });
    this->ethernetProcess->start("sh", QStringList() << "-c" << "ifconfig | grep 'eth0'");
}


