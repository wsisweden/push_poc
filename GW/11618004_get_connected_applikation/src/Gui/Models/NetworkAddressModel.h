#pragma once

#include "../Shared.h"
#include "AbstractModel.h"

#include <QtCore/QTimer>
#include <QtCore/QProcess>

class GUI_EXPORT NetworkAddressModel : public AbstractModel
{
    Q_OBJECT

    Q_PROPERTY(bool addressDetected READ getAddressDetected NOTIFY addressDetectedChanged)
    Q_PROPERTY(QString addressDetectedDescription READ getAddressDetectedDescription NOTIFY addressDetectedDescriptionChanged)

    public:
        explicit NetworkAddressModel(Context* context, QObject* parent = 0);
        ~NetworkAddressModel();

        void runTests() override;

        Q_SIGNAL void addressDetectedChanged(bool value);
        Q_SIGNAL void addressDetectedDescriptionChanged(const QString& value);

        bool getAddressDetected() { return this->addressDetected; }
        QString getAddressDetectedDescription() { return this->addressDetectedDescription; }

    private:
        int ttl = 0;

        QTimer* timer = nullptr;
        QProcess* address = nullptr;

        bool wlanDetected = false;
        bool ethernetDetected = false;
        bool addressDetected = false;

        QString addressDetectedDescription;

        void writeWirelessConfig();
        void resetWirelessConfig();

        Q_SLOT void timeout();
};
