import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import "qrc:/Controls"

ApplicationWindow {
    id: applicationWindow
    objectName: "RootView"
    width: Style.applicationWidth
    height: Style.applicationHeight
    x: Screen.width / 2 - width / 2
    y: Screen.height / 2 - height / 2
    color: Style.colorLight
    title: qsTr("Get Connected")
    visible: true

    property bool activateLoaders: false

    Rectangle {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right }
        height: Style.applicationHeaderHeight
        color: Style.colorHeader

        Image {
            anchors { left: parent.left; verticalCenter: parent.verticalCenter; leftMargin: Style.defaultMargin * 4 }
            source: "qrc:/Images/Logo.png"
            height: 50
            fillMode: Image.PreserveAspectFit
        }
    }

    ListModel {
       id: listModel

       ListElement {
           source: "qrc:/Views/NetworkDelegateView.qml"
           title: qsTr("Network")
           subtitle: qsTr("TC11")
       }

       ListElement {
           source: "qrc:/Views/RadioDelegateView.qml"
           title: qsTr("Radio")
           subtitle: qsTr("TC12")
       }

       ListElement {
           source: "qrc:/Views/CanDelegateView.qml"
           title: qsTr("CAN")
           subtitle: qsTr("TC13")
       }

       ListElement {
           source: "qrc:/Views/BluetoothDelegateView.qml"
           title: qsTr("Bluetooth")
           subtitle: qsTr("TC14")
       }

       ListElement {
           source: "qrc:/Views/XxcDelegateView.qml"
           title: qsTr("RTC, NFC")
           subtitle: qsTr("TC15, TC16")
       }
    }

    ListView {
        id: listView
        anchors { top: header.bottom; right: parent.right; left: parent.left; bottom: parent.bottom; topMargin: Style.defaultMargin * 4 }
        width: parent.width
        height: parent.height - header.height
        spacing: 20
        clip: true
        model: listModel

        ScrollIndicator.vertical: ScrollIndicator { }

        delegate: Rectangle {
            width: listView.width
            height: childrenRect.height
            color: Style.colorLight

            property var delegateModel: model

            Loader {
                id: loader
                width: parent.width
                source: model.source

                property var model: parent.delegateModel
            }
        }
    }
}
