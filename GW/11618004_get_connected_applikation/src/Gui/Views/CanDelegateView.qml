import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import "qrc:/Controls"

Rectangle {
    id: delegate
    objectName: "CanDelegateView"
    anchors { left: parent.left; right: parent.right; leftMargin: Style.defaultMargin * 4; rightMargin: Style.defaultMargin * 4 }
    width: parent.width
    height: Style.delegateHeight * 4

    Rectangle {
        id: background
        anchors { fill: parent }
        radius: 4
        color: Style.colorDelegate
        border { width: 1; color: Style.colorDelegateShadow }
    }

    DropShadow {
        anchors { fill: background }
        horizontalOffset: 1
        verticalOffset: 3
        radius: 3
        source: background
        color: Style.colorDelegateShadow
    }

    RowLayout {
        id: header
        anchors { top: parent.top; left: parent.left; right: parent.right; topMargin: Style.defaultMargin * 2; leftMargin: Style.defaultMargin * 2; rightMargin: Style.defaultMargin * 2 }

        TitleItem {
            anchors { left: parent.left }
            text: model ? model.title : ""
        }

        SubTitleItem {
            anchors { right: parent.right }
            text: model ? model.subtitle : ""
        }
    }

    RowLayout {
        anchors { top: header.bottom; left: parent.left; right: parent.right; bottom: parent.bottom; topMargin: Style.defaultMargin * 2; leftMargin: Style.defaultMargin * 30 }

        ColumnLayout {
            anchors.top: parent.top
            width: parent.width
            spacing: 0

            RowLayout {
                anchors { left: parent.left; right: parent.right }

                ResultItem {
                    anchors.top: parent.top
                    result: canDelegateViewModel ? canDelegateViewModel.hardwareDetected : "false"

                    Layout.preferredWidth: 32
                    Layout.preferredHeight: 32
                }

                ColumnLayout {
                    anchors.top: parent.top
                    spacing: -10

                    TextItem {
                        text: "Hardware detected"

                        Layout.margins: { left: Style.defaultMargin }
                    }

                    SubTextItem {
                        text: canDelegateViewModel ? canDelegateViewModel.hardwareDetectedDescription : ""

                        Layout.margins: { left: Style.defaultMargin }
                    }
                }
            }

            RowLayout {
                anchors { left: parent.left; right: parent.right }

                ResultItem {
                    anchors.top: parent.top
                    result: canDelegateViewModel ? canDelegateViewModel.interfaceDetected : "false"

                    Layout.preferredWidth: 32
                    Layout.preferredHeight: 32
                }

                ColumnLayout {
                    anchors.top: parent.top
                    spacing: -10

                    TextItem {
                        text: "Interface detected"

                        Layout.margins: { left: Style.defaultMargin }
                    }

                    SubTextItem {
                        text: canDelegateViewModel ? canDelegateViewModel.interfaceDetectedDescription : ""

                        Layout.margins: { left: Style.defaultMargin }
                    }
                }
            }
        }

        ColumnLayout {
            anchors.top: parent.top
            width: parent.width
            spacing: 10

            RowLayout {
                anchors { left: parent.left; right: parent.right }

                /*
                ResultItem {
                    anchors.top: parent.top
                    result: canDelegateViewModel ? canDelegateViewModel.? : "false"

                    Layout.preferredWidth: 32
                    Layout.preferredHeight: 32
                }

                ColumnLayout {
                    anchors.top: parent.top
                    spacing: -10

                    TextItem {
                        text: ""

                        Layout.margins: { left: Style.defaultMargin }
                    }

                    SubTextItem {
                        text: canDelegateViewModel ? canDelegateViewModel.? : ""

                        Layout.margins: { left: Style.defaultMargin }
                    }
                }
                */
            }
        }
    }

    Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", delegate); canDelegateViewModel.runTests() }
    Component.onDestruction: { viewModelHandler.removeViewModel(objectName + "Model") }
}

