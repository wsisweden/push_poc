import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

import "qrc:/Controls"

Rectangle {
    id: delegate
    objectName: "XxcDelegateView"
    anchors { left: parent.left; right: parent.right; leftMargin: Style.defaultMargin * 4; rightMargin: Style.defaultMargin * 4 }
    width: parent.width
    height: Style.delegateHeight * 4

    Rectangle {
        anchors { left: parent.left }
        width: (parent.width / 2) - (Style.defaultMargin * 2)
        height: Style.delegateHeight * 4

        Rectangle {
            id: rtcBackground
            anchors { fill: parent }
            radius: 4
            color: Style.colorDelegate
            border { width: 1; color: Style.colorDelegateShadow }
        }

        DropShadow {
            anchors { fill: rtcBackground }
            horizontalOffset: 2
            verticalOffset: 3
            radius: 3
            source: rtcBackground
            color: Style.colorDelegateShadow
        }

        RowLayout {
            id: rtcHeader
            anchors { top: parent.top; left: parent.left; right: parent.right; topMargin: Style.defaultMargin * 2; leftMargin: Style.defaultMargin * 2; rightMargin: Style.defaultMargin * 2 }

            TitleItem {
                anchors { left: parent.left }
                text: model ? model.title.split(',')[0] : ""
            }

            SubTitleItem {
                anchors { right: parent.right }
                text: model ? model.subtitle.split(',')[0] : ""
            }
        }

        RowLayout {
            anchors { top: rtcHeader.bottom; left: parent.left; right: parent.right; bottom: parent.bottom; topMargin: Style.defaultMargin * 2; leftMargin: Style.defaultMargin * 30 }

            ColumnLayout {
                anchors.top: parent.top
                width: parent.width
                spacing: 0

                RowLayout {
                    anchors { left: parent.left; right: parent.right }

                    ResultItem {
                        anchors.top: parent.top
                        result: xxcDelegateViewModel ? xxcDelegateViewModel.hardwareDetected : "false"

                        Layout.preferredWidth: 32
                        Layout.preferredHeight: 32
                    }

                    ColumnLayout {
                        anchors.top: parent.top
                        spacing: -10

                        TextItem {
                            text: "Hardware clock detected"

                            Layout.margins: { left: Style.defaultMargin }
                        }

                        SubTextItem {
                            text: xxcDelegateViewModel ? xxcDelegateViewModel.hardwareDetectedDescription : ""

                            Layout.margins: { left: Style.defaultMargin }
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        anchors { right: parent.right }
        width: (parent.width / 2) - (Style.defaultMargin * 2)
        height: Style.delegateHeight * 4

        Rectangle {
            id: nfcBackground
            anchors { fill: parent }
            radius: 4
            color: Style.colorDelegate
            border { width: 1; color: Style.colorDelegateShadow }
        }

        DropShadow {
            anchors { fill: nfcBackground }
            horizontalOffset: 2
            verticalOffset: 3
            radius: 3
            source: nfcBackground
            color: Style.colorDelegateShadow
        }

        RowLayout {
            id: nfcHeader
            anchors { top: parent.top; left: parent.left; right: parent.right; topMargin: Style.defaultMargin * 2; leftMargin: Style.defaultMargin * 2; rightMargin: Style.defaultMargin * 2 }

            TitleItem {
                anchors { left: parent.left }
                text: model ? model.title.split(',')[1] : ""
            }

            SubTitleItem {
                anchors { right: parent.right }
                text: model ? model.subtitle.split(',')[1] : ""
            }
        }

        RowLayout {
            anchors { top: nfcHeader.bottom; left: parent.left; right: parent.right; bottom: parent.bottom; topMargin: Style.defaultMargin * 2; leftMargin: Style.defaultMargin * 30 }

            ColumnLayout {
                anchors.top: parent.top
                width: parent.width
                spacing: 10

                RowLayout {
                    anchors { left: parent.left; right: parent.right }

                    ResultItem {
                        anchors.top: parent.top
                        result: xxcDelegateViewModel ? xxcDelegateViewModel.serialNumberDetected : "false"

                        Layout.preferredWidth: 32
                        Layout.preferredHeight: 32
                    }

                    ColumnLayout {
                        anchors.top: parent.top
                        spacing: -10

                        TextItem {
                            text: "Serial number detected"

                            Layout.margins: { left: Style.defaultMargin }
                        }

                        SubTextItem {
                            text: xxcDelegateViewModel ? xxcDelegateViewModel.serialNumberDetectedDescription : ""

                            Layout.margins: { left: Style.defaultMargin }
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted: { viewModelHandler.addViewModel(objectName + "Model", delegate); xxcDelegateViewModel.runTests() }
    Component.onDestruction: { viewModelHandler.removeViewModel(objectName + "Model") }
}
