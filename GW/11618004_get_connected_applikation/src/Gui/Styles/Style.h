#pragma once

#include "../Shared.h"

#include <QtCore/QString>
#include <QtCore/QObject>

class GUI_EXPORT Style : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int delegateWidth READ getDelegateWidth WRITE setDelegateWidth NOTIFY delegateWidthChanged)
    Q_PROPERTY(int applicationWidth READ getApplicationWidth WRITE setApplicationWidth NOTIFY applicationWidthChanged)
    Q_PROPERTY(int applicationHeight READ getApplicationHeight WRITE setApplicationHeight NOTIFY applicationHeightChanged)
    Q_PROPERTY(int applicationHeaderHeight READ getApplicationHeaderHeight NOTIFY applicationHeaderHeightChanged)
    Q_PROPERTY(int defaultMargin READ getDefaultMargin NOTIFY defaultMarginChanged)
    Q_PROPERTY(int delegateHeight READ getDelegateHeight NOTIFY delegateHeightChanged)

    Q_PROPERTY(QString defaultFont READ getDefaultFont NOTIFY defaultFontChanged)
    Q_PROPERTY(QString defaultFontLight READ getDefaultFontLight NOTIFY defaultFontLightChanged)

    Q_PROPERTY(QString colorLight READ getColorLight NOTIFY colorLightChanged)
    Q_PROPERTY(QString colorDark READ getColorDark NOTIFY colorDarkChanged)
    Q_PROPERTY(QString colorMarked READ getColorMarked NOTIFY colorMarkedChanged)
    Q_PROPERTY(QString colorNight READ getColorNight NOTIFY colorNightChanged)
    Q_PROPERTY(QString colorAccent READ getColorAccent NOTIFY colorAccentChanged)
    Q_PROPERTY(QString colorTextMarked READ getColorTextMarked NOTIFY colorTextMarkedChanged)
    Q_PROPERTY(QString colorText READ getColorText NOTIFY colorTextChanged)
    Q_PROPERTY(QString colorTextDisabled READ getColorTextDisabled NOTIFY colorTextDisabledChanged)
    Q_PROPERTY(QString colorTextDim READ getColorTextDim NOTIFY colorTextDimChanged)
    Q_PROPERTY(QString colorLightGrey READ getColorLightGrey NOTIFY colorLightGreyChanged)
    Q_PROPERTY(QString colorHeader READ getColorHeader NOTIFY colorHeaderChanged)
    Q_PROPERTY(QString colorDelegate READ getColorDelegate NOTIFY colorDelegateChanged)
    Q_PROPERTY(QString colorDelegateShadow READ getColorDelegateShadow NOTIFY colorDelegateShadowChanged)

    public:
        Q_SIGNAL void applicationWidthChanged();
        Q_SIGNAL void applicationHeightChanged();
        Q_SIGNAL void applicationHeaderHeightChanged();
        Q_SIGNAL void defaultMarginChanged();
        Q_SIGNAL void delegateWidthChanged();
        Q_SIGNAL void delegateHeightChanged();

        Q_SIGNAL void defaultFontChanged();
        Q_SIGNAL void defaultFontLightChanged();

        Q_SIGNAL void colorLightChanged();
        Q_SIGNAL void colorDarkChanged();
        Q_SIGNAL void colorMarkedChanged();
        Q_SIGNAL void colorNightChanged();
        Q_SIGNAL void colorAccentChanged();
        Q_SIGNAL void colorTextMarkedChanged();
        Q_SIGNAL void colorTextChanged();
        Q_SIGNAL void colorTextDisabledChanged();
        Q_SIGNAL void colorTextDimChanged();
        Q_SIGNAL void colorLightGreyChanged();
        Q_SIGNAL void colorHeaderChanged();
        Q_SIGNAL void colorDelegateChanged();
        Q_SIGNAL void colorDelegateShadowChanged();

    private:
        int delegateWidth = 1280;
        int applicationWidth = 1280;
        int applicationHeight = 1024;

        const int defaultMargin = 5;
        const int delegateHeight = 40;
        const int applicationHeaderHeight = 100;

        const QString defaultFont = "Roboto";
        const QString defaultFontLight = "Roboto Light";

        const QString colorDark = "#415e6e";
        const QString colorLight = "#fafafa";
        const QString colorMarked = "#627890";
        const QString colorNight = "#525e6c";
        const QString colorAccent = "#74e2f7";
        const QString colorTextMarked = "white";
        const QString colorText = "#000000";
        const QString colorTextDisabled = "#a0a0a0";
        const QString colorTextDim = "#80ffffff";
        const QString colorLightGrey = "#80c8c8c8";
        const QString colorHeader = "#415e6e";
        const QString colorDelegate = "white";
        const QString colorDelegateShadow = "#dde0e1";

        void setDelegateWidth(int width) { this->delegateWidth = width; emit delegateWidthChanged(); }
        void setApplicationWidth(int width) { this->applicationWidth = width; emit applicationWidthChanged(); }
        void setApplicationHeight(int height) { this->applicationHeight = height; emit applicationHeightChanged(); }

        int getApplicationWidth() const { return this->applicationWidth; }
        int getApplicationHeight() const { return this->applicationHeight; }
        int getApplicationHeaderHeight() const { return this->applicationHeaderHeight; }
        int getDefaultMargin() const { return this->defaultMargin; }
        int getDelegateWidth() const { return this->delegateWidth; }
        int getDelegateHeight() const { return this->delegateHeight; }

        QString getDefaultFont() const { return this->defaultFont; }
        QString getDefaultFontLight() const { return this->defaultFontLight; }

        QString getColorLight() const { return this->colorLight; }
        QString getColorDark() const { return this->colorDark; }
        QString getColorMarked() const { return this->colorMarked; }
        QString getColorNight() const { return this->colorNight; }
        QString getColorAccent() const { return this->colorAccent; }
        QString getColorTextMarked() const { return this->colorTextMarked; }
        QString getColorText() const { return this->colorText; }
        QString getColorTextDisabled() const { return this->colorTextDisabled; }
        QString getColorTextDim() const { return this->colorTextDim; }
        QString getColorLightGrey() const { return this->colorLightGrey; }
        QString getColorHeader() const { return this->colorHeader; }
        QString getColorDelegate() const { return this->colorDelegate; }
        QString getColorDelegateShadow() const { return this->colorDelegateShadow; }
};
