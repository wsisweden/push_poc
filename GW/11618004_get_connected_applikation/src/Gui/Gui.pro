QT += core quick

CONFIG += c++11 staticlib

TARGET = gui
TEMPLATE = lib

DEFINES += GUI_LIBRARY

RESOURCES += Gui.qrc

HEADERS += \
    Shared.h \
    ViewModels/ViewModelHandler.h \
    ViewModels/AbstractViewModel.h \
    ViewModels/RootViewModel.h \
    Styles/Style.h \
    ViewModels/BluetoothDelegateViewModel.h \
    ViewModels/CanDelegateViewModel.h \
    ViewModels/RadioDelegateViewModel.h \
    Models/BluetoothHardwareModel.h \
    Models/CanHardwareModel.h \
    Models/WifiFirmwareModel.h \
    Models/AbstractModel.h \
    Models/WifiHardwareModel.h \
    Models/BluetoothDeviceModel.h \
    Models/CanInterfaceModel.h \
    Models/NfcSerialNumberModel.h \
    Models/RadioDeviceModel.h \
    Models/WifiSignalQualityModel.h \
    Models/WifiSignalStrengthModel.h \
    ViewModels/NetworkDelegateViewModel.h \
    Models/NetworkInterfaceModel.h \
    Models/NetworkAddressModel.h \
    Models/RtcHardwareModel.h \
    ViewModels/XxcDelegateViewModel.h

SOURCES += \
    ViewModels/ViewModelHandler.cpp \
    ViewModels/AbstractViewModel.cpp \
    ViewModels/RootViewModel.cpp \
    ViewModels/BluetoothDelegateViewModel.cpp \
    ViewModels/CanDelegateViewModel.cpp \
    ViewModels/RadioDelegateViewModel.cpp \
    Models/BluetoothHardwareModel.cpp \
    Models/CanHardwareModel.cpp \
    Models/WifiFirmwareModel.cpp \
    Models/AbstractModel.cpp \
    Models/WifiHardwareModel.cpp \
    Models/BluetoothDeviceModel.cpp \
    Models/CanInterfaceModel.cpp \
    Models/NfcSerialNumberModel.cpp \
    Models/RadioDeviceModel.cpp \
    Models/WifiSignalQualityModel.cpp \
    Models/WifiSignalStrengthModel.cpp \
    ViewModels/NetworkDelegateViewModel.cpp \
    Models/NetworkInterfaceModel.cpp \
    Models/NetworkAddressModel.cpp \
    Models/RtcHardwareModel.cpp \
    ViewModels/XxcDelegateViewModel.cpp

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:!ios:QMAKE_LFLAGS += -Wl,--start-group

DEPENDPATH += $$OUT_PWD/../Common $$PWD/../Common
INCLUDEPATH += $$OUT_PWD/../Common $$PWD/../Common
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Common/release/ -lcommon
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Common/debug/ -lcommon
else:macx:LIBS += -L$$OUT_PWD/../Common/ -lcommon
else:unix:LIBS += -L$$OUT_PWD/../Common/ -lcommon
