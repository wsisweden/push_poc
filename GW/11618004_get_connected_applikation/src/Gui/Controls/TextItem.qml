import QtQuick 2.9
import QtQuick.Controls 2.2

Text {
    antialiasing: true

    property int size: 16
    property bool bold: false
    property string fontColor: Style.colorText
    property string fontName: Style.defaultFont

    color: fontColor

    font.bold: bold
    font.pointSize: size
    font.family: fontName
}
