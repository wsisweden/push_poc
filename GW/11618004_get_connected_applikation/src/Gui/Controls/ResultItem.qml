import QtQuick 2.9
import QtQuick.Controls 2.2

Image {
    fillMode: Image.PreserveAspectFit

    property string result

    source: {
        if (result === "true")
            return "qrc:/Images/Success.png"
        else if (result === "false")
            "qrc:/Images/Fail.png"
        else
            "qrc:/Images/Unknown.png"
    }
}
