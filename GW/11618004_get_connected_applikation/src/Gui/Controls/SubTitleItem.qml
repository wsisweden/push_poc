import QtQuick 2.9
import QtQuick.Controls 2.2

Text {
    antialiasing: true
    color: Style.colorTextDisabled

    property bool bold: false

    font.pointSize: 18
    font.bold: bold
    font.family: Style.defaultFontLight
}
