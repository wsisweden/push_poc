import QtQuick 2.9
import QtQuick.Controls 2.2

Text {
    antialiasing: true

    property int size: 10
    property bool bold: false
    property string fontName: Style.defaultFontLight
    property string fontColor: Style.colorTextDisabled

    color: fontColor

    font.bold: bold
    font.pointSize: size
    font.family: fontName
}
