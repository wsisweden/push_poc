QT += core network serialport sql webchannel websockets

CONFIG += c++11

TARGET = gateway
TEMPLATE = app

SOURCES += \
    Main.cpp \
    Unix/Backtrace.cpp \
    Unix/Signal.cpp \
    LedManager.cpp

HEADERS += \
    Unix/Backtrace.h \
    Unix/Signal.h \
    LedManager.h

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:QMAKE_LFLAGS += -Wl,--start-group

DEPENDPATH += $$OUT_PWD/../Common $$PWD/../Common
INCLUDEPATH += $$OUT_PWD/../Common $$PWD/../Common
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Common/release/ -lcommon
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Common/debug/ -lcommon
else:macx:LIBS += -L$$OUT_PWD/../Common/ -lcommon
else:unix:LIBS += -L$$OUT_PWD/../Common/ -lcommon

DEPENDPATH += $$OUT_PWD/../Storage $$PWD/../Storage
INCLUDEPATH += $$OUT_PWD/../Storage $$PWD/../Storage
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Storage/debug/ -lstorage
else:macx:LIBS += -L$$OUT_PWD/../Storage/ -lstorage
else:unix:LIBS += -L$$OUT_PWD/../Storage/ -lstorage

DEPENDPATH += $$OUT_PWD/../Cloud $$PWD/../Cloud
INCLUDEPATH += $$OUT_PWD/../Cloud $$PWD/../Cloud
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Cloud/release/ -lcloud
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Cloud/debug/ -lcloud
else:macx:LIBS += -L$$OUT_PWD/../Cloud/ -lcloud
else:unix:LIBS += -L$$OUT_PWD/../Cloud/ -lcloud

DEPENDPATH += $$OUT_PWD/../Config $$PWD/../Config
INCLUDEPATH += $$OUT_PWD/../Config $$PWD/../Config
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Config/release/ -lconfig
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Config/debug/ -lconfig
else:macx:LIBS += -L$$OUT_PWD/../Config/ -lconfig
else:unix:LIBS += -L$$OUT_PWD/../Config/ -lconfig

DEPENDPATH += $$OUT_PWD/../MpAccess $$PWD/../MpAccess
INCLUDEPATH += $$OUT_PWD/../MpAccess $$PWD/../MpAccess
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../MpAccess/release/ -lmpaccess
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../MpAccess/debug/ -lmpaccess
else:macx:LIBS += -L$$OUT_PWD/../MpAccess/ -lmpaccess
else:unix:LIBS += -L$$OUT_PWD/../MpAccess/ -lmpaccess

DEPENDPATH += $$OUT_PWD/../Gui $$PWD/../Gui
INCLUDEPATH += $$OUT_PWD/../Gui $$PWD/../Gui
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Gui/release/ -lgui
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Gui/debug/ -lgui
else:macx:LIBS += -L$$OUT_PWD/../Gui/ -lgui
else:unix:LIBS += -L$$OUT_PWD/../Gui/ -lgui

# Se for more info: http://stackoverflow.com/questions/37866187/libs-vs-pre-targetdeps-in-pro-files-of-qt
unix:PRE_TARGETDEPS += $$OUT_PWD/../Common/libcommon.a $$OUT_PWD/../Storage/libstorage.a $$OUT_PWD/../Cloud/libcloud.a $$OUT_PWD/../Config/libconfig.a $$OUT_PWD/../MpAccess/libmpaccess.a $$OUT_PWD/../Gui/libgui.a

unix {
    isEmpty(target.path) {
        target.path = /opt/gateway/bin
        QMAKE_RPATHDIR += /opt/gateway/bin

        export(target.path)
    }
    INSTALLS += target
}

export(INSTALLS)
