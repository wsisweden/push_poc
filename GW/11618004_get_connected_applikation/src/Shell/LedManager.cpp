#include "LedManager.h"

#include "../Storage/IStorage.h"
#include "../Storage/Model/ConfigurationModel.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QProcess>

/**
 * @brief LedManager::LedManager Construct a new led manager
 */
LedManager::LedManager(Context* context, QObject* parent)
    : QObject(parent)
{
    qInfo().noquote() << "Initialize led manager";

    this->context = context;

    this->heartbeatTimer = new QTimer(this);

    connect(this->heartbeatTimer, &QTimer::timeout, this, &LedManager::heartbeat);
    connect(this->context->getEventManager(), &EventManager::factoryReset, this, &LedManager::doFactoryReset, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::forceHeartbeat, this, &LedManager::forceHeartbeat, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::initializeComplete, this, &LedManager::initializeComplete, Qt::QueuedConnection);
    connect(this->context->getEventManager(), &EventManager::authorizationChanged, this, &LedManager::authorizationChanged, Qt::QueuedConnection);

    setCloudLed(false);
    setHeartbeatLed(false);

    loadConfiguration();
}

/**
 * @brief LedManager::LedManager Construct a new led manager
 */
LedManager::~LedManager()
{
    qInfo().noquote() << "Teardown led manager";

    this->heartbeatTimer->stop();

    setCloudLed(false);
    setHeartbeatLed(false);
}

/**
 * @brief LedManager::loadConfiguration Load configuration
 */
void LedManager::loadConfiguration()
{
    int heartbeatInterval = this->context->getStorage()->getConfigurationByName("HeartbeatInterval").getValue().toInt();
    this->heartbeatTimer->setInterval(heartbeatInterval);
}

/**
 * @brief LedManager::heartbeat Heartbeat handler
 */
void LedManager::heartbeat()
{
    if (this->heartbeatLed)
        setHeartbeatLed(false);
    else
        setHeartbeatLed(true);

    // Make sure the cloud led and the heartbeat led flash at same pace.
    if (this->cloudLed == this->heartbeatLed)
        this->cloudLed = !this->heartbeatLed;

    if (this->authorized)
        setCloudLed(true);
    else if (!this->authorized && this->serverResponse)
        setCloudLed(!this->cloudLed);
    else
        setCloudLed(false);

}

/**
 * @brief LedManager::forceHeartbeat Force heartbeat handler
 */
void LedManager::forceHeartbeat(const HeartbeatEvent& event)
{
    if (event.getHeartbeat())
    {
        this->heartbeatTimer->start();
        setHeartbeatLed(true);
    }
    else
    {
        this->heartbeatTimer->stop();
        setHeartbeatLed(false);
    }
}

/**
 * @brief LedManager::setHeartbeatLed Set heartbeat led status
 * @param status The status (on / off)
 */
void LedManager::setHeartbeatLed(bool status)
{
    QFile file("/sys/class/leds/led:usr0/brightness");
    if (file.open(QIODevice::WriteOnly))
    {
        file.write(QString((status == true) ? "1" : "0").toLocal8Bit());
        file.close();
    }

    this->heartbeatLed = status;
}

/**
 * @brief LedManager::setCloudLed Set cloud led status
 * @param status The status (on / off)
 */
void LedManager::setCloudLed(bool status)
{
    QFile file("/sys/class/leds/led:usr1/brightness");
    if (file.open(QIODevice::WriteOnly))
    {
        file.write(QString((status == true) ? "1" : "0").toLocal8Bit());
        file.close();
    }

    this->cloudLed = status;
}

/**
 * @brief LedManager::doFactoryReset Factory reset handler
 */
void LedManager::doFactoryReset()
{
    this->authorized = false;
    this->serverResponse = false;

    setCloudLed(false);
    setHeartbeatLed(false);
}

/**
 * @brief LedManager::authorizationChanged Authorization changed handler
 * @param event The event
 */
void LedManager::authorizationChanged(const AuthorizationEvent& event)
{
    this->authorized = event.getAuthorized();
    this->serverResponse = event.getServerResponse();
}

/**
 * @brief LedManager::initializeComplete Initialize complete handler
 */
void LedManager::initializeComplete()
{
    setHeartbeatLed(true);

    // Verify hardware time before we flash the heartbeat.
    if (!verifyHardwareTime())
        return;

    this->heartbeatTimer->start();
}

/**
 * @brief LedManager::verifyHardwareTime Verify hardware time.
 */
bool LedManager::verifyHardwareTime()
{
    QProcess process;
    process.start("sh", QStringList() << "-c" << "cat /var/log/hwclock.log | grep 'ok'");
    process.waitForFinished();
    bool result = process.readAllStandardOutput().trimmed().isEmpty() ? false : true;
    process.close();

    return result;
}
