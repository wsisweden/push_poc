#include "LedManager.h"
#include "../Storage/IStorage.h"
#include "../Storage/Database.h"
#include "../Config/ListenerWorker.h"
#include "../MpAccess/NodeWorker.h"
#include "../Cloud/CloudWorker.h"
#include "../Cloud/Request/RequestPackage.h"
#include "../Common/Context.h"
#include "../Common/Version.h"
#include "../Common/EventManager.h"
#include "../Common/Event/NodeAddedEvent.h"
#include "../Common/Event/NodeRemovedEvent.h"
#include "../Common/Event/NodeUpdatedEvent.h"
#include "../Common/Event/NodeTimeoutEvent.h"
#include "../Common/Event/FirmwareUpdatedEvent.h"
#include "../Common/Event/RequestEvent.h"
#include "../Common/Event/ActivityEvent.h"
#include "../Common/Event/ConfigurationEvent.h"
#include "../Common/Event/CollectDataIntervalEvent.h"
#include "../Common/Event/ScanNetworkIntervalEvent.h"
#include "../Common/Event/RemoveNodeIntervalEvent.h"
#include "../Common/Event/AuthorizationEvent.h"
#include "../Common/Event/UplinkEvent.h"
#include "../Common/Event/HeartbeatEvent.h"
#include "../Common/Event/FactoryResetEvent.h"
#include "../Common/Event/NetworkRestrictionEvent.h"
#include "../Common/Event/NodeRestrictionEvent.h"
#include "../Common/Event/InvalidIndexEvent.h"
#include "../Gui/Styles/Style.h"
#include "../Gui/ViewModels/ViewModelHandler.h"
#include "../Gui/ViewModels/RootViewModel.h"

#ifdef Q_OS_UNIX
    #include "Unix/Backtrace.h"
    #include "Unix/Signal.h"
#endif

#include <QtCore/QCommandLineParser>
#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QLocale>
#include <QtCore/QLoggingCategory>
#include <QtCore/QMessageLogContext>
#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtCore/QUrl>
#include <QtCore/QVariant>
#include <QtCore/QStringList>

#include <QtSql/QSqlDatabase>

#include <QtGui/QFont>
#include <QtGui/QFontDatabase>
#include <QtGui/QGuiApplication>

#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>

#include <string.h>

struct CommandLineArgs
{
    QString dbpath;
    QString logpath;
    QString devicetype;
    QString devicename;

    QString node;
    QString network;

    bool dbmemory = false;
    bool debuglog = false;
    bool debugmpalog = false;
};

enum CommandLineParseResult
{
    CommandLineOk,
    CommandLineError,
    CommandLineVersionRequested,
    CommandLineHelpRequested
};

static CommandLineArgs commandLineArguments;

/**
 * @brief messageHandler Handler for log messages
 * @param type Type of message (Info, Debug, Warning, Critical, Fatal)
 * @param context Log context
 * @param message The log message
 */
void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message)
{
    QString logMessage;
    QString threadId = QString::number((long long)QThread::currentThreadId(), 16);
    QString timestamp = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");

    switch (type)
    {
        case QtInfoMsg:
            logMessage = QString("[%1] [%2] [I] %3").arg(timestamp).arg(threadId).arg(message);
            break;
        case QtDebugMsg:
            logMessage = QString("[%1] [%2] [D] %3").arg(timestamp).arg(threadId).arg(message);
            break;
        case QtWarningMsg:
            logMessage = QString("[%1] [%2] [W] %3").arg(timestamp).arg(threadId).arg(message);
            break;
        case QtCriticalMsg:
            logMessage = QString("[%1] [%2] [C] %3").arg(timestamp).arg(threadId).arg(message);
            break;
        case QtFatalMsg:
            logMessage = QString("[%1] [%2] [F] %3").arg(timestamp).arg(threadId).arg(message);
        default:
            break;
    }

    // Supported outputs: mpa, sim, default -> log
    //
    // C++
    // qDebug("This message will end up in yyyy-MM-dd.log")
    // qCDebug(QLoggingCategory("default"), "This message will end up in yyyy-MM-dd.log")
    // qCDebug(QLoggingCategory("mpa"), "This message will end up in yyyy-MM-dd.mpa")

    logMessage = (logMessage.endsWith("\n") == true) ? logMessage : logMessage.append("\n");

    fprintf(stderr, "%s", qPrintable(logMessage));

    QString path = QString("%1/logs").arg(::commandLineArguments.logpath.isEmpty() ? qApp->applicationDirPath() : ::commandLineArguments.logpath);

    QDir directory(path);
    if (!directory.exists())
        directory.mkpath(".");

    QFile file(QString("%1/%2.%3").arg(path)
                                  .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd"))
                                  .arg((QString(context.category) == "default") ? "log" : context.category));
    file.open(QIODevice::WriteOnly | QIODevice::Append);
    file.write(logMessage.toUtf8());
    file.close();
}

/**
 * @brief parseCommandLine Parse the application command line
 * @param parser Instance to a command line parser
 * @param arguments The argument list
 * @return Parser result
 */
CommandLineParseResult parseCommandLine(QCommandLineParser* parser, const QStringList& arguments)
{
    parser->addHelpOption();
    parser->addVersionOption();

    parser->addOption({{"d", "dbpath"}, "Database path", "dbpath"});
    parser->addOption({{"l", "logpath"}, "Log path", "logpath"});
    parser->addOption({{"m", "dbmemory"}, "Use SQLite in memory database", "dbmemory"});
    parser->addOption({{"n", "debuglog"}, "Enable log", "debuglog"});
    parser->addOption({{"o", "debugmpalog"}, "Enable MP-ACCESS log", "debugmpalog"});
    parser->addOption({{"t", "devicetype"}, "Device type", "devicetype"});
    parser->addOption({{"r", "node"}, "Use only specified node <address>", "node"});
    parser->addOption({{"s", "network"}, "Use only specified network <channel:panId>", "network"});
    parser->addOption({{"u", "devicename"}, "Device name", "devicename"});
    parser->addOption({{"x", "test"}, "Test mode"});

    if (!parser->parse(arguments))
        return CommandLineError;

    if (parser->isSet("version"))
        return CommandLineVersionRequested;

    if (parser->isSet("help"))
        return CommandLineHelpRequested;

    if (parser->isSet("node"))
        ::commandLineArguments.node = parser->value("node");

    if (parser->isSet("network"))
        ::commandLineArguments.network = parser->value("network");

    if (parser->isSet("dbpath"))
        ::commandLineArguments.dbpath = parser->value("dbpath");

    if (parser->isSet("logpath"))
        ::commandLineArguments.logpath = parser->value("logpath");

    if (parser->isSet("devicetype"))
        ::commandLineArguments.devicetype = parser->value("devicetype");

    if (parser->isSet("devicename"))
        ::commandLineArguments.devicename = parser->value("devicename");

    if (parser->isSet("dbmemory"))
        ::commandLineArguments.dbmemory = QVariant(parser->value("dbmemory")).toBool();

    if (parser->isSet("debuglog"))
        ::commandLineArguments.debuglog = QVariant(parser->value("debuglog")).toBool();

    if (parser->isSet("debugmpalog"))
        ::commandLineArguments.debugmpalog = QVariant(parser->value("debugmpalog")).toBool();

    return CommandLineOk;
}

/**
 * @brief parseCommandLine Parse the application command line
 * @param parser Instance to a command line parser
 * @param arguments The argument list
 */
bool parseCommandLineArguments(QCommandLineParser* parser, const QStringList& arguments)
{
    switch (parseCommandLine(parser, arguments))
    {
        case CommandLineOk:
            break;
        case CommandLineError:
            qCritical().noquote() << "Unable to parse command line";
            parser->showHelp();
            return false;
        case CommandLineVersionRequested:
            parser->showVersion();
            return false;
        case CommandLineHelpRequested:
            parser->showHelp();
            return false;
        default:
            break;
    }

    return true;
}

/**
 * @brief registerMetaTypes Register meta types
 */
void registerMetaTypes()
{
    qRegisterMetaType<RequestEvent>();
    qRegisterMetaType<ActivityEvent>();
    qRegisterMetaType<NodeAddedEvent>();
    qRegisterMetaType<NodeRemovedEvent>();
    qRegisterMetaType<NodeUpdatedEvent>();
    qRegisterMetaType<NodeTimeoutEvent>();
    qRegisterMetaType<FirmwareUpdatedEvent>();
    qRegisterMetaType<ConfigurationEvent>();
    qRegisterMetaType<CollectDataIntervalEvent>();
    qRegisterMetaType<ScanNetworkIntervalEvent>();
    qRegisterMetaType<RemoveNodeIntervalEvent>();
    qRegisterMetaType<AuthorizationEvent>();
    qRegisterMetaType<UplinkEvent>();
    qRegisterMetaType<HeartbeatEvent>();
    qRegisterMetaType<FactoryResetEvent>();
    qRegisterMetaType<NetworkRestrictionEvent>();
    qRegisterMetaType<NodeRestrictionEvent>();
    qRegisterMetaType<InvalidIndexEvent>();
}

/**
 * @brief loadDatabase Load the database
 */
void loadDatabase()
{
    QString path = QString("%1").arg(qApp->applicationDirPath());

    QDir directory(path);
    if (!directory.exists())
        directory.mkpath(".");

    QSqlDatabase database;
    if (::commandLineArguments.dbmemory)
    {
        qInfo().noquote() << "Using SQLite in memory driver";

        database = QSqlDatabase::addDatabase("QSQLITE");
        database.setDatabaseName(":memory:");
    }
    else
    {
        qInfo().noquote() << "Using SQLite driver";

        database = QSqlDatabase::addDatabase("QSQLITE");
        QString databaseLocation = QString("%1/gateway.s3db").arg(path);
        if (!::commandLineArguments.dbpath.isEmpty())
            databaseLocation = ::commandLineArguments.dbpath;

        database.setDatabaseName(databaseLocation);
    }

    if (!database.open())
        qCritical().noquote() << "Unable to open database";
}

/**
 * @brief loadFonts Load application fonts
 */
void loadFonts()
{
    QFontDatabase::addApplicationFont(":/Fonts/Roboto-Light.ttf");
    QFontDatabase::addApplicationFont(":/Fonts/Roboto-Regular.ttf");
    QFontDatabase::addApplicationFont(":/Fonts/Roboto-Medium.ttf");
    QFontDatabase::addApplicationFont(":/Fonts/Roboto-Bold.ttf");

    QGuiApplication::setFont(QFont("Roboto"));
}

/**
 * @brief main Application entry point
 * @param argc Number of arguments
 * @param argv Command line arguments
 * @return Application exit code
 */
int main(int argc, char *argv[])
{
#ifdef Q_OS_UNIX
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGSEGV, backtraceHandler);
#endif

    qInstallMessageHandler(messageHandler);

    // Look for test mode flag before we instatiate the application class (QCoreApplication / QGuiApplication).
    bool testMode = false;
    for (int i = 0; i < argc; i++)
    {
        if (!strcmp(argv[i], "--test"))
        {
            testMode = true;
            break;
        }
    }

    Context context;
    int exitCode = 0; // Assume succsess.

    if (testMode)
    {
        QGuiApplication application(argc, argv);
        application.setApplicationName("Get Connected");

        QCommandLineParser commandLineParser;
        context.setCommandLineParser(&commandLineParser);
        if (!parseCommandLineArguments(&commandLineParser, application.arguments()))
            return -1;

#if QT_POINTER_SIZE == 4
        application.setApplicationVersion(QString("%1.%2.%3.%4, Qt %5 (32 bit)").arg(MAJOR_VERSION)
                                                                                .arg(MINOR_VERSION)
                                                                                .arg(REVISION_VERSION)
                                                                                .arg(BUILD_COMMIT)
                                                                                .arg(QT_VERSION_STR));
#else
        application.setApplicationVersion(QString("%1.%2.%3.%4, Qt %5 (64 bit)").arg(MAJOR_VERSION)
                                                                                .arg(MINOR_VERSION)
                                                                                .arg(REVISION_VERSION)
                                                                                .arg(BUILD_COMMIT)
                                                                                .arg(QT_VERSION_STR));
#endif

        qInfo().noquote() << "Test mode detected";
        qInfo().noquote() << "Starting" << application.applicationName() << application.applicationVersion();

        Q_INIT_RESOURCE(Gui);
        Q_INIT_RESOURCE(Storage);

        loadFonts();
        loadDatabase();
        registerMetaTypes();

        Database database;
        context.setStorage(&database);

        EventManager eventManager;
        context.setEventManager(&eventManager);

        ViewModelHandler viewModelHandler(&context);

        QQmlApplicationEngine engine;
        context.setQmlContext(engine.rootContext());
        engine.rootContext()->setContextProperty("viewModelHandler", &viewModelHandler);

        Style style;
        engine.rootContext()->setContextProperty("Style", &style);

        RootViewModel rootViewModel(&context);
        engine.rootContext()->setContextProperty("rootViewModel", &rootViewModel);

        engine.load(QUrl("qrc:/Views/RootView.qml"));

        exitCode = application.exec();
    }
    else
    {
        QCoreApplication application(argc, argv);
        application.setApplicationName("Get Connected");

        QCommandLineParser commandLineParser;
        context.setCommandLineParser(&commandLineParser);
        if (!parseCommandLineArguments(&commandLineParser, application.arguments()))
            return -1;

#if QT_POINTER_SIZE == 4
        application.setApplicationVersion(QString("%1.%2.%3.%4, Qt %5 (32 bit)").arg(MAJOR_VERSION)
                                                                                .arg(MINOR_VERSION)
                                                                                .arg(REVISION_VERSION)
                                                                                .arg(BUILD_COMMIT)
                                                                                .arg(QT_VERSION_STR));
#else
        application.setApplicationVersion(QString("%1.%2.%3.%4, Qt %5 (64 bit)").arg(MAJOR_VERSION)
                                                                                .arg(MINOR_VERSION)
                                                                                .arg(REVISION_VERSION)
                                                                                .arg(BUILD_COMMIT)
                                                                                .arg(QT_VERSION_STR));
#endif

        qInfo().noquote() << "Starting" << application.applicationName() << application.applicationVersion();

        Q_INIT_RESOURCE(Storage);

        loadDatabase();
        registerMetaTypes();

        Database database;
        context.setStorage(&database);

        EventManager eventManager;
        context.setEventManager(&eventManager);

        LedManager ledManager(&context);
        CloudWorker cloudWorker(&context);
        ListenerWorker listenerWorker(&context);
        NodeWorker nodeWorker(&context);

        exitCode = application.exec();
    }

    return exitCode;
}
