#pragma once

#include "../Common/Context.h"
#include "../Common/Event/HeartbeatEvent.h"
#include "../Common/Event/AuthorizationEvent.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>

class LedManager : public QObject
{
    Q_OBJECT

    public:
        explicit LedManager(Context* context, QObject* parent = nullptr);
        ~LedManager();

    private:
        bool cloudLed = false;
        bool heartbeatLed = false;

        bool authorized = false;
        bool serverResponse = false;

        Context* context = nullptr;
        QTimer* heartbeatTimer = nullptr;

        bool verifyHardwareTime();

        void loadConfiguration();
        void setCloudLed(bool status);
        void setHeartbeatLed(bool status);


        Q_SLOT void heartbeat();
        Q_SLOT void doFactoryReset();
        Q_SLOT void initializeComplete();
        Q_SLOT void forceHeartbeat(const HeartbeatEvent& event);
        Q_SLOT void authorizationChanged(const AuthorizationEvent& event);
};
