#include "Signal.h"

#include <QtCore/QDebug>

/**
 * @brief signalHandler Signal handler
 * @param sig The signal
 */
void signalHandler(int signal)
{
    Q_UNUSED(signal);

    exit(0);
}
