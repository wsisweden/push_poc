#include "Backtrace.h"

#include <QtCore/QDebug>

#include <execinfo.h>
#include <signal.h>

/**
 * @brief backtraceHandler Backtrace error signals to the log
 * @param sig The signal
 */
void backtraceHandler(int signal)
{
    size_t size;
    char** strings;
    void* array[10];

    // Get void*'s for all entries on the stack.
    size = backtrace(array, 10);
    strings = backtrace_symbols(array, size);

    qCritical().noquote() << "Error signal: " << signal;
    for (size_t i = 0; i < size; i++)
      qCritical().noquote() << strings[i];

    free(strings);

    exit(1);
}
