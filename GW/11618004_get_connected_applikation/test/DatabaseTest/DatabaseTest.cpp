#include "Version.h"
#include "Database.h"

#include <QtCore/QObject>

#include <QtSql/QSqlDatabase>

#include <QtTest/QTest>

class DatabaseTest : public QObject
{
    Q_OBJECT

    public:
        DatabaseTest()
        {
            Q_INIT_RESOURCE(Storage);
        }

        ~DatabaseTest()
        {

        }

    private:
        void initTestCase()
        {
            QString path = QString("%1").arg(QT_TESTCASE_BUILDDIR);
            QString databaseLocation = QString("%1/test.s3db").arg(path);

            QDir directory(path);
            if (!directory.exists())
                directory.mkpath(".");

            QFile file(databaseLocation);
            if (file.exists())
                file.remove();

            QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
            database.setDatabaseName(databaseLocation);

            if (!database.open())
                qCritical("Unable to open database");
        }

        void cleanupTestCase()
        {
            QString path = QString("%1").arg(QT_TESTCASE_BUILDDIR);
            QString databaseLocation = QString("%1/test.s3db").arg(path);

            QSqlDatabase::removeDatabase("qt_sql_default_connection");

            QDir directory(path);
            if (!directory.exists())
                directory.mkpath(".");

            QFile file(databaseLocation);
            if (file.exists())
                file.remove();
        }

    private slots:
        void createDatabaseTestCase()
        {
            initTestCase();

            Database* database = new Database();
            const ConfigurationModel model = database->getConfigurationByName("");
            QCOMPARE(model.getValue(), QString(""));

            delete database;

            cleanupTestCase();
        }

        void checkDatabaseVersionTestCase()
        {
            initTestCase();

            Database* database = new Database();
            const ConfigurationModel model = database->getConfigurationByName("DatabaseVersion");
            QCOMPARE(model.getValue(), QString("%1").arg(DATABASE_VERSION));

            delete database;

            cleanupTestCase();
        }
};

QTEST_APPLESS_MAIN(DatabaseTest)

#include "DatabaseTest.moc"
