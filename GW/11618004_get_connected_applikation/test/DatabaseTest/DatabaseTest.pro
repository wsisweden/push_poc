QT += core sql testlib

CONFIG += c++11 testcase

TARGET = databasetest
TEMPLATE = app

HEADERS += \

SOURCES += \
    DatabaseTest.cpp

# See for more info: http://stackoverflow.com/questions/45135/why-does-the-order-in-which-libraries-are-linked-sometimes-cause-errors-in-gcc
unix:!macx:QMAKE_LFLAGS += -Wl,--start-group

DEPENDPATH += $$OUT_PWD/../Common $$PWD/../Common $$OUT_PWD/../../../src/Common
INCLUDEPATH += $$OUT_PWD/../Common $$PWD/../Common $$OUT_PWD/../../../src/Common
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Common/release/ -lcommon
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Common/debug/ -lcommon
else:macx:LIBS += -L$$OUT_PWD/../Common/ -lcommon
else:unix:LIBS += -L$$OUT_PWD/../Common/ -lcommon

DEPENDPATH += $$OUT_PWD/../Storage $$PWD/../Storage $$OUT_PWD/../../../src/Storage
INCLUDEPATH += $$OUT_PWD/../Storage $$PWD/../Storage $$OUT_PWD/../../../src/Storage
win32:CONFIG(release, debug|release):LIBS += -L$$OUT_PWD/../Storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release):LIBS += -L$$OUT_PWD/../Storage/debug/ -lstorage
else:macx:LIBS += -L$$OUT_PWD/../Storage/ -lstorage
else:unix:LIBS += -L$$OUT_PWD/../Storage/ -lstorage

# See for more info: http://stackoverflow.com/questions/37866187/libs-vs-pre-targetdeps-in-pro-files-of-qt
unix:PRE_TARGETDEPS += $$OUT_PWD/../Common/libcommon.a $$OUT_PWD/../Storage/libstorage.a
