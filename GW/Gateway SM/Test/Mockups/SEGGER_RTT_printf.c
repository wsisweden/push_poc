/*
 * SEGGER_RTT_printf.c
 *
 *  Created on: 18 jan. 2018
 *      Author: AndCar_MEM
 */
#include <stdlib.h>
#include <stdarg.h>

void SEGGER_RTT_Init(void) { }

int SEGGER_RTT_vprintf(unsigned BufferIndex, const char * sFormat, va_list * pParamList) {
  (void)BufferIndex;
  (void)sFormat;
  (void)pParamList;
  return 0;
}

int SEGGER_RTT_printf(unsigned BufferIndex, const char * sFormat, ...) {
  int r;
  va_list ParamList;

  va_start(ParamList, sFormat);
  r = SEGGER_RTT_vprintf(BufferIndex, sFormat, &ParamList);
  va_end(ParamList);
  return r;
}
