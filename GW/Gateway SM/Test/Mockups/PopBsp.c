#include "../../Src/MP/MpGw.h"
#include "../../Lib/PopNet/PopPhy.h"
#include "../../Lib/PopNet/PopBsp.h"
#include "../../Lib/PopNet/PopNet.h"
#include <stdlib.h>
#include <time.h>

uint32_t * __SSTACK;
uint8_t giPopForever = 1;
popChannel_t giPhyChannel;
sMpJoinNetworkReq_t gsPopAppJoinReqNwkInfo;

popErr_t MPFlashEraseBlockBlocking(uint32_t address) {
  (void)address;
  return gPopErrNone_c;
}

void PopExternalStorageEraseAll(void) { }

void SwapBytes16(uint16_t *pWord)
{
  uint8_t *pBytes = (void*)pWord;
  uint8_t tmp;

  // This could use the REV instruction on Cortex-M

  // reverse the bytes
  tmp = pBytes[0];
  pBytes[0] = pBytes[1];
  pBytes[1] = tmp;
}

/*
  SwapBytes32
*/
void SwapBytes32(uint32_t *pWord32)
{
  uint8_t *pBytes = (void *)pWord32;
  uint8_t tmp;

  // This could use the REV instruction on Cortex-M

  // reverse the bytes
  tmp = pBytes[0];
  pBytes[0] = pBytes[3];
  pBytes[3] = tmp;
  tmp = pBytes[1];
  pBytes[1] = pBytes[2];
  pBytes[2] = tmp;
}

/*
  SwapBytes64
*/
void SwapBytes64(uint64_t *pWord64)
{
  uint8_t *pBytes = (void *)pWord64;
  uint8_t tmp;

  // This could use the REV instruction on Cortex-M

  // reverse the bytes
  tmp = pBytes[0];
  pBytes[0] = pBytes[7];
  pBytes[7] = tmp;
  tmp = pBytes[1];
  pBytes[1] = pBytes[6];
  pBytes[6] = tmp;
  tmp = pBytes[2];
  pBytes[2] = pBytes[5];
  pBytes[5] = tmp;
  tmp = pBytes[3];
  pBytes[3] = pBytes[4];
  pBytes[4] = tmp;
}

popErr_t PopSerialSend( uint16_t iLen, void *pBuffer, popSerialSendFlags_t iFlags) {
  (void)iLen;
  (void)pBuffer;
  (void)iFlags;
  return gPopErrNone_c;
}

popErr_t PopSerialSettings( gPopBaudRate_t iBaudRate, uint16_t iMinGapMs ) {
  (void)iBaudRate;
  (void)iMinGapMs;
  return gPopErrNone_c;
}

void PopHex2Ascii(char *psz, uint16_t iHex) {
  (void)psz;
  (void)iHex;
}

void PopNwkSetTransmitPower(popNwkTxPower_t iTxPower) {
  (void)iTxPower;
}

popErr_t PopStartTimer(popTimerId_t iTimerId, popTimeOut_t iMilliseconds) {
	(void)iTimerId;
  (void)iMilliseconds;
  return gPopErrNone_c;
}

bool IsThereAFreeTimer( void ) {return true;}
popErr_t PopStopTimer(popTimerId_t iTimerId) {
  (void)iTimerId;
  return gPopErrNone_c;
}
popErr_t PopStartTimerEx(popTaskId_t iTaskId, popTimerId_t iTimerId, popTimeOut_t iMilliseconds) {
  (void)iTaskId;
  (void)iTimerId;
  (void)iMilliseconds;
  return gPopErrNone_c;
}

popErr_t PopStopTimerEx(popTaskId_t iTaskId, popTimerId_t iTimerId) {
  (void)iTaskId;
  (void)iTimerId;
  return gPopErrNone_c;
}

uint8_t PopFindTimer(popTaskId_t iTaskId, popTimerId_t iTimerId, uint8_t *piFreeTimer) {
  (void)iTaskId;
  (void)iTimerId;
  (void)piFreeTimer;
  return 0;
}

void PopRandomInit(void) {
  srand (time(NULL));
}

uint8_t PopRandom08(void) {
  return (uint8_t)rand();
}

uint16_t PopRandom16(void) {
  return (uint16_t)rand();
}

uint8_t PopEraseBlock(popStorageSize_t iBlockOffset) {
  (void)iBlockOffset;
  return 0;
}

uint8_t PopStorageWriteBytes(popStorageSize_t iDstOffset, void *pSrcAddr, popStorageSize_t iLen) {
  (void)iDstOffset;
  (void)pSrcAddr;
  (void)iLen;
  return 0;
}

uint8_t PopSwitchToNewImage(bool fPreserveNVM) {
  (void)fPreserveNVM;
  return 0;
}

uint8_t PopStorageReadBytes(void *pDstAddr, popStorageSize_t iSrcOffset, popStorageSize_t  iLen) {
  (void)pDstAddr;
  (void)iSrcOffset;
  (void)iLen;
  return 0;
}

void PopGwSwapICanHearYouToLittle(uint8_t iNum, uint16_t *pArray) {
  (void)iNum;
  (void)pArray;
}
/*
  SwapBytes16 with return value needed only in gateway
*/
uint16_t SwapBytes16WithReturn(uint8_t *pWord)
{
  uint16_t newVal;

  /* First, get the correct value */
  newVal = *(uint8_t*)pWord  << 8;
  newVal += *((uint8_t*)pWord + 1);

  return newVal;
}

void PopReset(void) {}

uint8_t PopPhyLqi(void) {return 0;}

void PopPhyDataRequest(void *pData, uint8_t iLen) {
  (void)pData;
  (void)iLen;
}

void PopPhyReceive(sPopNwkDataIndication_t *pFrame) {
  (void)pFrame;
}

void PopPhySetChannel(uint8_t iChannel) {
  (void)iChannel;
}

void PopDisableInts() {}
void PopEnableInts() {}
void PopFeedTheDog(void) {}
void PopWatchDogInit(void) {}
void PopBspStartUpTimer(void) {}
void PopPhyProcessRadioMsg(void) {}

void PopFlashInit(void) {}
bool PopFlashWriteBytes(void *pDstAddr, void *pSrcAddr, uint32_t iLen) {
  (void)pSrcAddr;
  (void)iLen;
  return true;
}

void * PopNvGetPageHeader(void *pPageSrcAddress) {
  (void)pPageSrcAddress;
  return NULL;
}

void * PopNvGetEntryData(void *pEntrySrcAddress, uint8_t iNvEntrySise) {
  (void)pEntrySrcAddress;
  (void)iNvEntrySise;
  return NULL;
}

void PopNvmEraseAll(void) {}
void PopNvmErasePage(popNvmPage_t iPage) {(void)iPage;}
bool PopFlashEraseNvmFlash( void *pAddress ) {
  (void)pAddress;
  return true;
}

popNvPageSize_t PopNvCountFFs(uint8_t * pBuffer, popNvPageSize_t iMax) {
  (void)pBuffer;
  (void)iMax;
  return 0;
}

bool PopFlashCopyNvPage(void * pSrcAddress, void * pDstAddress) {
  (void)pSrcAddress;
  (void)pDstAddress;
  return true;
}

bool PopFlashBackUpPagesInSpareSector(void) {
  return true;
}

uint8_t PopNvmReadBytes(void *pToAddr, popNvmOffset_t iFromOffset, popNvmSize_t iLen) {
  (void)pToAddr;
  (void)iFromOffset;
  (void)iLen;
  return 0;
}

uint8_t PopNvmWriteBytes(popNvmOffset_t iToOffset, void *pFromAddr, popNvmSize_t iLen) {
  (void)iToOffset;
  (void)pFromAddr;
  (void)iLen;
  return 0;
}

void PopStorageEraseAll(void) {}

popEnergyLevel_t PopPhyScanForNoise(uint8_t iChannel) {
  (void)iChannel;
  return 0;
}

void PopStopAllTimers(popTaskId_t iTaskId) {(void)iTaskId;}

uint32_t PopGetSystick(void)
{
  return 0;
}

void PopPhyTaskInit(void) {}

void PopPhyTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  (void)iEvtId;
  (void)sEvtData;
}

void PopBspTaskInit(void) {}

void PopBspTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  (void)iEvtId;
  (void)sEvtData;
}

void DelayMs(uint32_t ms) {
  (void)ms;
}
