#include <Hw/TIDriverLib/source/sys_ctrl.h>
#include <Hw/TIDriverLib/source/uart.h>
#include <Hw/TIDriverLib/source/interrupt.h>
#include <Hw/TIDriverLib/source/gpio.h>
#include <Hw/TiPhy/hal_int.h>
#include <Hw/TIDriverLib/source/spi.h>
#include <Hw/TIDriverLib/source/gptimer.h>
#include <Hw/TIDriverLib/source/ioc.h>
#include <Hw/TIDriverLib/source/i2c.h>
#include <Hw/TIDriverLib/source/tiadc.h>
#include <Hw/Segger/RTT/SEGGER_RTT.h>

void SysCtrlClockSet(bool bOsc32k, bool bInternalOsc,
                            uint32_t ui32SysDiv) {
	  (void)bOsc32k;
	  (void)bInternalOsc;
	  (void)ui32SysDiv;
}

void phyPLMEXtalAdjust(uint8_t coarseTrim, uint8_t fineTrim) {
  (void)coarseTrim;
  (void)fineTrim;
}

uint32_t SysCtrlClockGet(void) {
	return 0;
}

void SysCtrlIOClockSet(uint32_t ui32IODiv) {
	  (void)ui32IODiv;
}

uint32_t SysCtrlIOClockGet(void) {return 0;}
void SysCtrlDelay(uint32_t ui32Count) {
	  (void)ui32Count;
}

void SysCtrlReset(void) {}
void SysCtrlSleep(void) {}
void SysCtrlDeepSleep(void) {}

bool SysCtrlPeripheralPresent(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
	  return false;
}

void SysCtrlPeripheralReset(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
}

void SysCtrlPeripheralEnable(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
}

void SysCtrlPeripheralDisable(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
}

void SysCtrlPeripheralSleepEnable(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
}

void SysCtrlPeripheralSleepDisable(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
}

void SysCtrlPeripheralDeepSleepEnable(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
}

void SysCtrlPeripheralDeepSleepDisable(uint32_t ui32Peripheral) {
	  (void)ui32Peripheral;
}

void SysCtrlPowerModeSet(uint32_t ui32PowerMode) {
	  (void)ui32PowerMode;
}

uint32_t SysCtrlPowerModeGet(void) {
	return 0;
}

void SysCtrlClockLossDetectEnable(void) {}

void UARTParityModeSet(uint32_t ui32Base, uint32_t ui32Parity) {
	  (void)ui32Base;
	  (void)ui32Parity;
}

uint32_t UARTParityModeGet(uint32_t ui32Base) {
	  (void)ui32Base;
	  return 0;
}

void UARTFIFOLevelSet(uint32_t ui32Base, uint32_t ui32TxLevel,
                             uint32_t ui32RxLevel) {
	  (void)ui32Base;
	  (void)ui32TxLevel;
	  (void)ui32RxLevel;
}

void UARTFIFOLevelGet(uint32_t ui32Base, uint32_t *pui32TxLevel,
                             uint32_t *pui32RxLevel) {
	  (void)ui32Base;
	  (void)pui32TxLevel;
	  (void)pui32RxLevel;
}

void UARTConfigSetExpClk(uint32_t ui32Base, uint32_t ui32UARTClk,
                                uint32_t ui32Baud, uint32_t ui32Config) {
	  (void)ui32Base;
	  (void)ui32UARTClk;
	  (void)ui32Baud;
	  (void)ui32Config;
}

void UARTConfigGetExpClk(uint32_t ui32Base, uint32_t ui32UARTClk,
                                uint32_t *pui32Baud,
                                uint32_t *pui32Config) {
	  (void)ui32Base;
	  (void)ui32UARTClk;
	  (void)pui32Baud;
	  (void)pui32Config;
}

void UARTEnable(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UARTDisable(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UARTFIFOEnable(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UARTFIFODisable(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UARTEnableSIR(uint32_t ui32Base, bool bLowPower) {
	  (void)ui32Base;
	  (void)bLowPower;
}

void UARTDisableSIR(uint32_t ui32Base) {
	  (void)ui32Base;
}

bool UARTCharsAvail(uint32_t ui32Base) {
	  (void)ui32Base;
	  return false;
}

bool UARTSpaceAvail(uint32_t ui32Base) {
	  (void)ui32Base;
	  return false;
}

int32_t UARTCharGetNonBlocking(uint32_t ui32Base) {
	  (void)ui32Base;
	  return false;
}

int32_t UARTCharGet(uint32_t ui32Base) {
	  (void)ui32Base;
	  return false;
}

bool UARTCharPutNonBlocking(uint32_t ui32Base,
                                       uint8_t ui8Data) {
	  (void)ui32Base;
	  (void)ui8Data;
	  return false;
}

void UARTCharPut(uint32_t ui32Base, uint8_t ui8Data) {
	  (void)ui32Base;
	  (void)ui8Data;
}

void UARTBreakCtl(uint32_t ui32Base, bool bBreakState) {
	  (void)ui32Base;
	  (void)bBreakState;
}

bool UARTBusy(uint32_t ui32Base) {
	  (void)ui32Base;
	  return false;
}

void UARTIntRegister(uint32_t ui32Base, void (*pfnHandler)(void)) {
	  (void)ui32Base;
	  (void)pfnHandler;
}

void UARTIntUnregister(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UARTIntEnable(uint32_t ui32Base, uint32_t ui32IntFlags) {
	  (void)ui32Base;
	  (void)ui32IntFlags;
}

void UARTIntDisable(uint32_t ui32Base, uint32_t ui32IntFlags) {
	  (void)ui32Base;
	  (void)ui32IntFlags;
}

uint32_t UARTIntStatus(uint32_t ui32Base, bool bMasked) {
	  (void)ui32Base;
	  (void)bMasked;
	  return 0;
}

void UARTIntClear(uint32_t ui32Base, uint32_t ui32IntFlags) {
	  (void)ui32Base;
	  (void)ui32IntFlags;
}

void UARTDMAEnable(uint32_t ui32Base, uint32_t ui32DMAFlags) {
	  (void)ui32Base;
	  (void)ui32DMAFlags;
}

void UARTDMADisable(uint32_t ui32Base, uint32_t ui32DMAFlags) {
	  (void)ui32Base;
	  (void)ui32DMAFlags;
}

uint32_t UARTRxErrorGet(uint32_t ui32Base) {
	  (void)ui32Base;
	  return 0;
}

void UARTRxErrorClear(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UARTTxIntModeSet(uint32_t ui32Base, uint32_t ui32Mode) {
	  (void)ui32Base;
	  (void)ui32Mode;
}

uint32_t UARTTxIntModeGet(uint32_t ui32Base) {
	  (void)ui32Base;
	  return 0;
}

void UARTClockSourceSet(uint32_t ui32Base, uint32_t ui32Source) {
	  (void)ui32Base;
	  (void)ui32Source;
}

uint32_t UARTClockSourceGet(uint32_t ui32Base) {
	  (void)ui32Base;
	  return 0;
}

void UART9BitEnable(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UART9BitDisable(uint32_t ui32Base) {
	  (void)ui32Base;
}

void UART9BitAddrSet(uint32_t ui32Base, uint8_t ui8Addr,
                            uint8_t ui8Mask) {
	  (void)ui32Base;
	  (void)ui8Addr;
	  (void)ui8Mask;
}

void UART9BitAddrSend(uint32_t ui32Base, uint8_t ui8Addr) {
	  (void)ui32Base;
	  (void)ui8Addr;
}

bool IntMasterEnable(void) {
	return true;
}

bool
IntMasterDisable(void) {
	return true;
}

void IntRegister(uint32_t ui32Interrupt, void (*pfnHandler)(void)) {
	  (void)ui32Interrupt;
	  (void)pfnHandler;
}

void IntUnregister(uint32_t ui32Interrupt) {
	  (void)ui32Interrupt;
}

void IntPriorityGroupingSet(uint32_t ui32Bits) {
	  (void)ui32Bits;
}

uint32_t IntPriorityGroupingGet(void) {
	  return 0;
}

void IntPrioritySet(uint32_t ui32Interrupt,
                           uint8_t ui8Priority) {
	  (void)ui32Interrupt;
	  (void)ui8Priority;
}

int32_t IntPriorityGet(uint32_t ui32Interrupt) {
	  (void)ui32Interrupt;
	  return 0;
}

void IntEnable(uint32_t ui32Interrupt) {
	  (void)ui32Interrupt;
}

void IntDisable(uint32_t ui32Interrupt) {
	  (void)ui32Interrupt;
}

void IntPendSet(uint32_t ui32Interrupt) {
	  (void)ui32Interrupt;
}

void IntPendClear(uint32_t ui32Interrupt) {
	  (void)ui32Interrupt;
}

void IntPriorityMaskSet(uint32_t ui32PriorityMask) {
	  (void)ui32PriorityMask;
}

uint32_t IntPriorityMaskGet(void) {
	return 0;
}

void IntAltMapEnable(void) {}
void IntAltMapDisable(void) {}
bool IntAltMapIsEnabled(void) {
	return true;
}

void GPIODirModeSet(uint32_t ui32Port, uint8_t ui8Pins,
                           uint32_t ui32PinIO) {
	  (void)ui32Port;
	  (void)ui8Pins;
	  (void)ui32PinIO;
}

uint32_t GPIODirModeGet(uint32_t ui32Port, uint8_t ui8Pin) {
	  (void)ui32Port;
	  (void)ui8Pin;
	  return 0;
}

uint32_t GPIOGetIntNumber(uint32_t ui32Port) {
	  (void)ui32Port;
	  return 0;
}

void GPIOIntTypeSet(uint32_t ui32Port, uint8_t ui8Pins,
                           uint32_t ui32IntType) {
	  (void)ui32Port;
	  (void)ui8Pins;
	  (void)ui32IntType;
}

uint32_t GPIOIntTypeGet(uint32_t ui32Port, uint8_t ui8Pin) {
	  (void)ui32Port;
	  (void)ui8Pin;
	  return 0;
}

void GPIOPinIntEnable(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPinIntDisable(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

uint32_t GPIOPinIntStatus(uint32_t ui32Port, bool bMasked) {
	  (void)ui32Port;
	  (void)bMasked;
	  return 0;
}

void GPIOPinIntClear(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPortIntRegister(uint32_t ui32Port,
                                void (*pfnHandler)(void)) {
	  (void)ui32Port;
	  (void)pfnHandler;
}

void GPIOPortIntUnregister(uint32_t ui32Port) {
	  (void)ui32Port;
}

uint32_t GPIOPinRead(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
	  return 0;
}

void GPIOPinWrite(uint32_t ui32Port, uint8_t ui8Pins,
                         uint8_t ui8Val) {
	  (void)ui32Port;
	  (void)ui8Pins;
	  (void)ui8Val;
}

void GPIOPinTypeGPIOInput(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPinTypeGPIOOutput(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPinTypeI2C(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPinTypeSSI(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPinTypeTimer(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPinTypeUARTInput(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPinTypeUARTOutput(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPowIntEnable(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPowIntDisable(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOPowIntTypeSet(uint32_t ui32Port, uint8_t ui8Pins,
                              uint32_t ui32IntType) {
	  (void)ui32Port;
	  (void)ui8Pins;
	  (void)ui32IntType;
}

uint32_t GPIOPowIntTypeGet(uint32_t ui32Port,
                                       uint8_t ui8Pin) {
	  (void)ui32Port;
	  (void)ui8Pin;
	  return 0;
}

uint32_t GPIOPowIntStatus(uint32_t ui32Port, bool bMasked) {
	  (void)ui32Port;
	  (void)bMasked;
	  return 0;
}

void GPIOPowIntClear(uint32_t ui32Port, uint8_t ui8Pins) {
	  (void)ui32Port;
	  (void)ui8Pins;
}

void GPIOIntWakeupEnable(uint32_t ui32Config) {
	  (void)ui32Config;
}

void GPIOIntWakeupDisable(uint32_t ui32Config) {
	  (void)ui32Config;
}

void halIntOn(void) {}
void halIntOff(void) {}

void (* volatile spi_Callback)(uint32_t);
void (* volatile spi_Callback1)(uint32_t);

void spi_init(void) {}

void spi_setup_SS_SETUP(uint16_t out) {
  (void)out;
}

void spi_reserve(spi_ReserveReq * pReserveReq) {
  (void)pReserveReq;
}

void spi_free(void) {}

void spi_command(uint32_t data, int dataLen, int Clocks) {
  (void)data;
  (void)dataLen;
  (void)Clocks;
}

void spi_init1(void) {}

void spi_reserve1(spi_ReserveReq * pReserveReq) {
  (void)pReserveReq;
}

void spi_free1(void) {}

void spi_command1(uint32_t data, int dataLen, int Clocks) {
  (void)data;
  (void)dataLen;
  (void)Clocks;
}

void TimerEnable(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
}

void TimerDisable(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
}

void TimerConfigure(uint32_t ui32Base, uint32_t ui32Config) {
  (void)ui32Base;
  (void)ui32Config;
}

void TimerControlLevel(uint32_t ui32Base, uint32_t ui32Timer, bool bInvert) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)bInvert;
}

void TimerControlTrigger(uint32_t ui32Base, uint32_t ui32Timer, bool bEnable) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)bEnable;
}

void TimerControlEvent(uint32_t ui32Base, uint32_t ui32Timer, uint32_t ui32Event) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)ui32Event;
}

void TimerControlStall(uint32_t ui32Base, uint32_t ui32Timer, bool bStall) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)bStall;
}

void TimerControlWaitOnTrigger(uint32_t ui32Base, uint32_t ui32Timer, bool bWait) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)bWait;
}

void TimerPrescaleSet(uint32_t ui32Base, uint32_t ui32Timer, uint32_t ui32Value) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)ui32Value;
}

uint32_t TimerPrescaleGet(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
  return 0;
}

void TimerPrescaleMatchSet(uint32_t ui32Base, uint32_t ui32Timer, uint32_t ui32Value) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)ui32Value;
}

uint32_t TimerPrescaleMatchGet(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
  return 0;
}

void TimerLoadSet(uint32_t ui32Base, uint32_t ui32Timer, uint32_t ui32Value) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)ui32Value;
}

uint32_t TimerLoadGet(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
  return 0;
}

uint32_t TimerValueGet(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
  return 0;
}

void TimerMatchSet(uint32_t ui32Base, uint32_t ui32Timer, uint32_t ui32Value) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)ui32Value;
}

uint32_t TimerMatchGet(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
  return 0;
}

void TimerIntRegister(uint32_t ui32Base, uint32_t ui32Timer, void (*pfnHandler)(void)) {
  (void)ui32Base;
  (void)ui32Timer;
  (void)pfnHandler;
}

void TimerIntUnregister(uint32_t ui32Base, uint32_t ui32Timer) {
  (void)ui32Base;
  (void)ui32Timer;
}

void TimerIntEnable(uint32_t ui32Base, uint32_t ui32IntFlags) {
  (void)ui32Base;
  (void)ui32IntFlags;
}

void TimerIntDisable(uint32_t ui32Base, uint32_t ui32IntFlags) {
  (void)ui32Base;
  (void)ui32IntFlags;
}

uint32_t TimerIntStatus(uint32_t ui32Base, bool bMasked) {
  (void)ui32Base;
  (void)bMasked;
  return 0;
}

void TimerIntClear(uint32_t ui32Base, uint32_t ui32IntFlags) {
  (void)ui32Base;
  (void)ui32IntFlags;
}

void TimerSynchronize(uint32_t ui32Base, uint32_t ui32Timers) {
  (void)ui32Base;
  (void)ui32Timers;
}

int SEGGER_RTT_ConfigUpBuffer(unsigned BufferIndex, const char* sName, void* pBuffer, unsigned BufferSize, unsigned Flags) {
  (void)BufferIndex;
  (void)sName;
  (void)pBuffer;
  (void)BufferSize;
  (void)Flags;
  return 0;
}

void IOCPinConfigPeriphOutput(uint32_t ui32Port, uint8_t ui8Pins, uint32_t ui32OutputSignal) {
  (void)ui32Port;
  (void)ui8Pins;
  (void)ui32OutputSignal;
}

void IOCPinConfigPeriphInput(uint32_t ui32Port, uint8_t ui8Pin, uint32_t ui32PinSelectReg) {
  (void)ui32Port;
  (void)ui8Pin;
  (void)ui32PinSelectReg;
}

void IOCPadConfigSet(uint32_t ui32Port, uint8_t ui8Pins, uint32_t ui32PinDrive) {
  (void)ui32Port;
  (void)ui8Pins;
  (void)ui32PinDrive;
}

uint32_t IOCPadConfigGet(uint32_t ui32Port, uint8_t ui8Pin) {
  (void)ui32Port;
  (void)ui8Pin;
  return 0;
}

void I2CIntRegister(void (*pfnHandler)(void)) {
  (void)pfnHandler;
}

void I2CIntUnregister(void) {}

bool I2CMasterBusBusy(void) {
  return false;
}

bool I2CMasterBusy(void) {
  return false;
}

void I2CMasterControl(uint32_t ui32Cmd) {
  (void)ui32Cmd;
}

uint32_t I2CMasterDataGet(void) {
  return 0;
}

void I2CMasterDataPut(uint8_t ui8Data) {
  (void)ui8Data;
}

void I2CMasterDisable(void) {}
void I2CMasterEnable(void) {}

uint32_t I2CMasterErr(void) {
  return 0;
}

void I2CMasterInitExpClk(uint32_t ui32I2CClk, bool bFast) {
  (void)ui32I2CClk;
  (void)bFast;
}

void I2CMasterIntClear(void) {}
void I2CMasterIntDisable(void) {}
void I2CMasterIntEnable(void) {}

bool I2CMasterIntStatus(bool bMasked) {
  return false;
}

void I2CMasterSlaveAddrSet(uint8_t ui8SlaveAddr, bool bReceive) {
  (void)ui8SlaveAddr;
  (void)bReceive;
}

uint32_t I2CSlaveDataGet(void) {
  return 0;
}

void I2CSlaveDataPut(uint8_t ui8Data) {
  (void)ui8Data;
}

void I2CSlaveDisable(void) {}
void I2CSlaveEnable(void) {}

void I2CSlaveInit(uint8_t ui8SlaveAddr) {
  (void)ui8SlaveAddr;
}

void I2CSlaveIntClear(void) {}
void I2CSlaveIntDisable(void) {}
void I2CSlaveIntEnable(void) {}

void I2CSlaveIntClearEx(uint32_t ui32IntFlags) {
  (void)ui32IntFlags;
}

void I2CSlaveIntDisableEx(uint32_t ui32IntFlags) {
  (void)ui32IntFlags;
}

void I2CSlaveIntEnableEx(uint32_t ui32IntFlags) {
  (void)ui32IntFlags;
}

bool I2CSlaveIntStatus(bool bMasked) {
  (void)bMasked;
  return false;
}

uint32_t I2CSlaveIntStatusEx(bool bMasked) {
  (void)bMasked;
  return 0;
}

uint32_t I2CSlaveStatus(void) {
  return 0;
}

void SOCADCIntRegister(void (*pfnHandler)(void)) {
  (void)pfnHandler;
}

void SOCADCIntUnregister(void) {}

void SOCADCSingleConfigure(uint32_t ui32Resolution, uint32_t ui32Reference) {
  (void)ui32Resolution;
  (void)ui32Reference;
}

void SOCADCSingleStart(uint32_t ui32Channel) {
  (void)ui32Channel;
}

uint16_t SOCADCDataGet(void) {
  return 0;
}

bool SOCADCEndOfCOnversionGet(void) {
  return false;
}
