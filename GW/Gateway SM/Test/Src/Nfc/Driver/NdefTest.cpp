#include "NdefTest.h"

extern "C" {
#include <string.h>
}

TEST_F(NdefTest, ToNdef) {
  uint8_t data[MAX_NDEF_PAYLOAD_SIZE];
  SramNdef_t ndef;
  uint8_t index = 0;

  memset(data, 0xaa, MAX_NDEF_PAYLOAD_SIZE);
  NdefFromPayload(data, MAX_NDEF_PAYLOAD_SIZE, &ndef);

  EXPECT_EQ(ndef.length, NT3H2111_SIZE_SRAM);
  EXPECT_EQ(ndef.payload[index++], NDEF_SOF);
  EXPECT_EQ(ndef.payload[index++], MAX_NDEF_PAYLOAD_SIZE + SRAM_NDEF_RECORD_HEADER_SIZE);
  EXPECT_EQ(ndef.payload[index++], NDEF_RECORD_HEADER);
  EXPECT_EQ(ndef.payload[index++], NDEF_TYPE_VALUE);
  EXPECT_EQ(ndef.payload[index++], MAX_NDEF_PAYLOAD_SIZE);
  EXPECT_EQ(memcmp(data, &ndef.payload[index], MAX_NDEF_PAYLOAD_SIZE), 0);
  index += MAX_NDEF_PAYLOAD_SIZE;
  EXPECT_EQ(ndef.payload[index], NDEF_EOF);
}

TEST_F(NdefTest, FromNdef) {
  const uint8_t PAYLOAD_SIZE = 10;
  uint8_t origPayload[PAYLOAD_SIZE];
  uint8_t data[PAYLOAD_SIZE];
  SramNdef_t ndef;
  uint8_t index = 0;

  // Clear data
  memset(data, 0, PAYLOAD_SIZE);

  // Setup payload
  memset(origPayload, 0x55, PAYLOAD_SIZE);

  // Setup NDEF
  ndef.length = PAYLOAD_SIZE + SRAM_NDEF_OVERHEAD_SIZE;
  ndef.payload[index++] = NDEF_SOF;
  ndef.payload[index++] = PAYLOAD_SIZE + SRAM_NDEF_RECORD_HEADER_SIZE;
  ndef.payload[index++] = NDEF_RECORD_HEADER;
  ndef.payload[index++] = NDEF_TYPE_VALUE;
  ndef.payload[index++] = PAYLOAD_SIZE;
  memcpy(&ndef.payload[index], origPayload, PAYLOAD_SIZE);
  index += PAYLOAD_SIZE;
  ndef.payload[index] = NDEF_EOF;

  EXPECT_EQ(NdefGetPayload(data, &ndef), PAYLOAD_SIZE);
  EXPECT_EQ(memcmp(data, origPayload, PAYLOAD_SIZE), 0);
}
