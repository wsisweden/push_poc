#pragma once
#include <gtest/gtest.h>

extern "C" {
#include "../../../../Src/Nfc/Driver/Nt3h2111_I2c.h"
#include "../../../../Src/Nfc/Driver/Nt3h2111.h"
}

struct Nt3h2111Test : testing::Test {
  virtual void SetUp() {
    Nt3h2111_I2c_init();
    Nt3h2111_init();
  }
};
