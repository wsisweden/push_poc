#include "Nt3h2111Test.h"

TEST_F(Nt3h2111Test, Init) {
  uint8_t reg = 0;
  Nt3h2111_I2c_readRegister(Nt3h2111_SessionRegisterNc, &reg);
  EXPECT_EQ(reg & NT3H2111_NC_REG_MASK_PTHRU_SRAM, 0);
  EXPECT_EQ(reg & (NT3H2111_NC_REG_MASK_FD_OFF | NT3H2111_NC_REG_MASK_FD_ON),
      (NT3H2111_NC_REG_MASK_FD_OFF | NT3H2111_NC_REG_MASK_FD_ON));
}

TEST_F(Nt3h2111Test, ReadEepromNdef) {
  uint8_t temp[EEPROM_NDEF_SIZE];
  EepromNdef_t ndef;

  memset(temp, 0, EEPROM_NDEF_SIZE);

  ndef.length = EEPROM_NDEF_SIZE;
  Nt3h2111_readEepromNdef(&ndef);
  EXPECT_EQ(memcmp(ndef.payload, temp, EEPROM_NDEF_SIZE), 0);
}

TEST_F(Nt3h2111Test, WriteEepromNdef) {
  uint8_t temp[EEPROM_NDEF_SIZE];
  uint8_t i;
  EepromNdef_t ndef;

  for (i = 0; i < EEPROM_NDEF_SIZE; ++i) {
    temp[i] = i;
    ndef.payload[i] = i;
  }

  ndef.length = EEPROM_NDEF_SIZE;
  Nt3h2111_writeEepromNdef(&ndef);
  memset(ndef.payload, 0, EEPROM_NDEF_SIZE);
  Nt3h2111_readEepromNdef(&ndef);
  EXPECT_EQ(memcmp(ndef.payload, temp, EEPROM_NDEF_SIZE), 0);
}

TEST_F(Nt3h2111Test, ReadSram) {
  uint8_t temp[NT3H2111_SIZE_SRAM];
  uint8_t temp2[NT3H2111_SIZE_SRAM];

  memset(temp, 0, NT3H2111_SIZE_SRAM);
  memset(temp2, 0, NT3H2111_SIZE_SRAM);

  Nt3h2111_readSram(temp2);
  EXPECT_EQ(memcmp(temp2, temp, NT3H2111_SIZE_SRAM), 0);
}

TEST_F(Nt3h2111Test, WriteSramNdef) {
  uint8_t temp[NT3H2111_SIZE_SRAM];
  uint8_t i;
  SramNdef_t ndef;

  for (i = 0; i < NT3H2111_SIZE_SRAM; ++i) {
    temp[i] = i;
    ndef.payload[i] = i;
  }

  ndef.length = NT3H2111_SIZE_SRAM;
  Nt3h2111_writeSramNdef(&ndef);
  memset(temp, 0, NT3H2111_SIZE_SRAM);
  Nt3h2111_readSram(temp);
  EXPECT_EQ(memcmp(ndef.payload, temp, NT3H2111_SIZE_SRAM), 0);
}

TEST_F(Nt3h2111Test, SramEepromCollision) {
  uint8_t origSram[NT3H2111_SIZE_SRAM];
  uint8_t origEeprom[EEPROM_NDEF_SIZE];
  uint8_t i;
  SramNdef_t sram;
  EepromNdef_t eeprom;
  uint8_t temp[NT3H2111_SIZE_SRAM];

  for (i = 0; i < NT3H2111_SIZE_SRAM; ++i) {
    sram.payload[i] = i;
    origSram[i] = i;
  }

  for (i = 0; i < EEPROM_NDEF_SIZE; ++i) {
    eeprom.payload[i] = i;
    origEeprom[i] = i;
  }

  sram.length = NT3H2111_SIZE_SRAM;
  eeprom.length = EEPROM_NDEF_SIZE;

  Nt3h2111_writeSramNdef(&sram);
  Nt3h2111_writeEepromNdef(&eeprom);
  Nt3h2111_readSram(temp);
  Nt3h2111_readEepromNdef(&eeprom);
  EXPECT_EQ(memcmp(temp, origSram, NT3H2111_SIZE_SRAM), 0);
  EXPECT_EQ(memcmp(eeprom.payload, origEeprom, EEPROM_NDEF_SIZE), 0);

  Nt3h2111_writeEepromNdef(&eeprom);
  Nt3h2111_writeSramNdef(&sram);
  Nt3h2111_readEepromNdef(&eeprom);
  Nt3h2111_readSram(temp);
  EXPECT_EQ(memcmp(eeprom.payload, origEeprom, EEPROM_NDEF_SIZE), 0);
  EXPECT_EQ(memcmp(temp, origSram, NT3H2111_SIZE_SRAM), 0);
}
