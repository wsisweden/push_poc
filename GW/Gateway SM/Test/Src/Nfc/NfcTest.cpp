#include "NfcTest.h"

extern "C" {
#include "../../Src/Nfc/Nfc_Defines.h"
#include "../../Src/Nfc/Driver/Nt3h2111.h"
#include "../../Mockups/Nt3h2111_I2c_test.h"
#include <Hw/TiPhy/hal_defs.h>
#include <GenericProtocol/GenericPack.h>
#include <GenericProtocol/GenericParse.h>
#include <GenericProtocol/GenericReceive.h>
#include <GenericProtocol/ProtocolBuffers/Gen/ReadRequest.pb.h>
#include <Crc16/Crc16.h>
}

TEST_F(NfcTest, Init) {
  EepromNdef_t eeprom;
  SramNdef_t sram;
  uint8_t temp[NT3H2111_SIZE_SRAM];
  sPopEvtData_t evt;

  evt.iTimerId = 1;
  NfcTaskEventLoop(gPopEvtTimer_c, evt);

  eeprom.length = NFC_AAR_LENGTH;
  Nt3h2111_readEepromNdef(&eeprom);
  EXPECT_EQ(memcmp(eeprom.payload, Nfc_Aar_Message, NFC_AAR_LENGTH), 0);

  sram.length = MIN(NFC_AAR_LENGTH, NT3H2111_SIZE_SRAM);
  Nt3h2111_readSram(temp);
  EXPECT_EQ(memcmp(temp, Nfc_Aar_Message, sram.length), 0);
}

TEST_F(NfcTest, Crc) {
  uint8_t test[20];
  uint8_t i;
  for (i = 0; i < 20; ++i)
    test[i] = i;

  uint16_t crc = Crc16(test, 20);
  EXPECT_EQ(crc, 28558);
}

TEST_F(NfcTest, ReadInfo) {
  GenericProtocol_ReadRequest message =
      GenericProtocol_ReadRequest_init_default;
  sPopEvtData_t evt;
  uint8_t payload[NT3H2111_SIZE_SRAM];
  uint8_t payloadRead[NT3H2111_SIZE_SRAM];

  // Initialize
  evt.iTimerId = 1;
  NfcTaskEventLoop(gPopEvtTimer_c, evt);

  message.type = GenericProtocol_ReadRequest_Type_INFO;

  GenericPackStartFrame(&genericPack);
  GenericPackAddCmd(&genericPack, GenericTypeReadReq, GenericProtocol_ReadRequest_fields,
      &message);
  GenericPackEndFrame(&genericPack);
  GenericPackGetFrameChunk(&genericPack, payload, NT3H2111_SIZE_SRAM);
  Nt3h2111_writeSram(payload, NT3H2111_SIZE_SRAM);

  // Write to register that field is detected
  Nt3h2111_I2c_writeRegister(Nt3h2111_SessionRegisterNs, 0xff, 0x11);

  // Poll the state machine to discover the field
  NfcTaskEventLoop(gPopEvtTimer_c, evt);

  // Poll the state machine to handle message
  NfcTaskEventLoop(gPopEvtTimer_c, evt);
  Nt3h2111_readSram(payloadRead);

  uint16_t typeVal;
  GenericType_t type;
  GenericReadU16(CMD_TYPE_POS, &typeVal, &payloadRead[1]);
  type = (GenericType_t)typeVal;
  EXPECT_EQ(type, GenericTypeInfoResp);
}
