#pragma once
#include <gtest/gtest.h>

extern "C" {
#include "../../Src/Nfc/Driver/Nt3h2111_I2c.h"
#include "../../Src/Nfc/NfcMessageHandler.h"
#include "../../Src/Nfc/Nfc_Defines.h"
#include <GenericProtocol/GenericPack.h>
#include <GenericProtocol/GenericParse.h>
#include <GenericProtocol/GenericReceive.h>
}

struct NfcMessageHandlerTest : testing::Test {
  GenericReceive_t genericReceive;
  GenericPack_t genericPack;
  uint8_t nfcMessageReadBuffer[BUFFER_SIZE];
  uint8_t nfcMessageWriteBuffer[BUFFER_SIZE];

  virtual void SetUp() {
    GenericReceiveInit(&genericReceive);
    GenericReceiveSetBuffer(&genericReceive, nfcMessageReadBuffer,
                            sizeof(nfcMessageReadBuffer));

    GenericPackInit(&genericPack, nfcMessageWriteBuffer,
                    sizeof(nfcMessageWriteBuffer));

    Nt3h2111_I2c_init();
    NfcMessageInit();
  }
};
