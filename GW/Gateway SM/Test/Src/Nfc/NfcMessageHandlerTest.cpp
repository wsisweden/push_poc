/*
 * NfcMessageHandlerTest.cpp
 *
 *  Created on: 31 jan. 2017
 *      Author: AndCar_MEM
 */

#include "NfcMessageHandlerTest.h"

extern "C" {
#include "../../Src/Nfc/Driver/Nt3h2111_Defines.h"
#include "../../Src/Nfc/Driver/Nt3h2111.h"
#include "../../Lib/Crc16/Crc16.h"
#include <GenericProtocol/GenericPack.h>
#include <GenericProtocol/GenericParse.h>
#include <GenericProtocol/GenericReceive.h>
#include <GenericProtocol/ProtocolBuffers/Gen/ReadRequest.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/WriteRequest.pb.h>
}

TEST_F(NfcMessageHandlerTest, Init) {
  EXPECT_TRUE(NfcMessageIsIdle());
}

TEST_F(NfcMessageHandlerTest, GenericPackMax) {
  GenericProtocol_ReadRequest message =
      GenericProtocol_ReadRequest_init_default;

  message.type = GenericProtocol_ReadRequest_Type_STATUS;

  GenericPackInit(&genericPack, NULL, 0);
  GenericPackStartFrame(&genericPack);
  EXPECT_FALSE(GenericPackAddCmd(&genericPack, GenericTypeReadReq,
      GenericProtocol_ReadRequest_fields, &message));
}

TEST_F(NfcMessageHandlerTest, HandleMessage) {
  GenericProtocol_ReadRequest message =
      GenericProtocol_ReadRequest_init_default;
  uint8_t payload[NT3H2111_SIZE_SRAM];

  message.type = GenericProtocol_ReadRequest_Type_INFO;

  GenericPackStartFrame(&genericPack);
  GenericPackAddCmd(&genericPack, GenericTypeReadReq,
      GenericProtocol_ReadRequest_fields, &message);
  GenericPackEndFrame(&genericPack);
  GenericPackGetFrameChunk(&genericPack, payload, NT3H2111_SIZE_SRAM);

  NfcMessageHandle(payload, NT3H2111_SIZE_SRAM);
  EXPECT_FALSE(NfcMessageIsIdle());

  for (int i = 0; i < 4; ++i)
    NfcMessageCycle();
  EXPECT_TRUE(NfcMessageIsIdle());
}

TEST_F(NfcMessageHandlerTest, ReadAllData) {
  GenericProtocol_ReadRequest message =
      GenericProtocol_ReadRequest_init_default;
  uint8_t payload[NT3H2111_SIZE_SRAM];

  GenericPackStartFrame(&genericPack);

  message.type = GenericProtocol_ReadRequest_Type_INFO;
  GenericPackAddCmd(&genericPack, GenericTypeReadReq,
      GenericProtocol_ReadRequest_fields, &message);
  message.type = GenericProtocol_ReadRequest_Type_STATUS;
  GenericPackAddCmd(&genericPack, GenericTypeReadReq,
      GenericProtocol_ReadRequest_fields, &message);
  message.type = GenericProtocol_ReadRequest_Type_CONFIG;
  GenericPackAddCmd(&genericPack, GenericTypeReadReq,
      GenericProtocol_ReadRequest_fields, &message);

  GenericPackEndFrame(&genericPack);
  GenericPackGetFrameChunk(&genericPack, payload, NT3H2111_SIZE_SRAM);
  NfcMessageHandle(payload, NT3H2111_SIZE_SRAM);
  EXPECT_FALSE(NfcMessageIsIdle());
  Nt3h2111_readSram(payload);
  GenericReceiveAddData(&genericReceive, payload, NT3H2111_SIZE_SRAM);

  while (!NfcMessageIsIdle()) {
    NfcMessageCycle();
    NfcMessageCallbackCycle();
    Nt3h2111_readSram(payload);
    GenericReceiveAddData(&genericReceive, payload, NT3H2111_SIZE_SRAM);
  }

  EXPECT_EQ(GenericReceiveIsComplete(&genericReceive), GenericReceiveErrorOk);
  EXPECT_EQ(GenericReceiveIsCmdValid(&genericReceive), GenericReceiveErrorOk);
  EXPECT_EQ(GenericReceiveGetCmdType(&genericReceive), GenericTypeInfoResp);
  EXPECT_TRUE(GenericReceiveNextCmd(&genericReceive));
  EXPECT_EQ(GenericReceiveIsCmdValid(&genericReceive), GenericReceiveErrorOk);
  EXPECT_EQ(GenericReceiveGetCmdType(&genericReceive), GenericTypeAcknowledgeResp);
  EXPECT_TRUE(GenericReceiveNextCmd(&genericReceive));
  EXPECT_EQ(GenericReceiveIsCmdValid(&genericReceive), GenericReceiveErrorOk);
  EXPECT_EQ(GenericReceiveGetCmdType(&genericReceive), GenericTypeAcknowledgeResp);
  EXPECT_FALSE(GenericReceiveNextCmd(&genericReceive));
}

TEST_F(NfcMessageHandlerTest, CrcError) {
  GenericProtocol_ReadRequest message =
      GenericProtocol_ReadRequest_init_default;
  uint8_t payload[NT3H2111_SIZE_SRAM];

  message.type = GenericProtocol_ReadRequest_Type_INFO;

  GenericPackStartFrame(&genericPack);
  GenericPackAddCmd(&genericPack, GenericTypeReadReq,
      GenericProtocol_ReadRequest_fields, &message);
  GenericPackEndFrame(&genericPack);
  GenericPackGetFrameChunk(&genericPack, payload, NT3H2111_SIZE_SRAM);

  payload[0] = 1;
  payload[1] = 2;

  NfcMessageHandle(payload, NT3H2111_SIZE_SRAM);

  Nt3h2111_readSram(payload);

  uint16_t typeVal;
  GenericType_t type;
  GenericReadU16(CMD_TYPE_POS, &typeVal, &payload[1]);
  type = (GenericType_t)typeVal;
  EXPECT_EQ(type, GenericTypeAcknowledgeResp);
}

TEST_F(NfcMessageHandlerTest, UnknownCommandError) {
  GenericProtocol_ReadRequest message =
      GenericProtocol_ReadRequest_init_default;
  uint8_t payload[NT3H2111_SIZE_SRAM];

  message.type = GenericProtocol_ReadRequest_Type_INFO;

  GenericPackStartFrame(&genericPack);
  GenericPackAddCmd(&genericPack, (GenericType_t)99,
      GenericProtocol_ReadRequest_fields, &message);
  GenericPackEndFrame(&genericPack);
  GenericPackGetFrameChunk(&genericPack, payload, NT3H2111_SIZE_SRAM);
  NfcMessageHandle(payload, NT3H2111_SIZE_SRAM);

  Nt3h2111_readSram(payload);

  uint16_t typeVal;
  GenericType_t type;
  GenericReadU16(CMD_TYPE_POS, &typeVal, &payload[1]);
  type = (GenericType_t)typeVal;
  EXPECT_EQ(type, GenericTypeAcknowledgeResp);
}
