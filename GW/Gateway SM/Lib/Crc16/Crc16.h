#ifndef MODBUS_CRC16_H
#define MODBUS_CRC16_H

#include <stdint.h>

uint16_t Crc16(const uint8_t* const data, const uint32_t length);

#endif
