//*****************************************************************************
//! @file       setup.c
//! @brief      Setup code for CC2538 for use with IAR EWARM.
//!
//! Revised     $Date: 2016-06-15 13:17:23 +0300 (ke, 15 kesä 2016) $
//! Revision    $Revision: 831 $
//
//  Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
//
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//
//    Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//****************************************************************************/

#include <stdint.h>

/**
 *  Compile main application for use with bootloader. The bootloader
 *  sectors and the CCA (Customer Configuration Area) sector will be left
 *  untouched.
 *
 *  This option could be useful for production builds. The CCA sector needs
 *  to be included with the bootloader or flashed separately.
 */
#define SETUP_BOOTOPTION_BL_NOCCA     1

/**
 *  Compile main application for use with bootloader. The bootloader
 *  sectors will be left untouched. The CCA sector will be written and it
 *  will point to the bootloader interrupt vector.
 *
 *  This option is useful if we need to rewrite the lock bits sector.
 */
#define SETUP_BOOTOPTION_BL_CCA       2

/**
 *  Compile main application for use without bootloader. The bootloader
 *  sectors will be left untouched. The lock bit sector will be written and it
 *  will point to the main application interrupt vector.
 *
 *  This option is useful if we want to run the main application directly
 *  without the bootloader e.g. when debugging the main application.
 */
#define SETUP_BOOTOPTION_NOBL_CCA     3




/**
 * Define this to one of the defines above (SETUP_BOOTOPTION_BL_NOCCA,
 * SETUP_BOOTOPTION_BL_CCA or SETUP_BOOTOPTION_NOBL_CCA). 
 */

#define SETUP_BOOTOPTION  SETUP_BOOTOPTION_BL_NOCCA





#if SETUP_BOOTOPTION != SETUP_BOOTOPTION_BL_NOCCA


#if SETUP_BOOTOPTION == SETUP_BOOTOPTION_BL_CCA
/** 
 * The bootloader ISR vector is used.
 */
# define FLASH_START_ADDR               0x00200000


#elif SETUP_BOOTOPTION == SETUP_BOOTOPTION_NOBL_CCA
/** 
 * The main application ISR vector is used.
 */
# define FLASH_START_ADDR               0x00202000
#endif


#define BACKDOOR_RESERVED               0xE0FFFFFFUL
#define BACKDOOR_ENABLE                 (1<<28)
#define BACKDOOR_ACTIVE_HIGH            (1<<27)
#define BACKDOOR_ACTIVE_LOW             (0<<27)
#define BACKDOOR_PA0                    (0<<24)
#define BACKDOOR_PA1                    (1<<24)
#define BACKDOOR_PA2                    (2<<24)
#define BACKDOOR_PA3                    (3<<24)
#define BACKDOOR_PA4                    (4<<24)
#define BACKDOOR_PA5                    (5<<24)
#define BACKDOOR_PA6                    (6<<24)
#define BACKDOOR_PA7                    (7<<24)

#define BOOTLOADER_BACKDOOR_DISABLE     (BACKDOOR_RESERVED | BACKDOOR_PA7 | BACKDOOR_ACTIVE_LOW)
#define SYS_CTRL_EMUOVR                 0x400D20B4
#define SYS_CTRL_I_MAP                  0x400D2098


//*****************************************************************************
//
// Customer Configuration Area in Lock Page
// Holds Image Vector table address (bytes 2013 - 2015) and
// Image Valid bytes (bytes 2008 -2011)
//
//*****************************************************************************
typedef struct
{
    uint32_t ui32BootldrCfg;
    uint32_t ui32ImageValid;
    uint32_t ui32ImageVectorAddr;
}
lockPageCCA_t;

#ifdef __IAR_SYSTEMS_ICC__
__root const lockPageCCA_t __cca @ ".flashcca" =
#elif __TI_COMPILER_VERSION__
#pragma DATA_SECTION(__cca, ".flashcca")
#pragma RETAIN(__cca)
const lockPageCCA_t __cca =
#else
__attribute__ ((section(".flashcca"), used))
const lockPageCCA_t __cca =
#endif
{
  BOOTLOADER_BACKDOOR_DISABLE,  // Backdoor disabled
  0,                            // Image valid bytes
  FLASH_START_ADDR              // Vector table located at flash start address
};

#endif
