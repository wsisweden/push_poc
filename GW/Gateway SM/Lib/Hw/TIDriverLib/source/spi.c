#include "spi.h"
#include "../inc/hw_ssi.h"
#include "../inc/hw_ints.h"
#include "../inc/hw_memmap.h"
#include "../inc/hw_gpio.h"
#include "../inc/hw_ioc.h"
#include "tissi.h"
#include "gpio.h"
#include "interrupt.h"
#include "sys_ctrl.h"
#include "ioc.h"
#include <stddef.h>

/**
 * Slave select pin modes.
 */
typedef enum{
  TISPI_SS_AUTOINV,
  TISPI_SS_AUTO,  /**< SS pin is automatically set low during frame transfer. */
  TISPI_SS_LOW,   /**< SS pin is low. */
  TISPI_SS_HIGH   /**< SS pin is high. */
} tispi_SsMode;

static void spi_EmptyCallback(uint32_t output);
static void SPI_Isr(void);

void (* volatile spi_Callback)(uint32_t) = spi_EmptyCallback;

static volatile uint32_t tispiTxBytesLeft = 0;
static volatile uint32_t tispiTotalBytesLeft = 0;
static volatile tispi_SsMode tispiSsMode = TISPI_SS_AUTO;
static volatile uint32_t tispiRxData;
static volatile uint32_t tispiReserved = false;

static spi_ReserveReq * volatile pFifoHead;
static spi_ReserveReq * volatile pFifoTail;

/**
 * @brief   Initialize SPI master.
 */
void spi_init(void){
  /* Pin configuration in PortConfig.h */

  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_SSI0);

  /* SCK PC1 */
  IOCPinConfigPeriphOutput(GPIO_C_BASE, GPIO_PIN_1, IOC_MUX_OUT_SEL_SSI0_CLKOUT);
  GPIOPinTypeSSI(GPIO_C_BASE, GPIO_PIN_1);

  /* MOSI PD1 */
  IOCPinConfigPeriphOutput(GPIO_D_BASE, GPIO_PIN_1, IOC_MUX_OUT_SEL_SSI0_TXD);
  GPIOPinTypeSSI(GPIO_D_BASE, GPIO_PIN_1);

  /* MISO PD2 */
  IOCPinConfigPeriphInput(GPIO_D_BASE, GPIO_PIN_2, IOC_SSIRXD_SSI0);
  GPIOPinTypeSSI(GPIO_D_BASE, GPIO_PIN_2);

  /* SS PC0 */
  GPIOPinWrite(GPIO_C_BASE, GPIO_PIN_0, GPIO_PIN_0);
  GPIOPinTypeGPIOOutput(GPIO_C_BASE, GPIO_PIN_0);

  SSIConfigSetExpClk(
    SSI0_BASE,
    SysCtrlClockGet(),
    SSI_FRF_MOTO_MODE_0,
    SSI_MODE_MASTER,
    1000000,          /* 1 MHz SPI clock */
    8                 /* Using 8-bit frames. */
  );

  SSIIntRegister(SSI0_BASE, &SPI_Isr);
  IntPrioritySet(INT_SSI0, 2<<5);
  SSIIntEnable(
    SSI0_BASE,
    SSI_IM_RXIM
    | SSI_IM_RTIM
  );

  SSIEnable(SSI0_BASE);

  pFifoHead = NULL;
  pFifoTail = NULL;
}

/**
 * @brief   Setup slave select pin mode.
 *
 * @param   out   0=Automatic SS (high while frame transfer is in progress).
 *                1=Automatic SS (low while frame transfer is in progress).
 *                2=Sets SS pin low.
 *                3=Sets SS pin high.
 */
void spi_setup_SS_SETUP(uint16_t out){
  tispiSsMode = (tispi_SsMode) out;
  if(tispiSsMode == TISPI_SS_LOW || tispiSsMode == TISPI_SS_AUTOINV){
    /*
     *  Configure Auto SS active low.
     */
    HWREG(GPIO_C_BASE + (GPIO_O_DATA + (GPIO_PIN_0 << 2))) = 0;
  }
  else{
    /*
     *  Configure SS low.
     */
    HWREG(GPIO_C_BASE + (GPIO_O_DATA + (GPIO_PIN_0 << 2))) = GPIO_PIN_0;
  }
}

/**
 * @brief   Reserve the SPI bus.
 *
 * @param   pReservedCb   Pointer to function that will be called when the
 *                        bus has been reserved.
 */
void spi_reserve(spi_ReserveReq * pReserveReq){

  IntMasterDisable();

  if (tispiReserved == true){
    /*
     *  Some other driver has reserved the bus. Store the reservation request
     *  pointer so that we can inform the waiting driver when the bus is free.
     */

    if (pFifoTail == NULL){
      pFifoHead = pReserveReq;
    }
    else{
      pFifoTail->pNext = pReserveReq;
    }

    pFifoTail = pReserveReq;
    pReserveReq->pNext = NULL;
    IntMasterEnable();
  }
  else{
    /*
     *  The bus is currently not reserved. Just reserve the bus and call the
     *  callback function directly.
     */

    tispiReserved = true;
    IntMasterEnable();

    pReserveReq->pReservedCb();
  }
}

/**
 * @brief   Free the SPI bus.
 */
void spi_free(void){
  spi_ReserveReq * pReserveReq;

  IntMasterDisable();

  /*
   *  Pop the next bus reservation request from the FIFO.
   */

  pReserveReq = pFifoHead;
  if (pReserveReq != NULL){
    pFifoHead = pReserveReq->pNext;
    if (pFifoHead == NULL){
      pFifoTail = NULL;
    }

    pReserveReq->pNext = NULL;

    /*
     *  There was a driver waiting. Call the reserved callback function to
     *  inform the driver that the bus has been reserved.
     */
    IntMasterEnable();
    pReserveReq->pReservedCb();
  }
  else{
    /*
     *  No driver was waiting for the bus. Mark the bus as free.
     */
    tispiReserved = false;
    IntMasterEnable();
  }
}

/**
 * @brief   Transfer data on SPI bus.
 *
 * @param   data        Left to be written (left-justified).
 * @param   txCount     Number of bits to be written.
 * @param   totalCount  Total number of bits to be transferred (read and write).
 */
void spi_command(uint32_t data, int txCount, int totalCount){

  // Should never be busy here, but checking it anyway.
  while((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_BSY) != 0);

  totalCount++;

  /* Just checking while debugging. */
  while((txCount % 8) != 0);
  while((totalCount % 8) != 0);

  /* Converting to bytes instead of bits */
  totalCount >>= 3;
  txCount >>= 3;

  tispiTotalBytesLeft = totalCount;
  tispiTxBytesLeft = txCount;
  tispiRxData = 0;

  if(tispiSsMode == TISPI_SS_AUTO){
    // Set CS pin low
    HWREG(GPIO_C_BASE + (GPIO_O_DATA + (GPIO_PIN_0 << 2))) = 0;
  }

  // Copy data to the TX FIFO
  do{
    if(txCount != 0){
      HWREG(SSI0_BASE + SSI_O_DR) = data >> 24;
      data = data << 8;
      txCount--;
    }
    else{
      /*
       *  Copy dummy byte to the data register to generate clock for RX.
       */
      HWREG(SSI0_BASE + SSI_O_DR) = 0;
    }
  }
  while(--totalCount != 0);
}

/**
 * @brief   SSI peripheral interrupt service routine.
 */
static void SPI_Isr(void){
  uint32_t ssiMis;

  ssiMis = HWREG(SSI0_BASE + SSI_O_MIS);

  if(ssiMis & SSI_MIS_RTMIS){
    HWREG(SSI0_BASE + SSI_O_ICR) = SSI_ICR_RTIC;
  }

  /*
   *  Read bytes from the data register until it is empty.
   */

  while((HWREG(SSI0_BASE + SSI_O_SR) & SSI_SR_RNE) != 0){
    uint8_t rxByte;

    rxByte = HWREG(SSI0_BASE + SSI_O_DR);

    if(tispiTxBytesLeft != 0){
      tispiTxBytesLeft--;
    }
    else{
      tispiRxData <<= 8;
      tispiRxData |= rxByte;
    }

    if(--tispiTotalBytesLeft == 0){
      uint32_t rxData = tispiRxData;

      if(tispiSsMode == TISPI_SS_AUTO){
        // Set CS pin high
        HWREG(GPIO_C_BASE + (GPIO_O_DATA + (GPIO_PIN_0 << 2))) = GPIO_PIN_0;
      }
      spi_Callback(rxData);
    }
  }
}

/**
 * @brief   Dummy callback function.
 */
static void spi_EmptyCallback(uint32_t output){
}
