
#include <stdlib.h>

#include "PopCfg.h"
#include "TiUart.h"
#include "uart.h"
#include "../inc/hw_memmap.h"
#include "interrupt.h"



typedef struct {
  uint32_t uartAddr;          /**< UART peripheral address.                 */
  void (*pfnIsr)(void);       /**< ISR function.                            */
  bool isOpen;              /**< true if UART is initialized.             */
  volatile bool isReading;  /**< true if reading is enabled.              */
  volatile bool isWriting;  /**< true if write is in progress.            */
  uint16_t bufferBytes;       /**< Number of bytes in TX buffer.            */
  uint16_t unsentBytes;       /**< Number of unsent bytes.                  */
  uint32_t uartClock;         /**< Clock frequency for the UART peripheral. */
  uint8_t txLevel;            /**< The configured TX interrupt trigger level.*/
  uint8_t * pTxBuf;           /**< Pointer to TX data buffer.               */
  UartCallbackFunctions_t cb; /**< Callback function pointers.              */
} TiUart_Inst;


static volatile TiUart_Inst * tiUartGetUartInst(uint8_t UartNumber);
static void tiUartFillReadCbArgs(volatile TiUart_Inst * pUartInst, UartReadCallbackArgs_t * pArgs);
static void tiUartFillTxFifo(volatile TiUart_Inst * pUartInst);
static void tiUartIsr0(void);
static void tiUartIsr1(void);
static void tiUartIsr(volatile TiUart_Inst * pUartInst);


volatile TiUart_Inst tiUart__inst0 = {UART0_BASE, &tiUartIsr0};
volatile TiUart_Inst tiUart__inst1 = {UART1_BASE, &tiUartIsr1};



/**
 *  @brief    Get a pointer to the specified UART instance.
 *
 *  @param    UartNumber  The UART peripheral number.
 *
 *  @return   Pointer to the UART instance. Null if the UartNumber parameter was
 *            invalid.
 */
static volatile TiUart_Inst * tiUartGetUartInst(uint8_t UartNumber)
{
  volatile TiUart_Inst * pUartInst = NULL;

  if (UartNumber == 0)
  {
    pUartInst = &tiUart__inst0;
  }
  else if (UartNumber == 1)
  {
    pUartInst = &tiUart__inst1;
  }

  return pUartInst;
}

/**
 *  @brief    Fill read callback arguments structure.
 *
 *  @param    pUartInst   Pointer to the UART instance.
 *  @param    pArgs       Pointer to the read callback arguments structure.
 *
 */
static void tiUartFillReadCbArgs(volatile TiUart_Inst * pUartInst, UartReadCallbackArgs_t * pArgs)
{
  uint32_t errors;

  errors = UARTRxErrorGet(pUartInst->uartAddr);
  UARTRxErrorClear(pUartInst->uartAddr);

  /*
   *  Handle error states.
   */

  pArgs->UartReadError.UartReadOverrunError = 0;
  pArgs->UartReadError.UartParityError = 0;
  pArgs->UartReadError.UartFrameError = 0;
  pArgs->UartReadError.UartStartBitError = 0;

  if (errors & UART_RXERROR_OVERRUN)
  {
    pArgs->UartReadError.UartReadOverrunError = 1;
  }

  if (errors & UART_RXERROR_PARITY)
  {
    pArgs->UartReadError.UartParityError = 1;
  }

  if (errors & UART_RXERROR_FRAMING)
  {
    pArgs->UartReadError.UartFrameError = 1;
  }

  /*
   *  Get number of bytes in FIFO.
   *
   *  The TI driver API does not provide a function that tells the number of
   *  received bytes. Just reporting one byte for now when there is data in the
   *  FIFO.
   */

  pArgs->UartNumberBytesReceived = 0;
  if (UARTCharsAvail(pUartInst->uartAddr) > 0)
  {
    pArgs->UartNumberBytesReceived = 1;
  }
}

/**
 *  @brief    Fill the TX FIFO with data from the TX buffer.
 *
 *  @param    pUartInst   Pointer to the UART instance.
 *
 *  @details  The function will fill the 16-byte TX FIFO with data from the 
 *            TX buffer until the FIFO is full or the buffer is empty.
 */
static void tiUartFillTxFifo(volatile TiUart_Inst * pUartInst)
{
  if (pUartInst->unsentBytes != 0)
  {
    uint32_t uartAddr = pUartInst->uartAddr;

    do
    {
      if (!UARTSpaceAvail(uartAddr))
      {
        break;
      }

      UARTCharPutNonBlocking(uartAddr, *pUartInst->pTxBuf++);
    }
    while (--pUartInst->unsentBytes != 0);
  }
}


void tiUartCallWriteCb(volatile TiUart_Inst * pUartInst)
{
  uint16_t sentBytes;
  UartWriteCbFn * pUartWriteCbFn;

  pUartWriteCbFn = pUartInst->cb.pfUartWriteCallback;
  sentBytes = pUartInst->bufferBytes;

  pUartInst->isWriting = false;

  if (pUartWriteCbFn != NULL)
  {
    UartWriteCallbackArgs_t writeCbArgs;

    writeCbArgs.UartNumberBytesSent = sentBytes;
    writeCbArgs.UartStatus = gUartWriteStatusComplete_c;

    pUartWriteCbFn(&writeCbArgs);
  }
}


static void tiUartIsr0(void)
{
  tiUartIsr(&tiUart__inst0);
}


static void tiUartIsr1(void)
{
  tiUartIsr(&tiUart__inst1);
}

/**
 *  @brief    Common code executed in both UART interrupts.
 *
 *  @param    pUartInst   Pointer to the UART instance.
 */
static void tiUartIsr(volatile TiUart_Inst * pUartInst)
{
  uint32_t intFlags;
  
  intFlags = UARTIntStatus(pUartInst->uartAddr, true);
  
  /*
   *  It is recommended that the interrupt source be cleared early in the
   *  interrupt handler (as opposed to the very last action) to avoid returning
   *  from the interrupt handler before the interrupt source is actually
   *  cleared. 
   */

  UARTIntClear(pUartInst->uartAddr, intFlags);

  if (intFlags & UART_INT_RT)
  {
    if (pUartInst->isReading && pUartInst->cb.pfUartReadCallback != NULL)
    {
      UartReadCallbackArgs_t readCbArgs;

      readCbArgs.UartStatus = gUartReadStatusComplete_c;
      tiUartFillReadCbArgs(pUartInst, &readCbArgs);
      pUartInst->cb.pfUartReadCallback(&readCbArgs);
    }
    else
    {
      /*
       *  Data received while not in reading state. Just throw the data away.
       */

      while (UARTCharsAvail(pUartInst->uartAddr))
      {
        UARTCharGetNonBlocking(pUartInst->uartAddr);
      }
    }
  }

  if (intFlags & UART_INT_TX)
  {
    if (pUartInst->isWriting)
    {
      /*
       *  The TX FIFO threshold has been reached. Copy more data to the FIFO.
       */

      tiUartFillTxFifo(pUartInst);

      if (pUartInst->unsentBytes == 0)
      {
        /*
         *  All data has been sent.
         */

        UARTIntDisable(pUartInst->uartAddr, UART_INT_TX);
        
        tiUartCallWriteCb(pUartInst);
      }
    }
  }

  if (intFlags & UART_INT_RX)
  {
    if (pUartInst->isReading && pUartInst->cb.pfUartReadCallback != NULL)
    {
      UartReadCallbackArgs_t readCbArgs;

      readCbArgs.UartStatus = gUartReadStatusComplete_c;
      tiUartFillReadCbArgs(pUartInst, &readCbArgs);
      pUartInst->cb.pfUartReadCallback(&readCbArgs);
    }
    else
    {
      /*
       *  Data received while not in reading state. Just throw the data away.
       */

      while (UARTCharsAvail(pUartInst->uartAddr))
      {
        UARTCharGetNonBlocking(pUartInst->uartAddr);
      }
    }
  }
}

/**
 *  @brief    The function is called to initialize the UART peripheral.
 *
 *  @param    UartNumber      The UART peripheral number. 
 *  @param    PlatformClock   Platform clock (in Khz). Will be used for baudrate calculation.
 *
 *  @details  There are some tests performed before initializing the UART peripheral.
 *            - If the UART instance number to open exceeds the UART_NR_INSTANCES (number of UART peripherals present on platform), the
 *              function exits with gUartErrWrongUartNumber_c value.
 *            - If PlatformClock parameter is equal to zero or greater then PLATFORM_MAX_CLOCK,
 *              the function exits with gUartErrInvalidClock_c value.
 *            - If UART module is already initialized, the function exits with gUartErrUartAlreadyOpen_c value.
 *
 *            If all of these tests have been passed, the function initializes the internal driver variables, enables the
 *            hardware transmitter and receiver modules and exits with gUartErrNoError_c value.
 */
UartErr_t UartOpen(uint8_t UartNumber, uint32_t PlatformClock)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (PlatformClock == 0 || PlatformClock > (SYS_CTRL_32MHZ/1000))
  {
    return gUartErrInvalidClock_c;
  }

  if (pUartInst->isOpen)
  {
    return gUartErrUartAlreadyOpen_c;
  }

  UARTDisable(pUartInst->uartAddr);

  {
    uint32_t uartAddr = pUartInst->uartAddr;
    UARTIntRegister(uartAddr, pUartInst->pfnIsr);
  }

  UARTFIFOEnable(pUartInst->uartAddr);
  UARTTxIntModeSet(pUartInst->uartAddr, UART_TXINT_MODE_FIFO);
  UARTFIFOLevelSet(pUartInst->uartAddr, UART_FIFO_TX2_8, UART_FIFO_RX2_8);

  UARTIntClear(pUartInst->uartAddr, 0x17FF);
  UARTIntEnable(pUartInst->uartAddr, UART_INT_RT | UART_INT_RX);

  UARTEnable(pUartInst->uartAddr);

  pUartInst->uartClock = PlatformClock * 1000;
  pUartInst->isOpen = true;
  
  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to get the communication parameters (parity, stop bits, baud rate,
 *            flow control) of the UART peripheral.
 *
 *  @param    UartNumber  The number of the UART instance to get configuration from.
 *  @param    pConfig     The pointer to a structure where the configuration parameters shall be placed.
 *
 *  @details  There are some tests performed before getting the configuration:
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART
 *              peripherals present on platform), the function exits with gUartErrWrongUartNumber_c value.
 *            - If the pointer to the structure where the configuration parameters shall be placed is not
 *              initialized, the function exits with gUartErrNullPointer_c value.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *
 *            If all of these tests have been passed, the function is filling the structure with the communication
 *            parameters according with the values in the peripheral hardware registers and returns the
 *            gUartErrNoError_c value.
 */
UartErr_t UartGetConfig(uint8_t UartNumber, UartConfig_t* pConfig)
{
  uint32_t config;
  uint32_t parity;
  uint32_t stopBits;
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (pConfig == NULL)
  {
    return gUartErrNullPointer_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  {
    uint32_t uartAddr = pUartInst->uartAddr;
    UARTConfigGetExpClk(
      uartAddr,
      pUartInst->uartClock,
      &pConfig->UartBaudrate,
      &config
    );
  }

  parity = config & UART_CONFIG_PAR_MASK;
  if (parity == UART_CONFIG_PAR_EVEN) {
    pConfig->UartParity = gUartParityEven_c;
  }
  else if (config == UART_CONFIG_PAR_ODD)
  {
    pConfig->UartParity = gUartParityOdd_c;
  }
  else
  {
    pConfig->UartParity = gUartParityNone_c;
  }

  stopBits = config & UART_CONFIG_STOP_MASK;
  if (stopBits == UART_CONFIG_STOP_ONE)
  {
    pConfig->UartStopBits = gUartStopBits1_c;
  }
  else
  {
    pConfig->UartStopBits = gUartStopBits2_c;
  }

  pConfig->UartFlowControlEnabled = false;
  pConfig->UartRTSActiveHigh = false;

  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to set the communication parameters (parity, stop bits, baud rate, flow control) for
 *            the UART peripheral.
 *
 *  @param    UartNumber  The number of the UART instance to get configuration from.
 *  @param    pConfig     Pointer to a structure containing the configuration parameters.
 *
 *  @details  There are some tests performed before configuring the UART peripheral.
 *            - If the UART peripheral instance number exceeds the UART_NR_INSTANCES, the function
 *              exits with gUartErrWrongUartNumber_c value.
 *            - If the pointer to the structure containing the configuration parameters is not initialized,
 *              the function exits with gUartErrNullPointer_c value.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *            - If the value of the baud rate in the configuration structure is lower than
 *              1200 bps or if it exceeds the maximum baud rate obtainable considering the frequency of the platform
 *              clock, the function exits with gUartErrInvalidBaudrate_c value.
 *            - If the value of the parity is out of the permitted range, the function exits with gUartErrInvalidParity_c value.
 *            - If the value of the stop bits is out of the permitted range, the function exits with gUartErrInvalidStop_c value.
 *            - If a read operation is in place, the function exits with gUartErrReadOngoing_c value.
 *            - If a write operation is currently running, the function exits with gUartErrWriteOngoing_c value.
 *
 *            If all of these tests have been passed, the function sets the peripheral hardware registers according with the
 *            values received in the configuration structure and returns the gUartErrNoError_c value .
 */
UartErr_t UartSetConfig(uint8_t UartNumber, UartConfig_t* pConfig)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (pConfig == NULL)
  {
    return gUartErrNullPointer_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  /*
   *  Only implemented no parity and one stop bit because thats what PopNet
   *  uses.
   */

  if (pConfig->UartParity != gUartParityNone_c)
  {
    return gUartErrInvalidParity_c;
  }

  if (pConfig->UartStopBits != gUartStopBits1_c)
  {
    return gUartErrInvalidStop_c;
  }

  if (pUartInst->isReading)
  {
    return gUartErrReadOngoing_c;
  }

  if (pUartInst->isWriting)
  {
    return gUartErrWriteOngoing_c;
  }

  UARTClockSourceSet(pUartInst->uartAddr, UART_CLOCK_SYSTEM);

  {
    uint32_t uartAddr = pUartInst->uartAddr;
    UARTConfigSetExpClk(
      uartAddr,
      pUartInst->uartClock,
      pConfig->UartBaudrate,
      UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE
    );
  }

  // The UARTConfigSetExpClk function will disable the UART.
  UARTEnable(pUartInst->uartAddr);

  return gUartErrNoError_c;
}

/**
 *  @brief    Set the receiver threshold.
 *
 *  @param    UartNumber  The number of the UART instance to set the Rx FIFO threshold.
 *  @param    Threshold   The value of the threshold (Number of bytes).
 *  
 *  @details  The UART peripheral receiver module has an internal 16 bytes FIFO. During the receive
 *            operation, the characters received are placed in this FIFO. An internal Rx interrupt is
 *            triggered when the number of characters in the FIFO exceeds a specified threshold.
 *            The UartSetReceiverThreshold() function sets the Rx threshold for a UART peripheral.
 *
 *            There are some tests performed before setting the threshold:
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART peripherals
 *              present on platform), the function exits with gUartErrWrongUartNumber_c value.
 *            - The value of the threshold should be in the interval [2, 14]. If the value is out
 *              of this interval, the function exits with gUartErrInvalidThreshold_c value.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *            - If a read operation is currently ongoing, the function exits with gUartErrReadOngoing_c value.
 *
 *            If all of these tests have been passed, the function sets the Rx threshold and exits
 *            with gUartErrNoError_c value.
 */
UartErr_t UartSetReceiverThreshold(uint8_t UartNumber, uint8_t Threshold)
{
  volatile TiUart_Inst * pUartInst;
  uint32_t rxLevel;
  uint32_t txLevel;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  if (pUartInst->isReading)
  {
    return gUartErrReadOngoing_c;
  }

  /*
   *  Get the current levels so that we don't have to modify the TX level in
   *  this function.
   */

  UARTFIFOLevelGet(pUartInst->uartAddr, &txLevel, &rxLevel);

  /*
   * The HW FIFO is 16 bytes. The TI hardware only supports five different
   * threshold levels.
   */

  if (Threshold == 2)
  {
    rxLevel = UART_FIFO_RX1_8;
  }
  else if (Threshold == 4)
  {
    rxLevel = UART_FIFO_RX2_8;
  }
  else if (Threshold == 8)
  {
    rxLevel = UART_FIFO_RX4_8;
  }
  else if (Threshold == 12)
  {
    rxLevel = UART_FIFO_RX6_8;
  }
  else if (Threshold == 14)
  {
    rxLevel = UART_FIFO_RX7_8;
  }
  else
  {
    return gUartErrInvalidThreshold_c;
  }

  UARTFIFOLevelSet(pUartInst->uartAddr, txLevel, rxLevel);
  
  return gUartErrNoError_c;
}


/**
 *  @brief    Set the transmitter threshold.
 *
 *  @param    UartNumber  The number of the UART instance to set the Tx FIFO threshold.
 *  @param    Threshold   The value of the threshold (Number of bytes).
 *
 *  @details  The UART peripheral transmitter module has an internal 16 bytes FIFO. During the transmit
 *            operation, the characters from FIFO are sent by shifting on the Tx pin. An internal
 *            Tx interrupt is triggered when the number of characters in the FIFO goes under a specified
 *            threshold. The UartSetTransmitterThreshold() function sets the threshold for the UART
 *            peripheral.
 *
 *            There are some tests performed before setting the threshold:
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES
 *              (number of UART peripherals present on platform), the function exits with
 *              gUartErrWrongUartNumber_c value.
 *            - The value of the threshold should be in the interval [1, 14]. If the value is out of
 *              this interval, the function exits with gUartErrInvalidThreshold_c value.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *            - If a write operation is currently ongoing, the function exits with gUartErrWriteOngoing_c
 *              value.
 *
 *            If all of these tests have been passed, the function sets the TX threshold and exits with
 *            gUartErrNoError_c value.
 */
UartErr_t UartSetTransmitterThreshold(uint8_t UartNumber, uint8_t Threshold)
{
  volatile TiUart_Inst * pUartInst;
  uint32_t rxLevel;
  uint32_t txLevel;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  if (pUartInst->isWriting)
  {
    return gUartErrWriteOngoing_c;
  }

  /*
   *  Get the current levels so that we don't have to modify the RX level in
   *  this function.
   */

  UARTFIFOLevelGet(pUartInst->uartAddr, &txLevel, &rxLevel);

  /*
   * The HW FIFO is 16 bytes. The TI hardware only supports five different
   * threshold levels.
   */

  if (Threshold == 1)
  {
    /*
     *  TI UART does not support a one byte threshold. Using a two byte
     *  threshold instead.
     */

    txLevel = UART_FIFO_TX1_8;
    pUartInst->txLevel = 2;
  }
  else if (Threshold == 2)
  {
    txLevel = UART_FIFO_TX1_8;
    pUartInst->txLevel = 2;
  }
  else if (Threshold == 4)
  {
    txLevel = UART_FIFO_TX2_8;
    pUartInst->txLevel = 4;
  }
  else if (Threshold == 8)
  {
    txLevel = UART_FIFO_TX4_8;
    pUartInst->txLevel = 8;
  }
  else if (Threshold == 12)
  {
    txLevel = UART_FIFO_TX6_8;
    pUartInst->txLevel = 12;
  }
  else if (Threshold == 14)
  {
    txLevel = UART_FIFO_TX7_8;
    pUartInst->txLevel = 14;
  }
  else
  {
    return gUartErrInvalidThreshold_c;
  }

  UARTFIFOLevelSet(pUartInst->uartAddr, txLevel, rxLevel);
  
  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to set the callback functions for the read and write operations for the UART
 *            peripheral.
 *
 *  @param    UartNumber  The number of the UART instance to set the Rx FIFO threshold.
 *  @param    pConfig     Pointer to a structure containing the pointers to the callback functions.
 *
 *  @details  There are some tests performed before setting the callback functions:
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART peripherals
 *              present on platform), the function exits with gUartErrWrongUartNumber_c value.
 *            - If the pointer to the structure containing the callback pointers is not initialized, the function
 *              exits with gUartErrNullPointer_c value.
 *            - The function will also exit with this error if both the pointers to the callback functions are null.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *
 *            If all of these tests have been passed, the function is setting the internal driver callback pointers with the
 *            ones received in the structure and returns the gUartErrNoError_c value.
 */
UartErr_t UartSetCallbackFunctions(uint8_t UartNumber, UartCallbackFunctions_t* pConfig)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (pConfig == NULL)
  {
    return gUartErrNullPointer_c;
  }

  if (pConfig->pfUartReadCallback == NULL && pConfig->pfUartWriteCallback == NULL)
  {
    return gUartErrNullPointer_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  pUartInst->cb.pfUartReadCallback = pConfig->pfUartReadCallback;
  pUartInst->cb.pfUartWriteCallback = pConfig->pfUartWriteCallback;

  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to get the status of the UART peripheral instance.
 *
 *  @param    UartNumber      The UART peripheral number.
 *
 *  @details  There is a test performed before getting the status of the UART:
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART peripherals
 *              present on platform), the function exits with gUartErrWrongUartNumber_c value.
 *            - If UART module is not initialized, the function returns the gUartErrUartNotOpen_c value.
 *            - If a read operation is in place, the function returns the gUartErrReadOngoing_c value.
 *            - If a write operation is in place, the function returns the gUartErrWriteOngoing_c value.
 *            - If no callback functions are defined for at least one of the read and write operations,
 *              the function returns the gUartErrNoCallbackDefined_c value.
 *            - If none of these conditions happens, the function returns the gUartErrNoError_c value indicating
 *              that UART is open, callback functions are defined but no operation is in place.
 */
UartErr_t UartGetStatus(uint8_t UartNumber)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  if (pUartInst->cb.pfUartReadCallback == NULL && pUartInst->cb.pfUartWriteCallback == NULL)
  {
    return gUartErrNoCallbackDefined_c;
  }
  
  if (pUartInst->isWriting)
  {
    return gUartErrWriteOngoing_c;
  }
  
  if (pUartInst->isReading)
  {
    return gUartErrReadOngoing_c;
  }

  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to cancel a read operation on the UART peripheral.
 *
 *  @param    UartNumber      The UART peripheral number.
 *
 *  @details  There are some tests performed before canceling the operation_
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART peripherals
 *              present on platform), the function exit with gUartErrWrongUartNumber_c value.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *
 *            If all of these tests have been passed, the function is setting the internal driver variables,
 *            calls the read callback function and exits with gUartErrNoError_c value.
 *
 *            The read callback function is called with parameter UartStatus set to
 *            gUartReadStatusCanceled_c  and parameter UartNbBytes set to number of characters received 
 *            before the UartCancelReadData() call.
 */
UartErr_t UartCancelReadData(uint8_t UartNumber)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  if (pUartInst->isReading == true)
  {
    UartReadCbFn * pReadCbFn;

    pReadCbFn = pUartInst->cb.pfUartReadCallback;
    pUartInst->isReading = false;

    if (pReadCbFn != NULL)
    {
      UartReadCallbackArgs_t cbArgs;

      cbArgs.UartStatus = gUartReadStatusCanceled_c;
      tiUartFillReadCbArgs(pUartInst, &cbArgs);

      pReadCbFn(&cbArgs);
    }

    UARTRxErrorClear(pUartInst->uartAddr);
  }

  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to cancel a write operation on the UART peripheral.
 *
 *  @param    UartNumber      The UART peripheral number.
 *
 *  @details  
 *            There are some tests performed before canceling the operation.
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART
 *              peripherals present on platform), the function exits with gUartErrWrongUartNumber_c value.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *
 *            If all of these tests have been passed, the function is setting the internal driver variables and exits with
 *            gUartErrNoError_c value.
 *
 *            The write callback function will be called with parameter UartStatus set to
 *            gUartWriteStatusCanceled_c and parameter UartNbBytes set to number of characters transmitted
 *            before the transmission was interrupted.
 */
UartErr_t UartCancelWriteData(uint8_t UartNumber)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  IntMasterDisable();
  UARTIntDisable(pUartInst->uartAddr, UART_INT_TX);
  UARTIntClear(pUartInst->uartAddr, UART_INT_TX);
  IntMasterEnable();

  tiUartCallWriteCb(pUartInst);

  return gUartErrNoError_c;
}

/**
 *  @brief    This function is called to initiate a read data operation on the Uart peripheral module.
 *
 *  @param    UartNumber          The UART peripheral number. 
 *  @param    pBuf                Pointer to the destination buffer, where the received characters will be copied.
 *  @param    BufferSize          The number of bytes to receive.
 *  @param    UartDirectFifoMode  Selects the read functionality. See the function description.
 *
 *  @details  The following tests are performed before the function starts the actual read:
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART
 *              peripherals present on platform), the function exits with gUartErrWrongUartNumber_c value.
 *            - If the pointer to the destination buffer is not initialized, the function exits with the
 *              gUartErrNullPointer_c value.
 *            - If the buffer length is 0, the function exits with gUartErrInvalidNrBytes_c value.
 *            - If the UART peripheral instance was not previously opened, the function exists with
 *              gUartErrUartNotOpen_c value
 *            - If a read operation is currently ongoing, the function exits with gUartErrReadOngoing_c value.
 *            - If no callback was previously configured for the current UART instance, the function exits with
 *              gUartErrNoCallbackDefined_c.
 *
 *            If no error condition is detected, the function sets the internal driver variables, peripheral hardware
 *            registers and exists with gUartErrNoError_c
 *
 *            There are two modes for this function:
 *            -# The function waits for the requested number of bytes to be received and exits.
 *            -# The function exposes directly the receive HW FIFO to the user.
 *
 *  @par      Mode 1
 *            This can be achieved by passing the UartDirectFifoMode parameter as 0 (false).
 *
 *            After a UartReadData() function call, if no data is received on the UART, the driver remains in the �uart
 *            read ongoing� state, until at least one character is received. The application can cancel at any time a read
 *            operation, by calling the UartCancelReadData() function. There is no timeout mechanism implemented in
 *            the UART driver.
 *
 *  @par      Mode 2
 *            This can be achieved by passing the UartDirectFifoMode as 1 (true).
 *
 *            After a UartReadData() call, the driver remains in the �uart read ongoing� state. The application can cancel
 *            at any time a read operation, by calling the UartCancelReadData() function. There is no timeout
 *            mechanism implemented in the UART driver.
 *
 *            The UART driver exits from a read operation in the following situations:
 *            - During the receive procedure, if the UartCancelReadData() function is called. In this case, the read
 *              callback function is triggered with the parameters UartStatus set to gUartReadStatusCanceled_c
 *              and UartNbBytes set to number of characters received before the process cancellation.
 *            
 *            In direct FIFO mode both pBuf and BufferSize passed-in parameters are ignored. The contents of the
 *            HW fifo are not copied in pBuf as soon as characters are received, instead the ReadCallback function is
 *            called if:
 *            - The Receive FIFO threshold is reached. The default receive threshold is set to 4 characters and can
 *              be changed by using the UartSetReceiverThreshold function.
 *            - After receiving at least one character, no characters have been received for at least 8 character
 *              periods (uart data aging).
 *
 *            After the driver calls the receive callback in the above described situations, the user have to read directly
 *            the HW FIFO contents by calling the UartGetByteFromRxBuffer until it returns false (FIFO emptied).
 */
UartErr_t UartReadData(uint8_t UartNumber, uint8_t* pBuf, uint16_t BufferSize, bool UartDirectFifoMode)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  if (pUartInst->isReading)
  {
    return gUartErrReadOngoing_c;
  }

  if (pUartInst->cb.pfUartReadCallback == NULL)
  {
    return gUartErrNoCallbackDefined_c;
  }

  if (UartDirectFifoMode)
  {
      pUartInst->isReading = true;

  }
  else
  {
    if (pBuf == NULL)
    {
      return gUartErrNullPointer_c;
    }

    if (BufferSize == 0)
    {
      return gUartErrInvalidNrBytes_c;
    }

    /*
     *  Mode 1 not implemented. Not used by PopNet.
     */

    return gUartErrUartNotOpen_c;
  }

  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to start a write data operation to the UART peripheral.
 *
 *  @param    UartNumber      The UART peripheral number. 
 *  @param    pBuf            The pointer to a buffer where the data will be sent from.
 *  @param    NumberBytes     The number of bytes to send.
 *
 *  @details  There are some tests performed before starting the operation.
 *            - If the UART peripheral instance number exceeds UART_NR_INSTANCES (number of UART
 *              peripherals present on platform), the function exits with gUartErrWrongUartNumber_c value.
 *            - If the pointer to the buffer where the data will be sent from is not initialized,
 *              the function exits with gUartErrNullPointer_c value.
 *            - If the number of characters to send is zero, the function exits with
 *              gUartErrInvalidNrBytes_c value.
 *            - If UART module is not initialized, the function exits with the gUartErrUartNotOpen_c value.
 *            - If a write operation is in place, the function exits with gUartErrWriteOngoing_c value.
 *            - If there is no callback function defined for the write operation, the function exits with
 *              gUartErrNoCallbackDefined_c value.
 *
 *            If all of these tests have been passed, the function sets the internal driver variables,
 *            peripheral hardware registers and exits with gUartErrNoError_c value.
 *
 *            After a UartWriteData() call the driver remains in the �uart write ongoing� state, until all
 *            the characters are sent. The upper layer can cancel at any time a write operation, by calling
 *            the UartCancelWriteData() function.
 *
 *            The UART driver exits from a write operation in the following situations:
 *            - the number of characters specified in the NumberBytes parameter has been sent. In this case,
 *              the write callback function is triggered with the parameters UartStatus set to
 *              gUartWriteStatusComplete_c and UartNbBytes set to NumberBytes.
 *            - during the transmit procedure, the UartCancelWriteData() function is called. In this case,
 *              the write callback function is triggered with the parameters UartStatus set to
 *              gUartWriteStatusCanceled_c and UartNbBytes set to number of characters sent before the
 *              process cancellation.
 */
UartErr_t UartWriteData(uint8_t UartNumber, uint8_t* pBuf, uint16_t NumberBytes)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return gUartErrWrongUartNumber_c;
  }

  if (pBuf == NULL)
  {
    return gUartErrNullPointer_c;
  }

  if (NumberBytes == 0)
  {
    return gUartErrInvalidNrBytes_c;
  }

  if (!pUartInst->isOpen)
  {
    return gUartErrUartNotOpen_c;
  }

  if (pUartInst->isWriting)
  {
    return gUartErrWriteOngoing_c;
  }

  if (pUartInst->cb.pfUartWriteCallback == NULL)
  {
    return gUartErrNoCallbackDefined_c;
  }

  pUartInst->isWriting = true;
  pUartInst->pTxBuf = pBuf;
  pUartInst->unsentBytes = NumberBytes;
  pUartInst->bufferBytes = NumberBytes;
  
  if (NumberBytes > 16)
  {
    /*
     *  The interrupt will copy the rest of the bytes and call the callback.
     */
    
    UARTIntEnable(pUartInst->uartAddr, UART_INT_TX);
    tiUartFillTxFifo(pUartInst);
  }
  else
  {
    /*
     * No need to use the interrupt if all bytes will fit on the TX FIFO.
     */
    
    UARTIntDisable(pUartInst->uartAddr, UART_INT_TX);
    tiUartFillTxFifo(pUartInst);
    tiUartCallWriteCb(pUartInst);
  }

  return gUartErrNoError_c;
}

/**
 *  @brief    The function is called to copy one character from the UART receive FIFO into the
 *            user-selected destination memory.
 *
 *  @param    UartNumber      The UART peripheral number.
 *  @param    pDst            Pointer to where the data will be stored.
 *
 *  @details  This function is intended to be used in the DirectFifoMode (see the UartReadData function)
 *            in which the driver exposes the receive HW FIFO directly to the user.
 *
 *            If at least one character is available in the HW receive FIFO, it will be extracted and
 *            updated in the memory location provided by the user at function call (pDst). The HW FIFO
 *            is emptied starting with the first received character (oldest in the FIFO). In this case
 *            the function returns true.
 *
 *            If no character was received, or the FIFO was already emptied, this function returns false
 *            and the pDst location is not updated.
 */
bool UartGetByteFromRxBuffer(uint8_t UartNumber, uint8_t *pDst)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst == NULL)
  {
    return false;
  }

  if (!pUartInst->isOpen)
  {
    return false;
  }

  if (pDst == NULL)
  {
    return false;
  }

  if (!pUartInst->isReading)
  {
    return false;
  }

  if (!UARTCharsAvail(pUartInst->uartAddr))
  {
    return false;
  }

  *pDst = UARTCharGetNonBlocking(pUartInst->uartAddr);

  return true;
}

/**
 *  @brief  Clears UART errors.
 *
 *  @param    UartNumber      The UART peripheral number.
 */
void UartClearErrors(uint8_t UartNumber)
{
  volatile TiUart_Inst * pUartInst;

  pUartInst = tiUartGetUartInst(UartNumber);
  if (pUartInst != NULL)
  {
    UARTRxErrorClear(pUartInst->uartAddr);
  }
}

void UartOpenCloseTransceiver(uint8_t UartNumber, uint8_t Pin, bool Open)
{

}
