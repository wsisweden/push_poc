

#ifndef _TIUART_H_
#define _TIUART_H_

#include "PopCfg.h"
#include <stdint.h>
#include <stdbool.h>

#define  UART_1                1    // driver internal number associated to UART1
#define  UART_2                0    // driver internal number associated to UART0

/* The CC2538 has two UART hardware interfaces. Define the one(s) in use. */
#if ( gPopSerialPort_c == 2)
#define gUart_PortDefault_d     UART_2
#else
#define gUart_PortDefault_d     UART_1
#endif 

/* Use hardware flow control? */

#define gUart1_EnableHWFlowControl_d     false
#define gUart2_EnableHWFlowControl_d     false

typedef uint32_t UartBaudRate_t;

#define gUARTBaudRate1200_c     ((UartBaudRate_t) 1200)
#define gUARTBaudRate2400_c     ((UartBaudRate_t) 2400)
#define gUARTBaudRate4800_c     ((UartBaudRate_t) 4800)
#define gUARTBaudRate9600_c     ((UartBaudRate_t) 9600)
#define gUARTBaudRate19200_c    ((UartBaudRate_t) 19200)
#define gUARTBaudRate38400_c    ((UartBaudRate_t) 38400)
#define gUARTBaudRate57600_c    ((UartBaudRate_t) 57600)
#define gUARTBaudRate115200_c   ((UartBaudRate_t) 115200)   /* Might not work for all clients */

#define Baudrate_1200   gUARTBaudRate1200_c
#define Baudrate_2400   gUARTBaudRate2400_c
#define Baudrate_4800   gUARTBaudRate4800_c
#define Baudrate_9600   gUARTBaudRate9600_c
#define Baudrate_19200  gUARTBaudRate19200_c
#define Baudrate_38400  gUARTBaudRate38400_c
#define Baudrate_57600  gUARTBaudRate57600_c
#define Baudrate_115200 gUARTBaudRate115200_c

/* Default baud rate. */
#define gUartDefaultBaud_c Baudrate_115200  //Baudrate_38400 default


#define BIT_TX_EN              (1<<0)
#define BIT_RX_EN              (1<<1)
#define BIT_PARITY_EN          (1<<2)
#define BIT_PARITY_EVEN        (1<<3)
#define BIT_STOP_BITS2         (1<<4)
#define BIT_RX_INT             (1<<6)
#define BIT_TX_INT             (1<<7)
#define BIT_XTIM               (1<<10)
#define BIT_MASK_TX_INT        (1<<13)
#define BIT_MASK_RX_INT        (1<<14)
#define BIT_FLOW_CNT_EN        (1<<12)
#define BIT_RTS_ACTIVE_HIGH    (1<<11)

#define UartOpenReceiver(UartNumber) UartOpenCloseTransceiver((UartNumber), BIT_RX_EN, true)
#define UartCloseReceiver(UartNumber) UartOpenCloseTransceiver((UartNumber), BIT_RX_EN, false)
#define UartOpenTransmitter(UartNumber) UartOpenCloseTransceiver((UartNumber), BIT_TX_EN, true)
#define UartCloseTransmitter(UartNumber) UartOpenCloseTransceiver((UartNumber), BIT_TX_EN, false)

/** This data type enumerates the UART API calls return values */
typedef enum {
	gUartErrNoError_c = 0,                 
	gUartErrUartAlreadyOpen_c,                 
	gUartErrUartNotOpen_c,
	gUartErrNoCallbackDefined_c,
	gUartErrReadOngoing_c,
	gUartErrWriteOngoing_c,
	gUartErrInvalidClock_c,
	gUartErrNullPointer_c,
	gUartErrInvalidNrBytes_c,
	gUartErrInvalidBaudrate_c,
	gUartErrInvalidParity_c,
	gUartErrInvalidStop_c,
	gUartErrInvalidCTS_c,
	gUartErrInvalidThreshold_c,
	gUartErrWrongUartNumber_c,
	gUartErrMax_c
} UartErr_t;

/** This data type enumerates the possible read operation status */
typedef enum {
	gUartReadStatusComplete_c = 0,
	gUartReadStatusCanceled_c ,
	gUartReadStatusError_c,
	gUartReadStatusMax_c
} UartReadStatus_t;

/** This data type enumerates the possible write operation status */
typedef enum {
	gUartWriteStatusComplete_c = 0,
	gUartWriteStatusCanceled_c,
	gUartWriteStatusMax_c
} UartWriteStatus_t;

/** This data type enumerates the possible values of UART parity modes */
typedef enum {
	gUartParityNone_c = 0,
	gUartParityEven_c,
	gUartParityOdd_c,
	gUartParityMax_c
} UartParityMode_t;

/** This data type enumerates the possible values of UART stop bits */
typedef enum {
	gUartStopBits1_c = 0,
	gUartStopBits2_c,
	gUartStopBitsMax_c
} UartStopBits_t;

/** This data type describes the configuration of the UART module */
typedef struct {
	uint32_t         UartBaudrate;
	UartParityMode_t UartParity;
	UartStopBits_t   UartStopBits;
	bool           UartFlowControlEnabled;
	bool           UartRTSActiveHigh;
} UartConfig_t;

/** This data type describes the possible errors passed to the read callback function  */
typedef struct {
	uint32_t   UartReadOverrunError:1;
	uint32_t   UartParityError:1;
	uint32_t   UartFrameError:1;
	uint32_t   UartStartBitError:1;
	uint32_t   Reserved:4;
} UartReadErrorFlags_t;

/** This data type describes the parameter passed to the read callback function */
/** Do not modify the order of the members in the structure,  because it is optimized for lower space consumption*/
typedef struct {
	UartReadStatus_t            UartStatus;
	uint16_t                    UartNumberBytesReceived;    
	UartReadErrorFlags_t        UartReadError;
} UartReadCallbackArgs_t;

/** This data type describes the parameter passed to the write callback function */
typedef struct {
	UartWriteStatus_t       UartStatus;
	uint16_t                UartNumberBytesSent;
} UartWriteCallbackArgs_t;

typedef void UartReadCbFn(UartReadCallbackArgs_t* args);
typedef void UartWriteCbFn(UartWriteCallbackArgs_t* args);

/** This data type lists the callback functions for the UART driver */
typedef struct {
	UartReadCbFn * pfUartReadCallback;
	UartWriteCbFn * pfUartWriteCallback;
} UartCallbackFunctions_t;



extern UartErr_t UartOpen(uint8_t UartNumber, uint32_t PlatformClock);
extern UartErr_t UartGetConfig(uint8_t UartNumber, UartConfig_t* pConfig);
extern UartErr_t UartSetConfig(uint8_t UartNumber, UartConfig_t* pConfig);
extern UartErr_t UartSetReceiverThreshold(uint8_t UartNumber, uint8_t Threshold);
extern UartErr_t UartSetTransmitterThreshold(uint8_t UartNumber, uint8_t Threshold);
extern UartErr_t UartSetCallbackFunctions(uint8_t UartNumber, UartCallbackFunctions_t* pConfig);
extern UartErr_t UartGetStatus(uint8_t UartNumber);
extern UartErr_t UartCancelReadData(uint8_t UartNumber);
extern UartErr_t UartCancelWriteData(uint8_t UartNumber);
extern UartErr_t UartReadData(uint8_t UartNumber, uint8_t* pBuf, uint16_t BufferSize, bool UartDirectFifoMode);
extern UartErr_t UartWriteData(uint8_t UartNumber, uint8_t* pBuf, uint16_t NumberBytes);
extern bool UartGetByteFromRxBuffer(uint8_t UartNumber, uint8_t *pDst);
extern void UartClearErrors(uint8_t UartNumber);
extern void UartOpenCloseTransceiver(uint8_t UartNumber, uint8_t Pin, bool Open);

#endif
