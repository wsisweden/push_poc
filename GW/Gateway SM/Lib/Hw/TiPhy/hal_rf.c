//*****************************************************************************
//! @file       hal_rf.c
//! @brief      CC2538 radio interface. Supports
//!             \li CC2538EM
//!             \li CC2538/CC2591EM
//!             \li CC2538/CC2592EM
//!
//! Revised     $Date: 2017-09-08 16:10:15 +0300 (pe, 08 syys 2017) $
//! Revision    $Revision: 864 $
//
//  Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
//
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//
//    Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//****************************************************************************/


/**************************************************************************//**
* @addtogroup hal_rf_api
* @{
******************************************************************************/


/******************************************************************************
* INCLUDES
*/

//#include <stdint.h>
#include <stddef.h>

#include "hw_types.h"               // Using HWREG() macro
#include "hal_defs.h"
#include "hal_int.h"
#include "hal_rf.h"
#include "mac_pib.h"

#include "hw_memmap.h"              // Peripheral base definitions
#include "hw_flash_ctrl.h"          // DIECFGx register definitions
#include "hw_ints.h"                // Interrupt definitions
#include "hw_ana_regs.h"            // Register definitions
#include "hw_rfcore_ffsm.h"         // Register definitions
#include "hw_rfcore_sfr.h"          // Register definitions
#include "hw_rfcore_xreg.h"         // Register definitions
#include "hw_sys_ctrl.h"            // Register definitions
#include "hw_gpio.h"                // Register definitions
#include "hw_ioc.h"                 // Register definitions
#include "hw_cctest.h"              // Register definitions
#include "hw_soc_adc.h"

#include "sys_ctrl.h"
#include "ioc.h"
#include "gpio.h"

/******************************************************************************
* CONSTANTS AND DEFINES
*/

// Chip revision
#define REV_A
#define CHIPREVISION

// TXPOWER values
#define CC2538_TXPOWER_7_DBM          0xFF
#define CC2538_TXPOWER_3_DBM          0xD5
#define CC2538_TXPOWER_0_DBM          0xB6
#define CC2538_TXPOWER_MIN_3_DBM      0xA1
#define CC2538_TXPOWER_MIN_9_DBM      0x72
#define CC2538_TXPOWER_MIN_15_DBM     0x42

// For CC2538/CC2591 combo
#define CC2538_CC2591_TXPOWER_20_DBM  0xE5

// For CC2538/CC2592 combo
#define CC2538_CC2592_TXPOWER_22_DBM  0xFF
#define CC2538_CC2592_TXPOWER_21_DBM  0xD5
#define CC2538_CC2592_TXPOWER_20_DBM  0xC5
#define CC2538_CC2592_TXPOWER_19_DBM  0xB0
#define CC2538_CC2592_TXPOWER_18_DBM  0xA1
#define CC2538_CC2592_TXPOWER_16_DBM  0x91
#define CC2538_CC2592_TXPOWER_15_DBM  0x88
#define CC2538_CC2592_TXPOWER_13_DBM  0x72
#define CC2538_CC2592_TXPOWER_11_DBM  0x62
#define CC2538_CC2592_TXPOWER_7_DBM   0x42
#define CC2538_CC2592_TXPOWER_4_DBM   0x24
#define CC2538_CC2592_TXPOWER_0_DBM   0x00


#define HAL_CLOCK_STABLE()  st( while ( !((HWREG(SYS_CTRL_CLOCK_STA)) & (SYS_CTRL_CLOCK_STA_XOSC_STB))) ; )
#define HAL_CPU_CLOCK_MHZ   32


/* Strobe processor instructions */
#define SKIP(s,c)           (0x00 | (((s) & 0x07) << 4) | ((c) & 0x0F)) /**< skip 's' instructions if 'c' is true.  */
#define WHILE(c)            SKIP(0,c)   /**< pend while 'c' is true (derived instruction).        */
#define WAITW(w)            (0x80 | ((w) & 0x1F))  /* wait for 'w' number of MAC timer overflows. */
#define WEVENT1             (0xB8)      /**< wait for MAC timer compare.                          */
#define WAITX               (0xBC)      /**< wait for CSPX number of MAC timer overflows.         */
#define LABEL               (0xBB)      /**< set next instruction as start of loop.               */
#define RPT(c)              (0xA0 | ((c) & 0x0F)) /**< if condition is true jump to last label.   */
#define INT                 (0xBA)      /**< assert IRQ_CSP_INT interrupt.                        */
#define INCY                (0xC1)      /**< increment CSPY.                                      */
#define INCMAXY(m)          (0xC8 | ((m) & 0x07)) /**< increment CSPY but not above maximum value of 'm'.*/
#define DECX                (0xC3)      /**< decrement CSPX.                                      */
#define DECY                (0xC4)      /**< decrement CSPY.                                      */
#define DECZ                (0xC5)      /**< decrement CSPZ.                                      */
#define RANDXY              (0xBD)      /**< load the lower CSPY bits of CSPX with random value.  */

/* Strobe processor command instructions */
#define SSTOP               (0xD2)      /**< stop program execution.            */
#define SNOP                (0xD0)      /**< no operation.                      */
#define STXCAL              (0xDC)      /**< enable and calibrate frequency synthesizer for TX. */
#define SRXON               (0xD3)      /**< turn on receiver.                  */
#define STXON               (0xD9)      /**< transmit after calibration.        */
#define STXONCCA            (0xDA)      /**< transmit after calibration if CCA indicates clear channel. */
#define SRFOFF              (0xDF)      /**< turn off RX/TX.                    */
#define SFLUSHRX            (0xDD)      /**< flush receive FIFO.                */
#define SFLUSHTX            (0xDE)      /**< flush transmit FIFO.               */
#define SACK                (0xD6)      /**< send ACK frame.                    */
#define SACKPEND            (0xD7)      /**< send ACK frame with pending bit set.*/

/* Immediate strobe commands */
#define HALRF_CMD_ISSTART   0xE1
#define HALRF_CMD_ISSTOP    0xE2
#define HALRF_CMD_ISCLEAR   0xFF
#define HALRF_CMD_ISTXCAL   0xEC
#define HALRF_CMD_ISRXON    0xE3
#define HALRF_CMD_ISTXON    0xE9
#define HALRF_CMD_ISTXONCCA 0xEA
#define HALRF_CMD_ISRFOFF   0xEF
#define HALRF_CMD_ISFLUSHRX 0xED
#define HALRF_CMD_ISFLUSHTX 0xEE
#define HALRF_CMD_ISACK     0xE6
#define HALRF_CMD_ISACKPEND 0xE7
#define HALRF_CMD_ISNACK    0xE8

// Selected strobes
#define RFST                HWREG(RFCORE_SFR_RFST)
#define ISRXON()            st(RFST = HALRF_CMD_ISRXON;)
#define ISTXON()            st(RFST = HALRF_CMD_ISTXON;)
#define ISTXONCCA()         st(RFST = HALRF_CMD_ISTXONCCA;)
#define ISRFOFF()           st(RFST = HALRF_CMD_ISRFOFF;)
#define ISFLUSHRX()         st(RFST = HALRF_CMD_ISFLUSHRX;)
#define ISFLUSHTX()         st(RFST = HALRF_CMD_ISFLUSHTX;)
#define CSP_STOP_AND_CLEAR_PROGRAM() st(RFST = HALRF_CMD_ISSTOP; RFST = HALRF_CMD_ISCLEAR; )
#define CSP_START_PROGRAM()          st(RFST = HALRF_CMD_ISSTART; )

/* conditions for use with instructions SKIP and RPT */
#define C_CCA_IS_VALID      0x00
#define C_SFD_IS_ACTIVE     0x01
#define C_CPU_CTRL_IS_ON    0x02
#define C_END_INSTR_MEM     0x03
#define C_CSPX_IS_ZERO      0x04
#define C_CSPY_IS_ZERO      0x05
#define C_CSPZ_IS_ZERO      0x06
#define C_RSSI_IS_VALID     0x07

/* negated conditions for use with instructions SKIP and RPT */
#define C_NEGATE(c)         ((c) | 0x08)
#define C_CCA_IS_INVALID    C_NEGATE(C_CCA_IS_VALID)
#define C_SFD_IS_INACTIVE   C_NEGATE(C_SFD_IS_ACTIVE)
#define C_CPU_CTRL_IS_OFF   C_NEGATE(C_CPU_CTRL_IS_ON)
#define C_NOT_END_INSTR_MEM C_NEGATE(C_END_INSTR_MEM)
#define C_CSPX_IS_NON_ZERO  C_NEGATE(C_CSPX_IS_ZERO)
#define C_CSPY_IS_NON_ZERO  C_NEGATE(C_CSPY_IS_ZERO)
#define C_CSPZ_IS_NON_ZERO  C_NEGATE(C_CSPZ_IS_ZERO)
#define C_RSSI_IS_INVALID   C_NEGATE(C_RSSI_IS_VALID)

/* CSPZ return values from CSP program */
#define CSPZ_CODE_TX_DONE           0
#define CSPZ_CODE_CHANNEL_BUSY      1
#define CSPZ_CODE_TX_ACK_TIME_OUT   2

#define CSP_WEVENT_CLEAR_TRIGGER()  st( HWREG(RFCORE_SFR_MTIRQF) = ~TIMER2_COMPARE1F; )

/* T2CTRL */
#define LATCH_MODE          BV(3)
#define TIMER2_STATE        BV(2)
#define TIMER2_SYNC         BV(1)
#define TIMER2_RUN          BV(0)

/* T2IRQF */
#define TIMER2_OVF_COMPARE2F BV(5)
#define TIMER2_OVF_COMPARE1F BV(4)
#define TIMER2_OVF_PERF     BV(3)
#define TIMER2_COMPARE2F    BV(2)
#define TIMER2_COMPARE1F    BV(1)
#define TIMER2_PERF         BV(0)

/* T2IRQM */
#define TIMER2_OVF_COMPARE2M BV(5)
#define TIMER2_OVF_COMPARE1M BV(4)
#define TIMER2_OVF_PERM     BV(3)
#define TIMER2_COMPARE2M    BV(2)
#define TIMER2_COMPARE1M    BV(1)
#define TIMER2_PERM         BV(0)

#define T2M_OVF_BITS        (BV(6) | BV(5) | BV(4))
#define T2M_BITS            (BV(2) | BV(1) | BV(0))

#define T2M_OVFSEL(x)       ((x) << 4)
#define T2M_SEL(x)          (x)

#define T2M_T2OVF           T2M_OVFSEL(0UL)
#define T2M_T2OVF_CAP       T2M_OVFSEL(1UL)
#define T2M_T2OVF_PER       T2M_OVFSEL(2UL)
#define T2M_T2OVF_CMP1      T2M_OVFSEL(3UL)
#define T2M_T2OVF_CMP2      T2M_OVFSEL(4UL)

#define T2M_T2TIM           T2M_SEL(0UL)
#define T2M_T2_CAP          T2M_SEL(1UL)
#define T2M_T2_PER          T2M_SEL(2UL)
#define T2M_T2_CMP1         T2M_SEL(3UL)
#define T2M_T2_CMP2         T2M_SEL(4UL)

/* RFIRQF0 */
#define IRQ_SFD             BV(1)
#define IRQ_FIFOP           BV(2)

/* RFIRQM1 */
#define IM_TXACKDONE        BV(0)
#define IM_TXDONE           BV(1)
#define IM_CSP_MANINT       BV(3)
#define IM_CSP_STOP         BV(4)

/* RFIRQF1 */
#define IRQ_TXACKDONE       BV(0)
#define IRQ_TXDONE          BV(1)
#define IRQ_CSP_MANINT      BV(3)
#define IRQ_CSP_STOP        BV(4)

#if 0
# define MAC_ASSERT(condition_)  while (!(condition_));
#else
# define MAC_ASSERT(condition_)
#endif

/** Maximum value for energy detect */
#define MAC_SPEC_ED_MAX               0xFF

/* The number of symbols forming a basic CSMA-CA time period */
#define MAC_A_UNIT_BACKOFF_PERIOD     20

/* Microseconds in one symbol */
#define MAC_SPEC_USECS_PER_SYMBOL     16

/* Microseconds in one backoff period */
#define MAC_SPEC_USECS_PER_BACKOFF    (MAC_SPEC_USECS_PER_SYMBOL * MAC_A_UNIT_BACKOFF_PERIOD)

/* bit value used to form values of macTxActive */
#define MAC_TX_ACTIVE_PHYSICALLY_BV   0x80

/* state values for macTxActive; note zero is reserved for inactive state */
#define MAC_TX_ACTIVE_NO_ACTIVITY     0x00 /* zero reserved for boolean use, e.g. !macTxActive */
#define MAC_TX_ACTIVE_INITIALIZE      0x01
#define MAC_TX_ACTIVE_QUEUED          0x02
#define MAC_TX_ACTIVE_GO              (0x03 | MAC_TX_ACTIVE_PHYSICALLY_BV)
#define MAC_TX_ACTIVE_CHANNEL_BUSY    0x04
#define MAC_TX_ACTIVE_DONE            (0x05 | MAC_TX_ACTIVE_PHYSICALLY_BV)
#define MAC_TX_ACTIVE_LISTEN_FOR_ACK  (0x06 | MAC_TX_ACTIVE_PHYSICALLY_BV)
#define MAC_TX_ACTIVE_POST_ACK        (0x07 | MAC_TX_ACTIVE_PHYSICALLY_BV)

#define MAC_TX_IS_PHYSICALLY_ACTIVE() (macTxActive & MAC_TX_ACTIVE_PHYSICALLY_BV)

/* bit value used to form values of macRxActive */
#define MAC_RX_ACTIVE_PHYSICAL_BV       0x80

#define MAC_RX_ACTIVE_NO_ACTIVITY       0x00  /* zero reserved for boolean use, e.g. !macRxActive */
#define MAC_RX_ACTIVE_STARTED           (0x01 | MAC_RX_ACTIVE_PHYSICAL_BV)
#define MAC_RX_ACTIVE_DONE              0x02

/* Enable interrupts */
#define HAL_ENABLE_INTERRUPTS()       IntMasterEnable()

/* Disable interrupts */
#define HAL_DISABLE_INTERRUPTS()      IntMasterDisable()

#if 0
static bool halIntsAreEnabled(void)
{
  bool status = !IntMasterDisable();
  if (status)
  {
    IntMasterEnable();
  }
  return status;
}

#define HAL_INTERRUPTS_ARE_ENABLED()  halIntsAreEnabled()
#endif

typedef unsigned char halIntState_t;
#define HAL_ENTER_CRITICAL_SECTION(x)  \
  do { (x) = !IntMasterDisable(); } while (0)

#define HAL_EXIT_CRITICAL_SECTION(x) \
  do { if (x) { (void) IntMasterEnable(); } } while (0)

/* Hal Critical statement definition */
#define HAL_CRITICAL_STATEMENT(x)       st( halIntState_t s; HAL_ENTER_CRITICAL_SECTION(s); x; HAL_EXIT_CRITICAL_SECTION(s); )

#define MAC_MCU_WRITE_RFIRQF0(x)      HAL_CRITICAL_STATEMENT( IntPendClear(INT_RFCORERTX); HWREG(RFCORE_SFR_RFIRQF0) = x; )
#define MAC_MCU_WRITE_RFIRQF1(x)      HAL_CRITICAL_STATEMENT( IntPendClear(INT_RFCORERTX); HWREG(RFCORE_SFR_RFIRQF1) = x; )
#define MAC_MCU_OR_RFIRQM0(x)         st( HWREG(RFCORE_XREG_RFIRQM0) |= x; )  /* compiler must use atomic ORL instruction */
#define MAC_MCU_AND_RFIRQM0(x)        st( HWREG(RFCORE_XREG_RFIRQM0) &= x; )  /* compiler must use atomic ANL instruction */
#define MAC_MCU_OR_RFIRQM1(x)         st( HWREG(RFCORE_XREG_RFIRQM1) |= x; )  /* compiler must use atomic ORL instruction */
#define MAC_MCU_AND_RFIRQM1(x)        st( HWREG(RFCORE_XREG_RFIRQM1) &= x; )  /* compiler must use atomic ANL instruction */

#define MAC_MCU_FIFOP_ENABLE_INTERRUPT()        MAC_MCU_OR_RFIRQM0(IM_FIFOP)
#define MAC_MCU_FIFOP_DISABLE_INTERRUPT()       MAC_MCU_AND_RFIRQM0((IM_FIFOP ^ 0xFF))
#define MAC_MCU_FIFOP_CLEAR_INTERRUPT()         MAC_MCU_WRITE_RFIRQF0((IRQ_FIFOP ^ 0xFF))

#define MAC_MCU_TXACKDONE_ENABLE_INTERRUPT()    MAC_MCU_OR_RFIRQM1(IM_TXACKDONE)
#define MAC_MCU_TXACKDONE_DISABLE_INTERRUPT()   MAC_MCU_AND_RFIRQM1(~IM_TXACKDONE)
#define MAC_MCU_TXACKDONE_CLEAR_INTERRUPT()     MAC_MCU_WRITE_RFIRQF1(~IRQ_TXACKDONE)

#define MAC_MCU_CSP_STOP_ENABLE_INTERRUPT()     MAC_MCU_OR_RFIRQM1(IM_CSP_STOP)
#define MAC_MCU_CSP_STOP_DISABLE_INTERRUPT()    MAC_MCU_AND_RFIRQM1((IM_CSP_STOP ^ 0xFF))
#define MAC_MCU_CSP_STOP_CLEAR_INTERRUPT()      MAC_MCU_WRITE_RFIRQF1((IRQ_CSP_STOP ^ 0xFF))
#define MAC_MCU_CSP_STOP_INTERRUPT_IS_ENABLED() (HWREG(RFCORE_XREG_RFIRQM1) & IM_CSP_STOP)

#define MAC_MCU_CSP_INT_ENABLE_INTERRUPT()      MAC_MCU_OR_RFIRQM1(IM_CSP_MANINT)
#define MAC_MCU_CSP_INT_DISABLE_INTERRUPT()     MAC_MCU_AND_RFIRQM1((IM_CSP_MANINT ^ 0xFF))
#define MAC_MCU_CSP_INT_CLEAR_INTERRUPT()       MAC_MCU_WRITE_RFIRQF1((IRQ_CSP_MANINT ^ 0xFF))
#define MAC_MCU_CSP_INT_INTERRUPT_IS_ENABLED()  (HWREG(RFCORE_XREG_RFIRQM1) & IM_CSP_MANINT)

#define MAC_MCU_T2_ACCESS_OVF_COUNT_VALUE()     st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2OVF; )
#define MAC_MCU_T2_ACCESS_OVF_CAPTURE_VALUE()   st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2OVF_CAP; )
#define MAC_MCU_T2_ACCESS_OVF_PERIOD_VALUE()    st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2OVF_PER; )
#define MAC_MCU_T2_ACCESS_OVF_CMP1_VALUE()      st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2OVF_CMP1; )
#define MAC_MCU_T2_ACCESS_OVF_CMP2_VALUE()      st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2OVF_CMP2; )

#define MAC_MCU_T2_ACCESS_COUNT_VALUE()         st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2TIM; )
#define MAC_MCU_T2_ACCESS_CAPTURE_VALUE()       st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2_CAP; )
#define MAC_MCU_T2_ACCESS_PERIOD_VALUE()        st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2_PER; )
#define MAC_MCU_T2_ACCESS_CMP1_VALUE()          st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2_CMP1; )
#define MAC_MCU_T2_ACCESS_CMP2_VALUE()          st( HWREG(RFCORE_SFR_MTMSEL) = T2M_T2_CMP2; )

#define MAC_MCU_CONFIG_CSP_EVENT1()             st( HWREG(RFCORE_SFR_MTCSPCFG) = 1UL; )


#define MAC_RADIO_SET_RX_THRESHOLD(x)                 st( HWREG(RFCORE_XREG_FIFOPCTRL) = ((x)-1); )
#define MAC_RADIO_RX_IS_AT_THRESHOLD()                (HWREG(RFCORE_XREG_FSMSTAT1) & FIFOP)
#define MAC_RADIO_ENABLE_RX_THRESHOLD_INTERRUPT()     MAC_MCU_FIFOP_ENABLE_INTERRUPT()
#define MAC_RADIO_DISABLE_RX_THRESHOLD_INTERRUPT()    MAC_MCU_FIFOP_DISABLE_INTERRUPT()
#define MAC_RADIO_CLEAR_RX_THRESHOLD_INTERRUPT_FLAG() MAC_MCU_FIFOP_CLEAR_INTERRUPT()

#define MAC_RADIO_TIMER_TICKS_PER_USEC()              HAL_CPU_CLOCK_MHZ /* never fractional */
#define MAC_RADIO_TIMER_TICKS_PER_BACKOFF()           (HAL_CPU_CLOCK_MHZ * MAC_SPEC_USECS_PER_BACKOFF)
#define MAC_RADIO_TIMER_TICKS_PER_SYMBOL()            (HAL_CPU_CLOCK_MHZ * MAC_SPEC_USECS_PER_SYMBOL)

/*
 * Since T2MOVF is 24 bit so the value can't exceed 24 bit.
 * so choosing 0xFF0000 a  value which is a 4 byte aligned
 */
#define MAC_BACKOFF_TIMER_DEFAULT_NONBEACON_ROLLOVER  0xFF0000

#define MAC_RADIO_BACKOFF_COMPARE_CLEAR_INTERRUPT()    st( HWREG(RFCORE_SFR_MTIRQF)  = ~TIMER2_OVF_COMPARE1F; )
#define MAC_RADIO_BACKOFF_COMPARE_ENABLE_INTERRUPT()   st( HWREG(RFCORE_SFR_MTIRQM) |= TIMER2_OVF_COMPARE1M; )
#define MAC_RADIO_BACKOFF_COMPARE_DISABLE_INTERRUPT()  st( HWREG(RFCORE_SFR_MTIRQM) &= ~TIMER2_OVF_COMPARE1M; )

#define MAC_RADIO_BACKOFF_PERIOD_CLEAR_INTERRUPT()    st( HWREG(RFCORE_SFR_MTIRQF)  = ~TIMER2_OVF_PERF; )
#define MAC_RADIO_BACKOFF_PERIOD_ENABLE_INTERRUPT()   st( HWREG(RFCORE_SFR_MTIRQM) |= TIMER2_OVF_PERM; )
#define MAC_RADIO_BACKOFF_PERIOD_DISABLE_INTERRUPT()  st( HWREG(RFCORE_SFR_MTIRQM) &= ~TIMER2_OVF_PERM; )


#define MAC_RX_IS_PHYSICALLY_ACTIVE()   (macRxActive & MAC_RX_ACTIVE_PHYSICAL_BV)

#define MAC_SUCCESS                 0x00  /* Operation successful */
#define MAC_CHANNEL_ACCESS_FAILURE  0xE1  /* The operation or data request failed because of
                                             activity on the channel */

typedef struct {
  int8_t rssiOffset;      /**< RSSI offset from HW value to correct dBm value.*/
  int8_t recvSensitivity; /**< Receiver sensitivity (dBm) */
  int8_t recvSaturation;  /**< Receiver saturation point (dBm) */
} HalRfModeSpecs;

typedef struct {
  HalRfModeSpecs gainModes[2];
} HalRfModuleSpecs;


/**
 * Table with values from the data sheets of the corresponding modules.
 */

static const HalRfModuleSpecs halRfModules[] = {
  /* HAL_RF_CC2538EM (values from CC2538 data sheet) */
  {
    {
    /* RSSI offset (dB)   Receiver sensitivity (dBm)  Receiver saturation (dBm) */
      {73,                -97,                        10},  /* LGM */
      {73,                -97,                        10}   /* HGM */
    }
  },

  /* HAL_RF_CC2538_CC2591EM */
  {
    {
    /* RSSI offset (dB)   Receiver sensitivity (dBm)  Receiver saturation (dBm) */
      {69,                -90,                        10},  /* LGM */
      {79,                -99,                        10}   /* HGM */
    }
  },

  /* HAL_RF_CC2538_CC2592EM (values from swra447) */
  {
    {
    /* RSSI offset (dB)   Receiver sensitivity (dBm)  Receiver saturation (dBm) */
      {81,                -99,                        -2},  /* LGM */
      {85,                -101,                       -3}   /* HGM */
    }
  }
};

/**
 *  Threshold above receiver sensitivity for minimum energy detect in dBm.
 *
 *  IEEE 802.15.4:2011 8.2.5 Receiver ED
 *  The minimum ED value (zero) shall indicate received power less than 10 dB
 *  above the maximum allowed receiver sensitivity for the PHY.
 */

#define MAC_SPEC_ED_MIN_DBM_ABOVE_RECV_SENSITIVITY    10

/******************************************************************************
* LOCAL VARIABLES
*/

static void (*pfISR)(void);

static halRfGain halRfGainMode = HAL_RF_GAIN_LOW;
static halRfModule halRfEmModule = HAL_RF_CC2538EM;

static bool updateRolloverflag = FALSE;
/*
 *  This number is used to calculate the precision count for OSAL timer update. In Beacon mode,
 *  the overflow count may be initialized to zero or to a constant. The "skip" in overflow count
 *  needs to be accounted for in this variable.
 */
static uint32 accumulatedOverflowCount = 0;

uint8 macRxActive = MAC_RX_ACTIVE_NO_ACTIVITY;
uint8 macRxOnFlag = 0;
uint8 macRxEnableFlags = 0;

uint8 macTxActive = MAC_TX_ACTIVE_NO_ACTIVITY;
uint8 macTxCsmaBackoffDelay;
uint8 macTxGpInterframeDelay;

static uint32 backoffTimerRollover;

/**
 * Backoff exponent, which is related to how many backoff periods a device shall
 * wait before attempting to assess a channel. In unslotted systems BE shall be
 * initialized to the value of macMinBE.
 */
uint8 macTxBe;

/**
 * number of times the CSMA-CA algorithm was required to back off while
 * attempting the current transmission; this value shall be initialized to zero
 * before each new transmission attempt.
 */
static uint8 nb;

volatile uint32_t halRfSending = FALSE;

/******************************************************************************
* FUNCTION PROTOTYPES
*/

static void halRfIsr(void);
static void halRfErrorIsr(void);
static void halRfMacTimer2Isr(void);
static void halRfPaLnaInit(void);
static void halInitRandom(void);

static void halRfBackoffTimerInit(void);
static void halRfRadioTimerWakeUp(void);

static void halRfTxFrameCsma(void);
static void halRfBackoffTimerPeriodIsr(void);
static void halRfTxCompleteCallback(uint8 status);
static void halRfRxOff(void);
static void halRfRxOn(void);
static void halRfRxOffRequest(void);
static void halRftxComplete(uint8 status);
static void halRfPrepForTxProgram(void);
static void halRftxCsmaPrep(void);
static void halRfWeventSetTriggerNow(void);
static void halRftxCsmaGo(void);
static void halRfTxChannelBusyCallback(void);
static void halRfTxDoneCallback(void);
static void halRfCspTxStopIsr(void);
static void halRfCspTxIntIsr(void);
static void halRfOverflowSetPeriod(uint32 count);
static uint32 halRfOverflowCount(void);
static void halRfOverflowSetCount(uint32 count);
static void halRfBackoffTimerSetRollover(uint32 rolloverBackoff);
static uint32 halRfGetBackOffTimerRollover(void);
static void halRfMcuAccumulatedOverFlow(void);


/******************************************************************************
* GLOBAL FUNCTIONS
*/
/**************************************************************************//**
* @brief    Power up, sets default tuning settings, enables autoack.
*
* @return   Returns SUCCESS (for interface compatibility)
******************************************************************************/
unsigned char halRfInit(void)
{
  halIntState_t  s;
  //
  // Some of the below settings are indeed the reset value.
  //

  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_RFC);

  // Enable RF core clocks in active mode (not necessary on CC2538 PG1.0)
  HWREG(SYS_CTRL_RCGCRFC) = 1;

  // Enable auto ack and auto crc
#if 0
  HWREG(RFCORE_XREG_FRMCTRL0) =
    (
      HWREG(RFCORE_XREG_FRMCTRL0)
      | (RFCORE_XREG_FRMCTRL0_AUTOACK | RFCORE_XREG_FRMCTRL0_AUTOCRC)
    );
#else
  HWREG(RFCORE_XREG_FRMCTRL0) =
    (
      HWREG(RFCORE_XREG_FRMCTRL0)
      | (RFCORE_XREG_FRMCTRL0_AUTOCRC)
    );
#endif


#if 0
  // Enable frame filtering and set maximum frame version number
  HWREG(RFCORE_XREG_FRMFILT0) =
    RFCORE_XREG_FRMFILT0_FRAME_FILTER_EN
    || (3U << RFCORE_XREG_FRMFILT0_MAX_FRAME_VERSION_S);

#else
  // Disable frame filtering and set maximum frame version number
  HWREG(RFCORE_XREG_FRMFILT0) =
    3U << RFCORE_XREG_FRMFILT0_MAX_FRAME_VERSION_S;
#endif

  HWREG(RFCORE_XREG_SRCMATCH) = 0x00; // Disable source address matching

  // Recommended values (described in user manual 23.15 Register Settings Update)
  HWREG(RFCORE_XREG_AGCCTRL1) = 0x15;
  HWREG(RFCORE_XREG_TXFILTCFG)= 0x09;
  HWREG(ANA_REGS_O_IVCTRL)    = 0x0B;
  HWREG(RFCORE_XREG_FSCAL1)   = 0x01;

  HWREG(RFCORE_XREG_FSCTRL)   = 0x5A;
  HWREG(RFCORE_XREG_FRMCTRL1) = 0x00; // STXON does not affect RXENABLE[6]
  HWREG(RFCORE_XREG_MDMTEST1) = 2U << RFCORE_XREG_MDMTEST0_DC_WIN_SIZE_S;
  
  HWREG(RFCORE_XREG_CCACTRL0) = 0x00; // Set CCA threshold

  /*----------------------------------------------------------------------------
   *  Initialize MAC timer.
   */
 
  /* set timer rollover */
  HAL_ENTER_CRITICAL_SECTION(s);
  MAC_MCU_T2_ACCESS_PERIOD_VALUE();
  HWREG(RFCORE_SFR_MTM0) = MAC_RADIO_TIMER_TICKS_PER_BACKOFF() & 0xFFUL;
  HWREG(RFCORE_SFR_MTM1) = MAC_RADIO_TIMER_TICKS_PER_BACKOFF() >> 8UL;
  HAL_EXIT_CRITICAL_SECTION(s);
 
  /* start timer */
  halRfRadioTimerWakeUp();

  /* Enable latch mode and T2 SYNC start. OSAL timer is based on MAC timer. 
   * The SYNC start msut be on when POWER_SAVING is on for this design to work.
   */
  HWREG(RFCORE_SFR_MTCTRL) |= (LATCH_MODE | TIMER2_SYNC);
  
  IntRegister(INT_MACTIMR, &halRfMacTimer2Isr);

  // Set MAC timer interrupt priority to same as the RF interrupt
  IntPrioritySet(INT_MACTIMR, 0);

  /* enable timer interrupts */
  IntEnable(INT_MACTIMR);

 /*-----------------------------------------------------------------------------
  *  Initialize random seed value.
  */

  halInitRandom();
  
  macRxActive = MAC_RX_ACTIVE_NO_ACTIVITY;

  if(halRfEmModule != HAL_RF_CC2538EM)
  {
      // Configure PA/LNA
      halRfPaLnaInit();
  }

  // Set RF interrupt priority to maximum
  IntPrioritySet(INT_RFCORERTX, 0);
  IntPrioritySet(INT_RFCOREERR, 0);

  // Register halRfIsr() as RX interrupt function
  IntRegister(INT_RFCORERTX, &halRfIsr);
  IntRegister(INT_RFCOREERR, &halRfErrorIsr);

  // Enable RX interrupt
  halRfEnableRxInterrupt();

  // Clearing TX done interrupt
  HWREG(RFCORE_SFR_RFIRQF1) &= ~(
    RFCORE_SFR_RFIRQF1_CSP_WAIT
    | RFCORE_SFR_RFIRQF1_CSP_STOP
    | RFCORE_SFR_RFIRQF1_RFIDLE
    | RFCORE_SFR_RFIRQF1_TXDONE
    | RFCORE_SFR_RFIRQF1_TXACKDONE
  );

  HWREG(RFCORE_SFR_RFERRF) = ~RFCORE_SFR_RFERRF_RXOVERF;
  
  // enable general RF interrupts
  IntEnable(INT_RFCORERTX);
  IntEnable(INT_RFCOREERR);

  halRfBackoffTimerInit();

  halRfFlushRxBuff();
  
  return SUCCESS;
}


/**************************************************************************//**
* @brief    Function returns the chip ID.
*
* @return   Chip ID
******************************************************************************/
unsigned short halRfGetChipId(void)
{
  return HAL_RF_CHIP_ID_CC2538;
}


/**************************************************************************//**
* @brief    Function returns chip version.
*
* @return   Chip version (0 for CC2538 PG1, 2 for CC2538 PG2)
******************************************************************************/
unsigned char halRfGetChipVer(void)
{
  return ((HWREG(FLASH_CTRL_DIECFG2) >> 12) & 0x0F);
}


/**************************************************************************//**
* @brief   Function returns RSSI offset.
*
* @return  RSSI offset
******************************************************************************/
int8_t halRfGetRssiOffset(void)
{
  return halRfModules[halRfEmModule].gainModes[halRfGainMode].rssiOffset;
}


/**************************************************************************//**
* @brief    Set RF channel in the 2.4GHz band. The Channel must be in the
*           range 11-26 (inclusive). 11=2405 MHz, channel spacing 5 MHz.
*
* @param    channel         Channel to set [11,26]
*
* @return   None
******************************************************************************/
void halRfSetChannel(unsigned char channel)
{
  HWREG(RFCORE_XREG_FREQCTRL) =
    (MIN_CHANNEL + (channel - MIN_CHANNEL) * CHANNEL_SPACING);
}


/**************************************************************************//**
* @brief    Function sets the device's short address
*
* @param    shortAddr       Chip short address
*
* @return   None
******************************************************************************/
void halRfSetShortAddr(unsigned short shortAddr)
{
  /*
   *  Short address is currently not used because filtering is disabled!
   */

  pMacPib->shortAddress = shortAddr;

  HWREG(RFCORE_FFSM_SHORT_ADDR0) = LO_UINT16(shortAddr);
  HWREG(RFCORE_FFSM_SHORT_ADDR1) = HI_UINT16(shortAddr);
}


/**************************************************************************//**
* @brief    Function sets the device's PAN ID.
*
* @param    panIdchannel    PAN ID to assign
*
* @return   None
******************************************************************************/
void halRfSetPanId(unsigned short panId)
{
  /*
   *  PAN is currently not used because filtering is disabled!
   */

  pMacPib->panId = panId;

  HWREG(RFCORE_FFSM_PAN_ID0) = LO_UINT16(panId);
  HWREG(RFCORE_FFSM_PAN_ID1) = HI_UINT16(panId);
}


/**************************************************************************//**
* @brief    Function sets the devices's TX power
*
* @param    power       Power level (dBm)
*
* @return   SUCCSES or FAILED
******************************************************************************/
unsigned char halRfSetTxPower(int8_t power)
{
  unsigned char n;
  char          retVal;
  
  retVal = SUCCESS;

  if(halRfEmModule == HAL_RF_CC2538EM)
  {
    switch(power)
    {
      case -15:
      case -14:
      case -13:
      case -12:
      case -11:
        n = CC2538_TXPOWER_MIN_15_DBM;
        break;
        
      case -10:
      case -9:
      case -8:
      case -7:
      case -6:
      case -5:
        n = CC2538_TXPOWER_MIN_9_DBM;
        break;
        
      case -4:
      case -3:
      case -2:
        n = CC2538_TXPOWER_MIN_3_DBM;
        break;
        
      case -1:
      case 0:
      case 1:
        n = CC2538_TXPOWER_0_DBM;
        break;
        
      case 2:
      case 3:
      case 4:
      case 5:
        n = CC2538_TXPOWER_3_DBM;
        break;

      case 6:
      case 7:
        n = CC2538_TXPOWER_7_DBM;
        break;
                
      default:
        if (power > 7) {
          n = CC2538_TXPOWER_7_DBM;
        } else {
          n = CC2538_TXPOWER_MIN_15_DBM;
        }
        retVal = FAILED;
    }
  }
  else if(halRfEmModule == HAL_RF_CC2538_CC2591EM)
  {
    if(power != 20)
    {
      retVal = FAILED;
    }
    n = CC2538_CC2591_TXPOWER_20_DBM;

  }
  else if(halRfEmModule == HAL_RF_CC2538_CC2592EM)
  {
    switch(power)
    {
      case 0:
      case 1:
        n = CC2538_CC2592_TXPOWER_0_DBM;
        break;
       
      case 2:
      case 3:
      case 4:
      case 5:
        n = CC2538_CC2592_TXPOWER_4_DBM;
        break;
        
      case 6:
      case 7:
      case 8:
      case 9:
        n = CC2538_CC2592_TXPOWER_7_DBM;
        break;

      case 10:
      case 11:
      case 12:
        n = CC2538_CC2592_TXPOWER_11_DBM;
        break;
        
      case 13:
      case 14:
        n = CC2538_CC2592_TXPOWER_13_DBM;
        break;
        
      case 15:
        n = CC2538_CC2592_TXPOWER_15_DBM;
        break;

      case 16:
      case 17:
        n = CC2538_CC2592_TXPOWER_16_DBM;
        break;

      case 18:
        n = CC2538_CC2592_TXPOWER_18_DBM;
        break;          
        
      case 19:
        n = CC2538_CC2592_TXPOWER_19_DBM;
        break;

      case 20:
        n = CC2538_CC2592_TXPOWER_20_DBM;
        break;

      case 21:
        n = CC2538_CC2592_TXPOWER_21_DBM;
        break;       
        
      case 22:
        n = CC2538_CC2592_TXPOWER_22_DBM;
        break;

      default:
        if (power > 22) {
          n = CC2538_CC2592_TXPOWER_22_DBM;
        } else {
          n = CC2538_CC2592_TXPOWER_0_DBM;
        }
        retVal = FAILED;
    }
  }
  else
  {
    retVal = FAILED;
  }

  if (retVal == SUCCESS)
  {
    // Set TX power
    HWREG(RFCORE_XREG_TXPOWER) = n;
  }

  return retVal;
}


/**************************************************************************//**
* @brief    Set module. 
*
* @param    uint8   emModule
*
* @details  Enables to change between CC2538EM / CC2520-CC2592EM support
*           runtime. halRfInit must be called after running this function.
*
* @return   none
******************************************************************************/
unsigned char halRfSetModule(halRfModule emModule)
{
  switch(emModule)
  {
    case HAL_RF_CC2538EM:
    case HAL_RF_CC2538_CC2591EM:
    case HAL_RF_CC2538_CC2592EM:
      halRfEmModule = emModule;
      break;

    default:
      return FAILED;
  }

  return SUCCESS;
}


/**************************************************************************//**
* @brief    Function sets the gain mode. Only applicable for units with
*           CC2590/91. This function assumes that CC2538 GPIO pins have been
*           configured as GPIO output, for example, by using halRfInit();
*
* @param    gainMode    Gain mode
*
* @return   None
******************************************************************************/
void halRfSetGain(halRfGain gainMode)
{
  if (gainMode == HAL_RF_GAIN_LOW)
  {
    // Setting low gain mode (HGM pin low)
    GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_0, 0);
  }
  else
  {
    // Setting high gain mode (HGM pin high)
    GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_0, GPIO_PIN_0);
  }

  halRfGainMode = gainMode;
}


/**************************************************************************//**
* @brief    Function writes \e length bytes to the TX FIFO. 
*
* @param    pData       Pointer to source buffer
* @param    length      Number of bytes to write
*
* @details  The function flushes the TX FIFO before writing.
*
* @return   None
******************************************************************************/
void halRfWriteTxBuf(unsigned char* pData, unsigned char length)
{
  // Making sure that the TX FIFO is empty.
  ISFLUSHTX();

  // Clearing TX done interrupt
  HWREG(RFCORE_SFR_RFIRQF1) &= ~RFCORE_SFR_RFIRQF1_TXDONE;

  // Insert data
  halRfAppendTxBuf(pData, length);
}


/**************************************************************************//**
* @brief    Function appends \e length bytes to the TX FIFO, i.e. it does not
*           flush the TX FIFO before writing.
*
* @param    pData       Pointer to source buffer
* @param    length      Number of bytes to write
*
* @return   None
******************************************************************************/
void halRfAppendTxBuf(unsigned char* pData, unsigned char length)
{
  unsigned char i;

  // Insert data
  for(i = 0; i < length; i++)
  {
    HWREG(RFCORE_SFR_RFDATA) = pData[i];
  }
}


/**************************************************************************//**
* @brief    Function reads \e length bytes from the RX FIFO.
*
* @param    pData       Pointer to destination buffer
* @param    length      Number of bytes to read
*
* @return   None
******************************************************************************/
void halRfReadRxBuf(unsigned char* pData, unsigned char length)
{
  // Read data
  while (length > 0)
  {
    *pData++ = HWREG(RFCORE_SFR_RFDATA);
    length--;
  }
}


/**************************************************************************//**
* @brief    Transmit frame. Function returns when frame is sent.
*
* @return   SUCCESS or FAIL
******************************************************************************/
unsigned char halRfTransmit(void)
{
#if 0
  // Sending
  ISTXON();

  // Waiting for transmission to finish
  while(!(HWREG(RFCORE_SFR_RFIRQF1) & RFCORE_SFR_RFIRQF1_TXDONE) );

  // Clear TXDONE interrupt flag
  HWREG(RFCORE_SFR_RFIRQF1) = HWREG(RFCORE_SFR_RFIRQF1) & ~RFCORE_SFR_RFIRQF1_TXDONE;
#else

  halRfSending = TRUE;
  halRfTxFrameCsma();

  /*
   *  Wait for sending to complete. This waits until the frame has been sent or
   *  could not be sent. This may include multiple backoff wait times because 
   *  of the CSMA/CA algorithm.
   *
   *  Maybe it would be better not to wait here.. waiting will lock the whole
   *  application while the radio is sending. Instead wait in the
   *  halRfWaitTransceiverReady() function and before changing TX power or
   *  channel.
   */

  while (halRfSending == TRUE);

#endif

  return SUCCESS;
}


/**************************************************************************//**
* @brief    Turn receiver on.
*
* @return   None
******************************************************************************/
void halRfReceiveOn(void)
{
  // Make sure the RX FIFO is empty and enter RX
  ISFLUSHRX();
  ISRXON();
}


/**************************************************************************//**
* @brief    Turn receiver off.
*
* @note     May be called from an ISR.
*
* @return   None
******************************************************************************/
void halRfReceiveOff(void)
{
  // Disable RX and make sure the RX FIFO is empty.
  ISRFOFF();
  ISFLUSHRX();
}


/**************************************************************************//**
* @brief    Flush RX buffer.
*
* @return   None
******************************************************************************/
void halRfFlushRxBuff(void)
{
  // Make sure the RX FIFO is empty
  ISFLUSHRX();
}


#ifndef MRFI
/**************************************************************************//**
* @brief    Clear and disable RX interrupt.
*
* @return   None
******************************************************************************/
void halRfDisableRxInterrupt(void)
{
  // disable RXPKTDONE interrupt
  HWREG(RFCORE_XREG_RFIRQM0) &= (~BV(6));

  // disable general RF interrupts
 // IntDisable(INT_RFCORERTX);
}


/**************************************************************************//**
* @brief    Enable RX interrupt.
*
* @return   None
******************************************************************************/
void halRfEnableRxInterrupt(void)
{
  // enable RXPKTDONE interrupt
  HWREG(RFCORE_XREG_RFIRQM0) |= BV(6);
  HWREG(RFCORE_XREG_RFERRM) = BV(2);
}


/**************************************************************************//**
* @brief    Enable RX interrupt.
*
* @return   None
******************************************************************************/
void halRfEnableTxInterrupt(void)
{
  // enable RXPKTDONE interrupt
  HWREG(RFCORE_XREG_RFIRQM0) |= BV(6);
}


/**************************************************************************//**
* @brief    Configure RX interrupt.
*
* @return   None
******************************************************************************/
void halRfRxInterruptConfig(void (*pf)(void))
{
  unsigned short s;
  HAL_INT_LOCK(s);
  pfISR= pf;
  HAL_INT_UNLOCK(s);
}
#endif


/**************************************************************************//**
* @brief    Function waits until the transceiver is ready (SFD inactive).
*
* @return   None
******************************************************************************/
void halRfWaitTransceiverReady(void)
{
  while (
    HWREG(RFCORE_XREG_FSMSTAT1)
    & (RFCORE_XREG_FSMSTAT1_TX_ACTIVE | RFCORE_XREG_FSMSTAT1_SFD)
  );

  /*
   *  Maybe wait for halRfSending to be FALSE here so that we do not have
   *  to wait for that after activating sending.
   */
}


/**************************************************************************//**
* @brief    Get an 16-bit random number.
*
* @return   A random number.
******************************************************************************/
uint16_t halRfGetRandomWord(void)
{
  uint16_t random_word;

  /* clock the random generator to get a new random value */
  HWREG(SOC_ADC_ADCCON1) =
    (HWREG(SOC_ADC_ADCCON1) & ~SOC_ADC_ADCCON1_RCTRL_M)
    | (1<<SOC_ADC_ADCCON1_RCTRL_S);

  /* read random word */
  random_word  = HWREG(SOC_ADC_RNDH) << 8;
  random_word +=  HWREG(SOC_ADC_RNDL);

  /* return new randomized value from hardware */
  return(random_word);
}



/**************************************************************************//**
* @brief      Compute energy detect measurement.
*
* @param      rssiDbm   RSSI value from radio hardware.
*
* @return     Energy detect measurement in the range of 0x00-0xFF.
******************************************************************************/
uint8_t halRfComputeED(int8_t rssiDbm)
{
  uint8 ed;
  int8_t minEdRssi;
  int8_t maxEdRssi;

  /*
   *  IEEE 802.15.4:2011 8.2.5 Receiver ED
   *
   *  The minimum ED value (zero) shall indicate received power less than 10 dB
   *  above the maximum allowed receiver sensitivity for the PHY. The range of
   *  received power spanned by the ED values shall be at least 40 dB. Within
   *  this range, the mapping from the received power in decibels to ED value
   *  shall be linear with an accuracy of � 6 dB.
   */

  /*
   *  Keep RF power between minimum and maximum values.
   *  This min/max range is derived from datasheet and specification.
   */

  minEdRssi =
    halRfModules[halRfEmModule].gainModes[halRfGainMode].recvSensitivity
    + MAC_SPEC_ED_MIN_DBM_ABOVE_RECV_SENSITIVITY;

  maxEdRssi =
    halRfModules[halRfEmModule].gainModes[halRfGainMode].recvSaturation;

  if (rssiDbm < minEdRssi)
  {
    rssiDbm = minEdRssi;
  }
  else if (rssiDbm > maxEdRssi)
  {
    rssiDbm = maxEdRssi;
  }

  /*
   *  Create energy detect measurement by normalizing and scaling RF power level.
   */

  ed =
    (MAC_SPEC_ED_MAX * (rssiDbm - minEdRssi))
    / (maxEdRssi - minEdRssi);

  return(ed);
}

/**************************************************************************//**
* @brief      Starts recording of the maximum received RSSI value.
*
* @return     none
******************************************************************************/
void halRfRecordMaxRssiStart(void)
{
  HWREG(RFCORE_XREG_FRMCTRL0) |=
    RFCORE_XREG_FRMCTRL0_ENERGY_SCAN        /* Enable ENERGY_SCAN     */
    | (3<<RFCORE_XREG_FRMCTRL0_RX_MODE_S);  /* Disable symbol search  */
}


/**************************************************************************//**
* @brief       Stops recording of the maximum received RSSI.
*
* @return      Maximum RSSI value received since starting the recording.
******************************************************************************/
int8_t halRfRecordMaxRssiStop(void)
{
  int8_t maxRssi;
  
  /*
   *  Wait for RSSI value to be valid.
   *
   *  IEEE 802.15.4:2011 8.2.5 Receiver ED
   *  No attempt is made to identify or decode signals on the channel. The ED
   *  measurement time, to average over, shall be equal to 8 symbol periods.
   */

  while ((HWREG(RFCORE_XREG_RSSISTAT) & RFCORE_XREG_RSSISTAT_RSSI_VALID) == 0);
  maxRssi = HWREG(RFCORE_XREG_RSSI);
  
  HWREG(RFCORE_XREG_FRMCTRL0) &= ~(
    RFCORE_XREG_FRMCTRL0_ENERGY_SCAN      /* Disable ENERGY_SCAN      */
    | (3<<RFCORE_XREG_FRMCTRL0_RX_MODE_S) /* Enable symbol search     */
  );    
  
  return(maxRssi);
}


/**************************************************************************//**
* @brief    Tune the crystal frequency to get correct radio frequency.
*
* @param    tuneValue   The tuning value (0...15). A higher value gives a 
*                       higher frequency. 15 is the default value.
*
* @return   None
******************************************************************************/
void halRfXtalAdjust(uint8_t tuneValue)
{
  tuneValue &= RFCORE_XREG_FREQTUNE_XOSC32M_TUNE_M;

  HWREG(RFCORE_XREG_FREQTUNE) = tuneValue;
}





/**************************************************************************//**
* LOCAL FUNCTIONS
*/



/**************************************************************************//**
* @brief    Wait until main clock is stable and start the MAC timer.
*
* @return   None
******************************************************************************/
static void halRfRadioTimerWakeUp(void)
{
  HAL_CLOCK_STABLE();

  HWREG(RFCORE_SFR_MTCTRL) |= (TIMER2_RUN);

  while(!((HWREG(RFCORE_SFR_MTCTRL) & TIMER2_STATE) != 0));
}


/**************************************************************************//**
* @brief    Callback called when transmission has completed.
*
* @param    status    Status of the transmit that just went out.
*
* @details  This function is called from an ISR!
*
* @note     Called from an ISR!
*
* @return   None
******************************************************************************/
static void halRfTxCompleteCallback(uint8 status)
{
  halRfSending = FALSE;
}


/**************************************************************************//**
* @brief    Turn off the receiver if it's not already off.
*
* @note     Called from an ISR!
*
* @return   none
******************************************************************************/
static void halRfRxOff(void)
{
  halIntState_t  s;

  HAL_ENTER_CRITICAL_SECTION(s);

  if (macRxOnFlag)
  {
    macRxOnFlag = 0;

    halRfReceiveOff();

    /* clear any receive interrupt that happened to squeak through */
    MAC_RADIO_CLEAR_RX_THRESHOLD_INTERRUPT_FLAG();
  }

  HAL_EXIT_CRITICAL_SECTION(s);
}

/**************************************************************************//**
* @brief    Turn on the receiver if it's not already on.
*
* @return   none
******************************************************************************/
static void halRfRxOn(void)
{
  halIntState_t  s;

  HAL_ENTER_CRITICAL_SECTION(s);

  if (!macRxOnFlag)
  {
    macRxOnFlag = 1;
    ISRXON();
  }

  HAL_EXIT_CRITICAL_SECTION(s);
}


/**************************************************************************//**
* @brief    Turn off receiver if permitted.
*
* @note     Called from an ISR!
*
* @return   none
******************************************************************************/
static void halRfRxOffRequest(void)
{
  halIntState_t  s;

  HAL_ENTER_CRITICAL_SECTION(s);
  if (!macRxEnableFlags)
  {
    if (!MAC_RX_IS_PHYSICALLY_ACTIVE() && !MAC_TX_IS_PHYSICALLY_ACTIVE())
    {
      halRfRxOff();
    }
  }
  HAL_EXIT_CRITICAL_SECTION(s);
}


/**************************************************************************//**
* @brief      Transmit has completed.  Perform needed maintenance and return
*             status of the transmit via callback function.
*
* @param      status    Status of the transmit that just went out.
*
* @note       Called from an ISR!
*
* @return     none
******************************************************************************/
static void halRftxComplete(uint8 status)
{
  /* update tx state; turn off receiver if nothing is keeping it on */
  macTxActive = MAC_TX_ACTIVE_NO_ACTIVITY;

  /* turn off receive if allowed */
  halRfRxOffRequest();

  /* update transmit power in case there was a change */
  //macRadioUpdateTxPower();

  /*
   *  Channel cannot change during transmit so update it here.  (Channel *can*
   *  change during a receive. The update function resets receive logic and any
   *  partially received frame is purged.)
   */
  //macRadioUpdateChannel();

  /* return status of transmit via callback function */
  halRfTxCompleteCallback(status);
}


/**************************************************************************//**
* @brief    Prepare and initialize for transmit CSP program.
*
* @note     Call before loading the CSP program!
*
* @return   none
******************************************************************************/
static void halRfPrepForTxProgram(void)
{
  MAC_ASSERT(!(HWREG(RFCORE_XREG_RFIRQM1) & IM_CSP_STOP)); /* already an active CSP program */

  /* set CSP EVENT1 to T2 CMP1 */
  MAC_MCU_CONFIG_CSP_EVENT1();

  /* set up parameters for CSP transmit program */
  HWREG(RFCORE_XREG_CSPZ) = CSPZ_CODE_CHANNEL_BUSY;

  /* clear the currently loaded CSP, this generates a stop interrupt which must be cleared */
  CSP_STOP_AND_CLEAR_PROGRAM();
  MAC_MCU_CSP_STOP_CLEAR_INTERRUPT();
  MAC_MCU_CSP_INT_CLEAR_INTERRUPT();
}

/**************************************************************************//**
* @brief    Prepare CSP for "Unslotted CSMA" transmit.
*
*           Load CSP program and set CSP parameters.
*
* @return   none
******************************************************************************/
static void halRftxCsmaPrep(void)
{
  /*
   *  IEEE 802.15.4:2011 5.1.1.4 CSMA-CA algorithm
   *
   *  Calculate random backoff delay. The CSP program loaded in this function
   *  will first wait for a while.
   *
   *  Delay backoff periods = random * (2^BE - 1)
   */

  macTxCsmaBackoffDelay = (halRfGetRandomWord() & 0xFFU) & ((1 << macTxBe) - 1);
  
  halRfPrepForTxProgram();

  /*----------------------------------------------------------------------
   *  Load CSP program :  Unslotted CSMA transmit
   */

  /*
   *  Wait for X number of backoffs, then wait for intra-backoff count
   *  to reach value set for WEVENT1.
   */
  RFST = (uint32)WAITX;
  RFST = (uint32)WEVENT1;

  /* wait until RSSI is valid */
  RFST = (uint32)WHILE(C_RSSI_IS_INVALID);
  
  /* Note that the CCA signal is updated four clock cycles (system clock) 
   * after the RSSI_VALID signal has been set.
   */
  RFST = SNOP;
  RFST = SNOP;
  RFST = SNOP;
  RFST = SNOP;

  /* sample CCA, if it fails exit from here, CSPZ indicates result */
  RFST = (uint32)SKIP(1, C_CCA_IS_VALID);
  RFST = (uint32)SSTOP;

  /* CSMA has passed so transmit (actual frame starts one backoff from when strobe is sent) */
  RFST = (uint32)STXON;

  /*
   *  Wait for the start of frame delimiter of the transmitted frame.  If SFD happens to
   *  already be active when STXON is strobed, it gets forced low.  How long this takes
   *  though, is not certain.  For bulletproof operation, the first step is to wait
   *  until SFD is inactive (which should be very fast if even necessary), and then wait
   *  for it to go active.
   */
  RFST = (uint32)WHILE(C_SFD_IS_ACTIVE);
  RFST = (uint32)WHILE(C_SFD_IS_INACTIVE);

  /*
   *  Record the timestamp.  The INT instruction causes an interrupt to fire.
   *  The ISR for this interrupt records the timestamp (which was just captured
   *  when SFD went high).
   */
  RFST = (uint32)INT;

  /*
   *  Wait for SFD to go inactive which is the end of transmit.  Decrement CSPZ to indicate
   *  the transmit was successful.
   */
  RFST = (uint32)WHILE(C_SFD_IS_ACTIVE);
  RFST = (uint32)DECZ;

  /*
   * CC2530 requires SSTOP to generate CSP_STOP interrupt.
   */
  RFST = (uint32)SSTOP;
}


/**************************************************************************//**
* @brief       Sets the WEVENT1 trigger point at the current timer count.
*
* @return      none
******************************************************************************/
static void halRfWeventSetTriggerNow(void)
{
  halIntState_t  s;
  uint8          temp0, temp1;

  /* Clear the compare interrupt flag for debugging purpose. */
  CSP_WEVENT_CLEAR_TRIGGER();

  /* copy current timer count to compare */
  HAL_ENTER_CRITICAL_SECTION(s);
  MAC_MCU_T2_ACCESS_COUNT_VALUE();
  temp0 = HWREG(RFCORE_SFR_MTM0);
  temp1 = HWREG(RFCORE_SFR_MTM1);

  MAC_MCU_T2_ACCESS_CMP1_VALUE();
  HWREG(RFCORE_SFR_MTM0) = temp0;
  HWREG(RFCORE_SFR_MTM1) = temp1;
  HAL_EXIT_CRITICAL_SECTION(s);
}


/**************************************************************************//**
* @brief      Run previously loaded CSP program for CSMA transmit.
*
* @details    Handles either unslotted CSMA transmits. When CSP program has
*             finished, an interrupt occurs and halRfCspTxStopIsr() is called.
*             This ISR will in turn call halRfTxDoneCallback().
*
* @return     none
******************************************************************************/
static void halRftxCsmaGo(void)
{
  /*
   *  Set CSPX with the countdown time of the CSMA delay.  
   */
  HWREG(RFCORE_XREG_CSPX) = (uint32)macTxCsmaBackoffDelay;

  /*
   *  Set WEVENT to trigger at the current value of the timer.  This allows
   *  unslotted CSMA to transmit just a little bit sooner.
   */
  halRfWeventSetTriggerNow();

  /*
   *  Enable interrupt that fires when CSP program stops.
   *  Also enable interrupt that fires when INT instruction
   *  is executed.
   */
  MAC_MCU_CSP_STOP_ENABLE_INTERRUPT();
  MAC_MCU_CSP_INT_ENABLE_INTERRUPT();

  /*
   *  Turn on the receiver if it is not already on.  Receiver must be 'on' for at
   *  least one backoff before performing clear channel assessment (CCA).
   */
  halRfRxOn();

  /* start the CSP program */
  CSP_START_PROGRAM();
}


/**************************************************************************//**
* @brief      This callback is executed if a CSMA transmit was attempted but
*             the channel was busy.
*
* @note       Called from an ISR!
*
* @return     none
******************************************************************************/
static void halRfTxChannelBusyCallback(void)
{
  /* turn off receiver if allowed */
  macTxActive = MAC_TX_ACTIVE_CHANNEL_BUSY;
  halRfRxOffRequest();

  /*  clear channel assessment failed, follow through with CSMA algorithm */

  /*
   *  IEEE 802.15.4:2011 5.1.1.4 CSMA-CA algorithm
   *
   *  If channel was not idle:
   *  NB = NB+1
   *  BE = min(BE+1, macMinBE)
   */

  nb++;

  if (nb > pMacPib->maxCsmaBackoffs)
  {
    /*
     *  Waited for max back offs but could not get a clear channel. Just give
     *  up.
     */

    halRftxComplete(MAC_CHANNEL_ACCESS_FAILURE);
  }
  else
  {
    macTxBe = MIN(macTxBe+1, pMacPib->maxBe);

    halRftxCsmaPrep();
    macTxActive = MAC_TX_ACTIVE_GO;
    halRftxCsmaGo();
  }
}


/**************************************************************************//**
* @brief      This callback is executed when transmit completes.
*
* @note       Called from an ISR!
*
* @return     none
******************************************************************************/
static void halRfTxDoneCallback(void)
{
  halIntState_t  s;

  /*
   *  There is a small chance this function could be called twice for a single
   *  transmit. To prevent logic from executing twice, the state variable
   *  macTxActive is used as a gating mechanism to guarantee single time
   *  execution.
   */

  HAL_ENTER_CRITICAL_SECTION(s);
  if (macTxActive == MAC_TX_ACTIVE_GO)
  {
    macTxActive = MAC_TX_ACTIVE_DONE;
    HAL_EXIT_CRITICAL_SECTION(s);

    halRftxComplete(MAC_SUCCESS);
  }
  else
  {
    HAL_EXIT_CRITICAL_SECTION(s);
  }
}


/**************************************************************************//**
* @brief       Transmit data in the TX buffer with CSMA/CA.
*
* @return      none
******************************************************************************/
static void halRfTxFrameCsma(void)
{
  MAC_ASSERT(!macTxActive);            /* transmit on top of transmit */

  /* mark transmit as active */
  macTxActive = MAC_TX_ACTIVE_INITIALIZE;

  /*----------------------------------------------------------------------------
   *  Prepare for transmit.
   */

  nb = 0;
  macTxBe = pMacPib->minBe;

  halRftxCsmaPrep();

  /*----------------------------------------------------------------------------
   *  If not receiving, start the transmit.  If receive is active
   *  queue up the transmit.
   *
   *  Critical sections around the state change prevents any sort of race
   *  condition with  macTxStartQueuedFrame(). This guarantees function
   *  halRftxCsmaGo() will only be called once.
   */
  {
    halIntState_t  s;

    HAL_ENTER_CRITICAL_SECTION(s);
    macTxActive = MAC_TX_ACTIVE_GO;
    HAL_EXIT_CRITICAL_SECTION(s);

    /*
     *  If execution has reached this point, any transmitted ACK has long since
     *  completed. It is possible though that there is still a pending callback.
     *  If so, it is irrelevant and needs to be canceled at this point.
     */
    MAC_MCU_TXACKDONE_DISABLE_INTERRUPT();

    halRftxCsmaGo();
  }
}


/**************************************************************************//**
* @brief      Interrupt service routine for handling STOP type interrupts from CSP.
*              
* @details    This interrupt occurs when the CSP program stops by 1) reaching
*             the end of the program, 2) executing SSTOP within the program,
*             3) executing immediate instruction ISSTOP.
*
*             The value of CSPZ indicates if interrupt is being used for ACK
*             timeout or is the end of a transmit.
*
* @return     none
******************************************************************************/
static void halRfCspTxStopIsr(void)
{
  MAC_MCU_CSP_STOP_DISABLE_INTERRUPT();

  if (HWREG(RFCORE_XREG_CSPZ) == CSPZ_CODE_TX_DONE)
  {
    halRfTxDoneCallback();
  }
  else if (HWREG(RFCORE_XREG_CSPZ) == CSPZ_CODE_CHANNEL_BUSY)
  {
    halRfTxChannelBusyCallback();
  }
}


/**************************************************************************//**
* @brief      Interrupt service routine for handling INT type interrupts from
*             CSP.
*
* @details    This interrupt happens when the CSP instruction INT is executed.
*             It occurs once the SFD signal goes high indicating that transmit
*             has successfully started. The timer value has been captured at
*             this point and timestamp can be stored.
*
* @return     none
******************************************************************************/
static void halRfCspTxIntIsr(void)
{
  MAC_MCU_CSP_INT_DISABLE_INTERRUPT();
}


/**************************************************************************//**
* @brief    This function initializes the CC2538 to control the CC2592 PA/LNA
*           signals.
*
* @details  CC2538 GPIO connected to CC2592 HGM is configured as high.
*           CC2538 RX(TX) active status signals are mapped to CC2592 EN(PAEN)
*           signals.
*
* @return   None
******************************************************************************/
static void halRfPaLnaInit(void)
{
  /*
   *  LNAEN and PAEN pins seems to match the EMB-Z2538PA module. The
   *  Low/High_Sensitivity (HGM) must be checked.
   */

  // Configure CC2538 PD0 (CC2592 HGM) as GPIO output
  GPIODirModeSet(GPIO_D_BASE, IOC_PIN_0, GPIO_DIR_MODE_OUT);
  HWREG(IOC_PD0_OVER) = IOC_OVERRIDE_DIS;

  // Set CC2592 to HGM
  halRfSetGain(HAL_RF_GAIN_HIGH);

  // Use CC2538 RF status signals to control CC2592 LNAEN and PAEN.
  // CC2538 PC2 is connected to CC2592 LNAEN
  // CC2538 PC3 is connected to CC2592 PAEN

#if 0
  HWREG(RFCORE_XREG_RFC_OBS_CTRL0) = 0x11;                    // rfc_obs_sig0 = rx_active
  HWREG(CCTEST_OBSSEL2)            = CCTEST_OBSSEL2_EN;       // rfc_obs_sig0 => PC2

  HWREG(RFCORE_XREG_RFC_OBS_CTRL1) = 0x10;                    // rfc_obs_sig1 = tx_active
  HWREG(CCTEST_OBSSEL3)            = CCTEST_OBSSEL2_EN | 1U;  // rfc_obs_sig1 => PC3

#else
  /*
   *  Recommended settings from "AN130 - Using CC2592 Front End With CC2538"
   */

  HWREG(IOC_PC2_OVER) = IOC_OVERRIDE_OE;

  HWREG(RFCORE_XREG_RFC_OBS_CTRL0) = 
    RFCORE_XREG_RFC_OBS_CTRL0_RFC_OBS_POL0 | 0x2A;  // rfc_obs_sig0 = lna_pd (LNA power-down) signal inverted

  HWREG(CCTEST_OBSSEL2) = CCTEST_OBSSEL2_EN;        // rfc_obs_sig0 => PC2

  HWREG(IOC_PC3_OVER) = IOC_OVERRIDE_OE;

  HWREG(RFCORE_XREG_RFC_OBS_CTRL1) = 
    RFCORE_XREG_RFC_OBS_CTRL1_RFC_OBS_POL1 | 0x28;  // rfc_obs_sig1 = pa_pd (Power amplifier power-down) signal inverted

  HWREG(CCTEST_OBSSEL3) = CCTEST_OBSSEL2_EN | 1U;   // rfc_obs_sig1 => PC3
#endif
}


#ifndef MRFI
/**************************************************************************//**
* @brief    Interrupt service routine that handles RFPKTDONE interrupt.
*
* @return   None
******************************************************************************/
static void halRfIsr(void)
{
  unsigned short s;
  uint8 rfim;

  HAL_INT_LOCK(s);

  rfim = (uint8) HWREG(RFCORE_XREG_RFIRQM1);

  // Clear general RF interrupt flag
  IntPendClear(INT_RFCORERTX);

  if ((HWREG(RFCORE_SFR_RFIRQF1) & IRQ_CSP_MANINT) & ((uint32)rfim))
  {
    /*
     *  Important!  Because of how the CSP programs are written, CSP_INT interrupts should
     *  be processed before CSP_STOP interrupts.  This becomes an issue when there are
     *  long critical sections.
     */
    /* clear flag */
    HWREG(RFCORE_SFR_RFIRQF1) = ~IRQ_CSP_MANINT;
    halRfCspTxIntIsr();
  }
  else if ((HWREG(RFCORE_SFR_RFIRQF1) & IRQ_CSP_STOP) & ((uint32)rfim))
  {
    /* clear flag */
    HWREG(RFCORE_SFR_RFIRQF1) = ~IRQ_CSP_STOP;
    halRfCspTxStopIsr();
  }

 // rfim = (uint8) HWREG(RFCORE_XREG_RFIRQM0);

  if(HWREG(RFCORE_SFR_RFIRQF0) & RFCORE_SFR_RFIRQF0_RXPKTDONE)
  {
    // Clear RXPKTDONE interrupt
    HWREG(RFCORE_SFR_RFIRQF0) &= ~RFCORE_SFR_RFIRQF0_RXPKTDONE;
    
    if(pfISR)
    {
      // Execute the custom ISR
      pfISR();
    }
  }

  HAL_INT_UNLOCK(s);
}

/**************************************************************************//**
* @brief    Interrupt service routine that handles RF ERROR interrupt.
*
* @return   None
******************************************************************************/
static void halRfErrorIsr(void)
{
  uint8 rfErrMask;

  rfErrMask = (uint8) HWREG(RFCORE_XREG_RFERRM);

  if((HWREG(RFCORE_SFR_RFERRF) & RFCORE_SFR_RFERRF_RXOVERF) & rfErrMask)
  {
    // Clear RXPKTDONE interrupt
    HWREG(RFCORE_SFR_RFIRQF0) &= ~RFCORE_SFR_RFIRQF0_RXPKTDONE;
    
    // Clear RXOVERF interrupt flag
    HWREG(RFCORE_SFR_RFERRF) = ~RFCORE_SFR_RFERRF_RXOVERF;
    
    halRfFlushRxBuff();
  }
}

/**************************************************************************//**
* @brief      Set overflow count period value.
*
* @details    An interrupt is triggered when the overflow count equals this
*             period value.
*
* @param      count  Overflow count compare value
*
* @return     none
******************************************************************************/
static void halRfOverflowSetPeriod(uint32 count)
{
  halIntState_t  s;
  uint8 enableCompareInt = 0;

  MAC_ASSERT( !(count >> 24) );   /* illegal count value */

  HAL_ENTER_CRITICAL_SECTION(s);

  /*  Disable overflow compare interrupts. */
  if (HWREG(RFCORE_SFR_MTIRQM) & TIMER2_OVF_PERM)
  {
    enableCompareInt = 1;
    HWREG(RFCORE_SFR_MTIRQM) &= ~TIMER2_OVF_PERM;
  }

  MAC_MCU_T2_ACCESS_OVF_PERIOD_VALUE();

  /* for efficiency, the 32-bit value is decoded using endian abstracted indexing */
  HWREG(RFCORE_SFR_MTMOVF0) = ((uint8 *)&count)[0];
  HWREG(RFCORE_SFR_MTMOVF1) = ((uint8 *)&count)[1];
  HWREG(RFCORE_SFR_MTMOVF2) = ((uint8 *)&count)[2];

  /*
   *  Now that new compare value is stored, clear the interrupt flag.  This is important just
   *  in case a false match was generated as the multi-byte compare value was written.
   */
  HWREG(RFCORE_SFR_MTIRQF) &= ~TIMER2_OVF_PERF;

  /* re-enable overflow compare interrupts if they were previously enabled */
  if (enableCompareInt)
  {
    HWREG(RFCORE_SFR_MTIRQM) |= TIMER2_OVF_PERM;
  }

  HAL_EXIT_CRITICAL_SECTION(s);
}

/**************************************************************************//**
* @brief       Returns the value of the overflow counter which is a special hardware feature.
*              The overflow count actually is 24 bits of information.
*
* @return      value of overflow counter
******************************************************************************/
static uint32 halRfOverflowCount(void)
{
  uint32         overflowCount;
  halIntState_t  s;

  /* for efficiency, the 32-bit value is encoded using endian abstracted indexing */

  HAL_ENTER_CRITICAL_SECTION(s);

  /* This T2 access macro allows accessing both T2MOVFx and T2Mx */
  MAC_MCU_T2_ACCESS_OVF_COUNT_VALUE();

  /* Latch the entire T2MOVFx first by reading T2M0. */
  HWREG(RFCORE_SFR_MTM0);
  ((uint8 *)&overflowCount)[0] = HWREG(RFCORE_SFR_MTMOVF0);
  ((uint8 *)&overflowCount)[1] = HWREG(RFCORE_SFR_MTMOVF1);
  ((uint8 *)&overflowCount)[2] = HWREG(RFCORE_SFR_MTMOVF2);
  ((uint8 *)&overflowCount)[3] = 0;
  HAL_EXIT_CRITICAL_SECTION(s);

  return (overflowCount);
}


/**************************************************************************//**
* @brief       Sets the value of the hardware overflow counter.
*
* @param       count    New overflow count value
*
* @return      none
******************************************************************************/
static void halRfOverflowSetCount(uint32 count)
{
  halIntState_t  s;

  MAC_ASSERT(! (count >> 24) );   /* illegal count value */

  /* save the current overflow count */
  accumulatedOverflowCount += halRfOverflowCount();

  /* deduct the initial count */
  accumulatedOverflowCount -= count;

  HAL_ENTER_CRITICAL_SECTION(s);
  MAC_MCU_T2_ACCESS_OVF_COUNT_VALUE();

  /* for efficiency, the 32-bit value is decoded using endian abstracted indexing */
  /* T2OF2 must be written last */
  HWREG(RFCORE_SFR_MTMOVF0) = (uint32)((uint8 *)&count)[0];
  HWREG(RFCORE_SFR_MTMOVF1) = (uint32)((uint8 *)&count)[1];
  HWREG(RFCORE_SFR_MTMOVF2) = (uint32)((uint8 *)&count)[2];
  HAL_EXIT_CRITICAL_SECTION(s);
}


/**************************************************************************//**
* @brief      Set rollover count of backoff timer.
*
* @param      rolloverBackoff   Backoff count where count is reset to zero
*
* @return     none
******************************************************************************/
static void halRfBackoffTimerSetRollover(uint32 rolloverBackoff)
{
  halIntState_t  s;

  MAC_ASSERT(rolloverBackoff > halRfOverflowCount());  /* rollover value must be greater than count */

  HAL_ENTER_CRITICAL_SECTION(s);
  backoffTimerRollover = rolloverBackoff;
  halRfOverflowSetPeriod(rolloverBackoff);
  HAL_EXIT_CRITICAL_SECTION(s);
}

/**************************************************************************//**
* @brief      Intializes backoff timer.
*
* @return     none
******************************************************************************/
static void halRfBackoffTimerInit(void)
{
  halRfOverflowSetCount(0);
  halRfBackoffTimerSetRollover(MAC_BACKOFF_TIMER_DEFAULT_NONBEACON_ROLLOVER);

  MAC_RADIO_BACKOFF_PERIOD_CLEAR_INTERRUPT();
  MAC_RADIO_BACKOFF_PERIOD_ENABLE_INTERRUPT();
  MAC_RADIO_BACKOFF_COMPARE_CLEAR_INTERRUPT();
  MAC_RADIO_BACKOFF_COMPARE_ENABLE_INTERRUPT();
}


/**************************************************************************//**
* @brief      Function to get the timer 2 rollover value
*
* @return     timer 2 rollover value
******************************************************************************/
static uint32 halRfGetBackOffTimerRollover(void)
{
  return backoffTimerRollover;
}


/**************************************************************************//**
* @brief      This function is used to accumulate timer 2 overflow if applicable 
*             on the relevant platform
*
* @return     none
******************************************************************************/
static void halRfMcuAccumulatedOverFlow(void)
{
  halIntState_t  s;
  HAL_ENTER_CRITICAL_SECTION(s);
  
  if(updateRolloverflag == FALSE)
  { 
    accumulatedOverflowCount += halRfGetBackOffTimerRollover(); 
  }
  else
  { 
    updateRolloverflag = FALSE; 
  }

  HAL_EXIT_CRITICAL_SECTION(s);
}


/**************************************************************************//**
* @brief      Interrupt service routine that fires when the backoff count rolls
*             over on overflow period.
*
* @return     none
******************************************************************************/
static void halRfBackoffTimerPeriodIsr(void)
{
  halRfMcuAccumulatedOverFlow();
  //macBackoffTimerRolloverCallback();
}


/**************************************************************************//**
* @brief       Interrupt service routine for timer2, the MAC timer.
*
* @return      none
******************************************************************************/
static void halRfMacTimer2Isr(void)
{
  uint8 t2irqm;
  uint8 t2irqf;

  t2irqm = HWREG(RFCORE_SFR_MTIRQM);
  t2irqf = HWREG(RFCORE_SFR_MTIRQF);

  /*---------------------------------------------------------------------------
   *  Overflow compare interrupt - triggers when then overflow counter is
   *  equal to the overflow compare register.
   */
  if ((t2irqf & TIMER2_OVF_COMPARE1F) & t2irqm)
  {

    /* call function for dealing with the timer compare interrupt */
    //halRfBackoffTimerCompareIsr();

    /* clear overflow compare interrupt flag */
    HWREG(RFCORE_SFR_MTIRQF) = ~TIMER2_OVF_COMPARE1F;
  }

  /*---------------------------------------------------------------------------
   *  Overflow compare interrupt - triggers when then overflow counter is
   *  equal to the overflow compare register.
   */
  if ((t2irqf & TIMER2_OVF_PERF) & t2irqm)
  {
    /* call function for dealing with the timer compare interrupt */
    halRfBackoffTimerPeriodIsr();

    /* clear overflow compare interrupt flag */
    HWREG(RFCORE_SFR_MTIRQF) = ~TIMER2_OVF_PERF;
  }

  /*---------------------------------------------------------------------------
   *  Overflow interrupt - triggers when the hardware timer rolls over.
   */
  else if ((t2irqf & TIMER2_PERF) & t2irqm)
  {
    /*
     *  Call energy detect interrupt function, this interrupt not used for any
     *  other functionality.
     */
   // mcuRecordMaxRssiIsr();

    /* clear the interrupt flag */
    HWREG(RFCORE_SFR_MTIRQF) = ~TIMER2_PERF;
  }
  
  //CLEAR_SLEEP_MODE(); 
}

#endif

/**************************************************************************//**
* @brief    Initialize the random number generator.
*
* @return   None
******************************************************************************/
static void halInitRandom(void)
{
  uint32_t frmCtrlValue;

  /*
   *  Set radio for infinite reception. Once radio reaches this state,
   *  it will stay in receive mode regardless RF activity. After that turn on
   *  the reciever.
   */
  
  frmCtrlValue = HWREG(RFCORE_XREG_FRMCTRL0);
  HWREG(RFCORE_XREG_FRMCTRL0) = 2U << RFCORE_XREG_FRMCTRL0_RX_MODE_S;
  halRfReceiveOn();

  /*
   *  Wait for radio to reach infinite reception state by checking RSSI valid
   *  flag. Once it does, the least significant bit of ADTSTH should be pretty
   *  random.
   */

  while (!(HWREG(RFCORE_XREG_RSSISTAT) & RFCORE_XREG_RSSISTAT_RSSI_VALID));

  /* put 16 random bits into the seed value */
  {
    uint16 rndSeed;
    uint8  i;

    rndSeed = 0;

    for(i=0; i<16; i++)
    {
      /* 
       *  Use most random bit of analog to digital receive conversion to
       *  populate the random seed
       */

      rndSeed =
        (rndSeed << 1) | (HWREG(RFCORE_XREG_RFRND) & RFCORE_XREG_RFRND_IRND_M);
    }

    /*
     *  The seed value must not be zero or 0x0380 (0x8003 in the polynomial).
     *  If it is, the psuedo random sequence won�t be random. There is an
     *  extremely small chance this seed could randomly be zero or 0x0380.  The
     *  following check makes sure this does not happen.
     */

    if (rndSeed == 0x0000 || rndSeed == 0x0380)
    {
      rndSeed = 0xBABE; /* completely arbitrary "random" value */
    }

    /*
     *  Two writes to RNDL will set the random seed.  A write to RNDL copies
     *  current contents of RNDL to RNDH before writing new the value to RNDL.
     */

    HWREG(SOC_ADC_RNDL) = rndSeed & 0xFF;
    HWREG(SOC_ADC_RNDL) = rndSeed >> 8;
  }

  /*
   *  Turn off the receiver and take it out of infinite reception mode; set back
   *  to normal operation.
   */

  halRfReceiveOff();
  HWREG(RFCORE_XREG_FRMCTRL0) = frmCtrlValue;
}

/**************************************************************************//**
* Close the Doxygen group.
* @}
******************************************************************************/
