//*****************************************************************************
//! @file       TIPhy.c
//! @brief      TI PHY driver.
//!
//! Revised     $Date: 2015-11-05 14:04:12 +0200 (to, 05 marras 2015) $
//! Revision    $Revision: 769 $
//
//  Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
//
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//
//    Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//****************************************************************************/


/**************************************************************************//**
* @addtogroup basic_rf_api
* @{
******************************************************************************/


/******************************************************************************
* INCLUDES
*/

#include <stdlib.h>

#include "hal_int.h"
#include "hal_rf.h"
#include "TIPhy.h"
#include "mac_pib.h"

/******************************************************************************
* CONSTANTS AND DEFINES
*/
// Packet and packet part lengths
#define PKT_LEN_MIC                         8

#define BASIC_RF_MAX_PSDU_SIZE	            127U  /**< Maximum PSDU size. */
#define BASIC_RF_MFR_SIZE                   2U    /**< MAC footer size.   */
#define BASIC_RF_PHR_SIZE                   1U    /**< PHY header size.   */
#define BASIC_RF_MAX_PPDU_SIZE              BASIC_RF_MAX_PSDU_SIZE + BASIC_RF_PHR_SIZE

// The length byte
#define BASIC_RF_PLD_LEN_MASK               0x7F

// Frame control field
#define BASIC_RF_FCF_NOACK                  0x8841
#define BASIC_RF_FCF_ACK                    0x8861
#define BASIC_RF_FCF_ACK_BM                 0x0020
#define BASIC_RF_FCF_BM                     (~BASIC_RF_FCF_ACK_BM)
#define BASIC_RF_SEC_ENABLED_FCF_BM         0x0008

// Frame control field LSB
#define BASIC_RF_FCF_NOACK_L                LO_UINT16(BASIC_RF_FCF_NOACK)
#define BASIC_RF_FCF_ACK_L                  LO_UINT16(BASIC_RF_FCF_ACK)
#define BASIC_RF_FCF_ACK_BM_L               LO_UINT16(BASIC_RF_FCF_ACK_BM)
#define BASIC_RF_FCF_BM_L                   LO_UINT16(BASIC_RF_FCF_BM)
#define BASIC_RF_SEC_ENABLED_FCF_BM_L       LO_UINT16(BASIC_RF_SEC_ENABLED_FCF_BM)

// Auxiliary Security header
#define BASIC_RF_AUX_HDR_LENGTH             5
#define BASIC_RF_LEN_AUTH                   BASIC_RF_PACKET_OVERHEAD_SIZE + \
    BASIC_RF_AUX_HDR_LENGTH - BASIC_RF_FOOTER_SIZE
#define BASIC_RF_SECURITY_M                 2
#define BASIC_RF_LEN_MIC                    8
#ifdef SECURITY_CCM
#undef BASIC_RF_HDR_SIZE
#define BASIC_RF_HDR_SIZE                   15
#endif

// Footer
#define BASIC_RF_CRC_OK_BM                  0x80

/******************************************************************************
* TYPEDEFS
*/

// The receive struct
typedef struct
{
  volatile uint8_t isReady;
  phyDataInd_t * pDataInd;
} TiPhyRxInfo_t;

// Tx state
typedef struct
{
  uint8_t receiveOn;
} TiPhyTxState_t;

typedef struct 
{
  uint8_t fineTune;
  uint8_t coarseTune;
  phyConfig_t * pConfig;
  TiPhyRxInfo_t rxInfo;
  TiPhyTxState_t txInfo;
} TiPhyInst;

/******************************************************************************
* LOCAL VARIABLES
*/

static TiPhyInst tiPhyInst = {0};

/******************************************************************************
* GLOBAL VARIABLES
*/

/**
 *  MAC PIB
 *
 *  Most of the values in the PIB are not used by the driver. The driver is a
 *  very low level driver and PopNet takes care of all higher level operations.
 */
macPib_t macPib =
{
  54,                                         /* macAckWaitDuration */
  FALSE,                                      /* macAssociationPermit */
  TRUE,                                       /* macAutoRequest */
  FALSE,                                      /* macBattLifeExt */
  6,                                          /* macBattLifeExtPeriods */

  NULL,                                       /* macBeaconPayload */
  0,                                          /* macBeaconPayloadLength */
 // MAC_BO_NON_BEACON,                        /* macBeaconOrder */
  0,                                          /* macBeaconTxTime */
  0,                                          /* macBSN */

  //  {0, SADDR_MODE_EXT},                    /* macCoordExtendedAddress */
  0xFFFF,                                     /* macCoordShortAddress */
  0,                                          /* macDSN */
  FALSE,                                      /* macGTSPermit */
  4,                                          /* macMaxCSMABackoffs */

  3,                                          /* macMinBe */
  0xFFFF,                                     /* macPANId */
  FALSE,                                      /* macPromiscuousMode */
  FALSE,                                      /* macRxOnWhenIdle */
  0x0000,                                     /* macShortAddress */

  // MAC_SO_NONE,                             /* macSuperframeOrder */
  0x01F4,                                     /* macTransactionPersistenceTime */
  FALSE,                                      /* macAssociatedPANCoord */
  5,                                          /* macMaxBe */
  1220,                                       /* macMaxFrameTotalWaitTime */

  3,                                          /* macMaxFrameRetries */
  32,                                         /* macResponseWaitTime */
  0,                                          /* macSyncSymbolOffset */
  TRUE,                                       /* macTimestampSupported */
  FALSE,                                      /* macSecurityEnabled */
};

/******************************************************************************
* LOCAL FUNCTIONS
*/

/**************************************************************************//**
* @brief    Builds PPDU (PHR + PSDU).
*
* @param    pPsdu           Pointer to buffer with PSDU.
* @param    psduLen         Length of PSDU including FCS bytes.
*
* @details  The function will store the PHR to the hardware TX buffer and also
*           copy the PSDU data to the HW TX buffer.
*
* @return   None.
******************************************************************************/
static void phyBuildPpdu(uint8_t* pPsdu, uint8_t psduLen)
{
  // Add the PHY header.
  halRfWriteTxBuf(&psduLen, 1);

  // No need to copy FCS bytes because they will be added by hardware.
  psduLen -= 2;
  halRfAppendTxBuf(pPsdu, psduLen);
}


/**************************************************************************//**
* @brief    Interrupt service routine for received frame from radio
*           (either data or acknowledgment)
*
* @return   None
******************************************************************************/
static void basicRfRxFrmDoneIsr(void)
{
  // Clear interrupt and disable new RX frame done interrupt
  halRfDisableRxInterrupt();

  // Enable all other interrupt sources (enables interrupt nesting)
  halIntOn();

  if (tiPhyInst.rxInfo.pDataInd != NULL)
  {
    uint8_t * pStatusWord;
    
    // Read payload length.
    halRfReadRxBuf(&tiPhyInst.rxInfo.pDataInd->data.psduLen, 1);
    tiPhyInst.rxInfo.pDataInd->data.psduLen &= BASIC_RF_PLD_LEN_MASK; // Ignore MSB

    halRfReadRxBuf(
      tiPhyInst.rxInfo.pDataInd->data.pPsdu,
      tiPhyInst.rxInfo.pDataInd->data.psduLen
    );
  
    /*
     *  Remove the FCS from the psduLen. There is no point in providing the
     *  FCS bytes to the higher layer because the hardware has replaced the
     *  FCS with RSSI, correlation and CRC check status.
     */

    tiPhyInst.rxInfo.pDataInd->data.psduLen -= 2;

    /*
     *  Read the FCS bytes to get the RSSI and CRC. TI hardware replaces the 
     *  actual FCS with RSSI and CRC status information.
     */

    pStatusWord =
      tiPhyInst.rxInfo.pDataInd->data.pPsdu
      + tiPhyInst.rxInfo.pDataInd->data.psduLen;
    
    // Using the RSSI value as LQI
    // Maybe use the correlation value and calculate better LQI?
    {
      int8_t rssiDbm;
      
      rssiDbm = *((int8_t *) pStatusWord) - halRfGetRssiOffset();
      tiPhyInst.rxInfo.pDataInd->lqi = halRfComputeED(rssiDbm);
    }
    
    // Notify the application about the received data packet if the CRC is OK
    if(pStatusWord[1] & BASIC_RF_CRC_OK_BM)
    {
      tiPhyInst.rxInfo.isReady = TRUE;
      tiPhyInst.pConfig->pDataIndCb();
    }
    else
    {
      // CRC error. Just throw the packet away. This should not happen because
      // hardware CRC checking is enabled.
      tiPhyInst.rxInfo.isReady = FALSE;
    }
    
    // Enable RX frame done interrupt again
    halIntOff();
    halRfEnableRxInterrupt();
  }
  else
  {
    /*
     *  Frame received but we do not have a buffer for the data. Leave it in the
     *  HW RX buffer until we receive a buffer that we can copy it to.
     */

    halRfFlushRxBuff();
  }

  // Enable RX frame done interrupt again
  halIntOff();
  halRfEnableRxInterrupt();
}


/******************************************************************************
* GLOBAL FUNCTIONS
*/

/**************************************************************************//**
* @brief    Initialize basic RF data structures. Sets channel, short address and
*           PAN id in the chip and configures interrupt on packet reception
*
* @param    pPhyConfig  Pointer to configuration struct. This struct must be
*                       allocated by higher layer.
*
* @return   None
******************************************************************************/
uint8_t phyInit(phyConfig_t * pPhyConfig)
{
  halRfSetModule(HAL_RF_CC2538_CC2592EM);
  
  if (halRfInit()==FAILED)
    return FAILED;

  halIntOff();

  // Set the protocol configuration
  tiPhyInst.pConfig = pPhyConfig;
  tiPhyInst.txInfo.receiveOn = TRUE;

  // Set channel
  halRfSetChannel(tiPhyInst.pConfig->channel);

  // Write the short address and the PAN ID to the CC2520 RAM
  halRfSetShortAddr(tiPhyInst.pConfig->myAddr);
  //halRfSetPanId(tiPhyInst.pConfig->panId);
  
  // Set up receive interrupt (received data or acknowledgment)
  halRfRxInterruptConfig(basicRfRxFrmDoneIsr);

  halIntOn();

  return SUCCESS;
}


/**************************************************************************//**
* @brief    Send packet.
*
* @param    pDataReq    Data request information.
*
* @return   Returns SUCCESS or FAILED
******************************************************************************/
uint8_t phyPDRequest(phyDataReq_t * pDataReq)
{
  uint8_t psduLength;
  
  // Turn on receiver if its not on
  if(!tiPhyInst.txInfo.receiveOn) {
    halRfReceiveOn();
  }

  // Check packet length (Add two bytes for FCS)
  psduLength = MIN(pDataReq->data.psduLen + 2, BASIC_RF_MAX_PSDU_SIZE);
  
  // Wait until the transceiver is idle
  halRfWaitTransceiverReady();

  // Turn off RX frame done interrupt to avoid interference on the SPI interface
  halRfDisableRxInterrupt();

  phyBuildPpdu(pDataReq->data.pPsdu, psduLength);

  // Turn on RX frame done interrupt for ACK reception
  halRfEnableRxInterrupt();
  
  // Send frame with CCA. return FAILED if not successful
  halRfTransmit();

  // Turn off the receiver if it should not continue to be enabled
  if (!tiPhyInst.txInfo.receiveOn) {
    halRfReceiveOff();
  }

  return SUCCESS;
}


/**************************************************************************//**
* @brief    Check if a new packet is ready to be read by next higher layer.
*
* @param    none
*
* @return   uint8 - TRUE if a packet is ready to be read by higher layer.
******************************************************************************/
uint8_t basicRfPacketIsReady(void)
{
  return tiPhyInst.rxInfo.isReady;
}


/**************************************************************************//**
* @brief    Copies the payload of the last incoming packet into a buffer.
*
* @return   Pointer to the data indication struct containing information about
*           the received frame. NULL if no frame has been received.
******************************************************************************/
phyDataInd_t * phyPDGetData(void)
{
  phyDataInd_t * pRetVal;
  
  pRetVal = NULL;
  
  // Critical region start
  halIntOff();

  if (tiPhyInst.rxInfo.isReady) {
    tiPhyInst.rxInfo.isReady = FALSE;
    pRetVal = tiPhyInst.rxInfo.pDataInd;
    tiPhyInst.rxInfo.pDataInd = NULL;
  }
  
  // Critical region end
  halIntOn();
  
  return pRetVal;
}


/**************************************************************************//**
* @brief    Sets up data reception.
*
* @param    pData   Pointer to data indication structure that will be filled
*                   with information about the next received frame. Must remain
*                   allocated until the data received callback is called.
*
* @return   -
******************************************************************************/
void phyPDReceive(phyDataInd_t * pData)
{
  tiPhyInst.rxInfo.pDataInd = pData;
  tiPhyInst.txInfo.receiveOn = TRUE;
  halRfReceiveOn();
}

/**************************************************************************//**
* @brief    Turns on receiver on radio.
*
* @return   None
******************************************************************************/
void phyPLMEReceiveOn(void)
{
  tiPhyInst.txInfo.receiveOn = TRUE;
  halRfReceiveOn();
}


/**************************************************************************//**
* @brief    Turns off receiver on radio
*
* @return   None
******************************************************************************/
void phyPLMEReceiveOff(void)
{
  tiPhyInst.txInfo.receiveOn = FALSE;
  halRfReceiveOff();
}


/**************************************************************************//**
* @brief    Sets the active channel.
*
* @param    channel   The desired channel number (11...26).
*
* @return   -
******************************************************************************/
void phyPLMESetChannel(uint8_t channel)
{
  /*
   *  Turn receiver off and then on again to recalibrate the frequency.
   */

  if (tiPhyInst.txInfo.receiveOn) {
    halRfReceiveOff();
  }

  halRfSetChannel(channel);

  if(tiPhyInst.txInfo.receiveOn) {
    halRfReceiveOn();
  }
}


/**************************************************************************//**
* @brief    Sets the TX power level.
*
* @param    power   The desired power level (dBm). The supported power level
*                   range depends on the hardware configuration.
*
* @retval   TRUE    The power level was set successfully.
* @retval   FALSE   The power level could not be set. Probably out of range.
******************************************************************************/
uint8_t phyPLMESetTxPower(int8_t power)
{
  return halRfSetTxPower(power) == SUCCESS ? TRUE : FALSE;
}


/**************************************************************************//**
* @brief    Initiates energy detect.
*
* @details  The highest energy detected is recorded from the time when this
*           function is called until the energy detect is stopped.
*
* @return   -
******************************************************************************/
void phyPLMEEnergyDetectStart(void)
{
  halRfRecordMaxRssiStart();
  
  if(!tiPhyInst.txInfo.receiveOn) {
    halRfReceiveOn();
  }
}


/**************************************************************************//**
* @brief    Stops energy detect feature.
*
* @details  
*
* @return   The highest energy detected during scanning.
******************************************************************************/
uint8_t phyPLMEEnergyDetectStop(void)
{
  int8_t rssiDbm;
  uint8_t energyDetectMeasurement;

  rssiDbm = halRfRecordMaxRssiStop() - halRfGetRssiOffset();
  energyDetectMeasurement = halRfComputeED(rssiDbm);

  // Turn off the receiver if it should not continue to be enabled
  if (!tiPhyInst.txInfo.receiveOn) {
    halRfReceiveOff();
  }
  
  return(energyDetectMeasurement);
}

/**************************************************************************//**
* @brief    Tunes the crystal frequency to get correct radio frequency.
*
* @param    coarseTrim    Coarse tuning value.
* @param    fineTrim      Fine tuning value.
*
* @details  
*
* @return   -
******************************************************************************/
void phyPLMEXtalAdjust(uint8_t coarseTrim, uint8_t fineTrim)
{
  if (coarseTrim != 0xFF)
  {
    tiPhyInst.coarseTune = coarseTrim;
  }

  if (fineTrim != 0xFF)
  {
    tiPhyInst.fineTune = fineTrim;
  }
}

/**************************************************************************//**
* Close the Doxygen group.
* @}
******************************************************************************/
