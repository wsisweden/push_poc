//*****************************************************************************
//! @file       hal_rf.h
//! @brief      HAL radio interface header file
//!
//! Revised     $Date: 2015-10-27 11:33:59 +0200 (ti, 27 loka 2015) $
//! Revision    $Revision: 747 $
//
//  Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
//
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//
//    Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//****************************************************************************/
#ifndef HAL_RF_H
#define HAL_RF_H


/******************************************************************************
* If building with a C++ compiler, make all of the definitions in this header
* have a C binding.
******************************************************************************/
#ifdef __cplusplus
extern "C"
{
#endif


/******************************************************************************
* INCLUDES
*/
#include "../TIDriverLib/inc/hw_types.h"
#include "hal_types.h"

/******************************************************************************
* TYPEDEFS
*/


/******************************************************************************
* CONSTANTS AND DEFINES
*/
// Chip ID's
#define HAL_RF_CHIP_ID_CC1100         0x00
#define HAL_RF_CHIP_ID_CC1110         0x01
#define HAL_RF_CHIP_ID_CC1111         0x11
#define HAL_RF_CHIP_ID_CC2420         0x02
#define HAL_RF_CHIP_ID_CC2500         0x80
#define HAL_RF_CHIP_ID_CC2510         0x81
#define HAL_RF_CHIP_ID_CC2511         0x91
#define HAL_RF_CHIP_ID_CC2550         0x82
#define HAL_RF_CHIP_ID_CC2520         0x84
#define HAL_RF_CHIP_ID_CC2430         0x85
#define HAL_RF_CHIP_ID_CC2431         0x89
#define HAL_RF_CHIP_ID_CC2530         0xA5
#define HAL_RF_CHIP_ID_CC2531         0xB5
#define HAL_RF_CHIP_ID_CC2533         0x95
#define HAL_RF_CHIP_ID_CC2540         0x8D
#define HAL_RF_CHIP_ID_CC2541         0x41
#define HAL_RF_CHIP_ID_CC2538         0xB964

typedef enum {
  HAL_RF_CC2538EM,
  HAL_RF_CC2538_CC2591EM,
  HAL_RF_CC2538_CC2592EM
} halRfModule;

typedef enum {
  HAL_RF_GAIN_LOW,
  HAL_RF_GAIN_HIGH
} halRfGain;

// IEEE 802.15.4 defined constants (2.4 GHz logical channels)
#define MIN_CHANNEL 				  11    //!< Min. channel (2405 MHz)
#define MAX_CHANNEL                   26    //!< Max. channel (2480 MHz)
#define CHANNEL_SPACING               5     //!< Channel spacing in MHz

/******************************************************************************
* GLOBAL FUNCTIONS
*/
// Generic RF interface
uint8_t halRfInit(void);
uint8_t halRfSetTxPower(int8_t power);
uint8_t halRfTransmit(void);
void  halRfSetGain(halRfGain gainMode); 
uint8_t halRfSetModule(halRfModule emModule);  

uint16_t halRfGetChipId(void);
uint8_t halRfGetChipVer(void);
int8_t halRfGetRssiOffset(void);

void  halRfWriteTxBuf(uint8_t* pData, uint8_t length);
void  halRfAppendTxBuf(uint8_t* pData, uint8_t length);
void  halRfReadRxBuf(uint8_t* pData, uint8_t length);
void  halRfWaitTransceiverReady(void);

void  halRfReceiveOn(void);
void  halRfReceiveOff(void);
void  halRfDisableRxInterrupt(void);
void  halRfEnableRxInterrupt(void);
void  halRfRxInterruptConfig(void (*pf)(void));
void  halRfFlushRxBuff(void);

// IEEE 802.15.4 specific interface
void  halRfSetChannel(uint8_t channel);
void  halRfSetShortAddr(uint16_t shortAddr);
void  halRfSetPanId(uint16_t panId);

uint8_t halRfComputeED(int8_t rssiDbm);
void  halRfRecordMaxRssiStart(void);
int8_t  halRfRecordMaxRssiStop(void);

void halRfXtalAdjust(uint8_t tuneValue);

uint16_t halRfGetRandomWord(void);


void macRxHaltCleanup(void);


extern uint8 macRxActive;

/******************************************************************************
* Mark the end of the C bindings section for C++ compilers.
******************************************************************************/
#ifdef  __cplusplus
}
#endif
#endif // #ifndef HAL_RF_H
