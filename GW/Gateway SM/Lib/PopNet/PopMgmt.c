/***************************************************************************
  PopMgmt.c
  Copyright (c) 2006-2010 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.
  
  This module defines the over-the-air and internal management functions for
  PopNet networking. For example, asking the other node for its node
  information (essentially a beacon response).

****************************************************************************/
#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"
#include "PopNwk.h"
#include <stddef.h>

/***************************************************************************
  Local Types & Defines
***************************************************************************/
static bool popOtaEnabled = true;

/***************************************************************************
  Local Prototypes
***************************************************************************/
void PopNwkAddNewRouteRequest(sPopNwkDataIndication_t *pIndication, uint8_t iEntry);
bool PopNwkAddExistingRouteRequest(sPopNwkDataIndication_t *pIndication, uint8_t iEntry);
void PopNwkAgeRoutes(uint8_t iIndex);

/***************************************************************************
  Globals & Externs
***************************************************************************/
extern popStatus_t  giPopNwkStateStatus;          // what status to report to user?
extern sPopOtaUpgradeImageInfo_t gsPopOtaUpgradeInfo;
extern void PopNwkStatusFailed(popStatus_t iStatus);

/***************************************************************************
  Code
***************************************************************************/

/**
 * Set if PopNet OTA shall be enabled
 * @param enabled If enabled
 */
void PopNwkMgmtSetOtaEnabled(const bool enabled) {
  popOtaEnabled = enabled;
}

/*
  PopNwkMgmtIndication

  All network managament commands come in through PopNwkMgmtIndication. It is 
  similar to the the app handling data indications, but for network management 
  commands.
*/
void PopNwkMgmtIndication(sPopNwkDataIndication_t *pIndication)
{
  sPopNwkMgmtCmd_t  *pMgmtCmd;
  popNwkAddr_t      iSrcAddr;

  // if the node is no longer in the network it can not send anything OTA
  if(!PopNwkIsOnNetwork())
    (void)PopMemFree(pIndication);

  pMgmtCmd = PopNwkPayload(pIndication);
  iSrcAddr = pIndication->sFrame.sNwk.iSrcAddr;

  PopSetMessageEvent(gPopMessageRadioReceived_c, pIndication->sFrame.aPayload);

  switch(pMgmtCmd->iCommandId)
  {
    // another node would like some addresses from this node
    case gPopNwkMgmtAddrRequest_c:
      PopNwkMgmtSendAddrPoolResponse(iSrcAddr, &pMgmtCmd->sAddressRequest);
    break;

    // another node has sent addresses to this node in response to a request from this node
    case gPopNwkMgmtAddrResponse_c:
      PopNwkMgmtProcessAddressPoolRsp(&pMgmtCmd->sAddressResponse);
    break;

    // process join enable indication
    case gPopNwkMgmtJoinEnable_c:
      PopNwkMgmtSetJoinEnable(pMgmtCmd->sJoinEnable.fEnable);
    break;

#if 0
    // Route replies are processed in PopNwkRouteFrame().
    // process a route reply
    case gPopNwkMgmtRouteReply_c:
      PopNwkMgmtRouteReply(pIndication);
    break;
#endif

    // process a find node request (received OTA)
    case gPopNwkMgmtFindNodeRequest_c:
      PopNwkMgmtFindNodeRequest(iSrcAddr, &pMgmtCmd->sFindNodeRequest);
    break;

    // process a find node response (received from a found node)
    case gPopNwkMgmtFindNodeResponse_c:
      PopNwkMgmtFindNodeResponse(iSrcAddr);
    break;

    // OTA Upgrades commands are optional. May be disabled in PopCfg.h (and stubbed in PopCfg.c)
    case gPopGwNwkMgmtOtaUpgradeFirstSaveReq_c:
    case gPopGwNwkMgmtOtaUpgradeSaveReq_c:
    case gPopGwNwkMgmtOtaUpgradeSaveRsp_c:
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c:
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c:
    case gPopGwNwkMgmtOtaUpgradeCancelReq_c:
    case gPopGwNwkMgmtOtaUpgradeCancelRsp_c:
    case gPopGwNwkMgmtOtaUpgradeCompleteReq_c:
    case gPopGwNwkMgmtOtaUpgradeCompleteRsp_c:
      if (popOtaEnabled) {
        PopNwkMgmtProcessOtaUpgrade(pIndication);
      }
    break;
  default:
    break;
  }

  // all done, free the indication
  (void)PopMemFree(pIndication);
}

/*
  PopNwkMgmtTimerHandler

  This function concentrates the timer for the network layer.

  Receives the timer id who has expired.

  Returns nothing.
*/
void PopNwkMgmtTimerHandler(popTimerId_t iTimerId)
{
  switch (iTimerId)
  {
    // scan next channel (if any)
    case gPopNwkScanTimer_c:
      PopNwkScanNextChannel();
      break;

    // couldn't find a node to join. Try the ScanForNetworks again after waiting a bit.
    case gPopNwkJoinTimer_c:
      (void)PopSetEventEx(cNwkTaskId, gPopEvtNwkNextStartState_c, NULL);
      break;

    // MAC association request didn't respond. Try again.
    case gPopNwkJoinRspTimer_c:
      PopNwkMgmtJoinRequest();      
      break;

    case gPopNwkJoinConfirmTimer_c:
      {
        sPopEvtData_t  sEvtData;
        sEvtData.iNwkStartConfirm = giPopNwkStateStatus;
        (void)PopSetAppEvent(gPopEvtNwkStartConfirm_c, sEvtData.pData);
      }
      break;

    // time to send out the beacon response (to a beacon request)
    case gPopNwkSendBeaconTimer_c:
      PopNwkSendBeacon();
      break;
    
    // Throttling the image for OTA upgrade
    case gPopOtaUpgradeThrottleTimer_c:
      PopOtaUpgradeStateMachine();
      break;
    case gPopOtaUpgradeCompleteTimer_c:
      if(PopGetOtaUpgradeState()== ePopOtaWaitForCompleteRsp)
        PopSetOtaUpgradeState(ePopOtaIdle);
      else
        PopNwkMgmtDataRequest((sPopNwkDataRequest_t *)&(gsPopOtaUpgradeInfo.imageCopy),false);
      break;
    default:
      break;
  }

}


/*
  PopNwkMgmtSetJoinEnabled  (Internal Function)

  Set the current status of the join capability. Receives a boolean value to be set as 
  the current join capability. This may be called by the higher layer (app), or from an 
  OTA packet.
*/
void PopNwkMgmtSetJoinEnable(bool fEnable)
{
  sPopEvtData_t     sEvtData;

  // disable joining if we're sleepy or we've just run out of addresses
  if( /*PopNwkIsSleepyNode() ||*/ (PopNwkIsOnNetwork() && !PopNwkGetAddrCount()) )  // #JJ enable joining also if routing disabled
    fEnable = false;

  // set the join status
  PopNwkSetJoinStatus(fEnable);

  // Let the application know about the change in the state.
  sEvtData.iNwkJoinEnabled  = fEnable;
  (void)PopSetAppEvent(gPopEvtNwkJoinEnable_c, sEvtData.pData);

  // Inform that the Nwk data has change, so the aplpication can act on this if it wants to.
  // #JJ removed to avoid writing to flash
  //PopNvmDataMarkAsChanged(gPopNvmNwkDataChanged_c);
}

/*
  PopNwkJoinEnable  (Public Function)

  Turn join enable on or off, for this local node only or network wide.
*/
void PopNwkJoinEnable(bool fEnable, bool fNetworkWide)
{
  sPopNwkDataRequest_t sDataRequest;
  sPopNwkMgmtJoinEnableRequest_t  sJoinCmmd;

  // set join enable locally (always)
  PopNwkMgmtSetJoinEnable(fEnable);

  // set up the data request
  if(fNetworkWide)
  {
    sJoinCmmd.iCommandId = gPopNwkMgmtJoinEnable_c;
    sJoinCmmd.fEnable = fEnable;
    sDataRequest.iDstAddr = gPopNwkBroadcastAddr_c;
    sDataRequest.iPayloadLength = sizeof(sJoinCmmd);
    sDataRequest.pPayload = &sJoinCmmd;
    sDataRequest.iRadius = PopNwkGetDefaultRadius();
    sDataRequest.iOptions = gPopNwkDataReqOptsDefault_c;

    // request the other end toggle
    PopNwkMgmtDataRequest(&sDataRequest, false);
  }
}

/*
  PopNwkAddrPoolRequest [Public Command]

  Ask over-the-air for a set of addresses from an address server. The iDstAddress should 
  be the address server (node 0x0000) by default.
  
  The results will be an address response which will replace the current address pool 
  with the address response. The address server may have less than "iCount" addresses, in 
  which case the response will reflect how many.
*/
void PopNwkAddrPoolRequest(popNwkAddr_t iDstAddress, popAddrCount_t iCount)
{
  sPopNwkDataRequest_t      sDataRequest;
  sPopNwkMgmtAddrRequest_t  sAddrReq;

  // if the node is not in the network it can not send any request.
  // note: this call must work during joining, so it works even if in the NWK state machine.
  if(PopNwkIsOffNetwork())
  {
    PopNwkSendAppDataConfirm(gPopErrNoNwk_c);
    return;
  }

  // set up the OTA request
  sDataRequest.iPayloadLength = sizeof(sPopNwkMgmtAddrRequest_t);
  sDataRequest.iDstAddr = iDstAddress;
  sDataRequest.iOptions = gPopNwkDataReqOptsDefault_c;  // gPopNwkDataReqOptsUnibroadcast_c;
  sDataRequest.iRadius = PopNwkGetDefaultRadius();

  // set up the OTA payload
  sAddrReq.iCommandId = gPopNwkMgmtAddrRequest_c;
  sAddrReq.iAddrCount = iCount;
  NativeToOta16(sAddrReq.iAddrCount);
  sDataRequest.pPayload = &sAddrReq;

  // send address request to address server
  (void)PopNwkMgmtDataRequest(&sDataRequest, false);
}

/*
  PopNwkMgmtSendAddrPoolResponse

  Send back OTA an address which is the the first address of the requested set,
  and the amount of addresses asigned to the requesting node.

  Receives the the address of the requesting node and the command with the whole
  request.

  Returns nothing.
*/
void PopNwkMgmtSendAddrPoolResponse(popNwkAddr_t iSrcAddr, sPopNwkMgmtAddrRequest_t  *pAddrReq)
{
  sPopNwkDataRequest_t  sDataRequest;
  sPopNwkMgmtAddrResponse_t  sAddressResponse;
  popAddrCount_t iAddrCount;

  // create a local varible to work around compiler bug...
  iAddrCount = pAddrReq->iAddrCount;
  OtaToNative16(iAddrCount);

  // responding back to the sender
  sDataRequest.iDstAddr = iSrcAddr;
  sDataRequest.iOptions = gPopNwkDataReqOptsDefault_c;  // gPopNwkDataReqOptsUnibroadcast_c;
  sDataRequest.iRadius = PopNwkGetDefaultRadius();
  sDataRequest.iPayloadLength = sizeof(sPopNwkMgmtAddrResponse_t);

  // this will also inform the application that the local addr pool has changed
  sAddressResponse.iFirstAddress = PopNwkMgmtGetAddrFromPool(&iAddrCount);

  // sAddressResponse.iAddrCount = iAddrCount;
  sAddressResponse.iCommandId = gPopNwkMgmtAddrResponse_c;
  sAddressResponse.iAddrCount = iAddrCount;
  NativeToOta16(sAddressResponse.iAddrCount);
  NativeToOta16(sAddressResponse.iFirstAddress);
  sDataRequest.pPayload = &sAddressResponse;

  // send the data out OTA.
  (void)PopNwkMgmtDataRequest(&sDataRequest, false);
}

/*
  PopNwkMgmtProcessAddressPoolRsp

  Received an address response from the address server. Sets up the local 
  address pool based on the response recieved OTA from the address server.
*/
void PopNwkMgmtProcessAddressPoolRsp(sPopNwkMgmtAddrResponse_t *pAddressResponse)
{
  sPopEvtData_t sEvtData;

  // convert from OTA endian to native endian
  OtaToNative16(pAddressResponse->iFirstAddress);
  OtaToNative16(pAddressResponse->iAddrCount);

  // set up our address pool
  PopNwkSetNextAddrAvailable(pAddressResponse->iFirstAddress);
  PopNwkSetAddrCount(pAddressResponse->iAddrCount);

  // turn off join enable if no addresses
  if (!PopNwkGetAddrCount() || !PopNwkIsValidNwkAddress(PopNwkGetNextAddrAvailable()))
    PopNwkMgmtSetJoinEnable(false);

  // inform the application that the address response has arrived, and the address pool has changed
  sEvtData.iAddrCount = PopNwkGetAddrCount();
  (void)PopSetAppEvent(gPopEvtNwkAddrPoolConfirm_c, sEvtData.pData);
}


/*
  PopNwkMgmtGetAddrFromPool

  This function handles the address pool of the current device, it does not
  matter if it is the address server or any node with available addresses.

  On Input, *pNumOfAddress is the amount of addresses to extract from the
  address pool.

  On output, *pNumOfAddress will contain the amount of addresses extracted, and 
  the starting address will be updated accordingly. If the total # of addresses 
  available are less than the requested amount, then *pNumOfAddress will contain
  the number available.

  Returns the first address of the requested set.
*/
popNwkAddr_t PopNwkMgmtGetAddrFromPool(popAddrCount_t  *pNumOfAddress)
{
  sPopEvtData_t sEvtData;

  // By default the returning address is an invalid one, we do not know if the
  // address pool has something available.
  popNwkAddr_t  iAddress = gPopNwkInvalidAddr_c;
  popNwkAddr_t  iNextAddr = gPopNwkInvalidAddr_c;

  // we have run out of addresses. return invalid address.
  if (!PopNwkIsValidUnicastAddress(PopNwkGetNextAddrAvailable()) || !PopNwkGetAddrCount())
  {
    return iAddress;    // returns invalid address
  }

  // we must have at least 1 address available at this point
  iAddress = PopNwkGetNextAddrAvailable();

  // gt the requested addresses. 
  if (*pNumOfAddress > PopNwkGetAddrCount())
    *pNumOfAddress = PopNwkGetAddrCount();
  PopNwkReduceAddrCount(*pNumOfAddress);

  // move local pool start (if we have any addresses left)
  if (PopNwkGetAddrCount())
    iNextAddr = iAddress + *pNumOfAddress;

  // update local pool start (to either invalid or new start)
  PopNwkSetNextAddrAvailable(iNextAddr);

  // inform the application that we got addresses from the address pool 
  sEvtData.iAddrCount = PopNwkGetAddrCount();
  (void)PopSetAppEvent(gPopEvtNwkAddrPoolChanged_c, sEvtData.pData);  

  // Inform the NVM engine that network data has changed
  PopNvmDataMarkAsChanged(gPopNvmNwkDataChanged_c);

  return iAddress;
}

/*
  PopNwkMgmtJoinResponse  (Internal Function)

  Receives the incoming association (join) request. Sends an event to the application to allow
  the app some flexibility in joining.

  See also PopNwkJoinIndication().
*/
void PopNwkMgmtJoinResponse(aPopMacAddr_t aDstIeeeAddr)
{
  sPopEvtData_t sEvtData;
  bool fAllow;

  // If the node is off the network, don't respond
  if (!PopNwkIsOnNetwork())
    return;

  // allocate and fill in structure
  sEvtData.pNwkJoinIndication = PopMemAlloc(sizeof(sPopNwkJoinIndication_t));
  if(!sEvtData.pNwkJoinIndication)
    return;

  // Let app decide if this node may join or not (and which short address to get)
  // By default:
  // 0xffff = use Addr Pool (if joining enabled).
  // 0xfffe = don't allow (if joining disabled). 
  fAllow = PopNwkGetJoinStatus() && PopNwkGetAddrCount() ? true : false;
  sEvtData.pNwkJoinIndication->iNwkAddr = fAllow ? gPopNwkBroadcastAddr_c : gPopNwkInvalidAddr_c;
  PopMemCpy(sEvtData.pNwkJoinIndication->aMacAddr, aDstIeeeAddr, sizeof(aPopMacAddr_t));

  // send the join indication to the app, 
  // which will normally send it back to the NWK task, via PopNwkJoinIndication
  (void)PopSetAppEvent(gPopEvtNwkJoinIndication_c, sEvtData.pNwkJoinIndication);
}

/*
  PopNwkJoinIndication (Internal Function)

  A node is attempting to join this node. The application has already had a chance to
  look at this and (optionally) assign a short address. Now send the association response
  to the joining node.

  The app may set iNwkAddr to:
  0x0000 - 0xfff0  = NwkAddr (short address) that will be assigned to joining node
  0xffff           = PopNet will chose an address from address pool (default)
  0xfffe           = Prevent node from joining   

  See also PopNwkMgmtJoinResponse().
*/
void PopNwkJoinIndication(sPopNwkJoinIndication_t *pJoinIndication)
{
  popAddrCount_t iCount;
  popNwkAddr_t iNewAddr;

  // determine a valid (unique) short address for the child
  iNewAddr = pJoinIndication->iNwkAddr;

  // application has chosen to let PopNet get an address from the address pool  
  if(iNewAddr == gPopNwkBroadcastAddr_c)
  {
    iCount = 1;
    iNewAddr = PopNwkMgmtGetAddrFromPool(&iCount);
  }

  // send the response only if valid address
  if(PopNwkIsValidUnicastAddress(iNewAddr))
    PopNwkSendAssociationResponse(pJoinIndication->aMacAddr, iNewAddr);

  // done with join indication
  (void)PopMemFree(pJoinIndication);
}

/*
  PopNwkSetChannel

  Sets both the global channel variable, and the physical radio channel to the specified channel.
  Channel range is 11 - 26.
*/
void PopNwkSetChannel(popChannel_t iChannel)
{
  // don't set to invalid channel
  if (iChannel != 0 && !PopNwkIsValidChannel(iChannel))
    iChannel = gPopNwkFirstChannel_c;

  // Set the channel in the NWK data information
  PopNwkSetNwkDataChannel(iChannel);

  // Set the channel at the PHY level.
  PopPhySetChannel(iChannel);
}

// Compares 8 bytes to be equal or not.
bool PopNwkIsEqual8Bytes(void *pPtr1, void *pPtr2)
{
  if (PopMemCmp(pPtr1, pPtr2, 8) == 0)
    return true;

  return false;
}

/*
  PopNwkMgmtJoinRequest (internal function)

  Set up the inputs, then call this to join the network.

  Inputs:
  -------------------
  giPopNwkStateParent           the parent node to try to join
  giPopNwkStateJoinTriesLeft    the # of times to attempt joining
  gsPopNwkData.iChannel         the channel on which to join
  gsPopNwkData.iPanId           the PAN ID to associate with

  Will send either gPopStatusJoinFailure_c or gPopStatusSuccess_c to the PopNwkStartNetworkStateMachine().
  See also PopNwkMgmtJoinResponse(). The expired timer just calls this same function.
*/
void PopNwkMgmtJoinRequest(void)
{
  // if there are not more attemps then notify the failure
  if( !giPopNwkAssociateTriesLeft )
  {
    (void)PopStopTimerEx(cNwkTaskId, gPopNwkJoinRspTimer_c);

    // terminate the state machine with a join failure
    PopNwkStatusFailed(gPopStatusJoinFailure_c);
    return;
  }

  // try again. send out the association request
  PopNwkSendAssociationRequest(PopNwkGetPanId(), giPopNwkStateParent);

  // Set the timer to control the attempts
  (void)PopStartTimerEx(cNwkTaskId, gPopNwkJoinRspTimer_c, giPopNwkJoinExpired + PopNwkJitter16(giPopNwkReJoinJitter));

  // decrement the attempts variable
  --giPopNwkAssociateTriesLeft ;
  
}

/*
  PopNwkMgmtProcessJoinRsp

  Receives the join responses packet and process it. it has only two error
  conditions: If the node is already in the network the packet will be ignored,
  or if the IEEE icomming back in the response packet is not the current one.

  Receives the address of the node responding and the response packet.

  Returns nothing.
*/
void PopNwkMgmtProcessJoinRsp(popNwkAddr_t iDstAddr)
{
  // Avoid processing join responses if the node is already on the network.
  if (!PopNwkIsJoiningOrForming())
    return;

  // Stop the waiting timer to prevent the join retries.
  (void)PopStopTimerEx(cNwkTaskId, gPopNwkJoinTimer_c);
  (void)PopStopTimerEx(cNwkTaskId, gPopNwkJoinRspTimer_c);
  
  // we got a valid address, set it.
  PopNwkSetNodeAddr(iDstAddr);

  // tell the state machine we have joined, successfully (which will in turn tell the app)
  (void)PopSetEventEx(cNwkTaskId, gPopEvtNwkNextStartState_c, NULL);
}


/**********************************************************
  PopNet Route Requests/Replies And Related Functions
***********************************************************/

/*
  (NWK Internal Only)

  This function process route replies.

  A route reply must be ACKed by the next node in the route, otherwise an alternate route will be checked.
  It will try each of the top 3 "best" routes before giving up.
*/
void PopNwkProcessRouteReplyFrame(sPopNwkDataIndication_t *pIndication)
{
  sPopEvtData_t sEvtData;
  uint8_t i;

  // find the route discovery table entry for processing
  // if we don't have a route discovery table entry for this RREP, we're done
  i = PopNwkFindRouteReq(pIndication->sFrame.sNwk.iDstAddr, pIndication->sFrame.sNwk.iSequence, NULL);
  if(gPopEndOfIndex_c == i)
    return;

  // Notify to the APP Layer that a Route Reply has passed thru us 
  if( pIndication->sFrame.sNwk.iDstAddr != PopNwkGetNodeAddr() )
  {
     sEvtData.iNwkAddr = pIndication->sFrame.sNwk.iSrcAddr;
    (void)PopSetAppEvent(gPopEvtNwkRoutePassThru_c, sEvtData.pData);
  }
    
  // go on to next state (send out the RREP to next destination, or back to parent)
  PopNwkProcessNextRREPState(i, pIndication->sFrame.sMac.iSrcAddr);
}

/*
  (NWK Internal Only)

  Deal with route replies.
*/
void PopNwkProcessNextRREPState(uint8_t iDiscoveryTableIndex, popNwkAddr_t iMacSrcAddr)
{
  uint8_t iPrevHopIndex;
  sPopNwkRouteDiscovery_t *pEntry;
  popNwkAddr_t iNwkSrcAddr;
  popNwkAddr_t iNwkDstAddr;
  popNwkAddr_t iPrevHop = gPopNwkInvalidAddr_c;
  sPopEvtData_t sEvtData;
  
  // get pointer to the route discovery table entry
  pEntry = &gaPopNwkRouteDiscoveryTable[iDiscoveryTableIndex];

  // nothing to do if we're a sleepy node (and this isn't the ultimate source or destination)
  iNwkSrcAddr = pEntry->iSrcAddr;
  iNwkDstAddr = pEntry->iDstAddr;
  if(PopNwkIsSleepyNode() && iNwkSrcAddr != PopNwkGetNodeAddr() && iNwkDstAddr != PopNwkGetNodeAddr())
    return;

  // if we are the originating node of this RREQ/RREP, just record the route and we're done
  if(iNwkSrcAddr == PopNwkGetNodeAddr())
  {
    // Notify to app layer that we discovered the route (next hop address is included in the confirm)
    sEvtData.iRouteConfirm = iMacSrcAddr;
    (void)PopSetEventTo(false,gPopEvtNwkRouteConfirm_c,sEvtData.pData);
    
    PopNwkAddRoute(iNwkDstAddr, iMacSrcAddr);
    PopNwkFreePendingRetryEntries(iDiscoveryTableIndex, true);
    return;
  } else {
    PopNwkFreePendingRetryEntries(iDiscoveryTableIndex, true);
  }

  // if this is the first route reply we've received, consider it our parent node (the root has no parent)
  if(PopNwkGetNodeAddr() != pEntry->iDstAddr && pEntry->iRRepSrcAddr == gPopNwkInvalidAddr_c)
    pEntry->iRRepSrcAddr = iMacSrcAddr;

  // no longer waiting to start (if we were)
  pEntry->iFlags.fWaitingToStart = false;

  // find the next item in the list to send the RREP token to
  iPrevHop = pEntry->iRRepSrcAddr;
  iPrevHopIndex = pEntry->iPrevHopIndex;
  if(iPrevHopIndex < cPopNwkAsymmetricLinkEntries)
  {
    // if we don't have a valid previous hop, then stop (we're at the end of the list)
    if(!PopNwkIsValidUnicastAddress(pEntry->aLinkEntry[iPrevHopIndex].iPrevHop))
      pEntry->iPrevHopIndex = cPopNwkAsymmetricLinkEntries;

    else
    {
      iPrevHop = pEntry->aLinkEntry[iPrevHopIndex].iPrevHop;
      ++pEntry->iAckTries;
      if(pEntry->iAckTries >= giPopNwkMaxRetries)
      {
        pEntry->iAckTries = 0;
        ++pEntry->iPrevHopIndex;
      }
    }
  }

  // send the route reply (if we haven't run out at the root)
  // remember, the NwkSrcAddr/NwkDstAddr are reversed on the way back
  if(PopNwkIsValidUnicastAddress(iPrevHop))
  {
    // indicate in state machine to wait
    pEntry->iTimeAckWait = giPopNwkAckExpired;
    pEntry->iFlags.fWaitingOnAck = true;

    PopNwkSendRouteReply(iPrevHop, pEntry->iDstAddr, pEntry->iSrcAddr, pEntry->iSequence);
  }
}

/*
  (NWK Internal Only)

  Sends a route reply out the radio to another node. The route reply packet (a NWK mgmt packet) contains:

  Parameters
  iPrevHop        hop to send this RREP to
  iNwkSrcAddr     source addr for RREP (=DstAddr for RREQ)
  iNwkDstAddr     destination addr for RREP (=SrcAddr for RREQ)
  iSequence       sequence # used in RREQ
*/
void PopNwkSendRouteReply(popNwkAddr_t iPrevHop, popNwkAddr_t iNwkSrcAddr, popNwkAddr_t iNwkDstAddr, popSequence_t iSequence)
{
  sPopNwkDataIndication_t *pOtaFrame; 
  sPopNwkDataRequest_t sDataReq;
  popNwkRetryFlags_t iFlags = { 0 };
  static sPopNwkMgmtRouteReply_t sMgmtRouteReply = { gPopNwkMgmtRouteReply_c, 0 };

  /* create the RREP frame */
  sDataReq.iDstAddr = iNwkDstAddr;
  sDataReq.iOptions = gPopNwkDataReqOptsNoDiscover_c | gPopNwkDataReqOptsMgmt_c;
  sDataReq.iRadius  = 0;  // default
  sDataReq.iPayloadLength = sizeof(sMgmtRouteReply);
  sDataReq.pPayload = &sMgmtRouteReply;
  sMgmtRouteReply.iSequence = iSequence;
  pOtaFrame = PopNwkCreateDataFrame(&sDataReq, iFlags);

  /* if we don't have memory to create the frame, give up. The caller will timeout and try again */
  if(!pOtaFrame)
    return;

  /* PopNwkCreateDataFrame() modified the global sequence number. Set it back. */
  --giPopNwkSequence;

  /* modify the RREP frame for where we actually want it to go */
  pOtaFrame->sFrame.sMac.iDstAddr = iPrevHop;
  pOtaFrame->sFrame.sNwk.iSrcAddr = iNwkSrcAddr;
  pOtaFrame->sFrame.sNwk.iSequence = iSequence;

  /* send the RREP packet */
  PopNwkConvertOutgoingOtaFrame(pOtaFrame);
  PopNwkRawRadioTransmit(pOtaFrame);

  /* done, free the memory */
  (void)PopMemFree(pOtaFrame);
}


/*
  PopNwkFindRouteReq

  Find a route request based on NWK srcAddr and iSequence (which uniquely 
  identifies this request).

  Returns the index to the route request, or gPopEndOfIndex_c if not found.
*/
uint8_t PopNwkFindRouteReq(popNwkAddr_t iSrcAddr, popSequence_t iSequence, uint8_t *piFree)
{
  uint8_t i;
  uint8_t iFreeEntry;

  // find this route request
  iFreeEntry = gPopEndOfIndex_c;
  if(piFree)
    *piFree = iFreeEntry;

  for(i=0; i < cPopNwkRouteDiscoveryEntries; ++i)
  {
    // find the right route reply (the usual unique identifier, source addr, seq #)
    if(gaPopNwkRouteDiscoveryTable[i].iFlags.fIsInUse)
    {
      if(gaPopNwkRouteDiscoveryTable[i].iSrcAddr == iSrcAddr &&
        gaPopNwkRouteDiscoveryTable[i].iSequence == iSequence)
      {
        return i;
      }
    }

    // have we found a free entry?
    else if(iFreeEntry == gPopEndOfIndex_c)
      iFreeEntry = i;
  }

  // does user want free entry?
  if(piFree)
    *piFree = iFreeEntry;

  // not found
  return gPopEndOfIndex_c;
}

/*
  PopNwkLinkCostFromLqi

  Determine path cost from LQI. 1-0x50=poor, 0x51-0x90=medium, 0x91-0xff=good, 0-from this node
*/
popNwkPathCost_t PopNwkLinkCostFromLqi(uint8_t iLqi)
{
  popNwkPathCost_t iPathCost;

  if(iLqi > gaPopNwkLqi[1])
    iPathCost = 1;    // 1 = good route 
  else if(iLqi > gaPopNwkLqi[0])
    iPathCost = 3;    // 3 = medium route
  else if(iLqi)
    iPathCost = 5;    // 5 = poor route
  else
    iPathCost = 0;    // 0 = originating node
  return iPathCost;
}

/*
  (NWK Internal)

  Process a request. Has different behavior if we want new vs. existing...

  Each route discovery table entry will keep track of 3 possible "best" routes, and will 
  try each one in turn until it finds a symmetrical link on the route replies.

  The pIndication, and the RetryEntry associated with this route request MUST be in 
  native form at the time this is called.

  Returns gPopErrNone_c if added a new entry, gPopErrFull_c if no room to add the route 
  request, or gPopErrAlreadyThere_c if it's existing.
*/
popErr_t PopNwkProcessRouteRequest(sPopNwkDataIndication_t *pIndication, uint8_t *pIndex)
{
  uint8_t i;              // the route discovery table entry index
  uint8_t iFreeEntry;     // if not found, the first free index
  uint8_t iRetryEntry;
  bool  fBestPathCost;
  popNwkPathCost_t iPathCost;
  popSequence_t iSequence;
  popNwkAddr_t  iNwkAddr;
  popErr_t iErr = gPopErrNone_c;
  sPopNwkRetryTable_t *pRetryEntry;

  // don't process for sleepy nodes unless from us or for us
  // that is, allow a sleepy node to be the originator or destination of a route discovery, but do not participate in the middle
  if(PopNwkIsSleepyNode() && !(pIndication->sFrame.sNwk.iDstAddr == PopNwkGetNodeAddr() || pIndication->sFrame.sNwk.iSrcAddr == PopNwkGetNodeAddr()))
    return gPopErrNone_c;

  // add in path cost for this link (based on LQI). This modifies the incoming data indication.
  // path cost must be modified in the indication BEFORE adding to the updating RREQ table
  pIndication->sFrame.sMac.iPathCost += PopNwkLinkCostFromLqi(pIndication->sPre.iLqi);
  iPathCost = pIndication->sFrame.sMac.iPathCost;
  iNwkAddr  = pIndication->sFrame.sNwk.iSrcAddr;
  iSequence = pIndication->sFrame.sNwk.iSequence;

  // did we find the RouteReq entry already in our route discovery table?
  i = PopNwkFindRouteReq(iNwkAddr, iSequence, &iFreeEntry);

  // add the new entry (if possible)
  if(gPopEndOfIndex_c == i)
  {
    // table full, we cannot add a new route request
    if(gPopEndOfIndex_c == iFreeEntry)
      return gPopErrFull_c;

    // add the new entry. Also starts up the state machine
    i = iFreeEntry;
    PopNwkAddNewRouteRequest(pIndication, i);
    fBestPathCost = true;
  }

  // add to the existing entry if it's a better path cost
  else
  {
    // add path cost to existing route discovery table
    fBestPathCost = PopNwkAddExistingRouteRequest(pIndication, i); 
    iErr = gPopErrAlreadyThere_c;
  }

  // if user wanted it, send back the index to this route discovery table entry
  if(pIndex)
    *pIndex = i;

  // if there is a retry table entry with this route, and it's a better path cost, record it in our outgoing path cost
  iRetryEntry = gaPopNwkRouteDiscoveryTable[i].iRetryEntry;
  if(fBestPathCost && gPopEndOfIndex_c != iRetryEntry)
  {
    // retry entry
    pRetryEntry = &gaPopNwkRetryTable[iRetryEntry];
 
    // if in the retry table, make sure to update the path cost/MAC Addr in the OTA frame
    // but not if we are the destination node. The Dest node uses the retry entry for a delayed data indication
    // note: the associated retry entry is already in native (not OTA) form.
    if(!pRetryEntry->iFlags.fWaitDataInd && iPathCost <= gaPopNwkRouteDiscoveryTable[i].aLinkEntry[0].iPathCost)
    {
      // convert if not already in native form
      if(!pRetryEntry->iFlags2.fIsOtaFrameNative)
        (void)PopNwkConvertIncomingOtaFrame(pRetryEntry->pOtaFrame);
      pRetryEntry->iFlags2.fIsOtaFrameNative = true;

      // modify the PathCost/MacSrcAddr in the frame
      pRetryEntry->pOtaFrame->sFrame.sMac.iPathCost = iPathCost;
      pRetryEntry->pOtaFrame->sFrame.sMac.iSrcAddr  = PopNwkGetNodeAddr();
    }
  }

  // worked
  return iErr;
  
}


/*
  (NWK Internal)

  Add the route request. Fills in the path cost of the first entry. This is now the best 
  (and the only).
*/
void PopNwkAddNewRouteRequest(sPopNwkDataIndication_t *pIndication, uint8_t iEntry)
{
  sPopNwkRouteDiscovery_t *pEntry;
  popNwkAddr_t iNwkAddr;
  popSequence_t iSequence;

  // add in the route request
  pEntry = &gaPopNwkRouteDiscoveryTable[iEntry];
  iSequence = pIndication->sFrame.sNwk.iSequence;
  iNwkAddr = pIndication->sFrame.sNwk.iSrcAddr;

  // fill in flags
  pEntry->iFlags.fIsInUse = true;

  // Fill the new entry 
  pEntry->iTimeLeftMs = giPopNwkBroadcastExpired;   // time to expire the entry
  pEntry->iSequence   = iSequence;
  pEntry->iSrcAddr    = iNwkAddr;
  pEntry->iDstAddr    = pIndication->sFrame.sNwk.iDstAddr;
  pEntry->iRRepSrcAddr= gPopNwkInvalidAddr_c;
  pEntry->iRetryEntry = PopNwkFindRetryEntry(iSequence, iNwkAddr, NULL);

  // some fields must be zero, but they already are, as PopNwkFreeRouteDiscoveryEntry() will zero it out

  // mark all RREP entries as unused (0xff)
  PopMemSet(pEntry->aLinkEntry, 0xFF, cPopNwkAsymmetricLinkEntries * sizeof(sPopNwkAsymmetricLinkEntry_t));

  /*
    if this is the root node for RREPs (the dest node for route discovery), then wait 
    awhile before starting the RREP process this allows us to gather better entries 
    before proceeding
  */
  if(pEntry->iDstAddr == PopNwkGetNodeAddr())
  {
    pEntry->iFlags.fWaitingToStart = true;
    pEntry->iTimeAckWait = giPopNwkWaitRREPStartup;

    // delete the route to the source node. we don't know how to get there anymore...
    PopNwkDeleteRoute(iNwkAddr);
  }

  // keep track of adding for instrumentation
  giPopNwkRouteDiscoveryCount++;
  if (giPopNwkRouteDiscoveryCount > giPopNwkRouteDiscoveryHighWaterMark)
    giPopNwkRouteDiscoveryHighWaterMark = giPopNwkRouteDiscoveryCount;
  
  // if we ever over run we need toset the event
  if (giPopNwkRouteDiscoveryCount >= cPopNwkRouteDiscoveryEntries)
    (void)PopSetTableFullEvent(gPopRouteDiscoveriesTableFull_c);

  // add in the path cost of this first entry
  (void)PopNwkAddExistingRouteRequest(pIndication, iEntry);
}

/*
  (NWK Internal) 

  We have an existing route discovery entry. Add to its table if this is indication 
  inlcludes a better path cost.

  There can be only three (only if this is better will it be added).

  returns true if this was a new best path cost (slot 0). false if not.
*/
bool PopNwkAddExistingRouteRequest(sPopNwkDataIndication_t *pIndication, uint8_t iEntry)
{
  uint8_t   i;
  uint8_t   iBestSlot;
  popNwkAddr_t            iPrevHop;
  popNwkPathCost_t        iPathCost;
  sPopNwkRouteDiscovery_t *pEntry;
  sPopNwkAsymmetricLinkEntry_t *pLinkEntry;

  // get pointer to entry (code size reduction)
  pEntry = &gaPopNwkRouteDiscoveryTable[iEntry];

  // get the prevhop/path cost of incoming RREQ (code size reduction)
  iPrevHop  = pIndication->sFrame.sMac.iSrcAddr;
  iPathCost = pIndication->sFrame.sMac.iPathCost;

  // search for the best place to add this incoming entry (if any)
  iBestSlot = gPopEndOfIndex_c;
  for(i=0; i < cPopNwkAsymmetricLinkEntries ; ++i)
  {
    pLinkEntry = &pEntry->aLinkEntry[i];

    // if we found a matching previous hop, then reuse that entry
    if(pLinkEntry->iPrevHop == iPrevHop)
    {
      // but only if it's better
      if(iPathCost < pLinkEntry->iPathCost)
        iBestSlot = i;

      // path cost was worse (or same), ignore it
      else
        iBestSlot = gPopEndOfIndex_c;
      break;
    }

    // record the path cost if it's better
    if(iBestSlot == gPopEndOfIndex_c && iPathCost < pLinkEntry->iPathCost)
      iBestSlot = i;
  }

  // if the incoming RREQ is a better path cost, insert it in the right place
  if(iBestSlot != gPopEndOfIndex_c)
  {
    // make room for this entry so they stay in sorted order, with lowest path cost first
    pLinkEntry = &pEntry->aLinkEntry[iBestSlot];
    PopMemMove(pLinkEntry + 1, pLinkEntry, ((cPopNwkAsymmetricLinkEntries - 1) - iBestSlot) * sizeof(sPopNwkAsymmetricLinkEntry_t));

    pLinkEntry->iPrevHop = iPrevHop;
    pLinkEntry->iPathCost = iPathCost;
  }

  // not a better path cost, didn't add it
  return (iBestSlot == 0);
}

/*
  PopNwkAddRoute

  Adds a route to the routing table. If there is a pending route to this destination, 
  then that route entry is used. If the destination already exists, then that route is 
  used. If there are no routes available, the oldest one is retired.

  Cannot fail.  
*/
void PopNwkAddRoute(popNwkAddr_t iDstAddr, popNwkAddr_t iNextHop)
{
  uint8_t i;
  uint8_t iFreeEntry;
  uint8_t iOldest;
  sPopNwkRouteEntry_t *pEntry;

  // add the route to the table
  iFreeEntry = iOldest = gPopEndOfIndex_c;
  for (i = 0; i < giPopNwkRouteEntries; i++)
  {
    pEntry = &gaPopNwkRouteTable[i];

    // record the first free route
    if(!pEntry->iRouteAge)
    {
      if(iFreeEntry == gPopEndOfIndex_c)
        iFreeEntry = i;

      //  Process the next entry
      continue;
    }

    // if this route is already to this destination, replace it
    // doesn't matter if the route is current valid or pending.
    if (pEntry->iDstAddr == iDstAddr)
    {
      iFreeEntry = i;
      continue;
    }

    // Age only the active ones. If route is too old it will expire.
    pEntry->iRouteAge++;

    // not for us, is this the oldest one?
    if (iOldest == gPopEndOfIndex_c || (gaPopNwkRouteTable[iOldest].iRouteAge < pEntry->iRouteAge))
    {
      iOldest = i;
    }
  } //For

  // if we didn't find the entry, use the free one.
  // if no free entry, use the oldest one.
  // there MUST be either a free one or oldest one (can not fail)
  i = (iFreeEntry != gPopEndOfIndex_c)? iFreeEntry: iOldest;
  pEntry = &gaPopNwkRouteTable[i];

  // fill out the entry
  pEntry->iDstAddr  = iDstAddr;
  pEntry->iFlags    = gPopNwkRouteValid_c;
  pEntry->iNextHop  = iNextHop;
  pEntry->iRouteAge = 1;  // The youngest age possible.

  // Inform the application that the network data has changed
  // We don't want to store routes in NV flash
  //PopNvmDataMarkAsChanged(gPopNvmRoutesChanged_c);
}

/*
  PopNwkNextHop

  (For Internal Nwk Layer Use Only)

  Determine next hop on the path to this node.
  Returns gPopNwkInvalidAddr_c if no route (yet) to the node.
*/
popNwkAddr_t PopNwkNextHop(popNwkAddr_t iDstAddr)
{
  uint8_t	i;

  // check route table
  for(i=0; i<giPopNwkRouteEntries; ++i)
  {
    if((gaPopNwkRouteTable[i].iFlags & gPopNwkRouteValid_c) && gaPopNwkRouteTable[i].iDstAddr == iDstAddr)
    {
      // Age the other routes.
      PopNwkAgeRoutes(i);
      gaPopNwkRouteTable[i].iRouteAge = 1;  // Set the next hop to the newest route. Just after aging the rest of them.
      return gaPopNwkRouteTable[i].iNextHop;
    }
  }

  // no entry found
  return gPopNwkInvalidAddr_c;
}

/*
  PopNwkAgeRoutes

  (For Internal Nwk Layer Use Only)

  Age all routes but the given one which has been just used
*/
void PopNwkAgeRoutes(uint8_t iIndex)
{
  uint8_t	i;

  for(i=0; i<giPopNwkRouteEntries; ++i)
  {
    if((gaPopNwkRouteTable[i].iFlags & gPopNwkRouteValid_c) && i != iIndex )
    {  
      gaPopNwkRouteTable[i].iRouteAge++;
    }
  }
}

/*
  PopNwkDeleteEntireRouteTable

  Delete all the route from the route table
*/
void PopNwkDeleteEntireRouteTable(void)
{
  PopMemSetLarge(gaPopNwkRouteTable, 0, (uint16_t)giPopNwkRouteEntries * sizeof(sPopNwkRouteEntry_t));
}
 

/*
  PopNwkDeleteRoute

  Delete this route from the list. Can be called by application.
*/
void PopNwkDeleteRoute(popNwkAddr_t iDstAddr)
{
  uint8_t i;
  for (i = 0; i < giPopNwkRouteEntries; i++)
  {
    if (gaPopNwkRouteTable[i].iFlags != gPopNwkRouteFree_c && gaPopNwkRouteTable[i].iDstAddr == iDstAddr)
    {
      gaPopNwkRouteTable[i].iFlags = gPopNwkRouteFree_c;
      gaPopNwkRouteTable[i].iRouteAge = 0;
    }
  }  
}

/*
  PopNwkSetRoute : public

  Adds in a set of routes to the routing table.
*/
void PopNwkSetRoutes(uint8_t iCount, sPopNwkSimpleRoute_t *pRoutes)
{
  uint8_t i;

  for(i=0; i < iCount; ++i)
    PopNwkAddRoute(pRoutes[i].iDstAddr, pRoutes[i].iNextHop);
}

/*
  PopNwkGetRoutes : public

  Returns route entries from the table, up to the maximum.
*/
uint8_t PopNwkGetRoutes(uint8_t iMax, sPopNwkSimpleRoute_t *pRoutes)
{
  uint8_t i;
  uint8_t iCount;

  iCount = 0;
  for(i=0; i<giPopNwkRouteEntries && iCount < iMax; ++i)
  {
    if(gaPopNwkRouteTable[i].iFlags == gPopNwkRouteValid_c)
    {
      pRoutes[iCount].iDstAddr = gaPopNwkRouteTable[i].iDstAddr;
      pRoutes[iCount].iNextHop = gaPopNwkRouteTable[i].iNextHop;
    }
  }
  return iCount;
}

/*
  PopNwkFindNode

  Find one or more nodes on the network. The search criteria is in the sPopNwkFindNode_t.
*/
void PopNwkFindNode(sPopNwkFindNode_t *pFindNode)
{
  sPopNwkDataRequest_t sDataRequest;
  sPopNwkMgmtFindNodeRequest_t sFindReq;

  // don't find anything if no options
  if(!pFindNode->iOptions)
    return;

  // copy relevant fields from pFindNode structure
  sFindReq.iCommandId = gPopNwkMgmtFindNodeRequest_c;
  PopMemCpy(&sFindReq.iOptions, &pFindNode->iOptions, sizeof(sPopNwkMgmtFindNodeRequest_t) - 1);

  // fix endian of app ID (MAC Addr is already little endian) 
  NativeToOta16(sFindReq.iAppId);

  // send the find request OTA
  sDataRequest.iDstAddr = pFindNode->iNwkAddr;
  sDataRequest.iRadius = pFindNode->iRadius;
  sDataRequest.iPayloadLength = sizeof(sFindReq);
  sDataRequest.pPayload = &sFindReq;
  sDataRequest.iOptions = gPopNwkDataReqOptsDefault_c;

  /* At this time the app only calls this PopNwkFindNode function */
  PopNwkMgmtDataRequest(&sDataRequest, false);
}

/*
  Recieved a find node request from OTA. Respond if matches.
*/
void PopNwkMgmtFindNodeRequest(popNwkAddr_t iSrcAddr, sPopNwkMgmtFindNodeRequest_t *pFindReq)
{
  sPopNwkDataRequest_t sDataRequest;
  sPopNwkMgmtFindNodeResponse_t sFindRsp;
  bool fMatch = false;
  popNwkFindOptions_t iOptions;

  // fix endian of app ID (MAC Addr is already little endian) 
  OtaToNative16(pFindReq->iAppId);

  // does it match?
  iOptions = pFindReq->iOptions;
  if((iOptions & gPopNwkFindOptMacAddr_c) && PopNwkIsEqual8Bytes(pFindReq->aMacAddr, PopNwkGetMacAddrPtr()))
    fMatch = true;
  if((iOptions & gPopNwkFindOptAppId_c) && pFindReq->iAppId == PopNwkGetApplicationId())
    fMatch = true;
  if((iOptions & gPopNwkFindOptIdentify_c) && PopNwkGetIdentifyMode())
    fMatch = true;

  // did anything match? If not, do nothing (no response)
  if(!fMatch)
    return;

  // let app know we matched
  (void)PopSetAppEvent(gPopEvtNwkMatchedFindNode_c, (void *)&iSrcAddr);

  // matched. send the find node response
  sFindRsp.iCommandId = gPopNwkMgmtFindNodeResponse_c;

  // send the command back to originating node
  sDataRequest.iDstAddr = iSrcAddr;
  sDataRequest.iPayloadLength = sizeof(sFindRsp);
  sDataRequest.pPayload = &sFindRsp;
  sDataRequest.iRadius = PopNwkGetDefaultRadius();
  sDataRequest.iOptions = gPopNwkDataReqOptsDefault_c;
  PopNwkMgmtDataRequest(&sDataRequest, false);
}

/*
  Received a find node response. Inform the application.
*/
void PopNwkMgmtFindNodeResponse(popNwkAddr_t iSrcAddr)
{
  (void)PopSetAppEvent(gPopEvtNwkFindNodeConfirm_c, (void *)&iSrcAddr);
}

/* EOF */

