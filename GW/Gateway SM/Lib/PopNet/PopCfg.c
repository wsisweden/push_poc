/***************************************************************************
  PopCfg.c
  Copyright (c) 2006-2010 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  This file contains any global data configured through PopCfg.h
  
****************************************************************************/
#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"

/***************************************************************************
  Local Types & Defines (Overrides)
***************************************************************************/
#if gPopOverrideOnMcuType_d

// 2 = MC9S08GT16 (16K ROM, 1K RAM)
#if (gPopTargetMcuType_c == 2)
// The override for the gPopStackSize_c gets done in the popCfg.h file, at the very bottom
// For each board type.

// For small RAM systems, limit beacon indications to 4 (80 bytes)
#define mPopScanForNetworksSize_c         (4 * sizeof(sPopNwkBeaconIndication_t))

// Change the size of the maximum amount of the maximum amount of byte to send over the air.
// allows up to 64-20 = 44 bytes of app payload
#undef  gPopPhySize_c
#define gPopPhySize_c             64

// change the size of the heap to fit in 1K of RAM
#undef  gPopMemHeapSize_c
#define gPopMemHeapSize_c         224  // 3 packets of 64 bytes plus some extra (32 bytes).

#undef  gPopStackSize_c
#define gPopStackSize_c           256   // 256 is enough bytes in a simple app

// The amount of timers get cutted to the half.
#undef  gPopMaxTimers_c
#define gPopMaxTimers_c           8  // each byte entry in the global array is 4 bytes long (8 * 4 = 32bytes)

// change the size for the Event Queue.
#undef  gPopMaxEvents_c
#define gPopMaxEvents_c           16

// The route can be reused and the system can keep replacing the oldest one at any time.
#undef  gPopRouteEntries_c
#define gPopRouteEntries_c        3

// Reducing the amount of neighbors that we can keep track of the counters on a secure system.
// if and only if security is On.
#undef  gPopNwkNeighborEntries_c
#define gPopNwkNeighborEntries_c  3 // Each entry is 7 bytes, keep at least a route for each neighbor

// reduce retry entries to fit into small RAM
#undef  gPopRetryTableEntries_c
#define gPopRetryTableEntries_c        3

// 1 = MC9S08GB32/MC9S08GT32  (32K ROM, 2K RAM)
#elif (gPopTargetMcuType_c == 1)

// For medium sized RAM systems, limit beacon indications to 6 (120 bytes).
#define mPopScanForNetworksSize_c         (6 * sizeof(sPopNwkBeaconIndication_t))

// Change the size of the maximum amount of the maximum amount of byte to send over the air.
// allows up to 96-20 = 76 bytes of app payload
#undef  gPopPhySize_c
#define gPopPhySize_c             96

// Change the size of the heap to fit in 1K of RAM
#undef  gPopMemHeapSize_c
#define gPopMemHeapSize_c         495  // 5 packet of 64 bytes plus some extra (15 bytes).

// The amount of timers get cutted to the half.
#undef  gPopMaxTimers_c
#define gPopMaxTimers_c           12  // each byte entry in the global array is 4 bytes long (8 * 4 = 32bytes)

// change the size for the Event Queue.
#undef  gPopMaxEvents_c
#define gPopMaxEvents_c           16

// The route can be reused and the system can keep replacing the oldest one at any time.
#undef  gPopRouteEntries_c
#define gPopRouteEntries_c        5

// Reducing the amount of neighbors that we can keep track of the counters on a secure system.
// if and only if security is On.
#undef  gPopNwkNeighborEntries_c
#define gPopNwkNeighborEntries_c  5 // Each entry is 7 bytes, keep at least a route for each neighbor

#undef  gPopRetryTableEntries_c
#define gPopRetryTableEntries_c        6

#endif  // End of (gPopTargetMcuType_c == 2) and (gPopTargetMcuType_c == 1)

#endif // gPopOverrideOnMcuType_d

// 0 = MC9S08GB60/MC9S08GT60  (60K ROM, 4K RAM)
#if (gPopTargetMcuType_c == 0)

// For the largest memory systems, allow scan to be up to maximum memory allocation size
#define mPopScanForNetworksSize_c gPopMemMaxAlloc_c

#endif //(gPopTargetMcuType_c == 0)

// define whether we get an address pool or not by default with PopNwkStartNetwork().
#if gPopNwkManageAddrPool_d
 #define mPopGetOwnAddrPool_c gPopNwkJoinOptsNormal_c
#else
 #define mPopGetOwnAddrPool_c gPopNwkJoinOptsDontGetAddr_c
#endif

// define scan options for PopNwkStartNetwork() based on PopCfg.h settings
// does user want to do a passive (quiet) or active (with Beacon Request) scan?
#if gPopScanSilently_d
 #define mPopDefaultScanOptions_c gPopNwkJoinOptsPassiveScan_c
#else
 #define mPopDefaultScanOptions_c 0
#endif

// if security is off, reduce # of neighbor table entries to 1
#if (!gPopSecurityLevel_c)
#undef gPopNwkNeighborEntries_c
#define gPopNwkNeighborEntries_c  1
#endif

/***************************************************************************
  Local Prototypes
***************************************************************************/

/***************************************************************************
  Globals
***************************************************************************/

/************* NodePlacement Ranges ****************/
/*
  Strong Signal Description:
    array[0] : used to calculate one bar
      e.g. LQI < gaPopNodePlacementStrongSignal[0]

    array[1] and array[2] : used to calculate two bars.
      e.g. LQI > gaPopNodePlacementStrongSignal[1] AND  LQI <= gaPopNodePlacementStrongSignal[2]
    
    array[3] : used to calculate three and four bars 
      e.g. LQI > gaPopNodePlacementStrongSignal[3]

    Some strong signal examples:
    -  <= 0x40           = Weak signal
    - > 0x40 and <= 0x80 = Medium signal
    - > 0x80             = Strong signal
    0xff = from this node

    Note: In this order they are used. See PopNet Developer's Guide for a Node placement explanation.
*/
#if gPopNwkNodePlacement_d  
  const popLqi_t gaPopNodePlacementStrongSignal[] = {0x40,0x80};
#endif

/************* Kernel Globals ****************/

// The PopNet heap (where messages are allocated)
#ifdef gPopMemUnitTest_d
#undef gPopMemHeapSize_c
#define gPopMemHeapSize_c (256*2 - 1)
#endif
uint8_t gaPopMemHeap[gPopMemHeapSize_c];
const popHeapSize_t cPopMemHeapSize = gPopMemHeapSize_c;

// table of timers
const uint8_t cPopMaxTimers = gPopMaxTimers_c;
sPopTimer_t gaPopTimer[gPopMaxTimers_c];
uint8_t giPopTimerWaterMark;
uint8_t giPopTotalTimers;


// global events (for all tasks) 
const uint8_t cPopMaxEvents = gPopMaxEvents_c;
sPopEvt_t gaPopEvents[gPopMaxEvents_c];

/* HCS08 not for ARM7 */
#if (gPopBigEndian_c == true)

#if gPopNumberOfNvPages_c

#ifndef gPopNvUnitTest_d
 #define EXT_CONST  // unit tests for NVM conducted in RAM
#else
 #define EXT_CONST const  // real NVM is in flash
#endif

// NVM needs these constants to interact with flash memory for storage.
// These, plus the routines PopFlashInit(), PopNvmErasePage() and
// PopFlashWriteBytes() form the physical interface to flash memory storage.
extern EXT_CONST uint8_t gaPopNvPages[];
#endif

#endif

/************* BSP Globals ****************/
#if gPopNumberOfNvPages_c
const uint8_t * const gpaPopNvFirstFlashPage = gaPopNvPages;
#endif

const uint8_t giPopNvNumPages = gPopNumberOfNvPages_c;
const popNvPageSize_t giPopNvPageSize = gPopNvPageSize_c;


/************* Network Globals ****************/

// network data (recorded to NVM if enabled) - 39 bytes
sPopNwkData_t gsPopNwkData =
{
  gPopDefaultChannelList_c,   // channel list defines list of channels to scan for forming/joining
  gPopNwkUseChannelList_c,                                      // channel 0 means use channel list
  gPopLowPower_d,               // sleepy mode or not, by default, if low power is enabled, the device is sleepy
  gPopPanId_c,
  gPopNodeAddr_c,
  {gPopMacAddr_c},
  gPopAppId_c,
  {gPopSecurityKey_c},
  0,                      // iAddressServer
  0,                      // iNextAddr
  0,                      // iAddrCount
  gPopAddrReqSize_c,
  gPopJoinEnabled_d
};


// these values are inputs to PopNwkStartNetwork(), PopNwkJoinNetwork() and PopNwkFormNetwork()
// they are normally set at compile tikme and are not saved in NVM
// if the app needs to preserve them across resets, make sure to save in application NVM space
popTimeOut_t      giPopNwkScanTimeMs  = gPopScanTime_c;         // abount of time to scan for beacons
uint8_t           giPopNwkJoinRetries = gPopNwkJoinRetries_c;   // # of times to retry join, 0xff=forever
const uint16_t    giPopNwkJoinExpired = gPopNwkJoinExpired_c;   // time(ms) between join attempts
popNwkJoinOpts_t  giPopJoinOpts       = mPopGetOwnAddrPool_c | mPopDefaultScanOptions_c;  // join/form options
const uint8_t     giPopNwkScanAttempts = 1;                     // # of times to do a beacon request when scanning for networks (increases scanning time)

// general network data (not saved in NVM)
popMfgId_t        giPopMfgId         = gPopMfgId_c;
bool            gfPopIdentifyMode;
const uint8_t     giPopDefaultRadius = gPopDefaultRadius_c;
const uint8_t      giPopPhySize       = gPopPhySize_c;

// the size of the discovery table is how many beacon indications can be processed
// during PopNwkScanForNetworks(), which is also called by PopNwkStartNetwork().
const popMemSize_t giMaxSizeDiscoveryTable = mPopScanForNetworksSize_c / sizeof(sPopNwkBeaconIndication_t);

// security data
const popSecurityLevel_t  giPopSecurityLevel = gPopSecurityLevel_c;           // security levels use different algorithms
sNwkNeighborTable_t       gaNeighborTable[gPopNwkNeighborEntries_c];          // used to keep track of frame counters (Not saved on NVM)
const uint8_t              giPopNwkNeighborEntries = gPopNwkNeighborEntries_c; // Const to control the neighbor table limits in the library.

// secure coutners update
const uint16_t giPopNwkSecureCounterIncrement = gPopNwkSecureCounterIncrement_c;

// Mac Header Frame Control (from 7.2.1.1 Frame Control Field - IEEE 802.15.4 2006 spec)
// 
// xxxx xxxx xxxx xTTT  Frame type (000=beacon, 001=data, 010=ack, 011=cmd) 
// xxxx xxxx xxxx Sxxx  Security enabled
// xxxx xxxx xxxP xxxx  Pending frame (to inform sleepy node)
// xxxx xxxx xxAx xxxx  Ack requested
// xxxx xxxx xCxx xxxx  Pan ID compression (0=src & dst PANs, 1=dst PAN only)
// xxxx xxRR Rxxx xxxx  Reserved bits
// xxxx DDxx xxxx xxxx  Destination address mode (00=not present, 10=16-bit, 11=64-bit)
// xxVV xxxx xxxx xxxx  Frame version (00=2003, 01=2006)
// SSxx xxxx xxxx xxxx  Source address mode (00=not present, 10=16-bit, 11=64-bit)
// little endian
#define gMacFrameControl_HiByte_c 0x88
#if ((gPopSecurityLevel_c == gPopNwkLiteSecurity_c) || (gPopSecurityLevel_c == gPopNwkAESSecurity_c))
  #define gMacFrameControl_LoByte_c 0x69
#else
  #define gMacFrameControl_LoByte_c 0x61
#endif
#if gPopBigEndian_c
  #define gMacOtaFrameControl_c ((gMacFrameControl_LoByte_c << 8) | gMacFrameControl_HiByte_c)
#else
  #define gMacOtaFrameControl_c ((gMacFrameControl_HiByte_c << 8) | gMacFrameControl_LoByte_c)
#endif

// Defines the size for the Auxiliary Frame based on the security level
#if gPopSecurityLevel_c == gPopNwkLiteSecurity_c
#define gPopAuxFrameSize_c sizeof(sNwkLiteAuxFrame_t)
#elif gPopSecurityLevel_c == gPopNwkAESSecurity_c
#define gPopAuxFrameSize_c sizeof(sNwkAESAuxFrame_t)
#else
#define gPopAuxFrameSize_c 0
#endif

/* The current size for the Auxilairy Frame. */
const uint8_t giPopNwkAuxFrameSize = gPopAuxFrameSize_c;
const popMacFrameControl_t gMacOtaFrameControlData = gMacOtaFrameControl_c; 
  
// starts out with no routing table entries (Saves to NVM with the Nwk Data)
sPopNwkRouteEntry_t gaPopNwkRouteTable[gPopRouteEntries_c];
const uint8_t giPopNwkRouteEntries = gPopRouteEntries_c;

// for handling route requests
sPopNwkRouteDiscovery_t gaPopNwkRouteDiscoveryTable[gPopRouteDiscoveryEntries_c];
const uint8_t cPopNwkRouteDiscoveryEntries = gPopRouteDiscoveryEntries_c;
uint8_t giPopNwkRouteDiscoveryHighWaterMark;
uint8_t giPopNwkRouteDiscoveryCount;
popNwkAddr_t gaPopNwkICanHearYouTable[gTmpICanHearYouEntries_c];
const uint8_t giPopNwkICanHearYouEntries = gPopICanHearYouEntries_c;

// # of messages that can be processed at one time
// Make sure to set the array size to the same as cPopNwkOtaMsgQueueEntries
sPopNwkDataIndication_t *maPopNwkMsgQueue[gPopRetryTableEntries_c];
const uint8_t cPopNwkOtaMsgQueueEntries = gPopRetryTableEntries_c;

// retry table keeps track of transmitting retries for unicasts
sPopNwkRetryTable_t gaPopNwkRetryTable[gPopRetryTableEntries_c];
const uint8_t cPopNwkRetryEntries = gPopRetryTableEntries_c;

// determines # of asymmetric link entries to try before giving up
const uint8_t cPopNwkAsymmetricLinkEntries = gPopAsymmetricLinkEntries_c;

// used to prevent duplicates from going to higher layers
sPopNwkDuplicateTable_t gaPopNwkDuplicateTable[gPopRetryTableEntries_c];
const uint8_t cPopNwkDuplicateEntries = gPopRetryTableEntries_c;
uint8_t giPopNwkDuplicateEntriesCount;
uint8_t giPopNwkDuplicateHighWaterMark;

// how many times to retry an over-the-air packet (for unicasts and route discoveries)
const uint8_t giPopNwkMaxRetries = gPopMaxRetries_c;

// how many times to retry an over-the-air packet (for normal broadcasts)
const uint8_t giPopNwkBroadcastRetries = gPopBroadcastRetries_c;


// Jitter is the random delay before retrying a unicast or broadcast. 
// Time out for unicast depends on security level delay
#if(gPopSecurityLevel_c == 0)
  #define mPopUnicastTimeOut_c    8
  #define mPopUnicastJitter_c     43
#elif (gPopSecurityLevel_c == 1)
  #define mPopUnicastTimeOut_c    250
  #define mPopUnicastJitter_c     512
#elif (gPopSecurityLevel_c == 2)
  #define mPopUnicastTimeOut_c    750
  #define mPopUnicastJitter_c     512
#endif

// 30 - 150ms jitter per broadcast
#define mPopBroadcastTimeOut_c  30
#define mPopBroadcastJitter_c   121

// jitter times for unicasts broadcasts (time between sending again)
const uint16_t giPopNwkUnicastTimeOut    = mPopUnicastTimeOut_c;
const uint16_t giPopNwkUnicastJitter     = mPopUnicastJitter_c;      // 8 - 50ms total per unicast
const uint8_t   giPopNwkBroadcastTimeOut  = mPopBroadcastTimeOut_c;
const uint8_t   giPopNwkBroadcastJitter   = mPopBroadcastJitter_c;    // 30-150ms total per broadcast

// wait about 50ms 
const uint16_t giPopNwkAckExpired        = (mPopUnicastTimeOut_c + mPopUnicastJitter_c);
const uint16_t giPopNwkWaitInd           = 300; // how long to wait before sending the data indication up to app on receiving a RREQ
const uint16_t giPopNwkWaitRREPStartup   = 20; // how long to wait before sending out the 1st RREP on RREQ dest node

// about 3.6 seconds for a broadcast table entry to expire at default radius 12
const uint16_t giPopNwkBroadcastExpired  = (gPopDefaultRadius_c * (gPopMaxRetries_c + 1) * (mPopBroadcastTimeOut_c + mPopBroadcastJitter_c)) / 2;

// expiry time for route reply
const uint16_t giPopNwkRouteReplyWait    = gPopDefaultRadius_c * (gPopMaxRetries_c + 1) * (mPopBroadcastTimeOut_c + mPopBroadcastJitter_c);

// beacon and rejoin jitter
const uint8_t   giPopNwkBeaconJitter      = 240;  // jitter for sending out a beacon in response to beacon request
const uint8_t   giPopNwkBeaconJitterJoinEnable      = 10;  // jitter before sending out a beacon in response to beacon request if join enable
const uint16_t giPopNwkReJoinJitter      = 250;  // jitter before attempting join/rejoin to allow many nodes to start at the same time

// time for an end-to-end app to acknowledge. Note: at up to 12 hops, this could be 4.8 seconds (worst case).
// This is set to average (1/2 worst case) at 2.4 seconds.
const popTimeOut_t giPopAppEndToEndAckTimeout = gPopDefaultRadius_c * gPopMaxRetries_c * (mPopUnicastTimeOut_c + mPopUnicastJitter_c);

/* The maximum amount of bytes available for the application payload. */
const uint8_t giPopNwkMaxPayload = (gPopMaxFrame_c - (sizeof(sMacDataFrame_t) + sizeof(sNwkDataFrame_t) + gPopAuxFrameSize_c));

// weak or poor signal <= 0x50, medium signal 0x51-0x90, strong signal > 0x90 
// it is up to the PHY layer to adjust the incoming LQI to match this list
const uint8_t gaPopNwkLqi[] = { 0x50, 0x90 };

// over the air upgrades HW identification number used to distinguish different
// hardware boards using the same platform temporarily uses TARGET_BOARD values
// but it will change to use SJS defined board ids
const popOtauHwId_t giPopHardwareId = TARGET_BOARD;


/*
  PopNwkSecureData

  Applies security to a data or NWK command packet.
*/
void PopNwkSecureData(sPopNwkDataIndication_t *pOtaFrame)
{
#if gPopSecurityLevel_c == gPopNwkLiteSecurity_c
  PopNwkLiteSecureData(pOtaFrame);
#elif gPopSecurityLevel_c == gPopNwkAESSecurity_c
  PopNwkAESSecureData(pOtaFrame);
#else
  (void)pOtaFrame;
#endif

  // inform user that the secure material has changed, and ready to be saved if needed.
#if gPopSecurityLevel_c
  PopNvmDataMarkAsChanged(gPopNvmCountersChanged_c);
#endif
}

/*
  PopNwkRemoveSecurity

  Removes security from a data or NWK command packet
*/
bool PopNwkRemoveSecurity(sPopNwkDataIndication_t *pOtaFrame)
{
#if gPopSecurityLevel_c == gPopNwkLiteSecurity_c
  return PopNwkLiteRemoveSecurity(pOtaFrame);
#elif gPopSecurityLevel_c == gPopNwkAESSecurity_c
  return PopNwkAESRemoveSecurity(pOtaFrame);
#else
  (void)pOtaFrame;
  return true;
#endif
}

/*
  PopNwkStalenessCheck
  
  This security function checks to make sure the packet received isn't stale. This 
  prevents replay attacks, as anything repeated that's already been heard is discarded.

  At this point, we already know the packet is for this network.

  Returns false if stale, true if good.
*/
bool PopNwkStalenessCheck(sPopNwkDataIndication_t *pIndication)
{
#if !gPopSecurityLevel_c

  (void)pIndication;
  return true;

#else
  sNwkAuxFrame_t *pAuxFrame;
  popNwkAddr_t iMacDstAddr;

  // AUX is at the end of the packet (after the app data) 
  pAuxFrame = (void *)((uint8_t *)(&pIndication->sFrame.sNwk) + (pIndication->sPre.iFrameLen - PopSizeOfMember(sPopDataFrame_t, sMac)));

  // only check staleness on unicast packets. So if not for this node, ignore it. (say it's OK)
  iMacDstAddr = pIndication->sFrame.sMac.iDstAddr;
  if(iMacDstAddr != PopNwkGetNodeAddr())
    return true;

  // check staleness on packets coming from this neighbor
 #if gPopSecurityLevel_c == gPopNwkLiteSecurity_c
  return PopNwkCheckNeighborTable(iMacDstAddr, pIndication->sFrame.sMac.iSrcAddr, pAuxFrame->sLite.iCounter);
 #else
  return PopNwkCheckNeighborTable(iMacDstAddr, pIndication->sFrame.sMac.iSrcAddr, pAuxFrame->sAes.iCounter);
 #endif

#endif
}

/*
  Needed if void PopNwkSetSecurityKey(void *pKey) is called, but only if AES security is 
  enabled.
*/
#if (gPopSecurityLevel_c >= gPopNwkAESSecurity_c)
void ExpandKey(void *pKey)
{
  Expand128Key(pKey);
}
#endif

/*
  Set the current local security key.

  This function was moved to here because it was being called from Nwk layer always. Assumes the 
  key is 16 bytes (128-bits).
*/
void PopNwkSetSecurityKey(void *pKey)
{
#if gPopSecurityLevel_c
  // Set the give parameter as the global value for the secure key.
  PopMemCpy(gsPopNwkData.aSecurityKey, pKey, sizeof(aPopNwkKey_t));

  // Expand the key to be used during the AES encryption/decryption.
  ExpandKey(pKey);
#else
  (void)pKey;
#endif
}

/*
  PopNvmDataMarkAsChanged

  Mark a particular area as changed (such as routes, NWK data, etc...)

  xxxx xxxS   Security counters changed
  xxxx xxRx   Routes have changed
  xxxx xNxx   NWK data has changed
*/
void PopNvmDataMarkAsChanged(popNvmDataChangedBitMask_t iNvmDataChanged)
{
#if !gPopNumberOfNvPages_c
  (void)iNvmDataChanged;
#else
  sPopEvtData_t sEvtData;

  // if already marked as changed, do nothing
  if (PopNvmCheckBitOnMask(iNvmDataChanged))
    return;

  // otherwise, mark as changed and inform app
  PopNvmSetBitOnMask(iNvmDataChanged);
  sEvtData.iNvmChanged = giPopNvmDataChanged;
  (void)PopSetAppEvent(gPopEvtNvmDataChanged_c, sEvtData.pData);
#endif
}


/*
  Called from the network layer to (optionally) scan for noise, depending on user
  compiler flags.
*/
void PopNwkOptionallyScanForNoise(void)
{
#if gPopScanForNoise_d
  if(PopNwkSetEnergyScanOn())
    (void)PopNwkScanForNoise(PopNwkGetChannelList());
#endif
}


