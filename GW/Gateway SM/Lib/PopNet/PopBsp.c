/***************************************************************************
  PopBsp.c
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  This is the Board Support Package for the Freescale Reference Design Boards
  (SARD, EVB, SRB and NCB).

  The Board Support Package (BSP) is the interface between PopNet and the physical board or OS.
  The BSP includes the following interfaces:

  1. MCU interface
  2. Random # interface
  3. Timer interface
  4. Serial I/O interface
  5. LEDs interface
  6. LCD interface
  7. Keyboard interface
  8. Sound interface
  9. Low Power interface
  10. Flash (or other Non-volatile) storage interface
  11. Reset control interface
  12. Security interface

  03/31/2007 dg  Now includes full SMAC
  11/14/2006 dg  Updated for NCB, SRB support. Added in Radio Support.
  05/31/2005 dg  Updated to support SMAC
  11/22/2004 dg  Created by Drew Gislason
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "board_config.h"
#include <Hw/TIDriverLib/source/TiUart.h>
#include <Hw/TIDriverLib/source/sys_ctrl.h>
#include <Hw/TIDriverLib/source/gpio.h>
#include <Hw/TIDriverLib/source/ioc.h>
#include <Hw/TIDriverLib/source/watchdog.h>
#include <Hw/TIDriverLib/source/interrupt.h>
#include <Hw/TIPhy/hal_rf.h>
#include <Hw/TIDriverLib/source/tiflash.h>
#include <Hw/TIDriverLib/source/rom.h>

#if gPopMicroPowerExternalFlash_d
//to use Micropower definition for GPIO
#include <PortConfig.h>
#endif

#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"

#if gPopMicroPowerExternalFlash_d
// to use flash routines during bootloader function
#include <Flash/flash.h>
#endif

/***************************************************************************
  Local Types & Defines
***************************************************************************/

// Local events
#define mPopBspEvtInit_c  gPopEvtUser_c

// Local timers
#define gPopKeyTimer_c                gPopFirstTimerId_c
#define gPopSoundTimer_c              (gPopFirstTimerId_c+1)
#define gPopWakeupTimer_c             (gPopFirstTimerId_c+2)
#define gPopOtaUpgradeResetTimer_c    (gPopFirstTimerId_c+3)
#define gPopBspSoftwareResetTimer_c   (gPopFirstTimerId_c+4)

/*
  Get the LVW flag. This read only bit is set when the supply voltage drops below
  the VLWV threshold
*/
#define PopLvwGetFlag() gPopLvwGetFlag

// keyboard defines
#define mPopKeyInterval_c       100   // interval used for debounce
#define mPopLongKeyCountDown_c  10    // # of intervals to indicate long press

/*Operations mode*/
#define gPopStop_c     0
#define gPopRunning_c  1
#define gPopWait_c     2

// <SJS: Added Serial Tx and Rx State Machine>

// define some events to be used for the state machine
#define mPopBspSerialSendNext_c (gPopEvtUser_c+11)
#define mPopBspSerialSendDone_c (gPopEvtUser_c+12)

#if gPopSerialPort_c == 1
 #define UARTx    UART_1
#else
 #define UARTx    UART_2
#endif

typedef struct sPopSerialSendQueue_tag
{
  popSerialSendFlags_t iFlags;
  uint16_t             iLen;
  void *               pData;
} sPopSerialSendQueue_t;

#if gPopSerial_d
void PopSerialSendNext(void);
void PopSerialSendDone(void);

// SJS: Serial Send Queue
sPopSerialSendQueue_t gaPopSerialSendQueue[gPopSerialSendQueueSize_c];
uint8_t               giPopSerialSendNumInQueue;
uint8_t               giPopSerialSendQueueHead;
uint8_t               giPopSerialSendQueueTail;
#endif

// </SJS:>

/***************************************************************************
  Local Prototypes
***************************************************************************/

// external prototypes
void PopNwkCheckRetries(popTimeOut_t iTicksMs);
void PopNwkCheckDuplicates(popTimeOut_t iDiffTime);
void PopNwkCheckRouteDiscovery(popTimeOut_t iDiffTime);
uint32_t GetMcuTimer1(void);

// Get MAC address from NVM
#if gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d
void PopRetrieveMacAddr(void * pDst);
#else
#define PopRetrieveMacAddr(pDst)
#endif
// Store MAC address to NVM
popErr_t PopStoreMacAddr(void * pSrc);

// keyboard prototypes
#if gPopKeyboardEnabled_d

void PopKeyInit(pfKBCallback_t kbCallback);
void SendKeyEventToApp(popKeyId_t iKeyIdPressed);
void PopKeyBoard_ISR(void);
uint8_t PopGetKey(void);

#else
#define PopKeyInit(kbCallback)
#define SendKeyEventToApp()
#define PopKeyBoard_ISR()
#endif


// timer prototypes
void PopBspTimerCheck(void);
popErr_t PopStopTimerEx(popTaskId_t iTaskId, popTimerId_t iTimerId);

// LED prototypes
#if gPopLedEnabled_d
void PopLedInit(void);
#else
#define PopLedInit()
#endif

// LCD prototypes
#if gPopLcdEnabled_d
void PopLcdInit(void);
#else
#define PopLcdInit()
#endif

// Sound (buzzer) prototypes
#if gPopSoundEnabled_d
void PopSoundInit(void);
#else
#define PopSoundInit()
#endif

#if gPopNumberOfNvPages_c

/* Used for NVM in ARM7 SMAC */
uint32_t const maNvRawSectorAddressTable[ gNvNumberOfRawSectors_c] = {gNvSector0Address_c, gNvSector1Address_c };

#endif

#if gPopLowPower_d
void PopLowPowerDisableRTCWuInterrupt(void);
void PopInitLowPower(void);
#else
#define PopLowPowerDisableRTCWuInterrupt()
#define PopInitLowPower()
#endif

#if gPopLowVoltageDetection_d
uint16_t PopGetBatteryAdcRef(void);
void PopCheckBaterryLevel(uint16_t adcValue);
#else
#define PopGetBatteryAdcRef 0
#define PopCheckBaterryLevel 0
#endif

/************ events **************/
uint8_t PopFindEvent(popTaskId_t iTaskId, popEvtId_t iEvtId);
void PopRemoveEvent(uint8_t i);


/***************************************************************************
  Globals & Externs
***************************************************************************/

// externals
extern sPopTimer_t gaPopTimer[];
extern sPopEvt_t   gaPopEvents[];
extern volatile popTimeOut32_t gMillisecondsElapsed;
extern const uint8_t cPopNumTasks;


#if gPopSerial_d
extern gPopBaudRate_t giPopSerialBaudRate;
#endif

// STACK variables
__no_init uint32_t * __SSTACK; /* Points to begin of the stack */

//for PopRandom08Number
uint8_t giPopRandomSeed=0xFF;
uint8_t giPopRandomNumber;

// for the timer
uint8_t gPopRunMode = gPopRunning_c;
popTimeOut32_t  giPopLastTime;
bool      gfRTISentEvent = true;

// interrupts variables
/* Variable to store what interruptions were disabled */
unsigned int saveInt;
unsigned int intDisabled;

#if gPopKeyboardEnabled_d
const int_t cPopNumKeys = gPopNumKeys_c;
int_t giPopLongKeyCountDown;  // long or short press?
uint8_t giPopKeyWaiting;       // what key was first recorded?
uint8_t giPopKeyToSend;        // finally deteremined key to send
uint8_t gKeyPressedMask;
uint8_t gActualKeyPressed;
#endif


#if gPopLedEnabled_d
const int_t cPopNumLeds = gPopNumLeds_c;
#endif

// how many rows and columns for the LCD?
#if gPopLcdEnabled_d
const int_t cPopLcdRows = gPopLcdRows_c;
const int_t cPopLcdCols = gPopLcdCols_c;
#endif

// TODO: (ai) Erase the next 4 variables.
#if gPopLowPower_d
uint32_t  giRTI_Timer_Count=0;
uint32_t  giUseTimeToWakeUp = 0xffff;

bool    gfKBI_Wakeup_Flag = gFalse_c;
bool    gfUseKBIToWakeUp = gTrue_c;
bool    giWakeUpByTime = gTrue_c;
#endif

/*--- For Reset function ---*/
/* An HCS08 illegal instruction. Used to force an illegal instruction reset */
const uint16_t miIllegalOpcode = 0x9e62;

/* A pointer to the address of illegal opcode */
const uint16_t *pIllegalOpcode = &miIllegalOpcode;

// For LVD
uint8_t gPopLvwGetFlag;

#if gPopMicroPowerExternalFlash_d
// For Micropower external flash blocking control
volatile uint8_t MPWriteDoneFlag;
volatile uint8_t MPReadDoneFlag;
volatile uint8_t MPEraseBlockFlag;
#if 0
static uint8_t gEraseBlock;
static uint8_t gEndBlock;
#endif
#endif

#if gPopSerial_d
// SJS: Serial Send Queue
sPopSerialSendQueue_t gaPopSerialSendQueue[gPopSerialSendQueueSize_c];
#endif

/***************************************************************************
  Code
***************************************************************************/
#if gPopMicroPowerExternalFlash_d
/*
  MPEraseDoneCallback
*/
static void MPEraseBlockDoneCallback(uint32_t address){
 MPEraseBlockFlag = true;
}

/*
  MPEraseBlockRangeCallback
*/
#if 0
static void MPEraseBlockRangeCallback(uint32_t address){

  gEraseBlock++;

  if(gEraseBlock <= gEndBlock){
    FlashEraseBlock(address,MPEraseBlockRangeCallback);
  }
}
#endif
/*
 MPFlashEraseBlockBlocking
 uses the FlashWrite routine from Micropower as a blocking routine for compatibility
 with actual over the air upgrade code
*/
popErr_t MPFlashEraseBlockBlocking(uint32_t address)
{
  MPEraseBlockFlag = false;
  FlashEraseBlock(address,MPEraseBlockDoneCallback); 

  while(!MPEraseBlockFlag);

  return gPopErrNone_c;
}

popErr_t MPFlashEraseBlockRangeBlocking(uint8_t startBlock, uint8_t endBlock)
{
  popErr_t result = gPopErrFailed_c;
  uint32_t address;

  if(startBlock < endBlock){
    uint8_t i = startBlock;
    uint8_t iEnd = endBlock;
    address = i * 4096;

    while(i <= iEnd){
      MPFlashEraseBlockBlocking(address);
      i++;
      address = i * 4096;
    }
    result = gPopErrNone_c;
  }
  return result;
}
#if 0
popErr_t MPFlashEraseBlockRange(uint8_t startBlock, uint8_t endBlock)
{
  popErr_t result = gPopErrFailed_c;
  uint32_t address;

  if(startBlock < endBlock){
    gEraseBlock = startBlock;
    gEndBlock = endBlock;
    address = gEraseBlock * 4096;

    FlashEraseBlock(address,MPEraseBlockRangeCallback);

    result = gPopErrNone_c;
  }
  return result;
}
#endif
/*
  MPWriteDoneCallback
*/
static void MPWriteDoneCallback(uint32_t address){
 MPWriteDoneFlag = true;
}
/*
  MPReadDoneCallback
*/
static void MPReadDoneCallback(uint32_t address){
 MPReadDoneFlag = true;
}
/*
 MPFlashWriteBlocking
 uses the FlashWrite routine from Micropower as a blocking routine for compatibility
 with actual over the air upgrade coding
*/
popErr_t MPFlashWriteBlocking(uint32_t address, void* data, int len)
{
  MPWriteDoneFlag = false;
  FlashWrite(address,data,len,MPWriteDoneCallback); 

  while(!MPWriteDoneFlag);

  return gPopErrNone_c;
}

/*
 MPFlashReadBlocking
 uses the FlashRead routine from Micropower as a blocking routine for compatibility
 with actual over the air upgrade coding
*/
popErr_t MPFlashReadBlocking(uint32_t address, void* data, int len)
{
  MPReadDoneFlag = false;
  FlashRead(address,data,len,MPReadDoneCallback); 

  while(!MPReadDoneFlag);

  return gPopErrNone_c;
}
#endif
/*
  PopBspTaskInit

  Initializes the board support package. Specific to each board/board type.
*/
void PopBspTaskInit(void)
{
  PopWatchDogInit();
  // initialize the subsystems
#if gPopNumberOfNvPages_c
  PopArm7PowerUpNVM();
  PopNvmInit();
#endif

  // dg - no need to power down NVM unless going to sleep. Low power code will take care of that
  // PopArm7PowerDownNVM();
#if gPopUsingStorageArea_c
  PopStorageInit();
#endif

  //Get Mac address from spare section after OTA upgrade
  PopRetrieveMacAddr(&(gsPopNwkData.aMacAddr));

#if gPopMicroPowerLpcUpgrade_d == 1
  /*
   *  Pull LPC17xx reset and EINT0 pins low to release the controller from
   *  reset and let it start the main application.
   */

  // LPC1765 EINT0
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, 0);
  GPIOPinTypeGPIOOutput(GPIO_A_BASE, GPIO_PIN_4);
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, 0);

  // LPC1765 RESET
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, 0);
  GPIOPinTypeGPIOOutput(GPIO_A_BASE, GPIO_PIN_5);
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, 0);
#endif
}

/*
  PopBspTaskEventLoop

  Handles BSP events.
*/
void PopBspTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{

  /* Firstly, reset the COP because we don't know if some task will take more than COP timeout configured */
  PopFeedTheDog();

  // BSP events
  switch(iEvtId)
  {
    // nothing is happening, check timers, radio, etc...
    case gPopEvtNull_c:
 #if gPopLowVoltageDetection_d
        PopCheckBaterryLevel( PopGetBatteryAdcRef() );
#endif
        // polled timers
        PopBspTimerCheck();
        break;

      // check keys once every 100ms
    case gPopEvtTimer_c:

#if gPopNumberOfNvPages_c || gPopUsingStorageArea_c
        if(sEvtData.iTimerId == gPopOtaUpgradeResetTimer_c)
        {
          // Restart to let the control flow go away.
          PopReset();  // Let the boot loader handle the rest.
        }
#endif
#if gPopLowPower_d
        if(sEvtData.iTimerId == gPopWakeupTimer_c)
        {
          // Notify to network layer
          (void)PopSetEventEx(cNwkTaskId,gPopEvtPwrWakeUp_c,NULL);

          // Notify to application what was the wakeup reason
          if(giWakeUpByTime)
          {
            sEvtData.iWakeupReason = gPopTimesUp_c;
            (void)PopSetAppEvent(gPopEvtPwrWakeUp_c,sEvtData.pData);
            giWakeUpByTime = false;
          }
        }
#endif

#if gPopKeyboardEnabled_d
        if(sEvtData.iTimerId == gPopKeyTimer_c)
        {
          /* Check if the key is not still pressed then send short key */
          if ( !IsStillKeyPressed( gKeyPressedMask ) )
          {
            /* Sent the event to app */
            SendKeyEventToApp( PopGetKey() );

            /* Enable key interrupt again */
            KbEnableAllIrq();
          }
          /* otherwise, it is a long switch. To verify it we will put other timer (500 ms) more */
          else
          {
            // Activate long switch
            gKeyPressedMask  |= (gActualKeyPressed >> 4);

            // Set timer for long switch
            PopStartTimer(gPopKeyTimer_c , 750);
          }
        }
#endif
        if( sEvtData.iTimerId == gPopBspSoftwareResetTimer_c )
        {
          SysCtrlReset();
        }

        break;

    case mPopBspEvtInit_c:
        break;

    case gPopEvtKey_c:

    break;

    // <SJS: Added Serial Tx Rx State Machines>
#if gPopSerial_d
    case mPopBspSerialSendNext_c:
      PopSerialSendNext();
    break;

    case mPopBspSerialSendDone_c:
      PopSerialSendDone();
    break;
#endif
    // </SJS:>

  default:
    break;
  }
  // avoid compile warnings
  (void)sEvtData;
}

/***************************************************************************
  System halt and reset functions
***************************************************************************/

/*
  Fatal error, halt the system
*/
void PopHalt(void)
{
  PopDisableInts();
  while(giPopForever)
    /* do nothing */;
}

/*
  System reset.
*/
void PopReset(void)
{
  // Wait for any register to engage before actually doing the reset.
  PopStartTimerEx(cBspTaskId,gPopBspSoftwareResetTimer_c, 200);
}

/*
  PopDisabledInt
*/
void PopDisableInts(void)
{
  saveInt = IntMasterDisable();
}
/*
  PopEnabledInt
*/
void PopEnableInts(void)
{
  if (saveInt == 0)
  {
    IntMasterEnable();
  }
}

/*
  A pointer to the bottom of the stack.
*/
void *PopStackFloor(void)
{
  uint32_t *pSP = SP_Starts;
  return pSP;
}

/*
  PopStackLeftAtHighWaterMark

  Returns the amount of bytes left in the C stack.
*/
uint16_t PopStackLeftAtHighWaterMark(void)
{
  uint32_t *pSP = SP_Starts;
  uint16_t stackLeft = 0;

  // If the stack pass the water mark it will assert by default. Or if
  // We didn't pass the water mark but we are stepping on it, also assert, this part is
  // already handle.

  // Avoid the eater mark (4 bytes).
  pSP = (uint32_t *)( (uint8_t*)pSP + 4);

  // While the initialization value reamins in the C stack means that the space is unused.
  while (*(uint8_t*)pSP == 0x4d)
  {
    stackLeft++;
    pSP = (uint32_t *)( (uint8_t*)pSP + 1);
  }

  // Include the water mark. (2 bytes.)
  return (stackLeft + 2);
}

void DelayMs(uint32_t ms)
{
  /* SysCtrlDelay takes three clock cycles for each iteration. */

  SysCtrlDelay(ms * ((SYS_CTRL_32MHZ / 3) / 1000));
}

/***************************************************************************
  Sound (Beep) Functions
***************************************************************************/

#if gPopSoundEnabled_d
/*
  Initialize the sound subsystem.
*/
void PopSoundInit(void)
{
  BuzzerInit();
}

/*
  PopPop

  Create a popping sound or beep, depending on implementation.
*/
void PopPop(void)
{

  PopSoundOn();
}

/*
  PopSoundOn

  Turn on the buzzer
*/
void PopSoundOn(void)
{
  /* Turn on the Buzzer and after 1/4 of sec it is turned off */
  BuzzerBeep(250);
}

#endif // gPopSoundEnabled_d


/***************************************************************************
  Lcd Functions
***************************************************************************/

#if gPopLcdEnabled_d
/*
  Initialize LCD controller (if it exists)
*/
void PopLcdInit(void)
{
  LCD_Init();
}

/*
  Write a string to LCD controller. Fills the end of the line with spaces
  if string is shorter than display.
*/
void PopLcdWriteString(uint8_t iLine, char *pszStr)
{
  LCD_WriteString_NormalFont(iLine, (uint8_t *)pszStr);
}
#endif

/***************************************************************************
  Led Functions
***************************************************************************/

#if gPopLedEnabled_d
/*
  PopInitLeds

  Initialize the LEDs. Returns the # of LEDs.
*/
void PopLedInit(void)
{
  /* Led init function for SMAC ARM7 */
  LED_Init();
}

/*
  PopSetLed

  Sets a group of LEDs to the same state. Careful: the iLeds uses bit 0 for
  LED1, bit1 for LED2, etc... this may be VERY different than LED1 or LED2
  macros, which define the specific GPIO ports to set the LED.

  Supports up to 8 LEDs.

  For SMAC ARM7 the LED's are manage differently for that reason we have created one function more. And
  for the moment we put the code just for 4 LED's.
*/
void PopSetLedSmacArm7(LED_t LEDNr, popLedState_t iState)
{
  if(iState == gPopLedToggle_c)
    LED_ToggleLed(LEDNr);

  if(iState == gPopLedOn_c)
    LED_TurnOnLed(LEDNr);

  if(iState == gPopLedOff_c)
    LED_TurnOffLed(LEDNr);
}

void PopSetLed(popLedMask_t iLedMask, popLedState_t iState)
{
  LED_t LEDNr;

  uint8_t  iLed = 0;

  while(iLed < PopNumLeds())
  {
    ++iLed;
    if(iLedMask & 1)
    {
      switch(iLed)
      {
        case 1: LEDNr = LED1;
              break;
        case 2: LEDNr = LED2;
              break;
        case 3: LEDNr = LED3;
              break;
        case 4: LEDNr = LED4;
              break;
      }

      PopSetLedSmacArm7(LEDNr,iState);
    }
    iLedMask = iLedMask >> 1;  // on to next bit
  }

}

/*
  PopLedDisplayHex

  Displays byte on LEDs, starting with lowest bit.
*/
void PopLedDisplayHex(uint8_t iHexValue)
{
  LED_SetHex(iHexValue);
}
#endif // gPopLedEnabled_d


/***************************************************************************
  Keyboard Functions
***************************************************************************/

#if gPopKeyboardEnabled_d
void PopKeyBoard_ISR(void)
{
  gActualKeyPressed = (((uint8_t)SwitchGet) << 4);

  /* Avoid duplicated interruptions just procces one at time */
  if( (gActualKeyPressed != 0x00)  && (gKeyPressedMask & gActualKeyPressed) == 0)
  {
    // set the key
    gKeyPressedMask |= gActualKeyPressed;

    /*Disable all external interrupts and send the SW event to handle the key detection*/
    KbDisableAllIrq();

    // Force timer for BspTask
    giPopThisTask  = cBspTaskId;

    // Set the timer (250 ms)
    (void)PopStartTimer(gPopKeyTimer_c, 250);
  }
}
/*
  Initialize the keyboard
*/
void PopKeyInit(pfKBCallback_t kbCallback)
{

  /* Initialize the switch system */
  KbGpioInit();

#if gPopNumKeys_c >= 1
  CRM_RegisterISR(gCrmKB4WuEvent_c, kbCallback);
#endif
#if gPopNumKeys_c >= 2
  CRM_RegisterISR(gCrmKB5WuEvent_c, kbCallback);
#endif
#if gPopNumKeys_c >= 3
  CRM_RegisterISR(gCrmKB6WuEvent_c, kbCallback);
#endif
#if gPopNumKeys_c >= 4
  CRM_RegisterISR(gCrmKB7WuEvent_c, kbCallback);
#endif

  // Set the external out polarity to low
  CRM_WU_CNTL.extOutPol = 0x00;

  /* This instruction is important to trigger the switches */
  MLMESetWakeupSource(gPopKbiMask, 0x00, gPopKBIEdge_c);
}

/* We need to create a callback function for Kb on SMAC ARM7 */
void SendKeyEventToApp(popKeyId_t iKeyIdPressed)
{

  sPopEvtData_t sEvtData;

  sEvtData.iKeyId = iKeyIdPressed;

  // Send Key press notification
  (void)PopSetAppEvent(gPopEvtKey_c,sEvtData.pData);

#if  gPopLowPower_d
  /*
    In case that the Low power is included and that the keyboard was
    the reason to wakeup, then inform the application that it wakeup
    and why, so it can take the proper actions.
  */
  sEvtData.iWakeupReason = gPopKeyboardPress_c;
  (void)PopSetAppEvent(gPopEvtPwrWakeUp_c,sEvtData.pData);
#endif
}
/************************************************************************************/
popKeyId_t PopGetKey(void)
{
  popKeyId_t keyPressed;
  uint8_t IsLongKey;

  IsLongKey = (gActualKeyPressed >> 4);

  /* In case if it was a long key ... */
  if(gKeyPressedMask & IsLongKey)
    keyPressed = 0x40; // plus long switch base

  /* In case if it was a short key ... */
  if( gKeyPressedMask & gExtWuKBI4En_c )
    keyPressed |= gPopSw1_c;
  else if( gKeyPressedMask & gExtWuKBI5En_c )
    keyPressed |= gPopSw2_c;
  else if( gKeyPressedMask & gExtWuKBI6En_c )
    keyPressed |= gPopSw3_c;
  else if( gKeyPressedMask & gExtWuKBI7En_c )
    keyPressed |= gPopSw4_c;

  /* Clean this key information */
  gKeyPressedMask &= ~( gActualKeyPressed | IsLongKey);

 return keyPressed;
}
/************************************************************************************/
GpioPin_t PopGpioPin(uint8_t keyMask )
{
  GpioPin_t GpioKbPin;

  /* In case if it was a short key ... */
  if( keyMask & gExtWuKBI4En_c )
    GpioKbPin = gKbGpioPinSwitch1_c;
  else if( keyMask & gExtWuKBI5En_c )
    GpioKbPin = gKbGpioPinSwitch2_c;
  else if( keyMask & gExtWuKBI6En_c )
    GpioKbPin = gKbGpioPinSwitch3_c;
  else if( keyMask & gExtWuKBI7En_c )
    GpioKbPin = gKbGpioPinSwitch4_c;

 return GpioKbPin;
}
/************************************************************************************/
bool IsStillKeyPressed(uint8_t keyMask )
{
  GpioPinState_t gpioPinState;

  (void)Gpio_GetPinData(PopGpioPin(keyMask), &gpioPinState);

  return (( gpioPinState == gGpioPinStateLow_c ) ? true : false);
}
#endif // gPopKeyboardEnabled_d


/***************************************************************************
  Timer Functions
***************************************************************************/
/*
  PopBspStartUpTimer

  Start up the timer. It may have been running for some time, but this
  resets it so it's marked as 0 right now.
*/
void PopBspStartUpTimer(void)
{
  giPopLastTime = GetMcuTimer1();
}

/*
  Returns the total time this node has been running
*/
uint32_t PopGetCurrentTimeMs(void)
{
  return gMillisecondsElapsed;
}


/*
  PopBspTimerCheck

  See if any timers have expired. If so, notify the appropriate task with at
  SetEventEx(). This routine may result in the network layer sending packets
  out the radio (for unicast retry and broadcasts).
*/
void PopBspTimerCheck(void)
{
  popTimeOut32_t  iCurrentTime;
  popTimeOut_t  iDiffTime;
  uint8_t       i;
  sPopEvtData_t sEvtData;

  // get current time
  iCurrentTime = GetMcuTimer1();

  // if no time has expired, nothing to do
  iDiffTime = iCurrentTime - giPopLastTime;
  if(!iDiffTime)
    return;


  // check for expired timers
  for (i=0; i<PopNumTimers(); i++)
  {
    // for all valid timers, check
    if(gaPopTimer[i].iTimerId)
    {
      if(gaPopTimer[i].iTimeLeft <= iDiffTime)
      {
         // send the timer event to the proper task
        sEvtData.iTimerId = gaPopTimer[i].iTimerId;
        (void)PopSetEventEx(gaPopTimer[i].iTaskId, gPopEvtTimer_c, sEvtData.pData);

        // remove the timer
        gaPopTimer[i].iTimerId  = gPopTimerFree_c;   // Stop timer
        gaPopTimer[i].iTaskId   = gPopTimerFree_c;
        gaPopTimer[i].iTimeLeft = gPopTimerFree_c;

        // Decrease timers in use count
        --giPopTotalTimers;
      }

      // count down
      else
        gaPopTimer[i].iTimeLeft -= iDiffTime;
    }
  }

  // check both retry table and duplicate table for expired entries
  PopNwkCheckRetries(iDiffTime);
  PopNwkCheckDuplicates(iDiffTime);
  PopNwkCheckRouteDiscovery(iDiffTime);

  // remember the last time we were in this rouinte
  giPopLastTime = iCurrentTime;

}

/*
  PopFindTimer

  Find a timer. Finds a free entry if timer is not found.

  Parameter   Dir     Description
  ---------   ----    ---------------
  iTaskId     in      task ID of the timer
  iTimerId    in      timer ID of the timer
  piFreeTimer in/out  returns index of free entry or gPopEndOfIndex_c

  Returns index of timer, or gPopEndOfIndex_c if not found.
*/
uint8_t PopFindTimer(popTaskId_t iTaskId, popTimerId_t iTimerId, uint8_t *piFreeTimer)
{
  uint8_t i;
  uint8_t iFree;

  // assume no free timer found
  iFree = gPopEndOfIndex_c;

  // find the given timer
  for(i=0; i<PopNumTimers(); i++)
  {
    // return if this timer is found
    if(gaPopTimer[i].iTimerId == iTimerId && gaPopTimer[i].iTaskId == iTaskId){
      if(iTimerId != gPopTimerFree_c && iTaskId <= cPopNumTasks){
          return i;
      }
    }

    // record the first free timer
    if(iFree == gPopEndOfIndex_c && gaPopTimer[i].iTimerId == gPopTimerFree_c)
      iFree = i;
  }

  if(piFreeTimer)
    *piFreeTimer = iFree;
  return gPopEndOfIndex_c;
}

/*
  PopStopTimer

  Stops a timer that is in progress for current task.

  Returns gPopErrNone_c if worked.
*/
popErr_t PopStopTimer(popTimerId_t iTimerId)
{
  return PopStopTimerEx(giPopThisTask, iTimerId);
}

/*
  PopStopTimerEx

  Stops a timer that is in progress for any task.

  Returns gPopErrNone_c if worked.
*/
popErr_t PopStopTimerEx(popTaskId_t iTaskId, popTimerId_t iTimerId)
{
  uint8_t i;

  // free the timer if found
  i = PopFindTimer(iTaskId, iTimerId, NULL);
  if(i == gPopEndOfIndex_c)
    return gPopErrNotFound_c;

  // free the timer
  gaPopTimer[i].iTimerId = gPopTimerFree_c;
  --giPopTotalTimers;

  // pull this timer ID out of the event queue (if one is in there)
  i = PopFindEvent(iTaskId, gPopEvtTimer_c);
  if(i != gPopEndOfIndex_c && gaPopEvents[i].sEvtData.iTimerId == iTimerId)
    PopRemoveEvent(i);

  // worked
  return gPopErrNone_c;
}

/*
  PopStopAllTimers

  Stops all active timers from the Timer Table for specific task id or all tasks.
  cBspTaskId;
  cNwkTaskId;
  cSerialTaskId;
  cAppTaskId;

  The user needs to be sure when stop all the timers of a specific task because this
  function is not smart in that way.
*/
void PopStopAllTimers(popTaskId_t iTaskId)
{
  uint8_t indexTT;

  // Stop all the timers
  for(indexTT = 0; indexTT < PopNumTimers(); indexTT++)
  {
    // Just the task indicated.
    if(gaPopTimer[indexTT].iTaskId != iTaskId)
      continue;

    /* Assume the timer was stopped sucessfully */
    (void)PopStopTimerEx(gaPopTimer[indexTT].iTaskId , gaPopTimer[indexTT].iTimerId);
  }
}

/*
  IsThereAFreeTimer

  This function verifies if there is a timer available, so the user can check it
  before to set a timer.

  return:
    true is there is a free timer
    false otherwise
*/
bool IsThereAFreeTimer( void )
{
  uint8_t indexTT;

  // Stop all the timers
  for(indexTT = 0; indexTT < PopNumTimers(); indexTT++)
  {
    if( gaPopTimer[indexTT].iTimerId == gPopTimerFree_c )
    {
      // There is a timer free
      return true;
    }
  }

  // there is no a free timer
  return false;
}


/*
  PopStartTimer
*/
popErr_t PopStartTimer(popTimerId_t iTimerId, popTimeOut_t iMilliseconds)
{
  return PopStartTimerEx(giPopThisTask, iTimerId, iMilliseconds);
}

// <SJS: Fixed Early timeout for things like jitter>
/*
  Adjust the timeout for PopStartTimerEx() so that timers don't fire too early if the BSP hasn't been idle
  in awhile. In the example below the BSP hasn't been idle in 25ms when the StartTimerEx is set. This fix
  prevents the timer from expiring at NextIdleA, and instead expires on NextIdleB.

   LastIdle        StartTimer(30ms)        TimerExpires
     |  25ms             |  33ms               |
  ---+-------------------+-----+---------------+-----
                         | 6ms |     27ms      |
                            NextIdleB       NextIdleB
*/
popTimeOut_t PopAdjustTimeoutForLastIdle(popTimeOut_t iMilliseconds)
{
  popTimeOut_t iDiffTime;

  // handles wrap just fine. If GetMcuTimer1() is 0x0005, and giPopLastTime is 0xfff8, then 0x000D ms have elapsed.
  iDiffTime = GetMcuTimer1() - giPopLastTime;

  // don't let iMilliSeconds + iDiffTime exceed maximum timeout
  if(gPopMaxTimeout_c - iMilliseconds  < iDiffTime)
    iDiffTime = gPopMaxTimeout_c - iMilliseconds;

  return iMilliseconds + iDiffTime;
}
// </SJS:>

/*
  PopStartTimerEx

  Returns gPopErrNone_c if timer was started. Returns error code otherwise.
*/
popErr_t PopStartTimerEx(popTaskId_t iTaskId, popTimerId_t iTimerId, popTimeOut_t iMilliseconds)
{
  uint8_t  i;
  uint8_t  iFound;

  // invalid parameter
  if(!iTimerId || iMilliseconds > gPopMaxTimeout_c)
    return gPopErrBadParm_c;

  // set the event immediately (0ms timer)
  if(!iMilliseconds)
  {
    (void)PopSetEventEx(iTaskId, gPopEvtTimer_c, (void *)(iTimerId));
    return gPopErrNone_c;
  }

  // look for the timer
  i = PopFindTimer(iTaskId, iTimerId, &iFound);
  if(i != gPopEndOfIndex_c)
    iFound = i;

  // no free timers (and timer does not already exist)
  if(iFound == gPopEndOfIndex_c)
    return gPopErrFull_c;

  if(iTimerId != gPopTimerFree_c && iTaskId <= cPopNumTasks){
    // fill in timer structure
    gaPopTimer[iFound].iTimerId  = iTimerId;
    gaPopTimer[iFound].iTaskId   = iTaskId;
    gaPopTimer[iFound].iTimeLeft = PopAdjustTimeoutForLastIdle(iMilliseconds);

    // Increase timers in use count
    if(i == gPopEndOfIndex_c){
      ++giPopTotalTimers;
    }
    // Keep Track of the highest amount of timers registered. Always after the timer is started.
    if(giPopTimerWaterMark < giPopTotalTimers)
      giPopTimerWaterMark = giPopTotalTimers;
  }

  // return true if timer was stared, false if not
  return gPopErrNone_c;
}

/***************************************************************************
  Swap bytes routines.
***************************************************************************/
void PopGwSwapICanHearYouToLittle(uint8_t iNum, uint16_t *pArray)
{
  uint8_t i;

  for(i = 0; i < iNum; i++)
   GatewayToNative16( (popNwkAddr_t *)((uint8_t*)pArray + (i * sizeof(popNwkAddr_t))) );
}
/*
  SwapBytes16 with return value needed only in gateway
*/
uint16_t SwapBytes16WithReturn(uint8_t *pWord)
{
  uint16_t newVal;

  /* First, get the correct value */
  newVal = *(uint8_t*)pWord  << 8;
  newVal += *((uint8_t*)pWord + 1);

  return newVal;
}

void SwapBytes16(uint16_t *pWord)
{
  uint8_t *pBytes = (void*)pWord;
  uint8_t tmp;

  // This could use the REV instruction on Cortex-M

  // reverse the bytes
  tmp = pBytes[0];
  pBytes[0] = pBytes[1];
  pBytes[1] = tmp;
}

/*
  SwapBytes32
*/
void SwapBytes32(uint32_t *pWord32)
{
  uint8_t *pBytes = (void *)pWord32;
  uint8_t tmp;

  // This could use the REV instruction on Cortex-M

  // reverse the bytes
  tmp = pBytes[0];
  pBytes[0] = pBytes[3];
  pBytes[3] = tmp;
  tmp = pBytes[1];
  pBytes[1] = pBytes[2];
  pBytes[2] = tmp;
}

/*
  SwapBytes64
*/
void SwapBytes64(uint64_t *pWord64)
{
  uint8_t *pBytes = (void *)pWord64;
  uint8_t tmp;

  // This could use the REV instruction on Cortex-M

  // reverse the bytes
  tmp = pBytes[0];
  pBytes[0] = pBytes[7];
  pBytes[7] = tmp;
  tmp = pBytes[1];
  pBytes[1] = pBytes[6];
  pBytes[6] = tmp;
  tmp = pBytes[2];
  pBytes[2] = pBytes[5];
  pBytes[5] = tmp;
  tmp = pBytes[3];
  pBytes[3] = pBytes[4];
  pBytes[4] = tmp;
}

/***************************************************************************
  Random Number Functions
***************************************************************************/
/*
 Function:  Wait a number of mS in SCM - Self-Clocked-Mode.
    Approximate bus rate assumed to be 4MHz.
 Parameters: 32-bit mS to delay; 50 timer counts ~ 1ms
    Pass 1 for 1 mS, 1000 for 1S, etc.
 Return:  none.
*/
void PopWait(uint32_t count) {
 uint32_t i;
 count = count*50;
  if (count != 0)
  {
      for (i=0; i<count; i++);
   }
}

/*
  PopRandomInit

  Seed the random number engine. Calling the maca random variable.

 Parameters: none
 Returns an 8-bit pseudo random number that can be used to seed the
 random sequence generator.
*/
void ADC_Setup(void);
void PopRandomInit(void)
{
}

/*
  PopRandom08

  Returns an 8-bit pseudo random number.
*/
uint8_t PopRandom08(void)
{
  return (uint8_t) (halRfGetRandomWord() & 0xFFU);
}

/*
  PopRandom16

  Returns an 16-bit pseudo random number.
*/
uint16_t PopRandom16(void)
{
  return halRfGetRandomWord();
}


/***************************************************************************
  Flash (NVM) Functions
***************************************************************************/

bool gfPopNvNoFlashInit;

#if gPopNumberOfNvPages_c || gPopUsingStorageArea_c

// used to control the power up and down of the nvm
uint8_t iNvmPowerUpDownCounter = 0;
#endif
/*
  PopArm7PowerUpNVM - Turns on the NVM voltage regulator.

  If external SPI is used the user should configure the SPI pins as function 1 from GPIO
  (they are in GPIO mode default). If internal SPI is used the 1.8V regulator should be
  turn on first.

  This function need to be called before any function provided in the NVM driver.
*/
void PopArm7PowerUpNVM(void)
{

}

/*
  PopArm7PowerUpNVM - Turns off the NVM voltage regulator.
*/
void PopArm7PowerDownNVM(void)
{

}

/*
  PopFlashWriteBytes

  Write a set of bytes to flash. Returns true if worked, false if failed.
*/
bool PopFlashWriteBytes(void *pDstAddr, void *pSrcAddr, uint32_t iLen)
{
#if gPopNumberOfNvPages_c || gPopUsingStorageArea_c

  /* Reset the COP in each write. With this we can avoid a COP reset when is writing a big quantity of data */
  PopFeedTheDog();

  {
    int32_t retVal;

    retVal = TiFlashMainPageProgramExt((uint8_t *) pSrcAddr, (uint32_t) pDstAddr, iLen);
    if (retVal != 0)
    {
      return false;
    }
  }

  // everything worked
  return true;
#else
  (void)pDstAddr;
  (void)pSrcAddr;
  (void)iLen;
  return false;
#endif
}

/*
  PopFlashReadBytes

  Write a set of bytes to flash. Returns true if worked, false if failed.
*/
bool PopFlashReadBytes(void *pDstAddr, void * pSrcAddr, uint8_t iLen)
{
#if gPopNumberOfNvPages_c || gPopUsingStorageArea_c

  /* Reset the COP in each read. With this we can avoid a COP reset when is reading a big quantity of data */
  PopFeedTheDog();

  /* Flash memory is mapped to the same address space. Just copy the bytes. */
  // memcpy(pDstAddr, pSrcAddr, iLen);
	ROM_Memcpy(pDstAddr, pSrcAddr, iLen);

  // everything worked
  return true;
#else
(void)pDstAddr;
(void)pSrcAddr;
(void)iLen;
  return false;
#endif
}

#ifndef gPopNvUnitTest_d

/*
  PopNvmErasePage

  Erase a page of flash. Returns true if worked, false if failed.
*/
void PopNvmErasePage(popNvmPage_t iPage)
{
#if gPopNumberOfNvPages_c
  void * pAddress = PopNvGetPage(iPage);

  PopFlashEraseNvmFlash(pAddress);
#else
  (void)iPage;
#endif
}
#endif
/*
  PopNvmEraseAll : public

  This routine clears all the flash pages and reinitializes the system. Use to
  factory reset a system.
*/
// TODO: Convert Flash Routines to Offsets, rather than addresses, for read/write/erase. Make more BSP friendly.
// TODO: Do a better job of separating BSP from PopMem.c routines, for storage, NVM, etc...
void PopNvmEraseAll(void)
{
#if gPopNumberOfNvPages_c
  PopFlashEraseNvmFlash((void *)gNvSector0Address_c);
  PopFlashEraseNvmFlash((void *)gNvSector1Address_c);
#endif
}

// only available if NVM enabled
/*
 Erase NVM flash sectors
*/
bool PopFlashEraseNvmFlash( void *pAddress )
{
#if gPopNumberOfNvPages_c
  {
    int32_t eraseResult;

    eraseResult = TiFlashMainPageErase((uint32_t) pAddress);

    if (eraseResult != 0)
    {
      return false;
    }
  }

  return true;
#else
  (void)pAddress;
  return false;
#endif
}

/*
  PopNvCountFFs : internal

  Return count of 0xff in this buffer up to iMax characters, 0 - (iMax-1).
*/
popNvPageSize_t PopNvCountFFs(uint8_t *pBuffer, popNvPageSize_t iMax)
{
#if gPopNumberOfNvPages_c
  popNvPageSize_t iCount;

  /*
   *  Flash memory is mapped to the same address space. Just read through the
   *  pointer.
   */
  for(iCount = 0; iCount < iMax; iCount++)
  {
    if(*pBuffer++ != 0xff)
    {
      break;
    }
  }

  /* Return the maximum require to check */
  return iCount;
#else
  (void)pBuffer;
  (void)iMax;
  return 0;
#endif
}


/*
  PopNvmReadBytes

  Read a set of bytes from flash. Returns true if worked, false if failed.
*/
uint8_t PopNvmReadBytes(void *pToAddr, popNvmOffset_t iFromOffset, popNvmSize_t iLen)
{
#if gPopNumberOfNvPages_c

  /* Reset the COP in each read. With this we can avoid a COP reset when is reading a big quantity of data */
  PopFeedTheDog();

  /* Flash memory is mapped to the same address space. Just copy the bytes. */
  memcpy(pToAddr, &gpaPopNvFirstFlashPage[iFromOffset], (uint32_t)iLen);

  // everything worked
  return gPopErrNone_c;
#else
  (void)pToAddr;
  (void)iFromOffset;
  (void)iLen;
  return gPopErrFailed_c;
#endif
}


/*
  PopNvmWriteBytes

  Write a set of bytes to flash. Returns true if worked, false if failed.
*/
uint8_t PopNvmWriteBytes(popNvmOffset_t iToOffset, void *pFromAddr, popNvmSize_t iLen)
{
#if gPopNumberOfNvPages_c

  /* Reset the COP in each write. With this we can avoid a COP reset when is writing a big quantity of data */
  PopFeedTheDog();

  {
    int32_t retVal;

    /*
     *  The CC2538 can only write to aligned addresses. The write size must be
     *  multiples of four.
     *
     *  If the program stops here then PopNet writes in a way that CC2538 does
     *  not support directly. Then we need to handle it the same way as in
     *  the PopFlashWriteBytes() function. The code should then be put in a
     *  common function which is called from here and the PopFlashWriteBytes()
     *  function.
     */
    while ((iToOffset & 0x00000003) != 0);
    while ((iLen & 0x00000003) != 0);

    retVal = TiFlashMainPageProgram(pFromAddr, (uint32_t)&gpaPopNvFirstFlashPage[iToOffset], (uint32_t)iLen);

    if (retVal != 0)
    {
      return gPopErrFailed_c;
    }
  }

  // everything worked
  return gPopErrNone_c;
#else
  (void)iToOffset;
  (void)pFromAddr;
  (void)iLen;
  return gPopErrFailed_c;
#endif
}

/*
  This function copy all the pages to spare sector. Remember, in ARM7 SMAC you can not erase pages just sector (= 4096)
*/
bool PopFlashCopyNvPage(void * pSrcAddress, void * pDstAddress)
{
#if gPopNumberOfNvPages_c
  bool fWorked = false;
  uint8_t bytePageBkup[128];

  /* Read the bytes from Nv */
  fWorked = PopFlashReadBytes((void*)(&bytePageBkup), pSrcAddress, 128);

  if(!fWorked)
    return false;

  fWorked = PopFlashWriteBytes(pDstAddress, (void*)(&bytePageBkup), 128);

  if(!fWorked)
    return false;

  return true;
#else
  (void)pSrcAddress;
  (void)pDstAddress;
  return false;
#endif
}

/*
  This function copy all the pages to spare sector. Remember, in ARM7 SMAC you can not erase pages just sector (= 4096)
*/
bool PopFlashBackUpPagesInSpareSector(void)
{
#if gPopNumberOfNvPages_c
  uint8_t iNvPage;
  uint16_t iNvPageByte;
  uint32_t nvSectorAddress, nvSpareSectorAddress;
  bool fWorked = false;

  nvSectorAddress = maNvRawSectorAddressTable[gNvSector_c];
  nvSpareSectorAddress = maNvRawSectorAddressTable[gNvSpareSector];

  /*First always update the spare sector */
  fWorked = PopFlashEraseNvmFlash( (void *)(nvSpareSectorAddress) );

  /* If was an error then go out */
  if(!fWorked)
    return false;

  /* After back up the pages */
  for(iNvPage=0; iNvPage < giPopNvNumPages; iNvPage++)
  {

    for(iNvPageByte = 0; iNvPageByte < giPopNvPageSize; iNvPageByte+=128)
    {
      fWorked = PopFlashCopyNvPage((void *)(nvSectorAddress + (iNvPage * giPopNvPageSize) + iNvPageByte),
                                   (void *)(nvSpareSectorAddress + (iNvPage * giPopNvPageSize) + iNvPageByte));
      if(!fWorked)
        return false;
    }
  }

  return true;

#else
  return false;

#endif
}



// test the low-level flashing routines: just tests erasing and
#ifdef gPopFlashUnitTest_d

/*
  PopFlashUnitTestFail

  Fail a particular test. Displays the failure # (0-ff) in hex both in the LCD
  and the lower nibble on the LEDs.
*/
void PopFlashUnitTestFail(uint8_t iTestNum)
{
#if gPopLcdEnabled_d
  char szStr[17];
  static char szFail[] = "FlUtErr ";

  // display error on LCD
  PopMemSetZero(szStr, sizeof(szStr));
  PopMemCpy(szStr, szFail, sizeof(szFail)-1);
  PopHex2Ascii(szStr + sizeof(szFail), iTestNum);
  PopLcdWriteString(1, szStr);
#endif

  // display error and hang
  PopLedDisplayHex(iTestNum);
  while(giPopForever)
    /* do nothing */;
}

/*
  PopFlashUnitTest
  Test the low-level flash routines
*/
void PopFlashUnitTest(void)
{
  uint8_t   i;
  bool    fWorked;
  popNvPageSize_t iSize;
  uint32_t  iDWord;
  uint8_t    *pMem;
  uint32_t data;
  uint8_t data2;

  PopArm7PowerUpNVM();
  PopFlashInit();

 // test 1 - erase all pages and verify they are erased
  for(i=0; i<giPopNvNumPages; ++i)
  {
    fWorked = PopNvmErasePage((void *)(&gpaPopNvFirstFlashPage[i * giPopNvPageSize]));
    if(!fWorked)
      PopFlashUnitTestFail(1);
  }
  for(i=0; i<giPopNvNumPages; ++i)
  {
    iSize = PopNvCountFFs((void *)(&gpaPopNvFirstFlashPage[i * giPopNvPageSize]), giPopNvPageSize);
    if(iSize != giPopNvPageSize)
      PopFlashUnitTestFail(1);
  }

  // test 2 - write pattern to each page, verify pattern worked
  for(i=0; i<giPopNvNumPages; ++i)
  {
    for(iSize=0; iSize < giPopNvPageSize; iSize += sizeof(iDWord))
    {
      iDWord = ((uint32_t)iSize << 16) | (0xa900 | i);  // combine offset (iSize) and page (i)
      fWorked = PopFlashWriteBytes((void *)(&gpaPopNvFirstFlashPage[i * giPopNvPageSize + iSize]),
                &iDWord, sizeof(iDWord));
      if(!fWorked)
        PopFlashUnitTestFail(2);
    }
  }
  // verify all bytes where written
  for(i=0; i<giPopNvNumPages; ++i)
  {
    for(iSize=0; iSize < giPopNvPageSize; iSize += sizeof(iDWord))
    {
      iDWord = ((uint32_t)iSize << 16) | (0xa900 | i);

      fWorked = PopFlashReadBytes((void *)(&data), (void *)(&gpaPopNvFirstFlashPage[i * giPopNvPageSize + iSize]), sizeof(data));

      if(fWorked)
      {
        if(PopMemCmp((void *)(&data), &iDWord, sizeof(iDWord)))
          PopFlashUnitTestFail(2);
      }
    }
  }

  // test 3 - write a single byte and a full sized packet
  for(i=0; i<giPopNvNumPages; ++i)
  {
    fWorked = PopNvmErasePage((void *)(&gpaPopNvFirstFlashPage[i * giPopNvPageSize]));
    if(!fWorked)
      PopFlashUnitTestFail(3);
  }
  fWorked = PopFlashWriteBytes((void *)gpaPopNvFirstFlashPage, &i, sizeof(i));

  fWorked = PopFlashReadBytes((void *)(&data2), (void *)gpaPopNvFirstFlashPage, sizeof(data2));

  if(!fWorked || data2 != i)
      PopFlashUnitTestFail(3);

  pMem = PopMemAlloc(gPopMemMaxAlloc_c);

  if(!pMem)
      PopFlashUnitTestFail(3);

  PopMemSet(pMem, 0xa5, gPopMemMaxAlloc_c);

  fWorked = PopFlashWriteBytes((void *)(gpaPopNvFirstFlashPage + 1), pMem, gPopMemMaxAlloc_c);

  if(!fWorked)
    PopFlashUnitTestFail(3);

  /* Erase the 0xa5 and set 0xa7 to verify that read bytes is working well */
  PopMemSet(pMem, 0xa7, gPopMemMaxAlloc_c);

  /* use the same allocated buffer to store the nvm data read */
  fWorked = PopFlashReadBytes((void *)pMem, (void *)(gpaPopNvFirstFlashPage + 1), gPopMemMaxAlloc_c);

  if(!fWorked || !PopMemIsFilledWith((void *)pMem, 0xa5, gPopMemMaxAlloc_c))
      PopFlashUnitTestFail(3);

  /* Free memory */
  PopMemFree(pMem);

  /* Turn off the NVM Internal SPIF */
  PopArm7PowerDownNVM();
}

#endif  // #ifdef gPopFlashUnitTest_d


uint8_t gaNvPageHeader[ sizeof(sPopNvPage_t) ];
// This intermediary array will handle the maximum nvm data permitted.
uint8_t gaNvEntryData[0x04 + gPopNvMaxDataLen_c + 0x04];

void * PopNvGetPageHeader(void *pPageSrcAddress)
{
  bool iReadStatus = false;

  iReadStatus = PopFlashReadBytes( (void*)(&gaNvPageHeader), pPageSrcAddress, sizeof(sPopNvPage_t));

  return  ( (iReadStatus ) ? (&gaNvPageHeader) : NULL );
}

void * PopNvGetEntryData(void *pEntrySrcAddress, uint8_t iNvEntrySise)
{
  bool iReadStatus = false;

  /* First we need to locate the NvEntry here we don't know the length of the entry yet */
  iReadStatus  = PopFlashReadBytes((void*)(&gaNvEntryData), pEntrySrcAddress, (uint8_t)iNvEntrySise);

  return  ( (iReadStatus ) ? (&gaNvEntryData) : NULL );
}

#ifndef gPopNvUnitTest_d
/*
  PopFlashInit

  Initializes the flash type
*/
void PopFlashInit(void)
{
  // don't wipe flash
  if(gfPopNvNoFlashInit)
    return;
}

#endif
/***************************************************************************
  Low Power Functions
***************************************************************************/

#if gPopLowPower_d

uintn32_t macaClk;

/************************************************************************************
* void after_wakeup_isr(void)
*
* This function enabled the wake up event after the transceiver wakes up from
* sleep mode. This is the callback of the KBI4 Interruption.
************************************************************************************/
void PopKeyBoard_Wakeup_ISR(void)
{
  // Disable RTC wake up interrupt to avoid this interruption be triggered
  PopLowPowerDisableRTCWuInterrupt();

  /* Debounce time in milliseconds 2 ms per 1K plus 2 ms */
  DelayMs(200);

}

void PopInitLowPower(void)
{
  PopKeyBoard_Wakeup_ISR();

  /*
    Just calls the Keyboard init to configure the keyboard. However, it does not matter because this function
    is called in BspTaskInit function when the node starts.
  */

 PopKeyInit( PopKeyBoard_ISR );
}

/**************************************************************
*  Low Power main function - enter ONLY when ready to enter low power mode
*  Parameters: none.
*  Return:    none.
**************************************************************/
void PopThingToDoBeforeToEnterLowPower(void)
{

  // Turn off the nvm
  PopArm7PowerDownNVM();

  /* Before to enter to low power is necessarie turn off the PA - just for CELFreeStarPro-*/
  PAVoltageRegOff();

  /* Turn off the ADC Module */
  Adc_TurnOff();
}
/**************************************************************
*  Low Power main function - enter ONLY when ready to enter low power mode
*  Parameters: none.
*  Return:    none.
**************************************************************/
void PopEnterLowPower(void)
{
  crmSleepCtrl_t SleepCtl;
  uint32_t u32LoopDiv;

  // All LEDs off
  PopSetLed(gPopLedAll_c, gPopLedOff_c);

  /* How many ram we want to retain?. In this case, we are retained all the ram */
  SleepCtl.ramRet = gRAMRetentionMode_c;

  /* Mcu retained */
  SleepCtl.mcuRet = gMcuRet_c;

  /*
    For Kaibab is not necessary do something before to sleep because the ram
    and rom is retained all the time, at least, in this moment I haven't consider nothing.
  */
  SleepCtl.pfToDoBeforeSleep = PopThingToDoBeforeToEnterLowPower;

  // Reset the COP before to sleep.
  PopFeedTheDog();

  // Send to hibernate request to turn off the radio
  MLMEHibernateRequest(gRingOsc2khz_c, SleepCtl);

  // Waits 200 milliseconds
  DelayMs(200);

  CRM_Xtal32Disable();

// After awake is necessarie turn on the PA - just fir CELFreeStarPro-
  PAVoltageRegOn();

  DelayMs(120);

  u32LoopDiv = ((gDigitalClock_RN <<25) + gDigitalClock_RAFC);

  RadioInit(PLATFORM_CLOCK, gDigitalClock_PN, u32LoopDiv);
  MLMEPHYXtalAdjust(gDefaultCoarseTrim, gDefaultFineTrim);

  SetComplementaryPAState(gEnableComplementaryPAOutput);
  SetPowerLevelLockMode(gPowerLevelLock);
  SetEdCcaThreshold(gCcaThreshold);

  CRM_REGS_P->VregCntl = 0x00000F78; //Enables the radio voltage source

  DelayMs(250);

  // Set the Power level
  (void)MLMEPAOutputAdjust(gDefaultPowerLevel);

  // Set the channel
  MLMESetChannelRequest((channel_num_t)(PopNwkGetChannel() - 11));

  /* Restore normal keyboard function */
  PopKeyInit( PopKeyBoard_ISR );

  /* Notify to Bsp we have waken */
  (void)PopStartTimerEx(cBspTaskId,gPopWakeupTimer_c,200);

  /* Reset the COP after wake up.*/
  PopFeedTheDog();

 // Turn on the nvm
  PopArm7PowerUpNVM();

  // Waits 200 milliseconds
  DelayMs(200);

  /* Turn on the ADC Module */
  Adc_TurnOn();
}

void PopLowPowerDisableRTCWuInterrupt(void)
{
  // Disable the RTC Wake up
  CRM_WU_CNTL.rtcWuEn = 0;

  // Disable RTC Wake up Interrupt
  CRM_RTCInterruptDisable();

  // Clear the counter
  CRM_RTCSetCountToZero();

  // Force the RTC to timeout
  CRM_RTCSetTimeout(0);

}

/*
  PopLowPowerRtcCallback
*/

void PopLowPowerRtcCallback()
{
  //uint32_t u32LoopDiv;
  static uint8_t ready = 0;

  // Disable RTC wake up interrupt to avoid this interruption be triggered again
  PopLowPowerDisableRTCWuInterrupt();

  if ( ready != 0 )
    return;

  // Do it just one time
  ready = 1;

  /* Debounce time in milliseconds 2 ms per 1K plus 2 ms */
  DelayMs(200);

  /*
    The status bit in CRM STATUS register will be asserted and must be cleared.

    The status/interrupt request will persist for 2 cycles of the low frequency
    hibernate clock after servicing.

    Note: This may require that this interrupt be disabled in the RTI for these 2
    cycles or until the status reads as clear.
  */
  do
  {
 //clear the RTC stattus event
  CRM_STATUS.rtcWuEvt = 1;

    // It wil waits 7 cycles
    DelayMs(7);
  }
  while ( 1 == CRM_STATUS.rtcWuEvt );

  giWakeUpByTime = true;
}

/*
  PopPwrSleep

  This function puts the node to sleep. It won't wake up until a keyboard
  interrupt.
*/
popErr_t PopPwrSleep(popPwrWakeUpOn_t iWakeUpOn, popPwrTimeOut_t iTimeOutMs)
{
  popErr_t Error_WuKBI  = gPopErrFailed_c;
  FuncReturn_t drvErrmsg;
  bool disableOtherWuSrc = true;

  /* Disable the timer wake-up source because it is not needed in PopNet */
  CRM_WU_CNTL.timerWuEn = 0;

  /* If the order is use both then don't disable the others wake up sources */
  if( iWakeUpOn == gPopPwrWakeUpOnBoth_c)
  {
    disableOtherWuSrc = false;
  }

  /* Just KBI Source */
  if (iWakeUpOn & gPopPwrWakeUpOnKbd_c)
  {

    if( disableOtherWuSrc )
    {
      /* Disable the RTC wake-up source because we want just KBI */
      CRM_WU_CNTL.rtcWuEn = 0;
    }

    // Set to no error
    Error_WuKBI = gCrmErrNoError_c;

    // Change the keyboard Isr and set the KBI as wake up source
    PopInitLowPower();

  }

  /* Just RTC wake up Source */
  if (iWakeUpOn & gPopPwrWakeUpOnTime_c)
  {

#if (gPopKeyboardEnabled_d)
    if( disableOtherWuSrc )
    {
      /* Disable the KBI wake-up source because we want just KBI */
      CRM_WU_CNTL.extWuEn = 0;
    }
#endif

    // Reset the RTC count
    CRM_REGS_P->RtcCount = 0;

    // Force the RTC timeout
    CRM_RTCSetTimeout(0);

    CRM_WuTimerInterruptDisable();

    CRM_RingOscillatorEnable ();

    // Set the wake up source in this case like RTC form
    (void)MLMESetWakeupSource(gRTCWuEn_c, 0x00, 0x00);

    // Remember that 20,000 ticks is more or less 10s for 2 KHz oscillator.
    // So, 1625 ticks are more or less 1.03s.
    drvErrmsg = DRVConfigureRTC(gTimerRef_2Khz_c, (uint32_t)(iTimeOutMs * 1625), PopLowPowerRtcCallback);

  }

  // Enter to low power only if there was no error
  if( !Error_WuKBI || !drvErrmsg )
  {
    // Save the maca clock before to enter low power
    macaClk = maca_clk;

    // Enter to low power
    PopEnterLowPower();
  }

  return (Error_WuKBI | ((popErr_t)drvErrmsg));
}

#endif

#if gPopLowVoltageDetection_d
/*
  PopIsThereALowVoltageWarning

  This function checks and return a value acording to the value in the Lvw flag,
  then clears it by acknoleging the Lvw.
*/
bool PopIsThereALowVoltageWarning(void)
{
  bool  fWarning;

  /*
    Set the return value depending the value in the Lvw flag, befor is clear by
    the ack.
  */
  fWarning = PopLvwGetFlag()?gTrue_c:gFalse_c;

  /* Ack the Lvw, to clear the Lvw flag*/
  gPopLvwGetFlag = false;

  return fWarning;
}

/*
  This function consult the battery channel from ADC and gets the value.
*/
uint16_t PopGetBatteryAdcRef(void)
{
  uint16_t adcBattRef;

  /* Conversion  for adc battery ref */
  Adc_StartManualConv(gAdcPrimary_c, gAdcBatt_c);

  /* Wait 1 ms */
  DelayMs(1);

  /* Read the ADC value */
  Adc_ManualRead(gAdcPrimary_c, &adcBattRef);

  return adcBattRef;
}

/*
This function checks the battery level and set the gPopLvwGetFlag to true is the voltage
is under Risisng 2.19V.

The next formula is fixed to 1.2 V.

0x0FFF      VBATT
------   = -------
ADCValue    1.2 V
*/
void PopCheckBaterryLevel(uint16_t adcValue)
{

  float VBatt = 0;

  /* Calcute the voltage ref*/
  VBatt = (float) ((1.2 * (float)0x0FFF) / adcValue);

  /* Just one time */
  if( !gPopLvwGetFlag && (VBatt < gPopVoltageRisingValue_c) )
  {
    /* We are under the voltage specified. */
    gPopLvwGetFlag = true;
  }
}

#endif
// Set the funciton into the boot loader section. First 4KB.
#if gPopOverTheAirUpgrades_d
#pragma location="popFillCStack"
void PopFillCStack(void);
#endif

/*
  PopGetStackSize

  Returns the amount of bytes in the C stack.
*/
uint32_t PopGetStackSize(void)
{
  return (uint32_t) &STACK_SIZE;
}

/*
  PopFillCStack

  Fill the C Stack with a known value to determine how much stack is used at run-time.
  Also mark the bottom of the stack to detect stack over-runs.
*/
void PopFillCStack(void)
{
  uint32_t *pSP;
  __SSTACK = (uint32_t *)(((uint8_t *) &STACK_TOP - (uint32_t) &STACK_SIZE));
  pSP = __SSTACK;

  // for detecting stack usage. This -1 fix a SP override in MC13226
  while (pSP < ((uint32_t *) &STACK_TOP - 1))
    *pSP++ = 0x4D4D4D4D;

  // for detecting stack over-runs
 pSP = __SSTACK;
 *pSP = SP_WaterMark_c;
}


#if gPopSerial_d

/* restart the entire module for new parameters */
void PopSerialRestartModule(void)
{
  // for the smallest serial I/O, never reset buffer pointers
  gpPopSerialTxBufHead =
  gpPopSerialTxBufTail = gaPopSerialTxBuf;
  gpPopSerialRxBufHead =
  gpPopSerialRxBufTail = gaPopSerialRxBuf;

  Uart_Init(gaPopSerialRxBuf);

  // nothing received yet
  gfPopSerialStatus = 0;
}

/*
  PopSerialSettings

  application serial settings
*/
popErr_t PopSerialSettings( gPopBaudRate_t iBaudRate, uint16_t iMinGapMs )
{
  UartConfig_t uartConf;
  static bool fInitialized;

  // note: this list must match the values in PopNet.h. See gPopBaudRate_t
  static uint32_t maBaudRates[] =
  {
    Baudrate_2400,
    Baudrate_4800,
    Baudrate_9600,
    Baudrate_19200,
    Baudrate_38400,
    Baudrate_57600,
    Baudrate_115200
  };

  if(!fInitialized)
  {
    fInitialized = true;
    PopSerialRestartModule();
  }

  // in case this is called run-time after system has started
  (void)PopStopTimerEx(cSerialTaskId, gPopSerMinGapTimer_c );

  // don't allow invalid baud rates
  if(iBaudRate > PopNumElements(maBaudRates))
    return gPopErrBadParm_c;

  // set baud rate, etc.. on UART
  giPopSerialBaudRate = iBaudRate;
  giPopSerialMinGapMs = iMinGapMs;

  /* if the status is not equals to gUartErrUartNotOpen_c then check the status */
  UartErr_t uartError = UartGetStatus(gUart_PortDefault_d);

  if(uartError != gUartErrUartNotOpen_c)
  {
    /* Cancel Read ongoing */
    while( UartGetStatus(gUart_PortDefault_d) == gUartErrReadOngoing_c)
    {
      (void)UartCancelReadData(gUart_PortDefault_d);
    }
    /* Cancel write ongoing */
    while( UartGetStatus(gUart_PortDefault_d) == gUartErrWriteOngoing_c)
    {
      (void)UartCancelWriteData(gUart_PortDefault_d);
    }
  }

  /* We assume that there are more problems */

  // Get the actual configuration
  UartGetConfig(gUart_PortDefault_d, &uartConf);

  // Set the new baud rate (leave other parameters alone)
  uartConf.UartBaudrate = maBaudRates[iBaudRate];

  // Set the new Serial Port configuration
  UartSetConfig(gUart_PortDefault_d, &uartConf);

  /* then we set RX again */
  UartReadData(gUart_PortDefault_d, gaPopSerialRxBuf, gPopSerialRxBufferSize_c, true);

  return gPopErrNone_c;
}

// <SJS: Added Serial Tx Rx State Machine>
/*
  PopSerialSend()

  Send data out the UART when there is time available. Uses interrupt driven I/O and returns immediately.

  Returns
  gPopErrNone_c         Everything worked.
  gPopErrBusy_c         Could not send out data... system is busy
  gPopSerInvalidArg_c   Invalid argument
*/
popErr_t PopSerialSend( uint16_t iLen, void *pBuffer, popSerialSendFlags_t iFlags)
{
  popErr_t iErr = gPopErrNone_c;
  bool fFree = false;

  // user wants packet freed
  if(iFlags & gPopSerialSendFlags_FreeOnTxDone_c)
    fFree = true;

    // no buffer, invalid argument
  if ( !pBuffer )
    iErr = gPopSerInvalidArg_c;

  // no data, done
  else if( !iLen )
    iErr = gPopErrNone_c;

  // if no room in the queue, don't add it
  else if(giPopSerialSendNumInQueue >= gPopSerialSendQueueSize_c)
    iErr = gPopSerBusy_c;

  // add the packet at the head for sending out
  else
  {
    gaPopSerialSendQueue[giPopSerialSendQueueHead].iFlags = iFlags;
    gaPopSerialSendQueue[giPopSerialSendQueueHead].iLen   = iLen;
    gaPopSerialSendQueue[giPopSerialSendQueueHead].pData  = pBuffer;
    ++giPopSerialSendQueueHead;
    if(giPopSerialSendQueueHead >= gPopSerialSendQueueSize_c)
      giPopSerialSendQueueHead = 0;
    ++giPopSerialSendNumInQueue;

    // if this is the 1st, start up state machine
    if(giPopSerialSendNumInQueue == 1)
      (void)PopSetEventEx(cBspTaskId, mPopBspSerialSendNext_c, NULL);

    fFree = false;   // will be freed when done transmitting
  }

  if(fFree)
    PopMemFree(pBuffer);

  return iErr;
}

/*
  Send next packet in the state machine
*/
void PopSerialSendNext(void)
{
  if(giPopSerialSendNumInQueue)
  {
    DelayMs(1);

    if(UartGetStatus(UARTx) != gUartErrWriteOngoing_c)
      UartWriteData(UARTx,gaPopSerialSendQueue[giPopSerialSendQueueTail].pData,gaPopSerialSendQueue[giPopSerialSendQueueTail].iLen);
    else
     (void)PopSetEventEx(cBspTaskId, mPopBspSerialSendNext_c, NULL);
  }
}

/*
  Done with transmission of a packet. Go on to next.
*/
void PopSerialSendDone(void)
{
  // done, remove item from queue
  if(giPopSerialSendNumInQueue)
  {
    // free memory if requested
    if(gaPopSerialSendQueue[giPopSerialSendQueueTail].iFlags & gPopSerialSendFlags_FreeOnTxDone_c)
      PopMemFree(gaPopSerialSendQueue[giPopSerialSendQueueTail].pData);

    // done with entry
    PopMemSetZero(&gaPopSerialSendQueue[giPopSerialSendQueueTail], sizeof(sPopSerialSendQueue_t));
    ++giPopSerialSendQueueTail;
    if(giPopSerialSendQueueTail >= gPopSerialSendQueueSize_c)
      giPopSerialSendQueueTail = 0;
    --giPopSerialSendNumInQueue;
  }

  // on to next (if there is one)
  if(giPopSerialSendNumInQueue)
  {
   (void)PopSetEventEx(cBspTaskId, mPopBspSerialSendNext_c, NULL);
  }

  // Tell the Serial Task that the transmission has been completed.
  else
  {
    gfPopSerialStatus |= gPopSerTxDone_c;
    (void)PopSetEventEx(cSerialTaskId, gPopSerTxDone_c, NULL);
  }
}

/*
  PopSerialTxIsr1

  UART Transmit ISR. Called on Tx interrupt - data register empty. Data added
  to head, removed from tail.
*/
void PopSerialTxIsr(UartWriteCallbackArgs_t* args)
{
  // indicate we're done with that packet
  (void)PopSetEventEx(cBspTaskId, mPopBspSerialSendDone_c, NULL);
}

/*
  PopSerialRxIsr

  UART Receive ISR. Called on Rx interrupt. Data added to head, removed from
  tail.
*/
void PopSerialRxIsr(UartReadCallbackArgs_t* pArgs)
{
  uint8_t c;

  gu8SCIDataFlag = true;

  gu8SCIStatus = pArgs->UartStatus;

  if( gu8SCIStatus == gUartReadStatusComplete_c )
  {
    /*
     *  Ari:  Modified this part because the UartNumberBytesReceived does not
     *        contain the actually received number on the CC2538 port.
     */

#if 0
    uint16_t iCount;

    for(iCount=0; iCount < pArgs->UartNumberBytesReceived; ++iCount)
    {
      UartGetByteFromRxBuffer(UARTx, &c);

      *gpPopSerialRxBufHead++ = c;

      // head has wrapped in ring buffer
      if ( gpPopSerialRxBufHead >= (gaPopSerialRxBuf + gPopSerialRxBufferSize_c) )
        gpPopSerialRxBufHead = gaPopSerialRxBuf;

      // if head catches up to tail, we've overrun, set (sticky) flag to indicate such
      if ( gpPopSerialRxBufHead == gpPopSerialRxBufTail )
        gfPopSerialStatus |= gPopSerRxOverflow_c;
    }
#else
    bool byteReceived;

    byteReceived = UartGetByteFromRxBuffer(UARTx, &c);

    while(byteReceived)
    {
      *gpPopSerialRxBufHead++ = c;

      // head has wrapped in ring buffer
      if ( gpPopSerialRxBufHead >= (gaPopSerialRxBuf + gPopSerialRxBufferSize_c) )
        gpPopSerialRxBufHead = gaPopSerialRxBuf;

      // if head catches up to tail, we've overrun, set (sticky) flag to indicate such
      if ( gpPopSerialRxBufHead == gpPopSerialRxBufTail )
        gfPopSerialStatus |= gPopSerRxOverflow_c;

      byteReceived = UartGetByteFromRxBuffer(UARTx, &c);
    }
#endif

    // indicate byte received to appropriate task
    gfPopSerialStatus |= gPopSerRxData_c;

    // tell serial task one or more bytes have been received
    (void)PopSetEventEx(cSerialTaskId, gPopSerByteReceivedEvt_c, NULL);
  }

  // reenable interupts. We had an error
  else if( gu8SCIStatus == gUartReadStatusError_c)
  {
    /*
      Disabling Rx interrupt corresponding to UART driver. If we disable global interruptions then we
      will be affecting the other interrupts.
    */
    UartCloseReceiver(gUart_PortDefault_d);

    // Clear uart status error
    UartClearErrors(gUart_PortDefault_d);

    // Read the data stored in Rx buffer at the moment of the error.
    while( UartGetByteFromRxBuffer(gUart_PortDefault_d,&c) );

    /*
      Enabling Rx interrupt corresponding to UART driver. If we disable global interruptions then we
      will be affecting the other interrupts.
    */
    UartOpenReceiver(gUart_PortDefault_d);

    // Clear the args status.
    pArgs->UartReadError.UartReadOverrunError = 0;
    pArgs->UartReadError.UartParityError = 0;
    pArgs->UartReadError.UartFrameError = 0;
    pArgs->UartReadError.UartStartBitError = 0;
    pArgs->UartStatus = gUartReadStatusComplete_c;
    pArgs->UartNumberBytesReceived = 0;

  }
}

#endif //gPopSerial_d

/******************************************************************************************
*                                                                                         *
*                               STORAGE FEATURE                                           *
*                                                                                         *
******************************************************************************************/

/*
  PopStorageReadBytes

  Reads from the storage area N amount of bytes on a given offset.
*/

uint8_t PopStorageReadBytes(void *pDstAddr, popStorageSize_t iSrcOffset, popStorageSize_t  iLen)
{
#if gPopUsingStorageArea_c
#if gPopMicroPowerExternalFlash_d
  // use Micropower External Flash routine instead
  return MPFlashReadBlocking( (gStorageStart_c + iSrcOffset), pDstAddr, iLen);
#else
  bool fStatus;
  /*
    Read form the beginning of the storage are plus an offset N bytes and copy one at
    the time to the destination address.
  */
  PopArm7PowerUpNVM();
  fStatus = PopFlashReadBytes((void*)pDstAddr, ((unsigned char *)gStorageStart_c + iSrcOffset), iLen);
  PopArm7PowerDownNVM();

  if(fStatus)
    return gPopErrNone_c;

  // Functions must return a value by default.
  return gPopErrFailed_c;
#endif //gPopMicroPowerExternalFlash_d
#else
  (void)pDstAddr;
  (void)iSrcOffset;
  (void)iLen;

  return gPopErrFailed_c;
#endif //gPopUsingStorageArea_c
}

/*
  PopStorageWriteBytes

  Write N bytes bytes into the mass storage area. This assumes it has already been erased.

  Takes the mount of bytes described by parameter iLen and copies them from the address
  specified by pSrcAddr, and writes them into the storage area on a given offset described
  bythe parameter iDstOffset.

  The function assumes that the parameter pSrsAddr is not a null pointer

  Returns gPopErrNone_c if the writing process was complete with success, and gPopErrFailed_c otherwise.
*/
uint8_t PopStorageWriteBytes(popStorageSize_t iDstOffset, void *pSrcAddr, popStorageSize_t iLen)
{
#if gPopUsingStorageArea_c
#if gPopMicroPowerExternalFlash_d
  // use Micropower External Flash routine instead
  return MPFlashWriteBlocking( (gStorageStart_c + iDstOffset), pSrcAddr,iLen);
#else
  bool fStatus;
  PopArm7PowerUpNVM();
  fStatus = PopFlashWriteBytes((void *)(((uint8_t *)gStorageStart_c) + iDstOffset), (void *)pSrcAddr, iLen);
  PopArm7PowerDownNVM();

  if(fStatus)
    return gPopErrNone_c;

  // Functions must return a value by default.
  return gPopErrFailed_c;
#endif
#else
  (void)iDstOffset;
  (void)pSrcAddr;
  (void)iLen;

  return gPopErrFailed_c;
#endif
}


/*
  Erases a single block (4096 bytes) containing the address of the specified offset

  Returns gPopErrNone_c if worked, gPopErrFailed_c if an error occurred
  gPopErrBadParm_c if the offset is out of boundaries
*/
uint8_t PopEraseBlock(popStorageSize_t iBlockOffset)
{
#if gPopUsingStorageArea_c
  if (iBlockOffset > PopStorageTotalSize())
    return gPopErrBadParm_c;
#else
  (void)iBlockOffset;
#endif
  // Functions must return a value by default.
  return gPopErrNone_c;
}

/*
  Erases the entire storage device in preparation for writing to it.

  Returns true if worked, false if failed.
*/
void PopStorageEraseAll(void)
{
#if gPopUsingStorageArea_c
  // code assumes storage size is an even multiple of page size
  // Move the pointer form where the storage start 4 bytes (GBIF size) to read the size of the whole storage image
  PopArm7PowerUpNVM();
  // Initialize Nvm
  PopFlashInit();

# if gPopMicroPowerExternalFlash_d
  TiFlashMainPageErase((uint32_t) StorageMacAddress_c);
# endif //gPopMicroPowerExternalFlash_d

  PopArm7PowerDownNVM();
#endif
}


/*
  Erases the "entire" external storage device in preparation for writing to it.

  Returns true if worked, false if failed.
*/
void PopExternalStorageEraseAll(void)
{
#if gPopUsingStorageArea_c
#if gPopMicroPowerExternalFlash_d
  // use Micropower External Flash routine instead
  MPFlashEraseBlockRangeBlocking(FLASH_OTA_BINARY_ADDRESS_FIRST, (FLASH_OTA_BINARY_ADDRESS_LAST/FLASH_ERASE_BLOCK_SIZE));
#endif //gPopMicroPowerExternalFlash_d
#endif //gPopUsingStorageArea_c
}

#if gPopMicroPowerLpcUpgrade_d
void PopLpcExternalStorageEraseAll(void)
{
  // use Micropower External Flash routine instead
  MPFlashEraseBlockRangeBlocking(FLASH_OTA_BINARY_ADDRESS_FIRST, (FLASH_OTA_BINARY_ADDRESS_LAST/FLASH_ERASE_BLOCK_SIZE));
}
#endif //gPopMicroPowerLpcUpgrade_d

/*
  Initialize flash for storage, enables the appropriate interface to communicate
  with the flash.
*/
void PopStorageInit(void)
{
#if gPopUsingStorageArea_c
  PopArm7PowerUpNVM();

  // Init Nvm
  PopFlashInit();

  PopArm7PowerDownNVM();
#if gPopMicroPowerExternalFlash_d
  // Initialize external flash
  FlashInit();
#endif

#endif
}

/*
  PopBootSectionWriteBytes

  Write N bytes bytes into the mass boot storage area. This assumes it has already been erased.

  Takes the mount of bytes described by parameter iLen and copies them from the address
  specified by pSrcAddr, and writes them into the boot section on a given offset described
  by the parameter iDstOffset.

  The function assumes that the parameter pSrsAddr is not a null pointer

  Returns true if the writing process was complete with success, and false otherwise.
*/
#if gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d
bool PopBootSectionWriteBytes(popStorageSize_t iDstOffset, void *pSrcAddr, popStorageSize_t iLen)
{
  bool fStatus;

  PopArm7PowerUpNVM();
  fStatus = PopFlashWriteBytes((void *)(((uint8_t *)gBootStart_c) + iDstOffset), (void *)pSrcAddr, iLen);
  PopArm7PowerDownNVM();

  return fStatus;
}
#endif //gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d

/*
  PopRetrieveMacAddr

  Gets the Mac Address from its storage right after an ota upgrade
*/
#if gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d
void PopRetrieveMacAddr(void * pDst)
{
  // Get the MAC Address. We assume that the read always will be successful
  // If a mac is stored in nvm this value will be overwritten
  PopArm7PowerUpNVM();
  (void)PopFlashReadBytes((void *)pDst, (void *)StorageMacAddress_c , sizeof(aPopMacAddr_t) );
  PopArm7PowerDownNVM();

  // delete storage we do not need that info anymore
  PopStorageEraseAll();
}
#endif //gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d

/*
  PopStoreMacAddr

  Saves the Mac Address during an Ota upgrade
*/
#if gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d
popErr_t PopStoreMacAddr(void * pSrc)
{
  /* Write the MAC IEEE address given */
  PopArm7PowerUpNVM();
  if(!PopFlashWriteBytes((void *)StorageMacAddress_c, pSrc, sizeof(aPopMacAddr_t)))
  {
    PopArm7PowerDownNVM();
    return gPopErrFailed_c;
  }
  PopArm7PowerDownNVM();
  return gPopErrNone_c;
}
#endif //gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d

/*****************************************************************************************
*                                                                                        *
*                            MC1322X OTA UPGRADE FEATURE                                 *
*                                                                                        *
******************************************************************************************/
#if gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d
/*
  PopCheckStoredImage

  Verifies that the new image contains the appropriate signature and checksum

  Returns gPopErrNoIma
*/
popErr_t PopCheckStoredImage(void)
{
  uint8_t aSignature[4];   //store possible "A7IF"
  uint16_t iSum = 0;      //store calculated checksum
  uint8_t iTemp[255];     //temporal data
#if gPopMicroPowerExternalFlash_d
  uint32_t iOffSet = sizeof(aSignature);  // offset after signature
#else
  uint16_t iOffSet = sizeof(aSignature);  // offset after signature
#endif
  uint32_t iEndOfImage;   //store image length
  uint16_t iChecksum = 0; //stored checksum to compare with iSum
  uint8_t iCount;

  // Read the first 4 bytes ("A7IF") of the stored image to validate it.
  if(gPopErrNone_c != PopStorageReadBytes((void *)aSignature, 0, sizeof(aSignature)))
    return gPopErrNoImage_c;

  // See if the current image has the proper Signature.. if not, is not a valid image.
  if (PopMemCmp(aSignature, "A7IF", sizeof(aSignature)))
  {
    // Signature is not there.... we can not switch
    return gPopErrNoImage_c;
  }

  // Find the image length.
  if( gPopErrNone_c != PopStorageReadBytes((void *)&iEndOfImage, iOffSet, 4))
    return gPopErrNoImage_c;

  // 4 bytes to skip the size of the image.
  iOffSet += sizeof(iEndOfImage);
  //Preserve NVM byte is right after the size of the image skip it
  iOffSet += sizeof(uint8_t);
  // Calculate length of image to be copied.
  iEndOfImage = iOffSet + iEndOfImage;

  // Turn on nvm to get the info we need doing it outside the cicle keeps it on
  // and makes the process faster
  PopArm7PowerUpNVM();
  // Calculate checksum to validate what is in the storage
  while(iOffSet < iEndOfImage)
  {
    //Get 255 bytes from storage
    if(gPopErrNone_c != PopStorageReadBytes((void *)iTemp, iOffSet,sizeof(iTemp)))
      return gPopErrNoImage_c;

    // keep generating the check sum
    for(iCount=0;iCount < (sizeof(iTemp));iCount++)
    {
      iSum += iTemp[iCount];
      if((iOffSet+iCount) == iEndOfImage)
        break;
    }
    //Increase offset
    iOffSet+=sizeof(iTemp);
  }
  // finally turn nvm down after the cicle
  PopArm7PowerDownNVM();

  //End of image and then 4 0bytes and then there is the checksum
  iOffSet = iEndOfImage + 4;
  //Get the stored checksum
  if(gPopErrNone_c != PopStorageReadBytes((void *)&iChecksum, iOffSet,2))
    return gPopErrNoImage_c;

  // If the checksums are different the stored images has an error.
  if (iSum != iChecksum)
  {
    // Inform the current error.
    return gPopErrFailed_c;
  }

  /* everything ok */
  return gPopErrNone_c;
}
#endif // gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d

#if gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d
#pragma location="boot_loader"
#endif

/*
  Switch to an image stored in the storage area. See PopStorageRead().

  The image must included a valid code image for this hardware. This was already verified with
  the over-the-serial or over-the-air protocol prior to storage in this area.

  This routine:
  1. Validates the image: is the image 100% there (and checksummed), is there a valid signature?
  2. It preserves the NVM (application and NWK data) into storage if the user requests it.
  3. Updates a "maker" area in NVM to inform the bootloader it's time to update to a new image
  4. Resets the board so it will enter the bootloader code

  It returns an error if the code image does not validate.
  It does NOT return if code image is valid (instead the board is rebooted).
*/
#if gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d
uint8_t PopSwitchToNewImage(bool fPreserveNVM)
{
  popErr_t iImageCheck = gPopErrNone_c;

  PopSetLed(gPopLed3_c, gPopLedOn_c); // Indicate we received a Boot To New Image command

  // store mac address before changing image
  if( gPopErrNone_c != PopStoreMacAddr(&(gsPopNwkData.aMacAddr)))
    return gPopErrFailed_c;

  // Check we have a valid image
  iImageCheck = PopCheckStoredImage();
  if( iImageCheck != gPopErrNone_c)
    return iImageCheck;

  // if preserve nvm is false erase it
  if(!(fPreserveNVM))
  {
    int32_t eraseResult;

    eraseResult = TiFlashMainPageErase(gNvSector0Address_c);
    if (eraseResult != 0)
    {
      return gPopErrNoMem_c;
    }
  }

  {
    uint8_t aSignature[4] = {0x00,0x00,0x49,0x46};

    // Write some zeros at the beginning of he storage image to indicate we are ready to use the new image
    (void)PopStorageWriteBytes(0, aSignature, sizeof(aSignature));
  }

  // Set timer to boot into the new image after sending the response message.
  (void)PopStartTimerEx(cBspTaskId,gPopOtaUpgradeResetTimer_c,1000);

  return gPopErrNone_c;
}
#endif //gPopUsingStorageArea_c && gPopOverTheAirUpgrades_d


/******************************************************************************************
*                                                                                         *
*                               ACCELEROMETER                                             *
*                                                                                         *
******************************************************************************************/

/*******************************************************************************
*  This function setup the GPIOs connected to the adc module
*******************************************************************************/
void AdcGpioInit(void)
{
  // ADC not used
}

/*******************************************************************************
*  This function setup the ADC
*******************************************************************************/
void ADC_Setup(void)
{
  // ADC not used
}
/* To config ATD module.*/
void SetupAccelerometer(void)
{
  // No accelerometer available
}

void PopAtdConfiguration(void)
{
  /* Setup the accel first */
  SetupAccelerometer();
}

/******************************************************************************************
*                                                                                         *
*                                     Watch Dog                                           *
*                                                                                         *
******************************************************************************************/
/*
  PopFeedTheDog

  Reset the COP to avoid restarting the whole system
*/
void PopFeedTheDog(void)
{
  // Reset the watchdog.
  WatchdogClear();
}

/*
  5.6 Computer Operating Properly (COP) or Watchdog Timer Module

  Important features:
     The watchdog is by default disabled exiting device reset, and must be enabled for use.
     Programmable time out period (between 87ms and 11sec, in 128 steps).
     The watchdog is disabled for ARM debug mode
     The COP time-out can cause a complete reset POR power-up sequence or interrupt
     Programmable response to COP time-out (interrupt or reset, default is interrupt)
       Cause a complete reset POR power-up sequence
       Cause interrupt request
     COP does not run in sleep modes

  (see Section 5.9.5, COP Control (COP_CNTL))
*/
void PopWatchDogInit(void)
{
#if gPopWatchDog_c
  /*
   *  WATCHDOG_INTERVAL_32768 gives about 1s timeout.
   */
  WatchdogEnable(WATCHDOG_INTERVAL_32768);
#endif
}

#if gPopLowPower_d
// COnfiguration Macros used to initialize the radio
uint8_t  gDigitalClock_RN = gDigitalClock_RN_c;
uint8_t  gDigitalClock_RAFC = gDigitalClock_RAFC_c;
uint8_t  gDigitalClock_PN = gDigitalClock_PN_c;
uint8_t  gDefaultCoarseTrim = DEFAULT_COARSE_TRIM;
uint8_t  gDefaultFineTrim = DEFAULT_FINE_TRIM;
uint8_t  gEnableComplementaryPAOutput = gEnableComplementaryPAOutput_c;
uint8_t  gPowerLevelLock = gPowerLevelLock_c;
uint8_t  gCcaThreshold = gCcaThreshold_c;
popNwkTxPower_t  gDefaultPowerLevel = gDefaultPowerLevel_c;
#endif

/******************************************************************************************
*                                                                                         *
*                            LPC1765 Upgrade Feature                                      *
*                                                                                         *
******************************************************************************************/
#if gPopMicroPowerLpcUpgrade_d

extern uint8_t ispCommanderModeFlag;
void MPEnterIspCommandHandler(void)
{
  // Set GPIO pins
  // Reset LPC (Set the pin high)
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, GPIO_PIN_5);
  // Hardware request for ISP command handler (Driving pin high)
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, GPIO_PIN_4);
  // keep values for some ms
  DelayMs(100);

  // Release Reset in LPC
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, 0);
  // Allow ISP command handler hardware request signal to be read by LPC bootloader
  DelayMs(100);
  // Release hardware req for ISp
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, 0);
  DelayMs(200);

  ispCommanderModeFlag=true;
}
void MPResetLPC(void)
{
  // Reset LPC (Set the pin high)
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, GPIO_PIN_5);
  // Hardware request for ISP command handler (Driving pin low)
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_4, 0);
  // keep values for some ms
  DelayMs(100);
  // Release Reset in LPC
  GPIOPinWrite(GPIO_A_BASE, GPIO_PIN_5, 0);
}
#endif
