/***************************************************************************
  PopPhy.c
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  PopPhy.c and PopPhy.h make up the PHY layer of PopNet. It's job is to send
  and receive packets over-the-air.

  This module supports the Freescale development boards, including the SARD,
  EVB, SRB and NCB,and the PopNet (Panasonic) boards, and uses Freescale
  SMAC for the low-level drivers.

  It also supports the TI CC2538.

  03/31/2006 dg  Created
****************************************************************************/
#include "PopPhy.h"

#include <Hw/TIPhy/TIPhy.h>
#include <Hw/TIDriverLib/source/interrupt.h>
#include <Hw/TIDriverLib/source/systick.h>
#include <Hw/TIDriverLib/source/gpio.h>
#include <Hw/TIDriverLib/inc/hw_memmap.h>
#include <Hw/TIDriverLib/source/ioc.h>
#include <board_config.h>
#include <PopCfg.h>
#include <stddef.h>

#if gUseMpSerial_d
#include <MP/MpSerial.h>
#endif

/***************************************************************************
  Local Types & Defines
***************************************************************************/
/* Timer to wait the transmition of the packet */
#define gTxPacketTimer_c gTmr1_c

/* Current message because there is only one slot in the queue, equals to HCS08 SMAC */
#define TRANSMIT_END() ((MSG_TX_ACTION_COMPLETE_SUCCESS == get_current_message()->u8Status.msg_state) || \
  (MSG_TX_ACTION_COMPLETE_FAIL == get_current_message()->u8Status.msg_state) || \
  (MSG_TX_ACTION_COMPLETE_CHANN_BUSY == get_current_message()->u8Status.msg_state) || \
  (MSG_TX_ABORTED == get_current_message()->u8Status.msg_state) || \
  (MSG_TX_RQST_ABORT == get_current_message()->u8Status.msg_state))

#define ED_END() ((MSG_ED_ACTION_COMPLETE_SUCCESS == Scan_msg.u8Status.msg_state) || \
  (MSG_ED_ACTION_COMPLETE_FAIL == Scan_msg.u8Status.msg_state) || \
  (MSG_ED_ABORTED == Scan_msg.u8Status.msg_state) || \
  (MSG_ED_RQST_ABORT == Scan_msg.u8Status.msg_state))



/***************************************************************************
  Local Prototypes
***************************************************************************/

void MCPSDataIndicationCallback(void);
void PopNwkReceive(void);
bool Timer_Setup(void);
bool TxPacketTmr_Setup(void);
void Tmr_Isr(void);


popStatus_t MCPSDataIndication(phyDataInd_t *pRxPacket);


/***************************************************************************
  Globals and Externs
***************************************************************************/

extern const popTaskId_t gPopPhyIndicationTask;   // where do PHY indications go (NWK layer or APP?)

phyDataReq_t txPacket;
phyDataInd_t rxPacket;

/* Flag to knows when and indication msg is incoming */
bool gbDataIndicationFlag;
volatile popTimeOut32_t gMillisecondsElapsed;

// what channel are we physically on?
popChannel_t giPhyChannel;

static uint8_t lastLqi;
static phyConfig_t phyConfig;

#if gPopAppSniffer_d
extern index_t giPopSnifferHead;
extern index_t giPopSnifferTail;
extern index_t giPopSnifferNumUsed;
extern bool gfPopSnifferEnabled;
extern uint8_t giPopSnifferBuffers;
extern sPopSnifferBuffer_t gaPopSnifferBuffer[];
#endif
/***************************************************************************
  Code
***************************************************************************/
/*
  PopPhyTaskInit

  Initialize the PHY. This is called only once when PopNet starts up.
*/
void PopPhyTaskInit(void)
{
  IntMasterDisable();

  phyConfig.channel = PopNwkGetChannel();
  phyConfig.myAddr = 0;
  phyConfig.pDataIndCb = &MCPSDataIndicationCallback;
  phyInit(&phyConfig);

  IntMasterEnable();

  // Adjust the power of the transmiter.
  {
    popErr_t iErr;
    uint8_t iTxPowerLevel;

    /* Retrieve power level from NVM */
    iErr = PopNvRetrieve(gPopAppNvTxPowerLevel_c, &iTxPowerLevel);
    if(iErr != gPopErrNone_c){
      /* Set and store default power level to NVM*/
      iTxPowerLevel = gPopNwkTxPower_Normal_c;
      //PopNvStore(gPopAppNvTxPowerLevel_c, 1, &iTxPowerLevel);
    }
    PopNwkSetTransmitPower(iTxPowerLevel);
  }

  /* Configure and start the Tmr */
  Timer_Setup();
}

/*
  PopPhyTaskEventLoop

  Event loop for the PHY task. The PHY issues no events for itself by default, but this is available
  to the PHY for porting purposes.
*/
void PopPhyTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
  /* Firstly, reset the COP because we don't know if some task will take more than COP timeout configured */
  PopFeedTheDog();

  //avoid compiler warnings
  (void)iEvtId;
  (void)sEvtData.pData;
}


/*
  PopPhyReceive

  Puts node into receive mode. Expects the frame is large enough to recieve the
  largest possible packet.

  This is used after a data request (assuming the radio is half duplex) and when
  waking up the PHY on sleeping systems.
*/
void PopPhyReceive(sPopNwkDataIndication_t *pFrame)
{

  // if no frame, assert
  if(!pFrame)
    PopAssert(gPopAssertBadReceivePtr_c);

  /*
    Set the indication flag to false. This indication flag is used by the SMAC to notify
    that MACA interruption has an indication packet.
  */
  gbDataIndicationFlag = false;

  rxPacket.data.psduLen = giPopPhySize;
  rxPacket.data.pPsdu = (uint8_t *) &pFrame->sFrame;

  phyPDReceive(&rxPacket);
}

/*
  PopPhyDataRequest

  Send (transmit) data out the radio. PopNet assumes it cannot fail. If it does fail (HW
  not working, etc...) that's OK. The higher protocol will realize that an ACK was not
  delivered an inform the application accordingly.
*/
void PopPhyDataRequest(void *pData, uint8_t iLen)
{
  /* If the pData is null then return without notify nothing*/
  if( !pData )
    return;

  // set up the tx packet
  txPacket.data.psduLen = iLen;

  /* Copy the info to the new packet */
  txPacket.data.pPsdu = pData;

  // send it out the radio
  phyPDRequest(&txPacket);
}

/*
  DetectEnergy:

  MAC Spec 802154MPSRM Rev. 2.1 05/2009
  4.2.2Energy Detection Scan
  The level for Energy Detection is reported as required by the 802.15.4
  Standard with an integer value from 0x00-0xFF. The hardware measured
  values are scaled and normalized for this range with the minimum value
  of 0x00 set to -100dBm and the maximum value of 0xFF set to -15 dBm.
  Measured values between -15dBm and -100dBm are scaled linearly between 0x00 and 0xFF.

  MC1322x family:
  �The energy level is mathematically derived from several reported hardware values.
  The values range from 0x00 (-100dBm) to 0xFF (-15dBm) as required.

  iChannelNum is 0-15.
*/
popEnergyLevel_t DetectEnergy(uint8_t iChannelNum)
{
  popEnergyLevel_t energy;

  /*
   *  IEEE 802.15.4:2011 8.2.5 Receiver ED
   *
   *  The minimum ED value (zero) shall indicate received power less than 10 dB
   *  above the maximum allowed receiver sensitivity for the PHY. The range of
   *  received power spanned by the ED values shall be at least 40 dB. Within
   *  this range, the mapping from the received power in decibels to ED value
   *  shall be linear with an accuracy of � 6 dB.
   */

  phyPLMESetChannel(iChannelNum);
  phyPLMEEnergyDetectStart();

  /*
   *  Wait a while to get longer scanning time?
   *
   *  The standard only requires that the scan lasts for 8 symbol periods. The
   *  phyPLMEEnergyDetectStop() takes care of that.
   */
  DelayMs(10);

  energy = phyPLMEEnergyDetectStop();

  return energy;
}

/*
  PopPhyScanForNoise.

  Check the given channel for noise. Returns an energy level for the channel.
  Channel is 11-26.
*/
popEnergyLevel_t PopPhyScanForNoise(uint8_t iChannel)
{
  // return detected energy on given channel. convert from 11-26 channel, to 0-15 ChannleNum
  return DetectEnergy(iChannel);
}

/*
  PopPhyLqi

  Returns link quality (a number betweeen 1-250) of the packet most recently received. Used for
  both signal strength of beacon indications and data packets.

  This values are for ARM boards:
  weak to poor signal <= 0x50, medium signal 0x51-0x90, strong signal >0x90.
*/
uint8_t PopPhyLqi(void)
{
  uint8_t iLqi = 0;

  // Random LQI is used for testing PopNet routing
#if gPopRandomLqi_d
  iLqi = PopRandom08();
  if(iLqi < 1)
    return 1;
  else if(iLqi > 250)
    return 250;
  return iLqi;
#endif

  /* Get the LQI from the MAC */
  iLqi = lastLqi;
  if(iLqi == gPopNwkLqiFromThisNode_c)
    --iLqi;

  /* return the LQI value */
  return iLqi;
}

/*
  Set the channel. Cannot fail.

  Note: PopNet numbers channels like 802.15.4, (11-26 for 2.4GHz space).
*/
void PopPhySetChannel(uint8_t iChannel)
{
  giPhyChannel = iChannel;
  phyPLMESetChannel(iChannel);
}


/***************************************************************************

    THE REST OF THE CODE IS SPECIFIC TO THE PHY PORT of PopPhy.c

*****************************************************************************/
/*
  PopPhyProcessRadioMsg

  ARM SMAC requires to check this functions everytime inside of the main loop.
*/
void PopPhyProcessRadioMsg(void)
{
  // Verify if there is a data indication ready to send to NHL
  data_indication_execute();

#if gPopSerial_d
  // indicate byte received to appropriate task
  if( gfPopSerialStatus & gPopSerRxData_c)
  {
    // only set 1 event in serial task
    (void)PopSetUniqueEventEx(cSerialTaskId, gPopSerByteReceivedEvt_c, NULL);
    gfPopSerialStatus &= ~gPopSerRxData_c;
  }
#endif
#if gUseMpSerial_d
  // MpSerial indicate byte received to appropriate task
  if( gfMpSerialStatus & gMpSerRxData_c)
  {
    // only set 1 event in serial task
    (void)PopSetUniqueEventEx(cMpSerialTaskId, gMpSerByteReceivedEvt_c, NULL);
    gfMpSerialStatus &= ~gMpSerRxData_c;
  }
#endif

}

/*
  MCPSDataIndicationCallback

  Called by the PHY driver from an ISR.
 */

void MCPSDataIndicationCallback(void)
{
  gbDataIndicationFlag = true;
}
/*
  MCPSDataIndication

  Inside the interrupt handler: got data
*/
popStatus_t MCPSDataIndication(phyDataInd_t *pRxPacket)
{
  sPopNwkDataIndication_t *pFrame;
  bool gPopBackToListen = false;

  /* Reset the COP (if needed) */
  PopFeedTheDog();

#if gPopAppSniffer_d
	/* Set the ms time */
	if(gfPopSnifferEnabled)
	{
		gaPopSnifferBuffer[giPopSnifferHead].iTimeMs = PopGetCurrentTimeMs();
	}
#endif

  lastLqi = pRxPacket->lqi;

  // if it worked, tell the network layer so it can route
  // the frame, in addition to OTA, includes a few bytes info (e.g. length)
  pFrame = (void *)((uint8_t*)pRxPacket->data.pPsdu - sizeof(sPopNwkPreHeader_t));

  /* Set the real size of the data received */
  pFrame->sPre.iFrameLen = pRxPacket->data.psduLen;
  pFrame->sPre.iLqi = pRxPacket->lqi;

  /*
    Tell network layer we have a packet (so NWK can process it). What happen if there is no
    events? then the nwk layer will never receive the notification (gPopEvtPhyIndication_c)
    ,worse yet, it won't put the radio back into receive mode after the indication and the
    indication buffer won't be free as well.

    So, we need to free the Message queue entry and put the radio back into receive mode
    reusing the buffer.
  */
  if( gPopErrNone_c != PopSetEventEx(gPopPhyIndicationTask, gPopEvtPhyIndication_c, pFrame) )
  {
    gPopBackToListen = true;
  }

  // Back to listen and go out
  if( gPopBackToListen )
  {
    // Radio back to listen
    PopPhyBackToListen();

    // Notify that radio was put back to liste. In deed, the inidcation event was not set.
    return gPopPhyRadioBackToListen_c;
  }

  // Everything works fine.
  return gPopPhyIndicationSuccess_c;
}

void data_indication_execute(void)
{
  if(true == gbDataIndicationFlag)
  {
    gbDataIndicationFlag = false;

    /* Generate the Mcps-Data.Indication and tells to nwk layer. Network Layer enables the Rx. */
    if( gPopPhyIndicationSuccess_c == MCPSDataIndication(phyPDGetData()) )
    {
#if gPopAppSniffer_d
			/* Immediately receive another packet */
			PopPhyAppSnifferReceive();
#endif
    }
  }
}

void PopPhyBackToListen(void)
{
  /* Clear the data indication flag */
  gbDataIndicationFlag = false;

#if gPopAppSniffer_d
	if(gfPopSnifferEnabled)
	{
		/* Immediately receive another packet with the same buffer */
		PopPhyReceive((sPopNwkDataIndication_t *)(gaPopSnifferBuffer[giPopSnifferHead].aPhyBuffer));
  }
#else
  /* Back to listen. Useful when the rx had an error */
  PopNwkReceive();
#endif
}

/*******************************************************************************
*  This function setup the TMR Module
*******************************************************************************/
bool Timer_Setup(void)
{
  /*
   *  Configuring SysTick timer to generate an interrupt each millisecond.
   */

  SysTickIntRegister(Tmr_Isr);
  SysTickIntEnable();
  SysTickPeriodSet(PLATFORM_CLOCK/1000);
  SysTickEnable();

  return true;
}

/*******************************************************************************
*  This function setup the TMR Module
*******************************************************************************/
bool TxPacketTmr_Setup(void)
{
  // The MAC timer is handled completely by the MAC/PHY driver

  return true;
}

/*******************************************************************************
*  This is the Timer1 interrupt.
*******************************************************************************/
void Tmr_Isr(void)
{
  gMillisecondsElapsed++;
}

/**************************************************************
*	Function: 	Read MCU timer; put two bytes of timer into UINT16
*	Parameters: none
*	Return:		16-bit timer value.
**************************************************************/
uint32_t GetMcuTimer1(void)
{
  popTimeOut32_t temp;

  IntMasterDisable();
  temp = gMillisecondsElapsed;
  IntMasterEnable();

  return temp;
}

/**
 * Get the current systick count in ms
 */
uint32_t PopGetSystick(void)
{
  return GetMcuTimer1();
}

/*
  PopNwkSetTransmitPower

  Adjust the Output power of the transmitter

  The Over flow range is 0x12, so any value must be below 0x12 is acceptable. Internally
  the value to obtain the maximum power is 0x12 (18 decimal).

  so if...
     iTxPower > 0x12 is over flow.

     if...
     0 >= iTxPower < 0x12 is a valid range of values.

  therefore..
    gPopNwkTxPower_Maximum_c = MAX_POWER (0x12)
    gPopNwkTxPower_Normal_c  = 0x0C
    gPopNwkTxPower_Medium_c  = 0x09
    gPopNwkTxPower_Mininum_c = 0x00

  See RF_Config.h for the power values definition.
*/
void PopNwkSetTransmitPower(popNwkTxPower_t iTxPower)
{
//  sPopEvtData_t sEvtData;
  int8_t power_dBm;
  uint8_t powerWasSet;

  // set the values needed by the PHY driver
  switch (iTxPower)
  {
    case gPopNwkTxPower_Maximum_c:
      power_dBm = 22;
      break;

    case gPopNwkTxPower_Normal_c:
      power_dBm = 13;
      break;

    case gPopNwkTxPower_Medium_c:
      power_dBm = 7;
      break;

    case gPopNwkTxPower_Mininum_c:
      power_dBm = 5;
      break;

    default:
      power_dBm = 0;
      break;
  }

  // Set the given power..
  powerWasSet = phyPLMESetTxPower(power_dBm);

  // Kaibab needs to wait a little bit so the power get set correctly
  DelayMs(100);

  // Change from PHY error code to PopNet error code.
  if (powerWasSet != true)
  {
    //sEvtData.iStatus = gPopErrPowerOutOfRange_c;
  }

  // Report the results all the way up to the application.
  //#JJ removed, not used, but could enable interrupt during init
  //(void)PopSetAppEvent(gPopEvtNwkTxPowerConfirm_c, sEvtData.pData);
}

#if gPopAppSniffer_d
void PopPhyAppSnifferReceive(void)
{
	if(gfPopSnifferEnabled)
	{
		// advance head
		++giPopSnifferHead;
		if(giPopSnifferHead >= giPopSnifferBuffers)
		  giPopSnifferHead = 0;

		// if we have another buffer, use it to receive next OTA packet
		// (otherwise, we'll wait until the current buffer is out the serial port
		++giPopSnifferNumUsed;
		if(giPopSnifferNumUsed < giPopSnifferBuffers)
	    PopPhyReceive((sPopNwkDataIndication_t *)(gaPopSnifferBuffer[giPopSnifferHead].aPhyBuffer));
	}
}
#endif

