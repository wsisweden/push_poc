/***************************************************************************
  PopBsp.h
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is released under Non-Disclosure
  and is not a public work. This notice is not to be removed.

  The Board Support Package (BSP) is the interface between PopNet and the physical board or OS.
  The BSP includes the following interfaces:

  1. MCU interface
  2. Random # interface
  3. Timer interface
  4. Serial I/O interface
  5. LEDs interface
  6. LCD interface
  7. Keyboard interface
  8. Sound interface
  9. Low Power interface
  10. Flash (or other Non-volatile) storage interface
  11. Reset control interface
  12. Security interface

****************************************************************************/
#ifndef _POPBSP_H_
#define _POPBSP_H_

#include "board_config.h"
#include <Hw/TIDriverLib/source/sys_ctrl.h>
#include <stdint.h>
#include <stdbool.h>

#include "Utilities/UartUtil.h"

/*
  Define the endian
  ARM   -> FALSE
  HCS08 -> TRUE
*/
#define gPopBigEndian_c     false
#define gPopNeedPack_c      true


/************* MCU Interface ****************/

/* universal limits */
#define gPopInt8Max_c    0x7F
#define gPopUint8Max_c   0xFF
#define gPopInt16Max_c   0x7FFF
#define gPopUint16Max_c  0xFFFFU
#define gPopInt32Max_c   0x7FFFFFFFL
#define gPopUint32Max_c  0xFFFFFFFFUL

/* for use with uint8_t */
#define gPopEndOfIndex_c  0xff

// MC13224 is little endian as configured for PopNet.

// OTA macros need no conversion: they are little endian already
#define NativeToOta16(var)
#define OtaToNative16(var)
#define NativeToOta32(var)
#define OtaToNative32(var)

// Gateway macros need conversion: the must become big endian
#define GatewayToNative16(pVar)  SwapBytes16(pVar)
#define NativeToGateway16(pVar)  SwapBytes16(pVar)
#define GatewayToNative32(pVar)  SwapBytes32(pVar)
#define NativeToGateway32(pVar)  SwapBytes32(pVar)

extern void PopGwSwapICanHearYouToLittle(uint8_t iNum, uint16_t *pArray);
extern uint16_t SwapBytes16WithReturn(uint8_t *pWord);
extern void SwapBytes16(uint16_t *pWord);
extern void SwapBytes32(uint32_t *pWord32);
extern void SwapBytes64(uint64_t *pWord64);

// see gPopMemMaxAlloc_c for maximum size
typedef uint8_t popMemSize_t; // naturual size for memory allocation


// turn off LCD if not NCB
#if(TARGET_BOARD != MC1322XNCB)
#undef gPopLcdEnabled_d
#define gPopLcdEnabled_d gDisabled_c
#endif

// turn off Sound if not NCB or SRB
#if ( (TARGET_BOARD != MC1322XNCB ) && (TARGET_BOARD != MC1322XSRB) )
#undef gPopSoundEnabled_d
#define gPopSoundEnabled_d gDisabled_c
#endif

#if TARGET_BOARD == MC1322XSRB
#define gPopTargetDefined_d  10   // prevents multiple targets
#define gPopNumLeds_c 4
#define gPopNumKeys_c 4
#endif

#if TARGET_BOARD == MC1322XNCB
#define gPopTargetDefined_d  11   // prevents multiple targets
#define gPopNumLeds_c 4
#define gPopNumKeys_c 4
#define gPopLcdCols_c 21 // Max columns for Kaibab NCB board
#define gPopLcdRows_c 8  // Max rows for Kaibab NCB board
#endif

/* Low Power board */
#if TARGET_BOARD == MC1322XLPB
#define gPopTargetDefined_d  12   // prevents multiple targets
#define gPopNumLeds_c 2
#define gPopNumKeys_c 1  // Sw1 and SW2 (= center key)
#endif

/* CEL Free Star Pro board  */
#if TARGET_BOARD == CELFreeStarPro
#define gPopTargetDefined_d  13   // prevents multiple targets
#define gPopNumLeds_c 4
#define gPopNumKeys_c 4
#endif

#if TARGET_BOARD == MC1322XUSB
#define gPopTargetDefined_d  14   // prevents multiple targets
#define gPopNumLeds_c 1
#define gPopNumKeys_c 1 // This target board does not has switches just 2 leds and 1 reset switch
#endif

// normally defined above by the target, but if not defined, use the following defaults...
#ifndef gPopNumLeds_c
 #define gPopNumLeds_c 1
#endif
#ifndef gPopNumKeys_c
 #define gPopNumKeys_c 1
#endif
#ifndef gPopLcdCols_c
 #define gPopLcdCols_c 1
#endif
#ifndef gPopLcdRows_c
 #define gPopLcdRows_c 1
#endif

/************* Miscellaneous Board Support ****************/

// Voltage reference for ARM7 used with low voltage detection feature
#define gPopVoltageRisingValue_c 2.1

// reports board capabilities
typedef uint16_t popBspCaps_t;

#define gPopCapsKeyboard_c  0x0001  // is some form of key or switch input
#define gPopCapsLeds_c      0x0002  // at least 1 LED
#define gPopCapsLcd_c       0x0004  // an LCD
#define gPopCapsUart_c      0x0008  // at least 1 UART
#define gPopCapsSound_c     0x0010  // some form of speaker, even just a beep
popBspCaps_t PopBspCapabilities(void);


/* Random Number Generator */
void PopRandomInit(void);
uint8_t PopRandom08(void);
uint16_t PopRandom16(void);

/***************************************************************************
  Transmitter types and functions
***************************************************************************/
#define gPopMaxFrame_c  (gPopPhySize_c - 2)

typedef uint8_t popNwkTxPower_t;
#define gPopNwkTxPower_Mininum_c  1  // this is very quiet (used for pairing)
#define gPopNwkTxPower_Medium_c   2  // short range
#define gPopNwkTxPower_Normal_c   3  // normal range
#define gPopNwkTxPower_Maximum_c  4  // this turns on the PA, maximum range

extern popNwkTxPower_t  gDefaultPowerLevel;
extern const uint8_t giPopPhySize;

void PopNwkSetTransmitPower(popNwkTxPower_t iTxPower);

extern void PopPhyProcessRadioMsg(void);
/***************************************************************************
  System halt and reset functions
***************************************************************************/

/*
  Fatal error, halt the system
*/
void PopHalt(void);

/*
  System reset.
*/
void PopReset(void);

// TODO: Add PopEnterDebug() to ARM7 port
#define PopEnterDebug() PopHalt()

/* Varibale to store what interruptions were disabled */
extern unsigned int saveInt;

/* for enabling/disabling interrupts */
void PopDisableInts(void);
void PopEnableInts(void);
/************* Keyboard Interface ****************/

// keys are delivered one at a time
typedef uint8_t popKeyId_t;
enum {
  gPopNoSw_c = 0,
  gPopSw1_c = 1,
  gPopSw2_c,
  gPopSw3_c,
  gPopSw4_c,
  gPopLongSw1_c = 0x41,   /* press and hold Sw 1 */
  gPopLongSw2_c,   /* press and hold Sw 2 */
  gPopLongSw3_c,   /* press and hold Sw 3 */
  gPopLongSw4_c    /* press and hold Sw 4 */
};

#if gPopKeyboardEnabled_d

/* KeyBoard callback */
typedef void (*pfKBCallback_t)(void);

/* Number of keys that this board have */
extern const int_t cPopNumKeys;

/* Key Control Flags (Switch and long switch bit are consideraded as enabled) */
#define gPopSwitch1Flag_c 0x11
#define gPopSwitch2Flag_c 0x22
#define gPopSwitch3Flag_c 0x44
#define gPopSwitch4Flag_c 0x88

/* Add comment */
#define gPopKbiMask (gExtWuKBI4En_c | gExtWuKBI5En_c | gExtWuKBI6En_c | gExtWuKBI7En_c)

/* Add comment */
#define gPopKBIPol_c 0x00

/* Add comment */
#define gPopKBIEdge_c 0x0f

/* Add comment */
#define gPopMaxKBI_c 0x0f

/* Add comment */
#define PopNumKeys() cPopNumKeys

/* Add comment */
popKeyId_t PopGetKey(void);

/* Add comment */
bool IsStillKeyPressed(uint8_t keyMask );

#else
#define PopNumKeys()
#define popKeyId_t()
#endif

/************* Timer Interface ****************/

// timer types
typedef uint8_t   popTimerId_t;   // ID for timer
typedef uint16_t  popTimeOut_t;   // type of a single timer
typedef uint32_t  popTimeOut32_t;   // type of a single timer. - Used for ARM7 SMAC
#define gPopMaxTimeout_c  0xfff0    // maximum timeout

// Timer IDs are unique per task. Each task must manage its own IDs.
// Timer IDs may be 1-255.
#define gPopFirstTimerId_c  1   // timer IDs must start with 1. Timer IDs are unique per task.
#define gPopLastTimerId_c   255 // final timer ID
#define gPopTimerFree_c     0   // 0 means timer is free

// determine # of timers in the system.
#define PopNumTimers()  cPopMaxTimers


/************* Led Interface ****************/


// possible iLedState for PopSetLed()
typedef uint8_t popLedState_t;
#define gPopLedOff_c      (1 << 0)  // solid off LED
#define gPopLedOn_c       (1 << 1)  // solid on LED
#define gPopLedToggle_c   (1 << 2)  // toggle

// combine to make possible iLedMask. To determine # of LEDs, use PopNumLeds()
// only those LEDs that are ussported are set, toggled, etc... with gPopLedAll_c.
typedef uint8_t popLedMask_t;
#define gPopLed1_c    (1 << 0)
#define gPopLed2_c    (1 << 1)
#define gPopLed3_c    (1 << 2)
#define gPopLed4_c    (1 << 3)
#define gPopLed5_c    (1 << 4)
#define gPopLed6_c    (1 << 5)
#define gPopLed7_c    (1 << 6)
#define gPopLed8_c    (1 << 7)
#define gPopLedAll_c  (0xff)

#if gPopLedEnabled_d

// set LEDs
void PopSetLed(popLedMask_t iLedMask, popLedState_t iLedState);   // iLed is a bitmask of LEDs

/* display a hex # on LEDs */
void PopLedDisplayHex(uint8_t iHexValue);

extern const int_t cPopNumLeds;
#define PopNumLeds() cPopNumLeds
#else

// stubbed if disabled
#define PopNumLeds()
#define PopSetLed(iLedMask, iLedState)
#define PopLedDisplayHex(iHexValue)
#endif

typedef struct _tagsPopLedStatus
{
  uint8_t    iLedMask;
  uint8_t    iLedState;
}sPopLedStatus_t;

// control an extern I/O pin (e.g. for On/Off light). Used by PopAppOnOffLight.c
#define ExternalDeviceInit()
#define ExternalDeviceOn()
#define ExternalDeviceOff()
#define ExternalDeviceToggle()

/************* Display Interface ****************/

#if gPopLcdEnabled_d
extern const int_t cPopLcdRows;
extern const int_t cPopLcdCols;
#define PopLcdRows() cPopLcdRows
#define PopLcdCols() cPopLcdCols

#else

// stubbed if disabled
#define PopLcdRows()
#define PopLcdCols()
#endif


/************* Sound Interface ****************/

#if gPopSoundEnabled_d

// make a popping sound (or beep if speaker not capable of a pop)
void PopPop(void);

// turn on or off the buzzer
void PopSoundOn(void);
#else
#define PopPop()
#define PopSoundOn()
#endif

/************* Low Power Interface ***************/
typedef uint8_t popWakeUpReason_t;
// indicates the reason why the board came wakeup from its sleep.

enum{
  gPopKeyboardPress_c = 0, // was a key pressed
  gPopTimesUp_c,          // the sleep time expire
  gPopLowVotageDetected_c // the node detected a low voltage not enough energy to Tx , time to change bateries.
  };

/************* Security Interface ****************/

// San Juan SW AES engine
void PopNwkAES(uint8_t *pIn, /* uint8_t *pKey,*/ uint8_t *pOut);
void Expand128Key(uint8_t *pKey);

/************* Low-level Flash Memory Routines ****************/

// size of erasable page in flash memory (On CC2538 2KiB is the smallest erasable block)
# define gPopNvPageSize_c    2048

typedef uint16_t popNvPageSize_t;   // a variable large enough to hold the size of a page (512 bytes)
typedef uint8_t popNvmPage_t;       // to hold a page number
typedef uint32_t popNvmOffset_t;
typedef uint32_t popNvmSize_t;
//#define gaStorage ((uint8_t*));
#define gBootStart_c    0x0000
#if gPopMicroPowerExternalFlash_d
// This should be defined using a macro from FLASH.h
#define gStorageStart_c 0x00000000
#else
#define gStorageStart_c 0xE000
#endif
#define gWaterMark_c    (gBootStart_c + 4096)

#define StorageMacAddress_c 0x0027E000

void PopArm7PowerUpNVM(void);
void PopArm7PowerDownNVM(void);
bool PopFlashReadBytes(void * pDstAddr, void * pSrcAddr, uint8_t iLen);

// BASIC NVM ROUTINES
void PopNvmInit(void);
void PopNvmEraseAll(void);
void PopNvmErasePage(popNvmPage_t iPage);
void PopNvmInit(void);
bool PopFlashEraseNvmFlash( void *pAddress );
popNvPageSize_t PopNvCountFFs(uint8_t * pBuffer, popNvPageSize_t iMax);
bool PopFlashCopyNvPage(void * pSrcAddress, void * pDstAddress);
bool PopFlashBackUpPagesInSpareSector(void);
void PopNvmErasePage(popNvmPage_t iPage);
// note: returns a popErr_t error code
uint8_t PopNvmReadBytes(void *pToAddr, popNvmOffset_t iFromOffset, popNvmSize_t iLen);
// note: returns a popErr_t error code
uint8_t PopNvmWriteBytes(popNvmOffset_t iToOffset, void *pFromAddr, popNvmSize_t iLen);

#if gPopNumberOfNvPages_c

#ifndef gPopNvUnitTest_d
/* IAR platform */
/*
 * Use the upper sectors of the CC2538 MCU internal flash. The last sector
 * is reserved for lock bits (See CC2538 user manual).
 */
# define gaPopNvPages ((uint8_t*)0x0027E800);
#else
extern uint8_t gaPopNvPages[];
#endif

/* sector addresses */
#define gNvSector0Address_c 0x0027E800
#define gNvSector1Address_c 0x0027F000


/* Sector to use */
#define gNvNumberOfRawSectors_c 2

/* Sector indexes */
#define gNvSector_c 0
#define gNvSpareSector 1
/*
  Sector ctrl:
  Nvm data will be written in the sector 0x1D000 and the sector 0x1E000 will be the spare sector.
*/
extern uint32_t const maNvRawSectorAddressTable[gNvNumberOfRawSectors_c];
extern uint8_t gNvEntryLength;
extern bool gfPopNvNoFlashInit;
extern bool gfPopGarbageNoHeader;
extern bool gfPopGarbageNoErase;

// prototype

void *PopNvGetPage(uint8_t iPage);
#define PopNvmNumPages() giPopNvNumPages
#define PopNvmPageSize() giPopNvPageSize
#define PopNvmTotalSize() (PopNvmNumPages()*giPopNvPageSize)
#endif /* #if gPopNumberOfNvPages_c... */

#ifndef gPopNvUnitTest_d
void PopFlashInit(void);
#endif
bool PopFlashWriteBytes(void *pDstAddr, void *pSrcAddr, uint32_t iLen);
void * PopNvGetPageHeader(void *pPageSrcAddress);
void * PopNvGetEntryData(void *pEntrySrcAddress, uint8_t iNvEntrySise);

// defined in PopCfg.c
extern const uint8_t * const gpaPopNvFirstFlashPage;
extern const uint8_t giPopNvNumPages;
extern const popNvPageSize_t giPopNvPageSize;

// Low-level, raw flash routines.

/************* Serial Interface ****************/

#define gSTX_c            0x02
#define gPopNetOpGroup_c  0x50


/************* Miscellaneous ****************/

// globals to prevent compiler warnings
extern uint8_t giPopForever; // to prevent compiler warnings on forever loops
extern void DelayMs(uint32_t ms);
extern void SetComplementaryPAState(bool);
extern uint8_t SetPowerLevelLockMode(bool);
/***************************************************************************
  SERIAL INTERFACE
***************************************************************************/

#if gPopSerial_d

/***************************************************************************
  Types & Definitions
***************************************************************************/

/*
  It will default to 0 (smart), which will use the proper serial port for all platforms:
    UART (SCI) 1 boards:  SARD, ARM7, CEL, XBee
    UART (SCI) 2 boards:  NCB, SRB, EVB and QE
*/

// If the user has selected the smart option then undefine the actual value to set the right one. Otherwise,
// use the serial port number selected by the user.
#if !gPopSerialPort_c

#undef gPopSerialPort_c

#if ( TARGET_BOARD == MC1319XSARD || TARGET_BOARD == MC1319X_XBEE || TARGET_BOARD == MC1322XLPB || TARGET_BOARD == MC1322XSRB || TARGET_BOARD == MC1322XNCB ||  TARGET_BOARD == MC1322XUSB || TARGET_BOARD == CELFreeStarPro)
  #define gPopSerialPort_c 1
#else
  #define gPopSerialPort_c 2
#endif

#endif

/*New events that are need it for test tool*/

// Timers
#define gPopSerMinGapTimer_c                       0x0011     /* minimum inter-byte gap event */

#define POPSER_RX_BUFFER_WATERMARK 4

/* helpers for manipulating interrupts */
/* the following for example only; race condition caution. */
#define PopSerialDisableTxInts()
#define PopSerialEnableTxInts()
#define PopSerialDisableRxInts()
#define PopSerialEnableRxInts()
#define PopSerialDisableTxRxInts()
#define PopSerialEnableTxRxInts()
#define PopSerialSaveAndDisableTxRxInts()
#define PopSerialRestoreTxRxInts()
#define PopSerialSaveAndDisableTxInts()
#define PopSerialRestoreTxInts()

// internal events for serial module
#define gPopSerByteReceivedEvt_c     0x0010     /* ISR received a byte */

// status flags
#define gPopSerRxData_c         0x01
#define gPopSerRxOverflow_c     0x02
#define gPopSerTxDone_c         0x04
#define gPopSerTxOverflow_c     0x08
#define gPopSerTooBig_c         0x10  //application buffer size exceeds maximum tx buffer size.
#define gPopSerBusy_c            0x12  //no room in xmit buffer for data.
#define gPopSerInvalidArg_c     0x14  //check parameters

// set baud rate divisor based on 16MHz bus rate (PopNet runs at 16MHz)
#if gPopSerialBaudRate_c == 1200
#define POPSER_DIVISOR Baudrate_1200
#elif gPopSerialBaudRate_c == 2400
#define POPSER_DIVISOR Baudrate_2400
#elif gPopSerialBaudRate_c == 4800
#define POPSER_DIVISOR Baudrate_4800
#elif gPopSerialBaudRate_c == 19200
#define POPSER_DIVISOR Baudrate_19200
#elif gPopSerialBaudRate_c == 57600
#define POPSER_DIVISOR Baudrate_57600
#elif gPopSerialBaudRate_c == 115
#define POPSER_DIVISOR Baudrate_115200
#else
#define POPSER_DIVISOR Baudrate_38400
#endif


extern UartErr_t UartWriteData( uint8_t UartNumber, uint8_t* pBuf, uint16_t NumberBytes);

void PopSerialTxIsr(UartWriteCallbackArgs_t* args);
void PopSerialRxIsr(UartReadCallbackArgs_t* pArgs);
void PopConvertEventFromNativeToGatewayEndian(uint8_t iEvtId, void *pBuffer);

extern uint8_t  gaPopSerialTxBuf[gPopSerialTxBufferSize_c]; /* ISR tx buf */
extern uint8_t  *gpPopSerialTxBufHead;    /* buffer head */
extern uint8_t  *gpPopSerialTxBufTail;    /* buffer tail */
extern uint8_t   gaPopSerialRxBuf[gPopSerialRxBufferSize_c]; /* ISR rx buf */
extern uint8_t  *gpPopSerialRxBufHead;    /* buffer head */
extern uint8_t  *gpPopSerialRxBufTail;    /* buffer tail */
extern uint16_t  giPopSerialMinGapMs; /* mingap timeout, in ms. can be set by app */
extern volatile uint8_t gfPopSerialStatus;     /* current status of xmit and recv */
extern UartReadStatus_t gu8SCIStatus;
extern volatile uint8_t gu8SCIDataFlag;
extern uint16_t gu16SCINumOfBytes;
#endif

/******************************************************************************************
*                                                                                         *
*                               ACCELEROMETER                                             *
*                                                                                         *
******************************************************************************************/
#define ACCEL_GSLEEP     (gGpioPin8_c)
#define ACCEL_GSELECT1   (gGpioPin9_c)
#define ACCEL_GSELECT2   (gGpioPin10_c)
#define COMPENSATED_VALUE 128

 typedef struct {
   uint8_t NxOff;
   uint8_t NxMax;
   uint8_t NyOff;
   uint8_t NyMax;
   uint8_t NzOff;
   uint8_t NzMax;
   uint8_t FlashCounter;
} tAccelCal;

extern void PopAppHandleKeys(popKeyId_t iKeyId);
void PopAtdConfiguration( void );
void PopReadxy( void );

/******************************************************************************************
*                                                                                         *
*                                     Watch Dog                                           *
*                                                                                         *
******************************************************************************************/
/*
  gPopWatchDog_c

  This options allow to decide if the Watchdog should be enable or disabled, by setting
  this option to gEnable_c will let the watchdog active, setting this option to gDisable_c
  will shutdown the watchdog enable which is the default status.

  Default: gDisabled_c
*/
#ifndef gPopWatchDog_c
#define gPopWatchDog_c  gDisabled_c
#endif

/*
  PopFeedTheDog

  This function reset the COP Service to avoid a hardware reset or a triggered
  interruption (depending of the COP configuration).
*/
void PopFeedTheDog(void);

void PopWatchDogInit(void);

/******************************************************************************************
*                                                                                         *
*                            CEL FreeStar Pro STUFF                                       *
*                                                                                         *
******************************************************************************************/
#if TARGET_BOARD == CELFreeStarPro
void PAVoltageRegOn (void);
void PAVoltageRegOff(void);
#else
#define PAVoltageRegOn()
#define PAVoltageRegOff()
#endif
/************* Miscellaneous ****************/

// globals to prevent compiler warnings
extern uint8_t giPopForever; // to prevent compiler warnings on forever loops
extern uint8_t giPopTotalTimers;


/*
  Storage Routines, enabled with gPopUsingStorageArea_c

  The storage area, like NVM, is a way to permanently store data. Unlike the NVM routines,
  storage routines are very low level. (block write, erase, etc... no garbage collection
  or item IDs).

  If gPopOverTheAirUpgrades_d is enabled, this area is in use by OTA upgrades, and is not
  available to the application.

  Storage may be in flash memory, or me be even off-chip over a SPI port to SRAM. Storage
  blocks must be "erased" before they can be written to.
*/
#if gPopMicroPowerExternalFlash_d
// External Flash routines use 32 bits
typedef uint32_t popStorageSize_t;
#else
typedef uint16_t popStorageSize_t;
#endif
typedef uint8_t popStoragePage_t;
typedef uint32_t popStorageOffset_t;

#define gPopUpgradeReqMyHwType_c ePopHwTypeArm7
#if gPopMicroPowerLpcUpgrade_d
#define gPopLpcAvailableMemory_c 0x80000
#define gPopLpcUpgradeAppId_c 0xFFFE
#define gPopLpcUpgradeHwId_c 0xFE // JJ debug
#define gPopLpcStorageStart_c 0x00000000
#endif
#if gPopMicroPowerExternalFlash_d
#define gPopAvailableMemory_c 0x7C000
#else
#define gPopAvailableMemory_c 0xE000
#endif

/* Size of the Page */
#define gPopStoragePageSize_c 0x1000

void PopStorageInit(void);
void PopStorageEraseAll(void);                  // erases all blocks in storage area
void PopStorageErasePage(popStoragePage_t iPage);
void PopExternalStorageEraseAll(void);               // erases "all" blocks in external storage area
#if gPopMicroPowerLpcUpgrade_d
void PopLpcExternalStorageEraseAll(void);            // erases all blocks in external flash for LPC image storage
#endif

#define PopStoragePageSize()  gPopStoragePageSize_c  //512 for HS08, 0x1000 for Arm7
#define PopStorageTotalSize() gPopAvailableMemory_c         // returns total size of storage area
#define PopStorageNumPages() (PopStorageTotalSize() / PopStoragePageSize())         // returns total size of storage area
// note: returns a popErr_t error code
uint8_t PopEraseBlock(popStorageSize_t iBlockOffset);  // erases a single block containing the address of the ofset
// note: returns a popErr_t error code
uint8_t PopStorageWriteBytes(popStorageSize_t iDstOffset, void *pSrcAddr, popStorageSize_t iLen);
// note: returns a popErr_t error code
uint8_t PopEraseBlock(popStorageSize_t iBlockOffset);
// note: returns a popErr_t error code
uint8_t PopSwitchToNewImage(bool fPreserveNVM);
// note: returns a popErr_t error code
uint8_t PopStorageReadBytes(void *pDstAddr, popStorageSize_t iSrcOffset, popStorageSize_t  iLen);

extern uint8_t  gDigitalClock_RN;
extern uint8_t  gDigitalClock_RAFC;
extern uint8_t  gDigitalClock_PN;
extern uint8_t  gDefaultCoarseTrim;
extern uint8_t  gDefaultFineTrim;
extern uint8_t  gEnableComplementaryPAOutput;
extern uint8_t  gPowerLevelLock;
extern uint8_t  gCcaThreshold;
#endif  // _POPBSP_H_

