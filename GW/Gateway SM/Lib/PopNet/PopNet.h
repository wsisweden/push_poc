/***************************************************************************
  PopNet.h
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  PopNet 2.04

  San Juan Software Confidential and Proprietary. This source code is
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  This header file defines the public interface between applications and
  PopNet (including all networking and BSP functions).

****************************************************************************/
#ifndef _POPNET_H_
#define _POPNET_H_
#include "MpCfg.h"
#include <stdint.h>
#include <stdbool.h>

/************* Handy Macros ****************/
#define PopNumElements(array)         ((sizeof(array) / (sizeof(array[0]))))
#define PopMemberOffset(type, member) ((uintptr_t)&((type *)0)->member)
#define PopSizeOfMember(type, member) (sizeof(((type *)0)->member))


/************* PopNet Defines ****************/

// PopNet version and protocol information
#define gPopNetId_c             0x50    // Protocol ID in beacon (0x50 = PopNet, 0x01=ZigBee)
#define gPopNetProtocolVer_c    0x04    // Version of PopNet over-the-air Protocol
#define gPopNetVersion_c        0x0204  // Version of PopNet source base (2.1)
typedef uint16_t gPopVersion_t;
extern const gPopVersion_t giPopNetVersion;
#define PopNetGetVersion()  (giPopNetVersion)


// PopNet error codes
typedef uint8_t popErr_t;    //(Now defined in PopBsp.h)
#define gPopErrNone_c             0x00  // no error
#define gPopErrFailed_c           0x01  // generic failure
#define gPopErrNoMem_c            0x02  // out of memory
#define gPopErrBadTask_c          0x03  // bad task
#define gPopErrBadParm_c          0x04  // invalid parameter
#define gPopErrNotFound_c         0x05  // not found
#define gPopErrFull_c             0x06  // queue or buffer full
#define gPopErrEmpty_c            0x07  // queue or buffer is empty
#define gPopErrBusy_c             0x08  // subsystem full
#define gPopErrAlreadyThere_c     0x09  // attempting to add to a table that already contains the entry
// codes 0x10 - 0x1f are used for form/join status codes. see popStatus_t

#define gPopErrNoNwk_c            0x20  // nwk: not on a network yet (can't send messages)
#define gPopErrAlreadyOnNwk_c     0x21  // nwk: not on a network yet (can't send messages)
#define gPopErrRouteNotFound_c    0x22  // nwk: cannot find route to node
#define gPopErrRouteReplyNoAck_c  0x23  // nwk: unable to deliver the route reply as unicast.
#define gPopErrRouting_c          0x24  // nwk: packet needed a route discovery (in progress)
#define gPopErrICHYTDisabled_c    0x25  // nwk: i can hear you table disabled.

#define gPopErrNoImage_c          0x26  // ota: There is no image being saved
#define gPopErrBadAppId_c         0x27  // ota: Application Id does not correspond
#define gPopErrBadHwId_c          0x28  // ota: Hardware Id does not correspond
#define gPopErrBadPlatform_c      0x29  // ota: Platform does not correspond
#define gPopErrTimeOut_c          0x2A  // ota: Never got a response
#define gPopErrBlockSizeTooBig_c  0x2B  // ota: Can't send a message too big
#define gPopErrNotSupported_c     0x2C  // ota: Can't send a message too big
#define gPopErrCanceled_c         0x2D  // ota: User canceled transmission
#define gPopErrNvmSize_c          0x2E  // ota: Number of nv pages is different
#define gPopErrDeviceNotFound_c   0x2F  // nwk: The device was not found (Table, or Network).
#define gPopErrWrongBlock_c       0x30  // ota: The device was not found (Table, or Network).
#define gPopErrPowerOutOfRange_c  0x31  // phy: The given power level is out of range.
#define gPopErrNoValidCondition_c  0x32 // phy: Problem setting the given power level.
#define gPopErrBlockSaveValFail_c  0x33  // When checking that the block was saved correcly it failed.

// user defined error codes start at this #...
#define gPopErrUser_c             0x80   // 0x80-0xff is for application use

// these status codes are used for forming and joining networks
// see also popErr_t
typedef uint8_t popStatus_t;
#define gPopStatusSuccess_c             0x00 // same as gPopErrNone_c
#define gPopStatusNoMem_c               0x02 // out of memory when scanning
#define gPopStatusBadParms_c            0x04 // one or more invalid parameter
#define gPopStatusBusy_c                0x08 // system already forming/joining
#define gPopStatusFormSuccess_c         0x10 // successful formation of the network!
#define gPopStatusJoinSuccess_c         0x11 // successful joining of the network
#define gPopStatusSilentStart_c         0x12 // silently joined or formed (parms already valid)
#define gPopStatusAlreadyOnTheNetwork_c 0x13 // The node is on running mode.
#define gPopStatusPanAlreadyExists_c    0x14 // can't form as PAN ID already exists
#define gPopStatusJoinFailure_c         0x15 // join timed out
#define gPopStatusJoinDisabled_c        0x16 // all potential parents have join disabled
#define gPopStatusWrongMfgId_c          0x17 // all potential parents have different MfgId (use 0xffff to mean any)
#define gPopStatusNoParent_c            0x18 // no parent to join

/************* Assert Interface *************/
typedef uint8_t popAssertCode_t;
// Any assert code from 0x7F and below is reserved to be use by the stack.
#define gPopAssertStackOverRun_c     0x00
#define gPopAssertHeapCompromised_c  0x01
#define gPopAssertOutOfEvents_c      0x02
#define gPopAssertBadReceivePtr_c    0x03
#define gPopAssertNwkWatchdog_c      0x04
#define gPopAssertGwWatchdog_c       0x05

#define gPopAssertUnKnown_c          0x7f

// Any assert Id from 0x80and above is available for the user and the application to use.
#define gPopUserAssert_c  0x80

// Assert Prototype.
void PopAssert(popAssertCode_t iAssertCode);

// <SJS: Store Assert Count and last code in NVM, if NVM enabled >
// this structure is stored in NVM in PopAssert() if NVM enabled.
#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct sPopAssertCount_tag
{
  uint8_t        iAssertStackOverRun;
  uint8_t        iAssertHeapCompromised;
  uint8_t        iAssertOutOfEvents;
  uint8_t        iAssertBadReceivePtr;
  uint8_t        iAssertNwkWatchdog;
  uint8_t        iAssertGwWatchdog;
} sPopAssertCount_t;
#if gPopNeedPack_c
#pragma pack()
#endif

// the NVM code for the assert structure
#define gPopNvIdAssert_c             9
/// </SJS:>

#if DEBUG >= 2
#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct sMpDebugCount_tag
{
  uint8_t        iMaxPayloadCnt;
  uint8_t        iGwCmdCnt;
  uint8_t        iEventCnt;
  uint8_t        iHeapCnt;
  uint8_t        iNwkCmdCnt;
  uint16_t       iHeapFreeNow;
  uint16_t       iHeapFreeMin;
  uint16_t       iHeapFreeBlockSizeNow;
  uint16_t       iHeapFreeBlockSizeMin;
  uint8_t        iMemallocCounterNow;
  uint8_t        iMemallocCounterMax;
} sMpDebugCount_t;
#if gPopNeedPack_c
#pragma pack()
#endif

// the NVM code for the assert structure
#define gMpNvIdDebug_c             10
#endif

/* debugging tools */

// C stack bytes left in worst case usage. If within 20% of total stack size, increase gPopStackSize_c in PopCfg.h
uint16_t PopStackLeftAtHighWaterMark(void);

// Calculate stack size using end and start;
uint32_t PopGetStackSize(void);

// where is the signature mark for the C stack?
void *PopStackFloor(void);

// retry entries left in worst case usage. See gPopRetryEntries_c in PopCfg.h
uint8_t PopRetryEntriesLeftAtHighWaterMark(void);

// route discovery table entries left in worst case usage. See gPopRouteDiscoveryEntries_c in PopCfg.h
uint8_t PopRouteDiscoveryEntriesLeftAtHighWaterMark(void);

// duplicate table entries left in worst case usage. See gPopRetryEntries_c in PopCfg.h
uint8_t PopDuplicateEntriesLeftAtHighWaterMark(void);

// timer entries left in worst case usage. See gPopMaxTimers_c in PopCfg.c
uint8_t PopTimersLeftAtHighWaterMark(void);

// events left in worst case usage. See gPopMaxEvents_c in PopCfg.h
uint8_t PopEventsLeftAtHighWaterMark(void);

// debugging variables (for each of the tables: events, timers, retry, duplicate, routediscovery)
extern const uint8_t cPopMaxEvents;
extern uint8_t giPopNumEvents;
extern uint8_t giPopEventsHigWaterMark;
extern const uint8_t cPopMaxTimers;
extern uint8_t giPopTotalTimers;
extern uint8_t giPopTimerWaterMark;
extern const uint8_t cPopNwkRouteDiscoveryEntries;
extern uint8_t giPopNwkRouteDiscoveryHighWaterMark;
extern uint8_t giPopNwkRouteDiscoveryCount;
extern const uint8_t cPopNwkRetryEntries;
extern uint8_t giPopNwkRetryEntriesWaterMark;
extern uint8_t giPopNwkRetryEntriesCount;
extern const uint8_t cPopNwkDuplicateEntries;
extern uint8_t giPopNwkDuplicateEntriesCount;
extern uint8_t giPopNwkDuplicateHighWaterMark;



/************* Radio (Over-the-air) Interface ****************/
typedef uint16_t popPanId_t;
#define gPopNwkBroadcastPan_c   0xffff  // for broadcasting across PANs
#define gPopNwkAnyPanId_c       0xffff  // for PopNwkFormNetwork()
#define gPopNwkInvalidPan_c     0xfffe  // invalid PAN ID
#define gPopNwkReservedPan_c    0xfff1  // PAN IDs 0xfff1 - 0xfffe reserved

typedef uint16_t popNwkAddr_t;      // short address (only valid on the network)
#define gPopNwkBroadcastAddr_c   0xffff  // for broadcasting to nodes within the same PAN
#define gPopNwkAnyParentAddr_c   0xffff  // for PopNwkJoinNetwork()
#define gPopNwkFirstAddr_c       0x0000  // first valid address
#define gPopNwkReservedAddr_c    0xfff1  // all addresses 0xfff1 - 0xfffe reserved by PopNet
#define gPopNwkInvalidAddr_c     0xfffe  // invalid network address



typedef uint8_t aPopMacAddr_t[8];  // 64-bit MAC address, little endian (aka IEEE addr, long addr)
typedef uint8_t popChannel_t;      // (11-26 for 802.15.4 2.4GHz)
#define gPopNwkInvalidChannel_c  ((popChannel_t)0xfe)  // The invalid channel ID.
#define gPopNwkUseChannelList_c   0x00  // For the case of joining or forming, if we like to use the channel list.

extern popChannel_t giPhyChannel;   // what channel are we physically on right now?

/*
  PopNet channel list values.

      Channel Mask       Hex     802.15.4 channel #
  0000 0000 0000 0001 - 0x0001 - 11
  0000 0000 0000 0010 - 0x0002 - 12
  0000 0000 0000 0100 - 0x0004 - 13
  0000 0000 0000 1000 - 0x0008 - 14
  0000 0000 0001 0000 - 0x0010 - 15
  0000 0000 0010 0000 - 0x0020 - 16
  0000 0000 0100 0000 - 0x0040 - 17
  0000 0000 1000 0000 - 0x0080 - 18
  0000 0001 0000 0001 - 0x0100 - 19
  0000 0010 0000 0001 - 0x0200 - 20
  0000 0100 0000 0001 - 0x0400 - 21
  0000 1000 0000 0001 - 0x0800 - 22
  0001 0000 0000 0001 - 0x1000 - 23
  0010 0000 0000 0001 - 0x2000 - 24
  0100 0000 0000 0000 - 0x4000 - 25
  1000 0000 0000 0000 - 0x8000 - 26

  Or any combination of the above including all of them.
  e.g. channels 13, 14, and 20 would be: 0x020C
*/
typedef uint16_t popChannelList_t;  // each bit is a channel in a channel mask
#define gPopNwkNumOfChannels_c   16   // sixteen 802.15.4 channels
#define gPopNwkFirstChannel_c    11   // channels numbered 11 - 26

// Used by PopNwkStartNetwork(), PopNwkFormNetwork() and PopNwkJoinNetwork()
typedef uint8_t popNwkJoinOpts_t;
#define gPopNwkJoinOptsNormal_c       0     // normal form/join options
#define gPopNwkJoinOptsSilent_c       0x01  // do NOT send beacon after forming (form only)
#define gPopNwkJoinOptsDontGetAddr_c  0x02  // do NOT get addresses after joining (join only)
#define gPopNwkJoinOptsPassiveScan_c  0x04  // do not issue a beacon request when scanning for beacons
#define gPopNwkJoinOptsEnergyScan_c   0x08  // do an energy scan in addition to a beacon scan (form only)
#define gPopNwkJoinOptsReserved_c     0xf0  // reserved bits

// options for the scan PopNwkScanForNetworks(). Note: these MUST be the same bits as in popNwkJoinOpts_t
typedef uint8_t popScanOption_t;
#define gPopNwkScanOptsNormal_c       0x00  // normal scan options
#define gPopNwkScanOptsPassiveScan_c  0x04  // do NOT issue a beacon request when scanning for beacons
#define gPopNwkScanOptsReserved_c     0xf0  // reserved bits


typedef uint8_t popNwkRadius_t;
typedef uint8_t popSequence_t;        // a sequence #
typedef uint8_t popProtocol_t;       // Protocol Id ( 0x50=PopNet, 0x01=ZigBee )
typedef uint8_t popProtocolVer_t;    // Protocol Version (top bit indicates join enable)
typedef uint8_t popEnergyLevel_t;    // the energy level obtanied during the scan mainly.
typedef uint8_t popLqi_t;            // Link quality indicator (0 = weak signal, 64 = medium, 128+ = strong signal, 255=from this node)
#define gPopNwkLqiFromThisNode_c  0xff  /* indicates from this node */
typedef uint8_t popCmd_t;              // the command id.
typedef uint16_t popAddrCount_t;     // the amount of address in the address pool.

// Mac Header Frame Control (from 7.2.1.1 Frame Control Field - IEEE 802.15.4 2006 spec)
//
// xxxx xxxx xxxx xTTT  Frame type (000=beacon, 001=data, 010=ack, 011=cmd)
// xxxx xxxx xxxx Sxxx  Security enabled
// xxxx xxxx xxxP xxxx  Pending frame (to inform sleepy node)
// xxxx xxxx xxAx xxxx  Ack requested
// xxxx xxxx xCxx xxxx  Pan ID compression (0=src & dst PANs, 1=dst PAN only)
// xxxx xxRR Rxxx xxxx  Reserved bits
// xxxx DDxx xxxx xxxx  Destination address mode (00=not present, 10=16-bit, 11=64-bit)
// xxVV xxxx xxxx xxxx  Frame version (00=2003, 01=2006)
// SSxx xxxx xxxx xxxx  Source address mode (00=not present, 10=16-bit, 11=64-bit)
typedef uint16_t popMacFrameControl_t;  // frame control used in the MAC headers

// NWK Header Framne Control
//
// xxxx xxxx xxxx xxTT  Frame type (00=data, 01=cmd)
// xxxx xxxx xxVV VVxx  Protocol version (always 0010 = 0x02)
// xxxx xxxx DDxx xxxx  Discover route (00=none, 01=route request, 02=forced)
// xRRR RRRR xxxx xxxx  Reserved
// xxxx xxxx xxxx xxxx  Route discovery is forced
//
typedef uint16_t popNwkFrameControl_t;  // frame control used in the NWK headers

// The manufacturer ID is assigned by San Juan Software. This ID uniquely identifies nodes
// from one manufacturer. This list is never shared or published. Used to join the right
// network.
typedef uint16_t popMfgId_t;   // 16-bit manufacturer identifier
#define gPopMfgIdAny_c      0xffff      // in find node, find any Mfg ID
#define gPopMfgIdSjs_c      0x0001      // San Juan Software's Mfg ID

// Application Id
typedef uint16_t popAppId_t;          // 16-bit application identitifer
#define gPopAppIdAny_c                  0xffff // Used in PopNwkFindNode(), and OTA Upgrades to match any App ID
#define gPopAppIdTest_c                 0x0001 // PopAppTest
#define gPopAppIdOnOffLight_c           0x0002 // PopAppOnOffLight (as a switch)
#define gPopAppIdOnOffSwitch_c          0x0003 // PopAppOnOffLight (as a light)
#define gPopAppIdMazePlayer_c           0x0004 // PopAppMaze       (as a player)
#define gPopAppIdMazeGateway_c          0x0005 // PopAppMaze       (as a gateway)
#define gPopAppIdMobileTrackingNode_c   0x0006 // PopAppTracking   (as mobile-node)
#define gPopAppIdRouterTrackingNode_c   0x0007 // PopAppTracking   (as router)
#define gPopAppIdGatewayTrackingNode_c  0x0008 // PopAppTracking   (as gateway)
#define gPopAppIdGenericApp_c           0x0009 // PopAppGeneric
#define gPopAppIdUser_c                 0x00ff // user IDs are 0xFF and above, and are managed by the customer


/* The key for security. */
typedef uint8_t aPopNwkKey_t[16];

/* size of a heap */
typedef uint16_t popHeapSize_t;        // size of entire heap

// get current sequence #. Used to handle ACKs.
extern popSequence_t giPopNwkSequence;

// total packet size may be as large as PHY (subtract 16-bits for FCS)
// actuall app data will be smaller (gPopMaxFrame_c - (sizeof(MAC Hdr) + sizeof(NWK Hdr))
#define gPopMaxFrame_c  (gPopPhySize_c - 2)
extern const uint8_t giPopPhySize;

// the number of PHY packets received (for this node or not) since last calling PopPhyPacketCount();
// use to determine if messages aren't being delivered due to congengestion or range issues.
uint32_t PopPhyPacketCount(void);

extern const uint8_t giPopNwkMaxPayload;
#define PopNwkGetMaxPayload() (giPopNwkMaxPayload)


// These are the TxOptions for PopNwkDataRequest(), which sends data from this node
// to one or more other nodes over-the-air. To broadcast, use destination address
// 0xffff (gPopNwkBroadcastAddr_c) for iDstAddr in sPopnwkDataRequest_t.
//
typedef uint8_t popNwkDataReqOpts_t;
#define gPopNwkDataReqOptsDefault_c       0x00  // unicast: disover a route if needed
#define gPopNwkDataReqOptsNoDiscover_c    0x01  // unicast: don't discover a route
#define gPopNwkDataReqOptsForce_c         0x02  // unicast: force route discovery
#define gPopNwkDataReqOptsUnibroadcast_c  0x04  // unicast to destination via broadcast
#define gPopNwkDataReqOptsNoRetry_c       0x08  // don't retry (set retries to 1)
#define gPopNwkDataReqOptsDuplicatesOk_c  0x10  // don't track in the duplicate table of receiving nodes
#define gPopNwkDataReqOptsFromThisNode_c  0x80  // used in sPopNwkPreHeader_t to indicate packet from this node
#define gPopNwkDataReqOptsReserved_c      0xe0  // reserved options

// used for path cost calculation during route discovery
typedef uint8_t popNwkPathCost_t;

// a data request from the application
#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct sPopNwkDataRequest_tag
{
  popNwkAddr_t        iDstAddr;       // 0-n or 0xffff (gPopNwkBroadcastAddr_c)for broadcast
  popNwkDataReqOpts_t iOptions;       // discover route, security enable
  popNwkRadius_t      iRadius;        // radius 0xff means forever. Usually set to gPopDefaultRadius_c
  uint8_t              iPayloadLength; // up to PopNwkMaxPayload()
  void               *pPayload;
} sPopNwkDataRequest_t;

// a data confirm
typedef struct sPopNwkDataConfirm_tag
{
  popErr_t      iErr;
  popSequence_t iNwkSequence;
} sPopNwkDataConfirm_t;

/*
  The route discovery flags are used to determine where confirms go, and whether to modify the packet to retry
  via routing. Communicated via the overloaded LQI field when sending from this node...

  This data is communicated to NWK layer via preheader flags when sending from this node
*/
typedef struct popNwkRetryFlags_tag
{
  unsigned int fIsValid     : 1;   // this record is valid (contains pointer to a data request)
  unsigned int fIsBroadcast : 1;   // packet is unicast or broadcast?
  unsigned int fIsItFromNwk : 1;   // higher layer is APP or NWK layer? (where does the confirm go)?
  unsigned int fConfirm     : 1;   // send confirm to higher layer when done? (that is, was the data req from this node?)
  unsigned int fRoute       : 1;   // discover a route if failed to deliver to neighbor
  unsigned int fNoRetry     : 1;   // don't retry. just set retries to 1.
  unsigned int fWaitDataInd : 1;   // wait a bit to send indication to app, as this came in on a route request.
  unsigned int fReserved    : 1;   // reserved
} popNwkRetryFlags_t;

/*
  Used with event gPopEvtNwkJoinIndication_c, to indicate to the application that a join
  Set iNwkAddr to:
  0x0000 - 0xfff0  = NwkAddr (valid short address) that will be assigned to joining node
  0xffff           = PopNet will chose an address from address pool (default)
  0xfffe           = Prevent node from joining
*/
typedef struct sPopNwkJoinIndication_tag
{
  popNwkAddr_t  iNwkAddr;   // set to 0xffff (default), 0xfffe (forbit joining), 0x0001-0xfff0 (NwkAddr)
  aPopMacAddr_t aMacAddr;   // MAC address of node wishing to join
} sPopNwkJoinIndication_t;

// part of the over-the-air frame (MAC data). 9 bytes.
// See PopNet Developer's Guide Section x.x.x
typedef struct _tagMacDataFrame
{
  popMacFrameControl_t iFrameControl;
  popNwkPathCost_t  iPathCost;  // path cost if discovering route (in 802.15.4 this is SeqNo)
  popPanId_t iPanId;      // 0x0000 - 0xfff0, or 0xffff for broadcast
  popNwkAddr_t iDstAddr;  // dst for this hop (0xffff = broadcast)
  popNwkAddr_t iSrcAddr;  // src for this hop
} sMacDataFrame_t;

// part of the over-the-air frame (nwk data). 9 bytes.
typedef struct _sNwkDataFrame_t
{
  popNwkFrameControl_t iFrameControl;  // similar in frame to ZigBee
  popNwkAddr_t      iDstAddr;       // ultimate destination
  popNwkAddr_t      iSrcAddr;       // original source
  popNwkRadius_t    iRadius;        // radius (dec by 1 each time)
  popSequence_t     iSequence;      // seq + srcAddr uniquely identifies this packet
} sNwkDataFrame_t;

// over-the-air (OTA) data frame (0-125 bytes total)
typedef struct _tagPopDataFrame
{
  sMacDataFrame_t sMac;   // MAC portion of frame
  sNwkDataFrame_t sNwk;   // NWK portion of frame
  uint8_t aPayload[1];     // variable length payload (0-n)
} sPopDataFrame_t;

// header in front of the OTA frame for indications
// note that this information is NOT sent over-the-air, but is still useful in the indcation.
typedef struct _tagPopNwkPreHeader
{
  uint8_t                iFrameLen;    // length of payload
  popLqi_t              iLqi;         // will be 0xff if sending data from this node
  popNwkRetryFlags_t    iRetryFlags;  // if sending data from this node
} sPopNwkPreHeader_t;

// this structure is passed to the application layer when data is received
typedef struct _tagPopNwkDataIndication
{
  sPopNwkPreHeader_t  sPre;   // LQI and frame length
  sPopDataFrame_t    sFrame;   // OTA frame
} sPopNwkDataIndication_t;

/*
  sPopNwkDuplicateTable_t

  Remember packets so duplicates aren't sent to higher layers. This is checked BEFORE
  sending the packet to higher layers (the application). Used for both per-hop unicast
  ACKs and broadcasts. The Duplicate table has the same # of entries as the retry table.

  5 bytes per table entry.

  See PopCfg.h, gPopRetryTableEntries_c
*/
typedef struct sPopNwkDuplicateTable_tag
{
  popTimeOut_t  iTimeLeftMs;  // how long until expiring this entry? (if 0, entry is free)
  popNwkAddr_t  iNwkSrc;      // NWK src and sequence uniquely identify packet
  popSequence_t iSequence;    // for uniquely identifying packet
} sPopNwkDuplicateTable_t;


// secondary flags not communicated via the overloaded LQI field when sending packets
typedef struct popNwkRetryFlags2_tag
{
  unsigned int iRREQIndex         : 5;   // index into route request table if fWait4RREP set
  unsigned int fIsOtaFrameNative  : 1;   // is the OTA frame in native or OTA form?
  unsigned int fDidRouteDiscovery : 1;   // did we do a route discovery to deliver this packet?
  unsigned int fWait4RREP         : 1;   // waiting for a route reply (before confirm can be sent)
} popNwkRetryFlags2_t;

/*
  sPopNwkRetryTable_t

  This keeps track of anything that needs to be (re)sent out the radio. This
  includes both unicasts and broadcasts.

  10 Bytes per entry. The OTA frame is stored secured and OTA endian.

  See PopCfg.h, gPopRetryTableEntries_c
*/
typedef struct sPopNwkRetryTable_tag
{
  popNwkRetryFlags_t  iFlags;          // user options communicated tothe retry table
  popNwkRetryFlags2_t iFlags2;         // includes # of retries, and other fields
  popTimeOut_t        iTimeLeftMs;     // # milliseconds left before retrying...
  uint8_t             iRetries;        // # of retries before giving up
  popSequence_t       iSequence;       // NWK sequence # for packet
  popNwkAddr_t        iSrcAddr;        // NWK addr for packet (together NwkAddr+Seq = unique packet ID)
  sPopNwkDataIndication_t *pOtaFrame;  // pointer to frame that will go over-the-air, stored secured and OTA endian
} sPopNwkRetryTable_t;

// For keeping track of asymmetric links when sending route replies (RREP).
typedef struct _tagPopNwkAsymmetricLinkEntry
{
  popNwkAddr_t          iPrevHop;
  popNwkPathCost_t      iPathCost;
} sPopNwkAsymmetricLinkEntry_t;

// route flags are used during route discovery
typedef struct popNwkRouteReqFlags_tag
{
  unsigned char fIsInUse        : 1;  // entry currently in use or not
  unsigned char fWaitingToStart : 1;  // waiting to start
  unsigned char fWaitingOnAck   : 1;  // waiting on an ACK from a RREP
  unsigned char iReserved       : 5;  // reserved
} popNwkRouteReqFlags_t;

/*
  Route discovery structure

  Used during the entire route discovery process. This structure is kept around for the entire broadcast time.

  size = 24 bytes (if gPopAsymmetricLinkEntries_c == 3).
*/
typedef struct _tagPopNwkRouteDiscovery
{
  popNwkRouteReqFlags_t iFlags;         // flags for this route entry
  popSequence_t         iSequence;      // NwkSeq
  popNwkAddr_t          iSrcAddr;       // NwkSrcAddr + NwkSeq # uniquely identifies this route discovery
  popTimeOut_t          iTimeLeftMs;    // # milliseconds left before expiring this entire entry
  popTimeOut_t          iTimeAckWait;   // # of milliseconds to wait for the RREP ACK (also used to start)
  popNwkAddr_t          iDstAddr;       // which ultimate destination are we discovering
  popNwkAddr_t          iRRepSrcAddr;   // where did the first RREP come from?
  uint8_t               iRetryEntry;    // contains the index of the entry in retries table associated to this discovery
  uint8_t               iAckTries;      // # of ACK tries completed
  uint8_t               iPrevHopIndex;  // which entry are we trying in the array below?
  sPopNwkAsymmetricLinkEntry_t aLinkEntry[gPopAsymmetricLinkEntries_c];
} sPopNwkRouteDiscovery_t;

// whether route is valid or not
// See PopNet Developer's Guide Section x.x.x
typedef uint8_t popNwkRouteFlags_t;
#define gPopNwkRouteFree_c    0x00  // not used
#define gPopNwkRouteValid_c   0x80  // currently in use

// route structure (used once a route is valid)
// See PopNet Developer's Guide Section x.x.x
typedef struct _tagPopNwkRouteEntry
{
  popNwkRouteFlags_t  iFlags;     // flags for this route entry
  popSequence_t       iRouteAge;  // allows older routes to be expired
  popNwkAddr_t        iDstAddr;   // ultimate destination
  popNwkAddr_t        iNextHop;   // either next hop (when route is valid) or previous hop (when pending)
} sPopNwkRouteEntry_t;

// Use this simplified route entry structure for PopNwkSetRoutes() and PopNwkGetRoutes().
// See PopNet Developer's Guide Section x.x.x
typedef struct _tagPopNwkSimpleRoute
{
  popNwkAddr_t        iDstAddr;   // ultimate destination
  popNwkAddr_t        iNextHop;   // either next hop (when route is valid) or previous hop (when pending)
} sPopNwkSimpleRoute_t;

#if gPopNeedPack_c
#pragma pack()
#endif

// Get/Set route entry functions. Gets/sets a copy of the entries.
// See PopNet Developer's Guide Section x.x.x
extern const uint8_t giPopNwkRouteEntries;  // maximum # of routes to get or set
void PopNwkSetRoutes(uint8_t iCount, sPopNwkSimpleRoute_t *pRoutes);
uint8_t PopNwkGetRoutes(uint8_t iMax, sPopNwkSimpleRoute_t *pRoutes);
void PopNwkAddRoute(popNwkAddr_t iDstAddr, popNwkAddr_t iNextHop);

// maximum application payload for transmitting, given current security scheme
extern const uint8_t giPopNwkMaxPayload;
#define PopNwkMaxPayload() (giPopNwkMaxPayload)

// data requests result in a data confirm, via gPopEvtDataConfirm_c
void PopNwkDataRequest(sPopNwkDataRequest_t *pDataReq);

// MAC control frame is secure if security is on.
#define gMacFcSecure_c 0x0008

// data indication macros
#define PopNwkWasBroadcast(pIndication) (((sPopNwkDataIndication_t *)(pIndication))->sFrame.sMac.iDstAddr == gPopNwkBroadcastAddr_c)
#define PopNwkWasSecured(pIndication) (((sPopNwkDataIndication_t *)(pIndication))->sFrame.sMac.iFrameControl & gMacFcSecure_c)
#define PopNwkLqi(pIndication) (((sPopNwkDataIndication_t *)(pIndication))->sPre.iLqi)
#define PopNwkPayload(pIndication) (void *)(((sPopNwkDataIndication_t *)(pIndication))->sFrame.aPayload)  // ptr to payload
#define PopNwkPayloadLen(pIndication) (((sPopNwkDataIndication_t *)(pIndication))->sPre.iFrameLen - (sizeof(sMacDataFrame_t) + sizeof(sNwkDataFrame_t)))
#define PopNwkSrcAddr(pIndication) (((sPopNwkDataIndication_t *)(pIndication))->sFrame.sNwk.iSrcAddr)
#define PopNwkPrevHop(pIndication) (((sPopNwkDataIndication_t *)(pIndication))->sFrame.sMac.iSrcAddr)


/*
  sPopNwkData_t defines the globals that will be saved in NVM (if NVM is enabled)
  See PopNvStoreNwkData() and PopNvRetrieveNwkData().
*/
#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct sPopNwkData_tag
{
  popChannelList_t  iChannelList;     // channel list used for scanning
  popChannel_t      iChannel;         // channel chosen when on the network
  bool            fSleepyMode;      // go into sleepy mode (no routing, no repeating broadcasts)
  popPanId_t        iPanId;           // the PAN ID this node is on
  popNwkAddr_t      iNodeAddr;        // this nodes address (0xfffe if undefined)
  aPopMacAddr_t     aMacAddr;         // 64-bit MAC (IEEE) address
  popAppId_t          iAppId;           // application ID
  aPopNwkKey_t      aSecurityKey;     // current security key
  popNwkAddr_t      iAddressServer;   // short address of address server
  popNwkAddr_t      iNextAddr;        // start address in the pool
  popAddrCount_t    iAddrCount;       // current address count in the pool
  popAddrCount_t    iAddrReqSize;     // address request size
  bool                fJoinEnable;      // is join enabled or disabled?
} sPopNwkData_t;
#if gPopNeedPack_c
#pragma pack()
#endif


typedef uint8_t popNvPageSequence_t;

// each valid flash page begins with one of these
typedef struct _tagPopNvPage_t
{
  uint8_t              marker1;    // valid page marker 1 ('M')
  uint8_t              marker2;    // valid page marker 2 ('P')
  uint8_t              marker3;    // valid page marker 3 ('A')
  uint8_t              marker4;    // valid page marker 4 ('C')
  uint8_t              marker5;    // activated page marker 1 ('C')
  uint8_t              marker6;    // activated page marker 2 ('E')
  uint8_t              marker7;    // activated page marker 3 ('S')
  uint8_t              marker8;    // activated page marker 4 ('S')
  popNvPageSequence_t iSequence1; // rolling sequence # (0x00-0xfe)
  popNvPageSequence_t iSequence2; // rolling sequence # (0x00-0xfe)
  popNvPageSequence_t iSequence3; // rolling sequence # (0x00-0xfe)
  uint8_t              dummy;
} sPopNvPage_t;

// some useful globals for getting/setting various network fields
// do not access globals directly. Go through interfaces below.
extern sPopNwkData_t gsPopNwkData;
extern popTimeOut_t giPopNwkScanTimeMs;
extern const uint8_t giPopDefaultRadius;
extern const popTimeOut_t giPopAppEndToEndAckTimeout;
extern popMfgId_t giPopMfgId;
extern popNwkJoinOpts_t  giPopJoinOpts;
extern const uint8_t giPopSecurityLevel;
extern const uint16_t giPopNwkSecureCounterIncrement;
extern uint8_t giPopNwkJoinRetries;
extern bool gfPopNwkIsProcessingBeaconEnabled;


// For getting and setting the current channel. Generally channel is set by PopNwkStartNetwork().
// An application should ONLY set this if also setting PAN ID and Node Addr.
#define PopNwkGetChannel()                  (gsPopNwkData.iChannel)
void PopNwkSetChannel(popChannel_t iChannel);

// Return the current channel list. Only used with PopNwkStartNetwork();
#define PopNwkGetChannelList()              (gsPopNwkData.iChannelList)
// Set the list of channel proveded into the current channel list.
#define PopNwkSetChannelList(list)          (gsPopNwkData.iChannelList = ((popChannelList_t)(list)))

// Internal Use ONLY: changes channel field, but not physical channel
#define PopNwkSetNwkDataChannel(iValue)     (gsPopNwkData.iChannel = (iValue))

// Get the current MAC (aka IEEE) address into the provided parameter.
#define PopNwkGetMacAddr(pMacAddr)           PopMemCpy(pMacAddr, gsPopNwkData.aMacAddr, sizeof(aPopMacAddr_t))
// Get a pointer to the current MAC (IEEE) address
#define PopNwkGetMacAddrPtr()               (gsPopNwkData.aMacAddr)
// Set the provided parameter as the current MAC (IEEE) address.
void PopNwkSetMacAddr(aPopMacAddr_t aMacAddr);

// Returns the current node address.
#define PopNwkGetNodeAddr()                  ((popNwkAddr_t)gsPopNwkData.iNodeAddr)
// Sets the provede parameter as the curretn node address.
#define PopNwkSetNodeAddr(nodeAddr)          (gsPopNwkData.iNodeAddr = (popNwkAddr_t)(nodeAddr))

// Returns the current PAN ID.
#define PopNwkGetPanId()                     (gsPopNwkData.iPanId)
// Sets the provided parameter as the current PAN ID.
#define PopNwkSetPanId(panId)               (gsPopNwkData.iPanId = (popPanId_t)(panId))

// Returns the current time out to scan on each channel. (see channel list)
#define PopNwkGetScanTimeMs()                ((popTimeOut_t)giPopNwkScanTimeMs)
// Set the given time as the default time to scan on each given channel. (see channel list).
#define PopNwkSetScanTimeMs(time)            (giPopNwkScanTimeMs = ((popTimeOut_t)(time)))

// scan (aka join) options determine how PopNwkStartNetwork() works. For PopNwkFormNetwork(), and
// PopNwkJoinNetwork(), the parameters are passed to the function.
#define PopNwkGetScanOptions()               ( giPopJoinOpts )
#define PopNwkGetJoinOptions()               ( giPopJoinOpts )

#define PopNwkSetScanOptions(iOpts)          ( giPopJoinOpts = iOpts )
#define PopNwkSetJoinOptions(iOpts)          ( giPopJoinOpts = iOpts )

// Scan options for the PopNwkStartNetwork() process.
// note: giPopJoinOpts are used both for joining and forming
// Sets active scan On.
#define PopNwkSetActiveScanOn()              (giPopJoinOpts |= gPopNwkJoinOptsPassiveScan_c)
// Sets active scan Off.
#define PopNwkSetActiveScanOff()             (giPopJoinOpts &= (~gPopNwkJoinOptsPassiveScan_c))

// Set energy scan On.
#define PopNwkSetEnergyScanOn()              (giPopJoinOpts |= gPopNwkJoinOptsEnergyScan_c)
// Set energy scan Off.
#define PopNwkSetEnergyScanOff()             (giPopJoinOpts &= ~(gPopNwkJoinOptsEnergyScan_c))

// Get the current join status.
#define PopNwkIsJoiningEnabled(options)      ((options) & 0x80)
// Check if the protecol is PopNet protocol
#define PopNwkIsValidProtocol(protocol)      ((protocol) == gPopNetId_c)
#define PopNwkIsValidProtocolVer(protVer)    (((protVer) & 0x7f) == gPopNetProtocolVer_c)

// Returns the current manufacturer Id.
#define PopNwkGetManufacturerId()            (giPopMfgId)
// Sets the given parameter as the current Manufacturer Id.
#define PopNwkSetManufacturerId(mfgId)       (giPopMfgId = ((popMfgId_t)(mfgId)))

// Returns the current manufacturer Id.
#define PopNwkGetApplicationId()            (gsPopNwkData.iAppId)
// Sets the given parameter as the current Manufacturer Id.
#define PopNwkSetApplicationId(appId)       (gsPopNwkData.iAppId = ((popAppId_t)(appId)))

// Return the default radius (the maximum # of hops a message will propogate)
#define PopNwkGetDefaultRadius()             ( giPopDefaultRadius )

// return the amount of time an application should wait before assuming a message
// was not acknowledged by the other side. The application must send the acknowledgement,
// as PopNet has no end-to-end ACKs built in the stack. This just gives a convenient
// timeout.
#define PopNwkAppEndToEndAckTimeout()        ( giPopAppEndToEndAckTimeout )

// Get the current local join status set by PopNwkJoinEnabled()
// Applications should not call PopNwkSetJoinStatus() directly, instead use:
// void PopNwkJoinEnable(bool fEnable, bool fNetworkWide)
#define PopNwkGetJoinStatus()                ( gsPopNwkData.fJoinEnable )
#define PopNwkSetJoinStatus(status)          (gsPopNwkData.fJoinEnable = (status))

// Enable or disable requesting addresses if joining via PopNwkStartNetwork()
#define PopNwkSetAddrPoolReqOn()             (giPopJoinOpts &= (~gPopNwkJoinOptsDontGetAddr_c))
#define PopNwkSetAddrPoolReqOff()            (giPopJoinOpts |= gPopNwkJoinOptsDontGetAddr_c)

// Get or set the join retries (1 to 255)
#define PopNwkSetJoinRetries(amount)         (giPopNwkJoinRetries = ( amount ))
#define PopNwkGetJoinRetries()               (giPopNwkJoinRetries)

// Returns the current level of security. See popSecurityLevel_t.
// (0= none, 1= light, 2 = AES128-bit).
// note: security level cannot be set at run-time. It is a compile-time option.
#define PopNwkGetSecurityLevel()              (giPopSecurityLevel)

// get or set a copy of the local security key
#define PopNwkGetSecurityKey(aKey)            (PopMemCpy(aKey, gsPopNwkData.aSecurityKey, sizeof(aPopNwkKey_t)))
#define PopNwkGetSecurityKeyPtr()             (gsPopNwkData.aSecurityKey)


// get the curretn network status TRUE if we are on the network FALSE otehrwise.
bool PopNwkGetNetworkStatus(void);

// The "I can hear you" table allows a developer to build a tabletop network
// that discovers routes. Set "gPopICanHearYouEntries_c" to 0 to disble.
#if !gPopICanHearYouEntries_c
 #define gTmpICanHearYouEntries_c 1
#else
 #define gTmpICanHearYouEntries_c gPopICanHearYouEntries_c
#endif

/*
  The I-Can-Hear-You table allows for table-top routing by limiting this node to accept
  packets only from those nodes in the table, can only hear a set of other nodes instead
  of all in range.
*/
extern popNwkAddr_t gaPopNwkICanHearYouTable[gTmpICanHearYouEntries_c];
extern const uint8_t giPopNwkICanHearYouEntries;
void PopNwkClearICanHearYou(void);
void PopNwkSetICanHearYou(uint8_t iNum, popNwkAddr_t *pArray);
#define PopNwkGetICanHearYouEntries() (giPopNwkICanHearYouEntries)

/* Helper macros toduring the forming or joining process. */
#define PopNwkIsValidPanId(panid)              (((panid) < gPopNwkReservedPan_c))
#define PopNwkIsRandomPanId(panid)             (((panid) == gPopNwkAnyPanId_c))
#define PopNwkIsValidUnicastAddress(address)   ((address) < gPopNwkReservedAddr_c)
#define PopNwkIsValidChannelList(channelList)  ((popChannelList_t)channelList)
bool PopNwkIsOnNetwork(void);
bool PopNwkIsJoiningOrForming(void);

/*
  Return the first channel in this bitmask of channels. Returns 0 if no more channel bits.
  iStartChannel=0 for first channel, iStartChannel=previously returned channel for next channel.

  Example:
  iChannel = PopNwkGetFirstChannel(iChannelList, 0);
  iChannel = PopNwkGetFirstChannel(iChannelList, iChannel);
*/
popChannel_t PopNwkGetFirstChannel(popChannelList_t iChannelList, popChannel_t iStartChannel);


/*
  Network layer: sPopNwkBeaconIndication_t

  Serves for informing the NHL that we just got a beacon indication.
*/
#if gPopNeedPack_c
#pragma pack(1)
#endif

// for association requests
typedef struct _tagPopNwkAssociationReq_t
{
  uint16_t      iFrameControl;  // 0xc823
  uint8_t       iSequence;      // 0x50 = 'P'
  popPanId_t    iPanId;         // pan ID, little endian
  popNwkAddr_t  iDstAddr;       // desintation address, little endian
  popNwkAddr_t  iSrcPan;        // 0xffff
  aPopMacAddr_t aSrcIeeeAddr;   // source MAC address
  uint8_t       iCmd;           // 0x01 - association request
  uint8_t       iCapInfo;       // 0x8e - router
} sPopNwkAssociationReq_t;


// 20 bytes each
typedef struct _sPopNwkBeaconIndication_t
{
  popChannel_t      iChannel;      // channel 11-26
  popLqi_t          iLqi;          // link quality indicator
  popNwkAddr_t      iNwkAddr;      // address of responding device
  popPanId_t        iPanId;        // 16-bit pan identifier
  popProtocol_t     iProtocol;     // 0x50=PopNet, 0x01=ZigBee
  popProtocolVer_t  iProtocolVer;  // top bit indicates join enable
  aPopMacAddr_t     aMacAddr;      // 64-bit IEEE (MAC) address
  popMfgId_t        iMfgId;        // 16-bit manufacturer identifier
  popAppId_t        iAppId;        // 16-bit application identitifer
} sPopNwkBeaconIndication_t;
#if gPopNeedPack_c
#pragma pack()
#endif

/* Node Placement Result*/
typedef struct popNodePlacement_tag
{
  uint8_t    iNodeCount;       // number of nodes this node could possibly join (or neighbors)
  popLqi_t  iLqi;             // Link Quality
  uint8_t    iBars;            // 0-4, a number indicating how strong this route will be. See below.
} sPopNodePlacement_t;


/*
  Structure returned on the scan confirm (gPopEvtNwkScanConfirm_c), initiated by a
  call to PopNwkScanForNetworks() or PopNwkStartNetwork().

  This structure shows the results of the scan. The beacon list will be as
  large as needed to hold all the beacons, up to a maximum of 12 beacons. Each
  beacon is 20 bytes in size. See sPopNwkBeaconIndication_t.
*/
typedef struct popNwkScanForNetworks_tag
{
  uint8_t iNumberOfBeacons; // # of nodes in the following list
  sPopNwkBeaconIndication_t aBeaconList[1];
} sPopNwkScanForNetworks_t;

// an array of 16-bytes represents the channels
typedef struct sPopNwkScanForNoise_tag
{
  popEnergyLevel_t aEnergyLevels[gPopNwkNumOfChannels_c];
} sPopNwkScanForNoise_t;


// enable or disable joining. Cannot fail.
// See PopNet Developer's Guide section x.x.x
void PopNwkJoinEnable(bool fEnable, bool fNetworkWide);

// A bitmask of options used by PopNwkFindNode()
typedef uint8_t popNwkFindOptions_t;
#define gPopNwkFindOptMacAddr_c   0x80  // find a node by its MAC address
#define gPopNwkFindOptAppId_c     0x40  // find a node by its application ID
#define gPopNwkFindOptIdentify_c  0x20  // find a node that is in identify mode

// Parameter used by PopNwkFindNode()
typedef struct _sPopNwkFindNode_t
{
  popNwkAddr_t        iNwkAddr;   // the network address to send to
  popNwkRadius_t      iRadius;    // radius to send command
  popNwkFindOptions_t iOptions;   // options (identify, app id, mac addr)
  popAppId_t          iAppId;     // find by application ID
  aPopMacAddr_t       aMacAddr;   // find by MAC (IEEE) address
} sPopNwkFindNode_t;

// Find an application on the
// See PopNet Developer's Guide section x.x.x
void PopNwkFindNode(sPopNwkFindNode_t *pFindNode);

// enter/exit identify mode
// See PopNet Developers's Guide section
extern bool gfPopIdentifyMode;
#define PopNwkSetIdentifyMode(fEnable) gfPopIdentifyMode = (bool)(fEnable);
#define PopNwkGetIdentifyMode()       (gfPopIdentifyMode)



/* Node Placement Prototypes*/
#if gPopNwkNodePlacement_d
void PopNwkStartNodePlacement(popChannel_t iChannel);
void PopNwkEndNodePlacement(void);
#define PopNwkInNodePlacementMode()     gfPopNwkInNodePlacementMode
extern bool gfPopNwkInNodePlacementMode;
#else
#define PopNwkStartNodePlacement(iChannel)
#define PopNwkEndNodePlacement()
#define PopNwkInNodePlacementMode()     false
#endif

// start the blinking pattern used for demo applications
// # of blinks is low nybble of application id (gPopAppId_c)
#define PopNwkStartBlinkPattern() (void)PopSetEventNoData(gPopEvtAppBlink_c);

/* Bit masks for NVM NWK Data categories, to indicate which has changed */
typedef uint8_t popNvmDataChangedBitMask_t;
#define gPopNvmCountersChanged_c    (1<<0)  /* Security counters change often (every packet received/sent) */
#define gPopNvmRoutesChanged_c        (1<<1)    /* Network Routes ... not necessary to save them, but saves time on resets */
#define gPopNvmNwkDataChanged_c     (1<<2)  /* Network NwkAddress, PanId, MacAddress, Address Pool, etc. */

/* Interface to the Nvm Changed data functionality. */
#define PopNvmClearDataChangedBit(iBitMask) (giPopNvmDataChanged &= ~(iBitMask))
#define PopNvmClearAllDataChangedBits()     (giPopNvmDataChanged = 0)
#define PopNvmCheckBitOnMask(iBit)          (giPopNvmDataChanged & (iBit))
#define PopNvmSetBitOnMask(iBit)            (giPopNvmDataChanged |= (iBit))


/* mark a particular NVM network category as changed. Will generate the event gPopEvtNvmDataChanged_c if new change. */
void PopNvmDataMarkAsChanged(popNvmDataChangedBitMask_t iNvmDataChanged);

/* Bitmask that indicates what Nvm data changed since last time it was saved. */
extern popNvmDataChangedBitMask_t giPopNvmDataChanged;
#define PopNvmGetDataChanged()   giPopNvmDataChanged

/************** Security Interface ****************/
typedef uint8_t popSecurityLevel_t;
#define gPopNwkNoSecurity_c    0
#define gPopNwkLiteSecurity_c  1
#define gPopNwkAESSecurity_c   2

typedef uint16_t popNwkLiteCounter_t;
typedef uint16_t popNwkLiteMic_t;
typedef uint32_t popNwkAESCounter_t;
typedef uint32_t popNwkAESMic_t;
typedef uint32_t popLargeCounter_t;
typedef int32_t  popIntLargeCounter_t;
typedef uint32_t popAESPrime_t;
typedef uint8_t   popLitePrime_t;
#if gPopNeedPack_c
#pragma pack(1)
#endif
// the secure header frame (OTA) (4 bytes).
typedef struct _sNwkLiteAuxFrame_t
{
  // The unique per transmition tag used to do the packet intergity check.
  popNwkLiteMic_t      iMic;

  // The unique per transaction secure counter.
  popNwkLiteCounter_t  iCounter;
}sNwkLiteAuxFrame_t;

// the secure header frame (OTA)(16 bytes).
typedef struct _sNwkAESAuxFrame_t
{
  // The unique per transmition tag used to do the packet intergity check.
  popNwkAESMic_t      iMic;

  // The unique per transaction secure counter.
  popNwkAESCounter_t  iCounter;

  aPopMacAddr_t       aIeeeAddr;
}sNwkAESAuxFrame_t;
#if gPopNeedPack_c
#pragma pack()
#endif
//
typedef union _sNwkAuxFrame_t
{
  sNwkAESAuxFrame_t   sAes;
  sNwkLiteAuxFrame_t  sLite;
} sNwkAuxFrame_t;

typedef struct _sNwkAESNonce_t
{
  aPopMacAddr_t       aIeeeAddr;
  popNwkAESCounter_t  iCounter;
  popAESPrime_t       iPrime;
}sNwkAESNonce_t;

// The Neighbor Table type.
typedef struct _sNwkNeighborTable_t
{
  uint8_t             iAge;
  popNwkAddr_t       iNwkAddr;
  popLargeCounter_t  iCounter;
}sNwkNeighborTable_t;

// Sets the given parameter as the current network status.
#define PopNwkSetNetworkState(status)        (giPopNetworkState = ((popStatus_t)(status)))
#define gPopNwkScanForNoiseTimer_c    0x07
typedef uint8_t popNwkState_t;
extern popNwkState_t giPopNwkState;


/**************** PopNet Ota Upgrades Interface **************/
// To identify the source of an upgrade request either OTA or over the serial port
#define gPopOtaUpgradeReqSrcOTA_c 0x00 /* upgrade comes OTA */
#define gPopOtaUpgradeReqSrcOTS_c 0x01  /* upgrade comes over the serial port */
#define PopIsUpgradeReqSrcOTA(source) (source == gPopOtaUpgradeReqSrcOTA_c)

// Maximum possible payload over the air without security
#define gPopImageBlockSize_c 0x6C /* 108 bytes of block to have a maximum of 127 */
// Maximum possible jitter for complete response aprox. broadcast timeout
#define gPopOtaUpgradeCompleteRspJitter_c 7000
// Ota upgrade timers see other timer in PopNwk.h
/*
  Platform definitions for OTA upgrades. The reseaon of this is to avoid kill the board with
  a wrong download image to it.
*/
typedef enum OtaUpgrade_PlatfDef_tag
{
  ePopHwTypeHCS08,
  ePopHwTypeArm7
}OtaUpgrade_PlatfDef_t;

// State machine for application retries definitions
typedef uint8_t PopOtaUpgradeState_t;
extern PopOtaUpgradeState_t gOtaUpgradeState;
#define PopSetOtaUpgradeState(state) (gOtaUpgradeState = state)
#define PopGetOtaUpgradeState()  gOtaUpgradeState
enum
{
  ePopOtaGotBlockFromSerial,
  ePopOtaReceivedRsp,
  ePopOtaWaitForThrottle,
  // it is very important not to change the order of the retries and wait
  // for response,the state machine counts on them to be subsequent values
  // and that at the end
  ePopOtaWaitForRsp,
  ePopOtaFirstRetry,
  ePopOtaLastRetry,
  ePopOtaWaitForCompleteRsp,
  ePopOtaIdle
};
// Maximum time to wait for an upgrade response before retrying
#define PopOtaWaitForRspTimeout 4000

// pack the structure
#if gPopNeedPack_c
#pragma pack(1)
#endif
// over the air upgrades types
typedef uint8_t	popOtauNvmOpts_t;
enum
{
  ePopOtauNvmOptsDiscardNvm,
  ePopOtauNvmOptsPreserveNvm,
};

/* popOtauCmdId_t for  ota upgrade commands
 gPopGwCmdOtaUpgradePassThruReq_c             0x4e // send OTA formated pieces of an image
 gPopGwCmdOtaUpgradeFirstPassThruReq_c        0x4f // send first OTA formated pieces of an image
 gPopGwNwkMgmtOtaUpgradeFirstSaveReq_c        0x6f // requests the storage module to save the first piece of image
 gPopGwNwkMgmtOtaUpgradeSaveReq_c             0x70 // requests the storage module to save a piece of image
 gPopGwNwkMgmtOtaUpgradeSaveRsp_c             0x71 // response to the image storage request
 gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c  0x72 // requests a node to reboot and use a new image
 gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c  0x73 // response to the reboot request
 gPopGwNwkMgmtOtaUpgradeCancelReq_c           0x74 // indicates an OTA transmission is over
 gPopGwNwkMgmtOtaUpgradeCancelRsp_c           0x75 // indication of End of OTA upgrade when transmission mode is unicast
 gPopGwCmdNwkScanForNoise_c                   0x76 // scan for noises (builds list)
 gPopGwNwkMgmtOtaUpgradeCompleteReq_c         0x77 // requests a node to reboot and use a new image
 gPopGwNwkMgmtOtaUpgradeCompleteRsp_c         0x78 // requests a node to reboot and use a new image
 */
typedef uint8_t  popOtauCmdId_t;
// used to know when to send the last complete rsp to the host
#define popOtauAnyCompleteRsp   0
#define popOtauLastCompleteRsp  1

typedef uint8_t  popOtauChecksum_t;
typedef uint8_t  popOtauPlatform_t;
typedef uint8_t  popOtauHwId_t;
typedef uint16_t popOtauHandler_t;
typedef uint16_t popOtauBlocksCount_t;
typedef uint32_t popOtauImageSize_t;
typedef uint32_t popOtauGwErrInfo_t;

/*
 * actual save local request either coming from Gw or OTA
 * first and subsequent packets
 *
 */
typedef struct sPopOtauGwFirstBlockSaveReq_tag
{
  popOtauBlocksCount_t  blockNumber;
  popOtauHandler_t      handler;
  popOtauChecksum_t     checksum;
  popOtauPlatform_t     platform;
  popOtauImageSize_t    imageSize;
  popAppId_t            appId;
  popOtauHwId_t         hwId;
  uint8_t               blockSize;
  popOtauNvmOpts_t      preserveNvm;
  uint8_t               nvPages;
}sPopOtauGwFirstBlockSaveReq_t;

typedef struct sPopOtauGwSubseqBlockSaveReq_tag
{
  popOtauBlocksCount_t  blockNumber;
  popOtauHandler_t      handler;
  popOtauChecksum_t     checksum;
  uint8_t               imgBlock[1];
}sPopOtauGwSubseqBlockSaveReq_t;

/*
 * Pass thru ota upgrade save request from Pc to Gw
 * First and subsequent blocks
 */
typedef struct sPopOtauGwFirstBlockPassThruSaveReq_tag
{
  popNwkAddr_t          dstAddr;
  popTimeOut_t          throttle;
  popOtauBlocksCount_t  blockNumber;
  popOtauHandler_t      handler;
  popOtauChecksum_t     checksum;
  popOtauPlatform_t     platform;
  popOtauImageSize_t    imageSize;
  popAppId_t            appId;
  popOtauHwId_t         hwId;
  uint8_t               blockSize;
  popOtauNvmOpts_t      preserveNvm;
  uint8_t               nvPages;

}sPopOtauGwFirstBlockPassThruSaveReq_t;

typedef struct
{
  popNwkAddr_t          dstAddr;
  popTimeOut_t          throttle;
  popOtauBlocksCount_t  blockNumber;
  popOtauHandler_t      handler;
  popOtauChecksum_t     checksum;
  popOtauPlatform_t     platform;
  popOtauImageSize_t    imageSize;
  popAppId_t            appId;
  popOtauHwId_t         hwId;
  uint16_t               blockSize;
  popOtauNvmOpts_t      preserveNvm;
  uint8_t               nvPages;

}sPopOtauGwFirstBlockPassThruSaveInternalReq_t;

typedef struct sPopOtauGwSubseqBlockPassThruSaveReq_tag
{
  popNwkAddr_t          dstAddr;
  popTimeOut_t          throttle;
  popOtauBlocksCount_t  blockNumber;
  popOtauHandler_t      handler;
  popOtauChecksum_t     checksum;
  uint8_t               imgBlock[1];
}sPopOtauGwSubseqBlockPassThruSaveReq_t;

/*
 * Actual over-the-air save request
 * First and subsequent blocks
 */
typedef struct sPopOtauOtaFirstBlockPassThruSaveReq_tag
{
  popOtauCmdId_t        reqType;
  popOtauBlocksCount_t  blockNumber;
  popOtauHandler_t      handler;
  popOtauChecksum_t     checksum;
  popOtauPlatform_t     platform;
  popOtauImageSize_t    imageSize;
  popAppId_t            appId;
  popOtauHwId_t         hwId;
  uint8_t               blockSize;
  popOtauNvmOpts_t      preserveNvm;
  uint8_t               nvPages;
}sPopOtauOtaFirstBlockPassThruSaveReq_t;

typedef struct sPopOtauOtaSubseqBlockPassThruSaveReq_tag
{
  popOtauCmdId_t        reqType;
  popOtauBlocksCount_t  blockNumber;
  popOtauHandler_t      handler;
  popOtauChecksum_t     checksum;
  uint8_t               imgBlock[1];
}sPopOtauOtaSubseqBlockPassThruSaveReq_t;


// actual payload of an ota upgrade req
typedef struct sPopOtaUpgradeReq_tag
{
  popOtauCmdId_t           reqType;
  popOtauBlocksCount_t     blockNumber;
  popOtauHandler_t         handler;
  popOtauChecksum_t        checksum;
  //sPopOtaUpgradeSaveReq_t saveOtaReq;
  uint8_t  imageBlock[gPopImageBlockSize_c];
}sPopOtaUpgradeReq_t;

// to return the correct information in case of an error in Platform, HwId or AppId.
typedef union uPopOtaUpgradeErrInfo_tag
{
  popOtauPlatform_t   platform;
  popOtauHwId_t       hwId;
  uint8_t             nvPages;
  popAppId_t          appId;
  popOtauImageSize_t  availableMemory;
  popOtauBlocksCount_t  blockNumber;
}uPopOtaUpgradeErrInfo_t;

// to return a type of error and the correct value that should be used
 typedef struct sPopOtaUpgradeErr_tag
{
  popErr_t                errType;
  uPopOtaUpgradeErrInfo_t errInfo;
}sPopOtaUpgradeErr_t;

// actual payload of an ota upgrade rsp
typedef struct sPopOtaUpgradeRsp_tag
{
  popOtauCmdId_t        rspType;
  popOtauBlocksCount_t  blockNumber;
  sPopOtaUpgradeErr_t   otaUpgradeError;
}sPopOtaUpgradeRsp_t;

// used to pass a received ota upgrade rsp through the gateway to the pc app
typedef struct sPopOtaUpgradeRspThruSerial_tag
{
  popOtauCmdId_t      rspType;
  popErr_t            errType;
  popOtauGwErrInfo_t  errInfo;
}sPopOtaUpgradeRspThruSerial_t;

typedef struct sPopOtaUpgradeResetRsp_tag
{
  popOtauCmdId_t  rspType;
  popErr_t        errType;
}sPopOtaUpgradeResetRsp_t;

// Serial Port reset to new image request
typedef struct sPopGwResetToNewImageReq_tag
{
  popOtauCmdId_t    reqType;
  popOtauHandler_t  handler;
  popAppId_t        appId;
  popOtauHwId_t     hwId;
  popOtauPlatform_t platform;
  popNwkAddr_t      dstAddr;
}sPopGwResetToNewImageReq_t;

// Over The Air reset to new image request
typedef struct sPopOtaResetToNewImageReq_tag
{
  popOtauCmdId_t    reqType;
  popOtauHandler_t  handler;
  popAppId_t        appId;
  popOtauHwId_t     hwId;
  popOtauPlatform_t platform;
}sPopOtaResetToNewImageReq_t;

// Serial Port Ota Upgrade Complete request
typedef struct sPopGwUpgradeCompleteReq_tag
{
  popOtauHandler_t  handler;
  popNwkAddr_t      dstAddr;
}sPopGwUpgradeCompleteReq_t;

// Over the serial port Ota Upgrade Complete rsp
typedef struct sPopGwUpgradeCompleteRsp_tag
{
  popOtauHandler_t      handler;
  popOtauBlocksCount_t  blockNumber;
  popNwkAddr_t          dstAddr;
}sPopGwUpgradeCompleteRsp_t;

// Over the air ota upgrade complete req
typedef struct sPopOtaUpgradeCompleteReq_tag
{
  popOtauCmdId_t    reqType;
  popOtauHandler_t  handler;
}sPopOtaUpgradeCompleteReq_t;

// Over the air otaupgrade complete rsp
typedef struct sPopOtaUpgradeCompleteRsp_tag
{
  popOtauCmdId_t        reqType;
  popOtauHandler_t      handler;
  popOtauBlocksCount_t  blockNumber;
}sPopOtaUpgradeCompleteRsp_t;

// available information of the current download
typedef struct sPopOtaUpgradeImageInfo_tag
{
  popOtauHandler_t      handler;
  popOtauChecksum_t     imageChecksum;
  popOtauPlatform_t     platform;
  popOtauImageSize_t    imageSize;
  popOtauBlocksCount_t  totalBlocks;
  popTimeOut_t          throttleMilliseconds;
  popOtauBlocksCount_t  blockNumber;
  popErr_t              txStatus;
  popAppId_t            appId;
  popOtauHwId_t         hwId;
  uint16_t              blockSize;
  popOtauNvmOpts_t      preserveNvm;
  uint8_t               nvPages;
  sPopOtaUpgradeReq_t   imageCopy;
}sPopOtaUpgradeImageInfo_t;

#if gPopNeedPack_c
#pragma pack()
#endif

// Prototypes
void PopOtaUpgradeSendRspToSerial(uint8_t rspOpcode, popErr_t errStatus, sPopOtaUpgradeErr_t* pErrorInfo);


/**************** PopNet Memory Interface ****************/

// standard memory interface
// see also specifics in BSP for popMemSize_t, etc....
void PopMemInit(void);      /* initialize memory subsystem (done by BSP task) */
void *PopMemAlloc(popMemSize_t iSize);  /* allocate memory */
popErr_t PopMemAllocAgain(void *pMemory);  /* incremement allocation count */
popErr_t PopMemRealloc(void *pMemory, popMemSize_t iSize);  /* shrink a block */
bool PopMemFree(void *pMemory);  /* free memory */
void PopMemFreeCompletely(void *pMemory);
void PopMemCpy(void *pDst, void *pSrc, uint16_t iLen);
void PopMemMove(void *pDst, void *pSrc, uint8_t iLen);
void PopMemSet(void *pBuffer, uint8_t iValue, uint8_t iLen);
void PopMemSetZero(void *pBuffer, uint8_t iLen);
void PopMemSetLarge(void * pBuffer, uint8_t iValue, uint16_t iLen);
bool PopMemIsFilledWith(void *pBuffer, uint8_t iValue, uint8_t iLen);
uint8_t PopMemCmp(void *pBuffer1, void *pBuffer2, uint16_t iLen);
void *PopMemChr(void *pBuffer, uint8_t iValue, uint8_t iLen);
uint8_t PopStrLen(char *s);

// the largest memory block possible when calling PopMemAlloc()
// (assuming the heap has this much room left)
#define gPopMemMaxAlloc_c   250

// routines for debugging memory leaks, etc...

// check the integrity of the heap right now. *pFree returns total free bytes,
// *pBiggestFree returns largest block size. Pass NULL for parameters to not
// return that field.
// Returns offset of error if the heap is corrupted. Returns gPopMemHeapOk_c if all is OK.
popHeapSize_t PopMemHeapCheck(popHeapSize_t *pFree, popHeapSize_t *pBiggestFree);
popHeapSize_t PopMemFreeThemAll(void);
#define gPopMemHeapOk_c   0xffff

#ifdef gPopAssertLevel_c
extern popHeapSize_t iHeapFree;
extern popHeapSize_t iHeapLowestFree;
extern uint16_t iPopMemNoAllocCount;
void DebugHeapCheck(void);               /* will fire an assert if heap is corrupted */
#define PopDebugHeapFree()    iHeapFree        /* bytes free now */
#define PopDebugHeapMinFree() iHeapLowestFree  /* bytes free in the worst case so far */
#else
#define PopDebugHeapCheck()
#define PopDebugHeapFree()
#define PopDebugHeapMinFree()
#endif

/********** Flash Memory Storage Interface ***************/
typedef uint8_t popNvId_t;
#define gPopNvUserId_c      0x80    // user IDs start at 0x80 (to 0xff). PopNet reserves IDs 0x00-0x7f for system NV items
#define gPopNvInvalidId_c   0xff    // ID 0xff is reserved
#define gPopNvMaxDataLen_c  251     // maximum length of a single data item to store in NVM

#if gPopNumberOfNvPages_c

// NVM enabled, include public interface
popErr_t PopNvStore(popNvId_t iItemId, uint8_t iLen, void *pData);
popErr_t PopNvRetrieve(popNvId_t iItemId, void *pData);
bool PopNvEnoughRoom(uint8_t iLen);
void PopNvGarbageCollect(void);
popErr_t PopNvStoreNwkData(void);
popErr_t PopNvRetrieveNwkData(void);
popErr_t PopNvStoreSecureCounterData(void);
popErr_t PopNvRetrieveSecureCounterData(void);


#else

// stub routine prototypes
popErr_t PopNvStoreStub(popNvId_t iItemId, uint8_t iLen, void *pData);
popErr_t PopNvRetrieveStub(popNvId_t iItemId, void *pData);
bool PopNvEnoughRoomStub(uint8_t iLen);
popErr_t PopNvStoreNwkDataStub(void);
popErr_t PopNvRetrieveNwkDataStub(void);
popErr_t PopNvStoreSecureCounterDataStub(void);
popErr_t PopNvRetrieveSecureCounterDataStub(void);

// define stubs for these routines if NVM disabled, so app code will still compile
#define PopNvStore(iItemId, iLen, pData)  PopNvStoreStub(iItemId, iLen, pData)
#define PopNvRetrieve(iItemId, pData)     PopNvRetrieveStub(iItemId, pData)
#define PopNvEnoughRoom(iLen)             PopNvEnoughRoomStub(iLen)
#define PopNvStoreNwkData()               PopNvStoreNwkDataStub()
#define PopNvRetrieveNwkData()            PopNvRetrieveNwkDataStub()
#define PopNvStoreSecureCounterData()     PopNvStoreSecureCounterDataStub()
#define PopNvRetrieveSecureCounterData()  PopNvRetrieveSecureCounterDataStub()

#endif



/*************** PopNet Serial Interface (aka UART or SCI) ********************/

// baud rates to send to PopSerialSettings()
typedef uint8_t gPopBaudRate_t;
#define gPopBaud2400_c      0
#define gPopBaud4800_c      1
#define gPopBaud9600_c      2
#define gPopBaud19200_c     3
#define gPopBaud38400_c     4
#define gPopBaud57600_c     5
#define gPopBaud115200_c    6

// <SJS: Added Serial Tx Rx State Machine>
typedef uint8_t popSerialSendFlags_t;
#define gPopSerialSendFlags_Normal_c          0x00    // buffer will not be freed when TxDone
#define gPopSerialSendFlags_FreeOnTxDone_c    0x01    // buffer will be freed when TxDone

#if gPopSerial_d
popErr_t PopSerialGet1Byte(void *pByte);
#else
#define PopSerialReceive(piLen, pBuffer) gPopErrEmpty_c
#endif
// </SJS:>

#if gPopSerial_d
popErr_t PopSerialSend( uint16_t iLen, void *pBuffer, popSerialSendFlags_t iFlags);
popErr_t PopSerialReceive(uint16_t *piLen, void *pBuffer);
bool PopSerialDoneTransmitting(void);
uint8_t PopSerialGetLength(void);
popErr_t PopSerialSettings(gPopBaudRate_t iBaudRate, uint16_t iMinGapMs );
void PopSerialDebugStr(char *s);
void PopSerialDebugHex(uint8_t iValue);
#else
#define PopSerialSend(iLen, pBuffer) gPopErrNone_c
#define PopSerialReceive(piLen, pBuffer) gPopErrEmpty_c
#define PopSerialGetLength(void) 0
#define PopSerialSettings(iBaudRate, iMinGapMs )
#endif

#if (gPopSerial_d && gPopAssertLevel_c)
 #define PopDebugStr(s) PopSerialDebugStr(s)
 #define PopDebugHex(s) PopSerialDebugHex(s)
#else
 #define PopDebugStr(s)
 #define PopDebugHex(s)
#endif

/**************** Lower Power Interface ****************/
// to place system into low-power mode
typedef uint32_t  popPwrTimeOut_t;    // timeout in ticks (defaults to 1 second ticks in PopBsp.c)
typedef uint8_t    popPwrWakeUpOn_t;
#define gPopPwrWakeUpOnKbd_c    0x01
#define gPopPwrWakeUpOnTime_c   0x02
#define gPopPwrWakeUpOnBoth_c   0x03

// this sets a node into "sleepy" mode, which doesn't route and doesn't repeat
// externally received broadcasts. Turn this mode on if the node will typically
// be in low power, waking up for brief periods of time.
void PopNwkSetRoutingSleepingMode(bool fOn);
#define PopNwkIsSleepyNode()       (gsPopNwkData.fSleepyMode)

// will wake up on the event
#if gPopLowPower_d
popErr_t PopPwrSleep(popPwrWakeUpOn_t iWakeUpOn, popPwrTimeOut_t iTimeOutMs);
#else
#define PopPwrSleep(iWakeUpOn, iTimeOutMs) gPopErrNone_c
#endif

#if gPopLowVoltageDetection_d
bool PopIsThereALowVoltageWarning(void);
#else
#define PopIsThereALowVoltageWarning()  gFalse_c
#endif

/******************** Instrumentation Types ********************/
typedef uint8_t popTableFullMask_t;
#define gPopRetryTableFull_c                    (1<<0)
#define gPopDuplicateTableFull_c                (1<<1)
#define gPopRouteDiscoveriesTableFull_c         (1<<2)
#define gPopTimersTableFull_c                   (1<<3)

/************ Helper Function Interface *******************/
uint8_t PopSprintf(char *szDst, char *szFmt, ...);  // simple sprintf()
void PopHex2Ascii(char *psz, uint16_t iHex);  // 4 digit #s only
void PopInt2Ascii(char *psz, int_fast8_t iInt);       // 2 digit #s only
void PopIToA(char *szValue, uint16_t iValue, uint8_t iRadix); // convert an integer to ascii or hex
void PopReverseDigits(char *psz);   // reverse all bytes in string
bool PopIsDigit(char c);            // is this character a decimal digit?
void PopBitOr(uint8_t *pArray, uint8_t iBit);       // or a bit in a multi-byte bit array
void PopBitAnd(uint8_t *pArray, uint8_t iBit);      // and a bit in a multi-byte bit array
bool PopIsZeros(uint8_t *pArray, uint8_t iSize); // is the entire bit array empty (size in bytes)?


/************* Task & Event Interface ****************/

typedef uint8_t popTaskId_t;

// PopNet tasks
extern const popTaskId_t cBspTaskId;
extern const popTaskId_t cPhyTaskId;
extern const popTaskId_t cNwkTaskId;
extern const popTaskId_t cSerialTaskId;
extern const popTaskId_t cAppTaskId;
extern const popTaskId_t cMessageTaskId;

#if gUseMpSerial_d
extern const popTaskId_t cMpSerialTaskId;
#endif

// popnet application events (sent from PopNet to the application)
// note: each task (NWK, BSP, Serial, etc...) has it's own list of events that may overlap these event IDs
typedef uint8_t popEvtId_t;

// user defined application events 0x80-0xFF are available to the application
#define gPopEvtUser_c              0x80    // 0x80-0xFF availalbe for application specific events

// BSP and Kernel events (0x00 - 0x1f)
#define gPopEvtNull_c                     0x00  // krn: null event (idle), sent only to BSP task
#define gPopEvtTimer_c                    0x01  // bsp: timer has expired (see sEvtData.iTimerId)
#define gPopEvtKey_c                      0x02  // bsp: a key has been pressed (see sEvtData.iKeyId)
#define gPopEvtSerialDataReady_c          0x04  // bsp: serial data coming in on port (no data)
#define gPopEvtSerialTxDone_c             0x05  // bsp: transmit of last packet complete (no data)
#define gPopEvtNvmDataChanged_c           0x07  // Nvm: Indicates that the data saved on Nvm has changed (see sEvtData.iNvmChanged)
#define gPopEvtLowVoltageDetected_c       0x08  // lvd: Announce that there has been a low voltage detected (no data)
#define gPopEvtPwrWakeUp_c                0x09  // pwr: Announce that the node has waked from low power (see sEvtData.iWakeupReason)
#define gPopEvtTableFull_c                0x0a  // krn: for example, the route discovery table has filled

#define gPopEvtSnifferData_c              0x11    // packet from Sniffer to PC
#define gPopEvtPhyIndication_c            0x12  // phy: data has come in on the radio (PHY packet, may be MAC or PopNet)
#define gPopEvtEnableSniffer_c            0x13  // enable/disable the sniffer

// events from network (0x20 - 0x3f)
#define gPopEvtDataIndication_c           0x20  // nwk: a message has come in from over-the-air
#define gPopEvtDataConfirm_c              0x21  // nwk: data confirm means data req has left this radio
#define gPopEvtDataPassThru_c             0x22  // nwk: data is just passing through (being routed). Does not include broadcasts.
#define gPopEvtNwkScanConfirm_c           0x24  // nwk: results of a PopNwkScanForNetworks()
#define gPopEvtNwkBeaconIndication_c      0x25  // nwk: add one beacon from the scan
#define gPopEvtNwkJoinIndication_c        0x26  // nwk: a node wants to join this node
#define gPopEvtNwkStartConfirm_c          0x27  // nwk: start, form or join confirm.
#define gPopEvtNwkStart_c                 0x28  // nwk: (not received in app, send to NWK layer to form/join network)
#define gPopEvtNwkLeaveConfirm_c          0x29  // nwk: have left the network
#define gPopEvtNwkAddrPoolChanged_c    0x2a  // address pool has changed (sEvtData.iAddrCount)
#define gPopEvtNwkJoinEnable_c            0x2b  // nwk: informs the app if join enabled has changed
#define gPopEvtNwkInitiateRoute_c         0x2d  // nwk: a route discovery has started
#define gPopEvtNwkRouteConfirm_c          0x2e  // nwk: the route request is complete: indicate success or failure
#define gPopEvtNwkRoutePassThru_c         0x2f  // nwk: route reply is passing through (determining route)
#define gPopEvtNwkFindNodeConfirm_c       0x30  // nwk: a node has responded to find node
#define gPopEvtNwkMatchedFindNode_c       0x31  // nwk: this node recieved a successful find node
#define gPopEvtNwkScanConfirmError_c      0x33  // nwk: (internal, not received in app, gateway status
#define gPopEvtNwkNextStartState_c        0x35  // nwk: (internal, not received in app) goes to next start network state
#define gPopEvtNwkAddrPoolConfirm_c       0x36  // Address pool confirm has arrived (sEvtData.iAddrCount)
#define gPopEvtNwkTxPowerConfirm_c        0x39  // Power now at new setting (sEvtData.iStatus)

//
// Event IDs 0x40 - 0x7f are reserved by the gateway commands.
//
#define gPopGwCmdGetPopNetVersion_c          0x40 // get version of PopNet
#define gPopGwCmdCompleted_c                 0x41 // the command was completed
#define gPopGwCmdSetLed_c                    0x42 // set LED(s) on or off
#define gPopGwCmdPop_c                       0x43 // beep the speaker
#define gPopGwCmdLcdWriteString_c            0x44 // write to the LCD screen
#define gPopEvtAssert_c                      0x45 // something asserted
#define gPopEvtMemoryAlloc_c                 0x46 // a memory allocation occurred (assert level 2 only)
#define gPopEvtMemoryFree_c                  0x47 // memory was freed (assert level 2 only)
#define gPopEvtMemoryAllocFailure_c          0x48 // a memory alloc failed
#define gPopGwCmdSetEvtRange_c               0x49 // limit reporting range in gateway
#define gPopGwCmdHeapCheck_c                 0x4a // check the heap
#define gPopGwCmdPopNvStore_c                0x4b // call PopNvStoreNwkData
#define gPopGwCmdPopNvRetrieve_c             0x4c // call PopNvRetrieveNwkData
#define gPopGwCmdPopNvForget_c               0x4d // forget all NVM data
#define gPopGwCmdOtaUpgradePassThruReq_c     0x4e // send OTA formated pieces of an image
#define gPopGwCmdOtaUpgradeFirstPassThruReq_c  0x4f // send first OTA formated pieces of an image

// gateway commands handled immediately (no events sent to app)
#define gPopGwCmdInitNwkValues_c             0x50 // initialize channel, etc...
#define gPopGwCmdGetMaxPayload_c             0x51 // return max data request payload
#define gPopGwCmdSetChannel_c                0x52 // set channel alone
#define gPopGwCmdSetChannelList_c            0x53 // set channel list
#define gPopGwCmdSetPanId_c                  0x54 // set pan ID
#define gPopGwCmdSetNwkAddr_c                0x55 // set NWK addr
#define gPopGwCmdSetMacAddr_c                0x56 // set MAC addr
#define gPopGwCmdSetManufactureId_c          0x57 // set Mfg ID
#define gPopGwCmdSetScanOptions_c            0x58 // set scan options (affects start network)
#define gPopGwCmdSetScanTime_c               0x59 // set scan time (affects start network)
#define gPopGwCmdSetJoinOptions_c            0x5a // set join options (affects start network)
#define gPopGwCmdSetDefaultAddressPoolSize_c 0x5b // set default pool size (affects start network)
#define gPopGwCmdSetNetworkKey_c             0x5c // set 16-byte network key
#define gPopGwCmdSetICanHearYouTable_c       0x5d // set i-can-hear-you table
#define gPopGwCmdSetSleepyMode_c             0x5e // set sleepy mode on/off
#define gPopGwCmdGetInitNwkValues_c          0x5f // return the global nwk data.

// gateway commands that initiate network commands
#define gPopGwCmdNwkStartNetwork_c           0x60 // start network
#define gPopGwCmdNwkFormNetwork_c            0x61 // form network
#define gPopGwCmdNwkJoinNetwork_c            0x62 // join network
#define gPopGwCmdNwkScanNetworks_c           0x63 // scan for networks (builds list)
#define gPopGwCmdNwkScanComplete_c           0x64 // scan complete (when done with above)
#define gPopGwCmdNwkAddrRequest_c            0x65 // request more addresses from server
#define gPopGwCmdSendBeacon_c                0x66 // send out a beacon (only when on network)
#define gPopGwCmdBeaconRequest_c             0x67 // send beacon request
#define gPopGwCmdNwkLeaveNetwork_c           0x68 // leave network
#define gPopGwCmdNwkDataRequest_c            0x69 // data request

// gateway commands that read the instrumentation data (no events sent to the app)
#define gPopGwCmdGetHighWaterMarks_c           0x6a // returns the # of events left at high water mark.
#define gPopGwCmdGetJoinStatus_c               0x6b // retunrs if the join capacity is enable or not.
#define gPopGwCmdGetSecurityKey_c              0x6c // returns the current security key in use.
#define gPopGwCmdGetRoutingSleepingMode_c      0x6d // returns the current status of the sleeping mode.
#define gPopGwCmdGetNetworkStatus_c            0x6e // return TRUE is the node is on the network FALSE other wise.

// OTA upgrade commands
#define gPopGwNwkMgmtOtaUpgradeFirstSaveReq_c       0x6f // requests the storage module to save the first piece of image
#define gPopGwNwkMgmtOtaUpgradeSaveReq_c            0x70 // requests the storage module to save a piece of image
#define gPopGwNwkMgmtOtaUpgradeSaveRsp_c            0x71 // response to the image storage request
#define gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c 0x72 // requests a node to reboot and use a new image
#define gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c 0x73 // response to the reboot request
#define gPopGwNwkMgmtOtaUpgradeCancelReq_c          0x74 // indicates an OTA transmission is over
#define gPopGwNwkMgmtOtaUpgradeCancelRsp_c          0x75 // indication of End of OTA upgrade when transmission mode is unicast
#define gPopGwCmdNwkScanForNoise_c                  0x76 // scan for noises (builds list)
#define gPopGwNwkMgmtOtaUpgradeCompleteReq_c        0x77 // requests a node to reboot and use a new image
#define gPopGwNwkMgmtOtaUpgradeCompleteRsp_c        0x78 // requests a node to reboot and use a new image

// common application in the gateway events
#define gPopEvtNwkNodePlacement_c            0x7a
#define gPopGwCmdSetApplicationId_c          0x7b   // set the id of the current application.
#define gPopGwCmdGetApplicationId_c          0x7c   // gets the id of the current id.
#define gPopEvtAppBlink_c                              0x7d   // blink at application startup.

// set/get the address pool from the Gateway
#define gPopGwCmdSetAddrPool_c      0x7e
#define gPopGwCmdGetAddrPool_c      0x7f

#define gPopTimerIdMemAllocFail_c           0xF8

#if DEBUG >= 1
// SJS: Special debugging event
#define gPopAppAssertEvt_c      0xFE
#define gPopAppCountEvt_c       0xFF
#endif //DEBUG >= 1

// Message events
#define gPopMessageReceived_c               0x00 // A serial message has been received
#define gPopMessageSent_c                   0x01 // A serial message has been sent
#define gPopMessageRadioReceived_c          0x02 // A radio message has been received

/* set the range for events that are watched by the Gateway */
#if gPopGateway_d && gPopSerial_d
void PopGatewaySetEventRange(uint8_t iLow, uint8_t iHigh);
#else
#define PopGatewaySetEventRange(iLow, iHigh)
#endif

// This union defines all the event data types that can be sent to the application
typedef union sPopEvtData_tag
{
  uint8_t                     iByte;             // gPopEvtUser_c - generic byte
  uint16_t                    iWord;             // gPopEvtUser_c - generic 16-bit word
  uint8_t                     iIndex;            // pPopevtUser_c - generic index
  void                        *pData;            // gPopEvtUser_c - generic pointer
  popErr_t                    iStatus;           // generic status - used mainly by gateway
  popTimerId_t                iTimerId;          // gPopEvtTimer_c
  popKeyId_t                  iKeyId;            // gPopEvtKey_c
  popAddrCount_t              iAddrCount;     // gPopEvtNwkAddrPoolChanged_c, gPopEvtNwkAddrPoolConfirm_c
  popNvmDataChangedBitMask_t  iNvmChanged;       // gPopEvtNvmDataChanged_c
  popWakeUpReason_t           iWakeupReason;     // gPopEvtPwrWakeUp_c - used for low power
  popErr_t                    iDataConfirm;      // gPopEvtDataConfirm_c
  popNwkAddr_t                iRouteConfirm;     // next hop address or 0xfffe if failed
  uint8_t                      iPwrWakeUpReason;  // gPopEvtPwrWakeUp_c
  popStatus_t                 iNwkStartConfirm;  // gPopNwkStatusJoin
  popStatus_t                 iNwkState;         // Network state
  bool                      iNwkJoinEnabled;   // gPopEvtNwkJoinEnable_c - TRUE if enabled, FALSE if not
  popNwkAddr_t                iNwkAddr;          // gPopEvtNwkDataPassThru_c, gPopEvtNwkRoutePassThru_c, gPopEvtNwkFindNodeConfirm_c
  popAssertCode_t             iAssertId;              // The Assert Id code to be reported.
  popTableFullMask_t          iTableFullMask;         // gPopEvtTableFull_c - Mask with over run bits.
  sPopNwkScanForNetworks_t    *pNwkScanConfirm;       // gPopEvtNwkScanConfirm_c
  sPopNwkScanForNoise_t       *pNwkScanForNoiseConfirm;  // gPopEvtNwkScanForNoiseConfirm_c
  sPopNwkJoinIndication_t     *pNwkJoinIndication;    // gPopEvtNwkJoinIndication_c
  sPopNwkBeaconIndication_t   *pNwkBeaconIndication;  //gPopEvtNwkBeaconIndication_c
  sPopNwkDataIndication_t     *pDataIndication;       // gPopEvtDataIndication_c
  sPopNodePlacement_t         *pPopNodePlacementResult;
  sPopOtaUpgradeErr_t         *pPopOtaUpgradeError;
  sPopGwUpgradeCompleteRsp_t  *pPopOtaUpgradeGwCompleteRsp;
} sPopEvtData_t;

// a single event in the event queue. note: event IDs are unique to task IDs: that is,
// task A can have event X, which may have different meaning than task B's event X
// event data is a union with all possible event data types. Small data (16-bits or less)
// is in the event itself. Larger data is returned via a pointer.
#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct sPopEvt_tag
{
  popTaskId_t   iTaskId;    // task
  popEvtId_t    iEvtId;     // event
  sPopEvtData_t sEvtData;   // event data (always size of a pointer!)
} sPopEvt_t;
#if gPopNeedPack_c
#pragma pack()
#endif


/************* Task Interface ****************/

// task prototypes
typedef void (*pfnPopInitTask_t)(void);
typedef void (*pfnPopEventLoop_t)(popEvtId_t iEvtId, sPopEvtData_t sEvtData);

// default handler for application event loop.
// allows for default behavior for joining/forming networks, etc...
// and for freeing messages.
void PopAppDefaultHandler(popEvtId_t iEvtId, sPopEvtData_t sEvtData);


#define gPopTaskSignature_c 'T'

// each task has one of these structures in RAM for run-time
typedef struct sPopTask_tag
{
  pfnPopEventLoop_t  pfnEventLoop;  /* ptr to event handling loop for task */
  uint8_t      iSignature;           /* signature for a task 'T' */
  uint8_t      iNumEvents;           /* # of events queued up for this task */
} sPopTask_t;

// each application has a task list
typedef struct sPopTaskList_tag
{
  pfnPopInitTask_t  pfnInitTask;    /* ptr to init function for task */
  pfnPopEventLoop_t pfnEventLoop;   /* ptr to event handling loop for task */
} sPopTaskList_t;

// the current task (if interrupt handlers need to know)
extern popTaskId_t giPopThisTask;

// normal set events in routes. The no-data simply sets the data to NULL.
// only fails if event queue is full (or task does not exist)
popErr_t PopSetEvent(popEvtId_t iId, void *pData);
popErr_t PopSetEventNoData(popEvtId_t iId);

// set an event for another tak
popErr_t PopSetEventEx(popTaskId_t iTaskId, popEvtId_t iEvtId, void *pData);

// set an event only once (that is, if already in the event queue, don't set it again)
popErr_t PopSetUniqueEventEx(popTaskId_t iTaskId, popEvtId_t iEvtId,void *pData);

/*
  PopSetTableFullEvent

  Only add the event to the application if not already in the queue. If the event
  already exist in the event queue, then, set the bit for specific over run.
*/
popErr_t PopSetTableFullEvent(popTableFullMask_t iMask);


// set an event for application task
popErr_t PopSetAppEvent(popEvtId_t iId, void *pData);

// Set an event for message task
popErr_t PopSetMessageEvent(popEvtId_t iId, void *pData);

// sends to either the NWK or APP task (code size reduction function... used internally).
popErr_t PopSetEventTo(bool isNwkData, popEvtId_t iId, void *pData);

// find first occurrence of the event ID in the event queue
uint8_t PopFindEvent(popTaskId_t iTaskId, popEvtId_t iEvtId);

// remove the particular event found by PopFindEvent. Call immediately after PopFindEvent.
void PopRemoveEvent(uint8_t i);


/******************** Timer Interface ********************/

// total # of soft timers in the system
extern const uint8_t cPopMaxTimers;

// an array of soft timers share a single HW timer
#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct _tagPopTimer
{
  popTimerId_t  iTimerId;    // ID==0 is a free timer
  popTaskId_t   iTaskId;    // 0=HAL, 1=app
  popTimeOut_t  iTimeLeft;  // time (in ms) left for the timer
} sPopTimer_t;
#if gPopNeedPack_c
#pragma pack()
#endif



/******************** Network Interface ********************/

/*
  PopNwkFormnetwork

  This routine assumes that any PAN ID conflicts have been resolved and does not
  scan for beacons over-the-air.

  Any PopNet node may issue this function prior to forming or joining a network.
  Once on a network, this routine will return an error code if an application
  attempts to form again.

  Normally when forming a network, a PopNet node will issue a beacon (just as if
  it received a MAC beacon request). This helps to identify the forming PopNet
  network.

  The channel must be in the range 11-26. The iPanId and iNwkAddr must both be
  in the range 0x0000 � 0xffef.

  IN: fOptions is the flag that indicates if the fourming process should be announce.
  IN: iChannel is the physical channel were the network wil be form.
  IN: iPanId is the Personal area network Identifyer.

  The fucntion does not returns any value.
*/
void PopNwkFormNetwork
(
  popNwkJoinOpts_t iOptions,  // The flag to indicates if the forming should be announced.
  popChannel_t iChannel,      // The channel where to form the network (11-26)
  popPanId_t iPanId           // The Pan Id where to communicate during the active life of the node.
);

/*
  PopNwkJoinNetwork

  Join a network.
  The join

  The success or failre will be returned through the event gPopEvtNwkStartConfirm_c
*/
void PopNwkJoinNetwork
(
  popNwkJoinOpts_t  iOptions, // in: options
  popChannel_t      iChannel, // in: channel, 0x00 = use global channel list, see PopNwkSetChannelList()
  popPanId_t        iPanId,   // in: PAN identifier, 0xffff = any PAN ID
  popNwkAddr_t      iNwkAddr  // in: address of node to join, 0xffff = any parent will do
);

/*
  PopNwkStartNetwork

  This function can be called from the application, the propouse of this function
  is form a network, join a network or start quiet the wetwork.

  Receives no parameters, relays on the globals and responds any of the previously
  define error codes.

  Returns an error code.
*/
void PopNwkStartNetwork( void );

/* help function. Pass the status from the gPopEvtStartConfirm_c as the parameter. */
bool PopNwkFormJoinSuccess(popStatus_t iStatus);

/*

  PopNwkScanForNetworks() scans for beacons on a list of channels. The beacon indicates
  which network other nodes are on. Any 802.15.4 node in hearing range will respond with
  a beacon if it hears the active scan (also called a beacon request).

  The beacons are returned collectively in the gPopEvtNwkScanConfirm_c. The structure
  typed returned in the event is sPopNwkScanForNetworks_t, field pNwkScanConfirm. See the
  PopNet Developer's Guide for more information.

  The maximum # of beacons returned in the scan confirm depend on giMaxSizeDiscoveryTable.

  The event gPopEvtNwkScanConfirm_c contains a pointer to the
*/
popErr_t PopNwkScanForNetworks
(
  popChannelList_t iChannelList,
  popScanOption_t  iOptions,
  popTimeOut_t iTimeForEachScanMs
);

/*
  PopNwkScanForNetworks() allocates memory.

  If using PopNwkScanForNetworks(), and NOT calling the PopDefaultHandler(), make sure to call
  PopNwkScanComplete() to free up the memory allocated by the scan. The PopDefaultHandler() does
  this automatically on the gPopEvtNwkScanConfirm_c.
*/
void PopNwkScanComplete(void);

/*
  Returns an index into the scan table of a suitable parent. This checks join enabled, PAN ID, and
  MfgId.

  Returns
    gPopStatusJoinSuccess_c         Found a suitable parent to join (but haven't joined yet)
    gPopStatusJoinDisabled_c        Join disabled on all suitable parents
    gPopStatusWrongMfgId_c          Wrong Mfg ID on all suitable parents
    gPopStatusPanAlreadyExists_c    Same PAN ID exists on another protocol (could be ZigBee, etc...)
    gPopStatusNoParent_c            No suitable parent found

  See the sEvtData.pNwkScanConfirm field on the gPopEvtNwkScanConfirm_c.
*/
popStatus_t PopNwkFindSuitableParentToJoin(uint8_t *piJoinIndex);

/* checks the global scan table, and returns a random PAN ID that does not conflict */
popPanId_t PopNwkGetNoConflictRandomPanId(void);

/* checks the global scan table for a conflict with the given PAN ID */
bool PopNwkCheckForPanIdConflict(popPanId_t iPanId);

/* find the best channel. Assumes that PopNwkScanForNetworks() and PopNwkScanForNoise() have been called and not yet freed. */
popChannel_t PopNwkFindBestChannel(void);



/*
  Scan for noise on a given set of channels. Note: this is a blocking function.
  It will not return until complete... Will set all unscanned channels to 0x00 in list.
*/
sPopNwkScanForNoise_t * PopNwkScanForNoise(popChannelList_t iChannelList);
void PopNwkBeaconRequest( popChannel_t channel );
void PopNwkSendBeacon( void );
void PopNwkProcessBeaconIndication(sPopNwkBeaconIndication_t *pNwkBeaconIndication);

/* Returns TRUE if the given channel Id is between 11 and 26, FALSE otehrwise. */
extern bool PopNwkIsValidChannel(popChannel_t iChannel);

/* Returns TRUE if the given address is not a reserved one or broadcast, FALSE otherwise. */
extern bool PopNwkIsValidNwkAddress(popNwkAddr_t iAddress);

/************* Security Interface *************/
void PopNwkSecureData(sPopNwkDataIndication_t *pOtaFrame);
void PopNwkLiteSecureData(sPopNwkDataIndication_t *pOtaFrame);
void PopNwkAESSecureData(sPopNwkDataIndication_t *pOtaFrame);

bool PopNwkRemoveSecurity(sPopNwkDataIndication_t *pOtaFrame);
bool PopNwkLiteRemoveSecurity(sPopNwkDataIndication_t *pOtaFrame);
bool PopNwkAESRemoveSecurity(sPopNwkDataIndication_t *pOtaFrame);

// Security routines.
bool PopNwkStalenessCheck(sPopNwkDataIndication_t *pIndication);
bool PopNwkCheckNeighborTable(popNwkAddr_t iMacDstAddr, popNwkAddr_t iNwkAddr, popLargeCounter_t iCounter);
#if (gPopSecurityLevel_c >= gPopNwkAESSecurity_c)
void ExpandKey(void *pKey);
#else
#define ExpandKey(pKey)
#endif

void PopNwkSetSecurityKey(void *pKey);

/* for generating random jitters in the application */
uint8_t PopNwkJitter08(uint8_t iUpperLimit);
uint16_t PopNwkJitter16(uint16_t iUpperLimit);

/*
  PopNwkLeaveNetwork

  Gets the node off the network by chanching the current network state to be Off the network
  and by setting the node address to be an invalid address.

  Receives no values and returns nothing.
*/
void PopNwkLeaveNetwork(void);


/*
  PopNwkAddrPoolRequest

  Ask for a address or a set of address to another node with capacity for it. This could be any
  node in the network, but the default is node 0x0000. The default address server can be set using
  PopNwkSetAddressServer().

  Receives the destiantion address of the node where we will ask and the amount
  of addresses requested.

  gPopEvtNwkAddressPoolModified_c

*/
void PopNwkAddrPoolRequest(popNwkAddr_t iDstAddress, popAddrCount_t iCount);

// Find or set the NwkAddr of the address server (where this node will go to find more addresses).
// By default the address server is node 0x0000 (the node that formed the network).
#define PopNwkGetAddressServer()             (gsPopNwkData.iAddressServer)
#define PopNwkSetAddressServer(nwkAddr)      (gsPopNwkData.iAddressServer = (nwkAddr))

// Get or set the next available address in the local address pool
#define PopNwkGetNextAddrAvailable()         (gsPopNwkData.iNextAddr)
#define PopNwkSetNextAddrAvailable(address)  (gsPopNwkData.iNextAddr = ((popNwkAddr_t)(address)))

// Get or set the number of available addresses in the local address pool
#define PopNwkGetAddrCount()                 (gsPopNwkData.iAddrCount)
#define PopNwkSetAddrCount(count)            (gsPopNwkData.iAddrCount = ((popAddrCount_t)(count)))
#define PopNwkReduceAddrCount(count)         (gsPopNwkData.iAddrCount -= ((popAddrCount_t)(count)))

// Get or set the size of the request for addresses during joining a network
#define PopNwkGetAddrReqSize()               (gsPopNwkData.iAddrReqSize)
#define PopNwkSetAddrReqSize(value)          (gsPopNwkData.iAddrReqSize = (value))




/************************* Instrumentation *************************/
/* Needs to calculate SP_Starts and SP_WaterMark*/
#if !gPopBigEndian_c

/* The stack is filled from final to start.*/
extern const void * STACK_TOP; /* Points to end of the stack */
extern const void * STACK_SIZE; /* Size of the stack*/
extern uint32_t * __SSTACK; /* Points to begin of the stack */
#define SP_WaterMark_c  0x424e424e
#define SP_Starts  __SSTACK
#else
extern char __SEG_START_SSTACK[];
#define SP_Ends         (__SEG_END_SSTACK)
#define SP_Starts       (__SEG_START_SSTACK)
#define SP_WaterMark_c  0x424e
#endif

/*
  PopEventsLeftAtHighWaterMark

  This function reports the maximum # of events available left in the worst case scenario.
  It does the difference between the event high water mark and the maximum number of events
  in sthe stack.
*/
uint8_t PopEventsLeftAtHighWaterMark(void);

/*
  PopStackLeftAtHighWaterMark

  Returns the amount of bytes left in the C stack.
*/
uint16_t PopStackLeftAtHighWaterMark(void);

/*
  PopTimersLeftAtHighWaterMark

  Returns the mountof timers left on the highest water mark.
*/
uint8_t PopTimersLeftAtHighWaterMark(void);

/*
  PopRetryEntriesLeftAtHighWaterMark

  Return the amount of entries left on the retry table at high water mark.
*/
uint8_t PopRetryEntriesLeftAtHighWaterMark(void);

/*
  PopRouterDiscoveryEntriesLeftAtHighWaterMark

  Returns the amount of route discovery table entries left at high water mark.
*/
uint8_t PopRouterDiscoveryEntriesLeftAtHighWaterMark(void);

/*
  PopDuplicateEntriesLeftAtHighWaterMark()

  Returns the maximum amount of entries in the duplicate table at high water mark.
*/
uint8_t PopDuplicateEntriesLeftAtHighWaterMark(void);

/************* Timer Interface ****************/

popErr_t PopStartTimer(popTimerId_t iTimerId, popTimeOut_t iMilliseconds);
bool IsThereAFreeTimer( void );
popErr_t PopStopTimer(popTimerId_t iTimerId);
popErr_t PopStartTimerEx(popTaskId_t iTaskId, popTimerId_t iTimerId, popTimeOut_t iMilliseconds);
popErr_t PopStopTimerEx(popTaskId_t iTaskId, popTimerId_t iTimerId);

// only 1 long timer can be used at a time. See PopDefault.c
typedef uint32_t popLongTimeOut_t;
popErr_t PopStartLongTimer(popTimerId_t iTimerId, popLongTimeOut_t iMilliseconds);
popErr_t PopStopLongTimer(popTimerId_t iTimerId);
void PopStopAllTimers(popTaskId_t iTaskId);

#define gPopTimerIdUser_c         1
#define gPopTimerIdUserLast_c  0xff


/***************** PopNet Gateway communication interface *********************/
void PopGatewayTargetToPC(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
#ifdef gPopLargeSerialPacket_d
popErr_t PopGatewaySendLargeEventToPC(popEvtId_t iEvtId, uint16_t iLen, void *pBuffer, void* pStaticBuffer);
#endif // gPopLargeSerialPacket_d
popErr_t PopGatewaySendEventToPC(popEvtId_t iEvtId, uint16_t iLen, void *pBuffer);

// maximum size of a gateway payload to be sent out via PopGatewaySendEventToPC()
/* 5 = STX + OpGroupCode + iEvtID + iLen + ChkSum */
#define gMaxGatewayPayloadSize_c        (gPopSerialTxBufferSize_c - 5)
#define PopGatewayMaxPayload() gMaxGatewayPayloadSize_c


/************* Miscellaneous ****************/
bool PopNwkIsEqual8Bytes(void *pPtr1, void *pPtr2);

uint8_t PopCheckSum(uint16_t iBufferLen, void* pBuffer);

#if gPopLcdEnabled_d

// displays network information on Line 2 of LCD (NCB boards only)
void PopDisplayLcdNetInfo(void);

// write a string to the LCD (up to 16 characters). Extra columns are filled with spaces.
// The line must be 1 or 2
void PopLcdWriteString(uint8_t iLine, char *pszStr);

#else

#define PopDisplayLcdNetInfo()
#define PopLcdWriteString(iLine, pszStr)

#endif  // gPopLcdEnable_c

/* save image information */

void PopNwkMgmtProcessOtaUpgrade(sPopNwkDataIndication_t * pIndication);
void PopOtaUpgradeStateMachine(void);

#if gPopOverTheAirUpgrades_d
popErr_t PopNwkMgmtOtaUpgradeResetToNewImage(sPopGwResetToNewImageReq_t * pResetReq);
extern popErr_t PopNwkMgmtDataRequest(sPopNwkDataRequest_t *pDataReq, bool fFromApp);
uint8_t PopOtaUpgradeAddErrorInfo(popErr_t errType, sPopOtaUpgradeErr_t * pOtaUpgradeError,uint8_t sourceType);
void PopOtaUpgradeSaveRspToGw(popOtauCmdId_t cmdId,popErr_t errType);
#else
#define PopOtaUpgradeAddErrorInfo(errType, pOtaUpgradeError, sourceType)
#endif

#if gPopMicroPowerLpcUpgrade_d
void MPResetLPC(void);
void MPEnterIspCommandHandler(void);
popErr_t PopNwkMgmtLpcUpgradeResetToNewImage(sPopGwResetToNewImageReq_t * pResetReq);
popErr_t PopLpcOtaUpgradeStorageModule(sPopOtauGwSubseqBlockSaveReq_t *pBlockUpgrade,uint8_t reqSource);
void MPGatewaySendLpcCommand(uint8_t iLen, void *pBuffer);
typedef enum eMPIspState_tag{
  eSendQnMark = 0,
  eSendSynchronized = 1,
  eSendClkFreq = 2,
  eSendEcho = 3,
  eSendReadPartId = 4,
  eSendUnlock = 5,
  eSendPrepAllFlash = 6,
  eSendEraseAllFlash = 7,
  eSendWriteRam = 8,
  eSendDataToRam = 9,
  eSendChecksum = 10,
  eSendPrepSector = 11,
  eSendCopyRamToFlash = 12,
  eEndOfTransmission = 13
}eMPIspState_t;
#endif

#if gPopOverTheAirUpgrades_d || gPopUsingStorageArea_c
popErr_t PopOtaUpgradeStorageModule(sPopOtauGwSubseqBlockSaveReq_t *pBlockUpgrade,uint8_t reqSource);
popErr_t PopOtaUpgradeStorageModuleInternal(sPopOtauGwFirstBlockPassThruSaveInternalReq_t *pFirstImageBlock,uint8_t reqSource);

/*
  The GBIF File used on the GB60, stores the OTA image in using chunks and each chunk
  of data start wiht a header in the way described here in this data strucutre.
*/
typedef struct sStoredImageHeader_tag
{
  popStorageSize_t  iLength;  //  The length in bytes of the current chunk of data.
  uint16_t  iAddress;       // The address in ROM where the current chunk of data should be stored.
  /* NOTE: the next fiedl after this address is the actual data to be written. */
}sStoredImageHeader_t;

/*
  The control data refers to the Size of the stored image plus the flag that controls if the
  current NVM section wil be preserved or not.
*/
typedef struct sControlData_tag
{
  popStorageSize_t  iSizeOfImage;
  bool fPreserveNVM;
}sControlData_t;
#endif
#endif  // _POPNET_H_

