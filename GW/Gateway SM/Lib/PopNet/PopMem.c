/***************************************************************************
  popmem.c
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is 
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  This module contains memory management routes for PopNet, PopMemAlloc() and 
  PopMemFree(). Includes memory copy functions, such as PopMemCpy().

  Also includes high-level storage routines (NVM). Low-level (erase, write) are 
  in PopBsp.c.

  The memory subsystem is limited to 254 byte allocations. Anything larger will 
  fail. An application that needs a larger buffer should not use the heap. The 
  heap is largely used by the over-the-air message system to allocate and free 
  messages, by the timer subsystem, and other internal PopNet systems.

  These memory routines are NOT thread safe. Do not allocate from inside an 
  interrupt handler.

****************************************************************************/
#include <string.h>
#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"
#include "PopNwk.h"
#include <stddef.h>

/***************************************************************************
  Local Types
***************************************************************************/

// memory block structure for heap
struct _tagPopMemBlock
{
  uint8_t			iControl;	// control flags = 1010 1xxx (A8 - AF)
  popMemSize_t	iSize;		// size of allocated block
};
typedef struct _tagPopMemBlock sPopMemBlock;

#define PopMemControl_c		0xC0	// initial control value for free blocks
#define PopMemAllocBits_c	0x07	// allocation count is lower 3 bits
#define PopMemFreeVal_c		0x3F	// (F for free) block contents when freed (debug only)
#define PopMemAllocVal_c		0x3A	// (A for alloc) block contents when allocated (debug only)


/****** Flash memory support ********/

// each valid (non-erased) page contain a sPopNvPage_t header
//#define mPopNvPageId_c              0x50  // valid page, 'P'

#define mPopNvInvalidSequence_c     0xff  // indicates page is erased

// in memory representation of each page
typedef struct _tagPopNvPageInfo_t
{
  popNvPageSequence_t iSequence;  // rolling sequence # (0x00-0xfe), or 0xff if erased
  
} sPopNvPageInfo_t;


// each valid entry on a page has the mPopNvStx_c symbol.
#define mPopNvEntryStx_c   'p'    // each flash packet starts with this start of packet

typedef uint8_t popNvEntryLen_t;

// each valid flash entry begins with one of these
#if gPopNeedPack_c
#pragma pack(1)
#endif
typedef struct _tagPopNvEntry_t
{
  uint8_t          iStx;       // start of packet, 'p'
  popNvId_t       iId;        // caller (0x00-0xfe)
  popNvEntryLen_t iLen;       // length of data in entry (0x00 - 0x7f)
  uint8_t          iChecksum;  // byte additive checksum
  uint8_t          aData[1];   // variable length data
} sPopNvEntry_t;
#if gPopNeedPack_c
#pragma pack()
#endif
#define mPopNvSizeEntryHdr_c (sizeof(sPopNvEntry_t)- PopSizeOfMember(sPopNvEntry_t,aData))
#define mPopNvSizePageHdr_c  sizeof(sPopNvPage_t) 
// size of a single entry
#define PopNvEntrySize(pEntry) (mPopNvSizeEntryHdr_c + (pEntry)->iLen)

// NV IDs internally for the stack
#define gPopNvIdNwkData_c            1
#define gPopNvIdNwkRouteTable_c      2
#define gPopNvIdNwkSecureCounters_c  3

/***************************************************************************
  Prototypes
***************************************************************************/
popErr_t PopOtaUpgradeFirstBlockVerification(sPopOtauGwFirstBlockSaveReq_t  * pFirstImageBlock);
/***************************************************************************
  Local Prototypes
***************************************************************************/

// debugging
#if (gPopAssertLevel_c >= 2)
popHeapSize_t iPopMemLowestFree;	// how low did free value go?
popHeapSize_t iPopMemFree;
uint16_t iPopMemNoAllocCount;		// how many times did alloc fail?
#define PopMemDbgSetFree(iFree) iPopMemFree = iPopMemLowestFree = iFree
#define PopMemDbgIncNoAllocCount() ++iPopMemNoAllocCount
#define PopMemDbgResetNoAllocCount() iPopMemNoAllocCount = 0
#define PopMemDbgRecordFree(iFree)
#define PopMemDbgRecordAlloc(iFree)
#define PopMemDbgMemSet(pBuffer, iValue, iSize) PopMemSet(pBuffer, iValue, iSize)//PopMemSetLarge(pBuffer, iValue, iSize)
#define PopMemDbgMemSetLarge(pBuffer, iValue, iSize) PopMemSetLarge(pBuffer, iValue, iSize)//PopMemSetLarge(pBuffer, iValue, iSize)
#else
#define PopMemDbgSetFree(iFree)
#define PopMemDbgIncNoAllocCount()
#define PopMemDbgResetNoAllocCount()
#define PopMemDbgRecordFree(iFree)
#define PopMemDbgRecordAlloc(iFree)
#define PopMemDbgMemSet(pBuffer, iValue, iSize)
#define PopMemDbgMemSetLarge(pBuffer, iValue, iSize)
#endif

/******************** Assert Interface ********************/
#if (gPopAssertLevel_c == 5)
void PopMemDbgAllocSuccess(uint16_t iAddress);
void PopMemDbgFreeSuccess(uint16_t iAddress);
#else
#define PopMemDbgAllocSuccess(iAddress)
#define PopMemDbgFreeSuccess(iAddress)
#endif

/* memory allocation failed */
void PopMemAllocFailed(void);


/* local prototypes */
void PopMakeBlocks(void *pStartMem, popHeapSize_t iSizeLeft);

#ifdef gPopMemUnitTest_d
void PopUnitTestFail(uint8_t iTestNum);
#endif

bool PopMemCombineFree(sPopMemBlock *pFreeBlock);
bool PopMemIsValidBlock(sPopMemBlock *pBlock);
uint8_t PopMemAllocCount(sPopMemBlock *pBlock);
uint8_t PopMemAllocCountAtMax(sPopMemBlock *pBlock);
sPopMemBlock *PopMemGetHeader(void *pMemory);
popHeapSize_t PopMemBlockSize(sPopMemBlock *pBlock);
sPopMemBlock *PopMemNextBlock(sPopMemBlock *pBlock);
void PopMemDownsizeBlock(sPopMemBlock *pBlock, popMemSize_t iSize);
void PopNvmRewriteEntry(uint8_t pageId, popNvPageSize_t offset);

/* common macro functions */
#define PopMemDecrementAllocCount(pBlock) --((pBlock)->iControl);
#define PopMemIncrementAllocCount(pBlock) ++((pBlock)->iControl);
#define PopMemIsFree(pBlock) (!PopMemAllocCount(pBlock))


/*  NVM macro functions */
uint8_t PopNvNextPage(uint8_t iPage);
sPopNvPage_t *PopNvGetCurrentPage(void);
bool PopNvIsValidPage(sPopNvPage_t *pPage);
/***************************************************************************
  Globals
***************************************************************************/

// The heap can be found in PopCfg.c
extern uint8_t gaPopMemHeap[];
extern const popHeapSize_t cPopMemHeapSize;
extern uPopSecureCounters_t  guPopNwkSecureCounters;

#if DEBUG >= 2
extern sMpDebugCount_t gsMpDebug;
extern uint8_t giUnknownEvent;
extern uint8_t giUnknownEventMemFree;
#endif

sPopOtaUpgradeImageInfo_t gsPopOtaUpgradeInfo;
PopOtaUpgradeState_t gOtaUpgradeState;

// have we reported an out of memory error?
bool gfPopReportedOutOfMemory;


#if (gPopAssertLevel_c >= 2)
popHeapSize_t iPopMemLowestFree;	// how low did free value go?
popHeapSize_t iPopMemFree;
uint16_t iPopMemNoAllocCount;		// how many times did alloc fail?
#endif


#if (gPopAssertLevel_c >= 2)
bool gfReportAlloc = true;
#endif

// NVM enabled
#if gPopNumberOfNvPages_c
bool gfPopGarbageNoHeader;
bool gfPopGarbageNoErase;

// unit test version
#ifdef gPopNvUnitTest_d 
uint8_t gaPopNvPages[gPopNumberOfNvPages_c * gPopNvPageSize_c];
bool gfPopNvNoFlashInit;
// real NVM version
#else

#if (gPopBigEndian_c == true)

#pragma  CONST_SEG  NV_DATA
const uint8_t gaPopNvPages[1] = { 0xff };
#pragma  CONST_SEG  DEFAULT

#endif

#endif


#endif

/* Bitmask to indicates what NVM NWK data has changed since the last time it was saved. */
popNvmDataChangedBitMask_t giPopNvmDataChanged;

/* 
  Entry length.
  nvEntryLength will be updated by each function that need to return a nvEntry. With this length
  will be easy to get from NVM the data. 
*/
uint8_t gNvEntryLength;

// defined in PopCfg.c
extern sPopNwkRouteEntry_t gaPopNwkRouteTable[];

// NV globals
popNvPageSequence_t mPopNvPageSequence;   // current sequence # (0x00-0xfe)
uint8_t             mPopNvCurrentPage;    // current page
uint8_t             mPopNvSparePage;      // current spare page
uint8_t             mPopNvSpareCount;     // # of spare pages
popNvPageSize_t     mPopNvWriteOffset;    // current write offset
popNvId_t           mPopNvDontCollectId;  // don't collect on this ID (used during write only)


/***************************************************************************
  Memory Helper Routines
***************************************************************************/

/*
  PopMemCpy
  Copies in the forward direction (PopMemMove() counts on this). 
*/
void PopMemCpy(void *pDst, void *pSrc, uint16_t iLen)
{
  uint16_t i;

  for(i=0; i<iLen; ++i)
  {
    ((uint8_t *)pDst)[i] = ((uint8_t *)pSrc)[i];
  }
}

/*
  PopMemMove
  Move either forward or backward.
*/
void PopMemMove(void *pDst, void *pSrc, uint8_t iLen)
{
  if(pDst < pSrc)
    PopMemCpy(pDst, pSrc, iLen);
  else
  {
    while(iLen)
    {
      --iLen;
      ((uint8_t *)pDst)[iLen] = ((uint8_t *)pSrc)[iLen];
    }
  }
}

/*
  PopMemSet
*/
void PopMemSet(void *pBuffer, uint8_t iValue, uint8_t iLen)
{
  uint8_t i;

  for(i=0; i<iLen; ++i)
    ((uint8_t *)pBuffer)[i] = iValue;
}

/*
  Set memory to 0.
*/
void PopMemSetZero(void *pBuffer, uint8_t iLen)
{
  PopMemSet(pBuffer, 0, iLen);
}
  
/*
  PopMemSetLarge.
*/
void PopMemSetLarge(void *pBuffer, uint8_t iValue, uint16_t iLen)
{
  uint16_t i;

  for(i=0; i<iLen; ++i)
    ((uint8_t *)pBuffer)[i] = iValue;
}


/*
  PopMemCmp

  Returns -1 < for less than, 1 > for greater than, 0 == for equal to.
*/
uint8_t PopMemCmp(void *pBuffer1, void *pBuffer2, uint16_t iLen)
{
  while(iLen--)
  {
    if(*(uint8_t *)pBuffer1 < *(uint8_t *)pBuffer2)
      return 0xff;
    else if(*(uint8_t *)pBuffer1 > *(uint8_t *)pBuffer2)
      return 1;
     pBuffer1 = (void *)((uint8_t *)pBuffer1 + 1);
     pBuffer2 = (void *)((uint8_t *)pBuffer2 + 1);
  }
  return 0;
}

/*
  PopMemChr

  Search an array of memory for a particular byte
*/
void *PopMemChr(void *pBuffer, uint8_t iValue, uint8_t iLen)
{
  uint8_t i;

  for(i=0; i<iLen; ++i)
  {
    if(((uint8_t *)pBuffer)[i] == iValue)
      return &(((uint8_t *)pBuffer)[i]);
  }
  return NULL;
}

/*
  PopMemIsFilledWith

  Check that an array is filled with a particular byte value.
*/
bool PopMemIsFilledWith(void *pBuffer, uint8_t iValue, uint8_t iLen)
{
  uint8_t i;

  for(i=0; i<iLen; ++i)
  {
    if(((uint8_t *)pBuffer)[i] != iValue)
      return false;
  }
  return true;
}

/*
  PopStrLen

  Return the length of the asciiz (NULL terminated) string. Up to 255 characters.
*/
uint8_t PopStrLen(char *s)
{
  uint8_t iLen=0;
  if(!s)
    return iLen;
  while(*s)
  {
    ++iLen;
    ++s;
  }
  return iLen;
}

/***************************************************************************
  Memory Allocation Subsystem
***************************************************************************/

/*
  PopMemInit
  (Internal PopNet Use Only)

  Initialize the memory subsystem in PopNet. Must be called prior
  to allocating memory.
*/
void PopMemInit(void)
{
  // set up debugging
  PopMemDbgSetFree(cPopMemHeapSize);
  PopMemDbgResetNoAllocCount();

  // create the blocks
  PopMakeBlocks(gaPopMemHeap, (popHeapSize_t)cPopMemHeapSize);
}

/*
  PopMemDownsizeBlock
  (Internal PopNet Use Only)
  
  Internal helper function. Downsizes a block and makes a free block after it.
*/
void PopMemDownsizeBlock(sPopMemBlock *pBlock, popMemSize_t iSize)
{
  popMemSize_t iOrgSize;
  sPopMemBlock *pNextBlock;

  // downsize the block
  iOrgSize = pBlock->iSize;
  pBlock->iSize = iSize;

  // make another free block following this one
  pNextBlock = PopMemNextBlock(pBlock);
  pNextBlock->iControl = PopMemControl_c;
  pNextBlock->iSize = iOrgSize - iSize -sizeof(sPopMemBlock);

  // if in debug mode, set next block contents to free
  PopMemDbgMemSet(pNextBlock + 1, PopMemFreeVal_c, pNextBlock->iSize);
}

/*
  PopDbgMemAllocSuccess

  Sends a message to test tool for every succeful memory allocation.
*/
#if (gPopAssertLevel_c == 5)
void PopMemDbgAllocSuccess(uint16_t iAddress)
{
//#ifdef gPopMemUnitTest_d
  // The serial gateway needs to be On to be able to see the message over the serial port
#if gPopGateway_c
  sPopEvtData_t sEvtData;

  if (!gfReportAlloc)
    return;

  sEvtData.iWord = iAddress;
  (void)PopSetAppEvent(gPopEvtMemoryAlloc_c, sEvtData.pData);
#else
  (void)iAddress;
#endif
//#endif
}
#endif

/*
  PopDbgMemFreeSuccess

  Sends a message to test tool for every succeful memory allocation.
*/
#if (gPopAssertLevel_c == 5)
void PopMemDbgFreeSuccess(uint16_t iAddress)
{
  // The serial gateway needs to be On to be able to see the message over the serial port
#if gPopGateway_c
  sPopEvtData_t sEvtData;

  if (!gfReportAlloc)
    return;

  sEvtData.iWord = iAddress;
  (void)PopSetAppEvent(gPopEvtMemoryFree_c, sEvtData.pData);
#else
  (void)iAddress;
#endif
}
#endif

/*
  PopMemAllocFailed

  Called if memory alloc ever fails. Used for debugging, resetting system, etc...
*/
void PopMemAllocFailed(void)
{

  /* don't report if turned off at gateway */
#if gPopGateway_c && (gPopAssertLevel_c > 2)
  if (!gfReportAlloc)
      return;
#endif

  /* don't report if already reported */
  if(gfPopReportedOutOfMemory)
    return;

  /* report to application */
  (void)PopSetAppEvent(gPopEvtMemoryAllocFailure_c, NULL);
  gfPopReportedOutOfMemory = true;
}

/*
  Indicate out of memory report has been received by application.
*/
void PopMemResetOutOfMemoryFlag(void)
{
  gfPopReportedOutOfMemory = false;
}

/*
  PopMemAlloc

  Allocate memory from the heap. Uses a first-fit scheme.

  If failed, returns NULL (0). If found, returns a pointer to the memory.
*/
void *PopMemAlloc(popMemSize_t iSize)
{
  sPopMemBlock *pBlock;
  sPopMemBlock *pNextBlock;
  void *pFoundMem;
  popHeapSize_t iSizeLeft;
#if DEBUG >= 2
  popHeapSize_t iBlockSize = 0xff;
#endif        
  // don't allow too-large allocations
  if(iSize > gPopMemMaxAlloc_c)
    return NULL;

  // disable interrupts
  PopDisableInts();  // #JJ the heap was corrupted unless interrupt was disabled
  
  // find the first freeblock that will fit
  pBlock = (void *)gaPopMemHeap;
  iSizeLeft = cPopMemHeapSize;
  pFoundMem = 0;
  while(iSizeLeft && pBlock)
  {
    // we'll need to check next block in anycase
    pNextBlock = PopMemNextBlock(pBlock);
    if((uint8_t *)pNextBlock - (uint8_t *)pBlock >= iSizeLeft)
      pNextBlock = 0;	// no next block
    else{
      if(!PopMemIsValidBlock(pNextBlock)){
#if DEBUG >= 1        
        #warning debug
        extern sPopAssertCount_t gsPopAssert;
        gsPopAssert.iAssertBadReceivePtr++;    
#endif
        break;
      }
    }
    
    // found a free block
    if(PopMemIsFree(pBlock) && pBlock->iSize >= iSize)
    {  
#if DEBUG >= 2
      if(pBlock->iSize < iBlockSize)
        iBlockSize = pBlock->iSize;
#endif      
      // free block is big enough for this alloc.
      // is it also big enough to make another free block?
      if(pBlock->iSize - iSize > sizeof(sPopMemBlock))
      	PopMemDownsizeBlock(pBlock, iSize);

      // indicate memory is allocated
      PopMemIncrementAllocCount(pBlock);

      // (optionally) fill with known value
      pFoundMem = (void *)(pBlock + 1);
      PopMemDbgMemSet(pFoundMem, PopMemAllocVal_c, pBlock->iSize);

      // (optionally) record change in free space
      PopMemDbgRecordAlloc(pBlock->iSize);
      break;
    }

    // not free, on to next block
    else
    {
      // if we could combine some free blocks, try again
      if(PopMemIsFree(pBlock)){
        if(PopMemCombineFree(pBlock)){
      	  continue;
        }
      }

      // go on to next block
      iSizeLeft -= PopMemBlockSize(pBlock);
      pBlock = pNextBlock;
    }
  }
  // enable interrupts
  PopEnableInts();
  
  // for debugging, determine # of times alloc failed
  if(!pFoundMem)
  {
    PopMemAllocFailed();
    PopMemDbgIncNoAllocCount();
#if DEBUG >= 2
    gsMpDebug.iHeapFreeBlockSizeMin = iBlockSize;
#endif
  }

  // Assert function call.
  if (pFoundMem){
    PopMemDbgAllocSuccess((uint16_t)pFoundMem);
#if DEBUG >= 2
  gsMpDebug.iHeapFreeNow = gsMpDebug.iHeapFreeNow - pBlock->iSize;
  if(gsMpDebug.iHeapFreeNow < gsMpDebug.iHeapFreeMin)
    gsMpDebug.iHeapFreeMin = gsMpDebug.iHeapFreeNow;  
  
  gsMpDebug.iMemallocCounterNow++;
  if(gsMpDebug.iMemallocCounterNow > gsMpDebug.iMemallocCounterMax)
    gsMpDebug.iMemallocCounterMax = gsMpDebug.iMemallocCounterNow;
#endif
  }
  // return ptr to allocated memory
  return pFoundMem;
}

/*
  PopMemAllocAgain

  Increments the allocation count (up to the maximum). Returns gPopErrFailed_c 
  if already at maximum.
*/
popErr_t PopMemAllocAgain(void *pMemory)
{
  sPopMemBlock *pBlock;

  // memory header precedes actual memory
  pBlock = PopMemGetHeader(pMemory);
  if(!PopMemIsValidBlock(pBlock) || PopMemAllocCountAtMax(pBlock))
    return gPopErrFailed_c;

  PopMemIncrementAllocCount(pBlock);
#if DEBUG >= 2
  gsMpDebug.iMemallocCounterNow++;
  if(gsMpDebug.iMemallocCounterNow > gsMpDebug.iMemallocCounterMax)
    gsMpDebug.iMemallocCounterMax = gsMpDebug.iMemallocCounterNow;
#endif
  return gPopErrNone_c;
}

/*
  PopMemRealloc

  Like realloc, but allows a block to be shrunk only, not expanded.
*/
popErr_t PopMemRealloc(void *pMemory, popMemSize_t iSize)
{
  sPopMemBlock *pBlock;

  // if trying to grow block, give up
  pBlock = PopMemGetHeader(pMemory);
  if(!PopMemIsValidBlock(pBlock) || pBlock->iSize < iSize)
    return gPopErrFailed_c;

  // if shrinking enough to form another free block, do it
  if(pBlock->iSize - iSize > sizeof(sPopMemBlock))
    PopMemDownsizeBlock(pBlock, iSize);

  // worked
  return gPopErrNone_c;
}

/*
  PopMemFree

  Free memory previously allocated by PopMemAlloc.

  Returns true if freed, false if not (either due to PopMemAllocAgain() or a bad pointer).
*/
bool PopMemFree(void *pMemory)
{
  sPopMemBlock *pBlock;

  // not a valid block, ignore it
  pBlock = PopMemGetHeader(pMemory);
  if(!PopMemIsValidBlock(pBlock))
    return false;

  // is block not valid or already freed?
  if(PopMemAllocCount(pBlock))
  {
    // decrement alloc count
    PopMemDecrementAllocCount(pBlock);
#if DEBUG >= 2
    gsMpDebug.iMemallocCounterNow--;
#endif
    if(PopMemIsFree(pBlock))
    {
      PopMemDbgFreeSuccess((uint16_t)pMemory);
      PopMemDbgMemSet(pMemory, PopMemFreeVal_c, pBlock->iSize);
      PopMemDbgRecordFree(pBlock->iSize);
#if DEBUG >= 2
      gsMpDebug.iHeapFreeNow = gsMpDebug.iHeapFreeNow + pBlock->iSize;
      if(giUnknownEventMemFree)
      {
        gsMpDebug.iHeapCnt++;
      }
#endif
      return true;	// freed
    }
  }

  // not actually freed
  return false;
}

/*
  PopMemFreeCompletely

  Frees the memory regardless of the allocation count
*/
void PopMemFreeCompletely(void *pMemory)
{
  sPopMemBlock *pBlock;

  // clear all the alloc bits
  pBlock = PopMemGetHeader(pMemory);
  if(PopMemIsValidBlock(pBlock))
    pBlock->iControl &= (~PopMemAllocBits_c);
}

/*
  PopMakeBlocks (Internal Routine)

  Split up some memory into largest possible free blocks.
*/
void PopMakeBlocks(void *pStartMem, popHeapSize_t iSizeLeft)
{
  sPopMemBlock *pBlock;

  // indicate bytes are free (only if gPopAssertLevel_c >= 2 selected)
  PopMemDbgMemSetLarge(pStartMem, PopMemFreeVal_c, iSizeLeft);

  // make a set of free blocks throughout this memory
  pBlock = pStartMem;
  while(iSizeLeft)
  {
    pBlock->iControl = PopMemControl_c;

    // allocate largest possible block, but make sure not to strand memory if 
    // not enough room for the header
    if(iSizeLeft >= gPopMemMaxAlloc_c + 2 * sizeof(sPopMemBlock))
      pBlock->iSize = gPopMemMaxAlloc_c;
    else if(iSizeLeft > gPopMemMaxAlloc_c + sizeof(sPopMemBlock))
      pBlock->iSize = iSizeLeft - 2 * sizeof(sPopMemBlock);
    else
      pBlock->iSize = iSizeLeft - sizeof(sPopMemBlock);

    // on to next block
    iSizeLeft -= pBlock->iSize + sizeof(sPopMemBlock);
    pBlock = (void *)((uint8_t *)pBlock + pBlock->iSize + sizeof(sPopMemBlock));
  }
}

/*
  PopMemCombineFree (Internal Routine)

  If there is a set of contiguous free blocks that are smaller than maximum size,
  combine them.

  Returns true if it combined memory, false if not
*/
bool PopMemCombineFree(sPopMemBlock *pFreeBlock)
{
  sPopMemBlock *pNextBlock;
  popHeapSize_t iSizeLeft;
  bool fBig;

  // how many bytes are left from here to end of heap?
  iSizeLeft = cPopMemHeapSize - ((uint8_t *)pFreeBlock - (uint8_t *)gaPopMemHeap);

  iSizeLeft -= PopMemBlockSize(pFreeBlock);
  pNextBlock = PopMemNextBlock(pFreeBlock);
  
  if(!iSizeLeft || !PopMemIsValidBlock(pNextBlock) || !PopMemIsFree(pNextBlock))
    return false;

  // keep going until we find:
  // a) a non-free block
  // b) a free block that's full size, or
  // c) the end of the heap
  fBig = false;
  while(iSizeLeft)
  {
    // is this one a big block?
    if(pNextBlock->iSize == gPopMemMaxAlloc_c)
      fBig = true;

    // check next block
    iSizeLeft -= PopMemBlockSize(pNextBlock);
    pNextBlock = PopMemNextBlock(pNextBlock);

    // at one of our conditions, break
    if(!iSizeLeft || !PopMemIsFree(pNextBlock) || fBig) // #JJ add !PopMemIsValidBlock(pNextBlock)?
      break;
  }

  // combine the memory individual free blocks
  PopMakeBlocks(pFreeBlock, (uint8_t *)pNextBlock - (uint8_t *)pFreeBlock);

  return true;
}

/*
  PopMemIsValidBlock
*/
bool PopMemIsValidBlock(sPopMemBlock *pBlock)
{
  // range check the block
  if(((uint8_t *)pBlock < gaPopMemHeap) || ((uint8_t *)pBlock >= gaPopMemHeap + cPopMemHeapSize))
    return false;

  // verify signature is correct
  return ((pBlock->iControl & ~PopMemAllocBits_c) == PopMemControl_c);
}

/*
  PopMemAllocCount
*/
uint8_t PopMemAllocCount(sPopMemBlock *pBlock)
{
  return (pBlock->iControl & PopMemAllocBits_c);
}

/*
  PopMemAllocCountAtMax
*/
uint8_t PopMemAllocCountAtMax(sPopMemBlock *pBlock)
{
  return ((pBlock->iControl & PopMemAllocBits_c) == PopMemAllocBits_c);
}

/*
  PopMemGetHeader
  (Internal PopNet Use Only)

  Returns a pointer to the header, given a ptr to the memory.
*/
sPopMemBlock *PopMemGetHeader(void *pMemory)
{
  return (sPopMemBlock *)((uint8_t *)pMemory - sizeof(sPopMemBlock));
}

/*
  PopMemBlockSize

  Returns the size of the block + header (which may exceed a byte in size)
*/
popHeapSize_t PopMemBlockSize(sPopMemBlock *pBlock)
{
  return ((popHeapSize_t)pBlock->iSize + sizeof(sPopMemBlock));
}

/*
  PopMemNextBlock

  Return the next block (we may be passed the end of the heap).
*/
sPopMemBlock *PopMemNextBlock(sPopMemBlock *pBlock)
{
  return (void *)((uint8_t *)pBlock + PopMemBlockSize(pBlock));
}



/*
  PopMemHeapCheck

  Returns 0xffff if everything is OK and the total amount of free bytes plus the size of the
  biggest free block.

  Returns offset within the heap of the bad block. The heap 
*/
popHeapSize_t PopMemHeapCheck(popHeapSize_t *pFree, popHeapSize_t *pBiggestFree)
{
  sPopMemBlock *pBlock;
  popHeapSize_t iSizeLeft;
  popHeapSize_t iTotalFree = 0;
  popHeapSize_t iBiggestFree = 0;

  pBlock = (void *)gaPopMemHeap;
  iSizeLeft = cPopMemHeapSize;

  // disable interrupts
  PopDisableInts(); // #JJ the heap check caused assert unless interrupt was disabled
  while(iSizeLeft)
  {
    // if this block doesn't have a valid marker, there is a problem
    if (!PopMemIsValidBlock(pBlock)){
      PopEnableInts();  // #JJ the heap check caused assert unless interrupt was disabled
      return (popHeapSize_t)((uint8_t *)pBlock - gaPopMemHeap);
    }
    // if this block extends past end of heap, there is a problem
    if (iSizeLeft < (popHeapSize_t)pBlock->iSize + sizeof(sPopMemBlock)){
      PopEnableInts();  // #JJ the heap check caused assert unless interrupt was disabled
      return (popHeapSize_t)((uint8_t *)pBlock - gaPopMemHeap);
    }

    // add up total free bytes
    if(PopMemIsFree(pBlock))
    {
      iTotalFree += pBlock->iSize + sizeof(sPopMemBlock);

      // Register the biggest chunck available.
      if (pBlock->iSize > iBiggestFree)
        iBiggestFree = pBlock->iSize;
    }

    // on to next block
    iSizeLeft -= pBlock->iSize + sizeof(sPopMemBlock);
    pBlock = PopMemNextBlock(pBlock);
  }

  // enable interrupts
  PopEnableInts();  // #JJ the heap check caused assert unless interrupt was disabled
  
  // no failures, return free space if user wants it
  if(pFree)
    *pFree = iTotalFree;

  // return largest free block if user wants it
  if(pBiggestFree)
    *pBiggestFree = iBiggestFree;

  // everthing is OK
  return gPopMemHeapOk_c;
}

/*
  PopMemFreeThemAll

  This function frees all the buffers allocated in memory.
*/

popHeapSize_t PopMemFreeThemAll(void)
{
 sPopMemBlock *pBlock;  // Each and every memory block on teh heap..

 pBlock = (void *)gaPopMemHeap;

 while((void *)pBlock < (void *)((uint8_t *)gaPopMemHeap + cPopMemHeapSize))
 {
   // If the block is invalid return the address of teh invalid block, heap compromise.
   if (!PopMemIsValidBlock(pBlock))
     return (popHeapSize_t)((uint8_t*)pBlock - gaPopMemHeap);

   // If the buffer is allocated free it...
   pBlock->iControl &= (~PopMemAllocBits_c);  // It will set the whole count to 0

   // we'll need to check next block in anycase
   pBlock = PopMemNextBlock(pBlock);
 }

 // Ok we are done ... check the heap in order to detect errors.
 return PopMemHeapCheck(NULL, NULL);
} 

#ifdef gPopMemUnitTest_d

#define POPMEM_TEST_INIT		1	// initialize test system
#define POPMEM_TEST_ALLOC		2	// simple allocate/free test
#define POPMEM_TEST_FILL		3	// keep allocating until full
#define POPMEM_TEST_COMBINE		4	// test combining free memory
#define POPMEM_TEST_AGAIN		5	// test allocating again and freeing memory
#define POPMEM_TEST_REALLOC		6	// shrink size of block (like realloc, but can't grow)

/*
  PopMemUnitTest

  This will use the LEDs to indicate which test is being issued.

  This scheme puts all of the work on allocate. When freeing memory, it 
  simply marks the memory as free. When allocating, it will "defragment" any 
  contiguous free chunks.
*/
void PopMemUnitTest(void)
{
  void *pMemory;
  void *pMemory2;


  /*
    Test Case 1: Initialize memory
  */
  PopMemInit();
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_INIT);


  /*
    Test Case 2: Allocate and free some memory
    small (1 byte)
    medium (53 bytes)
    almost large (full block - 1)
    large (full block)
  */
  pMemory = PopMemAlloc(1);
  PopMemFree(pMemory);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_ALLOC);
  pMemory = PopMemAlloc(53);
  PopMemFree(pMemory);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_ALLOC);
  pMemory = PopMemAlloc(gPopMemMaxAlloc_c-1);
  PopMemFree(pMemory);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_ALLOC);
  pMemory = PopMemAlloc(gPopMemMaxAlloc_c);
  PopMemFree(pMemory);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_ALLOC);


  /*
    Test Case 3: Fill all of memory
  */
  do
  {
    pMemory = PopMemAlloc(gPopMemMaxAlloc_c);
  } while(pMemory);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_FILL);
  PopMemInit();	// free all the memory
  do
  {
    pMemory = PopMemAlloc(1);
  } while(pMemory);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_FILL);
  PopMemInit();	// free all the memory


  /*
    Test Case 4: Combine free memory blocks
  */
  PopMemAlloc(60);			// set up test case
  pMemory = PopMemAlloc(5);
  pMemory2 = PopMemAlloc(5);
  PopMemAlloc(60);
  PopMemFree(pMemory);
  PopMemFree(pMemory2);

  pMemory2 = PopMemAlloc(10);	// allocate smaller block
  if(pMemory2 != pMemory || PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_COMBINE);
  PopMemFree(pMemory2);

  pMemory2 = PopMemAlloc(10 + sizeof(sPopMemBlock));	// allocate exactly right size
  if(pMemory2 != pMemory || PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_COMBINE);
  PopMemFree(pMemory2);

  pMemory2 = PopMemAlloc(11 + sizeof(sPopMemBlock));	// allocate too large
  if(pMemory2 == pMemory || PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_COMBINE);
  PopMemInit();	// free all the memory


  /*
    Test Case 5: Allocating Again

    This increments the alloc count on a block (so tasks that didn't 
    initially allocate the memory can keep it.
  */
  pMemory = PopMemAlloc(100);
  PopMemAllocAgain(pMemory);
  if(PopMemFree(pMemory))
    PopUnitTestFail(POPMEM_TEST_AGAIN);
  if(!PopMemFree(pMemory))
    PopUnitTestFail(POPMEM_TEST_AGAIN);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_AGAIN);
  PopMemInit();	// free all the memory


  /*
    Test Case 6: Realloc
  */
  pMemory = PopMemAlloc(100);
  if(PopMemRealloc(pMemory, 101) == gPopErrNone_c)	// don't allow expanding
    PopUnitTestFail(POPMEM_TEST_REALLOC);
  pMemory2 = PopMemAlloc(5);
  if(PopMemRealloc(pMemory, 20) != gPopErrNone_c)	// allow shrinking
    PopUnitTestFail(POPMEM_TEST_REALLOC);
  pMemory = PopMemAlloc(6);		// allocate some more memory
  if(pMemory >= pMemory2)
    PopUnitTestFail(POPMEM_TEST_REALLOC);
  if(PopMemHeapCheck(NULL, NULL) != gPopMemHeapOk_c)
    PopUnitTestFail(POPMEM_TEST_REALLOC);


  // done testing memory subsystem. Everything passed.
  PopMemInit();	// free all the memory
}

/*
  PopMemUnitTestFail

  Fail a particular test. Allows up to 256 tests. Uses LEDs to display failure.
*/
void PopMemUnitTestFail(uint8_t iTestNum)
{
  int i;

  PopLedDisplayHex(iTestNum);

  while(iTestNum)
  {
    ++i;
  }
}
#endif	// gPopMemUnitTest_d



/***************************************************************************
  Non-Volatile Memory Subsystem
***************************************************************************/

/*
  Stub routines do nothing... used to make code compile with and without NVM.
  These avoid the compiler warnings. For example:

  if(PopNvRetrieve(iItemId, &sStructure))
  {
    // do something
  }

  would warn "Condition Always true" if the retrieve function were just a macro.
  But with the stub, the compiler doesn't know it's always true.
*/
popErr_t PopNvStoreStub(popNvId_t iItemId, uint8_t iLen, void *pData) { (void)iItemId; (void)iLen; (void)pData; return gPopErrNone_c; }
popErr_t PopNvRetrieveStub(popNvId_t iItemId, void *pData) { (void)iItemId; (void)pData; return gPopErrNotFound_c; }
bool PopNvEnoughRoomStub(uint8_t iLen) { (void)iLen; return false; }
popErr_t PopNvStoreNwkDataStub(void) { return gPopErrNone_c; }
popErr_t PopNvRetrieveNwkDataStub(void) { return gPopErrNotFound_c; } 
popErr_t PopNvStoreSecureCounterDataStub(void) { return gPopErrNone_c; }
popErr_t PopNvRetrieveSecureCounterDataStub(void) { return gPopErrNotFound_c; }


/*
  PopAdditiveChecksum : internal

  Add all bytes together for the given length. Works up to 255 bytes. Additive 
  checksum rolls over, that is 0xfe + 0x05 = 0x03.
*/
uint8_t PopAdditiveChecksum(uint8_t iLen, uint8_t *pBuffer)
{
  uint8_t iChecksum;
  uint8_t i;

  // add up all bytes
  iChecksum = 0;
  for(i=0; i<iLen; ++i)
    iChecksum += pBuffer[i];

  return iChecksum;
}

/*
  PopNvSetChecksum : internal

  Assumes the NV entry is filled out completely except for the checksum. 

  Returns the NV entry filled with the checksum (all bytes add to 0x00).
  Cannot fail.
*/
void PopNvSetChecksum(sPopNvEntry_t *pEntry, uint8_t *pData)
{
  uint8_t iChecksum;

  // checksum both the header and the data. The sum of all bytes should be 0x00.
  pEntry->iChecksum = 0;
  iChecksum  = PopAdditiveChecksum(PopMemberOffset(sPopNvEntry_t, iChecksum), (void *)pEntry);
  iChecksum += PopAdditiveChecksum(pEntry->iLen, pData);
  pEntry->iChecksum = (~iChecksum) + 1;
}

/*
  PopNvIsValidEntry : internal

  If this is a valid entry, 
*/
bool PopNvIsValidEntry(sPopNvEntry_t *pEntry)
{
  // if anything looks wrong with the header, the entry is invalid
  if(pEntry->iStx != mPopNvEntryStx_c || pEntry->iId == gPopNvInvalidId_c || pEntry->iLen > gPopNvMaxDataLen_c)
    return false;

  // if the checksum isn't right, the entry is invalid
  if(PopAdditiveChecksum(PopNvEntrySize(pEntry), (void *)pEntry) != 0)
    return false;

  // entry looks ok to me
  return true;
}

/*
  PopNvScanForNextValidEntry : internal

  Scan for the next valid entry on this flash page. Note that is starts 
  searching exactly at pEntry, so pEntry can start out at pPage.

  Returns next valid entry, or NULL if none found.
*/
sPopNvEntry_t *PopNvScanForNextValidEntry(sPopNvPage_t *pPage, sPopNvEntry_t *pEntry)
{
  uint8_t *pPageByte;
  popNvPageSize_t i;
  sPopNvEntry_t *pNvEntry;
  bool status = false;
  
  // scan for next valid entry
  pPageByte = (uint8_t *)pPage;
  i = ((uint8_t *)pEntry) - pPageByte;
  for(;;)
  {
    // scan for next STX
    while(i < giPopNvPageSize )
    {
      /* First we need to locate the NvEntry here we don't know the length of the entry yet */
      pNvEntry = (sPopNvEntry_t *)PopNvGetEntryData((void *)(&pPageByte[i]), (uint8_t)sizeof(sPopNvEntry_t) );

      if( pNvEntry->iStx == mPopNvEntryStx_c)
      {
        break;
      }

      ++i;
    }
    
    // no more entries found
    if(i >= giPopNvPageSize - sizeof(sPopNvEntry_t) + 1)
      break;

    /* Before to valid the entry we need to reallocate the NvEntry to have more space and read all the information of the entry */
    gNvEntryLength = PopNvEntrySize( pNvEntry );

    /* Now we knows tha real size of the data for this Nv entry then read it */
    pNvEntry = (sPopNvEntry_t *)PopNvGetEntryData( (void *)(&pPageByte[i]), gNvEntryLength);
    
    pEntry = (sPopNvEntry_t *)(&pPageByte[i]);
    
    status = PopNvIsValidEntry( pNvEntry );
    if( status  )
      return pEntry;

    // must be garbage, skip the STX
    ++i;
  }

  return NULL;
}

/*
  PopNvGetPage : internal

  Return the address of the page, based on a page index.
*/
void *PopNvGetPage(uint8_t iPage)
{
#if gPopNumberOfNvPages_c
  return (sPopNvPage_t *)(&gpaPopNvFirstFlashPage[giPopNvPageSize * iPage]);
#else
  (void)iPage;
  return NULL;
#endif
}

/*
  PopNvGetCurrentPage : internal

  Return the address of the current page
*/
sPopNvPage_t *PopNvGetCurrentPage(void)
{
  return PopNvGetPage(mPopNvCurrentPage);
}

/*
  PopNvIsValidPage : internal

  Is this a valid page? (only checks the header, some or even all entries may be invalid).
*/
bool PopNvIsValidPage(sPopNvPage_t *pPage)
{
  bool isValid;

  isValid = true;

  /*
   * Check that the page valid markers and the sequence number bytes are valid.
   */

  if (pPage->marker1 != 'M' || pPage->marker2 != 'P' || pPage->marker3 != 'A'
    || pPage->marker4 != 'C' || pPage->marker5 != 'C' || pPage->marker6 != 'E'
    || pPage->marker7 != 'S' || pPage->marker8 != 'S')
  {
    /*
     * At least one marker byte contained the wrong value. The page is empty
     * or corrupted. Maybe power was lost while erasing the page. The page
     * must be erased before it can be used.
     */

    isValid = false;
  }
  else 
  {
    /*
     *  Checking the sequence number. It is critical that the sequence number
     *  is correct. Checking that at least two out of three contain matching
     *  values.
     */

    if (pPage->iSequence1 != mPopNvInvalidSequence_c
      && pPage->iSequence1 == pPage->iSequence2)
    {
      pPage->iSequence3 = pPage->iSequence1;
    }
    else if (pPage->iSequence2 != mPopNvInvalidSequence_c
      && pPage->iSequence2 == pPage->iSequence3)
    {
      pPage->iSequence1 = pPage->iSequence2;
    }
    else if (pPage->iSequence3 != mPopNvInvalidSequence_c
      && pPage->iSequence3 == pPage->iSequence1)
    {
      pPage->iSequence2 = pPage->iSequence3;
    }
    else
    {
      isValid = false;
    }
  }

  return isValid;
}

/*
  PopNvPreparePage : internal

  Used both during garbage collection and on the initial page (when all are erased).
  Prepares 
*/
void PopNvPreparePage(uint8_t iPage)
{
  uint8_t * pPage;
  sPopNvPage_t sPage;

  sPage.iSequence1 = mPopNvPageSequence;
  sPage.iSequence2 = mPopNvPageSequence;
  sPage.iSequence3 = mPopNvPageSequence;

  pPage = PopNvGetPage(iPage);
  (void)PopFlashWriteBytes(pPage + offsetof(sPopNvPage_t, iSequence1),
    &sPage.iSequence1, 3);

  // increment the page sequence (0x00-0xfe, don't allow 0xff)
  ++mPopNvPageSequence;
  if(mPopNvPageSequence == mPopNvInvalidSequence_c)
    mPopNvPageSequence = 0;   // page sequence rolled
}

/*
  PopNvErasePage : internal

  Erases the specified page and writes the valid page marker.
*/
static void PopNvErasePage(uint8_t iPage)
{
  uint8_t * pPage;
  sPopNvPage_t sPage;

  pPage = PopNvGetPage(iPage);

  // Destroy the page valid marker.
  sPage.marker1 = 0xBF;
  sPage.marker2 = 0xBF;
  sPage.marker3 = 0xBF;
  sPage.marker4 = 0xBF;
  sPage.marker5 = 0xBF;
  sPage.marker6 = 0xBF;
  sPage.marker7 = 0xBF;
  sPage.marker8 = 0xBF;
  (void)PopFlashWriteBytes(pPage, &sPage, offsetof(sPopNvPage_t, iSequence1));

  // Perform the actual erasing.
  PopNvmErasePage(iPage);

  // Write the erased page marker.
  sPage.marker1 = 'M';
  sPage.marker2 = 'P';
  sPage.marker3 = 'A';
  sPage.marker4 = 'C';
  (void)PopFlashWriteBytes(pPage, &sPage, 4);
}

/*
  PopNvFindLastThisPage : internal

  Find the logical "last" entry on this page that matches the item ID (for 0x00-0xfe).
  Since we write backwards, the "last" will be the first one found.

  Each valid entry looks like:
  [p][id][len][chk][ data ]
  [p][id][len][chk][ data ]
  [p][id][len][chk][ data ]

  Valid entries may be interspersed with garbage if the system was reset during
  writing to entries.

  Returns NULL if not found. Returns pointer to entry if found.
*/
sPopNvEntry_t *PopNvFindLastThisPage(sPopNvPage_t *pPage, popNvId_t iItemId)
{
  sPopNvEntry_t *pEntry;      // temporary variable
  sPopNvEntry_t *pNvEntry;
  
  // look through entire page each time
  pEntry = (sPopNvEntry_t *)(((uint8_t *)pPage) + sizeof(sPopNvPage_t));
  while(pEntry)
  {
    // any more entries?
    pEntry = PopNvScanForNextValidEntry(pPage, pEntry);
    if(!pEntry)
      break;    // no more entries

    pNvEntry = (sPopNvEntry_t *)PopNvGetEntryData(pEntry, gNvEntryLength );

    // is this the one we're looking for?
    if( pNvEntry->iId == iItemId)
      return pEntry;

    // skip past this entry (not the one we're looking for)
     pEntry = ( sPopNvEntry_t *)((uint8_t *)pEntry + PopNvEntrySize(pNvEntry));
  }

  // return not found
  return NULL;
}

/*
  PopNvFindLatestCopy : internal

  Find the latest copy of a given item ID.

  Returns a pointer to the latest entry, or NULL if not found.
*/
sPopNvEntry_t *PopNvFindLatestCopy(popNvId_t iItemId)
{
  uint8_t iPage;
  sPopNvPage_t *pPage;
  sPopNvEntry_t *pEntry;
  sPopNvPage_t *pNvEntry;
  // start with current page, and look from newest to oldest page
  iPage = mPopNvCurrentPage;

  // go through each page until done
  pEntry = NULL;
  do
  {
    // does page look like a valid page?
    pPage = PopNvGetPage(iPage);

    /* read the info of that page */ 
    pNvEntry = (sPopNvPage_t *)PopNvGetPageHeader(pPage);

    if(PopNvIsValidPage(pNvEntry))
    {
      // look for the LAST entry on this page that matches (that will be the newest on the page)     
      pEntry = PopNvFindLastThisPage(pPage, iItemId);
      if(pEntry)
        break;
    }

    // go to previous (older) page
    if(iPage == 0)
      iPage = giPopNvNumPages - 1;
    else
      --iPage;

  } while(iPage != mPopNvCurrentPage);

  return pEntry;
}


/*
  PopNvNextPage

  Increment page. Goes to next page #.
*/
uint8_t PopNvNextPage(uint8_t iPage)
{
  ++iPage;
  if(iPage >= giPopNvNumPages)
    iPage = 0;
  return iPage;
}

/*
  PopNvGarbageCollect : external

  This function garbage collects the oldest valid page into the spare page. 
  Updates the current page to the spare. The oldest page now becomes the spare.

  This takes at least 20ms (probably closer to 40ms).

  Cannot fail. System may be reset during garbage collection with no harm.

  Since the PopNet NVM uses a round-robin approach to page use, the oldtest 
  page is the spare page + 1 (wrapped if needed).

  TODO: how to deal with a failing system that can't NVM write anymore?
*/
void PopNvGarbageCollect(void)
{
  sPopNvPage_t *pFromPage;  // copy from this page
  sPopNvPage_t *pToPage;    // to this page
  sPopNvEntry_t *pEntry;
  sPopNvEntry_t *pNvEntry;
  uint8_t iOldestPage;
  uint8_t iEntrySize;
  sPopNvPage_t sPage;

  /* Header size + maximum data */
  
  // we always write starting from end of flash toward beginning
  mPopNvWriteOffset = giPopNvPageSize;

  pToPage = PopNvGetPage(mPopNvSparePage);

  // Write the garbage collection started marker.
  sPage.marker5 = 'C';
  sPage.marker6 = 'E';
  sPage.marker7 = 'S';
  sPage.marker8 = 'S';
  (void)PopFlashWriteBytes(((uint8_t *)pToPage) + offsetof(sPopNvPage_t, marker5),
    &sPage.marker5, 4);

  // if more than 1 spare, then just use the next spare page (no need to collect garbage)
  if(mPopNvSpareCount > 1)
  {
    mPopNvCurrentPage = mPopNvSparePage;
    PopNvPreparePage(mPopNvCurrentPage);
    mPopNvSparePage = PopNvNextPage(mPopNvSparePage);
    --mPopNvSpareCount;
    return;
  }

  // determine oldest page (always the next one after the spare)
  iOldestPage = PopNvNextPage(mPopNvSparePage);

  // prepare to copy from oldtest page to spare page
  pFromPage = PopNvGetPage(iOldestPage);

  // copy all valid entries from the oldest page to the "spare" page, which 
  // will become the newest page. The oldest page will then become the spare.
  pEntry = (sPopNvEntry_t *)(((uint8_t *)pFromPage) + sizeof(sPopNvPage_t));
  while(pEntry)
  {
    bool entryHandled;

    // look for next entry
    pEntry = PopNvScanForNextValidEntry(pFromPage, pEntry);

    // no more entries? we're done
    if(!pEntry)
      break;

    do 
    {
      /*
	     *	Read the data of this nvEntry
	     */
      pNvEntry =  (sPopNvEntry_t *)PopNvGetEntryData(pEntry, gNvEntryLength);

      // determine total size of entry
      iEntrySize = PopNvEntrySize(pNvEntry);

      if (!PopNvEnoughRoom(pNvEntry->iLen))
        break;

      entryHandled = true;

      // only copy entry if it's the latest copy. If there is a later copy, don't copy it.
      if (PopNvFindLatestCopy(pNvEntry->iId) == pEntry)
      {
        // write to this offset
        mPopNvWriteOffset -= iEntrySize;
        (void)PopFlashWriteBytes(&((uint8_t *)pToPage)[mPopNvWriteOffset], pNvEntry, iEntrySize);

        // Verify that the entry was written correctly
        pNvEntry = PopNvGetEntryData(&((uint8_t *)pToPage)[mPopNvWriteOffset], iEntrySize);
        entryHandled = PopNvIsValidEntry( pNvEntry );
      }
    }
    while (entryHandled == false);

    // skip past entry
    pEntry =  (sPopNvEntry_t *)((uint8_t *)pEntry + iEntrySize);
  }

  // debugging
#ifdef gPopNvUnitTest_d
  if(gfPopGarbageNoHeader)
    return;
#endif

  // all done with entries, write the header
  mPopNvCurrentPage = mPopNvSparePage;
  PopNvPreparePage(mPopNvCurrentPage);
  mPopNvSparePage = iOldestPage;

  // debugging
#ifdef gPopNvUnitTest_d
  if(gfPopGarbageNoErase)
    return;
#endif

  // oldest page is now spare, erase it
  PopNvErasePage(iOldestPage);
}

// only include these routines if NVM is enabled
// note: compile libraries with this option on
#if gPopNumberOfNvPages_c

/*
  PopNvEnoughRoom : external

  Is there enough room for a PopNvStore() of this size?
*/
bool PopNvEnoughRoom(uint8_t iLen)
{
  return (mPopNvWriteOffset - sizeof(sPopNvPage_t) >= (mPopNvSizeEntryHdr_c + iLen));
}
/*
  PopNvStore : external

  Store the data at the given offset. May need to garbage collect to store the 
  data if the current page is full.

  Parameters  Dir   Description
  ----------  ----  -----------------
  iItemId     I     item identifier (0x00-0xfe)
  iLen        I     length of data item to store
  pData       I     pointer to data item to store

  Returns
  gPopErrBadParm_c  if bad parameter(s)
  gPopErrNoMem_c    if not enough memory (even after garbage collecting oldest page)
  gPopErrNone_c     if worked
*/
popErr_t PopNvStore(popNvId_t iItemId, uint8_t iLen, void *pData)
{
  sPopNvEntry_t sEntry;
  sPopNvEntry_t *pEntry;
  bool wasWritten;

  do
  {
    sPopNvEntry_t * pReadEntry;

    // invalid parameters
    if(iLen > gPopNvMaxDataLen_c || iItemId == gPopNvInvalidId_c)
      return gPopErrBadParm_c;

    // TODO: don't actually store if data is identical to existing data

    // enough room for the data on the current page?
    if(!PopNvEnoughRoom(iLen))
    {
      // not enough room, see if garbage collection makes room
      // tell garbage collect not to preserve the ID we're trying to write
      mPopNvDontCollectId = iItemId;
      PopNvGarbageCollect();
      mPopNvDontCollectId = gPopNvInvalidId_c;

      // still not enough room? give up
      if(!PopNvEnoughRoom(iLen))
        return gPopErrNoMem_c;
    }

    // prepare and store the data
    sEntry.iStx = mPopNvEntryStx_c;
    sEntry.iId = iItemId;
    sEntry.iLen = iLen;
    PopNvSetChecksum(&sEntry, pData);

    // allocate the space in the flash page
    mPopNvWriteOffset -= mPopNvSizeEntryHdr_c + iLen;
    pEntry = (sPopNvEntry_t *)(&((uint8_t *)PopNvGetCurrentPage())[mPopNvWriteOffset]);

    // actually write the bytes
    (void)PopFlashWriteBytes(pEntry->aData, pData, iLen);
    (void)PopFlashWriteBytes(pEntry, &sEntry, mPopNvSizeEntryHdr_c);

    // Verify that the entry was written correctly
    pReadEntry = PopNvGetEntryData(pEntry, iLen + mPopNvSizeEntryHdr_c);
    wasWritten = true;
    if (memcmp(&sEntry, pReadEntry, mPopNvSizeEntryHdr_c) != 0
      || memcmp(pData, pReadEntry->aData, iLen) != 0)
    {
      wasWritten = false;
    }
  }
  while (wasWritten == false);

  // all worked!
  return gPopErrNone_c;
}

/*
  PopNvRetrieve

  Find latest copy of the particular item in the flash store.

  Parameters  Dir   Description
  ----------  ----  ----------------
  iItemId     I     identifier for the item (0x00-0xfe)
  pData       I/O   pointer to data to be retrieved. Data is filled in if found.

  Returns
    gPopErrNotFound_c if not found.
    gPopErrNone_c if found (and the contents have been copied to pData).
*/
popErr_t PopNvRetrieve(popNvId_t iItemId, void *pData)
{
  sPopNvEntry_t *pEntry;
  sPopNvEntry_t *pNvEntry; 
   
  // check for invalid parameters
  if(iItemId == gPopNvInvalidId_c || !pData)
    return gPopErrBadParm_c;

  // find latest copy of this item
  pEntry = PopNvFindLatestCopy(iItemId);

  // entry not found
  if(!pEntry)
    return gPopErrNotFound_c;

  /* if the entry was found then we need to get the data from Nvm */
  pNvEntry =  (sPopNvEntry_t *)PopNvGetEntryData(pEntry, gNvEntryLength);

  if(!pNvEntry)
    return gPopErrBadParm_c;

  // entry found, return the data
  PopMemCpy(pData, pNvEntry->aData, pNvEntry->iLen);
  return gPopErrNone_c;
}

/*
  PopNvStoreNwkData

  Store the network data into flash memory for protection after a reset.

  Returns gPopErrNone_c if worked.
  Returns gPopErrNoMem_c if not enough room in flash to store the information.
*/
popErr_t PopNvStoreNwkData(void)
{
  popErr_t iErr;
  
  // first store the general NWK data (PAN ID, node addr, channel, etc..)
  iErr = PopNvStore(gPopNvIdNwkData_c, sizeof(sPopNwkData_t), &gsPopNwkData);

  // Clear the bits on the mask for the info stored on the Nwk data set and routes.
  PopNvmClearDataChangedBit(gPopNvmNwkDataChanged_c);
  PopNvmClearDataChangedBit(gPopNvmRoutesChanged_c);
            
  // return error code
  return iErr;
}

/*
  PopNvRetrieveNwkData

  Retrieve network data so the status of the device can be back on the network 
  after a reset.

  Returns gPopErrNone_c if worked.
  Returns gPopErrNotFound_c if nwk data not stored.
*/
popErr_t PopNvRetrieveNwkData(void)
{
  extern sPopAssertCount_t gsPopAssert;
  popErr_t iErr;

  // first retieve the general NWK data (PAN ID, node addr, channel, etc..)
  iErr = PopNvRetrieve(gPopNvIdNwkData_c, &gsPopNwkData);

  // <SJS: restore optional fields from NVM, including assert count>
  // restore optionally saved routes 
  (void)PopNvRetrieve(gPopNvIdNwkRouteTable_c, gaPopNwkRouteTable);
  
  // restore optionally saved assert data
  (void)PopNvRetrieve(gPopNvIdAssert_c, &gsPopAssert);
  // </SJS:>
  {
#if DEBUG >= 2
    extern sMpDebugCount_t gsMpDebug;
    (void)PopNvRetrieve(gMpNvIdDebug_c, &gsMpDebug);
#endif
  }
  return iErr;
}

/*
  PopNvStoreSecureCounterData

  Store into non-volatile memory the outgoing secure counter.

  Returns gPopErrNone_c if worked.
  Returns gPopErrNoMem_c if not enough room in flash to store the information.
*/
popErr_t PopNvStoreSecureCounterData(void)
{

  if (!giPopSecurityLevel)
    return gPopErrNone_c;

  // Clear the bit for the change flag, mark the data as already saved.
  PopNvmClearDataChangedBit(gPopNvmCountersChanged_c);

  // Store the counter set directly into non-volatile memory.
  return gPopErrNone_c; // This function is not used, commented away write to NV flash to avoid future unwanted writes to flash
  //return PopNvStore(gPopNvIdNwkSecureCounters_c, sizeof(uPopSecureCounters_t), &guPopNwkSecureCounters);
}

/*
  PopNvRetrieveSecureCounterData

  Reads from non-volatile memory the secure counters set if found.

  Returns gPopErrNone_c if worked.
  Returns gPopErrNotFound_c if nwk data not stored.
*/
popErr_t PopNvRetrieveSecureCounterData(void)
{
  popErr_t iErr;

  if (!giPopSecurityLevel)
    return gPopErrNotFound_c;

  // With security enable retrieve the counter set.
  iErr = PopNvRetrieve(gPopNvIdNwkSecureCounters_c, &guPopNwkSecureCounters);

  // If there was no error on the retrieve process, and secure level is 1 = lite security,
  // increase the 2 byte long counters by some pre-defined amount. See gPopNwkSecureCounterIncrement_c
  // in popCfg.h
  if (!iErr && (giPopSecurityLevel == 1))
    PopNwkIncreaseLiteCounter(giPopNwkSecureCounterIncrement);

  // If there was no error on the retrieve process, and secure level is 1 = lite security,
  // increase the 4 byte long counters by some pre-defined amount. See gPopNwkSecureCounterIncrement_c
  // in popCfg.h
  if (!iErr && (giPopSecurityLevel == 2))
    PopNwkIncreaseAESCounter(giPopNwkSecureCounterIncrement);

  // Return gPopErrNone_c if success or gPopErrNotFound_c if the counter set was not found.
  return iErr;
}
#endif  // gPopNumberOfNvPages_c

/*
  PopNvmInit : public

  Initialize the NV memory subsystems
*/
void PopNvmInit(void)
{
#if gPopNumberOfNvPages_c
  sPopNvPage_t *pPage;
  uint8_t iPage;
  uint8_t iOldestPage;
  popNvPageSequence_t iOldestSeq;
  sPopNvPage_t *pNvPageInfo;
  // initialize the flash memory subsystem
  PopFlashInit();

  // collect all IDs when garbage collecting
  mPopNvDontCollectId = gPopNvInvalidId_c;

  // check each flash page for one of:
  // a. filled with valid entries.
  // b. already erased.
  // c. contains garbage and must be erased.
  // d. too many used pages (garbage collect didn't complete), erase oldest
  mPopNvSpareCount = 0;
  mPopNvPageSequence = iOldestSeq = mPopNvInvalidSequence_c;
  for(iPage=0; iPage < giPopNvNumPages; ++iPage)
  {
    // does page look like a valid page?
    pPage = PopNvGetPage(iPage);

    /* read the info of that page */ 
    pNvPageInfo = PopNvGetPageHeader(pPage);
      
    if(PopNvIsValidPage(pNvPageInfo))
    {
      // record latest sequence #
      // allows for wrapping of sequence #: that is, 0x00 is later than 0xfe,
      // just as 0x04 is later than 0x03.
      if(mPopNvPageSequence == mPopNvInvalidSequence_c || ((uint8_t)(pNvPageInfo->iSequence1 - mPopNvPageSequence) < (uint8_t)0x80))
      {
        mPopNvPageSequence = pNvPageInfo->iSequence1;
        mPopNvCurrentPage = iPage;
      }

      // record oldest (in case we have none erased, so we can erase the oldtest)
      if(iOldestSeq == mPopNvInvalidSequence_c || ((uint8_t)(iOldestSeq - pNvPageInfo->iSequence1) < (uint8_t)0x80))
      {
        iOldestSeq = pNvPageInfo->iSequence1;
        iOldestPage = iPage;
      }
    }

    // not a valid page. Is it bad or just erased?
    else
    {
      /*
       *  Erase page if it does not contain the valid page marker. Also erase
       *  page if the sequence number bytes contains data.
       */

      if (pNvPageInfo->marker1 != 'M' || pNvPageInfo->marker2 != 'P' || pNvPageInfo->marker3 != 'A'
        || pNvPageInfo->marker4 != 'C')
      {
        PopNvErasePage(iPage);
      }
      else if (pNvPageInfo->marker5 != 0xFF || pNvPageInfo->marker6 != 0xFF
        || pNvPageInfo->marker7 != 0xFF || pNvPageInfo->marker8 != 0xFF
        || pNvPageInfo->iSequence1 != mPopNvInvalidSequence_c
        || pNvPageInfo->iSequence2 != mPopNvInvalidSequence_c       
        || pNvPageInfo->iSequence3 != mPopNvInvalidSequence_c)
      {
        PopNvErasePage(iPage);
      }

      // found one more spare page
      ++mPopNvSpareCount;
    }
  }

  // no valid pages, just use the first one
  if(mPopNvPageSequence == mPopNvInvalidSequence_c)
  {
    // prepare the first page (garbage collect is smart about multiple spare pages)
    mPopNvSparePage = mPopNvPageSequence = 0;
    PopNvGarbageCollect();
  }

  // valid pages available, use 'em
  else
  {
    // no spare pages, erase the oldest one
    if(!mPopNvSpareCount)
    {
      //PopNvmErasePage(iOldestPage);
      PopNvErasePage(iOldestPage);
      mPopNvSparePage = iOldestPage;
      mPopNvSpareCount = 1;
    }

    // spare is always after current page
    else
      mPopNvSparePage = PopNvNextPage(mPopNvCurrentPage);

    // next page sequence # will start just after last one found
    ++mPopNvPageSequence;

    // find write offset on the current page
    mPopNvWriteOffset = sizeof(sPopNvPage_t) + 
        PopNvCountFFs((void *)(PopNvGetCurrentPage() + 1), 
        giPopNvPageSize - sizeof(sPopNvPage_t));

    // We don't do much writes, don't rewrite anymore
    // Rewrite the last written entry in case power was lost while writing
    //PopNvmRewriteEntry(mPopNvCurrentPage, mPopNvWriteOffset);
  }
#endif
}

/*
  PopNvmRewriteLastEntry

  Rewrites the entry at the specified offset.
*/

void PopNvmRewriteEntry(uint8_t pageId, popNvPageSize_t offset)
{
  uint8_t *pEntry;           // Pointer to program memory
  sPopNvEntry_t *pNvEntry;  // Pointer to RAM

  pEntry = (uint8_t *)PopNvGetPage(pageId) + offset;

  // Read entry header
  pNvEntry = (sPopNvEntry_t *)PopNvGetEntryData(pEntry, (uint8_t)sizeof(sPopNvEntry_t));
  if (pNvEntry->iStx == mPopNvEntryStx_c
    && pNvEntry->iLen <= gPopNvMaxDataLen_c
    && pNvEntry->iId != gPopNvInvalidId_c)
  {
    uint8_t entryLen;
    bool status;

    /*
     * Read the whole entry to RAM and write write it again to a new location.
     */

    entryLen = PopNvEntrySize( pNvEntry );
    pNvEntry = (sPopNvEntry_t *)PopNvGetEntryData(pEntry, entryLen);

    status = PopNvIsValidEntry(pNvEntry);
    if (status)
    {
      PopNvStore(pNvEntry->iId, pNvEntry->iLen, pNvEntry->aData);
    }
  }
}
/*****************************************

  For the NV Unit tests, use RAM to place the pages. Tests garbage collection, 
  etc.. in a "safe" environment. Then, these routines are swapped out for real 
  flash routines.
  
******************************************/
#ifdef gPopNvUnitTest_d
                  
/*
  PopFlashInit

  Initialize all flash pages.
*/
void PopFlashInit(void)
{
  uint8_t i;

  // don't wipe flash
  if(gfPopNvNoFlashInit)
    return;

  // wipe flash (to all 0xff)
  for(i=0; i<giPopNvNumPages; ++i)
    PopNvmErasePage(i);
}

/*
  PopNvmErasePage

  Erase a single page. The address must be the start of the page.
*/
void PopNvmErasePage(popNvmPage_t iPage)
{
  popNvPageSize_t i;  

  // erase the page
  for(i=0; i < giPopNvPageSize; i += 0x80)
    PopMemSet(&((uint8_t *)PopNvGetPage(iPage))[i], 0xff, 0x80);
  return;
}

// write a single block. A page is made of a set of blocks
bool PopFlashWriteBytes(void *pDstAddr, void *pSrcAddr, uint8_t iLen)
{
  PopMemCpy(pDstAddr, pSrcAddr, iLen);
  return true;
}

/*
  PopNvUnitTestFail

  Fail a particular test. Displays the failure # (0-ff) in hex both in the LCD 
  and the lower nybble on the LEDs.
*/
void PopNvUnitTestFail(uint8_t iTestNum)
{
#if gPopLcdEnabled_d
  char szStr[17];
  static char szFail[] = "NvUtErr ";

  // display error on LCD
  PopMemSetZero(szStr, sizeof(szStr));
  PopMemCpy(szStr, szFail, sizeof(szFail)-1);
  PopHex2Ascii(szStr + sizeof(szFail), iTestNum);
  PopLcdWriteString(1, szStr);
#endif

  // display error and hang
  PopLedDisplayHex(iTestNum);
  while(giPopForever)
    /* do nothing */;
}

/*
  PopNvUnitTest

  Unit test the NVM routines.
*/
void PopNvUnitTest(void)
{
  char szBuffer[20];
  char szBuffer2[10];
  popErr_t iErrWr;
  popErr_t iErrRd;
  uint8_t i;
  uint8_t letter;
/*
  Code for ARM7 SMAC, it is waiting decision...  
  uint8_t aGarbage[3] = { 0x55, 0xFF, 0xFF };
  sPopNvPage_t *pNvPage;
*/
  // initialize NVM
  PopNvmInit();

  // test 1 - retrieve a string not there
  iErrRd = PopNvRetrieve(0x7f, szBuffer);
  if(iErrRd != gPopErrNotFound_c)
    PopNvUnitTestFail(1);

  // test 2 - write a single string to NVM, retrieve it, compare
  iErrWr = PopNvStore(0x00, 5, "Hello");
  iErrRd = PopNvRetrieve(0x00, szBuffer);
  if(iErrWr || iErrRd || PopMemCmp(szBuffer, "Hello", 5) != 0)
    PopNvUnitTestFail(2);

  // test 3 - write a different string, different ID, Retrieve it, compare
  iErrWr = PopNvStore(0xfe, 7, "Goodbye");
  iErrRd = PopNvRetrieve(0xfe, szBuffer);
  if(iErrWr || iErrRd || PopMemCmp(szBuffer, "Goodbye", 7) != 0)
    PopNvUnitTestFail(3);

  // test 4 - retrieve first string
  iErrRd = PopNvRetrieve(0x00, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer, "Hello", 5) != 0)
    PopNvUnitTestFail(4);

  // test 5 - retrieve a string not there
  iErrRd = PopNvRetrieve(0x01, szBuffer);
  if(iErrRd != gPopErrNotFound_c)
    PopNvUnitTestFail(5);

  // test 6 - write again with a new value, then garbage collect
  iErrWr = PopNvStore(0x00, 10, "Testing123");
  if(iErrWr)
    PopNvUnitTestFail(6);
  PopNvGarbageCollect();
  iErrRd = PopNvRetrieve(0x00, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer, "Testing123", 10))
    PopNvUnitTestFail(6);

  // test 7 - garbage collect without preserving entry 0
  mPopNvDontCollectId = 0x00;
  PopNvGarbageCollect();
  mPopNvDontCollectId = gPopNvInvalidId_c;
  iErrRd = PopNvRetrieve(0x00, szBuffer);
  if(iErrRd != gPopErrNotFound_c)
    PopNvUnitTestFail(7);

  // test 8 - fill up page and force garbage collection
  PopNvmInit();
  PopMemCpy(szBuffer, "Packet 00", 10);
  for(i=0; i < 36; ++i)   // almost fill up memory (36 * 14 = 504)
  {
    szBuffer[7] = (i/10) + '0';
    szBuffer[8] = (i%10) + '0';
    iErrWr = PopNvStore(i, 10, szBuffer);
    if(iErrWr)
      PopNvUnitTestFail(8);
  }

  // one too large, should force garbage collection
  iErrWr = PopNvStore(i, 3, "xxx");

  // should only fail if 2 pages (1 active 1 spare)
  if(giPopNvNumPages == 2)
  {
    if(iErrWr != gPopErrNoMem_c)        // 512 - 2 = 510 - 504 = 6. 4 + 3 = 7, 1 too large.
      PopNvUnitTestFail(8);
  }

  // should work for 3 or more pages. Make sure it's been retrieved
  else
  {
    iErrRd = PopNvRetrieve(i, szBuffer);
    if(iErrRd || PopMemCmp(szBuffer, "xxx", 3))
      PopNvUnitTestFail(8);
  }

  // test 9 - write to fill up to exact size
  iErrWr = PopNvStore(i, 2, "OK");    // should work (exactly full)
  if(iErrWr)                          // 4 + 2 = 6, exactly right.
    PopNvUnitTestFail(9);
  iErrRd = PopNvRetrieve(0x00, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer, "Packet 00", 10))
    PopNvUnitTestFail(9);
  iErrRd = PopNvRetrieve(i, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer, "OK", 2))
    PopNvUnitTestFail(9);

  // test 10 - keep writing for many writes, with many garbage collections
  PopNvmInit();
  (void)PopNvStore(99, 7, "TestA99");
  for(letter='A'; letter <='Z'; ++letter)
  {
    PopMemCpy(szBuffer, "TestA00", 7);
    szBuffer[4] = letter;
    for(i=0; i < 32; ++i)
    {
      szBuffer[5] = (i/10) + '0';
      szBuffer[6] = (i%10) + '0';
      iErrWr = PopNvStore(i, 7, szBuffer);
      if(iErrWr)
        PopNvUnitTestFail(10);
    }
  }

  // test 11 - make sure we can retrieve all of them
  PopMemCpy(szBuffer2, "TestA99", 7);
  iErrRd = PopNvRetrieve(99, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer, szBuffer2, 7) != 0)
    PopNvUnitTestFail(11);
  for(i=0; i < 32; ++i)
  {
    szBuffer2[4] = 'Z';
    szBuffer2[5] = (i/10) + '0';
    szBuffer2[6] = (i%10) + '0';
    iErrRd = PopNvRetrieve(i, szBuffer);
    if(iErrRd || PopMemCmp(szBuffer, szBuffer2, 7) != 0)
      PopNvUnitTestFail(11);
  }

  // test 12 - put some garbage in a page (at begining, middle and end)
  PopNvmInit();
  (void)PopNvStore(99, 4, "xx99");
  (void)PopNvStore(98, 4, "xx98");
  (void)PopNvStore(97, 4, "xx97");
  (void)PopNvStore(96, 4, "xx96");
  (void)PopNvStore(95, 4, "xx95");
/* In ARM7 SMAC we cannot do this. So, neither we can overwrite data before written
  ((uint8_t *)PopNvGetCurrentPage())[mPopNvWriteOffset + 1] = 0x55;  // garbage at beginning
  ((uint8_t *)PopNvGetCurrentPage())[mPopNvWriteOffset + 19] = 0xff;  // garbage in middle
  ((uint8_t *)PopNvGetCurrentPage())[510] = 0xff;  // garbage in end

  Code for ARM7 SMAC it causes a NVM verify error
  
  pNvPage = PopNvGetCurrentPage();
 
  PopFlashWriteBytes( (void*)((uint32_t)pNvPage + mPopNvWriteOffset + 1 ) , (void *)(&aGarbage[0]), 1);
  PopFlashWriteBytes( (void*)((uint32_t)pNvPage + mPopNvWriteOffset + 19) , (void *)(&aGarbage[1]), 1);
  PopFlashWriteBytes( (void*)((uint32_t)pNvPage + 510), (void *)(&aGarbage[2]), 1);
    
  iErrRd = PopNvRetrieve(99, szBuffer);
  if(!iErrRd)
    PopNvUnitTestFail(12);
  iErrRd = PopNvRetrieve(98, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer,"xx98",4))
    PopNvUnitTestFail(12);
  iErrRd = PopNvRetrieve(95, szBuffer);
  if(!iErrRd)
    PopNvUnitTestFail(12);
*/
  // test 13 - garbage collection away garbage
  gfPopNvNoFlashInit = true;
  PopNvmInit();
  PopNvGarbageCollect();
  iErrRd = PopNvRetrieve(98, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer,"xx98",4))
    PopNvUnitTestFail(13);
  iErrRd = PopNvRetrieve(96, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer,"xx96",4))
    PopNvUnitTestFail(13);

  // test 14 - garbage collection dies after writing to spare but before writing page header
  gfPopGarbageNoHeader = true;
  PopNvGarbageCollect();
  gfPopNvNoFlashInit = true;
  PopNvmInit();
  iErrRd = PopNvRetrieve(98, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer,"xx98",4))
    PopNvUnitTestFail(14);
  iErrRd = PopNvRetrieve(96, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer,"xx96",4))
    PopNvUnitTestFail(14);

  // test 15 - garbage collection dies after writing to spare but before erasing new spare
  gfPopGarbageNoHeader = false;
  gfPopGarbageNoErase = true;
  PopNvGarbageCollect();
  gfPopNvNoFlashInit = true;
  PopNvmInit();
  iErrRd = PopNvRetrieve(98, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer,"xx98",4))
    PopNvUnitTestFail(15);
  iErrRd = PopNvRetrieve(96, szBuffer);
  if(iErrRd || PopMemCmp(szBuffer,"xx96",4))
    PopNvUnitTestFail(15);

}

#endif  // #ifdef gPopNvUnitTest_d

/******************************************************************************
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PopNet OTA Upgrade

This section contains all the functions to send an OTA upgrade of popnet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
******************************************************************************/


/*
  PopOtaUpgradeStorageModule

  Verifies the image blocks arriving and calls storage routines to save it

  Possible result status:
  gPopErrNone_c =  success
  gPopErrNoMem_c = total image is bigger than available memory
  gPopErrBusy_c = another transfer is taking place
  gPopErrFailed_c = hw check or block or image checksum failed
  gPopErrNoImage_c = non block 0 received after 3xThrottle expired
  or without a previous block 0

*/

popErr_t PopOtaUpgradeStorageModule(sPopOtauGwSubseqBlockSaveReq_t *pBlockUpgrade,uint8_t reqSource)
{
  uint32_t flashOffset=0;
  sPopOtauGwFirstBlockSaveReq_t  * pFirstImageBlock;
  uint16_t blockSize = gsPopOtaUpgradeInfo.blockSize;
  popErr_t status;

  /* swap image id if necessary */
  if(PopIsUpgradeReqSrcOTA(reqSource))
    OtaToNative16(pBlockUpgrade->handler);
  
  /* subsequent blocks process */
  if (pBlockUpgrade->blockNumber)
  {
    /* checks image id */
    if(!(gsPopOtaUpgradeInfo.handler))
      return gPopErrNoImage_c;

    if(PopIsUpgradeReqSrcOTA(reqSource))
      OtaToNative16(pBlockUpgrade->blockNumber);

    flashOffset = (pBlockUpgrade->blockNumber - 1)  * blockSize;
    /* if block number is not the expected return the correct error */
    if(pBlockUpgrade->blockNumber > (gsPopOtaUpgradeInfo.blockNumber +1))
      return gPopErrWrongBlock_c;
    else if(pBlockUpgrade->blockNumber < gsPopOtaUpgradeInfo.blockNumber)
      return gPopErrNone_c;  
    else
      gsPopOtaUpgradeInfo.blockNumber = pBlockUpgrade->blockNumber;
    
    /* if image id does not correspond and is not 0xFF return an error */
    if(pBlockUpgrade->handler != gsPopOtaUpgradeInfo.handler)
      return gPopErrBusy_c;

    /* checks block checksum */
    if(PopCheckSum(blockSize, pBlockUpgrade->imgBlock) != pBlockUpgrade->checksum)
      return gPopErrFailed_c;
    
    /*Now call the function to save the data on flash*/
    if(gPopErrNone_c != (PopStorageWriteBytes(flashOffset, pBlockUpgrade->imgBlock,blockSize)))
    {      
      return gPopErrNoMem_c;
    }

  }
  /* first block process */
  else
  {
    /* use the appropriate pointer */
    pFirstImageBlock = (sPopOtauGwFirstBlockSaveReq_t  *)pBlockUpgrade;

    /* get image size */
    if(PopIsUpgradeReqSrcOTA(reqSource)) {
      /* swap Image Size */
      OtaToNative32(pFirstImageBlock->imageSize);
      /* swap Application Id */
      OtaToNative16(pFirstImageBlock->appId);
    }

    status = PopOtaUpgradeFirstBlockVerification(pFirstImageBlock);
    if(status != gPopErrNone_c)
      return status;
    /* saves image information */
    gsPopOtaUpgradeInfo.handler = pBlockUpgrade->handler;
    gsPopOtaUpgradeInfo.platform = pFirstImageBlock->platform;
    gsPopOtaUpgradeInfo.imageChecksum  = pBlockUpgrade->checksum; 
    gsPopOtaUpgradeInfo.blockSize = pFirstImageBlock->blockSize;
    gsPopOtaUpgradeInfo.preserveNvm = pFirstImageBlock->preserveNvm;
    gsPopOtaUpgradeInfo.blockNumber = 0;
    /* reset offset */
    flashOffset = 0;
    
#if gPopMicroPowerExternalFlash_d
    PopExternalStorageEraseAll();
#else      
    /* prepare storage */
    PopStorageEraseAll();
#endif      
  }

  return gPopErrNone_c;
}


popErr_t PopOtaUpgradeStorageModuleInternal(sPopOtauGwFirstBlockPassThruSaveInternalReq_t *pFirstImageBlock,uint8_t reqSource)
{
  popErr_t status;
  sPopOtauGwFirstBlockSaveReq_t firstImageBlockCopy;

  firstImageBlockCopy.appId = pFirstImageBlock->appId;
  firstImageBlockCopy.blockNumber = pFirstImageBlock->blockNumber;
  firstImageBlockCopy.blockSize = 0;
  firstImageBlockCopy.checksum = pFirstImageBlock->checksum;
  firstImageBlockCopy.handler = pFirstImageBlock->handler;
  firstImageBlockCopy.hwId = 0x20;
  firstImageBlockCopy.imageSize = pFirstImageBlock->imageSize;
  firstImageBlockCopy.nvPages = pFirstImageBlock->nvPages;
  firstImageBlockCopy.platform = ePopHwTypeArm7;
  firstImageBlockCopy.preserveNvm = pFirstImageBlock->preserveNvm;
  status = PopOtaUpgradeFirstBlockVerification(&firstImageBlockCopy);
  if(status != gPopErrNone_c)
    return status;

  /* saves image information */
  gsPopOtaUpgradeInfo.handler = pFirstImageBlock->handler;
  gsPopOtaUpgradeInfo.platform = pFirstImageBlock->platform;
  gsPopOtaUpgradeInfo.imageChecksum  = pFirstImageBlock->checksum;
  gsPopOtaUpgradeInfo.blockSize = pFirstImageBlock->blockSize;
  gsPopOtaUpgradeInfo.preserveNvm = pFirstImageBlock->preserveNvm;
  gsPopOtaUpgradeInfo.blockNumber = 0;

#if gPopMicroPowerExternalFlash_d
  PopExternalStorageEraseAll();
#else
  /* prepare storage */
  PopStorageEraseAll();
#endif

  return gPopErrNone_c;
}

#if gPopMicroPowerLpcUpgrade_d
popErr_t PopLpcOtaUpgradeStorageModule(sPopOtauGwSubseqBlockSaveReq_t *pBlockUpgrade,uint8_t reqSource)
{
  /* at the begining of the image */
  uint32_t flashOffset = gPopLpcStorageStart_c;
  sPopOtauGwFirstBlockSaveReq_t  * pFirstImageBlock;
  uint8_t blockSize = gsPopOtaUpgradeInfo.blockSize;
  popErr_t status;

  /* swap image id if necessary */
  if(PopIsUpgradeReqSrcOTA(reqSource))
    OtaToNative16(pBlockUpgrade->handler);
    
  /* subsequent blocks process */
  if (pBlockUpgrade->blockNumber)
  {
    if(gsPopOtaUpgradeInfo.hwId != gPopLpcUpgradeHwId_c)
      return gPopErrBadHwId_c;  
    
    /* checks image id */
    if(!(gsPopOtaUpgradeInfo.handler))
      return gPopErrNoImage_c;

    if(PopIsUpgradeReqSrcOTA(reqSource))
      OtaToNative16(pBlockUpgrade->blockNumber);

    flashOffset += (pBlockUpgrade->blockNumber - 1)  * blockSize;
    /* if block number is not the expected return the correct error */
    if(pBlockUpgrade->blockNumber > (gsPopOtaUpgradeInfo.blockNumber +1))
      return gPopErrWrongBlock_c;
    else
      gsPopOtaUpgradeInfo.blockNumber = pBlockUpgrade->blockNumber;

    /* checks block checksum */
    if(PopCheckSum(blockSize, pBlockUpgrade->imgBlock) != pBlockUpgrade->checksum)
      return gPopErrFailed_c;
          
    /*Now call the function to save the data on flash*/    
    if(gPopErrNone_c != (PopStorageWriteBytes(flashOffset, pBlockUpgrade->imgBlock,blockSize)))
    {      
      return gPopErrNoMem_c;
    }
    uint8_t unaLine[150];
    PopStorageReadBytes(unaLine, flashOffset, blockSize);
    if(PopMemCmp(pBlockUpgrade->imgBlock,unaLine,blockSize)){
      unaLine[140]=0xFF;
      return gPopErrBlockSaveValFail_c;
    }

  }
  /* first block process */
  else
  {
    /* use the appropriate pointer */
    pFirstImageBlock = (sPopOtauGwFirstBlockSaveReq_t  *)pBlockUpgrade;    
    status = PopOtaUpgradeFirstBlockVerification(pFirstImageBlock);
    if(status != gPopErrNone_c)
      return status;
    /* not for LPC? */
    if(pFirstImageBlock->hwId != gPopLpcUpgradeHwId_c)
    {
      if(PopIsUpgradeReqSrcOTA(reqSource)) {
        /* swap back Image Size */
        NativeToOta32(pFirstImageBlock->imageSize);
        /* swap back Application Id */
        NativeToOta16(pFirstImageBlock->appId);
        /* swap back handler id */
        NativeToOta16(pBlockUpgrade->handler);
      }
      return gPopErrBadHwId_c;
    }       
    /* get image size */
    if(PopIsUpgradeReqSrcOTA(reqSource)) {
      /* swap Image Size */
      OtaToNative32(pFirstImageBlock->imageSize);
      /* swap Application Id */
      OtaToNative16(pFirstImageBlock->appId);
    } 
    /* saves image information */
    gsPopOtaUpgradeInfo.handler = gPopLpcUpgradeAppId_c;
    gsPopOtaUpgradeInfo.platform = pFirstImageBlock->platform;
    gsPopOtaUpgradeInfo.imageChecksum  = pBlockUpgrade->checksum; 
    gsPopOtaUpgradeInfo.blockSize = pFirstImageBlock->blockSize;
    gsPopOtaUpgradeInfo.preserveNvm = pFirstImageBlock->preserveNvm;
    gsPopOtaUpgradeInfo.blockNumber = 0;
    /* reset offset */
    flashOffset = gPopLpcStorageStart_c;

    /* prepare storage */
    PopLpcExternalStorageEraseAll();
    
  }

  return gPopErrNone_c;
}
#endif
