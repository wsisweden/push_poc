/***************************************************************************
  PopAes.c
  Copyright (c) 2006-2011 San Juan Software, LLC. All Rights Reserved.

  San Juan Software Confidential and Proprietary. This source code is released under Non-Disclosure 
  and is not a public work. This notice is not to be removed.

  The AES 128-bit engine for PopNet. See PopCfg.h, gPopSecurityLevel_c.

  Please refer to Federal Information Processing Standards (FIPS), Publication 197
  November 26, 2001.

****************************************************************************/

// PopNet header files, included to be able to use the same types.
#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"


/* Precalculated SBox for the Ecnryption system please see wikipedia: Rijndael_S-box. */
const uint8_t SBox[256] = {
/*      | 0     1     2     3     4     5     6     7     8     9     a     b     c     d     e     f
        |----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|*/
/* 00 */ 0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,\
/* 01 */ 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,\
/* 02 */ 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,\
/* 03 */ 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,\
/* 04 */ 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,\
/* 05 */ 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,\
/* 06 */ 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,\
/* 07 */ 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,\
/* 08 */ 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,\
/* 09 */ 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,\
/* 0A */ 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,\
/* 0B */ 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,\
/* 0C */ 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,\
/* 0D */ 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,\
/* 0E */ 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,\
/* 0F */ 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
};

/* Each word is 4 bytes */
#define Nb     4   /* Block Size (Nb words) */
#define Nk     4   /* Key Length (Nk words) */
#define Nr     10  /* Number of Rounds */
#define Nblock (Nb * Nk)  /* The size of a block of 4 words, where each word is 4 bytes. */

// The AES-128 algorithm must use the formula in order to complete the expansion rounds.
#define ExpansionRounds (Nb * (Nr +1))

// The Space defined to handle the key and the full expansion.
uint8_t w[ Nblock * (Nr + 1) ];

// This flag is only used to avoid calculatin the same table every single time the function is used.
//bool_t RoundStatus = FALSE;

// The precalculated values of Rcon array to be used during the key expansion time.
const uint8_t gaRcon[ExpansionRounds] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80,0x1b,0x36};

// The MCU has a 8bit processor, lets try to optimize the copy of many bytes.
void Copy16Bytes(uint8_t *pDst, uint8_t *pSrc)
{
  uint8_t i;

  // depending on the size of the block (Nblock = 4 * 4).
  for (i = 0; i < Nblock; i++)
    pDst[i] = pSrc[i];
}

// depending on the size of one set of the block (4).
void Copy4Bytes(uint8_t *pDst, uint8_t *pSrc)
{
  uint8_t i;

  for (i = 0; i < Nb; i++)
    pDst[i] = pSrc[i];
}

void MixOneSet(uint8_t *pDst, uint8_t *pSrc)
{
  /* The array 'b' is each element of the array 'a' multiplied by 2 * in Rijndael's Galois
    field */
  uint8_t b[Nb];

  /* The loop index. */
  uint8_t i;

  /*
    Formula explanation:
    (pSrc[i] << 1):
      Means what ever the value of pSrc[i] multiplied by 2.

    (((pSrc[i] >> 7)&1) * 0x1B):
      Means what ever pSrc[i] has set it into the Rijndael's Galois field
  */
  for (i = 0; i < Nb; i++)
    b[i] = ( (pSrc[i] << 1) ^ (((pSrc[i] >> 7) & 1) * 0x1B) );

  /* For the formula of the defined space.*/
  pDst[0] = b[0] ^ pSrc[3] ^ pSrc[2] ^ b[1] ^ pSrc[1];  /* 2 * pSrc[0] + pSrc[3] + pSrc[2] + 3 * pSrc[1] */
  pDst[1] = b[1] ^ pSrc[0] ^ pSrc[3] ^ b[2] ^ pSrc[2];  /* 2 * pSrc[1] + pSrc[0] + pSrc[3] + 3 * pSrc[2] */
  pDst[2] = b[2] ^ pSrc[1] ^ pSrc[0] ^ b[3] ^ pSrc[3];  /* 2 * pSrc[2] + pSrc[1] + pSrc[0] + 3 * pSrc[3] */
  pDst[3] = b[3] ^ pSrc[2] ^ pSrc[1] ^ b[0] ^ pSrc[0];  /* 2 * pSrc[3] + pSrc[2] + pSrc[1] + 3 * pSrc[0] */
}

/* A mixing operation which operates on the columns of the state, combining the four bytes
  in each column */
void MixColumns( uint8_t *pSrc )
{
  uint8_t i;
  uint8_t aTemp[Nblock];

  Copy16Bytes(aTemp, pSrc);

  for (i = 0; i < Nblock; i += Nb)
    MixOneSet(&pSrc[i], &aTemp[i]);
}

// The way the key is arrenged the sets of 4 bytes aligned in one line are the columns of
// the matrix.
void BuildRow(uint8_t *pDst, uint8_t *pSrc)
{
  uint8_t i;
  uint8_t j;

  pDst[0] = pSrc[0];

  for (i = 1, j = 0; i < Nb; i++)
  {
    j = j + Nb;
    pDst[i] = pSrc[j];
  }
}

// This function is used to re-arenge the key array with the columns ins ets of 4 bytes
// aligned in one linear array.
void SetRow(uint8_t *pDst, uint8_t *pSrc)
{
  uint8_t i;
  uint8_t j;

  pDst[0] = pSrc[0];

  for (i = 1, j = 0; i < Nb; i++)
  {
    j = j + Nb;
    pDst[j] = pSrc[i];
  }
}

// Take a Rijndael's Galois word (4 bytes) and shoft it to the left bye one byte.
void ShiftByOne(uint8_t *pSrc)
{
  uint8_t i;
  uint8_t tempValue;

  tempValue = pSrc[0];

  for (i = 0; i < (Nb -1); i++)
    pSrc[i] = pSrc[i + 1];

  pSrc[3] = tempValue;
}

// Depending on the row of the amtrix is the amount of byte to be shifted to the left.
void ShiftRows(uint8_t *pSrc)
{
  /* Loop counters */
  uint8_t i;
  uint8_t j;

  
  uint8_t aRow[Nb];

  for (i = 1; i < Nb; i++)
  {
    BuildRow(aRow, &pSrc[i]);

    for (j = 0; j < i; j++)
      ShiftByOne(aRow);

    SetRow(&pSrc[i], aRow);
  }
}

// Replace a byte for is representation on the Rijndael's Galois space, it take a byte,
// for isntance 0x53, and separate it to be {5,3}, wich means the row number {5} and the
// column number {3}, of the SBox array.
// in this particular example the byte 0x53 = {5,3} will be replace for the byte 0xed.
void SubBytes(uint8_t *pSrc, uint8_t iLength)
{
  uint8_t i;
  uint8_t iPosition;

  for (i = 0; i < iLength; i++)
  {
    iPosition = (((pSrc[i] & 0xF0) >> 4) * Nblock) + (pSrc[i] & 0x0F);
    pSrc[i] = SBox[iPosition];
  }
}

// XOR a word of the Rijndael's Galois space.
void Xor4Bytes(uint8_t *pDst, uint8_t *pSrc)
{
  uint8_t i;

  for (i = 0; i < 4; i++)
    pDst[i] ^= pSrc[i];
}

// XOR 4 words of the Rijndael's Galois space.
void Xor16Bytes( uint8_t *pDst, uint8_t *pSrc)
{
  uint8_t i;

  for (i = 0; i < Nblock; i++)
    pDst[i] ^= pSrc[i];
}

/* Rcon: Calculates the Rijndael's Galois space is 2, so applying the formula X^(exp-1). */
uint8_t Rcon(uint8_t exp)
{
  uint8_t i;
  uint8_t x = 2;  // X on the Rijndael's Galois space.

  exp -= 1;

  if (!exp) {
    return 1;
  }

  if (exp > 1)
  {
    for (i = 0; i < (exp-1); i++)
      x *= 2;
  }

  return x;
}

/*
  Prepare a new key for the AES engine.
*/
void Expand128Key(uint8_t *pKey)
{
  uint8_t i;
  uint8_t j;
  uint8_t k;
  uint8_t aTemp[Nk];
/*
  uint8_t aRcon[Nb] = {0};
*/
  Copy16Bytes(&w[0], pKey);

  k = 0;
  for (i = 4, j = 4; i < ExpansionRounds; i++)
  {
    Copy4Bytes(aTemp , &w[(i-1) * Nk]);
    if (i == j)
    {
      ShiftByOne(aTemp);
      SubBytes(aTemp, Nk);
      /* Use the value for Rcon precalculated to speed up the process:

         Before:
          aRcon[0] = Rcon((uint8_t)(i/Nk));
          Xor4Bytes(aTemp, aRcon);

         Now:
      */
      aTemp[0] ^= gaRcon[k];  // We just make the operation with the value that gets affected.
      j += 4;
      k++;
    }

    Xor4Bytes(aTemp, &w[(i - Nk) * Nb]);
    Copy4Bytes(&w[i * Nk], aTemp);
  }
}

//void MasaruAES( uint8_t *pIn, uint8_t *pOut, uint8_t *pKey )
void PopNwkAES(uint8_t *pIn, /* uint8_t *pKey,*/ uint8_t *pOut)
{
  uint8_t aState[Nblock];
  uint8_t i;

  //if (!RoundStatus)
  //{
    //RoundStatus = TRUE;
    //Expand128Key(pKey);
  //}

  Copy16Bytes(aState, pIn);

  Xor16Bytes( aState,&w[0]);

  /* On AES 128 the number of rounds is 10. Please see FIPS-197 reference. */
  for( i = 1 ; i < Nr ; i++ )
  {
    SubBytes(aState, Nblock);
    ShiftRows(aState);
    MixColumns(aState);
    Xor16Bytes(aState, &w[Nblock * i]);
  }
  SubBytes(aState, Nblock);
  ShiftRows(aState);
  Xor16Bytes(aState, &w[Nblock * i]);

  Copy16Bytes(pOut, aState);
}

