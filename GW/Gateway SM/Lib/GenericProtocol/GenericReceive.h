#pragma once

#include "GenericProtocol.h"
#include <pb.h>
#include <pb_decode.h>
#include <stdint.h>
#include <stdbool.h>

// Public typedef

typedef struct {
  uint8_t* pBuffer;
  uint32_t bufferSize;
  uint32_t length;
  uint32_t offset;
  pb_istream_t stream;
  pb_istream_t substream;
} GenericReceive_t;

typedef enum {
  GenericReceiveErrorOk,
  GenericReceiveErrorCrc,
  GenericReceiveErrorLength,
  GenericReceiveErrorInternal
} GenericReceiveError_t;

// Public methods

void GenericReceiveInit(GenericReceive_t* const pInstance);
void GenericReceiveSetBuffer(GenericReceive_t* const pInstance,
                             uint8_t* pBuffer, const uint32_t size);
void GenericReceiveClear(GenericReceive_t* const pInstance);
bool GenericReceiveAddData(GenericReceive_t* const pInstance,
                           const uint8_t* const data, const uint32_t count);
GenericReceiveError_t GenericReceiveIsComplete(
    GenericReceive_t* const pInstance);
GenericReceiveError_t GenericReceiveIsCmdValid(
    GenericReceive_t* const pInstance);
GenericType_t GenericReceiveGetCmdType(GenericReceive_t* const pInstance);
bool GenericReceiveNextCmd(GenericReceive_t* const pInstance);
bool GenericReceiveDecode(GenericReceive_t* const pInstance,
                          const pb_field_t fields[], void* src_struct);
bool GenericReceiveDecodeSubstream(GenericReceive_t* const pInstance,
                          const pb_field_t fields[], void* src_struct);
bool GenericReceiveGetSubstreamFromRequest(GenericReceive_t* const pInstance,
                                           const pb_field_t fields[]);
