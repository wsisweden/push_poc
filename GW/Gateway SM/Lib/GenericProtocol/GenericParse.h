#pragma once

#include <stdint.h>

uint8_t GenericReadU8(const uint32_t offset, uint8_t* data,
    const uint8_t* const payload);
uint8_t GenericReadU16(const uint32_t offset, uint16_t* data,
    const uint8_t* const payload);
uint8_t GenericReadU32(const uint32_t offset, uint32_t* data,
    const uint8_t* const payload);
uint8_t GenericReadU64(const uint32_t offset, uint8_t* data,
    const uint8_t* const payload);
uint8_t GenericPutU8(const uint32_t offset, const uint8_t data,
    uint8_t* payload);
uint8_t GenericPutU16(const uint32_t offset, const uint16_t data,
    uint8_t* payload);
uint8_t GenericPutU32(const uint32_t offset, const uint32_t data,
    uint8_t* payload);
uint8_t GenericPutU64(const uint32_t offset, const uint64_t data,
    uint8_t* payload);
