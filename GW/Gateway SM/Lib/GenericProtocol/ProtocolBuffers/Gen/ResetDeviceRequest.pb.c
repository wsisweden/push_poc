/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.1 */

#include "ResetDeviceRequest.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t GenericProtocol_ResetDeviceRequest_fields[2] = {
    PB_FIELD(  1, UENUM   , SINGULAR, STATIC  , FIRST, GenericProtocol_ResetDeviceRequest, type, type, 0),
    PB_LAST_FIELD
};



/* @@protoc_insertion_point(eof) */
