/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.1 */

#ifndef PB_GENERICPROTOCOL_LOGRESPONSE_PB_H_INCLUDED
#define PB_GENERICPROTOCOL_LOGRESPONSE_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _GenericProtocol_UnknownEvent {
    pb_callback_t data;
/* @@protoc_insertion_point(struct:GenericProtocol_UnknownEvent) */
} GenericProtocol_UnknownEvent;

typedef struct _GenericProtocol_GenericAlarm {
    uint32_t code;
    bool active;
/* @@protoc_insertion_point(struct:GenericProtocol_GenericAlarm) */
} GenericProtocol_GenericAlarm;

/* Default values for struct fields */

/* Initializer values for message structs */
#define GenericProtocol_UnknownEvent_init_default {{{NULL}, NULL}}
#define GenericProtocol_GenericAlarm_init_default {0, 0}
#define GenericProtocol_UnknownEvent_init_zero   {{{NULL}, NULL}}
#define GenericProtocol_GenericAlarm_init_zero   {0, 0}

/* Field tags (for use in manual encoding/decoding) */
#define GenericProtocol_UnknownEvent_data_tag    1
#define GenericProtocol_GenericAlarm_code_tag    1
#define GenericProtocol_GenericAlarm_active_tag  2

/* Struct field encoding specification for nanopb */
extern const pb_field_t GenericProtocol_UnknownEvent_fields[2];
extern const pb_field_t GenericProtocol_GenericAlarm_fields[3];

/* Maximum encoded size of messages (where known) */
/* GenericProtocol_UnknownEvent_size depends on runtime parameters */
#define GenericProtocol_GenericAlarm_size        8

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define LOGRESPONSE_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
