/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.1 */

#include "BmuConfigRequest.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t GenericProtocol_BmuConfigRequest_fields[37] = {
    PB_FIELD(  1, UINT32  , SINGULAR, STATIC  , FIRST, GenericProtocol_BmuConfigRequest, batteryId, batteryId, 0),
    PB_FIELD(  2, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, batteryType, batteryId, 0),
    PB_FIELD(  3, STRING  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, batteryTag, batteryType, 0),
    PB_FIELD(  4, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, bmuType, batteryTag, 0),
    PB_FIELD(  5, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, cells, bmuType, 0),
    PB_FIELD(  6, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, capacity, cells, 0),
    PB_FIELD(  7, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, cyclesAvailable, capacity, 0),
    PB_FIELD(  8, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, ahAvailable, cyclesAvailable, 0),
    PB_FIELD(  9, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, instantLogPeriod, ahAvailable, 0),
    PB_FIELD( 10, BOOL    , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, everyDayUpdateHistoryLog, instantLogPeriod, 0),
    PB_FIELD( 11, BOOL    , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, highHistoryCurrentIdleLimit, everyDayUpdateHistoryLog, 0),
    PB_FIELD( 12, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, temperatureMax, highHistoryCurrentIdleLimit, 0),
    PB_FIELD( 13, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, remoteOutFunction, temperatureMax, 0),
    PB_FIELD( 14, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, dischargeStateThreshold, remoteOutFunction, 0),
    PB_FIELD( 15, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, chargeStateThreshold, dischargeStateThreshold, 0),
    PB_FIELD( 16, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, chargeEfficency, chargeStateThreshold, 0),
    PB_FIELD( 17, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, securityCode1, chargeEfficency, 0),
    PB_FIELD( 18, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, securityCode2, securityCode1, 0),
    PB_FIELD( 19, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, stateOfChargeLow, securityCode2, 0),
    PB_FIELD( 20, BOOL    , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, fahrenheitScale, stateOfChargeLow, 0),
    PB_FIELD( 21, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, clearStatistics, fahrenheitScale, 0),
    PB_FIELD( 22, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, cellTap, clearStatistics, 0),
    PB_FIELD( 23, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, baseLoad, cellTap, 0),
    PB_FIELD( 24, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, cableResistance, baseLoad, 0),
    PB_FIELD( 25, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, algorithmNumber, cableResistance, 0),
    PB_FIELD( 26, FLOAT   , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, voltageBalanceLimit, algorithmNumber, 0),
    PB_FIELD( 27, BOOL    , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, electrolyteSensor, voltageBalanceLimit, 0),
    PB_FIELD( 28, BOOL    , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, automaticCalibration, electrolyteSensor, 0),
    PB_FIELD( 29, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, equalizeFunction, automaticCalibration, 0),
    PB_FIELD( 30, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, equalizeParameter, equalizeFunction, 0),
    PB_FIELD( 31, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, wateringFunction, equalizeParameter, 0),
    PB_FIELD( 32, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, wateringParameter, wateringFunction, 0),
    PB_FIELD( 33, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, bbcGroup, wateringParameter, 0),
    PB_FIELD( 34, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, panId, bbcGroup, 0),
    PB_FIELD( 35, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, channel, panId, 0),
    PB_FIELD( 36, UINT32  , SINGULAR, STATIC  , OTHER, GenericProtocol_BmuConfigRequest, nodeAddress, channel, 0),
    PB_LAST_FIELD
};


/* @@protoc_insertion_point(eof) */
