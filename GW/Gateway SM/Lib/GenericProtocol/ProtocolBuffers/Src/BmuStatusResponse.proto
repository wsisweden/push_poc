syntax = "proto3";

option optimize_for = LITE_RUNTIME;
package GenericProtocol;

// BMU status response
message BmuStatusResponse {
	// Generic BMU
	// The battery state of charge. Unit (%). Res 0 - 100
	uint32 stateOfCharge = 1;
	// The current time in POSIX format
	uint32 currentTime = 2;
	// The current battery current. Unit (A). Res mA (0.001)
	float batteryCurrent = 3;
	// The current battery voltage. Unit (V). Res mV (0.001)
	float batteryVoltage = 4;
	// The current battery temperature. Unit C. Res (0.1)
	float batteryTemperature = 5;
	// The average life time battery temperature. Unit C. Res (0.1)
	float batteryTemperatureAverage = 6;
	// The actual battery capacity. Unit (Ah). Res (0.1)
	float actualCapacity = 7;
	// The total remaining Ah available for the life time of the battery. Unit (Ah)
	uint32 ahLeft = 8;
	// Days left until battery is considered as dead. Unit (Days)
	uint32 timeLeft = 9;
	// Cycles charge level of 2 to 25 % of capacity. Res 0 - 65535
	uint32 cycles2To25 = 10;
	// Cycles charge level of 26 to 50 % of capacity. Res 0 - 65535
	uint32 cycles26To50 = 11;
	// Cycles charge level of 51 to 80 % of capacity. Res 0 - 65535
	uint32 cycles51To80 = 12;
	// Cycles charge level of 81 to 90 % of capacity. Res 0 - 65535
	uint32 cycles81To90 = 13;
	// Cycles charge level above 90 % capacity. Res 0 - 65535
	uint32 cyclesAbove90 = 14;
	// Number of charge cycles stopped before battery is fully charged. Res 0 - 65535
	uint32 stoppedCycles = 15;
	// Cycles left in battery. Res 0 - 65535
	uint32 cyclesLeft = 16;
	// Battery total running time in discharge. Unit (s)
	uint32 totalDischargeTime = 17;
	// Battery total Ah discharged. Unit (Ah)
	uint32 totalDischargeAh = 18;
	// Battery total Wh discharged. Unit (Wh)
	uint32 totalDischargeWh = 19;
	// Battery total running time in charge. Unit (s)
	uint32 totalChargeTime = 20;
	// Battery total Ah charged. Unit (Ah)
	uint32 totalChargeAh = 21;
	// Battery total Wh charged. Unit (Wh)
	uint32 totalChargeWh = 22;
	// Battery total energy consumption from AC. Unit (Wh)
	uint32 totalChargeWhAc = 23;
	// Percent of capacity discharge Ah during 24h. Unit (%). Res 0 - 100
	uint32 ebUnits = 24;
	// Amount of available history logs.
	uint32 historyLogCount = 25;
	// The index of the latest history log, logs are stored in a circular buffer. To read logs the index is needed
	uint32 historyLogIndex = 26;
	// The date of the last history log in POSIX format
	uint32 historyLogDate = 27;
	// Amount of available event logs
	uint32 eventLogCount = 28;
	// The index of the latest event log, logs are stored in a circular buffer
	uint32 eventLogIndex = 29;
	// The date of the last event log in POSIX format
	uint32 eventLogDate = 30;
	// Amount of available instant logs
	uint32 instantLogCount = 31;
	// The index of the latest instant log, logs are stored in a circular buffer
	uint32 instantLogIndex = 32;
	
	// Lead acid BMU
	// The current middle voltage. Unit (V). Res (0.001)
	float middleVoltage = 33;
	// The current electrolyte voltage. Unit (V). Res (0.001)
	float electrolyteVoltage = 34;

	// Access parameters
	// The current active device radio PAN ID. Res 0 - 0xffff
	uint32 panId = 35;
	// The current active device radio channel. Res 11 - 26
	uint32 channel = 36;
  
	// A list of all active alarm codes, see Device alarm specification for codes
	repeated uint32 alarmCodes = 37;
  // A list of all active device status, see Device status specification for ids
  repeated uint32 deviceStatus = 38;
}
