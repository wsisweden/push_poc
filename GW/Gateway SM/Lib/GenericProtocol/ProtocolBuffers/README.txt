If changes has been made on the protocol in the Src directory,
the following command has to be run in the Src directory to generate new code:
protoc --nanopb_out=-T:../Gen *.proto

The protoc tool is available in, nanopb-x.y.z-windows-x86.zip, this folder.