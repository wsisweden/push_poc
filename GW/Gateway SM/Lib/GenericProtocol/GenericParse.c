#include "GenericParse.h"

/**
 * Read uint8_t from payload
 * @param offset The position to start read
 * @param data The resulting data
 * @param payload Read from here
 * @return The size in bytes read
 */
uint8_t GenericReadU8(const uint32_t offset, uint8_t* data,
    const uint8_t* const payload) {
  *data = payload[offset];
  return 1;
}

/**
 * Read uint16_t from payload at offset position
 * @param offset The position to start read
 * @param data The resulting data
 * @param payload Read from here
 * @return The size in bytes read
 */
uint8_t GenericReadU16(const uint32_t offset, uint16_t* data,
    const uint8_t* const payload) {
  *data = (payload[offset] << 8) | payload[offset + 1];
  return 2;
}

/**
 * Read uint32_t from payload at offset position
 * @param offset The position to start read
 * @param data The resulting data
 * @param payload Read from here
 * @return The size in bytes read
 */
uint8_t GenericReadU32(const uint32_t offset, uint32_t* data,
    const uint8_t* const payload) {
  *data = (payload[offset] << 24) | (payload[offset + 1] << 16) |
      (payload[offset + 2] << 8) | payload[offset + 3];
  return 4;
}

/**
 * Read uint64_t from payload at offset position
 * @param offset The position to start read
 * @param data The resulting data
 * @param payload Read from here
 * @return The size in bytes read
 */
uint8_t GenericReadU64(const uint32_t offset, uint8_t* data,
    const uint8_t* const payload) {
  uint8_t i;
  for (i = 0; i < 8; ++i)
    data[i] = payload[offset + i];
  return i;
}

/**
 * Put uint8_t in payload at offset position
 * @param offset The position to start write
 * @param data The data to write
 * @param payload Write to here
 * @return The size in bytes written
 */
uint8_t GenericPutU8(const uint32_t offset, const uint8_t data,
    uint8_t* payload) {
  payload[offset] = data;
  return 1;
}

/**
 * Put uint16_t in payload at offset position
 * @param offset The position to start write
 * @param data The data to write
 * @param payload Write to here
 * @return The size in bytes written
 */
uint8_t GenericPutU16(const uint32_t offset, const uint16_t data,
    uint8_t* payload) {
  uint8_t i = 0;
  GenericPutU8(offset + i++, (uint8_t)((data >> 8) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)(data & 0xff), payload);
  return i;
}

/**
 * Put uint32_t in payload at offset position
 * @param offset The position to start write
 * @param data The data to write
 * @param payload Write to here
 * @return The size in bytes written
 */
uint8_t GenericPutU32(const uint32_t offset, const uint32_t data,
    uint8_t* payload) {
  uint8_t i = 0;
  GenericPutU8(offset + i++, (uint8_t)((data >> 24) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 16) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 8) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)(data & 0xff), payload);
  return i;
}

/**
 * Put uint64_t in payload at offset position
 * @param offset The position to start write
 * @param data The data to write
 * @param payload Write to here
 * @return The size in bytes written
 */
uint8_t GenericPutU64(const uint32_t offset, const uint64_t data,
    uint8_t* payload) {
  uint8_t i = 0;
  GenericPutU8(offset + i++, (uint8_t)((data >> 56) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 48) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 40) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 32) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 24) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 16) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)((data >> 8) & 0xff), payload);
  GenericPutU8(offset + i++, (uint8_t)(data & 0xff), payload);
  return i;
}
