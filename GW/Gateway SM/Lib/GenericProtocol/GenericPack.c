#include "GenericPack.h"
#include "GenericParse.h"
#include <stddef.h>
#include <Crc16/Crc16.h>
#include <pb_encode.h>

// Private typedef

// Private variables

// Private function declarations

static uint8_t* GenericPackGetCmdPtr(GenericPack_t* const pInstance);

// Public functions

/**
 * Initialize the data
 * @param pInstance The instance
 * @param pBuffer Buffer used for the frame
 * @param size The size of the buffer
 */
void GenericPackInit(GenericPack_t* const pInstance, uint8_t* pBuffer,
                     const uint32_t size) {
  pInstance->offset = 0;
  pInstance->chunkPos = 0;
  pInstance->pBuffer = pBuffer;
  pInstance->counter = 0;

  if (size < 6) {
    pInstance->bufferSize = 0;
  } else {
    pInstance->bufferSize = size - 6; // Message + end of frame message
  }
}

/**
 * Start packaging a frame
 * @param pInstance The instance
 */
void GenericPackStartFrame(GenericPack_t* const pInstance) {
  pInstance->offset = 0;
  pInstance->chunkPos = 0;
}

/**
 * Get the max size of the next added command
 * @param pInstance The instance
 * @return The size
 */
uint32_t GenericPackGetCmdMaxSize(GenericPack_t* const pInstance) {
  if (pInstance->offset + END_CMD > pInstance->bufferSize) {
    return 0;
  }

  return pInstance->bufferSize - (pInstance->offset + END_CMD);
}

/**
 * Add command to send queue
 * @param pInstance The instance
 * @param type The type of command
 * @param fields The message fields
 * @param src_struct The message
 * @return If the message was added
 */
bool GenericPackAddCmd(GenericPack_t* const pInstance, const GenericType_t type,
    const pb_field_t fields[], const void *src_struct) {
  uint8_t* data;
  pb_ostream_t stream;
  bool status;
  uint16_t crc;
  uint16_t cmdSize;

  data = GenericPackGetCmdPtr(pInstance);
  stream = pb_ostream_from_buffer(data, GenericPackGetCmdMaxSize(pInstance));
  status = pb_encode(&stream, fields, src_struct);

  if (!status) {
    return false;
  }

  pInstance->offset += CRC_SIZE; // Empty CRC
  pInstance->offset += GenericPutU16(pInstance->offset, (uint16_t)type,
      pInstance->pBuffer);
  pInstance->offset += GenericPutU16(pInstance->offset,
      (uint32_t)stream.bytes_written, pInstance->pBuffer);
  pInstance->offset += stream.bytes_written;

  // Fill in CRC
  cmdSize = stream.bytes_written + CMD_LENGTH_SIZE + CMD_TYPE_SIZE;
  crc = Crc16(&pInstance->pBuffer[pInstance->offset - cmdSize], cmdSize);
  (void)GenericPutU16(pInstance->offset - (cmdSize + CRC_SIZE), crc,
      pInstance->pBuffer);
  return true;
}

/**
 * End the frame, calculate CRC and write length
 * @param pInstance The instance
 */
void GenericPackEndFrame(GenericPack_t* const pInstance) {
  uint16_t crc;
  uint16_t cmdSize;

  pInstance->offset += CRC_SIZE; // Empty CRC
  pInstance->offset += GenericPutU16(pInstance->offset,
      (uint16_t)GenericTypeEndResp, pInstance->pBuffer);
  pInstance->offset += GenericPutU16(pInstance->offset, 0,
      pInstance->pBuffer);

  // Fill in CRC
  cmdSize = CMD_LENGTH_SIZE + CMD_TYPE_SIZE;
  crc = Crc16(&pInstance->pBuffer[pInstance->offset - cmdSize], cmdSize);
  (void)GenericPutU16(pInstance->offset - (cmdSize + CRC_SIZE), crc,
      pInstance->pBuffer);
}

/**
 * Get the next frame chunk
 * @param pInstance The instance
 * @param pBuffer The buffer
 * @param size The size of the chunk
 */
void GenericPackGetFrameChunk(GenericPack_t* const pInstance, uint8_t* pBuffer,
                              const uint32_t size) {
  pBuffer[0] = pInstance->counter++;
  memcpy(&pBuffer[1], &pInstance->pBuffer[pInstance->chunkPos], size - 1);
  pInstance->chunkPos += size - 1;
}

/**
 * Check if any chunk is left to send
 * @param pInstance The instance
 * @return If chunk is left
 */
bool GenericPackIsChunkLeft(GenericPack_t* const pInstance) {
  if (pInstance->chunkPos < pInstance->offset) {
    return true;
  }

  pInstance->offset = 0;
  return false;
}

/**
 * Check if the frame is started
 * @param pInstance The instance
 * @return If frame is started
 */
bool GenericPackIsFrameStarted(GenericPack_t* const pInstance) {
  return pInstance->offset > 0;
}

// Private functions

/**
 * Get pointer to next command
 * @param pInstance The instance
 * @return Pointer to next command in buffer
 */
static uint8_t* GenericPackGetCmdPtr(GenericPack_t* const pInstance) {
  return &pInstance->pBuffer[pInstance->offset + CMD_DATA_POS];
}
