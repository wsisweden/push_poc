syntax = "proto3";
import "ChargerStatusResponse.proto";

option optimize_for = LITE_RUNTIME;
package GenericProtocol;

// Charger status response
message AccessStatusResponse {
	// Generic charger status
	ChargerStatusResponse status = 1;
	// The radio firmware type number, what kind of firmware is on the device
	uint32 radioFirmwareType = 2;
	// The radio firmware version number, which version of the firmware type is on the device
	uint32 radioFirmwareVersion = 3;
	// The current active device radio PAN ID. Res 0 - 0xffff
	uint32 panId = 4;
	// The current active device radio channel. Res 11 - 26 or 0 if inactive
	uint32 channel = 5;
	// A list of all charging algorithms with support for user parameters
	repeated uint32 userParameterCurves = 6;
}
