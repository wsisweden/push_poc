syntax = "proto3";

option optimize_for = LITE_RUNTIME;
package GenericProtocol;

// Charging curve user parameter response
message UserParameterResponse {
	// The type of parameter
	enum Type {
		VOLT_PER_CELL = 0;
		CAPACITY = 1;
		CELL_MUL_CAP = 2;
		SECOND = 3;
		MINUTE = 4;
		HOUR = 5;
		PERCENT = 6;
		INTEGER = 7;
		AH = 8;
	}
  
  // The parameter index
  uint32 index = 1;
  // The parameter name
  string name = 2;
  // The parameter type
	Type type = 3;
  // The parameter value
  float value = 4;
  // The parameter minimum value
  float minValue = 5;
  // The parameter maximum value
  float maxValue = 6;
  // The parameter default value
  float defaultValue = 7;
}

// Charging curve response
message ChargingCurveResponse {
  // The requested charging curve
  uint32 chargingCurve = 1;
  // The charging curve revision
  uint32 revision = 2;
  // The amount of user parameters in the curve
  uint32 userParameterCount = 3;
}

// User parameter
message UserParameter {
  // The parameter index
  uint32 index = 1;
  // The parameter value
  float value = 2;
}

// Charging curve user parameter request
message UserParameterRequest {
  // The charging curve
  uint32 chargingCurve = 1;
  // The charging curve revision
  uint32 revision = 2;
  // The user parameters
  repeated UserParameter userParameters = 3;
}