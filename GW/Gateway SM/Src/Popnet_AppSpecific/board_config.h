/************************************************************************************
* Board Parameters Configuration
*
* (c) Copyright 2008, Freescale, Inc.  All rights reserved.
*
* No part of this document must be reproduced in any form - including copied,
* transcribed, printed or by any electronic means - without specific written
* permission from Freescale.
*
* Last Inspected: 
* Last Tested:
************************************************************************************/
 
 #ifndef __BOARD_CONFIG_H__
 #define __BOARD_CONFIG_H__

#include "MpCfg.h"

/* ARM Targets */
#define MC1322XSRB    0
#define MC1322XNCB    1
#define MC1322XUSB    2
#define MC1322XLPB     3
#define MC1322XUSERDEF 4
#define CELFreeStarPro 24
#define MC1322XMPCMB   30
#define CC2538SF53     32 // SM

#define TARGET_BOARD  CC2538SF53

#define DEFAULT_COARSE_TRIM   0x08 
#define DEFAULT_FINE_TRIM     0x0F

#include <Hw/TIDriverLib/source/sys_ctrl.h>

/*Clock definitions*/
#define PLATFORM_CLOCK (SYS_CTRL_32MHZ)

#endif /* __BOARD_CONFIG_H__ */
