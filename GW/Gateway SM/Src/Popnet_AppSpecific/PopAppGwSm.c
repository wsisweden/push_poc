/***************************************************************************
 PopAppSm.c

 Revision history:
 Rev   Date      Comment   Description
 this  110912    000       first revision
 ***************************************************************************/

#include "PopAppGwSm.h"
#include "../MP/MpGw.h"
#include "../MP/MpNwk.h"
#include "../Tui/Tui.h"
#include "../Message/MessageTask.h"
#include "../Ota/OtaTask.h"
#include "../Nfc/Nfc.h"
#include "../CirrusGw/CirrusGw.h"
#include <Hw/TIDriverLib/source/systick.h>
#include <PopNet/PopNwk.h>
#include <string.h>

/***************************************************************************
 Local Types & Defines
 ***************************************************************************/
// must set gPopMicroPowerExternalFlash_d=1 in compiler and linker options
// Define any event IDs required by the application here...
#define gPopAppFormNetwork_c (gPopEvtUser_c + 32)
#define gPopAppJoinNetwork_c (gPopEvtUser_c + 33)

// Define any application timers here.
#define gPopAppFormNetworkTimerId_c      (gPopAppTimerFirstPos_c + 3)
#define gPopAppJoinNetworkTimerId_c      (gPopAppTimerFirstPos_c + 4)
#define gPopAppNwkDataChangeTimer_c      (gPopAppTimerFirstPos_c + 5)
#define gPopAppAckRspTimer_c             (gPopAppTimerFirstPos_c + 6)

// define any over-the-air command IDs here.
// By convention, application over-the-air payloads start with a command ID byte
#define gPopAppCmdData_c            0x90

// try retries if haven't received the ACK in 2 seconds
#define gPopAppLedBlinkTime_c              100
#define gPopAppBeaconIndicationTime_c      1000
#define gPopAppBusyTime_c                  5000
#define gPopAppFormNetworkTime_c           30000
#define gPopAppJoinNetworkTime_c           30000

/***************************************************************************
 Prototypes
 ***************************************************************************/
void PopAppHandleTimer(popTimerId_t iTimerId);
bool PopAppValidNwkData(void);
void PopAppSetNewState(popStatus_t iNwkStartConfirm);
void PopAppScanForParentHandling(
    sPopNwkBeaconIndication_t *pNwkBeaconIndication);
void PopAppScanForParentConfirm(sPopAppBeaconIndication_t *pBeacon);
void PopAppFormNetwork(sPopEvtData_t *pData);
void PopAppJoinNetwork(sPopEvtData_t *pData);
void PopAppJoinIndication(void);
void PopAppDataIndication(sPopNwkDataIndication_t *pIndication,
    bool *pSendThruGateway);
void PopAppGetAddrPoolAddr(void);
void PopAppAddrPoolAddr(uint16_t iDstAddr, uint8_t iMpSequence,
    uint16_t iAddrToSpare, uint16_t iFirstAvailableAddr);
void PopAppAcknowledge(uint16_t iDstAddr, uint8_t iMpSequence,
    uint8_t popNwkDataReqOpts, uint8_t iReqCmd, uint8_t iStatus);
bool MpEventIsOutOfRange(popEvtId_t iEvtId);

// PopNet Task prototypes
void PopPhyTaskInit(void);
void PopPhyTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopBspTaskInit(void);
void PopBspTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopSerialTaskInit(void);
void PopSerialTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopNwkTaskInit(void);
void PopNwkTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void PopAppTaskInit(void);
void PopAppTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);

/***************************************************************************
 Globals and Externals
 ***************************************************************************/
/* PopNet */
// every application has a list of tasks called gaPopNetTasks[]
const sPopTaskList_t gaPopNetTaskList[] = {
// do NOT reorder these tasks without reordering the task ID
// constants below (cPhyTaskId)
    { PopPhyTaskInit, PopPhyTaskEventLoop },       // PopNet physical layer task
    { PopBspTaskInit, PopBspTaskEventLoop },       // PopNet BSP task
    { PopNwkTaskInit, PopNwkTaskEventLoop },       // PopNet networking layer task
    { PopSerialTaskInit, PopSerialTaskEventLoop }, // Serial I/O task
    { PopAppTaskInit, PopAppTaskEventLoop },       // application task
    { MpGwTaskInit, MpGwTaskEventLoop },   	       // Micropower gateway task
    { MpNwkTaskInit, MpNwkTaskEventLoop },   	     // Micropower network task
    { TuiTaskInit, TuiTaskEventLoop },             // Tiny user interface task
    { MessageTaskInit, MessageTaskEventLoop },     // Message interface task
    { OtaTaskInit, OtaTaskEventLoop },             // OTA interface task
    { NfcTaskInit, NfcTaskEventLoop }              // NFC interface task
};
const uint8_t cPopNumTasks = PopNumElements(gaPopNetTaskList);

// task ID constants (must match the above list)
const popTaskId_t cPhyTaskId = 0;
const popTaskId_t cBspTaskId = 1;
const popTaskId_t cNwkTaskId = 2;
const popTaskId_t gPopPhyIndicationTask = 2; // MUST be same as cNwkTaskId
const popTaskId_t cSerialTaskId = 3;
const popTaskId_t cAppTaskId = 4;
const popTaskId_t cGwTaskId = 5;
const popTaskId_t cMpNwkTaskId = 6;
const popTaskId_t cTuiTaskId = 7;
const popTaskId_t cMessageTaskId = 8;
const popTaskId_t cOtaTaskId = 9;
const popTaskId_t cNfcTaskId = 10;

/* Mp */
uint16_t giState; //  declaration of the global state
bool gfMpIsProcessingScanConfirm = false; // set if gPopEvtNwkScanConfirm_c should be handled
bool gfMpBeaconFound = false; // number of beacons in list (with join enable)

uint32_t giFwType = 11618015;
uint32_t giFwVer = 203;
uint32_t giChkSum = 0x0A0B0C0D;
uint8_t gaGwEvtLimit[] = { 0x41, 0x4e, 0x4f, 0x51, 0x6f, 0x70, 0x71, 0x71, 0x72,
    0x73, 0x74, 0x75, 0x77, 0x78 };

uint8_t gCmConfigCtr = 0; //debug

sPopAppNwkInfo_t gsPopAppParentNwkInfo; // global struct for saving nwk info of parent node
sMpJoinNetworkReq_t gsPopAppJoinReqNwkInfo; // global struct for saving nwk info from join request
sPopAppMsgAckData_t gsPopAppMsgAckData; // global struct for ack rsp data
sPopAppBeaconIndication_t gsPopAppBeaconIndication; // global struct for beacons with join enable

#if gPopICanHearYouEntries_c
#warning debug
popNwkAddr_t gaAppICanHearYouTable[] = {0x0002, 0x0002};
#endif
/***************************************************************************
 Code
 ***************************************************************************/

/*
 PopAppTaskInit
 */
void PopAppTaskInit(void) {
#if gPopICanHearYouEntries_c
#warning debug
  if( giPopNwkICanHearYouEntries ) //Set to 2
  {
    PopNwkSetICanHearYou(PopNumElements(gaAppICanHearYouTable), gaAppICanHearYouTable);
  }
#endif
  //006 Set initial state
  giState = INIT;
  // set the event range for gateway
  PopGatewaySetEventRange(0xFF, 0xFF);
  // Read Nwk data from NVM
  (void) PopNvRetrieveNwkData();

  // Generate a random node address 0xff01 - 0xfff0
  PopNwkSetNodeAddr(0xff00 | ((PopRandom08() % 0xf0) + 1));

#if DEBUG == 1
#warning debug
  PopNwkSetChannel(gPopAppChannel_c);
  PopNwkSetPanId(gPopAppPanId_c);
  PopNwkSetNodeAddr(gPopAppNodeAddr_c);

  PopNwkStartNetwork();
#else
  // If valid network data stored in NVM
  if (PopAppValidNwkData()) {
    PopNwkStartNetwork();
  } else {
    giState = NOT_CONNECTED;
  }
#endif

  CirrusGwInit();
}

/*
 PopAppTaskEventLoop

 All events from PopNet to the application occur here. The iEvtId is a number
 from 0x00-0xff (not a bit mask). The contents of sEvtData depends on the iEvtId.
 See type sPopEvtData_t in PopNet.h.
 */
void PopAppTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  switch (giState) {
    case INIT:
      switch (iEvtId) {
        case gPopEvtNwkStartConfirm_c:
          // check if succeeded and set state thereafter
          PopAppSetNewState(sEvtData.iNwkStartConfirm);
          break;
        default:
          // Nothing to do
          break;
      }
      break;

    case NOT_CONNECTED:
      switch (iEvtId) {
        case gPopEvtNwkScanConfirm_c:
          MpGwScanNetworkRsp();
          break;
        default:
          // Nothing to do
          break;
      }
      break;

    case START:
      switch (iEvtId) {
        case gPopEvtNwkStartConfirm_c:
          // check if succeeded and set state thereafter
          PopAppSetNewState(sEvtData.iNwkStartConfirm);
          // answer with StartNetworkRsp since state = START
          if (sEvtData.iNwkStartConfirm != gPopStatusBusy_c) {
            MpGwStartNetworkRsp(sEvtData.iNwkStartConfirm);
          }
          break;
        case gPopAppFormNetwork_c:
          PopAppFormNetwork(sEvtData.pData);
          PopMemFree(sEvtData.pData);
          break;
        default:
          // Nothing to do
          break;
      }
      break;

    case JOIN:
      switch (iEvtId) {
        case gPopEvtNwkStartConfirm_c:
          // check if succeeded and set state thereafter
          PopAppSetNewState(sEvtData.iNwkStartConfirm);
          // answer with JoinNetworkRsp since state = JOIN
          if (sEvtData.iNwkStartConfirm != gPopStatusBusy_c) {
            // Send gateway response
            MpGwJoinNetworkRsp(sEvtData.iNwkStartConfirm);
            if (sEvtData.iNwkStartConfirm == gPopStatusJoinSuccess_c) {
              // Send data OTA to inform parent that joining was successfull
              PopAppGetAddrPoolAddr();
            }
          }
          break;
        case gPopEvtNwkScanConfirm_c:
          if (gfMpIsProcessingScanConfirm) {
            PopAppScanForParentConfirm(&gsPopAppBeaconIndication);
          }
          break;
        case gPopAppJoinNetwork_c:
          PopAppJoinNetwork(sEvtData.pData);
          break;
        default:
          // Nothing to do
          break;
      }
      break;

    case CONNECTED:
      switch (iEvtId) {
        // a message has come in on the radio
        case gPopEvtDataIndication_c: {
          if (PopNwkPayloadLen(sEvtData.pDataIndication) <= MAX_PAYLOAD) {
            bool SendThruGateway = true;

            // 002 Handle DataIndication in MpNwk
            MpNwkDataIndication(sEvtData.pDataIndication, &SendThruGateway);
            PopAppDataIndication(sEvtData.pDataIndication, &SendThruGateway);
            // Check if message should be sent thru gateway
            if (SendThruGateway) {
              // send thru gateway
              MpGwDataIndication(sEvtData.pDataIndication);
            }
          }
        }
          break;
          // when data has been sent with PopNwkDataRequest(), this confirm indicates
          // the request has been sent out this radio (but not necessarily delivered
          // to the final destination).
        case gPopEvtDataConfirm_c:
          // 003 Handle DataConfirm in MpNwk
          MpNwkDataConfirm(sEvtData.iDataConfirm);
          break;
          // Do not store routes to NVM
#if 0
          case gPopEvtNvmDataChanged_c:
          if (PopNvmCheckBitOnMask(gPopNvmRoutesChanged_c))
          {
            /*
             When receiving this event indicates that routing tables has been modifyed
             and it needs to be saved to NVM
             */
            // Save the Nwk data 4 second (4000ms) after it has change for the last time.
            (void)PopStartTimer(gPopAppNwkDataChangeTimer_c, 4000);
          }
          break;
#endif
          // 006 Handle initiate route event
        case gPopEvtNwkInitiateRoute_c:
          MpNwkInitiateRouteHandling();
          break;
          // 006 Handle route onfirm event
        case gPopEvtNwkRouteConfirm_c:
          MpNwkEndRouteHandling(sEvtData.iWord);
          break;
        case gPopEvtNwkLeaveConfirm_c:
          // set state to NotConnected
          PopAppSetNewState(gMpLeaveNetworkReq_c);
          MpGwLeaveNetworkRsp();
          break;
        case gPopEvtNwkScanConfirm_c:
          MpGwScanNetworkRsp();
          break;
        case gPopEvtNwkNodePlacement_c:
          if (PopNwkInNodePlacementMode()) {
            MpGwNodePlacementRsp(sEvtData.pPopNodePlacementResult);
          }
          break;
        default:
          // Nothing to do
          break;
      }
      break;

    default:
      // No valid state. Reset MCU and store assert
      PopAssert(gPopAssertBadReceivePtr_c);
      break;
  }

  /******************************************************************************/
  // General events outside state machine
  switch (iEvtId) {

      // beacon indication  available in state = NOT_CONNECTED, CONNECTED, START, JOIN
    case gPopEvtNwkBeaconIndication_c:
      // process beacon indications
      if (gfMpGwIsProcessingBeacon_c) {
        MpGwScanNetworkHandling(sEvtData.pNwkBeaconIndication);
      } else if (gfMpIsProcessingScanConfirm) {
        PopAppScanForParentHandling(sEvtData.pNwkBeaconIndication);
      }
      break;

      // Timer handling handled outside state machine
    case gPopEvtTimer_c:
      PopAppHandleTimer(sEvtData.iTimerId);
      break;

    default:
      // Corrupt data received, free memory allocated from heap to avoid loss of memory
      PopMemFree(sEvtData.pData);
      break;

  }

  /******************************************************************************/
  // always call the default handler to free messages and clean up.
  PopAppDefaultHandler(iEvtId, sEvtData);
}

/*
 PopAppHandleTimer

 A timer has expired. Handle it.
 o  The application may use up to 255 timers
 o  The maximum number of timers is set in PopCfg.h (gPopMaxTimers_c)
 o  The timers are not real-time
 */
void PopAppHandleTimer(popTimerId_t iTimerId) {
  switch (iTimerId) {
    case gPopAppFormNetworkTimerId_c:
      // reset state to NOT_CONNECTED
      PopNwkScanComplete();
      PopAppSetNewState(gPopErrFailed_c);
      MpGwStartNetworkRsp(gPopErrFailed_c);
      break;

    case gPopAppJoinNetworkTimerId_c:
      // reset state to NOT_CONNECTED
      PopNwkScanComplete();
      PopAppSetNewState(gPopErrFailed_c);
      MpGwJoinNetworkRsp(gPopErrFailed_c);
      break;

    case gPopAppNwkDataChangeTimer_c: {
      // Time to store network data
      (void) PopNvStoreNwkData();
    }
      break;

    case gPopAppAckRspTimer_c: {
      // Send ack response
      PopAppAcknowledge(gsPopAppMsgAckData.iDstAddr,
          gsPopAppMsgAckData.iMpSequence, gsPopAppMsgAckData.iOptions,
          gsPopAppMsgAckData.iReqCmd, gsPopAppMsgAckData.iStatus);
    }
      break;

    case gPopTimerIdMemAllocFail_c: // Reset flag for MemAllocFail
      MpNwkMemInit(); // reset MP message memory
      break;

    default:
      break;

  }
}

/*******************************************************************************
 ---------------- PopAppValidNwkData -----------------------------------------------
 Checks if Network data is within limits and returns true if so.
 -------------------------------------------------------------------------------
 *******************************************************************************/
bool PopAppValidNwkData(void) {

  // retrieve any stored NWK data (e.g., PAN ID, channel, nwk key, etc...)
  // this must occur before starting the network
  if ((11 <= PopNwkGetChannel() && PopNwkGetChannel() <= 26)
      && (PopNwkGetPanId() < gPopNwkReservedPan_c)
      && (PopNwkGetNodeAddr() < gPopNwkReservedAddr_c)) {
    return true;
  }

  return false;
}

/*******************************************************************************
 ---------------- PopAppSetNewState ------------------------------------------
 Checks if Network has been successfully started or not and set state
 accordingly. If busy start a timer which will reset state to NOT_CONNECTED if
 no other event has been received
 -------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppSetNewState(popStatus_t iNwkStartConfirm) {

  // Check if network has started
  switch (iNwkStartConfirm) {
    case gPopStatusJoinSuccess_c:
      PopStopTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c);
      giState = CONNECTED;  // Set state to Connected
      (void) PopNvStoreNwkData();  // store network data
      break;

    case gPopStatusFormSuccess_c:
      PopStopTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c);
      giState = CONNECTED;  // Set state to Connected
      PopNwkSetAddrCount(gMpSmNwkReservedAddr_c - 1); // addr pool 0x0001 - 0xFEFF
      PopNwkSetNextAddrAvailable(0x0001); // First available addr
      (void) PopNvStoreNwkData();  // store network data
      break;

    case gPopStatusAlreadyOnTheNetwork_c:
    case gPopStatusSilentStart_c:
      PopStopTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c);
      PopStopTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c);
      giState = CONNECTED;
      break;

    case gPopErrFailed_c:
    case gPopStatusNoMem_c:
    case gPopStatusBadParms_c:
    case gPopStatusPanAlreadyExists_c:
    case gPopStatusJoinFailure_c:
    case gPopStatusJoinDisabled_c:
    case gPopStatusWrongMfgId_c:
    case gPopStatusNoParent_c:
      PopStopTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c);
      PopStopTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c);
      giState = NOT_CONNECTED;
      break;

    case gPopStatusBusy_c:
      ;
      break;

    case gMpStartNetworkReq_c:
      // start timer which will reset state to NOT_CONNECTED when fired
      PopStartTimerEx(cAppTaskId, gPopAppFormNetworkTimerId_c,
      gPopAppFormNetworkTime_c);
      giState = START;
      break;

    case gMpJoinNetworkReq_c:
      // start timer which will reset state to NOT_CONNECTED when fired
      PopStartTimerEx(cAppTaskId, gPopAppJoinNetworkTimerId_c,
      gPopAppJoinNetworkTime_c);
      giState = JOIN;
      break;

    case gMpLeaveNetworkReq_c:
      giState = NOT_CONNECTED; // set state to NotConnected
      PopNwkSetAddressServer(0x0000); // set back to default
      PopNwkSetNextAddrAvailable(0x0000); // reset
      PopNwkSetAddrCount(0x0000); // reset
      (void) PopNvStoreNwkData();  // store network data
      break;

    default:
      giState = NOT_CONNECTED;
      break;
  }
}

/*******************************************************************************
 ---------------- PopAppScanForParentHandling ---------------------------------------
 popEvtNwkBeaconIndication event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppScanForParentHandling(
    sPopNwkBeaconIndication_t *pNwkBeaconIndication) {

  if ((pNwkBeaconIndication->iProtocolVer & 0x80)
      && (gfMpBeaconFound != true)) {
    // copy beacon to global struct
    gsPopAppBeaconIndication.iChannel = pNwkBeaconIndication->iChannel;
    gsPopAppBeaconIndication.iLqi = pNwkBeaconIndication->iLqi;
    gsPopAppBeaconIndication.iNwkAddr = pNwkBeaconIndication->iNwkAddr;
    gsPopAppBeaconIndication.iPanId = pNwkBeaconIndication->iPanId;
    gsPopAppBeaconIndication.iProtocol = pNwkBeaconIndication->iProtocol;
    gsPopAppBeaconIndication.iProtocolVer = pNwkBeaconIndication->iProtocolVer;
    gsPopAppBeaconIndication.aMacAddr[0] = pNwkBeaconIndication->aMacAddr[0];
    gsPopAppBeaconIndication.aMacAddr[1] = pNwkBeaconIndication->aMacAddr[1];
    gsPopAppBeaconIndication.aMacAddr[2] = pNwkBeaconIndication->aMacAddr[2];
    gsPopAppBeaconIndication.aMacAddr[3] = pNwkBeaconIndication->aMacAddr[3];
    gsPopAppBeaconIndication.aMacAddr[4] = pNwkBeaconIndication->aMacAddr[4];
    gsPopAppBeaconIndication.aMacAddr[5] = pNwkBeaconIndication->aMacAddr[5];
    gsPopAppBeaconIndication.aMacAddr[6] = pNwkBeaconIndication->aMacAddr[6];
    gsPopAppBeaconIndication.aMacAddr[7] = pNwkBeaconIndication->aMacAddr[7];
    gsPopAppBeaconIndication.iMfgId = pNwkBeaconIndication->iMfgId;
    gsPopAppBeaconIndication.iAppId = pNwkBeaconIndication->iAppId;
    // Set flag for beacon found
    gfMpBeaconFound = true;
  }

}

/*******************************************************************************
 ---------------- PopAppScanForParentConfirm ---------------------------------------
 PopAppScanForParentConfirm event handling.
 If State = JOIN. Find a node with join enabled and call JoinNetwork
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppScanForParentConfirm(sPopAppBeaconIndication_t *pBeacon) {

  if (giState == JOIN) {

    bool ParentFound;
    ParentFound = false;

    // find the network to join
    if (gfMpBeaconFound) {
      // verify this is PopNet, and our MfgId, and has join enabled and the right app ID
      if (pBeacon->iProtocol == 0x50 &&
      /*pBeacon->iAppId == 0x0002 &&*/
      pBeacon->iMfgId == gPopMfgId_c && (pBeacon->iProtocolVer & 0x80)) {
        // Check if specific channel was requested
        if (gsPopAppJoinReqNwkInfo.iChannel != 0) {
          // compare found channel with requested
          if (gsPopAppJoinReqNwkInfo.iChannel == pBeacon->iChannel) {
            // Check if specific PanId requested
            if (gsPopAppJoinReqNwkInfo.iPanId != 0xFFFF) {
              // Compare found PanId with requested
              if (gsPopAppJoinReqNwkInfo.iPanId == pBeacon->iPanId) {
                // Parent found
                ParentFound = true;

              }  // Pan Id did not match requested
              else {
                // Parent not found (yet)
                ;
              }

            }  // PanId is don't care
            else {
              // Parent found
              ParentFound = true;
            }

          }  // Channel did not match requested
          else {
            // Parent not found (yet)
            ;
          }

        }  // Channel is don't care
        else {
          // Check if specific PanId requested
          if (gsPopAppJoinReqNwkInfo.iPanId != 0xFFFF) {
            // Compare found PanId with requested
            if (gsPopAppJoinReqNwkInfo.iPanId == pBeacon->iPanId) {
              // Parent found
              ParentFound = true;

            }  // Pan Id did not match requested
            else {
              // Parent not found (yet)
              ;
            }

          }  // PanId is don't care
          else {
            // Parent found
            ParentFound = true;
          }

        }

      }

    }  // End if beacon found

    // Check if parent found
    if (ParentFound) {
      // set global parent info and try to join this node
      gsPopAppParentNwkInfo.iChannel = pBeacon->iChannel;
      gsPopAppParentNwkInfo.iPanId = pBeacon->iPanId;
      gsPopAppParentNwkInfo.iNwkAddr = pBeacon->iNwkAddr;
      PopSetEventEx(cAppTaskId, gPopAppJoinNetwork_c, &gsPopAppParentNwkInfo);

    }  // No network found, send response directly!
    else {
      PopAppSetNewState(gPopErrFailed_c);
      MpGwJoinNetworkRsp(gPopStatusNoParent_c);
    }

  }  // End JOIN
  gfMpIsProcessingScanConfirm = false; // now it is taken care of
  gfMpBeaconFound = false;
}

/*******************************************************************************
 ---------------- PopAppFormNetwork ----------------------------------------------
 PopAppFormNetwork event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppFormNetwork(sPopEvtData_t *pData) {
  sPopAppNwkInfo_t *pPopAppNwkParams = (sPopAppNwkInfo_t*) pData;
  // Form network using JoinOptions normal
  PopNwkFormNetwork(PopNwkGetJoinOptions(), pPopAppNwkParams->iChannel,
      pPopAppNwkParams->iPanId);

}

/*******************************************************************************
 ---------------- PopAppJoinNetwork ----------------------------------------------
 PopAppJoinNetwork event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppJoinNetwork(sPopEvtData_t *pData) {
  sPopAppNwkInfo_t *pPopAppNwkParams = (sPopAppNwkInfo_t*) pData;
  // call Join network
  PopNwkJoinNetwork(PopNwkGetJoinOptions(), pPopAppNwkParams->iChannel,
      pPopAppNwkParams->iPanId, pPopAppNwkParams->iNwkAddr);
}

/*******************************************************************************
 ---------------- MpAppJoinIndication -------------------------------------------
 Send MpAppJoinIndication message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppJoinIndication(void) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMessage_t sMessage;
  uint8_t *pData;

  sMpMsgHeaderRsp.iCmd = gJoinIndication_c;
  sMpMsgHeaderRsp.iMpSequence = MessageStepMpSequence();
  sMpMsgHeaderRsp.iControl = 0x00;

  // add JoinIndication data
  // no data

  // add network data
  sMessage.iDstAddr = gsPopAppParentNwkInfo.iNwkAddr;
  sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  // send the message
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppGetAddrPoolAddr ------------------------------------------
 Build and send PopAppGetAddrPoolAddr message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppGetAddrPoolAddr(void) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMessage_t sMessage;
  uint8_t *pData;

  sMpMsgHeaderRsp.iCmd = gGetAddrPoolAddr_c;
  sMpMsgHeaderRsp.iMpSequence = MessageStepMpSequence();
  sMpMsgHeaderRsp.iControl = 0x00;

  // add GetAddrPoolAddr data
  // no data

  // add network data
  sMessage.iDstAddr = gsPopAppParentNwkInfo.iNwkAddr;
  sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  // send the message
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppAddrPoolAddr ---------------------------------------------
 Build and send PopAppAddrPoolAddr message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppAddrPoolAddr(uint16_t iDstAddr, uint8_t iMpSequence,
    uint16_t iAddrToSpare, uint16_t iFirstAvailableAddr) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMpMsgAddrPoolAddr_t sMpMsgAddrPoolAddr;
  sMessage_t sMessage;
  uint8_t *pData;

  // add header
  sMpMsgHeaderRsp.iCmd = gAddrPoolAddr_c;
  sMpMsgHeaderRsp.iMpSequence = iMpSequence;
  sMpMsgHeaderRsp.iControl = 0x00;

  // add AddrPoolAddr data
  sMpMsgAddrPoolAddr.iAddrPoolSize = iAddrToSpare;
  sMpMsgAddrPoolAddr.iFirstAddr = iFirstAvailableAddr;

  // add network data
  sMessage.iDstAddr = iDstAddr;
  sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t)
      + sizeof(sMpMsgAddrPoolAddr_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)), &sMpMsgAddrPoolAddr,
      sizeof(sMpMsgAddrPoolAddr_t));
  // send the response
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppAcknowledge ------------------------------------------
 Build and send PopAppAcknowledge message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppAcknowledge(uint16_t iDstAddr, uint8_t iMpSequence,
    uint8_t popNwkDataReqOpts, uint8_t iReqCmd, uint8_t iStatus) {
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMpMsgAcknowledge_t sMpMsgAcknowledge;
  sMessage_t sMessage;
  uint8_t *pData;
  CirrusGwConfig_t* config = CirrusGwGetConfig();

  sMpMsgHeaderRsp.iCmd = gAcknowledge_c;
  sMpMsgHeaderRsp.iMpSequence = iMpSequence;
  sMpMsgHeaderRsp.iControl = 0x00;

  // add  data
  sMpMsgAcknowledge.iReqCmd = iReqCmd;
  sMpMsgAcknowledge.iStatus = iStatus;
  sMpMsgAcknowledge.type = 5;
  sMpMsgAcknowledge.id = config->deviceId;

  // add network data
  sMessage.iDstAddr = iDstAddr;
  sMessage.iOptions = popNwkDataReqOpts;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t)
      + sizeof(sMpMsgAcknowledge_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
  PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)), &sMpMsgAcknowledge,
      sizeof(sMpMsgAcknowledge_t));
  // send the message
  MpNwkDataRequest(&sMessage);
}

/*******************************************************************************
 ---------------- PopAppDataIndication -------------------------------------------
 PopAppDataIndication event handling.
 --------------------------------------------------------------------------------
 *******************************************************************************/
void PopAppDataIndication(sPopNwkDataIndication_t *pIndication,
    bool *pSendThruGateway) {
  sMessage_t sMessage;
  sMpMsgHeader_t sMpMsgHeaderReq;
  sMpMsgHeader_t sMpMsgHeaderRsp;
  sMpMsgAcknowledge_t sMpMsgAcknowledge;
  uint8_t *pData;
  uint8_t *pPayload;
  uint8_t iPayloadLen;
  uint8_t MsgType;

  pPayload = PopNwkPayload(pIndication);
  iPayloadLen = PopNwkPayloadLen(pIndication);
  MsgType = MpNwkGetMsgType(pPayload);

  // Check if response or request
  switch (MsgType) {
    case RspMsg_c:
      sMpMsgHeaderRsp.iCmd = pPayload[iCmdOffset_c];
      sMpMsgHeaderRsp.iMpSequence = pPayload[iMpSeqOffset_c];

      switch (sMpMsgHeaderRsp.iCmd) {

        case 0x52:  // CmConfig
          gCmConfigCtr++; //debug
          if (gCmConfigCtr == 4) {
            ; //gCmConfigCtr = 0;
          }
          break;

        case gAddrPoolAddr_c: {
          uint16_t iAddrPoolSize;
          uint16_t iFirstAvailableAddr;

          // This message should not be sent thru gateway
          *pSendThruGateway = false;

          // Get data from message
          pData = &pPayload[aDataOffset_c];

          iAddrPoolSize = (uint16_t) (pData[ADDR_POOL_ADDR_ADDRPOOLSIZE_H]);
          iAddrPoolSize |=
              (uint16_t) (pData[ADDR_POOL_ADDR_ADDRPOOLSIZE_L] << 8);

          iFirstAvailableAddr = (uint16_t) (pData[ADDR_POOL_ADDR_FIRSTADDR_H]);
          iFirstAvailableAddr |= (uint16_t) (pData[ADDR_POOL_ADDR_FIRSTADDR_L]
              << 8);

          //store Addr pool info
          PopNwkSetAddrCount(iAddrPoolSize);
          PopNwkSetNextAddrAvailable(iFirstAvailableAddr);
          // store network data to NVM
          (void) PopNvStoreNwkData();

          // Send gateway response
          //          MpGwJoinNetworkRsp(gPopStatusJoinSuccess_c); debug 110203
          MpGwGetRadioDataRsp();  //debug 110203

          // Send data OTA to inform parent that joining was successfull
          PopAppJoinIndication();
        }
          break;

        case gAcknowledge_c: {
          uint8_t iReqCmd;

          // Get data from message
          pData = &pPayload[aDataOffset_c];
          iReqCmd = (uint8_t) (pData[ACK_REQCMD]);

          if (iReqCmd == gSmGetAcknowledge_c) {
            // This message should not be sent thru gateway
            *pSendThruGateway = false;
            MpGwScanForNodesHandling(pIndication, iPayloadLen);
            break;
          }

          break;
        }

        default:
          // Nothing to do
          break;

      }
      break;

    case ReqMsg_c:
      sMpMsgHeaderReq.iCmd = pPayload[iCmdOffset_c];
      sMpMsgHeaderReq.iMpSequence = pPayload[iMpSeqOffset_c];

      switch (sMpMsgHeaderReq.iCmd) {

      case gSmGetInfo_c: {
          sMpMsgGetConnectedInfo_t info;

          // This message should not be sent thru gateway
          *pSendThruGateway = false;

          // add header
          sMpMsgHeaderRsp.iCmd = gGetConnectedInfo_c;
          sMpMsgHeaderRsp.iMpSequence = sMpMsgHeaderReq.iMpSequence;
          sMpMsgHeaderRsp.iControl = 0x00;

          PopNwkGetMacAddr(info.aId);
          info.iUserLevel = MessageGetUserLevel();

          // add network data
          sMessage.iDstAddr = PopNwkSrcAddr(pIndication);
          sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;
          sMessage.iRadius = PopNwkGetDefaultRadius();
          sMessage.iPayloadLength = sizeof(sMpMsgHeader_t) + sizeof(sMpMsgSmInfo_t);
          pData = sMessage.aPayload;
          PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
          PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)), &info, sizeof(sMpMsgSmInfo_t));

          // send the response
          MpNwkDataRequest(&sMessage);
          break;
      }

        case gGetAddrPoolAddr_c: {
          uint16_t iAddrToSpare = 0;
          uint16_t iFirstAvailableAddr = 0;

          // This message should not be sent thru gateway
          *pSendThruGateway = false;

          // Get own addr pool info
          iFirstAvailableAddr = PopNwkGetNextAddrAvailable(); /* Get first available addr in addr pool */
          iAddrToSpare = PopNwkGetAddrCount() / PART_OF_ADDRPOOL; /* Get 10 % of addresses in the address pool */

          // Compare with limits
          if (iAddrToSpare > ADDR_TO_SPARE_MAX) {
            iAddrToSpare = ADDR_TO_SPARE_MAX;
          } else if (iAddrToSpare < ADDR_TO_SPARE_MIN) {
            iAddrToSpare = 0;
          }

          // update own addrs pool info
          PopNwkSetNextAddrAvailable(iFirstAvailableAddr + iAddrToSpare);
          PopNwkSetAddrCount(PopNwkGetAddrCount() - iAddrToSpare);
          // store network data to NVM
          (void) PopNvStoreNwkData();

          // Send gateway response
          MpGwGetRadioDataRsp();

          // Send AddrPoolAddr as response
          uint16_t iDstAddr = PopNwkSrcAddr(pIndication);
          uint8_t iMpSequence = sMpMsgHeaderReq.iMpSequence;
          PopAppAddrPoolAddr(iDstAddr, iMpSequence, iAddrToSpare,
              iFirstAvailableAddr);
          break;
        }

        case gSmGetAcknowledge_c: {
          uint8_t iRandom;
          popTimeOut_t iPopAppAckRspDelay;
          // This message should not be sent thru gateway
          *pSendThruGateway = false;

          gsPopAppMsgAckData.iDstAddr = PopNwkSrcAddr(pIndication);
          gsPopAppMsgAckData.iOptions = gPopNwkDataReqOptsUnibroadcast_c;
          gsPopAppMsgAckData.iReqCmd = sMpMsgHeaderReq.iCmd;
          gsPopAppMsgAckData.iMpSequence = sMpMsgHeaderReq.iMpSequence;
          gsPopAppMsgAckData.iStatus = 0; // Ok

          // Get random delay 0-255*30 (0-7650ms)
          iRandom = PopRandom08();
          iPopAppAckRspDelay = (iRandom * 30);
          // start the timer for delay before sending response
          PopStartTimer(gPopAppAckRspTimer_c, iPopAppAckRspDelay);
          break;
        }

        case gSetCirrusParamReq_c: {
          sMpMsgSetCirrusParamReq_t* setCirrusParamReq =
            (sMpMsgSetCirrusParamReq_t*)&pIndication->sFrame.aPayload[MP_ACCESS_DATA];
          bool status;

          SwapBytes32(&setCirrusParamReq->index);
          status = CirrusGwSetParam(setCirrusParamReq->index,
                                    setCirrusParamReq->data, iPayloadLen - 4 - MP_ACCESS_DATA);

          // This message should not be sent thru gateway
          *pSendThruGateway = false;

          // add header
          sMpMsgHeaderRsp.iCmd = gAcknowledge_c;
          sMpMsgHeaderRsp.iMpSequence = sMpMsgHeaderReq.iMpSequence;
          sMpMsgHeaderRsp.iControl = 0x00;

          // add network data
          sMpMsgAcknowledge.iReqCmd = gSetCirrusParamReq_c;
          sMpMsgAcknowledge.iStatus = status ? 0 : 0xff;
          sMessage.iDstAddr = PopNwkSrcAddr(pIndication);
          sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;
          sMessage.iRadius = PopNwkGetDefaultRadius();
          sMessage.iPayloadLength = sizeof(sMpMsgHeader_t) +
            sizeof(sMpMsgAcknowledge_t);
          pData = sMessage.aPayload;
          PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
          PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)),
                    &sMpMsgAcknowledge, sizeof(sMpMsgAcknowledge_t));

          // send the response
          MpNwkDataRequest(&sMessage);
          break;
        }

        case gGetCirrusParamReq_c: {
          sMpMsgGetCirrusParamReq_t* getCirrusParamReq =
            (sMpMsgGetCirrusParamReq_t*)&pIndication->sFrame.aPayload[MP_ACCESS_DATA];
          sMpMsgGetCirrusParamResp_t getCirrusParamResp;
          uint32_t paramSize;

          // This message should not be sent thru gateway
          *pSendThruGateway = false;

          // add header
          sMpMsgHeaderRsp.iCmd = gGetCirrusParamResp_c;
          sMpMsgHeaderRsp.iMpSequence = sMpMsgHeaderReq.iMpSequence;
          sMpMsgHeaderRsp.iControl = 0x00;

          // add network data
          SwapBytes32(&getCirrusParamReq->index);
          getCirrusParamResp.index = getCirrusParamReq->index;
          paramSize = CirrusGwGetParam(getCirrusParamResp.index,
                                       getCirrusParamResp.data);
          CirrusGwNetworkFormatParam(getCirrusParamResp.index,
                                       getCirrusParamResp.data);
          SwapBytes32(&getCirrusParamResp.index);

          sMessage.iDstAddr = PopNwkSrcAddr(pIndication);
          sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;
          sMessage.iRadius = PopNwkGetDefaultRadius();
          sMessage.iPayloadLength = sizeof(sMpMsgHeader_t) + 4 + paramSize;
          pData = sMessage.aPayload;
          PopMemCpy(pData, &sMpMsgHeaderRsp, sizeof(sMpMsgHeader_t));
          PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)),
                    &getCirrusParamResp, sizeof(getCirrusParamResp));

          // send the response
          MpNwkDataRequest(&sMessage);
          break;
        }

        default:
        // Nothing to do
          break;
      }
      break;
    default:
      // Nothing to do
      break;
  }

}

/*******************************************************************************
 ---------------- MpEventIsOutOfRange -------------------------------------------
 Decides if gw event is to be handled.
 --------------------------------------------------------------------------------
 *******************************************************************************/
bool MpEventIsOutOfRange(popEvtId_t iEvtId) {
  uint8_t iNum = 0;
  uint8_t index = 0;
  bool iEvtMatch = false;

  iNum = PopNumElements(gaGwEvtLimit);

  for (index = 0; index < iNum; index++) {
    if (iEvtId == gaGwEvtLimit[index]) {
      iEvtMatch = true;
      break;
    }
  }

  if (iEvtMatch) {
    return false;
  } else {
    return true;
  }
}

