/***************************************************************************
  PopAppSm.h

  Revision history:
  Rev   Date      Comment   Description
  this  110912              first revision

***************************************************************************/
#pragma once

#include "PopCfg.h"
#include "MpCfg.h"
#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

#define PART_OF_ADDRPOOL 10   //Divide addr pool by 10
#define ADDR_TO_SPARE_MAX 400
#define ADDR_TO_SPARE_MIN 10

// time (in milliseconds) between packets. Default 100.
#ifndef gPopAppInterval_c
 #define gPopAppInterval_c 1000
#endif

// size of the application data (up to 108 bytes). Range 5 - 108. Default 32.
#ifndef gPopAppDataSize_c
 #define gPopAppDataSize_c 90
#endif

// the node hard-coded address (so no join needed)
// if 0, then normal forming/joining using PopNwkStartNetwork() is done
#ifndef gPopAppNodeAddr_c
 #define gPopAppNodeAddr_c    0xff01
#endif
// the node hard-coded Pan id
#ifndef gPopAppPanId_c
 #define gPopAppPanId_c    0x0001
#endif
// the node hard-coded Channel
#ifndef gPopAppChannel_c
 #define gPopAppChannel_c    11
#endif

// Define any application NVM (Non-Volatile Memory) Storage IDs here...
#define gPopAppUserLevelNvId_c      (gPopNvUserId_c + 10)
#define gPopAppNvId2_c     (gPopNvUserId_c + 11)



typedef struct sPopAppNwkInfo_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
}sPopAppNwkInfo_t;

typedef struct sMpMsgSmInfo_tag
{
  uint8_t aSmId[8];
  uint8_t iUserLevel;
}sMpMsgSmInfo_t;

typedef struct
{
  uint8_t aId[8];
  uint8_t iUserLevel;
}sMpMsgGetConnectedInfo_t;

typedef struct sMpMsgAddrPoolAddr_tag
{
  uint16_t iAddrPoolSize;
  uint16_t iFirstAddr;
}sMpMsgAddrPoolAddr_t;

enum eMpMsgAddrPoolAddrRsp
{
  ADDR_POOL_ADDR_ADDRPOOLSIZE_H,
  ADDR_POOL_ADDR_ADDRPOOLSIZE_L,
  ADDR_POOL_ADDR_FIRSTADDR_H,
  ADDR_POOL_ADDR_FIRSTADDR_L,
  ADDR_POOL_ADDR_SIZE
};

typedef struct sPopAppMsgAckData_tag
{
  popNwkAddr_t iDstAddr;
  uint8_t iOptions;
  uint8_t iReqCmd;
  uint8_t iMpSequence;
  uint8_t iStatus;
}sPopAppMsgAckData_t;

typedef struct sPopAppBeaconIndication_tag
{
  popChannel_t      iChannel;      // channel 11-26
  popLqi_t          iLqi;          // link quality indicator
  popNwkAddr_t      iNwkAddr;      // address of responding device
  popPanId_t        iPanId;        // 16-bit pan identifier
  popProtocol_t     iProtocol;     // 0x50=PopNet, 0x01=ZigBee
  popProtocolVer_t  iProtocolVer;  // top bit indicates join enable
  aPopMacAddr_t     aMacAddr;      // 64-bit IEEE (MAC) address
  popMfgId_t        iMfgId;        // 16-bit manufacturer identifier
  popAppId_t        iAppId;        // 16-bit application identitifer
} sPopAppBeaconIndication_t;
