/***************************************************************************
  PopCfg.h
  Copyright (c) 2006-2010 San Juan Software, LLC. All Rights Reserved.
  PopNet v2.1

  San Juan Software Confidential and Proprietary. This source code is
  released under Non-Disclosure and is not a public work. This notice is not
  to be removed.

  This header contains the compile-time options for PopNet. Each option can be
  overridden by a command-line switch, and can be set through BeeKit. This file
  must NOT contain anything other than constants as this is the very first file
  included.

****************************************************************************/
#ifndef _POPCFG_H_
#define _POPCFG_H_

#include "board_config.h"

// Standard defines

#define gEnabled_c  1
#define gDisabled_c 0

/***************************************************************************
  PopNet Basic Network Parameters
***************************************************************************/

/*
  gPopDefaultChannelList_c

  The default channel list defines which set channels to scan when forming or joining a
  network via PopNwkStartNetwork(). The channel list is a bit mask of channels, ORed
  together. Note: this does not affect PopNwkFormNetwork() or PopNwkJoinNetwork(), which
  specify the channel directly. Each channel is scanned for beacons for 300ms, so setting
  this parameter to all channels will scan for 300ms * 16 = 4,800ms. See gPopScanTime_c
  to set default scan time. See type popChannelList_t in PopNet.h.

  Examples:
  0000 0000 0000 0001 = 0x0001 = channel 11
  1000 0000 0000 0000 = 0x8000 = channel 26
  0100 0000 0000 0000 = 0x4000 = channel 25 (default)
  1111 1111 1111 1111 = 0xffff = all channels 11-26
  0001 0000 0000 0010 = 0x1002 = channels 23 and 12
  0100 0010 0001 0000 = 0x4210 = channels 25, 20 and 15

  Default: 0x4000
  Range: 0x0000 - 0xffff
*/
#ifndef gPopDefaultChannelList_c
 #define gPopDefaultChannelList_c  0x0001
#endif

/*
  gPopDefaultChannel_c

  A compile-time channel may be selected if the network will be preconfigured at the factory for a
  specific channel using silent join. This channel number only affects the node if starting using
  PopNwkStartNetwork(). If using PopNwkFormNetwork() or PopNwkJoinNetwork() it has no effect. See
  instead gPopDefaultChannelList_c.

  A value of 0 means to use the channel list (gPopDefaultChannelList_c).

  Default: 0
  Range: 0, 11 - 26
*/
#ifndef gPopDefaultChannel_c
 #define gPopDefaultChannel_c  11
#endif

/*
  gPopPanId_c

  PAN ID can either be selected dynamically (0xffff), or hard-coded to a particular PAN
  ID (e.g. 0x1234). The valid range is 0x0000 to 0xfff0. Different PAN IDs allow multiple
  networks to share a single channel. See type popPanId_t in PopNet.h.

  Default: 0xcafe
  Range: 0x0000 - 0xfff0, or 0xffff
*/
#ifndef gPopPanId_c
 #define gPopPanId_c  0xCAFE
#endif

/*
  gPopNodeAddr_c

  The node address, sometimes called the network address, is a 16-bit number that
  uniquely identifies a node (a single device) on a PopNet network. The node address may
  be set at compile-time, or may be assigned automatically by automatically by PopNet at
  join-time. Address 0xffff is reserved for broadcasts. Set to 0xfffe to mean "assigned
  by PopNet at join-time". See type popNwkAddr_t in PopNet.h.

  Default: 0xfffe
  Range: 0x0000 - 0xfff0, or 0xfffe
*/
#ifndef gPopNodeAddr_c
 #define gPopNodeAddr_c  0xFFFE
#endif

/*
  gPopMfgId_c

  The manufacturing code is supplied by San Juan Software to customers. The
  manufacturing code is part of the beacon and is used to prevent nodes from
  joining the wrong network. Set to 0xffff if this node doesn't care which PopNet
  network it joins. See type popMfgId_t in PopNet.h.

  Default: 0x0001
  Range: 0x0000 - 0xffff
*/
#ifndef gPopMfgId_c
 #define gPopMfgId_c  0x0001
#endif

/*
  gPopAppId_c

  The Application ID uniquely identifies an application in the beacon, and can be used
  with PopNwkFindNode(). The Application ID only has meaning within a manufacturer ID.
  See type popAppId_t in PopNet.h.

  The PopNet Sample Applications (MfgId 0x0001), have the following AppIDs:
  0x0001 - PopAppTest
  0x0002 - PopAppOnOffLight (as a switch)
  0x0003 - PopAppOnOffLight (as a light)
  0x0004 - PopAppMaze       (as a player)
  0x0005 - PopAppMaze       (as a gateway)
  0x0006 - PopAppTracking   (as mobile-node)
  0x0007 - PopAppTracking   (as router)
  0x0008 - PopAppTracking   (as gateway)
  0x0009 - PopAppGeneric
  0x000A - PopAppSniffer

  Default: 0x0001
  Range: 0x0000 - 0xffff
*/
#ifndef gPopAppId_c
 #define gPopAppId_c  0x0001
#endif


/*
  gPopMacAddr_c

  The MAC address is used to uniquely identify this node, even when not currently on a
  network (for example, it is used during the association process).  The MAC address is a
  total of 64-bits (8-bytes) in length. See type aPopMacAddr_t in PopNet.h.

  The top 24-bits are called the Organizational Unique Identifier, and may be purchased
  from IEEE.org. For lab purposes, using 0x50 0x6f 0x70 for the OUI is acceptible.

  Use 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff for a random MAC address (for lab or demo
  purposes only). The Mac address is specified in little endian byte order. For example,
  the MAC address 0x506f70123456789a would be represented by the bytes:
  0x9a,0x78,0x56,0x34,0x12,0x70,0x6f,0x50.

  Default: 0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
  Range: any combination of bytes
*/
#ifndef gPopMacAddr_c
 #define gPopMacAddr_c	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
#endif

/*
  gPopSecurityLevel_c

  Enables security (both authentication and encryption). Disabled is the smallest code.
  Light is medium security with minor code impact of around 500 bytes. AES requires 2.5K
  code. Set to one of:

  0 (disable)
  1 (light security)
  2 (AES 128-bit security)

  Default: 0
  Range: 0 - 2
*/
#ifndef gPopSecurityLevel_c
 #define gPopSecurityLevel_c  0
#endif

/*
  gPopSecurityKey_c

  This 16-byte key is used whether using AES 128-bit security or PopNet Light Security.
  It is ignored if security is disabled. This field is little endian, with the smallest
  byte first. See type aPopNwkKey_t in PopNet.h.

  Default: 0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff
*/
#ifndef gPopSecurityKey_c
 #define gPopSecurityKey_c  0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff
#endif

/*
  gPopNwkNodePlacement_d

  Enables node placement code in the Default handler so applications can call on
  PopNwkStartNodePlacement() to place nodes within range of other PopNet nodes easily
  during deployment. The code by default uses the LEDs to indicate a signal strength of
  of nodes.

  4 bars = really good
  3 bars = good signal
  2 bars = medium signal or only 1 other PopNet node in range
  1 bar = weak signal
  0 bars = no signal

  Default: gDisabled_c
*/
#ifndef gPopNwkNodePlacement_d
 #define gPopNwkNodePlacement_d   gDisabled_c
#endif

/*
  gPopOverTheAirUpgrades_d

  Enabling gPopOverTheAirUpgrades_c allows a device to receive over-the-air upgrade
  commands to update the running code image over-the-air. This requires the device has a
  storage area large enough to hold the entire new code image. See also gPopUsingStorageArea_c.

  If the gPopSerial_d and gPopGateway_d are also enabled, then the device can also
  receive a new code image over the serial port to be distributed over the network
  (unicast or broadcast).

  Make sure to enable gPopUsingStorageArea_c in order for OTA Upgrades to be able to store the
  incoming image.

  If you have created this solution thru BeeKit then make sure to update the value of
  this flag in compiler line for CW and preprocessor options for IAR projects.

  Default: gDisabled_c
*/
#ifndef gPopOverTheAirUpgrades_d
  #define gPopOverTheAirUpgrades_d  gEnabled_c  //gDisabled_c //gEnabled_c
#endif


/***************************************************************************
  PopNet Network Advanced Parameters
***************************************************************************/

/*
  gPopDefaultRadius_c

  How far (the maximum number of hops) can a packet travel before being dropped? This
  field affects timeouts and jitter times increase as the default radius increases. See
  timeout and jitter times in PopCfg.c.

  The radius for a given message can be less than this value using PopNwkDataRequest().

  Default: 8
  Range 1 - 50
*/
#ifndef gPopDefaultRadius_c
  #define gPopDefaultRadius_c    3
#endif

/*
  gPopICanHearYouEntries_c

  Used during testing to build a table-top network that can mulit-hop, otherwise, the
  nodes can all hear each other and will always communicate directly to their neighbor.
  gPopICanHearYouEntries_c may range from 1 to 64. Set to 0 to disable the feature.

  Each table entry is 2 bytes.

  Default: 4
  Range: 0 - 64
*/
#ifndef gPopICanHearYouEntries_c
  #define gPopICanHearYouEntries_c    0
#endif

/*
  gPopJoinEnabled_d

  If joining is enabled, the network will allow other nodes to join. If joining is
  disabled, the network does not allow other nodes to join (the network is essentially
  closed off). This field can be set at run-time using PopNwkJoinEnable().

  Default: gEnabled_c
*/
#ifndef gPopJoinEnabled_d
  #define gPopJoinEnabled_d  gDisabled_c
#endif

/*
  gPopScanForNoise_d

  If enabled, PopNwkStartNetwork() or PopNwkFormNetwork() will scan for noise on a
  channel in addition to listening for beacons. This can be used to find the most quiet
  channel for forming a network, but does take extra time and code space. The join
  options must also include gPopNwkJoinOptsEnergyScan_c to enable energy scan.

  Default: gDisabled_c
*/
#ifndef gPopScanForNoise_d
  #define gPopScanForNoise_d  gEnabled_c
#endif

/*
  gPopScanSilently_d

  If enabled, PopNwkStartNetwork() will not send a beacon request on startup. Use Scan Silently to
  listen for beacons without sending out a beacon request. This is appropriate for environments
  such as airplanes where radios may not be actively transmitting.

  Default: gDisabled_c
*/
#ifndef gPopScanSilently_d
  #define gPopScanSilently_d  gDisabled_c
#endif

/*
  gPopScanTime_c

  The default time (in milliseconds) to scan for beacons (active scan) or noise (passive
  scan) on each channel. Multiply this number by the number of channels in the
  gPopDefaultChannelList_c to determine the total time spent in scanning when using
  PopNwkStartNetwork(). Don't scan for less than 256 milliseconds because beacon
  responses have random jitter and take from 0-256ms to respond. The scan time can be set
  at run-time using PopNwkSetScanTimeMs().

  Default: 300
  Range: 256 - 64000
*/
#ifndef gPopScanTime_c
  #define gPopScanTime_c  300
#endif

/*
  gPopNwkManageAddrPool_d

  Indicates the stack should manage the address pool on its own. This includes requesing more
  addresses from the address server and saving to NVM at the appropriate times.

  If disabled, the application will manage addressing.

  Default: gEnabled_c
*/
#ifndef gPopNwkManageAddrPool_d
  #define gPopNwkManageAddrPool_d gDisabled_c //gEnabled_c
#endif

/*
  gPopAddrReqSize_c

  The default number of addresses to request from the address server (usually node
  0x0000) at one time. If a node runs out of addresses, it will automatically request
  more addresses from the address server if joining is enabled. This can be set at
  run-time using PopNwkSetAddrReqSize(). The address server short address can be set at
  run-time with PopNwkSetAddressServer().

  Default: 10
  Range: 0x0000 - 0xfff0
*/
#ifndef gPopAddrReqSize_c
  #define gPopAddrReqSize_c  10
#endif

/*
  gPopNwkJoinRetries_c

  Total number of times to attempt to join with PopNwkJoinNetwork(). This field may be set at
  run-time using PopNwkSetJoinRetries(). See also gPopNwkJoinExpired_c.

  The value 255 (x0ff) = try forever.
  Default: 3
  Range: 1 - 255
*/
#ifndef gPopNwkJoinRetries_c
  #define gPopNwkJoinRetries_c   3
#endif

/*
  gPopNwkJoinExpired_c

  The default time to wait for between join attempts (plus a little join jitter). This affects
  PopNwkJoinNetwork() and PopNwkStartNetwork(). This field is settable compile-time only. See also
  gPopNwkJoinRetries_c.

  Default: 1500
*/
#ifndef gPopNwkJoinExpired_c
  #define gPopNwkJoinExpired_c 1500
#endif

/*
  gPopRouteEntries_c

  The number of simultaneous routes PopNet can keep track of at one time. Unicast routes
  use bandwidth very efficiently once established. Note: this list will never run out. If
  a new route is needed, the least recently used route is flushed and that entry is used.
  The size of this array must not exceed 41 entries to be stored in flash (NVM) memory.
  See type sPopNwkRouteEntry_t in PopNet.h.

  Default: 8
  Range: 1 - 255 (each entry is 6 bytes)
*/
#ifndef gPopRouteEntries_c
  #define gPopRouteEntries_c   8  //40
#endif

/*
  gPopRouteDiscoveryEntries_c

  How many route discoveries can this node initiate or keep track of at one time? If a
  route discovery comes along and this table is full, the route discovery will be
  dropped. The entries remain used for the broadcast persistance time. Nodes can always
  retry to find a route later. See type sPopNwkRouteDiscovery_t in PopNet.h.

  entry size = 15 + (3 * gPopAsymmetricLinkEntries_c). Default size = 24 bytes per entry.

  Default: 3
  Range: 1 - 31
*/
#ifndef gPopRouteDiscoveryEntries_c
  #define gPopRouteDiscoveryEntries_c   3
#endif

/*
  gPopAsymmetricLinkEntries_c

  This determines how many links can be asymmetric per hop before routing will fail. Mesh
  networks work best when nodes overlap and can hear each other. If too many links are
  asymmetric (one side can hear, the other cannot), then the application should fall back
  on unibroadcasts instead of using routing.

  This affects the size of the route discovery table. See gPopRouteDiscoveryEntries_c.

  Default: 3
  Range: 1 - 8
*/
#ifndef gPopAsymmetricLinkEntries_c
  #define gPopAsymmetricLinkEntries_c  3
#endif

/*
  gPopRetryTableEntries_c

  The number of packets that this node can keep track of at one time. Used to retry both
  broadcasts and unicasts, and for duplicate detection of incoming (retried) packets. See
  gaPopNwkRetryTable (10 bytes) and gaPopNwkDuplicateTable (5 bytes) in PopCfg.c.

  Default: 8
  Range: 1 - 255 (each entry is 10)
*/
#ifndef gPopRetryTableEntries_c
  #define gPopRetryTableEntries_c   48
#endif

/*
  gPopMaxRetries_c

  The total number of tries used to deliver a packet to the next hop when unicast. # of
  tries before giving up on delivery of a packet to the next hop. Affects timeouts and
  bandwidth. More retries mean a slower and busier network. 3 is optimal. See also
  unicast jitter time mPopUnicastJitter_c in PopCfg.c.

  Default: 3
  Range: 1 - 7
*/
#ifndef gPopMaxRetries_c
  #define gPopMaxRetries_c     3
#endif

/*
  gPopBroadcastRetries_c

  Total # of times each node repeats a broadcast. Tests at San Juan Software have
  determined that three (3) guarantees optimal delivery. See also broadcast jitter time
  mPopBroadcastJitter_c in PopCfg.c.

  Default: 3
  Range: 1 - 7
*/
#ifndef gPopBroadcastRetries_c
  #define gPopBroadcastRetries_c     3
#endif

/*
  gPopNwkNeighborEntries_c

  Used to keep track of security counters for purposes of preventing replay attacks on a
  secure network. Set this number to the maximum expected density of a given network. Not
  used in unsecured networks. See also gPopSecurityLevel_c.

  Default: 12
  Range: 4 - 255 (each entry is 7 bytes)
*/
#ifndef gPopNwkNeighborEntries_c
  #define gPopNwkNeighborEntries_c  12
#endif

/*
  gPopNwkSecureCounterIncrement_c

  The value to increment the secure counter after a NVM restore. The value must be big
  enough to avoid staleness compared with the previous value.

  For example, assume node B is sending packets to node A, and B last saved its outgoing
  counter into NVM on packet 800. If the system is on packet 1000 and node B resets, it
  will update its outgoing counter to 800 + 256 = 1056, so A will think the next incoming
  packet from B is fresh. If however, B had never saved the outgoing security counter and
  was reset, 0 + 256 = 256, so all packets between 256 and 1000 would be dropped by A as
  stale (thinking it's a replay attack). Use PopNvStoreSecureCounterData() to store the
  outgoing security counter.

  Default: 256
*/
#ifndef gPopNwkSecureCounterIncrement_c
  #define gPopNwkSecureCounterIncrement_c  256
#endif

/*
  gPopPhySize_c

  Define this as the size of the largest packet to be received to save some memory during
  message processing. This size includes all octets in the over-the-air frame (FCS). If a
  packet larger than this value is received, it is dropped. The physical 802.15.4 radio
  is capable of up to 127 bytes. Use for RAM limited (1K or less) systems. For example,
  this could be set to 63 bytes on a 1K RAM system.

  Default: 127
  Range: 32 - 127
*/
#ifndef gPopPhySize_c
  #define gPopPhySize_c  127
#endif


/***************************************************************************
  PopNet Board Support Parameters
***************************************************************************/

/*
  gPopGateway_d

  Enable or disable the PopNet gateway functionality. Also enable gPopSerial_d when
  enabling the gateway. The gateway a PC program or host-processor to access PopNet
  network events and commands. All events received by the application are sent out the
  serial port, and events may be sent to the application via the serial port. The Gateway
  code may be modified to support application specific events. See PopSerial.c.

  Default: gDisabled_c
*/
#ifndef gPopGateway_d
  #define gPopGateway_d     gEnabled_c
#endif

/*
  gPopSerial_d

  Enable or disable serial port (UART). Disable the serial port to reduce code size if a
  UART is not needed. See PopSerial.c. Only 1 serial port at a time is enabled. See also
  gPopSerialPort_c, gPopSerialBaudRate_c, and gPopSerialMinGapMs_c.

  Default: gDisabled_c
*/
#ifndef gPopSerial_d
  #define gPopSerial_d    gEnabled_c
#endif

/*
  gPopSerialPort_c

  Which UART (1 or 2) is the default UART if the serial port is enabled? See PopBsp.c for UART port
  definitions. See also gPopSerial_d. Note: PopNet as coded supports only 1 serial port at a time.
  See PopNet Developer's Guide for more information.

  When using the "smart" setting, PopNet will choose the appropriate serial port depending on board
  type:

  UART(SCI) 1 boards:  MC1319x SARD, MC1322x ARM7 (SB, NB, LPB, Dongle), CEL, XBee
  UART(SCI) 2 boards:  MC1321x NCB, SRB, MC1319x EVB and QE-128

  0 = Smart
  1 = UART(SCI) 1
  2 = UART(SCI) 2

  Default: 0 (smart)
*/
#ifndef gPopSerialPort_c

  #if TARGET_BOARD == MC1322XMPCMB
    #define gPopSerialPort_c      2
  #else
    #define gPopSerialPort_c      1
  #endif

#endif

/*
  gPopSerialBaudRate_c

  Specify an index between 0 and 6. This is translates to the baudrate sent to PopSerialSettings()
  at init time. Only relevent if gPopSerial_d is enabled. Serial I/O in PopNet is always 8 data
  bits, 1 stop bit, no parity (8N1). No HW flow control required.

  gPopBaud2400_c      0
  gPopBaud4800_c      1
  gPopBaud9600_c      2
  gPopBaud19200_c     3
  gPopBaud38400_c     4
  gPopBaud57600_c     5
  gPopBaud115200_c    6

  Default: 4  (38,400 baud, 8N1)
*/
#ifndef gPopSerialBaudRate_c
  #define gPopSerialBaudRate_c  6
#endif

/*
  gPopSerialMinGapMs_c

  Minimum period of silence in milliseconds before informing the application bytes are
  available on the serial port. Specify 0 to receive notification on every byte (or as
  quickly as possible). Set to 10ms or so to receive the entire serial packet at once.

  Default: 10
*/
#ifndef gPopSerialMinGapMs_c
  #define gPopSerialMinGapMs_c    10
#endif

/*
  gPopSerialRxBufferSize_c

  Size of the serial receive ring buffer. Set to 134 to enable the largest possible
  over-the-air packets to be sent over the gateway connection. Set to 255 to allow for
  maximum # of beacons over the serial port (otherwise, they will be truncated on a
  gPopEvtNwkScanConfirm_c).

  Default 134
*/
#ifndef gPopSerialRxBufferSize_c
  #define gPopSerialRxBufferSize_c 2048
#endif

/*
  gPopSerialTxBufferSize_c

  Size of transmit ring buffer. Set to 134 to enable the gateway to receive the largest
  possible data indications. Set to 255 to allow for maximum # of beacons over the serial
  port (otherwise, they will be truncated on a gPopEvtNwkScanConfirm_c)

  Default 134
*/
#ifndef gPopSerialTxBufferSize_c
  #define gPopSerialTxBufferSize_c 2048
#endif

// <SJS: Added Serial Tx Rx State Machine >
/*
  gPopSerialSendQueueSize_c

  Defines the maximum number of messages that can be queued to be sent out the serial port.

  Default 8
*/
#ifndef gPopSerialSendQueueSize_c
  #define gPopSerialSendQueueSize_c   48
#endif
// </SJS:>

/*
  gPopNumberOfNvPages_c

  Determines number of 512-byte flash pages devoted to storage for Non-Volatile
  Memory subsystem. This must be between 2 and 8, or set to 0 to disable NVM.
  One spare page is always used for garbage collection, so setting this value to
  2 provides 1 page of storage and 1 spare.
  NOTE: For the QE platform the configuration only allows up to 4 pages, to avoid
  using to much Non banked common ROM space.

  Default: 0
  Range: 0, 2 - 8
*/
#ifndef gPopNumberOfNvPages_c
  #define gPopNumberOfNvPages_c  2
#endif

/*
  gPopManageNvm_d

  This will automatically manage the saving the PopNet networking information to NVM at the
  appropriate times in the PopAppDefaultHandler(). Disable this to manually manage saving stack
  information to NVM. Does nothing if gPopNumberOfNvPages_c is 0.

  Default: gEnabled_c
*/
#ifndef gPopManageNvm_d
 #define gPopManageNvm_d  gEnabled_c
#endif

/*
  gPopUsingStorageArea_c

  Enabling the bulk storage area creates a non-volatile mass storage area, usually for storing code
  images for over-the-air upgrades. This is separate from the record-based NVM storage used to
  store application and network data. See gPopNumberOfNvPages_c for record-based storage.

  0 (Disable)
  1 (Internal)
  2 (External)

  Default: 0
  Range: 0 - 2
*/
#ifndef gPopUsingStorageArea_c
  #define gPopUsingStorageArea_c   2
#endif

/*
  gPopLowPower_d

  Enable or disable low-power support. If disabled, low power support is compiled-out, saving code
  space. If enabled, the function PopPwrSleep() will put the node into low power mode. A low power
  node by default is also considered a sleepy node, and won't repeat incoming broadcasts or
  participate in route requests (other than initiating them). Joining will also be disabled for the
  sleepy node:

  A sleepy node automatically sets:
    PopNwkJoinEnable(FALSE, FALSE);
    PopNwkSetRoutingSleepingMode(TRUE);

  Default: gDisabled_c
*/
#ifndef gPopLowPower_d
  #define gPopLowPower_d     gDisabled_c
#endif

/*
  gPopLowVoltageDetection_d

  Enable or disable low-voltage-detection support. If disabled, low voltage detection
  support is compiled-out, saving code space. If enabled, only the Low voltage
  warning are set, in which the LVWF has to be checked calling the function
  PopIsThereALowVoltageWarning()every time we want to verify if we are running out
  of batery.

  Default: gDisabled_c
*/
#ifndef gPopLowVoltageDetection_d
  #define gPopLowVoltageDetection_d   gDisabled_c
#endif

/*
  gPopEnableLVDInterrupt_d

  Available only on HCS08 based platforms.
  This flag enable the use of Hw interruption for the Low Voltage detection, the
  gPopLowVoltageDetection_d has to be enable as well for this to work. If the
  interruptions are enable, and the low power is enable the node will be wake up
  by the LVD interruption and inform to the app by the gPopEvtWakeUp_c in which
  will indicate the reason why it has been wakeup. And in case there is no low
  power enabled then the application will receive a gPopEvtLowVoltageDected_c.
*/
#ifndef gPopEnableLVDInterrupt_d
#define gPopEnableLVDInterrupt_d      gDisabled_c
#endif

/*
  gPopKeyboardEnabled_d

  Enables keyboard support. If disabled, no key events will come to the application.
  Disable to reduce code size if keys are not needed. See PopBsp.c.

  Default: gEnabled_c
*/
#ifndef gPopKeyboardEnabled_d
  #define gPopKeyboardEnabled_d   gDisabled_c
#endif

/*
  gPopLcdEnabled_d

  Enables LCD support. If disabled, nothing will go to the screen. Only useful for board
  with an LCD screen. See PopLcdWriteString() in PopNet.h. See also gPopLcdRows_c, and
  gPopLcdCols_c in PopBsp.h.

  Default: gDisabled_c
*/
#ifndef gPopLcdEnabled_d
 #define gPopLcdEnabled_d    gDisabled_c
#endif

/*
  gPopLedEnabled_d

  Enables LED support. If disabled, PopSetLed() is stubbed. Disable to reduce code size
  if LEDs are not used. See PopBsp.c.

  Default: gEnabled_c
*/
#ifndef gPopLedEnabled_d
 #define gPopLedEnabled_d    gDisabled_c
#endif

/*
  gPopSoundEnabled_d

  Enables sound support (beep). Used for NCB and SRB boards only. If disabled,
  PopPop() is stubbed. Disable to reduce code size if Sound is not used. See PopBsp.c.

  Default: gDisabled_c
*/
#ifndef gPopSoundEnabled_d
 #define gPopSoundEnabled_d   gDisabled_c
#endif

/*
  gPopOverrideOnMcuType_d

  Override defaults for table sizes so PopNet fits better into smaller MCUs, but is more
  robust in larger MCUs.

  Affects gPopStackSize_c, gPopMemHeapSize_c, gPopMaxTimers_c, gPopMaxEvents_c,
  gPopRouteTableEntries_c, gPopRouteDiscoveryEntries_c, gPopNeighborEntries_c,
  gPopPhySize_c,gPopSerialRxBufferSize_c, gPopSerialTxBufferSize_c

  Default: gDisabled_c
*/
#ifndef gPopOverrideOnMcuType_d
 #define gPopOverrideOnMcuType_d  gDisabled_c
#endif

/*
  gPopTargetMcuType_c

  This option is only useful on platforms that have different Flash (ROM) and RAM size
  options for the MCU. The option sets MCU type (which determines the size of the ROM and
  RAM areas in the MCU).

  For example, if using the Freescale MC13211 (16K Flash, 1K RAM), set this to 2. If using an
  MC13212 (32K Flash, 2K RAM), set this to 1. Otherwise, 0 will default to the MC13213
  (60K flash, 4K RAM).

  If using the Freescale MC13224, for example, it is always 128K Flash, 96K RAM, so this setting
  has no effect for that platform.

  It is suggested to also set gPopOverrideOnMcuType_d true if setting this option to
  reduce RAM size.

  Default: 0
  Range:
  0 = Normal for Platform
  1 = MC9S08GB32/MC9S08GT32  (32K ROM, 2K RAM)
  2 = MC9S08GT16             (16K ROM, 1K RAM)
*/
#ifndef gPopTargetMcuType_c
 #define gPopTargetMcuType_c  0
#endif

/***************************************************************************
  PopNet Kernel Parameters
***************************************************************************/

/*
  gPopAssertLevel_c

  Assert levels 0 (off) and 1 (on) are available for all kits. Assert levels 2 - 5 (more
  strict assert code) can be used with the Full Source Code Kit. See PopAssert() in
  PopDefault.c.

  Default: 0
  Range: 0-5
*/
#ifndef gPopAssertLevel_c
 #define gPopAssertLevel_c     0
#endif

/*
  gPopMaxEvents_c

  Total maximum number of concurrent events in the event queue for all tasks. A well
  designed system does not run out of events (otherwise, tasks can't communicate). Each
  event requires 4 bytes of RAM. See PopEventsLeftAtHighWaterMark().

  Default: 24
  Range: 8 - 255
*/
#ifndef gPopMaxEvents_c
 #define gPopMaxEvents_c   196
#endif

/*
  gPopMaxTimers_c

  Number of soft-timers. Must be a minimum of 6 for use by PopNet, plus the timers
  required by the application. Each timer requires 4 bytes of RAM. A single hardware
  timer (RTI) is used to feed the soft-timers. A well designed system does not run out of
  timers. See PopTimersLeftAtHighWaterMark().

  Default: 16
  Range: 8 - 255
*/
#ifndef gPopMaxTimers_c
 #define gPopMaxTimers_c   128
#endif

/*
  gPopMemHeapSize_c

  Size of heap for all C memory allocations including over-the-air packets. Can be 128
  bytes only with a reduced PHY size because the system requires at least 3 full packets
  to function. See also gPopPhySize_c.

  Default: 1024
  Range: 128 - 65,535 bytes (depending on RAM in the target system)
*/
#ifndef gPopMemHeapSize_c
 #define gPopMemHeapSize_c  0x2000
#endif

/*
  gPopStackSize_c

  Size of the C stack (for local variables, nested functions, etc...). Must be at least
  192 bytes to prevent stack overrun.

  Default: 384
  Range: 192 - 2048 (depending on RAM in the target system)
*/
#ifndef gPopStackSize_c
 #define gPopStackSize_c    0x1000
#endif


#endif  // _POPCFG_H_

