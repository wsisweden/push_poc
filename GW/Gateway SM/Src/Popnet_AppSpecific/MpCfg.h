/***************************************************************************
  MpCfg.h

  Revision history:
  Rev   Date      Comment   Description
  this  110912              first revision

***************************************************************************/
#ifndef _MPCFG_H_
#define _MPCFG_H_

#define STATISTICS_MODULE 1
#define CHARGER_MODULE 2
#define BATTERY_MODULE 3

// define node type to build
// Target board is set depending of node type
#define gNodeType_c    STATISTICS_MODULE 

// Define any application NVM (Non-Volatile Memory) Storage IDs here
#define gPopAppNvCoarseTune_c      (gPopNvUserId_c)     // Not used in CC2538EM
#define gPopAppNvFineTune_c        (gPopNvUserId_c + 1) // Not used in CC2538EM
#define gPopAppNvTime_c            (gPopNvUserId_c + 2)
#define gPopAppNvAsCnt_c           (gPopNvUserId_c + 3)
#define gPopAppNvRadioReset_c      (gPopNvUserId_c + 4)
#define gPopAppNvDefaultNodeAddr_c (gPopNvUserId_c + 5)
#define gPopAppNvTxPowerLevel_c    (gPopNvUserId_c + 6)
#define gPopAppNvDefaultChannel_c  (gPopNvUserId_c + 7)
#define gPopAppNvDefaultPanId_c    (gPopNvUserId_c + 8)

// Used when debugging with IAR, JTAG communication is lost otherwise
#define DEBUG_SM 0
#define DEBUG 0

#endif // _MPCFG_H_
