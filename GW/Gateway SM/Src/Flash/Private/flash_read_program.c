#include "flash_read_program.h"
#include "flash_opcodes.h"
#include "../flash.h"

#include <Hw/TIDriverLib/source/spi.h>

volatile static int buffer_pos;                             // current position in buffer
volatile static uint8_t* bufferAddress;                     // address to start of buffer
volatile static unsigned int bufferSize;                    // size of buffer in bytes
volatile static uint32_t ReadFlashAddress;                  // address in flash
static void (* volatile callback)(uint32_t);                // this function is called then program done

static void program0(uint32_t output);                      // this is program state 0 and it is set to be called from SPI then SPI operation done
static void program1(uint32_t output);                      // this is program state 1 and it is set to be called from SPI then SPI operation done
static void program2(uint32_t output);                      // this is program state 2 and it is set to be called from SPI then SPI operation done

void flash_ReadDataProgramStart(uint8_t* buffer, int size, uint32_t flashAddress, void (* volatile program)(uint32_t)){
  bufferAddress = buffer;
  bufferSize = size;
  ReadFlashAddress = flashAddress;
  callback = program;

  program0(0);
}

static void program0(uint32_t output){
  spi_Callback = program1;
  buffer_pos = 0;

  {
    int len = bufferSize;
    len -= buffer_pos;
    if(len > 4)
      len = 4;                      // length in byts of current read operation
    SPI_COMMAND((FLASH_OPCODE_READ_ARRAY_LOW_FREQUENCY << 24) | ReadFlashAddress, 32, 32 + 8*len - 1); // write 32 bits + read 32 bits = 64 bits
    ReadFlashAddress += len;
  }
}

static void program1(uint32_t output){
  int len = bufferSize;
  len -= buffer_pos;
  if(len > 4)
    len = 4;                      // length in bytes of last read operation

  {
    union{
      uint32_t  output;
      uint8_t data[4];
    } convert;
    convert.output = output;

    int n;
    for(n = 0; n < len; n++){
      bufferAddress[buffer_pos] = convert.data[len - 1 - n];
      buffer_pos++;
    }
  }

  len = bufferSize;
  len -= buffer_pos;
  if(len > 4)
    len = 4;                      // length in bytes of current read operation

  if(len){                        // read more data ?
    SPI_COMMAND((FLASH_OPCODE_READ_ARRAY_LOW_FREQUENCY << 24) | ReadFlashAddress, 32, 32 + 8*len - 1); // write 32 bits + read 32 bits = 64 bits
    ReadFlashAddress += len;
  }
  else
  {
    spi_Callback = program2;
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
}

static void program2(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    uint32_t address = ReadFlashAddress;
    address += bufferSize;
    callback(address);
  }
}
