#ifndef FLASH_BLOCK_PROGRAM_H
#define FLASH_BLOCK_PROGRAM_H

#include <stdint.h>

void flash_EraseBlockProgramStart(uint32_t flashAddress, void (* volatile program)(uint32_t));

#endif
