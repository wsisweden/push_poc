#include "flash_erase_block_program.h"
#include "flash_opcodes.h"
#include "../flash.h"

#include <PortConfig.h>
#include <Hw/TIDriverLib/source/spi.h>

volatile static int ProgramState;                           // program state

volatile static uint32_t WriteFlashAddress;                 // address in flash
static void (* volatile callback)(uint32_t);                // called then program done

static void flash_EraseBlockProgram(uint32_t output);       // erase block at address

void flash_EraseBlockProgramStart(uint32_t flashAddress, void (* volatile program)(uint32_t)){
  WriteFlashAddress = flashAddress;
  callback = program;

  ProgramState = 0;

  spi_Callback = flash_EraseBlockProgram;
  flash_EraseBlockProgram(0);
}

static void flash_EraseBlockProgram(uint32_t output){
  switch(ProgramState){
  case 0 :
    ProgramState = 1;
    FLASH_WRITE_PROTECT_HIGH;                               // allow write to chip
    SPI_COMMAND(FLASH_OPCODE_WRITE_ENABLE << 24, 8, 7);     // set write enable, write 8 bits + read 0 bits = 8 bits
    break;
  case 1 :
    ProgramState = 2;
    SPI_COMMAND(FLASH_OPCODE_WRITE_STATUS << 24, 16, 15);   // turn off sector protection lock, write 16 bits + read 0 bits = 16 bits
    break;
  case 2 :
    ProgramState = 3;
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
    break;
  case 3 :
    if(output & (1<<0)){                                    // wait for flash not busy
      SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
    }
    else{
      ProgramState = 4;
      SPI_COMMAND(FLASH_OPCODE_WRITE_ENABLE << 24, 8, 7);   // set write enable, write 8 bits + read 0 bits = 8 bits
    }
    break;
  case 4 :
    ProgramState = 5;
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
    break;
  case 5 :
    if(output & (1<<0)){                                    // wait for flash not busy
      SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
    }
    else{
      ProgramState = 6;
      SPI_COMMAND((FLASH_OPCODE_UNPROTECT_SECTOR << 24) | WriteFlashAddress, 32, 31); // unprotect sector, write 32 bits + read 0 bits = 32 bits
    }
    break;
  case 6 :
    ProgramState = 7;
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
    break;
  case 7 :
    if(output & (1<<0)){                                    // wait for flash not busy
      SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
    }
    else{
      ProgramState = 8;
      SPI_COMMAND(FLASH_OPCODE_WRITE_ENABLE << 24, 8, 7);   // set write enable, write 8 bits + read 0 bits = 8 bits
    }
    break;
  case 8 :
    ProgramState = 9;
    SPI_COMMAND((FLASH_OPCODE_BLOCK_ERASE_4K << 24) | WriteFlashAddress, 32, 31); // block erase, write 32 bits + read 0 bits = 32 bits
    break;
  case 9 :
    ProgramState = 10;
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
    break;
  case 10 :
    if(output & (1<<0)){                                    // wait for flash not busy
      SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
    }
    else{
      ProgramState = 11;
      {
        uint32_t address = FLASH_ERASE_BLOCK_SIZE + WriteFlashAddress;
        FLASH_WRITE_PROTECT_LOW;
        callback(address);
      }
    }
    break;
  case 11 :
    break;
  default:
    // Nothing to do
    break;
  }
}
