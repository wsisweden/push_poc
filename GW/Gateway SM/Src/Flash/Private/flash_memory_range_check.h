#ifndef FLASH_MEMORY_RANGE_CHECK
#define FLASH_MEMORY_RANGE_CHECK

#if FLASH_MC1322X_OTA_BINARY_ADDRESS_LAST - FLASH_MC1322X_OTA_BINARY_ADDRESS_FIRST < 0                                    // start adress must be smaller than end address
#error FLASH_MC1322X_OTA_BINARY_ADDRESS range is negative
#endif

#if FLASH_APPLICATION_PARAMETERS_ADDRESS_LAST - FLASH_APPLICATION_PARAMETERS_ADDRESS_FIRST < 0                                    // start adress must be smaller than end address
#error FLASH_APPLICATION_PARAMETERS_ADDRESS range is negative
#endif

#if FLASH_LPC1765_OTA_BINARY_ADDRESS_LAST - FLASH_LPC1765_OTA_BINARY_ADDRESS_FIRST < 0                                    // start adress must be smaller than end address
#error FLASH_LPC1765_OTA_BINARY_ADDRESS range is negative
#endif

//#if FLASH_HISTORY_ADDRESS_LAST - FLASH_HISTORY_ADDRESS_FIRST < 0                                                                  // start adress must be smaller than end address
//#error FLASH_HISTORY_ADDRESS_FIRST range is negative
//#endif

//#if FLASH_INSTANT_ADDRESS_LAST - FLASH_INSTANT_ADDRESS_FIRST < 0                                                                  // start adress must be smaller than end address
//#error FLASH_INSTANT_ADDRESS_FIRST range is negative
//#endif

//#if FLASH_EVENT_ADDRESS_LAST - FLASH_EVENT_ADDRESS_FIRST < 0                                                                      // start adress must be smaller than end address
//#error FLASH_EVENT_ADDRESS_FIRST range is negative
//#endif


typedef int Dummy_ApplicationParameters[1 - (FLASH_APPLICATION_PARAMETERS_ADDRESS_FIRST & (FLASH_ERASE_BLOCK_SIZE - 1))];         // must be on first address of block
//typedef int Dummy_History[1 - (FLASH_HISTORY_ADDRESS_FIRST & (FLASH_ERASE_BLOCK_SIZE - 1))];                                      // must be on first address of block
//typedef int Dummy_Instant[1 - (FLASH_INSTANT_ADDRESS_FIRST & (FLASH_ERASE_BLOCK_SIZE - 1))];                                      // must be on first address of block
//typedef int Dummy_Event[1 - (FLASH_EVENT_ADDRESS_FIRST & (FLASH_ERASE_BLOCK_SIZE - 1))];                                          // must be on first address of block

//typedef int Dummy_ApplicationParameters2[1 - ((FLASH_APPLICATION_PARAMETERS_ADDRESS_LAST + 1) & (FLASH_ERASE_BLOCK_SIZE - 1))];   // must be on last address of block
//typedef int Dummy_History2[1 - ((FLASH_HISTORY_ADDRESS_LAST + 1) & (FLASH_ERASE_BLOCK_SIZE - 1))];                                // must be on last address of block
//typedef int Dummy_Instant2[1 - ((FLASH_INSTANT_ADDRESS_LAST + 1) & (FLASH_ERASE_BLOCK_SIZE - 1))];                                // must be on last address of block
//typedef int Dummy_Event2[1 - ((FLASH_EVENT_ADDRESS_LAST + 1) & (FLASH_ERASE_BLOCK_SIZE - 1))];                                    // must be on last address of block

#endif
