#ifndef FLASH_OPCODES_H
#define FLASH_OPCODES_H

/* Available commands AT25DF041A
 *   Read Commands
 *     Read Array
 *     Read Array (Low Frequency)
 *   Program and Erase Commands
 *     Block Erase (4 Kbytes)
 *     Block Erase (32 Kbytes)
 *     Block Erase (64 Kbytes)
 *     Chip Erase
 *     Byte/Page Program (1 to 256 Bytes)
 *     Sequential Program Mode
 *   Protection Commands
 *     Write Enable
 *     Write Disable
 *     Protect Sector
 *     Unprotect Sector
 *     Global Protect/Unprotect
 *     Read Sector Protection Registers
 *   Status Register Commands
 *     Read Status Register
 *     Write Status Register
 *   Miscellaneous Commands
 *     Read Manufacturer and Device ID
 *     Deep Power-down
 *     Resume from Deep Power-Down
 */
#define FLASH_OPCODE_READ_ARRAY                   0x0BU
#define FLASH_OPCODE_READ_ARRAY_LOW_FREQUENCY     0x03U
#define FLASH_OPCODE_BLOCK_ERASE_4K               0x20U
#define FLASH_OPCODE_BLOCK_ERASE_32K              0x52U
#define FLASH_OPCODE_BLOCK_ERASE_64K              0xD8U
#define FLASH_OPCODE_CHIP_ERASE                   0x60U
#define FLASH_OPCODE_PROGRAM_1_TO_256_BYTES       0x02U
#define FLASH_OPCODE_PROGRAM_SEQUENTIAL           0xADU
#define FLASH_OPCODE_WRITE_ENABLE                 0x06U
#define FLASH_OPCODE_WRITE_DISABLE                0x04U
#define FLASH_OPCODE_PROTECT_SECTOR               0x36U
#define FLASH_OPCODE_UNPROTECT_SECTOR             0x39U
#define FLASH_OPCODE_READ_SECTOR_PROTECT          0x3CU
#define FLASH_OPCODE_READ_STATUS                  0x05U
#define FLASH_OPCODE_WRITE_STATUS                 0x01U
#define FLASH_OPCODE_READ_MANUFACTURER_DEVICE_ID  0x9FU
#define FLASH_OPCODE_DEEP_POWER_DOWN              0xB9U
#define FLASH_OPCODE_RESUME_FROM_DEEP_POWER_DOWN  0xABU

#endif
