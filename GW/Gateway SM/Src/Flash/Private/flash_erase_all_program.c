#include "flash_erase_all_program.h"
#include "flash_write_program.h"
#include "flash_opcodes.h"
#include "../flash.h"

#include <PortConfig.h>
#include <Hw/TIDriverLib/source/spi.h>

static volatile void (* volatile callback)(void);                      // this function is called then program done

static void program0(uint32_t output);                    // this is program state 0 and it is set to be called from SPI then SPI operation done
static void program1(uint32_t output);                    // this is program state 1 and it is set to be called from SPI then SPI operation done
static void program2(uint32_t output);                    // this is program state 2 and it is set to be called from SPI then SPI operation done
static void program3(uint32_t output);                    // this is program state 3 and it is set to be called from SPI then SPI operation done
static void program4(uint32_t output);                    // this is program state 4 and it is set to be called from SPI then SPI operation done
static void program5(uint32_t output);                    // this is program state 5 and it is set to be called from SPI then SPI operation done
static void program6(uint32_t output);                    // this is program state 6 and it is set to be called from SPI then SPI operation done
static void program7(uint32_t output);                    // this is program state 7 and it is set to be called from SPI then SPI operation done
static void program8(uint32_t output);                    // this is program state 8 and it is set to be called from SPI then SPI operation done
static void program9(uint32_t output);                    // this is program state 9 and it is set to be called from SPI then SPI operation done
static void program10(uint32_t output);                   // this is program state 9 and it is set to be called from SPI then SPI operation done

void flash_EraseAllProgramStart(volatile void (* volatile program)(void)){
  callback = program;

  program0(0);
}

static void program0(uint32_t output){
  spi_Callback = program1;
  FLASH_WRITE_PROTECT_HIGH;                               // allow write to chip
  SPI_COMMAND(FLASH_OPCODE_WRITE_STATUS << 24, 16, 15);   // turn off sector protection lock, write 16 bits + read 0 bits = 16 bits
}

static void program1(uint32_t output){
  spi_Callback = program2;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program2(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program3;
    SPI_COMMAND(FLASH_OPCODE_WRITE_ENABLE << 24, 8, 7);   // set write enable, write 8 bits + read 0 bits = 8 bits
  }
}

static void program3(uint32_t output){
  spi_Callback = program4;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program4(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program5;
    SPI_COMMAND((FLASH_OPCODE_WRITE_STATUS << 24), 16, 15); // global unprotect sector, write 32 bits + read 0 bits = 32 bits
  }
}

static void program5(uint32_t output){
  spi_Callback = program6;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program6(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program7;
    SPI_COMMAND(FLASH_OPCODE_WRITE_ENABLE << 24, 8, 7);   // set write enable, write 8 bits + read 0 bits = 8 bits
  }
}

static void program7(uint32_t output){
  spi_Callback = program8;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program8(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    spi_Callback = program9;
    SPI_COMMAND(FLASH_OPCODE_CHIP_ERASE << 24, 8, 7);     // chip erase
  }
}

static void program9(uint32_t output){
  spi_Callback = program10;
  SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);     // read flash status, write 8 bits + read 8 bits = 16 bits
}

static void program10(uint32_t output){
  if(output & (1<<0)){                                    // wait for flash not busy
    SPI_COMMAND(FLASH_OPCODE_READ_STATUS << 24, 8, 15);   // read flash status, write 8 bits + read 8 bits = 16 bits
  }
  else{
    FLASH_WRITE_PROTECT_LOW;                              // Write protect chip
    callback();
  }
}
