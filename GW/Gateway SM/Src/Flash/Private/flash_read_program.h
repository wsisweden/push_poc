#ifndef FLASH_READ_PROGRAM_H
#define FLASH_READ_PROGRAM_H

#include <stdint.h>

void flash_ReadDataProgramStart(uint8_t* buffer, int size, uint32_t flashAddress, void (* volatile program)(uint32_t));  // should be called to reset flash_ReadDataProgram(...) before program is started

#endif
