#ifndef FLASH_ERASE_ALL_PROGRAM_H
#define FLASH_ERASE_ALL_PROGRAM_H

void flash_EraseAllProgramStart(volatile void (* volatile program)(void));  // called to erase all flash

#endif
