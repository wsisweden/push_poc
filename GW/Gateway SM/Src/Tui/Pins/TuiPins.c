#include "TuiPins.h"
#include <Hw/TIDriverLib/inc/hw_memmap.h>
#include <Hw/TIDriverLib/source/gpio.h>

// Public functions

/**
 * Initialize the TUI pins
 */
void TuiPinInit(void) {
  GPIOPinTypeGPIOOutput(GPIO_D_BASE, GPIO_PIN_6);
  GPIOPinTypeGPIOOutput(GPIO_D_BASE, GPIO_PIN_7);
}

/**
 * Set the output on the radio status pin
 * @param on If the pin should be on
 */
void TuiPinRadioStatus(const bool on) {
  if (on) {
    GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_7, GPIO_PIN_7);
  } else {
    GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_7, 0);
  }
}

/**
 * Set the output on the processor running pin
 * @param on If the pin should be on
 */
void TuiPinProcessorRunning(const bool on) {
  if (on) {
    GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_6, GPIO_PIN_6);
  } else {
    GPIOPinWrite(GPIO_D_BASE, GPIO_PIN_6, 0);
  }
}

