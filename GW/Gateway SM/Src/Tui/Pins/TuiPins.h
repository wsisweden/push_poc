#ifndef TUI_PINS
#define TUI_PINS

#include <stdbool.h>

// Public functions

void TuiPinInit(void);
void TuiPinRadioStatus(const bool on);
void TuiPinProcessorRunning(const bool on);

#endif
