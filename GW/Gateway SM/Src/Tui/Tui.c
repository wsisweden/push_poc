#include "Tui.h"
#include "Pins/TuiPins.h"
#include <stdint.h>

// Private definitions

#define TUI_PINFUNCTIONS_LEN 2

typedef struct {
  void (*pinFunction)(const bool on);
  TuiBlinkType_t blink;
  uint8_t functionParams;
} TuiPinFunctions_t;

// Timer IDs
enum {
  TimerId_Blink = 1
};

// Private variables

static uint8_t tuiTimer;
static TuiPinFunctions_t tuiPinFunctions[TUI_PINFUNCTIONS_LEN] = {
  { TuiPinRadioStatus, eTuiOff },
  { TuiPinProcessorRunning, eTuiOff }
};

// Private functions

static void TuiProcessorRunning(const TuiBlinkType_t function);
static void TuiBlinkLeds(void);

// Public functions

/**
 * Initialize the task
 */
void TuiTaskInit(void) {
  tuiTimer = 0;
  TuiPinInit();
  TuiRadioStatus(eTuiOff);
  TuiProcessorRunning(eTuiToggle0p5Hz);
  PopStartTimer(TimerId_Blink, 125);
}

/**
 * Handle the task event loop
 * @param iEvtId The event id
 * @param sEvtData The event data
 */
void TuiTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  switch(iEvtId) {
    case gPopEvtTimer_c:
      switch(sEvtData.iTimerId) {
        case TimerId_Blink:
          TuiBlinkLeds();
          PopStartTimer(TimerId_Blink, 125);
          break;
        default:
          // Nothing to do
          break;
      }
      break;
    default:
      // Nothing to do
      break;
  }
}

/**
 * Set the blink function on the radio status pin
 * @param function The function
 */
void TuiRadioStatus(const TuiBlinkType_t function) {
  tuiPinFunctions[0].blink = function;
  tuiPinFunctions[0].functionParams = 0;
}

// Static functions

/**
 * Set the blink function on the processor running pin
 * @param function The function
 */
static void TuiProcessorRunning(const TuiBlinkType_t function) {
  tuiPinFunctions[1].blink = function;
  tuiPinFunctions[1].functionParams = 0;
}

/**
 * Blink the LEDs according to function
 */
static void TuiBlinkLeds(void) {
  uint_fast8_t n;

  for (n = 0; n < TUI_PINFUNCTIONS_LEN; n++) {
    switch(tuiPinFunctions[n].blink) {
    case eTuiOff:
      tuiPinFunctions[n].pinFunction(eTuiOff);
      break;
    case eTuiOn:
      tuiPinFunctions[n].pinFunction(eTuiOn);
      break;
    case eTuiToggle0p5Hz:
      switch(tuiTimer) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
          tuiPinFunctions[n].pinFunction(eTuiOff);
          break;
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
          tuiPinFunctions[n].pinFunction(eTuiOn);
          break;
        default:
          // Should not happen
          break;
      }
      break;
    case eTuiToggle2Hz:
      switch(tuiTimer) {
        case 0:
        case 1:
        case 4:
        case 5:
        case 8:
        case 9:
        case 12:
        case 13:
          tuiPinFunctions[n].pinFunction(eTuiOff);
          break;
        case 2:
        case 3:
        case 6:
        case 7:
        case 10:
        case 11:
        case 14:
        case 15:
          tuiPinFunctions[n].pinFunction(eTuiOn);
          break;
        default:
          // Should not happen
          break;
      }
      break;
    case eTuiToggle4Hz:
      switch(tuiTimer) {
        case 0:
        case 2:
        case 4:
        case 6:
        case 8:
        case 10:
        case 12:
        case 14:
          tuiPinFunctions[n].pinFunction(eTuiOff);
          break;
        case 1:
        case 3:
        case 5:
        case 7:
        case 9:
        case 11:
        case 13:
        case 15:
          tuiPinFunctions[n].pinFunction(eTuiOn);
          break;
        default:
          // Should not happen
          break;
      }
      break;
      case eTuiBlink:
        switch(tuiTimer) {
          case 0:
          case 2:
          case 4:
          case 6:
          case 8:
          case 10:
          case 12:
          case 14:
            tuiPinFunctions[n].pinFunction(eTuiOff);

            if (tuiPinFunctions[n].functionParams >= 1) {
              tuiPinFunctions[n].blink = eTuiOff;
            }
            break;
          case 1:
          case 3:
          case 5:
          case 7:
          case 9:
          case 11:
          case 13:
          case 15:
            tuiPinFunctions[n].pinFunction(eTuiOn);
            tuiPinFunctions[n].functionParams++;
            break;
          default:
            // Should not happen
            break;
        }
        break;
      default:
        // Nothing to do
        break;
    }
  }

  tuiTimer = (tuiTimer + 1) % 16;
}
