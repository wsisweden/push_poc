#ifndef _TUI_H_
#define _TUI_H_

#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

// Public enumerations

typedef enum {
  eTuiOff,
  eTuiOn,
  eTuiToggle0p5Hz,
  eTuiToggle2Hz,
  eTuiToggle4Hz,
  eTuiBlink,
  eTuiUndefined
} TuiBlinkType_t;

// Public functions

void TuiTaskInit(void);
void TuiTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void TuiRadioStatus(const TuiBlinkType_t function);

#endif
