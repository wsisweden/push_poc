#include "NfcMessageHandler.h"
#include "../CirrusGw/CirrusGw.h"
#include "Driver/Nt3h2111_Defines.h"
#include "Driver/Nt3h2111.h"

#include <GenericProtocol/GenericReceive.h>
#include <GenericProtocol/GenericPack.h>
#include <GenericProtocol/ProtocolBuffers/Gen/AcknowledgeResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/ReadRequest.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/WriteRequest.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/CapabilityResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/InfoResponse.pb.h>
#include <GenericProtocol/ProtocolBuffers/Gen/ParameterResponse.pb.h>
#include <string.h>

// Private definition

#define BUFFER_SIZE 0x200

// Private typedefs

typedef enum {
  NfcMessageStateIdle,
  NfcMessageStateReading,
  NfcMessageStateSending
} NfcMessageState_t;

// External variables

extern uint32_t giFwType;
extern uint32_t giFwVer;

// Private variables

static uint8_t nfcMessageReadBuffer[BUFFER_SIZE];
static uint8_t nfcMessageWriteBuffer[BUFFER_SIZE];
static NfcMessageState_t nfcMessageState;
static GenericReceive_t genericReceive;
static GenericPack_t genericPack;

// Private function declarations

static void NfcMessageSendingCycle(void);
static void NfcMessageSendingCallbackCycle(void);
static bool NfcMessageHandleReadRequest(void);
static void NfcMessageHandleWriteRequest(void);
static void NfcSend(void);
static void NfcMessageSend(void);
static void NfcSendAcknowledge(
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericType_t type);
static void NfcSendWriteAcknowledge(
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericProtocol_WriteRequest_Type type);
static void NfcHandlerReadCapability(void);
static void NfcHandlerReadInfo(void);
static bool NfcHandleReadParam(const uint32_t index);
static void NfcHandleWriteParam(
    const GenericProtocol_ParameterRequest* const request);
static void NfcHandleWriteTime(
    const GenericProtocol_TimeRequest* const request);

// Public functions

/**
 * Initialize the message handler
 */
void NfcMessageInit(void) {
  nfcMessageState = NfcMessageStateIdle;

  GenericReceiveInit(&genericReceive);
  GenericReceiveSetBuffer(&genericReceive, nfcMessageReadBuffer,
                          sizeof(nfcMessageReadBuffer));

  GenericPackInit(&genericPack, nfcMessageWriteBuffer,
                  sizeof(nfcMessageWriteBuffer));
}

/**
 * Cycle the message handler
 */
void NfcMessageCycle(void) {
  switch (nfcMessageState) {
  case NfcMessageStateIdle:
    // Nothing to do
    break;
  case NfcMessageStateReading:
    // Nothing to do, waiting for data
    break;
  case NfcMessageStateSending:
    NfcMessageSendingCycle();
    break;
  default:
    // Should not happen
    break;
  }
}

/**
 * Callback cycle the message handler
 */
void NfcMessageCallbackCycle(void) {
  switch (nfcMessageState) {
  case NfcMessageStateIdle:
    // Nothing to do
    break;
  case NfcMessageStateReading:
    // Nothing to do
    break;
  case NfcMessageStateSending:
    NfcMessageSendingCallbackCycle();
    break;
  default:
    // Should not happen
    break;
  }
}

/**
 * Check if message is reading
 */
bool NfcMessageIsReading(void) {
  return false;
}

/**
 * Check if message is sending
 */
bool NfcMessageIsSending(void) {
  return nfcMessageState == NfcMessageStateSending;
}

/**
 * Check if the message handler is idle
 */
bool NfcMessageIsIdle(void) {
  return NfcMessageStateIdle == nfcMessageState;
}

/**
 * Handle a message received by NFC
 * @param payload The data
 * @param count The data length
 */
void NfcMessageHandle(uint8_t* payload, const uint32_t count) {
  GenericReceiveAddData(&genericReceive, payload, count);

  if (GenericReceiveIsComplete(&genericReceive) != GenericReceiveErrorLength) {
    nfcMessageState = NfcMessageStateIdle;
    GenericPackStartFrame(&genericPack);

    do {
      GenericType_t type = GenericReceiveGetCmdType(&genericReceive);
      GenericReceiveError_t error = GenericReceiveIsCmdValid(&genericReceive);

      if (error == GenericReceiveErrorOk) {
        switch (type) {
        case GenericTypeReadReq:
          if (!NfcMessageHandleReadRequest()) {
            GenericReceiveClear(&genericReceive);
          }
          break;
        case GenericTypeWriteReq:
          NfcMessageHandleWriteRequest();
          break;
        default:
          // Not a valid command
          NfcSendAcknowledge(
              GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND, type);
          break;
        }
      } else if (error == GenericReceiveErrorCrc) {
        NfcSendAcknowledge(GenericProtocol_AcknowledgeResponse_Type_CRC,
            GenericTypeInvalid);
      } else if (error == GenericReceiveErrorLength) {
        NfcSendAcknowledge(GenericProtocol_AcknowledgeResponse_Type_DATA_LENGTH,
            GenericTypeInvalid);
      }
    } while (GenericReceiveNextCmd(&genericReceive));

    GenericReceiveClear(&genericReceive);

    if (GenericPackIsFrameStarted(&genericPack)) {
      GenericPackEndFrame(&genericPack);
      NfcMessageSend();
    }
  } else {
    nfcMessageState = NfcMessageStateReading;
  }
}

// Private functions

/**
 * Cycle the sending state
 */
static void NfcMessageSendingCycle(void) {
  if (!GenericPackIsChunkLeft(&genericPack)) {
    nfcMessageState = NfcMessageStateIdle;
    return;
  }
}

/**
 * Cycle the sending callback state
 */
static void NfcMessageSendingCallbackCycle(void) {
  if (GenericPackIsChunkLeft(&genericPack)) {
    NfcSend();
  } else {
    nfcMessageState = NfcMessageStateIdle;
  }
}

/**
 * Handle a command received by NFC
 */
static bool NfcMessageHandleReadRequest(void) {
  GenericProtocol_ReadRequest message =
      GenericProtocol_ReadRequest_init_default;
  bool status =
      GenericReceiveDecode(&genericReceive, GenericProtocol_ReadRequest_fields,
                           &message);

  // Check if message was parsed OK
  if (!status) {
    NfcSendAcknowledge(GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
        GenericTypeReadReq);
    return true;
  }

  switch (message.type) {
  case GenericProtocol_ReadRequest_Type_INFO:
    NfcHandlerReadInfo();
    break;
  case GenericProtocol_ReadRequest_Type_CAPABILITY:
    NfcHandlerReadCapability();
    break;
  case GenericProtocol_ReadRequest_Type_PARAMETER:
    return NfcHandleReadParam(message.index);
  default:
    // Command not supported
    NfcSendAcknowledge(GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
        GenericTypeReadReq);
    break;
  }

  return true;
}

/**
 * Handle a command received by NFC
 */
static void NfcMessageHandleWriteRequest(void) {
  GenericProtocol_WriteRequest message =
      GenericProtocol_WriteRequest_init_default;
  bool status =
      GenericReceiveDecode(&genericReceive, GenericProtocol_WriteRequest_fields,
                           &message);

  // Check if message was parsed OK
  if (!status) {
    NfcSendAcknowledge(GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
        GenericTypeWriteReq);
    return;
  }

  switch (message.type) {
    case GenericProtocol_WriteRequest_Type_PARAMETER:
      NfcHandleWriteParam(&message.data.parameter);
      break;
    case GenericProtocol_WriteRequest_Type_TIME:
      NfcHandleWriteTime(&message.data.time);
      break;
    default:
      // Command not supported
      NfcSendAcknowledge(
         GenericProtocol_AcknowledgeResponse_Type_UNKNOWN_COMMAND,
         GenericTypeReadReq);
      break;
  }
}

/**
 * Send a message
 */
static void NfcSend(void) {
  uint8_t buffer[NT3H2111_SIZE_SRAM];

  GenericPackGetFrameChunk(&genericPack, buffer, NT3H2111_SIZE_SRAM);
  Nt3h2111_writeSram(buffer, NT3H2111_SIZE_SRAM);
}

/**
 * Start sending the NFC message
 */
static void NfcMessageSend(void) {
  nfcMessageState = NfcMessageStateSending;
  Nt3h2111_setPassThroughI2cToRf();
  NfcSend();
}

/**
 * Send an acknowledge with a return code
 */
static void NfcSendAcknowledge(
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericType_t type) {
  GenericProtocol_AcknowledgeResponse message =
      GenericProtocol_AcknowledgeResponse_init_default;

  message.type = code;
  message.genericType = type;

  GenericPackAddCmd(&genericPack, GenericTypeAcknowledgeResp,
      GenericProtocol_AcknowledgeResponse_fields, &message);
}

/**
 * Send an acknowledge with a return code
 */
static void NfcSendWriteAcknowledge(
    const GenericProtocol_AcknowledgeResponse_Type code,
    const GenericProtocol_WriteRequest_Type type) {
  GenericProtocol_AcknowledgeResponse message =
      GenericProtocol_AcknowledgeResponse_init_default;

  message.type = code;
  message.genericType = GenericTypeWriteReq;
  message.writeType = type;

  GenericPackAddCmd(&genericPack, GenericTypeAcknowledgeResp,
      GenericProtocol_AcknowledgeResponse_fields, &message);
}

/**
 * Handle the read device capability command
 */
static void NfcHandlerReadCapability(void) {
  GenericProtocol_CapabilityResponse message =
      GenericProtocol_CapabilityResponse_init_default;

  message.config = true;
  message.info = true;
  message.status = true;
  message.log = false;
  message.logCapability.event = false;
  message.logCapability.history = false;
  message.logCapability.instant = false;
  message.parameter = true;

  GenericPackAddCmd(&genericPack, GenericTypeCapabilityResp,
      GenericProtocol_CapabilityResponse_fields, &message);
}

/**
 * Handle the read device info command
 */
static void NfcHandlerReadInfo(void) {
  GenericProtocol_InfoResponse message =
      GenericProtocol_InfoResponse_init_default;
  CirrusGwInfo_t* info = CirrusGwGetInfo();

  message.firmwareType = giFwType;
  message.firmwareVersion = giFwVer;
  message.mui = info->mui;
  message.protocolVersion = PROTOCOL_VERSION;
  message.productId = GenericProtocol_ProductId_CIRRUS_GW;

  GenericPackAddCmd(&genericPack, GenericTypeInfoResp,
      GenericProtocol_InfoResponse_fields, &message);
}

/**
 * Handle the read configuration command
 */
static bool NfcHandleReadParam(const uint32_t index) {
  GenericProtocol_ParameterResponse message =
      GenericProtocol_ParameterResponse_init_default;

  if (index >= eCirrusParamLast) {
    NfcSendAcknowledge(
        GenericProtocol_AcknowledgeResponse_Type_PARAMETER_OUT_OF_BOUNDS,
        GenericTypeReadReq);
    return true;
  }

  message.index = index;
  message.size = CirrusGwGetParam(message.index,
                                  (uint8_t*)message.data.textParameter.value);

  switch (index) {
    case eCirrusParamSoftwareType:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamSoftwareVersion:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamFirmwareType:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamFirmwareVersion:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamMui:
      message.which_data = GenericProtocol_ParameterResponse_longParameter_tag;
      break;
    case eCirrusParamBuildCommit:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamQtVersion:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamEthernetMac:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamWirelessMac:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamDeviceId:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamName:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamCloudUrl:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamCloudUsername:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamCloudPassword:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamSecurityCode:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamScanNetworkInterval:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamCollectDataInterval:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamPeriodicUpdateInterval:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamSshActive:
      message.which_data = GenericProtocol_ParameterResponse_boolParameter_tag;
      break;
    case eCirrusParamDebugLog:
      message.which_data = GenericProtocol_ParameterResponse_boolParameter_tag;
      break;
    case eCirrusParamDebugMpaLog:
      message.which_data = GenericProtocol_ParameterResponse_boolParameter_tag;
      break;
    case eCirrusParamEthernetAddress:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamEthernetStatic:
      message.which_data = GenericProtocol_ParameterResponse_boolParameter_tag;
      break;
    case eCirrusParamEthernetNetmask:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamEthernetGateway:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamWirelessAddress:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamFoundNodes:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamWirelessNetmask:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamWirelessSsid:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamWirelessPassword:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamWirelessSignalStrength:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamWirelessLinkQuality:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamFreeStorage:
      message.which_data = GenericProtocol_ParameterResponse_longParameter_tag;
      break;
    case eCirrusParamDateTime:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamSerialNumber:
      message.which_data = GenericProtocol_ParameterResponse_longParameter_tag;
      break;
    case eCirrusParamFactoryDefault:
      message.which_data = GenericProtocol_ParameterResponse_boolParameter_tag;
      break;
    case eCirrusParamRemoveNodeInterval:
      message.which_data = GenericProtocol_ParameterResponse_intParameter_tag;
      break;
    case eCirrusParamNetworkRestriction:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamNodeRestriction:
      message.which_data = GenericProtocol_ParameterResponse_textParameter_tag;
      break;
    case eCirrusParamReady:
      message.which_data = GenericProtocol_ParameterResponse_boolParameter_tag;
      break;
    default:
      // Nothing to do
      return true;
  }

  return GenericPackAddCmd(&genericPack, GenericTypeParameterResp,
      GenericProtocol_ParameterResponse_fields, &message);
}

/**
 * Handle the write configuration command
 */
static void NfcHandleWriteParam(
    const GenericProtocol_ParameterRequest* const request) {
  uint8_t* data = (uint8_t*)request->data.textParameter.value;

  CirrusGwNetworkFormatParam(request->index, data);
  CirrusGwSetParam(request->index, data, request->size);
  NfcSendWriteAcknowledge(GenericProtocol_AcknowledgeResponse_Type_OK,
                          GenericProtocol_WriteRequest_Type_PARAMETER);
}

/**
 * Handle the write time command
 */
static void NfcHandleWriteTime(
    const GenericProtocol_TimeRequest* const request) {
  CirrusGwNetworkFormatParam(eCirrusParamDateTime, (uint8_t*)&request->time);
  CirrusGwSetParam(eCirrusParamDateTime, (uint8_t*)&request->time,
      sizeof(request->time));
  NfcSendWriteAcknowledge(GenericProtocol_AcknowledgeResponse_Type_OK,
      GenericProtocol_WriteRequest_Type_TIME);
}
