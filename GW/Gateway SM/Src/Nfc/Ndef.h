#ifndef NFC_NDEF_H_
#define NFC_NDEF_H_

#include "Driver/Nt3h2111_Defines.h"
#include <stdint.h>
#include <stdbool.h>

typedef struct {
  uint8_t payload[EEPROM_NDEF_SIZE];
  uint8_t length;
} EepromNdef_t;

typedef struct {
  uint8_t payload[NT3H2111_SIZE_SRAM];
  uint8_t length;
} SramNdef_t;

#define SRAM_NDEF_MESSAGE_HEADER_SIZE 2
#define SRAM_NDEF_RECORD_HEADER_SIZE  3
#define SRAM_NDEF_MESSAGE_EOF_SIZE    1
#define SRAM_NDEF_OVERHEAD_SIZE       (SRAM_NDEF_MESSAGE_HEADER_SIZE + SRAM_NDEF_RECORD_HEADER_SIZE + SRAM_NDEF_MESSAGE_EOF_SIZE)
#define MAX_NDEF_PAYLOAD_SIZE         NT3H2111_SIZE_SRAM - SRAM_NDEF_OVERHEAD_SIZE

#define NDEF_SOF                      3
#define NDEF_EOF                      0xfe
#define NDEF_RECORD_HEADER            0xd5
#define NDEF_TYPE_VALUE               0

bool NdefFromPayload(const uint8_t* const payload, const uint8_t count,
    SramNdef_t* const ndef);
uint8_t NdefGetPayload(uint8_t* const payload, const SramNdef_t* const ndef);

#endif /* NFC_NDEF_H_ */
