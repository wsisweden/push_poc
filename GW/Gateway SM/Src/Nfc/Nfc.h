#ifndef _NFC_H_
#define _NFC_H_

#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

void NfcTaskInit(void);
void NfcTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);

#endif
