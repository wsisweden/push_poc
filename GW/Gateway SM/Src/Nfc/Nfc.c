#include "Nfc.h"
#include "Ndef.h"
#include "Nfc_Defines.h"
#include "NfcMessageHandler.h"
#include "Driver/Nt3h2111.h"
#include "Driver/Nt3h2111_Debug.h"
#include <Hw/TiPhy/hal_defs.h>
#include <Hw/TIDriverLib/source/systick.h>
#include <Hw/Segger/RTT/SEGGER_RTT.h>
#include <string.h>

// Timer IDs
enum {
  TimerIdNfc_Poll = 1
};

typedef enum {
  NfcStateInitialize,
  NfcStateWaitForRf,
  NfcStateActive,
  NfcStateMessage
} NfcState_t;

// Static functions
static void NfcCycle(void);
static void NfcCycleWaitForRf(void);
static void NfcCycleActive(void);
static void NfcCycleMessage(void);
static void NfcCheckEepromAar(void);
static void NfcWriteSramAar(void);

// Static variables
static volatile NfcState_t nfcState;
static uint32_t nfcLastSystick;
static uint32_t nfcLastActive;

/**
 * Initialize the NFC state machine
 */
void NfcTaskInit(void) {
  PopStartTimer(TimerIdNfc_Poll, 500);
  nfcLastSystick = 0;
  nfcLastActive = 0;
  nfcState = NfcStateInitialize;
  NfcMessageInit();
}

/**
 * Handle NFC events
 */
void NfcTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {

  switch(iEvtId) {
    case gPopEvtTimer_c:
      switch(sEvtData.iTimerId) {
        case TimerIdNfc_Poll:
          NfcCycle();
          break;
        default:
          // Nothing to do
          break;
      }
      break;
    default:
      // Nothing to do
      break;
  }
}

/**
 * Poll for new data
 */
static void NfcCycle(void) {
  PopStartTimer(TimerIdNfc_Poll, 20);

  switch (nfcState) {
    case NfcStateInitialize:
      Nt3h2111_init();
      NfcCheckEepromAar();
      NfcWriteSramAar();
      nfcState = NfcStateWaitForRf;
      break;
    case NfcStateWaitForRf:
      NfcCycleWaitForRf();
      break;
    case NfcStateActive:
      NfcCycleActive();
      break;
    case NfcStateMessage:
      NfcCycleMessage();
      break;
    default:
      // Should not happen
      break;
  }
}

/**
 * Cycle the wait for RF state
 */
static void NfcCycleWaitForRf(void) {
  Nt3h2111_readStatus();
  if (Nt3h2111_isRfField()) {
    SEGGER_RTT_printf(0, "%3d, NFC cycle wait for RF: Enter active\r\n", PopGetSystick() - nfcLastSystick);
    SEGGER_RTT_printf(0, "%3d, NFC cycle message: Last active\r\n", PopGetSystick() - nfcLastActive);
    nfcLastSystick = PopGetSystick();
    nfcLastActive = PopGetSystick();
    Nt3h2111_enablePassThroughMode();
    nfcState = NfcStateActive;
  }
}

/**
 * Cycle the active state
 */
static void NfcCycleActive(void) {
  Nt3h2111_readStatus();
  if (!Nt3h2111_isRfField() || !Nt3h2111_isPassThroughMode()) {
    SEGGER_RTT_printf(0, "%3d, NFC cycle active: Enter wait for RF\r\n", PopGetSystick() - nfcLastSystick);
    nfcLastSystick = PopGetSystick();
    Nt3h2111_setPassThroughRfToI2c();
    NfcMessageInit();
    nfcState = NfcStateWaitForRf;
    return;
  }

  PopStartTimer(TimerIdNfc_Poll, 2);

  if (Nt3h2111_isDataAvailable()) {
    uint8_t payload[NT3H2111_SIZE_SRAM];

    SEGGER_RTT_printf(0, "%3d, NFC cycle active: Data available\r\n", PopGetSystick() - nfcLastSystick);
    nfcLastSystick = PopGetSystick();
    Nt3h2111_readSram(payload);
    NfcMessageHandle(payload, NT3H2111_SIZE_SRAM);

    if (NfcMessageIsSending() || NfcMessageIsReading()) {
      SEGGER_RTT_printf(0, "%3d, NFC cycle active: Enter message\r\n", PopGetSystick() - nfcLastSystick);
      nfcLastSystick = PopGetSystick();
      nfcState = NfcStateMessage;
    }
  }
}

/**
 * Cycle the message state
 */
static void NfcCycleMessage(void) {
  Nt3h2111_readStatus();
  if (!Nt3h2111_isRfField()) {
    SEGGER_RTT_printf(0, "%3d, NFC cycle message: Enter wait for RF\r\n", PopGetSystick() - nfcLastSystick);
    nfcLastSystick = PopGetSystick();
    Nt3h2111_setPassThroughRfToI2c();
    NfcMessageInit();
    nfcState = NfcStateWaitForRf;
    return;
  }

  PopStartTimer(TimerIdNfc_Poll, 2);

  if (NfcMessageIsSending() && Nt3h2111_isDataRead()) {
    SEGGER_RTT_printf(0, "%3d, NFC cycle message: Data read\r\n", PopGetSystick() - nfcLastSystick);
    nfcLastSystick = PopGetSystick();
    NfcMessageCallbackCycle();
  }

  NfcMessageCycle();

  if (NfcMessageIsIdle()) {
    Nt3h2111_readStatus();

    if (Nt3h2111_isDataRead()) {
      SEGGER_RTT_printf(0, "%3d, NFC cycle message: Enter active\r\n", PopGetSystick() - nfcLastSystick);
      SEGGER_RTT_printf(0, "%3d, NFC cycle message: Last active\r\n", PopGetSystick() - nfcLastActive);
      nfcLastSystick = PopGetSystick();
      nfcLastActive = PopGetSystick();
      Nt3h2111_setPassThroughRfToI2c();
      nfcState = NfcStateActive;
    }
  }
}

/**
 * Check that the AAR NDEF in EEPROM is correct
 */
static void NfcCheckEepromAar(void) {
  EepromNdef_t eepromNdef;

  eepromNdef.length = NFC_AAR_LENGTH;
  Nt3h2111_readEepromNdef(&eepromNdef);

  if (memcmp(eepromNdef.payload, Nfc_Aar_Message, NFC_AAR_LENGTH) != 0) {
    memcpy(eepromNdef.payload, Nfc_Aar_Message, NFC_AAR_LENGTH);
    Nt3h2111_writeEepromNdef(&eepromNdef);
  }
}

/**
 * Write the AAR NDEF to SRAM
 */
static void NfcWriteSramAar(void) {
  SramNdef_t sramNdef;

  sramNdef.length = MIN(NFC_AAR_LENGTH, NT3H2111_SIZE_SRAM);
  memcpy(sramNdef.payload, Nfc_Aar_Message, sramNdef.length);
  Nt3h2111_writeSramNdef(&sramNdef);
}
