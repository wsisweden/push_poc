#pragma once

#include <stdint.h>

#define BUFFER_SIZE 0x200

extern const uint8_t Nfc_Aar_Message[];
extern const uint8_t NFC_AAR_LENGTH;
