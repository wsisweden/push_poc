#include "Nfc_Defines.h"

const uint8_t Nfc_Aar_Message[] = {
    0x03, // Start of NDEF
      100, // NDEF Length - 3 (SOF + EOF + this byte)
    0x94, // MB + SR + External type
      27, // Type length
       0, // Payload length
    (uint8_t)'c', // Start of type
    (uint8_t)'o',
    (uint8_t)'m',
    (uint8_t)'.',
    (uint8_t)'m',
    (uint8_t)'i',
    (uint8_t)'c',
    (uint8_t)'r',
    (uint8_t)'o',
    (uint8_t)'p',
    (uint8_t)'o',
    (uint8_t)'w',
    (uint8_t)'e',
    (uint8_t)'r',
    (uint8_t)'_',
    (uint8_t)'g',
    (uint8_t)'r',
    (uint8_t)'o',
    (uint8_t)'u',
    (uint8_t)'p',
    (uint8_t)':',
    (uint8_t)'c',
    (uint8_t)'i',
    (uint8_t)'r',
    (uint8_t)'r',
    (uint8_t)'u',
    (uint8_t)'s', // End of type
    0x11, // SR + TNF (Well-known Type)
       1, // Type length
      21, // Payload length
    0x55, // Type (URI record)
    0x01, // Stands for http://www.
    (uint8_t)'m', // Start of URI
    (uint8_t)'i',
    (uint8_t)'c',
    (uint8_t)'r',
    (uint8_t)'o',
    (uint8_t)'p',
    (uint8_t)'o',
    (uint8_t)'w',
    (uint8_t)'e',
    (uint8_t)'r',
    (uint8_t)'-',
    (uint8_t)'g',
    (uint8_t)'r',
    (uint8_t)'o',
    (uint8_t)'u',
    (uint8_t)'p',
    (uint8_t)'.',
    (uint8_t)'c',
    (uint8_t)'o',
    (uint8_t)'m', // End of URI
    0x54, // ME + SR + External type
      15, // Type length
      27, // Payload length
    (uint8_t)'a', // Start of type
    (uint8_t)'n',
    (uint8_t)'d',
    (uint8_t)'r',
    (uint8_t)'o',
    (uint8_t)'i',
    (uint8_t)'d',
    (uint8_t)'.',
    (uint8_t)'c',
    (uint8_t)'o',
    (uint8_t)'m',
    (uint8_t)':',
    (uint8_t)'p',
    (uint8_t)'k',
    (uint8_t)'g', // End of type
    (uint8_t)'c', // Start of package name
    (uint8_t)'o',
    (uint8_t)'m',
    (uint8_t)'.',
    (uint8_t)'m',
    (uint8_t)'i',
    (uint8_t)'c',
    (uint8_t)'r',
    (uint8_t)'o',
    (uint8_t)'p',
    (uint8_t)'o',
    (uint8_t)'w',
    (uint8_t)'e',
    (uint8_t)'r',
    (uint8_t)'_',
    (uint8_t)'g',
    (uint8_t)'r',
    (uint8_t)'o',
    (uint8_t)'u',
    (uint8_t)'p',
    (uint8_t)'.',
    (uint8_t)'c',
    (uint8_t)'i',
    (uint8_t)'r',
    (uint8_t)'r',
    (uint8_t)'u',
    (uint8_t)'s', // End of package name
    0xfe, // End of NDEF
};
const uint8_t NFC_AAR_LENGTH = sizeof(Nfc_Aar_Message);
