#ifndef NFC_NFCMESSAGEHANDLER_H_
#define NFC_NFCMESSAGEHANDLER_H_

#include <stdint.h>
#include <stdbool.h>

void NfcMessageInit(void);
void NfcMessageCycle(void);
void NfcMessageCallbackCycle(void);
bool NfcMessageIsReading(void);
bool NfcMessageIsSending(void);
bool NfcMessageIsIdle(void);
void NfcMessageHandle(uint8_t* payload, const uint32_t count);

#endif /* NFC_NFCMESSAGEHANDLER_H_ */
