#ifndef _NT3H2111_I2C_H_
#define _NT3H2111_I2C_H_

#include <stdint.h>
#include <stdbool.h>

void Nt3h2111_I2c_init(void);
void Nt3h2111_I2c_setFdInterrupt(const bool enable);
bool Nt3h2111_I2c_readRegister(const uint8_t reg, uint8_t* const value);
bool Nt3h2111_I2c_writeRegister(const uint8_t reg, const uint8_t mask,
    const uint8_t value);
bool Nt3h2111_I2c_readBlock(const uint8_t address, uint8_t* const buf);
bool Nt3h2111_I2c_writeBlock(const uint8_t address, const uint8_t* const buf);

#endif
