#ifndef _NT3H2111_H_
#define _NT3H2111_H_

#include "../Ndef.h"

void Nt3h2111_init(void);
bool Nt3h2111_readEepromNdef(EepromNdef_t* const ndef);
bool Nt3h2111_writeEepromNdef(const EepromNdef_t* const ndef);
bool Nt3h2111_readSram(uint8_t* const payload);
bool Nt3h2111_writeSram(const uint8_t* const payload, const uint8_t count);
bool Nt3h2111_writeSramNdef(const SramNdef_t* const ndef);
bool Nt3h2111_readStatus(void);
bool Nt3h2111_isRfField(void);
bool Nt3h2111_isDataAvailable(void);
bool Nt3h2111_isDataRead(void);
bool Nt3h2111_isPassThroughMode(void);
bool Nt3h2111_isReady(void);
bool Nt3h2111_enablePassThroughMode(void);
bool Nt3h2111_setPassThroughRfToI2c(void);
bool Nt3h2111_setPassThroughI2cToRf(void);

#endif
