/*
 * Nt3h2111_Debug.h
 *
 *  Created on: 22 feb. 2017
 *      Author: AndCar_MEM
 */

#ifndef NFC_DRIVER_NT3H2111_DEBUG_H_
#define NFC_DRIVER_NT3H2111_DEBUG_H_

#include <stdbool.h>

bool Nt3h2111_Debug_isDebugging(void);

#endif /* NFC_DRIVER_NT3H2111_DEBUG_H_ */
