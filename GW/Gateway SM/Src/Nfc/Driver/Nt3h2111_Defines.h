#ifndef _NT3H2111_DEFINES_H_
#define _NT3H2111_DEFINES_H_

#define NT3H2111_ADDRESS 0x55
#define NT3H2111_BLOCK_SIZE                           16

#define NT3H2111_BLOCK_SESSION_REGS                   0xfe
#define NT3H2111_BLOCK_MANAGEMENT                     0
#define NT3H2111_I2C_ADDRESS_SELECT                   0

#define NT3H2111_BLOCK_START_ADDR_USER_MEMORY         0x01
#define NT3H2111_START_ADDR_USER_MEMORY               (NT3H2111_BLOCK_START_ADDR_USER_MEMORY * NT3H2111_BLOCK_SIZE)
#define NT3H2111_BLOCK_START_ADDR_SRAM                0xf8
#define NT3H2111_START_ADDR_SRAM                      (NT3H2111_BLOCK_START_ADDR_SRAM * NT3H2111_BLOCK_SIZE)
#define NT3H2111_BLOCK_SIZE_SRAM                      4
#define NT3H2111_BLOCK_END_ADDR_SRAM                  (NT3H2111_BLOCK_START_ADDR_SRAM + NT3H2111_BLOCK_SIZE_SRAM - 1)
#define NT3H2111_SIZE_SRAM                            (NT3H2111_BLOCK_SIZE_SRAM * NT3H2111_BLOCK_SIZE)

#define EEPROM_NDEF_SIZE 0xff

#define NT3H2111_NC_REG_MASK_I2C_RST_ON_OFF           0x80
#define NT3H2111_NC_REG_MASK_PTHRU_SRAM               0x40
#define NT3H2111_NC_REG_MASK_FD_OFF                   0x30
#define NT3H2111_NC_REG_MASK_FD_ON                    0x0C
#define NT3H2111_NC_REG_MASK_SRAM_MIRROR              0x02
#define NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF          0x01

#define NT3H2111_NS_REG_MASK_RF_FIELD_PRESENT         0x01
#define NT3H2111_NS_REG_MASK_EEPROM_WR_BUSY           0x02
#define NT3H2111_NS_REG_MASK_EEPROM_WR_ERR            0x04
#define NT3H2111_NS_REG_MASK_SRAM_RF_READY            0x08
#define NT3H2111_NS_REG_MASK_SRAM_I2C_READY           0x10
#define NT3H2111_NS_REG_MASK_RF_IF_ON_OFF             0x20
#define NT3H2111_NS_REG_MASK_I2C_IF_ON_OFF            0x40
#define NT3H2111_NS_REG_MASK_NDEF_DATA_TRANS          0x80

typedef enum {
  Nt3h2111_SessionRegisterNc = 0,
  Nt3h2111_SessionRegisterLd = 1,
  Nt3h2111_SessionRegisterSm = 2,
  Nt3h2111_SessionRegisterWdtLs = 3,
  Nt3h2111_SessionRegisterWdtMs = 4,
  Nt3h2111_SessionRegisterI2cc = 5,
  Nt3h2111_SessionRegisterNs = 6
} Nt3h2111_SessionRegister_t;

#endif
