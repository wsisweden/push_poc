#include "Nt3h2111_I2c.h"
#include "Nt3h2111_Defines.h"
#include <Hw/TIDriverLib/inc/hw_memmap.h>
#include <Hw/TIDriverLib/inc/hw_ints.h>
#include <Hw/TIDriverLib/inc/hw_ioc.h>
#include <Hw/TIDriverLib/source/gpio.h>
#include <Hw/TIDriverLib/source/sys_ctrl.h>
#include <Hw/TIDriverLib/source/ioc.h>
#include <Hw/TIDriverLib/source/i2c.h>
#include <Hw/TIDriverLib/source/interrupt.h>
#include <PopNet/PopBsp.h>
#include <string.h>

// NFC HW
#define GPIO_I2C_BASE GPIO_C_BASE
#define PIN_I2C_SCL GPIO_PIN_6
#define PIN_I2C_SDA GPIO_PIN_7

#define GPIO_NFC_FD_BASE GPIO_B_BASE
#define PIN_NFC_FD GPIO_PIN_6

// Static functions
static bool Nt3h2111_I2c_readBytes(uint8_t* const data, const uint8_t count);
static bool Nt3h2111_I2c_writeBytes(uint8_t* const data, const uint8_t count);
static bool Nt3h2111_I2c_readByte(uint8_t* const data);
static bool Nt3h2111_I2c_writeByte(const uint8_t data);
static void Nt3h2111_I2c_fieldDetectInt(void);

/**
 * Initialize the NT3H2111 I2C communication
 */
void Nt3h2111_I2c_init(void) {
  IntMasterDisable();

  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_I2C);
  SysCtrlPeripheralReset(SYS_CTRL_PERIPH_I2C);

  GPIOPinTypeI2C(GPIO_I2C_BASE, PIN_I2C_SCL);
  GPIOPinTypeI2C(GPIO_I2C_BASE, PIN_I2C_SDA);

  IOCPinConfigPeriphInput(GPIO_I2C_BASE, PIN_I2C_SCL, IOC_I2CMSSCL);
  IOCPinConfigPeriphInput(GPIO_I2C_BASE, PIN_I2C_SDA, IOC_I2CMSSDA);
  IOCPinConfigPeriphOutput(GPIO_I2C_BASE, PIN_I2C_SCL,
      IOC_MUX_OUT_SEL_I2C_CMSSCL);
  IOCPinConfigPeriphOutput(GPIO_I2C_BASE, PIN_I2C_SDA,
      IOC_MUX_OUT_SEL_I2C_CMSSDA);

  GPIOPinTypeGPIOInput(GPIO_NFC_FD_BASE, PIN_NFC_FD);
  IOCPadConfigSet(GPIO_NFC_FD_BASE, PIN_NFC_FD, IOC_OVERRIDE_PUE);

  GPIOIntTypeSet(GPIO_NFC_FD_BASE, PIN_NFC_FD, GPIO_BOTH_EDGES);

  I2CMasterInitExpClk(SysCtrlClockGet(), true);

  IntMasterEnable();

  GPIOPortIntRegister(GPIO_NFC_FD_BASE, &Nt3h2111_I2c_fieldDetectInt);
}

/**
 * Set if the Field detect interrupt shall be enabled
 */
void Nt3h2111_I2c_setFdInterrupt(const bool enable) {
  if (enable)
    GPIOPinIntEnable(GPIO_NFC_FD_BASE, PIN_NFC_FD);
  else
    GPIOPinIntDisable(GPIO_NFC_FD_BASE, PIN_NFC_FD);
}

/**
 * Read value from register
 */
bool Nt3h2111_I2c_readRegister(const uint8_t reg, uint8_t* const value) {
  uint8_t buf[] = {
      NT3H2111_BLOCK_SESSION_REGS,
      reg
  };

  if (!Nt3h2111_I2c_writeBytes(buf, 2))
    return false;

  return Nt3h2111_I2c_readByte(value);
}

/**
 * Write value to register
 */
bool Nt3h2111_I2c_writeRegister(const uint8_t reg, const uint8_t mask,
    const uint8_t value) {
  uint8_t buf[] = {
      NT3H2111_BLOCK_SESSION_REGS,
      reg,
      mask,
      value
  };

  return Nt3h2111_I2c_writeBytes(buf, 4);
}

/**
 * Read data from block
 */
bool Nt3h2111_I2c_readBlock(const uint8_t address, uint8_t* const buf) {
  if (!Nt3h2111_I2c_writeByte(address))
    return false;

  return Nt3h2111_I2c_readBytes(buf, NT3H2111_BLOCK_SIZE);
}

/**
 * Write data to block
 */
bool Nt3h2111_I2c_writeBlock(const uint8_t address, const uint8_t* const buf) {
  uint8_t i = 0;
  uint8_t temp[NT3H2111_BLOCK_SIZE + 1];
  temp[0] = address;
  memcpy(&temp[1], buf, NT3H2111_BLOCK_SIZE);

  if (!Nt3h2111_I2c_writeBytes(temp, NT3H2111_BLOCK_SIZE + 1))
    return false;

  // Don't wait if block is in SRAM
  if (address >= NT3H2111_BLOCK_START_ADDR_SRAM && address <= NT3H2111_BLOCK_END_ADDR_SRAM )
    return true;

  // Wait for EEPROM write 4,8ms
  for (i = 0; i < 2; ++i) {
    uint8_t nsReg;

    DelayMs(5);
    if (!Nt3h2111_I2c_readRegister(Nt3h2111_SessionRegisterNs, &nsReg))
      return false;
    if (!(nsReg & NT3H2111_NS_REG_MASK_EEPROM_WR_BUSY))
      return true;
  }

  return false;
}

/**
 * Read bytes from the requested address
 */
static bool Nt3h2111_I2c_readBytes(uint8_t* const data, const uint8_t count) {
  uint32_t err = 0;
  uint8_t i;

  // If count is 0, nothing to do
  if (count == 0)
    return true;

  I2CMasterSlaveAddrSet(NT3H2111_ADDRESS, true);

  for (i = 0; i < count; ++i) {
    if (i == 0)
      I2CMasterControl(I2C_MASTER_CMD_BURST_RECEIVE_START);
    else if (i < count - 1)
      I2CMasterControl(I2C_MASTER_CMD_BURST_RECEIVE_CONT);
    else
      I2CMasterControl(I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
    while(I2CMasterBusy()) {}
    err = I2CMasterErr();

    if (err == I2C_MASTER_ERR_NONE)
      data[i] = (uint8_t)(I2CMasterDataGet() & 0xff);
    else {
      I2CMasterControl(I2C_MASTER_CMD_BURST_RECEIVE_ERROR_STOP);
      while(I2CMasterBusy()) {}
      return false;
    }
  }

  return true;
}

/**
 * Write bytes to the requested address
 */
static bool Nt3h2111_I2c_writeBytes(uint8_t* const data, const uint8_t count) {
  uint32_t err = 0;
  uint8_t i;

  // If count is 0, nothing to do
  if (count == 0)
    return true;

  I2CMasterSlaveAddrSet(NT3H2111_ADDRESS, false);

  for (i = 0; i < count; ++i) {
    I2CMasterDataPut(data[i]);

    if (i == 0)
      I2CMasterControl(I2C_MASTER_CMD_BURST_SEND_START);
    else if (i < count - 1)
      I2CMasterControl(I2C_MASTER_CMD_BURST_SEND_CONT);
    else
      I2CMasterControl(I2C_MASTER_CMD_BURST_SEND_FINISH);
    while(I2CMasterBusy()) {}
    err = I2CMasterErr();

    if (err != I2C_MASTER_ERR_NONE) {
      I2CMasterControl(I2C_MASTER_CMD_BURST_SEND_ERROR_STOP);
      while(I2CMasterBusy()) {}
      return false;
    }
  }

  return true;
}

/**
 * Read byte from the requested address
 */
static bool Nt3h2111_I2c_readByte(uint8_t* const data) {
  uint32_t err = 0;

  I2CMasterSlaveAddrSet(NT3H2111_ADDRESS, true);
  I2CMasterControl(I2C_MASTER_CMD_SINGLE_RECEIVE);
  while(I2CMasterBusy()) {}
  err = I2CMasterErr();

  if (err == I2C_MASTER_ERR_NONE)
    *data = (uint8_t)(I2CMasterDataGet() & 0xff);

  return err == I2C_MASTER_ERR_NONE;
}

/**
 * Write byte to the requested address
 */
static bool Nt3h2111_I2c_writeByte(const uint8_t data) {
  uint32_t err = 0;

  I2CMasterSlaveAddrSet(NT3H2111_ADDRESS, false);
  I2CMasterDataPut(data);
  I2CMasterControl(I2C_MASTER_CMD_SINGLE_SEND);
  while(I2CMasterBusy()) {}
  err = I2CMasterErr();

  return err == I2C_MASTER_ERR_NONE;
}

/**
 * Handle the field detect interrupt
 */
static void Nt3h2111_I2c_fieldDetectInt(void) {
  uint32_t ui32GPIOIntStatus = GPIOPinIntStatus(GPIO_NFC_FD_BASE, true);
  GPIOPinIntClear(GPIO_NFC_FD_BASE, ui32GPIOIntStatus);
}
