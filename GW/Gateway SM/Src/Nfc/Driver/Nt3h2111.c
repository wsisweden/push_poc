#include "Nt3h2111.h"
#include "Nt3h2111_I2c.h"
#include <Hw/TiPhy/hal_defs.h>
#include <string.h>

// Static functions
static bool Nt3h2111_isBitsSet(const uint8_t value, const uint8_t bits);
static bool Nt3h2111_read(const uint16_t address, uint8_t* const data,
    const uint8_t count);
static bool Nt3h2111_write(const uint16_t address, const uint8_t* const data,
    const uint8_t count);
static bool Nt3h2111_setRegisterBits(const Nt3h2111_SessionRegister_t reg,
    const uint8_t bits);
static bool Nt3h2111_resetRegisterBits(const Nt3h2111_SessionRegister_t reg,
    const uint8_t bits);
static bool Nt3h2111_readRegister(const Nt3h2111_SessionRegister_t reg,
    uint8_t* const value);
static bool Nt3h2111_writeRegister(const Nt3h2111_SessionRegister_t reg,
    const uint8_t mask, const uint8_t value);

static const uint8_t nt3h2111_ndef_init_data_address = 12;
static const uint8_t nt3h2111_ndef_init_data[] = {
    0xe1, 0x10, 0x6d, 0x00
};

// Static variables
static uint8_t nt3h2111_status = 0;

/**
 * Initialize the NT3H2111
 */
void Nt3h2111_init(void) {
  uint8_t buf[NT3H2111_BLOCK_SIZE];

  Nt3h2111_I2c_init();
  Nt3h2111_I2c_setFdInterrupt(false);
  Nt3h2111_I2c_readBlock(0, buf); // Receive dummy bytes to clean bus

  // Mirror SRAM to first user memory block and enable pass-through mode
  Nt3h2111_resetRegisterBits(Nt3h2111_SessionRegisterNc,
      NT3H2111_NC_REG_MASK_I2C_RST_ON_OFF | NT3H2111_NC_REG_MASK_SRAM_MIRROR);
  Nt3h2111_setRegisterBits(Nt3h2111_SessionRegisterNc,
      NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF |
      NT3H2111_NC_REG_MASK_FD_OFF | NT3H2111_NC_REG_MASK_FD_ON);

  Nt3h2111_read(nt3h2111_ndef_init_data_address, buf,
      sizeof(nt3h2111_ndef_init_data));
  if (memcmp(buf, nt3h2111_ndef_init_data,
      sizeof(nt3h2111_ndef_init_data)) != 0) {
    Nt3h2111_write(nt3h2111_ndef_init_data_address, nt3h2111_ndef_init_data,
        sizeof(nt3h2111_ndef_init_data));
  }
}

/**
 * Read NDEF from EEPROM
 */
bool Nt3h2111_readEepromNdef(EepromNdef_t* const ndef) {
  return Nt3h2111_read(NT3H2111_START_ADDR_USER_MEMORY,
      ndef->payload, EEPROM_NDEF_SIZE);
}

/**
 * Write NDEF to EEPROM
 */
bool Nt3h2111_writeEepromNdef(const EepromNdef_t* const ndef) {
  return Nt3h2111_write(NT3H2111_START_ADDR_USER_MEMORY,
      ndef->payload, ndef->length);
}

/**
 * Read data from SRAM
 */
bool Nt3h2111_readSram(uint8_t* const payload) {
  return Nt3h2111_read(NT3H2111_START_ADDR_SRAM, payload,
      NT3H2111_SIZE_SRAM);
}

/**
 * Write data to SRAM
 */
bool Nt3h2111_writeSram(const uint8_t* const payload, const uint8_t count) {
  // Nothing to do
  if (count == 0)
    return true;

  return Nt3h2111_write(NT3H2111_START_ADDR_SRAM, payload, count);
}

/**
 * Write NDEF to SRAM
 */
bool Nt3h2111_writeSramNdef(const SramNdef_t* const ndef) {
  // Nothing to do
  if (ndef->length == 0)
    return true;

  return Nt3h2111_write(NT3H2111_START_ADDR_SRAM, ndef->payload, ndef->length);
}

/**
 * Read the status register
 */
bool Nt3h2111_readStatus(void) {
  uint8_t reg;
  if (!Nt3h2111_readRegister(Nt3h2111_SessionRegisterNs, &reg))
    return false;

  if (reg != nt3h2111_status)
    nt3h2111_status = reg;

  return true;
}

/**
 * If the RF field is detected
 */
bool Nt3h2111_isRfField(void) {
  return Nt3h2111_isBitsSet(nt3h2111_status,
      NT3H2111_NS_REG_MASK_RF_FIELD_PRESENT);
}

/**
 * If data is available
 */
bool Nt3h2111_isDataAvailable(void) {
  return Nt3h2111_isBitsSet(nt3h2111_status,
      NT3H2111_NS_REG_MASK_SRAM_I2C_READY);
}

/**
 * If I2C data is read by RF
 */
bool Nt3h2111_isDataRead(void) {
  return !Nt3h2111_isBitsSet(nt3h2111_status,
      NT3H2111_NS_REG_MASK_SRAM_RF_READY);
}

/**
 * If pass through mode is enabled
 */
bool Nt3h2111_isPassThroughMode(void) {
  uint8_t reg;
  if (!Nt3h2111_readRegister(Nt3h2111_SessionRegisterNc, &reg))
    return false;

  return Nt3h2111_isBitsSet(reg,
      NT3H2111_NC_REG_MASK_PTHRU_SRAM);
}

/**
 * If the NFC is ready for write
 */
bool Nt3h2111_isReady(void) {
  return !Nt3h2111_isBitsSet(nt3h2111_status,
      NT3H2111_NS_REG_MASK_RF_IF_ON_OFF);
}

/**
 * Enable the pass through mode
 */
bool Nt3h2111_enablePassThroughMode(void) {
  return Nt3h2111_setRegisterBits(Nt3h2111_SessionRegisterNc,
      NT3H2111_NC_REG_MASK_PTHRU_SRAM);
}

/**
 * Set pass through direction RF to I2C
 */
bool Nt3h2111_setPassThroughRfToI2c(void) {
  return Nt3h2111_setRegisterBits(Nt3h2111_SessionRegisterNc,
      NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF);
}

/**
 * Set pass through direction I2C to RF
 */
bool Nt3h2111_setPassThroughI2cToRf(void) {
  return Nt3h2111_resetRegisterBits(Nt3h2111_SessionRegisterNc,
      NT3H2111_NC_REG_MASK_RF_WRITE_ON_OFF);
}

/**
 * Check if bits is set
 */
static bool Nt3h2111_isBitsSet(const uint8_t value, const uint8_t bits) {
  return (bits & value) == bits;
}

/**
 * Read data
 */
static bool Nt3h2111_read(const uint16_t address, uint8_t* const data,
    const uint8_t count) {
  uint8_t buf[NT3H2111_BLOCK_SIZE];
  uint16_t bytesRead = 0;

  while (bytesRead < count) {
    uint8_t currentBlock = (address + bytesRead) / NT3H2111_BLOCK_SIZE;
    uint8_t blockPos = (address + bytesRead) % NT3H2111_BLOCK_SIZE;
    uint8_t currentCount =
        MIN(count - bytesRead, NT3H2111_BLOCK_SIZE - blockPos);

    if (currentCount < NT3H2111_BLOCK_SIZE) {
      // Read whole block
      if (!Nt3h2111_I2c_readBlock(currentBlock, buf))
        return false;

      // Copy data from buffer
      memcpy(&data[bytesRead], &buf[blockPos], currentCount);
    } else {
      // Read block
      if (!Nt3h2111_I2c_readBlock(currentBlock, &data[bytesRead]))
        return false;
    }

    bytesRead += currentCount;
  }

  return true;
}

/**
 * Write data
 */
static bool Nt3h2111_write(const uint16_t address, const uint8_t* const data,
    const uint8_t count) {
  uint16_t bytesWritten = 0;
  uint8_t buf[NT3H2111_BLOCK_SIZE];

  while (bytesWritten < count) {
    uint8_t currentBlock = (address + bytesWritten) / NT3H2111_BLOCK_SIZE;
    uint8_t blockPos = (address + bytesWritten) % NT3H2111_BLOCK_SIZE;
    uint8_t currentCount =
        MIN(count - bytesWritten, NT3H2111_BLOCK_SIZE - blockPos);

    if (currentCount < NT3H2111_BLOCK_SIZE) {
      // Read current data
      if (!Nt3h2111_I2c_readBlock(currentBlock, buf))
        return false;

      // Don't overwrite the I2C address
      if ((NT3H2111_BLOCK_MANAGEMENT == currentBlock) &&
          (NT3H2111_I2C_ADDRESS_SELECT < blockPos))
        buf[0] = (NT3H2111_ADDRESS << 1);

      // Write current data
      memcpy(&buf[blockPos], &data[bytesWritten], currentCount);
      if (!Nt3h2111_I2c_writeBlock(currentBlock, buf))
        return false;
    } else {
      // Write block
      memcpy(buf, &data[bytesWritten], NT3H2111_BLOCK_SIZE);
      if (!Nt3h2111_I2c_writeBlock(currentBlock, buf))
        return false;
    }

    bytesWritten += currentCount;
  }

  return true;
}

/**
 * Set register bits
 */
static bool Nt3h2111_setRegisterBits(const Nt3h2111_SessionRegister_t reg,
    const uint8_t bits) {
  uint8_t current;

  if (!Nt3h2111_readRegister(reg, &current))
    return false;

  if ((current & bits) != bits) {
    current |= bits;
    return Nt3h2111_writeRegister(reg, bits, current);
  }

  return true;
}

/**
 * Reset register bits
 */
static bool Nt3h2111_resetRegisterBits(const Nt3h2111_SessionRegister_t reg,
    const uint8_t bits) {
  uint8_t current;

  if (!Nt3h2111_readRegister(reg, &current))
    return false;

  if (current & bits) {
    current &= ~bits;
    return Nt3h2111_writeRegister(reg, bits, current);
  }

  return true;
}

/**
 * Read a register
 */
static bool Nt3h2111_readRegister(const Nt3h2111_SessionRegister_t reg,
    uint8_t* const value) {
  return Nt3h2111_I2c_readRegister(reg, value);
}

/**
 * Write a register
 */
static bool Nt3h2111_writeRegister(const Nt3h2111_SessionRegister_t reg,
    const uint8_t mask, const uint8_t value) {
  return Nt3h2111_I2c_writeRegister(reg, mask, value);
}
