/*
 * Nt3h2111_Debug.c
 *
 *  Created on: 22 feb. 2017
 *      Author: AndCar_MEM
 */

#include "Nt3h2111_Debug.h"
#include <Hw/Cpu/ARMCM3.h>

/**
 * Initialize the debug driver
 */
bool Nt3h2111_Debug_isDebugging(void) {
  if ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) ==
      CoreDebug_DHCSR_C_DEBUGEN_Msk)
    return true;
  return false;
}
