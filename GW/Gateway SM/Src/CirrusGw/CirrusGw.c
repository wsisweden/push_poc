#include "CirrusGw.h"
#include "../MP/MpNwk.h"
#include "../MP/MpGw.h"
#include <string.h>
#include <PopNet/PopBsp.h>

// Private variables

static CirrusGwInfo_t cirrusGwInfo;
static CirrusGwStatus_t cirrusGwStatus;
static CirrusGwConfig_t cirrusGwConfig;

// External variables

extern uint32_t giFwType;
extern uint32_t giFwVer;

// Public functions

/**
 * Initialize the Cirrus gateway data
 */
void CirrusGwInit(void) {
  memset(&cirrusGwInfo, 0, sizeof(CirrusGwInfo_t));
  memset(&cirrusGwStatus, 0, sizeof(CirrusGwStatus_t));
  memset(&cirrusGwConfig, 0, sizeof(CirrusGwConfig_t));

  cirrusGwInfo.firmwareType = giFwType;
  cirrusGwInfo.firmwareVersion = giFwVer;
}

/**
 * Get the information data
 * @return The data
 */
CirrusGwInfo_t* CirrusGwGetInfo(void) {
  return &cirrusGwInfo;
}

/**
 * Get the status data
 * @return The data
 */
CirrusGwStatus_t* CirrusGwGetStatus(void) {
  return &cirrusGwStatus;
}

/**
 * Get the configuration data
 * @return The data
 */
CirrusGwConfig_t* CirrusGwGetConfig(void) {
  return &cirrusGwConfig;
}

/**
 * Set the information data
 * @param info The data
 */
void CirrusGwSetInfo(const CirrusGwInfo_t* const info) {
  memcpy(&cirrusGwInfo, info, sizeof(CirrusGwInfo_t));
}

/**
 * Set the status data
 * @param status The data
 */
void CirrusGwSetStatus(const CirrusGwStatus_t* const status) {
  memcpy(&cirrusGwStatus, status, sizeof(CirrusGwStatus_t));
}

/**
 * Set the configuration data
 * @param config The data
 */
void CirrusGwSetConfig(const CirrusGwConfig_t* const config) {
  memcpy(&cirrusGwConfig, config, sizeof(CirrusGwConfig_t));
}

/**
 * Set a paramter data
 * @param index The paramter index
 * @param data The data
 * @return If the data was set
 */
bool CirrusGwSetParam(const uint32_t index, const uint8_t* const data,
                      const int32_t dataLength) {
  uint8_t payload[200];
  bool res = CirrusGwSetParamInternal(index, data, dataLength);
  sMpMsgSetCirrusParamReq_t setCirrusParamReq;
  sPopNwkDataIndication_t* indication = (sPopNwkDataIndication_t*)payload;

  setCirrusParamReq.index = index;
  memset(setCirrusParamReq.data, 0, sizeof(setCirrusParamReq.data));
  memcpy(setCirrusParamReq.data, data, dataLength);
  SwapBytes32(&setCirrusParamReq.index);

  indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
  indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gSetCirrusParamReq_c;
  indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
  indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
  PopMemCpy(&indication->sFrame.aPayload[MP_ACCESS_DATA], &setCirrusParamReq,
            sizeof(sMpMsgSetCirrusParamReq_t));
  indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
      sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA +
        sizeof(setCirrusParamReq.index) + dataLength;
  MpGwDataIndication(indication);

  return res;
}

/**
 * Set a paramter data used by the gateway
 * @param index The paramter index
 * @param data The data
 * @return If the data was set
 */
bool CirrusGwSetParamInternal(const uint32_t index, const uint8_t* const data,
                              const int32_t dataLength) {
  switch (index) {
    case eCirrusParamSoftwareType:
      if (dataLength == 4) {
        cirrusGwInfo.softwareType = *((uint32_t*)data);
        SwapBytes32(&cirrusGwInfo.softwareType);
        return true;
      } else {
        return false;
      }
    case eCirrusParamSoftwareVersion:
      if (dataLength == 4) {
        cirrusGwInfo.softwareVersion = *((uint32_t*)data);
        SwapBytes32(&cirrusGwInfo.softwareVersion);
        return true;
      } else {
        return false;
      }
    case eCirrusParamFirmwareType:
      if (dataLength == 4) {
        cirrusGwInfo.firmwareType = *((uint32_t*)data);
        SwapBytes32(&cirrusGwInfo.firmwareType);
        return true;
      } else {
        return false;
      }
    case eCirrusParamFirmwareVersion:
      if (dataLength == 4) {
        cirrusGwInfo.firmwareVersion = *((uint32_t*)data);
        SwapBytes32(&cirrusGwInfo.firmwareVersion);
        return true;
      } else {
        return false;
      }
    case eCirrusParamMui:
      if (dataLength == 8) {
        memcpy(&cirrusGwInfo.mui, data, 8);
        SwapBytes64(&cirrusGwInfo.mui);
        return true;
      } else {
        return false;
      }
    case eCirrusParamBuildCommit:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwInfo.buildCommit, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamQtVersion:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwInfo.qtVersion, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamEthernetMac:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwInfo.ethernetMac, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamWirelessMac:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwInfo.wirelessMac, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamDeviceId:
      if (dataLength == 4) {
        cirrusGwConfig.deviceId = *((uint32_t*)data);
        SwapBytes32(&cirrusGwConfig.deviceId);
        return true;
      } else {
        return false;
      }
    case eCirrusParamName:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.gatewayName, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamCloudUrl:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.cloudUrl, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamNetworkRestriction:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.networkRestriction, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamNodeRestriction:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.nodeRestriction, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamCloudUsername:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.cloudUserName, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamCloudPassword:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.cloudPassword, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamScanNetworkInterval:
      if (dataLength == 4) {
        cirrusGwConfig.scanNetworkInterval = *((uint32_t*)data);
        SwapBytes32(&cirrusGwConfig.scanNetworkInterval);
        return true;
      } else {
        return false;
      }
    case eCirrusParamRemoveNodeInterval:
      if (dataLength == 4) {
        cirrusGwConfig.removeNodeInterval = *((uint32_t*)data);
        SwapBytes32(&cirrusGwConfig.removeNodeInterval);
        return true;
      } else {
        return false;
      }
    case eCirrusParamCollectDataInterval:
      if (dataLength == 4) {
        cirrusGwConfig.collectDataInterval = *((uint32_t*)data);
        SwapBytes32(&cirrusGwConfig.collectDataInterval);
        return true;
      } else {
        return false;
      }
    case eCirrusParamPeriodicUpdateInterval:
      if (dataLength == 4) {
        cirrusGwConfig.periodicUpdateInterval = *((uint32_t*)data);
        SwapBytes32(&cirrusGwConfig.periodicUpdateInterval);
        return true;
      } else {
        return false;
      }
    case eCirrusParamSshActive:
      if (dataLength == 1) {
        cirrusGwConfig.sshActive = *((bool*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamDebugLog:
      if (dataLength == 1) {
        cirrusGwConfig.debugLog = *((bool*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamDebugMpaLog:
      if (dataLength == 1) {
        cirrusGwConfig.debugMpaLog = *((bool*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamEthernetAddress:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.ethernetAddress, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamEthernetStatic:
      if (dataLength == 1) {
        cirrusGwConfig.ethernetStatic = *((bool*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamEthernetNetmask:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.ethernetNetmask, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamEthernetGateway:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.ethernetGateway, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamWirelessAddress:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.wirelessAddress, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamFoundNodes:
      if (dataLength == 4) {
        cirrusGwStatus.foundNodes = *((uint32_t*)data);
        SwapBytes32(&cirrusGwStatus.foundNodes);
        return true;
      } else {
        return false;
      }
    case eCirrusParamWirelessSignalStrength:
      if (dataLength == 4) {
        cirrusGwStatus.wirelessSignalStrength = *((uint32_t*)data);
        SwapBytes32((uint32_t*)&cirrusGwStatus.wirelessSignalStrength);
        return true;
      } else {
        return false;
      }
    case eCirrusParamWirelessLinkQuality:
      if (dataLength == 4) {
        cirrusGwStatus.wirelessLinkQuality = *((uint32_t*)data);
        SwapBytes32(&cirrusGwStatus.wirelessLinkQuality);
        return true;
      } else {
        return false;
      }
    case eCirrusParamWirelessNetmask:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.wirelessNetmask, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamWirelessSsid:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.wirelessSsid, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamWirelessPassword:
      if (data[dataLength - 1] == '\0') {
        strcpy((char*)cirrusGwConfig.wirelessPassword, (char*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamFreeStorage:
      if (dataLength == 8) {
        memcpy(&cirrusGwStatus.freeStorage, data, 8);
        SwapBytes64(&cirrusGwStatus.freeStorage);
        return true;
      } else {
        return false;
      }
    case eCirrusParamDateTime:
      if (dataLength == 4) {
        cirrusGwStatus.deviceTime = *((uint32_t*)data);
        SwapBytes32(&cirrusGwStatus.deviceTime);
        return true;
      } else {
        return false;
      }
    case eCirrusParamSecurityCode:
      if (dataLength == 4) {
        cirrusGwConfig.securityCode = *((uint32_t*)data);
        SwapBytes32(&cirrusGwConfig.securityCode);
        return true;
      } else {
        return false;
      }
    case eCirrusParamSerialNumber:
      if (dataLength == 8) {
        memcpy(&cirrusGwConfig.serialNumber, data, 8);
        SwapBytes64(&cirrusGwConfig.serialNumber);
        return true;
      } else {
        return false;
      }
    case eCirrusParamFactoryDefault:
      if (dataLength == 1) {
        cirrusGwStatus.factoryDefault = *((bool*)data);
        return true;
      } else {
        return false;
      }
    case eCirrusParamReady:
      if (dataLength == 1) {
        cirrusGwStatus.ready = *((bool*)data);
        return true;
      } else {
        return false;
      }
    default:
      return false;
  }
}

/**
 * Get a paramter data
 * @param index The paramter index to get
 * @param data The buffer for the parameter data
 * @return The data size
 */
uint32_t CirrusGwGetParam(const uint32_t index, const uint8_t* data) {
  switch (index) {
    case eCirrusParamSoftwareType:
      *((uint32_t*)data) = cirrusGwInfo.softwareType;
      return sizeof(cirrusGwInfo.softwareType);
    case eCirrusParamSoftwareVersion:
      *((uint32_t*)data) = cirrusGwInfo.softwareVersion;
      return sizeof(cirrusGwInfo.softwareVersion);
    case eCirrusParamFirmwareType:
      *((uint32_t*)data) = cirrusGwInfo.firmwareType;
      return sizeof(cirrusGwInfo.firmwareType);
    case eCirrusParamFirmwareVersion:
      *((uint32_t*)data) = cirrusGwInfo.firmwareVersion;
      return sizeof(cirrusGwInfo.firmwareVersion);
    case eCirrusParamMui:
      *((uint64_t*)data) = cirrusGwInfo.mui;
      return sizeof(cirrusGwInfo.mui);
    case eCirrusParamBuildCommit:
      strcpy((char*)data, (char*)cirrusGwInfo.buildCommit);
      return strlen((char*)cirrusGwInfo.buildCommit);
    case eCirrusParamQtVersion:
      strcpy((char*)data, (char*)cirrusGwInfo.qtVersion);
      return strlen((char*)cirrusGwInfo.qtVersion);
    case eCirrusParamEthernetMac:
      strcpy((char*)data, (char*)cirrusGwInfo.ethernetMac);
      return strlen((char*)cirrusGwInfo.ethernetMac);
    case eCirrusParamWirelessMac:
      strcpy((char*)data, (char*)cirrusGwInfo.wirelessMac);
      return strlen((char*)cirrusGwInfo.wirelessMac);
    case eCirrusParamDeviceId:
      *((uint32_t*)data) = cirrusGwConfig.deviceId;
      return sizeof(cirrusGwConfig.deviceId);
    case eCirrusParamName:
      strcpy((char*)data, (char*)cirrusGwConfig.gatewayName);
      return strlen((char*)cirrusGwConfig.gatewayName);
    case eCirrusParamCloudUrl:
      strcpy((char*)data, (char*)cirrusGwConfig.cloudUrl);
      return strlen((char*)cirrusGwConfig.cloudUrl);
    case eCirrusParamNetworkRestriction:
      strcpy((char*)data, (char*)cirrusGwConfig.networkRestriction);
      return strlen((char*)cirrusGwConfig.networkRestriction);
    case eCirrusParamNodeRestriction:
      strcpy((char*)data, (char*)cirrusGwConfig.nodeRestriction);
      return strlen((char*)cirrusGwConfig.nodeRestriction);
    case eCirrusParamCloudUsername:
      strcpy((char*)data, (char*)cirrusGwConfig.cloudUserName);
      return strlen((char*)cirrusGwConfig.cloudUserName);
    case eCirrusParamCloudPassword:
      strcpy((char*)data, (char*)cirrusGwConfig.cloudPassword);
      return strlen((char*)cirrusGwConfig.cloudPassword);
    case eCirrusParamScanNetworkInterval:
      *((uint32_t*)data) = cirrusGwConfig.scanNetworkInterval;
      return sizeof(cirrusGwConfig.scanNetworkInterval);
    case eCirrusParamRemoveNodeInterval:
      *((uint32_t*)data) = cirrusGwConfig.removeNodeInterval;
      return sizeof(cirrusGwConfig.removeNodeInterval);
    case eCirrusParamCollectDataInterval:
      *((uint32_t*)data) = cirrusGwConfig.collectDataInterval;
      return sizeof(cirrusGwConfig.collectDataInterval);
    case eCirrusParamPeriodicUpdateInterval:
      *((uint32_t*)data) = cirrusGwConfig.periodicUpdateInterval;
      return sizeof(cirrusGwConfig.periodicUpdateInterval);
    case eCirrusParamSshActive:
      *((bool*)data) = cirrusGwConfig.sshActive;
      return 1;
    case eCirrusParamDebugLog:
      *((bool*)data) = cirrusGwConfig.debugLog;
      return 1;
    case eCirrusParamDebugMpaLog:
      *((bool*)data) = cirrusGwConfig.debugMpaLog;
      return 1;
    case eCirrusParamEthernetAddress:
      strcpy((char*)data, (char*)cirrusGwConfig.ethernetAddress);
      return strlen((char*)cirrusGwConfig.ethernetAddress);
    case eCirrusParamEthernetStatic:
      *((bool*)data) = cirrusGwConfig.ethernetStatic;
      return 1;
    case eCirrusParamEthernetNetmask:
      strcpy((char*)data, (char*)cirrusGwConfig.ethernetNetmask);
      return strlen((char*)cirrusGwConfig.ethernetNetmask);
    case eCirrusParamEthernetGateway:
      strcpy((char*)data, (char*)cirrusGwConfig.ethernetGateway);
      return strlen((char*)cirrusGwConfig.ethernetGateway);
    case eCirrusParamWirelessAddress:
      strcpy((char*)data, (char*)cirrusGwConfig.wirelessAddress);
      return strlen((char*)cirrusGwConfig.wirelessAddress);
    case eCirrusParamWirelessNetmask:
      strcpy((char*)data, (char*)cirrusGwConfig.wirelessNetmask);
      return strlen((char*)cirrusGwConfig.wirelessNetmask);
    case eCirrusParamWirelessSsid:
      strcpy((char*)data, (char*)cirrusGwConfig.wirelessSsid);
      return strlen((char*)cirrusGwConfig.wirelessSsid);
    case eCirrusParamWirelessPassword:
      strcpy((char*)data, (char*)cirrusGwConfig.wirelessPassword);
      return strlen((char*)cirrusGwConfig.wirelessPassword);
    case eCirrusParamFreeStorage:
      *((uint64_t*)data) = cirrusGwStatus.freeStorage;
      return sizeof(cirrusGwStatus.freeStorage);
    case eCirrusParamDateTime:
      *((uint32_t*)data) = cirrusGwStatus.deviceTime;
      return sizeof(cirrusGwStatus.deviceTime);
    case eCirrusParamFoundNodes:
      *((uint32_t*)data) = cirrusGwStatus.foundNodes;
      return sizeof(cirrusGwStatus.foundNodes);
    case eCirrusParamWirelessSignalStrength:
      *((uint32_t*)data) = cirrusGwStatus.wirelessSignalStrength;
      return sizeof(cirrusGwStatus.wirelessSignalStrength);
    case eCirrusParamWirelessLinkQuality:
      *((uint32_t*)data) = cirrusGwStatus.wirelessLinkQuality;
      return sizeof(cirrusGwStatus.wirelessLinkQuality);
    case eCirrusParamSecurityCode:
      *((uint32_t*)data) = cirrusGwConfig.securityCode;
      return sizeof(cirrusGwConfig.securityCode);
    case eCirrusParamSerialNumber:
      *((uint64_t*)data) = cirrusGwConfig.serialNumber;
      return sizeof(cirrusGwConfig.serialNumber);
    case eCirrusParamFactoryDefault:
      *((bool*)data) = cirrusGwStatus.factoryDefault;
      return 1;
    case eCirrusParamReady:
      *((bool*)data) = cirrusGwStatus.ready;
      return 1;
    default:
      return 0;
  }
}

/**
 * Format paramter data for network
 * @param index The paramter index to format
 * @param data The buffer for the parameter data
 */
void CirrusGwNetworkFormatParam(const uint32_t index, const uint8_t* data) {
  switch (index) {
    case eCirrusParamSoftwareType:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamSoftwareVersion:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamFirmwareType:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamFirmwareVersion:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamMui:
      SwapBytes64((uint64_t*)data);
      break;
    case eCirrusParamBuildCommit:
      // Nothing to do
      break;
    case eCirrusParamQtVersion:
      // Nothing to do
      break;
    case eCirrusParamEthernetMac:
      // Nothing to do
      break;
    case eCirrusParamWirelessMac:
      // Nothing to do
      break;
    case eCirrusParamDeviceId:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamName:
      // Nothing to do
      break;
    case eCirrusParamCloudUrl:
      // Nothing to do
      break;
    case eCirrusParamNetworkRestriction:
      // Nothing to do
      break;
    case eCirrusParamNodeRestriction:
      // Nothing to do
      break;
    case eCirrusParamCloudUsername:
      // Nothing to do
      break;
    case eCirrusParamCloudPassword:
      // Nothing to do
      break;
    case eCirrusParamScanNetworkInterval:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamRemoveNodeInterval:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamCollectDataInterval:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamPeriodicUpdateInterval:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamSshActive:
      // Nothing to do
      break;
    case eCirrusParamDebugLog:
      // Nothing to do
      break;
    case eCirrusParamDebugMpaLog:
      // Nothing to do
      break;
    case eCirrusParamEthernetAddress:
      // Nothing to do
      break;
    case eCirrusParamEthernetStatic:
      // Nothing to do
      break;
    case eCirrusParamEthernetNetmask:
      // Nothing to do
      break;
    case eCirrusParamEthernetGateway:
      // Nothing to do
      break;
    case eCirrusParamWirelessAddress:
      // Nothing to do
      break;
    case eCirrusParamFoundNodes:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamWirelessSignalStrength:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamWirelessLinkQuality:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamWirelessNetmask:
      // Nothing to do
      break;
    case eCirrusParamWirelessSsid:
      // Nothing to do
      break;
    case eCirrusParamWirelessPassword:
      // Nothing to do
      break;
    case eCirrusParamFreeStorage:
      SwapBytes64((uint64_t*)data);
      break;
    case eCirrusParamDateTime:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamSecurityCode:
      SwapBytes32((uint32_t*)data);
      break;
    case eCirrusParamSerialNumber:
      SwapBytes64((uint64_t*)data);
      break;
    case eCirrusParamFactoryDefault:
      // Nothing to do
      break;
    case eCirrusParamReady:
      // Nothing to do
      break;
    default:
      // Nothing to do
      break;
  }
}
