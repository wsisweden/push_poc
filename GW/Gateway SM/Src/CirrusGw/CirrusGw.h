#pragma once

#include <stdbool.h>
#include <stdint.h>

// Public definitions

enum {
  /*  0 */ eCirrusParamSoftwareType,
  /*  1 */ eCirrusParamSoftwareVersion,
  /*  2 */ eCirrusParamMui,
  /*  3 */ eCirrusParamBuildCommit,
  /*  4 */ eCirrusParamQtVersion,
  /*  5 */ eCirrusParamEthernetMac,
  /*  6 */ eCirrusParamWirelessMac,
  /*  7 */ eCirrusParamDeviceId,
  /*  8 */ eCirrusParamName,
  /*  9 */ eCirrusParamCloudUrl,
  /* 10 */ eCirrusParamCloudUsername,
  /* 11 */ eCirrusParamCloudPassword,
  /* 12 */ eCirrusParamSecurityCode,
  /* 13 */ eCirrusParamScanNetworkInterval,
  /* 14 */ eCirrusParamCollectDataInterval,
  /* 15 */ eCirrusParamNotUsed,
  /* 16 */ eCirrusParamSshActive,
  /* 17 */ eCirrusParamPeriodicUpdateInterval,
  /* 18 */ eCirrusParamDebugLog,
  /* 19 */ eCirrusParamDebugMpaLog,
  /* 20 */ eCirrusParamEthernetAddress,
  /* 21 */ eCirrusParamEthernetStatic,
  /* 22 */ eCirrusParamEthernetNetmask,
  /* 23 */ eCirrusParamEthernetGateway,
  /* 24 */ eCirrusParamWirelessAddress,
  /* 25 */ eCirrusParamFoundNodes,
  /* 26 */ eCirrusParamWirelessNetmask,
  /* 27 */ eCirrusParamWirelessSsid,
  /* 28 */ eCirrusParamWirelessPassword,
  /* 29 */ eCirrusParamNotUsed1,
  /* 30 */ eCirrusParamWirelessSignalStrength,
  /* 31 */ eCirrusParamWirelessLinkQuality,
  /* 32 */ eCirrusParamFreeStorage,
  /* 33 */ eCirrusParamDateTime,
  /* 34 */ eCirrusParamSerialNumber,
  /* 35 */ eCirrusParamFactoryDefault,
  /* 36 */ eCirrusParamRemoveNodeInterval,
  /* 37 */ eCirrusParamNetworkRestriction,
  /* 38 */ eCirrusParamNodeRestriction,
  /* 39 */ eCirrusParamFirmwareType,
  /* 40 */ eCirrusParamFirmwareVersion,
  /* 41 */ eCirrusParamReady,
  /* 42 */ eCirrusParamLast
};

typedef struct {
  uint32_t softwareType;
  uint32_t softwareVersion;
  uint32_t firmwareType;
  uint32_t firmwareVersion;
  uint64_t mui;
  uint8_t buildCommit[82];
  uint8_t qtVersion[82];
  uint8_t wirelessMac[82];
  uint8_t ethernetMac[82];
} CirrusGwInfo_t;

typedef struct {
  uint32_t deviceId;
  uint8_t gatewayName[82];
  uint8_t cloudUrl[82];
  uint8_t cloudUserName[82];
  uint8_t cloudPassword[82];
  uint32_t scanNetworkInterval;
  uint32_t collectDataInterval;
  uint32_t periodicUpdateInterval;
  bool sshActive;
  bool debugLog;
  bool debugMpaLog;
  uint8_t ethernetAddress[82];
  bool ethernetStatic;
  uint8_t ethernetNetmask[82];
  uint8_t ethernetGateway[82];
  uint8_t wirelessAddress[82];
  uint8_t wirelessNetmask[82];
  uint8_t wirelessSsid[82];
  uint8_t wirelessPassword[82];
  uint32_t securityCode;
  uint64_t serialNumber;
  uint32_t removeNodeInterval;
  uint8_t networkRestriction[82];
  uint8_t nodeRestriction[82];
} CirrusGwConfig_t;

typedef struct {
  uint64_t freeStorage;
  uint32_t deviceTime;
  uint32_t foundNodes;
  int32_t wirelessSignalStrength;
  uint32_t wirelessLinkQuality;
  bool factoryDefault;
  bool ready;
} CirrusGwStatus_t;

// Public function definitions

void CirrusGwInit(void);
CirrusGwInfo_t* CirrusGwGetInfo(void);
CirrusGwStatus_t* CirrusGwGetStatus(void);
CirrusGwConfig_t* CirrusGwGetConfig(void);
void CirrusGwSetInfo(const CirrusGwInfo_t* const info);
void CirrusGwSetStatus(const CirrusGwStatus_t* const status);
void CirrusGwSetConfig(const CirrusGwConfig_t* const config);
bool CirrusGwSetParam(const uint32_t index, const uint8_t* const data,
                      const int32_t dataLength);
bool CirrusGwSetParamInternal(const uint32_t index, const uint8_t* const data,
                      const int32_t dataLength);
uint32_t CirrusGwGetParam(const uint32_t index, const uint8_t* data);
void CirrusGwNetworkFormatParam(const uint32_t index, const uint8_t* data);
