/***************************************************************************
  MpSerial.c
  
  Revision history:
  Rev   Date      Comment   Description
  this  120503    000       First revision 

***************************************************************************/
#if gUseMpSerial_d 

#include "PopCfg.h"
#include "PopBsp.h"
#include "PopNet.h"
#include "MpSerial.h"  
#include "MpCfg.h"

/***************************************************************************
  Local Types & Defines
***************************************************************************/

/***************************************************************************
  Prototypes
***************************************************************************/
void MpSerialTimerHandler(popTimerId_t  iTimerId);
void MpGpioUart1Init(void);
void MpGpioUart2Init(void);

extern void GpioUart1Init(void);
/***************************************************************************
  Globals and Externals
***************************************************************************/
/* globals for this module only */
uint8_t   giMpSerialActive;
uint8_t   gaMpSerialTxBuf[gMpSerialTxBufferSize_c]; /* ISR tx buf */
uint8_t  *gpMpSerialTxBufHead = gaMpSerialTxBuf;    /* buffer head */
uint8_t  *gpMpSerialTxBufTail = gaMpSerialTxBuf;    /* buffer tail */
uint8_t   gaMpSerialRxBuf[gMpSerialRxBufferSize_c]; /* ISR rx buf */
uint8_t  *gpMpSerialRxBufHead = gaMpSerialRxBuf;    /* buffer head */
uint8_t  *gpMpSerialRxBufTail = gaMpSerialRxBuf;    /* buffer tail */

volatile uint8_t gfMpSerialStatus;                      /* current status of xmit and recv */
uint8_t gfMpSerialActive = 0;                           /* Serial active flag */

/***************************************************************************
  Code
***************************************************************************/

/*
  MppSerialTaskInit

  Initialize the serial task
*/
void MpSerialTaskInit(void)
{
  // enables serial input
  gfMpSerialActive=1;
  
  gpMpSerialTxBufHead = 
  gpMpSerialTxBufTail = gaMpSerialTxBuf;
  gpMpSerialRxBufHead =
  gpMpSerialRxBufTail = gaMpSerialRxBuf;

  /* Initialize the Mp serial port*/
  (void)MpUart_Init(gaMpSerialRxBuf);
    
  // nothing received yet
  gfMpSerialStatus = 0;

}

/*
  MpSerialEventLoop
  This routine process serial events before sending them on to the 
  application. Takes care of things like keeping track of mingap.
*/

void MpSerialTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
  // not yet active, don't send messages
  if(!gfMpSerialActive)
    return;

  // handle serial task event
  switch(iEvtId)
  {
  case gMpSerByteReceivedEvt_c:
    (void)PopStartTimerEx(cMpSerialTaskId,gMpSerMinGapTimer_c,gMpSerialMinGapMs_c);
    break;

  // time to inform the application of serial input 
    case gPopEvtTimer_c:
      MpSerialTimerHandler(sEvtData.iTimerId);
    break;

    case gMpSerTxDone_c:
      //(void)PopSetAppEvent(gMpSerialTxDoneEvt_c, NULL);
      break;
  } // end of switch
}

/*
  Serial driver timer handler.
*/
void MpSerialTimerHandler(popTimerId_t  iTimerId)
{
  // the min gap has occurred, send the data to the application
  if (iTimerId == gMpSerMinGapTimer_c)
  {
    // only set 1 app event for serial data ready (not many)
    (void)PopSetUniqueEventEx(cAppTaskId, gMpSerialDataReadyEvt_c, NULL);
  }
}

/************************************************************************************  
* MpGpioUart1Init
*
* This function initializate the gpio�s for the Uart1 module
*************************************************************************************/
void MpGpioUart1Init(void)
{
  register uint32_t tmpReg;

#if gUart1_EnableHWFlowControl_d == TRUE
  GPIO.PuSelLo |= (GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit);  // Pull-up select: UP type
  GPIO.PuEnLo  |= (GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit);  // Pull-up enable
  GPIO.InputDataSelLo &= ~(GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit); // read from pads
  GPIO.DirResetLo = (GPIO_UART1_RTS_bit | GPIO_UART1_RX_bit); // inputs
  GPIO.DirSetLo = (GPIO_UART1_CTS_bit | GPIO_UART1_TX_bit);  // outputs
        
  tmpReg = GPIO.FuncSel0 & ~((FN_MASK << GPIO_UART1_RX_fnpos) | (FN_MASK << GPIO_UART1_TX_fnpos));
  GPIO.FuncSel0 = tmpReg | ((FN_ALT << GPIO_UART1_RX_fnpos) | (FN_ALT << GPIO_UART1_TX_fnpos));
  tmpReg = GPIO.FuncSel1 & ~((FN_MASK << GPIO_UART1_CTS_fnpos) | (FN_MASK << GPIO_UART1_RTS_fnpos));
  GPIO.FuncSel1 = tmpReg | ((FN_ALT << GPIO_UART1_CTS_fnpos) | (FN_ALT << GPIO_UART1_RTS_fnpos));
#else
  GPIO.PuSelLo |= GPIO_UART1_RX_bit;  // Pull-up select: UP type
  GPIO.PuEnLo  |= GPIO_UART1_RX_bit;  // Pull-up enable
  GPIO.InputDataSelLo &= ~GPIO_UART1_RX_bit; // read from pads
  GPIO.DirResetLo = GPIO_UART1_RX_bit; // inputs
  GPIO.DirSetLo = GPIO_UART1_TX_bit;  // outputs
        
  tmpReg = GPIO.FuncSel0 & ~((FN_MASK << GPIO_UART1_RX_fnpos) | (FN_MASK << GPIO_UART1_TX_fnpos));
  GPIO.FuncSel0 = tmpReg | ((FN_ALT << GPIO_UART1_RX_fnpos) | (FN_ALT << GPIO_UART1_TX_fnpos)); 
  
#endif
}

/************************************************************************************  
* MpGpioUart2Init
*
* This function initializate the gpio�s for the Uart2 module
*************************************************************************************/
void MpGpioUart2Init(void)
{
  register uint32_t tmpReg;
  
#if gUart2_EnableHWFlowControl_d == TRUE 
  GPIO.PuSelLo |= (GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit);  // Pull-up select: UP type
  GPIO.PuEnLo  |= (GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit);  // Pull-up enable
  GPIO.InputDataSelLo &= ~(GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit); // read from pads
  GPIO.DirResetLo = (GPIO_UART2_RTS_bit | GPIO_UART2_RX_bit); // inputs
  GPIO.DirSetLo = (GPIO_UART2_CTS_bit | GPIO_UART2_TX_bit);  // outputs

  tmpReg = GPIO.FuncSel1 & ~((FN_MASK << GPIO_UART2_CTS_fnpos) | (FN_MASK << GPIO_UART2_RTS_fnpos)\
  | (FN_MASK << GPIO_UART2_RX_fnpos) | (FN_MASK << GPIO_UART2_TX_fnpos));
  GPIO.FuncSel1 = tmpReg | ((FN_ALT << GPIO_UART2_CTS_fnpos) | (FN_ALT << GPIO_UART2_RTS_fnpos)\
  | (FN_ALT << GPIO_UART2_RX_fnpos) | (FN_ALT << GPIO_UART2_TX_fnpos));
#else 
  // Don't care about RTS and CTS pins
  GPIO.PuSelLo |= GPIO_UART2_RX_bit;  // Pull-up select: UP type
  GPIO.PuEnLo  |= GPIO_UART2_RX_bit;  // Pull-up enable
  GPIO.InputDataSelLo &= ~GPIO_UART2_RX_bit; // read from pads
  GPIO.DirResetLo = GPIO_UART2_RX_bit; // inputs
  GPIO.DirSetLo = GPIO_UART2_TX_bit;  // outputs
  
  tmpReg = GPIO.FuncSel1 & ~((FN_MASK << GPIO_UART2_RX_fnpos) | (FN_MASK << GPIO_UART2_TX_fnpos));
  GPIO.FuncSel1 = tmpReg | ((FN_ALT << GPIO_UART2_RX_fnpos) | (FN_ALT << GPIO_UART2_TX_fnpos)); 
  
#endif
}

/************************************************************************************  
* MpUart_Init
*
* Initializate the Uart module.
*************************************************************************************/
UartErr_t result;
void MpUart_Init(uint8_t *mUARTRxBuffer) 
{
  UartConfig_t pConfig;
  UartCallbackFunctions_t pCallback;
  
  
#if gMpSerialPort_c == UART_1    
    result = UartGetStatus(UART_1);
    //Consult the current status of this serial port
    if( result != gUartErrUartNotOpen_c )
    {
      /* if the uart is already open then do nothing */
      return;
    }
  
    //initialize GPIOs for UART1   
    MpGpioUart1Init();
  
    //configure the uart parameters 
    pConfig.UartParity = gUartParityNone_c; // 0
    pConfig.UartStopBits = gUartStopBits1_c;  // 0
    pConfig.UartBaudrate = gMpSerialBaudrate_c;  // 115200
    pConfig.UartFlowControlEnabled = gMpUart_EnableHWFlowControl_d;  // FALSE
    pConfig.UartRTSActiveHigh = FALSE;
  
    
    //mount the interrupts corresponding to UART driver
    IntAssignHandler(gUart1Int_c, (IntHandlerFunc_t)UartIsr1);
    ITC_SetPriority(gUart1Int_c, gItcNormalPriority_c);
    //enable the interrupts corresponding to UART driver
    ITC_EnableInterrupt(gUart1Int_c);
  
    //initialize the uart
    result = UartOpen(UART_1,gPlatformClock_c);  
  
    if( result != gUartErrNoError_c )
      return;
  
    result = UartSetConfig(UART_1, &pConfig);  
  
  #if gMpUart_EnableHWFlowControl_d == TRUE
    UartSetCTSThreshold(UART_1, gUart_RxFlowControlSkew_d);
  #endif
  
    //configure the Uart Rx and Tx Threshold
    result = UartSetTransmitterThreshold(UART_1, 1);
    result = UartSetReceiverThreshold(UART_1, 2);
  
    //set pCallback functions
    pCallback.pfUartWriteCallback = MpSerialTxIsr;
    pCallback.pfUartReadCallback = MpSerialRxIsr;
    result = UartSetCallbackFunctions(UART_1, &pCallback);
#endif
    
#if gMpSerialPort_c == UART_2
    result = UartGetStatus(UART_2);
    //Consult the current status of this serial port
    if( result != gUartErrUartNotOpen_c )
    {
      /* if the uart is already open then do nothing */
      return;
    }
      
    //initialize GPIOs for UART2
    MpGpioUart2Init(); 
  
    //configure the uart parameters
    pConfig.UartParity = gUartParityNone_c;
    pConfig.UartStopBits = gUartStopBits1_c;
    pConfig.UartBaudrate = gMpSerialBaudrate_c;
    pConfig.UartFlowControlEnabled = gMpUart_EnableHWFlowControl_d;
    pConfig.UartRTSActiveHigh = FALSE;
  
    //mount the interrupts corresponding to UART driver
    IntAssignHandler(gUart2Int_c, (IntHandlerFunc_t)UartIsr2);
    ITC_SetPriority(gUart2Int_c, gItcNormalPriority_c);
    //enable the interrupts corresponding to UART driver
    ITC_EnableInterrupt(gUart2Int_c);
    
    //initialize the uart
    result = UartOpen(UART_2,gPlatformClock_c);
  
    if( result != gUartErrNoError_c )
      return;
    
    result = UartSetConfig(UART_2,&pConfig);
  
  #if gMpUart_EnableHWFlowControl_d == TRUE
    UartSetCTSThreshold(UART_2, gUart_RxFlowControlSkew_d);
  #endif
  
    //configure the Uart Rx and Tx Threshold
    result = UartSetTransmitterThreshold(UART_2,1);
    result = UartSetReceiverThreshold(UART_2,2);
  
    //set pCallback functions
    pCallback.pfUartWriteCallback = MpSerialTxIsr;
    pCallback.pfUartReadCallback = MpSerialRxIsr;
    result = UartSetCallbackFunctions(UART_2,&pCallback);
  }
#endif

  // global enable interrupts in AITC driver
  IntEnableIRQ();
  
  result = UartReadData(gMpSerialPort_c,mUARTRxBuffer,sizeof(mUARTRxBuffer),TRUE);
} 

/*
  MpSerialTxIsr1

  UART Transmit ISR. Called on Tx interrupt - data register empty. Data added 
  to head, removed from tail.
*/
void MpSerialTxIsr(UartWriteCallbackArgs_t* args) 
{
  // send next byte to UART
  if(args->UartStatus == gUartWriteStatusComplete_c )
  { 
    gu16SCINumOfBytes = args->UartNumberBytesSent;

    gfMpSerialStatus |= gMpSerTxDone_c;

    /* Tell the Serial Task that the transmission has been completed.
       This is optional */
   (void)PopSetEventEx(cMpSerialTaskId, gMpSerTxDone_c, NULL);
  }

}

/*
  MpSerialRxIsr

  UART Receive ISR. Called on Rx interrupt. Data added to head, removed from 
  tail.
*/
void MpSerialRxIsr(UartReadCallbackArgs_t* args)
{
  uint8_t TestBuff;
 
  gu8SCIDataFlag = TRUE;

  gu8SCIStatus = args->UartStatus;

  if( gu8SCIStatus == gUartReadStatusComplete_c )
  {
    gu16SCINumOfBytes = args->UartNumberBytesReceived;

    UartGetByteFromRxBuffer(gMpSerialPort_c,&TestBuff );

    *gpMpSerialRxBufHead++ = TestBuff;
    
    // head has wrapped in ring buffer
    if ( gpMpSerialRxBufHead >= (gaMpSerialRxBuf + gMpSerialRxBufferSize_c) )
      gpMpSerialRxBufHead = gaMpSerialRxBuf;

    // if overrun, set (sticky) flag to indicate such
    if ( gpMpSerialRxBufHead == gpMpSerialRxBufTail )
      gfMpSerialStatus |= gMpSerRxOverflow_c;

    // indicate byte received to appropriate task
    gfMpSerialStatus |= gMpSerRxData_c;
    
  }

  // reenable interupts. We had an error
  else if( gu8SCIStatus == gUartReadStatusError_c)
  {
    /*
      Disabling Rx interrupt corresponding to UART driver. If we disable global interruptions then we
      will be affecting the other interrupts.
    */  
    UartCloseReceiver(gMpSerialPort_c); 

    // Clear uart status error 
    UartClearErrors(gMpSerialPort_c);

    // Read the data stored in Rx buffer at the moment of the error. 
    while( UartGetByteFromRxBuffer(gMpSerialPort_c,&TestBuff ) ); 

    /*
      Enabling Rx interrupt corresponding to UART driver. If we disable global interruptions then we
      will be affecting the other interrupts.
    */  
    UartOpenReceiver(gMpSerialPort_c);

    // Clear the args status.
    args->UartReadError.UartReadOverrunError = 0;
    args->UartReadError.UartParityError = 0;
    args->UartReadError.UartFrameError = 0;
    args->UartReadError.UartStartBitError = 0;
    args->UartStatus = gUartReadStatusComplete_c;
    args->UartNumberBytesReceived = 0;

  }

}


popErr_t MpSerialSend( uint8_t iLen, void *pBuffer)
{
  // no data, success
  if( !iLen )
    return gPopErrNone_c;

  // no buffer, invalid argument
  if ( !pBuffer )
    return gMpSerInvalidArg_c;

  // data too large for transmit buffer, give up
  if ( iLen > gMpSerialTxBufferSize_c )
    return gMpSerTooBig_c;

  /* If there is a write ongoing return busy */      
  if (UartGetStatus(gMpSerialPort_c)== gUartErrWriteOngoing_c)
    return gMpSerBusy_c;

  UartWriteData(gMpSerialPort_c,pBuffer,iLen);

  return gPopErrNone_c;
}

/*
  MpSerialReceive

  

  paraeters:
    piLen:   [in/out]   in: length of app's input buffer. 
                        out: n. bytes received to buffer, or on error n. bytes required.
    pBuffer: [in]       pointer to application receive buffer.
    
    return values:
    gPopErrNone_c     successful receive to application buffer
    gPopErrBadParm_c  invalid parameter

    side effects:
    on successful receive to app buffer, clears the gMpSerRxData_c status flag.
*/
popErr_t MpSerialReceive(byte_t *piLen, void *pBuffer)
{
  uint8_t * pRxHead;
  uint8_t * pRxTail;
  uint8_t  iChunkData1;
  uint8_t  iChunkData2;

  // critical section, disable ints
  PopSerialSaveAndDisableTxRxInts();

  // no longer have data (must disable both tx and rx which can affect this flag
  gfMpSerialStatus &= ~gMpSerRxData_c;

  // grab head and tail for comparison (must disable ints because rx int will affect tail
  pRxHead = gpMpSerialRxBufHead;
  pRxTail = gpMpSerialRxBufTail;

  PopSerialEnableRxInts();

  // check input parameters (but only if not trying to keep size small
  if ( !piLen || !*piLen || !pBuffer )
    return gMpSerInvalidArg_c;

  // no data. this case should not actually happen...
  if ( pRxTail == pRxHead )
  {
    *piLen = 0;
    return gPopErrNone_c;
  }

  // data is in one chunk...
  iChunkData1 = (uint8_t)(pRxHead - pRxTail);
  iChunkData2 = 0;
  if(pRxTail >= pRxHead)  // or two
  {
      iChunkData1 = (uint8_t)(gaMpSerialRxBuf + gMpSerialRxBufferSize_c - pRxTail);
      iChunkData2 = (uint8_t)(pRxHead - gaMpSerialRxBuf);
  }

  // application may not want all of the data (if small buffer size)
  if ( *piLen < iChunkData1 )
  {
    iChunkData1 = *piLen;
    iChunkData2 = 0;
  }
  else if(*piLen < iChunkData1 + iChunkData2)
    iChunkData2 = *piLen - iChunkData1;
 
  // copy data to app buffer in one chunk...
  if ( iChunkData1 )
    PopMemCpy( pBuffer, pRxTail, iChunkData1 );
  if ( iChunkData2 )  // or two
    PopMemCpy( (byte_t *)pBuffer + iChunkData1, gaMpSerialRxBuf, iChunkData2 );

  // tell application how many bytes received
  *piLen = iChunkData1 + iChunkData2;

  // disable rx ints during tail update...
  PopSerialDisableRxInts();

  // data added to tail, update tail
  if ( !iChunkData2 )
    gpMpSerialRxBufTail += iChunkData1;
  else
    gpMpSerialRxBufTail = gaMpSerialRxBuf + iChunkData2;

  // tail wrapped
  if ( gpMpSerialRxBufTail >= gaMpSerialRxBuf + gMpSerialRxBufferSize_c )
    gpMpSerialRxBufTail = gaMpSerialRxBuf;

  // enable interrupts for serial receive
  PopSerialEnableRxInts();

  // let caller know it was successful
  return gPopErrNone_c;
}


void MpSerialHandler(void)
{
  uint8_t aSerialRxData[gMpSerialRxBufferSize_c];
  uint8_t aSerialTxData[gMpSerialTxBufferSize_c];
  uint8_t iLen;
  
  uint8_t cmd;
  uint8_t aMACAddr[8];
  uint8_t iCoarseTune;
  uint8_t iFineTune;
  
  iLen = sizeof(aSerialRxData);  
  MpSerialReceive(&iLen,aSerialRxData);
  cmd = aSerialRxData[0];
  
  switch(cmd)
  {
     case 0x0A:
      /* Set crystal coarse tune */
      iCoarseTune = aSerialRxData[1];    
      set_xtal_coarse_tune(iCoarseTune);
      PopNvStore(gPopAppNvCoarseTune_c, 1, &iCoarseTune); 
      
      /* Send response */
      aSerialTxData[0] = 0x0A;
      aSerialTxData[1] = iCoarseTune;
      MpSerialSend(2, aSerialTxData);
      break;
    
    case 0x0B:
      /* Set crystal fine tune */
      iFineTune = aSerialRxData[1]; 
      set_xtal_fine_tune(iFineTune);   
      PopNvStore(gPopAppNvFineTune_c, 1, &iFineTune);  
      
      /* Send response */
      aSerialTxData[0] = 0x0B;
      aSerialTxData[1] = iFineTune;
      MpSerialSend(2, aSerialTxData);
      break;
      
    case 0x0D:
      /* Set MAC address */
      PopMemCpy(&aMACAddr[0], &aSerialRxData[1], 8);
      PopNwkSetMacAddr(aMACAddr);
      (void)PopNvStoreNwkData(); 
      
      /* Send response */
      PopNwkGetMacAddr(&aMACAddr[0]);
      aSerialTxData[0] = 0x0D;
      PopMemCpy(&aSerialTxData[1], &aMACAddr[0], 8);
      MpSerialSend(9, aSerialTxData);     
      break;
      
  }
}
#endif //gUseMpSerial_d 