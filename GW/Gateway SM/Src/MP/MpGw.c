/***************************************************************************
  MpGw.c

  This file includes functions for handling  resending and rerouting.
  of application messages

  Revision history:
  Rev   Date      Comment   Description
  this  100707              first revision
***************************************************************************/
#include "MpGw.h"
#include "MpNwk.h"
#include "../Tui/Tui.h"
#include "../Ota/OtaTask.h"
#include "../CirrusGw/CirrusGw.h"

#include <Hw/TIDriverLib/source/systick.h>
#include <Hw/Segger/RTT/SEGGER_RTT.h>
#include <string.h>

// Local timers
#define gMpGwNodePlacementTimerId_c                gPopFirstTimerId_c
#define gMpGwBeaconIndicationTimerId_c            (gPopFirstTimerId_c + 1)
#define gMpGwResetCPUTimerId_c                    (gPopFirstTimerId_c + 2)
#define gMpGwTimerIdNodePlaceBeaconReq_c          (gPopFirstTimerId_c + 3)
#define gMpGwScanNetworkTimerId_c                 (gPopFirstTimerId_c + 4)
#define gMpGwScanForNodesTimerId_c                (gPopFirstTimerId_c + 5)
#define gMpGwWatchdogTimerId_c                    (gPopFirstTimerId_c + 6)

// Local definitions
#define gMpGwNodePlacementTime_c  1500
#define gMpGwBeaconIndicationTime_c  1000
#define gMpGwScanNetworkTime_c  5000
#define gMpGwScanForNodesTime_c  10000
#define gMpGwWatchdogTime_c  60000

/*Function prototypes */
static void MpGwHandleTimer(popTimerId_t iTimerId);
static bool MpGwValidNwkData(popChannel_t iChannel, popPanId_t iPanId, popNwkAddr_t iNwkAddr);
// node placement
static void MpGwStartNodePlacement(popChannel_t iChannel);
static void MpGwEndNodePlacement(void);
static void MpGwSendPacket(uint8_t *pData);
static void MpGwSmDataReq(sPopEvtData_t *pData);

/*Extern variables*/
extern const popTaskId_t cGwTaskId;

/*Global variables */
bool gfMpGwIsProcessingBeacon_c = false;
bool gfHeartbeat = false; // Set heartbeat flag to zero as init value
// ScanForNetworks
sMpScanNetworkRsp_t gsMpScanNetworkRspTable[MAXNWK];
uint8_t giExclusiveNetworks;
// ScanForNodes
sMpScanForNodesReq_t gsMpScanForNodesReq;
sMpNodeList_t gsMpNodeList[NODELIST_SIZE];
uint8_t giNodesInList = 0; // number of nodes in NodeList

// Nodeplacement stuff
const popLqi_t gaMpNodePlacementStrongSignal[] = {0x40,0x80};
uint8_t giMpNodePlaceBars;
sPopNodePlacement_t gsMpNodePlacementResult;
popChannel_t giMpNodePlaceChannel;
bool gfMpPlacementStart = false;
popTimeOut_t giMpNodePlacementRate_c = 1000;

uint8_t *pgPayload; // debug
sMpJoinEnableReq_t sMpJoinEnableReq;  // debug


/*
  MpGwTaskInit
*/
void MpGwTaskInit(void) {
  SEGGER_RTT_Init();
}

/*
  MpGwTaskEventLoop

  All events from PopNet to the application occur here. The iEvtId is a number
  from 0x00-0xff (not a bit mask). The contents of sEvtData depends on the iEvtId.
  See type sPopEvtData_t in PopNet.h.
*/
void MpGwTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData)
{
  switch(iEvtId)
  {
    case gPopEvtTimer_c:
      MpGwHandleTimer(sEvtData.iTimerId);
      break;
    default:
      // Nothing to do
      break;
  }
}

static void MpGwHandleTimer(popTimerId_t iTimerId)
{
  switch(iTimerId)
  {
    case gMpGwNodePlacementTimerId_c:
      MpGwEndNodePlacement();
      break;

    case gMpGwBeaconIndicationTimerId_c:
      PopStopTimerEx(cGwTaskId, gMpGwScanNetworkTimerId_c);
      MpGwScanNetworkRsp();
      break;

    case gMpGwScanForNodesTimerId_c:
      MpGwScanForNodesRsp(true);
      break;

    case gMpGwResetCPUTimerId_c:
      // reset CPU
      PopReset();
      break;

    case gMpGwTimerIdNodePlaceBeaconReq_c:
      {
        // Node placement timer has gone off
        if(gfMpPlacementStart){
          // Send response to gateway
          MpGwNodePlacementRsp(&gsMpNodePlacementResult);
          // start the timer again
          (void)PopStartTimerEx(cGwTaskId,gMpGwTimerIdNodePlaceBeaconReq_c,giMpNodePlacementRate_c);
        }

        // Clean Variables before next beacon request
        gsMpNodePlacementResult.iNodeCount=0;
        gsMpNodePlacementResult.iLqi=0;
        gsMpNodePlacementResult.iBars=0;

        // send out a beacon request on this particular channel
        PopNwkBeaconRequest(giMpNodePlaceChannel);
      }
      break;

    case gMpGwScanNetworkTimerId_c:
        PopStopTimerEx(cGwTaskId, gMpGwBeaconIndicationTimerId_c);
        MpGwScanNetworkRsp();
      break;

    case gMpGwWatchdogTimerId_c:
    {
      static uint8_t iMinuteCntr = 0;
      extern uint8_t ispCommanderModeFlag;
      extern uint8_t otaFwdlModeFlag;

      if((gfHeartbeat == true) || (ispCommanderModeFlag == true) || (otaFwdlModeFlag == true))
      {
        // reset minute counter
        iMinuteCntr = 0;
        // reset heartbeat flag
        gfHeartbeat = false;
      }
      else
      {
        iMinuteCntr ++;
      }

      if(iMinuteCntr == 7)
      {
        // reset CPU
        PopAssert(gPopAssertGwWatchdog_c);
      }
      else
      {
        // feed watchdog timer
        if(PopStartTimerEx(cGwTaskId,gMpGwWatchdogTimerId_c,gMpGwWatchdogTime_c) != gPopErrNone_c){
          PopAssert(gPopAssertGwWatchdog_c);
        }
      }
      break;
    }
    default:
      // Nothing to do
      break;
  }
}

/*******************************************************************************
---------------- MpGwValidNwkData ----------------------------------------------
 Checks if Network data is within limits and returns true if so.
--------------------------------------------------------------------------------
*******************************************************************************/
static bool MpGwValidNwkData(popChannel_t iChannel, popPanId_t iPanId , popNwkAddr_t iNwkAddr)
{

  // compare input data with valid values
#if gNodeType_c == CHARGER_MODULE
  if((gPopNwkFirstChannel_c <= iChannel && iChannel <= (gPopNwkFirstChannel_c + gPopNwkNumOfChannels_c)) &&
     (iPanId < gPopNwkReservedPan_c) &&
     (iNwkAddr < gMpSmNwkReservedAddr_c)){
    return true;
  }
#elif gNodeType_c == STATISTICS_MODULE
  if((gPopNwkFirstChannel_c <= iChannel && iChannel <= (gPopNwkFirstChannel_c + gPopNwkNumOfChannels_c)) &&
     (iPanId < gPopNwkReservedPan_c) &&
     (gMpSmNwkReservedAddr_c <= iNwkAddr && iNwkAddr < gPopNwkReservedAddr_c)){
    return true;
  }
#endif
  return false;
}

/*******************************************************************************
---------------- MpGwStartNodePlacement ---------------------------------------
  Start node placement on the given channel. Note: this function works whether on a network on not.
  PanId() should be set either to the desired pan, or to 0xffff. See PopNwkSetPanId().
--------------------------------------------------------------------------------
*******************************************************************************/
static void MpGwStartNodePlacement(popChannel_t iChannel)
{
  //Start Node Placement
  gfMpPlacementStart = true;
//  gfPopPlacementStart = true;

  //if node placement on then permit joining should be off;
  PopNwkJoinEnable(!gfMpPlacementStart, false);

  // use the selected channel
  giMpNodePlaceChannel = iChannel;
//  giPopNodePlaceChannel= iChannel;

  // now we're paying attention to beacons we hear
  gfPopNwkIsProcessingBeaconEnabled = true;
//  gfPopNwkIsProcessingBeaconEnabled = true;

  // send out a beacon request on this particular channel
  PopNwkBeaconRequest(giMpNodePlaceChannel);
//  PopNwkBeaconRequest(giPopNodePlaceChannel);

  // start timer for next beacon
  (void)PopStartTimerEx(cGwTaskId,gMpGwTimerIdNodePlaceBeaconReq_c,giMpNodePlacementRate_c);
//  (void)PopStartTimer(gMpGwTimerIdNodePlaceBeaconReq_c, giMpNodePlacementRate_c);
//  (void)PopStartTimer(gPopTimerIdNodePlaceBeaconReq_c, giPopNodePlacementRate_c);
}

/*******************************************************************************
---------------- MpGwEndNodePlacement ------------------------------------------
End node placement (restores LEDs to former state)
--------------------------------------------------------------------------------
*******************************************************************************/
static void MpGwEndNodePlacement(void)
{
  //STOP Node Placement
  gfMpPlacementStart = false;

  // Stop paying attention to beacons
  gfPopNwkIsProcessingBeaconEnabled = false;

  // Clean Variables
  gsMpNodePlacementResult.iNodeCount=0;
//  gsPopNodePlacementResult.iNodeCount=0;
  gsMpNodePlacementResult.iLqi=0;
//  gsPopNodePlacementResult.iLqi=0;
  gsMpNodePlacementResult.iBars=0;
//  gsPopNodePlacementResult.iBars=0;
}

/*******************************************************************************
---------------- MpGwSendPacket ------------------------------------------------
  Gateway send packet event handling
--------------------------------------------------------------------------------
*******************************************************************************/
static void MpGwSendPacket(uint8_t *pData)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  uint8_t *pPayload;

  iEvtId = (popEvtId_t)(pData[GW_PACKET_EVTID]);
  iLen = (uint8_t)(pData[GW_PACKET_LEN]);
  pPayload = &(pData[GW_PACKET_PAYLOAD]);

  PopGatewaySendEventToPC(iEvtId, iLen, pPayload);
}

/**
 * Handle data request for SM
 * @param pData The data request
 */
static void MpGwSmDataReq(sPopEvtData_t *pData) {
  uint8_t *pPayload = (uint8_t *)pData;
  uint8_t length = pPayload[DATA_REQ_PAYLOADLEN];
  uint8_t command = pPayload[MP_ACCESS_COMMAND + DATA_REQ_PAYLOAD];
  uint8_t respPayload[100];
  sPopNwkDataIndication_t* indication = (sPopNwkDataIndication_t*)respPayload;
  popErr_t status = gPopErrNone_c;
  pPayload = &pPayload[MP_ACCESS_DATA + DATA_REQ_PAYLOAD];

  switch (command) {
    case gMpOtaAbort_c: // OTA abort
    {
      SEGGER_RTT_printf(0, "%d, OTA abort\r\n", PopGetSystick());
      OtaTaskAbort();

      indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
      indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gMpOtaAbortResp_c;
      indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
      indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
      indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
          sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + MP_OTA_ABORT_RESP_SIZE;
      MpGwDataIndication(indication);
      break;
    }
    case gMpOtaStart_c: // OTA start
    {
      uint8_t platform = pPayload[MP_OTA_START_PLATFORM];
      uint32_t imageSize =
          (uint32_t)(pPayload[MP_OTA_START_IMAGE_SIZE_H] << 24);
      imageSize |= (uint32_t)(pPayload[MP_OTA_START_IMAGE_SIZE_HM] << 16);
      imageSize |= (uint32_t)(pPayload[MP_OTA_START_IMAGE_SIZE_LM] << 8);
      imageSize |= (uint32_t)pPayload[MP_OTA_START_IMAGE_SIZE_L];
      uint16_t applicationId =
          (uint16_t)(pPayload[MP_OTA_START_APPLICATION_ID_H] << 8);
      applicationId |= (uint16_t)pPayload[MP_OTA_START_APPLICATION_ID_L];
      uint8_t hwId = pPayload[MP_OTA_START_HW_ID];
      bool preserveNvm = pPayload[MP_OTA_START_PERSERVE_NVM];
      bool localGateway = pPayload[MP_OTA_START_LOCAL_GATEWAY];
      uint32_t firmwarePart =
          (uint32_t)(pPayload[MP_OTA_START_FIRMWARE_PART_H] << 24);
      firmwarePart |= (uint32_t)(pPayload[MP_OTA_START_FIRMWARE_PART_HM] << 16);
      firmwarePart |= (uint32_t)(pPayload[MP_OTA_START_FIRMWARE_PART_LM] << 8);
      firmwarePart |= (uint32_t)pPayload[MP_OTA_START_FIRMWARE_PART_L];
      uint32_t firmwareRevision =
          (uint32_t)(pPayload[MP_OTA_START_FIRMWARE_REVISION_H] << 24);
      firmwareRevision |= (uint32_t)(pPayload[MP_OTA_START_FIRMWARE_REVISION_HM] << 16);
      firmwareRevision |= (uint32_t)(pPayload[MP_OTA_START_FIRMWARE_REVISION_LM] << 8);
      firmwareRevision |= (uint32_t)pPayload[MP_OTA_START_FIRMWARE_REVISION_L];
      sPopOtauGwFirstBlockPassThruSaveInternalReq_t temp;

      OtaData_t otaData;
      PopStorageReadBytes(&otaData, 0x80000, sizeof(OtaData_t));

      if ((otaData.applicationId == applicationId) &&
          (otaData.hwId == hwId) && (otaData.imageSize == imageSize) &&
          (otaData.platform == platform) &&
          (otaData.preserveNvm == preserveNvm) &&
          (otaData.localGateway == localGateway) &&
          (otaData.firmwarePart == firmwarePart) &&
          (otaData.firmwareRevision == firmwareRevision) &&
          (otaData.complete == true)) {
        status = gPopErrAlreadyThere_c;
      } else {
        temp.platform = platform;
        temp.imageSize = imageSize;
        temp.appId = applicationId;
        temp.hwId = hwId;
        temp.preserveNvm = preserveNvm;
        temp.blockNumber = 0;
        temp.blockSize = gPopSerialRxBufferSize_c - 128;
        temp.checksum = 0;
        temp.handler = 1;
        temp.nvPages = 2;

        SEGGER_RTT_printf(0, "%d, OTA start, length: %d, platform: %d, image size: %d\r\napplication ID: %d, HW ID: %d, preserve NVM: %d\r\n",
            PopGetSystick(), length, platform, imageSize, applicationId, hwId, preserveNvm);
        status =
            PopOtaUpgradeStorageModuleInternal(&temp, gPopOtaUpgradeReqSrcOTS_c);

        if (status == gPopErrNone_c) {
          OtaTaskStartMessage(temp.appId, temp.handler, temp.hwId,
              temp.imageSize, temp.nvPages, temp.platform, temp.preserveNvm,
              localGateway, firmwarePart, firmwareRevision);
        }
      }

      indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
      indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gMpOtaStartResp_c;
      indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
      indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
      indication->sFrame.aPayload[MP_ACCESS_DATA +
                                  MP_OTA_START_RESP_BLOCK_SIZE_H] =
                                      (temp.blockSize >> 8) & 0xff;
      indication->sFrame.aPayload[MP_ACCESS_DATA +
                                  MP_OTA_START_RESP_BLOCK_SIZE_L] =
                                      temp.blockSize & 0xff;
      indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_START_RESP_ERROR] =
          status;
      indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
          sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + MP_OTA_START_RESP_SIZE;
      MpGwDataIndication(indication);
      break;
    }
    case gMpOtaBlock_c: // OTA block
    {
      uint16_t number = (uint16_t)(pPayload[MP_OTA_BLOCK_NUMBER_H] << 8);
      number |= (uint16_t)pPayload[MP_OTA_BLOCK_NUMBER_L];
      uint8_t checksum = pPayload[MP_OTA_BLOCK_CHECKSUM];
      uint8_t* data = &pPayload[MP_OTA_BLOCK_DATA];
      uint8_t buffer[gPopSerialRxBufferSize_c];
      sPopOtauGwSubseqBlockSaveReq_t* temp =
          (sPopOtauGwSubseqBlockSaveReq_t*)buffer;

      temp->blockNumber = number + 1;
      temp->checksum = checksum;
      memcpy(temp->imgBlock, data, gPopSerialRxBufferSize_c - 128);
      temp->handler = 1;

      SEGGER_RTT_printf(0, "%d, OTA block, length: %d, block number: %d, checksum: %d\r\n",
          PopGetSystick(), length, number, checksum);
      status = PopOtaUpgradeStorageModule(temp, gPopOtaUpgradeReqSrcOTS_c);

      OtaTaskCheckLastMessage(temp->blockNumber,
          gPopSerialRxBufferSize_c - 128);

      indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
      indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gMpOtaBlockResp_c;
      indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
      indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
      indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_RESP_NUMBER_H] =
          (number >> 8) & 0xff;
      indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_RESP_NUMBER_L] =
          number & 0xff;
      indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_RESP_ERROR] =
          status;
      indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
          sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + MP_OTA_BLOCK_RESP_SIZE;
      MpGwDataIndication(indication);
      break;
    }
    case gMpOtaSwitch_c: // OTA switch
    {
      uint16_t targetNodeId =
          (uint16_t)(pPayload[MP_OTA_SWITCH_TARGET_NODE_ID_H] << 8);
      targetNodeId |= (uint16_t)pPayload[MP_OTA_SWITCH_TARGET_NODE_ID_L];

      SEGGER_RTT_printf(0, "%d, OTA switch, length: %d, target node ID: %d\r\n",
          PopGetSystick(), length, targetNodeId);
      status = OtaTaskSwitchTarget(targetNodeId);

      indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
      indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gMpOtaSwitchResp_c;
      indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
      indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
      indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_SWITCH_RESP_ERROR] =
          status;
      indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
          sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + MP_OTA_SWITCH_RESP_SIZE;
      MpGwDataIndication(indication);
      break;
    }
    case gSetCirrusParamReq_c: {
      sMpMsgSetCirrusParamReq_t* setCirrusParamReq =
        (sMpMsgSetCirrusParamReq_t*)pPayload;
      sMpMsgAcknowledge_t sMpMsgAcknowledge;
      bool status = false;
      int32_t dataLength = 0;

      if (length > (3 + sizeof(setCirrusParamReq->index))) {
        dataLength = length - 3 - sizeof(setCirrusParamReq->index);

        SwapBytes32(&setCirrusParamReq->index);
        status = CirrusGwSetParamInternal(setCirrusParamReq->index,
                                  setCirrusParamReq->data,
                                  dataLength);

        SEGGER_RTT_printf(0, "%d, Set Cirrus Param, length: %d, index: %d\r\n",
            PopGetSystick(), dataLength, setCirrusParamReq->index);
      }

      sMpMsgAcknowledge.iReqCmd = gSetCirrusParamReq_c;
      sMpMsgAcknowledge.iStatus = status ? 0 : 0xff;

      indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
      indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gAcknowledge_c;
      indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
      indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
      PopMemCpy(&indication->sFrame.aPayload[MP_ACCESS_DATA], &sMpMsgAcknowledge,
                sizeof(sMpMsgAcknowledge_t));
      indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
          sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + sizeof(sMpMsgAcknowledge_t);
      MpGwDataIndication(indication);
      break;
    }
    case gGetCirrusParamReq_c: {
      sMpMsgGetCirrusParamReq_t* getCirrusParamReq =
        (sMpMsgGetCirrusParamReq_t*)pPayload;
      sMpMsgGetCirrusParamResp_t getCirrusParamResp;
      uint32_t paramSize;

      SwapBytes32(&getCirrusParamReq->index);
      SEGGER_RTT_printf(0, "%d, Get Cirrus Param, index: %d\r\n",
          PopGetSystick(), getCirrusParamReq->index);

      // add network data
      getCirrusParamResp.index = getCirrusParamReq->index;
      paramSize = CirrusGwGetParam(getCirrusParamResp.index,
                                   getCirrusParamResp.data);
      CirrusGwNetworkFormatParam(getCirrusParamResp.index,
                                   getCirrusParamResp.data);
      SwapBytes32(&getCirrusParamResp.index);

      indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
      indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gGetCirrusParamResp_c;
      indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
      indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
      PopMemCpy(&indication->sFrame.aPayload[MP_ACCESS_DATA], &getCirrusParamResp,
                sizeof(getCirrusParamResp));
      indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
          sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA +
          sizeof(getCirrusParamResp.index) + paramSize;
      MpGwDataIndication(indication);
      break;
    }
    default:
      // Not acknowledged command
      respPayload[DATA_RSP_EVTID] = gMpNwkDataRsp_c;
      respPayload[DATA_RSP_LEN] = 1;
      respPayload[DATA_RSP_STATUS] = gPopErrNotSupported_c;
      MpGwSendPacket(respPayload);
      break;
  }
}

/*******************************************************************************
---------------- MpGwNodePlaceProcessBeacon ------------------------------------
Process beacon indications for node placment. In "node placement" mode.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNodePlaceProcessBeacon(sPopNwkBeaconIndication_t *pNwkBeaconIndication)
{
  // Verify ProtocolID and PanID before setting status
  if(pNwkBeaconIndication->iProtocol == gPopNetId_c &&
     (pNwkBeaconIndication->iPanId == PopNwkGetPanId()))
  {
    // Update BeaconCount
    gsMpNodePlacementResult.iNodeCount++;

    // Update the Lqi
    if(pNwkBeaconIndication->iLqi > gsMpNodePlacementResult.iLqi) {
      gsMpNodePlacementResult.iLqi = pNwkBeaconIndication->iLqi;
    }

    // If LQI under min value put a 1 in iBar
    if(gsMpNodePlacementResult.iLqi < gaMpNodePlacementStrongSignal[0]) {
      gsMpNodePlacementResult.iBars=0x01;
    }

    // If LQI between min and max values iBar = 2
    if(gsMpNodePlacementResult.iLqi > gaMpNodePlacementStrongSignal[0] && gsMpNodePlacementResult.iLqi <= gaMpNodePlacementStrongSignal[1]) {
      gsMpNodePlacementResult.iBars=0x02;
    }

    // If LQI above max value iBar = 3
    if( gsMpNodePlacementResult.iLqi > gaMpNodePlacementStrongSignal[1] )
    {
      gsMpNodePlacementResult.iBars=0x03;

        // If node count >1 then iBar=4
      if(gsMpNodePlacementResult.iNodeCount>1) {
          gsMpNodePlacementResult.iBars=0x04;
      }
    }

  }

  // Remember the memory will be freed after leaving this function.
}

/*******************************************************************************

---------------- MpGwStartNetworkReq -------------------------------------------
  MpGwStartNetwork gatewy request handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwStartNetworkReq(sPopEvtData_t *pData)
{
  sMpStartNetworkReq_t sMpStartNetworkReq;
  uint8_t *pPayload = (uint8_t*)pData;

  // place data into struct and transform PanId to native
  sMpStartNetworkReq.iChannel = (popChannel_t)pPayload[START_NWK_REQ_CHANNEL];
  sMpStartNetworkReq.iPanId = (popPanId_t)(pPayload[START_NWK_REQ_PANID_H] << 8);
  sMpStartNetworkReq.iPanId |= (popPanId_t)(pPayload[START_NWK_REQ_PANID_L]);
  sMpStartNetworkReq.iNwkAddr = (popNwkAddr_t)(pPayload[START_NWK_REQ_NODEID_H] << 8);
  sMpStartNetworkReq.iNwkAddr |= (popNwkAddr_t)(pPayload[START_NWK_REQ_NODEID_L]);

  // Check if Auto or user defined start of network
  if(sMpStartNetworkReq.iChannel == 0){
    // Form network on any Channel and PAN Id with energy scan
    popChannelList_t iChannelList = 0xFFFF; // all channels
    PopNwkSetChannelList(iChannelList);

    PopNwkFormNetwork(gPopNwkJoinOptsEnergyScan_c,
                      gPopNwkUseChannelList_c,
                      gPopNwkAnyPanId_c);
  }
  else{
    bool result = false;

    // Check if valid node id
    result = (sMpStartNetworkReq.iNwkAddr < gPopNwkReservedAddr_c) ? true:false;

    if(result){
      // Perform silent start with nwk data parameters retrieved
      PopNwkSetChannel(sMpStartNetworkReq.iChannel);
      PopNwkSetPanId(sMpStartNetworkReq.iPanId);
      PopNwkSetNodeAddr(sMpStartNetworkReq.iNwkAddr);
      PopNwkStartNetwork();
    }
    else{
      // Form network using JoinOptions normal
      PopNwkFormNetwork(gPopNwkJoinOptsNormal_c,
                        sMpStartNetworkReq.iChannel,
                        sMpStartNetworkReq.iPanId);
    }
  }

}

/*******************************************************************************
---------------- MpGwStartNetworkRsp -------------------------------------------
  MpGwStartNetwork event response handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwStartNetworkRsp(popStatus_t iNwkStartConfirm)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpStartNetworkRsp_t sMpStartNetworkRsp;
  uint8_t pPayload[START_NWK_RSP_SIZE];

  gfPopNwkIsProcessingBeaconEnabled = false;

  // Get data and put to message
  // Event id
  iEvtId = gMpStartNetworkRsp_c;
  pPayload[START_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
  // Length
  iLen = START_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[START_NWK_RSP_LEN] = (uint8_t)(iLen);
  // Status
  sMpStartNetworkRsp.iStatus = iNwkStartConfirm;
  pPayload[START_NWK_RSP_STATUS] = (uint8_t)(sMpStartNetworkRsp.iStatus);
  // Channel
  sMpStartNetworkRsp.iChannel = PopNwkGetChannel();
  pPayload[START_NWK_RSP_CHANNEL] = (uint8_t)(sMpStartNetworkRsp.iChannel);
  // Pan id
  sMpStartNetworkRsp.iPanId = PopNwkGetPanId();
  pPayload[START_NWK_RSP_PANID_H] = (uint8_t)(sMpStartNetworkRsp.iPanId >> 8);
  pPayload[START_NWK_RSP_PANID_L] = (uint8_t)(sMpStartNetworkRsp.iPanId);
  // Node id
  sMpStartNetworkRsp.iNwkAddr = PopNwkGetNodeAddr();
  pPayload[START_NWK_RSP_NODEID_H] = (uint8_t)(sMpStartNetworkRsp.iNwkAddr >> 8);
  pPayload[START_NWK_RSP_NODEID_L] = (uint8_t)(sMpStartNetworkRsp.iNwkAddr);
  // transform from native to gateway

  // send response to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwJoinEnableReq --------------------------------------------
  MpGwJoinEnableReq event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinEnableReq(uint16_t iWord)
{
  sMpJoinEnableReq_t sMpJoinEnableReq;
  uint8_t *pPayload;
  GatewayToNative16(&iWord);
  pPayload = (uint8_t *)&iWord;

  sMpJoinEnableReq.fEnable = (bool)(pPayload[JOIN_ENABLE_REQ_ENABLE]);
  sMpJoinEnableReq.fNetworkWide = (bool)(pPayload[JOIN_ENABLE_REQ_NETWORKWIDE]);
  PopNwkJoinEnable(sMpJoinEnableReq.fEnable, sMpJoinEnableReq.fNetworkWide);
  // call response function
  MpGwJoinEnableRsp();
}

/*******************************************************************************
---------------- MpGwJoinEnableRsp --------------------------------------------
  MpGwJoinEnableRsp event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinEnableRsp(void)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpJoinEnableRsp_t sMpJoinEnableRsp;
  uint8_t pPayload[JOIN_ENABLE_RSP_SIZE];

  //Get data & fill message with data
  // Event id
  iEvtId = gMpJoinEnableRsp_c;
  pPayload[JOIN_ENABLE_RSP_EVTID] = (uint8_t)(iEvtId);
  // Length
  iLen = JOIN_ENABLE_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[JOIN_ENABLE_RSP_LEN] = (uint8_t)(iLen);
  // Status
  sMpJoinEnableRsp.fStatus =  PopNwkGetJoinStatus();
  pPayload[JOIN_ENABLE_RSP_ENABLE] = (uint8_t)(sMpJoinEnableRsp.fStatus);
  // Send event to PC
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwJoinNetworkReq-----------------------------------------
  MpGwJoinNetwork gateway request handling. Any node on the specified Channel
  and Pan Id will do as long as it has Join enable set. Hopefully only one node
  will have join enable set.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinNetworkReq(uint8_t *pPayload)
{
  sMpJoinNetworkReq_t sMpJoinNetworkReq;
  popChannelList_t iChannelList = 0;

  // place data into struct and transform PanId to native
  sMpJoinNetworkReq.iChannel = (popChannel_t)(pPayload[JOIN_NWK_REQ_CHANNEL]);
  sMpJoinNetworkReq.iPanId = (popPanId_t)(pPayload[JOIN_NWK_REQ_PANID_H] << 8);
  sMpJoinNetworkReq.iPanId |= (popPanId_t)(pPayload[JOIN_NWK_REQ_PANID_L]);

    // Store join request info
  gsPopAppJoinReqNwkInfo.iChannel = sMpJoinNetworkReq.iChannel;
  gsPopAppJoinReqNwkInfo.iPanId = sMpJoinNetworkReq.iPanId;

    // Check if Auto or user defined start of network
  if(sMpJoinNetworkReq.iChannel == 0){
    // Set channel list to scan all channels
    iChannelList = 0xFFFF;
    PopNwkSetChannelList(iChannelList);
  }
  else{
    // set channel list to match requested channel
    iChannelList = 1 << (sMpJoinNetworkReq.iChannel - 11); // one channel
    PopNwkSetChannelList(iChannelList);
  }

  gfPopNwkIsProcessingBeaconEnabled = true;
  gfMpIsProcessingScanConfirm = true; // handle scanConfirm
  PopNwkScanForNetworks(iChannelList, // PopNwkGetChannelList()
                        PopNwkGetJoinOptions(),
                        PopNwkGetScanTimeMs());
}

/*******************************************************************************
---------------- MpGwJoinNetworkRsp --------------------------------------------
  MpGwJoinNetwork event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwJoinNetworkRsp(popStatus_t iNwkStartConfirm)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpJoinNetworkRsp_t sMpJoinNetworkRsp;
  uint8_t pPayload[JOIN_NWK_RSP_SIZE];

  gfPopNwkIsProcessingBeaconEnabled = false;

  // get data and put to message
  iEvtId = gMpJoinNetworkRsp_c;
  pPayload[JOIN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
  iLen = JOIN_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[JOIN_NWK_RSP_LEN] = (uint8_t)(iLen);
  // Status
  sMpJoinNetworkRsp.iStatus = iNwkStartConfirm;
  pPayload[JOIN_NWK_RSP_STATUS] = (uint8_t)(sMpJoinNetworkRsp.iStatus);
  // channel
  sMpJoinNetworkRsp.iChannel = PopNwkGetChannel();
  pPayload[JOIN_NWK_RSP_CHANNEL] = (uint8_t)(sMpJoinNetworkRsp.iChannel);
  // Pan id
  sMpJoinNetworkRsp.iPanId = PopNwkGetPanId();
  pPayload[JOIN_NWK_RSP_PANID_H] = (uint8_t)(sMpJoinNetworkRsp.iPanId >> 8);
  pPayload[JOIN_NWK_RSP_PANID_L] = (uint8_t)(sMpJoinNetworkRsp.iPanId);
  // Node id
  sMpJoinNetworkRsp.iNwkAddr = PopNwkGetNodeAddr();
  pPayload[JOIN_NWK_RSP_NODEID_H] = (uint8_t)(sMpJoinNetworkRsp.iNwkAddr >> 8);
  pPayload[JOIN_NWK_RSP_NODEID_L] = (uint8_t)(sMpJoinNetworkRsp.iNwkAddr);
  // Send message to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwLeaveNetworkReq -------------------------------------------
  MpGwLeaveNetwork gateway request handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwLeaveNetworkReq(void)
{
  // leave network
  PopNwkLeaveNetwork();
}

/*******************************************************************************
---------------- MpGwLeaveNetworkRsp -------------------------------------------
  MpGwLeaveNetwork event handling. The PopNwkLeaveNetwork cannot fail so if
  the event gPopNekLeaveConfirm_c has arrived, which trigger this function
  it means that this node has left the network.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwLeaveNetworkRsp(void)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpLeaveNetworkRsp_t sMpLeaveNetworkRsp;
  uint8_t pPayload[LEAVE_NWK_RSP_SIZE];

  // get data and put to message
  // Event id
  iEvtId = gMpLeaveNetworkRsp_c;
  pPayload[LEAVE_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
  // Length
  iLen = LEAVE_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[LEAVE_NWK_RSP_LEN] = (uint8_t)(iLen);
  // Status
  sMpLeaveNetworkRsp.iStatus = gPopErrNone_c;
  pPayload[LEAVE_NWK_RSP_STATUS] =  (uint8_t)(sMpLeaveNetworkRsp.iStatus);
  // Send message to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwScanNetworkHandling ---------------------------------------
  popEvtNwkBeaconIndication event handling.
  Collect all beacons. restart timer for timeout
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanNetworkHandling(sPopNwkBeaconIndication_t *pNwkBeaconIndication)
{
  popChannel_t iChannel = pNwkBeaconIndication->iChannel;
  popPanId_t iPanId = pNwkBeaconIndication->iPanId;
  popNwkAddr_t iNwkAddr = pNwkBeaconIndication->iNwkAddr;
  bool iNwkDoublet = false;
  uint8_t iTableIndex = 0;

  // Loop throgh table of already received becons to find if new beacon is a doublet
  while((iTableIndex < giExclusiveNetworks) && !iNwkDoublet){
    if(iChannel == gsMpScanNetworkRspTable[iTableIndex].iChannel){
      if(iPanId == gsMpScanNetworkRspTable[iTableIndex].iPanId){
        // Network already in table
        iNwkDoublet = true;
      }
    }
    iTableIndex++;
  }

  // fill table with exclusive networks, no doubletts
  if(!iNwkDoublet && (giExclusiveNetworks < MAXNWK)){
    gsMpScanNetworkRspTable[giExclusiveNetworks].iChannel = iChannel;
    gsMpScanNetworkRspTable[giExclusiveNetworks].iPanId = iPanId;
    gsMpScanNetworkRspTable[giExclusiveNetworks].iNwkAddr = iNwkAddr;
    giExclusiveNetworks++;
  }
}

/*******************************************************************************
---------------- MpGwScanNetworkReq --------------------------------------------
  MpGwScanNetwork gateway request handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanNetworkReq(uint8_t *pPayload)
{
  popChannelList_t iChannelList;
  popChannel_t iChannel;
  popPanId_t iPanId;

  // Get channel and PanId from request data
  iChannel = (popChannel_t)pPayload[SCAN_NWK_REQ_CHANNEL];
  iPanId = (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_H] << 8);
  iPanId |= (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_L]);

  // reset global table of networks found
  for(int i = 0; i < MAXNWK; i++){
    gsMpScanNetworkRspTable[i].iChannel = 0;
    gsMpScanNetworkRspTable[i].iPanId = 0;
    gsMpScanNetworkRspTable[i].iNwkAddr = 0;
    giExclusiveNetworks = 0;
  }

  // Enable beacon events in app
  // set channel list from channel
  // if channel = 0 scan all channels
  if(iChannel == gPopNwkUseChannelList_c){
    iChannelList = 0xFFFF;
    // Set flag for Beacon handling
    gfMpGwIsProcessingBeacon_c = true;
    gfPopNwkIsProcessingBeaconEnabled = true;
    // scan for network
    PopNwkScanForNetworks(iChannelList, 0, PopNwkGetScanTimeMs());
  }
  // else set the corresponding bit in channellist
  else{
    if(iPanId == gPopNwkAnyPanId_c){
      // else set the corresponding bit in channellist
      iChannelList = 1 << (iChannel - 11);
      // Set flag for Beacon handling
      gfMpGwIsProcessingBeacon_c = true;
      gfPopNwkIsProcessingBeaconEnabled = true;
      // scan for network
      PopNwkScanForNetworks(iChannelList, 0, PopNwkGetScanTimeMs());
    }
    else{
      // wrong param send response with no data
      MpGwScanNetworkRsp();
    }
  }
}
/*******************************************************************************
---------------- MpGwScanNetworkRsp --------------------------------------------
  MpGwScanNetwork event handling.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanNetworkRsp(void)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  uint8_t pPayload[SCAN_NWK_RSP_SIZE];
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;

  if(giExclusiveNetworks){
    for(int i = 0; i < giExclusiveNetworks; i++){
      // Get data and put data to message buffer
      // Event id
      iEvtId = gMpScanNetworkRsp_c;
      pPayload[SCAN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
      // Length
      iLen = SCAN_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
      pPayload[SCAN_NWK_RSP_LEN] = (uint8_t)(iLen);
      // Channel
      iChannel = gsMpScanNetworkRspTable[i].iChannel;
      pPayload[SCAN_NWK_RSP_CHANNEL] = (uint8_t)(iChannel);
      // Pan Id
      iPanId = gsMpScanNetworkRspTable[i].iPanId;
      pPayload[SCAN_NWK_RSP_PANID_H] = (uint8_t)(iPanId >> 8);
      pPayload[SCAN_NWK_RSP_PANID_L] = (uint8_t)(iPanId);
      // Node Id
      iNwkAddr = gsMpScanNetworkRspTable[i].iNwkAddr;
      pPayload[SCAN_NWK_RSP_NODEID_H] = (uint8_t)(iNwkAddr >> 8);
      pPayload[SCAN_NWK_RSP_NODEID_L] = (uint8_t)(iNwkAddr);

      // send data to gateway
      MpGwSendPacket(pPayload);
    }
  }
  else{
    // No beacons found
    // Put default data to message buffer
    // Event id
    iEvtId = gMpScanNetworkRsp_c;
    pPayload[SCAN_NWK_RSP_EVTID] = (uint8_t)(iEvtId);
    // Length
    iLen = SCAN_NWK_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
    pPayload[SCAN_NWK_RSP_LEN] = (uint8_t)(iLen);
    // Channel
    iChannel = 0x00;
    pPayload[SCAN_NWK_RSP_CHANNEL] = (uint8_t)(iChannel);
    // Pan Id
    iPanId = 0x0000;
    pPayload[SCAN_NWK_RSP_PANID_H] = (uint8_t)(iPanId >> 8);
    pPayload[SCAN_NWK_RSP_PANID_L] = (uint8_t)(iPanId);
    // Node Id
    iNwkAddr = 0x0000;
    pPayload[SCAN_NWK_RSP_NODEID_H] = (uint8_t)(iNwkAddr >> 8);
    pPayload[SCAN_NWK_RSP_NODEID_L] = (uint8_t)(iNwkAddr);

    // send data to gateway
    MpGwSendPacket(pPayload);
  }
  // reset flag for beacon procesing
  gfMpGwIsProcessingBeacon_c = false;
  gfPopNwkIsProcessingBeaconEnabled = false;
}

/*******************************************************************************
---------------- MpGwSetRadioParamReq -------------------------------------------
  MpGwSetRadioParamReq event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSetRadioParamReq(sPopEvtData_t *pData)
{
  uint8_t iStatus;
  sMpSetRadioParamReq_t sMpSetRadioParamReq;
  uint8_t *pPayload;
// #106  uint8_t NewRadioParam;

  pPayload = (uint8_t *)pData;

  iStatus = gPopErrFailed_c;
  sMpSetRadioParamReq.iChannel = (popChannel_t)pPayload[SET_RADIO_PARAM_REQ_CHANNEL];
  sMpSetRadioParamReq.iPanId = (popPanId_t)(pPayload[SET_RADIO_PARAM_REQ_PANID_H] << 8);
  sMpSetRadioParamReq.iPanId |= (popPanId_t)(pPayload[SET_RADIO_PARAM_REQ_PANID_L]);
  sMpSetRadioParamReq.iNwkAddr = (popNwkAddr_t)(pPayload[SET_RADIO_PARAM_REQ_NODEID_H] << 8);
  sMpSetRadioParamReq.iNwkAddr |= (popNwkAddr_t)(pPayload[SET_RADIO_PARAM_REQ_NODEID_L]);

  if(MpGwValidNwkData(sMpSetRadioParamReq.iChannel, sMpSetRadioParamReq.iPanId, sMpSetRadioParamReq.iNwkAddr)){
    // set channel
    //PopNwkSetNwkDataChannel(sMpSetRadioParamReq.iChannel);
    PopNwkSetChannel(sMpSetRadioParamReq.iChannel);
    // set PanId
    PopNwkSetPanId(sMpSetRadioParamReq.iPanId);
    // set NodeAddr
    PopNwkSetNodeAddr(sMpSetRadioParamReq.iNwkAddr);

    // store network data
    iStatus = PopNvStoreNwkData();
    // reset CPU so that new values can be initialized
    if(iStatus == gPopErrNone_c){
      extern uint16_t giState;
      giState = START;
      PopNwkStartNetwork();
    }
  }
  else{
    // Invalid parameters
    iStatus = gPopErrBadParm_c;
  }
  // call response function
  MpGwSetRadioParamRsp(iStatus);
}

/*******************************************************************************
---------------- MpGwSetRadioParamRsp ------------------------------------------
  MpGwSetRadioParamRsp response handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSetRadioParamRsp(uint8_t iStatus)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpSetRadioParamRsp_t sMpSetRadioParamRsp;
  uint8_t pPayload[SET_RADIO_PARAM_RSP_SIZE];

  //get data and put to message array
  // Event Id
  iEvtId = gMpSetRadioParamRsp_c;
  pPayload[SET_RADIO_PARAM_RSP_EVTID] = (uint8_t)iEvtId;
  // Length
  iLen = SET_RADIO_PARAM_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[SET_RADIO_PARAM_RSP_LEN] = (uint8_t)iLen;
  // Status
  sMpSetRadioParamRsp.iStatus = iStatus;
  pPayload[SET_RADIO_PARAM_RSP_STATUS] = (uint8_t)sMpSetRadioParamRsp.iStatus;
  // send data to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwGetRadioDataReq -------------------------------------------
  MpGwGetRadioData event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwGetRadioDataReq(void)
{
  // nothing to do but call the response function
  MpGwGetRadioDataRsp();
}

/*******************************************************************************
---------------- MpGwGetRadioDataRsp -------------------------------------------
  MpGwGetRadioData response handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwGetRadioDataRsp(void)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpGetRadioDataRsp_t sMpGetRadioDataRsp;
  uint8_t pPayload[RADIO_DATA_RSP_SIZE];

  // Get data and put to message payload
  // Event Id
  iEvtId = gMpGetRadioDataRsp_c;
  pPayload[RADIO_DATA_RSP_EVTID] = (uint8_t)iEvtId;
  // Length
  iLen = RADIO_DATA_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[RADIO_DATA_RSP_LEN] = (uint8_t)iLen;
  // Firmware type
  sMpGetRadioDataRsp.iFwType = giFwType;
  pPayload[RADIO_DATA_RSP_FWTYPE_H] = (uint8_t)(sMpGetRadioDataRsp.iFwType >> 24);
  pPayload[RADIO_DATA_RSP_FWTYPE_MH] = (uint8_t)(sMpGetRadioDataRsp.iFwType >> 16);
  pPayload[RADIO_DATA_RSP_FWTYPE_ML] =(uint8_t)(sMpGetRadioDataRsp.iFwType >> 8);
  pPayload[RADIO_DATA_RSP_FWTYPE_L] = (uint8_t)(sMpGetRadioDataRsp.iFwType);
  // Firmware version
  sMpGetRadioDataRsp.iFwVer = giFwVer;
  pPayload[RADIO_DATA_RSP_FWVER_H] = (uint8_t)(sMpGetRadioDataRsp.iFwVer >> 24);
  pPayload[RADIO_DATA_RSP_FWVER_MH] = (uint8_t)(sMpGetRadioDataRsp.iFwVer >> 16);
  pPayload[RADIO_DATA_RSP_FWVER_ML] =(uint8_t)(sMpGetRadioDataRsp.iFwVer >> 8);
  pPayload[RADIO_DATA_RSP_FWVER_L] = (uint8_t)(sMpGetRadioDataRsp.iFwVer);
  // Checksum
  sMpGetRadioDataRsp.iChkSum = giChkSum;
  pPayload[RADIO_DATA_RSP_CHKSUM_H] = (uint8_t)(sMpGetRadioDataRsp.iChkSum >> 24);
  pPayload[RADIO_DATA_RSP_CHKSUM_MH] = (uint8_t)(sMpGetRadioDataRsp.iChkSum >> 16);
  pPayload[RADIO_DATA_RSP_CHKSUM_ML] =(uint8_t)(sMpGetRadioDataRsp.iChkSum >> 8);
  pPayload[RADIO_DATA_RSP_CHKSUM_L] = (uint8_t)(sMpGetRadioDataRsp.iChkSum);
  // CM id (MAC address)
  PopNwkGetMacAddr(sMpGetRadioDataRsp.aCmId);
  pPayload[RADIO_DATA_RSP_CMID_0] = sMpGetRadioDataRsp.aCmId[0];
  pPayload[RADIO_DATA_RSP_CMID_1] = sMpGetRadioDataRsp.aCmId[1];
  pPayload[RADIO_DATA_RSP_CMID_2] = sMpGetRadioDataRsp.aCmId[2];
  pPayload[RADIO_DATA_RSP_CMID_3] = sMpGetRadioDataRsp.aCmId[3];
  pPayload[RADIO_DATA_RSP_CMID_4] = sMpGetRadioDataRsp.aCmId[4];
  pPayload[RADIO_DATA_RSP_CMID_5] = sMpGetRadioDataRsp.aCmId[5];
  pPayload[RADIO_DATA_RSP_CMID_6] = sMpGetRadioDataRsp.aCmId[6];
  pPayload[RADIO_DATA_RSP_CMID_7] = sMpGetRadioDataRsp.aCmId[7];
  // Manufacturer id
  sMpGetRadioDataRsp.iMfgId = PopNwkGetManufacturerId();
  pPayload[RADIO_DATA_RSP_MFGID_H] = (uint8_t)(sMpGetRadioDataRsp.iMfgId >> 8);
  pPayload[RADIO_DATA_RSP_MFGID_L] = (uint8_t)(sMpGetRadioDataRsp.iMfgId);
  // Application id
  sMpGetRadioDataRsp.iAppId = PopNwkGetApplicationId();
  pPayload[RADIO_DATA_RSP_APPID_H] = (uint8_t)(sMpGetRadioDataRsp.iAppId >> 8);
  pPayload[RADIO_DATA_RSP_APPID_L] = (uint8_t)(sMpGetRadioDataRsp.iAppId);
  // Channel id
  sMpGetRadioDataRsp.iChannel = PopNwkGetChannel();
  pPayload[RADIO_DATA_RSP_CHANNEL] = (uint8_t)(sMpGetRadioDataRsp.iChannel);
  // PAN id
  sMpGetRadioDataRsp.iPanId = PopNwkGetPanId();
  pPayload[RADIO_DATA_RSP_PANID_H] = (uint8_t)(sMpGetRadioDataRsp.iPanId >> 8);
  pPayload[RADIO_DATA_RSP_PANID_L] = (uint8_t)(sMpGetRadioDataRsp.iPanId);
  // Node id
  sMpGetRadioDataRsp.iNwkAddr = PopNwkGetNodeAddr();
  pPayload[RADIO_DATA_RSP_NODEID_H] = (uint8_t)(sMpGetRadioDataRsp.iNwkAddr >> 8);
  pPayload[RADIO_DATA_RSP_NODEID_L] = (uint8_t)(sMpGetRadioDataRsp.iNwkAddr);
  // Address pool
  sMpGetRadioDataRsp.AddrPool = PopNwkGetAddrCount();
  pPayload[RADIO_DATA_RSP_ADDRPOOL_H] = (uint8_t)(sMpGetRadioDataRsp.AddrPool >> 8);
  pPayload[RADIO_DATA_RSP_ADDRPOOL_L] = (uint8_t)(sMpGetRadioDataRsp.AddrPool);

  // send data to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwNwkDataReq ------------------------------------------------
  If it is a request message add the message to queue and
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNwkDataReq(sPopEvtData_t *pData)
{

  uint8_t *pPayload;
  sMessage_t sThisMessage;
  uint8_t iStatus;

  // set return status to fail by default
  iStatus = gPopErrFailed_c;

  pPayload = (uint8_t *)pData;

  sThisMessage.iDstAddr = (popNwkAddr_t)(pPayload[DATA_REQ_DSTADDR_H] << 8);  // Get Destination address
  sThisMessage.iDstAddr |= (popNwkAddr_t)(pPayload[DATA_REQ_DSTADDR_L]);      // Get Destination address
  sThisMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;                     // Get Options
  sThisMessage.iRadius = PopNwkGetDefaultRadius();                            // Get Radius
  sThisMessage.iPayloadLength = pPayload[DATA_REQ_PAYLOADLEN];                // Get payload length

  // Check if message is for SM
  if (sThisMessage.iDstAddr == PopNwkGetNodeAddr()) {
    MpGwSmDataReq(pData);
    return;
  }

  if (sThisMessage.iPayloadLength <= MAX_PAYLOAD) {
    PopMemCpy(&(sThisMessage.aPayload), &pPayload[DATA_REQ_PAYLOAD],
        sThisMessage.iPayloadLength);    // copy payload to message

    // If application could handle the message set response status to true
    if (MpNwkDataRequest(&sThisMessage)) {
      iStatus = gPopErrNone_c;
    }
  } else {
#if DEBUG >= 2
    extern sMpDebugCount_t gsMpDebug;
    gsMpDebug.iMaxPayloadCnt++;
#endif
  }

  // Send message to Network application
  MpGwNwkDataRsp(iStatus);
}

/*******************************************************************************
---------------- MpGwNwkDataRsp --------------------------------------------
  Response sent as soon as radio app has checked if it can handle the data request
  message (if it has enough free buffers)
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNwkDataRsp(uint8_t iStatus)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpNwkDataRsp_t sMpNwkDataRsp;
  uint8_t pPayload[DATA_RSP_SIZE];

  // get data and put to message payload
  // Event Id
  iEvtId = gMpNwkDataRsp_c;
  pPayload[DATA_RSP_EVTID] = (uint8_t)iEvtId;
  // Length
  iLen = DATA_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[DATA_RSP_LEN] = (uint8_t)iLen;
  // Status
  sMpNwkDataRsp.iStatus = iStatus;
  pPayload[DATA_RSP_STATUS] = sMpNwkDataRsp.iStatus;
  // send data to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwDataIndication ---------------------------------------
  MpGwDataIndication event handling.

--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwDataIndication(sPopNwkDataIndication_t *pIndication)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  uint8_t payloadLen;
  sMpDataIndication_t sMpDataIndication;
  uint8_t pPayload[DATA_IND_SIZE + MAX_PAYLOAD];

  payloadLen = PopNwkPayloadLen(pIndication);

  if(payloadLen > MAX_PAYLOAD)
  {
#if DEBUG >= 1
    #warning debug
    extern sPopAssertCount_t gsPopAssert;
    gsPopAssert.iAssertBadReceivePtr++;
#endif
    payloadLen = MAX_PAYLOAD;
  }

  //Get data and put into message array
  // Event Id
  iEvtId = gMpDataIndication_c;
  pPayload[DATA_IND_EVTID] = (uint8_t)(iEvtId);
  // Length
  iLen = sizeof(sMpDataIndication.iSrcAddr) + sizeof(sMpDataIndication.iPayloadLength) + payloadLen;
  pPayload[DATA_IND_LEN] = (uint8_t)(iLen);
  // Source address
  sMpDataIndication.iSrcAddr = PopNwkSrcAddr(pIndication);
  pPayload[DATA_IND_SRCADDR_H] = (uint8_t)(sMpDataIndication.iSrcAddr >> 8);
  pPayload[DATA_IND_SRCADDR_L] = (uint8_t)(sMpDataIndication.iSrcAddr);
  // Payload length
  sMpDataIndication.iPayloadLength = payloadLen;
  pPayload[DATA_IND_PAYLOADLEN] = sMpDataIndication.iPayloadLength;
  // Payload
  sMpDataIndication.pPayload = PopNwkPayload(pIndication);
  PopMemCpy(&pPayload[DATA_IND_PAYLOAD], sMpDataIndication.pPayload, sMpDataIndication.iPayloadLength);

  // send data to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwNodePlacementReq ------------------------------------------
  MpGwNodePlacementReq event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNodePlacementReq(void)
{
  // start node placement
  if(!gfMpPlacementStart){
    MpGwStartNodePlacement(PopNwkGetChannel());
  }

  // start timer for function timeout
  (void)PopStartTimerEx(cGwTaskId,gMpGwNodePlacementTimerId_c,gMpGwNodePlacementTime_c);
}

/*******************************************************************************
---------------- MpGwNodePlacementRsp ------------------------------------------
  MpGwNodePlacementRsp event handling
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwNodePlacementRsp(sPopNodePlacement_t *pPopNodePlacementResult)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpNodePlacementRsp_t sMpNodePlacementRsp;
  uint8_t pPayload[NODE_PLACEMENT_RSP_SIZE];

  // get data and put to message payload
  // Event Id
  iEvtId = gMpNodePlacementRsp_c;
  pPayload[NODE_PLACEMENT_RSP_EVTID] = (uint8_t)iEvtId;
  // Length
  iLen = sizeof(sMpNodePlacementRsp.iStatus);
  pPayload[NODE_PLACEMENT_RSP_LEN] = (uint8_t)iLen;
  // Status
  sMpNodePlacementRsp.iStatus = pPopNodePlacementResult->iBars;
  pPayload[NODE_PLACEMENT_RSP_STATUS] = sMpNodePlacementRsp.iStatus;
  // send data to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwSetUserLevelReq -------------------------------------------
  Set user level
--------------------------------------------------------------------------------
*******************************************************************************/
uint8_t MpGwSetUserLevelReq(uint8_t *pByte)
{
  uint8_t *pPayload;
  uint8_t iUserLevel;

  pPayload = pByte;
  iUserLevel = pPayload[SET_USERLEVEL_REQ_USERLEVEL];

  return iUserLevel;
}

/*******************************************************************************
---------------- MpGwSetUserLevelRsp -------------------------------------------
  Response sent as soon as user level has been set
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwSetUserLevelRsp(uint8_t iStatus)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpSetUserLevelRsp_t sMpSetUserLevelRsp;
  uint8_t pPayload[SET_USERLEVEL_RSP_SIZE];

  // get data and put to message payload
  // Event Id
  iEvtId = gMpSetUserLevelRsp_c;
  pPayload[SET_USERLEVEL_RSP_EVTID] = (uint8_t)iEvtId;
  // Length
  iLen = SET_USERLEVEL_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[SET_USERLEVEL_RSP_LEN] = (uint8_t)iLen;
  // Status
  sMpSetUserLevelRsp.iStatus = iStatus;
  pPayload[SET_USERLEVEL_RSP_STATUS] = sMpSetUserLevelRsp.iStatus;
  // send data to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwGetUserLevelRsp -------------------------------------------
  Response sent as soon as user level has been set
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwGetUserLevelRsp(uint8_t iUserLevel)
{
  popEvtId_t iEvtId;
  uint8_t iLen;
  sMpGetUserLevelRsp_t sMpGetUserLevelRsp;
  uint8_t pPayload[GET_USERLEVEL_RSP_SIZE];

  // get data and put to message payload
  // Event Id
  iEvtId = gMpGetUserLevelRsp_c;
  pPayload[GET_USERLEVEL_RSP_EVTID] = (uint8_t)iEvtId;
  // Length
  iLen = GET_USERLEVEL_RSP_SIZE - sizeof(iEvtId) - sizeof(iLen);
  pPayload[GET_USERLEVEL_RSP_LEN] = (uint8_t)iLen;
  // Status
  sMpGetUserLevelRsp.iUserLevel = iUserLevel;
  pPayload[GET_USERLEVEL_RSP_USERLEVEL] = sMpGetUserLevelRsp.iUserLevel;
  // send data to gateway
  MpGwSendPacket(pPayload);
}

/*******************************************************************************
---------------- MpGwScanForNodesHandling ---------------------------------------
  ScanForNodes event handling.
  Collect all node addreses. restart timer for timeout
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanForNodesHandling(sPopNwkDataIndication_t *pDataIndication,
                              const uint8_t length)
{
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
  uint8_t radius;
  sMpMsgAcknowledge_t* msg = (sMpMsgAcknowledge_t*)
    &pDataIndication->sFrame.aPayload[MP_ACCESS_DATA];

  iPanId = pDataIndication->sFrame.sMac.iPanId;
  iNwkAddr = PopNwkSrcAddr(pDataIndication);
  radius = pDataIndication->sFrame.sNwk.iRadius;

  if(iPanId == gsMpScanForNodesReq.iPanId)
  {
    if(giNodesInList < NODELIST_SIZE)
    {
      // Add new node NwkAddr to table if list is not already full
      gsMpNodeList[giNodesInList].iNwkAddr = iNwkAddr;
      gsMpNodeList[giNodesInList].radius = radius;

      if (length < 10) {
        gsMpNodeList[giNodesInList].type = 0xff;
        gsMpNodeList[giNodesInList].id = 0xffffffff;
      } else {
        gsMpNodeList[giNodesInList].type = msg->type;
        gsMpNodeList[giNodesInList].id = msg->id;
      }

      giNodesInList++;

      // If list is full, send nodes and clear
      if (giNodesInList == NODELIST_SIZE) {
        MpGwScanForNodesRsp(true);
      }
    }
  }
}

/*******************************************************************************
---------------- MpGwScanForNodesReq --------------------------------------------
  MpGwForNodes gateway request handling
--------------------------------------------------------------------------------
*******************************************************************************/
bool MpGwScanForNodesReq(uint8_t *pPayload)
{
  popChannel_t iChannel;
  popPanId_t iPanId;
  bool fValidNwkData;

  // Assume correct parameters
  fValidNwkData = true;

  // clear NodeList
  giNodesInList = 0;

  // Store Channel and PanId in global structure for later use
  gsMpScanForNodesReq.iChannel = (popChannel_t)pPayload[SCAN_NWK_REQ_CHANNEL];
  gsMpScanForNodesReq.iPanId = (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_H] << 8);
  gsMpScanForNodesReq.iPanId |= (popPanId_t)(pPayload[SCAN_NWK_REQ_PANID_L]);

  // Get local network parameters
  iChannel = PopNwkGetChannel();
  iPanId = PopNwkGetPanId();

  // Check if valid network parameters
  if((gsMpScanForNodesReq.iChannel == iChannel) && (gsMpScanForNodesReq.iPanId == iPanId))
  {
    // start/restart timer
    if(PopStartTimerEx(cGwTaskId, gMpGwScanForNodesTimerId_c, gMpGwScanForNodesTime_c) != gPopErrNone_c){
      fValidNwkData = false;
    }
  }
  else
  {
    // wrong param
    fValidNwkData = false;
  }

  if(!fValidNwkData){
    // wrong param send response directly
    MpGwScanForNodesRsp(false);
  }

  return fValidNwkData;
}
/*******************************************************************************
---------------- MpGwScanForNodesRsp --------------------------------------------
  Send MpGwScanForNodes message.
--------------------------------------------------------------------------------
*******************************************************************************/
void MpGwScanForNodesRsp(bool fValidNwkData)
{
  uint16_t iLen;
  uint8_t pPayload[gPopSerialTxBufferSize_c];
  sMpScanForNodesRsp_t *msg;
  uint16_t count = 0;

  // Get data and put to message payload
  msg = (sMpScanForNodesRsp_t*)pPayload;

  count = MpGwGetRadiusSortedScannedNodesList(msg->node);

  if(count)
  {
    msg->iChannel = gsMpScanForNodesReq.iChannel;
    msg->iPanId = gsMpScanForNodesReq.iPanId;
    SwapBytes16(&msg->iPanId);

    // loop through node list and add nodes to payload data
    for(int i = 0; i < count; i++)
    {
      // Node Id
      SwapBytes16(&msg->node[i].address);
      SwapBytes32(&msg->node[i].id);
    }

    // Length
    iLen = sizeof(popChannel_t) + sizeof(popPanId_t) +
      (sizeof(sMpScanForNodesNode_t) * count);

    // send data to gateway
    SEGGER_RTT_printf(0,
        "%d, Scan for nodes response, length: %d, count: %d\r\n",
        PopGetSystick(), iLen, count);
    PopGatewaySendEventToPC(gMpScanForNodesRsp_c, iLen, pPayload);
  }
  else {
    // Get data and put to message payload
    // Length
    iLen = sizeof(popChannel_t) + sizeof(popPanId_t);
    // Channel
    if(fValidNwkData){
      msg->iChannel = 0x00;
    }
    else{
      msg->iChannel = 0xFF;
    }
    // Pan Id
    msg->iPanId = 0xFFFF;
    SwapBytes16(&msg->iPanId);
    // send data to gateway
    PopGatewaySendEventToPC(gMpScanForNodesRsp_c, iLen, pPayload);
  }

  // clear NodeList
  giNodesInList = 0;
}

/**
 * Get a sorted scanned nodes list based on the received radius
 */
uint8_t MpGwGetRadiusSortedScannedNodesList(sMpScanForNodesNode_t* buf)
{
  uint8_t count = 0;
  int_fast8_t i;
  uint_fast8_t j;

  for (i = PopNwkGetDefaultRadius(); i >= 0; --i)
  {
    for (j = 0; j < MpGwGetScannedNodesCount(); ++j)
    {
      if (gsMpNodeList[j].radius == i)
      {
        buf[count].address = gsMpNodeList[j].iNwkAddr;
        buf[count].type = gsMpNodeList[j].type;
        buf[count].id = gsMpNodeList[j].id;
        count++;
      }
    }
  }
  return count;
}

/**
 * Get the amount of scanned nodes
 */
uint8_t MpGwGetScannedNodesCount(void)
{
  return giNodesInList;
}

/**
 * Set the radio TX power request
 */
void MpGwSetRadioPowerReq(popNwkTxPower_t power) {
  if ((power >= gPopNwkTxPower_Mininum_c) &&
      (power <= gPopNwkTxPower_Maximum_c)) {
    PopNvStore(gPopAppNvTxPowerLevel_c, 1, &power);
    PopNwkSetTransmitPower(power);
    MpGwSetRadioPowerRsp(true);
  } else {
    MpGwSetRadioPowerRsp(false);
  }
}

/**
 * Set the radio TX power response
 */
void MpGwSetRadioPowerRsp(const bool status) {
  MpSetRadioPowerRsp_t msg;

  msg.status = status ? 0 : 1;
  PopGatewaySendEventToPC(gMpSetRadioPowerRsp_c, 1, &msg);
}
