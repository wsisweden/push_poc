/***************************************************************************
 MpNwk.c

 This file includes functions for handling  resending and rerouting.
 of application messages

 Revision history:
 Rev   Date      Comment   Description
 this  100707              first revision
 ***************************************************************************/

#include "MpNwk.h"
#include "MpGw.h"
#include <Hw/Segger/RTT/SEGGER_RTT.h>
#include <Hw/TIDriverLib/source/systick.h>
#include <PopNet/PopNwk.h>
#include <stddef.h>

// Local timers
#define gMpNwkSendingTimerId_c                   gPopAppTimerFirstPos_c

// Local definitions
#define gMpAppLocalAckTimeout 1000
#define gMpAppRoutingPostTimeout 1000
#define gMpAppLocalRetries 1

typedef enum {
  eMpNwkStateIdle,
  eMpNwkStateSend,
  eMpNwkStateRouting,
  eMpNwkStateResend
} eMpNwkState_t;

// Function prototype
static void MpNwkHandleTimer(popTimerId_t iTimerId);
static void MpNwkSendMessage(sMessage_t *pMessage);
static void MpNwkGetSegment(popNwkAddr_t iDstAddr, uint8_t iSequence,
    uint8_t iControl);
static void MpNwkClearPopNet(void);
static void MpNwkForceRoute(void);
static void MpNwkSendNewRoute(void);
static void MpNwkDropMessage(void);

// Extern variables
extern const popTaskId_t cMpNwkTaskId;

static sMessage_t mpNwkSending;
static uint16_t mpNwkSegmentSequence;
static uint16_t mpNwkSegmentPosition;
static uint8_t mpNwkRetries;
static bool mpNwkRouteFound;
static eMpNwkState_t mpNwkState;

/*
 MpNwkTaskInit
 */
void MpNwkTaskInit(void) {
  mpNwkSegmentSequence = 0xff00;
  mpNwkSegmentPosition = 0;
  mpNwkRetries = 0;
  mpNwkRouteFound = false;
  mpNwkState = eMpNwkStateIdle;
}

/*
 MpNwkTaskEventLoop

 All events from PopNet to the application occur here. The iEvtId is a number
 from 0x00-0xff (not a bit mask). The contents of sEvtData depends on the iEvtId.
 See type sPopEvtData_t in PopNet.h.
 */
void MpNwkTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  switch (iEvtId) {
    case gPopEvtTimer_c:
      MpNwkHandleTimer(sEvtData.iTimerId);
      break;
    default:
      // Nothing to do
      break;
  }
}

/*******************************************************************************
 ----------------- MpNwkGetMsgType ----------------------------------------------
 Find a message depending on Cmd byte bit 5. Returns value 0 (request) or 1
 (response).
 -------------------------------------------------------------------------------
 *******************************************************************************/
uint8_t MpNwkGetMsgType(uint8_t aPayload[]) {
  // Mask out 0x10 from Cmd and shift it 4 positions. This results in
  // Rsp commands 0x10-1F, 0x30-3F, 0x50-5F, 0x70-7F and so on will return true
  uint8_t Cmd = aPayload[iCmdOffset_c];
  return ((Cmd & CmdTypeMask_c) >> 4);
}

/*******************************************************************************
 ---------------- MpNwkDataRequest ----------------------------------------------
 If it is a request message add the message to queue
 --------------------------------------------------------------------------------
 *******************************************************************************/
bool MpNwkDataRequest(sMessage_t *pMessage) {
  // Check for response message
  if (MpNwkGetMsgType(pMessage->aPayload) == RspMsg_c) {
    MpNwkSendMessage(pMessage);
  } else {
    mpNwkState = eMpNwkStateSend;
    mpNwkRouteFound = false;
    mpNwkRetries = gMpAppLocalRetries;

    PopMemCpy(&mpNwkSending, pMessage, sizeof(sMessage_t));
    MpNwkClearPopNet();
    MpNwkSendMessage(&mpNwkSending);
    PopStartTimerEx(cMpNwkTaskId, gMpNwkSendingTimerId_c, gMpAppLocalAckTimeout);
  }

  return true;
}

/*******************************************************************************
 ------------------ MpNwkDataIndication -----------------------------------------
 If it is a response message remove the corresponding request message
 --------------------------------------------------------------------------------
 *******************************************************************************/
void MpNwkDataIndication(sPopNwkDataIndication_t *pIndication,
    bool *pSendThruGateway) {
  sMessage_t DummyMessage;
  sMessage_t *pIncommingMessage = NULL;

  uint8_t *pPayload;
  uint8_t iPayloadLen;
  uint8_t MsgSeq;
  uint8_t MsgCtrl;
  uint8_t MsgCmd;

  pPayload = PopNwkPayload(pIndication);
  iPayloadLen = PopNwkPayloadLen(pIndication);
  pIncommingMessage = &DummyMessage;

  // Check if message is valid
  if (iPayloadLen > MAX_PAYLOAD) {
    // Just ignore the message if payload is longer than supported
    return;
  }

  // Copy first bytes of payload until aDataOffset_c
  PopMemCpy(pIncommingMessage->aPayload, pPayload, aDataOffset_c);

  MsgSeq = pPayload[iMpSeqOffset_c];
  MsgCtrl = pPayload[iCtrlOffset_c];
  MsgCmd = pPayload[iCmdOffset_c];

  SEGGER_RTT_printf(0, "%d, Data indication\r\n", PopGetSystick());
  SEGGER_RTT_printf(0, "%d,    Originating address: %d\r\n", PopGetSystick(),
      PopNwkSrcAddr(pIndication));
  SEGGER_RTT_printf(0, "%d,    Source address: %d\r\n", PopGetSystick(),
      PopNwkPrevHop(pIndication));
  SEGGER_RTT_printf(0, "%d,    From broadcast: %s\r\n", PopGetSystick(),
      PopNwkWasBroadcast(pIndication) ? "true" : "false");
  SEGGER_RTT_printf(0, "%d,    PopNet sequence: %d\r\n", PopGetSystick(),
      pIndication->sFrame.sNwk.iSequence);
  SEGGER_RTT_printf(0, "%d,    Access sequence: %d\r\n", PopGetSystick(),
      MsgSeq);
  SEGGER_RTT_printf(0, "%d,    First segment: %s\r\n", PopGetSystick(),
      ((MsgCtrl & FirstSegMask_c) == FirstSegMask_c) ? "true" : "false");
  SEGGER_RTT_printf(0, "%d,    Segment: %d\r\n", PopGetSystick(),
      MsgCtrl & ~FirstSegMask_c);
  SEGGER_RTT_printf(0, "%d,    Command: 0x%02x\r\n", PopGetSystick(), MsgCmd);
  SEGGER_RTT_printf(0, "%d,    Length: %d\r\n", PopGetSystick(), iPayloadLen);
  SEGGER_RTT_printf(0, "\r\n");

  // Check if segmented response
  if (MsgCtrl != SingleMsg_c) {
    popNwkAddr_t iDstAddr = PopNwkSrcAddr(pIndication);

    SEGGER_RTT_printf(0, "%d, GotSegment\r\n", PopGetSystick());
    SEGGER_RTT_printf(0, "%d,    First segment: %d\r\n", PopGetSystick(),
                      (MsgCtrl & FirstSegMask_c) == FirstSegMask_c);
    SEGGER_RTT_printf(0, "%d,    Segment: %d\r\n", PopGetSystick(),
                      MsgCtrl & ~FirstSegMask_c);
    SEGGER_RTT_printf(0, "\r\n");

    // Check if acknowledge for current message
    if (MsgCtrl == LastSeg_c) {
      if (mpNwkSending.aPayload[0] == gGetSegment_c) {
        MpNwkDropMessage();
      }
      return;
    }

    // Check for new segmented packet
    if ((MsgCtrl & FirstSegMask_c) == FirstSegMask_c) {
      if (mpNwkSegmentSequence != MsgSeq) {
        mpNwkSegmentSequence = MsgSeq;
      }
    } else if (mpNwkSegmentSequence != MsgSeq || mpNwkSegmentPosition != MsgCtrl) {
      // Check for next segment
      SEGGER_RTT_printf(0,
                        "%d, Dropping old packet, Received segment: %d Requested: %d\r\n",
                        PopGetSystick(), MsgCtrl, mpNwkSegmentPosition);
      return;
    }

    // Send get segment request back to node
    MsgCtrl &= ~FirstSegMask_c;
    MsgCtrl--;
    mpNwkSegmentPosition = MsgCtrl;

    SEGGER_RTT_printf(0, "%d, GetSegment\r\n", PopGetSystick());
    SEGGER_RTT_printf(0, "%d,    Node: %d\r\n", PopGetSystick(), iDstAddr);
    SEGGER_RTT_printf(0, "%d,    Sequence: %d\r\n", PopGetSystick(), MsgSeq);
    SEGGER_RTT_printf(0, "%d,    First segment: %d\r\n", PopGetSystick(),
                      (MsgCtrl & FirstSegMask_c) == FirstSegMask_c);
    SEGGER_RTT_printf(0, "%d,    Segment: %d\r\n", PopGetSystick(),
                      MsgCtrl & ~FirstSegMask_c);
    SEGGER_RTT_printf(0, "%d,    Command: 0x%02x\r\n", PopGetSystick(),
                      MsgCmd);
    SEGGER_RTT_printf(0, "\r\n");

    MpNwkGetSegment(iDstAddr, MsgSeq, MsgCtrl);
  } else {
    // Got message from requested node?
    if (mpNwkSending.iDstAddr == PopNwkSrcAddr(pIndication)) {
      MpNwkDropMessage();
    }
  }
}

/*---------------- Handle data confirm event ---------------------------------*/
/* If no error, remove message from sending queue                             */
/* If request message move message to "waiting for response" queue instead    */
/* Else free memory                                                           */
/* If error routing, wait until a successful data request has been received   */
/*----------------------------------------------------------------------------*/
void MpNwkDataConfirm(popErr_t iDataConfirm) {
  SEGGER_RTT_printf(0, "%d, Data confirm: 0x%02x\r\n", PopGetSystick(), iDataConfirm);
  SEGGER_RTT_printf(0, "\r\n");

  if ((mpNwkState == eMpNwkStateSend) && (iDataConfirm == gPopErrRouteNotFound_c)) {
    MpNwkForceRoute();
  } else {
    // Tell PC about the status
    MpGwNwkDataRsp(iDataConfirm);
  }
}

/*******************************************************************************
 ---------------- MpNwkInitiateRouteHandling----------------------------------------
 Initiate Route  event handling
 --------------------------------------------------------------------------------
 *******************************************************************************/
void MpNwkInitiateRouteHandling(void) {
  SEGGER_RTT_printf(0, "%d, Initiate routing\r\n", PopGetSystick());
  SEGGER_RTT_printf(0, "\r\n");
}

/*******************************************************************************
 ---------------- MpNwkEndRouteHandling-----------------------------------------
 Reset gfWaitForNewRoute flag and check if waiting messages to send
 --------------------------------------------------------------------------------
 *******************************************************************************/
void MpNwkEndRouteHandling(const uint16_t address) {
  SEGGER_RTT_printf(0, "%d, End routing, Address: %d\r\n", PopGetSystick(), address);

  // Check is routing was successful
  if (address != gPopNwkInvalidAddr_c) {
    if (!mpNwkRouteFound) {
      mpNwkRouteFound = true;
      PopStartTimerEx(cMpNwkTaskId, gMpNwkSendingTimerId_c, gMpAppRoutingPostTimeout);
    }

    SEGGER_RTT_printf(0, "%d,    Routing success\r\n", PopGetSystick());
  } else {
    SEGGER_RTT_printf(0, "%d,    Routing failed\r\n", PopGetSystick());
  }

  SEGGER_RTT_printf(0, "\r\n");
}

/*******************************************************************************
 ---------------- MpNwkMemInit -----------------------------------------------
 Initialize the memory
 --------------------------------------------------------------------------------
 *******************************************************************************/
void MpNwkMemInit(void) {
  popTimerId_t i;

  // Stop all MpMsgTimers
  for (i = gMpMsgTimerFirstPos_c; i < (gMpMsgTimerFirstPos_c + MAX_MSG); i++) {
    PopStopTimerEx(cMpNwkTaskId, i);
  }

  mpNwkState = eMpNwkStateIdle;
}

/*******************************************************************************
 ---------------- MpNwkGetSegment -----------------------------------------------
 Send Get segment request
 --------------------------------------------------------------------------------
 *******************************************************************************/
static void MpNwkGetSegment(popNwkAddr_t iDstAddr, uint8_t iSequence,
    uint8_t iControl) {
  sMpMsgHeader_t sMpMsgHeaderReq;
  sMpMsgGetSegment_t sMpMsgGetSegment;
  sMessage_t sMessage;
  uint8_t *pData;

  sMpMsgHeaderReq.iCmd = gGetSegment_c;
  sMpMsgHeaderReq.iMpSequence = iSequence;
  sMpMsgHeaderReq.iControl = SingleMsg_c;

  // add JoinIndication data
  sMpMsgGetSegment.iCtrl = iControl;

  // add network data
  sMessage.iDstAddr = iDstAddr;
  sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c;
  sMessage.iRadius = PopNwkGetDefaultRadius();
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t) + sizeof(sMpMsgGetSegment_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderReq, sizeof(sMpMsgHeader_t));
  PopMemCpy((pData + (uint8_t) sizeof(sMpMsgHeader_t)), &sMpMsgGetSegment,
      sizeof(sMpMsgGetSegment_t));

  // send the message
  MpNwkDataRequest(&sMessage);
}

/*
 MpNwkHandleTimer
 A network timer has expired. Handle it.
 */
static void MpNwkHandleTimer(popTimerId_t iTimerId) {
  switch (iTimerId) {
    case gMpNwkSendingTimerId_c:
      SEGGER_RTT_printf(0, "%d, Sending timeout\r\n", PopGetSystick());

      switch (mpNwkState) {
        case eMpNwkStateIdle:
          // Nothing to do
          break;
        case eMpNwkStateSend:
          if ((mpNwkSending.iOptions & gPopNwkDataReqOptsNoRetry_c) !=
              gPopNwkDataReqOptsNoRetry_c) {
            MpNwkForceRoute();
          }
          break;
        case eMpNwkStateRouting:
          if (mpNwkRouteFound) {
            MpNwkSendNewRoute();
          } else {
            MpNwkDropMessage();
          }
          break;
        case eMpNwkStateResend:
          if (mpNwkRetries > 0) {
            mpNwkRetries--;
            MpNwkSendMessage(&mpNwkSending);
          } else {
            MpNwkDropMessage();
          }
          break;
        default:
          // Should not happen
          MpNwkTaskInit();
          break;
      }

      SEGGER_RTT_printf(0, "\r\n");
      break;
    default:
      // Nothing to do
      break;
  }
}

/*******************************************************************************
 --------------- MpNwkSendMessage -----------------------------------------------
 Fetch data from MsgMem and modify it so it can be sent as a DataRequest
 --------------------------------------------------------------------------------
 *******************************************************************************/
static void MpNwkSendMessage(sMessage_t *pMessage) {
  sPopNwkDataRequest_t dataReq;

  dataReq.iDstAddr = pMessage->iDstAddr;
  dataReq.iOptions = pMessage->iOptions;
  dataReq.iRadius = pMessage->iRadius;
  dataReq.iPayloadLength = pMessage->iPayloadLength;
  dataReq.pPayload = pMessage->aPayload;

  // Log information
  SEGGER_RTT_printf(0, "%d, Send message\r\n", PopGetSystick());
  SEGGER_RTT_printf(0, "%d,    Destination address: %d\r\n", PopGetSystick(),
      dataReq.iDstAddr);
  SEGGER_RTT_printf(0, "%d,    Next hop: %d\r\n", PopGetSystick(),
      PopNwkNextHop(dataReq.iDstAddr));
  SEGGER_RTT_printf(0, "%d,    Options: 0x%02x\r\n", PopGetSystick(),
      dataReq.iOptions);
  SEGGER_RTT_printf(0, "%d,    Radius: %d\r\n", PopGetSystick(),
      dataReq.iRadius);
  SEGGER_RTT_printf(0, "%d,    PopNet sequence: %d\r\n", PopGetSystick(),
      giPopNwkSequence);
  SEGGER_RTT_printf(0, "%d,    Access sequence: %d\r\n", PopGetSystick(),
      pMessage->aPayload[iMpSeqOffset_c]);
  SEGGER_RTT_printf(0, "%d,    Command: 0x%02x\r\n", PopGetSystick(),
      pMessage->aPayload[iCmdOffset_c]);
  SEGGER_RTT_printf(0, "\r\n");

  PopNwkDataRequest(&dataReq);
}

/*******************************************************************************
 ----------------- MpNwkClearPopNet ----------------------------------------------
 Clean all PopNet tables
 -------------------------------------------------------------------------------
 *******************************************************************************/
static void MpNwkClearPopNet(void) {
  // Clear retry table
  PopNwkFreeEntireRetryTable();

  // Clear duplicated table
  PopNwkFreeEntireDuplicateTable();

  // Clear both Route Discovery and Route Discovery Timer Id's Table
  PopNwkFreeEntireRouteDiscoveryTable();
}

/**
 * Force route finding
 */
static void MpNwkForceRoute(void) {
  SEGGER_RTT_printf(0, "%d, Resend wih force route\r\n", PopGetSystick());
  SEGGER_RTT_printf(0, "\r\n");

  mpNwkState = eMpNwkStateRouting;
  mpNwkSending.iOptions = gPopNwkDataReqOptsForce_c;
  MpNwkSendMessage(&mpNwkSending);
}

/**
 * Send new route
 */
static void MpNwkSendNewRoute(void) {
  SEGGER_RTT_printf(0, "%d, Resend wih new route\r\n", PopGetSystick());
  SEGGER_RTT_printf(0, "\r\n");

  mpNwkState = eMpNwkStateResend;
  mpNwkRetries = gMpAppLocalRetries;
  mpNwkSending.iOptions = gPopNwkDataReqOptsDefault_c;
  MpNwkSendMessage(&mpNwkSending);
}

/**
 * Drop message
 */
static void MpNwkDropMessage(void) {
  SEGGER_RTT_printf(0, "%d, Dropping message\r\n", PopGetSystick());
  SEGGER_RTT_printf(0, "\r\n");

  mpNwkState = eMpNwkStateIdle;
  PopStopTimerEx(cMpNwkTaskId, gMpNwkSendingTimerId_c);
}
