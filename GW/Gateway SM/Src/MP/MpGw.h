/***************************************************************************
  MpGw.h

  This file defines the compile-options for the MpGw.c source code.

  Revision history:
  Rev   Date      Comment   Description
  this  100707              first revision
***************************************************************************/

#ifndef MPGW_H_
#define MPGW_H_

#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

#define REQ 0
#define RSP 1
#define NORMAL 2
#define SCAN 3

#define INIT          0x0001
#define NOT_CONNECTED 0x0002
#define START         0x0004
#define JOIN          0x0008
#define CONNECTED     0x0010

#define NODELIST_SIZE 10
#define MAXNWK  20

/* Heartbeat status bit decsription */
#define STATUS_BBC  (1<<0)                                                      // BBC function active

/* Events */
#define gMpOtaSwitch_c                    0x6f
#define gMpOtaStartResp_c                 0x70
#define gMpOtaBlockResp_c                 0x71
#define gMpOtaSwitchResp_c                0x72
#define gMpOtaBlockIndication_c           0x73
#define gMpOtaSwitchIndication_c          0x74
#define gMpOtaAbortResp_c                 0x75
#define gMpOtaAbort_c                     0xb0
#define gMpOtaStart_c                     0xb1
#define gMpOtaBlock_c                     0xb2

// 0x80-0xFF open application specific events
#define gMpStartNetworkReq_c              0x80
#define gMpStartNetworkRsp_c              0x80
#define gMpJoinNetworkReq_c               0x81
#define gMpJoinNetworkRsp_c               0x81
#define gMpLeaveNetworkReq_c              0x82
#define gMpLeaveNetworkRsp_c              0x82
#define gMpScanNetworkReq_c               0x83
#define gMpScanNetworkRsp_c               0x83
#define gMpSetRadioParamReq_c             0x84
#define gMpSetRadioParamRsp_c             0x84
#define gMpGetRadioDataReq_c              0x85
#define gMpGetRadioDataRsp_c              0x85
#define gMpJoinEnableReq_c                0x86
#define gMpJoinEnableRsp_c                0x86
#define gMpNwkDataReq_c                   0x87
#define gMpNwkDataRsp_c                   0x87
#define gMpDataIndication_c               0x88
#define gMpNodePlacementReq_c             0x89
#define gMpNodePlacementRsp_c             0x89
#define gMpSetUserLevelReq_c              0x8A
#define gMpSetUserLevelRsp_c              0x8A
#define gMpGetUserLevelReq_c              0x8B
#define gMpGetUserLevelRsp_c              0x8B
#define gMpScanForNodesReq_c              0x8C
#define gMpScanForNodesRsp_c              0x8C
#define gMpHeartbeatReq_c                 0x8D
#define gMpHeartbeatRsp_c                 0x8D
#define gMpRoutingReq_c                   0x8E
#define gMpRoutingRsp_c                   0x8E
#define gMpSetRadioPowerReq_c             0x8F
#define gMpSetRadioPowerRsp_c             0x8F

#define gMpMessageTableOTAReq_c           0xFB
#define gMpMessageTableOTARsp_c           0xFB
#define gMpDebugOTAReq_c                  0xFC
#define gMpDebugOTARsp_c                  0xFC
#define gMpAssertOTAReq_c                 0xFD
#define gMpAssertOTARsp_c                 0xFD
#define gMpAssertOTSReq_c                 0xFE
#define gMpAssertOTSRsp_c                 0xFE
#define gMpTestOTSReq_c                   0xFF
#define gMpTestOTSRsp_c                   0xFF

//internal events
#define gMpGwSendPacket_c                 0x90

// Gateway messages structures
typedef struct sMpStartNetworkReq_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
}sMpStartNetworkReq_t;

enum eMpStartNetworkReq
{
//  START_NWK_REQ_EVTID,
//  START_NWK_REQ_LEN,
  START_NWK_REQ_CHANNEL,
  START_NWK_REQ_PANID_H,
  START_NWK_REQ_PANID_L,
  START_NWK_REQ_NODEID_H,
  START_NWK_REQ_NODEID_L,
  START_NWK_REQ_SIZE
};

typedef struct sMpStartNetworkRsp_tag
{
  uint8_t iStatus;
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
}sMpStartNetworkRsp_t;

enum eMpStartNetworkRsp
{
  START_NWK_RSP_EVTID,
  START_NWK_RSP_LEN,
  START_NWK_RSP_STATUS,
  START_NWK_RSP_CHANNEL,
  START_NWK_RSP_PANID_H,
  START_NWK_RSP_PANID_L,
  START_NWK_RSP_NODEID_H,
  START_NWK_RSP_NODEID_L,
  START_NWK_RSP_SIZE
};

#pragma pack(1)
typedef struct sMpJoinEnableReq_tag
{
  bool fEnable;
  bool fNetworkWide;
}sMpJoinEnableReq_t;
#pragma pack()

enum eMpJoinEnableReq
{
//  JOIN_ENABLE_REQ_EVTID,
//  JOIN_ENABLE_REQ_LEN,
  JOIN_ENABLE_REQ_ENABLE,
  JOIN_ENABLE_REQ_NETWORKWIDE,
  JOIN_ENABLE_REQ_SIZE
};

#pragma pack(1)
typedef struct sMpJoinEnableRsp_tag
{
  bool fStatus;
}sMpJoinEnableRsp_t;
#pragma pack()

enum eMpJoinEnableRsp
{
  JOIN_ENABLE_RSP_EVTID,
  JOIN_ENABLE_RSP_LEN,
  JOIN_ENABLE_RSP_ENABLE,
  JOIN_ENABLE_RSP_SIZE
};

typedef struct sMpJoinNetworkReq_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
}sMpJoinNetworkReq_t;

enum eMpJoinNetworkReq
{
//  JOIN_NWK_REQ_EVTID,
//  JOIN_NWK_REQ_LEN,
  JOIN_NWK_REQ_CHANNEL,
  JOIN_NWK_REQ_PANID_H,
  JOIN_NWK_REQ_PANID_L,
  JOIN_NWK_REQ_SIZE
};

typedef struct sMpJoinNetworkRsp_tag
{
  uint8_t iStatus;
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
}sMpJoinNetworkRsp_t;

enum eMpJoinNetworkRsp
{
  JOIN_NWK_RSP_EVTID,
  JOIN_NWK_RSP_LEN,
  JOIN_NWK_RSP_STATUS,
  JOIN_NWK_RSP_CHANNEL,
  JOIN_NWK_RSP_PANID_H,
  JOIN_NWK_RSP_PANID_L,
  JOIN_NWK_RSP_NODEID_H,
  JOIN_NWK_RSP_NODEID_L,
  JOIN_NWK_RSP_SIZE
};

/*
typedef struct sMpLeaveNetworkReq_tag
{
}sMpLeaveNetworkReq_t;
*/
typedef struct sMpLeaveNetworkRsp_tag
{
  uint8_t iStatus;
}sMpLeaveNetworkRsp_t;

enum eMpLeaveNetworkRsp
{
  LEAVE_NWK_RSP_EVTID,
  LEAVE_NWK_RSP_LEN,
  LEAVE_NWK_RSP_STATUS,
  LEAVE_NWK_RSP_SIZE
};

typedef struct sMpScanNetworkReq_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
}sMpScanNetworkReq_t;

enum eMpScanNetworkReq
{
//  SCAN_NWK_REQ_EVTID,
//  SCAN_NWK_REQ_LEN,
  SCAN_NWK_REQ_CHANNEL,
  SCAN_NWK_REQ_PANID_H,
  SCAN_NWK_REQ_PANID_L,
  SCAN_NWK_REQ_SIZE
};

typedef struct sMpScanNetworkRsp_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
}sMpScanNetworkRsp_t;

enum eMpScanNetworkRsp
{
  SCAN_NWK_RSP_EVTID,
  SCAN_NWK_RSP_LEN,
  SCAN_NWK_RSP_CHANNEL,
  SCAN_NWK_RSP_PANID_H,
  SCAN_NWK_RSP_PANID_L,
  SCAN_NWK_RSP_NODEID_H,
  SCAN_NWK_RSP_NODEID_L,
  SCAN_NWK_RSP_SIZE
};

typedef struct sMpSetRadioParamReq_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
}sMpSetRadioParamReq_t;

enum MpSetRadioParamReq
{
//  SET_RADIO_PARAM_REQ_EVTID,
//  SET_RADIO_PARAM_REQ_LEN,
  SET_RADIO_PARAM_REQ_CHANNEL,
  SET_RADIO_PARAM_REQ_PANID_H,
  SET_RADIO_PARAM_REQ_PANID_L,
  SET_RADIO_PARAM_REQ_NODEID_H,
  SET_RADIO_PARAM_REQ_NODEID_L,
  SET_RADIO_PARAM_REQ_SIZE
};

typedef struct sMpSetRadioParamRsp_tag
{
  uint8_t iStatus;
}sMpSetRadioParamRsp_t;

enum eMpSetRadioParamRsp
{
  SET_RADIO_PARAM_RSP_EVTID,
  SET_RADIO_PARAM_RSP_LEN,
  SET_RADIO_PARAM_RSP_STATUS,
  SET_RADIO_PARAM_RSP_SIZE
};

/*
typedef struct sMpGetRadioDataReq_tag
{
}sMpGetRadioParamReq_t;
*/
typedef struct sMpGetRadioDataRsp_tag
{
  uint32_t iFwType;
  uint32_t iFwVer;
  uint32_t iChkSum;
  uint8_t aCmId[8];
  uint16_t iMfgId;
  uint16_t iAppId;
  popChannel_t iChannel;
  popPanId_t iPanId;
  popNwkAddr_t iNwkAddr;
  uint16_t  AddrPool;
}sMpGetRadioDataRsp_t;

enum eMpGetRadioDataRsp
{
  RADIO_DATA_RSP_EVTID,
  RADIO_DATA_RSP_LEN,
  RADIO_DATA_RSP_FWTYPE_H,
  RADIO_DATA_RSP_FWTYPE_MH,
  RADIO_DATA_RSP_FWTYPE_ML,
  RADIO_DATA_RSP_FWTYPE_L,
  RADIO_DATA_RSP_FWVER_H,
  RADIO_DATA_RSP_FWVER_MH,
  RADIO_DATA_RSP_FWVER_ML,
  RADIO_DATA_RSP_FWVER_L,
  RADIO_DATA_RSP_CHKSUM_H,
  RADIO_DATA_RSP_CHKSUM_MH,
  RADIO_DATA_RSP_CHKSUM_ML,
  RADIO_DATA_RSP_CHKSUM_L,
  RADIO_DATA_RSP_CMID_0,
  RADIO_DATA_RSP_CMID_1,
  RADIO_DATA_RSP_CMID_2,
  RADIO_DATA_RSP_CMID_3,
  RADIO_DATA_RSP_CMID_4,
  RADIO_DATA_RSP_CMID_5,
  RADIO_DATA_RSP_CMID_6,
  RADIO_DATA_RSP_CMID_7,
  RADIO_DATA_RSP_MFGID_H,
  RADIO_DATA_RSP_MFGID_L,
  RADIO_DATA_RSP_APPID_H,
  RADIO_DATA_RSP_APPID_L,
  RADIO_DATA_RSP_CHANNEL,
  RADIO_DATA_RSP_PANID_H,
  RADIO_DATA_RSP_PANID_L,
  RADIO_DATA_RSP_NODEID_H,
  RADIO_DATA_RSP_NODEID_L,
  RADIO_DATA_RSP_ADDRPOOL_H,
  RADIO_DATA_RSP_ADDRPOOL_L,
  RADIO_DATA_RSP_SIZE
};

typedef struct sMpNwkDataReq_tag
{
  popNwkAddr_t iDstAddr;
  uint8_t iPayloadLength;
  void *pPayload;
}sMpNwkDataReq_t;

enum eMpNwkDataReq
{
//  DATA_REQ_EVTID,
//  DATA_REQ_LEN,
  DATA_REQ_DSTADDR_H,
  DATA_REQ_DSTADDR_L,
  DATA_REQ_PAYLOADLEN,
  DATA_REQ_PAYLOAD,
  DATA_REQ_SIZE
};

enum eMpAccessFrame
{
  MP_ACCESS_COMMAND,
  MP_ACCESS_SEQUENCE,
  MP_ACCESS_SEGMENT,
  MP_ACCESS_DATA
};

typedef struct sMpNwkDataRsp_tag
{
  uint8_t iStatus;
}sMpNwkDataRsp_t;

enum eMpNwkDataRsp
{
  DATA_RSP_EVTID,
  DATA_RSP_LEN,
  DATA_RSP_STATUS,
  DATA_RSP_SIZE
};

enum eMpOtaStart
{
  MP_OTA_START_PLATFORM,
  MP_OTA_START_IMAGE_SIZE_H,
  MP_OTA_START_IMAGE_SIZE_HM,
  MP_OTA_START_IMAGE_SIZE_LM,
  MP_OTA_START_IMAGE_SIZE_L,
  MP_OTA_START_APPLICATION_ID_H,
  MP_OTA_START_APPLICATION_ID_L,
  MP_OTA_START_HW_ID,
  MP_OTA_START_PERSERVE_NVM,
  MP_OTA_START_LOCAL_GATEWAY,
  MP_OTA_START_FIRMWARE_PART_H,
  MP_OTA_START_FIRMWARE_PART_HM,
  MP_OTA_START_FIRMWARE_PART_LM,
  MP_OTA_START_FIRMWARE_PART_L,
  MP_OTA_START_FIRMWARE_REVISION_H,
  MP_OTA_START_FIRMWARE_REVISION_HM,
  MP_OTA_START_FIRMWARE_REVISION_LM,
  MP_OTA_START_FIRMWARE_REVISION_L
};

enum eMpOtaBlock
{
  MP_OTA_BLOCK_NUMBER_H,
  MP_OTA_BLOCK_NUMBER_L,
  MP_OTA_BLOCK_CHECKSUM,
  MP_OTA_BLOCK_DATA,
  MP_OTA_BLOCK_SIZE
};

enum eMpOtaSwitch
{
  MP_OTA_SWITCH_TARGET_NODE_ID_H,
  MP_OTA_SWITCH_TARGET_NODE_ID_L,
  MP_OTA_SWITCH_SIZE
};

enum eMpOtaStartResp
{
  MP_OTA_START_RESP_BLOCK_SIZE_H,
  MP_OTA_START_RESP_BLOCK_SIZE_L,
  MP_OTA_START_RESP_ERROR,
  MP_OTA_START_RESP_SIZE
};

enum eMpOtaBlockResp
{
  MP_OTA_BLOCK_RESP_NUMBER_H,
  MP_OTA_BLOCK_RESP_NUMBER_L,
  MP_OTA_BLOCK_RESP_ERROR,
  MP_OTA_BLOCK_RESP_SIZE
};

enum eMpOtaSwitchResp
{
  MP_OTA_SWITCH_RESP_ERROR,
  MP_OTA_SWITCH_RESP_SIZE
};

enum eMpOtaBlockIndication
{
  MP_OTA_BLOCK_INDICATION_NUMBER_H,
  MP_OTA_BLOCK_INDICATION_NUMBER_L,
  MP_OTA_BLOCK_INDICATION_NUMBER_TOTAL_H,
  MP_OTA_BLOCK_INDICATION_NUMBER_TOTAL_L,
  MP_OTA_BLOCK_INDICATION_ERROR,
  MP_OTA_BLOCK_INDICATION_SIZE
};

enum eMpOtaSwitchIndication
{
  MP_OTA_SWITCH_INDICATION_ERROR,
  MP_OTA_SWITCH_INDICATION_SIZE
};

enum eMpOtaAbort
{
  MP_OTA_ABORT_SIZE
};

enum eMpOtaAbortResp
{
  MP_OTA_ABORT_RESP_SIZE
};

typedef struct sMpDataIndication_tag
{
  popNwkAddr_t iSrcAddr;
  uint8_t iPayloadLength;
  void *pPayload;
}sMpDataIndication_t;

enum eMpDataIndication
{
  DATA_IND_EVTID,
  DATA_IND_LEN,
  DATA_IND_SRCADDR_H,
  DATA_IND_SRCADDR_L,
  DATA_IND_PAYLOADLEN,
  DATA_IND_PAYLOAD,
  DATA_IND_SIZE
};

typedef struct sMpNodePlacementRsp_tag
{
  uint8_t iStatus;
}sMpNodePlacementRsp_t;

enum eMpNodePlacementRsp
{
  NODE_PLACEMENT_RSP_EVTID,
  NODE_PLACEMENT_RSP_LEN,
  NODE_PLACEMENT_RSP_STATUS,
  NODE_PLACEMENT_RSP_SIZE
};

typedef struct sMpSetUserLevelReq_tag
{
  uint8_t iUserLevel;
}sMpSetUserLevelReq_t;

enum eMpSetUserLevelReq
{
//  USERLEVEL_RSP_EVTID,
//  USERLEVEL_RSP_LEN,
  SET_USERLEVEL_REQ_USERLEVEL,
  SET_USERLEVEL_REQ_SIZE
};

typedef struct sMpSetUserLevelRsp_tag
{
  uint8_t iStatus;
}sMpSetUserLevelRsp_t;

enum eMpSetUserLevelRsp
{
  SET_USERLEVEL_RSP_EVTID,
  SET_USERLEVEL_RSP_LEN,
  SET_USERLEVEL_RSP_STATUS,
  SET_USERLEVEL_RSP_SIZE
};

typedef struct sMpGetUserLevelRsp_tag
{
  uint8_t iUserLevel;
}sMpGetUserLevelRsp_t;

enum eMpGetUserLevelRsp
{
  GET_USERLEVEL_RSP_EVTID,
  GET_USERLEVEL_RSP_LEN,
  GET_USERLEVEL_RSP_USERLEVEL,
  GET_USERLEVEL_RSP_SIZE
};

typedef struct sMpScanForNodesReq_tag
{
  popChannel_t iChannel;
  popPanId_t iPanId;
}sMpScanForNodesReq_t;

enum eMpScanForNodesReq
{
//  SCAN_NWK_REQ_EVTID,
//  SCAN_NWK_REQ_LEN,
  SCAN_NODE_REQ_CHANNEL,
  SCAN_NODE_REQ_PANID_H,
  SCAN_NODE_REQ_PANID_L,
  SCAN_NODE_REQ_SIZE
};

#pragma pack(1)
typedef struct {
  popNwkAddr_t address;
  uint8_t type;
  uint32_t id;
} sMpScanForNodesNode_t;
#pragma pack()

#pragma pack(1)
typedef struct {
  popChannel_t iChannel;
  popPanId_t iPanId;
  sMpScanForNodesNode_t node[NODELIST_SIZE];
} sMpScanForNodesRsp_t;
#pragma pack()

typedef struct sMpHeartbeatRsp_tag
{
  uint8_t iStatus;
}sMpHeartbeatRsp_t;

enum eMpHeartbeatRsp
{
  HEARTBEAT_RSP_EVTID,
  HEARTBEAT_RSP_LEN,
  HEARTBEAT_RSP_STATUS,
  HEARTBEAT_RSP_SIZE
};

typedef struct sMpRoutingRsp_tag
{
  uint8_t iEnable;
}sMpRoutingRsp_t;

enum eMpRoutingRsp
{
  ROUTING_RSP_EVTID,
  ROUTING_RSP_LEN,
  ROUTING_RSP_ENABLE,
  ROUTING_RSP_SIZE
};

typedef struct sMpNodeList_tag
{
  uint8_t radius;
  popNwkAddr_t iNwkAddr;
  uint8_t type;
  uint32_t id;
}sMpNodeList_t;

enum eNodeList
{
  NODELIST_NODEID_H,
  NODELIST_NODEID_L
};

// Set radio power level request
enum
{
  SET_RADIO_POWER_LEVEL,
  SET_RADIO_POWER_SIZE
};

// Set radio power level response
typedef struct
{
  uint8_t status; // 0 = OK, 1 = Fail
} MpSetRadioPowerRsp_t;

enum eBeaconList
{
  BEACONLIST_EVTID,
  BEACONLIST_LEN,
  BEACONLIST_CHANNEL,
  BEACONLIST_PANID_H,
  BEACONLIST_PANID_L,
  BEACONLIST_NODEID_H,
  BEACONLIST_NODEID_L
};

enum eMpGwSendPacket
{
  GW_PACKET_EVTID,
  GW_PACKET_LEN,
  GW_PACKET_PAYLOAD
};

extern bool gfMpIsProcessingScanConfirm;
extern uint32_t giFwType; // declared in MpApp
extern uint32_t giFwVer; // declared in MpApp
extern uint32_t giChkSum;  // declared in MpApp
extern bool gfMpGwIsProcessingBeacon_c;

extern sMpJoinNetworkReq_t gsPopAppJoinReqNwkInfo; // global struct for saving nwk info from join request
/* Node Placemente Global Variables*/
extern bool gfMpPlacementStart;

// Gateway functions
void MpGwTaskInit(void);
void MpGwTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
void MpGwNodePlaceProcessBeacon(sPopNwkBeaconIndication_t *pNwkBeaconIndication);
void MpGwStartNetworkReq(sPopEvtData_t *pData);
void MpGwStartNetworkRsp(popStatus_t iNwkStartConfirm);
void MpGwJoinEnableReq(uint16_t iWord);
void MpGwJoinEnableRsp(void);
void MpGwJoinNetworkReq(uint8_t *pPayload);
void MpGwJoinNetworkRsp(popStatus_t iNwkStartConfirm);
void MpGwLeaveNetworkReq(void);
void MpGwLeaveNetworkRsp(void);
void MpGwScanNetworkHandling(sPopNwkBeaconIndication_t *pNwkBeaconIndication);
void MpGwScanNetworkReq(uint8_t *pPayload);
void MpGwScanNetworkRsp(void);
void MpGwSetRadioParamReq(sPopEvtData_t *pData);
void MpGwSetRadioParamRsp(uint8_t iStatus);
void MpGwGetRadioDataReq(void);
void MpGwGetRadioDataRsp(void);
void MpGwNwkDataReq(sPopEvtData_t *pData);
void MpGwNwkDataRsp(uint8_t iStatus);
void MpGwDataIndication(sPopNwkDataIndication_t *pIndication);
void MpGwNodePlacementReq(void);
void MpGwNodePlacementRsp(sPopNodePlacement_t *pPopNodePlacementResult);
uint8_t MpGwSetUserLevelReq(uint8_t *pByte);
void MpGwSetUserLevelRsp(uint8_t iStatus);
void MpGwGetUserLevelRsp(uint8_t iUserLevel);
void MpGwScanForNodesHandling(sPopNwkDataIndication_t *pDataIndication,
                              const uint8_t length);
bool MpGwScanForNodesReq(uint8_t *pPayload);
void MpGwScanForNodesRsp(bool fValidNwkData);
uint8_t MpGwGetRadiusSortedScannedNodesList(sMpScanForNodesNode_t* buf);
uint8_t MpGwGetScannedNodesCount(void);
void MpGwSetRadioPowerReq(popNwkTxPower_t power);
void MpGwSetRadioPowerRsp(const bool status);

#endif
