#pragma once

#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

// Public functions

void MessageTaskInit(void);
void MessageTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
uint8_t MessageGetUserLevel(void);
uint8_t MessageStepMpSequence(void);
