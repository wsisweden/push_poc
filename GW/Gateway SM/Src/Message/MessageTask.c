/*
 * MessageTask.c
 *
 *  Created on: 24 apr. 2017
 *      Author: Andreas Carmvall
 */
#include "MessageTask.h"
#include "../Tui/Tui.h"
#include "../Ota/OtaTask.h"
#include "../MP/MpGw.h"
#include "../MP/MpNwk.h"
#include "../Popnet_AppSpecific/PopAppGwSm.h"
#include <PopNet/PopSerial.h>
#include <PopNet/PopNwk.h>

// Private attributes

extern bool gfMpBeaconFound;
static uint8_t messageUserLevel;
static uint8_t messageMpSequence;

#if DEBUG >= 2
extern sMpDebugCount_t gsMpDebug;
#endif

// Private function declarations

extern void PopAppSetNewState(popStatus_t iNwkStartConfirm);
static void MessageTaskHandleReceived(sPopEvtData_t data);
static void PopAppGetAcknowledge(void);

// Private definitions

#define gMpSmUserLevelMin_c              1
#define gMpSmUserLevelMax_c              6

// Public functions

/**
 * Initialize the task
 */
void MessageTaskInit(void) {
  messageUserLevel = 6;
  messageMpSequence = 0;
  PopNvRetrieve(gPopAppUserLevelNvId_c, &messageUserLevel);
}

/**
 * Handle the task event loop
 * @param iEvtId The event id
 * @param sEvtData The event data
 */
void MessageTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  switch(iEvtId) {
    case gPopMessageReceived_c:
      MessageTaskHandleReceived(sEvtData);
      TuiRadioStatus(eTuiBlink);
      break;
    case gPopMessageSent_c:
      TuiRadioStatus(eTuiBlink);
      break;
    case gPopMessageRadioReceived_c:
    {
      sPopNwkMgmtCmd_t* data = (sPopNwkMgmtCmd_t*)sEvtData.pData;
      if (data->iCommandId == gPopGwNwkMgmtOtaUpgradeSaveRsp_c) {
        OtaTaskSaveResponse((sPopOtaUpgradeRsp_t*)data);
      } else if (data->iCommandId ==
          gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c) {
        OtaTaskNewImageResponse((sPopOtaUpgradeResetRsp_t*)data);
      }

      TuiRadioStatus(eTuiBlink);
      break;
    }
    default:
      // Nothing to do
      break;
  }
}

/**
 * Get the user level
 */
uint8_t MessageGetUserLevel(void) {
  return messageUserLevel;
}

/**
 * Get the next Micropower Access sequence and step it to next index
 */
uint8_t MessageStepMpSequence(void) {
  return messageMpSequence++;
}

// Private functions

/**
 * Handle received message
 * @param data The data
 */
static void MessageTaskHandleReceived(sPopEvtData_t data) {
  sPopGatewayPacket_t* gatewayPacket = (sPopGatewayPacket_t*)data.pData;

  switch (gatewayPacket->header.iEvtId) {
    case gPopEvtKey_c:
    case gPopGwCmdSetLed_c:
    case gPopGwCmdPop_c:
    case gPopGwCmdLcdWriteString_c:
    case gPopGwCmdInitNwkValues_c:
    case gPopGwCmdGetInitNwkValues_c:
    case gPopGwCmdSetChannel_c:
    case gPopGwCmdSetChannelList_c:
    case gPopGwCmdSetPanId_c:
    case gPopGwCmdSetNwkAddr_c:
    case gPopGwCmdSetAddrPool_c:
    case gPopGwCmdGetAddrPool_c:
    case gPopGwCmdSetMacAddr_c:
    case gPopGwCmdSetManufactureId_c:
    case gPopGwCmdSetScanOptions_c:
    case gPopGwCmdSetJoinOptions_c:
    case gPopGwCmdSetScanTime_c:
    case gPopGwCmdNwkScanNetworks_c:
    case gPopGwCmdNwkScanForNoise_c:
    case gPopGwCmdNwkStartNetwork_c:
    case gPopEvtNwkJoinEnable_c:
    case gPopGwCmdNwkLeaveNetwork_c:
    case gPopGwCmdNwkDataRequest_c:
    case gPopGwCmdNwkAddrRequest_c:
    case gPopGwCmdSetNetworkKey_c:
    case gPopGwCmdSendBeacon_c:
    case gPopGwCmdBeaconRequest_c:
    case gPopEvtNwkNodePlacement_c:
    case gPopGwCmdNwkFormNetwork_c:
    case gPopGwCmdNwkJoinNetwork_c:
    case gPopGwCmdSetDefaultAddressPoolSize_c:
    case gPopGwCmdGetJoinStatus_c:
    case gPopGwCmdGetSecurityKey_c:
    case gPopGwCmdGetRoutingSleepingMode_c:
    case gPopGwCmdGetApplicationId_c:
    case gPopGwCmdGetNetworkStatus_c:
    case gPopGwCmdSetApplicationId_c:
    case gPopGwCmdGetPopNetVersion_c:
    case gPopGwCmdSetEvtRange_c:
    case gPopGwCmdHeapCheck_c:
    case gPopGwCmdPopNvStore_c:
    case gPopGwCmdPopNvRetrieve_c:
    case gPopGwCmdPopNvForget_c:
    case gPopGwCmdOtaUpgradeFirstPassThruReq_c:
    case gPopGwCmdOtaUpgradePassThruReq_c:
    case gPopGwNwkMgmtOtaUpgradeCancelReq_c:
    case gPopGwNwkMgmtOtaUpgradeFirstSaveReq_c:
    case gPopGwNwkMgmtOtaUpgradeSaveReq_c:
    case gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c:
    case gPopGwNwkMgmtOtaUpgradeCompleteReq_c:
    case gPopGwCmdGetMaxPayload_c:
    case gPopGwCmdSetICanHearYouTable_c:
    case gPopGwCmdGetHighWaterMarks_c:
    case gPopGwCmdSetSleepyMode_c:
#if DEBUG >= 1
    case gPopAppAssertEvt_c:
    case gPopAppCountEvt_c:
#endif
      break;
    default:
    {
      uint16_t length = gatewayPacket->header.iLen;
      sPopEvtData_t  sEvtData;

      sEvtData.iStatus = gPopErrNone_c;

#if DEBUG >= 2
      gsMpDebug.iGwCmdCnt--;
#endif

      if ((gatewayPacket->header.iEvtId >= gMpStartNetworkReq_c) &&
          (gatewayPacket->header.iEvtId <= gMpSetRadioPowerReq_c)) {
        // Special treatment to keep backward compatibility with CM software
        if (gatewayPacket->header.iEvtId == gMpStartNetworkReq_c) {
          sEvtData.pData = gatewayPacket->payload;
        } else if (gatewayPacket->header.iEvtId == gMpHeartbeatReq_c) {
           if (length == 1) {
             sEvtData.iByte = gatewayPacket->payload[0]; // New CM SW indicates if BBC is active
           } else {
             sEvtData.iByte = 0; // Old CM SW does not send this byte, assume BBC inactive
           }
        } else if (length > 2) {
          sEvtData.pData = gatewayPacket->payload;
        } else if (length == 2) {
          GatewayToNative16((uint16_t *)gatewayPacket->payload);
          sEvtData.iWord = *(uint16_t *)gatewayPacket->payload;
        } else {
          sEvtData.iByte = gatewayPacket->payload[0];
        }

        switch (gatewayPacket->header.iEvtId) {
          case gMpLeaveNetworkReq_c:  // 0x82
            MpGwLeaveNetworkReq();
            break;
          case gMpScanNetworkReq_c: // 0x83
            MpGwScanNetworkReq(sEvtData.pData);
            break;
          case gMpSetRadioParamReq_c: // 0x84
            MpGwSetRadioParamReq(sEvtData.pData);
            break;
          case gMpGetRadioDataReq_c:  // 0x85
            MpGwGetRadioDataRsp();
            break;
          case gMpJoinEnableReq_c:  // 0x86
            MpGwJoinEnableReq(sEvtData.iWord);
            break;
          case gMpNwkDataReq_c: // 0x87
            MpGwNwkDataReq(sEvtData.pData);
            break;
          case gMpNodePlacementReq_c: // 0x89
            MpGwNodePlacementReq();
            break;
          case gMpSetUserLevelReq_c: // 0x8A
          {
            uint8_t iStatus;
            uint8_t iUserLevel;

            iStatus = gPopErrFailed_c;
            iUserLevel = sEvtData.iByte;
            if (gMpSmUserLevelMin_c <= iUserLevel
                && iUserLevel <= gMpSmUserLevelMax_c) { // Store User level to NVM
              if (!PopNvStore(gPopAppUserLevelNvId_c, sizeof(uint8_t),
                  &iUserLevel)) { // if no errors, PopErr_t = gPopErrNone_c = 0, set global User level
                messageUserLevel = iUserLevel;
                iStatus = gPopErrNone_c;
              }
            }
            // send message to Gateway with status
            MpGwSetUserLevelRsp(iStatus);
            break;
          }
          case gMpGetUserLevelReq_c: // 0x8B
          {
            uint8_t iUserLevel;

            iUserLevel = messageUserLevel;
            // check if Userlevel is within limits
            if (!(gMpSmUserLevelMin_c <= iUserLevel
                && iUserLevel <= gMpSmUserLevelMax_c)) { // Set UserLevel to max allowed if outside limits
              iUserLevel = gMpSmUserLevelMax_c;
            }
            // send message to Gateway with user level
            MpGwGetUserLevelRsp(iUserLevel);
            break;
          }
          case gMpScanForNodesReq_c: // 0x8C
          {
            bool fValidNwkData;

            fValidNwkData = MpGwScanForNodesReq(sEvtData.pData);
            if (fValidNwkData) {
              // Send broadcast message to all nodes in network
              PopAppGetAcknowledge();
            }
            break;
          }
          case gMpStartNetworkReq_c:  // 0x80
            PopAppSetNewState(gMpStartNetworkReq_c);
            MpGwStartNetworkReq(sEvtData.pData);
            break;
          case gMpJoinNetworkReq_c: // 0x81
            PopAppSetNewState(gMpJoinNetworkReq_c);
            gfMpBeaconFound = false;
            MpGwJoinNetworkReq(sEvtData.pData);
            break;
          case gMpSetRadioPowerReq_c: // 0x8F
            MpGwSetRadioPowerReq(sEvtData.iByte);
            break;
          default:
            // Nothing to do
            break;
        }
      }
      break;
    }
  }
}

/*******************************************************************************
 ---------------- PopAppGetAcknowledge ------------------------------------------
 Build and send PopAppGetAcknowledge message.
 --------------------------------------------------------------------------------
 *******************************************************************************/
static void PopAppGetAcknowledge(void) {
  sMpMsgHeader_t sMpMsgHeaderReq;
  sMessage_t sMessage;
  uint8_t *pData;

  sMpMsgHeaderReq.iCmd = gSmGetAcknowledge_c;
  sMpMsgHeaderReq.iMpSequence = MessageStepMpSequence();
  sMpMsgHeaderReq.iControl = 0x00;

  // add data
  // no data

  // add network data
  sMessage.iDstAddr = gPopNwkBroadcastAddr_c;
  sMessage.iOptions = gPopNwkDataReqOptsNoDiscover_c | gPopNwkDataReqOptsNoRetry_c;
  sMessage.iRadius = PopNwkGetDefaultRadius() - 1;
  sMessage.iPayloadLength = sizeof(sMpMsgHeader_t);
  pData = sMessage.aPayload;
  PopMemCpy(pData, &sMpMsgHeaderReq, sizeof(sMpMsgHeader_t));
  // send the message
  MpNwkDataRequest(&sMessage);
}
