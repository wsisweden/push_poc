/*
 * OtaTask.c
 *
 *  Created on: 24 apr. 2017
 *      Author: Andreas Carmvall
 */
#include "OtaTask.h"
#include "../MP/MpGw.h"
#include <PopNet/PopNwk.h>
#include <Hw/TIDriverLib/source/systick.h>
#include <Hw/Segger/RTT/SEGGER_RTT.h>

// Private definitions

typedef enum {
  eOtaTaskIdle,
  eOtaTaskInitTransfer,
  eOtaTaskTransfer,
  eOtaTaskFinish
} eOtaTask_t;

// Timer IDs
enum {
  TimerIdOta_Timeout = 1
};

// Private attributes

static eOtaTask_t otaTaskState;
static OtaData_t otaTaskData;
static uint16_t otaTotalBlocks;
static uint8_t otaBlockLength;
extern const popTaskId_t cOtaTaskId;
static const uint16_t otaTaskTransferRetries[] = {
    500, 500, 500, 500, 500,
    500, 500, 500, 200, 100, 50
};
static const uint16_t otaTaskTransferRoutingRetries[] = {
    500, 10000, 500, 10000, 500,
    10000, 500, 10000, 500, 500,
    500
};

// Private function declarations

extern popErr_t MPFlashEraseBlockBlocking(uint32_t address);
static void OtaTaskSetState(const eOtaTask_t newState);
static void OtaTaskSendCanceled(void);
static void OtaTaskTimeout(void);
static void OtaTaskSendInitTransfer(const bool retry);
static void OtaTaskSendTransfer(void);
static void OtaTaskSendFinish(void);
static void OtaTaskClearPopNet(void);

// Public functions

/**
 * Initialize the task
 */
void OtaTaskInit(void) {
  otaBlockLength = PopNwkMaxPayload() - 6;
  otaTaskState = eOtaTaskIdle;
  otaTotalBlocks = 0;
}

/**
 * Handle the task event loop
 * @param iEvtId The event id
 * @param sEvtData The event data
 */
void OtaTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData) {
  switch(iEvtId) {
    case gPopEvtTimer_c:
      switch(sEvtData.iTimerId){
        case TimerIdOta_Timeout:
          OtaTaskTimeout();
          break;
        default:
          break;
      }
      break;
    default:
      // Nothing to do
      break;
  }
}

/**
 * Switch the firmware on target
 * @param address The target address
 * @return If the operation was allowed
 */
popErr_t OtaTaskSwitchTarget(const uint16_t address) {
  if (otaTaskState != eOtaTaskIdle) {
    return gPopErrBusy_c;
  }

  if (gPopErrNone_c !=
      PopStorageReadBytes(&otaTaskData, 0x80000, sizeof(OtaData_t))) {
    OtaTaskSetState(eOtaTaskIdle);
    return gPopErrFailed_c;
  }

  if (otaTaskData.complete != true) {
    OtaTaskSetState(eOtaTaskIdle);
    return gPopErrNoImage_c;
  }

  otaTaskData.target = address;

  if (otaTaskData.localGateway == true) {
    OtaTaskSetState(eOtaTaskFinish);
  } else {
    otaTotalBlocks = (otaTaskData.imageSize + otaBlockLength - 1) /
      otaBlockLength;
    OtaTaskSetState(eOtaTaskInitTransfer);
  }

  return gPopErrNone_c;
}

/**
 * Handle the start OTA message
 * @param applicationId The application ID
 * @param handler The handler ID
 * @param hwId The hardware ID
 * @param imageSize The size of the image
 * @param nvPages The amount if non volatile pages
 * @param platform The platform ID
 * @param preserveNvm If the non volatile pages shall be preserved
 */
void OtaTaskStartMessage(const uint16_t applicationId,
    const uint16_t handler, const uint8_t hwId, const uint32_t imageSize,
    const uint8_t nvPages, const uint8_t platform, const bool preserveNvm,
    const bool localGateway, const uint32_t firmwarePart,
        const uint32_t firmwareRevision) {
  OtaTaskSetState(eOtaTaskIdle);

  otaTaskData.applicationId = applicationId;
  otaTaskData.handler = handler;
  otaTaskData.hwId = hwId;
  otaTaskData.imageSize = imageSize;
  otaTaskData.nvPages = nvPages;
  otaTaskData.platform = platform;
  otaTaskData.preserveNvm = preserveNvm;
  otaTaskData.localGateway = localGateway;
  otaTaskData.firmwarePart = firmwarePart;
  otaTaskData.firmwareRevision = firmwareRevision;
  otaTaskData.complete = false;

  MPFlashEraseBlockBlocking(0x80000);

  if (gPopErrNone_c
      != PopStorageWriteBytes(0x80000, &otaTaskData, sizeof(OtaData_t))) {
    SEGGER_RTT_printf(0, "%d, OTA failed to save image info\r\n",
        PopGetSystick());
    OtaTaskSetState(eOtaTaskIdle);
    return;
  }

  SEGGER_RTT_printf(0, "%d, OTA task start, handler: %d\r\n",
      PopGetSystick(), handler);
}

/**
 * Check if last message
 * @param blockNumber The block number
 * @param blockSize The block size
 */
void OtaTaskCheckLastMessage(const uint16_t blockNumber,
    const uint16_t blockSize) {
  uint32_t writtenSize = blockNumber * blockSize;

  if (writtenSize >= otaTaskData.imageSize) {
    otaTaskData.complete = true;
    MPFlashEraseBlockBlocking(0x80000);
    PopStorageWriteBytes(0x80000, &otaTaskData, sizeof(OtaData_t));
  }
}

/**
 * Abort current OTA
 */
void OtaTaskAbort(void) {
  OtaTaskSetState(eOtaTaskIdle);
}

/**
 * Handle save block response
 * @param data The answer
 */
void OtaTaskSaveResponse(const sPopOtaUpgradeRsp_t* data) {
  uint8_t payload[100];
  sPopNwkDataIndication_t* indication = (sPopNwkDataIndication_t*)payload;
  popOtauBlocksCount_t blockNumber = data->blockNumber;

  indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
  indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gMpOtaBlockIndication_c;
  indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
  indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_H] =
      (blockNumber >> 8) & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_L] =
      blockNumber & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_TOTAL_H] =
      (otaTotalBlocks >> 8) & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_TOTAL_L] =
      otaTotalBlocks & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_ERROR] =
      data->otaUpgradeError.errType;
  indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
      sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_SIZE;

  SEGGER_RTT_printf(0,
      "%d, OTA task save response, type: %d, error: %d, block: %d\r\n",
      PopGetSystick(), data->rspType, data->otaUpgradeError.errType,
      blockNumber);

  if (data->otaUpgradeError.errType == 0x30) { // Wrong block, try to send previous
    otaTaskData.block -= 2;
    blockNumber = data->otaUpgradeError.errInfo.blockNumber;
  } else if (data->otaUpgradeError.errType != 0) {
    SEGGER_RTT_printf(0, "%d, OTA abort, received error\r\n", PopGetSystick());
    OtaTaskSetState(eOtaTaskIdle);
    indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_ERROR] = data->otaUpgradeError.errType;
    MpGwDataIndication(indication);
    return;
  }

  if (blockNumber < otaTaskData.block) {
    SEGGER_RTT_printf(0,
        "%d, OTA response, ignore already received block\r\n",
        PopGetSystick());
    MpGwDataIndication(indication);
    return;
  }

  otaTaskData.block++;

  if (data->otaUpgradeError.errType == 0x30) {
    if ((otaTaskData.retries == 0) && otaTaskData.routing) {
      OtaTaskSetState(eOtaTaskIdle);
      indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_ERROR] = gPopErrCanceled_c;
      MpGwDataIndication(indication);
      return;
    } else if (otaTaskData.retries == 0) {
      otaTaskData.routing = true;
      otaTaskData.retries = 10;
    } else {
      otaTaskData.retries--;
    }
  } else {
    otaTaskData.retries = 10;
  }

  MpGwDataIndication(indication);

  if (otaTaskState == eOtaTaskInitTransfer) {
    OtaTaskSetState(eOtaTaskTransfer);
  } else if (otaTaskState == eOtaTaskTransfer) {
    if (blockNumber * otaBlockLength >= otaTaskData.imageSize) {
      OtaTaskSetState(eOtaTaskFinish);
    } else {
      OtaTaskSendTransfer();
    }
  }
}

/**
 * Handle new image response
 * @param data The answer
 */
void OtaTaskNewImageResponse(const sPopOtaUpgradeResetRsp_t* data) {
  uint8_t payload[100];
  sPopNwkDataIndication_t* indication = (sPopNwkDataIndication_t*)payload;

  SEGGER_RTT_printf(0,
      "%d, OTA task new image response, type: %d, error: %d\r\n",
      PopGetSystick(), data->rspType, data->errType);

  if (otaTaskState == eOtaTaskFinish) {
    OtaTaskSetState(eOtaTaskIdle);
  }

  indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
  indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gMpOtaSwitchIndication_c;
  indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
  indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_SWITCH_INDICATION_ERROR] =
      data->errType;
  indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
      sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + MP_OTA_SWITCH_INDICATION_SIZE;
  MpGwDataIndication(indication);
}

// Private functions

/**
 * Set the new state
 * @param newState The new state
 */
static void OtaTaskSetState(const eOtaTask_t newState) {
  otaTaskState = newState;

  switch (otaTaskState) {
    case eOtaTaskIdle:
      PopNwkMgmtSetOtaEnabled(true);
      break;
    case eOtaTaskInitTransfer:
      otaTaskData.block = 0;
      otaTaskData.retries = 10;
      otaTaskData.routing = false;
      PopNwkMgmtSetOtaEnabled(false);

      // Clear Route Table to send directly to node
      PopNwkDeleteEntireRouteTable();

      OtaTaskSendInitTransfer(false);
      break;
    case eOtaTaskTransfer:
      OtaTaskSendTransfer();
      break;
    case eOtaTaskFinish:
      OtaTaskSendFinish();
      break;
    default:
      // Should not happen
      break;
  }
}

/**
 * Send canceled indication
 */
static void OtaTaskSendCanceled(void) {
  uint8_t payload[100];
  sPopNwkDataIndication_t* indication = (sPopNwkDataIndication_t*)payload;

  indication->sFrame.sNwk.iSrcAddr = PopNwkGetNodeAddr();
  indication->sFrame.aPayload[MP_ACCESS_COMMAND] = gMpOtaBlockIndication_c;
  indication->sFrame.aPayload[MP_ACCESS_SEQUENCE] = 0;
  indication->sFrame.aPayload[MP_ACCESS_SEGMENT] = 0;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_H] =
      (otaTaskData.block >> 8) & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_L] =
      otaTaskData.block & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_TOTAL_H] =
      (otaTotalBlocks >> 8) & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_NUMBER_TOTAL_L] =
      otaTotalBlocks & 0xff;
  indication->sFrame.aPayload[MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_ERROR] =
      gPopErrCanceled_c;
  indication->sPre.iFrameLen = sizeof(sMacDataFrame_t) +
      sizeof(sNwkDataFrame_t) + MP_ACCESS_DATA + MP_OTA_BLOCK_INDICATION_SIZE;

  MpGwDataIndication(indication);
}

/**
 * Handle timeout
 */
static void OtaTaskTimeout(void) {
  if ((otaTaskData.retries == 0) && otaTaskData.routing) {
    OtaTaskSendCanceled();
    OtaTaskSetState(eOtaTaskIdle);
    return;
  } else if (otaTaskData.retries == 0) {
    otaTaskData.routing = true;
    otaTaskData.retries = 10;
  } else {
    otaTaskData.retries--;
  }

  switch (otaTaskState) {
    case eOtaTaskIdle:
      // Nothing to do
      break;
    case eOtaTaskInitTransfer:
      OtaTaskSendInitTransfer(true);
      break;
    case eOtaTaskTransfer:
      PopNwkFreeEntireRetryTable();
      OtaTaskSendTransfer();
      break;
    case eOtaTaskFinish:
      OtaTaskSendFinish();
      break;
    default:
      // Should not happen
      break;
  }
}

/**
 * Send init transfer message
 */
static void OtaTaskSendInitTransfer(const bool retry) {
  sPopOtauOtaFirstBlockPassThruSaveReq_t data;
  sPopNwkDataRequest_t dataRequest;

  dataRequest.iDstAddr = otaTaskData.target;

  if (retry)
    dataRequest.iOptions = gPopNwkDataReqOptsForce_c;
  else
  dataRequest.iOptions = gPopNwkDataReqOptsNoDiscover_c;
  dataRequest.iRadius = PopNwkGetDefaultRadius();
  dataRequest.pPayload = &data;
  dataRequest.iPayloadLength = sizeof(sPopOtauOtaFirstBlockPassThruSaveReq_t);

  data.reqType = gPopGwNwkMgmtOtaUpgradeFirstSaveReq_c;
  data.appId = otaTaskData.applicationId;
  data.blockNumber = otaTaskData.block;
  data.blockSize = otaBlockLength;
  data.checksum = 0;
  data.handler = otaTaskData.handler;
  data.hwId = otaTaskData.hwId;
  data.imageSize = otaTaskData.imageSize;
  data.nvPages = otaTaskData.nvPages;
  data.platform = otaTaskData.platform;
  data.preserveNvm = otaTaskData.preserveNvm;

  OtaTaskClearPopNet();
  SEGGER_RTT_printf(0, "%d, OTA task init transfer, status: %d\r\n",
      PopGetSystick(), PopNwkMgmtDataRequest(&dataRequest, false));
  PopStartTimerEx(cOtaTaskId, TimerIdOta_Timeout, 14000);
}

/**
 * Send transfer message
 */
static void OtaTaskSendTransfer(void) {
  uint_fast8_t i;
  uint8_t payloadLength = sizeof(sPopOtauOtaSubseqBlockPassThruSaveReq_t) +
      otaBlockLength - 1;
  uint8_t buffer[150];
  sPopOtauOtaSubseqBlockPassThruSaveReq_t* data =
      (sPopOtauOtaSubseqBlockPassThruSaveReq_t*)buffer;
  sPopNwkDataRequest_t dataRequest;

  if (otaTaskData.routing) {
    if ((otaTaskData.retries < 8) && (otaTaskData.retries % 2)) {
      dataRequest.iOptions = gPopNwkDataReqOptsForce_c;
    } else {
      dataRequest.iOptions = gPopNwkDataReqOptsNoDiscover_c;
    }

    dataRequest.iRadius = PopNwkGetDefaultRadius();
  } else {
    dataRequest.iOptions = gPopNwkDataReqOptsNoDiscover_c |
        gPopNwkDataReqOptsNoRetry_c;
    dataRequest.iRadius = 1;
  }

  dataRequest.iDstAddr = otaTaskData.target;
  dataRequest.pPayload = data;
  dataRequest.iPayloadLength = payloadLength;

  data->reqType = gPopGwNwkMgmtOtaUpgradeSaveReq_c;
  data->blockNumber = otaTaskData.block;
  data->checksum = 0;
  data->handler = otaTaskData.handler;

  if (otaTaskData.block == 0) {
    SEGGER_RTT_printf(0,
        "%d, OTA trying to send block 0, should be sent with init command\r\n",
        PopGetSystick());
    OtaTaskSetState(eOtaTaskIdle);
    return;
  }

  if (gPopErrNone_c != PopStorageReadBytes(data->imgBlock,
      ((otaTaskData.block - 1) * otaBlockLength), otaBlockLength)) {
    OtaTaskSetState(eOtaTaskIdle);
    return;
  }

  if (otaTaskData.block * otaBlockLength > otaTaskData.imageSize) {
    // Calculate padding space, total block size - image size - pre and post
    // bytes
    uint8_t padding = (uint8_t)
        ((otaTaskData.block * otaBlockLength) - otaTaskData.imageSize - 9 - 6);

    for (i = otaBlockLength - padding; i < otaBlockLength; ++i) {
      data->imgBlock[i] = 0;
    }
  }

  for (i = 0; i < otaBlockLength; ++i) {
    data->checksum ^= data->imgBlock[i];
  }

  OtaTaskClearPopNet();
  SEGGER_RTT_printf(0, "%d, OTA task transfer, block: %d, status: %d\r\n",
      PopGetSystick(), otaTaskData.block,
      PopNwkMgmtDataRequest(&dataRequest, false));

  if (otaTaskData.routing) {
    PopStartTimerEx(cOtaTaskId, TimerIdOta_Timeout,
        otaTaskTransferRoutingRetries[otaTaskData.retries]);
  } else {
    PopStartTimerEx(cOtaTaskId, TimerIdOta_Timeout,
        otaTaskTransferRetries[otaTaskData.retries]);
  }
}

/**
 * Send finish message
 */
static void OtaTaskSendFinish(void) {
  if (otaTaskData.localGateway == true) {
    sPopGwResetToNewImageReq_t temp;
    sPopOtaUpgradeResetRsp_t data;
    popErr_t status;

    temp.appId = otaTaskData.applicationId;
    temp.handler = otaTaskData.handler;
    temp.hwId = otaTaskData.hwId;
    temp.platform = otaTaskData.platform;
    temp.reqType = 0;

    MPFlashEraseBlockBlocking(0x80000);
    status = PopNwkMgmtOtaUpgradeResetToNewImage(&temp);

    SEGGER_RTT_printf(0, "%d, OTA task finish, status: %d\r\n",
        PopGetSystick(), status);

    data.errType = status;
    data.rspType = gPopGwNwkMgmtOtaUpgradeResetToNewImageRsp_c;
    OtaTaskNewImageResponse(&data);
  } else {
    sPopNwkDataRequest_t dataRequest;
    sPopOtaResetToNewImageReq_t payload;

    if (otaTaskData.retries < 5) {
      dataRequest.iOptions = gPopNwkDataReqOptsForce_c;
    } else {
      dataRequest.iOptions = gPopNwkDataReqOptsNoDiscover_c;
    }

    dataRequest.iRadius = PopNwkGetDefaultRadius();
    dataRequest.iDstAddr = otaTaskData.target;
    dataRequest.iPayloadLength = sizeof(sPopOtaResetToNewImageReq_t);
    dataRequest.pPayload = &payload;

    payload.reqType = gPopGwNwkMgmtOtaUpgradeResetToNewImageReq_c;
    payload.appId = otaTaskData.applicationId;
    payload.handler = otaTaskData.handler;
    payload.hwId = otaTaskData.hwId;
    payload.platform = otaTaskData.platform;

    OtaTaskClearPopNet();
    SEGGER_RTT_printf(0, "%d, OTA task finish, status: %d\r\n",
        PopGetSystick(), PopNwkMgmtDataRequest(&dataRequest,false));
    PopStartTimerEx(cOtaTaskId, TimerIdOta_Timeout, 14000);
  }
}

/**
 * Clear PopNet data
 */
static void OtaTaskClearPopNet(void) {
  // Clear retry table
  PopNwkFreeEntireRetryTable();

  // Clear duplicated table
  PopNwkFreeEntireDuplicateTable();

  // Clear both Route Discovery and Route Discovery Timer Id's Table
  PopNwkFreeEntireRouteDiscoveryTable();
}
