#pragma once

#include <PopNet/PopBsp.h>
#include <PopNet/PopNet.h>

// Public definitions

typedef struct {
  uint16_t target;
  uint16_t block;
  uint8_t retries;
  bool routing;
  uint16_t applicationId;
  uint16_t handler;
  uint8_t hwId;
  uint32_t imageSize;
  uint8_t nvPages;
  uint8_t platform;
  bool preserveNvm;
  bool localGateway;
  uint32_t firmwarePart;
  uint32_t firmwareRevision;
  bool complete;
} OtaData_t;

// Public functions

void OtaTaskInit(void);
void OtaTaskEventLoop(popEvtId_t iEvtId, sPopEvtData_t sEvtData);
popErr_t OtaTaskSwitchTarget(const uint16_t address);
void OtaTaskStartMessage(const uint16_t applicationId,
    const uint16_t handler, const uint8_t hwId, const uint32_t imageSize,
    const uint8_t nvPages, const uint8_t platform, const bool preserveNvm,
    const bool localGateway, const uint32_t firmwarePart,
    const uint32_t firmwareRevision);
void OtaTaskCheckLastMessage(const uint16_t blockNumber,
    const uint16_t blockSize);
void OtaTaskAbort(void);
void OtaTaskSaveResponse(const sPopOtaUpgradeRsp_t* data);
void OtaTaskNewImageResponse(const sPopOtaUpgradeResetRsp_t* data);
