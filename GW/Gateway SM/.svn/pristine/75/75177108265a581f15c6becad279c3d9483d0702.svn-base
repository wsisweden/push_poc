#include "../../Src/Nfc/Driver/Nt3h2111_I2c.h"
#include "../../Src/Nfc/Driver/Nt3h2111_Defines.h"
#include "Nt3h2111_I2c_test.h"
#include <string.h>

#define REGISTER_COUNT 8
#define BLOCK_COUNT 0xff
static uint8_t registerData[REGISTER_COUNT];
static uint8_t blockData[BLOCK_COUNT][NT3H2111_BLOCK_SIZE];


void Nt3h2111_I2c_init(void) {
  memset(registerData, 0, REGISTER_COUNT);
  memset(blockData, 0, BLOCK_COUNT * NT3H2111_BLOCK_SIZE);
}

void Nt3h2111_I2c_setFdInterrupt(const bool enable) {
  (void)enable;
}

bool Nt3h2111_I2c_readRegister(const uint8_t reg, uint8_t* const value) {
  if (reg >= REGISTER_COUNT)
    return false;

  *value = registerData[reg];
  return true;
}

bool Nt3h2111_I2c_writeRegister(const uint8_t reg, const uint8_t mask,
    const uint8_t value) {
  if (reg >= REGISTER_COUNT)
      return false;

  registerData[reg] &= ~mask;
  registerData[reg] |= value;
  return true;
}

bool Nt3h2111_I2c_readBlock(const uint8_t address, uint8_t* const buf) {
  memcpy(buf, blockData[address], NT3H2111_BLOCK_SIZE);
  return true;
}

bool Nt3h2111_I2c_writeBlock(const uint8_t address, const uint8_t* const buf) {
  memcpy(blockData[address], buf, NT3H2111_BLOCK_SIZE);
  return true;
}
